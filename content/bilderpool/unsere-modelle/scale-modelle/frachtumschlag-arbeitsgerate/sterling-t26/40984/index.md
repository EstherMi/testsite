---
layout: "image"
title: "Sterling T26"
date: "2015-05-16T19:01:51"
picture: "P5160129.jpg"
weight: "4"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/40984
imported:
- "2019"
_4images_image_id: "40984"
_4images_cat_id: "3078"
_4images_user_id: "838"
_4images_image_date: "2015-05-16T19:01:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40984 -->
Onafhankelijke vering op alle assen/wielen