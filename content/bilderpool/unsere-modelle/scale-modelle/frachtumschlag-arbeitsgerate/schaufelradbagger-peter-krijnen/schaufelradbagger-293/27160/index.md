---
layout: "image"
title: "Frame des Abgabegarät_5"
date: "2010-05-08T20:05:19"
picture: "schaufelradbagger101.jpg"
weight: "101"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/27160
imported:
- "2019"
_4images_image_id: "27160"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:19"
_4images_image_order: "101"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27160 -->
Neben ein drehlagerpunkt zur steuerung der Raupen, gibts auch ein lager damit die Raupen auch auf Unebene strecken farhen können.