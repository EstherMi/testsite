---
layout: "image"
title: "S-Riegel"
date: "2010-05-08T20:05:20"
picture: "schaufelradbagger128.jpg"
weight: "128"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/27187
imported:
- "2019"
_4images_image_id: "27187"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:20"
_4images_image_order: "128"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27187 -->
31983 - 101x
36323 - 635x
36324 - 126x
36334 - 232x
36457 - 85x