---
layout: "image"
title: "Kran 3-6"
date: "2010-05-08T20:05:17"
picture: "schaufelradbagger025.jpg"
weight: "25"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/27084
imported:
- "2019"
_4images_image_id: "27084"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:17"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27084 -->
