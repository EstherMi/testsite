---
layout: "image"
title: "Raupen"
date: "2010-05-08T20:05:20"
picture: "schaufelradbagger129.jpg"
weight: "129"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/27188
imported:
- "2019"
_4images_image_id: "27188"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:20"
_4images_image_order: "129"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27188 -->
36263 - 3150x
37192 - 3150x
37210 - 1890x