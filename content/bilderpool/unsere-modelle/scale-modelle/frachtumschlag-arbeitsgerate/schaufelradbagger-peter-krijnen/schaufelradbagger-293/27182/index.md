---
layout: "image"
title: "Neue Verkleidungsplatten"
date: "2010-05-08T20:05:20"
picture: "schaufelradbagger123.jpg"
weight: "123"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/27182
imported:
- "2019"
_4images_image_id: "27182"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:20"
_4images_image_order: "123"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27182 -->
31013 - 4x
31555 - 2x
38241 - 40x
38242 - 61x
38244 - 23x
38245 - 34x
38246 - 16x
38247 - 9x
38248 - 33x
38249 - 23x
38251 - 45x
38259 - 18x
38277 - 1x
38464 - 13x (2 Zapfen)
38646 - 9x (4 Zapfen)