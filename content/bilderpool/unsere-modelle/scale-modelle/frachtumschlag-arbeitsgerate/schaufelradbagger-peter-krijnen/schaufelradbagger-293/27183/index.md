---
layout: "image"
title: "Winkelträger"
date: "2010-05-08T20:05:20"
picture: "schaufelradbagger124.jpg"
weight: "124"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/27183
imported:
- "2019"
_4images_image_id: "27183"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:20"
_4images_image_order: "124"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27183 -->
32070 - 1x
36300 - 25x
36301 - 74x
36302 - 40x
36303 - 111x
36304 - 32x
36305 - 13x