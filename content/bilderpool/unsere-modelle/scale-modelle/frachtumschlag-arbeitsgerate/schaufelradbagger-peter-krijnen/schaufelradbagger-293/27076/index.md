---
layout: "image"
title: "Kran 2_5"
date: "2010-05-08T20:05:17"
picture: "schaufelradbagger017.jpg"
weight: "17"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/27076
imported:
- "2019"
_4images_image_id: "27076"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:17"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27076 -->
Zur gleichlauf der 4 Seilwinden ist ein nicht gans einfachen regelung in der Software (Profilab Expert von Abacom) eingebaut. Als erstes in diesen regelkreis sind 4 Gabellichtschranken CNY71 eingebaut die von je einen, auf eine Seilrolle 23 angeklebten, unterbrecherscheibe unterbrochen werden.