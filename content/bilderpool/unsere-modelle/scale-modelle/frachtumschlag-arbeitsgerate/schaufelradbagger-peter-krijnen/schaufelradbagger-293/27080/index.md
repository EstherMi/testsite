---
layout: "image"
title: "Kran 3-2"
date: "2010-05-08T20:05:17"
picture: "schaufelradbagger021.jpg"
weight: "21"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/27080
imported:
- "2019"
_4images_image_id: "27080"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:17"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27080 -->
