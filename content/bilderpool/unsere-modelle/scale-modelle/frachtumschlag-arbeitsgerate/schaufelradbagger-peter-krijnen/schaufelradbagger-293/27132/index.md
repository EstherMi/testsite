---
layout: "image"
title: "frame der Bandschleife 4_2"
date: "2010-05-08T20:05:18"
picture: "schaufelradbagger073.jpg"
weight: "73"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/27132
imported:
- "2019"
_4images_image_id: "27132"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:18"
_4images_image_order: "73"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27132 -->
