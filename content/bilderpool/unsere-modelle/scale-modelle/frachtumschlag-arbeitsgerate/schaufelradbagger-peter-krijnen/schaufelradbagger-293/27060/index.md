---
layout: "image"
title: "Schaufelradbagger 293_1"
date: "2010-05-08T20:05:16"
picture: "schaufelradbagger001.jpg"
weight: "1"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/27060
imported:
- "2019"
_4images_image_id: "27060"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:16"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27060 -->
Modell in Maßstab 1:80 von Schaufelradbager 293 der RWE (Braunkohle AG).
Das Foto's ist beim treffen in Schoonhoven 2006 gemacht.