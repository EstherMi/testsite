---
layout: "image"
title: "Raupenschiffen 1"
date: "2010-05-08T20:05:19"
picture: "schaufelradbagger102.jpg"
weight: "102"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/27161
imported:
- "2019"
_4images_image_id: "27161"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:19"
_4images_image_order: "102"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27161 -->
8 Raupenschiffen hinter einander