---
layout: "image"
title: "Kabinen_5"
date: "2010-05-08T20:05:20"
picture: "schaufelradbagger141.jpg"
weight: "141"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/27200
imported:
- "2019"
_4images_image_id: "27200"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:20"
_4images_image_order: "141"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27200 -->
Höhenverstelung der Kabinen mittels Flaschesuch