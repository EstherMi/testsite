---
layout: "image"
title: "Seilen_1"
date: "2010-05-08T20:05:17"
picture: "schaufelradbagger026.jpg"
weight: "26"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/27085
imported:
- "2019"
_4images_image_id: "27085"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:17"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27085 -->
