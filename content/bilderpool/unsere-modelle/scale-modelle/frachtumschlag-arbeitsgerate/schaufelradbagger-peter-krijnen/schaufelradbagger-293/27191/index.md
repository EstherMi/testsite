---
layout: "image"
title: "Ausleger_2"
date: "2010-05-08T20:05:20"
picture: "schaufelradbagger132.jpg"
weight: "132"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/27191
imported:
- "2019"
_4images_image_id: "27191"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:20"
_4images_image_order: "132"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27191 -->
Hier zwischen solte ein 4,5cm breiten und 27cm durchmesser aufweisenden Schaufelrad kommen: hat leider nicht geklapt.