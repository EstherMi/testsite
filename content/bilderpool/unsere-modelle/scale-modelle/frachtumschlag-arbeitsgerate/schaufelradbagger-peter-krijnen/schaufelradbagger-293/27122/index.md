---
layout: "image"
title: "Baggerbrücke_15"
date: "2010-05-08T20:05:18"
picture: "schaufelradbagger063.jpg"
weight: "63"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/27122
imported:
- "2019"
_4images_image_id: "27122"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:18"
_4images_image_order: "63"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27122 -->
