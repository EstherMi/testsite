---
layout: "image"
title: "Gegengewichtausleger_11"
date: "2010-05-08T20:05:18"
picture: "schaufelradbagger044.jpg"
weight: "44"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/27103
imported:
- "2019"
_4images_image_id: "27103"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:18"
_4images_image_order: "44"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27103 -->
