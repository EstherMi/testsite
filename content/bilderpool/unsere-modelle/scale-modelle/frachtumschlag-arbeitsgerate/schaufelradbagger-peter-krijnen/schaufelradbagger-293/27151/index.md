---
layout: "image"
title: "Abgabegerät_11"
date: "2010-05-08T20:05:19"
picture: "schaufelradbagger092.jpg"
weight: "92"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/27151
imported:
- "2019"
_4images_image_id: "27151"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:19"
_4images_image_order: "92"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27151 -->
