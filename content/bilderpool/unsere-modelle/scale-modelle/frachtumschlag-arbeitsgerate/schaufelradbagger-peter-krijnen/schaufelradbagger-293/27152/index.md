---
layout: "image"
title: "Steuercentrale_5"
date: "2010-05-08T20:05:19"
picture: "schaufelradbagger093.jpg"
weight: "93"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/27152
imported:
- "2019"
_4images_image_id: "27152"
_4images_cat_id: "1951"
_4images_user_id: "144"
_4images_image_date: "2010-05-08T20:05:19"
_4images_image_order: "93"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27152 -->
Die beide letsten ROBO Ext