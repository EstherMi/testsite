---
layout: "image"
title: "rotator kop"
date: "2011-03-07T19:27:09"
picture: "pivot_020.jpg"
weight: "20"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/30236
imported:
- "2019"
_4images_image_id: "30236"
_4images_cat_id: "2245"
_4images_user_id: "838"
_4images_image_date: "2011-03-07T19:27:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30236 -->
Kop kan roteren en met 4 pennen een container vastzetten. Kan net als in het echt tot 4 hoog stapelen