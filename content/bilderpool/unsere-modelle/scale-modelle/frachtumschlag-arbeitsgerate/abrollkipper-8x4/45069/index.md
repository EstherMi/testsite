---
layout: "image"
title: "Abrollkipper Antrieb"
date: "2017-01-16T22:06:04"
picture: "abrollkipperx23.jpg"
weight: "25"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/45069
imported:
- "2019"
_4images_image_id: "45069"
_4images_cat_id: "3351"
_4images_user_id: "162"
_4images_image_date: "2017-01-16T22:06:04"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45069 -->
Gesamtbild vom Antrieb.