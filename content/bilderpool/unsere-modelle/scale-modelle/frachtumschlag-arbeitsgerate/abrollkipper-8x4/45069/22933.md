---
layout: "comment"
hidden: true
title: "22933"
date: "2017-01-20T11:31:20"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Hast du da verschiedene Übersetzungen zwischen Vorderachsen und Hinterachsen? Hinten sind Untersetzungen 1:3 drin, die es vorne nicht gibt, d.h. da müsste vom Mitteldifferenzial aus nach vorne auch eine Untersetzung 1:3 eingebaut werden.

Gruß,
Harald