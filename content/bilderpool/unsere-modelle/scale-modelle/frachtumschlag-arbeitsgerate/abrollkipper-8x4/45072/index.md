---
layout: "image"
title: "Antrieb Hinterachse"
date: "2017-01-16T22:06:04"
picture: "abrollkipperx26.jpg"
weight: "28"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/45072
imported:
- "2019"
_4images_image_id: "45072"
_4images_cat_id: "3351"
_4images_user_id: "162"
_4images_image_date: "2017-01-16T22:06:04"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45072 -->
An der durchgehenden Achse welche durch denn erste Schnecke geht, ist eine Achse mittels Kardangelenke verbunden nach der hintersten Achse. Diese ist ähnlich aufgebaut. Weil der Federung weg nicht viel ist, habe ich keine Verlängerung Kompensation gebraucht wie eine normale Kardanwelle (ich suche mich noch immer so ein Kleinteil der das kann)