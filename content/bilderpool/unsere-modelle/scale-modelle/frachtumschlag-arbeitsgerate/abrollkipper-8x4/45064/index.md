---
layout: "image"
title: "Mulde 1"
date: "2017-01-16T22:06:04"
picture: "abrollkipperx18.jpg"
weight: "20"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/45064
imported:
- "2019"
_4images_image_id: "45064"
_4images_cat_id: "3351"
_4images_user_id: "162"
_4images_image_date: "2017-01-16T22:06:04"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45064 -->
Der Boden von der Mulde ist aufgebaut aus 48 Stuck Grundbauplatten 45x90.

Eigentlich ist er etwas zu schwer geworden für den Abrollkipper um es einfach zu heben.