---
layout: "image"
title: "Abrollkipper Mulde"
date: "2017-01-16T22:06:04"
picture: "abrollkipperx09.jpg"
weight: "11"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/45055
imported:
- "2019"
_4images_image_id: "45055"
_4images_cat_id: "3351"
_4images_user_id: "162"
_4images_image_date: "2017-01-16T22:06:04"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45055 -->
Als erste wird die Mulde ein stuck nach hinten geschoben.