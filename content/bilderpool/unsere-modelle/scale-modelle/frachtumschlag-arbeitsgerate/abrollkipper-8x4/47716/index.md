---
layout: "image"
title: "Rollkipper vs Muldenkipper"
date: "2018-07-07T06:54:28"
picture: "muldenkipper2.jpg"
weight: "2"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/47716
imported:
- "2019"
_4images_image_id: "47716"
_4images_cat_id: "3351"
_4images_user_id: "162"
_4images_image_date: "2018-07-07T06:54:28"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47716 -->
Habe meinen Abrollkipper noch ein bisschen umgebaut damit es auch ein Muldenkipper werden kann.
Im Abroll systeem habe ich jetzt noch ein motorisierte Sperrung gemacht. Wann diese activiert ist, dan kann ich mitttels die gleiche Zylinder denn Mulde auch noch Kippen.