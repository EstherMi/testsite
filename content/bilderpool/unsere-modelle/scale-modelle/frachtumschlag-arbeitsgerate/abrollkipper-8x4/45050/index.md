---
layout: "image"
title: "Abrollkipper Sitz"
date: "2017-01-16T22:06:04"
picture: "abrollkipperx04.jpg"
weight: "6"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/45050
imported:
- "2019"
_4images_image_id: "45050"
_4images_cat_id: "3351"
_4images_user_id: "162"
_4images_image_date: "2017-01-16T22:06:04"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45050 -->
Hier eines deutlichen Bild vom Sitz. Das Lenkrad habe ich von ein stuck Luftschlauch gemacht und bestückt mit Abstandringe