---
layout: "image"
title: "Abrollkipper"
date: "2017-01-16T22:06:04"
picture: "abrollkipperx01.jpg"
weight: "3"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/45047
imported:
- "2019"
_4images_image_id: "45047"
_4images_cat_id: "3351"
_4images_user_id: "162"
_4images_image_date: "2017-01-16T22:06:04"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45047 -->
Hier ein Gesamtbild von dem Abrollkipper. Als erste wollte ich den LKW ins Gelb bauen, aber ich dachte das es mit Gelb zu viele Teile geben wird, der nicht ins Gelb verfügbar sind das ich trotzdem noch viele Rote Teile einbauen musste, das ich mich entscheiden habe das Mahl ins Rot zu bauen.

Das Modell hat allrad Antrieb und ist auch noch gefedert.