---
layout: "image"
title: "Abrollkipper Kabine geöffnet"
date: "2017-01-16T22:06:04"
picture: "abrollkipperx06.jpg"
weight: "8"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/45052
imported:
- "2019"
_4images_image_id: "45052"
_4images_cat_id: "3351"
_4images_user_id: "162"
_4images_image_date: "2017-01-16T22:06:04"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45052 -->
Unten die Kabine stecken 2 1:50 power Motoren. Diese 2 Motoren treiben die Hubzylinder. Diese 2 Motoren werden ziemlich schwer belastet in das erste Teil von der hub.