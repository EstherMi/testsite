---
layout: "image"
title: "Reachstacker at Modelshow europa 2008"
date: "2008-05-12T21:02:06"
picture: "bemmel6.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/14506
imported:
- "2019"
_4images_image_id: "14506"
_4images_cat_id: "1279"
_4images_user_id: "162"
_4images_image_date: "2008-05-12T21:02:06"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14506 -->
