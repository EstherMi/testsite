---
layout: "image"
title: "EMD SD40-2. 3"
date: "2016-06-12T19:48:23"
picture: "emdsd03.jpg"
weight: "27"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/43742
imported:
- "2019"
_4images_image_id: "43742"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-06-12T19:48:23"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43742 -->
Sicht auf der Kabine mit 3 Ton Horn, aber noch ohne Betriebsnummer an der seite. Die Nummerkasten brauchen noch Led's.