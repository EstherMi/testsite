---
layout: "image"
title: "EMD SD40-2. 38  CP5419"
date: "2016-07-12T18:05:26"
picture: "emdsd8.jpg"
weight: "22"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/43877
imported:
- "2019"
_4images_image_id: "43877"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-07-12T18:05:26"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43877 -->
Sieht man der pin nach oben raus, entriegeld man die Kupplung.