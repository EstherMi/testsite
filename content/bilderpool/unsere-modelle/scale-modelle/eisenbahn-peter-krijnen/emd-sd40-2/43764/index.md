---
layout: "image"
title: "EMD SD40-2. 25"
date: "2016-06-12T19:48:23"
picture: "emdsd25.jpg"
weight: "49"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/43764
imported:
- "2019"
_4images_image_id: "43764"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-06-12T19:48:23"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43764 -->
LED halter besteht aus:
#35831 Gelenkstein 22,5 schwarz
#35019 Gelenkmutter schwarz