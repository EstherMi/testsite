---
layout: "image"
title: "EMD SD40-2. 48  CP5419"
date: "2016-12-09T21:08:28"
picture: "emdsd09_2.jpg"
weight: "9"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/44872
imported:
- "2019"
_4images_image_id: "44872"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-12-09T21:08:28"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44872 -->
Der Platine mit Knetmasse auf ein V-Bauplatte 15x30 geklebt.