---
layout: "image"
title: "EMD SD40-2. 21"
date: "2016-06-12T19:48:23"
picture: "emdsd21.jpg"
weight: "45"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/43760
imported:
- "2019"
_4images_image_id: "43760"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-06-12T19:48:23"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43760 -->
Drehgestel von unten:
3x Tracktormotor #151178.
Die Achsen sind gelagert in #32064. Da aber das ganse Modell doch simlich schwehr geraten ist, werde ich doch Kogellager einbauen.