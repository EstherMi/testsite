---
layout: "image"
title: "EMD SD40-2. 1"
date: "2016-06-12T19:48:22"
picture: "emdsd01.jpg"
weight: "25"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/43740
imported:
- "2019"
_4images_image_id: "43740"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-06-12T19:48:22"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43740 -->
Immer wen ich mir dieses Bild ansehe, frage ich mir: was sol ich hier blos als Beschreibung ein typen.
- das das mein 3er 1:16 Maßstabmodell ist?
- das das mein erste Ami Lok ist?
- das der GROß ist?
- und SCHWER?
- und das der dabei auch gans viel LÄRM macht?
- oder das der einfach "KRAFT" ausstralt?
- oder das das Bild etwas unscharf ist?

Frage also doch selber beantworted.