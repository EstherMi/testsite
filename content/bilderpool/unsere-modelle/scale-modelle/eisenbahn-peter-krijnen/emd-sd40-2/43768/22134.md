---
layout: "comment"
hidden: true
title: "22134"
date: "2016-06-14T13:53:51"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ach jetzt verstehe ich das. Die *Antriebsmotoren* laufen beim Bremsen als Generator und treiben die Motoren der Ventilatoren an? Richtig so? Das ist ja mal eine ungewöhnliche Art zu bremsen. Aber bis zum Stillstand geht das ja nicht - gibt's dann auch noch mechanische Bremsen zusätzlich?

Gruß,
Stefan