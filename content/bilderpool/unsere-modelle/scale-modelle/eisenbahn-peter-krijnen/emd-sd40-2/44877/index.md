---
layout: "image"
title: "EMD SD40-2. 53 CP5419"
date: "2016-12-09T21:08:28"
picture: "emdsd14_2.jpg"
weight: "14"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/44877
imported:
- "2019"
_4images_image_id: "44877"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-12-09T21:08:28"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44877 -->
Sicht auf die vorderseite in der Nacht.
Herr Lokführer lauft zur kontrolle um seine Lokomotiv: die Leuchter in die Kabine und an die Treppen und Drehgestellen sind noch eingeschaltet. Erts beim abfahrt werden die aus geschaltet.