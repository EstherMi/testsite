---
layout: "image"
title: "EMD SD40-2. 6"
date: "2016-06-12T19:48:23"
picture: "emdsd06.jpg"
weight: "30"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/43745
imported:
- "2019"
_4images_image_id: "43745"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-06-12T19:48:23"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43745 -->
Neben der Bahnräumer, der LGB Klauenkupplung, sind auch die beide "Dichtlights" zu sehen.
Seit 1996 mus jeder Triebfahrseuch in Amerika 2 Blinkleuchten haben.
In höhe von 36inch und 36 oder 64 inch von einander. Fährt man langzamer als 20mile in der stunde, oder auf ein Bahnhofgelände oder man nähert ein Überweg, mussen die beide Leuchten 40x in der Minute blinken. Dabei wird nur die Lichtstarke varieirt. Durfen also nicht aus geschalted werden.