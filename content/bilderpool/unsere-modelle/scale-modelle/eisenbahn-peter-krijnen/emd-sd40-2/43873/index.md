---
layout: "image"
title: "EMD SD40-2. 34  CP5419"
date: "2016-07-12T18:05:26"
picture: "emdsd4.jpg"
weight: "18"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/43873
imported:
- "2019"
_4images_image_id: "43873"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-07-12T18:05:26"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43873 -->
Die #32064 die ich in zur Lagerung der Achsen benutst hatte, hab ich durch #32850, Riegelstein + Kugellager, ersetzt.