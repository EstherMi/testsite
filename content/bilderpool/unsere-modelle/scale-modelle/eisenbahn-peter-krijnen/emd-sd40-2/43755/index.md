---
layout: "image"
title: "EMD SD40-2. 16"
date: "2016-06-12T19:48:23"
picture: "emdsd16.jpg"
weight: "40"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/43755
imported:
- "2019"
_4images_image_id: "43755"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-06-12T19:48:23"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43755 -->
Das Gehirn:
ESU Loksound V4.0 XL, multi Protocol Decoder.
Gesteuert werd mit ein ESU ECoS 5000. Benutse das DCC Protocol.

Was alles drin und dran ist?
http://members.casema.nl/pkrijnen/EMD%20SD40%20elektra.xlsx