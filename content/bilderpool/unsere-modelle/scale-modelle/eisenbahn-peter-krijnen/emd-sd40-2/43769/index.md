---
layout: "image"
title: "EMD SD40-2. 30"
date: "2016-06-14T21:23:45"
picture: "emdsd1.jpg"
weight: "24"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/43769
imported:
- "2019"
_4images_image_id: "43769"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-06-14T21:23:45"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43769 -->
SD40 Cuteaway.