---
layout: "image"
title: "EMD SD40-2. 46  CP5419"
date: "2016-12-09T21:08:28"
picture: "emdsd07_2.jpg"
weight: "7"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/44870
imported:
- "2019"
_4images_image_id: "44870"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-12-09T21:08:28"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44870 -->
Nummerkasten ohne Fensterladen.