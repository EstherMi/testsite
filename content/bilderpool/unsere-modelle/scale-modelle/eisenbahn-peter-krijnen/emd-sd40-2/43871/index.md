---
layout: "image"
title: "EMD SD40-2. 32  CP5419"
date: "2016-07-12T18:05:26"
picture: "emdsd2.jpg"
weight: "16"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/43871
imported:
- "2019"
_4images_image_id: "43871"
_4images_cat_id: "3239"
_4images_user_id: "144"
_4images_image_date: "2016-07-12T18:05:26"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43871 -->
Auch hab ich weitere details an gebaut:
- 6 Leitungen
- ein vorrichtung zur Manuelen entkuppeln