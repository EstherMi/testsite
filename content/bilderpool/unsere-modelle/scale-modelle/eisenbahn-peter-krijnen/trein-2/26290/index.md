---
layout: "image"
title: "Trein 2: der gegengewichtswagen 2"
date: "2010-02-10T15:59:15"
picture: "trein40.jpg"
weight: "40"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/26290
imported:
- "2019"
_4images_image_id: "26290"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:15"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26290 -->
