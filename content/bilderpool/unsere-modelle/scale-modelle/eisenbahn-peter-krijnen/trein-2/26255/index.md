---
layout: "image"
title: "Trein 2: ...Ausleger-auflagewagen..."
date: "2010-02-10T15:59:13"
picture: "trein05.jpg"
weight: "5"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/26255
imported:
- "2019"
_4images_image_id: "26255"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:13"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26255 -->
Zur vorbild stand das LGB Modell #42610