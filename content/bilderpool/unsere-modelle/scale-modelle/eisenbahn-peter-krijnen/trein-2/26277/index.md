---
layout: "image"
title: "Trein 2: Lok, unterseite drehgestell"
date: "2010-02-10T15:59:14"
picture: "trein27.jpg"
weight: "27"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/26277
imported:
- "2019"
_4images_image_id: "26277"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:14"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26277 -->
Lagerung der Achsen durch Kugellager in halbierten Riegelstein #32850.
Der Kuppelbügel ist mit ein Bolzen an ein baustein #35049 verschraubt.