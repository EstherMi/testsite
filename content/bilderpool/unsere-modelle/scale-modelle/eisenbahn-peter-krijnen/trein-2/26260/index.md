---
layout: "image"
title: "Trein 2: Lok, detail der lok"
date: "2010-02-10T15:59:13"
picture: "trein10.jpg"
weight: "10"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/26260
imported:
- "2019"
_4images_image_id: "26260"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:13"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26260 -->
Aufstiegleiter, Bremse, Sandkaste