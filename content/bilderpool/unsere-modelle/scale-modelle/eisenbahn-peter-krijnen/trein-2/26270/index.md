---
layout: "image"
title: "Trein 2: Lok, Spurkranz spender"
date: "2010-02-10T15:59:14"
picture: "trein20.jpg"
weight: "20"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/26270
imported:
- "2019"
_4images_image_id: "26270"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:14"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26270 -->
Als spender werden 2 sats #67319 benutst.
Die Speichen werden mit ein messer oder kleinen Borhschleifer entfernt.