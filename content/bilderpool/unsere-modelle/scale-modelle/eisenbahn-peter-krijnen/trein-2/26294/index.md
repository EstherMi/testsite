---
layout: "image"
title: "Trein 2: Ausleger-auflagewagen 1"
date: "2010-02-10T15:59:15"
picture: "trein44.jpg"
weight: "44"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/26294
imported:
- "2019"
_4images_image_id: "26294"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:15"
_4images_image_order: "44"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26294 -->
