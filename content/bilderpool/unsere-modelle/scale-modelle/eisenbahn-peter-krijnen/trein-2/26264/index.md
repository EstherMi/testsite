---
layout: "image"
title: "Trein 2: Lok, der Elektromotor"
date: "2010-02-10T15:59:13"
picture: "trein14.jpg"
weight: "14"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/26264
imported:
- "2019"
_4images_image_id: "26264"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:13"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26264 -->
2 x 24Volt Motoren # 32820, sind in das frame der Lok eingebaut.
Die Schnecken der beide Motoren hab ich entfernt und erzetst durch Zahnrad z10.
Der Lampenfasung ist zur anschlus der in der Motorhauben eingebauten Leuchten