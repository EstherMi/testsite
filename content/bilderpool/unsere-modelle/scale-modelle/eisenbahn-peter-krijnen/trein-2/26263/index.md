---
layout: "image"
title: "Trein 2: Lok, Stirnseite"
date: "2010-02-10T15:59:13"
picture: "trein13.jpg"
weight: "13"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/26263
imported:
- "2019"
_4images_image_id: "26263"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:13"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26263 -->
Bremsslauchen, Puffer, Kupplung.
Die 6 Leuchten werden, über 2 Dioden, beim fahrrichtungswechsel umgeschaltet.