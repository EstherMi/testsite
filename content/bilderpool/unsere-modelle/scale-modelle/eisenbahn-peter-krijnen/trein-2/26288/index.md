---
layout: "image"
title: "Trein 2: Einsel Achsdrehgestelle"
date: "2010-02-10T15:59:15"
picture: "trein38.jpg"
weight: "38"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/26288
imported:
- "2019"
_4images_image_id: "26288"
_4images_cat_id: "1869"
_4images_user_id: "144"
_4images_image_date: "2010-02-10T15:59:15"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26288 -->
Da das loch 10mm weit ist, hab ich aus drei Kunststofrören 4x6, 6x8 und 8x10mm, einen Ring gemacht .
Das werd mit ein Clipsachse #32870 mit 4 Riegelscheiben und ein Hülse #35981 in das loch eines #32064 gesteckt.