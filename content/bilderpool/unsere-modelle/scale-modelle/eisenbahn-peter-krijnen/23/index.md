---
layout: "image"
title: "TREIN 2-2-12"
date: "2003-04-21T17:43:17"
picture: "TREIN 2-2-12.jpg"
weight: "14"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/23
imported:
- "2019"
_4images_image_id: "23"
_4images_cat_id: "17"
_4images_user_id: "1"
_4images_image_date: "2003-04-21T17:43:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23 -->
