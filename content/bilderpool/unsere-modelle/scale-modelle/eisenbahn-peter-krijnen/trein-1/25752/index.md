---
layout: "image"
title: "Als vorbild diente ein Hochbordwagen LGB #43210"
date: "2009-11-08T19:43:55"
picture: "trein24.jpg"
weight: "24"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/25752
imported:
- "2019"
_4images_image_id: "25752"
_4images_cat_id: "1805"
_4images_user_id: "144"
_4images_image_date: "2009-11-08T19:43:55"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25752 -->
