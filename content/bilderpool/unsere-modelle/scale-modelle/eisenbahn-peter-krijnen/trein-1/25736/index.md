---
layout: "image"
title: "Unterseite: antrieb"
date: "2009-11-08T19:43:54"
picture: "trein08.jpg"
weight: "8"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/25736
imported:
- "2019"
_4images_image_id: "25736"
_4images_cat_id: "1805"
_4images_user_id: "144"
_4images_image_date: "2009-11-08T19:43:54"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25736 -->
Kette auf ein Z20, und zwei Schnecken auf zwei z10.
Sur Stromabname benutse ich ein satz LGB#63193, Kontaktteile für Metallräder.