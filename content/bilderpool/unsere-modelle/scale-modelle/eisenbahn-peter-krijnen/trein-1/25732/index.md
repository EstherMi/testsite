---
layout: "image"
title: "Der kleine Köf"
date: "2009-11-08T19:43:54"
picture: "trein04.jpg"
weight: "4"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/25732
imported:
- "2019"
_4images_image_id: "25732"
_4images_cat_id: "1805"
_4images_user_id: "144"
_4images_image_date: "2009-11-08T19:43:54"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25732 -->
