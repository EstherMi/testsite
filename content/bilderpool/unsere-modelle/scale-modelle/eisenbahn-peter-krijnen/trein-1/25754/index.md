---
layout: "image"
title: "...und etwas neher."
date: "2009-11-08T19:43:55"
picture: "trein26.jpg"
weight: "26"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/25754
imported:
- "2019"
_4images_image_id: "25754"
_4images_cat_id: "1805"
_4images_user_id: "144"
_4images_image_date: "2009-11-08T19:43:55"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25754 -->
