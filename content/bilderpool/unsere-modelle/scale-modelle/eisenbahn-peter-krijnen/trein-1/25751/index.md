---
layout: "image"
title: "Kleine Kran von der andere seite"
date: "2009-11-08T19:43:55"
picture: "trein23.jpg"
weight: "23"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/25751
imported:
- "2019"
_4images_image_id: "25751"
_4images_cat_id: "1805"
_4images_user_id: "144"
_4images_image_date: "2009-11-08T19:43:55"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25751 -->
