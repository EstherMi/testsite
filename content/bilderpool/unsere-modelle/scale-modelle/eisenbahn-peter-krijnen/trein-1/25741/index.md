---
layout: "image"
title: "Ein vier Achsige Niederbordwagen mit Bremserbühne"
date: "2009-11-08T19:43:54"
picture: "trein13.jpg"
weight: "13"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/25741
imported:
- "2019"
_4images_image_id: "25741"
_4images_cat_id: "1805"
_4images_user_id: "144"
_4images_image_date: "2009-11-08T19:43:54"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25741 -->
LGB #42610
