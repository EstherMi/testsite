---
layout: "image"
title: "Selbst gebaute Radsats"
date: "2009-11-08T19:43:55"
picture: "trein17.jpg"
weight: "17"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/25745
imported:
- "2019"
_4images_image_id: "25745"
_4images_cat_id: "1805"
_4images_user_id: "144"
_4images_image_date: "2009-11-08T19:43:55"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25745 -->
Als spender wurden ein par Radsatsen LGB # 67319 gekauft.