---
layout: "image"
title: "Smart Fortwo 2"
date: "2007-01-07T17:53:02"
picture: "Smart_Fortwo_02.jpg"
weight: "2"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/8323
imported:
- "2019"
_4images_image_id: "8323"
_4images_cat_id: "766"
_4images_user_id: "328"
_4images_image_date: "2007-01-07T17:53:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8323 -->
