---
layout: "comment"
hidden: true
title: "1912"
date: "2007-01-08T16:53:19"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Bravissimo!


Als Notlösung für die Scheinwerfer gäbe es ja noch diese Aufkleber (vom "Master"?) mit den aufgedruckten Gläsern.

Elektro-Motorisierung gäbe es vielleicht mittels Ur-Minimotor (der wirklich die Maße eines BS30 hatte). Aber --- irgendwo ist auch einfach Schluß. ft hat eben zu "grobe" Teile, um in diesem Maßstab noch mehr rauszuholen. Ich trau mich noch nicht mal, meine Autos oder Flieger überhaupt als "Scale-Modell" zu deklarieren.

Gruß,
Harald