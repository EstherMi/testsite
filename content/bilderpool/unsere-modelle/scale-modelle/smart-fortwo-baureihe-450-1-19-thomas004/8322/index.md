---
layout: "image"
title: "Smart Fortwo 1"
date: "2007-01-07T17:53:02"
picture: "Smart_Fortwo_04.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/8322
imported:
- "2019"
_4images_image_id: "8322"
_4images_cat_id: "766"
_4images_user_id: "328"
_4images_image_date: "2007-01-07T17:53:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8322 -->
Mit diesem Modell habe ich versucht, meinen Beruf (Smart-Entwickler) zum Hobby zu machen. Es zeigt einen nahezu maßstabsgetreuen Smart Fortwo der Baureihe 450 (1998 bis 2006). Da ich die kleine Vorderachse der Rennwagen verwenden wollte, war der Maßstab mit ziemlich genau 1:19 vorgegeben. Radstand, Länge und Höhe sind bis auf wenige Millimeter maßstabsgetreu, und auch das FT-Männchen passt mit einer realen Körpergröße von 1,65 m gut dazu.

Außen habe ich versucht, die Zweifarbigkeit des Originals durch die Darstellung der Tridion-Sicherheitszelle mit gelben Bauteilen nachzubilden.

Innen gibt es eine erhöhte Sitzposition von Fahrer und Beifahrer, was beim echten Smart Fortwo durch den doppelten Boden mit Tank und Aggregaten verursacht wird. Die Vorderräder lassen sich voll lenken; das Lenkrad wurde mittels Kettentrieb seitlich versetzt und auf Höhe des Fahrersitzes positioniert.

Oben sieht man – wie beim Original – das Glasdach.