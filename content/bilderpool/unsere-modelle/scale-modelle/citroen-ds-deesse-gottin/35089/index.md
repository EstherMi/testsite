---
layout: "image"
title: "Deesse07m1.JPG"
date: "2012-07-03T21:32:25"
picture: "Deesse07m1.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/35089
imported:
- "2019"
_4images_image_id: "35089"
_4images_cat_id: "2603"
_4images_user_id: "4"
_4images_image_date: "2012-07-03T21:32:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35089 -->
Die DS beschäftigt mich schon ein ganzes Weilchen und ist mein erstes Modell, das auch im Maßstab stimmt. Hier sind außerdem die ersten Versuche mit Moosgummi-Beplankung zu sehen.