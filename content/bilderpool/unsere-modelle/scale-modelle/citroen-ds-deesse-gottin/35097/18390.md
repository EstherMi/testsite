---
layout: "comment"
hidden: true
title: "18390"
date: "2013-10-08T21:24:10"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Tja, es ist ein ständiges Auf- und Ab. Ich war mal eine ganze Zeitlang "clean" (Tarnkappenbomber, Deesse und der "Jumping" sind meine Zeugen), aber manchmal krieg ich so Zuckungen in der rechten Hand, und dann *schnief* kommt der Ausruf "Warum haben die bei ft nicht an *sowas* gedacht, das kann doch nicht sein" und ... dann passieren ganz schlimme Sachen.

Gruß,
Harald
(der wohl bald eine Selbsthilfegruppe der anonymen ft-Modder gründen sollte)