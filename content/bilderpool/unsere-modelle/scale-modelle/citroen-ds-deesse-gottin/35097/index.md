---
layout: "image"
title: "Deesse31m1.JPG"
date: "2012-07-03T22:02:49"
picture: "Deesse31m1.JPG"
weight: "12"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/35097
imported:
- "2019"
_4images_image_id: "35097"
_4images_cat_id: "2603"
_4images_user_id: "4"
_4images_image_date: "2012-07-03T22:02:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35097 -->
Das Gerippe, hier ohne Moosgummi.
Der Winkel 60° unterhalb vom Z20 im Motorraum schreit nach ein bisschen Modding, weil das Zahnrad nicht richtig rastet und nach ein paar Minuten Fahrbetrieb entschwindet. Aber ich lasse es mal dabei.

Insgesamt sind am Modell sage und schreibe ZWEI gemoddete Teile verbaut:
1. ein S-Riegel mit "Plattfuß", damit er in die Flachnut eines BS7,5 geschoben werden kann (im Antrieb, damit ist der Querträger mit dem Z20 am längs verlaufenden Winkelträger 30 befestigt).
2. ein Winkelträger 120 ohne Zapfen. Den hat er allen Ernstes bei einem Unfall verloren. Jetzt leistet er aber gute Dienste als Lehne der Rücksitzbank, wo er mit dem Zapfen klemmen würde.