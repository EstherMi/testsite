---
layout: "image"
title: "zDeesse10m.JPG"
date: "2012-07-03T22:07:53"
picture: "zDeesse10m.JPG"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/35098
imported:
- "2019"
_4images_image_id: "35098"
_4images_cat_id: "2603"
_4images_user_id: "4"
_4images_image_date: "2012-07-03T22:07:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35098 -->
So schräg von oben ist die Zuckerseite - man sieht die Wellen im Moosgummi nicht so deutlich.

Der Kenner wird die Scharniere hinter den vorderen Scheinwerfern bemerken. Die Deesse hatte schon ein Kurvenlicht, indem ein Gestänge aus der Lenkung die Scheinwerfer schwenkte. Mit dem Gestänge tu ich mich noch etwas schwer. Vielleicht muss da doch eine aufgebogene Büroklammer hin.