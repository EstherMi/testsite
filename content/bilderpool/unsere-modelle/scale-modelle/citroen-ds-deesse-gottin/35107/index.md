---
layout: "image"
title: "Deesse48.JPG"
date: "2012-07-08T11:22:11"
picture: "Deesse48.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/35107
imported:
- "2019"
_4images_image_id: "35107"
_4images_cat_id: "2603"
_4images_user_id: "4"
_4images_image_date: "2012-07-08T11:22:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35107 -->
Über die vier Winkel 60° (zwei links, zwei rechts unter den Scharnieren der Motorhaube) wird das Gewicht auf die Vorderachse abgestützt. Zusammen mit den Verstrebungen außen herum und insbesondere denjenigen unten herum ist das ein stabiler Käfig geworden. Ganz im Gegensatz zum Heck, das nur über die Bauteile im Wagenboden abgestützt wird (das Dach biegt sich weg, die Seitenteile sind nicht abgestützt) und deshalb deutlich weicher ist.