---
layout: "image"
title: "MB trac 800 12"
date: "2009-08-18T18:59:11"
picture: "MB_trac_18.jpg"
weight: "22"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/24810
imported:
- "2019"
_4images_image_id: "24810"
_4images_cat_id: "1706"
_4images_user_id: "328"
_4images_image_date: "2009-08-18T18:59:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24810 -->
Detailansicht Hinterachse. Direkt neben den Rädern sitzt - schön versteckt - je 1 9V-Blockakku. Damit kann das Fahrzeug erstaunlich lange durch die Gegend fahren.