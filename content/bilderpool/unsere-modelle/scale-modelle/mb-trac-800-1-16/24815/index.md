---
layout: "image"
title: "MB trac 800 17"
date: "2009-08-20T19:03:05"
picture: "MB_trac_24.jpg"
weight: "10"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/24815
imported:
- "2019"
_4images_image_id: "24815"
_4images_cat_id: "1706"
_4images_user_id: "328"
_4images_image_date: "2009-08-20T19:03:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24815 -->
Hier habe ich die Front kurz abgebaut (lässt sich einfach nach vorn abziehen). Gut zu sehen ist das sehr enge Package vom Antriebsmotor und der darunter liegenden Vorderachsaufhängung mit der Rastschnecke. Die Welle mit den beiden Rastschnecken zieht sich von vorn nach hinten durchs komplette Fahrzeug und dient dadurch nebenbei als Zapfwellen vorn/hinten sowie als Drehachse zur Verschränkung der Vorderachse.