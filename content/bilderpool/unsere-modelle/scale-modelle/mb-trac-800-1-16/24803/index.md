---
layout: "image"
title: "MB trac 800 5"
date: "2009-08-18T18:44:46"
picture: "MB_trac_09.jpg"
weight: "15"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/24803
imported:
- "2019"
_4images_image_id: "24803"
_4images_cat_id: "1706"
_4images_user_id: "328"
_4images_image_date: "2009-08-18T18:44:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24803 -->
Hinten gibt es Zapfwelle und Anhängevorrichtung.