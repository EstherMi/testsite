---
layout: "image"
title: "MB trac 800 23"
date: "2009-09-07T21:14:52"
picture: "MB_trac_33.jpg"
weight: "6"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/24883
imported:
- "2019"
_4images_image_id: "24883"
_4images_cat_id: "1706"
_4images_user_id: "328"
_4images_image_date: "2009-09-07T21:14:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24883 -->
Die Flachsteine 30 versteifen den Mittelteil des Fahrzeugs um den Antrieb ungemein und dienen gleichzeitig als Sichtschutz und Verkleidung.