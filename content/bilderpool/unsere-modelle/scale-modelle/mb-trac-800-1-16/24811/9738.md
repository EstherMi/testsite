---
layout: "comment"
hidden: true
title: "9738"
date: "2009-08-19T10:26:02"
uploadBy:
- "Reus"
license: "unknown"
imported:
- "2019"
---
Schönes Detail, diese L-Lasche. Wie sind die beiden kurzen Statikstreben auf der rechten Seite (also die Strebe an der Lasche und die Strebe an der Spurstange) miteinander verbunden? Sieht auf dem Bild aus wie ein schwarzer Ring.