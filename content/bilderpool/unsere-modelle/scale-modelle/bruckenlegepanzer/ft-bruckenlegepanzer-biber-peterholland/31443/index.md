---
layout: "image"
title: "FT-Brückenlegepanzer-Biber"
date: "2011-07-29T15:34:40"
picture: "ftbrueckenlegepanzerbiber10.jpg"
weight: "11"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/31443
imported:
- "2019"
_4images_image_id: "31443"
_4images_cat_id: "2338"
_4images_user_id: "22"
_4images_image_date: "2011-07-29T15:34:40"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31443 -->
FT-Brückenlegepanzer-Biber 

Massstab ca. 1:10
Gewicht: ca. 13,2 kg

Das "Hirschgeweih" das so wichtig für die Zusammenführung der beiden Segnente ist. 
Es ist so gestaltet, dass es die Laufschiene umgreift und ein Abrutschen unmöglich macht.

