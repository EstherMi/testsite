---
layout: "image"
title: "FT-Brückenlegepanzer-Biber"
date: "2011-07-29T15:34:40"
picture: "ftbrueckenlegepanzerbiber14.jpg"
weight: "15"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/31447
imported:
- "2019"
_4images_image_id: "31447"
_4images_cat_id: "2338"
_4images_user_id: "22"
_4images_image_date: "2011-07-29T15:34:40"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31447 -->
FT-Brückenlegepanzer-Biber 

Massstab ca. 1:10
Gewicht: ca. 13,2 kg

Ich habe für meine  FT-Brückenlegepanzer-Biber nur eine einfache Verschluss-mechanismus
Jürgen Warwel hat eher die tolle und zugleich patentierte Verbolzkinematik von Porsche nachgebaut.  Detailierte Zeichnungen könnt Ihr über DEPATSINET (Deutsches Patent und Markenamt) finden. Dokumentiert im US Patent 3,744,075 vom 10.07.1973. Das Deutsche Patent von 1970 ist nicht als pdf greifbar.
