---
layout: "image"
title: "Aluminium Panzerschnellbrücke:"
date: "2011-07-29T15:34:40"
picture: "ftbrueckenlegepanzerbiber40.jpg"
weight: "41"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/31473
imported:
- "2019"
_4images_image_id: "31473"
_4images_cat_id: "2338"
_4images_user_id: "22"
_4images_image_date: "2011-07-29T15:34:40"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31473 -->
Aluminium Panzerschnellbrücke:
2 x 2,6 kg  = 5,2 kg
2 x 1 m =  2,0 m (statt 2x 1,1 = 2,2m Massstab 1:10).  
