---
layout: "image"
title: "FT-Brückenlegepanzer-Biber"
date: "2011-07-29T15:34:40"
picture: "ftbrueckenlegepanzerbiber29.jpg"
weight: "30"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/31462
imported:
- "2019"
_4images_image_id: "31462"
_4images_cat_id: "2338"
_4images_user_id: "22"
_4images_image_date: "2011-07-29T15:34:40"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31462 -->
FT-Brückenlegepanzer-Biber 

Massstab ca. 1:10
Gewicht: ca. 13,2 kg
