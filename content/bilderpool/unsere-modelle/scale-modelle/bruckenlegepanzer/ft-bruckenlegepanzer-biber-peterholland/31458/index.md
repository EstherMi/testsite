---
layout: "image"
title: "FT-Brückenlegepanzer-Biber"
date: "2011-07-29T15:34:40"
picture: "ftbrueckenlegepanzerbiber25.jpg"
weight: "26"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/31458
imported:
- "2019"
_4images_image_id: "31458"
_4images_cat_id: "2338"
_4images_user_id: "22"
_4images_image_date: "2011-07-29T15:34:40"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31458 -->
FT-Brückenlegepanzer-Biber 

Massstab ca. 1:10
Gewicht: ca. 13,2 kg
