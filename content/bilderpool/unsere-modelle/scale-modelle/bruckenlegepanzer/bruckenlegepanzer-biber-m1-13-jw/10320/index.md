---
layout: "image"
title: "Prototyp_1_Schloss"
date: "2007-05-06T14:54:09"
picture: "Brueckenmodule_verkoppelt_3.jpg"
weight: "5"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- details/10320
imported:
- "2019"
_4images_image_id: "10320"
_4images_cat_id: "820"
_4images_user_id: "107"
_4images_image_date: "2007-05-06T14:54:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10320 -->
