---
layout: "image"
title: "Beginn Verlegevorgang"
date: "2007-04-06T19:01:53"
picture: "04_Beginn_Verlegevorgang_1.jpg"
weight: "12"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- details/10008
imported:
- "2019"
_4images_image_id: "10008"
_4images_cat_id: "820"
_4images_user_id: "107"
_4images_image_date: "2007-04-06T19:01:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10008 -->
Beginn Verlegevorgang:
Das vordere Brückenmodul wird nun ausgefahren. Das obere Brückenmodul rollt mittels kleiner Rollen im oberen Kantenbereich des unteren Brückenmoduls sauber ab und wird dabei seitlich geführt. Desweiteren wird hält der Heckausleger das obere Brückenmodul durch eine formschlüssige Verbindung. Auf die detailierte Beschreibung dessen was da so alles passiert möchte ich an dieser Stelle nicht eingehen. 
Im Laufe dieses Vorganges wird wohl auch das Stützschild voll ausgefahren...