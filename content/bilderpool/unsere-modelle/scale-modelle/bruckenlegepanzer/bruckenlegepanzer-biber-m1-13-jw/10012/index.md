---
layout: "image"
title: "Hilfsarme absenken"
date: "2007-04-06T19:08:57"
picture: "07_Hilfsarme_absenken_7_1.jpg"
weight: "16"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- details/10012
imported:
- "2019"
_4images_image_id: "10012"
_4images_cat_id: "820"
_4images_user_id: "107"
_4images_image_date: "2007-04-06T19:08:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10012 -->
Hilfsarme absenken:
Die Hilfsarme, formschlüssig mit dem Brückenmodul verbunden, senken sich ab und fahren in ihre Ruhestellung zurück. Das obere Brückenmodul wird vertikal durch eine exakt ausgerichtete Rolle zum unteren Brückenmodul positioniert. Bei diesem Vorgang greifen ausschließlich die unteren Brückenmodule (Untergurt) ineinander.