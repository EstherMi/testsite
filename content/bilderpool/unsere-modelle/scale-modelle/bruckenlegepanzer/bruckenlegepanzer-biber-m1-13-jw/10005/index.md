---
layout: "image"
title: "01_Transportstellung"
date: "2007-04-06T19:01:52"
picture: "01_Transportstellung1.jpg"
weight: "9"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- details/10005
imported:
- "2019"
_4images_image_id: "10005"
_4images_cat_id: "820"
_4images_user_id: "107"
_4images_image_date: "2007-04-06T19:01:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10005 -->
Transportstellung:
Der Heckausleger ist so positioniert, dass sich der hintere Bereich der Brückenteile nicht verschieben kann. Im vorderen Bereich wird das obere Brückenmodul mit eigens angebrachten Ketten spielfrei mit dem Panzer verspannt