---
layout: "image"
title: "Brücke verriegeln"
date: "2007-04-06T19:08:57"
picture: "08_Hilfsarme_absenken_-_Brcke_verriegeln_7.jpg"
weight: "17"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- details/10013
imported:
- "2019"
_4images_image_id: "10013"
_4images_cat_id: "820"
_4images_user_id: "107"
_4images_image_date: "2007-04-06T19:08:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10013 -->
Brücke verriegeln:
Der Ausleger wird nun soweit angehoben, bis die oberen Kontaktpunkte (Obergurt) sich berühren. Die Brückenmodule werden automatisch miteinander verbolzt. Diesen Verbolzvorgang werde ich in einer späteren Fotoreihe beschreiben.