---
layout: "image"
title: "FT-Brückenlegepanzer-Biber ( noch ohne noch zu entwicklen Aluminium/Kunststoff Brücke )"
date: "2011-02-18T14:12:43"
picture: "ftbrueckenlegepanzerbiber32.jpg"
weight: "51"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/30040
imported:
- "2019"
_4images_image_id: "30040"
_4images_cat_id: "2214"
_4images_user_id: "22"
_4images_image_date: "2011-02-18T14:12:43"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30040 -->
Idee noch zu entwicklen Aluminium/Kunststoff Brücke mit Kopplungen 

Ich habe selbst auch ein 1:72 Revell-Model Brückenlegepanzer Biber (03135) 
Wenn ich die Abmessungen meiner FT-Brückenlegepanzer Biber umrechne, hat dieser  ein Massstab ca. 1:10. 

Dass heisst bei einer 22m lange Aluminium Panzerschnellbrücke hat ein Fischertechnik-Brücke eine Länge ca.: 2x >1m = >2m. Extreme Kräfte gibt es dann im FT-Modell !.... 

Ich überdenke jetzt die Aluminium/Kunststoff Brücke mit Kopplungen............
