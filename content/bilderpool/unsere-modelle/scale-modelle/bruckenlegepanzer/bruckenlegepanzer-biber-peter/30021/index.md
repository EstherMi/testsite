---
layout: "image"
title: "FT-Brückenlegepanzer-Biber ( noch ohne noch zu entwicklen Aluminium/Kunststoff Brücke )"
date: "2011-02-18T14:12:43"
picture: "ftbrueckenlegepanzerbiber13.jpg"
weight: "32"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/30021
imported:
- "2019"
_4images_image_id: "30021"
_4images_cat_id: "2214"
_4images_user_id: "22"
_4images_image_date: "2011-02-18T14:12:43"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30021 -->
FT-Brückenlegepanzer-Biber mit FT-1:50-Powermotor-antrieb für jede Kette.