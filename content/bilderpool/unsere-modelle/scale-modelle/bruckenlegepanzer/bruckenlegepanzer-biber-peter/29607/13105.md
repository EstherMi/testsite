---
layout: "comment"
hidden: true
title: "13105"
date: "2011-01-05T12:36:14"
uploadBy:
- "fitec"
license: "unknown"
imported:
- "2019"
---
Finde ich sehr beeindruckend. Die BS7,5 sind jeweils mit 2 Kettengliedern verbunden, ich kann kaum glauben dass diese Verbundung auf Dauer oder unter Last hält, die Kettenglieder müssten sich doch zur Seite heraus lösen oder verschieben?

Gruß, Nils