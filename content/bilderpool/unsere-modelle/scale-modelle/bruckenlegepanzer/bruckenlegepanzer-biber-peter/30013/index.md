---
layout: "image"
title: "Brückenlegepanzer-Biber van J. de Grave (Nederlandse Vereniging van Modelbouwers) De Meern"
date: "2011-02-18T14:12:42"
picture: "ftbrueckenlegepanzerbiber05.jpg"
weight: "24"
konstrukteure: 
- "J.de Grave"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/30013
imported:
- "2019"
_4images_image_id: "30013"
_4images_cat_id: "2214"
_4images_user_id: "22"
_4images_image_date: "2011-02-18T14:12:42"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30013 -->
Brückenlegepanzer-Biber