---
layout: "image"
title: "FT-Brückenlegepanzer-Biber Genie-museum Vught"
date: "2011-02-18T14:12:43"
picture: "ftbrueckenlegepanzerbiber42.jpg"
weight: "61"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/30050
imported:
- "2019"
_4images_image_id: "30050"
_4images_cat_id: "2214"
_4images_user_id: "22"
_4images_image_date: "2011-02-18T14:12:43"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30050 -->
FT-Brückenlegepanzer-Biber Genie-museum Vught