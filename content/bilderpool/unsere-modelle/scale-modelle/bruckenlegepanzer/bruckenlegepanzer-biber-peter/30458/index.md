---
layout: "image"
title: "Meine Brückenlegepanzer-Biber (noch) ohne Brücken-Verbinder -unten mit Kugellager"
date: "2011-04-14T21:26:00"
picture: "Brckenlegepanzer-details_017.jpg"
weight: "8"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/30458
imported:
- "2019"
_4images_image_id: "30458"
_4images_cat_id: "2214"
_4images_user_id: "22"
_4images_image_date: "2011-04-14T21:26:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30458 -->
Meine Brückenlegepanzer-Biber (noch) ohne Brücken-Verbinder -unten mit Kugellager