---
layout: "image"
title: "Brückenlegepanzer-Biber (noch) ohne Brücken-Verbinder"
date: "2011-04-14T21:26:00"
picture: "Brckenlegepanzer-details_004.jpg"
weight: "6"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/30456
imported:
- "2019"
_4images_image_id: "30456"
_4images_cat_id: "2214"
_4images_user_id: "22"
_4images_image_date: "2011-04-14T21:26:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30456 -->
Brückenlegepanzer-Biber (noch) ohne Brücken-Verbinder