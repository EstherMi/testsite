---
layout: "image"
title: "Brückenlegepanzer-Biber in Entwicklung"
date: "2011-03-13T19:56:35"
picture: "Brckenlegepanzer-Biber_005.jpg"
weight: "16"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/30250
imported:
- "2019"
_4images_image_id: "30250"
_4images_cat_id: "2214"
_4images_user_id: "22"
_4images_image_date: "2011-03-13T19:56:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30250 -->
Brückenlegepanzer-Biber in Entwicklung