---
layout: "comment"
hidden: true
title: "14077"
date: "2011-04-14T08:17:41"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
Einfach nur genial! 
Und was ein Aufwand mit dem Achsen biegen :o
Aber warum hast du nicht am Ende noch eine 90 Grad Biegung gemacht um die Federung wirklich per Drehstab zu realisieren? Ich vermute die 4mm Achse ist dann (trotz des Gewichtes) zu starr und es federt kaum noch?