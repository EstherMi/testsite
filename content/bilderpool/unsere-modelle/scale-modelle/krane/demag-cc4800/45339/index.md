---
layout: "image"
title: "DEMAG CC4800_1"
date: "2017-03-01T15:57:19"
picture: "demagcc01.jpg"
weight: "1"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/45339
imported:
- "2019"
_4images_image_id: "45339"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45339 -->
Eine der Demag CC4800 der damaligen Firma van Seumeren, beim einsatz in Rotterdam.