---
layout: "image"
title: "DEMAG CC4800_40"
date: "2017-03-01T15:57:19"
picture: "demagcc40.jpg"
weight: "40"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/45378
imported:
- "2019"
_4images_image_id: "45378"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45378 -->
Sicht auf das Frame und einer der nicht angetriebene Radsatzen.
Zur steuerung werden 8 mini-motoren eingesätzt.