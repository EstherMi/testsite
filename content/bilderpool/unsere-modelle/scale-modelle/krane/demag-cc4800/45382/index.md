---
layout: "image"
title: "DEMAG CC4800_44"
date: "2017-03-01T15:57:19"
picture: "demagcc44.jpg"
weight: "44"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/45382
imported:
- "2019"
_4images_image_id: "45382"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "44"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45382 -->
Entfernt man der Aufbauplatte: 2 ringen von je 24 Kugellager zur Axialer führung und ein ring von 12 Kugellager zur Radialer führung.