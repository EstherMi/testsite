---
layout: "image"
title: "DEMAG CC4800_9"
date: "2017-03-01T15:57:19"
picture: "demagcc09.jpg"
weight: "9"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/45347
imported:
- "2019"
_4images_image_id: "45347"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45347 -->
Auslegerspitze.