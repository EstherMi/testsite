---
layout: "image"
title: "DEMAG CC4800_49"
date: "2017-03-01T15:57:19"
picture: "demagcc49.jpg"
weight: "49"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/45387
imported:
- "2019"
_4images_image_id: "45387"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "49"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45387 -->
Dieses Bild hatte Anton Jansen gemacht während der Modellshow Europe in 2014. I-phon und gegenlicht geht nicht gut: ein wenig unscharfes Bild.