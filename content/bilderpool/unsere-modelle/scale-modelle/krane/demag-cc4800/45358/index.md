---
layout: "image"
title: "DEMAG CC4800_20"
date: "2017-03-01T15:57:19"
picture: "demagcc20.jpg"
weight: "20"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/45358
imported:
- "2019"
_4images_image_id: "45358"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45358 -->
Abstützplatten an gehoben.