---
layout: "image"
title: "DEMAG CC4800_33"
date: "2017-03-01T15:57:19"
picture: "demagcc33.jpg"
weight: "33"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/45371
imported:
- "2019"
_4images_image_id: "45371"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45371 -->
Rechte Taster geschlossen: Raupe ist nicht gespant. Was man auch an das dürchhängen der Raupe sehen kan.