---
layout: "image"
title: "DEMAG CC4800_17"
date: "2017-03-01T15:57:19"
picture: "demagcc17.jpg"
weight: "17"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/45355
imported:
- "2019"
_4images_image_id: "45355"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45355 -->
Hier ist besser zu sehen das die Seilwinde für der A-Bock doppelt aufgebaut ist.
Deutlich ist auch zu sehen das eine Seilwinde Fehlt. Diesen Seilwinde ist für der Dereckausleger.
Der konte ich nicht aufbauen, deshalb ist die Seilwinde auch nicht eingebaut.
Beide genante Seilwinden werde von je 2x 20:1 Powermotoren angetrieben.
Die drei andere Seilwinden werden jeder von eine 20:1 Powermotor angetrieben.