---
layout: "image"
title: "DEMAG CC4800_51"
date: "2017-03-01T15:57:19"
picture: "demagcc51.jpg"
weight: "51"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/45389
imported:
- "2019"
_4images_image_id: "45389"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "51"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45389 -->
RC Fernsteuerung: Auf 22 Kanalen ausgebaute Robbe/Futaba F-14.