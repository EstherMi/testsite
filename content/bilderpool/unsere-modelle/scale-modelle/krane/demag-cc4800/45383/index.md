---
layout: "image"
title: "DEMAG CC4800_45"
date: "2017-03-01T15:57:19"
picture: "demagcc45.jpg"
weight: "45"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/45383
imported:
- "2019"
_4images_image_id: "45383"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "45"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45383 -->
Radioplatte 1: 8 Fahrtregler von Conrad, Accu und Emfenger R-118 von Robbe.
Der Accu werd gerade aufgeladen.

Regler 1: Emfängerkanal 1; Drehen; 3x Ikarachi 312:1
Regler 2: Emfängerkanal 2; Winde 2: 20;1 PM
Regler 3: Emfängerkanal 3; Winde 1; 20:1 PM
Regler 4: Emfängerkanal 4; Ballastwagen Fahren; 4x Faulhaber 2619S012SR 112:1
Regler 5: Emfängerkanal 5; Raupe Rechts Fahren; 2x Ikarachi 781:1
Regler 6: Emfängerkanal 6; Raupe Links Fahren; 2x Ikarachi 781:1
Regler 7: Multiswitchkanal 1; Raupe Rechts Spannen; Ikarachi 312:1
Regler 8: Multiswitchkanal 4; Raupe Links Spannen; Ikarachi 312:1