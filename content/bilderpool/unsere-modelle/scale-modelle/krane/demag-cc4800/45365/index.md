---
layout: "image"
title: "DEMAG CC4800_27"
date: "2017-03-01T15:57:19"
picture: "demagcc27.jpg"
weight: "27"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/45365
imported:
- "2019"
_4images_image_id: "45365"
_4images_cat_id: "3377"
_4images_user_id: "144"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45365 -->
Innere aufbau der Abstütztarmen.