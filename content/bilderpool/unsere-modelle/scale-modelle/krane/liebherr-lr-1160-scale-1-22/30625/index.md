---
layout: "image"
title: "f23"
date: "2011-05-22T21:01:58"
picture: "ltr23.jpg"
weight: "27"
konstrukteure: 
- "AntonJansen"
fotografen:
- "AntonJansen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "anton"
license: "unknown"
legacy_id:
- details/30625
imported:
- "2019"
_4images_image_id: "30625"
_4images_cat_id: "2279"
_4images_user_id: "541"
_4images_image_date: "2011-05-22T21:01:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30625 -->
