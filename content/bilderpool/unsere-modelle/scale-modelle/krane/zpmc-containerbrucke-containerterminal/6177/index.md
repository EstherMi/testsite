---
layout: "image"
title: "Hauptkatze 1"
date: "2006-04-29T18:58:43"
picture: "CTA_-_Hauptkatze_1.jpg"
weight: "10"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- details/6177
imported:
- "2019"
_4images_image_id: "6177"
_4images_cat_id: "531"
_4images_user_id: "107"
_4images_image_date: "2006-04-29T18:58:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6177 -->
Die Steuerung erfolgt von Hand, mangels RoBo Pro. Eine mehrstufige Geschwindigkeitsregelung und Kommunikation mit der Portalkatze würde die Sache abrunden. Die Hauptkatze läuft sehr sauber und ruhig – nur etwas zu langsam. Knackpunkt des Modells ist die Aufnahme der Container auf der Laschplattform. Vorraussetzung ist, dass das Modell in der Waage steht. Leider ist mir das in Bemmel nicht ganz gelungen. Mein Fehler war, die Holzplatte auf der das Modell zu Haus stand nicht mitzunehmen. 
Insgesamt muss man als Modellbauer akzeptieren, dass das Modell durch die Autofahrt nicht besser wird und kleinere Veränderungen im Modell, durch Erschütterungen und Transport, sich erst im Laufe der Ausstellung offenbaren.

Der Zugang zur Kabine fehlt noch, ist aber auf Video festgehalten.