---
layout: "image"
title: "CTA Gesamtansicht 1"
date: "2006-04-29T11:14:31"
picture: "CTA_-_Gesamtansicht_1.jpg"
weight: "1"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- details/6162
imported:
- "2019"
_4images_image_id: "6162"
_4images_cat_id: "531"
_4images_user_id: "107"
_4images_image_date: "2006-04-29T11:14:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6162 -->
Die Größe des Modells wurde durch die Containergröße vorgegeben - M1:50
Beplankt wurde die Brücke mit 2,5 Quadratmeter Polystyrol, lackiert mit MARABU - Spray bei Außentenperaturen von etwa 10°C. Die Flächen wurden vorher leicht angerauht. Desweiteren wurden über 20 Meter Alu - Profilstangen verbaut. Die obere "Hauptkatze" wird mangels RoBo Pro von Hand gesteuert, die untere "Portalkatze" automatisch mit LLWin.