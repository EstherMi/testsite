---
layout: "image"
title: "CTA - Portalkatze 1"
date: "2006-04-29T11:14:33"
picture: "CTA_-_Portalkatze_1.jpg"
weight: "5"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- details/6166
imported:
- "2019"
_4images_image_id: "6166"
_4images_cat_id: "531"
_4images_user_id: "107"
_4images_image_date: "2006-04-29T11:14:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6166 -->
Die acht Seile der "Portalkatze" haben mich fast um den Verstand gebracht. Außerdem hat dieser Bauabschnitt die meiste Zeit in Anspruch genommen. Größter Vorteil dieser Konstruktion ist die fast vollständige Unterdrückung von Pendelbewegungen. Ohne diese zuverlässige und sehr genaue Steuerung hätte die CTA mit der automatischen Steuerung so ihre Probleme...