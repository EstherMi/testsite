---
layout: "comment"
hidden: true
title: "1065"
date: "2006-05-05T16:43:12"
uploadBy:
- "jw-stg"
license: "unknown"
imported:
- "2019"
---
Hallo Thomas,
Container werden auf dem Schiff miteinander verbunden. Einerseits durch diagonal angeordnete Laschstangen oder durch diverse Sorten von "Twist Locks" an den Eckpunkten der Container. Seit dem die USA verboten hat, dass sogenannte "Lascher" auf Containern herumturnen, wurden die Twist Locks neu entwickelt und werden von den "Laschern" von Hand an die Container montiert (beladen)oder demiontiert (löschen). Bei Containerbrücken älterer Generation wird dieser Vorgang von den Laschern auf der Straße, vor dem Aufsetzen des Containers auf die Straße oder LKW durchgeführt. Großer Nachteil ist, dass die obere Hauptkatze nicht nur einen großen Weg machen muss sondern der Container auch noch starken Pendelbewegungen ausgesetzt ist was insgesamt die Taktzeiten nachteilig beeinflusst. Die Laschplattform verringert nicht nur den Weg der oberen Hauptkatze und somit des Container, sondern reduziert durch die Zentrierung des Containers auf der Plattform auch noch die Taktzeiten. Auf der Plattform können dann die Twist Locks schnell und sicher montiert oder demontiert werden. Anschließend kommt dann die automatisch gesteuerte Portalkatze, die ebenfalls die Taktzeiten stark reduziert.Im VOX Beitrag wurde gesagt das bis zu 50 Container pro Stunde bewegt werden können - etwa doppelt so viel wie bei den alten Containerbrücken

Gruß Jürgen