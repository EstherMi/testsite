---
layout: "image"
title: "Laschplattform 4"
date: "2006-04-29T18:58:43"
picture: "CTA_-_Laschplattform_4.jpg"
weight: "13"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- details/6180
imported:
- "2019"
_4images_image_id: "6180"
_4images_cat_id: "531"
_4images_user_id: "107"
_4images_image_date: "2006-04-29T18:58:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6180 -->
Die Laschplattform muss noch um 15mm abgesenkt werden. Die Ablageposition und Aussehen des Laschkorbes zum entriegeln der Twistlocks (Containerverbindung)war mir bis zum VOX Beitrag unbekannt. Die notwendigen Arbeiten habe ich bis Bemmel nicht mehr geschafft.
Der Laschkorb, mit der Größe eines Containers, steht unmittelbar auf dem Brückenquerträger.
Um es nachträglich zu erklären:
Die Container sind auf dem Schiff mit halbautomatischen Twistlocks verbunden. Die Lascher die die Twistlocks öffenen müssen dürfen nicht mehr auf den Containern herumlaufen (Unfallgefahr). Die Lösung ist ein Twistlock der sich beim Beladen selber verriegelt und beim entladen von den Laschern mit einer mehreren Meter langen Stange entriegelt werden. Die Lascher befinden sich in einen Laschkorb der wie ein Container über der obersten Containerreihe schwebt. Die Twistlocks haben einen Nippel in den die Laschtange greift. Durch drücken wird der Twistlock entriegelt. Nun kann die oberste Containerreihe entladen werden ... usw.