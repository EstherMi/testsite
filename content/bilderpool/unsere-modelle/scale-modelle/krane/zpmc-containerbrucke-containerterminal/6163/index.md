---
layout: "image"
title: "CTA - Gesamtansicht 2"
date: "2006-04-29T11:14:33"
picture: "CTA_-_Gesamtansicht_2.jpg"
weight: "2"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- details/6163
imported:
- "2019"
_4images_image_id: "6163"
_4images_cat_id: "531"
_4images_user_id: "107"
_4images_image_date: "2006-04-29T11:14:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6163 -->
Leider fehlt noch der Schiffsrumpf und die entsprechende Anzahl Container. Für die Ausstellung in Bemmel haben die ft Kisten jedoch auch Ihren Zweck erfüllt. Die Länge der Elektokabel zum Spreader sind so ausgelegt, dass die Container auf dem Schiffsboden abgestellt werden könnten. Dies bedeutet jedoch eine Kaimauer von ca. 30 cm Höhe...