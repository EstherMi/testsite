---
layout: "comment"
hidden: true
title: "1049"
date: "2006-04-30T09:48:27"
uploadBy:
- "jw-stg"
license: "unknown"
imported:
- "2019"
---
Also... ist eigendlich ganz einfach. Man muss nur darauf kommen.
Steckt doch mal die Klaue der Kegelradachse durch die Statikplatte und dreht sie um 90°. Ihr werdet feststellen, dass die Klaue hängen bleibt. Ihr braucht jetzt nur noch ein kleinen Holzkeil eines Streichholzes in die Klaue einzusetzen und leicht zu verspannen (die Klaue spreitzt sich um einige zehntel Millimeter).Das erhöht die Sicherheit gegen herausfallen des Containers. 
Das wars auch schon.

Gruß Jürgen Warwel