---
layout: "image"
title: "CTA - Spreader 1"
date: "2006-04-29T11:14:33"
picture: "CTA_-_Spreader_1.jpg"
weight: "4"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- details/6165
imported:
- "2019"
_4images_image_id: "6165"
_4images_cat_id: "531"
_4images_user_id: "107"
_4images_image_date: "2006-04-29T11:14:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6165 -->
Der "Spreader" zur Aufnahme der Container ist das Herzstück der Brücke. Monate habe ich mit dem Bau unterschiedlichster Varianten und Maßstäbe verbracht. Festforderung meinerseits war es, die Container untereinander horizontal und vertikal wie im Original zu beladen und zu positionieren. Die Funktionssicherheit bei automatik Betrieb ist insgesamt gut. Voraussetzung ist jedoch, dass das Modell in der Waage steht und die Seile sich exakt auf die Seiltrommel aufrollen. Die Schwenkbewegungen sind wie beim Original bei der oberen "Hauptkatze" recht groß. Bei der unteren "Portalkatze" werden diese ungeliebten Bewegungen durch acht Seile stark unterdrückt