---
layout: "image"
title: "asc_spreader V2_9"
date: "2009-06-07T14:43:36"
picture: "nelconasc43.jpg"
weight: "42"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: ["m05"]
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/24265
imported:
- "2019"
_4images_image_id: "24265"
_4images_cat_id: "1661"
_4images_user_id: "144"
_4images_image_date: "2009-06-07T14:43:36"
_4images_image_order: "43"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24265 -->
Antrieb der Twistlocks:
Mini-mot mit getriebebock #31069 treibt einen bei LEMO gekauften Z45 m0,5 an. Am anderen ende de achse ist ein gekurtsten Schnecke m1,5.