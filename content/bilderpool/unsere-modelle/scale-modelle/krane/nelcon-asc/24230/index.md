---
layout: "image"
title: "asc_5"
date: "2009-06-07T14:43:33"
picture: "nelconasc08.jpg"
weight: "7"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/24230
imported:
- "2019"
_4images_image_id: "24230"
_4images_cat_id: "1661"
_4images_user_id: "144"
_4images_image_date: "2009-06-07T14:43:33"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24230 -->
Mittels Schnecke auf die  Klemmzahnrad Z15  # 37685.