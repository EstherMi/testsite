---
layout: "image"
title: "asc_spreader V1_5"
date: "2009-06-07T14:43:35"
picture: "nelconasc32.jpg"
weight: "31"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/24254
imported:
- "2019"
_4images_image_id: "24254"
_4images_cat_id: "1661"
_4images_user_id: "144"
_4images_image_date: "2009-06-07T14:43:35"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24254 -->
Zwei Schnecken, von je eine mini-mot angetrieben, zur ausfahren der spreader.
Die beide Mots habben das aber nicht geschaft.
Beim aufbau der zweiten version fand ich heraus das das problem verursacht wurde von zwei #32064:
Aufbohren der löcher beider #32064 auf 4,5mm hatte das problem gelöst.