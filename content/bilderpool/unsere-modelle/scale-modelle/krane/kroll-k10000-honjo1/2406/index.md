---
layout: "image"
title: "kroll k 10000 maßstab 1:50 long jib"
date: "2004-04-26T20:19:34"
picture: "K_10000_017_2.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "honjo1"
license: "unknown"
legacy_id:
- details/2406
imported:
- "2019"
_4images_image_id: "2406"
_4images_cat_id: "222"
_4images_user_id: "14"
_4images_image_date: "2004-04-26T20:19:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2406 -->
2m hackenhöhe 3,30m bis zur spitze, von vorne bis hinten 3,45m,  0,4m spurweite des unterwagens