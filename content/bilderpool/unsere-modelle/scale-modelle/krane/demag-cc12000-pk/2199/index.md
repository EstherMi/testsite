---
layout: "image"
title: "CC12000_1"
date: "2004-02-29T21:26:11"
picture: "cc12000_1.jpg"
weight: "1"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/2199
imported:
- "2019"
_4images_image_id: "2199"
_4images_cat_id: "211"
_4images_user_id: "144"
_4images_image_date: "2004-02-29T21:26:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2199 -->
