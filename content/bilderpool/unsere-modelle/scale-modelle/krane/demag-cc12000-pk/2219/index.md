---
layout: "image"
title: "CC12000_28"
date: "2004-02-29T21:26:12"
picture: "cc12000_28.jpg"
weight: "21"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/2219
imported:
- "2019"
_4images_image_id: "2219"
_4images_cat_id: "211"
_4images_user_id: "144"
_4images_image_date: "2004-02-29T21:26:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2219 -->
