---
layout: "image"
title: "ltr6"
date: "2007-02-04T12:35:30"
picture: "ltr6.jpg"
weight: "22"
konstrukteure: 
- "AntonJansen"
fotografen:
- "AntonJansen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "anton"
license: "unknown"
legacy_id:
- details/8868
imported:
- "2019"
_4images_image_id: "8868"
_4images_cat_id: "799"
_4images_user_id: "541"
_4images_image_date: "2007-02-04T12:35:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8868 -->
