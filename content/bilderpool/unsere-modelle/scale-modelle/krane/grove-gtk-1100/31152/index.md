---
layout: "image"
title: "Stütze"
date: "2011-07-14T10:50:29"
picture: "grovegtk054.jpg"
weight: "53"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/31152
imported:
- "2019"
_4images_image_id: "31152"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "54"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31152 -->
Auf der Stütze sind schon die Streben zu sehen, die später für die Abspannung genutzt werden.