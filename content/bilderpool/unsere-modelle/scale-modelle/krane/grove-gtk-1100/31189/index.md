---
layout: "image"
title: "Hubvorgang"
date: "2011-07-14T10:50:29"
picture: "grovegtk091.jpg"
weight: "90"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/31189
imported:
- "2019"
_4images_image_id: "31189"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "91"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31189 -->
an den Bügel wird nun die Strebe einer Stütze befestigt, und der Bügel geht nach oben und zieht die Strebe mit.