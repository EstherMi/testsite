---
layout: "image"
title: "grovegtk014.jpg"
date: "2011-07-14T10:50:29"
picture: "grovegtk014.jpg"
weight: "14"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/31112
imported:
- "2019"
_4images_image_id: "31112"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31112 -->
