---
layout: "image"
title: "'Anfahrt'"
date: "2011-07-14T10:50:29"
picture: "grovegtk001.jpg"
weight: "1"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/31099
imported:
- "2019"
_4images_image_id: "31099"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31099 -->
2 Zugmaschienen fahren den Kran zur Einsatzstelle.