---
layout: "image"
title: "Aufbau"
date: "2011-07-14T10:50:29"
picture: "grovegtk029.jpg"
weight: "28"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/31127
imported:
- "2019"
_4images_image_id: "31127"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31127 -->
Auf dieser Winde ist das Seil aufgewickelt, das den Mast zuerst in Schrägstellung zieht.