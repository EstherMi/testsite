---
layout: "image"
title: "Aufbau"
date: "2011-07-14T10:50:29"
picture: "grovegtk058.jpg"
weight: "57"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/31156
imported:
- "2019"
_4images_image_id: "31156"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "58"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31156 -->
Hier sieht man die Schnecke, die den Hauptmast in die Vertikale gebracht hat.