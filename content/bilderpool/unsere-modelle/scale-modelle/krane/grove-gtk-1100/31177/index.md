---
layout: "image"
title: "Führerhäusschen"
date: "2011-07-14T10:50:29"
picture: "grovegtk079.jpg"
weight: "78"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/31177
imported:
- "2019"
_4images_image_id: "31177"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "79"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31177 -->
