---
layout: "image"
title: "Oberwagen"
date: "2011-07-14T10:50:29"
picture: "grovegtk059.jpg"
weight: "58"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/31157
imported:
- "2019"
_4images_image_id: "31157"
_4images_cat_id: "2320"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "59"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31157 -->
Im Gegensatz zum Original kommt der Oberwagen bei meinem Modell erst jetzt dran. Im Original wird erst der Oberwagen angbracht und anschließend richtet sich der Hauptmast mitsammt dem Oberwagen auf. Dabei sorgt ein Hydraulikzylinder dafür, dass der Opberwagen immer in der Wage bleibt. Leider konnte ich diesen Aufstellvorgang nicht realisieren und baue den Oberwagen nachträglich auf den Hauptmast.