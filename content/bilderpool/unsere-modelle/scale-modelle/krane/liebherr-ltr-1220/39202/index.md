---
layout: "image"
title: "Hefsysteem"
date: "2014-08-09T22:30:57"
picture: "P8080036.jpg"
weight: "4"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/39202
imported:
- "2019"
_4images_image_id: "39202"
_4images_cat_id: "2932"
_4images_user_id: "838"
_4images_image_date: "2014-08-09T22:30:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39202 -->
Machine tilt de rupsbanden van de grond zodat hij zichzelf daarna breder kan maken