---
layout: "image"
title: "Telescoop mast"
date: "2014-08-09T22:30:57"
picture: "P8080060.jpg"
weight: "13"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/39211
imported:
- "2019"
_4images_image_id: "39211"
_4images_cat_id: "2932"
_4images_user_id: "838"
_4images_image_date: "2014-08-09T22:30:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39211 -->
Mast delen worden uitgeschoven door 2 gekoppelde XM motoren en 4 axiaal lagers zorgen voor een soepele loop.
De motoren schuiven het middelste deel uit en het laatste deel gaat door draden mee
De mast alleen weegt al 2 kg