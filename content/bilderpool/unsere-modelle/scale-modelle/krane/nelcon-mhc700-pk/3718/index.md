---
layout: "image"
title: "MHC700_10"
date: "2005-03-05T15:29:11"
picture: "MHC700_10.jpg"
weight: "10"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/3718
imported:
- "2019"
_4images_image_id: "3718"
_4images_cat_id: "330"
_4images_user_id: "144"
_4images_image_date: "2005-03-05T15:29:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3718 -->
Nochmals der Fahrcabine.
Die Stutsen einsel, also 4 stück, sind aber wegen der Stabilität fest eingebaut.