---
layout: "image"
title: "MHC700_16"
date: "2005-03-05T17:32:34"
picture: "MHC700_16.jpg"
weight: "20"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/3728
imported:
- "2019"
_4images_image_id: "3728"
_4images_cat_id: "330"
_4images_user_id: "144"
_4images_image_date: "2005-03-05T17:32:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3728 -->
Ich hab nur einen Seilwinde eingebaut zur heben der Containergreifer oder der Kübel. Es gibt nur ein Draht die im top des Auslegers einmahl umgelengt wird, und am Haken zwei mafl.