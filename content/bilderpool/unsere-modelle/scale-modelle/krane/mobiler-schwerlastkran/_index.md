---
layout: "overview"
title: "Mobiler Schwerlastkran"
date: 2019-12-17T19:29:53+01:00
legacy_id:
- categories/3411
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3411 --> 
Vierachsiger mobiler Schwerlastkran, voll ferngesteuert, mit Stützen und Teleskop-Schwenkarm.