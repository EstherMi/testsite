---
layout: "image"
title: "Seitenansicht Stütze ausgefahren"
date: "2017-06-11T21:52:18"
picture: "20170531_104732_edit.jpg"
weight: "1"
konstrukteure: 
- "The Rob"
fotografen:
- "The Rob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "The Rob"
license: "unknown"
legacy_id:
- details/45931
imported:
- "2019"
_4images_image_id: "45931"
_4images_cat_id: "3411"
_4images_user_id: "2745"
_4images_image_date: "2017-06-11T21:52:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45931 -->
Man kann hier gut sehen, dass ich die Stütze aus FT-Steinen gebaut habe, verstärkt mit zwei Metallachsen.
Von der Tragfähigkeit her gibt es damit bis jetzt keine Probleme, allerdings ist das seitlich Ausfahren recht hakelig. Deshalb werde ich die Stütze nochmal durch ein Alu ersetzen, damit ist das höchstwahrscheinlich schön geschmeidig.