---
layout: "image"
title: "Demag CC1400_41"
date: "2016-12-29T19:33:43"
picture: "demagcc41.jpg"
weight: "41"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/44987
imported:
- "2019"
_4images_image_id: "44987"
_4images_cat_id: "3343"
_4images_user_id: "144"
_4images_image_date: "2016-12-29T19:33:43"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44987 -->
Neben der Akku gibts auch ein Anschluss für ein externe Spannungsversorgung. Mit der Polwendeschalter werd gewählt welche Spannungsverzorgung man benutsen wil. Da ich mehrere Motortypen eingebaut habe, die alle unterschiedlichen drehzalen haben, ist der Fahrtregler zwischen geschalted damit ich die drehzalen noch drosseln kan.