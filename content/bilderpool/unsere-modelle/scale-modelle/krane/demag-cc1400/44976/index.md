---
layout: "image"
title: "Demag CC1400_30"
date: "2016-12-29T19:33:43"
picture: "demagcc30.jpg"
weight: "30"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/44976
imported:
- "2019"
_4images_image_id: "44976"
_4images_cat_id: "3343"
_4images_user_id: "144"
_4images_image_date: "2016-12-29T19:33:43"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44976 -->
Alle abdeckungen entfernt. Auch die Seilwinden und Seilrollen sind ausgebaut.
Übrig bleiben die antrieben der Seilwinden: 3x XM Motoren. Der XS Motor für der Zusatzwinde ist nicht angeschlossen.
Der Obenwagen ist mit 4 Bolzen und 4x "Baustein 15 Buchse M4 R" auf ein 90x90mm Alu platte gebaut