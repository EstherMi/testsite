---
layout: "image"
title: "Demag CC1400_4"
date: "2016-12-29T19:33:43"
picture: "demagcc04.jpg"
weight: "4"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/44950
imported:
- "2019"
_4images_image_id: "44950"
_4images_cat_id: "3343"
_4images_user_id: "144"
_4images_image_date: "2016-12-29T19:33:43"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44950 -->
20er Power-Motor und Seilwinde für Wippspitzenverstellung.