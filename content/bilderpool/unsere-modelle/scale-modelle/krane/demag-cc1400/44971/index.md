---
layout: "image"
title: "Demag CC1400_25"
date: "2016-12-29T19:33:43"
picture: "demagcc25.jpg"
weight: "25"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/44971
imported:
- "2019"
_4images_image_id: "44971"
_4images_cat_id: "3343"
_4images_user_id: "144"
_4images_image_date: "2016-12-29T19:33:43"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44971 -->
Der kleine Haken.