---
layout: "image"
title: "gottwald 15"
date: "2010-03-06T19:20:54"
picture: "gottwald_15.jpg"
weight: "24"
konstrukteure: 
- "AntonJansen"
fotografen:
- "AntonJansen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "anton"
license: "unknown"
legacy_id:
- details/26608
imported:
- "2019"
_4images_image_id: "26608"
_4images_cat_id: "1898"
_4images_user_id: "541"
_4images_image_date: "2010-03-06T19:20:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26608 -->
