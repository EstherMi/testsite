---
layout: "image"
title: "gottwald 27"
date: "2010-03-07T10:12:48"
picture: "gottwald27.jpg"
weight: "10"
konstrukteure: 
- "AntonJansen"
fotografen:
- "AntonJansen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "anton"
license: "unknown"
legacy_id:
- details/26644
imported:
- "2019"
_4images_image_id: "26644"
_4images_cat_id: "1898"
_4images_user_id: "541"
_4images_image_date: "2010-03-07T10:12:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26644 -->
