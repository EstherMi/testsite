---
layout: "image"
title: "gottwald 36"
date: "2010-03-13T17:40:46"
picture: "gottwald_36.jpg"
weight: "4"
konstrukteure: 
- "AntonJansen"
fotografen:
- "AntonJansen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "anton"
license: "unknown"
legacy_id:
- details/26686
imported:
- "2019"
_4images_image_id: "26686"
_4images_cat_id: "1898"
_4images_user_id: "541"
_4images_image_date: "2010-03-13T17:40:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26686 -->
