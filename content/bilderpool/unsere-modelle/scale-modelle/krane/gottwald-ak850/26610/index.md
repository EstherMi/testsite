---
layout: "image"
title: "gottwald 13"
date: "2010-03-06T19:20:54"
picture: "gottwald_13.jpg"
weight: "26"
konstrukteure: 
- "AntonJansen"
fotografen:
- "AntonJansen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "anton"
license: "unknown"
legacy_id:
- details/26610
imported:
- "2019"
_4images_image_id: "26610"
_4images_cat_id: "1898"
_4images_user_id: "541"
_4images_image_date: "2010-03-06T19:20:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26610 -->
