---
layout: "image"
title: "gottwald 25"
date: "2010-03-07T10:12:48"
picture: "gottwald_25.jpg"
weight: "8"
konstrukteure: 
- "AntonJansen"
fotografen:
- "AntonJansen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "anton"
license: "unknown"
legacy_id:
- details/26642
imported:
- "2019"
_4images_image_id: "26642"
_4images_cat_id: "1898"
_4images_user_id: "541"
_4images_image_date: "2010-03-07T10:12:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26642 -->
