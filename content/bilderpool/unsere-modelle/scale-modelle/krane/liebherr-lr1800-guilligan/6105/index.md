---
layout: "image"
title: "Gesamtansicht"
date: "2006-04-14T22:36:19"
picture: "2.jpg"
weight: "3"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/6105
imported:
- "2019"
_4images_image_id: "6105"
_4images_cat_id: "501"
_4images_user_id: "389"
_4images_image_date: "2006-04-14T22:36:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6105 -->
