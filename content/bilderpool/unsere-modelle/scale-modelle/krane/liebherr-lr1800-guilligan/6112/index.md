---
layout: "image"
title: "Aufbau 5"
date: "2006-04-14T22:36:45"
picture: "IMG_4405.jpg"
weight: "9"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/6112
imported:
- "2019"
_4images_image_id: "6112"
_4images_cat_id: "501"
_4images_user_id: "389"
_4images_image_date: "2006-04-14T22:36:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6112 -->
