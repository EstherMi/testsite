---
layout: "image"
title: "LR1800 008"
date: "2006-03-05T12:34:12"
picture: "LR1800_008.JPG"
weight: "25"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/5819
imported:
- "2019"
_4images_image_id: "5819"
_4images_cat_id: "501"
_4images_user_id: "389"
_4images_image_date: "2006-03-05T12:34:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5819 -->
