---
layout: "comment"
hidden: true
title: "999"
date: "2006-04-15T10:37:50"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Was eine Materialschlacht. Halte doch mal zum Vergleich einen normalen ft-Haken daneben ;-) Beeindruckendes Projekt. Ob die Mini-Mots die Gewichte vom Ballastwagen heben können? Und wie lange die Hubgetriebe das wohl mitmachen?

Gruß,
Stefan