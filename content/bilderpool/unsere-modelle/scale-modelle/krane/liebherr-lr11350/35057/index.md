---
layout: "image"
title: "Ansicht von hinten"
date: "2012-06-15T21:09:19"
picture: "LR_11350_Ansicht_von_hinten.jpg"
weight: "18"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- details/35057
imported:
- "2019"
_4images_image_id: "35057"
_4images_cat_id: "2596"
_4images_user_id: "107"
_4images_image_date: "2012-06-15T21:09:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35057 -->
Eine Gesamtaufnahme ist leider nicht möglich, da sonst Teile der Liebherr Montagehalle zu sehen wären...