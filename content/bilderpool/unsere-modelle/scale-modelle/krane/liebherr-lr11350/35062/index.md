---
layout: "image"
title: "Haken"
date: "2012-06-15T21:09:19"
picture: "LR_11350_Haken.jpg"
weight: "23"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- details/35062
imported:
- "2019"
_4images_image_id: "35062"
_4images_cat_id: "2596"
_4images_user_id: "107"
_4images_image_date: "2012-06-15T21:09:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35062 -->
500g Blei sind erforderlich um den Haken mit der erforderlichen Eigenlast auszustatten. Der Haken wurde so gebau, dass der Ballast nicht zu sehen ist