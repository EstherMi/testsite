---
layout: "image"
title: "Antrieb Raube_Oberwagen"
date: "2012-07-16T20:37:42"
picture: "Antrieb_Oberwagen.jpg"
weight: "6"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- details/35175
imported:
- "2019"
_4images_image_id: "35175"
_4images_cat_id: "2596"
_4images_user_id: "107"
_4images_image_date: "2012-07-16T20:37:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35175 -->
