---
layout: "image"
title: "Sennebogen 8100EQ"
date: "2015-07-25T14:07:40"
picture: "P7250025.jpg"
weight: "18"
konstrukteure: 
- "Chef8"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/41482
imported:
- "2019"
_4images_image_id: "41482"
_4images_cat_id: "3101"
_4images_user_id: "838"
_4images_image_date: "2015-07-25T14:07:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41482 -->
De buitenste platen moesten extra om het scharnier heen. De krachten waren te groot en de arm begon hier te buigen.