---
layout: "image"
title: "Sennebogen 8100EQ"
date: "2015-07-25T14:07:40"
picture: "P7250016.jpg"
weight: "15"
konstrukteure: 
- "Chef8"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/41479
imported:
- "2019"
_4images_image_id: "41479"
_4images_cat_id: "3101"
_4images_user_id: "838"
_4images_image_date: "2015-07-25T14:07:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41479 -->
Aandrijving roteren. Deze windt zich op als een klok en ontspant dan in 1 keer.