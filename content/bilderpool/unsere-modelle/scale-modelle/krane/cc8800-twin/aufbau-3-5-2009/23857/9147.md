---
layout: "comment"
hidden: true
title: "9147"
date: "2009-05-05T18:05:12"
uploadBy:
- "jw"
license: "unknown"
imported:
- "2019"
---
Hallo Dirk,

die Gelenksteine werden dich, nach den Streben am Derrickemast, als nächstes beschäftigen. Kannst du ein Versuchsaufbau darstellen, der die Kräfte durch den sehr aufrecht stehenden Ausleger simuliert? Ich fürchte du must dich von diesen Gelenksteinen trennen oder zumindest so viele davon verbauen wie Anton es üblicherweise tut. Aus meinen eigenen Erfahrungen ist es besser Winkelsteine o.ä. zu verwenden die deutlich mehr Querschnittsfläche, gegen Abscherung, aufweisen.

Gruß Jürgen