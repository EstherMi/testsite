---
layout: "comment"
hidden: true
title: "9157"
date: "2009-05-05T21:53:10"
uploadBy:
- "Guilligan"
license: "unknown"
imported:
- "2019"
---
Hallo

@Stefan
so etwas in der Art gibt es im Baubereich. Da werden z.B. Binder(Spannbeton) für Gebäude mit einer "Überhöhung" hergestellt und nachher eingebaut. Später wenn Masse(Aufbeton, Estrich, Bodenbelag) aufgebracht wird legt sich der Binder dann in seine eigentlich Form. Wenn man dieses nicht macht gibt es eine zu große nicht gewünscht/ erlaubte Durchbiegung. Dieses gibt es auch bei Dachbindern(Holz /Beton).
Aber ob ich das bei ft machen würde - eher nein. Obwohl ausprobiert habe ich das noch nicht.

@Jürgen
Im Orginal werden die Seilenden während des Betriebes über Rollen miteinander verbunden. Bei meinem Modell fehlt diese Konstruktion noch und ich müßte bei der Premiere manchmal eine Rolle abschalten damit sich alles wieder geradezieht.

ciao Dirk