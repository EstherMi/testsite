---
layout: "image"
title: "CC8800 Twin 18/28"
date: "2009-05-04T21:14:30"
picture: "cctwin18.jpg"
weight: "18"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Frank Linde"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/23854
imported:
- "2019"
_4images_image_id: "23854"
_4images_cat_id: "1631"
_4images_user_id: "389"
_4images_image_date: "2009-05-04T21:14:30"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23854 -->
Der Ausleger biegt sich schon ein bißchen durch aber alles kein Problem.