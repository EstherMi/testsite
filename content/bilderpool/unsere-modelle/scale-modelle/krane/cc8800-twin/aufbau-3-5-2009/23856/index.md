---
layout: "image"
title: "CC8800 Twin 20/28"
date: "2009-05-04T21:14:30"
picture: "cctwin20.jpg"
weight: "20"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Frank Linde"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/23856
imported:
- "2019"
_4images_image_id: "23856"
_4images_cat_id: "1631"
_4images_user_id: "389"
_4images_image_date: "2009-05-04T21:14:30"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23856 -->
Gut das die Premiere in einer Turnhalle stattfand.