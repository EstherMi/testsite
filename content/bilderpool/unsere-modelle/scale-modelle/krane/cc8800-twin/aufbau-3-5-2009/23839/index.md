---
layout: "image"
title: "CC8800 Twin 3/28"
date: "2009-05-04T21:14:29"
picture: "cctwin03.jpg"
weight: "3"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Frank Linde"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/23839
imported:
- "2019"
_4images_image_id: "23839"
_4images_cat_id: "1631"
_4images_user_id: "389"
_4images_image_date: "2009-05-04T21:14:29"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23839 -->
Hier ist der Derrickausleger zu erkennen.