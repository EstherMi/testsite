---
layout: "image"
title: "CC8800 Twin 6/6"
date: "2009-04-26T19:08:26"
picture: "twin6.jpg"
weight: "6"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/23819
imported:
- "2019"
_4images_image_id: "23819"
_4images_cat_id: "1596"
_4images_user_id: "389"
_4images_image_date: "2009-04-26T19:08:26"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23819 -->
Vorderansicht