---
layout: "image"
title: "CC8800 Twin"
date: "2009-03-29T21:05:06"
picture: "komplettansicht1.jpg"
weight: "7"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/23556
imported:
- "2019"
_4images_image_id: "23556"
_4images_cat_id: "1596"
_4images_user_id: "389"
_4images_image_date: "2009-03-29T21:05:06"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23556 -->
Stand der Dinge 29.03.2009
Die Raupe auf dem Bild muß noch einmal neu gebaut werden.
Diese paßt nicht zu dem fertigen Unterwagen.
Alle Ausleger sind komplett fertig
Was jetzt noch fehlt ist die ganze Verkabelung, Schaltpult und
das Maschinenhaus des Oberwagens  bzw. Leitern und
Laufstege. Ridderkerk kann kommen;-)