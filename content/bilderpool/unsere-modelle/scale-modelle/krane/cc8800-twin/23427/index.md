---
layout: "image"
title: "Derrickwagen CC8800 Twin 6/12"
date: "2009-03-08T18:34:56"
picture: "cctwin06.jpg"
weight: "27"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/23427
imported:
- "2019"
_4images_image_id: "23427"
_4images_cat_id: "1596"
_4images_user_id: "389"
_4images_image_date: "2009-03-08T18:34:56"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23427 -->
