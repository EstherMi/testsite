---
layout: "image"
title: "Kranhaken 3/4"
date: "2009-03-24T06:43:31"
picture: "kranhaken3.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/23505
imported:
- "2019"
_4images_image_id: "23505"
_4images_cat_id: "1596"
_4images_user_id: "389"
_4images_image_date: "2009-03-24T06:43:31"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23505 -->
