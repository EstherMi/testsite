---
layout: "image"
title: "Derrickwagen CC8800 Twin 11/12"
date: "2009-03-08T18:35:01"
picture: "cctwin11.jpg"
weight: "32"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/23432
imported:
- "2019"
_4images_image_id: "23432"
_4images_cat_id: "1596"
_4images_user_id: "389"
_4images_image_date: "2009-03-08T18:35:01"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23432 -->
Das Ganze nochmal mit ft Figur..