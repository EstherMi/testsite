---
layout: "image"
title: "Derrickwagen CC8800 Twin 3/13"
date: "2009-03-08T18:34:56"
picture: "cctwin03.jpg"
weight: "24"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/23424
imported:
- "2019"
_4images_image_id: "23424"
_4images_cat_id: "1596"
_4images_user_id: "389"
_4images_image_date: "2009-03-08T18:34:56"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23424 -->
Hier nochmal ein Größenvergelich