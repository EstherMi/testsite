---
layout: "image"
title: "Ausleger"
date: "2011-12-13T23:22:52"
picture: "liebherrltr32.jpg"
weight: "32"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/33675
imported:
- "2019"
_4images_image_id: "33675"
_4images_cat_id: "2490"
_4images_user_id: "833"
_4images_image_date: "2011-12-13T23:22:52"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33675 -->
So sind die Teleskope gelagert.
Durch das Seil fahren die zwei innersten Teleskope aus obwohl der Motor primär nur das äusserste Teleskop durch die Gewindestange bewegt.