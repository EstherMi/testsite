---
layout: "image"
title: "Raupen"
date: "2011-12-13T23:22:51"
picture: "liebherrltr10.jpg"
weight: "10"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/33653
imported:
- "2019"
_4images_image_id: "33653"
_4images_cat_id: "2490"
_4images_user_id: "833"
_4images_image_date: "2011-12-13T23:22:51"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33653 -->
Die Raupen werden je von einem 1:50 Powermot. angetrieben, der die Kraft von einem Z10 auf ein Z30 überträgt, auf dem die Raupe liegt.