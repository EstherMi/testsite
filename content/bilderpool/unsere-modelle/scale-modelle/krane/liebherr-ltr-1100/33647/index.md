---
layout: "image"
title: "Das Heck des Liebherr LTR 1100"
date: "2011-12-13T23:22:51"
picture: "liebherrltr04.jpg"
weight: "4"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/33647
imported:
- "2019"
_4images_image_id: "33647"
_4images_cat_id: "2490"
_4images_user_id: "833"
_4images_image_date: "2011-12-13T23:22:51"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33647 -->
Die Raupen sind noch zusammengeschoben, die Ketten die von den Zylindern herunter hängen, dienen zur Ballastaufnahme.