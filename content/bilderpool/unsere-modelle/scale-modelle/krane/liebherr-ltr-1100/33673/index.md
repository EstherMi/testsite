---
layout: "image"
title: "Hubzylinder"
date: "2011-12-13T23:22:52"
picture: "liebherrltr30.jpg"
weight: "30"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/33673
imported:
- "2019"
_4images_image_id: "33673"
_4images_cat_id: "2490"
_4images_user_id: "833"
_4images_image_date: "2011-12-13T23:22:52"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33673 -->
Durch die Achse die den Zylinder mit dem Ausleger verbindet, kann sich der "Kolben" nicht mit der Gewinderstange mitdrehen.