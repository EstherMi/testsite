---
layout: "image"
title: "Das Heck des Liebherr LTR 1100"
date: "2011-12-13T23:22:51"
picture: "liebherrltr05.jpg"
weight: "5"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/33648
imported:
- "2019"
_4images_image_id: "33648"
_4images_cat_id: "2490"
_4images_user_id: "833"
_4images_image_date: "2011-12-13T23:22:51"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33648 -->
Die Raupen sind in der Transportstellung wirklich sehr eng beeinander, dass liegt daran, dass der Kran in echt so gebaut ist, dass er im ganzen von einem Tieflader transportiert werden kann. Bei mir ist der Kran knapp 20 cm breit, dass ist sogar schon zuviel, nach Maßstab dürften es nur etwa 16 cm sein.