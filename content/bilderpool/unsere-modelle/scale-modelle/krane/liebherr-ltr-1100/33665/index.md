---
layout: "image"
title: "Heckballast"
date: "2011-12-13T23:22:51"
picture: "liebherrltr22.jpg"
weight: "22"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/33665
imported:
- "2019"
_4images_image_id: "33665"
_4images_cat_id: "2490"
_4images_user_id: "833"
_4images_image_date: "2011-12-13T23:22:51"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33665 -->
In echt sind die Hylinder so groß, dass die Ketten bis auf den Boden reichen und dort den Ballast aufnehmen können.