---
layout: "image"
title: "Liebherr LTR 1100"
date: "2011-12-13T23:22:51"
picture: "liebherrltr03.jpg"
weight: "3"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/33646
imported:
- "2019"
_4images_image_id: "33646"
_4images_cat_id: "2490"
_4images_user_id: "833"
_4images_image_date: "2011-12-13T23:22:51"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33646 -->
Der Ausleger ist in dieser stellung knapp 70 cm lang.