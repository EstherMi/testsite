---
layout: "image"
title: "Ausleger"
date: "2011-12-13T23:22:51"
picture: "liebherrltr18.jpg"
weight: "18"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/33661
imported:
- "2019"
_4images_image_id: "33661"
_4images_cat_id: "2490"
_4images_user_id: "833"
_4images_image_date: "2011-12-13T23:22:51"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33661 -->
Das ist der Blick des Kranführers