---
layout: "overview"
title: "Liebherr LTR 1100"
date: 2019-12-17T19:29:01+01:00
legacy_id:
- categories/2490
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2490 --> 
Mit diesem Modell ahbe ich den Liebherr LTR 1100 im Maßstab 1:20 Nachgebaut. Ich habe versucht alle Funktionen des Vorbilds umzusetzen. Wie zum Beispiel Teleskopausleger, Spurweitenverstellung und anhebbare Fahrerkabine. Die Jack Up Zylinder des Vorbilds konnte ich nicht nachbauen. Den Teleskopausleger und den Hebezylinder habe ich nicht aus ft, sondern aus Sperholz und allu-profilen gebastelt.