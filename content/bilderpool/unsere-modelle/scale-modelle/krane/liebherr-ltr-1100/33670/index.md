---
layout: "image"
title: "Seilwinde"
date: "2011-12-13T23:22:52"
picture: "liebherrltr27.jpg"
weight: "27"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/33670
imported:
- "2019"
_4images_image_id: "33670"
_4images_cat_id: "2490"
_4images_user_id: "833"
_4images_image_date: "2011-12-13T23:22:52"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33670 -->
Das Seil kommt aus dem Baumarkt und die Seilwinde wir von einem Minimot. bewegt.