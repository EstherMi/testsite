---
layout: "image"
title: "Schuifsyteem"
date: "2012-02-26T12:53:07"
picture: "terex_022.jpg"
weight: "23"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/34415
imported:
- "2019"
_4images_image_id: "34415"
_4images_cat_id: "2543"
_4images_user_id: "838"
_4images_image_date: "2012-02-26T12:53:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34415 -->
Kabel voor het in en uitschuiven van de mast. De mast is zo stevig dat hij eigenlijk niet door buigt.