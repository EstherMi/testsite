---
layout: "image"
title: "Zij aanzicht"
date: "2012-02-26T12:53:06"
picture: "terex_004.jpg"
weight: "6"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/34398
imported:
- "2019"
_4images_image_id: "34398"
_4images_cat_id: "2543"
_4images_user_id: "838"
_4images_image_date: "2012-02-26T12:53:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34398 -->
Linkerkant van de zware lummel