---
layout: "image"
title: "Terex Demag AC150"
date: "2012-02-26T12:53:07"
picture: "terex_007.jpg"
weight: "9"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/34401
imported:
- "2019"
_4images_image_id: "34401"
_4images_cat_id: "2543"
_4images_user_id: "838"
_4images_image_date: "2012-02-26T12:53:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34401 -->
Geheel op zijn automatisch uitschuifbare poten (uitschuiven gebeurt voor en achter de de schakelaars aan de rechterkant) 
Poten hadden eigenlijk cilinders moeten zijn maar dan had ik 4 meer nodig. De compressor was klaar maar is niet ingebouwd geworden