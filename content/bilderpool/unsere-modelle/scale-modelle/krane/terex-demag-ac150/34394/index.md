---
layout: "image"
title: "Terex Demag AC150"
date: "2012-02-25T10:58:24"
picture: "terex_002.jpg"
weight: "28"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/34394
imported:
- "2019"
_4images_image_id: "34394"
_4images_cat_id: "2543"
_4images_user_id: "838"
_4images_image_date: "2012-02-25T10:58:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34394 -->
Mijn nieuwste bouwsel (bloed zweet en tranen) 3 maanden aan gebouwd om alles werkend en sterk genoeg te krijgen.
Voorzien van 8 motoren 2x IR set, stuurbekrachtiging , 1.60m hoog en weegt denk ik 8kg.

Meer foto's volgen nog