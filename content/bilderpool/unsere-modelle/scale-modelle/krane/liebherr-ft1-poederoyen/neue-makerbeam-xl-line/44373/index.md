---
layout: "image"
title: "Neue Makerbeam-XL   Ansicht 3"
date: "2016-09-14T22:14:35"
picture: "neuemakerbeamxlline3.jpg"
weight: "3"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/44373
imported:
- "2019"
_4images_image_id: "44373"
_4images_cat_id: "3277"
_4images_user_id: "22"
_4images_image_date: "2016-09-14T22:14:35"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44373 -->
In October 2016 bij Makerbeam verkrijgbaar in de lengtes van 50mm/100mm/150mm/200mm/300mm/ 360mm(kossel)/500mm/750mm/750mm(kossel)/1000mm en 2000mm
In eerste instantie alleen in Black, Clear waarschijnlijk in later stadium.

Het nieuwe profiel past nog beter bij de fischertechnik-bouwstenen; zowel qua passing als qua uiterlijk !