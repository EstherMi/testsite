---
layout: "overview"
title: "Neue Makerbeam-XL line"
date: 2019-12-17T19:29:26+01:00
legacy_id:
- categories/3277
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3277 --> 
In October 2016 bij Makerbeam verkrijgbaar in de lengtes van 50mm/100mm/150mm/200mm/300mm/ 360mm(kossel)/500mm/750mm/750mm(kossel)/1000mm en 2000mm
In eerste instantie alleen in Black, Clear waarschijnlijk in later stadium.
