---
layout: "image"
title: "CC4800_3"
date: "2004-02-29T21:26:12"
picture: "TwinringLD_3_2.jpg"
weight: "3"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/2239
imported:
- "2019"
_4images_image_id: "2239"
_4images_cat_id: "212"
_4images_user_id: "144"
_4images_image_date: "2004-02-29T21:26:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2239 -->
