---
layout: "image"
title: "CC4800_8"
date: "2004-02-29T21:26:20"
picture: "TwinringLD_8.jpg"
weight: "8"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/2244
imported:
- "2019"
_4images_image_id: "2244"
_4images_cat_id: "212"
_4images_user_id: "144"
_4images_image_date: "2004-02-29T21:26:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2244 -->
