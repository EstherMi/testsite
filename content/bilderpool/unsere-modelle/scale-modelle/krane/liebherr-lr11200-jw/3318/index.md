---
layout: "image"
title: "Hakenflasche 4"
date: "2004-11-23T19:48:21"
picture: "Hakenflasche4.jpg"
weight: "4"
konstrukteure: 
- "Juergen Warwel"
fotografen:
- "Juergen Warwel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- details/3318
imported:
- "2019"
_4images_image_id: "3318"
_4images_cat_id: "230"
_4images_user_id: "107"
_4images_image_date: "2004-11-23T19:48:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3318 -->
