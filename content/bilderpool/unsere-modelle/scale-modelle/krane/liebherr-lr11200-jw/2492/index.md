---
layout: "image"
title: "Raupe von oben 1"
date: "2004-06-06T10:42:26"
picture: "LR11200_Raupe_von_oben_1_November_03.jpg"
weight: "51"
konstrukteure: 
- "Juergen Warwel"
fotografen:
- "Juergen Warwel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/2492
imported:
- "2019"
_4images_image_id: "2492"
_4images_cat_id: "230"
_4images_user_id: "1"
_4images_image_date: "2004-06-06T10:42:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2492 -->
