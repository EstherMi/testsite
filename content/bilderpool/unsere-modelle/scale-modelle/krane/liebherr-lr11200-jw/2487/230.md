---
layout: "comment"
hidden: true
title: "230"
date: "2004-06-06T20:49:42"
uploadBy:
- "jw-stg"
license: "unknown"
imported:
- "2019"
---
LR11200 Gesamtansicht_6_Ostern 2004Ja wo sind denn nur die Gewichte? Ganz einfach: Drehgestell mit 12 x 250 Gramm von meinem Tauchgurt (sieht wie ein Patronengürtel aus), 3 x 2 kg Blei vom gängigen Bleigurt und zwei Styropor Attrappen jeweils außen.