---
layout: "image"
title: "Hakenflasche 1"
date: "2004-11-23T18:40:59"
picture: "Hakenflasche1.jpg"
weight: "1"
konstrukteure: 
- "Juergen Warwel"
fotografen:
- "Juergen Warwel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- details/3308
imported:
- "2019"
_4images_image_id: "3308"
_4images_cat_id: "230"
_4images_user_id: "107"
_4images_image_date: "2004-11-23T18:40:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3308 -->
