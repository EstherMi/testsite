---
layout: "comment"
hidden: true
title: "228"
date: "2004-06-06T20:43:33"
uploadBy:
- "jw-stg"
license: "unknown"
imported:
- "2019"
---
LR11200 Gesamtansicht_4_Ostern 2004Die Gelenke zur Mastaufnahme habe ich nach Geldermalsen 2004 noch sicherer gegen Verschieben gesichert. Bei dem Versuch einen zwei Meter Nadelausleger mit Motorkraft mit anzuheben haben sich die Gelenksteine um weniger als 1 Millimeter einseitig verschoben. Diese Schwachstelle konnte ich jedoch eine Woche vor Geldermalsen 2004 nicht mehr beseitigen und so blieb es bei 4,2 Meter Gesamthöhe.