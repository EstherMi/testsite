---
layout: "image"
title: "Drehkranz - Doppellagerung 2"
date: "2004-11-23T21:59:33"
picture: "Drehkranz_-_Doppellagerung_2.jpg"
weight: "13"
konstrukteure: 
- "Juergen Warwel"
fotografen:
- "Juergen Warwel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- details/3327
imported:
- "2019"
_4images_image_id: "3327"
_4images_cat_id: "230"
_4images_user_id: "107"
_4images_image_date: "2004-11-23T21:59:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3327 -->
