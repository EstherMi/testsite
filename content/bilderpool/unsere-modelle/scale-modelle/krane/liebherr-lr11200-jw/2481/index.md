---
layout: "image"
title: "Zeichnung Drehgestell"
date: "2004-06-06T10:40:02"
picture: "LR11200_Drehgestell_Zchng.jpg"
weight: "41"
konstrukteure: 
- "Juergen Warwel"
fotografen:
- "Juergen Warwel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/2481
imported:
- "2019"
_4images_image_id: "2481"
_4images_cat_id: "230"
_4images_user_id: "1"
_4images_image_date: "2004-06-06T10:40:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2481 -->
