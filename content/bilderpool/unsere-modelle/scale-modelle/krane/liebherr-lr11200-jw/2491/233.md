---
layout: "comment"
hidden: true
title: "233"
date: "2004-06-06T20:55:57"
uploadBy:
- "jw-stg"
license: "unknown"
imported:
- "2019"
---
LR11200 Raupe ISO November 2003Glaubt mir, die Raupe funktioniert in jeder Situation und in jeder von mir geprüften Belastung (8 kg Last mit einem 2,3 Meter Gittermast bei einer Ausladung von 56 Zentimeter), es sei denn man stellt den LR11200 auf eine Tischdecke wie in Geldermalsen 2004 !!