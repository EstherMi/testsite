---
layout: "image"
title: "Gesamtansicht 1"
date: "2004-06-06T10:40:03"
picture: "LR11200_Gesamtansicht_1_Ostern_03.jpg"
weight: "42"
konstrukteure: 
- "Juergen Warwel"
fotografen:
- "Juergen Warwel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/2482
imported:
- "2019"
_4images_image_id: "2482"
_4images_cat_id: "230"
_4images_user_id: "1"
_4images_image_date: "2004-06-06T10:40:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2482 -->
