---
layout: "image"
title: "Basic truck"
date: "2014-01-24T01:01:20"
picture: "LG6.jpg"
weight: "2"
konstrukteure: 
- "AntonJansen"
fotografen:
- "AntonJansen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "anton"
license: "unknown"
legacy_id:
- details/38118
imported:
- "2019"
_4images_image_id: "38118"
_4images_cat_id: "2836"
_4images_user_id: "541"
_4images_image_date: "2014-01-24T01:01:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38118 -->
Al 8 axis can stear in normal and in crab stearing mode axis 1-2-4-6 are driven