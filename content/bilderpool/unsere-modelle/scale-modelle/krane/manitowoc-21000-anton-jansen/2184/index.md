---
layout: "image"
title: "Mani03.JPG"
date: "2004-02-23T00:24:27"
picture: "Mani03.jpg"
weight: "3"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2184
imported:
- "2019"
_4images_image_id: "2184"
_4images_cat_id: "210"
_4images_user_id: "4"
_4images_image_date: "2004-02-23T00:24:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2184 -->
Die Kettenfahrgestelle sind alle angetrieben, weiterhin werden die seitlichen Stützen elektrisch ausgefahren. Nach groben Schätzungen sind gut 12 Powermotoren und etliche M-Mots verbaut worden.