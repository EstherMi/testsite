---
layout: "image"
title: "Mani08.JPG"
date: "2004-02-23T00:24:27"
picture: "Mani08.jpg"
weight: "8"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2189
imported:
- "2019"
_4images_image_id: "2189"
_4images_cat_id: "210"
_4images_user_id: "4"
_4images_image_date: "2004-02-23T00:24:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2189 -->
