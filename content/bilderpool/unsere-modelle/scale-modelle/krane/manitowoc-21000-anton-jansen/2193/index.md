---
layout: "image"
title: "Mani14.JPG"
date: "2004-02-23T00:24:27"
picture: "Mani14.jpg"
weight: "12"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2193
imported:
- "2019"
_4images_image_id: "2193"
_4images_cat_id: "210"
_4images_user_id: "4"
_4images_image_date: "2004-02-23T00:24:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2193 -->
Der Radantrieb im Detail. Bei den riesigen Reifen ist ein direkter Antrieb über die Radachse nicht zu machen.
Das Differenzial verteilt die Kräfte auf die beiden Reifen. Die untere Stahlachse vorn im Bild verhindert, dass die Schnecke von der Welle springt.

Durch das Differenzial kann dieser Antriebsblock auch auf der Stelle gedreht werden. Dazu wird der Fahrmotor (unten) ausgeschaltet und der Drehkranz (oben) gedreht. Das ist nötig, wenn der ganze Kran vorwärts fahren soll. Zum Drehen des Kranaufbaus müssen die Räder wieder quer gestellt werden.