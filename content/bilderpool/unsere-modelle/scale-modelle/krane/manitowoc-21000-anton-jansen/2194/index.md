---
layout: "image"
title: "Mani15.JPG"
date: "2004-02-23T00:24:27"
picture: "Mani15.jpg"
weight: "13"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2194
imported:
- "2019"
_4images_image_id: "2194"
_4images_cat_id: "210"
_4images_user_id: "4"
_4images_image_date: "2004-02-23T00:24:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2194 -->
Hier die Räder beim Umschwenken von Fahr- auf Drehposition. Das ganze sieht aus, als würden die Reifen in majestätischer Ruhe umeinander herumtanzen.

Trick 17: Die Adaptierung der Supermonsterreifen an die ft-Drehscheibe erfolgt über Kettenglieder mit Noppen.