---
layout: "image"
title: "MK600 van Driessche_13"
date: "2016-10-31T17:20:45"
picture: "mkvandriessche13.jpg"
weight: "13"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/44728
imported:
- "2019"
_4images_image_id: "44728"
_4images_cat_id: "3331"
_4images_user_id: "144"
_4images_image_date: "2016-10-31T17:20:45"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44728 -->
Von vorne Links.