---
layout: "image"
title: "MK600 van Driessche_2"
date: "2016-10-31T17:20:45"
picture: "mkvandriessche02.jpg"
weight: "2"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/44717
imported:
- "2019"
_4images_image_id: "44717"
_4images_cat_id: "3331"
_4images_user_id: "144"
_4images_image_date: "2016-10-31T17:20:45"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44717 -->
In gegensatz zu die MK500-88 und MK600-89, wurde die MK600 als Sockelkran geliefert: der kran wurde zur Transport zwischen 2 Tiefladerfahrgestellen angebolzt.