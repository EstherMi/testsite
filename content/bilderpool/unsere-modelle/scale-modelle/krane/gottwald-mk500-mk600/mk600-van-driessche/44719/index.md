---
layout: "image"
title: "MK600 van Driessche_4"
date: "2016-10-31T17:20:45"
picture: "mkvandriessche04.jpg"
weight: "4"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/44719
imported:
- "2019"
_4images_image_id: "44719"
_4images_cat_id: "3331"
_4images_user_id: "144"
_4images_image_date: "2016-10-31T17:20:45"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44719 -->
Mir stand ein 1:25 MaßstabModell van einer Mack DM800 zur verfügung.
Wegen der Kleinen Maßstab hab ich das Modell gans einfach gehalten.