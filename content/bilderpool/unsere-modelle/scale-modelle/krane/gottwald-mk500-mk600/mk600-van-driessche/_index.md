---
layout: "overview"
title: "MK600 van Driessche"
date: 2019-12-17T19:29:39+01:00
legacy_id:
- categories/3331
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3331 --> 
Modell der in 1969 an die belgische Firma van Driessche gelieferte MK600 in Maßstab 1:27.