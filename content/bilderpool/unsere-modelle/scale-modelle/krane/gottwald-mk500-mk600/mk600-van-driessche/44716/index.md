---
layout: "image"
title: "MK600 van Driessche_1"
date: "2016-10-31T17:20:45"
picture: "mkvandriessche01.jpg"
weight: "1"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/44716
imported:
- "2019"
_4images_image_id: "44716"
_4images_cat_id: "3331"
_4images_user_id: "144"
_4images_image_date: "2016-10-31T17:20:45"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44716 -->
Modell der in 1969 an die belgische Firma van Driessche gelieferte MK600 in Maßstab 1:27.