---
layout: "image"
title: "MK600-89 Sparrow_6"
date: "2016-10-17T17:40:21"
picture: "mksparrow06.jpg"
weight: "6"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/44605
imported:
- "2019"
_4images_image_id: "44605"
_4images_cat_id: "3320"
_4images_user_id: "144"
_4images_image_date: "2016-10-17T17:40:21"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44605 -->
Der MK600 bekam zur besseren Druckverteilung Abstützschwingen mit jeweils 2 Stützplatten.