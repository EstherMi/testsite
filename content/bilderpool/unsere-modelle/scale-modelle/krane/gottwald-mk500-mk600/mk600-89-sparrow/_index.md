---
layout: "overview"
title: "MK600-89 Sparrow"
date: 2019-12-17T19:29:37+01:00
legacy_id:
- categories/3320
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3320 --> 
Modell der in 1971 an die Britische Firma Sparrow gelieferten 500t MK600-89 in Maßstab 1:27.