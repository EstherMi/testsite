---
layout: "image"
title: "MK600-89 Sparrow_7"
date: "2016-10-17T17:40:21"
picture: "mksparrow07.jpg"
weight: "7"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/44606
imported:
- "2019"
_4images_image_id: "44606"
_4images_cat_id: "3320"
_4images_user_id: "144"
_4images_image_date: "2016-10-17T17:40:21"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44606 -->
Die Stützplatten von oben.