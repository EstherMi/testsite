---
layout: "image"
title: "MK600-89 Sparrow_3"
date: "2016-10-17T17:40:21"
picture: "mksparrow03.jpg"
weight: "3"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/44602
imported:
- "2019"
_4images_image_id: "44602"
_4images_cat_id: "3320"
_4images_user_id: "144"
_4images_image_date: "2016-10-17T17:40:21"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44602 -->
Der MK600-89 bekam einem 3,5m breiten Farhgestel die auch 500 länger war als der MK500-88.