---
layout: "image"
title: "MK650 Schmidbauer_2"
date: "2016-11-06T18:31:38"
picture: "mkschmidbauer02.jpg"
weight: "2"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/44730
imported:
- "2019"
_4images_image_id: "44730"
_4images_cat_id: "3332"
_4images_user_id: "144"
_4images_image_date: "2016-11-06T18:31:38"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44730 -->
Einsatzbereid.