---
layout: "image"
title: "MK650 Schmidbauer_1"
date: "2016-11-06T18:31:38"
picture: "mkschmidbauer01.jpg"
weight: "1"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/44729
imported:
- "2019"
_4images_image_id: "44729"
_4images_cat_id: "3332"
_4images_user_id: "144"
_4images_image_date: "2016-11-06T18:31:38"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44729 -->
Modell der in 1972 an die Deutsche Firma Schmidbauer gelieferte MK650.
Konte aber auch der in 1968 an Toense gelieferte MK600 sein.