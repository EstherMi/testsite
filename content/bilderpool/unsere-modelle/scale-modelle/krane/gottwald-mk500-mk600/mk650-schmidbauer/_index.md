---
layout: "overview"
title: "MK650 Schmidbauer"
date: 2019-12-17T19:29:41+01:00
legacy_id:
- categories/3332
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3332 --> 
Modell der in 1972 an die Deutsche Firma Schmidbauer gelieferte MK650.