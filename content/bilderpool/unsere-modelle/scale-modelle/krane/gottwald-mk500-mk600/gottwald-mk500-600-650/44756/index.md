---
layout: "image"
title: "Gottwald MK500/600/650_1"
date: "2016-11-13T15:14:19"
picture: "gottwaldmk1.jpg"
weight: "2"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/44756
imported:
- "2019"
_4images_image_id: "44756"
_4images_cat_id: "3335"
_4images_user_id: "144"
_4images_image_date: "2016-11-13T15:14:19"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44756 -->
Die beide MK500-88 und die beide MK600-89.
Wen ich mir diesen Kränen ansehe falt mir auf das man eigentlich kein Modell bauen kan ohne einen Teil in ein andere farbe einbauen zu müssen. Die meisten Teilen gib es in Rot. Leider keine Streben. Nur 3 längen eben.