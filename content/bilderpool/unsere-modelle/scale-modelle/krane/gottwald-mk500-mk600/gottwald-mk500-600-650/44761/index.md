---
layout: "image"
title: "Gottwald MK500/600/650_6"
date: "2016-11-13T15:14:19"
picture: "gottwaldmk6.jpg"
weight: "7"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/44761
imported:
- "2019"
_4images_image_id: "44761"
_4images_cat_id: "3335"
_4images_user_id: "144"
_4images_image_date: "2016-11-13T15:14:19"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44761 -->
Das ist also alles was drin ist.
Es mus aber schon ein voller Batterie oder Akku sein.