---
layout: "image"
title: "Gottwald MK500/600/650_5"
date: "2016-11-13T15:14:19"
picture: "gottwaldmk5.jpg"
weight: "6"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/44760
imported:
- "2019"
_4images_image_id: "44760"
_4images_cat_id: "3335"
_4images_user_id: "144"
_4images_image_date: "2016-11-13T15:14:19"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44760 -->
Zur drehen der Kran hab ich an der Servoanschlus ein sehr kleine Fahrtregler angeschlossen.