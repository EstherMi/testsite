---
layout: "image"
title: "MK500-88 van Twist_9"
date: "2016-10-16T16:02:29"
picture: "gottwaldmkvantwist09.jpg"
weight: "17"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/44585
imported:
- "2019"
_4images_image_id: "44585"
_4images_cat_id: "3318"
_4images_user_id: "144"
_4images_image_date: "2016-10-16T16:02:29"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44585 -->
Noch mal das Obere ende der Aufrichtebock.