---
layout: "image"
title: "MK500-88 van Twist_16"
date: "2016-10-17T17:40:21"
picture: "mkvantwist5.jpg"
weight: "5"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/44596
imported:
- "2019"
_4images_image_id: "44596"
_4images_cat_id: "3318"
_4images_user_id: "144"
_4images_image_date: "2016-10-17T17:40:21"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44596 -->
Einer der 3 "Micro Metal Gear" motoren mit ein 1:50 Überzetsung.