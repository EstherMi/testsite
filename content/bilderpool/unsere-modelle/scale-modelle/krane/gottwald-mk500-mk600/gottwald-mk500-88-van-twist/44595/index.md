---
layout: "image"
title: "MK500-88 van Twist_15"
date: "2016-10-17T17:40:21"
picture: "mkvantwist4.jpg"
weight: "4"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/44595
imported:
- "2019"
_4images_image_id: "44595"
_4images_cat_id: "3318"
_4images_user_id: "144"
_4images_image_date: "2016-10-17T17:40:21"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44595 -->
Das Modell besitzt 4 Funktionen die alle Motorisiert sind.
Zur Steuerung hab ich ein kleine Platine hergestelt mit 8 Kontaktflächen.
In einer der Gegengewichten hab ich ein IR Emfänger mit ein 9V Batterie eingebaut.
