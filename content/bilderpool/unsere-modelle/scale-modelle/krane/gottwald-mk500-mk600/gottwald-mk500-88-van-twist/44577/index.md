---
layout: "image"
title: "MK500-88 van Twist_1"
date: "2016-10-16T16:02:29"
picture: "gottwaldmkvantwist01.jpg"
weight: "9"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/44577
imported:
- "2019"
_4images_image_id: "44577"
_4images_cat_id: "3318"
_4images_user_id: "144"
_4images_image_date: "2016-10-16T16:02:29"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44577 -->
Der in 1967 an die Niederländische Firma van Twist ausgelieferte Gottwald MK500-88 mit der 17m lange basis Ausleger.