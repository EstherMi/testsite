---
layout: "image"
title: "MK500-88 van Twist_14"
date: "2016-10-17T17:40:21"
picture: "mkvantwist3.jpg"
weight: "3"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/44594
imported:
- "2019"
_4images_image_id: "44594"
_4images_cat_id: "3318"
_4images_user_id: "144"
_4images_image_date: "2016-10-17T17:40:21"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44594 -->
Links neben der Drehkranz is der Rasritzel, zur drehung der Kran, zu sehen.