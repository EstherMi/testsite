---
layout: "image"
title: "MK500-88 van Twist_13"
date: "2016-10-17T17:40:21"
picture: "mkvantwist2.jpg"
weight: "2"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/44593
imported:
- "2019"
_4images_image_id: "44593"
_4images_cat_id: "3318"
_4images_user_id: "144"
_4images_image_date: "2016-10-17T17:40:21"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44593 -->
In wirklichkeit besteht das Gegengewicht aus 2 Stapel.
Um zu verhindern das die Seilen das Gegengewicht berühren gibt es einen Aussparung.