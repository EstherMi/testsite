---
layout: "image"
title: "MK500-88 van Twist_6"
date: "2016-10-16T16:02:29"
picture: "gottwaldmkvantwist06.jpg"
weight: "14"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/44582
imported:
- "2019"
_4images_image_id: "44582"
_4images_cat_id: "3318"
_4images_user_id: "144"
_4images_image_date: "2016-10-16T16:02:29"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44582 -->
Charakteristisch war auch das 5m breiten Gegengewicht.