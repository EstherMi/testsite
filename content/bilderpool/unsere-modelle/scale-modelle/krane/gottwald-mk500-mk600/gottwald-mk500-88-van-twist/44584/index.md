---
layout: "image"
title: "MK500-88 van Twist_8"
date: "2016-10-16T16:02:29"
picture: "gottwaldmkvantwist08.jpg"
weight: "16"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/44584
imported:
- "2019"
_4images_image_id: "44584"
_4images_cat_id: "3318"
_4images_user_id: "144"
_4images_image_date: "2016-10-16T16:02:29"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44584 -->
Die oberen Umlenkrollen.
In wirklichkeit gab es ein Traverse zur ausgleig der 2 Seilen.
Ich habbe aber ein 12er Seilrolle benutst.