---
layout: "image"
title: "MK500-88 van Twist_4"
date: "2016-10-16T16:02:29"
picture: "gottwaldmkvantwist04.jpg"
weight: "12"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/44580
imported:
- "2019"
_4images_image_id: "44580"
_4images_cat_id: "3318"
_4images_user_id: "144"
_4images_image_date: "2016-10-16T16:02:29"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44580 -->
Charakteristisch waren auch die rundum doppelt bereiften 8 Achsen.