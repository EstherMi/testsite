---
layout: "overview"
title: "MK600-89 Buzzichelli"
date: 2019-12-17T19:29:38+01:00
legacy_id:
- categories/3321
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3321 --> 
Modell der in 1972 an der Französische Firma Buzzichelli gelieferte 500t MK600-89.