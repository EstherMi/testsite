---
layout: "image"
title: "MK600-89 Buzzichelli_9"
date: "2016-10-17T17:40:21"
picture: "mkbuzzichelli09.jpg"
weight: "9"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/44618
imported:
- "2019"
_4images_image_id: "44618"
_4images_cat_id: "3321"
_4images_user_id: "144"
_4images_image_date: "2016-10-17T17:40:21"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44618 -->
Abstützschwinge.
Auch LPE: die Gelbe Freilaufnaben .