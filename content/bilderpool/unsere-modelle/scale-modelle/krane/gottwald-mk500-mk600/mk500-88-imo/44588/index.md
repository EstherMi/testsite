---
layout: "image"
title: "MK500-88 IMO_1"
date: "2016-10-16T16:02:29"
picture: "mkimo1.jpg"
weight: "1"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/44588
imported:
- "2019"
_4images_image_id: "44588"
_4images_cat_id: "3319"
_4images_user_id: "144"
_4images_image_date: "2016-10-16T16:02:29"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44588 -->
Der MK500-88 in Fahrstellung mit entfernten Heckteil.