---
layout: "overview"
title: "MK500-88 IMO"
date: 2019-12-17T19:29:36+01:00
legacy_id:
- categories/3319
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3319 --> 
Modell der in 1971 an die Firma IMO Merseburg gelieferte MK500-88 in Maßstab 1:27.