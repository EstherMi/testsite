---
layout: "image"
title: "MK650 Stoof_4"
date: "2016-11-13T15:14:18"
picture: "mkstoof4.jpg"
weight: "4"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/44746
imported:
- "2019"
_4images_image_id: "44746"
_4images_cat_id: "3333"
_4images_user_id: "144"
_4images_image_date: "2016-11-13T15:14:18"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44746 -->
Auch hier fehlt der Unterwagenballast von nunmehr 60t.