---
layout: "overview"
title: "MK650 Stoof"
date: 2019-12-17T19:29:43+01:00
legacy_id:
- categories/3333
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3333 --> 
Modell der in 1972 an die niederländische Firma Stoof gelieferte MK650 in Maßstab 1:27.