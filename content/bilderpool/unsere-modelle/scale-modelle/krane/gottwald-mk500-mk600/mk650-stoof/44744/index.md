---
layout: "image"
title: "MK650 Stoof_2"
date: "2016-11-13T15:14:18"
picture: "mkstoof2.jpg"
weight: "2"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/44744
imported:
- "2019"
_4images_image_id: "44744"
_4images_cat_id: "3333"
_4images_user_id: "144"
_4images_image_date: "2016-11-13T15:14:18"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44744 -->
Vordere Tiefladerfahrgestel mit jetzt 4 Achslienen mit je 4 Räder.