---
layout: "image"
title: "MK650 Sarens_3"
date: "2016-11-13T15:14:19"
picture: "mksarens3.jpg"
weight: "3"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/44751
imported:
- "2019"
_4images_image_id: "44751"
_4images_cat_id: "3334"
_4images_user_id: "144"
_4images_image_date: "2016-11-13T15:14:19"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44751 -->
Der Sockelballast wurde auf 62,5t gesteigert, aber das Gegegngewicht wurde auf 125t reduziert.