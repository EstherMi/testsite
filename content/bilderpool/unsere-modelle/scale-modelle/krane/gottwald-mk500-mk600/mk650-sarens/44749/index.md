---
layout: "image"
title: "MK650 Sarens_1"
date: "2016-11-13T15:14:18"
picture: "mksarens1.jpg"
weight: "1"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/44749
imported:
- "2019"
_4images_image_id: "44749"
_4images_cat_id: "3334"
_4images_user_id: "144"
_4images_image_date: "2016-11-13T15:14:18"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44749 -->
Modell der in 1972 an die belgische Firma Saren gelieferte MK650 in Maßstab 1:27.