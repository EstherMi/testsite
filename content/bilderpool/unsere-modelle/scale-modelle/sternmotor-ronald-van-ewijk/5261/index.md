---
layout: "image"
title: "Stern02.JPG"
date: "2005-11-07T19:33:53"
picture: "Stern02.JPG"
weight: "1"
konstrukteure: 
- "Ronald van Ewijk"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5261
imported:
- "2019"
_4images_image_id: "5261"
_4images_cat_id: "451"
_4images_user_id: "4"
_4images_image_date: "2005-11-07T19:33:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5261 -->
Ein Sternmotor mit 9 Zylindern. Die Gesamtansicht ist nichts geworden, vielleicht hat das jemand anderes noch?

Vom Propeller sieht man nur die Nabe (links unten), eine Alu-Schiene (unten) und die Ecke eines Flügels (transparente Platte) am linken Rand.

Hinter dem großen "Zahnrad" aus Kettengliedern rotieren zwei Nockenwellen (mit schwarzen Bausteinen), die die Ein- und Auslassventile (Schubstangen und je graue Kardanklauen pro Zylinder) betätigen.

Die ft-Winkelsteine sind sehr gutmütig, so dass die Teilung eines 360°-Vollwinkels  in 9 gleiche Teile zu je 30°+7,5° = 37,5° sauber aufgeht ;-)