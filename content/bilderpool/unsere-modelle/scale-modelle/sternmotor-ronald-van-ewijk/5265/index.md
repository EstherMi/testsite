---
layout: "image"
title: "Stern06.JPG"
date: "2005-11-07T19:47:43"
picture: "Stern06.JPG"
weight: "5"
konstrukteure: 
- "Ronald van Ewijk"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5265
imported:
- "2019"
_4images_image_id: "5265"
_4images_cat_id: "451"
_4images_user_id: "4"
_4images_image_date: "2005-11-07T19:47:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5265 -->
Auch bei der Ansteuerung der Zündkerzen geht die Rechnung "360° durch 9 gleich 37,5°" sauber auf, wenn man die Bausteine ein wenig etwas nach außen versetzt (also nicht bündig mit den Winkelsteinen) anbringt.