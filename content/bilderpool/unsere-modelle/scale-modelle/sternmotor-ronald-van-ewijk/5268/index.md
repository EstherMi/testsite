---
layout: "image"
title: "Freilauf01.JPG"
date: "2005-11-07T19:53:29"
picture: "Freilauf01.JPG"
weight: "8"
konstrukteure: 
- "Ronald van Ewijk"
fotografen:
- "Harald Steinhaus"
keywords: ["Freilauf"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5268
imported:
- "2019"
_4images_image_id: "5268"
_4images_cat_id: "451"
_4images_user_id: "4"
_4images_image_date: "2005-11-07T19:53:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5268 -->
Die Nabe enthält noch ein besonderes Schmankerl: einen Freilauf für die gegenläufige Drehrichtung. Das ganze beginnt mit den beiden Federnocken in den Bohrungen der Drehscheibe.