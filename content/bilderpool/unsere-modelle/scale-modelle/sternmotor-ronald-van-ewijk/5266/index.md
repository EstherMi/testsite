---
layout: "image"
title: "Stern07.JPG"
date: "2005-11-07T19:48:51"
picture: "Stern07.JPG"
weight: "6"
konstrukteure: 
- "Ronald van Ewijk"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5266
imported:
- "2019"
_4images_image_id: "5266"
_4images_cat_id: "451"
_4images_user_id: "4"
_4images_image_date: "2005-11-07T19:48:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5266 -->
Die Kipphebel zur Ventilsteuerung. Die Ventile sitzen in den grauen Statik-BS30.