---
layout: "overview"
title: "Ur-Unimog"
date: 2019-12-17T19:30:56+01:00
legacy_id:
- categories/2915
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2915 --> 
Hallo,

bei diesem Model habe ich versucht möglichst genau den original Ur-Unimog nach zubauen.

Daten:
•	Vorbild: 		Mercedes Benz Ur Unimog (Unimog 401)
•	Antrieb: 		zwei 1:50 Powermotoren
•	Lenkung: 		Mini- Motor
•	Beleuchtung: 	zwei ft Lampen