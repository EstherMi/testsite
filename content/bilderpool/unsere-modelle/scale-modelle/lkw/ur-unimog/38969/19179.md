---
layout: "comment"
hidden: true
title: "19179"
date: "2014-06-21T17:26:00"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Die Optik ist 100% gelungen!

Aber wenn ich nur wüßt, was drinnen ist...

Mir will scheinen, dass die Hinterachse mit einem Gelenk in Richtung Fahrzeugmitte befestigt ist und der Antriebsstrang schräg nach unten führt.