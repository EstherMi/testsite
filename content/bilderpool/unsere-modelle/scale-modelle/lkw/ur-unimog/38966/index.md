---
layout: "image"
title: "Die Federung"
date: "2014-06-19T19:13:42"
picture: "Bild_022.jpg"
weight: "6"
konstrukteure: 
- "Julian Bußmeier"
fotografen:
- "Julian Bußmeier"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "JulianBussmeier"
license: "unknown"
legacy_id:
- details/38966
imported:
- "2019"
_4images_image_id: "38966"
_4images_cat_id: "2915"
_4images_user_id: "1474"
_4images_image_date: "2014-06-19T19:13:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38966 -->
Damit die Federn nicht zu hoch belastet werden, wurden pro Achse 4 Federn verbaut.