---
layout: "comment"
hidden: true
title: "15657"
date: "2011-11-08T18:57:47"
uploadBy:
- "Stainless"
license: "unknown"
imported:
- "2019"
---
Das lässt sich aber auch lösen, indem man einen Abstandsring in unten in das Gelenk einschiebt; da sich die Hülse nicht ganz einschieben lässt, hällt es auch so perfekt.
Das Konzept funktioniert also nicht nur auf Haralds Konzept!

Stainless :)