---
layout: "image"
title: "Unimog U1300L 10"
date: "2011-11-05T17:50:28"
picture: "Unimog_U1300L_10.jpg"
weight: "10"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/33401
imported:
- "2019"
_4images_image_id: "33401"
_4images_cat_id: "2473"
_4images_user_id: "328"
_4images_image_date: "2011-11-05T17:50:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33401 -->
Das Modell hat keine Federung, aber hinten eine Pendelachse, die per Blattfedern gerade gehalten wird. Die mögliche Pendelung ist beachtlich; pro Seite gibt es daher einen Anschlag der Achse am Rahmen, damit die Räder nirgends streifen.

Die Vorderachse ist nicht pendelnd gelagert. Die Pendelung im Gelände ergibt sich ausschließlich aus den Elastizitäten des kleinen Drehgelenks (streifen tut aber nichts)! Großartig, denn so ergibt sich eine beeindruckende Verschränkung der beiden Achsen.