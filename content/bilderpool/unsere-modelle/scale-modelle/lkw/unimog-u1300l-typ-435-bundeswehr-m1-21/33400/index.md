---
layout: "image"
title: "Unimog U1300L 09"
date: "2011-11-05T17:50:28"
picture: "Unimog_U1300L_09.jpg"
weight: "9"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/33400
imported:
- "2019"
_4images_image_id: "33400"
_4images_cat_id: "2473"
_4images_user_id: "328"
_4images_image_date: "2011-11-05T17:50:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33400 -->
Im Führerhaus der Hauptschalter zum Ein-/Ausschalten des Empfängers.