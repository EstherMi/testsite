---
layout: "image"
title: "Unimog U1300L 20"
date: "2011-11-05T17:50:28"
picture: "Unimog_U1300L_20.jpg"
weight: "20"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/33411
imported:
- "2019"
_4images_image_id: "33411"
_4images_cat_id: "2473"
_4images_user_id: "328"
_4images_image_date: "2011-11-05T17:50:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33411 -->
Man sieht gut, dass der Bastelkarton sehr dick ist. Der Verbau mit den Verschlussriegeln war auch nicht ganz einfach. Der Vorteil ist, dass die Plane wirklich sehr stabil ist und auch harten Spielalltag gut übersteht.

Höhe und Breite der Plane sind natürlich auch exakt maßstabsgetreu.