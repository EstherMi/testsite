---
layout: "image"
title: "Unimog U1300L 17"
date: "2011-11-05T17:50:28"
picture: "Unimog_U1300L_17.jpg"
weight: "17"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/33408
imported:
- "2019"
_4images_image_id: "33408"
_4images_cat_id: "2473"
_4images_user_id: "328"
_4images_image_date: "2011-11-05T17:50:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33408 -->
So sieht's unter der Plane aus.