---
layout: "image"
title: "Unimog U1300L 06"
date: "2011-11-05T17:50:28"
picture: "Unimog_U1300L_06.jpg"
weight: "6"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/33397
imported:
- "2019"
_4images_image_id: "33397"
_4images_cat_id: "2473"
_4images_user_id: "328"
_4images_image_date: "2011-11-05T17:50:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33397 -->
Die Plane ist hinten offen, damit der IR-Empfänger zuverlässig seine Signale empfangen kann.

Planen-Aufbau und Führerhaus lassen sich für Service-Arbeiten sehr leicht nach oben abziehen; es sind keine komplizierten Zerlegungen notwendig.