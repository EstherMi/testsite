---
layout: "image"
title: "Unimog U1300L 16"
date: "2011-11-05T17:50:28"
picture: "Unimog_U1300L_16.jpg"
weight: "16"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/33407
imported:
- "2019"
_4images_image_id: "33407"
_4images_cat_id: "2473"
_4images_user_id: "328"
_4images_image_date: "2011-11-05T17:50:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33407 -->
Das Führerhaus ist abgenommen. Gut zu sehen sind die Drehschemellenkung und die stabile Halterung des Servos.

Um im Gelände ein übermäßiges Einlenken der Vorderachse zu vermeiden, was evtl. zu Schäden am Servo führen kann, hat die Achse Lenkwinkelbegrenzungen seitlich neben dem Drehgelenk, die exakt zum maximalen Servoausschlag passen.