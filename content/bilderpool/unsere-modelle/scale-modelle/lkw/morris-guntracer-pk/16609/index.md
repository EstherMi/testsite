---
layout: "image"
title: "MORRIS C.8.Mkll _11"
date: "2008-12-14T10:53:30"
picture: "MORIS_GUNTRACER_3_2.jpg"
weight: "11"
konstrukteure: 
- "peter krijnen"
fotografen:
- "peter krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/16609
imported:
- "2019"
_4images_image_id: "16609"
_4images_cat_id: "1503"
_4images_user_id: "144"
_4images_image_date: "2008-12-14T10:53:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16609 -->
