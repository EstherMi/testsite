---
layout: "image"
title: "rad mit antrieb_1"
date: "2005-03-30T20:56:04"
picture: "rad_mit_antrieb_1.jpg"
weight: "28"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/3931
imported:
- "2019"
_4images_image_id: "3931"
_4images_cat_id: "329"
_4images_user_id: "144"
_4images_image_date: "2005-03-30T20:56:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3931 -->
Das Resept:

Man nehme:
3 x 31019 - Drehscheibe 60
1 x 31023 - Klembusche 10 mit federring
2 x 31058 - Nabenmutter mit scheibe
6 x 32316 - Verbindungstopfen
6 x 32882 - Baustein 15 mit 2 Zapfen
2 x 32928 - Freilaufnabe, Rot
1 x 35694 - Innenzahnrad Z30 m1,5
82 x 37192 - Förderglied
6 x 37237 - Baustein 5
6 x 37238 - Baustein 5 mit 2 zapfen
unterlegscheiben M4