---
layout: "image"
title: "terex_12"
date: "2005-03-26T19:25:19"
picture: "terex_12.jpg"
weight: "54"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/3867
imported:
- "2019"
_4images_image_id: "3867"
_4images_cat_id: "329"
_4images_user_id: "144"
_4images_image_date: "2005-03-26T19:25:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3867 -->
Einer der beide Powermots.
Hier noch die 8:1.