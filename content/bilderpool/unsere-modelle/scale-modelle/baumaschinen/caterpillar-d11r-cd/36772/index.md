---
layout: "image"
title: "CAT D11R CD (Carrydozer) Seitenansicht"
date: "2013-03-19T22:15:53"
picture: "drcd01.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/36772
imported:
- "2019"
_4images_image_id: "36772"
_4images_cat_id: "2727"
_4images_user_id: "162"
_4images_image_date: "2013-03-19T22:15:53"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36772 -->
1:10 Scale D11R CD Carrydozer nach Modell Norscot 1:50 und Caterpillar information auf www.

Lange: 115 cm
Breite Schauffel: 63cm
Hohe: 48cm
Breite Machine excl Schauffel: 49cm