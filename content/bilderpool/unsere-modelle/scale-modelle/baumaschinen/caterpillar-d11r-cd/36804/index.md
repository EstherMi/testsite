---
layout: "image"
title: "Innenleben ; Ripper hoch"
date: "2013-03-24T00:01:31"
picture: "catdrcd1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/36804
imported:
- "2019"
_4images_image_id: "36804"
_4images_cat_id: "2727"
_4images_user_id: "162"
_4images_image_date: "2013-03-24T00:01:31"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36804 -->
Hier einige Bilder vons Innenleben vom Cat D11R CD.
Viel ist hier nicht zu sehen ausser die Antriebsmotore und Motoren fur den Ripper

Als erste hatte ich fur denn Antrieb Powermotore verwendet, aber die Steckten zuviel nach innen damit Antrieb vom Ripper nicht mehr moglich war. Mit die M-motore passte das viel besser.