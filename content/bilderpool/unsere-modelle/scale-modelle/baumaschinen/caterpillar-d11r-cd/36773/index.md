---
layout: "image"
title: "CAT D11R CD (Carrydozer) Seitenansicht"
date: "2013-03-19T22:15:53"
picture: "drcd02.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/36773
imported:
- "2019"
_4images_image_id: "36773"
_4images_cat_id: "2727"
_4images_user_id: "162"
_4images_image_date: "2013-03-19T22:15:53"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36773 -->
Eingebaut sind jetzt:

2x 50:1 powermotoren fur den Ripper (hohe verstellung)
2x M-motoren fur Winkel richtung Ripper
4x M-motoren; 2 fur Raupen Antrieb links, 2 fur Raupen Antrieb rechts
2x 8:1 powermotoren fur Schauffel oben/unten
kommt noch: 2 Motoren fur den Schauffel Winkel einstellung