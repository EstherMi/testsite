---
layout: "image"
title: "CAT D11R CD (Carrydozer) Raupen"
date: "2013-03-19T22:15:53"
picture: "drcd11.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/36782
imported:
- "2019"
_4images_image_id: "36782"
_4images_cat_id: "2727"
_4images_user_id: "162"
_4images_image_date: "2013-03-19T22:15:53"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36782 -->
Das schwierigste war die durchmesser von den Rader zu bekommen. Der "Grundreifen" sind 90mm und den Hauptzahnrad ist 100mm. Wegen dieses underscheid was er schwierig das Raster vom Raupen zu machen. 
Als erste hatte ich den Distanzhulse 31983 gebraucht, aber damit wurde den Abstand zwischen die Achse zu klein und Laufte das nicht schon uber die Zahnrader. Spater habe ich die Distanzhulse ausgetauscht mit Lagerhulse 36819.
Ich muss sagen das die Raupen ganz gut Laufen. Jetzt ist es fur die M-motoren ein bisschen schwer, aber mit weniger Last lauft das super.