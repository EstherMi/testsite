---
layout: "image"
title: "CAT D11R CD (Carrydozer) 1:10 und 1:50"
date: "2013-03-19T22:15:53"
picture: "drcd13.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/36784
imported:
- "2019"
_4images_image_id: "36784"
_4images_cat_id: "2727"
_4images_user_id: "162"
_4images_image_date: "2013-03-19T22:15:53"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36784 -->
Hier das 1:50 Modell mit das 1:10 Modell