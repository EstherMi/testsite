---
layout: "image"
title: "CAT D11R CD (Carrydozer) Hinterseite Schauffel"
date: "2013-03-19T22:15:53"
picture: "drcd14.jpg"
weight: "20"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/36785
imported:
- "2019"
_4images_image_id: "36785"
_4images_cat_id: "2727"
_4images_user_id: "162"
_4images_image_date: "2013-03-19T22:15:53"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36785 -->
Den Schauffel ist gebaut aus Grundplatten 35129 und Grundplatten 36578. Die sind mit einander Verbunden durch Schaufelhalter 38411