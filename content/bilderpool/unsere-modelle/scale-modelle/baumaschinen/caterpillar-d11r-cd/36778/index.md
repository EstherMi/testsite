---
layout: "image"
title: "CAT D11R CD (Carrydozer) Schauffel"
date: "2013-03-19T22:15:53"
picture: "drcd07.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/36778
imported:
- "2019"
_4images_image_id: "36778"
_4images_cat_id: "2727"
_4images_user_id: "162"
_4images_image_date: "2013-03-19T22:15:53"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36778 -->
