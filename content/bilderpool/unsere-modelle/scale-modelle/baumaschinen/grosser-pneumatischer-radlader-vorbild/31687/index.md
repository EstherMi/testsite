---
layout: "image"
title: "hinten Rechts"
date: "2011-08-29T10:16:38"
picture: "catg17.jpg"
weight: "17"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/31687
imported:
- "2019"
_4images_image_id: "31687"
_4images_cat_id: "2362"
_4images_user_id: "833"
_4images_image_date: "2011-08-29T10:16:38"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31687 -->
das ist die hintere rechte Ecke. das blaue ist der Lufttank, die Taster sind zum Fahren. etwas tiefer sieht man einen Conrad Modellbauakku, der den STrom liefert.