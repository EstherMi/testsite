---
layout: "image"
title: "Vorder Achse"
date: "2011-08-29T10:16:37"
picture: "catg04.jpg"
weight: "4"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/31674
imported:
- "2019"
_4images_image_id: "31674"
_4images_cat_id: "2362"
_4images_user_id: "833"
_4images_image_date: "2011-08-29T10:16:37"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31674 -->
die Schnecke treibt das alte Differential an. Das Differential was als Portalachse noch eine Grundbausteinhöhe über der Radachse liegt treibt über ein Z10 dann die Achse 1:1 an.