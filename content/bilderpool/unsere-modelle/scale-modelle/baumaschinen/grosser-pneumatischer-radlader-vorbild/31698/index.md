---
layout: "image"
title: "Die Schaufel"
date: "2011-08-29T10:16:38"
picture: "catg28.jpg"
weight: "28"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/31698
imported:
- "2019"
_4images_image_id: "31698"
_4images_cat_id: "2362"
_4images_user_id: "833"
_4images_image_date: "2011-08-29T10:16:38"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31698 -->
Das Lackieren hat viel Spaß gemacht, die Zähne sehen wirklich echt aus!