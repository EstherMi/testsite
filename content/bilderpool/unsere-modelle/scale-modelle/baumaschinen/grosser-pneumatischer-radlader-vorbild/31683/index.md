---
layout: "image"
title: "Baggerschaufel"
date: "2011-08-29T10:16:37"
picture: "catg13.jpg"
weight: "13"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/31683
imported:
- "2019"
_4images_image_id: "31683"
_4images_cat_id: "2362"
_4images_user_id: "833"
_4images_image_date: "2011-08-29T10:16:37"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31683 -->
Die Schaufel habe ich aus einem Verbund von Sperrholz und Pappe gebaut, ebenso den Hubarm. Das ist stabiler und leichter als FT. Ausserdem kann man genau die Form herstellen die man braucht.