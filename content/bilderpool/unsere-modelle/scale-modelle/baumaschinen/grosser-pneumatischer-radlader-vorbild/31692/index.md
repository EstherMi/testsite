---
layout: "image"
title: "Bagger Heck"
date: "2011-08-29T10:16:38"
picture: "catg22.jpg"
weight: "22"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/31692
imported:
- "2019"
_4images_image_id: "31692"
_4images_cat_id: "2362"
_4images_user_id: "833"
_4images_image_date: "2011-08-29T10:16:38"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31692 -->
Der Grüne Schlauch führt vom Kompressor zum Tank, weil der Kompressoranschluss für einen ft-Schlauch zudick ist.