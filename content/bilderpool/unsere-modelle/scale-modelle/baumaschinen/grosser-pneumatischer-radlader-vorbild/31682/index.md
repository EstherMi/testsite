---
layout: "image"
title: "Radlader bei der Arbeit"
date: "2011-08-29T10:16:37"
picture: "catg12.jpg"
weight: "12"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/31682
imported:
- "2019"
_4images_image_id: "31682"
_4images_cat_id: "2362"
_4images_user_id: "833"
_4images_image_date: "2011-08-29T10:16:37"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31682 -->
Die Maschine stößt in den Abraum!