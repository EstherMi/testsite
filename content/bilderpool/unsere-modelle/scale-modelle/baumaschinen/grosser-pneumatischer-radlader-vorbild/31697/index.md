---
layout: "image"
title: "Scheinwerfer"
date: "2011-08-29T10:16:38"
picture: "catg27.jpg"
weight: "27"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/31697
imported:
- "2019"
_4images_image_id: "31697"
_4images_cat_id: "2362"
_4images_user_id: "833"
_4images_image_date: "2011-08-29T10:16:38"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31697 -->
weil der Akku 7,2 V liefert und die alten Lämpchen nur 6 V vertragen sind die Lichter in Reihe Geschaltet, was immer noch genug Licht bringt.