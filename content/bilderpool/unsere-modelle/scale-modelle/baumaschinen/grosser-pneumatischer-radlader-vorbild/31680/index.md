---
layout: "image"
title: "fahr den Berg hoch!"
date: "2011-08-29T10:16:37"
picture: "catg10.jpg"
weight: "10"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/31680
imported:
- "2019"
_4images_image_id: "31680"
_4images_cat_id: "2362"
_4images_user_id: "833"
_4images_image_date: "2011-08-29T10:16:37"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31680 -->
so wirklich zuverlässig bewegt sich das Monster nicht auf diesem Untergrund. Das Problem sind die die Z10 an den Achsen, sie werden von den Motoren gedreht aber die Rad-Achsen drehen sich nicht.