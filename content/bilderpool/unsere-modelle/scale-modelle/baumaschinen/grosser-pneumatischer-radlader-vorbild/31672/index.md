---
layout: "image"
title: "Der Radlader mit gefüllter Schaufel"
date: "2011-08-29T10:16:37"
picture: "catg02.jpg"
weight: "2"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/31672
imported:
- "2019"
_4images_image_id: "31672"
_4images_cat_id: "2362"
_4images_user_id: "833"
_4images_image_date: "2011-08-29T10:16:37"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31672 -->
Der Baggerarm und die Schaufel konnte bei dieser Befüllung noch bewegt werden, nicht optimal aber es bewegte sich. 
Das habe ich nicht selber angehoben!