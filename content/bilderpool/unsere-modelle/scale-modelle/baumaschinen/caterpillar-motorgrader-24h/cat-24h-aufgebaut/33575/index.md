---
layout: "image"
title: "Caterpillar motorgrader 24H, Pflug"
date: "2011-11-27T00:13:35"
picture: "cath12.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/33575
imported:
- "2019"
_4images_image_id: "33575"
_4images_cat_id: "2486"
_4images_user_id: "162"
_4images_image_date: "2011-11-27T00:13:35"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33575 -->
