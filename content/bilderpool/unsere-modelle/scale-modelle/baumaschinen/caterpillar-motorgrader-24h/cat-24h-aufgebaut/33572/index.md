---
layout: "image"
title: "Caterpillar motorgrader 24H, Freitreppe nach unten"
date: "2011-11-27T00:13:34"
picture: "cath09.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/33572
imported:
- "2019"
_4images_image_id: "33572"
_4images_cat_id: "2486"
_4images_user_id: "162"
_4images_image_date: "2011-11-27T00:13:34"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33572 -->
Freitreppen weden Pneumatisch bedient durch schalten von magnetventile. Insgesammt sind 6 Zylinder verbaut.