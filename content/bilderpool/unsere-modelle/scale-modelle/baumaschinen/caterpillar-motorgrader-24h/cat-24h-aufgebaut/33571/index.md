---
layout: "image"
title: "Caterpillar motorgrader 24H, Lenkanlage (von unten)"
date: "2011-11-27T00:13:34"
picture: "cath08.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/33571
imported:
- "2019"
_4images_image_id: "33571"
_4images_cat_id: "2486"
_4images_user_id: "162"
_4images_image_date: "2011-11-27T00:13:34"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33571 -->
Kette zwischen Zahnrad Z10 und Powermotor ist abgelegt.