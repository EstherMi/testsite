---
layout: "comment"
hidden: true
title: "15784"
date: "2011-11-29T22:31:25"
uploadBy:
- "jmn"
license: "unknown"
imported:
- "2019"
---
Hallo Marten,

Um der Motor 180 grad zu drehen habe ich mir auch schon uberlegt. Fur Erbes-Budesheim hatte ich kein Zeit es zu andern und auch leider bisher noch kein Zeit gefunden.
Ich wollte aber gerne den Motor ins Motorraum einbauen und dann mit achsen und kreuzkupplungen arbeiten, damit der Motor nicht mehr im Sicht ist. Werd aber schwierig, weil es etwas kraft braucht und ich denke das dann die Kreuzkupplungen ausbrechen.

Trotzdem dank fur deine Kommentare..

jmn