---
layout: "image"
title: "Caterpillar motorgrader 24H, Pflug"
date: "2011-11-27T00:13:35"
picture: "cath11.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/33574
imported:
- "2019"
_4images_image_id: "33574"
_4images_cat_id: "2486"
_4images_user_id: "162"
_4images_image_date: "2011-11-27T00:13:35"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33574 -->
Leider wenig raum ein Motor und Antrieb rein zu bauen.