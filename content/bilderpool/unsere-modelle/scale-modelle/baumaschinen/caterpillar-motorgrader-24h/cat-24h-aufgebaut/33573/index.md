---
layout: "image"
title: "Caterpillar motorgrader 24H, Freitreppe hoch"
date: "2011-11-27T00:13:35"
picture: "cath10.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/33573
imported:
- "2019"
_4images_image_id: "33573"
_4images_cat_id: "2486"
_4images_user_id: "162"
_4images_image_date: "2011-11-27T00:13:35"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33573 -->
Hier seht mann die Freitreppe hoch. Auf diese weise kann der Fahrer von die Treppe nach unten kommen. Mit die Freitreppe nach unten steigt er hinten die Reifen ab.