---
layout: "image"
title: "Caterpillar Motorgrader 24H blade"
date: "2011-04-02T23:50:37"
picture: "caterpillarmotorgradeh11.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/30385
imported:
- "2019"
_4images_image_id: "30385"
_4images_cat_id: "2246"
_4images_user_id: "162"
_4images_image_date: "2011-04-02T23:50:37"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30385 -->
Das Blatt kan nach links und rechts bewegen und auch noch kippen. Antrieb fur das kippen muss noch eingebaut werden. Blatt ist 75 cm breit