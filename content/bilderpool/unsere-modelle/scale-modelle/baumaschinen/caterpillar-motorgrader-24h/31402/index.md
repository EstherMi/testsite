---
layout: "image"
title: "Antrieb Cat 24H"
date: "2011-07-28T21:44:14"
picture: "cath2.jpg"
weight: "5"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/31402
imported:
- "2019"
_4images_image_id: "31402"
_4images_cat_id: "2246"
_4images_user_id: "162"
_4images_image_date: "2011-07-28T21:44:14"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31402 -->
Zahrader sind direct angetrieben damit die nicht uber eine Achse rutschen. Die Wahl des Zahrader ist nun so gemacht das es passt, nichts mehr.
Fur das Z20 Zahrad habe ich etwas gedreht damit es auf eine Achse verschraubt werden kann.