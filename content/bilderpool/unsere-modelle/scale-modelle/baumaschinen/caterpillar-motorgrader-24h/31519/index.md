---
layout: "image"
title: "Motorraum (2)"
date: "2011-08-03T07:52:54"
picture: "cath2_2.jpg"
weight: "3"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/31519
imported:
- "2019"
_4images_image_id: "31519"
_4images_cat_id: "2246"
_4images_user_id: "162"
_4images_image_date: "2011-08-03T07:52:54"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31519 -->
Detail Bild von Schalter zum abschalten Kompressor, gemacht aus en minitaster, Betätiger einfach und ein Gelenkstein mit Feder (Unten den Alustein).

Weiter zu sehen die 40 litze steuerkabel (20 funktionen) fur folgende funktionen:

- Fahren vorne/hinten
- Lenkung
- Magnetventil fur die Treppen hoch.
- Magnetventil fur die Treppen unten
- blinklicht links fahren
- blinklicht rechts fahren
- normales licht (2 Rot hinten, 2 Weiss hinten, 4 vorne und 4 an die Kabine)
- knicken (noch nicht eingebaut und weiss auch nicht ob es kommt/passt)
- Kompressor
- Hydraulik anlage Blatt Neigung
- Hydraulik anlage Reifen Neigung
- Motor fur Blatt hoch/unten links
- Motor fur Blatt hoch/unten rechts
- Blatt links/rechts
- Blatt drehen
- Blinklicht orange auf die Kabine