---
layout: "comment"
hidden: true
title: "17753"
date: "2013-01-27T17:19:11"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Hallo Anton,

Ziet er zoals gebruikelijk wederom professioneel uit ! ...Hut ab.

De kabeltrommel omvat waarschijnlijk een meer dan 10-aderigere kabel.

- Ga je hier ook een meervoudige sleepring aan monteren ?

Zum Thema Schleifring: 
http://www.b-command.com/produkte/schle ... -12mm.html 

Leider nicht billig: 

- Miniatur Schleifring Rotar X12/6 = RX-CA1206-001: 92,50€ / Stück ( 6 polig ) 

- Miniatur Schleifring Rotar X12/12 = RX-CA1212-001: 104,30€ / Stück (12 polig ) 

Alle Preise sind ohne MWSt, zzgl Fracht und Verpackung. 

Grüss, 

Peter 
Poederoyen NL