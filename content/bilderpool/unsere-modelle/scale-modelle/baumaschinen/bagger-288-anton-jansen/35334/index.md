---
layout: "image"
title: "bagger5"
date: "2012-08-15T17:21:05"
picture: "bagger-288-5.jpg"
weight: "24"
konstrukteure: 
- "AntonJansen"
fotografen:
- "AntonJansen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "anton"
license: "unknown"
legacy_id:
- details/35334
imported:
- "2019"
_4images_image_id: "35334"
_4images_cat_id: "2618"
_4images_user_id: "541"
_4images_image_date: "2012-08-15T17:21:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35334 -->
