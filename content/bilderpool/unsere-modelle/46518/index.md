---
layout: "image"
title: "Laufkatze"
date: "2017-09-30T11:52:18"
picture: "11_Verfahreinheit.jpg"
weight: "11"
konstrukteure: 
- "Fabian, Max, Christian & Stefan"
fotografen:
- "Christian und Stefan"
keywords: ["Laufkatze"]
uploadBy: "ftpi"
license: "unknown"
legacy_id:
- details/46518
imported:
- "2019"
_4images_image_id: "46518"
_4images_cat_id: "10"
_4images_user_id: "2611"
_4images_image_date: "2017-09-30T11:52:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46518 -->
Die Laufkatze zwischen den einzelnen Reihen des Trockenlagers läuft auf Laufschienen die oben und unten an dem Lager angebaut sind. Ein Mini-Motor mit U-Getriebe treibt die Laufkatze an einer Zahnstange an.