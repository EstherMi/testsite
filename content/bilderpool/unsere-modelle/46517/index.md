---
layout: "image"
title: "Trockenlager am Keksdrucker"
date: "2017-09-30T11:52:18"
picture: "10_Trockenlager.jpg"
weight: "10"
konstrukteure: 
- "Fabian, Max, Christian & Stefan"
fotografen:
- "Christian und Stefan"
keywords: ["Hochregal"]
uploadBy: "ftpi"
license: "unknown"
legacy_id:
- details/46517
imported:
- "2019"
_4images_image_id: "46517"
_4images_cat_id: "10"
_4images_user_id: "2611"
_4images_image_date: "2017-09-30T11:52:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46517 -->
Im Trockenlager sind die Kekse solange, bis der Zuckerguß hart geworden ist. Für die Steuerung ist die Belegung des Lagers egal. Vor dem Einlagern eines neuen Keks in eine Lagerposition wird zunächst die Postion geräumt - ob nun ein Keks in der Position ist oder nicht.