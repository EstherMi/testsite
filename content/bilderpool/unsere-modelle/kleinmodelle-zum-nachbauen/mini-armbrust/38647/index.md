---
layout: "image"
title: "Armbrust_013"
date: "2014-04-23T15:49:08"
picture: "013.jpg"
weight: "13"
konstrukteure: 
- "Lord of the 7"
fotografen:
- "Lord of the 7"
keywords: ["Armbrust", "Mini", "Kleinmodell", "Bogen", "Katapult", "Schießen"]
uploadBy: "Lord of the 7"
license: "unknown"
legacy_id:
- details/38647
imported:
- "2019"
_4images_image_id: "38647"
_4images_cat_id: "2885"
_4images_user_id: "2167"
_4images_image_date: "2014-04-23T15:49:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38647 -->
Hier die Armbrust nochmal im Fischertechnik Designer