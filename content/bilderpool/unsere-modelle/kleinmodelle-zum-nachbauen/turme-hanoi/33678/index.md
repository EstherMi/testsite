---
layout: "image"
title: "Hanoi 03"
date: "2011-12-15T10:39:28"
picture: "dietuermevonhanoi03.jpg"
weight: "3"
konstrukteure: 
- "Volker-James Münchhof"
fotografen:
- "Volker-James Münchhof"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- details/33678
imported:
- "2019"
_4images_image_id: "33678"
_4images_cat_id: "2491"
_4images_user_id: "895"
_4images_image_date: "2011-12-15T10:39:28"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33678 -->
Die Felder für die Ablage der Scheiben, ebenso die Scheiben, wurden für die Verfolgung des Lösungsablaufes numeriert. Der Mittelpunkt eines jeden Feldes wurde mit einer Markierung versehen. Hier wurden Abreibe-Symbole und Abreibe-Ziffern aus dem "edding transfer" Programm verwendet (jeder andere Filzschreiber erfüllt die gleiche Aufgabe) und mit einer Schicht Klarlack zum Schutz versehen. Im Bild ist eine 6. Scheibe (nicht aus dem "fischertechnik computing" ft# 30554 Baukasten) hinzugefügt. Das zeigt, daß "Die Türme von Hanoi" ganz leicht um weitere Scheiben erweitert werden können.

Die Federnocken, die der Verriegelung von Bausteinen dienen, sind, deutlich erkennbar, bis zur Auflage auf die Grundplatte eingedrückt worden.