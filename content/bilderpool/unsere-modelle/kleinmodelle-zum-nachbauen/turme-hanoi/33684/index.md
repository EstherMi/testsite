---
layout: "image"
title: "Hanoi 09"
date: "2011-12-15T10:39:28"
picture: "dietuermevonhanoi09.jpg"
weight: "9"
konstrukteure: 
- "Volker-James Münchhof"
fotografen:
- "Volker-James Münchhof"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- details/33684
imported:
- "2019"
_4images_image_id: "33684"
_4images_cat_id: "2491"
_4images_user_id: "895"
_4images_image_date: "2011-12-15T10:39:28"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33684 -->
In dieser Baustufe wird der Träger des Hubtisches auf dem Drehkranz fertig gestellt, der obere End-Taster I5 und die drei Zahnstangen, an denen sich er Hubtisch fortbewegt, 
werden angebracht. In die seitlichen Nuten der hohen und der mttelhohen Säulen werden Metallachsen eingeschoben. Diese dienen der Steifigkeit der Säulen und der Ausrichtung der Bausteine 30 der Säulen.

Bei der mittelhohen Säule helfen die beiden Metallachsen auch den "Gleitsteinen" des Hubtischen an der Säule entlang zu gleiten in dem sie die Oberfläche der Säule für die "Gleitsteine" glätten.

Die beiden Taster I4 und I5 werden an die schon verlegten Kabel angeschlossen und somit elektrisch mit der Verteilerplatte und der 28-poligen Buchsenplatte verbunden.