---
layout: "image"
title: "Hanoi 15"
date: "2011-12-15T10:39:28"
picture: "dietuermevonhanoi15.jpg"
weight: "15"
konstrukteure: 
- "Volker-James Münchhof"
fotografen:
- "Volker-James Münchhof"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- details/33690
imported:
- "2019"
_4images_image_id: "33690"
_4images_cat_id: "2491"
_4images_user_id: "895"
_4images_image_date: "2011-12-15T10:39:28"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33690 -->
Die Baustufe 14 zeigt den fertig montierten Hubtisch, mit auf- bzw. eingeschobenen Verbindern, Bauplatten (Abdeckplatten) und Kunststoffachsen.

Zusammengehörige elektrische Bauteile sind schon teilverkabelt.