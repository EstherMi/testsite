---
layout: "image"
title: "Das Vorbild"
date: "2015-12-28T19:08:43"
picture: "kleinerkran1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42619
imported:
- "2019"
_4images_image_id: "42619"
_4images_cat_id: "3168"
_4images_user_id: "104"
_4images_image_date: "2015-12-28T19:08:43"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42619 -->
In der Anleitung zum Ur-Baukasten "fischertechnik 200" gibt es diesen kleinen Kran. Er ist von Hand drehbar, kann das Seil per Kurbel aufwickeln und war gut für viele Stunden Spaß.