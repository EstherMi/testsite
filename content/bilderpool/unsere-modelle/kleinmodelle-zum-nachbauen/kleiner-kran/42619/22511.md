---
layout: "comment"
hidden: true
title: "22511"
date: "2016-09-14T17:01:38"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
... außerdem lernt man als Kind etwas wichtiges dabei, wenn man zu fest drauf drückt und die Beine gehen ab! Das vergisst man dann nie mehr.

Gruß,
Stefan