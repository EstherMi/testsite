---
layout: "image"
title: "Kranseil-Antrieb"
date: "2015-12-28T19:08:43"
picture: "kleinerkran4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42622
imported:
- "2019"
_4images_image_id: "42622"
_4images_cat_id: "3168"
_4images_user_id: "104"
_4images_image_date: "2015-12-28T19:08:43"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42622 -->
Das Kranseil wird über eine 2:1-Untersetzung angetrieben. Eine Sperrklinke könnte man noch ergänzen.