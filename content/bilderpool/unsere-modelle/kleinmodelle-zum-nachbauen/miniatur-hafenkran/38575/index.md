---
layout: "image"
title: "Kranaufbau 1"
date: "2014-04-21T17:02:59"
picture: "miniaturhafenkran6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/38575
imported:
- "2019"
_4images_image_id: "38575"
_4images_cat_id: "2882"
_4images_user_id: "104"
_4images_image_date: "2014-04-21T17:02:59"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38575 -->
Einige BS7,5, Rollenböcke, Federnocken. Die Drehung um die Senkrechte funktioniert über den unten sichtbaren Pneumatik-Adapter (der normalerweise auf P-Zylinder aufgesteckt wird).

Die Achse ganz rechts trägt das Kranhaken-Seil. Die Achse oben rechts führt das Seil zur Verstellung des Kranarms und wird von zwei weiteren Pneumatik-Adaptern gehalten, die mit einem Verbinder 15 am Rollenbock befestigt sind. Beide Seilachsen werden mangels winziger Kurbel über die Riegelscheiben betätigt.