---
layout: "image"
title: "Gesamtansicht 1"
date: "2014-04-21T17:02:59"
picture: "miniaturhafenkran1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/38570
imported:
- "2019"
_4images_image_id: "38570"
_4images_cat_id: "2882"
_4images_user_id: "104"
_4images_image_date: "2014-04-21T17:02:59"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38570 -->
Kennt jemand den ähnlich aufgebauten großen Kran aus der Anleitung des guten alten ft 400S Statikkastens? Mit diesem Modellchen wollte ich mal versuchen, wie klein ich es hinbekomme.