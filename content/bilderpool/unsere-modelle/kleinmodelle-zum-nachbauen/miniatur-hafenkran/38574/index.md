---
layout: "image"
title: "Fahrwerk"
date: "2014-04-21T17:02:59"
picture: "miniaturhafenkran5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/38574
imported:
- "2019"
_4images_image_id: "38574"
_4images_cat_id: "2882"
_4images_user_id: "104"
_4images_image_date: "2014-04-21T17:02:59"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38574 -->
Das Fahrwerk ist einfach aufgebaut. Alle verwendeten Riegelscheiben sind stramm sitzende (bei mir gibt es gut klemmende und locker drehbare im Bestand).