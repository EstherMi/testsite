---
layout: "image"
title: "Kranaufbau 2"
date: "2014-04-21T17:02:59"
picture: "miniaturhafenkran8.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/38577
imported:
- "2019"
_4images_image_id: "38577"
_4images_cat_id: "2882"
_4images_user_id: "104"
_4images_image_date: "2014-04-21T17:02:59"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38577 -->
Ein zweiter Blick auf die Seilantriebe.