---
layout: "image"
title: "Seilantrieb"
date: "2014-04-21T17:02:59"
picture: "miniaturhafenkran7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/38576
imported:
- "2019"
_4images_image_id: "38576"
_4images_cat_id: "2882"
_4images_user_id: "104"
_4images_image_date: "2014-04-21T17:02:59"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38576 -->
Beide Seile sind einfach mit je einer kleinen Klemmbuchse auf den Achsen fixiert. Das untere ist für das Hakenseil zuständig, das obere für die Armverstellung. Dessen Seil ist zwei Mal um seine Antriebs- und die Zugachse im Kranarm geführt und bildet so einen doppelten Flaschenzug. Sein zweites Ende ist zwischen dem hinteren Rollenbock und dem BS75 eingeklemmt. Damit der Kranarm nicht von alleine herunterfährt, gibt es rechts einen zweiten Klemmring, der mit dem des Seils eine Klemmung um den rechten Pneumatikadapter bewirkt.