---
layout: "image"
title: "FTMann - mit Sackrodel"
date: "2017-04-02T18:18:45"
picture: "rudel.jpg"
weight: "20"
konstrukteure: 
- "Primoz Cebulj"
fotografen:
- "Primoz Cebulj"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "primoz"
license: "unknown"
legacy_id:
- details/45713
imported:
- "2019"
_4images_image_id: "45713"
_4images_cat_id: "335"
_4images_user_id: "2667"
_4images_image_date: "2017-04-02T18:18:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45713 -->
