---
layout: "image"
title: "Fredy (1)"
date: "2007-02-12T06:12:49"
picture: "konstruktionswettbewerb1.jpg"
weight: "1"
konstrukteure: 
- "Fredy"
fotografen:
- "Fredy"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/8973
imported:
- "2019"
_4images_image_id: "8973"
_4images_cat_id: "817"
_4images_user_id: "104"
_4images_image_date: "2007-02-12T06:12:49"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8973 -->
6 Teile mit Seil (mit Fredy per Mail geeinigt; eingereicht waren 5)