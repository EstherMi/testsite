---
layout: "overview"
title: "Konstruktionswettbewerb Februar 2007"
date: 2019-12-17T19:40:59+01:00
legacy_id:
- categories/817
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=817 --> 
Die Aufgabe war, einen motorisierten Kran mit möglichst wenigen Teilen zu bauen. Ein Modell gilt als solcher Kran, wenn die Last ausschließlich mit einem Seil Kontakt hat, und wenn sie ausschließlich durch Motorkraft vollständig vom Boden gehoben werden kann.

Als Last sind 200g Schokolade vorgesehen. Das ist gleichzeitig der Preis, den jeder Teilnehmer am Ende behalten darf. Die Schokolade gilt es, so mit Seil zu umwickeln, dass sie angehoben werden kann. Die Packung darf nicht geöffnet werden, alle Sorten sind erlaubt.

Als Teil zählt alles, was in der Knobloch-Liste eine eigene Nummer hat (Teilebeutel nur vollständig). Kabel, Stecker, Schalter, Netzteile und Akkus zählen nicht als Teile, wenn sie so am Kran montiert werden, dass sie keine statische Funktion haben. Aluprofile, Platten 1000 und Platten 500 dürfen nicht verwendet werden. Beliebiges Seil darf sich jeder selber zuschneiden, zwei Enden zählen als ein Teil.