---
layout: "image"
title: "Fischertechnik Pneumatik Katapult"
date: "2008-04-30T14:55:56"
picture: "FT-Launching_Catapult-nov-2007_006.jpg"
weight: "3"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/14423
imported:
- "2019"
_4images_image_id: "14423"
_4images_cat_id: "1327"
_4images_user_id: "22"
_4images_image_date: "2008-04-30T14:55:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14423 -->
Fischertechnik Pneumatik Katapult