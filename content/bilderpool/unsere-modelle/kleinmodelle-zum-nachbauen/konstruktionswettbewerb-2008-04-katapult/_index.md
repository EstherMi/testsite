---
layout: "overview"
title: "Konstruktionswettbewerb 2008-04: Katapult"
date: 2019-12-17T19:41:21+01:00
legacy_id:
- categories/1327
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1327 --> 
Die Aufgabenstellung war folgende:[br][br]1. Es soll ein funktionstüchtiges Katapult gebaut werden, das einen Baustein 30 (z. B. die Art. Nr. 31003) mindestens einen Meter weit schleudert.[br][br]2. Mitmachen darf jeder, der Fischertechnikteile zur Verfügung hat. [br][br]3. Es darf aus maximal 250 original Fischertechnikteile bestehen (Motoren, Pneumatik, ... sind erlaubt). Ausnahme: Gummis sind nicht erlaubt. [br][br]4. Modellgröße: Es soll kompakt sein und möglichst auf z. B. eine Bauplatte 259 * 187 (32985) passen (Geringfügige Abweichungen sind kein K.O.-Kriterium, da das nicht gewinnentscheidend ist).[br][br]5. Bewertungskriterien sind die raffinierteste, eleganteste, pfiffigste Mechanik - einfach das genialste Modell.