---
layout: "image"
title: "Trebuchet 5"
date: "2008-04-21T23:33:43"
picture: "sm_side_2.jpg"
weight: "20"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["catapult", "trebuchet"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/14354
imported:
- "2019"
_4images_image_id: "14354"
_4images_cat_id: "1327"
_4images_user_id: "585"
_4images_image_date: "2008-04-21T23:33:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14354 -->
This is my late entry for the catapult contest. 

google translation: Dies ist meine Nachmeldung für die Katapult-Wettbewerb.