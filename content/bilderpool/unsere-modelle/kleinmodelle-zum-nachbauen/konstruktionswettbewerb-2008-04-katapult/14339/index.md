---
layout: "image"
title: "Speedy68 (4)"
date: "2008-04-21T16:01:00"
picture: "forumswettbewerb24.jpg"
weight: "44"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/14339
imported:
- "2019"
_4images_image_id: "14339"
_4images_cat_id: "1327"
_4images_user_id: "104"
_4images_image_date: "2008-04-21T16:01:00"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14339 -->
