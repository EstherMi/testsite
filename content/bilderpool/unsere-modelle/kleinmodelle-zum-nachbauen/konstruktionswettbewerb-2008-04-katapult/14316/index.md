---
layout: "image"
title: "Mirose Vorschlag A (1)"
date: "2008-04-21T16:00:59"
picture: "forumswettbewerb01.jpg"
weight: "21"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/14316
imported:
- "2019"
_4images_image_id: "14316"
_4images_cat_id: "1327"
_4images_user_id: "104"
_4images_image_date: "2008-04-21T16:00:59"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14316 -->
Miroses Beschreibung zu diesem Vorschlag:

Wurfweite eines Bausteins 30 mit 4 Schleifring-Federfüßen etwa 210 cm. (15 Bauteile)
Wurfweite eines Bausteins 30 mit 3 Schleifring-Federfüßen etwa 160 cm. (14 Bauteile)
Wurfweite eines Bausteins 30 mit 2 Schleifring-Federfüßen etwa 100 cm. (13 Bauteile)