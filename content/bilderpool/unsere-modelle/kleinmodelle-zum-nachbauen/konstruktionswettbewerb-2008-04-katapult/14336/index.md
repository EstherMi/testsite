---
layout: "image"
title: "Speedy68 (1)"
date: "2008-04-21T16:01:00"
picture: "forumswettbewerb21.jpg"
weight: "41"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/14336
imported:
- "2019"
_4images_image_id: "14336"
_4images_cat_id: "1327"
_4images_user_id: "104"
_4images_image_date: "2008-04-21T16:01:00"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14336 -->
Speedy68 hat keine Beschreibungen mitgeschickt. Evtl. kann er per Kommentarfunktion ja noch etwas erklären.