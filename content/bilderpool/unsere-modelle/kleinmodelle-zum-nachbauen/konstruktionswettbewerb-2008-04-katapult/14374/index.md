---
layout: "image"
title: "Rendered Trebuchet - Rear View"
date: "2008-04-24T23:06:05"
picture: "large_trebuchet.jpg"
weight: "14"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["trebuchet", "catapult"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/14374
imported:
- "2019"
_4images_image_id: "14374"
_4images_cat_id: "1327"
_4images_user_id: "585"
_4images_image_date: "2008-04-24T23:06:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14374 -->
This is the rendered version of my trebuchet entry. (Rear view).