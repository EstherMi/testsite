---
layout: "image"
title: "Speedy68 (10)"
date: "2008-04-21T16:01:00"
picture: "forumswettbewerb30.jpg"
weight: "50"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/14345
imported:
- "2019"
_4images_image_id: "14345"
_4images_cat_id: "1327"
_4images_user_id: "104"
_4images_image_date: "2008-04-21T16:01:00"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14345 -->
