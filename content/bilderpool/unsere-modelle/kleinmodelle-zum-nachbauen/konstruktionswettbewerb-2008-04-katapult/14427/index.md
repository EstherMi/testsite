---
layout: "image"
title: "Details Fischertechnik Katapult"
date: "2008-04-30T21:15:35"
picture: "Poederoyen-koninginnedag_017.jpg"
weight: "7"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/14427
imported:
- "2019"
_4images_image_id: "14427"
_4images_cat_id: "1327"
_4images_user_id: "22"
_4images_image_date: "2008-04-30T21:15:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14427 -->
Details Fischertechnik Katapult