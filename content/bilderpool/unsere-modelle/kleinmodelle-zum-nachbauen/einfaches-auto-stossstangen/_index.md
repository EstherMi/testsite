---
layout: "overview"
title: "Einfaches Auto mit Stoßstangen"
date: 2019-12-17T19:42:07+01:00
legacy_id:
- categories/3538
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3538 --> 
Kinder, die uns besuchten, legten fest: Es muss ein Auto mit Dach sein! Die Stoßstangen kamen dazu.