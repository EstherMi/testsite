---
layout: "image"
title: "Chassis"
date: "2018-10-14T22:59:17"
picture: "einfachesautomitstossstangen4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48215
imported:
- "2019"
_4images_image_id: "48215"
_4images_cat_id: "3538"
_4images_user_id: "104"
_4images_image_date: "2018-10-14T22:59:17"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48215 -->
Der Fahrzeugboden ist recht einfach aufgebaut. Der Sitz sitzt in einem BS 30x15x5 3N, der mit zwei Federnocken mit dem U-Träger verbunden ist.