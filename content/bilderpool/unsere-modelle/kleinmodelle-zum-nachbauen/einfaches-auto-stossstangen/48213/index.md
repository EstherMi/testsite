---
layout: "image"
title: "Rückseite"
date: "2018-10-14T22:59:17"
picture: "einfachesautomitstossstangen2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48213
imported:
- "2019"
_4images_image_id: "48213"
_4images_cat_id: "3538"
_4images_user_id: "104"
_4images_image_date: "2018-10-14T22:59:17"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48213 -->
Hinten gibt's dieselbe Stoßstangenkonstruktion, hier genügen aber normale Winkelsteine 60°.