---
layout: "image"
title: "Ansicht von unten"
date: "2006-11-26T12:47:56"
picture: "Walze05b.jpg"
weight: "5"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- details/7620
imported:
- "2019"
_4images_image_id: "7620"
_4images_cat_id: "713"
_4images_user_id: "488"
_4images_image_date: "2006-11-26T12:47:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7620 -->
Die Kette ist in den Winkelsteinen mit Schnurstückchen festgeklemmt, weil sie sonst zu leicht rausrutscht.