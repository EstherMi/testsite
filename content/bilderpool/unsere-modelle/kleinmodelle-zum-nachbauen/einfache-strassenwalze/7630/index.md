---
layout: "image"
title: "Walze mit Kotflügeln (alternativ) Heckansicht"
date: "2006-11-26T23:08:57"
picture: "Walze12b.jpg"
weight: "12"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- details/7630
imported:
- "2019"
_4images_image_id: "7630"
_4images_cat_id: "713"
_4images_user_id: "488"
_4images_image_date: "2006-11-26T23:08:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7630 -->
Und nochmal die Ansicht von hinten.

Bei der Farbwahl sind bei diesem Modell natürlich noch etliche Varianten möglich. Nur schwarze Bausteine, statt der roten Platten schwarze oder blaue oder gelbe oder.........
Statt der BS 15 mit Loch kann man Rollenlager oder einfache BS 15 nehmen, statt der Clipsachse (Drehachse vorne) tut´s auch eine Achse 50 mit Klemmbuchsen usw.
Auch die Form kann man je nach belieben noch ändern, mir ging es hier aber in erster Linie darum, das ganze möglichst einfach zu halten. Man könnte auch noch einfacher bauen, z.B. am Haubenrahmen.

Vielleicht hat ja mal jemand Lust, dieses Modell nachzubauen und stellt dann Fotos hier ein?