---
layout: "image"
title: "Frontansicht"
date: "2006-11-26T12:47:56"
picture: "Walze02b.jpg"
weight: "2"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- details/7617
imported:
- "2019"
_4images_image_id: "7617"
_4images_cat_id: "713"
_4images_user_id: "488"
_4images_image_date: "2006-11-26T12:47:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7617 -->
Anstatt der Z30 kann man natürlich auch Räder 45 mit Naben nehmen. Die Z30 sind mit drei 60er Achsen verbunden.