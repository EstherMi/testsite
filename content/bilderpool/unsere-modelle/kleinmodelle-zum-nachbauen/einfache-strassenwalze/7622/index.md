---
layout: "image"
title: "Frontansicht"
date: "2006-11-26T12:47:56"
picture: "Walze07b.jpg"
weight: "7"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- details/7622
imported:
- "2019"
_4images_image_id: "7622"
_4images_cat_id: "713"
_4images_user_id: "488"
_4images_image_date: "2006-11-26T12:47:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7622 -->
Das Ganze nochmal von vorne.