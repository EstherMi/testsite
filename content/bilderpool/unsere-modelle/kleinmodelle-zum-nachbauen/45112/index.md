---
layout: "image"
title: "FTMann - mit skeleton 2"
date: "2017-02-02T17:38:00"
picture: "skeleton1.jpg"
weight: "26"
konstrukteure: 
- "Primoz Cebulj"
fotografen:
- "Primoz Cebulj"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "primoz"
license: "unknown"
legacy_id:
- details/45112
imported:
- "2019"
_4images_image_id: "45112"
_4images_cat_id: "335"
_4images_user_id: "2667"
_4images_image_date: "2017-02-02T17:38:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45112 -->
