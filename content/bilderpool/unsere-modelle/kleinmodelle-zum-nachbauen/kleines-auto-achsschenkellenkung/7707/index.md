---
layout: "image"
title: "Die Unterseite"
date: "2006-12-06T23:23:53"
picture: "kleinesautomitlenkungundfederung2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/7707
imported:
- "2019"
_4images_image_id: "7707"
_4images_cat_id: "728"
_4images_user_id: "104"
_4images_image_date: "2006-12-06T23:23:53"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7707 -->
Die Achse, auf der das Lenk-Z10 befestigt ist, ist eine Achse 110, die bis zum zweiten BS30 mit Loch geht.