---
layout: "image"
title: "Der Fahrersitz"
date: "2006-12-06T23:23:54"
picture: "kleinesautomitlenkungundfederung5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/7710
imported:
- "2019"
_4images_image_id: "7710"
_4images_cat_id: "728"
_4images_user_id: "104"
_4images_image_date: "2006-12-06T23:23:54"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7710 -->
Nahezu ergonomisch geformt ;-) Man sieht aber noch mal gut die von der Rückenlehne bis vor zum Z10 laufende Achse 110.