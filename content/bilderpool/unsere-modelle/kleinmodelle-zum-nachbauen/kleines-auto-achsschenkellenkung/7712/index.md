---
layout: "image"
title: "Maximaler Lenkeinschlag links"
date: "2006-12-06T23:23:54"
picture: "kleinesautomitlenkungundfederung7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/7712
imported:
- "2019"
_4images_image_id: "7712"
_4images_cat_id: "728"
_4images_user_id: "104"
_4images_image_date: "2006-12-06T23:23:54"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7712 -->
... und genau so weit nach links.