---
layout: "image"
title: "Ventilator 2"
date: "2017-06-25T17:04:57"
picture: "zweiwegventilator2.jpg"
weight: "2"
konstrukteure: 
- "Wolfgang"
fotografen:
- "Wolfgang"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "webstar"
license: "unknown"
legacy_id:
- details/45996
imported:
- "2019"
_4images_image_id: "45996"
_4images_cat_id: "3419"
_4images_user_id: "1355"
_4images_image_date: "2017-06-25T17:04:57"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45996 -->
von hinten

Das blaue Teil ist ein E-Verteiler, den ich mit einem 3D-Drucker ausgedruckt habe (http://ftcommunity.de/categories.php?cat_id=3373).
Den Ventilator kann man auch ohne dieses Teil bauen.