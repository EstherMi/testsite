---
layout: "image"
title: "Die Turner"
date: "2015-10-12T07:24:21"
picture: "turner1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/42084
imported:
- "2019"
_4images_image_id: "42084"
_4images_cat_id: "3131"
_4images_user_id: "1359"
_4images_image_date: "2015-10-12T07:24:21"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42084 -->
die Turner könne 360 Grad rotieren, sind gut ausblanciert, Rotation läuft "wie geschmiert" ..