---
layout: "image"
title: "Hier wird schwer gearbeitet"
date: "2008-02-01T17:16:22"
picture: "DSCN2082.jpg"
weight: "1"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/13487
imported:
- "2019"
_4images_image_id: "13487"
_4images_cat_id: "1232"
_4images_user_id: "184"
_4images_image_date: "2008-02-01T17:16:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13487 -->
