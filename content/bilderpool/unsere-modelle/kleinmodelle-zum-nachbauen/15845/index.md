---
layout: "image"
title: "faberge-style egg"
date: "2008-10-08T19:20:06"
picture: "ft-fab_egg_s7.jpg"
weight: "53"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["instructables", "faberge-style", "egg", "PCS", "Edventures"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/15845
imported:
- "2019"
_4images_image_id: "15845"
_4images_cat_id: "335"
_4images_user_id: "585"
_4images_image_date: "2008-10-08T19:20:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15845 -->
Howdy all, 
Just entered the www.instructables.com faberge-style egg contest! 
http://www.instructables.com/id/ft_faberge_Style_Egg/
Thought to share. 
Richard

***google translation

Hallo alle,
Nur in den www.instructables.com Faberge-Stil Ei-Wettbewerb!
http://www.instructables.com/id/ft_faberge_Style_Egg/
Gedanken zu teilen.

Richard