---
layout: "image"
title: "Elektronische Schaltung"
date: "2015-07-04T16:11:48"
picture: "handventilatormitzweigeschwindigkeitsstufen4.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41378
imported:
- "2019"
_4images_image_id: "41378"
_4images_cat_id: "3093"
_4images_user_id: "104"
_4images_image_date: "2015-07-04T16:11:48"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41378 -->
Die Schaltung ist ein Graetz-Gleichrichter (bestehend aus vier Dioden) mit einem Widerstand (ein nicht sehr helle leuchtendes ft-Lämpchen) in einem Strang. Wegen des Gleichrichters dreht der Motor immer in die richtige Richtung, bläst die manchem so fatal fehlende frische Luft ums Hirn. Wenn jenes, wie zu hoffen steht, wieder im Normalbetrieb laufen sollte, sorgt der in der anderen Schalterstellung wirksame Vorwiderstand und dadurch langsamere Motorlauf für die fürs klare Denken notwendige Ruhe.