---
layout: "image"
title: "FTMann - am rollerbrett II"
date: "2017-10-26T17:01:50"
picture: "ze-rolke2.jpg"
weight: "3"
konstrukteure: 
- "Primoz Cebulj"
fotografen:
- "Primoz Cebulj"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "primoz"
license: "unknown"
legacy_id:
- details/46845
imported:
- "2019"
_4images_image_id: "46845"
_4images_cat_id: "335"
_4images_user_id: "2667"
_4images_image_date: "2017-10-26T17:01:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46845 -->
