---
layout: "image"
title: "FTMann - RaketeMann"
date: "2017-04-22T19:59:51"
picture: "raketa2.jpg"
weight: "17"
konstrukteure: 
- "Primoz Cebulj"
fotografen:
- "Primoz Cebulj"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "primoz"
license: "unknown"
legacy_id:
- details/45788
imported:
- "2019"
_4images_image_id: "45788"
_4images_cat_id: "335"
_4images_user_id: "2667"
_4images_image_date: "2017-04-22T19:59:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45788 -->
