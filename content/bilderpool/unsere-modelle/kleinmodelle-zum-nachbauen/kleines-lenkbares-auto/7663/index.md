---
layout: "image"
title: "Unterseite"
date: "2006-12-03T19:30:05"
picture: "kleineslenkbaresauto3.jpg"
weight: "3"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/7663
imported:
- "2019"
_4images_image_id: "7663"
_4images_cat_id: "723"
_4images_user_id: "104"
_4images_image_date: "2006-12-03T19:30:05"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7663 -->
Wichtig ist, dass die Räder der gelenkten Vorderachse locker auf der Achse sitzen. Deshalb sind auch die Klemmbuchsen drauf.