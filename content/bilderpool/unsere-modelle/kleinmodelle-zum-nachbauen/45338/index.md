---
layout: "image"
title: "FTMann - Speerwurf"
date: "2017-03-01T15:57:19"
picture: "kopje.jpg"
weight: "23"
konstrukteure: 
- "Primoz Cebulj"
fotografen:
- "Primoz Cebulj"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "primoz"
license: "unknown"
legacy_id:
- details/45338
imported:
- "2019"
_4images_image_id: "45338"
_4images_cat_id: "335"
_4images_user_id: "2667"
_4images_image_date: "2017-03-01T15:57:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45338 -->
Das kleinste Modell,  nur 3 Teile.