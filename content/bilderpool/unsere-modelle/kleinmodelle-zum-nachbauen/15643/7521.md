---
layout: "comment"
hidden: true
title: "7521"
date: "2008-10-08T21:49:47"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
@HLGR

Hallo Holger,
dachte ich mir doch gleich, daß du so antworten wirst. Also du machst an den Klemmstiften den fehlerhaften Grat zwischen Phase und Zylinder weg und suchst dir ein paar BS 7,5 mit größerer Nut. Ich sprach doch von einem spielfreien Lauf zwischen Klemmstift und BS 7,5, den ich vorher noch geprüft hatte. Da wirst du auf Anhieb dafür passende Teile nicht finden. Der BS 7,5 hat findbare Toleranzen an der Rundnut! Wenn dein Getriebeprinzip allerdings nur mit viel Spiel funktioniert, mußt du halt die Rastachsen 20 wieder einbauen.
Gruß, Ingo