---
layout: "image"
title: "ft-Mann und die schwebende Hantel"
date: "2017-03-18T13:33:46"
picture: "ft-Mann_und_die_schwebende_Hantel_2_kl.jpg"
weight: "1"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: ["ft-Mann", "Männchen"]
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- details/45555
imported:
- "2019"
_4images_image_id: "45555"
_4images_cat_id: "3385"
_4images_user_id: "2635"
_4images_image_date: "2017-03-18T13:33:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45555 -->
.. und zaubern kann er auch: Die schwebende Hantel