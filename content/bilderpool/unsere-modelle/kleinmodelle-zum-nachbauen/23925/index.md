---
layout: "image"
title: "auto von oben"
date: "2009-05-08T23:33:19"
picture: "minicar1.jpg"
weight: "40"
konstrukteure: 
- "manuMFfilms"
fotografen:
- "manuMFfilms"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- details/23925
imported:
- "2019"
_4images_image_id: "23925"
_4images_cat_id: "335"
_4images_user_id: "934"
_4images_image_date: "2009-05-08T23:33:19"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23925 -->
gesamtansicht vom auto