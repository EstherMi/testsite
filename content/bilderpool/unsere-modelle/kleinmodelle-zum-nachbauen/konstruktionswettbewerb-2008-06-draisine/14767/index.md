---
layout: "image"
title: "Gleisbefestigung"
date: "2008-06-23T10:56:26"
picture: "draisine08.jpg"
weight: "8"
konstrukteure: 
- "Johannes 2"
fotografen:
- "Johannes 2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/14767
imported:
- "2019"
_4images_image_id: "14767"
_4images_cat_id: "1351"
_4images_user_id: "104"
_4images_image_date: "2008-06-23T10:56:26"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14767 -->
Hier sieht man die doppelte Gleisbefestigung.