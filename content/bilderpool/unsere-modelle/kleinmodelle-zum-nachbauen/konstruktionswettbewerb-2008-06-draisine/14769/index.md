---
layout: "image"
title: "Masked-2"
date: "2008-06-23T10:56:26"
picture: "draisine10.jpg"
weight: "10"
konstrukteure: 
- "Masked"
fotografen:
- "Masked"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/14769
imported:
- "2019"
_4images_image_id: "14769"
_4images_cat_id: "1351"
_4images_user_id: "104"
_4images_image_date: "2008-06-23T10:56:26"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14769 -->
