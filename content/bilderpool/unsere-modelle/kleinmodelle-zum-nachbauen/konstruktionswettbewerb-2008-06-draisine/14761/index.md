---
layout: "image"
title: "Exzenter"
date: "2008-06-23T10:56:26"
picture: "draisine02.jpg"
weight: "2"
konstrukteure: 
- "Johannes 2"
fotografen:
- "Johannes 2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/14761
imported:
- "2019"
_4images_image_id: "14761"
_4images_cat_id: "1351"
_4images_user_id: "104"
_4images_image_date: "2008-06-23T10:56:26"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14761 -->
Auf diesem Bild sieht man den Exzenter.