---
layout: "image"
title: "Masked-3"
date: "2008-06-23T10:56:26"
picture: "draisine11.jpg"
weight: "11"
konstrukteure: 
- "Masked"
fotografen:
- "Masked"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/14770
imported:
- "2019"
_4images_image_id: "14770"
_4images_cat_id: "1351"
_4images_user_id: "104"
_4images_image_date: "2008-06-23T10:56:26"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14770 -->
