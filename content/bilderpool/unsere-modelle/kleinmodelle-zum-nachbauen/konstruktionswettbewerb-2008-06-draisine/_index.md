---
layout: "overview"
title: "Konstruktionswettbewerb 2008-06: Draisine"
date: 2019-12-17T19:41:28+01:00
legacy_id:
- categories/1351
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1351 --> 
Die Aufgabe war, eine Handhebeldraisine zu konstruieren: ein zweiachsiges Eisenbahnfahrzeug, das mit einem Hebel bewegt werden soll.[br][br]Siehe auch http://de.wikipedia.org/wiki/Eisenbahn-Draisine[br][br]Bedingungen:[br][br]1. Mitmachen darf jeder, der Fischertechnikteile zur Verfügung. hat.[br]2. Nur original ft-Teile dürfen verwendet werden[br]3. Spurweite (das ist der Innenabstand zwischen den Schienen) 60 bis 120 mm.[br]4. Möglichst wenig Teile, gut konstruierter Antrieb[br]5. Der Antrieb muss funktionstüchtig sein, beide Achsen sollen angetrieben werden.[br]6. Das Gleis / Gleisjoch zählt nicht zu den Bauteilen, muss aber gebaut werden. (Ein Eisenbahnfahrzeug braucht nun mal Schienen!)[br]7. Eine Kette gilt als ein Bauteil, egal wie lang.[br]8. Einsendeschluss: Sonntag, 22. Juni 2008 (oder ist dieser Termin jemandem zu knapp?)[br]9. Gewinner ist jener, der hier im Forum die meisten Stimmen erhält.[br]10. Gewinnpreis: Ein fischertechnik Baukasten Basic Tractors 96779, von Mirose (der den letzten Wettbewerb gewann und diesen deshalb ausschreiben durfte) gesponsert; OHNE Originalpackung, damit er ihn als Brief verschicken kann.[br][br]Mitgemacht haben: Andreas (Laserman), Johannes Deppert (Johannes 2), Martin Westphal (Masked), Michael Sengstschmid (Mirose, mit zwei Modellvorschlägen).