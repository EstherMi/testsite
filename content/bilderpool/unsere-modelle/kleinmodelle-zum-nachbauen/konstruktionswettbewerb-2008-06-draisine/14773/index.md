---
layout: "image"
title: "Draisine Mirose A3"
date: "2008-06-23T10:56:26"
picture: "draisine14.jpg"
weight: "14"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/14773
imported:
- "2019"
_4images_image_id: "14773"
_4images_cat_id: "1351"
_4images_user_id: "104"
_4images_image_date: "2008-06-23T10:56:26"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14773 -->
