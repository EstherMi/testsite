---
layout: "image"
title: "Draisine Mirose A5"
date: "2008-06-23T10:56:26"
picture: "draisine16.jpg"
weight: "16"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/14775
imported:
- "2019"
_4images_image_id: "14775"
_4images_cat_id: "1351"
_4images_user_id: "104"
_4images_image_date: "2008-06-23T10:56:26"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14775 -->
