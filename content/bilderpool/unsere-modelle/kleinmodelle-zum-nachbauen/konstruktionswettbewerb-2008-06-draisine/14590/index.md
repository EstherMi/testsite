---
layout: "image"
title: "Draisine vorne"
date: "2008-05-30T07:03:25"
picture: "Draisine_vorne.jpg"
weight: "26"
konstrukteure: 
- "Laserman"
fotografen:
- "Laserman"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/14590
imported:
- "2019"
_4images_image_id: "14590"
_4images_cat_id: "1351"
_4images_user_id: "724"
_4images_image_date: "2008-05-30T07:03:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14590 -->
