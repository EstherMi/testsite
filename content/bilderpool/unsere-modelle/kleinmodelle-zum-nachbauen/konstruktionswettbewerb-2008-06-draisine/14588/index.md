---
layout: "image"
title: "Draisine auf Schienen"
date: "2008-05-30T07:03:24"
picture: "Draisine_auf_Schienen.jpg"
weight: "24"
konstrukteure: 
- "Laserman"
fotografen:
- "Laserman"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/14588
imported:
- "2019"
_4images_image_id: "14588"
_4images_cat_id: "1351"
_4images_user_id: "724"
_4images_image_date: "2008-05-30T07:03:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14588 -->
Spurweite 60 mm