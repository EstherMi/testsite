---
layout: "image"
title: "Kompakter Bau"
date: "2015-07-05T21:33:11"
picture: "handventilatormitdreistufen5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41396
imported:
- "2019"
_4images_image_id: "41396"
_4images_cat_id: "3095"
_4images_user_id: "104"
_4images_image_date: "2015-07-05T21:33:11"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41396 -->
Durch Verzicht auf sinnloses Tam Tam kommt das Gerät mit wenigen Bauteilen aus, die alle eng demselben Ziel nachstreben: der Freude des Endbenutzers.

Ohne die ihr gesamtes Tun schließlich des Sinns beraubt wäre.