---
layout: "overview"
title: "Handventilator mit drei Stufen"
date: 2019-12-17T19:41:57+01:00
legacy_id:
- categories/3095
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3095 --> 
Dieser Ventilator benötigt neben dem 3 Minitaster, 2 Lampen, aber keine Elektronik und bietet drei Kühlstufen.