---
layout: "image"
title: "Elektrische Schaltung"
date: "2015-07-05T21:33:11"
picture: "handventilatormitdreistufen3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41394
imported:
- "2019"
_4images_image_id: "41394"
_4images_cat_id: "3095"
_4images_user_id: "104"
_4images_image_date: "2015-07-05T21:33:11"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41394 -->
Die oberen beiden Taster führen Strom über je eine der beiden Lämpchen zum Motor, der untere direkt. Die drei Geschwindigkeitsstufen funktionieren so:

1. Wenn man den oberen Taster drückt, bekommt der Motor Strom über ein Lämpchen als Vorwiderstand und läuft auf langsamster Stufe.

2. Drückt man den oberen Taster etwas tiefer ein, betätigt dieser auch den mittleren. Der führt ebenfalls von der Batterie über ein zweites Lämpchen zum Motor. So sind zwei Lämpchen parallel geschaltet, der Vorwiderstand halbiert sich, und der Motor läuft auf mittlerer Stufe.

3. Drückt man den oberen Taster ganz durch, wird auch der unterste durchgeschaltet, der den Motor direkt mit Strom verbindet. So läuft der Motor mit der vollen Batterieleistung.

Am obersten Taster ist auch ein Schiebeschalter befestigt, sodass man den Ventilator durch feines Verschieben des BS7,5 mit der Verkleidungsplatte 15x15 auf Dauerlauf in einer beliebigen Stufe einstellen kann.