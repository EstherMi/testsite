---
layout: "image"
title: "Taster mit Abstand"
date: "2015-07-05T21:33:11"
picture: "handventilatormitdreistufen4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41395
imported:
- "2019"
_4images_image_id: "41395"
_4images_cat_id: "3095"
_4images_user_id: "104"
_4images_image_date: "2015-07-05T21:33:11"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41395 -->
Die Taster als das Ganze steuernde Elemente kleben nicht aneinander, arbeiten aber durch den winzigen Abstand perfekt zusammen. So kann auch der letzte sehen, wie mit Teamwork einfach alles möglich ist, und ohne nur jämmerliche Hakelei und klägliches Scheitern bliebe. Wie man auf diesem Bild sieht, funktioniert das auch bei etwas kniffliger Justierung nur so lange nicht, bis es einfach jemand macht.