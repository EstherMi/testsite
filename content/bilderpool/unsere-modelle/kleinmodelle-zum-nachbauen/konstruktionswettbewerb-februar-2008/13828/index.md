---
layout: "image"
title: "speedy68 (4)"
date: "2008-03-03T12:39:06"
picture: "wettbewerbfebruar8.jpg"
weight: "8"
konstrukteure: 
- "speedy68"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/13828
imported:
- "2019"
_4images_image_id: "13828"
_4images_cat_id: "1268"
_4images_user_id: "104"
_4images_image_date: "2008-03-03T12:39:06"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13828 -->
