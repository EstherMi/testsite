---
layout: "image"
title: "Harald (1)"
date: "2008-03-03T12:39:06"
picture: "wettbewerbfebruar1.jpg"
weight: "1"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/13821
imported:
- "2019"
_4images_image_id: "13821"
_4images_cat_id: "1268"
_4images_user_id: "104"
_4images_image_date: "2008-03-03T12:39:06"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13821 -->
