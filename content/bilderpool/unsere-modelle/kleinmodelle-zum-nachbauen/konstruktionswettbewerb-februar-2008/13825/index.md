---
layout: "image"
title: "speedy68 (1)"
date: "2008-03-03T12:39:06"
picture: "wettbewerbfebruar5.jpg"
weight: "5"
konstrukteure: 
- "speedy68"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/13825
imported:
- "2019"
_4images_image_id: "13825"
_4images_cat_id: "1268"
_4images_user_id: "104"
_4images_image_date: "2008-03-03T12:39:06"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13825 -->
