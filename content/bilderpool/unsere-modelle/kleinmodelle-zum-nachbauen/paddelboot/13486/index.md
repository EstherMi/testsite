---
layout: "image"
title: "POWER"
date: "2008-01-31T14:31:37"
picture: "DSCN2073.jpg"
weight: "2"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/13486
imported:
- "2019"
_4images_image_id: "13486"
_4images_cat_id: "1231"
_4images_user_id: "184"
_4images_image_date: "2008-01-31T14:31:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13486 -->
