---
layout: "image"
title: "Kleiner Panzer"
date: "2010-11-25T17:33:13"
picture: "kleinerpanzer1.jpg"
weight: "1"
konstrukteure: 
- "Florian Geerken"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- details/29364
imported:
- "2019"
_4images_image_id: "29364"
_4images_cat_id: "2130"
_4images_user_id: "381"
_4images_image_date: "2010-11-25T17:33:13"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29364 -->
