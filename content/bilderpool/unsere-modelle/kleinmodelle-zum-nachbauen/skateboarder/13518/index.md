---
layout: "image"
title: "Skateboarder02"
date: "2008-02-04T13:59:55"
picture: "skateboarder2.jpg"
weight: "2"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/13518
imported:
- "2019"
_4images_image_id: "13518"
_4images_cat_id: "1239"
_4images_user_id: "729"
_4images_image_date: "2008-02-04T13:59:55"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13518 -->
von hinten