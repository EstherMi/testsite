---
layout: "image"
title: "Frontansicht"
date: "2015-07-04T11:50:08"
picture: "ventilatormitschwenkmechanik1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41371
imported:
- "2019"
_4images_image_id: "41371"
_4images_cat_id: "3092"
_4images_user_id: "104"
_4images_image_date: "2015-07-04T11:50:08"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41371 -->
Dieser Ventilator steht bei der aktuellen Hitze neben meinem Notebook. Er läuft mit nur 3 V Betriebsspannung, leise, ruhig, und gerade angenehm. Hinter Büroverglasung überhitzt man leicht und fängt dann an, Unsinn zu machen. Diese Zeit wäre natürlich sinnvoller in den Bau mehr solcher Modelle investiert, die einen akuten Bedarf decken.

Ein Video gibt's unter https://www.youtube.com/watch?v=4p4qco5mprE