---
layout: "image"
title: "FTMann - mit Schub­kar­re"
date: "2017-04-03T14:10:00"
picture: "samokolnica.jpg"
weight: "19"
konstrukteure: 
- "Primoz Cebulj"
fotografen:
- "Primoz Cebulj"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "primoz"
license: "unknown"
legacy_id:
- details/45714
imported:
- "2019"
_4images_image_id: "45714"
_4images_cat_id: "335"
_4images_user_id: "2667"
_4images_image_date: "2017-04-03T14:10:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45714 -->
