---
layout: "image"
title: "Go-Kart Lenkmotor"
date: "2007-10-25T16:46:26"
picture: "Immag119.jpg"
weight: "2"
konstrukteure: 
- "Norbert E. Wagner"
fotografen:
- "Norbert E. Wagner"
keywords: ["Go-Kart", "Auto", "Rennwagen"]
uploadBy: "zeuz"
license: "unknown"
legacy_id:
- details/12302
imported:
- "2019"
_4images_image_id: "12302"
_4images_cat_id: "1099"
_4images_user_id: "634"
_4images_image_date: "2007-10-25T16:46:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12302 -->
Der Motor ist nur vorne und von einer Kette links (nächstes Bild) gehalten.