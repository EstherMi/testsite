---
layout: "image"
title: "Go-Kart vorne rechts"
date: "2007-10-24T19:59:55"
picture: "Immag117.jpg"
weight: "4"
konstrukteure: 
- "Norbert E. Wagner"
fotografen:
- "Norbert E. Wagner"
keywords: ["Go-Kart", "Auto", "Rennwagen"]
uploadBy: "zeuz"
license: "unknown"
legacy_id:
- details/12300
imported:
- "2019"
_4images_image_id: "12300"
_4images_cat_id: "1099"
_4images_user_id: "634"
_4images_image_date: "2007-10-24T19:59:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12300 -->
Naja außer der Lenkung ist eigentlich nicht viel vom „Go Cart“ Bausatz geblieben.