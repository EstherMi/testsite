---
layout: "image"
title: "Propellerantrieb"
date: "2008-01-04T19:58:27"
picture: "IMG_0724.jpg"
weight: "63"
konstrukteure: 
- "Reus"
fotografen:
- "Reus"
keywords: ["Luftschraube", "Propeller"]
uploadBy: "Reus"
license: "unknown"
legacy_id:
- details/13270
imported:
- "2019"
_4images_image_id: "13270"
_4images_cat_id: "335"
_4images_user_id: "708"
_4images_image_date: "2008-01-04T19:58:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13270 -->
Trivial, aber unterhaltsam :-)
Laut Küchenwaage bringt der Propeller einen Schub von 0,128N (13Gramm) bei 9V. Das ganze Ding wiegt (ohne Batterie) 69Gramm und wird ganz schön flott. Fährt auf einer glatten Unterlage schon bei 3V an.