---
layout: "image"
title: "Give-away 10"
date: "2015-04-09T21:06:06"
picture: "20150407_190503.jpg"
weight: "12"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/40747
imported:
- "2019"
_4images_image_id: "40747"
_4images_cat_id: "3061"
_4images_user_id: "328"
_4images_image_date: "2015-04-09T21:06:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40747 -->
Feuerwehr mit klappbarer Leiter, 14 Teile