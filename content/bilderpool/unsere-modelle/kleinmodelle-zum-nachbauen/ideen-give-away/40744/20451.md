---
layout: "comment"
hidden: true
title: "20451"
date: "2015-04-11T14:39:26"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Ich kenne diese Walze aus dem Baukasten "King of the Road". Dort hat sie als "Dekoartikel" gedient und sollte eine Auspuff darstellen.
Siehe: http://ft-datenbank.de/details.php?ArticleVariantId=6bded85a-8916-4a65-a714-92bc16598658

Die Walze kann man dank einem runden Zapfen in einen Baustein 15 rot mit Loch stecken (wie oben im Bild). Zusammengesteckt lässt sich die Walze jedoch nur schlecht drehen, weshalb sie für ein Förderband meiner Meinung nach ungeignet ist. 

Hat jemand die Walze schon anderweitig eingesetzt?

Gruß, David