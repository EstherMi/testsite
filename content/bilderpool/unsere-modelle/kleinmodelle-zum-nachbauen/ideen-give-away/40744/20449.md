---
layout: "comment"
hidden: true
title: "20449"
date: "2015-04-11T12:03:21"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Es gibt doch immer wieder Teile, die ich nicht besitze, noch nie verbaut sah und deren Zweck ich auch nicht erahnte. Wofür war diese Schwarze Walze denn mal da? Und kann man die wirklich als Walze, z.B. für ein Förderband mit Gummiband, verwenden?
Gruß,
Stefan