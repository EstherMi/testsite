---
layout: "overview"
title: "Kleinmodelle zum Nachbauen"
date: 2019-12-17T19:40:43+01:00
legacy_id:
- categories/335
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=335 --> 
Die Kategorie für Modelle, die sich schnell nachbauen lassen und möglichst Material aus nur einem Baukasten benötigen.
Hier gilt: Je einfacher das Modell, desto besser!