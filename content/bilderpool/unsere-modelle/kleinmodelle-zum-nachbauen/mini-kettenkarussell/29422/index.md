---
layout: "image"
title: "Mini-Kettenkarussell"
date: "2010-12-05T16:35:22"
picture: "minikettenkarussell5.jpg"
weight: "5"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/29422
imported:
- "2019"
_4images_image_id: "29422"
_4images_cat_id: "2139"
_4images_user_id: "1162"
_4images_image_date: "2010-12-05T16:35:22"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29422 -->
Die Kreuzknotenplatte, auf die alle Streben zulaufen.