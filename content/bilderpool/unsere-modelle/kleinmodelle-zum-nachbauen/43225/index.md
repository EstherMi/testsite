---
layout: "image"
title: "Wagenheber - Mini 1"
date: "2016-04-02T17:06:56"
picture: "IMG_4016.jpg"
weight: "29"
konstrukteure: 
- "Techum"
fotografen:
- "Techum"
keywords: ["Kleinmodell", "Wagenheber", "Mini", "Weihnachtskalender"]
uploadBy: "techum"
license: "unknown"
legacy_id:
- details/43225
imported:
- "2019"
_4images_image_id: "43225"
_4images_cat_id: "335"
_4images_user_id: "1217"
_4images_image_date: "2016-04-02T17:06:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43225 -->
kleiner Wagenheber - voll funktionsfähig und kompatibel mit Playmobil - allerdings eher was für LKWs ;-)