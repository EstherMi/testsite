---
layout: "image"
title: "Männchen mit Boher"
date: "2007-03-01T16:56:01"
picture: "werkzeuge2.jpg"
weight: "2"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/9199
imported:
- "2019"
_4images_image_id: "9199"
_4images_cat_id: "851"
_4images_user_id: "445"
_4images_image_date: "2007-03-01T16:56:01"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9199 -->
So muss man den Bohrer an die Hand stecken.