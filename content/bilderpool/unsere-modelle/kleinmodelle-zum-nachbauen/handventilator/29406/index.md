---
layout: "image"
title: "Handventilator 1"
date: "2010-12-04T13:42:07"
picture: "handventilator1.jpg"
weight: "1"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/29406
imported:
- "2019"
_4images_image_id: "29406"
_4images_cat_id: "2137"
_4images_user_id: "1162"
_4images_image_date: "2010-12-04T13:42:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29406 -->
Hier sieht man den Ventilator von vorne.