---
layout: "image"
title: "Handventilator 3"
date: "2010-12-04T13:42:08"
picture: "handventilator3.jpg"
weight: "3"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/29408
imported:
- "2019"
_4images_image_id: "29408"
_4images_cat_id: "2137"
_4images_user_id: "1162"
_4images_image_date: "2010-12-04T13:42:08"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29408 -->
Ventilator von hinten.