---
layout: "image"
title: "Handventilator 7"
date: "2010-12-04T13:42:08"
picture: "handventilator7.jpg"
weight: "7"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/29412
imported:
- "2019"
_4images_image_id: "29412"
_4images_cat_id: "2137"
_4images_user_id: "1162"
_4images_image_date: "2010-12-04T13:42:08"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29412 -->
Hier sieht man, wie der Motor befestigt wurde.