---
layout: "overview"
title: "Propellerauto"
date: 2019-12-17T19:41:31+01:00
legacy_id:
- categories/1545
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1545 --> 
Das Auto wird von einem Propeller mit dem M-Motor angetrieben. Gesteuert wird es vom Control Set. Der Strom kommt von einem 9-Volt Block. Das Auto währt relativ schnell.