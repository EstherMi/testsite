---
layout: "image"
title: "FTMann - im boot"
date: "2017-05-08T18:14:36"
picture: "ze-coln.jpg"
weight: "15"
konstrukteure: 
- "Primoz Cebulj"
fotografen:
- "Primoz Cebulj"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "primoz"
license: "unknown"
legacy_id:
- details/45798
imported:
- "2019"
_4images_image_id: "45798"
_4images_cat_id: "335"
_4images_user_id: "2667"
_4images_image_date: "2017-05-08T18:14:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45798 -->
