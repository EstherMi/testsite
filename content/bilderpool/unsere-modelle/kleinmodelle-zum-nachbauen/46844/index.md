---
layout: "image"
title: "FTMann - am rollerbrett"
date: "2017-10-24T07:04:14"
picture: "ze-rolke1.jpg"
weight: "4"
konstrukteure: 
- "Primoz Cebulj"
fotografen:
- "Primoz Cebulj"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "primoz"
license: "unknown"
legacy_id:
- details/46844
imported:
- "2019"
_4images_image_id: "46844"
_4images_cat_id: "335"
_4images_user_id: "2667"
_4images_image_date: "2017-10-24T07:04:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46844 -->
