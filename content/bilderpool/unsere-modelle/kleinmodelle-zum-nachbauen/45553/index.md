---
layout: "image"
title: "Liegestütz"
date: "2017-03-17T14:52:34"
picture: "liegestuetz1.jpg"
weight: "21"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/45553
imported:
- "2019"
_4images_image_id: "45553"
_4images_cat_id: "335"
_4images_user_id: "1557"
_4images_image_date: "2017-03-17T14:52:34"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45553 -->
- "Wie viele Liegestütze schaffst Du?"
- "Alle! ... und noch Hundert dazu!"