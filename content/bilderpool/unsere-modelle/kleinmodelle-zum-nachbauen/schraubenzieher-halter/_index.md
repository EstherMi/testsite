---
layout: "overview"
title: "Schraubenzieher Halter"
date: 2019-12-17T19:41:40+01:00
legacy_id:
- categories/2770
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2770 --> 
Halter für die kleinen gelben Fischertechnik-Schraubenzieher.