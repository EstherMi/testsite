---
layout: "image"
title: "Belt Mule"
date: "2009-04-24T08:32:35"
picture: "powertrain_sm.jpg"
weight: "41"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["belt", "drive"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/23807
imported:
- "2019"
_4images_image_id: "23807"
_4images_cat_id: "335"
_4images_user_id: "585"
_4images_image_date: "2009-04-24T08:32:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23807 -->
This is a mechanism that demonstrates a belt changing plain of rotation.