---
layout: "image"
title: "Polwendeeinheit - Mechanik"
date: "2017-06-19T20:35:42"
picture: "einschienenmaus8.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/45992
imported:
- "2019"
_4images_image_id: "45992"
_4images_cat_id: "3418"
_4images_user_id: "104"
_4images_image_date: "2017-06-19T20:35:42"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45992 -->
Die Abdeckung drückt zwei Taster, die als Polwender geschaltet sind (siehe "Motorsteuerungen (1)" in ft:pedia 2011-1).