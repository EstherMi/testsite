---
layout: "comment"
hidden: true
title: "23474"
date: "2017-06-19T20:59:12"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Clever, wie du hier das Umpolen löst!

Aus den Elektronikkästen (ec3) kenne ich die Lösung, dass ein Seil durch das Loch im Polwendeschalter geführt wird. Ein Knoten verhindert das Durchrutschen und sorgt an der entsprechenden Stelle dafür, dass der Taster umgelegt wird. Dies hat jedoch einige Nachteile:
a) Der Seilzug muss über die gesamte Schienenstrecke geführt werden und gespannt sein
b) Es ist ganz schön knifflig, den Knoten an der richtigen Stelle zu setzen...
c) ... und später wieder zu lösen (Ich mag keine Knoten im Nylonseil)

Im Gegensatz dazu das hier: Viel besser!

Gruß
David