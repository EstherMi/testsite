---
layout: "image"
title: "Aufbau der Maus (2)"
date: "2017-06-19T20:35:42"
picture: "einschienenmaus5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/45989
imported:
- "2019"
_4images_image_id: "45989"
_4images_cat_id: "3418"
_4images_user_id: "104"
_4images_image_date: "2017-06-19T20:35:42"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45989 -->
Die Querstangen vorne und hinten verhindern ein Kippen der Maus. Sie tragen leichtgängige Hülsen 15, die sich ggf. auch einfach mit drehen können.