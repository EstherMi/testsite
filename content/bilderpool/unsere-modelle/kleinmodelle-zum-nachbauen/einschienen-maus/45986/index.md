---
layout: "image"
title: "Laufkatze"
date: "2017-06-19T20:35:42"
picture: "einschienenmaus2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/45986
imported:
- "2019"
_4images_image_id: "45986"
_4images_cat_id: "3418"
_4images_user_id: "104"
_4images_image_date: "2017-06-19T20:35:42"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45986 -->
Die Schrägansicht auf die "Maus" zeigt den Aufbau: Der Motor treibt letztlich eines der beiden Z30 unten an, was das zweite gegenläufig dreht.