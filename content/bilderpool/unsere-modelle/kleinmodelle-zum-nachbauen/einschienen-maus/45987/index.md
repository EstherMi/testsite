---
layout: "image"
title: "Räder"
date: "2017-06-19T20:35:42"
picture: "einschienenmaus3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/45987
imported:
- "2019"
_4images_image_id: "45987"
_4images_cat_id: "3418"
_4images_user_id: "104"
_4images_image_date: "2017-06-19T20:35:42"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45987 -->
Unter den Z30 befinden sich Flachnaben mit Statik-Spurkränzen (auch so ein Teil, was nicht mehr hergestellt wird, oder?) mit den originalen Gummis drauf. Das sitzt dann einfach auf der Schiene drauf - das reicht schon für die Bewegung.