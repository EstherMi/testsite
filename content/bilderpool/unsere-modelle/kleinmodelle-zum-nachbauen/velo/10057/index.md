---
layout: "image"
title: "Kette"
date: "2007-04-12T10:05:11"
picture: "velo02.jpg"
weight: "2"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/10057
imported:
- "2019"
_4images_image_id: "10057"
_4images_cat_id: "910"
_4images_user_id: "445"
_4images_image_date: "2007-04-12T10:05:11"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10057 -->
Die Kette ist in jeder Position einigermassen gespannt. Nur den Berg hinauf braucht es ziemlich viel Kraft.