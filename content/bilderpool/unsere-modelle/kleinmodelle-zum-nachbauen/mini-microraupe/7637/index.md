---
layout: "image"
title: "Miniraupe Variante 1"
date: "2006-11-27T19:12:54"
picture: "Miniraupe03b.jpg"
weight: "3"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- details/7637
imported:
- "2019"
_4images_image_id: "7637"
_4images_cat_id: "714"
_4images_user_id: "488"
_4images_image_date: "2006-11-27T19:12:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7637 -->
Wie schon erwähnt, kann man aus dem Grundmodell so einiges machen.
Z.B. eine Mini-Planierraupe.