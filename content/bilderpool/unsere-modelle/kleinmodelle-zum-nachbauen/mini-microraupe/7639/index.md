---
layout: "image"
title: "Miniraupe Variante 3"
date: "2006-11-27T19:12:54"
picture: "Miniraupe05b.jpg"
weight: "5"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- details/7639
imported:
- "2019"
_4images_image_id: "7639"
_4images_cat_id: "714"
_4images_user_id: "488"
_4images_image_date: "2006-11-27T19:12:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7639 -->
Oder einen Ackerschlepper, wobei hier ungünstigerweise das Kabel vor dem Aufbau (also in Fahrtrichtung vorne) nach oben führt.