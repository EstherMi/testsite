---
layout: "image"
title: "Mini-Motorrad vollgefedert 2"
date: "2005-10-01T16:14:48"
picture: "Mini-Motorrad_2.jpg"
weight: "2"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/5053
imported:
- "2019"
_4images_image_id: "5053"
_4images_cat_id: "715"
_4images_user_id: "328"
_4images_image_date: "2005-10-01T16:14:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5053 -->
