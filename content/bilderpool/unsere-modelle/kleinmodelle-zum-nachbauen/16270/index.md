---
layout: "image"
title: "Helicopter from Junior Starter Jumbo Pack"
date: "2008-11-14T00:31:46"
picture: "Amelia_helicopter_b.jpg"
weight: "50"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Helicopter"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/16270
imported:
- "2019"
_4images_image_id: "16270"
_4images_cat_id: "335"
_4images_user_id: "585"
_4images_image_date: "2008-11-14T00:31:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16270 -->
This is my daughter with the Junior Starter Jumbo Pack's helicopter.