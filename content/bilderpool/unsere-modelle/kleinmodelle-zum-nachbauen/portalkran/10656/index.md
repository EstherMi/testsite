---
layout: "image"
title: "Gesamtansicht Portalkran"
date: "2007-06-02T21:36:01"
picture: "portalkran1.jpg"
weight: "1"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/10656
imported:
- "2019"
_4images_image_id: "10656"
_4images_cat_id: "966"
_4images_user_id: "445"
_4images_image_date: "2007-06-02T21:36:01"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10656 -->
Das ist eine kleine Version eines grossen Portalkrans.