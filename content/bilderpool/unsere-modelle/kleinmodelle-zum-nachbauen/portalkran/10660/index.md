---
layout: "image"
title: "Gesamtansicht des Geländes des Portalkrans"
date: "2007-06-02T21:36:02"
picture: "portalkran5.jpg"
weight: "5"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/10660
imported:
- "2019"
_4images_image_id: "10660"
_4images_cat_id: "966"
_4images_user_id: "445"
_4images_image_date: "2007-06-02T21:36:02"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10660 -->
Es ist drei ft-Schienen lang. in der Mitte habe ich es noch mals zusammengemacht, zur besseren stabilität.