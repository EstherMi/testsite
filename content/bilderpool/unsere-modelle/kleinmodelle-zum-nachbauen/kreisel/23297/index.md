---
layout: "image"
title: "kreisel2.jpg"
date: "2009-03-01T20:05:03"
picture: "kreisel4.jpg"
weight: "2"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/23297
imported:
- "2019"
_4images_image_id: "23297"
_4images_cat_id: "1583"
_4images_user_id: "845"
_4images_image_date: "2009-03-01T20:05:03"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23297 -->
