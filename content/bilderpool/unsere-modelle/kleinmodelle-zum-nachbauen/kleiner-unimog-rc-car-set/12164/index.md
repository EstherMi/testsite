---
layout: "image"
title: "Abladerampe"
date: "2007-10-08T14:12:08"
picture: "kleinerunimogfuerrccarset2.jpg"
weight: "2"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/12164
imported:
- "2019"
_4images_image_id: "12164"
_4images_cat_id: "1088"
_4images_user_id: "445"
_4images_image_date: "2007-10-08T14:12:08"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12164 -->
Damit kein Teil unter den Unimog rutscht.