---
layout: "overview"
title: "Konstruktionswettbewerb März 2007"
date: 2019-12-17T19:41:01+01:00
legacy_id:
- categories/874
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=874 --> 
Es galt, mit einem motorbetriebenen Fahrzeug aus möglichst wenigen Bauteilen eine Grundplatte 90*45 rot (36576) zu überwinden, die auf ihre schmale lange Kante gestellt und seitlich abgestützt wird. Das Fahrzeug muss in einigem Abstand vor der Platte starten, sich im 90°-Winkel auf die Mitte der Platte zubewegen, die Platte überwinden und hinter der Platte landen. Von dort aus muss es noch ein wenig weiterfahren können! Das Fahrzeug muss nicht lenkbar sein.[br][br]Wer dies mit möglichst wenigen Bauteilen schafft, gewinnt.[br][br]Das Fahrzeug wird durch mindestens 1 Motor angetrieben, die Stromversorgung und die Steuerung (Schalter) können extern erfolgen, so dass im Idealfall nur ein Kabel zum Fahrzeugmotor geführt wird.[br][br]Die Abstützung der Platte, damit sie nicht umkippt, sollte seitlich erfolgen (über die beiden Löcher oder die Nut pro Seite). Die Elemente zur Abstützung dürfen nicht zum Überwinden des Hindernisses genutzt werden.[br][br]Bzgl. Bauteile gibt es keine Einschränkungen. Alles muss original FT sein. Seile zählen je als 1 Bauteil. Kabel, Schalter und Stromversorgung zählen nicht als Bauteile, solange sie nicht am Fahrzeug angebracht sind. Ketten zählen als 1 Bauteil je zusammenhängende Ketten, Kettenbeläge zählen einzeln als je 1 Bauteil.[br][br]Als Teil zählt ansonsten alles, was in der Knobloch-Liste eine eigene Nummer hat (Teilebeutel nur vollständig). Kabel, Stecker, Schalter, Netzteile und Akkus zählen nicht als Teile, wenn sie so am Kran montiert werden, dass sie keine statische Funktion haben. Aluprofile, Platten 1000 und Platten 500 dürfen nicht verwendet werden. Beliebiges Seil darf sich jeder selber zuschneiden, zwei Enden zählen als ein Teil.[br][br]Die Bilder sind in alphabetischer Reihenfolge der Nicknames der Teilnehmer aufgeführt.