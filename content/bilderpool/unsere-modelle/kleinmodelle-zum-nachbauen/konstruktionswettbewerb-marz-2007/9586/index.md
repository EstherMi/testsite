---
layout: "image"
title: "Joker 2"
date: "2007-03-19T15:08:22"
picture: "konstruktionswettbewerb16.jpg"
weight: "17"
konstrukteure: 
- "Joker"
fotografen:
- "Joker"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/9586
imported:
- "2019"
_4images_image_id: "9586"
_4images_cat_id: "874"
_4images_user_id: "104"
_4images_image_date: "2007-03-19T15:08:22"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9586 -->
