---
layout: "image"
title: "Harald 3"
date: "2007-03-19T15:08:21"
picture: "konstruktionswettbewerb03.jpg"
weight: "4"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/9573
imported:
- "2019"
_4images_image_id: "9573"
_4images_cat_id: "874"
_4images_user_id: "104"
_4images_image_date: "2007-03-19T15:08:21"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9573 -->
