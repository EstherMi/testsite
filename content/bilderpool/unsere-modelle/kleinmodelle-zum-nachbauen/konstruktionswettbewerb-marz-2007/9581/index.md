---
layout: "image"
title: "Jiriki 2"
date: "2007-03-19T15:08:22"
picture: "konstruktionswettbewerb11.jpg"
weight: "12"
konstrukteure: 
- "Jiriki"
fotografen:
- "Jiriki"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/9581
imported:
- "2019"
_4images_image_id: "9581"
_4images_cat_id: "874"
_4images_user_id: "104"
_4images_image_date: "2007-03-19T15:08:22"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9581 -->
