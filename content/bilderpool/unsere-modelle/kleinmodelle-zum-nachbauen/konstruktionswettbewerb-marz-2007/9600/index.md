---
layout: "image"
title: "kehrblech"
date: "2007-03-19T15:52:34"
picture: "kehrblech1.jpg"
weight: "2"
konstrukteure: 
- "kehrblech"
fotografen:
- "Käberich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- details/9600
imported:
- "2019"
_4images_image_id: "9600"
_4images_cat_id: "874"
_4images_user_id: "521"
_4images_image_date: "2007-03-19T15:52:34"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9600 -->
Hier jetzt mein Modell, es hat 9 Teile.
1xU-Träger,
1xM-Motor,
1xGetriebe M-Motor,
2xSeiltrommel,
2xGrauer Baustein 30 mit Bohrung,
und 2xdiese roten Schaufeln.