---
layout: "image"
title: "Ma-gi-er 3"
date: "2007-03-19T15:08:22"
picture: "konstruktionswettbewerb22.jpg"
weight: "23"
konstrukteure: 
- "Ma-gi-er"
fotografen:
- "Ma-gi-er"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/9592
imported:
- "2019"
_4images_image_id: "9592"
_4images_cat_id: "874"
_4images_user_id: "104"
_4images_image_date: "2007-03-19T15:08:22"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9592 -->
