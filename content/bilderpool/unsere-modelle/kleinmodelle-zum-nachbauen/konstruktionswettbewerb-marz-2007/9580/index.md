---
layout: "image"
title: "Jiriki 1"
date: "2007-03-19T15:08:22"
picture: "konstruktionswettbewerb10.jpg"
weight: "11"
konstrukteure: 
- "Jiriki"
fotografen:
- "Jiriki"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/9580
imported:
- "2019"
_4images_image_id: "9580"
_4images_cat_id: "874"
_4images_user_id: "104"
_4images_image_date: "2007-03-19T15:08:22"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9580 -->
Originalbeschreibung:

Das Hindernis habe ich mit der geforderten Grundplatte 36576, einer Erweiterung in Form Grundplatte 36539, Der Grundplatte 31001 für die notwendige Stabilität und zwei Winkelträger 30, 36301 zusammen gebaut. Die Teile werden mit 6 x Metallachse 30 zusammengehalten. Tatsächlich ist das Hindernis durch die Konstruktion etwas höher als gefordert.

Das Gefährt krault über den Boden und überwindet durch die besondere Konstruktion auch höhere Hindernisse. Die beiden Zahnräder sind nur dazu da, die Kraft auf die Schaufeln zu übertragen. Hier liegt mein größtes Problem:
Die Naben rutschen bei Überlast durch, dann laufen die Schaufeln nicht mehr synchron. Daher die breite Heckflosse, die das Gefährt wieder stabilisiert.
Tatsächlich könnte ich noch auf drei bis vier Teile verzichten. Gerade die Heckkonstruktion bietet da noch Möglichkeiten. Rein aus ästhetischen Gründen habe ich zwei Winkelträger eingebaut.

Mein "Teppichkrauler" ist nicht besonders hübsch geworden, aber er meistert das Hindernis mit Leichtigkeit.