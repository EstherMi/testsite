---
layout: "image"
title: "Joker 5"
date: "2007-03-19T15:08:22"
picture: "konstruktionswettbewerb19.jpg"
weight: "20"
konstrukteure: 
- "Joker"
fotografen:
- "Joker"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/9589
imported:
- "2019"
_4images_image_id: "9589"
_4images_cat_id: "874"
_4images_user_id: "104"
_4images_image_date: "2007-03-19T15:08:22"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9589 -->
