---
layout: "image"
title: "thomas004 1"
date: "2007-03-19T15:08:22"
picture: "konstruktionswettbewerb28.jpg"
weight: "29"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/9598
imported:
- "2019"
_4images_image_id: "9598"
_4images_cat_id: "874"
_4images_user_id: "104"
_4images_image_date: "2007-03-19T15:08:22"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9598 -->
Originalbeschreibung:

Ich habe folgende Teile verwendet:

2x Mini-Motor
2x U-Getriebe
2x U-Achse
4x Nabe
4x Reifen
2x Gelenkteile
1x Winkelträger
1x Baustein 15 mit Bohrung
1x Bauplatte 15x45 mit Zapfen

= 19 Teile (ohne Kabel)

Beide Achsen werden jeweils von 1 Motor angetrieben. Die Achse 1 berührt die Mauer und wird von Achse 2 drüber geschoben. Nachdem Achse 1 über das Hindernis gefahren ist, hakt der Baustein 15 mit Bohrung über der Mauer ein, der Aufbau klappt zusammen und Achse 1 zieht den Rest des Fahrzeugs über das Hindernis. Danach fährt das Fahrzeug aufgeklappt weiter.

Originalbeschreibung zum Video:

Es ist etwas finster, da ich das Hindernis auf die große Grundplatte gebaut habe und sich das Fahrzeug so etwas schlecht abhebt. Diese Platte hat aber keinerlei Auswirkungen auf die Bewältigung des Hindernisses.

Ich habe ein weißes Blatt Papier auf die große Grundplatte gelegt und das Hindernis darauf aufgebaut. Somit erkennt man bei meiner schlechten Beleuchtung besser die Details des Überwindens.

Der Motor an Achse 2 dreht aufgrund von Toleranzen geringfügig schneller als der Motor an Achse 1. Somit bleibt der Aufbau beim Fahren im Normalzustand immer zusammengeklappt!