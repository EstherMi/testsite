---
layout: "image"
title: "Joker 1"
date: "2007-03-19T15:08:22"
picture: "konstruktionswettbewerb15.jpg"
weight: "16"
konstrukteure: 
- "Joker"
fotografen:
- "Joker"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/9585
imported:
- "2019"
_4images_image_id: "9585"
_4images_cat_id: "874"
_4images_user_id: "104"
_4images_image_date: "2007-03-19T15:08:22"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9585 -->
Originalbeschreibung:

Fürs Fahrzeug benötigte ich 24 Bauteile + 2 Doppellitzen inkl. Stecker.
* Als Energieversorgung diente ein Energy Set.
* Die grauen Getriebe kommen zum Einsatz da die schwarzen Getriebe die Kräfte nicht aushalten.

Für die Bauplatte benötigte ich 3 weitere Teile (siehe Film), der Halter stützt sich dann an der Wand ab.
Um die Kraft aufzunehmen.

An sich ist dieses ja der Gewaltansatz aber es läuft trotzdem.