---
layout: "image"
title: "Harald 2"
date: "2007-03-19T15:08:21"
picture: "konstruktionswettbewerb02.jpg"
weight: "3"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/9572
imported:
- "2019"
_4images_image_id: "9572"
_4images_cat_id: "874"
_4images_user_id: "104"
_4images_image_date: "2007-03-19T15:08:21"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9572 -->
