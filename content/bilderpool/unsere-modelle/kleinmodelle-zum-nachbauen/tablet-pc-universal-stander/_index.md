---
layout: "overview"
title: "Tablet-PC Universal-Ständer"
date: 2019-12-17T19:41:51+01:00
legacy_id:
- categories/3243
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3243 --> 
Diese zusammenklappbare Vorrichtung dient dazu, dem geplagten Tablet-PC oder Groß-handy-Nutzer die Hände frei zu machen, um z.B. als digtaler Bilderrahmen, (e)Kochbuch oder Bauanleitungsanzeigegerät zu fungieren ;-)