---
layout: "image"
title: "Arktisraupe mit absetzbarem Wohnmodul 3"
date: "2005-02-25T22:51:26"
picture: "Heck.jpg"
weight: "3"
konstrukteure: 
- "Chevyfahrer"
fotografen:
- "Chevyfahrer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chevyfahrer"
license: "unknown"
legacy_id:
- details/3652
imported:
- "2019"
_4images_image_id: "3652"
_4images_cat_id: "577"
_4images_user_id: "103"
_4images_image_date: "2005-02-25T22:51:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3652 -->
die seitlichen Platten 15x90 dienen als Schienen beim Bergen des Moduls