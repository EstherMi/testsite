---
layout: "image"
title: "Arktisraupe mit absetzbarem Wohnmodul 10"
date: "2005-02-25T23:17:36"
picture: "Wohnmodul_beim_Bergen.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chevyfahrer"
license: "unknown"
legacy_id:
- details/3659
imported:
- "2019"
_4images_image_id: "3659"
_4images_cat_id: "577"
_4images_user_id: "103"
_4images_image_date: "2005-02-25T23:17:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3659 -->
Das Modul wird wieder geborgen um damit zum nächsten Camp aufzubrechen