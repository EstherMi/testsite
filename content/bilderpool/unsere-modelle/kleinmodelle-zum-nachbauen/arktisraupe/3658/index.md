---
layout: "image"
title: "Arktisraupe mit absetzbarem Wohnmodul 9"
date: "2005-02-25T23:17:36"
picture: "Wohnmodul_abgesetzt.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chevyfahrer"
license: "unknown"
legacy_id:
- details/3658
imported:
- "2019"
_4images_image_id: "3658"
_4images_cat_id: "577"
_4images_user_id: "103"
_4images_image_date: "2005-02-25T23:17:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3658 -->
Das Modul abgesetzt.Die Raupe kann dann Erkundungsfahrten durchführen