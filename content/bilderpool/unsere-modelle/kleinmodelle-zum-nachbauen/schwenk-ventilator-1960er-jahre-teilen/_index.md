---
layout: "overview"
title: "Schwenk-Ventilator aus 1960er-Jahre-Teilen"
date: 2019-12-17T19:41:58+01:00
legacy_id:
- categories/3139
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3139 --> 
Ein Ventilator mit Schwenk-Mechanik nur aus Teilen, die es bereits 1972 (Luftschraube), 1969 (Mini-Mot) bzw. gleich zu Anfang (1966 oder so) gab.