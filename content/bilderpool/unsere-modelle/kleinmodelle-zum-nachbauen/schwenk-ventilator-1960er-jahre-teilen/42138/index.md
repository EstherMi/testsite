---
layout: "image"
title: "Schraubenantrieb"
date: "2015-10-25T17:52:24"
picture: "schwenkventilatorauserjahreteilen3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42138
imported:
- "2019"
_4images_image_id: "42138"
_4images_cat_id: "3139"
_4images_user_id: "104"
_4images_image_date: "2015-10-25T17:52:24"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42138 -->
Im Bild ist zwar der O-Ring eines jüngeren ft-Selbstbaukompressors verwendet, aber jedes geeignet lange Haushaltsgummi tut's auch.