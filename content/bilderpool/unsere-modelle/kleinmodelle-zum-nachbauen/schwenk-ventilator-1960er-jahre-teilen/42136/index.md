---
layout: "image"
title: "Gesamtansicht"
date: "2015-10-25T17:52:24"
picture: "schwenkventilatorauserjahreteilen1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42136
imported:
- "2019"
_4images_image_id: "42136"
_4images_cat_id: "3139"
_4images_user_id: "104"
_4images_image_date: "2015-10-25T17:52:24"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42136 -->
Um mal zu probieren, wie man wohl einen kleinen automatisch schwenkenden Ventilator ausschließlich aus Ur-ft-Teilen bauen kann, wurde dieses Modell ersonnen. Die Mechanik funktioniert geradeso wie bei den anderen Schwenk-Ventilatoren. Das jüngste Teil ist die Luftschraube von 1972, der Ur-Mini-Motor in Bausteingröße stammt bereits von 1969. Bis auf das Gummi sind da nur wenige verschiedene unveränderte Originalteile verbaut.