---
layout: "image"
title: "Stativ"
date: "2017-10-22T12:50:33"
picture: "handykamerastativ1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46828
imported:
- "2019"
_4images_image_id: "46828"
_4images_cat_id: "3467"
_4images_user_id: "104"
_4images_image_date: "2017-10-22T12:50:33"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46828 -->
In so ein flugs aufbaubares Stativ kann man ein Smartphone aufrecht oder verschieden schräg einsetzen und z.B. Videos von Modellen aufnehmen.