---
layout: "comment"
hidden: true
title: "3461"
date: "2007-06-12T20:21:18"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Tipp: Die Zapfen des BS5 und vor allem des gelben Statikträgers werden ja ganz schön verdreht. Das tut denen auf Dauer nicht gut. Evtl. solltest Du die Verbindung stabiler bauen (nicht nur auf einer einzigen Verbindung basierend), damit die Teile keinen dauerhaften Schaden nehmen.

Gruß,
Stefan