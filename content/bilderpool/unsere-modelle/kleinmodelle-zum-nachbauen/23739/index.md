---
layout: "image"
title: "Worm Gear Build"
date: "2009-04-18T07:37:20"
picture: "ft_wormgear.jpg"
weight: "42"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Worm", "gear", "model"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/23739
imported:
- "2019"
_4images_image_id: "23739"
_4images_cat_id: "335"
_4images_user_id: "585"
_4images_image_date: "2009-04-18T07:37:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23739 -->
Thought to share.