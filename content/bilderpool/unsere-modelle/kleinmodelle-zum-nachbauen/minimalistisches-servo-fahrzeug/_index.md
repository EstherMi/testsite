---
layout: "overview"
title: "Minimalistisches Servo-Fahrzeug"
date: 2019-12-17T19:41:42+01:00
legacy_id:
- categories/2475
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2475 --> 
Zum Ausprobieren der neuen IR-Servo-Fernsteuerung hatte ich ein kleines voll gefedertes Fahrwerk mit minimal wenigen Teilen gebaut.