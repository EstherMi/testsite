---
layout: "image"
title: "Gesamtansicht"
date: "2011-11-05T17:50:29"
picture: "minimalistischesservofahrzeug1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/33415
imported:
- "2019"
_4images_image_id: "33415"
_4images_cat_id: "2475"
_4images_user_id: "104"
_4images_image_date: "2011-11-05T17:50:29"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33415 -->
Akku und IR-Empfänger sind tragende Teile. Links ist vorne und wird gelenkt, hinten wird angetrieben.