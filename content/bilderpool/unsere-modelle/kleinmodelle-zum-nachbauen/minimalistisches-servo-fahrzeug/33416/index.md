---
layout: "image"
title: "Frontansicht"
date: "2011-11-05T17:50:29"
picture: "minimalistischesservofahrzeug2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/33416
imported:
- "2019"
_4images_image_id: "33416"
_4images_cat_id: "2475"
_4images_user_id: "104"
_4images_image_date: "2011-11-05T17:50:29"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33416 -->
Der Servo schwingt mit der gesamten federnden Vorderachse. Das federnde Element sind die Bauplatten 15 * 60.