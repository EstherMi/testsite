---
layout: "comment"
hidden: true
title: "15636"
date: "2011-11-06T18:51:51"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ich hab hier mal nach "kardan" gesucht, unerwarteterweise aber nur ein Bild mit einer ähnlichen Anordnung gefunden: http://www.ftcommunity.de/details.php?image_id=23866 Die Ideen hierzu stammt aber, wenn ich mich recht erinnere, nicht von mir, sondern wurde schon vor Jahren schon mal irgendwo beschrieben. Vielleicht auch im alten Forum...

Gruß,
Stefan