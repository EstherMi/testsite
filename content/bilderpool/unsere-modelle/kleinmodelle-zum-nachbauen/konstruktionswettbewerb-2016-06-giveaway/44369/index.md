---
layout: "image"
title: "Modell H - Torsten Stuehn: Radlader mit Ladefunktion"
date: "2016-09-12T10:44:58"
picture: "giveawayfuermakerfaire12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/44369
imported:
- "2019"
_4images_image_id: "44369"
_4images_cat_id: "3276"
_4images_user_id: "104"
_4images_image_date: "2016-09-12T10:44:58"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44369 -->
Torsten erdachte sich diesen Radlader mt einer Lademechanik.