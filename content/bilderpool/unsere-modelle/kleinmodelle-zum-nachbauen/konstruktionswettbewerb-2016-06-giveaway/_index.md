---
layout: "overview"
title: "Konstruktionswettbewerb 2016-06: Giveaway für Maker Faire"
date: 2019-12-17T19:42:01+01:00
legacy_id:
- categories/3276
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3276 --> 
Hier seht ihr die Einreichtungen für Vorschläge zu einem kleinen Modell, dass auf MakerFaires abgegeben werden könnte, in alphabetischer Reihenfolge der Vornamen der Einreicher. Siehe dazu diesen Forumsthread: https://forum.ftcommunity.de/viewtopic.php?f=4&t=3603