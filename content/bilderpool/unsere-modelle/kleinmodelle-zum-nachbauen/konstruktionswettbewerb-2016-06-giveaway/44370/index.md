---
layout: "image"
title: "Modell H - Torsten Stuehn: Radlader mit Ladefunktion"
date: "2016-09-12T10:44:58"
picture: "giveawayfuermakerfaire13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/44370
imported:
- "2019"
_4images_image_id: "44370"
_4images_cat_id: "3276"
_4images_user_id: "104"
_4images_image_date: "2016-09-12T10:44:58"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44370 -->
Torsten berechnet die Kosten für das Modell mit ffm-Preisen auf10,17 ?. Hier die Stückliste:

4 x V-Rad silber 
4 x V-Radachse 
1 x Schaufelhalter 30 
1 x Baustein 30 
2 x Baustein 15 
1 x Baustein 15 mit Bohrung 
5 x Baustein 5 
1 x Winkelstein 60 
2 x Baustein 5, 15x30, 3 Nuten 
5 x Federnocken 
4 x Statikstrebe 75 silber mit Loch 
2 x Winkelträger 15, 1 Zapfen schwarz 
5 x Bauplatte 5 15x30 mit 1 Zapfen
4 x Statikadapter 
2 x Riegelstein 15