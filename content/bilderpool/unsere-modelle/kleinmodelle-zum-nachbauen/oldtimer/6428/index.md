---
layout: "image"
title: "Lenkung (2)"
date: "2006-06-13T22:47:53"
picture: "oldtimer6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/6428
imported:
- "2019"
_4images_image_id: "6428"
_4images_cat_id: "564"
_4images_user_id: "104"
_4images_image_date: "2006-06-13T22:47:53"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6428 -->
Hier ein Blick von der Seite auf die Lenkung.