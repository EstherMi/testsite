---
layout: "image"
title: "Lenkeinschlag nach rechts"
date: "2006-06-13T22:47:53"
picture: "oldtimer8.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/6430
imported:
- "2019"
_4images_image_id: "6430"
_4images_cat_id: "564"
_4images_user_id: "104"
_4images_image_date: "2006-06-13T22:47:53"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6430 -->
Man sieht, wie der Platz für die Räder tatsächlich gebraucht wird. Man sieht auch, wie das kurveninnere Rad korrekterweise etwas stärker eingeschlagen wird als das kurvenäußere.