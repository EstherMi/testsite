---
layout: "image"
title: "Flugzeug01"
date: "2008-01-27T20:10:16"
picture: "flugzeuge1.jpg"
weight: "4"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/13453
imported:
- "2019"
_4images_image_id: "13453"
_4images_cat_id: "1226"
_4images_user_id: "729"
_4images_image_date: "2008-01-27T20:10:16"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13453 -->
Gebaut von mir :-)