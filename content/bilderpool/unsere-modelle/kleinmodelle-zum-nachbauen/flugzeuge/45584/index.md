---
layout: "image"
title: "Raumschiff Spiral auf seinem Grünflug"
date: "2017-03-21T17:35:36"
picture: "P1080196a_kl.jpg"
weight: "3"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- details/45584
imported:
- "2019"
_4images_image_id: "45584"
_4images_cat_id: "1226"
_4images_user_id: "2635"
_4images_image_date: "2017-03-21T17:35:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45584 -->
Meine ersten grünen ft-Teile, aus "Gliders".