---
layout: "image"
title: "Gesamtansicht"
date: "2006-12-04T00:02:54"
picture: "homokinetischesgelenk1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/7695
imported:
- "2019"
_4images_image_id: "7695"
_4images_cat_id: "726"
_4images_user_id: "104"
_4images_image_date: "2006-12-04T00:02:54"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7695 -->
Das Gelenk benötigt 4 Winkelachsen, die leider nicht mehr in neueren Kästen enthalten sind. In den hobby-Begleitbüchern findet man diese Idee schon lange, ich habe auch das nur mal nachgebaut.