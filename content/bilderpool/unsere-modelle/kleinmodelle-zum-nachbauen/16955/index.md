---
layout: "image"
title: "August and the Heliocopter"
date: "2009-01-09T11:29:04"
picture: "ft-Aug_helio_sm.jpg"
weight: "48"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["heliocopter"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/16955
imported:
- "2019"
_4images_image_id: "16955"
_4images_cat_id: "335"
_4images_user_id: "585"
_4images_image_date: "2009-01-09T11:29:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16955 -->
My son playing with a ft heliocopter.