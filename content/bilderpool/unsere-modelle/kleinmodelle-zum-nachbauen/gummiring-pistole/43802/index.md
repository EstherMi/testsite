---
layout: "image"
title: "Gummi-Ring-Pistole"
date: "2016-06-27T13:52:54"
picture: "gummiringpistole09.jpg"
weight: "9"
konstrukteure: 
- "Lemkajen"
fotografen:
- "Lemkajen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/43802
imported:
- "2019"
_4images_image_id: "43802"
_4images_cat_id: "3244"
_4images_user_id: "1359"
_4images_image_date: "2016-06-27T13:52:54"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43802 -->
seitlich