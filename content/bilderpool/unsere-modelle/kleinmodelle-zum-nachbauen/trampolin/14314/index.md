---
layout: "image"
title: "Trampolin"
date: "2008-04-20T18:41:21"
picture: "trampolin1.jpg"
weight: "1"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/14314
imported:
- "2019"
_4images_image_id: "14314"
_4images_cat_id: "1326"
_4images_user_id: "747"
_4images_image_date: "2008-04-20T18:41:21"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14314 -->
Das ist das Trampolin