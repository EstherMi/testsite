---
layout: "image"
title: "Unmöglicher Würfel"
date: "2008-10-02T16:37:23"
picture: "Wuerfel.jpg"
weight: "54"
konstrukteure: 
- "HLGR"
fotografen:
- "HLGR"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "HLGR"
license: "unknown"
legacy_id:
- details/15772
imported:
- "2019"
_4images_image_id: "15772"
_4images_cat_id: "335"
_4images_user_id: "832"
_4images_image_date: "2008-10-02T16:37:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15772 -->
Wer kann den Würfel nachbauen? :-)