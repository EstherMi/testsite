---
layout: "image"
title: "Metallachse durchgehend (2)"
date: "2006-12-10T13:25:19"
picture: "magi12.jpg"
weight: "12"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/7817
imported:
- "2019"
_4images_image_id: "7817"
_4images_cat_id: "736"
_4images_user_id: "445"
_4images_image_date: "2006-12-10T13:25:19"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7817 -->
