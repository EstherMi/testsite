---
layout: "image"
title: "Stabilisierung in der mitte"
date: "2006-12-10T13:25:19"
picture: "magi13.jpg"
weight: "13"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/7818
imported:
- "2019"
_4images_image_id: "7818"
_4images_cat_id: "736"
_4images_user_id: "445"
_4images_image_date: "2006-12-10T13:25:19"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7818 -->
