---
layout: "image"
title: "Z30"
date: "2011-06-26T19:48:17"
picture: "komoedienvogel6.jpg"
weight: "6"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- details/30977
imported:
- "2019"
_4images_image_id: "30977"
_4images_cat_id: "2314"
_4images_user_id: "1322"
_4images_image_date: "2011-06-26T19:48:17"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30977 -->
Hier sieht man das Z30 und die Strebe.