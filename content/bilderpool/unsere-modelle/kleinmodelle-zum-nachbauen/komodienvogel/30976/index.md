---
layout: "image"
title: "Flügel"
date: "2011-06-26T19:48:17"
picture: "komoedienvogel5.jpg"
weight: "5"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- details/30976
imported:
- "2019"
_4images_image_id: "30976"
_4images_cat_id: "2314"
_4images_user_id: "1322"
_4images_image_date: "2011-06-26T19:48:17"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30976 -->
Hier sieht man die befestigung der Strebe am Gelenkstein.