---
layout: "overview"
title: "'Komödienvogel'"
date: 2019-12-17T19:41:41+01:00
legacy_id:
- categories/2314
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2314 --> 
Da Vinci baute den orginalen "Komödienvogel". Dieser war für das Theater gedacht und konnte über ein Seil gezogen werden, wobei er den Flügelschlagt des Vogels nachahmte. Mein Nachbau wird allerdings über den Boden geschoben. Er ist sehr einfach gehalten und die Klassikteile lassen sich leicht ersetzen. Dann wünsche ich noch viel Spaß beim Nachbauen!
PS: Ich würde mich über Bilder von motorisierten oder gar mit Solar betriebenen "Komödienvögeln" freuen.