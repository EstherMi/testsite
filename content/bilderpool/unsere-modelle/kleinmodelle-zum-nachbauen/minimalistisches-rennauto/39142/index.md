---
layout: "image"
title: "Rennauto Perspektive 2"
date: "2014-08-06T19:28:35"
picture: "minimalistischesrennauto2.jpg"
weight: "2"
konstrukteure: 
- "Kryn1998"
fotografen:
- "Kryn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kryn1998"
license: "unknown"
legacy_id:
- details/39142
imported:
- "2019"
_4images_image_id: "39142"
_4images_cat_id: "2926"
_4images_user_id: "2221"
_4images_image_date: "2014-08-06T19:28:35"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39142 -->
Die Hinterachse und der Spoiler wurden auf die Höhe des Winkels des Auspuffs Zurückgeschoben.