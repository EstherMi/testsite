---
layout: "image"
title: "Kühler Zerlegt"
date: "2014-08-06T19:28:35"
picture: "minimalistischesrennauto6.jpg"
weight: "6"
konstrukteure: 
- "Kryn1998"
fotografen:
- "Kryn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kryn1998"
license: "unknown"
legacy_id:
- details/39146
imported:
- "2019"
_4images_image_id: "39146"
_4images_cat_id: "2926"
_4images_user_id: "2221"
_4images_image_date: "2014-08-06T19:28:35"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39146 -->
Die 2x1 Platte wird an die Unterseite der 7,5er Bausteine Geschoben.Die 45er Strebe wird vorne in der mitte aufgeklippst