---
layout: "image"
title: "Der Mittelteil Zerlegt"
date: "2014-08-06T19:28:35"
picture: "minimalistischesrennauto7.jpg"
weight: "7"
konstrukteure: 
- "Kryn1998"
fotografen:
- "Kryn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kryn1998"
license: "unknown"
legacy_id:
- details/39147
imported:
- "2019"
_4images_image_id: "39147"
_4images_cat_id: "2926"
_4images_user_id: "2221"
_4images_image_date: "2014-08-06T19:28:35"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39147 -->
Hier sind alle Winkel 15 Grad Groß. Die beiden schwarzen Steine in der Mitte werden zusammengeschoben.
Die 15 Grad Winkel mit den 15er Streben werden auf die schwarzen Steine geschoben. Der Spoiler wird am anderen Ende ebenfalls mit zwei 15 Grad Winkel fixiert. Die 45er Strebe wird vorne angeklipst.