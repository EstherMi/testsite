---
layout: "image"
title: "Rennauto Perspektive 1"
date: "2014-08-06T19:28:35"
picture: "minimalistischesrennauto1.jpg"
weight: "1"
konstrukteure: 
- "Kryn1998"
fotografen:
- "Kryn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kryn1998"
license: "unknown"
legacy_id:
- details/39141
imported:
- "2019"
_4images_image_id: "39141"
_4images_cat_id: "2926"
_4images_user_id: "2221"
_4images_image_date: "2014-08-06T19:28:35"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39141 -->
Oben sieht man den Motor und den Spoier. Vorne die Kühlergrillstreben und den Frontspoiler. Zuletzt sieht man hinten den Doppelauspuff (Im nächsten Bild sieht man diesen besser).