---
layout: "overview"
title: "Minimalistisches Rennauto"
date: 2019-12-17T19:41:50+01:00
legacy_id:
- categories/2926
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2926 --> 
Ein kleines Rennauto, welches man schnell nachbauen kann und das wenig Ressourcen (Steine) verbraucht.