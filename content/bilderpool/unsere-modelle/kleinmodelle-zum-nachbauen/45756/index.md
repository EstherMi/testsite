---
layout: "image"
title: "FTMann - am Segway"
date: "2017-04-14T22:49:00"
picture: "segway.jpg"
weight: "18"
konstrukteure: 
- "Primoz Cebulj"
fotografen:
- "Primoz Cebulj"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "primoz"
license: "unknown"
legacy_id:
- details/45756
imported:
- "2019"
_4images_image_id: "45756"
_4images_cat_id: "335"
_4images_user_id: "2667"
_4images_image_date: "2017-04-14T22:49:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45756 -->
