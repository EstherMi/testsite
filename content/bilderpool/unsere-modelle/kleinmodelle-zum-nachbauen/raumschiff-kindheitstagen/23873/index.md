---
layout: "image"
title: "Spaceship"
date: "2009-05-04T21:14:32"
picture: "spaceship2_sm.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Richard Mussler-Wright"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/23873
imported:
- "2019"
_4images_image_id: "23873"
_4images_cat_id: "1621"
_4images_user_id: "585"
_4images_image_date: "2009-05-04T21:14:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23873 -->
I loved the Spaceship model so much I had to render it!