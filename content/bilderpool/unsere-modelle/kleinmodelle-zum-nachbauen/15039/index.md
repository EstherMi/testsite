---
layout: "image"
title: "Happy Spur Gears"
date: "2008-08-09T08:06:34"
picture: "ft_Happy_Gear_Wall_vb.jpg"
weight: "59"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Happy", "spur", "gear"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/15039
imported:
- "2019"
_4images_image_id: "15039"
_4images_cat_id: "335"
_4images_user_id: "585"
_4images_image_date: "2008-08-09T08:06:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15039 -->
A simple gear pair model demonstrating the Cog Wheel T10 meshing with other spur gears.