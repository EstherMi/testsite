---
layout: "image"
title: "Propeller Test"
date: "2009-04-18T07:37:20"
picture: "sm_prop_b.jpg"
weight: "44"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["propeller"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/23741
imported:
- "2019"
_4images_image_id: "23741"
_4images_cat_id: "335"
_4images_user_id: "585"
_4images_image_date: "2009-04-18T07:37:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23741 -->
This is a test of the PCS BRAIN with a propeller.