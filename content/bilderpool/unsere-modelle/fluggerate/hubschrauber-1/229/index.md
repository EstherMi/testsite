---
layout: "image"
title: "hubi09"
date: "2003-04-21T19:56:05"
picture: "hubi09.jpg"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/229
imported:
- "2019"
_4images_image_id: "229"
_4images_cat_id: "27"
_4images_user_id: "1"
_4images_image_date: "2003-04-21T19:56:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=229 -->
