---
layout: "image"
title: "Cesspipsault-20.JPG"
date: "2005-06-09T23:01:52"
picture: "Cesspipsault-20.jpg"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4394
imported:
- "2019"
_4images_image_id: "4394"
_4images_cat_id: "361"
_4images_user_id: "4"
_4images_image_date: "2005-06-09T23:01:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4394 -->
Der Anfang jedes Flugzeugs ist der Rumpfquerschnitt. Wenn der festgelegt ist, gibt es für viele andere Sachen nicht mehr viel zu entscheiden. Für die Innereien der Cessna-Piper-Dassault war mir der breitere Querschnitt links doch lieber; außerdem sollen ja unterhalb des "Fußbodens" noch Sachen eingebaut werden können.