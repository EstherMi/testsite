---
layout: "image"
title: "Cesspipsault-69.JPG"
date: "2005-06-09T23:01:53"
picture: "Cesspipsault-69.jpg"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Flugzeug"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4397
imported:
- "2019"
_4images_image_id: "4397"
_4images_cat_id: "361"
_4images_user_id: "4"
_4images_image_date: "2005-06-09T23:01:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4397 -->
Da kommt sie angerauscht. Allmählich wird es Zeit, das Fahrwerk auszufahren.

Das ft-Nylonseil wird nur am Boden gebraucht. Es verhindert, dass sich die Fahrwerksbeine beim Rangieren nach außen wegbiegen.