---
layout: "image"
title: "Cesspipsault-57.JPG"
date: "2005-06-09T23:02:18"
picture: "Cesspipsault-57.jpg"
weight: "21"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4409
imported:
- "2019"
_4images_image_id: "4409"
_4images_cat_id: "361"
_4images_user_id: "4"
_4images_image_date: "2005-06-09T23:02:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4409 -->
