---
layout: "image"
title: "Cesspipsault-04.JPG"
date: "2005-06-07T22:38:30"
picture: "Cesspipsault-04.jpg"
weight: "23"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4351
imported:
- "2019"
_4images_image_id: "4351"
_4images_cat_id: "361"
_4images_user_id: "4"
_4images_image_date: "2005-06-07T22:38:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4351 -->
Casablanca 1942. Humphrey B. verabschiedet sich noch, während auf dem Rollfeld die Motoren warmlaufen...


Die Entwicklung wurde von Cessna, Piper und Dassault inspiriert, weshalb sie sich alle im Namen wiederfinden.