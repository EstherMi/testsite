---
layout: "image"
title: "Rotorkopf-Spinne.jpg"
date: "2008-03-16T00:29:36"
picture: "Dt-Museum73_Spinne.JPG"
weight: "8"
konstrukteure: 
- "NN"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/13927
imported:
- "2019"
_4images_image_id: "13927"
_4images_cat_id: "359"
_4images_user_id: "4"
_4images_image_date: "2008-03-16T00:29:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13927 -->
Eine Alternative zur Verwendung einer Taumelscheibe im Rotorkopf eines Hubschraubers ist die "Spinne".

Taumelscheibe und Spinne ermöglichen die zyklische und die kollektive Blattverstellung am Rotorkopf. 

Die kollektive Blattverstellung wirkt auf alle Blätter gleichartig und gleichzeitig. Damit wird der Gesamtauftrieb des Hubschraubers eingestellt, also die Wahl zwischen Steig-, Sink- und Schwebeflug getroffen.

Die zyklische Blattverstellung sorgt dafür, dass die Rotorblätter auf ihrem Umlauf dort steiler angestellt werden, wo sie mehr Auftrieb erzeugen sollen (und anderswo entsprechend flacher). Erhöhter Auftrieb z.B. "hinten links" bewirkt, dass der ganze Hubschrauber nach vorne rechts bewegt wird. Die zyklische Blattverstellung steuert also die Flugrichtung.

Zur Spinne: die Rotor-Achse, auf der auch das große Antriebszahnrad sitzt, ist hohl. Am oberen Ende sitzt auf einem Kugelkopf-Lager die "Spinne", hier mit drei Beinen. Von unten, durch ein Gestänge innerhalb der Rotorachse, kann die Spinne in x/y-Richtung gekippt werden (zyklische Verstellung). Außerdem kann die Spinne gehoben und gesenkt werden (kollektive Verstellung). Die Gestänge übertragen diese Einstellung auf die Rotorblätter.

(Ausstellungsstück im Deutschen Museum, München, 2007)