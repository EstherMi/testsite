---
layout: "image"
title: "Düsenjet6"
date: "2011-05-29T12:09:22"
picture: "duesenjet06.jpg"
weight: "6"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- details/30649
imported:
- "2019"
_4images_image_id: "30649"
_4images_cat_id: "2286"
_4images_user_id: "1122"
_4images_image_date: "2011-05-29T12:09:22"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30649 -->
Das Hinterteil des Jets.