---
layout: "image"
title: "Düsenjet9"
date: "2011-05-29T12:09:22"
picture: "duesenjet09.jpg"
weight: "9"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- details/30652
imported:
- "2019"
_4images_image_id: "30652"
_4images_cat_id: "2286"
_4images_user_id: "1122"
_4images_image_date: "2011-05-29T12:09:22"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30652 -->
Vorderteil mit eingeklapptem Fahrwerk.