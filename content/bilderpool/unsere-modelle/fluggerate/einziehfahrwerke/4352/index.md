---
layout: "image"
title: "FWL01_01.JPG"
date: "2005-06-07T22:38:31"
picture: "FWL01_01.jpg"
weight: "47"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4352
imported:
- "2019"
_4images_image_id: "4352"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2005-06-07T22:38:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4352 -->
Es werden so wenig Fluggerätschaften mit ft gebaut, da dachte ich so bei mir, dass man dieses Neuland doch mal beackern sollte.

Den Anfang machen ein paar Ergebnisse von der Suche nach Konstruktionen für Einziehfahrwerke.