---
layout: "image"
title: "FWL03_01.JPG"
date: "2005-06-07T22:38:37"
picture: "FWL03_01.jpg"
weight: "56"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4361
imported:
- "2019"
_4images_image_id: "4361"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2005-06-07T22:38:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4361 -->
Fahrwerk (leicht) Nummer 3 besticht durch seine verwinkelte Mechanik. Es wird durch Ziehen an der Kette ausgefahren. Zum Einfahren muss allerdings per Hand nachgeholfen werden.