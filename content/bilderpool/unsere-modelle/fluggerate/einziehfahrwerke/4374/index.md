---
layout: "image"
title: "FWL05D_04.JPG"
date: "2005-06-08T22:01:54"
picture: "FWL05D_04.jpg"
weight: "31"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4374
imported:
- "2019"
_4images_image_id: "4374"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2005-06-08T22:01:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4374 -->
Der Mitnehmer für das Z15 ist der Baustein 5x15x30, der quer auf der anderen Seite des BS15 steckt.