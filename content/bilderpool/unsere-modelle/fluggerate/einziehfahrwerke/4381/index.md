---
layout: "image"
title: "FWL06_07.JPG"
date: "2005-06-08T22:01:54"
picture: "FWL06_07.jpg"
weight: "38"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Fahrwerk"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4381
imported:
- "2019"
_4images_image_id: "4381"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2005-06-08T22:01:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4381 -->
... und so kompakt verschwindet es, wenn es eingezogen ist.