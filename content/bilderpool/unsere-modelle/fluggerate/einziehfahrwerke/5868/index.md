---
layout: "image"
title: "FWH13_36.JPG"
date: "2006-03-12T13:27:03"
picture: "FWH13_36.JPG"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5868
imported:
- "2019"
_4images_image_id: "5868"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2006-03-12T13:27:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5868 -->
