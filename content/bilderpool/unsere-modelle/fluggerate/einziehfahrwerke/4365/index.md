---
layout: "image"
title: "FWL03_05.JPG"
date: "2005-06-08T11:23:03"
picture: "FWL03_05.jpg"
weight: "22"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4365
imported:
- "2019"
_4images_image_id: "4365"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2005-06-08T11:23:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4365 -->
Nummer 3 von der Rückseite.