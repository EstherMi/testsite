---
layout: "image"
title: "FWL05S_01.JPG"
date: "2005-06-08T22:01:54"
picture: "FWL05S_01.jpg"
weight: "32"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Fahrwerk"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4375
imported:
- "2019"
_4images_image_id: "4375"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2005-06-08T22:01:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4375 -->
Fahrwerk (leicht) Nummer 5 in Single-Version (mit 1 Rad pro Arm).