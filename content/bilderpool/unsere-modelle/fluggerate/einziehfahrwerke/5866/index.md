---
layout: "image"
title: "FWH13_34.JPG"
date: "2006-03-12T13:26:12"
picture: "FWH13_34.JPG"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["modding"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5866
imported:
- "2019"
_4images_image_id: "5866"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2006-03-12T13:26:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5866 -->
Diese Hebel-Mechanik lässt sich nur mit Lenkwürfeln aufbauen, die so bearbeitet wurden, dass sie einen größeren Knickwinkel erreichen.