---
layout: "image"
title: "FWL01_05.JPG"
date: "2005-06-07T22:38:31"
picture: "FWL01_05.jpg"
weight: "51"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4356
imported:
- "2019"
_4images_image_id: "4356"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2005-06-07T22:38:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4356 -->
Der Viergelenk-Mechanismus wird in einem der Hobby-Bände beschrieben, allerdings weiß ich nicht mehr, welcher das gewesen ist.