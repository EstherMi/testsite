---
layout: "image"
title: "FWL02_01.JPG"
date: "2005-06-07T22:38:31"
picture: "FWL02_01.jpg"
weight: "52"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4357
imported:
- "2019"
_4images_image_id: "4357"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2005-06-07T22:38:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4357 -->
Fahrwerk (leicht) Nr. 2: die S-Riegel dienen als Mitnehmer zwischen Zahnrad und Gestänge.