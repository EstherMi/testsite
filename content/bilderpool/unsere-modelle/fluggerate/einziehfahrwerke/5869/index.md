---
layout: "image"
title: "FWH13_37.JPG"
date: "2006-03-12T13:27:32"
picture: "FWH13_37.JPG"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5869
imported:
- "2019"
_4images_image_id: "5869"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2006-03-12T13:27:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5869 -->
