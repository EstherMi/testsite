---
layout: "image"
title: "FWH12_02.JPG"
date: "2005-06-08T22:02:08"
picture: "FWH12_02.jpg"
weight: "40"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: ["Fahrwerk"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4383
imported:
- "2019"
_4images_image_id: "4383"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2005-06-08T22:02:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4383 -->
Nr. 12 halb ausgefahren.