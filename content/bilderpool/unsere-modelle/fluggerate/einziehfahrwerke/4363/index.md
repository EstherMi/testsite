---
layout: "image"
title: "FWL03_03.JPG"
date: "2005-06-08T11:23:03"
picture: "FWL03_03.jpg"
weight: "20"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Fahrwerk", "Kniegelenk"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4363
imported:
- "2019"
_4images_image_id: "4363"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2005-06-08T11:23:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4363 -->
Nummer 3 ist zu 3/4 ausgefahren.



Bitte an die Moderatoren: ich hab aus Versehen bei einigen Fahrwerken die falsche Kategorie erwischt. Bitte alle FW*.* aus 'Cesspipsault' hierher verschieben!