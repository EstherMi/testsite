---
layout: "image"
title: "FWL01_02.JPG"
date: "2005-06-07T22:38:31"
picture: "FWL01_02.jpg"
weight: "48"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4353
imported:
- "2019"
_4images_image_id: "4353"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2005-06-07T22:38:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4353 -->
Die ft-Kurbeln ergeben einen sehr kompakten Viergelenk-Mechanismus. Man muss das Teil gesehen haben, um die Schwenkbewegung zu verstehen.