---
layout: "image"
title: "FWL06_01.JPG"
date: "2005-06-08T22:01:54"
picture: "FWL06_01.jpg"
weight: "35"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Fahrwerk"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4378
imported:
- "2019"
_4images_image_id: "4378"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2005-06-08T22:01:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4378 -->
Fahrwerk (light) Nummer 6 ist das Bugrad der "Cesspipsault".