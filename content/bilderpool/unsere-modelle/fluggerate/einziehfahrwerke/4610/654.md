---
layout: "comment"
hidden: true
title: "654"
date: "2005-08-21T12:01:41"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
EinsatzmöglichkeitenOk, als Fahrwerk für schwere Flugzeuge ist das nicht das richtige. Aber die Mechanik bietet sich an als nachstellbare Spannrolle für Ketten, Seile oder Förderbänder, und wenn man auf der Innenseite irgendwie noch Federn unterbringt, könnte auch ein Kettenfahrzeug draus werden.