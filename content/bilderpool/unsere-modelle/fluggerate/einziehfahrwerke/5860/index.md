---
layout: "image"
title: "FWL08_02.JPG"
date: "2006-03-12T13:16:05"
picture: "FWL08_02.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5860
imported:
- "2019"
_4images_image_id: "5860"
_4images_cat_id: "360"
_4images_user_id: "4"
_4images_image_date: "2006-03-12T13:16:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5860 -->
