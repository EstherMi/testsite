---
layout: "image"
title: "Landeklappe185.JPG"
date: "2008-03-19T11:21:37"
picture: "Landeklappe185.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/13949
imported:
- "2019"
_4images_image_id: "13949"
_4images_cat_id: "1199"
_4images_user_id: "4"
_4images_image_date: "2008-03-19T11:21:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13949 -->
Eine Landeklappe in ausgefahrenem Zustand.

Wenn man zwischen den Bildern "Landeklappe184" und "Landeklappe185" hin-und herwechselt, gibt das ein kleines Daumenkino.