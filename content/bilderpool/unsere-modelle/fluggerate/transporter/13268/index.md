---
layout: "image"
title: "Erlkönig08.jpg"
date: "2008-01-04T18:52:58"
picture: "EK008.JPG"
weight: "20"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/13268
imported:
- "2019"
_4images_image_id: "13268"
_4images_cat_id: "1199"
_4images_user_id: "4"
_4images_image_date: "2008-01-04T18:52:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13268 -->
Der Motor in Bildmitte betätigt das Höhenruder. Der "angebrochene" Zahnkranz gehört zu einer Zahnspurstange 38472.

Daneben gibt es eine Trimmung (Neigungsverstellung des ganzen Teils) per Hubgetriebe, das im Seitenruder, hinter der Platte 90x90, eingebaut ist.