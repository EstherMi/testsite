---
layout: "image"
title: "Erlkönig13"
date: "2008-03-19T11:27:38"
picture: "EK013.JPG"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/13951
imported:
- "2019"
_4images_image_id: "13951"
_4images_cat_id: "1199"
_4images_user_id: "4"
_4images_image_date: "2008-03-19T11:27:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13951 -->
Die Propeller sind aus den Aluminiumlamellen eines Fenster-Rollos gefertigt, die ich von Peter Damen geschenkt bekommen habe.