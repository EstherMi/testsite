---
layout: "image"
title: "Erlkönig03.jpg"
date: "2008-01-04T18:30:40"
picture: "EK003.JPG"
weight: "15"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/13263
imported:
- "2019"
_4images_image_id: "13263"
_4images_cat_id: "1199"
_4images_user_id: "4"
_4images_image_date: "2008-01-04T18:30:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13263 -->
Der ganze Rumpf ist um die Bedingung herumgebaut, dass der Laderaum mit den Platten 180x90 ausgelegt werden soll. Darunter sind in der Mitte 60 mm Höhe frei, sodass der ft-Akku noch hochkant drunter passt.

Dass die Winkelträger 120 quer unter dem Laderaumboden genau Loch auf Loch unter die Platten passen, war ein Glücksfall :-)