---
layout: "image"
title: "Erlkönig07.jpg"
date: "2008-01-04T18:46:13"
picture: "EK007.JPG"
weight: "19"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/13267
imported:
- "2019"
_4images_image_id: "13267"
_4images_cat_id: "1199"
_4images_user_id: "4"
_4images_image_date: "2008-01-04T18:46:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13267 -->
Das Bugfahrwerk nebst zugehörigen Luken wird wie das Hauptfahrwerk pneumatisch betätigt.

Ähh, das heißt, wenn ich nicht vorher noch meine P-Zylinder aus dem Fenster werfe. Entweder sie hakeln, trotz Schmierung mit allem was gut und teuer ist, oder die Luft zischt geradewegs innen durch...