---
layout: "image"
title: "Erlkönig17"
date: "2008-03-19T11:38:50"
picture: "EK017.JPG"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/13955
imported:
- "2019"
_4images_image_id: "13955"
_4images_cat_id: "1199"
_4images_user_id: "4"
_4images_image_date: "2008-03-19T11:38:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13955 -->
Der Flieger im Ganzen. Man beachte zum Größenvergleich den Mechaniker auf der linken Tragflächenwurzel.