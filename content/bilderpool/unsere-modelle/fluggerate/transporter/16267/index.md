---
layout: "image"
title: "BugFw14.JPG"
date: "2008-11-12T23:23:48"
picture: "BugFw14.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/16267
imported:
- "2019"
_4images_image_id: "16267"
_4images_cat_id: "1199"
_4images_user_id: "4"
_4images_image_date: "2008-11-12T23:23:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16267 -->
Das Bugfahrwerk komplett im Einbaurahmen, im eingezogenen Zustand und überkopf hingelegt. Der Trick bei diesem Aufbau ist, dass die Kugelköpfe aus der ft-Lenkung beim Ein/Ausfahren in der Nut des Gelenksteins entlang gleiten. Das spart eine sonst notwendige Gelenkstange (o.ä.) im Aufbau.