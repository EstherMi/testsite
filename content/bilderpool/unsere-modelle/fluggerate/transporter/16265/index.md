---
layout: "image"
title: "BugFw05.JPG"
date: "2008-11-12T23:17:23"
picture: "BugFw05.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/16265
imported:
- "2019"
_4images_image_id: "16265"
_4images_cat_id: "1199"
_4images_user_id: "4"
_4images_image_date: "2008-11-12T23:17:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16265 -->
Das Bugfahrwerk: 
Unten: die alte, pneumatische Version.
Oben: die neue, elektrische Version.

Ich hatte einmal vor, eine Sorte Magnetventil zu entwickeln, die kompakt genug für den Einbau im Unterleib des Transporters ist (so etwa 8 Stück in einer Kammer, in etwa 180x90x45 groß). Da wird auf absehbare Zeit nichts draus, und deswegen fliegen nach und nach die pneumatischen Funktionen wieder aus dem Modell heraus.