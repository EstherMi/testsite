---
layout: "image"
title: "Erlkönig16"
date: "2008-03-19T11:37:27"
picture: "EK016.JPG"
weight: "12"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/13954
imported:
- "2019"
_4images_image_id: "13954"
_4images_cat_id: "1199"
_4images_user_id: "4"
_4images_image_date: "2008-03-19T11:37:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13954 -->
Bei Kathedralen würde man diese Stelle als "Vierung" bezeichnen. Hier kommen Längs- und Quer"schiff" zusammen. Die Haupt-Tragflächen werden seitlich auf die hier sichtbaren Verbinder geschoben.

Um die Gleitführung ft-Alu / Verbinder / ft-Alu schön leichtgängig hinzubekommen, habe ich alle Teile durchprobiert und diejenigen mit den passenden Abweichungen (Alu weit, Verbinder eng) vom Nennmaß kombiniert.