---
layout: "image"
title: "Erlkönig12"
date: "2008-03-19T11:18:12"
picture: "EK012.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/13947
imported:
- "2019"
_4images_image_id: "13947"
_4images_cat_id: "1199"
_4images_user_id: "4"
_4images_image_date: "2008-03-19T11:18:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13947 -->
Das Hauptfahrwerk ist jetzt schon ziemlich am Ende. Schiebt man den Flieger etwas hin und her, womöglich noch in Kurven, dann zieht es die Gummis von den Reifen herunter.

Die linke Rumpfseite ist schon mal beplankt worden. Da sind auch ein paar durchsichtige Platten dabei :-)

Wie man rechts oben sieht, habe ich mich mittlerweile für Propeller entschieden.