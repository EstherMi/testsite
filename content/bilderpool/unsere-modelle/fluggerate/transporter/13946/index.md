---
layout: "image"
title: "Erlkönig11"
date: "2008-03-19T11:14:57"
picture: "EK011.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/13946
imported:
- "2019"
_4images_image_id: "13946"
_4images_cat_id: "1199"
_4images_user_id: "4"
_4images_image_date: "2008-03-19T11:14:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13946 -->
Die Trimmung für das Höhenleitwerk, eingebaut im Seitenleitwerk. Das Höhenruder wurde mittlerweile fast komplett geändert.