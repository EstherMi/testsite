---
layout: "image"
title: "Untergestell"
date: "2011-08-30T23:21:42"
picture: "rollenlager6.jpg"
weight: "6"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: ["Speichenrad"]
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- details/31731
imported:
- "2019"
_4images_image_id: "31731"
_4images_cat_id: "2364"
_4images_user_id: "381"
_4images_image_date: "2011-08-30T23:21:42"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31731 -->
Dieses Untergestell wird später noch ein wenig stabilisiert.
Erste Tests haben ergeben, dass es noch zuviel schwingt.