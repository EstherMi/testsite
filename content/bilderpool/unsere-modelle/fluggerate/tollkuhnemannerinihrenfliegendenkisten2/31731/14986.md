---
layout: "comment"
hidden: true
title: "14986"
date: "2011-08-31T11:28:16"
uploadBy:
- "ThanksForTheFish"
license: "unknown"
imported:
- "2019"
---
Ein bisschen Elastizität hilft mir an dieser Stelle sogar, weil dadurch eventuelle Schwingungen nicht nur durch das Lager abgefangen werden müssen.
Im Übrigen wird das Untergestell nur temporär gebraucht, die eigentliche Herausforderung ist es, den oberen Teil des Karussels so gut wie möglich auszuwuchten, damit die Nutation so gering wie möglich ausfällt.

Gruß, Ralf