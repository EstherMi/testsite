---
layout: "image"
title: "Die Rollen"
date: "2011-08-30T23:21:42"
picture: "rollenlager3.jpg"
weight: "3"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- details/31728
imported:
- "2019"
_4images_image_id: "31728"
_4images_cat_id: "2364"
_4images_user_id: "381"
_4images_image_date: "2011-08-30T23:21:42"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31728 -->
Sie laufen aber erst wie geschmiert, wenn das Oberteil drauf ist.