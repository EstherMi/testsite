---
layout: "image"
title: "indicator2"
date: "2011-06-10T09:06:27"
picture: "linktrainer05.jpg"
weight: "5"
konstrukteure: 
- "dutchbuilder"
fotografen:
- "dutchbuilder"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- details/30835
imported:
- "2019"
_4images_image_id: "30835"
_4images_cat_id: "2301"
_4images_user_id: "1315"
_4images_image_date: "2011-06-10T09:06:27"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30835 -->
The back of the indicator.