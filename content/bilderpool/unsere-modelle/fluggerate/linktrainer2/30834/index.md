---
layout: "image"
title: "indicator1"
date: "2011-06-10T09:06:27"
picture: "linktrainer04.jpg"
weight: "4"
konstrukteure: 
- "dutchbuilder"
fotografen:
- "dutchbuilder"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- details/30834
imported:
- "2019"
_4images_image_id: "30834"
_4images_cat_id: "2301"
_4images_user_id: "1315"
_4images_image_date: "2011-06-10T09:06:27"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30834 -->
The front of the indicator.