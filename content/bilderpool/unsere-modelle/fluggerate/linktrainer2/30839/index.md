---
layout: "image"
title: "bluebox1"
date: "2011-06-10T09:06:27"
picture: "linktrainer09.jpg"
weight: "9"
konstrukteure: 
- "dutchbuilder"
fotografen:
- "dutchbuilder"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- details/30839
imported:
- "2019"
_4images_image_id: "30839"
_4images_cat_id: "2301"
_4images_user_id: "1315"
_4images_image_date: "2011-06-10T09:06:27"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30839 -->
The bluebox overview.