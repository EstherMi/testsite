---
layout: "image"
title: "bluebox4"
date: "2011-06-10T09:06:27"
picture: "linktrainer12.jpg"
weight: "12"
konstrukteure: 
- "dutchbuilder"
fotografen:
- "dutchbuilder"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- details/30842
imported:
- "2019"
_4images_image_id: "30842"
_4images_cat_id: "2301"
_4images_user_id: "1315"
_4images_image_date: "2011-06-10T09:06:27"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30842 -->
The two control sticks. Links roles, rise and fall, right for the compass direction.
