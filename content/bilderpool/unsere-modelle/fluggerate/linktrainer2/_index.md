---
layout: "overview"
title: "Linktrainer2"
date: 2019-12-17T19:43:42+01:00
legacy_id:
- categories/2301
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2301 --> 
The most important different with the first version of the linktrainer is the replacement of the cockpit instruments.
The alti-meter, artificial horizon and the compass are replaced by a "hightech digital instrument".
On a screen you see a projection of the plane and his movements. Now you only have to hold the plane horizontal
on the two lines in the centre of the screen. Seemed to be easy!
The all-in-one stick is replaced by two sticks.
By the first version the time you used to reach your destiny was unlimited. Now there is a fuelmeter which slowly decreased.
When the tank is empty your plane crashed and the game is over.