---
layout: "image"
title: "Chipmunk10.JPG"
date: "2006-09-17T20:55:37"
picture: "Chipmunk10.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/6874
imported:
- "2019"
_4images_image_id: "6874"
_4images_cat_id: "658"
_4images_user_id: "4"
_4images_image_date: "2006-09-17T20:55:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6874 -->
