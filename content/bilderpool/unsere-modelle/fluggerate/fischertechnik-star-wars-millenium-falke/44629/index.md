---
layout: "image"
title: "FT_Star Wars_Millenium Falke"
date: "2016-10-19T16:58:21"
picture: "fischertechnikstarwarsmilleniumfalke04.jpg"
weight: "4"
konstrukteure: 
- "allsystemgmbh"
fotografen:
- "allsystemgmbh"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- details/44629
imported:
- "2019"
_4images_image_id: "44629"
_4images_cat_id: "3322"
_4images_user_id: "1688"
_4images_image_date: "2016-10-19T16:58:21"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44629 -->
