---
layout: "image"
title: "tarnkapp8381.jpg"
date: "2012-10-07T15:46:29"
picture: "IMG_8381.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/35810
imported:
- "2019"
_4images_image_id: "35810"
_4images_cat_id: "2673"
_4images_user_id: "4"
_4images_image_date: "2012-10-07T15:46:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35810 -->
Die Luftbremse besteht aus je zwei Klappen, die sich symmetrisch aufstellen. Der Antrieb kommt von rechts auf die untere Achse und wird links außen durch die Riegelscheiben gegensinnig auf die obere Achse übergeleitet.