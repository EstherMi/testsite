---
layout: "overview"
title: "Tarnkappenbomber"
date: 2019-12-17T19:43:46+01:00
legacy_id:
- categories/2673
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2673 --> 
Seit 2007 war der Tarnkappenbomber bei allen ftconventions dabei. 
Wie gut der Tarnmodus funktioniert, zeigen diese Fotos aus 2007 und aus 2011:
http://www.ftcommunity.de/details.php?image_id=11571
http://www.ftcommunity.de/details.php?image_id=28410

In 2012 wurde nun das Geheimnis gelüftet und der Bomber erstmals ungetarnt gezeigt.