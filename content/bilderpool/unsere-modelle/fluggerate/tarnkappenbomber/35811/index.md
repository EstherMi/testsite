---
layout: "image"
title: "tarnkapp8382.jpg"
date: "2012-10-07T15:49:51"
picture: "IMG_8382.JPG"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/35811
imported:
- "2019"
_4images_image_id: "35811"
_4images_cat_id: "2673"
_4images_user_id: "4"
_4images_image_date: "2012-10-07T15:49:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35811 -->
Auch bei den Querruder in Flügelmitte hat die Kardanwelle ohne Scherereien hinein gepasst. Das hat man selten.