---
layout: "image"
title: "Schiefe Verstrebungen 1r"
date: "2010-07-16T11:13:22"
picture: "IMG_3099_Winkelverbindung.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/27760
imported:
- "2019"
_4images_image_id: "27760"
_4images_cat_id: "359"
_4images_user_id: "4"
_4images_image_date: "2010-07-16T11:13:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27760 -->
