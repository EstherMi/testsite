---
layout: "image"
title: "Fighter-81.JPG"
date: "2006-11-19T20:10:21"
picture: "Fighter-81.JPG"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/7510
imported:
- "2019"
_4images_image_id: "7510"
_4images_cat_id: "707"
_4images_user_id: "4"
_4images_image_date: "2006-11-19T20:10:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7510 -->
Das Bugfahrwerk verwendet dieselbe Methode zum "Einrasten": auch hier steckt in der Lenkklaue ein S-Riegel 6 mit abgeschnittenem Griff drin und begrenzt den Drehbereich. Die beiden BS15, die da lose herumbaumeln, gehören zur Fahrwerksluke.