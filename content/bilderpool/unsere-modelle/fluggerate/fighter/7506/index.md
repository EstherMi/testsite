---
layout: "image"
title: "Fighter-69.JPG"
date: "2006-11-19T20:00:09"
picture: "Fighter-69.JPG"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/7506
imported:
- "2019"
_4images_image_id: "7506"
_4images_cat_id: "707"
_4images_user_id: "4"
_4images_image_date: "2006-11-19T20:00:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7506 -->
Das Hauptfahrwerk von innen gesehen, in Position 1 (ausgefahren). Das Fahrwerk beruht auf einer Art "Kniegelenk"-Mechanik, bei der im ausgefahrenen Zustand ein Gelenk überstreckt wird und dadurch das Fahrwerk "verriegelt" ist. Zentrales Element ist ein S-Riegel 6, dessen Griffstück abgeschnippelt wurde und der in der Lenkklaue drinsteckt und dessen Drehbereich begrenzt. 

Die Bilder 69, 70, 71 ergeben in Folge betrachtet die Sequenz beim Ein- und Ausfahren (à la Daumenkino).