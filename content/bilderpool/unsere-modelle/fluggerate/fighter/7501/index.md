---
layout: "image"
title: "Fighter-28.JPG"
date: "2006-11-19T19:44:52"
picture: "Fighter-28.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/7501
imported:
- "2019"
_4images_image_id: "7501"
_4images_cat_id: "707"
_4images_user_id: "4"
_4images_image_date: "2006-11-19T19:44:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7501 -->
Jetzt das Ganze mit ausgefahrenen Fahrwerken und offener Luke vorn.

Die Landescheinwerfer sind auf den oberen Schenkeln der Fahrwerksbeine montiert, deshalb gibt es beim Einfahren ein interessantes Bild: die Scheinwerfer und die Räder rotieren um unterschiedliche Drehpunkte (einer oben, einer unten).