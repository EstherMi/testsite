---
layout: "image"
title: "Fighter-01.JPG"
date: "2006-11-19T19:38:58"
picture: "Fighter-01.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/7498
imported:
- "2019"
_4images_image_id: "7498"
_4images_cat_id: "707"
_4images_user_id: "4"
_4images_image_date: "2006-11-19T19:38:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7498 -->
So eine Mischung aus allem, was "MiG" oder "Su" oder "F-xxx" heißt. Es sind keine Motoren eingebaut. Beweglich sind Bug- und Haupfahrwerk, die Luken fürs Bugfahrwerk sowie die Seitenruder. 

Ich hake diesen Flieger mal unter "Design-Studie" ab, denn man muss direkt per Hand ins Getriebe greifen, weil für gescheite Hebel oder Antriebe kein Platz ist.