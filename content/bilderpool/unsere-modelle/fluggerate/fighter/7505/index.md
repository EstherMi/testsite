---
layout: "image"
title: "Fighter-57.JPG"
date: "2006-11-19T19:51:55"
picture: "Fighter-57.JPG"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/7505
imported:
- "2019"
_4images_image_id: "7505"
_4images_cat_id: "707"
_4images_user_id: "4"
_4images_image_date: "2006-11-19T19:51:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7505 -->
