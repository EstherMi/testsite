---
layout: "overview"
title: "Pauls Flugzeugkarussell"
date: 2019-12-17T19:43:54+01:00
legacy_id:
- categories/2729
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2729 --> 
Paul, junger Sohn von Freunden, war zu Besuch und baute dieses Fliegerkarussell bis auf die Schleifringe ganz alleine. Das Modell durfte auch mit zur Convention 2012.