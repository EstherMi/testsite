---
layout: "image"
title: "Gegengewicht"
date: "2013-03-22T10:51:13"
picture: "paulsflugzeugkarussell3.jpg"
weight: "3"
konstrukteure: 
- "Paul Müller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/36798
imported:
- "2019"
_4images_image_id: "36798"
_4images_cat_id: "2729"
_4images_user_id: "104"
_4images_image_date: "2013-03-22T10:51:13"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36798 -->
Damit der Arm leicht dreht, sind auf der dem Flieger gegenüberliegenden Seite Metallachsen als Gegengewicht angebracht und der Deckel davon für den Convention-Transport mit einem Gummi gesichert.