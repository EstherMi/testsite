---
layout: "image"
title: "DC9-10_3120.JPG"
date: "2011-02-12T12:30:34"
picture: "DC9-10_3120.JPG"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/29906
imported:
- "2019"
_4images_image_id: "29906"
_4images_cat_id: "2208"
_4images_user_id: "4"
_4images_image_date: "2011-02-12T12:30:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29906 -->
Die Heckflosse im Rohbau. In den Hohlraum werden drei Motoren hineingequetscht. Einer davon, ein ft-Servo, ist schon drin, wird aber nochmal den Platz wechseln müssen. Die reichliche Beplankung mit S-Streben musste auch noch gestutzt werden.