---
layout: "image"
title: "DC9-03_4100.JPG"
date: "2011-02-12T12:10:45"
picture: "DC9-03_4100.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/29899
imported:
- "2019"
_4images_image_id: "29899"
_4images_cat_id: "2208"
_4images_user_id: "4"
_4images_image_date: "2011-02-12T12:10:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29899 -->
Das Blau der Bücherschutzfolie "kommt" ganz gut. Zusammen mit dem Silber sieht es ziemlich edel aus.