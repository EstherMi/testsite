---
layout: "image"
title: "DC9-14_3335.JPG"
date: "2011-02-12T12:49:24"
picture: "DC9-14_3335.JPG"
weight: "14"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/29910
imported:
- "2019"
_4images_image_id: "29910"
_4images_cat_id: "2208"
_4images_user_id: "4"
_4images_image_date: "2011-02-12T12:49:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29910 -->
Nochmal der Rumpfquerschnitt; hier mit zusätzlichen Streben außen (die sind später weggefallen) und der Methode, wie man zwei eTec-Module unter den Kabinenboden bekommt, ohne dass die Stecker über das Außenprofil hinausstehen.