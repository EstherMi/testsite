---
layout: "image"
title: "DC9-07_4615.JPG"
date: "2011-02-12T12:20:19"
picture: "DC9-07_4615.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/29903
imported:
- "2019"
_4images_image_id: "29903"
_4images_cat_id: "2208"
_4images_user_id: "4"
_4images_image_date: "2011-02-12T12:20:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29903 -->
Das Cockpit. Zwischen den Sitzen befindet sich der Motor fürs Bugfahrwerk.