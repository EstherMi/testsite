---
layout: "image"
title: "DC9-13_3095.JPG"
date: "2011-02-12T12:46:34"
picture: "DC9-13_3095.JPG"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/29909
imported:
- "2019"
_4images_image_id: "29909"
_4images_cat_id: "2208"
_4images_user_id: "4"
_4images_image_date: "2011-02-12T12:46:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29909 -->
Damit fängt ein jedes Fliegerprojekt an: zuerst muss ein Rumpfquerschnitt gefunden werden, der mit möglichst wenig Stückwerk auskommt und hinreichend Platz für stehende/sitzende Passagiere und ggf. Gepäck im Unterboden aufweist. Irgendwo (hier: unten links, unten rechts, oben mitte) müssen auch Längsverstrebungen über die ganze Zelle hinweg vorgesehen werden.