---
layout: "overview"
title: "McDonnell Douglas DC-9"
date: 2019-12-17T19:43:31+01:00
legacy_id:
- categories/2208
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2208 --> 
... ist zumindest das nächstliegende 'echte' Flugzeug zu diesem Modell. Die Konfiguration der Heckflosse (Position des Höhenruders) ist allerdings deutlich anders.