---
layout: "image"
title: "DC9-20_3334.JPG"
date: "2011-02-12T13:10:31"
picture: "DC9-20_3334.JPG"
weight: "20"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/29916
imported:
- "2019"
_4images_image_id: "29916"
_4images_cat_id: "2208"
_4images_user_id: "4"
_4images_image_date: "2011-02-12T13:10:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29916 -->
Das Untersetzungsgetriebe für das Hauptfahrwerk. Es hat erstens zwei rote Schnecken hintereinander und zweitens baut es so flach, dass es ins Profil der Tragflächen hineinpasst.

Dieses Getriebe lag schon recht lange halbgar als "Kaulquappe" in der Schublade, hat sich dann zum Frosch entwickelt und wurde per Kuss in einen Prinzen verwandelt. Das hat man auch nicht alle Tage :-)