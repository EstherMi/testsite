---
layout: "image"
title: "DC9-02_4602.JPG"
date: "2011-02-12T12:08:45"
picture: "DC9-02_4602.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/29898
imported:
- "2019"
_4images_image_id: "29898"
_4images_cat_id: "2208"
_4images_user_id: "4"
_4images_image_date: "2011-02-12T12:08:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29898 -->
Auf der linken Seite habe ich verschiedene Methoden zur Beplankung ausprobiert.
-- Tragfläche und Seitenfenster: Bücher-Einschlagfolie, in Heißlaminat-Tasche
-- Rumpf-Oberseite: Geschenkpapier in Alu-Look, auch in Heißlaminat-Tasche
-- Heckflosse: Küchen-Alufolie in Heißlaminat-Tasche