---
layout: "image"
title: "DC9-18_3089.JPG"
date: "2011-02-12T13:01:37"
picture: "DC9-18_3089.JPG"
weight: "18"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/29914
imported:
- "2019"
_4images_image_id: "29914"
_4images_cat_id: "2208"
_4images_user_id: "4"
_4images_image_date: "2011-02-12T13:01:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29914 -->
Das Hauptfahrwerk mit weiteren Teilen. Die BS30 unten stützen das Fahrwerk gegen das Rumpfheck ab. Mit den zwei BS15 links im Bild wird es in Rumpfmitte, direkt hinter den querliegenden Aluträger eingehängt.

Der BS7,5 und der Winkel 60 zeigen genau den Ärger, den ich schon seit Erbes-Büdesheim mit dem Teil habe: sie rutschen voneinander herunter, und dann baumelt das Fahrwerk nur noch lahm unterm Flieger herum.