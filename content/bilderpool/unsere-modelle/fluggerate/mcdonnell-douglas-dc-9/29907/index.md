---
layout: "image"
title: "DC9-11_4609.JPG"
date: "2011-02-12T12:38:13"
picture: "DC9-11_4609.JPG"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/29907
imported:
- "2019"
_4images_image_id: "29907"
_4images_cat_id: "2208"
_4images_user_id: "4"
_4images_image_date: "2011-02-12T12:38:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29907 -->
Die Heckflosse ist komplett. 

Hinten aufrecht: der Antrieb fürs Höhenruder. Das Hubgetriebe schiebt eine Zahnstange über das Eingangszahnrad des Mini-Stufengetriebes. Dessen Ausgang bewegt über die Kurbel und die vertikal verlaufende S-Strebe das Höhenruder.

Hinten unten: ein ft-Servo, der das Seitenruder betätigt.

Weiter vorn (im Bild rechts) das graue Schneckengetriebe treibt die Seiltrommel an, auf deren Achse weiter unten zwei Kurbeln sitzen, die die Neigung der ganzen horizontalen Heckflosse verändert -- das ist die Trimmung, mit der die Schwerpunktverlagerung des Fliegers bei Beladung und Umverteilung des Treibstoffvorrats ausgeglichen wird.