---
layout: "image"
title: "DC9-15_4103.JPG"
date: "2011-02-12T12:53:52"
picture: "DC9-15_4103.JPG"
weight: "15"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/29911
imported:
- "2019"
_4images_image_id: "29911"
_4images_cat_id: "2208"
_4images_user_id: "4"
_4images_image_date: "2011-02-12T12:53:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29911 -->
Das Triebwerk. Es ist ein reiner Dummy - da dreht sich nichts dran, und es leuchtet nichts drin. Vorne wurde mit etwas Knetmasse nachgeholfen, damit die Rastachsen nicht auf Reisen gehen.

Die blaue Folie sieht einfach nur gut aus :-) Trotzdem wird der Transporter nebenan noch etwas viel feineres kriegen...