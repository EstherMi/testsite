---
layout: "image"
title: "DC9-17_3085.JPG"
date: "2011-02-12T12:57:54"
picture: "DC9-17_3085.JPG"
weight: "17"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/29913
imported:
- "2019"
_4images_image_id: "29913"
_4images_cat_id: "2208"
_4images_user_id: "4"
_4images_image_date: "2011-02-12T12:57:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29913 -->
Die Mechanik des Hauptfahrwerks. Das Z20 rechts wird noch durch ein Z10 ersetzt. Der kurze Arm (mit Winkel 60 drin) des Kniegelenks bewegt den Rest auf und ab, und beim Überstrecken sorgt das Gewicht dafür, dass das Fahrwerk verriegelt, also unter Last nicht wieder einknickt.