---
layout: "comment"
hidden: true
title: "11877"
date: "2010-07-22T10:43:04"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
<Angebermodus>Ich hätt's gewusst, <Angebermodus>
obwohl die Streben nur als obenliegende Befestigung gedacht sind. Mangels richtiger Lochform kann man schlecht mit innenliegenden S-Riegeln arbeiten, mal ganz abgesehen davon, dass das beim beidseitigen Beplanken nur mit Hilfe von Ameisen (o.ä.) funktioniert. 

Die Alufolie hat auch so ihre Tücken. Einen ganzen DIN A4-Bogen krieg ich nicht faltenfrei zu Stande. Das Plastikzeugs schrumpft beim Erhitzen, das Alu nicht, und schwupps, hat der Bogen drei Längsfalten drin :-(

Gruß,
Harald