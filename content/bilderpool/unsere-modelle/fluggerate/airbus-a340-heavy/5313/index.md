---
layout: "image"
title: "BugFW05.JPG"
date: "2005-11-11T12:57:09"
picture: "BugFW05.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5313
imported:
- "2019"
_4images_image_id: "5313"
_4images_cat_id: "381"
_4images_user_id: "4"
_4images_image_date: "2005-11-11T12:57:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5313 -->
Die schräg abgeschnittenen 32085 liegen auf dem Baustein 15x30x5 auf und verhindern so, dass das Fahrwerk im ausgefahrenen Zustand einknickt. Die Metallachse ist unbedingt nötig, weil sie das ganze Gewicht auf den Rumpf überträgt.