---
layout: "image"
title: "Flaps45.JPG"
date: "2005-11-11T12:59:12"
picture: "Flaps45.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5314
imported:
- "2019"
_4images_image_id: "5314"
_4images_cat_id: "381"
_4images_user_id: "4"
_4images_image_date: "2005-11-11T12:59:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5314 -->
Die Zylinder für die Spoiler (unten) und Flaps (oben).