---
layout: "comment"
hidden: true
title: "4306"
date: "2007-10-15T20:24:44"
uploadBy:
- "heiko"
license: "unknown"
imported:
- "2019"
---
Mal so ganz am Rande ... wieso ist diese Beschreibung auf Englisch? Und dazu noch sprachlich so sauber, wie ich sie mir von meiner Videorecorder-Bedienungsanleitung gewünscht hätte? 

Wird Zeit, dass wir die ftc aktiv mehrsprachig machen.