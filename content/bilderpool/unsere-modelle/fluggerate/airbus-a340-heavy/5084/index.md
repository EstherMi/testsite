---
layout: "image"
title: "A340H_294.JPG"
date: "2005-10-06T17:28:46"
picture: "A340H_294.jpg"
weight: "39"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["modding"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5084
imported:
- "2019"
_4images_image_id: "5084"
_4images_cat_id: "381"
_4images_user_id: "4"
_4images_image_date: "2005-10-06T17:28:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5084 -->
The cockpit, seen from behind (inside). The radar antenna is mounted below the cabin floor.

This piece actually represents the initial cross-section of the whole aircraft in its very early days. Originally, the two BS15 at the bottom were marking the bottom of the floor of the fuselage. Anything below was 'underbelly', everything upward was 'inside of the body'. As it turned out later, the 'underbelly' section was too small, and the 'inside' section too. So it all had to grow somewhat.

However, the former cross-section of the fuselage was a perfect fit for the nose section of the plane.