---
layout: "image"
title: "A340H_158.JPG"
date: "2005-10-06T17:26:23"
picture: "A340H_158.jpg"
weight: "22"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5067
imported:
- "2019"
_4images_image_id: "5067"
_4images_cat_id: "381"
_4images_user_id: "4"
_4images_image_date: "2005-10-06T17:26:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5067 -->
We're at an advanced stage here. Lights are not yet mounted; tail and much of the cabling are still missing. Anyway, this is where the cargo pallets go in.