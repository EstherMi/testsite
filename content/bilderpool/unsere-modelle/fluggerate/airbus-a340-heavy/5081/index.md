---
layout: "image"
title: "A340H_244.JPG"
date: "2005-10-06T17:28:45"
picture: "A340H_244.jpg"
weight: "36"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5081
imported:
- "2019"
_4images_image_id: "5081"
_4images_cat_id: "381"
_4images_user_id: "4"
_4images_image_date: "2005-10-06T17:28:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5081 -->
The left main landing gear in all its glory and ready for landing. The spring is only there to ensure the correct wheel position when retracting the gear into the 'airborne' position.