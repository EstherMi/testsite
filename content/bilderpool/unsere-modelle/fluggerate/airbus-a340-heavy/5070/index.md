---
layout: "image"
title: "A340H_208.JPG"
date: "2005-10-06T17:26:23"
picture: "A340H_208.jpg"
weight: "25"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5070
imported:
- "2019"
_4images_image_id: "5070"
_4images_cat_id: "381"
_4images_user_id: "4"
_4images_image_date: "2005-10-06T17:26:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5070 -->
Floor tiling is removed for better view.
The mid and mid-rear compartments of the underbelly are used to house the main landing gear and the rudder and flap gearbox, respectively. 

The upper mid-rear compartment remains empty because a firm hand grip on the aluminum struts is required to move the whole model around.