---
layout: "image"
title: "A340H_027.JPG"
date: "2005-10-09T14:04:12"
picture: "A340H_327.jpg"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5085
imported:
- "2019"
_4images_image_id: "5085"
_4images_cat_id: "381"
_4images_user_id: "4"
_4images_image_date: "2005-10-09T14:04:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5085 -->
On the apron.