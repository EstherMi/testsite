---
layout: "image"
title: "A340H_118.JPG"
date: "2005-10-06T17:26:22"
picture: "A340H_118.jpg"
weight: "20"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5065
imported:
- "2019"
_4images_image_id: "5065"
_4images_cat_id: "381"
_4images_user_id: "4"
_4images_image_date: "2005-10-06T17:26:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5065 -->
Still not showing the current design in all details, this is how the landing gear folds into place once the plane is airborne.