---
layout: "image"
title: "A340H_268.JPG"
date: "2005-10-14T18:19:46"
picture: "A340H_268.jpg"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5092
imported:
- "2019"
_4images_image_id: "5092"
_4images_cat_id: "381"
_4images_user_id: "4"
_4images_image_date: "2005-10-14T18:19:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5092 -->
The nose landing gear seen from the inside. At the moment it is retracted and has pressed two switches (black and gray, stacked on top of each other). The black one stops the landing gear motor; the gray one controls the landing lights.

The black switch and its companion on the other side of the nose gear are all that is necessary to control the landing gear. This is because the nose gear moves slower than the main gear, and one gearwheel in each of the legs of the main gear isn't fully fastened. Therefore, you can be sure that the main gear is already in its end position (with an axle slipping somewhere in the drive chain) once the nose gear has reached one of its end switches and stops the motor.