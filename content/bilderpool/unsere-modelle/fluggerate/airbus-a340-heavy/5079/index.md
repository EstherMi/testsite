---
layout: "image"
title: "A340H_238.JPG"
date: "2005-10-06T17:28:45"
picture: "A340H_238.jpg"
weight: "34"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5079
imported:
- "2019"
_4images_image_id: "5079"
_4images_cat_id: "381"
_4images_user_id: "4"
_4images_image_date: "2005-10-06T17:28:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5079 -->
about to land... (4 of 5)

Main gear isn't yet there, and nose gear still isn't locked.