---
layout: "image"
title: "Tür"
date: "2015-02-22T13:47:35"
picture: "sikorskyshseahawk4.jpg"
weight: "4"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- details/40583
imported:
- "2019"
_4images_image_id: "40583"
_4images_cat_id: "3044"
_4images_user_id: "791"
_4images_image_date: "2015-02-22T13:47:35"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40583 -->
Hier erkennt man die Schiebetür.
Ein Magnet hält sie geschlossen.
Die Federung ist am Anschlag, hält aber.