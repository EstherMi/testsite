---
layout: "image"
title: "Piloten"
date: "2015-02-22T13:47:35"
picture: "sikorskyshseahawk2.jpg"
weight: "2"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- details/40581
imported:
- "2019"
_4images_image_id: "40581"
_4images_cat_id: "3044"
_4images_user_id: "791"
_4images_image_date: "2015-02-22T13:47:35"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40581 -->
Hier erkennt man das Cockpit mit den beiden Piloten.
Die Turbinen habe ich angedeutet.