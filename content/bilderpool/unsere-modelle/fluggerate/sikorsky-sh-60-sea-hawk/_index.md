---
layout: "overview"
title: "Sikorsky SH-60 Sea Hawk"
date: 2019-12-17T19:43:59+01:00
legacy_id:
- categories/3044
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3044 --> 
Hubschrauber der dem Sikorsky SH-60 Sea Hawk nachempfunden wurde.
Der Hubschrauber wurde bereits im Sommer 2011 gebaut und kurz danach auch wieder zerlegt.