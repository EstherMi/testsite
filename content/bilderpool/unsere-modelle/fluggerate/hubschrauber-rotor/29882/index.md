---
layout: "image"
title: "Detailansicht: Hauptrotor mit gesamter Taumelscheibe (Konstruktionszeichnung)"
date: "2011-02-07T11:39:01"
picture: "hubschrauberrotor05.jpg"
weight: "11"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/29882
imported:
- "2019"
_4images_image_id: "29882"
_4images_cat_id: "2205"
_4images_user_id: "1126"
_4images_image_date: "2011-02-07T11:39:01"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29882 -->
Der obere, mitdrehende Teil der Taumelscheibe liegt lose auf dem festen Unterteil, ebenfalls einer Drehscheibe 60. Die drei seitlich angebrachten Seilrollen halten die beiden Drehscheiben übereinander und sorgen dafür, dass bei einer Veränderung des Neigungswinkels der unteren Drehscheibe sich auch die mitdrehende Drehscheibe entsprechend verstellt.