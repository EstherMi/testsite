---
layout: "image"
title: "Gesamtansicht des Hauptrotors"
date: "2011-02-07T11:39:01"
picture: "hubschrauberrotor01.jpg"
weight: "7"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/29878
imported:
- "2019"
_4images_image_id: "29878"
_4images_cat_id: "2205"
_4images_user_id: "1126"
_4images_image_date: "2011-02-07T11:39:01"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29878 -->
Das oben abgebildete Funktionsmodell wurde zur Veranschaulichung der Funktionsweise eines Hubschrauberrotors entwickelt. Der Anstellwinkel der beiden Rotorblätter (Pitch) wird dabei über drei Schnecken eingestellt, die von Mini-Motoren angetrieben werden. Als Pitch dient der Hebel unten rechts im Bild (Achse an Lenkwürfel in Lenkklaue); er schaltet die Mini-Motoren. Statt eines Sticks (mit dem in einem echten Hubschrauber die Neigung der Taumelscheibe eingestellt wird) kann mit den drei Tastern auf der anderen Seite der Bauplatte die Stromzufuhr zu je einem Mini-Motor unterbrochen werden; so lässt sich die Taumelscheibe schräg stellen. (Die beiden Polwender dienen als "Not-Aus"-Schalter.)

P.S.: Wie im Forum schon gepostet, bietet der 15-Minuten-Film "Igor Sikorsky und der Hubschrauber" (http://www.youtube.com/watch?v=dgWi2YBi9Vc), der in der Reihe "Meilensteine aus Naturwissenschaft und Technik" erschien, eine sensationell gute Erläuterung der Funktionsweise eines Hubschraubers. Und eine sehr kompakte Erklärung von Harald findet sich hier: http://www.ftcommunity.de/details.php?image_id=13927. Daher setzen wir das bei den folgenden Beschreibungen einmal als bekannt voraus ;-)