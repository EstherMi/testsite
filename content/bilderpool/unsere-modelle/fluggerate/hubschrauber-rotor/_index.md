---
layout: "overview"
title: "Hubschrauber-Rotor"
date: 2019-12-17T19:43:27+01:00
legacy_id:
- categories/2205
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2205 --> 
Haupt- und Heckrotor eines Hubschraubers mit Pitch und Taumelscheibe.