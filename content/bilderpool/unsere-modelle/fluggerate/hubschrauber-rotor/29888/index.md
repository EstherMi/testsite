---
layout: "image"
title: "Detailansicht: Seitenansicht des Heckrotors"
date: "2011-02-07T11:39:01"
picture: "hubschrauberrotor11.jpg"
weight: "17"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/29888
imported:
- "2019"
_4images_image_id: "29888"
_4images_cat_id: "2205"
_4images_user_id: "1126"
_4images_image_date: "2011-02-07T11:39:01"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29888 -->
Seitenansicht der Pitch-Einstellung mit Rotorblättern, Antriebsachse und Verstellhebel. Als Antrieb dient ein 1:8-Powermotor.