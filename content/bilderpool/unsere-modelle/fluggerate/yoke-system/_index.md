---
layout: "overview"
title: "Yoke System"
date: 2019-12-17T19:43:23+01:00
legacy_id:
- categories/1930
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1930 --> 
Yoke System zum Steuern eines Flugsimulators. Man sieht das "Lenkrad" und den Gashebel. Die Pedale habe ich noch nicht konstruiert.