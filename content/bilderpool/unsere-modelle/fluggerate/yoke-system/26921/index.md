---
layout: "image"
title: "Yoke Gesamtansicht"
date: "2010-04-08T17:38:29"
picture: "yokesystem7.jpg"
weight: "7"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- details/26921
imported:
- "2019"
_4images_image_id: "26921"
_4images_cat_id: "1930"
_4images_user_id: "1112"
_4images_image_date: "2010-04-08T17:38:29"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26921 -->
Die Übertragung zum PC erfolgt mit einem Ultron Wireless Gamepad. Die Elektronik habe ich in eine Plastik-Box gebaut, und die Anschlüsse rausgeführt. Daran werden nun die Potentiometer und Taster angeschlossen.