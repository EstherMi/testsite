---
layout: "image"
title: "Fußpedale Mechanik (Yoke System)"
date: "2010-05-02T11:16:09"
picture: "fusspedale3.jpg"
weight: "3"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- details/27031
imported:
- "2019"
_4images_image_id: "27031"
_4images_cat_id: "1945"
_4images_user_id: "1112"
_4images_image_date: "2010-05-02T11:16:09"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27031 -->
Wenn ein Fußpedal nach vorne bewegt wird, wird das andere Pedal durch das Zahnrad nach hintenbewegt.