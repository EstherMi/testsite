---
layout: "image"
title: "Fußpedale Seitenansicht (Yoke System)"
date: "2010-05-02T11:16:09"
picture: "fusspedale7.jpg"
weight: "7"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- details/27035
imported:
- "2019"
_4images_image_id: "27035"
_4images_cat_id: "1945"
_4images_user_id: "1112"
_4images_image_date: "2010-05-02T11:16:09"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27035 -->
Der Anschluss des Potentiometer erfolgt wieder über die Gamepadbox, die ihr schon auf einem andern Bild von mir gesehen habt.