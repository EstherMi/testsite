---
layout: "image"
title: "Fußpedale Mechanik für das Potentiometer (Yoke System)"
date: "2010-05-02T11:16:09"
picture: "fusspedale4.jpg"
weight: "4"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- details/27032
imported:
- "2019"
_4images_image_id: "27032"
_4images_cat_id: "1945"
_4images_user_id: "1112"
_4images_image_date: "2010-05-02T11:16:09"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27032 -->
Eigentlich sollte die Kette vom Antriebszahnrad direkt auf das Potentiometer gehen. Leider ist Theorie nicht gleich Praxis. Durch das Gewicht der Füße verkantet sich wohl das Antriebszahnrad so, dass es nicht immer die Metallstange dreht. Deshalb habe ich ein gleich großes Zahnrad mit Verbindungsstangen darunter gebaut. Dadurch kann kein Durchdrehen mehr erfolgen. Um die Übersetzung aber einzuhalten, mußte ich noch mal eine Zwischenzahnradstange einbauen. Auf dem engen Raum reine Millimeterarbeit.