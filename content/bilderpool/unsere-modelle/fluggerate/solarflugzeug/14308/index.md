---
layout: "image"
title: "Solarflugzeug"
date: "2008-04-19T14:36:42"
picture: "solarflugzeug1.jpg"
weight: "1"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/14308
imported:
- "2019"
_4images_image_id: "14308"
_4images_cat_id: "1324"
_4images_user_id: "747"
_4images_image_date: "2008-04-19T14:36:42"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14308 -->
Das ist ein Gesamtbild von meinem Solarflugzeug.