---
layout: "image"
title: "Vordere Seite des Solarflugzeuges"
date: "2008-04-19T14:36:42"
picture: "solarflugzeug2.jpg"
weight: "2"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/14309
imported:
- "2019"
_4images_image_id: "14309"
_4images_cat_id: "1324"
_4images_user_id: "747"
_4images_image_date: "2008-04-19T14:36:42"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14309 -->
Hier sieht man den Propeller, das vordere Fahrwerk und die Streben, die die Tragflächen stabilisieren.