---
layout: "image"
title: "Segler04.JPG"
date: "2006-08-15T15:37:11"
picture: "Segler04.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/6683
imported:
- "2019"
_4images_image_id: "6683"
_4images_cat_id: "601"
_4images_user_id: "4"
_4images_image_date: "2006-08-15T15:37:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6683 -->
