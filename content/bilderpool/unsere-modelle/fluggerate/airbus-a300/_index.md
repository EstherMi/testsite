---
layout: "overview"
title: "Airbus A300"
date: 2019-12-17T19:43:35+01:00
legacy_id:
- categories/2259
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2259 --> 
Modell eines Airbus A300 mit funktionsfähigem Fahrwerk mit Endabschaltung, beweglichem Höhen-, und Seitenruder.