---
layout: "image"
title: "Ladeluke"
date: "2011-04-06T16:48:02"
picture: "a2.jpg"
weight: "2"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- details/30430
imported:
- "2019"
_4images_image_id: "30430"
_4images_cat_id: "2259"
_4images_user_id: "791"
_4images_image_date: "2011-04-06T16:48:02"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30430 -->
Hier sieht man die Ladeluke in geöffneten Zustand.
Normal wird sie nur bis zur Hälfte hochgeklappt.