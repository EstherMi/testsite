---
layout: "overview"
title: "Weltraumodelle"
date: 2019-12-17T19:44:07+01:00
legacy_id:
- categories/3503
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3503 --> 
Hier ein paar Kleinmodelle aus einer Welt die mir unbekannt, meinem Sohn aber umso bekannter ist :) Jedenfalls habe ich da nix mit zu tun und Konstantin (9) hat das selbst konstruiert :)