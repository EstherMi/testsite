---
layout: "image"
title: "Kleinmodelle aus dem All"
date: "2018-04-15T18:11:30"
picture: "weltraumodelle4.jpg"
weight: "6"
konstrukteure: 
- "Konstantin Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/47410
imported:
- "2019"
_4images_image_id: "47410"
_4images_cat_id: "3503"
_4images_user_id: "968"
_4images_image_date: "2018-04-15T18:11:30"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47410 -->
