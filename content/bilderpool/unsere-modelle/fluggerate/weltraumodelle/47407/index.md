---
layout: "image"
title: "Kleinmodelle aus dem All"
date: "2018-04-15T18:11:30"
picture: "weltraumodelle1.jpg"
weight: "3"
konstrukteure: 
- "Konstantin Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/47407
imported:
- "2019"
_4images_image_id: "47407"
_4images_cat_id: "3503"
_4images_user_id: "968"
_4images_image_date: "2018-04-15T18:11:30"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47407 -->
