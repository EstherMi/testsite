---
layout: "image"
title: "Tragfläche3"
date: "2010-07-16T11:20:27"
picture: "IMG_3125_Tragflche_fertig.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/27764
imported:
- "2019"
_4images_image_id: "27764"
_4images_cat_id: "359"
_4images_user_id: "4"
_4images_image_date: "2010-07-16T11:20:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27764 -->
Und hier die zur Hälfte fertige Tragfläche (die Unterseite ist noch roh, und die Vorderkante fehlt auch noch).