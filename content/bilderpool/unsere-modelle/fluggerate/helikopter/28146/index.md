---
layout: "image"
title: "Rundum06"
date: "2010-09-14T20:16:06"
picture: "rundumblick06.jpg"
weight: "18"
konstrukteure: 
- "Winki"
fotografen:
- "Winki"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Winki"
license: "unknown"
legacy_id:
- details/28146
imported:
- "2019"
_4images_image_id: "28146"
_4images_cat_id: "2042"
_4images_user_id: "1184"
_4images_image_date: "2010-09-14T20:16:06"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28146 -->
Ansicht 6