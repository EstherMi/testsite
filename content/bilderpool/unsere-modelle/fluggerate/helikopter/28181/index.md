---
layout: "image"
title: "Blick in das Cockpit1"
date: "2010-09-18T13:36:54"
picture: "DSCF0397.jpg"
weight: "2"
konstrukteure: 
- "Winki"
fotografen:
- "Winki"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Winki"
license: "unknown"
legacy_id:
- details/28181
imported:
- "2019"
_4images_image_id: "28181"
_4images_cat_id: "2042"
_4images_user_id: "1184"
_4images_image_date: "2010-09-18T13:36:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28181 -->
Blick zum Steuerpult des Piloten