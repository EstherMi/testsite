---
layout: "image"
title: "Rundum09"
date: "2010-09-14T20:16:06"
picture: "rundumblick09.jpg"
weight: "21"
konstrukteure: 
- "Winki"
fotografen:
- "Winki"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Winki"
license: "unknown"
legacy_id:
- details/28149
imported:
- "2019"
_4images_image_id: "28149"
_4images_cat_id: "2042"
_4images_user_id: "1184"
_4images_image_date: "2010-09-14T20:16:06"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28149 -->
Ansicht 9