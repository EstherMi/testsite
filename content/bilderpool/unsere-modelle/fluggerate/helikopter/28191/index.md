---
layout: "image"
title: "Bei Nacht"
date: "2010-09-18T13:36:55"
picture: "DSCF0415.jpg"
weight: "12"
konstrukteure: 
- "Winki"
fotografen:
- "Winki"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Winki"
license: "unknown"
legacy_id:
- details/28191
imported:
- "2019"
_4images_image_id: "28191"
_4images_cat_id: "2042"
_4images_user_id: "1184"
_4images_image_date: "2010-09-18T13:36:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28191 -->
Der Helikopter hat zwei Lichter 
  - vorne zum Beleuchten der Flugbahn
  - im Cockpit zum Beleuchten des Steuerpultes