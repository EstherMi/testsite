---
layout: "image"
title: "Steuermechanismus3"
date: "2010-09-18T13:36:55"
picture: "DSCF0413.jpg"
weight: "11"
konstrukteure: 
- "Winki"
fotografen:
- "Winki"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Winki"
license: "unknown"
legacy_id:
- details/28190
imported:
- "2019"
_4images_image_id: "28190"
_4images_cat_id: "2042"
_4images_user_id: "1184"
_4images_image_date: "2010-09-18T13:36:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28190 -->
Kardanwelle zum Kippen