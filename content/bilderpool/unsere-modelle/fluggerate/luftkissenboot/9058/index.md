---
layout: "image"
title: "Luftkissen 7"
date: "2007-02-18T20:57:36"
picture: "7.jpg"
weight: "6"
konstrukteure: 
- "Holger Howey"
fotografen:
- "Holger Howey"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/9058
imported:
- "2019"
_4images_image_id: "9058"
_4images_cat_id: "825"
_4images_user_id: "34"
_4images_image_date: "2007-02-18T20:57:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9058 -->
