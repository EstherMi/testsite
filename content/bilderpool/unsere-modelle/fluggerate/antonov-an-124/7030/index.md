---
layout: "image"
title: "AN124_45.JPG"
date: "2006-10-01T11:55:20"
picture: "AN124_45.JPG"
weight: "29"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/7030
imported:
- "2019"
_4images_image_id: "7030"
_4images_cat_id: "571"
_4images_user_id: "4"
_4images_image_date: "2006-10-01T11:55:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7030 -->
