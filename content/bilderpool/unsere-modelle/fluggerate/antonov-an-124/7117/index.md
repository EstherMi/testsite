---
layout: "image"
title: "AN124_117.JPG"
date: "2006-10-03T13:30:38"
picture: "AN124_117.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/7117
imported:
- "2019"
_4images_image_id: "7117"
_4images_cat_id: "571"
_4images_user_id: "4"
_4images_image_date: "2006-10-03T13:30:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7117 -->
Blick von oben auf die linke Landeklappe (Flugrichtung ist nach oben). Zentrales Element ist die "alte" ft-Lenkung mit der Zahnspurstange 38472, die aber hier verdeckt wird von den Lenkwürfeln und -klauen.