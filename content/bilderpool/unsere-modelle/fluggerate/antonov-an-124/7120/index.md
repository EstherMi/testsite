---
layout: "image"
title: "AN124_121.JPG"
date: "2006-10-03T13:51:17"
picture: "AN124_121.JPG"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/7120
imported:
- "2019"
_4images_image_id: "7120"
_4images_cat_id: "571"
_4images_user_id: "4"
_4images_image_date: "2006-10-03T13:51:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7120 -->
Das ist sozusagen der hintere Maschinenraum. 

Oben, halblinks: der schräg eingebaute graue Minimot betätigt die Landeklappen. Deren Antriebsachse läuft durch den BS15-Loch links unten steil nach oben bis zum Bildrand.

Mitte links: das Kardangelenk gehört zum kombinierten Antrieb von Quer- und Seitenruder. Unter dem Kardangelenk (hier aber verdeckt) sitzen drei Kegelräder, die die Querruder gegenläufig ansteuern. Die Antriebsachse wird vom Z15 in Bildmitte angetrieben und läuft dann nach rechts zum Rad45 und dem Seitenruder. Der zugehörige Motor ist ein grauer Mini-Mot, der unter dem Kardan zu erahnen ist.

Bildmitte: der schwarze M-Motor mit lila+grünem Stecker betätigt über Stufengetriebe, schwarze Schnecke (unten mitte) und Z15 (Rand unten mitte) das Alternativ-Hubgetriebe für die Heckrampe.

Rechter Bildrand, oberes Drittel: dieser graue Minimot treibt das Höhenruder über Stufengetriebe, rote Schnecke, Z15 an. Die Antriebsachse läuft durch die Spitze des Rumpfhecks auf die Unterseite der Heckflügel und treibt dort eine schwarze Schnecke m1 mit Schneckenmutter an.

Rechter Bildrand, unteres Drittel: die dunklen Striche sind die Seile, die die Heckluke öffen und schließen. Der Motor dazu befindet sich rechts außerhalb des Bildes.