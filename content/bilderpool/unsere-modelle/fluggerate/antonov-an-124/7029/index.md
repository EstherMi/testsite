---
layout: "image"
title: "AN124_43.JPG"
date: "2006-10-01T11:55:02"
picture: "AN124_43.JPG"
weight: "28"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/7029
imported:
- "2019"
_4images_image_id: "7029"
_4images_cat_id: "571"
_4images_user_id: "4"
_4images_image_date: "2006-10-01T11:55:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7029 -->
