---
layout: "image"
title: "Joystick03.JPG"
date: "2006-07-14T18:09:25"
picture: "Joystick03.JPG"
weight: "35"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/6637
imported:
- "2019"
_4images_image_id: "6637"
_4images_cat_id: "571"
_4images_user_id: "4"
_4images_image_date: "2006-07-14T18:09:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6637 -->
