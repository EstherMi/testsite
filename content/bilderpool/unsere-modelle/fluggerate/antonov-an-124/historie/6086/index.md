---
layout: "image"
title: "Erlkoenig04.JPG"
date: "2006-04-13T15:47:26"
picture: "Erlkoenig04.jpg"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/6086
imported:
- "2019"
_4images_image_id: "6086"
_4images_cat_id: "683"
_4images_user_id: "4"
_4images_image_date: "2006-04-13T15:47:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6086 -->
Die Nase wird mittlerweile per Motor geöffnet, die Rampe ist noch manuell betätigt.