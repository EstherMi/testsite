---
layout: "image"
title: "Erlkoenig02.JPG"
date: "2006-04-12T20:45:44"
picture: "Erlkoenig02.jpg"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/6080
imported:
- "2019"
_4images_image_id: "6080"
_4images_cat_id: "683"
_4images_user_id: "4"
_4images_image_date: "2006-04-12T20:45:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6080 -->
Erlkönige haben es an sich, dass sie ständig ihr Aussehen verändern. Auch dieser hier sieht längst wieder anders aus.