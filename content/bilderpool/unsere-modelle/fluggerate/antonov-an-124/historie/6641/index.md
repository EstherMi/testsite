---
layout: "image"
title: "Antonov208.JPG"
date: "2006-07-14T18:18:47"
picture: "Antonov208.jpg"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/6641
imported:
- "2019"
_4images_image_id: "6641"
_4images_cat_id: "683"
_4images_user_id: "4"
_4images_image_date: "2006-07-14T18:18:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6641 -->
Das neue Bugfahrwerk baut noch etwas flacher und ist elektrisch angetrieben. Mangels Platz für Endschalter muss hinter dem Mini-Getriebe noch eine Rutschkupplung eingebaut werden.