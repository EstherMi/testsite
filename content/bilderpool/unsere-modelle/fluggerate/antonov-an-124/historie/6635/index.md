---
layout: "image"
title: "Antonov150.JPG"
date: "2006-07-14T18:07:30"
picture: "Antonov150.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/6635
imported:
- "2019"
_4images_image_id: "6635"
_4images_cat_id: "683"
_4images_user_id: "4"
_4images_image_date: "2006-07-14T18:07:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6635 -->
Das ist der Kranschlitten, der auf einer Zahnstange entlangfährt, die von vorn bis hinten durch den Flieger verläuft. Das Hubgetriebe links ist ein Leergehäuse. Wegen der langgestreckten Bauform sind vorn und hinten Haken dran, damit man mit mindestens einem davon alle Stellen erreichen kann.