---
layout: "image"
title: "Antonov232.JPG"
date: "2006-07-14T18:31:51"
picture: "Antonov232.jpg"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/6644
imported:
- "2019"
_4images_image_id: "6644"
_4images_cat_id: "683"
_4images_user_id: "4"
_4images_image_date: "2006-07-14T18:31:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6644 -->
Ein schöner Rücken kann auch entzücken. Die Heckrampe wird per Seilzug geschlossen, die mittlere Heckluke (oben flachliegend zu sehen) per P-Zylinder geöffnet und geschlossen. Für die beiden seitlichen Heckluken ist die richtige Lösung noch nicht gefunden. Derzeit werden sie per Stahlfeder geöffnet (oben rechts erkennbar), aber überzeugen tut das nicht.