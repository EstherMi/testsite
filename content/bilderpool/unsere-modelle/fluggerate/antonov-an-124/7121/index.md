---
layout: "image"
title: "AN124_132.JPG"
date: "2006-10-03T13:52:48"
picture: "AN124_132.JPG"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/7121
imported:
- "2019"
_4images_image_id: "7121"
_4images_cat_id: "571"
_4images_user_id: "4"
_4images_image_date: "2006-10-03T13:52:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7121 -->
Das Höhenruder nebst Antrieb.