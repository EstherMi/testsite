---
layout: "image"
title: "Propeller und Hinterradantrieb des FliWaTüüts"
date: "2016-01-10T14:29:38"
picture: "dasfliwatueuetkonradfox3.jpg"
weight: "3"
konstrukteure: 
- "Konrad Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/42697
imported:
- "2019"
_4images_image_id: "42697"
_4images_cat_id: "3178"
_4images_user_id: "1126"
_4images_image_date: "2016-01-10T14:29:38"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42697 -->
Um den Stauraum (für Proviant, Schwimmwesten und Treibstoff) möglichst groß zu halten, wurden Propellerantrieb (für die Fortbewegung im Wasser) und die Hinterradlenkung (mit Servo) extra dicht gepackt.