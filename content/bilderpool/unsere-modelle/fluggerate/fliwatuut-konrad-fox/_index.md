---
layout: "overview"
title: "Das FliWaTüüt (Konrad Fox)"
date: 2019-12-17T19:44:01+01:00
legacy_id:
- categories/3178
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3178 --> 
Im Jahr 1967 erschien das Buch "Robbi, Tobbi und das FliWaTüüT" von Boy Lornsen - eine wunderschöne (Technik-) Geschichte über einen kleinen Erfinder in der dritten Volksschulklasse - und einen gleichaltrigen Roboter.
Die Geschichte wurde vom Drehbuchautor und Regisseur Armin Maiwald (bekannt aus "Die Sendung mit der Maus") für den WDR als Puppentrickfilm verfilmt und 1972 als 11(!)teilige Serie gesendet (inzwischen aufgearbeitet auf DVD erhältlich).

Als eines seiner Lieblingsbücher regte es Konrad an, das Fli(egen)Wa(sser)Tüüt mit fischertechnik nachzubauen.
Natürlich mit allen erforderlichen Schikanen: Rotor, Propeller, Frontantrieb, Lenkung, Beleuchtung und Hupe - und selbstverständlich ferngesteuert. (Auf der Convention 2015 in Dreieich war es zu bewundern.)