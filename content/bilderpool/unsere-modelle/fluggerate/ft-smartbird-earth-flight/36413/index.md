---
layout: "image"
title: "Fischertechnik-Smartbird-Earth-Flight"
date: "2013-01-05T18:08:54"
picture: "smartbirdearthflight04.jpg"
weight: "4"
konstrukteure: 
- "Peterholland"
fotografen:
- "Peterholland"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/36413
imported:
- "2019"
_4images_image_id: "36413"
_4images_cat_id: "2704"
_4images_user_id: "22"
_4images_image_date: "2013-01-05T18:08:54"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36413 -->
Met deze tragere centrale vleugelaandrijving kan je nu de vleugelverstellingen ook beter zien en volgen zoals op de BBC-serie Earth flight.