---
layout: "overview"
title: "FT-Smartbird Earth flight"
date: 2019-12-17T19:43:47+01:00
legacy_id:
- categories/2704
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2704 --> 
Ich habe die Kinematik der Silbermöwe nachgebaut in meiner "Fischertechnik-Smartbird-Earth-Flight".
Der Flügel besteht aus einem zweiteiligen Armflügelholm mit einer Achsaufnahme am Rumpfaustritt, einem Trapezgelenk, wie dies in vergrößerter Form bei Baggern vorkommt, und einem Handflügelholm.
Der Armflügel erzeugt den Auftrieb, der Handflügel nach dem Trapezgelenk den Vortrieb. 
Am Begin des Handflügels befindet sich der FT-Servomotor (mit Potmeter)  für die aktive Torsion. 

-----------------------------------------------------------------------
Festo heeft in het kader van Bionic-learning een Smartbird ontwikkeld.
Om een indruk hiervan te krijgen de volgende link :
https://www.youtube.com/watch?v=wm62SAXPB_c&list=FLvBlHQzqD-ISw8MaTccrfOQ&index=2 

tevens :
https://www.youtube.com/watch?v=Pzpv0yDdtNg 

Ik heb de aan het plafond hangende "Fischertechnik-Smartbird-Earth-Flight"  uitgerust met de oude IR-afstandsbediening (ivm vermogen) voor inschakeling van de centrale vleugelaandrijving (1), positionering van de staarthoogte (2) en naar links- en naar rechts vliegen (3).
Elke vleugel heeft een "ondervleugel" voor voldoende lift, én een FT-Servo-motor met potmeter voor verdraaiing van de "eindvleugel" ten behoeve van de voorwaartse stuwkracht.

De Robo Interface gebruik ik uitsluitend en alleen voor de bediening van de 2 FT-Servo-motoren ten behoeve van verdraaiing van de "eindvleugels".
Voor herkenning van de 0-Boven-Positie van de centrale vleugelaandrijving gebruik ik schakelaar I-1.
Voor de Positie-herkenning van de centrale vleugelaandrijving gebruik ik Pulsteller I-2.
Vanwege de potmeter-weerstandverschillen en om kapotdraaien te voorkomen, stuur ik elke FT-Servo apart aan met een max. snelheidswaarde 4. 
Om de zaak "eenvoudig" te houden laat ik elke FT-Servo volledig in de uiterste posities bewegen.
De Festo-Smartbird, die ook daadwerkelijk zelf kan vliegen, regelt ook de tussen-eindvleugel-posities afhankelijk van o.a. het toerental van de centrale vleugelaandrijving en de gewenste vlucht. 

Het Robopro-programma stuurt de 2 FT-Servo-motoren voor de verdraaiing van de "eindvleugels" aan afhankelijk van de centrale vleugelaandrijving-positie. 

- Nadat de centrale vleugel-aandrijving schakelaar I-1 (=0-boven-positie) heeft bekrachtigd, gaan na 4 pulsovergangen beide eindvleugels in de -horizontaal-hoog positie middels de servo's. 
- De centrale vleugel-aandrijving gaat ononderbroken door. Na 2 verdere pulsovergangen gaan beide eindvleugels middels de servo's in de -schuin-laag positie. 
- De centrale vleugel-aandrijving gaat ononderbroken door tot I-1 (=0-boven-positie) wederom wordt bekrachtigd en bovenstaande cyclus zich herhaald.

M'n eerste centrale vleugelaandrijving (grijs 20:1) bleek te snel om de eindvleugels middels een servo in één cylus zowel in -horizontaal-hoog als -schuin-laag te kunnen verstellen. De servo's zijn hiervoor te traag om dit bij te kunnen houden.  Middels een tragere centrale vleugelaandrijving (rood 50:1)  is door middel van een servo de eindvleugel in één cylus zowel in -horizontaal-hoog als -schuin-laag tijdig te verstellen.
Met deze tragere centrale vleugelaandrijving kan je nu de vleugelverstellingen ook beter zien en volgen zoals op de BBC-serie Eartrh flight.  Een prachtig gezicht.......

