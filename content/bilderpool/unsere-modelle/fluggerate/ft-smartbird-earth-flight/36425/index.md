---
layout: "image"
title: "Fischertechnik-Smartbird-Earth-Flight"
date: "2013-01-05T18:08:54"
picture: "smartbirdearthflight16.jpg"
weight: "16"
konstrukteure: 
- "Peterholland"
fotografen:
- "Peterholland"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/36425
imported:
- "2019"
_4images_image_id: "36425"
_4images_cat_id: "2704"
_4images_user_id: "22"
_4images_image_date: "2013-01-05T18:08:54"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36425 -->
M'n eerste centrale vleugelaandrijving (2x grijs 20:1) bleek te snel om de eindvleugels middels een servo in één cylus zowel in -horizontaal-hoog als -schuin-laag te kunnen verstellen. De servo's zijn hiervoor te traag om dit bij te kunnen houden.  Middels een tragere centrale vleugelaandrijving (2x rood 50:1)  is door middel van een servo de eindvleugel in één cylus zowel in -horizontaal-hoog als -schuin-laag tijdig te verstellen.
Met deze tragere centrale vleugelaandrijving kan je nu de vleugelverstellingen ook beter zien en volgen zoals op de BBC-serie Earth flight.  Een prachtig gezicht.......