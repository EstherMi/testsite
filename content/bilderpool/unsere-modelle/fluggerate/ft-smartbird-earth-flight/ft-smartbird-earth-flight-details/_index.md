---
layout: "overview"
title: "FT-Smartbird-Earth-flight-details"
date: 2019-12-17T19:43:47+01:00
legacy_id:
- categories/2708
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2708 --> 
Details der Kinematik meiner "Fischertechnik-Smartbird-Earth-Flight".

Der Flügel besteht aus einem zweiteiligen Armflügelholm mit einer Achsaufnahme am Rumpfaustritt, einem Trapezgelenk, wie dies in vergrößerter Form bei Baggern vorkommt, und einem Handflügelholm.
Der Armflügel erzeugt den Auftrieb, der Handflügel nach dem Trapezgelenk den Vortrieb. 
Am Begin des Handflügels befindet sich der FT-Servomotor (mit Potmeter)  für die aktive Torsion. 

