---
layout: "image"
title: "Details Fischertechnik Smartbird Earth Flight"
date: "2013-01-13T18:39:54"
picture: "smartbirdearthflightdetails07.jpg"
weight: "7"
konstrukteure: 
- "Peterholland"
fotografen:
- "Peterholland"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/36467
imported:
- "2019"
_4images_image_id: "36467"
_4images_cat_id: "2708"
_4images_user_id: "22"
_4images_image_date: "2013-01-13T18:39:54"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36467 -->
Empfehlenswert sind die Gelenkkopf mit Innengewinde M4 Stahl von Conrad 
Best.-Nr.: 216410 - 62    ( Am Bild gibt es ubrigens ein Alternativ ) 

Link naar Gelenkkopf mit Innengewinde M4 Stahl von Conrad 
Best.-Nr.: 216410 - 62 

http://www.conrad.de/ce/de/product/216410/Gelenkkopf-mit-Innengewinde-M4-Stahl    
