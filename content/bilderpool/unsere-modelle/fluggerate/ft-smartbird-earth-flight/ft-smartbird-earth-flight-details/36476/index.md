---
layout: "image"
title: "Details Fischertechnik Smartbird Earth Flight"
date: "2013-01-13T18:39:54"
picture: "smartbirdearthflightdetails16.jpg"
weight: "16"
konstrukteure: 
- "Peterholland"
fotografen:
- "Peterholland"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/36476
imported:
- "2019"
_4images_image_id: "36476"
_4images_cat_id: "2708"
_4images_user_id: "22"
_4images_image_date: "2013-01-13T18:39:54"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36476 -->
Oben