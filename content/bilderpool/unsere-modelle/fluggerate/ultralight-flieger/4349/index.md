---
layout: "image"
title: "Ultralight-Mobile - Motorsegler"
date: "2004-09-29T19:20:20"
picture: "Ultra04.jpg"
weight: "11"
konstrukteure: 
- "fishfriend"
fotografen:
- "Harald Steinhaus"
keywords: ["Mobile", "Ultralight"]
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/4349
imported:
- "2019"
_4images_image_id: "4349"
_4images_cat_id: "584"
_4images_user_id: "34"
_4images_image_date: "2004-09-29T19:20:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4349 -->
Ein Mobile aus ft-Ultralight-Fliegern. Hier der dritte von vier Teilnehmern, der Motorsegler.

Wenn das nichts für eine Minikit-Serie ist, dann weiß ich gar nichts mehr.

Gehört einfach in jedes Kinderzimmer!