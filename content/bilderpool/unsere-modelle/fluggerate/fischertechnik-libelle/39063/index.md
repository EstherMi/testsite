---
layout: "image"
title: "Zijaanzicht met Gelenkklaue 15 die draaischijf-60 verschuift voor vleugel-uitslag-verstelling"
date: "2014-07-26T20:51:26"
picture: "fischertechniklibelle23.jpg"
weight: "15"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/39063
imported:
- "2019"
_4images_image_id: "39063"
_4images_cat_id: "2921"
_4images_user_id: "22"
_4images_image_date: "2014-07-26T20:51:26"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39063 -->
Op de Schneckenmutter 35973 heb ik een Gelenkklaue 15  (38446) geschoven die in de draaischijf-60 grijpt. Hiertoe heb ik één zijde van de Gelenkklaue afgeknipt.