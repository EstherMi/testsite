---
layout: "comment"
hidden: true
title: "19282"
date: "2014-07-26T21:08:11"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Voor het inzicht zijn de volgende Festo weblinks Bionic Learning Network heel verhelderend :    

http://www.festo.com/cms/de_corp/10924.htm    

http://www.festo.com/cms/de_corp/9617.htm

http://www.festo.com/cms/de_corp/13655.htm

http://www.festo.com/net/SupportPortal/Files/248133/Festo_BionicOpter_de.pdf

http://www.festo.com/cms/de_corp/13165.htm

http://www.festo.com/rep/de_corp/assets/pdf/GEO_Infografik_Roboterlibelle.pdf

http://www.festo.com/PDF_Flip/trends_in_automation/1_2014/BE-

NL/files/assets/common/downloads/trends%20in%20automation.pdf