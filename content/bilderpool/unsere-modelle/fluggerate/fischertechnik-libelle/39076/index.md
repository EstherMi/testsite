---
layout: "image"
title: "Libelle vliegt en kijkt recht vooruit"
date: "2014-07-26T20:51:26"
picture: "fischertechniklibelle36.jpg"
weight: "28"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/39076
imported:
- "2019"
_4images_image_id: "39076"
_4images_cat_id: "2921"
_4images_user_id: "22"
_4images_image_date: "2014-07-26T20:51:26"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39076 -->
Libelle vliegt en kijkt recht vooruit met de kenmerkende grote ogen.