---
layout: "image"
title: "Uitwijking naar links waarbij de rechter vleugels een grotere uitslag maken voor extra stuwkracht."
date: "2014-07-26T20:51:26"
picture: "fischertechniklibelle38.jpg"
weight: "30"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/39078
imported:
- "2019"
_4images_image_id: "39078"
_4images_cat_id: "2921"
_4images_user_id: "22"
_4images_image_date: "2014-07-26T20:51:26"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39078 -->
Uitwijking naar links waarbij de rechter vleugels een grotere Amplitude-uitslag maken voor meer stuwkracht.
Libelle kijkt en hangt daarbij over naar links.