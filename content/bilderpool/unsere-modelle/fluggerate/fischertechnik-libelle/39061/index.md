---
layout: "image"
title: "Fischertechnik Libelle  -onderaanzicht  met 12V NiMh-Accu-pack voor meer vermogen en koppel"
date: "2014-07-26T20:51:26"
picture: "fischertechniklibelle21.jpg"
weight: "13"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/39061
imported:
- "2019"
_4images_image_id: "39061"
_4images_cat_id: "2921"
_4images_user_id: "22"
_4images_image_date: "2014-07-26T20:51:26"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39061 -->
Fischertechnik Libelle  -onderaanzicht met zelf gemaakte 12V NiMh-Accu-pack van de Aldi voor meer vermogen en koppel.