---
layout: "image"
title: "Fischertechnik Libelle in aanbouw 5"
date: "2014-07-26T20:51:26"
picture: "fischertechniklibelle15.jpg"
weight: "7"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/39055
imported:
- "2019"
_4images_image_id: "39055"
_4images_cat_id: "2921"
_4images_user_id: "22"
_4images_image_date: "2014-07-26T20:51:26"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39055 -->
Fischertechnik Libelle in aanbouw, met op de achtergrond de Fischertechniek Smartbird.
Zie:   https://www.youtube.com/watch?v=RjhEi15VK-4