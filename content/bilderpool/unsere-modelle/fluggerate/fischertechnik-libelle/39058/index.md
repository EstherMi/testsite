---
layout: "image"
title: "Centrale aandrijving 4 vleugels + vleugel-uitslag-verstelling door verschuiven draaischijf-60"
date: "2014-07-26T20:51:26"
picture: "fischertechniklibelle18.jpg"
weight: "10"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/39058
imported:
- "2019"
_4images_image_id: "39058"
_4images_cat_id: "2921"
_4images_user_id: "22"
_4images_image_date: "2014-07-26T20:51:26"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39058 -->
Midden-rechts de centrale aandrijf-Powermotor M1 (50:1) waarmee de 4 vleugels middels grote tandwielen 40  (31022) tegelijk op- en -neer kunnen bewegen.   Net als bij Festo hebben de voorvleugels een vaste fase-verschuiving ten opzichte van de achtervleugels.

Geheel links de amplitude-vleugel-uitslag-verstelling middels een S-Motor + U-Getriebe. Deze aandrijving heeft aan beide zijden een haakse overbrenging dmv een Rastkegel-tandwielen 35062 naar een zwarte worm M1 ( 35977) om een draaischijf-60  (31019)  te kunnen verstellen.