---
layout: "image"
title: "Vleugel-mallen voor 1mm dik transparant Polycarbonaat-plaat"
date: "2014-07-26T20:51:26"
picture: "fischertechniklibelle26.jpg"
weight: "18"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/39066
imported:
- "2019"
_4images_image_id: "39066"
_4images_cat_id: "2921"
_4images_user_id: "22"
_4images_image_date: "2014-07-26T20:51:26"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39066 -->
De 4 vleugels heb ik geknipt uit 1mm dik transparant Polycarbonaat-plaat. Dikker is niet goed meer knipbaar met een huishoudschaar en dunner heeft onvoldoende stijfheid.