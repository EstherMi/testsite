---
layout: "image"
title: "Fischertechnik Libelle in aanbouw 6       -onderaanzicht"
date: "2014-07-26T20:51:26"
picture: "fischertechniklibelle16.jpg"
weight: "8"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/39056
imported:
- "2019"
_4images_image_id: "39056"
_4images_cat_id: "2921"
_4images_user_id: "22"
_4images_image_date: "2014-07-26T20:51:26"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39056 -->
De amplitude-vleugel-uitslag-verstelling gebeurd per vleugel middels een S-Motor + U-Getriebe. (zie midden-links 2x) 
Deze aandrijving heeft aan beide zijden een haakse overbrenging dmv een Rastkegel-tandwielen 35062 naar een zwarte worm M1 ( 35977) om een draaischijf-60  (31019)  te kunnen verstellen.  
Op de Schneckenmutter 35973 heb ik een Gelenkklaue 15  (38446) geschoven die in de draaischijf-60 grijpt. Hiertoe heb ik één zijde van de Gelenkklaue afgeknipt.  
Voor het ompolen van de S-motoren gebruik ik 2 oude EM-5-Relais  (35793) die door M2 van de IR-afstand-bediening worden getriggerd. Voor de eindposities gebruik ik diodes.