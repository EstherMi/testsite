---
layout: "image"
title: "Amplitude- Vleugel-uitslag-verstelling + Gelenkklaue 15  die draaischijf-60 verschuift"
date: "2014-07-26T20:51:26"
picture: "fischertechniklibelle25.jpg"
weight: "17"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/39065
imported:
- "2019"
_4images_image_id: "39065"
_4images_cat_id: "2921"
_4images_user_id: "22"
_4images_image_date: "2014-07-26T20:51:26"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39065 -->
Met de vleugel-uitslag-amplitude kan de stuwkracht worden geregeld.

Op de Schneckenmutter 35973 heb ik een Gelenkklaue 15  (38446) geschoven die in de draaischijf-60 grijpt. Hiertoe heb ik één zijde van de Gelenkklaue afgeknipt.