---
layout: "image"
title: "Amplitude- Vleugel-uitslag-verstelling + 5mm Buisprofiel"
date: "2014-07-26T20:51:26"
picture: "fischertechniklibelle24.jpg"
weight: "16"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/39064
imported:
- "2019"
_4images_image_id: "39064"
_4images_cat_id: "2921"
_4images_user_id: "22"
_4images_image_date: "2014-07-26T20:51:26"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39064 -->
De "truuk" om de amplitude van de vleugel-uitslag per vleugel te kunnen verstellen gebeurd door de 4mm vleugelslag-aandrijfas (met een 40 graden hoek) vrij door te voeren via een 5mm messing-buisprofiel met een binnendiameter van 4,1 mm.  Deze is verkrijgbaar bij Conrad onder artikelnummer 297313.   
Zie :  http://www.conrad.nl/ce/nl/product/297313/Messing-Buisprofiel-5-mm-41-mm-500-mm?ref=searchDetail

De zwarte vrijloopnaaf 68535 heb ik hiervoor tot 5mm uitgeboord om het 5mm buisprofiel vast te zetten.  Een rode bouwsteen-15 met asgat (32064) heb ik iets groter uitgeboord zodat het 5mm buisprofiel hierin vrij kan draaien.  Met de vleugel-uitslag-amplitude kan de stuwkracht worden geregeld.
