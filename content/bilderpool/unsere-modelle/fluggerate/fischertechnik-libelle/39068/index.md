---
layout: "image"
title: "Verharde aderen-net-structuur voor stabiliteit van libelle-vleugel"
date: "2014-07-26T20:51:26"
picture: "fischertechniklibelle28.jpg"
weight: "20"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/39068
imported:
- "2019"
_4images_image_id: "39068"
_4images_cat_id: "2921"
_4images_user_id: "22"
_4images_image_date: "2014-07-26T20:51:26"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39068 -->
Verharde aderen-net-structuur