---
layout: "overview"
title: "Fischertechnik Libelle"
date: 2019-12-17T19:43:55+01:00
legacy_id:
- categories/2921
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2921 --> 
In de zomervakantie-2014 heb ik van Fischertechnik een Libelle met 4 onafhankelijk bewegende vleugels gemaakt. Een en ander geïnspireerd op de Festo-Bionic-Opter. 
Evenals m'n FT-smart-bird kan ik de Libelle wederom met de IR-afstand-bediening alle kanten op laten "vliegen". 


