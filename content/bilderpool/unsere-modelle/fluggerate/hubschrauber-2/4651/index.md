---
layout: "image"
title: "Haubschrauber Gesamtansicht"
date: "2005-08-26T17:09:40"
picture: "motorisierte_Roboter_075.jpg"
weight: "1"
konstrukteure: 
- "Marius Moosmann"
fotografen:
- "Marius Moosmann"
keywords: ["mari", "mari", "mari", "mari", "mari"]
uploadBy: "mari"
license: "unknown"
legacy_id:
- details/4651
imported:
- "2019"
_4images_image_id: "4651"
_4images_cat_id: "583"
_4images_user_id: "189"
_4images_image_date: "2005-08-26T17:09:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4651 -->
