---
layout: "image"
title: "flugz21.jpg"
date: "2018-01-03T19:28:44"
picture: "flugz21.jpg"
weight: "21"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/47033
imported:
- "2019"
_4images_image_id: "47033"
_4images_cat_id: "3481"
_4images_user_id: "2449"
_4images_image_date: "2018-01-03T19:28:44"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47033 -->
