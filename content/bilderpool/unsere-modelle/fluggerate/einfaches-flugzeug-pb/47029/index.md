---
layout: "image"
title: "flugz17.jpg"
date: "2018-01-03T19:28:44"
picture: "flugz17.jpg"
weight: "17"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/47029
imported:
- "2019"
_4images_image_id: "47029"
_4images_cat_id: "3481"
_4images_user_id: "2449"
_4images_image_date: "2018-01-03T19:28:44"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47029 -->
