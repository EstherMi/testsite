---
layout: "image"
title: "flugz04.jpg"
date: "2018-01-03T19:28:44"
picture: "flugz04.jpg"
weight: "4"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/47016
imported:
- "2019"
_4images_image_id: "47016"
_4images_cat_id: "3481"
_4images_user_id: "2449"
_4images_image_date: "2018-01-03T19:28:44"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47016 -->
