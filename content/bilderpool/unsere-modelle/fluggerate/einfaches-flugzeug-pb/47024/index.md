---
layout: "image"
title: "flugz12.jpg"
date: "2018-01-03T19:28:44"
picture: "flugz12.jpg"
weight: "12"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/47024
imported:
- "2019"
_4images_image_id: "47024"
_4images_cat_id: "3481"
_4images_user_id: "2449"
_4images_image_date: "2018-01-03T19:28:44"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47024 -->
