---
layout: "image"
title: "flugz15.jpg"
date: "2018-01-03T19:28:44"
picture: "flugz15.jpg"
weight: "15"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/47027
imported:
- "2019"
_4images_image_id: "47027"
_4images_cat_id: "3481"
_4images_user_id: "2449"
_4images_image_date: "2018-01-03T19:28:44"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47027 -->
