---
layout: "comment"
hidden: true
title: "24140"
date: "2018-08-14T15:22:06"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Die Konstruktion - und die Fotos - finde ich so richtig gut gelungen. Das sieht Klasse aus, und besonders gefällt mir die Kombination neuer und schon älterer Teile (die Flachplatten).

Gruß,
Stefan