---
layout: "image"
title: "Landing Gear A380 Unteransicht"
date: "2009-02-14T20:24:25"
picture: "ahauptfahrwerk2.jpg"
weight: "2"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- details/17409
imported:
- "2019"
_4images_image_id: "17409"
_4images_cat_id: "1566"
_4images_user_id: "107"
_4images_image_date: "2009-02-14T20:24:25"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17409 -->
Im Ansatz ist zu erkennen, dass ein Flugzeug im mittleren Bereich mit der Flügelwurzel (Verbindung der Tragflächen - nicht dargestellt) und den Fahrwerkskästen einen sehr stabilen Aufbau bilden ohne dem das Flugzeug nie die erforderliche Stabilität erreichen würde.