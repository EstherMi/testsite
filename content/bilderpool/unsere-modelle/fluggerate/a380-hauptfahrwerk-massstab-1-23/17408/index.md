---
layout: "image"
title: "Vorderansicht Landing Gear A380"
date: "2009-02-14T20:24:24"
picture: "ahauptfahrwerk1.jpg"
weight: "1"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- details/17408
imported:
- "2019"
_4images_image_id: "17408"
_4images_cat_id: "1566"
_4images_user_id: "107"
_4images_image_date: "2009-02-14T20:24:24"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17408 -->
In diesem Modell wollte ich mich mit dem Maßkonzept, Funktion und Kinematik des Hauptfahrwerkes A380 auseinandersetzen. Allein die Kinematik ist nicht nur sehr interessant sondern auch sehr anspruchsvoll. Im Internet finden sich nur sehr sehr wenige Informationen und Fotos. Aber die wenigen plus der im Handel erhältlichen DVD "Airbus A380  History - Technology - Testing"  haben es mir ermöglicht das Hauptfahrwerk im Maßstab 1:23 zu bauen. Gerne hätte ich die Flügelwurzel und die Tragflächen bis zur ersten Turbine mit Landklappen gebaut, aber der unvorstellbar große Zeitaufwand, Platzbedarf und die fehlenden Informationen der geometrischen Abmessungen im Flügelwurzel- und Traglächenbereich haben mich doch dazu bewogen es bei diesem Bauzustand zu belassen. Vielleicht werde ich später noch ein mal weiterarbeiten ...