---
layout: "image"
title: "Wing Landing Gear wird ausgefahren_1"
date: "2009-03-06T21:42:10"
picture: "Wing_Landing_Gear_wird_ausgefahren_1.jpg"
weight: "5"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- details/23403
imported:
- "2019"
_4images_image_id: "23403"
_4images_cat_id: "1587"
_4images_user_id: "107"
_4images_image_date: "2009-03-06T21:42:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23403 -->
Das Fahrwerk ist an dem hinteren Teil des Flügelkastens, unmittelbar unterhalb der Landeklappen angebracht. In diesem Bereich findet bereits eine deutliche Flügelprofilverjüngung statt. Der Bauraum ist an dieser Stelle deshalb extrem knapp was zwar den Modellbauer einerseits ärgert andererseit aber zu einer originalgetreuen Nachbildung der Gesamtkonstruktion geführt hat ...