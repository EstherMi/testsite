---
layout: "image"
title: "Wing Landing Gear A380 Radkasten"
date: "2009-02-14T20:24:25"
picture: "ahauptfahrwerk3.jpg"
weight: "3"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- details/17410
imported:
- "2019"
_4images_image_id: "17410"
_4images_cat_id: "1566"
_4images_user_id: "107"
_4images_image_date: "2009-02-14T20:24:25"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17410 -->
Der Radkasten ist in Wirklichkeit nicht so tief wie der des Rumpf - Fahrwerkes. Den Boden habe ich zur besseren Ansicht von oben weggelassen