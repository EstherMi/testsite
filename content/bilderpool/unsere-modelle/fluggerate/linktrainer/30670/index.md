---
layout: "image"
title: "Artificial horizon"
date: "2011-05-29T14:42:43"
picture: "artificial_horizon.jpg"
weight: "2"
konstrukteure: 
- "dutchbuilder"
fotografen:
- "dutchbuilder"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- details/30670
imported:
- "2019"
_4images_image_id: "30670"
_4images_cat_id: "2287"
_4images_user_id: "1315"
_4images_image_date: "2011-05-29T14:42:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30670 -->
Same composed as the altimeter. However, the mechanic solution is of course very simple. It is just one turning axis.