---
layout: "image"
title: "Pulsgenerator"
date: "2011-05-29T14:42:43"
picture: "pulsgenerator.jpg"
weight: "13"
konstrukteure: 
- "dutchbuilder"
fotografen:
- "dutchbuilder"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- details/30681
imported:
- "2019"
_4images_image_id: "30681"
_4images_cat_id: "2287"
_4images_user_id: "1315"
_4images_image_date: "2011-05-29T14:42:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30681 -->
A disc, made of styreen with little holes. All motors use this pulsgenerator when needed.