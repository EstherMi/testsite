---
layout: "image"
title: "Bluebox3"
date: "2011-05-29T14:42:43"
picture: "bluebox3.jpg"
weight: "7"
konstrukteure: 
- "dutchbuilder"
fotografen:
- "dutchbuilder"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- details/30675
imported:
- "2019"
_4images_image_id: "30675"
_4images_cat_id: "2287"
_4images_user_id: "1315"
_4images_image_date: "2011-05-29T14:42:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30675 -->
Another look in the cockpit.