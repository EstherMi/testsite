---
layout: "image"
title: "Destination plotter"
date: "2011-05-29T14:42:43"
picture: "destination_plotter.jpg"
weight: "11"
konstrukteure: 
- "dutchbuilder"
fotografen:
- "dutchbuilder"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- details/30679
imported:
- "2019"
_4images_image_id: "30679"
_4images_cat_id: "2287"
_4images_user_id: "1315"
_4images_image_date: "2011-05-29T14:42:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30679 -->
It is just a ordinary plotter, but the pencil is replaced by a lenslamp and illuminated the back of a carton flightmap. A photocel can be placed above a destination (by hand). When the photocel is activated by the lamp, you have reach the destination.