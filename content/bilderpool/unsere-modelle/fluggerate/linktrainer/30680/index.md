---
layout: "image"
title: "Photocell"
date: "2011-05-29T14:42:43"
picture: "destination.jpg"
weight: "12"
konstrukteure: 
- "dutchbuilder"
fotografen:
- "dutchbuilder"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- details/30680
imported:
- "2019"
_4images_image_id: "30680"
_4images_cat_id: "2287"
_4images_user_id: "1315"
_4images_image_date: "2011-05-29T14:42:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30680 -->
By starting the photocel  must be placed by hand above your destination.