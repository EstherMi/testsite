---
layout: "image"
title: "Compass"
date: "2011-05-29T14:42:43"
picture: "compass.jpg"
weight: "10"
konstrukteure: 
- "dutchbuilder"
fotografen:
- "dutchbuilder"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- details/30678
imported:
- "2019"
_4images_image_id: "30678"
_4images_cat_id: "2287"
_4images_user_id: "1315"
_4images_image_date: "2011-05-29T14:42:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30678 -->
Same composed as the other instruments. On the "glass" are with white adhesive plastic the arrows en the plane installed. The mechanic solution is only one turning axis.