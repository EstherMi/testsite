---
layout: "image"
title: "Rotorkopf07.JPG"
date: "2007-01-25T19:10:06"
picture: "Rotorkopf07.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/8703
imported:
- "2019"
_4images_image_id: "8703"
_4images_cat_id: "797"
_4images_user_id: "4"
_4images_image_date: "2007-01-25T19:10:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8703 -->
