---
layout: "image"
title: "Lichterorgel"
date: "2011-06-23T20:23:55"
picture: "lichterorgel1.jpg"
weight: "1"
konstrukteure: 
- "Tobias Horst"
fotografen:
- "Tobias Horst"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- details/30898
imported:
- "2019"
_4images_image_id: "30898"
_4images_cat_id: "2308"
_4images_user_id: "1007"
_4images_image_date: "2011-06-23T20:23:55"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30898 -->
Schaut euch mein Video dazu an: http://www.youtube.com/watch?v=CsfbLH14-LI