---
layout: "comment"
hidden: true
title: "13575"
date: "2011-02-15T23:17:58"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Mir erschließt sich zwar nicht ganz, warum Du die Töne nicht gleich mit den Tasten auslöst - allerdings wäre das Modell dann nicht halb so innovativ und pfiffig. Der "hintere Teil" Deines Klaviers ergäbe mit einem Abstandssensor statt des Fotowiderstands auch eine tolle Einparkhilfe...
Die Idee hat offenbar noch mächtig Potential.
Gruß, Dirk