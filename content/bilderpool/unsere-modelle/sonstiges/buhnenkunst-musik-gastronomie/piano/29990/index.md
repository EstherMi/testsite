---
layout: "image"
title: "Piano (Draufsicht)"
date: "2011-02-15T21:14:52"
picture: "piano4.jpg"
weight: "4"
konstrukteure: 
- "uhen"
fotografen:
- "uhen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- details/29990
imported:
- "2019"
_4images_image_id: "29990"
_4images_cat_id: "2211"
_4images_user_id: "1112"
_4images_image_date: "2011-02-15T21:14:52"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29990 -->
Funktionsbeschreibung:

Durch jede der 7 Tasten wird die Lampe an die jeweilige bestimmte Stelle gefahren (quasi wie eine Aufzugssteuerung) und dadurch ändert sich der Widerstandswert des Fotowiederstandes.  Nachdem die Lampe ihre Position erreicht hat, wird der Ton freigegeben. Das erfolgt über einen Magnet mit Reedkontakt. Die Elektronik stammt aus dem 80er Elektronikbaukasten und ist die Melo-Tron Schaltung. Wenn man nun ein Lied spielt, hat man also eine leichte Verzögerung vom Drücken der Taste bis zur Tonwiedergabe. Aber der Encodermotor ist verdammt schnell beim Erreichen der Position. Die Ansteuerung erfolgt über den TX-Controller mit Robopro programiert.