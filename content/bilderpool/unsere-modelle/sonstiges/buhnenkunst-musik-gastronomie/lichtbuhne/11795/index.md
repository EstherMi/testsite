---
layout: "image"
title: "Insel von vorne"
date: "2007-09-17T19:31:58"
picture: "lichtbuehne14.jpg"
weight: "14"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/11795
imported:
- "2019"
_4images_image_id: "11795"
_4images_cat_id: "1055"
_4images_user_id: "445"
_4images_image_date: "2007-09-17T19:31:58"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11795 -->
Hier ist ein Blitzer, Ein Richtscheinwerfer, ein beleuchtungs Scheinwerfer für die Discokugel.