---
layout: "image"
title: "Fischertechnik Vogelstimme"
date: "2013-03-16T10:56:51"
picture: "fiischertechnikvogelstimme3.jpg"
weight: "3"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/36766
imported:
- "2019"
_4images_image_id: "36766"
_4images_cat_id: "2726"
_4images_user_id: "968"
_4images_image_date: "2013-03-16T10:56:51"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36766 -->
Die Strebenmechanik bewegt in der Pfeife einen Stöpsel auf und ab
und verändert so die Tonhöhe.
Durch das ändern der Geschwindikeit ergeben sich viele
Vogelpiepsähnliche Effekte.