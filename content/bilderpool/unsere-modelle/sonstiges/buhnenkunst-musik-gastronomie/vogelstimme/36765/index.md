---
layout: "image"
title: "Fischertechnik Vogelstimme"
date: "2013-03-16T10:56:51"
picture: "fiischertechnikvogelstimme2.jpg"
weight: "2"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/36765
imported:
- "2019"
_4images_image_id: "36765"
_4images_cat_id: "2726"
_4images_user_id: "968"
_4images_image_date: "2013-03-16T10:56:51"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36765 -->
Das silberne ist eine kleine Orgepfeife.
