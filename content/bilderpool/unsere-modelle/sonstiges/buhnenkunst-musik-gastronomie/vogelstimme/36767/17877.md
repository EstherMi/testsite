---
layout: "comment"
hidden: true
title: "17877"
date: "2013-03-16T11:34:12"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
*Das* ist doch mal eine tolle Modellidee! Und nichts ist unnötig kompliziert oder effekthascherisch gebaut. Große Klasse!

Gruß,
Stefan