---
layout: "image"
title: "Fischertechnik Vogelstimme"
date: "2013-03-16T10:56:51"
picture: "fiischertechnikvogelstimme4.jpg"
weight: "4"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/36767
imported:
- "2019"
_4images_image_id: "36767"
_4images_cat_id: "2726"
_4images_user_id: "968"
_4images_image_date: "2013-03-16T10:56:51"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36767 -->
Der Motor treibt einen kleinen keilförmigen Schöpfbalg
an der den stossweisen Wind für die Pfeife erzeugt.
