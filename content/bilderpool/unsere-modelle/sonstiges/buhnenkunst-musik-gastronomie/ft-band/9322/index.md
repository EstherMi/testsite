---
layout: "image"
title: "Schlagzeuger"
date: "2007-03-06T18:15:50"
picture: "ftband18.jpg"
weight: "18"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/9322
imported:
- "2019"
_4images_image_id: "9322"
_4images_cat_id: "859"
_4images_user_id: "445"
_4images_image_date: "2007-03-06T18:15:50"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9322 -->
Mit zwei 30-er Klippachsen als Knebel.