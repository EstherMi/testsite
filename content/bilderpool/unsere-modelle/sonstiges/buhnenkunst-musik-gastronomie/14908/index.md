---
layout: "image"
title: "Keulen"
date: "2008-07-16T20:44:28"
picture: "ftKeulen.jpg"
weight: "6"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: ["Jonglierkeulen", "Keulen", "Clubs"]
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- details/14908
imported:
- "2019"
_4images_image_id: "14908"
_4images_cat_id: "2121"
_4images_user_id: "381"
_4images_image_date: "2008-07-16T20:44:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14908 -->
Diese Dinger nennt man Keulen und sie fliegen wirklich, wenn sie von jemandem bedient werden der das kann.