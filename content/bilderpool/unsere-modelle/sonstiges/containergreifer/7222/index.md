---
layout: "image"
title: "containergreifer07.jpg"
date: "2006-10-23T22:29:37"
picture: "containergreifer07.jpg"
weight: "8"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/7222
imported:
- "2019"
_4images_image_id: "7222"
_4images_cat_id: "693"
_4images_user_id: "5"
_4images_image_date: "2006-10-23T22:29:37"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7222 -->
