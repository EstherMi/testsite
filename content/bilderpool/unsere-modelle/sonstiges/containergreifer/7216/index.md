---
layout: "image"
title: "containergreifer01.jpg"
date: "2006-10-23T22:29:37"
picture: "containergreifer01.jpg"
weight: "2"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/7216
imported:
- "2019"
_4images_image_id: "7216"
_4images_cat_id: "693"
_4images_user_id: "5"
_4images_image_date: "2006-10-23T22:29:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7216 -->
