---
layout: "image"
title: "containergreifer02.jpg"
date: "2006-10-23T22:29:37"
picture: "containergreifer02.jpg"
weight: "3"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/7217
imported:
- "2019"
_4images_image_id: "7217"
_4images_cat_id: "693"
_4images_user_id: "5"
_4images_image_date: "2006-10-23T22:29:37"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7217 -->
