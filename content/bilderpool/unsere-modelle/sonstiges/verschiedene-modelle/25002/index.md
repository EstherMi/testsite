---
layout: "image"
title: "Mein ROBO-PRO-Modell"
date: "2009-09-20T19:18:08"
picture: "verschiedenemodelle7.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/25002
imported:
- "2019"
_4images_image_id: "25002"
_4images_cat_id: "1768"
_4images_user_id: "374"
_4images_image_date: "2009-09-20T19:18:08"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25002 -->
Die Frontansicht von dem ROBO-PRO