---
layout: "image"
title: "ohne Hülle"
date: "2007-02-11T15:18:39"
picture: "bettklimaanlage08.jpg"
weight: "8"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/8952
imported:
- "2019"
_4images_image_id: "8952"
_4images_cat_id: "812"
_4images_user_id: "445"
_4images_image_date: "2007-02-11T15:18:39"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8952 -->
von hinten