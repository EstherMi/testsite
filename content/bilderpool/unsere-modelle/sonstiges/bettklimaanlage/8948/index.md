---
layout: "image"
title: "Klimaanlage mit Haube"
date: "2007-02-11T15:18:39"
picture: "bettklimaanlage04.jpg"
weight: "4"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/8948
imported:
- "2019"
_4images_image_id: "8948"
_4images_cat_id: "812"
_4images_user_id: "445"
_4images_image_date: "2007-02-11T15:18:39"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8948 -->
aus Papier