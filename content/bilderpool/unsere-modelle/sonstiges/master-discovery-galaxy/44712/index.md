---
layout: "image"
title: "Discovery"
date: "2016-10-30T20:05:42"
picture: "DSC00424_sc01.jpg"
weight: "3"
konstrukteure: 
- "Peter Habermehl"
fotografen:
- "Peter Habermehl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- details/44712
imported:
- "2019"
_4images_image_id: "44712"
_4images_cat_id: "3330"
_4images_user_id: "2488"
_4images_image_date: "2016-10-30T20:05:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44712 -->
