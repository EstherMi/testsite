---
layout: "image"
title: "E-Tec Fahrstuhl"
date: "2015-12-27T19:27:40"
picture: "100_1550.jpg"
weight: "1"
konstrukteure: 
- "Fischertechnik"
fotografen:
- "Bernd Langer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- details/42615
imported:
- "2019"
_4images_image_id: "42615"
_4images_cat_id: "3167"
_4images_user_id: "2496"
_4images_image_date: "2015-12-27T19:27:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42615 -->
Fahrstuhl aus der Anleitung Digitale Funktionen des E-Tec Modul.