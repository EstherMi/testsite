---
layout: "overview"
title: "Kamera mit Pneumatik Einschalter & Auslöser"
date: 2019-12-17T19:38:07+01:00
legacy_id:
- categories/2743
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2743 --> 
Die Kamera wird über einen Pneumatik Zylinder eingeschaltet und über einen anderen Zylinder wir der Auslöser betätigt.
Für ein Fischertechnik-Gewächshaus wollte ich periodische Fotoaufnahmen tätigen (Zeitraffer). Anfangs machte ich mir Gedanken über einen SD-Kartenadapter auf USB.
Damit wollte ich die Fotos von der Kamera direkt auf dem PC Speichern. Um das Ganze mit unterbrechungsfreien Stromversorgung auszustatten hatte ich schon ein Akku-Gehäuse umfunktioniert.
Allerding war der Geistesblitz noch viel einfacher zu realisieren.
Das Gewächshaus wird per RoboTX Controller gesteuert. Da liegt es doch nah, auch die Steuerung der Kamera  per TX zu erledigen. Der Akku hält lange, lange, lange genung durch, da nach jedem Foto die Kamera wieder per Pneumatik ausgeschaltet wird.