---
layout: "image"
title: "Pneumatische Kamerasteuerrung - hinten"
date: "2013-05-14T21:23:14"
picture: "kameramitpneumatikeinschalterausloeser2.jpg"
weight: "2"
konstrukteure: 
- "Kai Baumgart"
fotografen:
- "Kai"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- details/36917
imported:
- "2019"
_4images_image_id: "36917"
_4images_cat_id: "2743"
_4images_user_id: "1677"
_4images_image_date: "2013-05-14T21:23:14"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36917 -->
