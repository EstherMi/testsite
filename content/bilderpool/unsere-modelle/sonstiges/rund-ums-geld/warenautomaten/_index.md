---
layout: "overview"
title: "Warenautomaten"
date: 2019-12-17T19:35:40+01:00
legacy_id:
- categories/2009
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2009 --> 
Geräte, die gegen Geldeinwurf eine Ware ausgeben.