---
layout: "image"
title: "Innenansicht(2)"
date: "2011-09-09T07:41:49"
picture: "bigdswarenautomat06.jpg"
weight: "6"
konstrukteure: 
- "big-d"
fotografen:
- "big-d"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "da-kid"
license: "unknown"
legacy_id:
- details/31760
imported:
- "2019"
_4images_image_id: "31760"
_4images_cat_id: "2369"
_4images_user_id: "1169"
_4images_image_date: "2011-09-09T07:41:49"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31760 -->
Das ist der Maoam-Spender.