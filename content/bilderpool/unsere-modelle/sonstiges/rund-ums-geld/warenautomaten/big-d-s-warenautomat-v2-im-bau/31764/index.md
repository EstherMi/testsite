---
layout: "image"
title: "kleines Aufsteller-Arsenal"
date: "2011-09-09T07:41:49"
picture: "bigdswarenautomat10.jpg"
weight: "10"
konstrukteure: 
- "big-d"
fotografen:
- "big-d"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "da-kid"
license: "unknown"
legacy_id:
- details/31764
imported:
- "2019"
_4images_image_id: "31764"
_4images_cat_id: "2369"
_4images_user_id: "1169"
_4images_image_date: "2011-09-09T07:41:49"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31764 -->
oben: abschliesbare Klappe, für z.B. Werkzeug
unten:Ein-ausschalter für den Verkauf (Alarm bleibt dann aktiv), ist aber nicht genutzt.
