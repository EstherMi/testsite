---
layout: "image"
title: "Ready to buy"
date: "2015-11-07T18:07:35"
picture: "DSC08438_sc01.jpg"
weight: "2"
konstrukteure: 
- "Peter Habermehl"
fotografen:
- "Peter Habermehl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- details/42296
imported:
- "2019"
_4images_image_id: "42296"
_4images_cat_id: "3141"
_4images_user_id: "2488"
_4images_image_date: "2015-11-07T18:07:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42296 -->
Nach erfolgreich gescannter QR-Karte