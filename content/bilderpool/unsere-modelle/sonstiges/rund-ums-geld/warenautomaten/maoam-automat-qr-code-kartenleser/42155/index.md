---
layout: "image"
title: "QR-Code-Karten"
date: "2015-10-26T17:17:22"
picture: "DSC08339_1.jpg"
weight: "9"
konstrukteure: 
- "Peter Habermehl"
fotografen:
- "Peter Habermehl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- details/42155
imported:
- "2019"
_4images_image_id: "42155"
_4images_cat_id: "3141"
_4images_user_id: "2488"
_4images_image_date: "2015-10-26T17:17:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42155 -->
Links die "Credit-"Karten, rechts Admin-Karten zur Verwaltung.

Alle Funktionen, die der Administrator ausführen muß, werden ebenfalls durch QR-Karten gesteuert, so z.B. das Aufladen einzelner Kundenkarten, das An- und Abmelden von Kundenkarten und sogar das Ausschalten des Automaten.

Mehr Infos im Forum: http://forum.ftcommunity.de/viewtopic.php?f=6&t=3168