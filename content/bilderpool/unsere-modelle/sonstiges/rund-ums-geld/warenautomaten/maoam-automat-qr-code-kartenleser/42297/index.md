---
layout: "image"
title: "Gesamtansicht von schräg oben, mit Gehäuse"
date: "2015-11-07T18:07:35"
picture: "DSC08440_sc01.jpg"
weight: "3"
konstrukteure: 
- "Peter Habermehl"
fotografen:
- "Peter Habermehl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- details/42297
imported:
- "2019"
_4images_image_id: "42297"
_4images_cat_id: "3141"
_4images_user_id: "2488"
_4images_image_date: "2015-11-07T18:07:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42297 -->
