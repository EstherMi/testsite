---
layout: "overview"
title: "Maoam-Automat mit QR-Code-Kartenleser"
date: 2019-12-17T19:35:47+01:00
legacy_id:
- categories/3141
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3141 --> 
Ein Maoam-Warenautomat, bei dem QR-Code-Karten als Bezahlsystem eingesetzt werden.