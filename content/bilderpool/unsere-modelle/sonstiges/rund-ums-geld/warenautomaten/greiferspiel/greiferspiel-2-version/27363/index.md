---
layout: "image"
title: "Greiferspiel 2.Version Bedienung"
date: "2010-06-04T10:54:31"
picture: "greiferspielversion7.jpg"
weight: "7"
konstrukteure: 
- "Udo Henkel"
fotografen:
- "Udo Henkel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- details/27363
imported:
- "2019"
_4images_image_id: "27363"
_4images_cat_id: "1965"
_4images_user_id: "1112"
_4images_image_date: "2010-06-04T10:54:31"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27363 -->
Ich habe hier wieder das alte Robo-Interface (gut getarnt an der Rückseite des Spiels) eingesetzt, weil dieses ja noch eine IR-Schnittstelle hat und man so bequem über die Fernbedienung den Greifer steuern kann. Natürlich kommt hier eine Ablaufsteuerung zum tragen. Nachdem man die Umschaltaste "1" gedrück hat, senkt sich der Greifer, greift zu oder ins leere, fährt wieder nach oben und zum Ausgangspunkt zurück, wo der Greifer dann automatisch wieder geöffnet wird. Diesmal habe ich alles mit Robopro geschrieben.