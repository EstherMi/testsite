---
layout: "image"
title: "Greiferspiel 2.Version Antrieb 2"
date: "2010-06-04T10:54:31"
picture: "greiferspielversion5.jpg"
weight: "5"
konstrukteure: 
- "Udo Henkel"
fotografen:
- "Udo Henkel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- details/27361
imported:
- "2019"
_4images_image_id: "27361"
_4images_cat_id: "1965"
_4images_user_id: "1112"
_4images_image_date: "2010-06-04T10:54:31"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27361 -->
