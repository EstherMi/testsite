---
layout: "image"
title: "Greiferspiel 2.Version Greifer"
date: "2010-06-04T10:54:31"
picture: "greiferspielversion3.jpg"
weight: "3"
konstrukteure: 
- "Udo Henkel"
fotografen:
- "Udo Henkel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- details/27359
imported:
- "2019"
_4images_image_id: "27359"
_4images_cat_id: "1965"
_4images_user_id: "1112"
_4images_image_date: "2010-06-04T10:54:31"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27359 -->
Der Kolben ist über einen Bindfaden mit den drei Streben der Greifarme verbunden.Unter Druckluft sind die Greifarme geöffnet. Wenn die Druckluft weg fällt, werden die Greifarme nicht nur durch die Kolbenfeder zurückgezogen, sonder auch noch durch kleine Gummis an den Würfeln. Es handelt sich hier bei um Zahnspangen Gummis von meinem Sohn. Diese sind sehr klein, passen genau und haben die richtige Spannung.