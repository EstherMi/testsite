---
layout: "overview"
title: "Greiferspiel 2.Version"
date: 2019-12-17T19:35:40+01:00
legacy_id:
- categories/1965
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1965 --> 
Hier ist eine Neuauflage meines Greiferspiels. Diesmal etwas kompakter und "ordentlicher".
Dafür war die Bauzeit auch sehr viel länger!