---
layout: "image"
title: "Greiferspiel Mechanik 1"
date: "2010-04-07T12:40:32"
picture: "greiferspiel2.jpg"
weight: "2"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- details/26890
imported:
- "2019"
_4images_image_id: "26890"
_4images_cat_id: "1927"
_4images_user_id: "1112"
_4images_image_date: "2010-04-07T12:40:32"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26890 -->
