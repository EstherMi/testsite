---
layout: "image"
title: "Greiferspiel (Greifer)"
date: "2013-01-06T18:56:58"
picture: "greiferspielversion3.jpg"
weight: "3"
konstrukteure: 
- "Udo Henkel"
fotografen:
- "Udo Henkel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- details/36445
imported:
- "2019"
_4images_image_id: "36445"
_4images_cat_id: "2706"
_4images_user_id: "1112"
_4images_image_date: "2013-01-06T18:56:58"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36445 -->
Hier ist der Greifer geschlossen.