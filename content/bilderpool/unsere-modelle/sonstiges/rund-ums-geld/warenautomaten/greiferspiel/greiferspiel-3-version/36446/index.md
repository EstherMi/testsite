---
layout: "image"
title: "Greiferspiel (Joystick)"
date: "2013-01-06T18:56:58"
picture: "greiferspielversion4.jpg"
weight: "4"
konstrukteure: 
- "Udo Henkel"
fotografen:
- "Udo Henkel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- details/36446
imported:
- "2019"
_4images_image_id: "36446"
_4images_cat_id: "2706"
_4images_user_id: "1112"
_4images_image_date: "2013-01-06T18:56:58"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36446 -->
Hier ist der Joystick, um den Greifer zu steuern.