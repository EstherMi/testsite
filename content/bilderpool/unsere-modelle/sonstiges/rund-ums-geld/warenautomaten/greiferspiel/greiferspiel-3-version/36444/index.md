---
layout: "image"
title: "Greiferspiel (Greifer)"
date: "2013-01-06T18:56:58"
picture: "greiferspielversion2.jpg"
weight: "2"
konstrukteure: 
- "Udo Henkel"
fotografen:
- "Udo Henkel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- details/36444
imported:
- "2019"
_4images_image_id: "36444"
_4images_cat_id: "2706"
_4images_user_id: "1112"
_4images_image_date: "2013-01-06T18:56:58"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36444 -->
Um die perfekte Greiferfunktionslänge zu bekommen ist die Radnabe unter dem Rad, so dass der Zylinder noch in das Loch des Rades kommt. Die Radnabe wird von den Zahnscheiben gehalten.