---
layout: "image"
title: "Geldrückgabe"
date: "2011-10-19T16:49:06"
picture: "DSCF7749.jpg"
weight: "7"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "majus"
license: "unknown"
legacy_id:
- details/33238
imported:
- "2019"
_4images_image_id: "33238"
_4images_cat_id: "2460"
_4images_user_id: "1239"
_4images_image_date: "2011-10-19T16:49:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33238 -->
Die Geldrückgabe wurde mit Hilfe des Forums gelöst. http://forum.ftcommunity.de/viewtopic.php?f=15&t=752
Der orangene Pfeil zeigt auf den unteren Gummi (Kettenglied).
So wird nur ein Schein angezogen.