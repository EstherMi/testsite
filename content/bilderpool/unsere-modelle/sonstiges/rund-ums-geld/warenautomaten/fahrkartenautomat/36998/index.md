---
layout: "image"
title: "Detailansichten"
date: "2013-05-27T15:45:22"
picture: "Automat3.jpg"
weight: "3"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "majus"
license: "unknown"
legacy_id:
- details/36998
imported:
- "2019"
_4images_image_id: "36998"
_4images_cat_id: "2460"
_4images_user_id: "1239"
_4images_image_date: "2013-05-27T15:45:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36998 -->
Oben links: Geldeinzug mit Schummelkontrolle (spuckt falsche Scheine wieder aus, dann leuchtet rote LED) Das grüne Licht hat keinen Sinn, das soll nur grün leuchten wie in echt.
Oben rechts: Münzsortierer nach gleichem Vorbild wie beim ersten Automat. Hier links 1€, Mitte 50 ct (werden aussortiert und zurückgegeben) und rechts 2€
Unten links: Anzeige, was akzeptiert wird (dahinter sind mehrfarbige LEDs)
Unten rechts: Ticketspender mit Druckfeder hinten für genauere Ausgabe.