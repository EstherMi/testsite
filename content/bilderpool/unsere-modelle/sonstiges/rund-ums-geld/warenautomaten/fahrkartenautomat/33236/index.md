---
layout: "image"
title: "Chaos hinter den Kulissen"
date: "2011-10-19T16:49:06"
picture: "DSCF7755.jpg"
weight: "5"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "majus"
license: "unknown"
legacy_id:
- details/33236
imported:
- "2019"
_4images_image_id: "33236"
_4images_cat_id: "2460"
_4images_user_id: "1239"
_4images_image_date: "2011-10-19T16:49:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33236 -->
Nun eine Aufnahme meines Gerätes von hinten. Man sieht dort ganz links die Münzkontrolle...(Sie stammt aus der Idee des Geldzählers von Mirose 
( http://www.ftcommunity.de/details.php?image_id=16339 ))
Etwas weiter rechts finden sich Bildschirm von hinten, sowie die Münzrückgabe.(1€ Münzen)
Etwas weiter unten sind die Tickets und weiter links die Geldscheinannahme.