---
layout: "image"
title: "Geldprüfer"
date: "2011-10-19T16:49:06"
picture: "DSCF7750.jpg"
weight: "8"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "majus"
license: "unknown"
legacy_id:
- details/33239
imported:
- "2019"
_4images_image_id: "33239"
_4images_cat_id: "2460"
_4images_user_id: "1239"
_4images_image_date: "2011-10-19T16:49:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33239 -->
Der nach dem Vorbild von Mirose entstandene Geldprüfer. Unzulässig große Münzen fallen gleich durch den ersten Spalt, 50ct durch den zweiten und die 2 Euro Münzen fallen komplett hinten durch. Die Zylinder drücken die Münzen dann nur noch durch einen Taster, da eine Münze alleine zu leicht dafür wäre.