---
layout: "image"
title: "Geldspeicher 3"
date: "2012-09-01T11:10:54"
picture: "DSC02439.jpg"
weight: "3"
konstrukteure: 
- "robbi2011"
fotografen:
- "robbi2011"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "robbi2011"
license: "unknown"
legacy_id:
- details/35416
imported:
- "2019"
_4images_image_id: "35416"
_4images_cat_id: "2628"
_4images_user_id: "1442"
_4images_image_date: "2012-09-01T11:10:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35416 -->
Bei offener Tür links der erste Teil der Lichtschranke