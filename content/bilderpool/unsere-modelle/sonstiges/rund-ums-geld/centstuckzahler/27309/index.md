---
layout: "image"
title: "Großansicht"
date: "2010-05-27T12:28:06"
picture: "IMG_9078kl.jpg"
weight: "1"
konstrukteure: 
- "B.-M. Klug"
fotografen:
- "B.-M. Klug"
keywords: ["Geldzähler", "Cent", "Münzzähler"]
uploadBy: "Birne"
license: "unknown"
legacy_id:
- details/27309
imported:
- "2019"
_4images_image_id: "27309"
_4images_cat_id: "1961"
_4images_user_id: "1142"
_4images_image_date: "2010-05-27T12:28:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27309 -->
Draufsicht auf den Centstückzähler. Im Hintergrund die Flasche mit den Centstücken. Zur einfacheren Handhabung habe ich die Centstücke vor dem Zählen in Plastikbecher abgefüllt.
Oben ist der Rüttler zur Vereinzelung der Centstücke zu sehen. Links über der Auffangschale kann man die Lichtschranke zur Zählung erkennen. Die beiden Taster rechts neben dem TX-Controller dienen dem Ein-, bzw. Ausschalten des Rüttlers, sowie der Lampe zur Beleuchtung des TX-Displays.
Es waren übrigens 48,82€ in der Flasche.