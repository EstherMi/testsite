---
layout: "image"
title: "Detailansicht Lichtschranke"
date: "2010-05-27T12:28:06"
picture: "IMG_9064kl.jpg"
weight: "3"
konstrukteure: 
- "B.-M. Klug"
fotografen:
- "B.-M. Klug"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Birne"
license: "unknown"
legacy_id:
- details/27311
imported:
- "2019"
_4images_image_id: "27311"
_4images_cat_id: "1961"
_4images_user_id: "1142"
_4images_image_date: "2010-05-27T12:28:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27311 -->
Hier die Lichtschranke die zur Zählung der Centstücke dient. Auch kurz nacheinander hindurchfallende Münzen werden korrekt gezählt.