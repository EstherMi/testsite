---
layout: "image"
title: "Die Bankkarten"
date: "2014-07-29T17:26:54"
picture: "Bild3.jpg"
weight: "3"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "majus"
license: "unknown"
legacy_id:
- details/39100
imported:
- "2019"
_4images_image_id: "39100"
_4images_cat_id: "2923"
_4images_user_id: "1239"
_4images_image_date: "2014-07-29T17:26:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39100 -->
Hier sieht man die selbstgebastelten Bankkarten. 
Sie haben einen Kern aus Pappe, auf dem eine dünne Schicht Aluminium aufgeklebt ist. Als Deckschicht ist vorne und hinten nochmal Papier aufgeklebt. Der lange Alustreifen auf der Rückseite wird immer unter Spannung gesetzt und die beiden Kontakte auf der Vorderseite sind je nach Karte mit der Rückseite elektrisch leitend verbunden.
Hierdurch wird die Karte vom Automat eindeutig einem Benutzer zugeordnet.

Ein Video des Automaten gibt&#180;s hier: http://youtu.be/_ULOComTNBo