---
layout: "image"
title: "07 Münzbehälter"
date: "2012-04-02T19:39:17"
picture: "geldwechsler7.jpg"
weight: "7"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/34748
imported:
- "2019"
_4images_image_id: "34748"
_4images_cat_id: "2567"
_4images_user_id: "860"
_4images_image_date: "2012-04-02T19:39:17"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34748 -->
In diesem Turm befinden sich die Münzen. 

Momentan passen 70 Münzen rein, er ist aber nach oben hin beliebig erweiterbar.