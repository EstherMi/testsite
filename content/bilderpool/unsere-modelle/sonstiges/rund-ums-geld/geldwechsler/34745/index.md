---
layout: "image"
title: "04 Ausgabe"
date: "2012-04-02T19:39:17"
picture: "geldwechsler4.jpg"
weight: "4"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/34745
imported:
- "2019"
_4images_image_id: "34745"
_4images_cat_id: "2567"
_4images_user_id: "860"
_4images_image_date: "2012-04-02T19:39:17"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34745 -->
die unterste Münze wird vom Schieber nach vorne rausgedückt