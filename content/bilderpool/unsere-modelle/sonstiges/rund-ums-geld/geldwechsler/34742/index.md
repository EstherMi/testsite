---
layout: "image"
title: "01 Geldwechselmaschine"
date: "2012-04-02T19:39:17"
picture: "geldwechsler1.jpg"
weight: "1"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/34742
imported:
- "2019"
_4images_image_id: "34742"
_4images_cat_id: "2567"
_4images_user_id: "860"
_4images_image_date: "2012-04-02T19:39:17"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34742 -->
Diese Maschine wechselt 20ct, 50ct, 1€ und 2€ Münzen in 10ct Münzen

Links ist der Geldeinwurf, rechts die Ausgabe

ein Video gibt es hier: http://www.youtube.com/watch?v=MIxV89TKvLc