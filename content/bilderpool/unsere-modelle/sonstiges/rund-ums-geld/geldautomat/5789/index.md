---
layout: "image"
title: "Geldautomat"
date: "2006-02-24T20:15:50"
picture: "Wale_006.jpg"
weight: "3"
konstrukteure: 
- "Jakob Wolf"
fotografen:
- "Jakob Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FFBBII"
license: "unknown"
legacy_id:
- details/5789
imported:
- "2019"
_4images_image_id: "5789"
_4images_cat_id: "496"
_4images_user_id: "420"
_4images_image_date: "2006-02-24T20:15:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5789 -->
Man wählt an den Schiebeschaltern wie 
viel Geld der Automat ausgeben soll (1 oder 2 Euro).  Danach schiebt man die
Karte in den Schlitz. Nun gibt der Geldautomat den gewählten Geldetrag aus.