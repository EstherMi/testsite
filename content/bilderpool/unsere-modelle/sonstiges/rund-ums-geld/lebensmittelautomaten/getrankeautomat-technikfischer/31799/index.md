---
layout: "image"
title: "Mixer"
date: "2011-09-14T19:23:07"
picture: "technikfischer06.jpg"
weight: "6"
konstrukteure: 
- "technikfischer"
fotografen:
- "technikfischer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- details/31799
imported:
- "2019"
_4images_image_id: "31799"
_4images_cat_id: "2373"
_4images_user_id: "1218"
_4images_image_date: "2011-09-14T19:23:07"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31799 -->
Hier wird Milch und Kakao verrührt (der Mixaufstatz ist nicht dran, kommt an die Nabe des sichtbaren Motors)