---
layout: "image"
title: "Wasserspender"
date: "2010-08-28T16:04:39"
picture: "wasserspender2.jpg"
weight: "2"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/28015
imported:
- "2019"
_4images_image_id: "28015"
_4images_cat_id: "2031"
_4images_user_id: "1162"
_4images_image_date: "2010-08-28T16:04:39"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28015 -->
Hier könnt ihr die Becherzufuhr sehen. Man stellt den BEcher einfach unter die Schläuche.