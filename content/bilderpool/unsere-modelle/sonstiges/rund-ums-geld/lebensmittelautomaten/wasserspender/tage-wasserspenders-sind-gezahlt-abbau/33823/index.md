---
layout: "image"
title: "Die Tage des Wasserspenders sind gezählt"
date: "2011-12-31T13:13:32"
picture: "wasserspender2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/33823
imported:
- "2019"
_4images_image_id: "33823"
_4images_cat_id: "2500"
_4images_user_id: "1162"
_4images_image_date: "2011-12-31T13:13:32"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33823 -->
Aus einem anderen Blickwinkel.