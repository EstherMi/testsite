---
layout: "image"
title: "Wasserspender"
date: "2010-08-28T14:44:03"
picture: "wasserspendervom1.jpg"
weight: "1"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/28011
imported:
- "2019"
_4images_image_id: "28011"
_4images_cat_id: "2030"
_4images_user_id: "1162"
_4images_image_date: "2010-08-28T14:44:03"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28011 -->
Hier könnt ihr meine erste Version (vom 23.01.2010) vom Wasserspender man sieht noch die sehr einfache Bauweise und die "Pumpe" rechts an der Seite, mit der man den Tank mit Wasser füllen kann. Und das die Kompressoreinhaeit noch nicht "eingepackt" ist. Man muss den Becher auch noch von Hand under die Schläuche schieben. Auch sind drei Schläuche angebracht. 

Weitere Bilder von zwei weiteren Version folgen noch.