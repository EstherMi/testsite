---
layout: "image"
title: "Ventilsteuerung"
date: "2010-09-28T16:46:05"
picture: "ag09.jpg"
weight: "9"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/28694
imported:
- "2019"
_4images_image_id: "28694"
_4images_cat_id: "2079"
_4images_user_id: "1162"
_4images_image_date: "2010-09-28T16:46:05"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28694 -->
Hier könnt ihr das blaue Handventil sehen, das über den Mini-Motor angesteuert wird. Je nach dem von welcher Flasche das Wasser kommen soll bewegt sich das Ventil in die jeweilige Richtung, der Endtaster ist dazu da, die Mittelstellung von Ventil abzufragen. 
Im Hintergrund kann man noch den Kompressor sehen, aber dazu später mehr.