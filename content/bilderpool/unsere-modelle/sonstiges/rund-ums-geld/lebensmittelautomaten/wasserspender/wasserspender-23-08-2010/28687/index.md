---
layout: "image"
title: "Wasserspender"
date: "2010-09-28T16:46:04"
picture: "ag02.jpg"
weight: "2"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/28687
imported:
- "2019"
_4images_image_id: "28687"
_4images_cat_id: "2079"
_4images_user_id: "1162"
_4images_image_date: "2010-09-28T16:46:04"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28687 -->
Hier könnt ihr den Wasserspender von vorne sehen. Man kann die Wanne sehen, wie sie halb ausgefahren ist für die Becher, und das Bedienfeld.