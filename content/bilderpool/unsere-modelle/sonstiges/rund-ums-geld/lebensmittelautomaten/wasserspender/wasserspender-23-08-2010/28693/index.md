---
layout: "image"
title: "Wasserspender"
date: "2010-09-28T16:46:05"
picture: "ag08.jpg"
weight: "8"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/28693
imported:
- "2019"
_4images_image_id: "28693"
_4images_cat_id: "2079"
_4images_user_id: "1162"
_4images_image_date: "2010-09-28T16:46:05"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28693 -->
Hier könnt ihr nochnmal den TX sehen, das Münzfach, und das Ventil dazu später mehr. Im Vordergrund könnt ihr noch eine Halterung sehen, in der der Regler vom PowerSet reingeschoben werden kann.