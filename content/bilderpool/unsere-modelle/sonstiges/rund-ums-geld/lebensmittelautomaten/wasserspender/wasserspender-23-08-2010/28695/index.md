---
layout: "image"
title: "Kompressor"
date: "2010-09-28T16:46:05"
picture: "ag10.jpg"
weight: "10"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/28695
imported:
- "2019"
_4images_image_id: "28695"
_4images_cat_id: "2079"
_4images_user_id: "1162"
_4images_image_date: "2010-09-28T16:46:05"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28695 -->
hier könnt ihr den Kompressor sehen, der Luft in die Flaschen pumpt, damit das Wasser aus der Flasche herausgedrückt werden kann. 
Die rote angepasse FT-Bauplatte, genaus wie der Excenter (das schwarze Teil am Motor) ist von Andreas Tacke (TST). Dieser Kompressor bringt mehr druck, sodass das Wasser nicht tröpfelt sowie beim FT-Kompressor sondern schön fließt. Der Motor hat eine 20:1 Übersetzung. 

Vielen Dank an Adreas hier an dieser Stelle.