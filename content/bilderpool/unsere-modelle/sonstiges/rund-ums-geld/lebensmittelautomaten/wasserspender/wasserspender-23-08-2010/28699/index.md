---
layout: "image"
title: "Ultraschallsensor"
date: "2010-09-28T16:46:06"
picture: "ag14.jpg"
weight: "14"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/28699
imported:
- "2019"
_4images_image_id: "28699"
_4images_cat_id: "2079"
_4images_user_id: "1162"
_4images_image_date: "2010-09-28T16:46:06"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28699 -->
Hier könnt ihr den Ultraschallsensor sehen, der für die Füllstandsmessung im Becher zuständig ist. 
Man kann auch die Schläuche sehen, die den Becher befüllen.