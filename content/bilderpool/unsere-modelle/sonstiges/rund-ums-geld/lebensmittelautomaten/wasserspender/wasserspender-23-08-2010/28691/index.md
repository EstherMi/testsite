---
layout: "image"
title: "Förderwanne"
date: "2010-09-28T16:46:05"
picture: "ag06.jpg"
weight: "6"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/28691
imported:
- "2019"
_4images_image_id: "28691"
_4images_cat_id: "2079"
_4images_user_id: "1162"
_4images_image_date: "2010-09-28T16:46:05"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28691 -->
Hier könnt ihr die Wanne sehen, in der man einen Becher hineinstellt. Man kann auch den vorderen Endtaster sehen, genauso wei die Schläuche, aus der das Wasser kommt.