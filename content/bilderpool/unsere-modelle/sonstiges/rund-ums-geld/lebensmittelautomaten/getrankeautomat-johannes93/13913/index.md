---
layout: "image"
title: "Blick von oben"
date: "2008-03-15T23:39:58"
picture: "getraenkeautomat10.jpg"
weight: "10"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/13913
imported:
- "2019"
_4images_image_id: "13913"
_4images_cat_id: "1277"
_4images_user_id: "636"
_4images_image_date: "2008-03-15T23:39:58"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13913 -->
Der Deckel lässt sich öffnen um Becher nachzufüllen.