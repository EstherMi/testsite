---
layout: "image"
title: "Blick ins Innere"
date: "2008-03-15T23:39:58"
picture: "getraenkeautomat02.jpg"
weight: "2"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/13905
imported:
- "2019"
_4images_image_id: "13905"
_4images_cat_id: "1277"
_4images_user_id: "636"
_4images_image_date: "2008-03-15T23:39:58"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13905 -->
Hier kann man die Becherzufuhr erkennen