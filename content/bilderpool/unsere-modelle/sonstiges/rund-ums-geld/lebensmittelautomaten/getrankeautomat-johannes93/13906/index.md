---
layout: "image"
title: "Bedienfeld"
date: "2008-03-15T23:39:58"
picture: "getraenkeautomat03.jpg"
weight: "3"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/13906
imported:
- "2019"
_4images_image_id: "13906"
_4images_cat_id: "1277"
_4images_user_id: "636"
_4images_image_date: "2008-03-15T23:39:58"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13906 -->
Auf dem Bedienfeld befinden sich alle Lampen und Schalter.