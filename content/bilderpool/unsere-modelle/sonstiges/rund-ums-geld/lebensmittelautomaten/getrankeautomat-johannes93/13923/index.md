---
layout: "image"
title: "Wasserschläuche"
date: "2008-03-15T23:39:58"
picture: "getraenkeautomat20.jpg"
weight: "20"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/13923
imported:
- "2019"
_4images_image_id: "13923"
_4images_cat_id: "1277"
_4images_user_id: "636"
_4images_image_date: "2008-03-15T23:39:58"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13923 -->
Durch die Wasserschläuche fließt das gewünschte Getränk in den Becher.