---
layout: "image"
title: "Münzprüfer mit Geldbehälter"
date: "2008-03-15T23:39:58"
picture: "getraenkeautomat06.jpg"
weight: "6"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/13909
imported:
- "2019"
_4images_image_id: "13909"
_4images_cat_id: "1277"
_4images_user_id: "636"
_4images_image_date: "2008-03-15T23:39:58"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13909 -->
