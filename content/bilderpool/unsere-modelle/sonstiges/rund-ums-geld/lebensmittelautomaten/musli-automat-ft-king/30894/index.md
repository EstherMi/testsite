---
layout: "image"
title: "Müsliautomat von oben"
date: "2011-06-22T11:49:08"
picture: "01_01_10-_24_01_10_008.jpg"
weight: "1"
konstrukteure: 
- "ft-King"
fotografen:
- "ft-King"
keywords: ["Müsliautomat", "Müslimaschine", "Muesliautomat", "Mueslimaschine"]
uploadBy: "ft-King"
license: "unknown"
legacy_id:
- details/30894
imported:
- "2019"
_4images_image_id: "30894"
_4images_cat_id: "2306"
_4images_user_id: "1329"
_4images_image_date: "2011-06-22T11:49:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30894 -->
Müsliautomat von oben