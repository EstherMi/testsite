---
layout: "image"
title: "Getränkeautomat Wasserzufuhr"
date: "2006-05-11T19:58:31"
picture: "getraenkeautomat_wasserzufuhr.jpg"
weight: "3"
konstrukteure: 
- "Jan Knobbe"
fotografen:
- "Jan Knobbe"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/6262
imported:
- "2019"
_4images_image_id: "6262"
_4images_cat_id: "539"
_4images_user_id: "5"
_4images_image_date: "2006-05-11T19:58:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6262 -->
Das sollte der Brause-Kunde ja eigentlich nicht unbedingt sehen: Eine FT-Box dient als Wasser-Reservoir. Man kann auch den Bechertransport durch eine Kette erkennen. Die rechts sichtbaren Nockenscheiben lösen den jeweils nächsten Programmschritt aus.