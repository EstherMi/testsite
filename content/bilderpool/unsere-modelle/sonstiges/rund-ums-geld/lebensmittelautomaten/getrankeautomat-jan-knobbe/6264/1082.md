---
layout: "comment"
hidden: true
title: "1082"
date: "2006-05-11T23:16:05"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Das ist eine tolle Modellidee! Die Ausführung mit Silberlingen und Elektromechanik finde ich Klasse. Kannst Du noch ein paar Details über die Steuerung verraten, insbesondere über die Verwendung der Nockenscheiben?

Gruß,
Stefan