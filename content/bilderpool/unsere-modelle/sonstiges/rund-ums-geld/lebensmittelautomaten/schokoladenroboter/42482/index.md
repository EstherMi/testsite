---
layout: "image"
title: "Der Saugheber (2)"
date: "2015-12-07T17:49:00"
picture: "schorobot04.jpg"
weight: "4"
konstrukteure: 
- "olagino"
fotografen:
- "olagino"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- details/42482
imported:
- "2019"
_4images_image_id: "42482"
_4images_cat_id: "3158"
_4images_user_id: "2042"
_4images_image_date: "2015-12-07T17:49:00"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42482 -->
Auf diesem Foto ist der Auslösemechanismus für den Sauger gut zu sehen, beziehungsweise die Schnecke, die den Sauger auf- und abbewegen kann.