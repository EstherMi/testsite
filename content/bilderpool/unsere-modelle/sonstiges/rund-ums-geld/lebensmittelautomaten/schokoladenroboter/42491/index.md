---
layout: "image"
title: "Kompressorstation"
date: "2015-12-07T17:49:00"
picture: "schorobot13.jpg"
weight: "13"
konstrukteure: 
- "olagino"
fotografen:
- "olagino"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- details/42491
imported:
- "2019"
_4images_image_id: "42491"
_4images_cat_id: "3158"
_4images_user_id: "2042"
_4images_image_date: "2015-12-07T17:49:00"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42491 -->
Die Kompressorstation versorgt den Roboter mit Druckluft beziehungsweise mit Unterdruck. Die zwei Magnetventile werden zum einen für den Saugmechanismus benötigt, zum anderen für die Ausgabe der fünften Schokoladensorte.