---
layout: "image"
title: "Der Geldeinwurf (2)"
date: "2015-12-07T17:49:00"
picture: "schorobot07.jpg"
weight: "7"
konstrukteure: 
- "olagino"
fotografen:
- "olagino"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- details/42485
imported:
- "2019"
_4images_image_id: "42485"
_4images_cat_id: "3158"
_4images_user_id: "2042"
_4images_image_date: "2015-12-07T17:49:00"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42485 -->
Die Sortenwahl funktioniert über das mehrmalige Drücken des Tasters links im Bild. Bestätigt wurde die Sortenwahl durch den Einwurf einer beliebigen Münze.