---
layout: "image"
title: "Der Geldeinwurf"
date: "2015-12-07T17:49:00"
picture: "schorobot06.jpg"
weight: "6"
konstrukteure: 
- "olagino"
fotografen:
- "olagino"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- details/42484
imported:
- "2019"
_4images_image_id: "42484"
_4images_cat_id: "3158"
_4images_user_id: "2042"
_4images_image_date: "2015-12-07T17:49:00"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42484 -->
Hier ist der Geldeinwurf mit der Lichtschranke und dem Geldfach zu sehen. Aus Platzgründen ist dieses Modul nicht auf der Grundplatte befestigt, sondern auf einer externen Platte mit einem zweiten Robo TX Controller montiert. Dieser Verwaltet eine Lichtschranke, einen Taster (mehr waren nicht übrig) noch einen Spursensor, der für spätere Anwendungen mit einer Benutzerkarte gedacht war. Diesen Gedanken habe ich aus Zeitgründen leider Verwerfen müssen.