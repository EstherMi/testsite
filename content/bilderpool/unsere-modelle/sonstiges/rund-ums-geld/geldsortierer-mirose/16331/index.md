---
layout: "image"
title: "Geldsortierer 05"
date: "2008-11-18T16:44:39"
picture: "Geldsortierer_05.jpg"
weight: "5"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Geld", "Euro", "Cent", "Motor", "sortieren"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/16331
imported:
- "2019"
_4images_image_id: "16331"
_4images_cat_id: "1858"
_4images_user_id: "765"
_4images_image_date: "2008-11-18T16:44:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16331 -->
Schütte und Förderband von der Stirnseite sowie Rutsche zum Vereinzelner.