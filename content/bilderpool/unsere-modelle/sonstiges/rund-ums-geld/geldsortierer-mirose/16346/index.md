---
layout: "image"
title: "Geldsortierer 21"
date: "2008-11-18T17:12:09"
picture: "Geldsortierer_21.jpg"
weight: "20"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Geld", "Euro", "Cent", "Motor", "sortieren"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/16346
imported:
- "2019"
_4images_image_id: "16346"
_4images_cat_id: "1858"
_4images_user_id: "765"
_4images_image_date: "2008-11-18T17:12:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16346 -->
Noch einmal Münzen im Vereinzelner.