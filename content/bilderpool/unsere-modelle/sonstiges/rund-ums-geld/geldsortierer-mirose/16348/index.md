---
layout: "image"
title: "Geldsortierer 23"
date: "2008-11-18T17:12:09"
picture: "Geldsortierer_23.jpg"
weight: "22"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Geld", "Euro", "Cent", "Motor", "sortieren"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/16348
imported:
- "2019"
_4images_image_id: "16348"
_4images_cat_id: "1858"
_4images_user_id: "765"
_4images_image_date: "2008-11-18T17:12:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16348 -->
Fast ein Münzstau auf dem Verteiler, aber brav hintereinander. Den Verteiler sollte ich wieder einmal putzen, denn man sollte gar nicht glauben, wie verdreckt die Münzen sind!
An Anfang waren die Münzen, bei gleichem Winkel des Verteilers, fast zu schnell unterwegs.