---
layout: "image"
title: "Geldsortierer 22"
date: "2008-11-18T17:12:09"
picture: "Geldsortierer_22.jpg"
weight: "21"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Geld", "Euro", "Cent", "Motor", "sortieren"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/16347
imported:
- "2019"
_4images_image_id: "16347"
_4images_cat_id: "1858"
_4images_user_id: "765"
_4images_image_date: "2008-11-18T17:12:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16347 -->
Aus einem anderen Blickwinkel.