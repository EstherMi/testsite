---
layout: "image"
title: "Geldsortierer 06"
date: "2008-11-18T16:44:39"
picture: "Geldsortierer_06.jpg"
weight: "6"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Geld", "Euro", "Cent", "Motor", "sortieren"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/16332
imported:
- "2019"
_4images_image_id: "16332"
_4images_cat_id: "1858"
_4images_user_id: "765"
_4images_image_date: "2008-11-18T16:44:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16332 -->
Das Ganze von oben.
Damit das Geld in der Schütte keinen falschen Weg gehen kann, wurde etwas Isolierband geklebt.
Gut zu sehen ist die Scheibe des Vereinzelners.