---
layout: "image"
title: "Geldsortierer 04"
date: "2008-11-18T16:44:38"
picture: "Geldsortierer_04.jpg"
weight: "4"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Geld", "Euro", "Cent", "Motor", "sortieren"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/16330
imported:
- "2019"
_4images_image_id: "16330"
_4images_cat_id: "1858"
_4images_user_id: "765"
_4images_image_date: "2008-11-18T16:44:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16330 -->
Die Schütte, der Vereinzelner und die erste Rutsche etwas größer.
Der Antrieb der Schütte erfolgt mit einem Powermotor 8:1; Schnecke > Zahnrad Z20 und noch eine Schnecke > Zahnrad Z 30 (das rote, sichtbare), das schließlich das Förderband antreibt.
Der Polwendeschalter dient zum fallweisen Abschalten des Förderbandes, falls sich zu viele Münzen im Vereinzelner befinden.

Der Antrieb des Vereinzelners erfolgt durch einen Powermotor 20:1, Ritzel 10 > Zahnrad Z 32 (von einem Z 40) und schließlich mit einem Ritzel auf ein Zahnrad Z 40, das die große Scheibe mit den Löchern antreibt.
Die Scheibe mit den 8 Löchern und die darunterliegende Platte sind Polystyrolplatten und wurden mit Laubsäge und Feilen entsprechend bearbeitet. (Außerdem gebohrt, angesenkt…)

Ganz rechts die Rutsche zum Verteiler.
Die Rollen dienen dazu, die Münzen zum Liegen zu zwingen. Dazu wird die Rolle in der Mitte regelmäßig angehoben, um das Hängenbleiben von Münzen zu verhindern.