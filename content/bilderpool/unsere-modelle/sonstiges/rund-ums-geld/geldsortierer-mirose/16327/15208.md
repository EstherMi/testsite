---
layout: "comment"
hidden: true
title: "15208"
date: "2011-09-26T17:29:03"
uploadBy:
- "mirose"
license: "unknown"
imported:
- "2019"
---
Hallo Kieseleck,

verzeih, daß ich Dir erst jetzt antworte.
Zwischen den 2ct und 20 ct-Münzen gibt es überhaupt kein Problem - fast 3 mm Differenz.
Es ist lediglich eine Einstellungssache, wo welche Münze hineinfällt. Wenn die Einstellung des Spaltes richtig ist, dann passieren fast keine Fehler. Wichtig ist auch die "Beruhigungsstrecke" vor dem ersten Schlitz, damit die Münze glatt an der Bahn aufliegt, sonst werden unerwünschte Münzen in den ersten Schlitz hineinfallen.

Viele Grüße
Mirose