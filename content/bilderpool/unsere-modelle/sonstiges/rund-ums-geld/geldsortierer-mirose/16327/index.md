---
layout: "image"
title: "Geldsortierer 01"
date: "2008-11-18T16:44:38"
picture: "Geldsortierer_01.jpg"
weight: "1"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Geld", "Euro", "Cent", "Motor", "sortieren"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/16327
imported:
- "2019"
_4images_image_id: "16327"
_4images_cat_id: "1858"
_4images_user_id: "765"
_4images_image_date: "2008-11-18T16:44:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16327 -->
Aufgabe dieses Apparats:
Euromünzen nach ihrer Größe sortieren.
Das Geld wird oben in die Schütte eingeleert, wird anschließend mit dem Förderband langsam zum Vereinzelner transportiert, der die Münzen einzeln zum Sortierer fördert, bzw. fallen läßt.