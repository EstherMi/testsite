---
layout: "image"
title: "Geldsortierer 14"
date: "2008-11-18T16:59:01"
picture: "Geldsortierer_14.jpg"
weight: "13"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Geld", "Euro", "Cent", "Motor", "sortieren"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/16339
imported:
- "2019"
_4images_image_id: "16339"
_4images_cat_id: "1858"
_4images_user_id: "765"
_4images_image_date: "2008-11-18T16:59:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16339 -->
Das erste Loch ist das kleinste für die 1-Cent Münzen, dann kommen die 2-Cent-, 10-Cent-, 5-Cent-, 20-Cent-, 1-Euro-, 50-Cent- und schließlich die 2-Euromünzen.
Und man soll’s gar nicht glauben, das Sortieren erfolgt OHNE Fehler!