---
layout: "image"
title: "Geldsortierer 25"
date: "2008-11-18T17:12:09"
picture: "Geldsortierer_25.jpg"
weight: "24"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Geld", "Euro", "Cent", "Motor", "sortieren"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/16350
imported:
- "2019"
_4images_image_id: "16350"
_4images_cat_id: "1858"
_4images_user_id: "765"
_4images_image_date: "2008-11-18T17:12:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16350 -->
Die mechanische Konstruktion von hinten.

Das einzige, was mit noch fehlt, ist ein Münzzähler. Hat wer einen Vorschlag, wie man auch die leichten Münzen (möglichst ohne großen Aufwand) verläßlich zählen kann?