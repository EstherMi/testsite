---
layout: "image"
title: "Geldsortierer 08"
date: "2008-11-18T16:44:39"
picture: "Geldsortierer_08.jpg"
weight: "8"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Geld", "Euro", "Cent", "Motor", "sortieren"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/16334
imported:
- "2019"
_4images_image_id: "16334"
_4images_cat_id: "1858"
_4images_user_id: "765"
_4images_image_date: "2008-11-18T16:44:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16334 -->
Noch einmal der obere Teil, ähnlich Bild 04.