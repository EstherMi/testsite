---
layout: "comment"
hidden: true
title: "7795"
date: "2008-11-19T15:06:27"
uploadBy:
- "mirose"
license: "unknown"
imported:
- "2019"
---
Hallo Sulu!


Vielen Dank für Deinen Vorschlag.
Ich habe leider keine Waage.
Problem mit der Lichtschranke:
1) 8 x notwendig
2) Die Münzen müssen in eine eindeutige Lage gebracht werden, um sie verläßlich zählen zu können. Und das braucht wieder viel Höhe und Platz.
3) Noch habe ich keine Möglichkeit, die Zählerei mit einem Computer auszuwerten.


Vielen Dank

Mirose