---
layout: "image"
title: "Tresor von oben"
date: "2007-09-25T09:37:28"
picture: "P1040343_002.jpg"
weight: "2"
konstrukteure: 
- "dragon"
fotografen:
- "dragon"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dragon"
license: "unknown"
legacy_id:
- details/11985
imported:
- "2019"
_4images_image_id: "11985"
_4images_cat_id: "1104"
_4images_user_id: "637"
_4images_image_date: "2007-09-25T09:37:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11985 -->
Draufsicht auf den Tresor