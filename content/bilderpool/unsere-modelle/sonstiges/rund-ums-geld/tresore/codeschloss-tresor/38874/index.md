---
layout: "image"
title: "Die Steuereinheit ohne Abdeckung"
date: "2014-05-26T20:08:08"
picture: "unbenannt-5241193_2.jpg"
weight: "28"
konstrukteure: 
- "AKtobi47"
fotografen:
- "AKtobi47"
keywords: ["Codeschloss", "Tresor", "Codeschloss/Tresor", "Steuereinheit", "Verkabelung"]
uploadBy: "AKtobi47"
license: "unknown"
legacy_id:
- details/38874
imported:
- "2019"
_4images_image_id: "38874"
_4images_cat_id: "2903"
_4images_user_id: "2183"
_4images_image_date: "2014-05-26T20:08:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38874 -->
Hier der Downloadlink: http://www.file-upload.net/download-10268862/Codeschloss-Tresor.zip.html