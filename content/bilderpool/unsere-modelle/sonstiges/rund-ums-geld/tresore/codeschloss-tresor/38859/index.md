---
layout: "image"
title: "Das Display"
date: "2014-05-26T20:08:07"
picture: "unbenannt-5241186.jpg"
weight: "13"
konstrukteure: 
- "AKtobi47"
fotografen:
- "AKtobi47"
keywords: ["Codeschloss", "Tresor", "Codeschloss/Tresor"]
uploadBy: "AKtobi47"
license: "unknown"
legacy_id:
- details/38859
imported:
- "2019"
_4images_image_id: "38859"
_4images_cat_id: "2903"
_4images_user_id: "2183"
_4images_image_date: "2014-05-26T20:08:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38859 -->
-Oben Links: Wird der Tag seit Inbetriebnahme angezeigt, wird für ein Sicherheitsprotokoll   benötigt, dass die Codeeingabe sperrt und anschließend einen der sieben Codes abfragt, die  jeden Tag wechseln(insgesamt sieben Tage= sieben Codes) 
-Oben Mitte/Rechts: Die Fehleranzeige, zeigt die Anzahl an Fehlern an.
-Das Zahlenfeld
-Die OK-Taste
-Die Ko-Taste: Das ist die Korrekturtaste, hiermit kann man den Code neu eingeben, wenn man sich vertippt hat(aber nur wenn man noch nicht OK gedrückt hat).

Hier der Downloadlink: http://www.file-upload.net/download-10268862/Codeschloss-Tresor.zip.html