---
layout: "image"
title: "Der Reedkontakt ohne Abdeckung"
date: "2014-05-26T18:53:09"
picture: "unbenannt-5241183.jpg"
weight: "11"
konstrukteure: 
- "AKtobi47"
fotografen:
- "AKtobi47"
keywords: ["Codeschloss", "Tresor", "Codeschloss/Tresor"]
uploadBy: "AKtobi47"
license: "unknown"
legacy_id:
- details/38857
imported:
- "2019"
_4images_image_id: "38857"
_4images_cat_id: "2903"
_4images_user_id: "2183"
_4images_image_date: "2014-05-26T18:53:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38857 -->
Dient einem Sicherheitsprotokoll: Bei 3 Fehlern muss man zusätzlich zum Code noch mit einem Magneten den Stromkreis des Reedkontakts schließen.

Hier der Downloadlink: http://www.file-upload.net/download-10268862/Codeschloss-Tresor.zip.html