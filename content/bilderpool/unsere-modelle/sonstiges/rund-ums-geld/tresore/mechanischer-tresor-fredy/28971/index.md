---
layout: "image"
title: "Tresorschloss"
date: "2010-10-10T15:36:10"
picture: "mechanischertresor01.jpg"
weight: "1"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/28971
imported:
- "2019"
_4images_image_id: "28971"
_4images_cat_id: "2104"
_4images_user_id: "453"
_4images_image_date: "2010-10-10T15:36:10"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28971 -->
Die Hebel aus den BS30 halten die Tür zu oder geben sie frei.