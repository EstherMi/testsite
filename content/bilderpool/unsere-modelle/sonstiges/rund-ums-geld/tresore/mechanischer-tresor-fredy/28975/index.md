---
layout: "image"
title: "Tresorschloss"
date: "2010-10-10T15:36:11"
picture: "mechanischertresor05.jpg"
weight: "5"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/28975
imported:
- "2019"
_4images_image_id: "28975"
_4images_cat_id: "2104"
_4images_user_id: "453"
_4images_image_date: "2010-10-10T15:36:11"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28975 -->
Die Hebel um die Tür zu verriegeln.