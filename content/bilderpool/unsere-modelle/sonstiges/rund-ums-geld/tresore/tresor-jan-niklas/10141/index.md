---
layout: "image"
title: "Tresor"
date: "2007-04-21T14:21:45"
picture: "tresor2.jpg"
weight: "2"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- details/10141
imported:
- "2019"
_4images_image_id: "10141"
_4images_cat_id: "917"
_4images_user_id: "557"
_4images_image_date: "2007-04-21T14:21:45"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10141 -->
Tür wird mit Magnetventil und Kolben festgehalten