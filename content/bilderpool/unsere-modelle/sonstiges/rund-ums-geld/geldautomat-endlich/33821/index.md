---
layout: "image"
title: "Rutsche vom Magazin in das Ausgabefach"
date: "2011-12-28T22:55:31"
picture: "geldautomat8.jpg"
weight: "8"
konstrukteure: 
- "M.Endlich"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/33821
imported:
- "2019"
_4images_image_id: "33821"
_4images_cat_id: "2499"
_4images_user_id: "1162"
_4images_image_date: "2011-12-28T22:55:31"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33821 -->
