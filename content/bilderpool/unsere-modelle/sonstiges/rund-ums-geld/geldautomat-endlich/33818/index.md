---
layout: "image"
title: "Vogelperspektive des hinteren Teils"
date: "2011-12-28T22:55:31"
picture: "geldautomat5.jpg"
weight: "5"
konstrukteure: 
- "M.Endlich"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/33818
imported:
- "2019"
_4images_image_id: "33818"
_4images_cat_id: "2499"
_4images_user_id: "1162"
_4images_image_date: "2011-12-28T22:55:31"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33818 -->
