---
layout: "image"
title: "Geldscheinzähler"
date: "2007-07-29T13:00:52"
picture: "geld4.jpg"
weight: "5"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- details/11222
imported:
- "2019"
_4images_image_id: "11222"
_4images_cat_id: "1014"
_4images_user_id: "557"
_4images_image_date: "2007-07-29T13:00:52"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11222 -->
Hier die Übertragung von den unteren auf den oberen Reifen