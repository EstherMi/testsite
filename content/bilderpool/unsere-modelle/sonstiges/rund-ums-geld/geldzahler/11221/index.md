---
layout: "image"
title: "Geldscheinzähler"
date: "2007-07-29T13:00:52"
picture: "geld3.jpg"
weight: "4"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- details/11221
imported:
- "2019"
_4images_image_id: "11221"
_4images_cat_id: "1014"
_4images_user_id: "557"
_4images_image_date: "2007-07-29T13:00:52"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11221 -->
Hier der Einzug, links sieht man das Z20 welches die oberen Reifen antreiben soll( nicht direkt am Pfosten da die oberen Reifen um Millimeter federn sollen.)