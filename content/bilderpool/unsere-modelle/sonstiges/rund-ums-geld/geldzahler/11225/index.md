---
layout: "image"
title: "Geldscheinzähler"
date: "2007-07-29T13:00:52"
picture: "geld7.jpg"
weight: "8"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- details/11225
imported:
- "2019"
_4images_image_id: "11225"
_4images_cat_id: "1014"
_4images_user_id: "557"
_4images_image_date: "2007-07-29T13:00:52"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11225 -->
Hier das ganze mit geldschein, die rote und blaue Lampe zeigen an ob schein eingelegt oder nicht