---
layout: "image"
title: "u-schall01"
date: "2003-04-27T17:46:14"
picture: "u-schall01.jpg"
weight: "22"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/971
imported:
- "2019"
_4images_image_id: "971"
_4images_cat_id: "28"
_4images_user_id: "1"
_4images_image_date: "2003-04-27T17:46:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=971 -->
Untere Doppelreihe: 17 Sender (die äußeren sind nur von einem anderen Hersteller), die alle vom gleichen Signal gespeist werden. Dazwischen liegt aber noch ein Diodennetzwerke, das die Amplituden nach außen hin schwächer werden lässt. Angepeilt ist eine Keulenbreite von unter 5°.

Oben drauf sind 3 Empfänger montiert. Jeder soll seinen eigenen Verstärker und A/D-Wandler bekommen, damit man die Signale gegeneinander vergleichen und eine Feinst-Winkelbestimmung ("Monopuls") machen kann. Dazu wird ein Empfängerpaar senkrecht und ein Paar waagerecht gebraucht. Der Empfänger links unten spielt in beiden Empfängerpaaren mit. Rechts oben wird vielleicht noch ein Dummy montiert, damit die Optik stimmt.

Das ist aber alles noch Theorie.