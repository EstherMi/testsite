---
layout: "image"
title: "Radar mit Kette-Antrieb und Kugellagerung"
date: "2008-01-01T18:58:07"
picture: "Radar_006.jpg"
weight: "13"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/13198
imported:
- "2019"
_4images_image_id: "13198"
_4images_cat_id: "1230"
_4images_user_id: "22"
_4images_image_date: "2008-01-01T18:58:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13198 -->
Radar mit Kette-Antrieb und Kugellagerung

Etwas ähnliches wie das schönes Modell in Mörshausen zu sehen war:
Konstrukteur: MisterWho (Joachim Jacobi)
http://www.ftcommunity.de/details.php?image_id=11527

Die Radaranlage bei mir hat aber eine Kette-Antrieb und Kugellagerung.

Ich möchte gerne eine Ultrasound-Radar-Anlage mit mehrere Schleifringen machen.

Beim Erfinderhome -Website gibt es die interessante Fischertechnik-Radaranlage von Constantin Neumann:
http://www.repage4.de/member/erfinderhome/projekte.html

Helmut Meyer hat auch interessante sachen entwickelt mit 3 Fotowiderstanden http://www.meyer-rhauderfehn.de/PrivateHomepage/Radarantenne/FTBilder/Fotogalerie/index.html

http://www.meyer-rhauderfehn.de/PrivateHomepage/


Gruss,

Peter Damen
Poederoyen NL