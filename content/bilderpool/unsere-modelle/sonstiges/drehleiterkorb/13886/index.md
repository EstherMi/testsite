---
layout: "image"
title: "Elektromagnet-Funktion"
date: "2008-03-07T15:29:10"
picture: "drehleiterkorb4.jpg"
weight: "4"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/13886
imported:
- "2019"
_4images_image_id: "13886"
_4images_cat_id: "1273"
_4images_user_id: "747"
_4images_image_date: "2008-03-07T15:29:10"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13886 -->
Auf diesem Bild sieht man einen Elektromagnet, der immer an ist, das die Tür geschlossen bleibt.