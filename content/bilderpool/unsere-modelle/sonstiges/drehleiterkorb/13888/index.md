---
layout: "image"
title: "Beleuchtung"
date: "2008-03-07T15:29:10"
picture: "drehleiterkorb6.jpg"
weight: "6"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/13888
imported:
- "2019"
_4images_image_id: "13888"
_4images_cat_id: "1273"
_4images_user_id: "747"
_4images_image_date: "2008-03-07T15:29:10"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13888 -->
Hier sieht man die Lampen, die den Korb beleuchten.