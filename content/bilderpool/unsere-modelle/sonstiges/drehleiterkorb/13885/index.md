---
layout: "image"
title: "geschlossene Tür"
date: "2008-03-07T15:29:10"
picture: "drehleiterkorb3.jpg"
weight: "3"
konstrukteure: 
- "Johannes"
fotografen:
- "johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/13885
imported:
- "2019"
_4images_image_id: "13885"
_4images_cat_id: "1273"
_4images_user_id: "747"
_4images_image_date: "2008-03-07T15:29:10"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13885 -->
Auf diesem Bild ist die Tür geschlossen.