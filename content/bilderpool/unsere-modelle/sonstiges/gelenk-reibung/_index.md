---
layout: "overview"
title: "Gelenk mit Reibung"
date: 2019-12-17T19:38:12+01:00
legacy_id:
- categories/2881
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2881 --> 
Ein Gelenk, das seine jeweilige Stellung beibehält.