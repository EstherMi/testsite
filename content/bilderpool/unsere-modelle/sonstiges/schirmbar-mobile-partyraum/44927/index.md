---
layout: "image"
title: "Aufbau 2"
date: "2016-12-27T10:51:06"
picture: "schirmbardermobilepartyraum07.jpg"
weight: "7"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- details/44927
imported:
- "2019"
_4images_image_id: "44927"
_4images_cat_id: "3342"
_4images_user_id: "2240"
_4images_image_date: "2016-12-27T10:51:06"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44927 -->
Nach dem Aufklappen. 
In der Mitte liegt der Lichtmast mit 4 bunten LEDs und einer weißen Lampe zur Beleuchtung der Bar.
Das restliche Material befindet sich zum Transport im LKW.