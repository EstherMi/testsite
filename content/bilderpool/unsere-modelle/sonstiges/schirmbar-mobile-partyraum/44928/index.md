---
layout: "image"
title: "Aufbau 3"
date: "2016-12-27T10:51:06"
picture: "schirmbardermobilepartyraum08.jpg"
weight: "8"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- details/44928
imported:
- "2019"
_4images_image_id: "44928"
_4images_cat_id: "3342"
_4images_user_id: "2240"
_4images_image_date: "2016-12-27T10:51:06"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44928 -->
Unterseite der Schirmbar
Die ausgezogenen Seitenteile werden zum Aufbau wieder nach innen geschoben so dass sie an der Mittelplatte anliegen.