---
layout: "image"
title: "ft-PC 001"
date: "2005-02-08T16:21:04"
picture: "ft-PC_001.JPG"
weight: "1"
konstrukteure: 
- "markus mack"
fotografen:
- "marku mack"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/3534
imported:
- "2019"
_4images_image_id: "3534"
_4images_cat_id: "324"
_4images_user_id: "5"
_4images_image_date: "2005-02-08T16:21:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3534 -->
Das Pc-Gehäuse besteht aus knapp 100 roten Flachsteinen 60 und ca. 250 grauen Bausteinen 30 sowie noch ein paar duzend anderen Bausteinen.

Derzeit beherbergt der Pc einen Pentium 300, auf dem Win2k läuft...

Diskettenlaufwerk habe ich vergessen einzubauen, den Einschaltknopf auch, weswegen der altbewährte NOTAUS-hau-drauf-Knopf links als Ein-Ausschalter herhalten muss. Praktisch, wenn das Ding mal auf die Idee kommen sollte, abzustürzen...
Auch die Boxen sind nur Attrappe, ich habe die Soundkarte auch vergessen...

MarMac