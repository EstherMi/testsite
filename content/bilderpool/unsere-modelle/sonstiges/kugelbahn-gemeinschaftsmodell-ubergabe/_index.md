---
layout: "overview"
title: "Kugelbahn-Gemeinschaftsmodell Übergabe der Kugeln zwischen einzelnen Modulen"
date: 2019-12-17T19:38:01+01:00
legacy_id:
- categories/2622
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2622 --> 
Im Forum wurde bereits ausführlich über eine Kugelbahn als Gemeinschaftsmodell auf der Convention diskutiert: http://forum.ftcommunity.de/viewtopic.php?f=6&t=593 Hauptproblem wäre die Abstimmung der einzelnen Module aufeinander. Dafür würde ich folgende Eigenschaften der einzelnen Modelle vorschlagen:

-die einzelnen Module stehen auf Experimentierplatten 500 (Nr. 32985 oder 30385)
-eine Kugelbahn steht immer auf einer oder zwei Platten 500 die mit der kürzeren Seite aneinnanderstoßen.
-Die Übergabe der Kugeln zwischen Modulen läuft wie auf den Fotos zu sehen ab
-Wenn die Lichtschranke für länger als 5s unterbrochen wird, bleibt das entsprechde Modul stehen, da das nachfolgende Modul offensichtlich nicht mit der Verarbeitung der Bälle hinterherkommt. 
-Alle Module sollten möglichst keine Bälle "velieren"
-Die Ecken solten nach Möglichkeit von einer einzelnen Person aufgebaut werden, da es hier spezielle Anforderungen gibt und es ärgerlich wäre wenn eine Ecke fehlt ;-)

-zwei platten sind immer mit bs15 verbunden
-die Übergabe erfolgt in 4,5 cm Höhe (damit sollte genug Platz für alle Möglichen Hebemechanismen vorhanden sein)
-den rest sieht man auf den bildern