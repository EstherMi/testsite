---
layout: "image"
title: "Transportband mit Bearbeitungsstation (alte Version)"
date: "2010-06-30T21:10:15"
picture: "transportbandmitbearbeitungsstationalteversion1.jpg"
weight: "1"
konstrukteure: 
- "Jürgen Ihrig"
fotografen:
- "Jürgen Ihrig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "juergen669"
license: "unknown"
legacy_id:
- details/27602
imported:
- "2019"
_4images_image_id: "27602"
_4images_cat_id: "1986"
_4images_user_id: "1158"
_4images_image_date: "2010-06-30T21:10:15"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27602 -->
Ansicht Vorne