---
layout: "image"
title: "Block Trophy"
date: "2010-03-03T20:05:23"
picture: "ft-blocktrophy.jpg"
weight: "2"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["PCS", "trophy"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/26575
imported:
- "2019"
_4images_image_id: "26575"
_4images_cat_id: "1497"
_4images_user_id: "585"
_4images_image_date: "2010-03-03T20:05:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26575 -->
Experimenting with different ft trophy designs.