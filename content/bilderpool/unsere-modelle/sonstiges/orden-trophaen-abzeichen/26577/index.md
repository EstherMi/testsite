---
layout: "image"
title: "ft Medal"
date: "2010-03-03T20:05:24"
picture: "ft-medal.jpg"
weight: "4"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["PCS", "Medal"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/26577
imported:
- "2019"
_4images_image_id: "26577"
_4images_cat_id: "1497"
_4images_user_id: "585"
_4images_image_date: "2010-03-03T20:05:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26577 -->
Experimenting with medal designs