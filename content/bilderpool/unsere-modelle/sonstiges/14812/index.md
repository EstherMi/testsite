---
layout: "image"
title: "Lucky Cat (version 1)"
date: "2008-07-13T16:36:14"
picture: "lucky_cat_fl.jpg"
weight: "20"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Lucky", "Cat"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/14812
imported:
- "2019"
_4images_image_id: "14812"
_4images_cat_id: "323"
_4images_user_id: "585"
_4images_image_date: "2008-07-13T16:36:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14812 -->
Cut from Wikipedia: "The Maneki Neko ("Beckoning Cat"...also known as Welcoming Cat, Lucky Cat, Money cat or Fortune Cat) is a common Japanese sculpture, often made of porcelain or ceramic, which is believed to bring good luck to the owner....In the design of the sculptures, a raised right paw supposedly attracts money, while a raised left paw attracts customers."


***google translation:
Cut aus Wikipedia: "Die Maneki Neko (" lockt Cat "... auch bekannt als Cat Genugtuung, Lucky Cat, Geld Fortune Katze oder Katze) ist eine weit verbreitete japanische Skulptur, die häufig aus Porzellan oder Keramik, das ist der Ansicht, dass die gute Glück an den Eigentümer .... bei der Gestaltung der Skulpturen, ein Bein angehoben rechts zieht angeblich Geld, während eine erhöhte linken Bein zieht Kunden.