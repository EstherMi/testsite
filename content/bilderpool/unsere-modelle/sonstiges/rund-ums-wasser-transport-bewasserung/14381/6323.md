---
layout: "comment"
hidden: true
title: "6323"
date: "2008-04-25T16:30:51"
uploadBy:
- "ludger-ftc"
license: "unknown"
imported:
- "2019"
---
Hallo Jakob,

da habe ich einen guten Tip für dich.
Früher gab es für die Fahrradventile einen Gummischlauch.
Dieser wurde auf die Ventile geschoben und hat somit einen Luftaustritt vermieden.
Leider gibt es den heute nicht mehr in allen Geschäften.
Vielleicht hast du ja in deiner Nähe einen Laden der den noch verkauft. Man kann ihn als Meterware kaufen. Er hat einen Außendurchmesser von ca. 4mm, ist hochflexibel und sehr gut geeignet.
Ich nutze ihn auch als Pneumatik Schlauch. Immer dann wenn es beweglich sein soll.

Gruß ludger