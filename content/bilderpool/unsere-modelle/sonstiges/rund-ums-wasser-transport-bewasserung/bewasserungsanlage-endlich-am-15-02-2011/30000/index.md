---
layout: "image"
title: "Bewässerungsanlage V2 - von oben"
date: "2011-02-15T21:14:59"
picture: "bwv10.jpg"
weight: "10"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/30000
imported:
- "2019"
_4images_image_id: "30000"
_4images_cat_id: "2212"
_4images_user_id: "1162"
_4images_image_date: "2011-02-15T21:14:59"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30000 -->
Die Bewässerungsanlage aus der Vogelperspektive.