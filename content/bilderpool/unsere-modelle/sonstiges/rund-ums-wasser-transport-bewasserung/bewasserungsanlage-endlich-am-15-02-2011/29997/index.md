---
layout: "image"
title: "Bewässerungsanlage V2 - Bedienfeld"
date: "2011-02-15T21:14:59"
picture: "bwv07.jpg"
weight: "7"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/29997
imported:
- "2019"
_4images_image_id: "29997"
_4images_cat_id: "2212"
_4images_user_id: "1162"
_4images_image_date: "2011-02-15T21:14:59"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29997 -->
Hier sieht man das Bedienfeld. Hier findet man 2 Schalter mit jeweils 2 verschiedenen Richtungen. 
Der rechte Schalter ist für das Licht und noch einer freien Funktion.
Der linke Schalter ist für das Extra-Gießen und für die Ventilatoren.