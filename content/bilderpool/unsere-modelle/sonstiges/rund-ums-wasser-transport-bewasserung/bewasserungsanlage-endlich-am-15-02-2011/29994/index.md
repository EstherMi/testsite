---
layout: "image"
title: "Bewässerungsanlage V2 - 'Pflanzenstellplatz'"
date: "2011-02-15T21:14:52"
picture: "bwv04.jpg"
weight: "4"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/29994
imported:
- "2019"
_4images_image_id: "29994"
_4images_cat_id: "2212"
_4images_user_id: "1162"
_4images_image_date: "2011-02-15T21:14:52"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29994 -->
Hier kann man den Platz sehen, an dem die Pflanzen später in Töpfen stehen werden, man sieht die Beleuchtung, und die zwei Ventilatoren. Der Arm steht jetzt in der Ausgangsposition.