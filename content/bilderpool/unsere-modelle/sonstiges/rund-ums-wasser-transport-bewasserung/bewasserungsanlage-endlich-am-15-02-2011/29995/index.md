---
layout: "image"
title: "Bewässerungsanlage V2 - Ventilator"
date: "2011-02-15T21:14:52"
picture: "bwv05.jpg"
weight: "5"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/29995
imported:
- "2019"
_4images_image_id: "29995"
_4images_cat_id: "2212"
_4images_user_id: "1162"
_4images_image_date: "2011-02-15T21:14:52"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29995 -->
Hier sieht man den Ventilator, der die Pflanzen später etwas kühlen soll, wenn es heiß ist.