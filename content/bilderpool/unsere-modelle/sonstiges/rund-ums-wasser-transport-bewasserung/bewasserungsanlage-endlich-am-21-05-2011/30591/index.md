---
layout: "image"
title: "Bewässerungsanlage mit Pflanzen"
date: "2011-05-21T12:12:17"
picture: "anlage3.jpg"
weight: "3"
konstrukteure: 
- "Endlich"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/30591
imported:
- "2019"
_4images_image_id: "30591"
_4images_cat_id: "2280"
_4images_user_id: "1162"
_4images_image_date: "2011-05-21T12:12:17"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30591 -->
Hier die Schilder die zeigen was für eine Sorte an Pflanzen in den Töpfchen steckt. In dem Fall Kresse und Buschbohnen.