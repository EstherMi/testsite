---
layout: "image"
title: "Wasserpumpe"
date: "2008-04-24T23:06:05"
picture: "PICT3949.jpg"
weight: "4"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/14383
imported:
- "2019"
_4images_image_id: "14383"
_4images_cat_id: "2155"
_4images_user_id: "729"
_4images_image_date: "2008-04-24T23:06:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14383 -->
Wasserpumpe von unten.