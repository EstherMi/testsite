---
layout: "image"
title: "Gießanlage"
date: "2010-08-25T17:55:06"
picture: "giessanlage1.jpg"
weight: "1"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/27947
imported:
- "2019"
_4images_image_id: "27947"
_4images_cat_id: "2024"
_4images_user_id: "1162"
_4images_image_date: "2010-08-25T17:55:06"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27947 -->
Hallo,

hier seht ihr meine FT- Gießanlage. Die Plastik-Flasche wird mit Wasser gefüllt und durch Luft herausgetrückt. Man kann mit der Gießanlage von Pflanze zu Pflanze gehn und sie dann gießen. 

Ich hoffe euch gefallen die Bilder und ihr schreibt fleißig Kommentarre. 

MfG
Endlich