---
layout: "image"
title: "Gießanlage"
date: "2010-08-25T17:55:07"
picture: "giessanlage8.jpg"
weight: "8"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/27954
imported:
- "2019"
_4images_image_id: "27954"
_4images_cat_id: "2024"
_4images_user_id: "1162"
_4images_image_date: "2010-08-25T17:55:07"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27954 -->
Hier seht ihr die von vorne. man dieht das Ventil, das das Wasser in den jeweiligen Schlauch leitet. Und den Verteiler an dem man noch weitere Schläuche anschließen kann, allerdings bringt dann es nicht mehr so viel Druck auf den einzelnen Schlauch, wenn man mehrere anschließt.