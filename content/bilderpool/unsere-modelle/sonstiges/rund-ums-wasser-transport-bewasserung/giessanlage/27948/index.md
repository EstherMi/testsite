---
layout: "image"
title: "Gießanlage"
date: "2010-08-25T17:55:07"
picture: "giessanlage2.jpg"
weight: "2"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/27948
imported:
- "2019"
_4images_image_id: "27948"
_4images_cat_id: "2024"
_4images_user_id: "1162"
_4images_image_date: "2010-08-25T17:55:07"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27948 -->
Hier seht ihr die Gießanlage von der Seite. Der Deckel der Flasche ist Marke Eigenbau und wurde so gebaut:

In den Deckel wurden 2 Löcher gebohrt und Wattestäbchen (Watte wurde weggeschnitten) hineingesteckt. Und dann innendrin mit 2-Komponenten-Kleber eingeklebt (und gleichzeitig wasserdicht gemacht). Die FT-Winkelanschlüsse passen perfekt auf die Wattestäbchen genauso wie die Schläuche. Die Idee von den Wattestäbchen stammt von fitec, vielen Dank an dieser Stelle nochmal für den Tipp.