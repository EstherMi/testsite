---
layout: "image"
title: "Gießanlage"
date: "2010-08-25T17:55:07"
picture: "giessanlage5.jpg"
weight: "5"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/27951
imported:
- "2019"
_4images_image_id: "27951"
_4images_cat_id: "2024"
_4images_user_id: "1162"
_4images_image_date: "2010-08-25T17:55:07"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27951 -->
Hier sieht man die Gießanlage von hinten. Man sieht den FT- Akku, das Luft- Entlüftungsventil, das dazu diehnt restliche Luft aus der Flasche zu lassen. Und man sieht den Kompressor (Motor 20:1) von hinten.