---
layout: "comment"
hidden: true
title: "20816"
date: "2015-07-03T07:14:31"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Da bin ich 100%ig Stefan's Meinung, anders macht das keinen Sinn. schnaggel's Gedanke ist aber auch nicht falsch. Bloß kein Wasser in den Kompressor, der eignet sich garantiert nicht als Wasserpumpe!

Ich hab übrigens auch schon geflucht daß er keinen Anschluß auf der Saugseite hat (die Pumpe saugt schlicht durch's Gehäuse!). Dann wäre er wunderschön auch als Unterdruckpumpe zu gebrauchen...

Grüße
H.A.R.R.Y.