---
layout: "image"
title: "Gießkopf"
date: "2015-07-01T18:05:08"
picture: "095.jpg"
weight: "3"
konstrukteure: 
- "Bauteil"
fotografen:
- "Bauteil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Bauteil"
license: "unknown"
legacy_id:
- details/41370
imported:
- "2019"
_4images_image_id: "41370"
_4images_cat_id: "3091"
_4images_user_id: "2452"
_4images_image_date: "2015-07-01T18:05:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41370 -->
Gießmaschine