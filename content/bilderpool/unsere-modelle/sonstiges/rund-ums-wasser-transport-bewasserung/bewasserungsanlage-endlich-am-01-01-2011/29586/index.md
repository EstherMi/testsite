---
layout: "image"
title: "Bewässerungsanlage- Schläuche"
date: "2011-01-01T17:41:04"
picture: "ssfsf5.jpg"
weight: "5"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/29586
imported:
- "2019"
_4images_image_id: "29586"
_4images_cat_id: "2156"
_4images_user_id: "1162"
_4images_image_date: "2011-01-01T17:41:04"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29586 -->
Hier kann man die eigentliche Bewässerungsanlage sehen, die Schläuche, die später über den Töpfen hängen und das Gestell, das über den Töpfen später steht.