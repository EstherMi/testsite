---
layout: "image"
title: "Bewässerungsanlage- Gesamtansicht"
date: "2011-01-01T17:41:04"
picture: "ssfsf1.jpg"
weight: "1"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/29582
imported:
- "2019"
_4images_image_id: "29582"
_4images_cat_id: "2156"
_4images_user_id: "1162"
_4images_image_date: "2011-01-01T17:41:04"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29582 -->
Hier nun meine Bewässerungsanlage sie ist jetzt momentan noch in der "Testphase" und hängt an der Zeitschaltuhr und gießt in eine FT- Aufbewahrungsbox.
Die Box 500 auf der die Anlage steht ist darunter, weil wenn Wasser aus den Schläuchen fließen würde, würde die Box 500 das Wasser aufsammeln.