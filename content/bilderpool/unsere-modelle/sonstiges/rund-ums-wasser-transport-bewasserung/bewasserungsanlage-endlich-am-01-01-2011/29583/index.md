---
layout: "image"
title: "Bewässerungsanlage- Steuereinheit."
date: "2011-01-01T17:41:04"
picture: "ssfsf2.jpg"
weight: "2"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/29583
imported:
- "2019"
_4images_image_id: "29583"
_4images_cat_id: "2156"
_4images_user_id: "1162"
_4images_image_date: "2011-01-01T17:41:04"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29583 -->
Die Bauplatte 500 auf der die Flasche, die Schalter, die Ventile und das Robo Interface drauf sind. 
Unten links neben der Flasche, sieht man noch das Ventil, das die Luft aus der Flasche lassen kann.