---
layout: "image"
title: "Bewässerungsanlage- Flasche"
date: "2011-01-01T17:41:05"
picture: "ssfsf9.jpg"
weight: "9"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/29590
imported:
- "2019"
_4images_image_id: "29590"
_4images_cat_id: "2156"
_4images_user_id: "1162"
_4images_image_date: "2011-01-01T17:41:05"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29590 -->
Hier sieht man die Flasche, in der das Wasser drin ist. Im Vordergrund sieht man noch die Schalter und die Statusleuchten.