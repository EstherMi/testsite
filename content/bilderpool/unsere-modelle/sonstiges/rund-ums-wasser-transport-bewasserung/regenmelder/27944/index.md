---
layout: "image"
title: "Regenmelder"
date: "2010-08-25T17:55:06"
picture: "regenmelder4.jpg"
weight: "4"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/27944
imported:
- "2019"
_4images_image_id: "27944"
_4images_cat_id: "2023"
_4images_user_id: "1162"
_4images_image_date: "2010-08-25T17:55:06"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27944 -->
Hier sieht man die Kontakte, die außen auf dem Dach in einem Batteriefachdeckel liegen, der Deckel wurde mit Klebeband befestigt.