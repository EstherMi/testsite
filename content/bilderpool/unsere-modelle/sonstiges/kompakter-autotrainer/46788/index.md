---
layout: "image"
title: "Fahrbetrieb"
date: "2017-10-15T13:19:00"
picture: "kompakterautotrainer16.jpg"
weight: "16"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46788
imported:
- "2019"
_4images_image_id: "46788"
_4images_cat_id: "3463"
_4images_user_id: "104"
_4images_image_date: "2017-10-15T13:19:00"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46788 -->
So liegt die Aufwickel-Rolle im Gerät, wenn der Motor sie antreiben soll und der Fahrer fahren will.