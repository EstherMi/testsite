---
layout: "image"
title: "Ersatzfahrbahnen"
date: "2017-10-15T13:19:00"
picture: "kompakterautotrainer07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46779
imported:
- "2019"
_4images_image_id: "46779"
_4images_cat_id: "3463"
_4images_user_id: "104"
_4images_image_date: "2017-10-15T13:19:00"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46779 -->
Zwei weitere Bahnen können in der Ersatzaufnahme gelagert werden.