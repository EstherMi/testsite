---
layout: "comment"
hidden: true
title: "23709"
date: "2017-10-19T12:20:09"
uploadBy:
- "Pilami"
license: "unknown"
imported:
- "2019"
---
Hallo Stefan,
sind das die Papierrollen, die du vor 11 Jahren hier: https://ftcommunity.de/details.php?image_id=7172#col3 schon beschrieben hast?

Ich finde es erstaunlich, das Dinge solang überleben können - großartig!!