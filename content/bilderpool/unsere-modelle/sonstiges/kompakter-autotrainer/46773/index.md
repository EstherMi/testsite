---
layout: "image"
title: "Gesamtansicht, betriebsbereit"
date: "2017-10-15T13:19:00"
picture: "kompakterautotrainer01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46773
imported:
- "2019"
_4images_image_id: "46773"
_4images_cat_id: "3463"
_4images_user_id: "104"
_4images_image_date: "2017-10-15T13:19:00"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46773 -->
Im Clubheft 1972-4 war ein tolles Modell in der Rubrik "Aktuelles zum Nachbauen", was unter den fischertechnik-Kindern von damals als der absolute Hammer galt und wirklich von jedem, der eine Lichtschranke bauen konnte, nachgebaut wurde: Man lenkt eine Lichtschranke über ein durchlaufendes Papierband - eine mit einer Fahrbahn bemalte Rolle einer druckenden Tischrechenmaschine. Auch für meine Kinder "musste" ich dieses Modell immer wieder bauen ("Papa baust Du den Autotrainer?" <ganzliebguck> ;-)