---
layout: "image"
title: "Rückseite"
date: "2017-10-15T13:19:00"
picture: "kompakterautotrainer03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46775
imported:
- "2019"
_4images_image_id: "46775"
_4images_cat_id: "3463"
_4images_user_id: "104"
_4images_image_date: "2017-10-15T13:19:00"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46775 -->
Die Elektronik für die Lichtschranke war damals einfach ein l-e1-Elektronikstab oder ein h4-Gleichrichter nebst h4-Relaisbaustein. Mit mehr Silberlingen ergänzte ich das damals, sodass mehr Fehler gezählt wurden, je länger man die Fahrbahn verfehlte.

Heute genügt ein Electronics-Modul (wenn auch ohne die gepulste Zählung). Man braucht aber sehr wohl das Poti davon zum Einstellen der Empfindlichkeit der Lichtschranke. Mit einem E-Tec könnte das Modell knifflig werden. Der "Schalter" links im Bild ist für an/aus der Maschine. Eingeschaltet läuft der Motor ständig durch, die Elektronik steht unter Strom, und man kann das gute alte em-6-Zählwerk per Taster (darunter) auf 0 stellen.