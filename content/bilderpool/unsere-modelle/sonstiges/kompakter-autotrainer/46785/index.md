---
layout: "image"
title: "Elektronik"
date: "2017-10-15T13:19:00"
picture: "kompakterautotrainer13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46785
imported:
- "2019"
_4images_image_id: "46785"
_4images_cat_id: "3463"
_4images_user_id: "104"
_4images_image_date: "2017-10-15T13:19:00"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46785 -->
Mehr als eine Lichtschranke, die bei Durchgang eine Fehlerlampe aufleuchten lässt, ist für den Spaß mit dem Modell nicht notwendig.