---
layout: "image"
title: "Feuerwerfer 11"
date: "2013-04-18T11:36:22"
picture: "feuerwerfer11.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/36838
imported:
- "2019"
_4images_image_id: "36838"
_4images_cat_id: "2735"
_4images_user_id: "860"
_4images_image_date: "2013-04-18T11:36:22"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36838 -->
Die Düse ist übrigens original ft!! Das ist der Anschluss eines Akkus, in den man normalerweise die Stecker steckt. Er lässt sich aber auch anderweitig verwenden :D

Der NTC-Widerstand reagiert auf die Temperatur der Düse und öffnet dann ggf. das Ablassventil, wenn es zu heiß wird.