---
layout: "image"
title: "Feuerwerfer 06"
date: "2013-04-18T11:36:22"
picture: "feuerwerfer06.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/36833
imported:
- "2019"
_4images_image_id: "36833"
_4images_cat_id: "2735"
_4images_user_id: "860"
_4images_image_date: "2013-04-18T11:36:22"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36833 -->
Das ist das Schlauchsystem.

Durch Öffnen der Dose strömt das Butan in den ft-Tank, den es bei einem ausreichend hohem Druck durch den Ausgang unten rechts verlassen kann. Die grünen Teile (wie heißen die?) verringern den Durchmesser des Schlauches, sodass vorne an der Düse nur eine etwa Kerzen-große Flamme entsteht. Wenn man nun das untere Ventil öffnet, gelangt das Gas  druch die weit aus größere Öffnung zur Düse und es entsteht eine große Flamme. 

Das Ablass Ventil dient zum Leeren des Tankes, zusätzlich wird Luft durch den Kompressor in den Tank gepumpt.