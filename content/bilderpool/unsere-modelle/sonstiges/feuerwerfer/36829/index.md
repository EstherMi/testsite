---
layout: "image"
title: "Feuerwerfer 02"
date: "2013-04-18T11:36:21"
picture: "feuerwerfer02.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/36829
imported:
- "2019"
_4images_image_id: "36829"
_4images_cat_id: "2735"
_4images_user_id: "860"
_4images_image_date: "2013-04-18T11:36:21"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36829 -->
Die genaue Funktionsweise werde ich später bei einem anderen Bild erklären. Hier nur ein paar Daten:

- Der Antrieb erfolgt über ein Gleichlaufgetriebe mit zwei Powermotoren.
- ein Motor für die Gaszufuhr, einer fürs Drehen der Düse
- eine Dose mit Feuerzeuggas, ein ft-Tank, zwei Ventile und ein Kompressor