---
layout: "image"
title: "Verschiedene Winkelmöglichkeiten für Tragwerk-Konstruktionen"
date: "2010-10-16T21:50:53"
picture: "IMG_1030_2.jpg"
weight: "1"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- details/29024
imported:
- "2019"
_4images_image_id: "29024"
_4images_cat_id: "1129"
_4images_user_id: "740"
_4images_image_date: "2010-10-16T21:50:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29024 -->
Von links nach rechts:

1.) 82,5°-Variante

2.) 75°-Variante

3.) 60°-Variante

4.) 30°-Variante