---
layout: "image"
title: "Bekannte Fußkonstruktion eines Statiktragwerkes"
date: "2007-11-08T07:58:39"
picture: "IMG_0128.jpg"
weight: "3"
konstrukteure: 
- "Burkhard Eins"
fotografen:
- "Burkhard Eins"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Burkhard Eins"
license: "unknown"
legacy_id:
- details/12533
imported:
- "2019"
_4images_image_id: "12533"
_4images_cat_id: "1129"
_4images_user_id: "611"
_4images_image_date: "2007-11-08T07:58:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12533 -->
Bekannte Fußkonstruktion eines Statiktragwerkes 

Leicht modifizierte Ausführung der Unterkonstruktion des Molekrans im klassischen ft-Grau.
Winkelsteine im unteren Bereich sind 15° im oberen Bereich 30°