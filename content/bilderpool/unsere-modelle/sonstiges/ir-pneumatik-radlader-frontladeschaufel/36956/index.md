---
layout: "image"
title: "IR Pneumatik Radlader mit Frontladeschaufel von DasKasperle - DETAIL  Dioden 1"
date: "2013-05-26T09:50:17"
picture: "radladermitfrontladeschaufelvon6.jpg"
weight: "6"
konstrukteure: 
- "DasKasperle alias Sushitechnik"
fotografen:
- "Kai"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- details/36956
imported:
- "2019"
_4images_image_id: "36956"
_4images_cat_id: "2747"
_4images_user_id: "1677"
_4images_image_date: "2013-05-26T09:50:17"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36956 -->
Die Dioden ermöglichen es mir mehr als nur 2 Magnetventile zu benutzen. Für jede Steuerrichtung eines Sticks kann ein Elktromagnetventil angesteuert werden.