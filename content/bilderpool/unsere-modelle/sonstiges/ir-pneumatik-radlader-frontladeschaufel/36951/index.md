---
layout: "image"
title: "IR Pneumatik Radlader mit Frontladeschaufel von DasKasperle - SEITE"
date: "2013-05-26T09:50:17"
picture: "radladermitfrontladeschaufelvon1.jpg"
weight: "1"
konstrukteure: 
- "DasKasperle alias Sushitechnik"
fotografen:
- "Kai"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- details/36951
imported:
- "2019"
_4images_image_id: "36951"
_4images_cat_id: "2747"
_4images_user_id: "1677"
_4images_image_date: "2013-05-26T09:50:17"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36951 -->
