---
layout: "image"
title: "Escher's Belvedere"
date: "2007-05-27T18:23:24"
picture: "belvedere.jpg"
weight: "2"
konstrukteure: 
- "Martijn Kerkhof"
fotografen:
- "Martijn Kerkhof"
keywords: ["escher", "belvedere"]
uploadBy: "martijn"
license: "unknown"
legacy_id:
- details/10518
imported:
- "2019"
_4images_image_id: "10518"
_4images_cat_id: "902"
_4images_user_id: "531"
_4images_image_date: "2007-05-27T18:23:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10518 -->
M.C. Escher's Belvedere