---
layout: "image"
title: "penrose stairs"
date: "2007-03-08T22:36:00"
picture: "IMG_1711.jpg"
weight: "8"
konstrukteure: 
- "Martijn Kerkhof"
fotografen:
- "Martijn Kerkhof"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "martijn"
license: "unknown"
legacy_id:
- details/9351
imported:
- "2019"
_4images_image_id: "9351"
_4images_cat_id: "902"
_4images_user_id: "531"
_4images_image_date: "2007-03-08T22:36:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9351 -->
Penrose (infinite) stairs