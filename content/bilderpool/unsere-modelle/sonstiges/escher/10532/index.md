---
layout: "image"
title: "Escher's Belvedere (side view)"
date: "2007-05-28T11:17:00"
picture: "belvedere_side.jpg"
weight: "1"
konstrukteure: 
- "Martijn Kerkhof"
fotografen:
- "Martijn Kerkhof"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "martijn"
license: "unknown"
legacy_id:
- details/10532
imported:
- "2019"
_4images_image_id: "10532"
_4images_cat_id: "902"
_4images_user_id: "531"
_4images_image_date: "2007-05-28T11:17:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10532 -->
