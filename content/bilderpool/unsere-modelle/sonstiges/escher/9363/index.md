---
layout: "image"
title: "penrosestairs"
date: "2007-03-10T15:40:50"
picture: "IMG_1713.jpg"
weight: "7"
konstrukteure: 
- "Martijn Kerkhof"
fotografen:
- "Martijn Kerkhof"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "martijn"
license: "unknown"
legacy_id:
- details/9363
imported:
- "2019"
_4images_image_id: "9363"
_4images_cat_id: "902"
_4images_user_id: "531"
_4images_image_date: "2007-03-10T15:40:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9363 -->
Penrose (infinite) stairs top view.