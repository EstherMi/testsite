---
layout: "image"
title: "FT-Waterlelie   Poederoyen NL"
date: "2007-08-03T16:12:02"
picture: "FT-Diafragma-dak-constructie_021.jpg"
weight: "7"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/11271
imported:
- "2019"
_4images_image_id: "11271"
_4images_cat_id: "1028"
_4images_user_id: "22"
_4images_image_date: "2007-08-03T16:12:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11271 -->
