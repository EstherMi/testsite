---
layout: "image"
title: "Elefant teilzerlegt"
date: "2006-12-20T19:22:17"
picture: "Elefant_5.jpg"
weight: "5"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/8004
imported:
- "2019"
_4images_image_id: "8004"
_4images_cat_id: "746"
_4images_user_id: "328"
_4images_image_date: "2006-12-20T19:22:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8004 -->
Spätestens diese Ansicht ist dann doch wieder sehr realistisch und wirkt auf mich wie ein Skelett im Museum. Die Rüssel-Technik ist gut zu erkennen.