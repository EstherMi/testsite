---
layout: "image"
title: "FtApp - Ausgänge ansteuern"
date: "2016-05-22T19:12:43"
picture: "ftapp3.jpg"
weight: "3"
konstrukteure: 
- "Bennik"
fotografen:
- "Bennik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Bennik"
license: "unknown"
legacy_id:
- details/43411
imported:
- "2019"
_4images_image_id: "43411"
_4images_cat_id: "3225"
_4images_user_id: "1549"
_4images_image_date: "2016-05-22T19:12:43"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43411 -->
Die 8 Ausgänge lassen sich ebenfalls ansteuern. Entweder einzeln oder paarweise als Motor.