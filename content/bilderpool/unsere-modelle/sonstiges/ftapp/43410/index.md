---
layout: "image"
title: "FtApp - Eingänge auslesen"
date: "2016-05-22T19:12:43"
picture: "ftapp2.jpg"
weight: "2"
konstrukteure: 
- "Bennik"
fotografen:
- "Bennik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Bennik"
license: "unknown"
legacy_id:
- details/43410
imported:
- "2019"
_4images_image_id: "43410"
_4images_cat_id: "3225"
_4images_user_id: "1549"
_4images_image_date: "2016-05-22T19:12:43"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43410 -->
Man kann alle 8 Eingänge auslesen und den Modus einstellen (Analog 5kOhm, Digital 10V, ...). Für besondere Sensoren, wie der NTC Sensor, kann der Wert auch sofort umgerechnet werden.