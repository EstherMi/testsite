---
layout: "image"
title: "FtApp - Camera einsehen"
date: "2016-05-22T19:12:43"
picture: "ftapp4.jpg"
weight: "4"
konstrukteure: 
- "Bennik"
fotografen:
- "Bennik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Bennik"
license: "unknown"
legacy_id:
- details/43412
imported:
- "2019"
_4images_image_id: "43412"
_4images_cat_id: "3225"
_4images_user_id: "1549"
_4images_image_date: "2016-05-22T19:12:43"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43412 -->
Wenn eine Kamera angeschlossen ist, kann man das aktuelle Bild anschauen.