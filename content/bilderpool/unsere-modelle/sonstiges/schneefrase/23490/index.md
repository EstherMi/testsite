---
layout: "image"
title: "(7) Garagenstellung"
date: "2009-03-23T07:37:10"
picture: "7_Garagenstellung.jpg"
weight: "7"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Schneefräse"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/23490
imported:
- "2019"
_4images_image_id: "23490"
_4images_cat_id: "1602"
_4images_user_id: "765"
_4images_image_date: "2009-03-23T07:37:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23490 -->
Damit die Fräse bei Nichtgebrauch nicht so viel Platz braucht, läßt sich das Griffgestänge einfach durch Lösen zweier Riegel klappen.