---
layout: "comment"
hidden: true
title: "8817"
date: "2009-03-23T19:11:10"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Sehr schön! 
Und mal ein ganz neuer Maßstab, so etwa 1:4 schätze ich? 
Bei der Blattstellung spielen vermutlich eine Menge 
Sachen mit, von der Drehzahl bis zur Temperatur und Klebrigkeit des Schnees. 

Gruß,
Harald