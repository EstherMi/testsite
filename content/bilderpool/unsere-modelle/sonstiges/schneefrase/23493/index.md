---
layout: "image"
title: "(10) Beleuchtung"
date: "2009-03-23T07:37:10"
picture: "10_Beleuchtung.jpg"
weight: "10"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Schneefräse"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/23493
imported:
- "2019"
_4images_image_id: "23493"
_4images_cat_id: "1602"
_4images_user_id: "765"
_4images_image_date: "2009-03-23T07:37:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23493 -->
Um im Finstern auch was sehen zu können, sind zwei leistungsfähige höhenverstellbare Scheinwerfer auf der Fräse montiert.