---
layout: "image"
title: "(6) Unterseite"
date: "2009-03-23T07:37:10"
picture: "6_Unterseite.jpg"
weight: "6"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Schneefräse"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/23489
imported:
- "2019"
_4images_image_id: "23489"
_4images_cat_id: "1602"
_4images_user_id: "765"
_4images_image_date: "2009-03-23T07:37:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23489 -->
