---
layout: "image"
title: "(5) von hinten"
date: "2009-03-23T07:37:10"
picture: "5_von_hinten.jpg"
weight: "5"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Schneefräse"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/23488
imported:
- "2019"
_4images_image_id: "23488"
_4images_cat_id: "1602"
_4images_user_id: "765"
_4images_image_date: "2009-03-23T07:37:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23488 -->
Energieversorgung: 8 x 1,2 V; abgesichert durch Multifuse (blauer + grüner Stecker)
Rechter Griff: Taster für die Fräse