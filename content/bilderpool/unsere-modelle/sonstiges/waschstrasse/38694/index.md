---
layout: "image"
title: "Pneumatik"
date: "2014-04-27T16:09:00"
picture: "waschstrasse12.jpg"
weight: "12"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- details/38694
imported:
- "2019"
_4images_image_id: "38694"
_4images_cat_id: "2888"
_4images_user_id: "1635"
_4images_image_date: "2014-04-27T16:09:00"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38694 -->
Dieser Zylinder hebt das Unterbodenkissen.