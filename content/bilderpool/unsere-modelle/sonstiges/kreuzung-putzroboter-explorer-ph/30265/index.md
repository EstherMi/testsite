---
layout: "image"
title: "Die Kamera und die LEDs"
date: "2011-03-15T20:25:17"
picture: "einekreuzungvonputzroboterundexplorervonph04.jpg"
weight: "4"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- details/30265
imported:
- "2019"
_4images_image_id: "30265"
_4images_cat_id: "2250"
_4images_user_id: "1275"
_4images_image_date: "2011-03-15T20:25:17"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30265 -->
LEDs an