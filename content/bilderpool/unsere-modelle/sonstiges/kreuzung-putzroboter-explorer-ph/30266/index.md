---
layout: "image"
title: "Eine LED"
date: "2011-03-15T20:25:17"
picture: "einekreuzungvonputzroboterundexplorervonph05.jpg"
weight: "5"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- details/30266
imported:
- "2019"
_4images_image_id: "30266"
_4images_cat_id: "2250"
_4images_user_id: "1275"
_4images_image_date: "2011-03-15T20:25:17"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30266 -->
So sieht eine LED einzeln aus.