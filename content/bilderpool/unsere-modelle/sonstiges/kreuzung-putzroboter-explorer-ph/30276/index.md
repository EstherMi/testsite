---
layout: "image"
title: "Der Joystick"
date: "2011-03-15T20:25:17"
picture: "einekreuzungvonputzroboterundexplorervonph15.jpg"
weight: "15"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- details/30276
imported:
- "2019"
_4images_image_id: "30276"
_4images_cat_id: "2250"
_4images_user_id: "1275"
_4images_image_date: "2011-03-15T20:25:17"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30276 -->
..und der Lichtschalter+Hupenschalter