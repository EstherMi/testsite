---
layout: "overview"
title: "Eine Kreuzung von Putzroboter und Explorer von PH"
date: 2019-12-17T19:37:54+01:00
legacy_id:
- categories/2250
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2250 --> 
Eine kreuzung von Explorer und Putzmaschiene
Mit:

2 TX,
1 IF,
2 EX 0/1,
Joystick,
Anzeigen,
und natürlich Putzlappen.