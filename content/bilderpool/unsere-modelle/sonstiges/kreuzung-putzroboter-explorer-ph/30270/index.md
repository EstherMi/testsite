---
layout: "image"
title: "Die Motoren für den Antrieb"
date: "2011-03-15T20:25:17"
picture: "einekreuzungvonputzroboterundexplorervonph09.jpg"
weight: "9"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- details/30270
imported:
- "2019"
_4images_image_id: "30270"
_4images_cat_id: "2250"
_4images_user_id: "1275"
_4images_image_date: "2011-03-15T20:25:17"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30270 -->
Hier sind viele Grundbausteine draufgegengen