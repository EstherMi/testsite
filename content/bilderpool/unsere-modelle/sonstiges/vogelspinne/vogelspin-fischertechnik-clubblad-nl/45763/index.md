---
layout: "image"
title: "fischertechnik-Spin met Encodermotor"
date: "2017-04-15T13:24:47"
picture: "vogelspinfischertechnikclubbladnl07.jpg"
weight: "7"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/45763
imported:
- "2019"
_4images_image_id: "45763"
_4images_cat_id: "3401"
_4images_user_id: "22"
_4images_image_date: "2017-04-15T13:24:47"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45763 -->
Uitgebreide info met achtergronden :  fischertechnik-clubblad nr1 april 2017