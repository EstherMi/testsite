---
layout: "image"
title: "Tino-Vogelspin-Z30-1"
date: "2017-04-15T13:24:47"
picture: "vogelspinfischertechnikclubbladnl01.jpg"
weight: "1"
konstrukteure: 
- "Tino Werner"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/45757
imported:
- "2019"
_4images_image_id: "45757"
_4images_cat_id: "3401"
_4images_user_id: "22"
_4images_image_date: "2017-04-15T13:24:47"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45757 -->
Uitgebreide info met achtergronden :  fischertechnik-clubblad nr1 april 2017