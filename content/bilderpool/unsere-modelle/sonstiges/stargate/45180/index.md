---
layout: "image"
title: "Krankenstation"
date: "2017-02-11T21:15:13"
picture: "stargate38.jpg"
weight: "38"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- details/45180
imported:
- "2019"
_4images_image_id: "45180"
_4images_cat_id: "3365"
_4images_user_id: "2240"
_4images_image_date: "2017-02-11T21:15:13"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45180 -->
Das Bett besteht aus einer Plexiglasscheibe unter der sich zwei blaue LEDs befinden.
