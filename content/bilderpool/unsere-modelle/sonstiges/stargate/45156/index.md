---
layout: "image"
title: "Seitentür"
date: "2017-02-11T21:15:13"
picture: "stargate14.jpg"
weight: "14"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- details/45156
imported:
- "2019"
_4images_image_id: "45156"
_4images_cat_id: "3365"
_4images_user_id: "2240"
_4images_image_date: "2017-02-11T21:15:13"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45156 -->
An der Innerseite befindet sich auch ein Taster zum öffnen der Tür.