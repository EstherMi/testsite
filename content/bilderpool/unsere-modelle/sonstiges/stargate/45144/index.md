---
layout: "image"
title: "Der Gateraum"
date: "2017-02-11T21:15:12"
picture: "stargate02.jpg"
weight: "2"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- details/45144
imported:
- "2019"
_4images_image_id: "45144"
_4images_cat_id: "3365"
_4images_user_id: "2240"
_4images_image_date: "2017-02-11T21:15:12"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45144 -->
Das Kernstück der Anlage