---
layout: "image"
title: "Die Elektronik"
date: "2017-02-11T21:15:13"
picture: "stargate20.jpg"
weight: "20"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- details/45162
imported:
- "2019"
_4images_image_id: "45162"
_4images_cat_id: "3365"
_4images_user_id: "2240"
_4images_image_date: "2017-02-11T21:15:13"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45162 -->
Alles verschwindet unter dem herausnehmbaren Viereck im Boden vor dem Gate.