---
layout: "image"
title: "Gateraum"
date: "2017-02-11T21:15:13"
picture: "stargate16.jpg"
weight: "16"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- details/45158
imported:
- "2019"
_4images_image_id: "45158"
_4images_cat_id: "3365"
_4images_user_id: "2240"
_4images_image_date: "2017-02-11T21:15:13"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45158 -->
Mit aktivem Alarm, die roten Leds über der Tür und im Steuerpult blinken.