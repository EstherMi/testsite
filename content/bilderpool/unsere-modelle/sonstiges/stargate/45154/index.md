---
layout: "image"
title: "Seitentür"
date: "2017-02-11T21:15:13"
picture: "stargate12.jpg"
weight: "12"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- details/45154
imported:
- "2019"
_4images_image_id: "45154"
_4images_cat_id: "3365"
_4images_user_id: "2240"
_4images_image_date: "2017-02-11T21:15:13"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45154 -->
Von außen, der Taster links ist zum öffnen der Tür, sie schließt automatisch wieder.