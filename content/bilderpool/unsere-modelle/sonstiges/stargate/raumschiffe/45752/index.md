---
layout: "image"
title: "Befestigung Heck Luke"
date: "2017-04-14T22:49:00"
picture: "raumschiff12.jpg"
weight: "12"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- details/45752
imported:
- "2019"
_4images_image_id: "45752"
_4images_cat_id: "3400"
_4images_user_id: "2240"
_4images_image_date: "2017-04-14T22:49:00"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45752 -->
