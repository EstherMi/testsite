---
layout: "image"
title: "Gate Kontrollpult"
date: "2017-02-11T21:15:12"
picture: "stargate05.jpg"
weight: "5"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- details/45147
imported:
- "2019"
_4images_image_id: "45147"
_4images_cat_id: "3365"
_4images_user_id: "2240"
_4images_image_date: "2017-02-11T21:15:12"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45147 -->
Das "DHD", es dient zur Kontrolle des Gates und des Schutzschilds