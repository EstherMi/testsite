---
layout: "image"
title: "Schlüsselbrett"
date: "2011-01-07T12:33:10"
picture: "schluesselbrett1.jpg"
weight: "15"
konstrukteure: 
- "T.Endlich (Endlichs Bruder)"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/29639
imported:
- "2019"
_4images_image_id: "29639"
_4images_cat_id: "323"
_4images_user_id: "1162"
_4images_image_date: "2011-01-07T12:33:10"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29639 -->
Hier ein Fischertechnik Schlüsselbrett. Die Idee war meine, aber mein Bruder hat es gebaut ;)
Dieses Schlüsselbrett besitzt 4 Haken und jede Menge Flachplatten. Die Flachplatten kann man natürlich beliebig verändern oder ein anderes Muster machen.
Ich hoffe euch gefällts.

MfG
Endlich, T.Endlich