---
layout: "image"
title: "Waage vertauscht belastet"
date: "2018-04-01T11:22:58"
picture: "experimentellernachweisdunklermaterie7.jpg"
weight: "7"
konstrukteure: 
- "Laborteam DHMO Institute of New Mexico"
fotografen:
- "Laborteam DHMO Institute of New Mexico"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/47405
imported:
- "2019"
_4images_image_id: "47405"
_4images_cat_id: "3502"
_4images_user_id: "1557"
_4images_image_date: "2018-04-01T11:22:58"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47405 -->
Vertauscht man die Probestücke auf den Waagschalen, so neigt sich die Waage stets in Richtung des dunkleren Probestücks. Auf diese Art werden Fehler der Waage oder beim Wägevorgang ausgeschlossen.