---
layout: "image"
title: "Waage unbelastet"
date: "2018-04-01T11:22:58"
picture: "experimentellernachweisdunklermaterie3.jpg"
weight: "3"
konstrukteure: 
- "Laborteam DHMO Institute of New Mexico"
fotografen:
- "Laborteam DHMO Institute of New Mexico"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/47401
imported:
- "2019"
_4images_image_id: "47401"
_4images_cat_id: "3502"
_4images_user_id: "1557"
_4images_image_date: "2018-04-01T11:22:58"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47401 -->
Die unbelastete Waage ist korrekt ausbalanciert.