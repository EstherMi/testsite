---
layout: "image"
title: "Waage vertauscht belastet - Zeiger"
date: "2018-04-01T11:22:58"
picture: "experimentellernachweisdunklermaterie8.jpg"
weight: "8"
konstrukteure: 
- "Laborteam DHMO Institute of New Mexico"
fotografen:
- "Laborteam DHMO Institute of New Mexico"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/47406
imported:
- "2019"
_4images_image_id: "47406"
_4images_cat_id: "3502"
_4images_user_id: "1557"
_4images_image_date: "2018-04-01T11:22:58"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47406 -->
Wertet man die Reproduzierbarkeit sowie die Symmetrie der Zeigerausschläge (siehe Waage belastet - Zeiger) aus, erhält man noch eine Aussage über die Qualität der Waage.