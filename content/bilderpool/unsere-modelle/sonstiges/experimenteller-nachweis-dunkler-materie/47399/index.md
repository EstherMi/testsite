---
layout: "image"
title: "Probekörper (1)"
date: "2018-04-01T11:22:58"
picture: "experimentellernachweisdunklermaterie1.jpg"
weight: "1"
konstrukteure: 
- "Laborteam DHMO Institute of New Mexico"
fotografen:
- "Laborteam DHMO Institute of New Mexico"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/47399
imported:
- "2019"
_4images_image_id: "47399"
_4images_cat_id: "3502"
_4images_user_id: "1557"
_4images_image_date: "2018-04-01T11:22:58"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47399 -->
Im Bild sind zwei volumengleiche Probekörper zu sehen, die sich in der Farbe leicht unterscheiden. Der Probekörper links im Bild ist etwas dunkler. Gemäß der Theorie enthält er daher mehr Dunkle Materie und muss erwartungsgemäß schwerer sein.