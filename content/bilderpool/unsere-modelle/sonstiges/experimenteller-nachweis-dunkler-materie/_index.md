---
layout: "overview"
title: "Experimenteller Nachweis Dunkler Materie"
date: 2019-12-17T19:40:30+01:00
legacy_id:
- categories/3502
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3502 --> 
An dieser Stelle einen herzlichen Dank an Frau Dr. Dr. N. Hell-Seeker, die mir die hochauflösenden Bilder der Versuchsapparatur zur Verfügung gestellt hat.