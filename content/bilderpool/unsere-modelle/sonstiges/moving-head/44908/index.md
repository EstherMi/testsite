---
layout: "image"
title: "Bau des beweglichen Kopfes"
date: "2016-12-15T17:20:56"
picture: "movinghead10.jpg"
weight: "10"
konstrukteure: 
- "Jahnn"
fotografen:
- "Jahnn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jahnn"
license: "unknown"
legacy_id:
- details/44908
imported:
- "2019"
_4images_image_id: "44908"
_4images_cat_id: "3341"
_4images_user_id: "2327"
_4images_image_date: "2016-12-15T17:20:56"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44908 -->
Hier wurde der Kopf auf einem "Montageständer" aufgebaut. Innen ist die Lupe zu sehen