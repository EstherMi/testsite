---
layout: "image"
title: "Drehantrieb mit Schrittzähler"
date: "2016-12-15T17:20:57"
picture: "movinghead16.jpg"
weight: "16"
konstrukteure: 
- "Jahnn"
fotografen:
- "Jahnn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jahnn"
license: "unknown"
legacy_id:
- details/44914
imported:
- "2019"
_4images_image_id: "44914"
_4images_cat_id: "3341"
_4images_user_id: "2327"
_4images_image_date: "2016-12-15T17:20:57"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44914 -->
Der Drehantrieb mit Schrittzähler