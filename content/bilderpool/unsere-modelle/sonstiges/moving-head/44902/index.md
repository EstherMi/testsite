---
layout: "image"
title: "LED-Modul von vorne"
date: "2016-12-15T17:20:56"
picture: "movinghead04.jpg"
weight: "4"
konstrukteure: 
- "Jahnn"
fotografen:
- "Jahnn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jahnn"
license: "unknown"
legacy_id:
- details/44902
imported:
- "2019"
_4images_image_id: "44902"
_4images_cat_id: "3341"
_4images_user_id: "2327"
_4images_image_date: "2016-12-15T17:20:56"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44902 -->
LED-Modul von vorne