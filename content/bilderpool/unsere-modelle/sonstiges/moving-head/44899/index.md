---
layout: "image"
title: "Moving-Head"
date: "2016-12-15T17:20:56"
picture: "movinghead01.jpg"
weight: "1"
konstrukteure: 
- "Jahnn"
fotografen:
- "Jahnn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jahnn"
license: "unknown"
legacy_id:
- details/44899
imported:
- "2019"
_4images_image_id: "44899"
_4images_cat_id: "3341"
_4images_user_id: "2327"
_4images_image_date: "2016-12-15T17:20:56"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44899 -->
So wird der fertige Moving-Head aussehen.
Moving-Heads sind Licht-Effekte aus der Veranstaltungstechnik und werden z.B. auf Bühnen und in Fernsehshows eingesetzt.