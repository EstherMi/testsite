---
layout: "image"
title: "Zerlegte Taschenlampe als Lichtquelle"
date: "2016-12-15T17:20:56"
picture: "movinghead02.jpg"
weight: "2"
konstrukteure: 
- "Jahnn"
fotografen:
- "Jahnn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jahnn"
license: "unknown"
legacy_id:
- details/44900
imported:
- "2019"
_4images_image_id: "44900"
_4images_cat_id: "3341"
_4images_user_id: "2327"
_4images_image_date: "2016-12-15T17:20:56"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44900 -->
Die LEDs dieser einfachen Taschenlampe werden das Licht-Muster erzeugen