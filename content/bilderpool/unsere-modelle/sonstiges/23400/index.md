---
layout: "image"
title: "Perpetuum Mobile"
date: "2009-03-06T21:42:10"
picture: "perpetuummobile1.jpg"
weight: "18"
konstrukteure: 
- "nula"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- details/23400
imported:
- "2019"
_4images_image_id: "23400"
_4images_cat_id: "323"
_4images_user_id: "592"
_4images_image_date: "2009-03-06T21:42:10"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23400 -->
Ein scheinbares Perpetuum Mobile.
Auf den 2. Blick ist klar, warums nicht geht...