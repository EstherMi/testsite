---
layout: "image"
title: "Horst 6"
date: "2010-07-06T14:06:23"
picture: "horstdieempfangsdame06.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/27692
imported:
- "2019"
_4images_image_id: "27692"
_4images_cat_id: "1993"
_4images_user_id: "1162"
_4images_image_date: "2010-07-06T14:06:23"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27692 -->
Der Körper von der Seite, der hintere Arm hat einen Taster zum Grüßen und der andere Arm hat eine Anschlussmöglichkeit.