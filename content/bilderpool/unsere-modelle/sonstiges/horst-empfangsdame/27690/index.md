---
layout: "image"
title: "Horst 4"
date: "2010-07-06T14:06:23"
picture: "horstdieempfangsdame04.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/27690
imported:
- "2019"
_4images_image_id: "27690"
_4images_cat_id: "1993"
_4images_user_id: "1162"
_4images_image_date: "2010-07-06T14:06:23"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27690 -->
Die Füße und Knie von Horst. Die Füße sind um 90° angewinkelt.