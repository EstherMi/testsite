---
layout: "image"
title: "Sarja mit Flügeln"
date: "2009-12-06T19:38:51"
picture: "sarjaupdate01.jpg"
weight: "1"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- details/25897
imported:
- "2019"
_4images_image_id: "25897"
_4images_cat_id: "1713"
_4images_user_id: "969"
_4images_image_date: "2009-12-06T19:38:51"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25897 -->
Sarja ist etwas gewachsen.
Hinzugekommen sind die "Solarpanele", welche das Original im All mit Strom versorgen, sowie die Speichertanks und der Rest de Korpus.
Das Modell hat eine Spannweite von 117 cm und ist 62 cm lang.