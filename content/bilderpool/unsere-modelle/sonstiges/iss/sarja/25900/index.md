---
layout: "image"
title: "Sarja mit Außentanks"
date: "2009-12-06T19:38:52"
picture: "sarjaupdate04.jpg"
weight: "4"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- details/25900
imported:
- "2019"
_4images_image_id: "25900"
_4images_cat_id: "1713"
_4images_user_id: "969"
_4images_image_date: "2009-12-06T19:38:52"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25900 -->
Unter den grauen Fachträgern befinden sich die Außentanks. Es sind 16 an der Zahl, die leider keine Luftspeicher ein konnten, da dies nicht gepasst hätte. Also habe ich aus Winkelsteinen 60, Bauplatten 5 und Statik-Streben diese Tanks nachgebaut.