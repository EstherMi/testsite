---
layout: "image"
title: "Sarja aufgeklappt"
date: "2009-09-07T21:17:19"
picture: "sarja4.jpg"
weight: "20"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- details/24888
imported:
- "2019"
_4images_image_id: "24888"
_4images_cat_id: "1713"
_4images_user_id: "969"
_4images_image_date: "2009-09-07T21:17:19"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24888 -->
Was hier zu sehen ist, ist natürlich bei dem echten Modul nicht möglich. Der Anschaubarkeit halber habe ich diesen Mechanismus eingebaut.
Das ganze funktioniert über sechs kleine Stab-Magneten-Paare, die genau in die Nut der Fischertechnikteile passen (wer welche beschaffen möchte: supermagnete.de und dann S-04-10-N suchen). Ich glaube die müssten einen Durchmesser von 4 mm haben (habe gerade kein geeignetes Messgerät zur Hand).

Wichtig ist, dass die Magnete Nord- und Südpol nicht an ihren Enden haben, sondern halbzylindrisch (gibt es das Wort? Eine bessere Beschreibung fällt mir nicht ein) ausgerichtet sind.