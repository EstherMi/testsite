---
layout: "image"
title: "Luftversorgung"
date: "2009-12-06T19:38:53"
picture: "sarjaupdate08.jpg"
weight: "8"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- details/25904
imported:
- "2019"
_4images_image_id: "25904"
_4images_cat_id: "1713"
_4images_user_id: "969"
_4images_image_date: "2009-12-06T19:38:53"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25904 -->
Und das ganze noch eimal aus einer anderen Perspektive.
Der Kolben wird von einer alten Kurbelwelle angetrieben und ist mit drei Gelenkwürfeln an dem Korpus von Sarja befestigt.
Damit man die Welle evtl. mal auswechseln kann (z.B. um mehr Kolben zu befestigen), habe ich sie auch oben mit einem Gelenwürfel befestigt.