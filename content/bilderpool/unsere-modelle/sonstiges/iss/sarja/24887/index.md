---
layout: "image"
title: "Sensor an der Luke"
date: "2009-09-07T21:17:19"
picture: "sarja3.jpg"
weight: "19"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- details/24887
imported:
- "2019"
_4images_image_id: "24887"
_4images_cat_id: "1713"
_4images_user_id: "969"
_4images_image_date: "2009-09-07T21:17:19"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24887 -->
Die Luke soll natürlich automatisch stoppen. Einmal beim aufgehen (hier mit Fototransistor gelößt) und einmal beim schließen (man kann im vorherigen Bild den Taster sehen, der bei geschlossener Luke betätigt wird).