---
layout: "image"
title: "Sarja - Das erste Modul der internationalen Raumstation (ISS)"
date: "2009-09-07T21:17:18"
picture: "sarja1.jpg"
weight: "17"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- details/24885
imported:
- "2019"
_4images_image_id: "24885"
_4images_cat_id: "1713"
_4images_user_id: "969"
_4images_image_date: "2009-09-07T21:17:18"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24885 -->
Hier wird im laufe der Zeit eine komplette Raumstation entstehen. Ich bemühe sich sie so Maßstabsgerecht wie möglich zu bauen, werde dabei aber immer die einfacheren Lösungen bevorzugen.
Das hier ist der Rohbau des ersten Moduls, das am 20.11.1998 ins All geschossen wurde. Weitere Infos gibt's dann auf Wikipedia.

Zum Bau: Ich habe vor die einzelnen Module so aufzubauen, dass man sie alle zur ISS verbinden kann, sowie in jedes einzelne hineinschauen kann (es wird also auch noch eine Inneneinrichtung geben). Hierbei orientiere ich mich an Bildern die einerseits von der Nasa sind, andererseits auf Homepages der entsprechenden Landesraumfahrtsbehörde zu finden sind. Die Informationen für meinen Rohbau habe ich größtenteils von russianspaceweb.com entnommen.

Viel Spaß beim anschauen

PS: Das Modell wird weiterentwickelt, braucht aber seine Zeit bis die komplette ISS bei mir im Zimmer steht ;-)