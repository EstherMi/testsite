---
layout: "image"
title: "Verkleidung der Tanks"
date: "2009-12-06T19:38:52"
picture: "sarjaupdate06.jpg"
weight: "6"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- details/25902
imported:
- "2019"
_4images_image_id: "25902"
_4images_cat_id: "1713"
_4images_user_id: "969"
_4images_image_date: "2009-12-06T19:38:52"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25902 -->
Die Verkleidung der Tanks besteht aus Flachträgern, an die mit Klemmhülsen Rastachsen 30 befestigt sind.
Diese Achsen ermöglichen es, dass man die Verkleidung ganz einfach in BS 30 stecken kann (und auch leicht wieder herausnehmen kann).