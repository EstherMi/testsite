---
layout: "image"
title: "Schaufenster Händler"
date: "2007-12-12T07:32:25"
picture: "Peppinghaus.jpg"
weight: "25"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Andreas Tacke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/13035
imported:
- "2019"
_4images_image_id: "13035"
_4images_cat_id: "323"
_4images_user_id: "182"
_4images_image_date: "2007-12-12T07:32:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13035 -->
Ich habe dem Händler, der Firma Peppinghaus in Wolbeck einige Modelle für das Schaufenster zur Verfügung gestellt. Ich finde es ist sehr gelungen.