---
layout: "image"
title: "LED-Modul: Vorderansicht"
date: "2014-01-06T08:31:03"
picture: "ledmodulfuervierstelligesegmentanzeige08.jpg"
weight: "8"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/38015
imported:
- "2019"
_4images_image_id: "38015"
_4images_cat_id: "2827"
_4images_user_id: "1126"
_4images_image_date: "2014-01-06T08:31:03"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38015 -->
