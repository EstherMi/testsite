---
layout: "image"
title: "Tieflader"
date: "2011-05-30T17:12:07"
picture: "tieflader2.jpg"
weight: "2"
konstrukteure: 
- "Tobias"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/30738
imported:
- "2019"
_4images_image_id: "30738"
_4images_cat_id: "2292"
_4images_user_id: "1162"
_4images_image_date: "2011-05-30T17:12:07"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30738 -->
Stütze zum Kurbeln.