---
layout: "image"
title: "Ansicht von Unten"
date: "2015-03-29T20:33:31"
picture: "spurfolger4.jpg"
weight: "4"
konstrukteure: 
- "Gunnar A."
fotografen:
- "G. Andresen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "gunand256"
license: "unknown"
legacy_id:
- details/40691
imported:
- "2019"
_4images_image_id: "40691"
_4images_cat_id: "3058"
_4images_user_id: "2357"
_4images_image_date: "2015-03-29T20:33:31"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40691 -->
Links ist ein ganz normaler IR-spursensor. Rechts daneben kann man den Antrieb mit den beiden Getriebeschnecken erkennen.