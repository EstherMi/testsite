---
layout: "image"
title: "Spurfahrt"
date: "2015-03-29T20:33:31"
picture: "spurfolger6.jpg"
weight: "6"
konstrukteure: 
- "Gunnar A."
fotografen:
- "G. Andresen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "gunand256"
license: "unknown"
legacy_id:
- details/40693
imported:
- "2019"
_4images_image_id: "40693"
_4images_cat_id: "3058"
_4images_user_id: "2357"
_4images_image_date: "2015-03-29T20:33:31"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40693 -->
Die Antriebsräder drehen sich entweder vorwärts oder sie stoppen.
Gerät z.B. die rechte Seite vom Spursensor auf weiss, stoppt der linke Motor. Dadurch dreht sich das Fahrzeug nach links , bis der Spursensor wieder auf dunklem Untergrund ist. Dann drehen sich wieder beide Antriebsräder. usw...
Auf weissem Untergrund steht das Fahrzeug still.