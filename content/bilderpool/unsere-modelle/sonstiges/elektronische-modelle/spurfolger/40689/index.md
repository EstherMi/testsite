---
layout: "image"
title: "rechte Seite"
date: "2015-03-29T20:33:31"
picture: "spurfolger2.jpg"
weight: "2"
konstrukteure: 
- "Gunnar A."
fotografen:
- "G. Andresen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "gunand256"
license: "unknown"
legacy_id:
- details/40689
imported:
- "2019"
_4images_image_id: "40689"
_4images_cat_id: "3058"
_4images_user_id: "2357"
_4images_image_date: "2015-03-29T20:33:31"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40689 -->
Das graue in der Mitte ist ein alter S-Motor.