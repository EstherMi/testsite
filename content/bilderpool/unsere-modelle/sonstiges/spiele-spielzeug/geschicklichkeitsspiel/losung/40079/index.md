---
layout: "image"
title: "Lösung zum Geschicklichkeitsspiel"
date: "2014-12-30T17:33:06"
picture: "loesung1_2.jpg"
weight: "4"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/40079
imported:
- "2019"
_4images_image_id: "40079"
_4images_cat_id: "3009"
_4images_user_id: "182"
_4images_image_date: "2014-12-30T17:33:06"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40079 -->
Bild7