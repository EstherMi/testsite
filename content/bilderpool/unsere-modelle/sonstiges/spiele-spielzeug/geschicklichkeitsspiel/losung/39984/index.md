---
layout: "image"
title: "Geschicklichkeitsspiel mit fischertechnik Lösung"
date: "2014-12-25T13:17:41"
picture: "loesung2_2.jpg"
weight: "14"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/39984
imported:
- "2019"
_4images_image_id: "39984"
_4images_cat_id: "3009"
_4images_user_id: "182"
_4images_image_date: "2014-12-25T13:17:41"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39984 -->
