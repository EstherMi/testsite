---
layout: "image"
title: "Lösung zum Geschicklichkeitsspiel"
date: "2014-12-28T16:55:41"
picture: "loesung2.jpg"
weight: "7"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/40008
imported:
- "2019"
_4images_image_id: "40008"
_4images_cat_id: "3009"
_4images_user_id: "182"
_4images_image_date: "2014-12-28T16:55:41"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40008 -->
Bild 2
