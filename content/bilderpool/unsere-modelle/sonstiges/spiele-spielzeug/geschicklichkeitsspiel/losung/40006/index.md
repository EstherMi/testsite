---
layout: "image"
title: "Bild 5 zum Geschicklichkeitsspiel"
date: "2014-12-28T14:41:04"
picture: "loesung1_3_2.jpg"
weight: "5"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/40006
imported:
- "2019"
_4images_image_id: "40006"
_4images_cat_id: "3009"
_4images_user_id: "182"
_4images_image_date: "2014-12-28T14:41:04"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40006 -->
So hier das 5. Bild, wird Euch die lösung nun klar?