---
layout: "image"
title: "Darda-Aufziehstation Bild 2"
date: "2009-12-13T16:39:59"
picture: "darda-aufziehstation_02.jpg"
weight: "2"
konstrukteure: 
- "Michael W."
fotografen:
- "Michael W."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Arsamenes"
license: "unknown"
legacy_id:
- details/25957
imported:
- "2019"
_4images_image_id: "25957"
_4images_cat_id: "1824"
_4images_user_id: "1027"
_4images_image_date: "2009-12-13T16:39:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25957 -->
