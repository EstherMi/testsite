---
layout: "image"
title: "Rückansicht"
date: "2008-10-10T13:44:08"
picture: "006.jpg"
weight: "19"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/15871
imported:
- "2019"
_4images_image_id: "15871"
_4images_cat_id: "1446"
_4images_user_id: "184"
_4images_image_date: "2008-10-10T13:44:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15871 -->
Die Schalter.

Links ist einer fürs Förderband.
Rechts oben ist der Antrieb des Bulldozers,
darunter ist der Schalter für den Kompressor,
darunter ist einer für´s Licht
und der letzte ist der für die Magnetventile.