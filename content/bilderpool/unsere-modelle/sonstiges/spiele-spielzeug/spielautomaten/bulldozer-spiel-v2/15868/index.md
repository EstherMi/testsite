---
layout: "image"
title: "Gesamt Ansicht"
date: "2008-10-10T12:29:00"
picture: "003.jpg"
weight: "16"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/15868
imported:
- "2019"
_4images_image_id: "15868"
_4images_cat_id: "1446"
_4images_user_id: "184"
_4images_image_date: "2008-10-10T12:29:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15868 -->
Die Funktionen sind genau so wie beim ersten Spiel.
Die Steine rutschen über die Rampe und werden vom Bulldozer nach vorn geschoben.
In den vorderen Schacht fallen die Steine die man sammeln soll.
Die weißen behält man, die schwarzen darf man wieder einsetzen.