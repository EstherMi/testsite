---
layout: "image"
title: "Bausteinspender"
date: "2013-02-14T13:45:40"
picture: "Bausteinspender_rot.jpg"
weight: "9"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/36622
imported:
- "2019"
_4images_image_id: "36622"
_4images_cat_id: "1446"
_4images_user_id: "184"
_4images_image_date: "2013-02-14T13:45:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36622 -->
Auch dieser wurde neu überarbeitet und optisch aufgewertet..
Der Schacht ist im ft-Raster aufgebaut und hat die Innenmaße 60x60.
Da die roten Boxen (130961) nicht genau 60x60mm groß sind, passt alles sehr gut.
Diese gleiten wunderbar durch den Schacht.