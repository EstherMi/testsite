---
layout: "image"
title: "Bausteinspender"
date: "2008-10-10T13:44:10"
picture: "055.jpg"
weight: "68"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/15920
imported:
- "2019"
_4images_image_id: "15920"
_4images_cat_id: "1446"
_4images_user_id: "184"
_4images_image_date: "2008-10-10T13:44:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15920 -->
Ansicht von unten.
Hier sieht man die Elektronik die die Funktionen steuert.