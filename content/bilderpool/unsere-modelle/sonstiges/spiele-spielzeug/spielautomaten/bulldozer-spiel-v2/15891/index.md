---
layout: "image"
title: "Bauphasen"
date: "2008-10-10T13:44:09"
picture: "026.jpg"
weight: "39"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/15891
imported:
- "2019"
_4images_image_id: "15891"
_4images_cat_id: "1446"
_4images_user_id: "184"
_4images_image_date: "2008-10-10T13:44:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15891 -->
Die Arbeitsplattform liegt auf der Statikkonstruktion.
Sie ist so gebaut das sie unter die seitlich montierten geln Bauplatten 16x90 passt.