---
layout: "image"
title: "Bauphasen"
date: "2008-10-10T13:44:09"
picture: "039.jpg"
weight: "52"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/15904
imported:
- "2019"
_4images_image_id: "15904"
_4images_cat_id: "1446"
_4images_user_id: "184"
_4images_image_date: "2008-10-10T13:44:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15904 -->
Den Antrieb habe ich vom Power Bulldozer übernommen.
Der gefällt mir aber überhaupt nicht.
Irgendwie ist die Konstruktion nur Behelf.