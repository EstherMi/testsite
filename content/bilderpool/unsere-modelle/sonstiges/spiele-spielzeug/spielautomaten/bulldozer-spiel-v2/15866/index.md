---
layout: "image"
title: "Version 2 des Bulldozer-Spieles"
date: "2008-10-10T12:29:00"
picture: "001.jpg"
weight: "14"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/15866
imported:
- "2019"
_4images_image_id: "15866"
_4images_cat_id: "1446"
_4images_user_id: "184"
_4images_image_date: "2008-10-10T12:29:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15866 -->
Ganz neu wieder aufgebaut. Andere Farbe, andere Technik und etwas besser. So hoffe ich doch