---
layout: "image"
title: "Mittelteil"
date: "2013-02-14T13:45:40"
picture: "DSCN4958.jpg"
weight: "4"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/36617
imported:
- "2019"
_4images_image_id: "36617"
_4images_cat_id: "1446"
_4images_user_id: "184"
_4images_image_date: "2013-02-14T13:45:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36617 -->
