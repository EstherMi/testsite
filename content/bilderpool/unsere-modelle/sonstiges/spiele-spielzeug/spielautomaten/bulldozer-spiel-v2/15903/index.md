---
layout: "image"
title: "Bauphasen"
date: "2008-10-10T13:44:09"
picture: "038.jpg"
weight: "51"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/15903
imported:
- "2019"
_4images_image_id: "15903"
_4images_cat_id: "1446"
_4images_user_id: "184"
_4images_image_date: "2008-10-10T13:44:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15903 -->
Der Bulldozer in einer frühen Version.