---
layout: "image"
title: "Bauphasen"
date: "2008-10-10T13:44:08"
picture: "017.jpg"
weight: "30"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/15882
imported:
- "2019"
_4images_image_id: "15882"
_4images_cat_id: "1446"
_4images_user_id: "184"
_4images_image_date: "2008-10-10T13:44:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15882 -->
Hier etwas mehr Grundkonstruktion.
Dadurch wird das Ganze fester und man kann es besser transportieren.