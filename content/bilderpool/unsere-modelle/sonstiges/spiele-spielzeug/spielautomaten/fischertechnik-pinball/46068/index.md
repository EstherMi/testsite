---
layout: "image"
title: "8x8 LED Matrix"
date: "2017-07-10T19:44:47"
picture: "piratesofthecaribbian41.jpg"
weight: "40"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/46068
imported:
- "2019"
_4images_image_id: "46068"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:47"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46068 -->
In der Mitte ist der Spannungswandler, um die weißen LED-Stripes zu dimmen. Darunter die vier Bicolor 8x8 Matrix LED Anzeigen von Adafruit. Ich kann damit 3 Farben ausgeben. Rot, Orange und Grün. Links und rechts 2 Soundmodule mit Tönen und Musik von "Fluch der Karibik".