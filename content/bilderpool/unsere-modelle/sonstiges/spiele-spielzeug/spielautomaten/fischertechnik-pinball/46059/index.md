---
layout: "image"
title: "Abdeckung Kugelförderung"
date: "2017-07-10T19:44:42"
picture: "piratesofthecaribbian32.jpg"
weight: "31"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/46059
imported:
- "2019"
_4images_image_id: "46059"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:42"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46059 -->
Die Kugel wird über einen Magnetkugelhalter nach oben mitgenommen.
