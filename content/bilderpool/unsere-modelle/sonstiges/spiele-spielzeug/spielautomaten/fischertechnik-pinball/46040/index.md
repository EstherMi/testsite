---
layout: "image"
title: "Hintere Abdeckung"
date: "2017-07-10T19:44:32"
picture: "piratesofthecaribbian13.jpg"
weight: "13"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/46040
imported:
- "2019"
_4images_image_id: "46040"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:32"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46040 -->
Die hintere Abdeckung besteht aus zwei 500er Bauplatten. Die Frontscheibe ist über die 2 Schrauben hinten verbunden.