---
layout: "image"
title: "I2C-Verteiler"
date: "2017-07-10T19:44:47"
picture: "piratesofthecaribbian46.jpg"
weight: "45"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/46073
imported:
- "2019"
_4images_image_id: "46073"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:47"
_4images_image_order: "46"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46073 -->
I2C-Verteiler: Halter unter:
https://ftcommunity.de/data/downloads/3ddruckdateien/icverteilerhalter.zip