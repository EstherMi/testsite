---
layout: "image"
title: "Spielfläche oben"
date: "2017-07-10T19:44:37"
picture: "piratesofthecaribbian25.jpg"
weight: "25"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/46052
imported:
- "2019"
_4images_image_id: "46052"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:37"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46052 -->
Der Bonus:
Oben ist der Farbsensor verbaut. Er schaltet das RGB-Lämpchen ein.