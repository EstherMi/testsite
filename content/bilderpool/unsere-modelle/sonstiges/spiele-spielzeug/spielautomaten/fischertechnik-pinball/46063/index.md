---
layout: "image"
title: "Verbindung Kopfaufbau mit Flipper"
date: "2017-07-10T19:44:42"
picture: "piratesofthecaribbian36.jpg"
weight: "35"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/46063
imported:
- "2019"
_4images_image_id: "46063"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:42"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46063 -->
Mit Pfostenstecker und Wanne ist die Elektrik verbunden.
