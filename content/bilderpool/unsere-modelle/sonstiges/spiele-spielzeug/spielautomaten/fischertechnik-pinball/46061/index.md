---
layout: "image"
title: "Abschußrampe"
date: "2017-07-10T19:44:42"
picture: "piratesofthecaribbian34.jpg"
weight: "33"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/46061
imported:
- "2019"
_4images_image_id: "46061"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:42"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46061 -->
Hinter dem Flipperfinger seht ihr einen roten Keil. Diesen musste ich anfertigen, da sich die Kugel öfter dahinter verfangen hatte.

3D Druck + Inventor Datei:
https://ftcommunity.de/data/downloads/3ddruckdateien/flipperkeil.zip