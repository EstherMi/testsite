---
layout: "image"
title: "Segmentanzeigen"
date: "2017-07-10T19:44:42"
picture: "piratesofthecaribbian40.jpg"
weight: "39"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/46067
imported:
- "2019"
_4images_image_id: "46067"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:42"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46067 -->
Oben seht ihr die vier 7-Segment Anzeigen von Sparkfun. Dazwischen die weiße LED-Hintergrundbeleuchtung für die Frontscheibe.