---
layout: "image"
title: "Flipper Seite"
date: "2017-07-10T19:44:42"
picture: "piratesofthecaribbian31.jpg"
weight: "30"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/46058
imported:
- "2019"
_4images_image_id: "46058"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:42"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46058 -->
Die Bauplatten lasen sich entfernen. Somit kommt man besser an die Elektronik.