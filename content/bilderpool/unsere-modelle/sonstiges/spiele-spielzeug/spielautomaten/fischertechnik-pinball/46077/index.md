---
layout: "image"
title: "Bei Nacht"
date: "2017-07-10T19:44:47"
picture: "piratesofthecaribbian50.jpg"
weight: "49"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/46077
imported:
- "2019"
_4images_image_id: "46077"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:47"
_4images_image_order: "50"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46077 -->
RGB-Stripes in Aktion.