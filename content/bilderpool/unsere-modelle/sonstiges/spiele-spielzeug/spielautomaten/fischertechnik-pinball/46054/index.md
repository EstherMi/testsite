---
layout: "image"
title: "Spielfläche mittig"
date: "2017-07-10T19:44:37"
picture: "piratesofthecaribbian27.jpg"
weight: "27"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/46054
imported:
- "2019"
_4images_image_id: "46054"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:37"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46054 -->
Der Multiplikator 2x, 4x, 6x, 8x und 10x wird immer dann hochgezählt, wenn links und rechts die beiden Fototransistoren oben ausgelöst haben.