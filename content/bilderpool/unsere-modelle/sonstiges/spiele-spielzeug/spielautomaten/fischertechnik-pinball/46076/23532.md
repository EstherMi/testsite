---
layout: "comment"
hidden: true
title: "23532"
date: "2017-07-16T11:50:48"
uploadBy:
- "DirkW"
license: "unknown"
imported:
- "2019"
---
Hallo Jens,

Die Platine I2C-Output 5 - 24V 8 Bit gibt es als Bausatz, unter http://www.horter.de. Die ROBOPro Datei dazu findest du
im Download unter  https://ftcommunity.de/data/downloads/robopro/i2coutput524v8bit0x20.rpp

Gruß
Dirk