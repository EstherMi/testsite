---
layout: "image"
title: "Flipper frontal"
date: "2017-07-10T19:44:15"
picture: "piratesofthecaribbian02.jpg"
weight: "2"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/46029
imported:
- "2019"
_4images_image_id: "46029"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:15"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46029 -->
Bei den Beinen bin ich vom Maßstab abgewichen. Die Beine sind etwas kürzer gehalten, damit Kinder auch etwas vom Spielfeld sehen können.