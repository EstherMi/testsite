---
layout: "overview"
title: "fischertechnik Pinball"
date: 2019-12-17T19:39:51+01:00
legacy_id:
- categories/3421
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3421 --> 
Modell fischertechnik Pinball "Pirates of the Caribbian"

Der fischertechnik-Flipper Baukasten "516186 ROBO TXT ElectroPneumatic" ist meiner Meinung ein schöner 
Baukasten, welcher etwas unterbewertet ist. Auf unseren Ausstellungen ist der Standard Flipper immer 
das Highlight bei Jung und Alt mit Spiel, Spaß und Spannung. Daher war mein Ziel, diesen zu modifizieren 
und mit Licht, Sound und Mehrspielermodus auszustatten. Ich habe diesen stark modifiziert, damit er einem "echten Flipper" ähnlicher sieht. 

Das Video dazu findet ihr unter: 

https://www.youtube.com/watch?v=tP-TJu1L9cA


Tipps und Tricks zum Bau in der ft:pedia 2017-2

https://ftcommunity.de/ftpedia_ausgaben/ftpedia-2017-2.pdf


3D-Druck Dateien unter:

https://ftcommunity.de/downloads.php?kategorie=3D-Druck+Dateien



Spezifikation:		
		
Bauzeit: 	               ca. 32 Wochen (nicht durchgehend)
Programmierung:     ca.  8 Wochen	
Gewicht:	               7,9 Kg	
Controller:               2x Robo TX Controller	
Stromversorgung:   9 Volt Labornetzteil	
		
Baukasten:		
ROBOTICS "ROBO TX ElectroPneumatic"		
Standardbaukasten + 3-D Teile + Sonderteile + ft Einzelteile		
		
Maße:	Länge = 600 mm	
	Breite  = 290 mm	
	Höhe  = 670 mm	
		
Unterbau:	Makerbeam XL (15 x15 )Aluprofile	
	Flipperscheibe Acrylglas	
		
Controller:	2x Robo TX Controller 9V	
	1x I2C-Output Modul	
		
Sensorik:		
	1x Farbsensor zum Punkte zählen	
	2x Fototransistor zum Punkte zählen	
	1x Fototransistor Kugeln zählen
	
Pneumatik:		
Luftkompressor:	    1x für Luftversorgung	
Ansteuerung Flipper:    2x Magnetventile	
Flipper:	                    2x Pneumatik-Zylinder
	
Lampen:		
Bumper:        2x Lämpchen 9V	
Bonus:         1x RGB Lämpchen	
Flipper:	    2x Lämpchen 9V	
Flippertisch: 1x RGB Stripe 12 Volt	
		
Kopfaufsatz: Opakes Plexiglas mit Backlite Folie	
	1x LED Stripe Weiß für 	
	4x I2C Segmentanzeige (Punkte)	
	4x I2C 8x8 Adafruit Bicolor LED DOT-Matrix	
	4x Lämpchen 9V (Jackpot)	
		
Taster:	2x Taster für Ansteuerung Flipper	
		
Sound:	2x Soundmodule mit 6 versch. Sounds	
		
Kugelrückführung:	Power Motor 125:1	
	Zahnrad mit Rastketten-Magnetkugelhalter	
		
Software:	
RoboPro Version 4.2.4 
Hauptprogramm und 92 Unterprogramme
Anzeige:	Punktestand, Anzahl der Kugeln, X-fach Multiplikator, Bonus,	
	Gold, Anzahl der Spieler, Jackpot, TILT, TOR etc.