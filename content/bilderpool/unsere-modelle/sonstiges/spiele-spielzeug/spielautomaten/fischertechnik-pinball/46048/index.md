---
layout: "image"
title: "Spielfläche"
date: "2017-07-10T19:44:37"
picture: "piratesofthecaribbian21.jpg"
weight: "21"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/46048
imported:
- "2019"
_4images_image_id: "46048"
_4images_cat_id: "3421"
_4images_user_id: "2303"
_4images_image_date: "2017-07-10T19:44:37"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46048 -->
Die Spielfläche wurde mit Teilen aus dem Baukasten "516186 ROBO TXT ElectroPneumatic" gebaut. Als Untergrund dient eine Acrylglasscheibe. Damit lässt sich die Kugel sehr gut spielen.