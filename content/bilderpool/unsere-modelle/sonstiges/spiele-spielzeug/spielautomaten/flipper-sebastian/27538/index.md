---
layout: "image"
title: "Männchen"
date: "2010-06-20T12:18:05"
picture: "flipper06.jpg"
weight: "6"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- details/27538
imported:
- "2019"
_4images_image_id: "27538"
_4images_cat_id: "1977"
_4images_user_id: "791"
_4images_image_date: "2010-06-20T12:18:05"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27538 -->
Trifft die Kugel die Drähte in der Bildmitte, so dreht sich das Männchen ca. 5 sec. lang.