---
layout: "image"
title: "Technik"
date: "2010-06-20T12:18:05"
picture: "flipper02.jpg"
weight: "2"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- details/27534
imported:
- "2019"
_4images_image_id: "27534"
_4images_cat_id: "1977"
_4images_user_id: "791"
_4images_image_date: "2010-06-20T12:18:05"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27534 -->
Der Münzakzeptor sortiert die Münzen mechanisch nach der Größe.