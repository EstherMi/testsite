---
layout: "image"
title: "schwarzes Loch"
date: "2010-06-20T12:18:05"
picture: "flipper09.jpg"
weight: "9"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- details/27541
imported:
- "2019"
_4images_image_id: "27541"
_4images_cat_id: "1977"
_4images_user_id: "791"
_4images_image_date: "2010-06-20T12:18:05"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27541 -->
Fällt die Kugel durch das Loch in der Rampe, gelangt sie in das schwarze Loch. Dort wird sie ca. 5 sec. gehalten und dann von einem Pneumatikzylinder zurück in das Spiel gebracht.