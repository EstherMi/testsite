---
layout: "image"
title: "Flipper"
date: "2010-06-20T12:18:05"
picture: "flipper04.jpg"
weight: "4"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- details/27536
imported:
- "2019"
_4images_image_id: "27536"
_4images_cat_id: "1977"
_4images_user_id: "791"
_4images_image_date: "2010-06-20T12:18:05"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27536 -->
Die Finger werden von dem Spieler mechanisch betätigt, so kann man stärker und schwächer schießen. Am rechten Rand ist die Startrampe zu sehen. Links oben befindet sich das schwarze Loch und die Spielanzeige. Der genaue Punktestand kann am PC abgelesen werden.