---
layout: "image"
title: "hier geht´s runter"
date: "2006-02-01T14:21:41"
picture: "DSCN0649.jpg"
weight: "3"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/5702
imported:
- "2019"
_4images_image_id: "5702"
_4images_cat_id: "490"
_4images_user_id: "184"
_4images_image_date: "2006-02-01T14:21:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5702 -->
die Spielsteine runtschen über diese Rampe auf das Feld. Die Rampe ist drehbar.