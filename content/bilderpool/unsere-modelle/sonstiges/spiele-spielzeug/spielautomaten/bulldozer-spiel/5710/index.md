---
layout: "image"
title: "Schacht Detail (1)"
date: "2006-02-01T14:21:51"
picture: "DSCN0647.jpg"
weight: "11"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/5710
imported:
- "2019"
_4images_image_id: "5710"
_4images_cat_id: "490"
_4images_user_id: "184"
_4images_image_date: "2006-02-01T14:21:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5710 -->
so sieht ein Schacht von innen aus