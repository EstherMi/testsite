---
layout: "image"
title: "Ansicht (5)"
date: "2006-02-01T14:21:41"
picture: "DSCN0653.jpg"
weight: "6"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/5705
imported:
- "2019"
_4images_image_id: "5705"
_4images_cat_id: "490"
_4images_user_id: "184"
_4images_image_date: "2006-02-01T14:21:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5705 -->
Der Bulldozer entspricht in etwa dem aus dem Kasten Power Bulldozer. Dieser wurde etwas verlängert .....
Über die gelben Statikstreben wird er angetrieben.