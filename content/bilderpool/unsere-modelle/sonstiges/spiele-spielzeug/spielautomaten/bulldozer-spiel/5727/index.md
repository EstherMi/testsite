---
layout: "image"
title: "Antrieb"
date: "2006-02-01T14:22:01"
picture: "DSCN0675.jpg"
weight: "28"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/5727
imported:
- "2019"
_4images_image_id: "5727"
_4images_cat_id: "490"
_4images_user_id: "184"
_4images_image_date: "2006-02-01T14:22:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5727 -->
Die beiden machen die ganze Arbeit.
Der obere treibt die Förderbänder, der unter den Bulldozer an.