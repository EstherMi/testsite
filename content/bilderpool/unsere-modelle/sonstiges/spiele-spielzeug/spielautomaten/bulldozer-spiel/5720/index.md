---
layout: "image"
title: "Bulldozer (1)"
date: "2006-02-01T14:22:01"
picture: "DSCN0665.jpg"
weight: "21"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/5720
imported:
- "2019"
_4images_image_id: "5720"
_4images_cat_id: "490"
_4images_user_id: "184"
_4images_image_date: "2006-02-01T14:22:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5720 -->
Ansicht von unten. Gut zu erkennen das er keinen eigenen Antrieb besitzt. Ober liegt der Pneumatikzylinder der die Spielsteine auswirft.