---
layout: "image"
title: "Ansicht aus der Sicht der Spieler"
date: "2012-05-21T17:24:21"
picture: "01-DSCN4859.jpg"
weight: "6"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/34978
imported:
- "2019"
_4images_image_id: "34978"
_4images_cat_id: "2590"
_4images_user_id: "184"
_4images_image_date: "2012-05-21T17:24:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34978 -->
Ich habe da in der letzten Tagen ein interessantes Spiel konstruiert.
Ich will es mal "Air Ball" nennen.
Ziel ist es mit Hilfe der Ventilatoren die Bälle in die Tore zu lenken.
Sieht einfach aus, ist es aber bei weitem nicht....

Jeder Spieler hat 4 Ventilatoren zur Verfügung die paarweise angeordnet sind.
Die Luftschrauben können einzeln angesteuert werden. Die Ventilatoren
drehen sich nur paarweise.

Ich habe das Problem das die Luftschrauben nicht genug Druck produzieren.
Vielleicht liegt es daran das ich die im Augenblick noch mit Akkus betreibe.

Mit wie viel Spannung kann ich die S-Motoren auf Dauer beanspruchen?