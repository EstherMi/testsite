---
layout: "image"
title: "Seitenansicht"
date: "2012-05-21T17:24:21"
picture: "02-DSCN4748.jpg"
weight: "7"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/34979
imported:
- "2019"
_4images_image_id: "34979"
_4images_cat_id: "2590"
_4images_user_id: "184"
_4images_image_date: "2012-05-21T17:24:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34979 -->
Ich habe die blauen Propeller gewählt weil diese gut mit den Speichenrädern kombiniert
werden können. So sind die Spieler gegen Verletzungen geschützt. Die kleinen blauen
haben echt scharfe Kanten.....