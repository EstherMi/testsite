---
layout: "image"
title: "Details"
date: "2012-05-21T17:24:21"
picture: "08-DSCN4848.jpg"
weight: "13"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/34985
imported:
- "2019"
_4images_image_id: "34985"
_4images_cat_id: "2590"
_4images_user_id: "184"
_4images_image_date: "2012-05-21T17:24:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34985 -->
In früheren Versionen gab es diese neu ausgebildeten Ecken nicht.
Hier blieben die Bälle oft liegen. Der Luftstrom konnte sie dann nicht erreichen.