---
layout: "image"
title: "Alternative"
date: "2013-02-17T23:43:43"
picture: "DSCN4971.jpg"
weight: "1"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/36636
imported:
- "2019"
_4images_image_id: "36636"
_4images_cat_id: "2590"
_4images_user_id: "184"
_4images_image_date: "2013-02-17T23:43:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36636 -->
Hier eine Alternative zur ersten Version, falls der Luftstrom nicht ausreichen sollte.