---
layout: "image"
title: "Alternative"
date: "2013-02-17T23:43:44"
picture: "DSCN4974.jpg"
weight: "2"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/36637
imported:
- "2019"
_4images_image_id: "36637"
_4images_cat_id: "2590"
_4images_user_id: "184"
_4images_image_date: "2013-02-17T23:43:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36637 -->
Mit zwei Propellern pro Einheit gibts auch zweifachen Druck. Dieses ist die kleinstmögliche Baugröße.