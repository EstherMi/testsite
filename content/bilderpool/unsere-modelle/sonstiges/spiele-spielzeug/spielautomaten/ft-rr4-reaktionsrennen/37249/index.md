---
layout: "image"
title: "ft-RR4-10: Schalt- und Kontrollpult"
date: "2013-08-21T21:30:38"
picture: "l10.jpg"
weight: "10"
konstrukteure: 
- "leonidas"
fotografen:
- "leonidas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "leonidas"
license: "unknown"
legacy_id:
- details/37249
imported:
- "2019"
_4images_image_id: "37249"
_4images_cat_id: "2771"
_4images_user_id: "1516"
_4images_image_date: "2013-08-21T21:30:38"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37249 -->
Das komplette Modell wird von diesem Pult bedient und kontrolliert.
Die Verbindung zwischen Pult und Modell wird über zwei alte Druckerkabel aus der PC-Urzeit hergestellt.