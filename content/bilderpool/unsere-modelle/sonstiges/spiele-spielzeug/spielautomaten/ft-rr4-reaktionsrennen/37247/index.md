---
layout: "image"
title: "ft-RR4-08: Interfaces"
date: "2013-08-21T21:30:38"
picture: "l08.jpg"
weight: "8"
konstrukteure: 
- "leonidas"
fotografen:
- "leonidas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "leonidas"
license: "unknown"
legacy_id:
- details/37247
imported:
- "2019"
_4images_image_id: "37247"
_4images_cat_id: "2771"
_4images_user_id: "1516"
_4images_image_date: "2013-08-21T21:30:38"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37247 -->
Die Interfaces bedienen 3 Motoren, 18 Taster, 24 Lampen und 1 Relais.