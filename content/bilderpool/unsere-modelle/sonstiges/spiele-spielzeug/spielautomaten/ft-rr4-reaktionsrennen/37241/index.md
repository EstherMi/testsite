---
layout: "image"
title: "ft-RR4-02: auf der Bahn"
date: "2013-08-21T21:30:38"
picture: "l02.jpg"
weight: "2"
konstrukteure: 
- "leonidas"
fotografen:
- "leonidas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "leonidas"
license: "unknown"
legacy_id:
- details/37241
imported:
- "2019"
_4images_image_id: "37241"
_4images_cat_id: "2771"
_4images_user_id: "1516"
_4images_image_date: "2013-08-21T21:30:38"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37241 -->
Die Autos werden mit Ketten über die Bahn gezogen.