---
layout: "image"
title: "22 Der Buzzer"
date: "2010-05-31T21:14:40"
picture: "m22.jpg"
weight: "22"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27339
imported:
- "2019"
_4images_image_id: "27339"
_4images_cat_id: "1963"
_4images_user_id: "860"
_4images_image_date: "2010-05-31T21:14:40"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27339 -->
Das ist der Buzzer. Der Taster, der daneben liegt, ist normalerweise in der Öffnung des Buzzers. Wenn man nun oben drauf haut, drückt der Grundbaustein auf den Taster, wenn man wieder loslässt, geht er aufgrund der Feder nach oben.