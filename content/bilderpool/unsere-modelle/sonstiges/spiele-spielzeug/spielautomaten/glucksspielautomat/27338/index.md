---
layout: "image"
title: "21 Not-Aus-Schalter"
date: "2010-05-31T21:14:40"
picture: "m21.jpg"
weight: "21"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27338
imported:
- "2019"
_4images_image_id: "27338"
_4images_cat_id: "1963"
_4images_user_id: "860"
_4images_image_date: "2010-05-31T21:14:40"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27338 -->
Das ist der Not-Aus-Schalter unter dem Förderband. 
Mit ihm kann man den Automaten auch wieder in den betriebsbereiten Zustand setzen, wenn das Geldhaus leer ist. Wenn man es wieder aufgefüllt hat und den Schalter drückt, ist der Automat wieder einsatzbereit.