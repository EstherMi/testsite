---
layout: "image"
title: "07 Gesamtansicht ohne Dach"
date: "2010-05-31T21:14:39"
picture: "m07.jpg"
weight: "7"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27324
imported:
- "2019"
_4images_image_id: "27324"
_4images_cat_id: "1963"
_4images_user_id: "860"
_4images_image_date: "2010-05-31T21:14:39"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27324 -->
Das Dach habe ich hier abgenommen. Man kann jetzt auch erkennen, wie das Geld herausgeschoben wird. Wenn es das Ende der langen Stange erreicht, fällt es auf das Förderband.