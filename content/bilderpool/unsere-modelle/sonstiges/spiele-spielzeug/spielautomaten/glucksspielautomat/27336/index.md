---
layout: "image"
title: "19 Der Powermoter"
date: "2010-05-31T21:14:40"
picture: "m19.jpg"
weight: "19"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27336
imported:
- "2019"
_4images_image_id: "27336"
_4images_cat_id: "1963"
_4images_user_id: "860"
_4images_image_date: "2010-05-31T21:14:40"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27336 -->
Der Powermoter treibt den Drehteller an. 
Das Z40 in der Mitte ist mit der Stange verbunden, die sich dreht. Auf dem Z40 ist eine rote Drehscheibe (so ein rotes Rad), auf dem die drei anderen roten Räder befestigt sind, die zusammen den Drehteller bilden.
Der gelbe Baustein links ist eine Lichtschranke.
Auch hier sind schon alle Kabel entfernt.