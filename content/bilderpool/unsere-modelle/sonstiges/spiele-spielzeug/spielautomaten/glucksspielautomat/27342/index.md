---
layout: "image"
title: "25 Der Drehteller"
date: "2010-05-31T21:14:40"
picture: "m25.jpg"
weight: "25"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27342
imported:
- "2019"
_4images_image_id: "27342"
_4images_cat_id: "1963"
_4images_user_id: "860"
_4images_image_date: "2010-05-31T21:14:40"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27342 -->
...von unten
Aus den drei Löchern kommt die Kugel, wenn sie durch eines der roten Räder fällt und das Spiel geht weiter.
(Leider nicht ganz scharf)