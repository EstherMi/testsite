---
layout: "image"
title: "24 Der Drehteller"
date: "2010-05-31T21:14:40"
picture: "m24.jpg"
weight: "24"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27341
imported:
- "2019"
_4images_image_id: "27341"
_4images_cat_id: "1963"
_4images_user_id: "860"
_4images_image_date: "2010-05-31T21:14:40"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27341 -->
... in seiner Grundform. 
