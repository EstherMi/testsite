---
layout: "image"
title: "15 Abschussrampe"
date: "2010-05-31T21:14:39"
picture: "m15.jpg"
weight: "15"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27332
imported:
- "2019"
_4images_image_id: "27332"
_4images_cat_id: "1963"
_4images_user_id: "860"
_4images_image_date: "2010-05-31T21:14:39"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27332 -->
nochmal die Abschussrampe