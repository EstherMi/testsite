---
layout: "image"
title: "02 Funktionsweise"
date: "2010-05-31T21:14:38"
picture: "m02.jpg"
weight: "2"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27319
imported:
- "2019"
_4images_image_id: "27319"
_4images_cat_id: "1963"
_4images_user_id: "860"
_4images_image_date: "2010-05-31T21:14:38"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27319 -->
Bei (1)  muss man eine 10ct Münze einwerfen, um das Spiel zu starten. Darufhin fängt  die Kette bei (4) an sich zu drehen, wenn sie ganz oben ist 'wirft' sie eine Kugel, die dann auf die Abschussrampe geleitet wird. 
Der Teller bei (6) fängt an sich zu drehen. Nun muss man den Hebel bei (5) ziehen und loslassen, damit die Kugel auf den Drehteller bei (6) geschossen wird. 
Jetzt gibt es zwei Möglichkeiten, in welches Loch die Kugel fällt: sie kann entweder in eines der drei roten Räder (Seriennummer #31019) fallen oder in eine Lücke zwischen ihnen. Wenn sie in eine der Lücken fällt, hat man sofort verloren, wenn sie in eines der Räder fällt, hat man 20ct schon mal sicher gewonnen. 
Nun fangen die Lichter bei (7) an, nacheinander (also im Kreis) zu blinken. Mit dem Buzzer (3) kann man nun das aufeinanderfolgende Blinken der Lampen anhalten. Je nachdem, ob man eine der drei weißen Lampen oder die gelbe Lampe erwischt hat, bekommt man 20 Cent oder das doppelte, also 40 Cent, ausgezahlt. Das Geld, das man ausgezahlt bekommt, befindet sich unter dem "Dach" und fällt dann bei (2) raus, wo man es entnehmen kann.

So kann man immer weiter spielen, bis der Geldspeicher leer ist und man in wieder auffüllen muss.