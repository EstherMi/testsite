---
layout: "image"
title: "03 Gesamtansicht von oben"
date: "2010-05-31T21:14:38"
picture: "m03.jpg"
weight: "3"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27320
imported:
- "2019"
_4images_image_id: "27320"
_4images_cat_id: "1963"
_4images_user_id: "860"
_4images_image_date: "2010-05-31T21:14:38"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27320 -->
Hier kann man die sechs Löcher des Drehtellers und die Abschussrampe besser erkennen.