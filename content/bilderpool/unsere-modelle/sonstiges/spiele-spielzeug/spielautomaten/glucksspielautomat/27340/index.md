---
layout: "image"
title: "23 Die Lampen"
date: "2010-05-31T21:14:40"
picture: "m23.jpg"
weight: "23"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27340
imported:
- "2019"
_4images_image_id: "27340"
_4images_cat_id: "1963"
_4images_user_id: "860"
_4images_image_date: "2010-05-31T21:14:40"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27340 -->
Der Stab mit den vier Lampen. Die Funktionsweise ist schon weiter oben erklärt, wenn man die gelbe Lampe trifft, verdoppelt sich der Gewinn.