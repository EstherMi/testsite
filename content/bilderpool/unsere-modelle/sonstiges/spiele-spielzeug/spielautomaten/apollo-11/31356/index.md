---
layout: "image"
title: "P3020058"
date: "2011-07-24T16:39:18"
picture: "apollo17.jpg"
weight: "17"
konstrukteure: 
- "Dutchbuilder"
fotografen:
- "Dutchbuilder"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- details/31356
imported:
- "2019"
_4images_image_id: "31356"
_4images_cat_id: "2331"
_4images_user_id: "1315"
_4images_image_date: "2011-07-24T16:39:18"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31356 -->
A look at the back of the lunarsurface, in the foreground the interface 
and the extension modules.