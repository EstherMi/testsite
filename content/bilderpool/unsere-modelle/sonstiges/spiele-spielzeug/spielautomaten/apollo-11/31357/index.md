---
layout: "image"
title: "P3020061"
date: "2011-07-24T16:39:18"
picture: "apollo18.jpg"
weight: "18"
konstrukteure: 
- "Dutchbuilder"
fotografen:
- "Dutchbuilder"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- details/31357
imported:
- "2019"
_4images_image_id: "31357"
_4images_cat_id: "2331"
_4images_user_id: "1315"
_4images_image_date: "2011-07-24T16:39:18"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31357 -->
The mechanics of the lunar module, up and down and approaching the command module.