---
layout: "image"
title: "P3020037"
date: "2011-07-24T16:39:17"
picture: "apollo01.jpg"
weight: "1"
konstrukteure: 
- "Dutchbuilder"
fotografen:
- "Dutchbuilder"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- details/31340
imported:
- "2019"
_4images_image_id: "31340"
_4images_cat_id: "2331"
_4images_user_id: "1315"
_4images_image_date: "2011-07-24T16:39:17"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31340 -->
An overview picture with remote control on the left and above the fuel / time 
indicator lights.