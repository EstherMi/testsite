---
layout: "image"
title: "P3020070"
date: "2011-07-24T16:39:18"
picture: "apollo25.jpg"
weight: "25"
konstrukteure: 
- "Dutchbuilder"
fotografen:
- "Dutchbuilder"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- details/31364
imported:
- "2019"
_4images_image_id: "31364"
_4images_cat_id: "2331"
_4images_user_id: "1315"
_4images_image_date: "2011-07-24T16:39:18"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31364 -->
The back of the remote control.