---
layout: "image"
title: "P3020043"
date: "2011-07-24T16:39:18"
picture: "apollo06.jpg"
weight: "6"
konstrukteure: 
- "Dutchbuilder"
fotografen:
- "Dutchbuilder"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- details/31345
imported:
- "2019"
_4images_image_id: "31345"
_4images_cat_id: "2331"
_4images_user_id: "1315"
_4images_image_date: "2011-07-24T16:39:18"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31345 -->
A closeup of the two vehicles, on the command module three lights and on the lunar module
three photocells. They must be exact in line with eachother.