---
layout: "image"
title: "P3020066"
date: "2011-07-24T16:39:18"
picture: "apollo22.jpg"
weight: "22"
konstrukteure: 
- "Dutchbuilder"
fotografen:
- "Dutchbuilder"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- details/31361
imported:
- "2019"
_4images_image_id: "31361"
_4images_cat_id: "2331"
_4images_user_id: "1315"
_4images_image_date: "2011-07-24T16:39:18"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31361 -->
Just a nice picture in space.