---
layout: "image"
title: "P3020040"
date: "2011-07-24T16:39:17"
picture: "apollo03.jpg"
weight: "3"
konstrukteure: 
- "Dutchbuilder"
fotografen:
- "Dutchbuilder"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- details/31342
imported:
- "2019"
_4images_image_id: "31342"
_4images_cat_id: "2331"
_4images_user_id: "1315"
_4images_image_date: "2011-07-24T16:39:17"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31342 -->
Closeup of the remote control.
LM stands for Lunar Module: + = approaching, - = go back.
With the blue lights, the command module can roll to the left or to the right.
With the orange lights, the command module can to be moved up and down.
With the yellow lights, the command module can to be moved to the left and to the right.
The red light is an emergency stop, all processes are stopped.
Note: The lampshades are the buttons.
