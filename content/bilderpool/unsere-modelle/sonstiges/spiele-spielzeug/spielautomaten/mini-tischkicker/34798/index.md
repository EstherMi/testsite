---
layout: "image"
title: "Auffangbehälter für den Ball"
date: "2012-04-14T18:08:47"
picture: "minitischkicker07.jpg"
weight: "7"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- details/34798
imported:
- "2019"
_4images_image_id: "34798"
_4images_cat_id: "2572"
_4images_user_id: "1122"
_4images_image_date: "2012-04-14T18:08:47"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34798 -->
-