---
layout: "image"
title: "Befestigung der Metallstangen"
date: "2012-04-14T18:08:47"
picture: "minitischkicker09.jpg"
weight: "9"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- details/34800
imported:
- "2019"
_4images_image_id: "34800"
_4images_cat_id: "2572"
_4images_user_id: "1122"
_4images_image_date: "2012-04-14T18:08:47"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34800 -->
-