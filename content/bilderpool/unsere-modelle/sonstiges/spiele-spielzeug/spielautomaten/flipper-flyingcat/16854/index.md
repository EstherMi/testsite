---
layout: "image"
title: "Solenoidbefestigung 1"
date: "2009-01-03T19:45:49"
picture: "flipper8.jpg"
weight: "8"
konstrukteure: 
- "flyingcat"
fotografen:
- "flyingcat"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- details/16854
imported:
- "2019"
_4images_image_id: "16854"
_4images_cat_id: "1520"
_4images_user_id: "853"
_4images_image_date: "2009-01-03T19:45:49"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16854 -->
So kriegt man die großen Solenoide an Fischertechnik: Einfach mit der Befestigungsmutter in eine Drehscheibe 60 setzen, das passt genau.