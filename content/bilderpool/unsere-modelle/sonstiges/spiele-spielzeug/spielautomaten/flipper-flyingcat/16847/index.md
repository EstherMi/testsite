---
layout: "image"
title: "Überblick"
date: "2009-01-03T19:45:48"
picture: "flipper1.jpg"
weight: "1"
konstrukteure: 
- "flyingcat"
fotografen:
- "flyingcat"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- details/16847
imported:
- "2019"
_4images_image_id: "16847"
_4images_cat_id: "1520"
_4images_user_id: "853"
_4images_image_date: "2009-01-03T19:45:48"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16847 -->
Noch ist die Spielfläche einfach glatt - die soll natürlich in späteren Bauphasen mit Spielelementen gefüllt werden. Die Abdeckungen links und rechts unten sind für die Photos herausgenommen. Im Loch rechts unten sieht man die Rampe, durch die Kugeln vor die Abschussvorrichtung vorgelegt werden (Detailfoto dazu folgt). Die Flipperfinger in der Mitte sind aus einem richtigen Flipper. Betrieben wird das Gerät mit Stahlkugeln, die 16 mm Durchmesser und 17 Gramm gewicht haben.