---
layout: "image"
title: "Vorlegerampe"
date: "2009-01-03T19:45:48"
picture: "flipper3.jpg"
weight: "3"
konstrukteure: 
- "flyingcat"
fotografen:
- "flyingcat"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- details/16849
imported:
- "2019"
_4images_image_id: "16849"
_4images_cat_id: "1520"
_4images_user_id: "853"
_4images_image_date: "2009-01-03T19:45:48"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16849 -->
Kugeln, die aus dem Spiel fallen, laufen durch einen Vereinzeler und werden dann von einem Solenoid über die hier zu sehende Rampe bei Bedarf vor die Abschussvorrichtung geschossen. Die Abschussvorrichtung selbst ist auch ein Solenoid (Zylindermagnet rechts unten im Bild). Für solche Zwecke kann ich die Magnete von www.tremba.de empfehlen - hier ist der Typ ZMF-2551d.001 im Einsatz, er kann mit der Befestigungsmutter direkt im Loch einer ft-Drehscheibe befestigt werden (Detailfotos weiter hinten). Der Magnet, der die Kugeln die Rampe hochbefördert, ist der gleiche Typ. Die Kraftentfaltung ist enorm.