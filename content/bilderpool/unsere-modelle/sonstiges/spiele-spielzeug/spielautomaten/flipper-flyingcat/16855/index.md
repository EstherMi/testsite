---
layout: "image"
title: "Solenoidbefestigung 2"
date: "2009-01-03T19:45:49"
picture: "flipper9.jpg"
weight: "9"
konstrukteure: 
- "flyingcat"
fotografen:
- "flyingcat"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- details/16855
imported:
- "2019"
_4images_image_id: "16855"
_4images_cat_id: "1520"
_4images_user_id: "853"
_4images_image_date: "2009-01-03T19:45:49"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16855 -->
Noch ein Bild von vorne. Auf die Art kriegt man ernorme Kräfte mit erstaunlicher Leichtigkeit in ft-Modelle. Die Solenoide gibt's auch ziehend statt drückend, dann passt in die Nut des Kerns genau eine Statikstrebe. Davon werde ich wohl bei den Spielelementen ein paar einsetzen.