---
layout: "image"
title: "Vorlegemechanismus"
date: "2009-01-03T19:45:48"
picture: "flipper4.jpg"
weight: "4"
konstrukteure: 
- "flyingcat"
fotografen:
- "flyingcat"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- details/16850
imported:
- "2019"
_4images_image_id: "16850"
_4images_cat_id: "1520"
_4images_user_id: "853"
_4images_image_date: "2009-01-03T19:45:48"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16850 -->
Ein Blick auf den Vorlegemechanismus, Unten etwas links von der Mitte ist der Solenoid, der die Kugeln die Rampe hochschießt. In der Bildmitte ist ein Teil der Rampe zu sehen, die die Kugeln herunterlaufen. Diese Rampe kann von zwei kleineren Solenoiden (HMF-1614d.001) blockiert werden, dadurch werden die Kugeln vereinzelt. Die Schwierigkeit bei den Rampen ist die Schräglage - damit die Kugeln richtig laufen, sind die Rampen ziemlich windschief, was ein aufwendiges Gefrickel mit Winkelsteinen nach sich zieht.