---
layout: "overview"
title: "Flipper (flyingcat)"
date: 2019-12-17T19:39:00+01:00
legacy_id:
- categories/1520
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1520 --> 
Ich habe mir in den Kopf gesetzt, einen Flipper - annähernd in Originalgröße - zu bauen. Das Projekt ist noch lange nicht fertig; dennoch gibt es hier schon Bilder der aktuellen Bauphase zu sehen - vielleicht hat ja jemand Anregungen dazu.