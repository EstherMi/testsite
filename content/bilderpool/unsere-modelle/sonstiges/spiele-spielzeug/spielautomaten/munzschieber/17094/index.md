---
layout: "image"
title: "(15) Antrieb"
date: "2009-01-19T17:21:55"
picture: "Mnzschieber_015.jpg"
weight: "39"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/17094
imported:
- "2019"
_4images_image_id: "17094"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-01-19T17:21:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17094 -->
Antrieb von der anderen Seite; unten die Lampenfassung für die Stromversorgung der Motore; von oben kommt das Kabel für die Beleuchtung