---
layout: "image"
title: "(29) die Schieber"
date: "2009-01-19T17:21:56"
picture: "Mnzschieber_029.jpg"
weight: "52"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/17107
imported:
- "2019"
_4images_image_id: "17107"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-01-19T17:21:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17107 -->
Detailaufnahme der ausgebauten Schieber: Gummi (von einem Fahrradschlauch) auf Platte 90 x 30 (38251) mit Superkleber geklebt.
Das war notwendig, damit auch die Münzen weitertransportiert werden, die unter den Schieber geraten.
Ich habe versucht, dieses Modell nur mit den alten ft-Bauteilen zu bauen. Ganz ist es mir aber nicht gelungen. (Hie und da blitzt etwas Gelbes hervor.)

Nicht Original ft-Teile:
Neben dem Umbau der Schieber sind 2 Stück 320 mm lange 4-mm Achsen eingebaut und die drei durchsichtigen Polystyrolscheiben zum Schutz der Münzen.