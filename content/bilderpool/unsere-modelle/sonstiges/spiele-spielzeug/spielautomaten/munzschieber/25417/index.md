---
layout: "image"
title: "(15) Münzprüfer"
date: "2009-09-28T21:32:25"
picture: "Mnzschieber_215.jpg"
weight: "16"
konstrukteure: 
- "mirose"
fotografen:
- "mirose"
keywords: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/25417
imported:
- "2019"
_4images_image_id: "25417"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-09-28T21:32:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25417 -->
… von oben. Die beiden blauen Teile sind batteriebetriebene Lampen (LED)