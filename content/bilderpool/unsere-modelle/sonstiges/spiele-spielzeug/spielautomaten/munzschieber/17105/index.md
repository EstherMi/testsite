---
layout: "image"
title: "(27) Betrieb"
date: "2009-01-19T17:21:56"
picture: "Mnzschieber_027.jpg"
weight: "50"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/17105
imported:
- "2019"
_4images_image_id: "17105"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-01-19T17:21:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17105 -->
Blick in den Münzschieber, während des Betriebs