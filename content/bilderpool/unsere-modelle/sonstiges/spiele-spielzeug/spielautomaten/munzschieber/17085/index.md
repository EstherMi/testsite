---
layout: "image"
title: "(6) Münzeinwurf"
date: "2009-01-19T17:21:55"
picture: "Mnzschieber_006.jpg"
weight: "30"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/17085
imported:
- "2019"
_4images_image_id: "17085"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-01-19T17:21:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17085 -->
Der Münzeinwurf ist so groß, daß höchstens 20-Cent Münzen eingeworfen werden können. Größere Münzen passen nicht durch den Schlitz.