---
layout: "image"
title: "(11) ohne Verkleidungsplatten"
date: "2009-09-28T21:32:24"
picture: "Mnzschieber_211.jpg"
weight: "12"
konstrukteure: 
- "mirose"
fotografen:
- "mirose"
keywords: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/25413
imported:
- "2019"
_4images_image_id: "25413"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-09-28T21:32:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25413 -->
Die Plastikplatten sind schon abmontiert