---
layout: "image"
title: "(13) Polwendeschalter"
date: "2009-09-28T21:32:25"
picture: "Mnzschieber_213.jpg"
weight: "14"
konstrukteure: 
- "mirose"
fotografen:
- "mirose"
keywords: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/25415
imported:
- "2019"
_4images_image_id: "25415"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-09-28T21:32:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25415 -->
Noch einmal der Polwendeschalter, der für die Hin- und Herbewegung des Rüssels notwendig ist.