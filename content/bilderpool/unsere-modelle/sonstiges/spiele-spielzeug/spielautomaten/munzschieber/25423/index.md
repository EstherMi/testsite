---
layout: "image"
title: "(21) Rüssel"
date: "2009-09-28T21:32:25"
picture: "Mnzschieber_221.jpg"
weight: "22"
konstrukteure: 
- "mirose"
fotografen:
- "mirose"
keywords: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/25423
imported:
- "2019"
_4images_image_id: "25423"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-09-28T21:32:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25423 -->
Über diesen Rüssel rollt das Geld auf die hintere Prallplatte