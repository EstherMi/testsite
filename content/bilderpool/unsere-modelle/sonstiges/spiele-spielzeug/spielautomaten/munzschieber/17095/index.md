---
layout: "image"
title: "(16) Antrieb"
date: "2009-01-19T17:21:55"
picture: "Mnzschieber_016.jpg"
weight: "40"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/17095
imported:
- "2019"
_4images_image_id: "17095"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-01-19T17:21:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17095 -->
Der Antrieb erfolgt mit einem Powermotor 8:1, das das Ritzel in der Bildmitte antreibt, welches in das Zahnrad Z40 eingreift, das die unteren Schieber antreibt. Über die Kette werden die oberen Schieber angetrieben.