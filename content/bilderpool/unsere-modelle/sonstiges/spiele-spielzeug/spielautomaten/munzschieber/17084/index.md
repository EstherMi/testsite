---
layout: "image"
title: "(5) Blick auf das Spielfeld"
date: "2009-01-19T17:21:55"
picture: "Mnzschieber_005.jpg"
weight: "29"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/17084
imported:
- "2019"
_4images_image_id: "17084"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-01-19T17:21:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17084 -->
Noch ein Blick aufs Spielfeld. Drei Münzen liegen zur Entnahme aus dem Gewinnfach bereit, um so lange wieder eingeworfen zu werden, bis nichts mehr herauskommt… Oder kommt doch der große Gewinn?