---
layout: "image"
title: "(8) von hinten, Antriebsseite"
date: "2009-09-28T21:32:24"
picture: "Mnzschieber_208.jpg"
weight: "9"
konstrukteure: 
- "mirose"
fotografen:
- "mirose"
keywords: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/25410
imported:
- "2019"
_4images_image_id: "25410"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-09-28T21:32:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25410 -->
