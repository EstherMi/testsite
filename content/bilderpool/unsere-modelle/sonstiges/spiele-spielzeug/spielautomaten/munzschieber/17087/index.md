---
layout: "image"
title: "(8) Seitenansicht von rechts"
date: "2009-01-19T17:21:55"
picture: "Mnzschieber_008.jpg"
weight: "32"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/17087
imported:
- "2019"
_4images_image_id: "17087"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-01-19T17:21:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17087 -->
Hier sieht man schön die beiden Ebenen mit den je drei Schiebern.

Funktionsweise:
Die Münzen rollen über die obere Schiene, fallen auf das Prallblech aus Kunststoff, fallen auf die obere Ebene, werden mit den Schiebern an die anderen Münzen geschoben. Manchmal fällt dann eine andere Münze auf die untere Ebene, wird vom unteren Schieber an die Münzen geschoben, und dann kann es sein, daß einige Münzen in das Gewinnfach fallen.