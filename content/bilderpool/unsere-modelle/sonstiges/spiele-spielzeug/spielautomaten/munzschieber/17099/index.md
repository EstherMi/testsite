---
layout: "image"
title: "(20) Schiene"
date: "2009-01-19T17:21:56"
picture: "Mnzschieber_020.jpg"
weight: "44"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Münzschieber", "Geld"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/17099
imported:
- "2019"
_4images_image_id: "17099"
_4images_cat_id: "1534"
_4images_user_id: "765"
_4images_image_date: "2009-01-19T17:21:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17099 -->
Noch einmal die Schiene, von der die Münzen auf das Prallblech fallen. Während des Betriebs wird diese Schiene hin- und herbewegt.