---
layout: "image"
title: "Fallziele 8 (noch im Bau)"
date: "2013-03-10T17:46:48"
picture: "bild09_2.jpg"
weight: "84"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36740
imported:
- "2019"
_4images_image_id: "36740"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-03-10T17:46:48"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36740 -->
Die kleinen Platten 15x15 an jedem Fallziel hält die Fallziele oben. Das Gummiband sorgt wie in den großen Flippern dafür, dass das Fallziel nicht so leicht herunterfällt. Wenn man stark genug am Automaten rüttelt, fliegen die Dinger trotzdem runter. Aber das macht nix, ich hab ja eine Tilt-Funktion.