---
layout: "image"
title: "Linke Innen- & Außenbahnen (noch im Bau)"
date: "2013-02-03T10:19:45"
picture: "bild09.jpg"
weight: "118"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36551
imported:
- "2019"
_4images_image_id: "36551"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-03T10:19:45"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36551 -->
Bei der linken Außenbahn dürfte wohl etwas auffallen. Erstens befindet sich über ihr kein Gummi, zweitens ist das Türmchen weit nach unten versetzt und drittens ist, anstelle des Türmchens, eine LED im Spielfeld. Dreimal dürft ihr raten, was hier los ist... :-)

Antwort: Es handelt sich um einen Kickback! Wenn die Kugel hier trifft, und die LED leuchtet oder blinkt, wird sie mittels einer Metallstange wieder auf das Spielfeld zurückgeschossen. Dafür darf die Kugel aber auch öfter hier rein...