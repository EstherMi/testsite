---
layout: "comment"
hidden: true
title: "21058"
date: "2015-09-05T20:07:51"
uploadBy:
- "DirkW"
license: "unknown"
imported:
- "2019"
---
Hallo Phil,

ich habe dein Modell beim 50 Jahre ft-Jubiläum gesehen und du hast mir die einzelnen Funktionen erklärt.
Hut ab. Du bist wirklich sehr am Original geblieben. Mir haben deine Detaillösungen, die Bumper, etc. sehr gefallen und das du nie aufgegeben hast und versuchst hast die Lösung in ft umzusetzen. Tolle Leistung. Weiter so. :-)

Viele Grüße
Dirk