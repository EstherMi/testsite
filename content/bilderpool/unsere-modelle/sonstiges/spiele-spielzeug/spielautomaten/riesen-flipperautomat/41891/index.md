---
layout: "image"
title: "Größenvergleich"
date: "2015-09-02T20:01:59"
picture: "bild15_3.jpg"
weight: "15"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/41891
imported:
- "2019"
_4images_image_id: "41891"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2015-09-02T20:01:59"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41891 -->
:-)
Endlich hab ich auch einen :-))))))))))))))))))))))))))))
Man sieht hier ganz gut, wie nah der "Kleine" an seinen "großen Bruder" herankommt... Ich hatte anfangs aber auch nicht geplant, den ft-Flipper in 1:1-Größe zu bauen, sondern etwas kleiner. Als Maßgabe dienten mir die Kugeln, die ich damals schon hatte. Die sind aber auch nicht ganz so groß wie die Stahlkugeln, die in einem solchen Flipper zum Einsatz kommen.

Noch zum Abschluss was zum Abschuss: Der Flipper links hat nur einen "Auto-Plunger", den man durch das Drücken des "Launch Ball"-Knopfs aktiviert. Sowas gibt es öfter. Aber am meisten findet man Flipper, die eine Kombination aus normalen Abschuss mit Feder und Auto-Plunger besitzen - so auch der aus ft.

...spätestens jetzt kennt jeder eins meiner Lieblingshobbys ;-)