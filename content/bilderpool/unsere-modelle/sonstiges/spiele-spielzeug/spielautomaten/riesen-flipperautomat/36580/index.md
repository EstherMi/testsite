---
layout: "image"
title: "Rechte Rampe 5 (noch im Bau)"
date: "2013-02-04T10:32:24"
picture: "bild5.jpg"
weight: "105"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36580
imported:
- "2019"
_4images_image_id: "36580"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-04T10:32:24"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36580 -->
Der Eingangsbereich der Rampe.