---
layout: "image"
title: "Kickback-Mechanismus (noch im Bau)"
date: "2013-02-03T10:19:45"
picture: "bild10.jpg"
weight: "119"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36552
imported:
- "2019"
_4images_image_id: "36552"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-03T10:19:45"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36552 -->
Ich hab vor, den Kickback mit einem Powermotor anzutreiben. Bei den Testversuchen scheint es zu funktionieren.