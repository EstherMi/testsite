---
layout: "image"
title: "'Roll-Under'"
date: "2015-09-02T20:01:59"
picture: "bild10_5.jpg"
weight: "10"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/41886
imported:
- "2019"
_4images_image_id: "41886"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2015-09-02T20:01:59"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41886 -->
Eigentlich sagt der Tittel alles. Ein Roll-Under, auch Gate genannt, sind Drähte, die von der Kugel nach oben gedrückt werden und so einen Schalter betätigen. Hier ist der Draht der Kupferdraht, der Schalter besteht aus den zwei dünnen Metallstreifen, die sich da im Dunkeln verstecken.

Rechts davon ist ein kleiner schwarzer Gummiring befestigt, der harte Stöße schneller Kugeln dämpfen soll.