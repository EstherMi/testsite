---
layout: "image"
title: "Fallziele 2 (noch im Bau)"
date: "2013-03-10T17:46:48"
picture: "bild03_2.jpg"
weight: "78"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36734
imported:
- "2019"
_4images_image_id: "36734"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-03-10T17:46:48"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36734 -->
Das linke und das mittlere Fallziel wurden bereits "getroffen" (von meinem Finger :-) ).