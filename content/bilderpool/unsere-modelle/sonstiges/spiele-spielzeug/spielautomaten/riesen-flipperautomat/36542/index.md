---
layout: "image"
title: "Gesamtansicht (noch im Bau)"
date: "2013-01-31T23:05:27"
picture: "Gesamtansicht.jpg"
weight: "126"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: ["Flipper", "Flipperautomat"]
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36542
imported:
- "2019"
_4images_image_id: "36542"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-01-31T23:05:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36542 -->
Der ganze Flipper auf einen Blick. Naja, er ist noch im Bau, aber in etwa so weit, dass man die untere Hälfte bespielen kann (wenn die Pappe drauf ist).