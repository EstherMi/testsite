---
layout: "image"
title: "Slingshot 3 (noch im Bau)"
date: "2013-02-17T23:43:44"
picture: "bild4_2.jpg"
weight: "99"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36642
imported:
- "2019"
_4images_image_id: "36642"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-17T23:43:44"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36642 -->
Die Slingshots auf meinem Flipper haben etwas schönes an sich: sie benötigen keinen Ausgang auf dem TX-Controller. Dafür wird aber ein 1000µF Kondensator benötigt. Sobald der Kontakt der Metallachse geschlossen wird, fährt der P-Zylinder aus und der parallel zum Magnetventil geschalteter Kondensator wird aufgeladen. Die Kugel wird weggeschleudert und der Kontakt öffnet sich wieder - der Kondensator entlädt sich über das Magnetventil, sodass dieses noch kurz eingeschaltet bleibt und auch ganz ausfährt.

In meinem allerersten Versuch hatte ich den Kondensator aus Versehen parallel zum Kontakt geschaltet. Warum blieb manchmal die Metallachse an der Kontaktachse (der Kontakt besteht aus zwei Metallachsen) kleben?