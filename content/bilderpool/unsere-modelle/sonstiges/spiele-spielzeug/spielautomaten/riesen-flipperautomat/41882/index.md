---
layout: "image"
title: "Einwegetor im Detail"
date: "2015-09-02T20:01:59"
picture: "bild06_5.jpg"
weight: "6"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/41882
imported:
- "2019"
_4images_image_id: "41882"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2015-09-02T20:01:59"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41882 -->
Hm, es müsste eigentlich Einbahntor oder so heißen. "Einwegetor" ist nur die wörtliche Übersetzung von "One-Way Gate". Dieses Tor lässt die Kugel im deaktivierten Zustand nur in eine Richtung durch. Wird es aktiviert, zieht der Elektromagnet eine hier nicht eingebaute Metallplatte nach oben, die auf der Platte 15x60 festgeklebt sein sollte. Dadurch wird der Winkelstein 30° und der BS5 nach oben gezogen und gibt den Weg für die Kugel in beide Richtungen frei. Wenn es deaktiviert ist, drückt die Kugel das Konstrukt einfach selbst nach oben.