---
layout: "image"
title: "Rechte Rampe (noch im Bau)"
date: "2013-07-01T09:24:42"
picture: "bild4_6.jpg"
weight: "41"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/37129
imported:
- "2019"
_4images_image_id: "37129"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-07-01T09:24:42"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37129 -->
Die rechte Rampe hat sich seit dem ersten Versuch ständig geändert. Jetzt ist sie relativ flach, dennoch müssen die Flipperfinger stärker sein, um die Kugel die Steigung hinaufzuschießen. Außerdem kommt auf diese Rampe noch eine Weiche, um Kugeln in das Loch unter dem Kran zu lenken.