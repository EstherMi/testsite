---
layout: "image"
title: "Gesamtansicht mit Beleuchtung (noch im Bau)"
date: "2013-04-18T20:27:44"
picture: "2013-04-181.jpg"
weight: "50"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: ["Flipper", "Flipperautomat", "Pinball", "Beleuchtung", "Lampe"]
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36848
imported:
- "2019"
_4images_image_id: "36848"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-04-18T20:27:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36848 -->
So sieht der Flipper mit Beleuchtung im Dunkeln aus.

Ich hab ein ziemlich großes Problem mit dieser Beleuchtung. Die Lampen, die nur zur Beleuchtung beitragen (also nicht die Lampen, die anzeigen ob man was getroffen hat), plus die Lampen für die Lichtschranken unterhalb des Spielfeldes plus die beiden Kompressoren ziehen so viel Strom, dass das Power Set-Netzteil heiß wird und nach einer Weile abschaltet. Für die Beleuchtung möchte ich die ft-Lampen verwenden, die sehen einfach schöner aus als LEDs. Dummerweise (warum??????) kann ich mit den hellen LEDs keine Lichtschranken bauen, auch nicht mit IR-LEDs, die ich mir extra für Lichtschranken bestellt habe.