---
layout: "image"
title: "Slingshot NEU (noch im Bau)"
date: "2013-07-01T09:24:42"
picture: "bild2_6.jpg"
weight: "39"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/37127
imported:
- "2019"
_4images_image_id: "37127"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-07-01T09:24:42"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37127 -->
Die gehn jetzt richtig ab, dank der neuen Kontakte. Wenn man solche länglichen Sicherheitsetiketten mit Barcode drauf aufschneidet, findet man da drin ganz dünne Metallstreifen, die sich supergut als leichtgängige Kontakte eigenen. Man muss sie nur mit einem Kabel zwischen zwei Bausteine fummeln. Die Kugel stößt an das Gummi, welches eines der beiden Metallstreifen gegen die Metallachse dahinter drückt. Dies schaltet ein Relais, welches den Power-Motor auslöst, der die gelbe Strebe nach außen fahren lässt. Ein Kondensator mit 47µF sorgt für eine kurze Verzögerung, sodass die Strebe auch wirklich gegen den Anschlag stößt und die Kugel mit voller Kraft wegschleudert. Nachdem das Relais wieder abgefallen ist, wird die Strebe durch das Gummiband wieder in die Ausgangsposition gedrückt.