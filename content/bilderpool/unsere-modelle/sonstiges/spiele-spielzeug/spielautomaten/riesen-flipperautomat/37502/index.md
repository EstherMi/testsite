---
layout: "image"
title: "Backbox 1"
date: "2013-10-03T09:29:05"
picture: "bild08_4.jpg"
weight: "34"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/37502
imported:
- "2019"
_4images_image_id: "37502"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-10-03T09:29:05"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37502 -->
So jetzt hab ich mich auch mal daran gemacht die Backbox zu bauen. Die Backbox ist an einem Flipper der Kasten, in dem sich das Display, die Lautsprecher und die ganze Steuerelektronik befinden, außerdem ist da vorne drauf meist auch ein Bild der Thematik des Flippers zu sehen. Dieser Kasten ist zu Transportzwecken herunterklappbar, sowohl im Original, als auch hier. Im hochgeklappten Zustand muss die Backbox noch irgendwie verriegelt werden.