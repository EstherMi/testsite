---
layout: "image"
title: "Türmchen (noch im Bau)"
date: "2013-02-03T10:19:45"
picture: "bild07.jpg"
weight: "116"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36549
imported:
- "2019"
_4images_image_id: "36549"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-03T10:19:45"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36549 -->
Diese "Türmchen" werden für die in den Innen- und Außenbahnen befindlichen Lichtschranken benötigt. Die Kugel rollt unter diesem Türmchen durch und unterbricht so die Lichtschranke.