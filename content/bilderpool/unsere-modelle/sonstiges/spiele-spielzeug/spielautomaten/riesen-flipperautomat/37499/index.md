---
layout: "image"
title: "Linke Rampe"
date: "2013-10-03T09:29:05"
picture: "bild05_4.jpg"
weight: "31"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/37499
imported:
- "2019"
_4images_image_id: "37499"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-10-03T09:29:05"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37499 -->
Müsste so gehen. Männle muss aufpassen, wenn die Kugel vorbeikommt und sich nicht verletzt ;-)