---
layout: "image"
title: "Kugelmagazin 1 (noch im Bau)"
date: "2013-02-03T10:19:44"
picture: "bild03.jpg"
weight: "112"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36545
imported:
- "2019"
_4images_image_id: "36545"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-03T10:19:44"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36545 -->
Der "Ball Return", wie ich so gerne sage... hier werden fünf Kugeln gelagert und bei Bedarf über einen P-Zylinder in die Abschussbahn befördert.