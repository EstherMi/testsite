---
layout: "image"
title: "Wieder eine Gesamtansicht..."
date: "2015-09-02T20:01:59"
picture: "bild01_5.jpg"
weight: "1"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/41877
imported:
- "2019"
_4images_image_id: "41877"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2015-09-02T20:01:59"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41877 -->
Upserl, ist aber lange her seit ich das letzte Bild vom Flipper hochgeladen hab... uiui... das war ja noch vor der Convention 2014...
Jedenfalls, so schaut der Flipper im Moment aus. Er bekam endlich mal eine vernünftige Backbox mit einem Plakat drin (dem sogenannten "Backglass" oder "Translite"). Dieses Plakat wird aber irgendwann mal durch eine von hinten beleuchtete, bedruckte Plexiglasscheibe ersetzt ;)
Außerdem ist der Runterklappmechanismus verändert worden - vorher waren das nur Gelenksteine, jetzt sind es zwei Drehkränze. Durch den neuen Mechanismus lässt sich die Backbox auch weiter runterklappen als vorher. Schließlich sind noch zwei Lautsprecher und ein Display eingebaut worden, die aber nicht angeschlossen sind. Dazu fehlten mir die Zeit und die Möglichkeiten. Das DMD ... hätte ich gern zum laufen gebracht. Wenn es nicht kaputt gewesen wäre, hätte ich wenigstens die Punkte anzeigen lassen können :(

Weiterhin hat der Flipper gescheite "Siderails" bekommen, das sind die beiden ganz langen Aluprofile. Vorher war da noch gar nichts, dann wurden erst gelbe Winkelträger eingebaut, um diie nötige Länge herauszufinden und schließlich sägte mir der fischerfriendsman zwei Profile passend zurecht - Danke dafür!
Übrigens: da das Alu Längsnuten hat, kann man da eine Glasscheibe einschieben...

Damit diese, wenn eine solche überhaupt mal eingebaut wird, auch nicht wieder nach vorne herausrutscht, kann sie durch die sogenannte "Lockbar" arretiert werden. Die Lockbar ist der Balken aus platten im Vordergrund, der die gelben Winkelträger teils verdeckt. Diese kann man herausnehmen, wenn man die momentan nicht eingebaute Kassentür öffnet und den rechts in der Öffnung sitzenden Hebel betätigt. Das löst die Verriegelung an zwei Stellen und man kann die Lockbar einfach nach oben herausnehmen. Den Hebel sieht man auf dem Bild ganz unten, es ist rechts von der Bildmitte der schräg stehende WT60.

Dann wurde der Startknopf überarbeitet. Er ist im Bild unten links, das weiße Rad 23. In dessen Aussparung befindet sich eine grüne LED, welche blinkt, wenn der Flipper startbereit ist. Auf der Convention 2014 war dieses Licht noch separat nebendran angebracht.

Die Flexschienen, die als Rampen fungierten, wurden durch Draht ersetzt. Leider ist der Aludraht nicht lötbar, daher musste ich ihn teilweise unschön mit Sekundenkleber in den Rillen von Federnocken einkleben. Aber sonst sieht es doch eleganter aus als vorher, wie ich finde. Mal schauen, wenn ich lötbaren, eleganten Draht finde, dann baue ich das um und dann denke ich mal werden auch die beiden VUKs aus Draht gebaut.

Achso, ich vergaß ganz. Die "General Illumination", kurz GI, wurde auch verkabelt. Das sind die vielen auf dem Bild leuchtenden Lampen. Nur leider zogen die immerhin 14 parallel geschalteten Lampen so viel Strom, dass der ft-Adaper das nicht abkann und die Lampen regelmäßig abschaltete. Dieser wird noch durch eine richtige Steuerung ersetzt, sodass ich die GI auch dimmen kann.

Die hüpfenden Männchen, die Momentan nicht eingebaut sind, kommen später wieder dazu. Außerdem fehlen noch die ganzen Flasher und die meisten Spielfeldlampen, diejenigen, die unter der Platte verbaut werden.

....wer errät, was das schwarzblaue ganz links am Bildrand ist? :-) :-) :-)