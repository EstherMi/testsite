---
layout: "image"
title: "Rechte Rampe 1 (noch im Bau)"
date: "2013-02-04T10:32:23"
picture: "bild1.jpg"
weight: "101"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36576
imported:
- "2019"
_4images_image_id: "36576"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-04T10:32:23"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36576 -->
Hier seht ihr einen Draufblick von der rechten Rampe.