---
layout: "image"
title: "Obere Bahnen (noch im Bau)"
date: "2013-07-01T09:24:42"
picture: "bild8_3.jpg"
weight: "45"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/37133
imported:
- "2019"
_4images_image_id: "37133"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-07-01T09:24:42"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37133 -->
Nach dem Abschuss und durch die Loops gelangt die Kugel in eine der drei Bahnen. Die Schläuche wirken als Gummipfosten.