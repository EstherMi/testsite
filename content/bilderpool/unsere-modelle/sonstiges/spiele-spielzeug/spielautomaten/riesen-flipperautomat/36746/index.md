---
layout: "image"
title: "Bonus-Anzeige"
date: "2013-03-15T00:41:17"
picture: "bild02_3.jpg"
weight: "58"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36746
imported:
- "2019"
_4images_image_id: "36746"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-03-15T00:41:17"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36746 -->
Wenn man die Kugel verloren hat, wird der Bonus vergeben. Dieser Bonus erhöht sich, je mehr Ziele, Bahnen oder sonstiges man auf dem Flipper trifft. Im Beispiel oben beträgt der Bonus 3,4 Millionen Punkte, und wird mit dem Bonus-Multiplikator 7 multipliziert, was dann 23,8 Millionen ergibt. Und diese 23,8 Millionen werden dem Punktestand hinzugefügt. Den Bonus-Multiplikator kann man dur das Komplettieren der unteren Bahnen um 2 und den oberen Bahnen 1 erhöht werden.