---
layout: "image"
title: "Abschussbahn"
date: "2014-08-20T17:27:31"
picture: "bild2_8.jpg"
weight: "17"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/39256
imported:
- "2019"
_4images_image_id: "39256"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2014-08-20T17:27:31"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39256 -->
Gut zu sehen sind hier...alles. Die manuelle Abschussstange (das runde, weiße Teil links), die beiden L-Laschen bilden, mit ihrer Unterkonstruktion, den Auto-Launcher. Oben drüber, die Plastikachse, geht von dem Z20, welches unten noch unscharf zu erkennen ist, durch einen BS25-Loch und eine Schneckenmutter zum Taster, der auf den vorigen zwei Bildern über dem Kugelrücklauf sichtbar ist. Im BS30-Loch ist dann noch der Fototransistor für die Lichtschranke zu sehen, die dazugehörige IR-LED schaut noch aus dem BS30-Loch über dem Fototransistor heraus. Die drei Winkelsteine 30 verhindern, dass die Kugel, wenn sie aus dem Kugelrücklauf in den Abschuss befördert wird, nicht wieder versehentlich zurückrollt.