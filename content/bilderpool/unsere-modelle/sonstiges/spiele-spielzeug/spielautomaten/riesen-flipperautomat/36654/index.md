---
layout: "image"
title: "Anzeige (noch im Bau)"
date: "2013-02-19T18:03:38"
picture: "bild8_2.jpg"
weight: "95"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36654
imported:
- "2019"
_4images_image_id: "36654"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-19T18:03:38"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36654 -->
Auf dieser Anzeige im RoboPro Bedienfeld erscheinen alle möglichen Anweisungen: Was man als nächstes treffen muss (z. B. für eine Extrakugel), oder wenn Multiball gestartet wird, und und und...
Der Spieler kann maximal neun Milliarden neunhundertneunundneunzig Millionen neunhundertneunundneunzig Tausend neunhundertneunzig Punkte erreichen. Die Einerstelle wird "nicht benutzt", das heißt, sie zeigt immer null, so wie bei den großen Flippern.

(Ich bin am überlegen, ob ich noch eine Zehnmilliardenstelle hinzufüge...)