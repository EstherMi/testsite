---
layout: "image"
title: "Fallziele 10 (noch im Bau)"
date: "2013-03-10T17:46:49"
picture: "bild11_2.jpg"
weight: "86"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36742
imported:
- "2019"
_4images_image_id: "36742"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-03-10T17:46:49"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36742 -->
Der Reset-Balken fährt nach oben, die Fallziele werden durch die Gummis in die Ausgangsposition gezogen und dort gehalten, bis die BÖSE Kugel kommt. Das linke Fallziel wurde übrigens "getroffen".