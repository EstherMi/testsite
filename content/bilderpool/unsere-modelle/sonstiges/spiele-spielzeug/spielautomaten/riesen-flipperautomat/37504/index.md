---
layout: "image"
title: "Backbox 3"
date: "2013-10-03T09:29:05"
picture: "bild10_4.jpg"
weight: "36"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/37504
imported:
- "2019"
_4images_image_id: "37504"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-10-03T09:29:05"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37504 -->
Das Gelenk der Backbox. Hier sieht man auch wie weit sie überdas Spielfeld ragt - das ist zwar nicht bei jedem Flipper so, aber auf dem hier mach ich was ich will. :-) Nee aber ich spare so Platz nach hinten und aufwändige Konstruktionen zum Stabilisieren der Backbox.