---
layout: "image"
title: "Platte 2"
date: "2014-06-03T22:44:12"
picture: "bild2_7.jpg"
weight: "25"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/38910
imported:
- "2019"
_4images_image_id: "38910"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2014-06-03T22:44:12"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38910 -->
Hier sieht man einen Teil der Abschussbahn, in der die (seperate) Platte schon testweise hineingeklemmt ist. Es sieht super aus. Aus dem Loch, wo die drei Winkel 30° sind, kommen die Kugeln aus dem Magazin raus. Oben in der Bildmitte sieht man noch die IR-LED für die Lichtschranke. Und das weiße ist die Stange, die vom Spieler nach hinten gezogen wird, um die Kugel abzuschießen.