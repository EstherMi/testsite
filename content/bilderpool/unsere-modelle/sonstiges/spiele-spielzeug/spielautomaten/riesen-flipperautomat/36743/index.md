---
layout: "image"
title: "Fallziele 11 (noch im Bau)"
date: "2013-03-10T17:46:49"
picture: "bild12_2.jpg"
weight: "87"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36743
imported:
- "2019"
_4images_image_id: "36743"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-03-10T17:46:49"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36743 -->
Die getroffenen Ziele liegen mit dem BS15 mit Loch auf der Schräge des Reset-Balkens.