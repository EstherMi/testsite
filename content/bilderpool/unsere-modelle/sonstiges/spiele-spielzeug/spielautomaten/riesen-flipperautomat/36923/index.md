---
layout: "image"
title: "Auswurfloch (noch im Bau)"
date: "2013-05-23T10:57:11"
picture: "bild2_5.jpg"
weight: "47"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36923
imported:
- "2019"
_4images_image_id: "36923"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-05-23T10:57:11"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36923 -->
Jetzt sind's schon vier Lichter am Auswurfloch.... man beachte die Rainbow-LED neben der vordersten Lampe. Der Grund, warum ich nicht noch ein weiteres Lichtfenster anbauen wollte, ist folgender: Das Zeug wird mir sonst zu groß. Und da die Lampe neben der Rainbow-LED nie leuchtet (oder vielmehr blinkt), während die LED aktiv ist, passt das doch ganz gut.