---
layout: "image"
title: "Detail Auswurfloch (noch im Bau)"
date: "2013-03-10T17:46:48"
picture: "bild01_2.jpg"
weight: "76"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36732
imported:
- "2019"
_4images_image_id: "36732"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-03-10T17:46:48"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36732 -->
Eine Detailaufnahme der überarbeiteten Version des Auswurflochs. Über dem P-Zylinder befinden sich Lampen.
Da der Flipper über Multikugel-Modi verfügt, mussten links und rechts von der Flexschiene eine Art Wand angebaut werden, ansonsten würden die Kugeln einfach herunterfallen. Im Leuchtstein, den man gerade noch unter dem Auswurfhebel sieht, befindet sich die Linsenlampe für die Lichtschranke.

Im Hintergrund sind sich die "Bel. Kickback"-Ziele an den Gelenksteinen befestigt.