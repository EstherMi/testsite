---
layout: "image"
title: "Kickback-'Schaltung' (noch im Bau)"
date: "2013-02-19T18:03:37"
picture: "bild2_3.jpg"
weight: "89"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36648
imported:
- "2019"
_4images_image_id: "36648"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-19T18:03:37"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36648 -->
Mit dieser Schaltung löse ich den Kickback-Motor aus. Da der Motor kurz vor- und dann zurückfahren muss, konnte (wollte!!!) ich ihn nicht an einen Motorausgang anschließen. Das Ergebnis: siehe Bild. Der Schlauch oben rechts wird an eine Druckluftquelle angeschlossen, das rote Kabel kommt in den TX an einen LAMPENausgang und das blaue Kabel unten rechts geht zum Motor. Sobald dieses Ding Strom über das rote Kabel bekommt, wird das Magnetventil ausgelöst, dadurch fährt der Zylinder aus. Gleichzeitig fährt der Kickback-Motor die Metallachse nach außen um die Kugel wegzuschleudern. Sobald der Zylinder den Taster drückt, wird die Polung des Motors umgedreht und dieser fährt zurück. Nun kann der Lampenausgang wieder ausgeschaltet werden, dadurch fährt der Zylinder auch zurück.