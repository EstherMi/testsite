---
layout: "image"
title: "Fallziele Reset (noch im Bau)"
date: "2013-04-18T11:36:22"
picture: "bild4_4.jpg"
weight: "53"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36844
imported:
- "2019"
_4images_image_id: "36844"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-04-18T11:36:22"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36844 -->
Hier sieht man mal den Reset-Mechanismus. Sobald alle drei Reedkontakte unterbrochen sind, drehen sich die beiden 20:1 Power-Motoren und drücken mit dem Drehteil in der Mitte den Reset-Balken nach oben. Die Motoren drehen sich nur in eine Richtung, das heißt, dass wenn sich das Drehteil vom Reset-Balken wegdreht, fällt dieser durch sein eigenes Gewicht herunter und die Motoren werden am Taster gestoppt.