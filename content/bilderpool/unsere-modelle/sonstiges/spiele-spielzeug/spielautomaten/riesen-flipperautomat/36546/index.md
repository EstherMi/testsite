---
layout: "image"
title: "Kugelmagazin 2 (noch im Bau)"
date: "2013-02-03T10:19:44"
picture: "bild04.jpg"
weight: "113"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36546
imported:
- "2019"
_4images_image_id: "36546"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-03T10:19:44"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36546 -->
Die Kugeln werden von einem P-Zylinder auf die Abschussrampe befördert. Unten sieht man gut die Kugel, die als nächstes auf die Abschussrampe befördert wird. Der Hebel, auf dem die Kugel liegt, schleudert diese gegen die Platte (oben, an einem 30° Winkel befestigt), wodurch sie in die Abschussrampe gelangt.

(Auf dem Bild sieht man von diesem Hebel wohl nicht viel, weil der Taster für den rechten Flipperknopf davor ist...)