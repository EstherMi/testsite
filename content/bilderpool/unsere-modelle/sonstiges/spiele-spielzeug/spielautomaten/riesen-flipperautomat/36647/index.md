---
layout: "image"
title: "Abschussrampe (noch im Bau)"
date: "2013-02-19T18:03:37"
picture: "bild1_3.jpg"
weight: "88"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36647
imported:
- "2019"
_4images_image_id: "36647"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-19T18:03:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36647 -->
Ich hab mal wieder BS 30 gebraucht - zack, ist die Abschussbahn überarbeitet worden.