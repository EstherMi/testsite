---
layout: "image"
title: "Abschussbahn Lichtschranke"
date: "2013-10-03T09:29:05"
picture: "bild07_4.jpg"
weight: "33"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/37501
imported:
- "2019"
_4images_image_id: "37501"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-10-03T09:29:05"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37501 -->
Irgendein Sensor muss in die Abschussbahn. Wenn eine Kugel nicht richtig in den Abschuss befördert wurde, wird dies von dieser IR-Lichtschranke erkannt, zudem dient sie auch zur Aktivierung des Kugelretters und des Auto-Abschusses bei Multiball etc, wo eine oder mehr Kugeln automatisch eingeschossen werden. Die IR-LED geht mit den Drähten durch einen BS30 mit Loch.