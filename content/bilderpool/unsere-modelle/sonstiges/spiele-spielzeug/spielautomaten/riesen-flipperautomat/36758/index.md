---
layout: "image"
title: "Auswurfloch, überarbeitet 2 (noch im Bau)"
date: "2013-03-15T00:41:17"
picture: "bild14_2.jpg"
weight: "70"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36758
imported:
- "2019"
_4images_image_id: "36758"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-03-15T00:41:17"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36758 -->
Hier sieht man gut, warum das Auswurfloch etwas nach rechts verschoben wurde. Hier, in den linken Loop kann man die Kugel hinaufschießen, und manchmal kommt sie dann rechts wieder herunter.