---
layout: "image"
title: "Zentrum 1 (noch im Bau)"
date: "2013-07-01T09:24:42"
picture: "bild5_4.jpg"
weight: "42"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/37130
imported:
- "2019"
_4images_image_id: "37130"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-07-01T09:24:42"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37130 -->
Das ist das "Zentrum" des Spielfeldes (mir ist kein besserer Name eingefallen). Die drei kleinen Platten im Vordergrund registrieren Treffer, und wenn sie alle getroffen wurden, werden sie von einem Motor im Spielfeld versenkt. Dahinter befinden sich nochmal drei Ziele, von denen das Mittlere ein Fallziel ist. Hinter dem Fallziel ist ein Loch.