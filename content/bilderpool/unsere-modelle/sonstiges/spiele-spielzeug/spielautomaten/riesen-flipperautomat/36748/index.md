---
layout: "image"
title: "Flipperfinger unten (noch im Bau)"
date: "2013-03-15T00:41:17"
picture: "bild04_3.jpg"
weight: "60"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36748
imported:
- "2019"
_4images_image_id: "36748"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-03-15T00:41:17"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36748 -->
Linker Flipperfinger in unterer Stellung.