---
layout: "image"
title: "Rechte Rampe 4 (noch im Bau)"
date: "2013-02-04T10:32:24"
picture: "bild4.jpg"
weight: "104"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36579
imported:
- "2019"
_4images_image_id: "36579"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-04T10:32:24"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36579 -->
So wird wohl der linke Flipperfinger die Rampe sehen... Ganz rechts im Bild ist das Gummi, das die Kugel vor der rechten Ausbahn "schützen" soll. Direkt links daneben ist so eine Mulde, wo sich der BS 30 des Krans in Ruhestellung befinden wird (der Kran kommt noch!). Zwischen der Mulde und der Rampe ist der rechte Loop. Schießt man die Kugel dort hinein, kommt sie manchmal im linken Loop wieder herunter...
Links neben den "gelben biegbaren Dingern" kommt noch eine Wand hin, dass die Kugel da nicht irgendwie runterfliegt.