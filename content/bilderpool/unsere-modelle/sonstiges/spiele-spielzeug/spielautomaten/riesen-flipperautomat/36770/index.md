---
layout: "image"
title: "Schaltplan Elektronik für Standziele"
date: "2013-03-17T22:29:30"
picture: "bild3_4.jpg"
weight: "56"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36770
imported:
- "2019"
_4images_image_id: "36770"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-03-17T22:29:30"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36770 -->
Zu der Schaltung habe ich auch gleich einen Schaltplan erstellt.