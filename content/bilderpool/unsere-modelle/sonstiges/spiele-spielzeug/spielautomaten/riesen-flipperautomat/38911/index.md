---
layout: "image"
title: "Platte 3"
date: "2014-06-03T22:44:12"
picture: "bild3_7.jpg"
weight: "26"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/38911
imported:
- "2019"
_4images_image_id: "38911"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2014-06-03T22:44:12"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38911 -->
Am Ende der Abschussrampe, wo die Kugel aufs eigentliche Spielfeld gelangt. Die Platte 15x30 hält die Platte erst mal provisorisch in ihrer Position. Leider ist die Platte an der Spitze beim Sägen etwas abgebrochen, aber das stört zum glück nicht und fällt auch kaum auf.