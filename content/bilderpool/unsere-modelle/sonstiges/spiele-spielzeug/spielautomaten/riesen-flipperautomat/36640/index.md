---
layout: "image"
title: "Slingshot (noch im Bau)"
date: "2013-02-17T23:43:44"
picture: "bild2_2.jpg"
weight: "97"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36640
imported:
- "2019"
_4images_image_id: "36640"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-02-17T23:43:44"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36640 -->
Beide Slingshots sind gleich aufgebaut - nur spiegelverkehrt. Die Metallachse ist der Auslöser: wird sie vom Gummiband nach hinten gedrückt, wird das Magnetventil aktiviert (und ein Kondensator geladen, siehe übernächstes Bild) und der P-Zylinder fährt aus.

FEUER!!!!!