---
layout: "image"
title: "Flipperfinger neu 2"
date: "2013-10-03T09:29:05"
picture: "bild02_4.jpg"
weight: "28"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/37496
imported:
- "2019"
_4images_image_id: "37496"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-10-03T09:29:05"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37496 -->
Hier hab ich den Flipperfinger abmontiert. Der große Teil rechts wird zum Zusammenbauen auf die Winkelachse geschoben, dann kommt über den Zapfen ein halber BS 7,5 und dann zum Sichern des BS 7,5 wird ein Verbinder 7,5 auf den BS30 geschoben.