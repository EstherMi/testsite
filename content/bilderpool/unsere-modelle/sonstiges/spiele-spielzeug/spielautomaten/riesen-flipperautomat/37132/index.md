---
layout: "image"
title: "Linke Rampe (noch im Bau)"
date: "2013-07-01T09:24:42"
picture: "bild7_3.jpg"
weight: "44"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/37132
imported:
- "2019"
_4images_image_id: "37132"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-07-01T09:24:42"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37132 -->
Jetzt hat es noch eine Rampe aufs Spielfeld geschafft. Sie ist sehr steil, weil sich direkt hinter ihr einer der Schlagtürme befindet. Da muss ich mal gucken, was ich da machen kann.