---
layout: "image"
title: "Böcke 1"
date: "2013-05-23T10:57:11"
picture: "bild1_5.jpg"
weight: "46"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36922
imported:
- "2019"
_4images_image_id: "36922"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-05-23T10:57:11"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36922 -->
Neuerdings hatte ich die Idee, den Flipper auf zwei Böcke zu stellen. Das hat zwei Vorteile: Erstens kann ich direkt von unten an den Flipper um z. B. Spielfeldobjekte zu verkabeln, und zweitens kann man den Flipper gleich "spielen", oder die einzelnen Objekte testen. Das war hier http://ftcommunity.de/details.php?image_id=36768 noch unmöglich, ohne erstmal den Flipper herunterzukippen.

Bitte entschuldigt das Chaos unter und hinter dem Flipper, ich sollte mal wieder aufräumen... :-(