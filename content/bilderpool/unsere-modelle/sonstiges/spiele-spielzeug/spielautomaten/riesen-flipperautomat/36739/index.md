---
layout: "image"
title: "Fallziele 7 (noch im Bau)"
date: "2013-03-10T17:46:48"
picture: "bild08_2.jpg"
weight: "83"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36739
imported:
- "2019"
_4images_image_id: "36739"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-03-10T17:46:48"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36739 -->
Der Reset-Balken in der oberen Position.