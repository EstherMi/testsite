---
layout: "image"
title: "Flipperfinger-Mechanismus 1"
date: "2013-10-03T09:29:05"
picture: "bild03_4.jpg"
weight: "29"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/37497
imported:
- "2019"
_4images_image_id: "37497"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-10-03T09:29:05"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37497 -->
Inzwischen habe ich die richtige Möglichkeit gefunden, die Flipperfinger stärker zu machen. 3V-Zylindermagnete sind, wenn sie mit 24V betrieben werden, sehr stark. Hier sieht man den Zylindermagnet oben rechts - auf seiner Metallachse, die vorne herausgeht, steckt ein Rastadapter 36227. Der ist mit einer Platte 15x30x5 verbunden, die mit einem Gelenkstein verbunden ist. Hinter der roten Bauplatte 30x45 versteckt sich ein roter BS15 mit Loch, der mit dem neben der Platte noch sichtbaren Gelenkstein verbunden ist. Dieser BS15 mit Loch bewegt die Winkelachse, an der der Flipperfinger befestigt ist.
Die Platte 15x30x5 betätigt außerdem, wenn der Hubmagnet ganz ausgefahren ist, einen Taster.