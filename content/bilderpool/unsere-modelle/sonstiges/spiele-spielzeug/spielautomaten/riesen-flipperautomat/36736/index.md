---
layout: "image"
title: "Fallziele 4 (noch im Bau)"
date: "2013-03-10T17:46:48"
picture: "bild05_2.jpg"
weight: "80"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36736
imported:
- "2019"
_4images_image_id: "36736"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-03-10T17:46:48"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36736 -->
Hier sieht man gut, wie getroffene Fallziele erkannt werden. In die Reedkontakthalter werden noch Reedkontakte hineingesteckt. Sobald ein Ziel getroffen wurde (hier links), wird der Reedkontakt unterbrochen und die Software kann 'ne Million Punkte werten.