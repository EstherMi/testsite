---
layout: "image"
title: "'Roll-Over' 1"
date: "2015-09-02T20:01:59"
picture: "bild12_4.jpg"
weight: "12"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/41888
imported:
- "2019"
_4images_image_id: "41888"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2015-09-02T20:01:59"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41888 -->
Und hier das Gegenstück zum "Roll-Under"! Es ist einfach ein Draht, der von der Kugel nach unten gedrückt wird.