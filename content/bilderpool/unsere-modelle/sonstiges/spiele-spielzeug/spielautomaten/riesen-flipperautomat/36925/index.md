---
layout: "image"
title: "Schlagtürme (noch im Bau)"
date: "2013-05-23T10:57:11"
picture: "bild4_5.jpg"
weight: "49"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36925
imported:
- "2019"
_4images_image_id: "36925"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2013-05-23T10:57:11"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36925 -->
Wer kennt sie nicht, diese runden Pilzförmigen Gebilde auf dem Spielfeld, die die Kugel bei Kontakt zurückschlagen? Prallt die Kugel gegen einen dieser drei Türme, wird ein Kontakt ausgelöst und der Schlagring nach unten gezogen, was das Wegschleudern der Kugel bewirkt.
Das sind garantiert die besten Spielfeldkomponenten, auch wenn sie noch ziemlich schwach sind. Vielleicht sollte ich den Power Motor 8:1 durch einen XM-Motor ersetzen? Es sollte ein möglichst schneller UND sehr starker Motor sein - ich kenne den XM noch nicht. Würde sich das lohnen, gleich drei davon zu kaufen?

Über ihnen befinden sich die sogenannten "Top Lanes", auch "Obere Bahnen" genannt. Rollt die Kugel durch eine dieser Bahnen, wird das entsprechende Licht eingeschaltet und bleibt auch bei mehrmaligem Durchlaufen an und wenn alle Bahnen getroffen wurden, gibt es einen kleinen Bonus von 10 Millionen oder so und der Bonus-Multiplikator wird um 1 erhöht. Hoffentlich kriege ich das hin, dass die Lichter mit den Flipperknöpfen nach links und rechts gewechselt werden können...