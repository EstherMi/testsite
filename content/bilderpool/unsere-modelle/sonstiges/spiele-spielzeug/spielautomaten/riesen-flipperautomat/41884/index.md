---
layout: "image"
title: "Der Schlagturm!"
date: "2015-09-02T20:01:59"
picture: "bild08_5.jpg"
weight: "8"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/41884
imported:
- "2019"
_4images_image_id: "41884"
_4images_cat_id: "2711"
_4images_user_id: "1624"
_4images_image_date: "2015-09-02T20:01:59"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41884 -->
Viele Leute auf dem Fan Tag interessierten sich auch für den Mechanismus der Schlagtürme. Ich erkläre das hier mal.
Die Kugel kommt, wenn sie an den Turm kommt, auf den leicht erhöhten "Kontaktring", das ist die untere Drehscheibe 60. Durch das Gewicht der Kugel wird diese runtergedrückt und löst unterhalb einen Kontakt zwischen zwei Metallstangen aus. Eine davon sieht man hier: diejenige, auf der das Rad 23 befestigt ist. Ist der Kontakt geschlossen, registriert der TX-C das und aktiviert den Powermotor, der über ein Exzenter ein paar Bausteine herunterzieht. Dies geschieht alles unter dem Spielfeld. Auf den Bausteinen sind zwei Achsenverschraubungen befestigt, in denen die zwei oben herausragenden Metallachsen festgeschraubt sind. Da Metallachsen in den Löchern der Drehscheibe fest sitzen, wird die obere Scheibe, der "Schlagring", schnell mit nach unten gezogen. Da am Schlagring Winkelsteine befestigt sind, wird die Kugel an die Seite weggedrückt. Der Kontaktring wurd nach Freigabe durch eine Feder wieder hochgedrückt. Übrigens mussten von diesem zwei Löcher aufgebohrt werden, da sonst die Metallschlangen des Schlagrings da stecken bleiben würden.