---
layout: "comment"
hidden: true
title: "17060"
date: "2012-08-10T20:50:52"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Meine Flipper funtioniert jetzt mit 2x I2C-Bus LED Display (Conrad # 198344) + 1x LCD Anzeige (Conrad # 198330) .

LED Display- nr2 mit Jumper 2 wird im Robo Pro Programm mit Adresse 0x39 ansprochen.

Link zum FT-Forum :
http://forum.ftcommunity.de/viewtopic.php?f=8&t=1274&start=80

Grüss,

Peter Poederoyen NL