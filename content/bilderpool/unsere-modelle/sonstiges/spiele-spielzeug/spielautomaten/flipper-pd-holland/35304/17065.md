---
layout: "comment"
hidden: true
title: "17065"
date: "2012-08-10T21:36:26"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Beim I2C -LED (2x) und -LCD -Anzeige für Flipper habe ich herausgefunden das die Stabilität des Zählen im Download-betrieb besser ist als im online-Betrieb.

Peter Poederoyen