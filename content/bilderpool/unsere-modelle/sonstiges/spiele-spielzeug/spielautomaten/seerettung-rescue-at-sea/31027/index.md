---
layout: "image"
title: "back3"
date: "2011-07-12T10:37:49"
picture: "back3.jpg"
weight: "3"
konstrukteure: 
- "The rescue of a pilot at the open sea."
fotografen:
- "thedutchbuilder"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dutchbuilder"
license: "unknown"
legacy_id:
- details/31027
imported:
- "2019"
_4images_image_id: "31027"
_4images_cat_id: "2318"
_4images_user_id: "1315"
_4images_image_date: "2011-07-12T10:37:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31027 -->
