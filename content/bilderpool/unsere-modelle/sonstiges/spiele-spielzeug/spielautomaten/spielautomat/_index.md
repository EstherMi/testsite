---
layout: "overview"
title: "Spielautomat"
date: 2019-12-17T19:39:10+01:00
legacy_id:
- categories/1928
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1928 --> 
Früher hatten meine Eltern uns einen Spielautomaten für den Keller gekauft. Es war immer mein Traum ein solches Modell mal aus FT nachzubauen.