---
layout: "image"
title: "Spielautomat Draufsicht 1"
date: "2010-04-07T12:40:32"
picture: "spielautomat1.jpg"
weight: "1"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- details/26897
imported:
- "2019"
_4images_image_id: "26897"
_4images_cat_id: "1928"
_4images_user_id: "1112"
_4images_image_date: "2010-04-07T12:40:32"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26897 -->
Man muss drei gleiche Symbole haben, oder links und rechts zwei gleiche und in der Mitte den Stern. Den Hauptgewinn hat man, wenn man drei mal den Stern hat. Dann werden 10 mal 2 Cent ausgezahlt. Das Spiel wurde inzwischen von meinen Kindern mehr als 200 mal getestet.