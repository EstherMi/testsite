---
layout: "image"
title: "Flipper4 - Aufzug"
date: "2012-06-06T22:26:53"
picture: "flipper09.jpg"
weight: "9"
konstrukteure: 
- "leonidas"
fotografen:
- "leonidas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "leonidas"
license: "unknown"
legacy_id:
- details/35032
imported:
- "2019"
_4images_image_id: "35032"
_4images_cat_id: "2594"
_4images_user_id: "1516"
_4images_image_date: "2012-06-06T22:26:53"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35032 -->
Die Kugel fällt in die Grube, wird von der Kette nach oben befördert und über die Flexschienen wieder ins Spiel gebracht.
