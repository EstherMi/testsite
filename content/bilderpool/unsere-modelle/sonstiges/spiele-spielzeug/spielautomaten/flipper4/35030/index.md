---
layout: "image"
title: "Flipper4 - Schaukel"
date: "2012-06-06T22:26:53"
picture: "flipper07.jpg"
weight: "7"
konstrukteure: 
- "leonidas"
fotografen:
- "leonidas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "leonidas"
license: "unknown"
legacy_id:
- details/35030
imported:
- "2019"
_4images_image_id: "35030"
_4images_cat_id: "2594"
_4images_user_id: "1516"
_4images_image_date: "2012-06-06T22:26:53"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35030 -->
Lichtschranke im Detail.