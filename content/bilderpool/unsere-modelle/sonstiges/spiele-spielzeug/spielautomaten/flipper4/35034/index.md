---
layout: "image"
title: "Flipper4 - Drehscheibe"
date: "2012-06-06T22:26:53"
picture: "flipper11.jpg"
weight: "11"
konstrukteure: 
- "leonidas"
fotografen:
- "leonidas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "leonidas"
license: "unknown"
legacy_id:
- details/35034
imported:
- "2019"
_4images_image_id: "35034"
_4images_cat_id: "2594"
_4images_user_id: "1516"
_4images_image_date: "2012-06-06T22:26:53"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35034 -->
Die Drehscheibe gibt der Kugel neuen Schwung und eine unvorhersehbare Richtung.
