---
layout: "image"
title: "Flipper4 - Aufzug"
date: "2012-06-06T22:26:53"
picture: "flipper10.jpg"
weight: "10"
konstrukteure: 
- "leonidas"
fotografen:
- "leonidas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "leonidas"
license: "unknown"
legacy_id:
- details/35033
imported:
- "2019"
_4images_image_id: "35033"
_4images_cat_id: "2594"
_4images_user_id: "1516"
_4images_image_date: "2012-06-06T22:26:53"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35033 -->
Ansicht von außen.