---
layout: "image"
title: "Flipper4 - Seitenansicht"
date: "2012-06-06T22:26:53"
picture: "flipper03.jpg"
weight: "3"
konstrukteure: 
- "leonidas"
fotografen:
- "leonidas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "leonidas"
license: "unknown"
legacy_id:
- details/35026
imported:
- "2019"
_4images_image_id: "35026"
_4images_cat_id: "2594"
_4images_user_id: "1516"
_4images_image_date: "2012-06-06T22:26:53"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35026 -->
Hinweis:
Für die Kabelstränge benutze ich Litzenleitungen, die verdrillt und anschließend durch Trinkhalme gezogen werden.