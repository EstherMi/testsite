---
layout: "comment"
hidden: true
title: "21956"
date: "2016-04-26T16:22:15"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Mir gefällt's auch. Sowohl die Silberlinge als auch das Labyrinth als auch das gut fotografierbare Grau. Und die Modellidee natürlich erst recht!

Gruß,
Stefan