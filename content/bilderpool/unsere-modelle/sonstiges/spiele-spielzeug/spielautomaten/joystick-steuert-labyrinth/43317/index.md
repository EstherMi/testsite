---
layout: "image"
title: "Powermotoren mit 125:1"
date: "2016-04-26T13:19:44"
picture: "bild3.jpg"
weight: "3"
konstrukteure: 
- "Gürol Dümrol"
fotografen:
- "Gürol Dümrol"
keywords: ["Powermotor"]
uploadBy: "guerol"
license: "unknown"
legacy_id:
- details/43317
imported:
- "2019"
_4images_image_id: "43317"
_4images_cat_id: "3216"
_4images_user_id: "891"
_4images_image_date: "2016-04-26T13:19:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43317 -->
Schade, daß ft die Powermotoren nie mit Getriebe 125:1 und 312:1 im Programm hatte. Ich finde die Powermotoren gerade mit diesen Getrieben besonders nützlich. So drehen sie sich nicht nur langsam genug, sondern erledigen die Aufgabe selbst dann, wenn sie -wie in diesem Fall- spannungsmäßig unterversorgt sind.