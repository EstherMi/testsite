---
layout: "image"
title: "Auslegeeinheit hinten"
date: "2012-07-18T18:50:53"
picture: "bild8.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- details/35196
imported:
- "2019"
_4images_image_id: "35196"
_4images_cat_id: "2608"
_4images_user_id: "1218"
_4images_image_date: "2012-07-18T18:50:53"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35196 -->
Hier die Auslegeeinheit von hinten gesehen: Man sieht Zylinder und Endtaster, der erkennt ob eine Kugel gekommen ist. Eine Lichtschranke über der Bahn ginge zwar auch, aber so war es einfacher.