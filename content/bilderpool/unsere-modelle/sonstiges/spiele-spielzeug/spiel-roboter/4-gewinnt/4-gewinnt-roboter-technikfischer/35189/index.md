---
layout: "image"
title: "Frontansicht"
date: "2012-07-18T18:50:53"
picture: "bild1.jpg"
weight: "1"
konstrukteure: 
- "technikfischer"
fotografen:
- "technikfischer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- details/35189
imported:
- "2019"
_4images_image_id: "35189"
_4images_cat_id: "2608"
_4images_user_id: "1218"
_4images_image_date: "2012-07-18T18:50:53"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35189 -->
Hier sieht man den Roboter von vorne. Unten im Bild die Taster zum steuern, darüber die Ablage für die Kugeln. Dann die Steuerung zum Auslegen, links das Interface, rechts die Lufteinheit und Kompressor. Die Kugeln werden mit abwechselnder Farbe in die Bahn ganz rechts eingeworfen. Weitere Bilder werden folgen (Gewinnanzeigelampe, Verbesserungen am Aussehen, ...)