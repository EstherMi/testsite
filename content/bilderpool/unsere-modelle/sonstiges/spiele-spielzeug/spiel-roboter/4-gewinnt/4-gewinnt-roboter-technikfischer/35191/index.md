---
layout: "image"
title: "Bedienfeld"
date: "2012-07-18T18:50:53"
picture: "bild3.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- details/35191
imported:
- "2019"
_4images_image_id: "35191"
_4images_cat_id: "2608"
_4images_user_id: "1218"
_4images_image_date: "2012-07-18T18:50:53"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35191 -->
Hier eine genaue Ansicht des Bedienfeldes mit abgennomenen Sichtklappen.