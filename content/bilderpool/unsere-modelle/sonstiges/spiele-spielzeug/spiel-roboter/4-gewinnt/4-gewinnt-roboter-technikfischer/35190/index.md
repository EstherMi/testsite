---
layout: "image"
title: "Von oben"
date: "2012-07-18T18:50:53"
picture: "bild2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- details/35190
imported:
- "2019"
_4images_image_id: "35190"
_4images_cat_id: "2608"
_4images_user_id: "1218"
_4images_image_date: "2012-07-18T18:50:53"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35190 -->
Hier sieht man das ganze noch mal von oben.