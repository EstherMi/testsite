---
layout: "image"
title: "Auslegeeinheit (2)"
date: "2012-07-18T18:50:53"
picture: "bild7.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- details/35195
imported:
- "2019"
_4images_image_id: "35195"
_4images_cat_id: "2608"
_4images_user_id: "1218"
_4images_image_date: "2012-07-18T18:50:53"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35195 -->
Hier die Auslegeeinheit, wenn sie eine Kugel ausgibt.