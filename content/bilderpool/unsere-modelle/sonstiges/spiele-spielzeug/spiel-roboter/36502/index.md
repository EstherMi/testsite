---
layout: "image"
title: "Flipper übersicht"
date: "2013-01-22T17:34:38"
picture: "Bild_1.jpg"
weight: "2"
konstrukteure: 
- "Jonas"
fotografen:
- "Jonas"
keywords: ["Flipper"]
uploadBy: "x coaster"
license: "unknown"
legacy_id:
- details/36502
imported:
- "2019"
_4images_image_id: "36502"
_4images_cat_id: "776"
_4images_user_id: "1608"
_4images_image_date: "2013-01-22T17:34:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36502 -->
Hier ist mein neuer großer Flipper auf 6 Grundplatten.
Er ist mit Sound.
VIDEO: http://www.youtube.com/watch?v=7aBtvJB4Pyo

Er macht (finde ich) noch viel mehr Spaß, als der aus dem Baukasten