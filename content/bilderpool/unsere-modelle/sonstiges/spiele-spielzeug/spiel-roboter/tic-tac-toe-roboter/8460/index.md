---
layout: "image"
title: "Auswerfer"
date: "2007-01-14T13:57:05"
picture: "tictactoeroboter6.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- details/8460
imported:
- "2019"
_4images_image_id: "8460"
_4images_cat_id: "777"
_4images_user_id: "521"
_4images_image_date: "2007-01-14T13:57:05"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8460 -->
Der Auswerfer wirft neue Steine aus.