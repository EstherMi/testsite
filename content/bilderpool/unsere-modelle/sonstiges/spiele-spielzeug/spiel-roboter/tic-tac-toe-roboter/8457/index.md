---
layout: "image"
title: "X-Achse"
date: "2007-01-14T13:57:05"
picture: "tictactoeroboter3.jpg"
weight: "3"
konstrukteure: 
- "Jan Käberich"
fotografen:
- "Käberich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- details/8457
imported:
- "2019"
_4images_image_id: "8457"
_4images_cat_id: "777"
_4images_user_id: "521"
_4images_image_date: "2007-01-14T13:57:05"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8457 -->
Der Powermoter wurde noch weiter untersetzt, weil der Roboter sonst zu schnell ist und die Impulsgenauigkeit abnimmt.