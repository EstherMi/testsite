---
layout: "image"
title: "Antrieb X-Achse"
date: "2007-01-14T13:57:05"
picture: "tictactoeroboter4.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- details/8458
imported:
- "2019"
_4images_image_id: "8458"
_4images_cat_id: "777"
_4images_user_id: "521"
_4images_image_date: "2007-01-14T13:57:05"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8458 -->
Man sieht den Antriebsmotor der X-Achse und die Taster mit denen man das Feld auswählt auf das man selber setzen möchte. Die Kondensatoren an den Tastern sind notwendig, weil die "alten" Taster Störimpulse erzeugen.