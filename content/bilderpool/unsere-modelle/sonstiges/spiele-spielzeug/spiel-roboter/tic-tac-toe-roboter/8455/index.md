---
layout: "image"
title: "Gesamtansicht"
date: "2007-01-14T13:57:05"
picture: "tictactoeroboter1.jpg"
weight: "1"
konstrukteure: 
- "kehrblech"
fotografen:
- "kehrblech"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- details/8455
imported:
- "2019"
_4images_image_id: "8455"
_4images_cat_id: "777"
_4images_user_id: "521"
_4images_image_date: "2007-01-14T13:57:05"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8455 -->
Der Roboter spielt und berechnet wo er den nächsten Stein hinlegen muss, um nicht zu verlieren oder um zu gewinnen.
Wenn man zwei Steine in einer Reihe hat blockiert er den letzten Platz und wenn man eine Zwickmühle vorbereitet zerstört er sie bevor sie fertig ist. Er selbst wählt per Zufall einen Platz aus, nur wenn er gewinnen oder eine Zwickmühle bauen kann tut er das.