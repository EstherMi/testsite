---
layout: "image"
title: "Flipper"
date: "2007-03-11T12:07:55"
picture: "IMG_0138.jpg"
weight: "14"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- details/9394
imported:
- "2019"
_4images_image_id: "9394"
_4images_cat_id: "776"
_4images_user_id: "557"
_4images_image_date: "2007-03-11T12:07:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9394 -->
