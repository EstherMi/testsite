---
layout: "image"
title: "Fußballroboter WM 2014"
date: "2014-06-16T19:00:23"
picture: "IMG_0265_800x598.jpg"
weight: "1"
konstrukteure: 
- "Swen"
fotografen:
- "Swen"
keywords: ["Spiel-Roboter"]
uploadBy: "Swen98"
license: "unknown"
legacy_id:
- details/38949
imported:
- "2019"
_4images_image_id: "38949"
_4images_cat_id: "776"
_4images_user_id: "2195"
_4images_image_date: "2014-06-16T19:00:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38949 -->
Hier ist mein selbstgebauter Fußballroboter.
Mehr Fotos sehen Sie in mein youtube Video.

Videolink: https://www.youtube.com/watch?v=9uN_HYPgiGU