---
layout: "image"
title: "Fußball WM 2014 Modell - Übersicht 2"
date: "2014-06-11T21:54:26"
picture: "WM3.jpg"
weight: "2"
konstrukteure: 
- "Jonas"
fotografen:
- "Markus"
keywords: ["Fußball", "WM", "2014", "Passspiel"]
uploadBy: "x coaster"
license: "unknown"
legacy_id:
- details/38932
imported:
- "2019"
_4images_image_id: "38932"
_4images_cat_id: "2912"
_4images_user_id: "1608"
_4images_image_date: "2014-06-11T21:54:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38932 -->
Hier eine weitere kleine Übersicht über das Modell. Die Spieler auf dem Spielfeld passen sich hin und her. Alle Spielfiguren können sich drehen und schießen.

Videolink: http://youtu.be/89wQyRqwreI