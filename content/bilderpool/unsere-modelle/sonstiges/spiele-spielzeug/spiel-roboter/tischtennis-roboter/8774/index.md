---
layout: "image"
title: "Gesamtansicht"
date: "2007-02-01T17:26:58"
picture: "tischtennisroboter7.jpg"
weight: "7"
konstrukteure: 
- "Jan Käberich"
fotografen:
- "A.Käberich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- details/8774
imported:
- "2019"
_4images_image_id: "8774"
_4images_cat_id: "801"
_4images_user_id: "521"
_4images_image_date: "2007-02-01T17:26:58"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8774 -->
Der komplette Roboter an der Platte.