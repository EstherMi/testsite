---
layout: "image"
title: "Seitenansicht"
date: "2007-02-01T17:26:58"
picture: "tischtennisroboter2.jpg"
weight: "2"
konstrukteure: 
- "Jan Käberich"
fotografen:
- "A.Käberich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- details/8769
imported:
- "2019"
_4images_image_id: "8769"
_4images_cat_id: "801"
_4images_user_id: "521"
_4images_image_date: "2007-02-01T17:26:58"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8769 -->
Der Roboter von der Seite. Die stützen braucht man, weil das Wurf-Rad ziehmlich schnell wird.