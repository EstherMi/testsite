---
layout: "image"
title: "Flipper"
date: "2012-02-04T20:53:42"
picture: "flipper1.jpg"
weight: "13"
konstrukteure: 
- "Hallo111"
fotografen:
- "Hallo111"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Hallo111"
license: "unknown"
legacy_id:
- details/34080
imported:
- "2019"
_4images_image_id: "34080"
_4images_cat_id: "776"
_4images_user_id: "1281"
_4images_image_date: "2012-02-04T20:53:42"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34080 -->
Ich habe das Bild der Verpackung des ROBO TX ElectroPneumatic Kastens als Anreiz genommen um  mal selbst einen Flipper mit Flexschienen zu basteln.