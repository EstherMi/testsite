---
layout: "image"
title: "'Bor de Wolf ' noch ohne Rotkäppchen"
date: "2008-05-16T21:40:32"
picture: "Bor_de_Wolf_005.jpg"
weight: "60"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/14528
imported:
- "2019"
_4images_image_id: "14528"
_4images_cat_id: "781"
_4images_user_id: "22"
_4images_image_date: "2008-05-16T21:40:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14528 -->
