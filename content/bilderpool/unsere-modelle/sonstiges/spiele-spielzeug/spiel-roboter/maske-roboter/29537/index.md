---
layout: "image"
title: "August  Pneumatik  ( =Bruder Eucalypta )"
date: "2010-12-26T10:55:44"
picture: "August-Maske-Robot_001.jpg"
weight: "1"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/29537
imported:
- "2019"
_4images_image_id: "29537"
_4images_cat_id: "781"
_4images_user_id: "22"
_4images_image_date: "2010-12-26T10:55:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29537 -->
Maske-Robot  August