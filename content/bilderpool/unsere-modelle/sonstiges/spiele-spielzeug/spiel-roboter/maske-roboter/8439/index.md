---
layout: "image"
title: "Eucalypta Hexe"
date: "2007-01-13T22:12:46"
picture: "Eucalypa_Heks__Hexe_002_2.jpg"
weight: "86"
konstrukteure: 
- "Peter Damen (Poederoyen Holland)"
fotografen:
- "Peter Damen (Poederoyen Holland)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/8439
imported:
- "2019"
_4images_image_id: "8439"
_4images_cat_id: "781"
_4images_user_id: "22"
_4images_image_date: "2007-01-13T22:12:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8439 -->
Mein (einfaches) Problem ist:

Wie schaffe ich es in einem Hauptprogram mehrere Unterprogrammen zugleicherzeit funktionieren zu lassen ?

Programme "Eucalypta-scenario-1" gibt es unter Downloads.

Gruss,

Peter Damen
Poederoyen, Holland