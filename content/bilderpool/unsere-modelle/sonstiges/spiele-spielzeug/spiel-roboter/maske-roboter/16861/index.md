---
layout: "image"
title: "Schlange-Antrieb"
date: "2009-01-04T14:10:38"
picture: "Fischertechnik__Kalmthoutse-Heide_038.jpg"
weight: "14"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/16861
imported:
- "2019"
_4images_image_id: "16861"
_4images_cat_id: "781"
_4images_user_id: "22"
_4images_image_date: "2009-01-04T14:10:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16861 -->
Schlange-Antrieb