---
layout: "image"
title: "Eucalypta Hexe"
date: "2007-01-13T20:16:26"
picture: "Eucalypa_Heks__Hexe_001.jpg"
weight: "78"
konstrukteure: 
- "Peter Damen (Poederoyen Holland)"
fotografen:
- "Peter Damen (Poederoyen Holland)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/8431
imported:
- "2019"
_4images_image_id: "8431"
_4images_cat_id: "781"
_4images_user_id: "22"
_4images_image_date: "2007-01-13T20:16:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8431 -->
Ik ben momenteel de Efteling-heks aan het nabouwen met allerlei functies. 

M'n heks Eucalypta heeft de volgende functie's:

pneumatisch omhoog 
nee-bewegen (electrisch) 
ja-knikken (electrisch) 
tong uitsteken (electrisch) 
wenkbrouwen fronzen (pneumatisch) 
wangen opblazen (pneumatisch) 
een vinger-signaal geven (pneumatisch) 
zwaaien (electrisch)

Voor alle functies heb ik een sub-programma geschreven.
 
Het probleem waar ik mee zit is dat ik binnen een hoofdprogramma tegelijkertijd meerdere sub-progamma's (functies) wil laten werken. 
Dus niet alleen achter elkaar. 
 
Hoe kan ik binnen een hoofdprogramma tegelijkertijd meerdere sub-progamma's laten werken ?