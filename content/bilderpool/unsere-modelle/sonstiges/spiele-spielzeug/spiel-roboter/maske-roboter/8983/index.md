---
layout: "image"
title: "Eucalypta Hexe mit Kleider und Voice-modulen"
date: "2007-02-12T17:46:56"
picture: "Explorer-Eucalypta_002.jpg"
weight: "74"
konstrukteure: 
- "Peter Damen (Poederoyen Holland)"
fotografen:
- "Peter Damen (Poederoyen Holland)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/8983
imported:
- "2019"
_4images_image_id: "8983"
_4images_cat_id: "781"
_4images_user_id: "22"
_4images_image_date: "2007-02-12T17:46:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8983 -->
Eucalypta Hexe mit Kleider und Voice-modulen