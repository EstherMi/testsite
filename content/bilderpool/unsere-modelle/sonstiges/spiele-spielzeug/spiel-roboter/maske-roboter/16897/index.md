---
layout: "image"
title: "Elefant mit Vakuum-Rüssel"
date: "2009-01-04T19:02:46"
picture: "Fischertechnik__Kampvuur-Poederoyen_001_2.jpg"
weight: "50"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/16897
imported:
- "2019"
_4images_image_id: "16897"
_4images_cat_id: "781"
_4images_user_id: "22"
_4images_image_date: "2009-01-04T19:02:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16897 -->
Elefant mit Vakuum-Rüssel