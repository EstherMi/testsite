---
layout: "comment"
hidden: true
title: "6524"
date: "2008-05-24T17:49:25"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Der Roboter ist tatsächlich für ein Puppenspiel für meine Kinder. 

Der graue Luftsack (von einer Blutdruckmessgerät) dient um die Wolf-rucken hoch zu setzen.

Die Haut der Wolf gibt es noch nicht. Meine Frau Elly macht das.

Mit eine Poti in einer SubProgram versuche ich die Zahl der Wolfbeissen ("happen" auf niederländisch) zu regeln. 
Ich schaffe dass leider noch nicht.
Vielleicht kann jemand mir helfen ?....

Eingang:
Min. Pot-werte =  8
Max. Pot-werte = 1023

Ausgang:
Min. = 1x beissen (happen)
Max.= 8x beissen

8a     +  b = 1
1023a + b = 8

Hieraus stelt sich aus:
a = 7/145
b = -8/145 + 1

Mit Operatoren schaffe ich dass noch in Robopro.

Wie schaffe ich es dann aber die ausgerechnete variabele (1 bis 8) dass subprogramm (1 bis 8) zu regeln ?....

Gruss,

Peter Damen
Poederoyen NL