---
layout: "image"
title: "August  Pneumatik  ( =Bruder Eucalypta )"
date: "2010-12-26T10:55:45"
picture: "August-Maske-Robot_004.jpg"
weight: "6"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/29542
imported:
- "2019"
_4images_image_id: "29542"
_4images_cat_id: "781"
_4images_user_id: "22"
_4images_image_date: "2010-12-26T10:55:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29542 -->
Maske-Robot  August