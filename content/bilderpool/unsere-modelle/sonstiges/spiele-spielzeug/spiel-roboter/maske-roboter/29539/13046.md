---
layout: "comment"
hidden: true
title: "13046"
date: "2010-12-27T13:31:11"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Die direkt gesteuerte 3/2-Wege-Magnetventile + Grundplatten für Mini-Magnetventile 15 mm
gibt es bei  Riegler :
http://www.riegler.de/pages/04zylinder-steuer/mini-magnet-vent.shop
oder in PDF:    http://www.riegler.de/PDF_DE/15d/15-35.pdf

Grüss, 

Peter 
Poederoyen NL