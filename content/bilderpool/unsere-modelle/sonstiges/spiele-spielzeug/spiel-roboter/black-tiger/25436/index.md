---
layout: "image"
title: "ohne Gehäuse"
date: "2009-09-28T21:32:25"
picture: "blacktiger10.jpg"
weight: "9"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebstian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- details/25436
imported:
- "2019"
_4images_image_id: "25436"
_4images_cat_id: "1780"
_4images_user_id: "791"
_4images_image_date: "2009-09-28T21:32:25"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25436 -->
