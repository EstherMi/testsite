---
layout: "image"
title: "Der Auswurf"
date: "2012-11-24T19:00:27"
picture: "der_auswurf.jpg"
weight: "9"
konstrukteure: 
- "Zahnrädchen 001"
fotografen:
- "Zahnrädchen 001"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Zahnrädchen001"
license: "unknown"
legacy_id:
- details/36170
imported:
- "2019"
_4images_image_id: "36170"
_4images_cat_id: "776"
_4images_user_id: "1565"
_4images_image_date: "2012-11-24T19:00:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36170 -->
Die Spielsteine des Computers werden in den Behälter gesteckt.