---
layout: "image"
title: "3ter Ballabschießer"
date: "2013-01-22T17:34:38"
picture: "Bild_2.jpg"
weight: "3"
konstrukteure: 
- "Jonas"
fotografen:
- "Jonas"
keywords: ["Flipper"]
uploadBy: "x coaster"
license: "unknown"
legacy_id:
- details/36503
imported:
- "2019"
_4images_image_id: "36503"
_4images_cat_id: "776"
_4images_user_id: "1608"
_4images_image_date: "2013-01-22T17:34:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36503 -->
Hier ist der dritte Ballabschießer (weiß nicht wie das heißt).
Er ist an der oberen Hälfte des Flippers angebracht.