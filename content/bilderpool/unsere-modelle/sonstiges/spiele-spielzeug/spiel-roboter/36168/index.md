---
layout: "image"
title: "Gesamtansicht"
date: "2012-11-24T19:00:27"
picture: "Gesamtansicht.jpg"
weight: "7"
konstrukteure: 
- "Zahnrädchen 001"
fotografen:
- "Zahnrädchen 001"
keywords: ["tic", "tac", "toe"]
uploadBy: "Zahnrädchen001"
license: "unknown"
legacy_id:
- details/36168
imported:
- "2019"
_4images_image_id: "36168"
_4images_cat_id: "776"
_4images_user_id: "1565"
_4images_image_date: "2012-11-24T19:00:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36168 -->
Dies ist die Gesamtansicht meines Tic-Tac-Toe-Roboters.