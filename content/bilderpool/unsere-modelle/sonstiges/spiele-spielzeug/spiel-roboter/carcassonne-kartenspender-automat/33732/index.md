---
layout: "image"
title: "Display (Fehler)"
date: "2011-12-23T19:30:21"
picture: "cac13.jpg"
weight: "13"
konstrukteure: 
- "B.-M. Klug"
fotografen:
- "B.-M. Klug"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Birne"
license: "unknown"
legacy_id:
- details/33732
imported:
- "2019"
_4images_image_id: "33732"
_4images_cat_id: "2494"
_4images_user_id: "1142"
_4images_image_date: "2011-12-23T19:30:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33732 -->
Displayansicht einer Fehlermeldung.
Der Automat führt nach dem Einschalten einen kurzen Selbsttest durch. In diesem Fall stand der hintere Auswurfmechanismus nicht in der richtigen Position.