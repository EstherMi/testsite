---
layout: "image"
title: "Gesamtansicht Vorne"
date: "2011-12-23T14:18:30"
picture: "cac1.jpg"
weight: "1"
konstrukteure: 
- "B.-M. Klug"
fotografen:
- "B.-M. Klug"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Birne"
license: "unknown"
legacy_id:
- details/33720
imported:
- "2019"
_4images_image_id: "33720"
_4images_cat_id: "2494"
_4images_user_id: "1142"
_4images_image_date: "2011-12-23T14:18:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33720 -->
Links ist der Ausgabebehälter zu sehen und oben sind die Schächte für die beiden Kartenstapel.