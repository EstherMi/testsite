---
layout: "image"
title: "Befüllter Automat (Draufsicht)"
date: "2011-12-23T19:30:21"
picture: "cac11.jpg"
weight: "10"
konstrukteure: 
- "B.-M. Klug"
fotografen:
- "B.-M. Klug"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Birne"
license: "unknown"
legacy_id:
- details/33729
imported:
- "2019"
_4images_image_id: "33729"
_4images_cat_id: "2494"
_4images_user_id: "1142"
_4images_image_date: "2011-12-23T19:30:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33729 -->
Blick von oben auf den befüllten Automaten.