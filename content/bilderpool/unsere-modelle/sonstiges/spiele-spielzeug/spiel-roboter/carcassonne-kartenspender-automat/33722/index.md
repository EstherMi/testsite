---
layout: "image"
title: "Blick in die Kartenschächte"
date: "2011-12-23T14:18:31"
picture: "cac3.jpg"
weight: "3"
konstrukteure: 
- "B.-M. Klug"
fotografen:
- "B.-M. Klug"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Birne"
license: "unknown"
legacy_id:
- details/33722
imported:
- "2019"
_4images_image_id: "33722"
_4images_cat_id: "2494"
_4images_user_id: "1142"
_4images_image_date: "2011-12-23T14:18:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33722 -->
Beim Blick in die noch leeren Kartenschächte kann man die Fototransistoren am Boden der Schächte erkennen. Nach dem Befüllen, werden zwei Lampen über die Schächte geklappt, welche vor jedem Auswurf für eine Sekunde eingeschaltet werden. Sollte einer der Fototransistoren nun Licht registrieren, wird automatisch der andere Schacht gewählt. Registrieren beide Licht, wird im Display des TX angezeigt, dass die Schächte leer sind.
Im Normalfall bekommen aber beide kein Licht und einer der Schächte wird zufällig ausgewählt.