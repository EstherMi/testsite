---
layout: "overview"
title: "Carcassonne Kartenspender Automat"
date: 2019-12-17T19:38:39+01:00
legacy_id:
- categories/2494
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2494 --> 
Nachdem die Bilder nun seit fast einem Jahr auf meiner HD warten, komme ich nun endlich zum Upload! Vorgestellt wird ein Automat, welcher nach dem Zufallsprinzip Carcassonnespielkarten aus einem von zwei Stapeln auswirft. Näheres steht unter den Bildern.