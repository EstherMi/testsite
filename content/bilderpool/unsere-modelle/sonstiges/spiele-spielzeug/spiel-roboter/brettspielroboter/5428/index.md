---
layout: "image"
title: "Greifer5"
date: "2005-11-29T20:12:06"
picture: "Brettspielroboter_005.jpg"
weight: "6"
konstrukteure: 
- "Christopher Wecht\ffcoe"
fotografen:
- "Christopher Wecht\ffcoe"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ffcoe"
license: "unknown"
legacy_id:
- details/5428
imported:
- "2019"
_4images_image_id: "5428"
_4images_cat_id: "461"
_4images_user_id: "332"
_4images_image_date: "2005-11-29T20:12:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5428 -->
