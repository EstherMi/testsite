---
layout: "comment"
hidden: true
title: "1842"
date: "2006-12-31T20:59:57"
uploadBy:
- "ffcoe"
license: "unknown"
imported:
- "2019"
---
Stefan hat es erfasst: Die drei Schnecken bilden die Z-Achse, wenn der Powermotor am hitnteren Ende des Arms vor dem Akku sich dreht. Die 2 äußeren Schnecken sind direkt an den Motor gekoppelt. Bei der Inneren schnecke wurde ein differentia zwischengeschaltet (links über dem Y-Drehkranz). Darüber wird dann noch ein Powermotor angebracht, mit dem man dann den Greifer steuern kann.

Gruß
Christoper aka ffcoe