---
layout: "image"
title: "5-Würfel-Puzzle 1"
date: "2008-10-11T22:48:04"
picture: "Puzzle1.jpg"
weight: "2"
konstrukteure: 
- "HLGR"
fotografen:
- "HLGR"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "HLGR"
license: "unknown"
legacy_id:
- details/15957
imported:
- "2019"
_4images_image_id: "15957"
_4images_cat_id: "1449"
_4images_user_id: "832"
_4images_image_date: "2008-10-11T22:48:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15957 -->
Es gibt 29 Möglichkeiten 5 Würfel flächig aneinanderzusetzen.
Diese bilden die Teile des 5-Würfel-Puzzles.