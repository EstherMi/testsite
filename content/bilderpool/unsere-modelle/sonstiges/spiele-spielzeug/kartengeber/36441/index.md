---
layout: "image"
title: "Kartengeber (Antrieb Kartenhalter)"
date: "2013-01-06T17:46:54"
picture: "kartengeber3.jpg"
weight: "3"
konstrukteure: 
- "Udo Henkel"
fotografen:
- "Udo Henkel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- details/36441
imported:
- "2019"
_4images_image_id: "36441"
_4images_cat_id: "2705"
_4images_user_id: "1112"
_4images_image_date: "2013-01-06T17:46:54"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36441 -->
Der Antrieb erfolgt direkt mit dem Encodermotor.