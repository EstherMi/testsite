---
layout: "overview"
title: "Kartengeber"
date: 2019-12-17T19:39:59+01:00
legacy_id:
- categories/2705
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2705 --> 
Das ist das alte Clubmodell des Kartengebers direkt auf einer Platte aufgebaut und erweitert mit einer Lichtschranke und Motorbetrieb. Der TX-Controller ist so programmiert, dass er zufällig die Karten an die 4 Kartenhalter ausgibt. Auf jedem Kartenhalter sind nach Ausgabe jeweils 10 Karten, z.B. für Doppelkopf. Man könnte es aber auch für Skat nutzen. Das Programm ist so optimiert, dass er immer den kürzesten Weg wählt zum nächsten Kartenhalter. Der Encodermotor sorgt dafür, dass die genaue Position angefahren werden kann. Falls Interesse besteht, kann ich das Programm sonst hochladen. In Funktion kann man den Kartengeber hier sehen: http://henkel.homedns.org/Fischertechnik/kartengeber.mp4