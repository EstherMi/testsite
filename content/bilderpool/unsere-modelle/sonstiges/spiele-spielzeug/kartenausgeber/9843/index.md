---
layout: "image"
title: "Kartenausgeber 12"
date: "2007-03-28T15:09:59"
picture: "kartenausgeber12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/9843
imported:
- "2019"
_4images_image_id: "9843"
_4images_cat_id: "886"
_4images_user_id: "502"
_4images_image_date: "2007-03-28T15:09:59"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9843 -->
Hier drückt die Karte auf die Platte die dann auf den Taster drückt.