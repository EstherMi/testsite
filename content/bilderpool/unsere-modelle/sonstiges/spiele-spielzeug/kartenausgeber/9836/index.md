---
layout: "image"
title: "Kartenausgeber 5"
date: "2007-03-28T15:09:55"
picture: "kartenausgeber05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/9836
imported:
- "2019"
_4images_image_id: "9836"
_4images_cat_id: "886"
_4images_user_id: "502"
_4images_image_date: "2007-03-28T15:09:55"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9836 -->
