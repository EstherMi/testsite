---
layout: "image"
title: "Kartenausgeber 6"
date: "2007-03-28T15:09:55"
picture: "kartenausgeber06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/9837
imported:
- "2019"
_4images_image_id: "9837"
_4images_cat_id: "886"
_4images_user_id: "502"
_4images_image_date: "2007-03-28T15:09:55"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9837 -->
Die großen Reifen mit den Gummis schieben die Karten nach oben.