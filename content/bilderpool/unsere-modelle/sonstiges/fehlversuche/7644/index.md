---
layout: "image"
title: "Fehlversuch 1"
date: "2006-11-28T21:19:00"
picture: "Vehikel01b.jpg"
weight: "4"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- details/7644
imported:
- "2019"
_4images_image_id: "7644"
_4images_cat_id: "716"
_4images_user_id: "488"
_4images_image_date: "2006-11-28T21:19:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7644 -->
Hier mal der ziemlich mißglückte Versuch, ein kleines, einfaches Fahrzeug zu bauen, das folgende Kriterien erfüllen sollte:

- Vier Räder
- Lenkung
- Pendelachse
- aus Teilen eines einfachen Master-Baukasten nachbaubar

Hat leider nicht so ganz geklappt, weil die Lenkstange viel zu lang war. Außerdem hab ich erst nach dem Fotografieren gemerkt, daß ich zwei verschiedene Räder erwischt hatte. Aber zumindest erfüllt es die geforderten Kriterien. Ich hab zwar hier graue Bausteine verwendet (meine schwarzen sind alle am Gittermastkran), aber ich habe darauf geachtet, daß die entsprechenden Pendants auch alle im Master-Kasten enthalten sind.