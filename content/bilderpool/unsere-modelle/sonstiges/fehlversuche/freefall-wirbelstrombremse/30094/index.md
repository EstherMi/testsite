---
layout: "image"
title: "Übersicht"
date: "2011-02-19T23:02:57"
picture: "ffwirbelstrom1.jpg"
weight: "1"
konstrukteure: 
- "Martin W. (Masked)"
fotografen:
- "Martin W. (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/30094
imported:
- "2019"
_4images_image_id: "30094"
_4images_cat_id: "2218"
_4images_user_id: "373"
_4images_image_date: "2011-02-19T23:02:57"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30094 -->
Das war der Versuch eines FreeFallTowers mit Wirbelstrombremsen. Ist ziemlich kläglich gescheitert, eine echte Bremswirkung konnte ich nicht feststellen. Die Kupferteile sind "Automatenschienen" aus dem Elektro-Zubehör, die werden in Sicherungskästen verwendet. Sind relativ preiswert, in diesem Fall 6 Stück bei ebay für 2 Euro. Die Magneten sind 15x15x8er von supermagnete.de. Im Versuch hatte ich auf beiden Seiten des Turms Magnete+Schienen, geholfen hats trotzdem nix (Fallhöhe etwa 1,50m)