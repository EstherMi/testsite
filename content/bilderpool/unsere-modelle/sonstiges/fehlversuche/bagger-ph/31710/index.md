---
layout: "image"
title: "Kette ab"
date: "2011-08-30T19:15:00"
picture: "bagger09.jpg"
weight: "9"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- details/31710
imported:
- "2019"
_4images_image_id: "31710"
_4images_cat_id: "2363"
_4images_user_id: "1275"
_4images_image_date: "2011-08-30T19:15:00"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31710 -->
Hier sieht man wie die Kette aussieht.