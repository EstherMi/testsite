---
layout: "image"
title: "Antrieb"
date: "2011-08-30T19:15:00"
picture: "bagger07.jpg"
weight: "7"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- details/31708
imported:
- "2019"
_4images_image_id: "31708"
_4images_cat_id: "2363"
_4images_user_id: "1275"
_4images_image_date: "2011-08-30T19:15:00"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31708 -->
Der Antrieb erfolgt direckt über die Welle.