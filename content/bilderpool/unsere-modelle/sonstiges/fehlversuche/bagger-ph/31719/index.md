---
layout: "image"
title: "Vorderes Gelenk"
date: "2011-08-30T19:15:00"
picture: "bagger18.jpg"
weight: "18"
konstrukteure: 
- "PHH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- details/31719
imported:
- "2019"
_4images_image_id: "31719"
_4images_cat_id: "2363"
_4images_user_id: "1275"
_4images_image_date: "2011-08-30T19:15:00"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31719 -->
Von der Seite