---
layout: "image"
title: "Vorderer Teil"
date: "2011-08-30T19:15:00"
picture: "bagger22.jpg"
weight: "22"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- details/31723
imported:
- "2019"
_4images_image_id: "31723"
_4images_cat_id: "2363"
_4images_user_id: "1275"
_4images_image_date: "2011-08-30T19:15:00"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31723 -->
