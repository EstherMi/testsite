---
layout: "image"
title: "Zapu47.jpg"
date: "2011-05-14T22:39:44"
picture: "Zapu47.JPG"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/30567
imported:
- "2019"
_4images_image_id: "30567"
_4images_cat_id: "2277"
_4images_user_id: "4"
_4images_image_date: "2011-05-14T22:39:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30567 -->
Der helle Block ist eine Hartschaumplatte, die man sonst im Bad hinter den Fliesen findet. Das schwarze Z15 ist hier nur drin, damit die Kamera etwas zum Fokussieren hat.

Ein Video gibt es auch:
http://www.youtube.com/watch?v=vv-J1YtZDjk