---
layout: "image"
title: "Traktor 12"
date: "2007-01-23T16:01:39"
picture: "traktor12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/8616
imported:
- "2019"
_4images_image_id: "8616"
_4images_cat_id: "793"
_4images_user_id: "502"
_4images_image_date: "2007-01-23T16:01:39"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8616 -->
