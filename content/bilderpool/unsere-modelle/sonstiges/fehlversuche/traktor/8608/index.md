---
layout: "image"
title: "Traktor"
date: "2007-01-23T16:01:39"
picture: "traktor04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/8608
imported:
- "2019"
_4images_image_id: "8608"
_4images_cat_id: "793"
_4images_user_id: "502"
_4images_image_date: "2007-01-23T16:01:39"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8608 -->
