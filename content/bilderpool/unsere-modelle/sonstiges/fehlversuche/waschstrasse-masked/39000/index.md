---
layout: "image"
title: "Waschbürste"
date: "2014-07-06T22:44:06"
picture: "waschstrassemasked4.jpg"
weight: "4"
konstrukteure: 
- "Martin W. (Masked)"
fotografen:
- "Martin W. (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/39000
imported:
- "2019"
_4images_image_id: "39000"
_4images_cat_id: "2919"
_4images_user_id: "373"
_4images_image_date: "2014-07-06T22:44:06"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39000 -->
Als eigentliche Waschbürste dienen hier kleine Malerwalzen aus dem Baumarkt. Gibts für wenig Geld in ganz vielen verschiedenen Längen und Durchmessern.