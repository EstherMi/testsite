---
layout: "image"
title: "Bodenöffnung geöffnet"
date: "2014-07-06T22:44:06"
picture: "waschstrassemasked3.jpg"
weight: "3"
konstrukteure: 
- "Martin W. (Masked)"
fotografen:
- "Martin W. (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/38999
imported:
- "2019"
_4images_image_id: "38999"
_4images_cat_id: "2919"
_4images_user_id: "373"
_4images_image_date: "2014-07-06T22:44:06"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38999 -->
