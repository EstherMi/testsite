---
layout: "image"
title: "Bodenöffnung für Mitnehmer"
date: "2014-07-06T22:44:06"
picture: "waschstrassemasked2.jpg"
weight: "2"
konstrukteure: 
- "Martin W. (Masked)"
fotografen:
- "Martin W. (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/38998
imported:
- "2019"
_4images_image_id: "38998"
_4images_cat_id: "2919"
_4images_user_id: "373"
_4images_image_date: "2014-07-06T22:44:06"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38998 -->
Als Felgenmitnehmer dienen BS 7,5. Hat gut funktioniert. Die Klappe der Bodenöffnung hat sich nur durch ihr Eigengewicht nach jedem Durchlauf geschlossen