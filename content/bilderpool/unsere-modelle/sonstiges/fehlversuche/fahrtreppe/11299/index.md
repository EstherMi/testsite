---
layout: "image"
title: "Rolltreppe01.JPG"
date: "2007-08-05T17:03:18"
picture: "Rolltreppe01.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Fahrtreppe"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/11299
imported:
- "2019"
_4images_image_id: "11299"
_4images_cat_id: "1016"
_4images_user_id: "4"
_4images_image_date: "2007-08-05T17:03:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11299 -->
Es ist mal wieder diese bestimmte Jahreszeit. Die Convention naht heran, und ich habe wieder viel zu viele Projekte gleichzeitig in Arbeit.

Sie funktioniert nicht gescheit, wackelt und rappelt allerorten, und verschlingt Unmengen von recht speziellen Teilen.