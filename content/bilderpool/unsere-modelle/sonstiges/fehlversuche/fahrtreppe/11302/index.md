---
layout: "image"
title: "Rolltreppe05.JPG"
date: "2007-08-05T17:10:21"
picture: "Rolltreppe05.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/11302
imported:
- "2019"
_4images_image_id: "11302"
_4images_cat_id: "1016"
_4images_user_id: "4"
_4images_image_date: "2007-08-05T17:10:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11302 -->
Das Ganze sollte möglichst leicht und klein gebaut werden, deshalb fiel die Wahl auf den U-Träger-Adapter. Zwei BS15 mit Loch, nebeneinander, würden zur Not auch funktionieren.