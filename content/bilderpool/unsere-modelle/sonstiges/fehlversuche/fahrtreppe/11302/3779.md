---
layout: "comment"
hidden: true
title: "3779"
date: "2007-08-05T17:50:15"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Wohl dem, der von überhaupt keinem Teil weniger als unendlich hat ;-)

Aber Klasse: Eine funktionierende Rolltreppe aus ft habe ich hier schon lange vermisst.

Gruß,
Stefan