---
layout: "image"
title: "Gesamtansicht von der Seite"
date: "2007-01-20T19:32:30"
picture: "Laufroboter1-01b.jpg"
weight: "1"
konstrukteure: 
- "Franz Osten (Jettaheizer"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- details/8574
imported:
- "2019"
_4images_image_id: "8574"
_4images_cat_id: "790"
_4images_user_id: "488"
_4images_image_date: "2007-01-20T19:32:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8574 -->
Ich wollte zwischendurch mal etwas anderes bauen, um auf andere Gedanken zu kommen. Es sollte ein kleiner Laufroboter nach bekanntem Konzept sein, der per Kabelfernsteuerung durch die Wohnung marschieren sollte. Leider waren die Hebelverhältnisse so ungünstig, daß die Kraft der Motoren kaum ausreichte und sich ständig die Achsen samt Rollenböcken verdreht haben.