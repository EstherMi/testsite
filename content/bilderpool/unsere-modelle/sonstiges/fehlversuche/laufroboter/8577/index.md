---
layout: "image"
title: "Ansicht von vorne"
date: "2007-01-20T19:32:30"
picture: "Laufroboter1-04b.jpg"
weight: "4"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- details/8577
imported:
- "2019"
_4images_image_id: "8577"
_4images_cat_id: "790"
_4images_user_id: "488"
_4images_image_date: "2007-01-20T19:32:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8577 -->
Ich würde ja gerne mal etwas bauen, was so richtig funktioniert, wie ich mir das vorstelle....