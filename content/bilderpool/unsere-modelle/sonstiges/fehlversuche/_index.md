---
layout: "overview"
title: "Fehlversuche..."
date: 2019-12-17T19:34:37+01:00
legacy_id:
- categories/716
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=716 --> 
Alles, was nicht so recht was geworden ist, landet hier. Vielleicht hat ja jemand anderes eine Idee, wie man daraus noch etwas sinnvolles machen kann...