---
layout: "image"
title: "Tür1_59.JPG"
date: "2007-01-19T13:56:54"
picture: "Tr1_59.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/8522
imported:
- "2019"
_4images_image_id: "8522"
_4images_cat_id: "786"
_4images_user_id: "4"
_4images_image_date: "2007-01-19T13:56:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8522 -->
Einer der diversen Entwürfe für die Kabinentüren. Sie sollen wie beim echten Flieger aus der Seitenwand nach außen schwenken und sich dann seitlich von der Öffnung weg bewegen. Das ganze auch, wenn aus der Außenwand noch S-Riegel herausstehen und Verkleidungsplatten aufgebracht sind. Außerdem müssen die Türen über den Flügeln aus demselben Grund noch nach oben ausweichen können, daher die schwarze K-Achse anstelle eines Scharniers. Auf dieser Achse kann die ganze Tür auf und ab verschoben werden.

Das schwarze Teil ist das Innenstück aus einem kaputten BS15 mit Doppelzapfen. 


Weitere Ergebnisse auf der Suche nach der richtigen Tür gibt es hier zu sehen:
http://www.ftcommunity.de/categories.php?cat_id=773