---
layout: "image"
title: "BugFW53.JPG"
date: "2007-01-19T13:38:09"
picture: "BugFW53.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["35998", "Lenkklaue", "modding"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/8519
imported:
- "2019"
_4images_image_id: "8519"
_4images_cat_id: "786"
_4images_user_id: "4"
_4images_image_date: "2007-01-19T13:38:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8519 -->
Bugfahrwerk ausgefahren. Flugrichtung ist nach rechts. Durch die Gelenksteine an den Drehscheiben ergibt sich eine Fahrwerksverriegelung, die mit einem satten "Klack" einrastet. Als federndes Element dient dazu die schwarze Rastachse.

Das Schneckengetriebe rechts ist etwas "gemoddet": die Achse wurde aufgebohrt, dann wurden eine dünne Kunststoffachse eingeklebt und darauf eine Plexiglasscheibe aufgeklebt. Damit kann die Schnecke in beiden Richtungen mit Kraft arbeiten, ohne dass das Getriebe auseinandergezogen wird. Die Schnecke (sieht man hier nicht) ist von der Sorte ohne Klemmung und ist mit der Achse verstiftet.