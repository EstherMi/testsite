---
layout: "image"
title: "Tür2_66.JPG"
date: "2007-01-19T14:01:13"
picture: "Tr2_66.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/8523
imported:
- "2019"
_4images_image_id: "8523"
_4images_cat_id: "786"
_4images_user_id: "4"
_4images_image_date: "2007-01-19T14:01:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8523 -->
Das hier kommt in etwa hin.