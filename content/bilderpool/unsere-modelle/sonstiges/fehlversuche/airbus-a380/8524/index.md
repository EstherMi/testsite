---
layout: "image"
title: "A380-Erlkönig06.JPG"
date: "2007-01-19T14:04:21"
picture: "A30-Erlknig06.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/8524
imported:
- "2019"
_4images_image_id: "8524"
_4images_cat_id: "786"
_4images_user_id: "4"
_4images_image_date: "2007-01-19T14:04:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8524 -->
Und dabei wollte ich, nach den Transportproblemen mit A340 und Antonov, nichts mehr so großes bauen.