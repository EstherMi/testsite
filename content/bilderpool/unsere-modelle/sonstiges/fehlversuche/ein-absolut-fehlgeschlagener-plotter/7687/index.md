---
layout: "image"
title: "Seilführung (1)"
date: "2006-12-03T21:28:40"
picture: "plotterfehlschlag24.jpg"
weight: "24"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/7687
imported:
- "2019"
_4images_image_id: "7687"
_4images_cat_id: "724"
_4images_user_id: "104"
_4images_image_date: "2006-12-03T21:28:40"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7687 -->
Das untere der beiden hellen Seile bewirkt das Verschieben des ganzen Querträgers. Das obere bewirkt das verschieben des Stiftträgers quer dazu. Das blaue Seil (links im Bild) bewirkt das Auf und Ab des Stiftes. Es kommt auf der gegenüberliegenden Seite heraus.