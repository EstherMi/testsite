---
layout: "image"
title: "Stiftaufnahme (2)"
date: "2006-12-03T21:28:17"
picture: "plotterfehlschlag03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/7666
imported:
- "2019"
_4images_image_id: "7666"
_4images_cat_id: "724"
_4images_user_id: "104"
_4images_image_date: "2006-12-03T21:28:17"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7666 -->
Ein Stift konnte eingeklemmt werden, wenn man ihn da rein drückt. Zieht man, kann man ihn entnehmen. Aber dazu muss jemand den Stift festhalten bzw. loslassen, und das leistete die simple Befestigung der Stifte außen am Rand des Plotterfeldes nicht.