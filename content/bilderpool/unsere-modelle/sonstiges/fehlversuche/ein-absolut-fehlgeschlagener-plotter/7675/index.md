---
layout: "image"
title: "Antrieb (5)"
date: "2006-12-03T21:28:28"
picture: "plotterfehlschlag12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/7675
imported:
- "2019"
_4images_image_id: "7675"
_4images_cat_id: "724"
_4images_user_id: "104"
_4images_image_date: "2006-12-03T21:28:28"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7675 -->
Hier sieht man's noch mal.