---
layout: "image"
title: "Antrieb (9)"
date: "2006-12-03T21:28:28"
picture: "plotterfehlschlag16.jpg"
weight: "16"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/7679
imported:
- "2019"
_4images_image_id: "7679"
_4images_cat_id: "724"
_4images_user_id: "104"
_4images_image_date: "2006-12-03T21:28:28"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7679 -->
Eine Detailaufnahme.