---
layout: "image"
title: "Seilführung (4)"
date: "2006-12-03T21:28:40"
picture: "plotterfehlschlag27.jpg"
weight: "27"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/7690
imported:
- "2019"
_4images_image_id: "7690"
_4images_cat_id: "724"
_4images_user_id: "104"
_4images_image_date: "2006-12-03T21:28:40"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7690 -->
Umlenkrollen ohne Ende.