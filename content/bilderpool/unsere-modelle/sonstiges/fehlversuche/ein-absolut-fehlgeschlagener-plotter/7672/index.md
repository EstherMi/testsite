---
layout: "image"
title: "Antrieb (2)"
date: "2006-12-03T21:28:17"
picture: "plotterfehlschlag09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/7672
imported:
- "2019"
_4images_image_id: "7672"
_4images_cat_id: "724"
_4images_user_id: "104"
_4images_image_date: "2006-12-03T21:28:17"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7672 -->
Alles in allem eine furchtbare Materialschlacht mit wenig Aussicht auf größeren Erfolg.