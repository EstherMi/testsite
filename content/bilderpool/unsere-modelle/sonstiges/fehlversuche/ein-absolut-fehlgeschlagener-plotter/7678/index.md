---
layout: "image"
title: "Antrieb (8)"
date: "2006-12-03T21:28:28"
picture: "plotterfehlschlag15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/7678
imported:
- "2019"
_4images_image_id: "7678"
_4images_cat_id: "724"
_4images_user_id: "104"
_4images_image_date: "2006-12-03T21:28:28"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7678 -->
Der Gummiring bewirkt das Zusammendrücken der Klemm-Zahnrädchen und ist der Typ, der für die alten Reifen 45 gedacht war.