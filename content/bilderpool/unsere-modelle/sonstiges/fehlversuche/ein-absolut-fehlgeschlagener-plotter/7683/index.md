---
layout: "image"
title: "Stiftheber (2)"
date: "2006-12-03T21:28:28"
picture: "plotterfehlschlag20.jpg"
weight: "20"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/7683
imported:
- "2019"
_4images_image_id: "7683"
_4images_cat_id: "724"
_4images_user_id: "104"
_4images_image_date: "2006-12-03T21:28:28"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7683 -->
Aus einem etwas anderen Blickwinkel.