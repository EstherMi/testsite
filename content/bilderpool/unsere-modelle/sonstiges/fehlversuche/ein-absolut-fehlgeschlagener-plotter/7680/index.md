---
layout: "image"
title: "Impulsgeber (1)"
date: "2006-12-03T21:28:28"
picture: "plotterfehlschlag17.jpg"
weight: "17"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/7680
imported:
- "2019"
_4images_image_id: "7680"
_4images_cat_id: "724"
_4images_user_id: "104"
_4images_image_date: "2006-12-03T21:28:28"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7680 -->
Etwas groß geraten, aber der hatte wenigstens funktioniert.