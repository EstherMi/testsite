---
layout: "image"
title: "Stiftaufnahme (3)"
date: "2006-12-03T21:28:17"
picture: "plotterfehlschlag04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/7667
imported:
- "2019"
_4images_image_id: "7667"
_4images_cat_id: "724"
_4images_user_id: "104"
_4images_image_date: "2006-12-03T21:28:17"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7667 -->
Das ganze Gebilde gleitet an Stahlachsen auf und ab. Es wird von einem Seil an der Umlenkrolle nach oben gezogen. Der Träger ist so schwer, dass er problemlos von selbst nach unten sinkt, wenn das Seil losgelassen wird.