---
layout: "image"
title: "O&K 400 unvollendet"
date: "2009-01-21T16:56:56"
picture: "ok_honjo1_011.jpg"
weight: "2"
konstrukteure: 
- "honjo1"
fotografen:
- "honjo1"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "honjo1"
license: "unknown"
legacy_id:
- details/17121
imported:
- "2019"
_4images_image_id: "17121"
_4images_cat_id: "1535"
_4images_user_id: "14"
_4images_image_date: "2009-01-21T16:56:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17121 -->
