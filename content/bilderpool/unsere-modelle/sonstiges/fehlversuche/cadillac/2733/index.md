---
layout: "image"
title: "Cadi02"
date: "2004-10-21T19:39:52"
picture: "Cadi02.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Auto", "PKW", "Kombi"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2733
imported:
- "2019"
_4images_image_id: "2733"
_4images_cat_id: "270"
_4images_user_id: "4"
_4images_image_date: "2004-10-21T19:39:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2733 -->
Die Platten unter dem Wagenboden sind nicht nur Verkleidung, sondern tragende Teile. Trotz der windigen Konstruktion ist das Ganze doch recht verwindungssteif.