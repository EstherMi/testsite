---
layout: "image"
title: "Cadi07"
date: "2004-10-21T19:39:52"
picture: "Cadi07.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Auto", "PKW", "Kombi"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2738
imported:
- "2019"
_4images_image_id: "2738"
_4images_cat_id: "270"
_4images_user_id: "4"
_4images_image_date: "2004-10-21T19:39:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2738 -->
Links unten der Mann vom TÜV, der von der Hinterachskonstruktion noch nicht ganz überzeugt ist.