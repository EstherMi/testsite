---
layout: "image"
title: "Cadi04"
date: "2004-10-21T19:39:52"
picture: "Cadi04.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Auto", "PKW", "Kombi"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2735
imported:
- "2019"
_4images_image_id: "2735"
_4images_cat_id: "270"
_4images_user_id: "4"
_4images_image_date: "2004-10-21T19:39:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2735 -->
Die Scharniere der Heckklappe und das Rücklicht haben ebenfalls genau ihr Plätzchen gefunden.