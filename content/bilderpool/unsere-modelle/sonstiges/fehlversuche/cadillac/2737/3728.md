---
layout: "comment"
hidden: true
title: "3728"
date: "2007-07-26T23:58:37"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Och, naja... Darf ich die Aussage verweigern? ;-)

Dieses Differenzial hat wirklich einiges durchgemacht. Einmal das Verlängern der Abtriebsachsen, und dann eine längere Sitzung mit dem Taschenmesser (die Drehbank hatte ich seinerzeit noch nicht), um einen Zahnkranz für direkten Antrieb mittels Kette freizustellen.


Gruß,
Harald