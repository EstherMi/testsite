---
layout: "image"
title: "Cadi06"
date: "2004-10-21T19:39:52"
picture: "Cadi06.jpg"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Auto", "PKW", "Kombi", "suchmich", "modding"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2737
imported:
- "2019"
_4images_image_id: "2737"
_4images_cat_id: "270"
_4images_user_id: "4"
_4images_image_date: "2004-10-21T19:39:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2737 -->
Die Achsen des Differenzials wurden mittels Messinghülsen und kurzer Metallachsen verlängert.