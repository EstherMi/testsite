---
layout: "image"
title: "Historie - wie es auch nicht so recht funktionieren wollte"
date: "2015-07-31T17:07:41"
picture: "glasmurmelbahnhebekunst15.jpg"
weight: "15"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/41683
imported:
- "2019"
_4images_image_id: "41683"
_4images_cat_id: "3106"
_4images_user_id: "1557"
_4images_image_date: "2015-07-31T17:07:41"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41683 -->
Hier seht ihr ein Foto aus einem frühen Experimentierstadium. Anstelle Kette waren die Hubstangen über ein Seil verbunden. Leider war die Seildehnung unter Last zu groß. Je mehr Murmeln hoch gehoben wurden, umso mehr verschoben sich die Hubstangen gegeneinander. Die Anlenkung erfolgte direkt an der Seilscheibe (Drehscheibe).

Das Problem mit der Seildehnung ließ sich durch die Verwendung einer Kette beseitigen.

------------

An early stage of how things do not work reliably. A small nylon string was used. Due to the load applied to the mechanism the rope extended. This extension disaligned the lifting platforms. A chain cured the problem