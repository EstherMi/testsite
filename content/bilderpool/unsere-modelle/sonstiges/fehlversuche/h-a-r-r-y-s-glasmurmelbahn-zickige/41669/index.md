---
layout: "image"
title: "Abschied von der Hebekunst"
date: "2015-07-31T17:07:35"
picture: "glasmurmelbahnhebekunst01.jpg"
weight: "1"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["zickige", "Hebekunst", "Fahrkunst", "Fehlversuch"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/41669
imported:
- "2019"
_4images_image_id: "41669"
_4images_cat_id: "3106"
_4images_user_id: "1557"
_4images_image_date: "2015-07-31T17:07:35"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41669 -->
Hier ist sie noch in der Glasmurmelbahn montiert, so quasi als Beweis, daß es sie tatsächlich mal gab. Das Kabel zum Motor hatte ich schon abgezogen, als mir gerade noch rechtzeitig einfiel, daß ich vielleicht doch mal besser die Kamera holen sollte.

Zur Funktion:
Die Murmeln rollen von links an, werden vom Verteiler abwechselnd nach vorne und nach hinten geleitet. Die breite Spur der Tischtennisballstrecke verdeckt den Einlauf ein bißchen.

----------------

This is what is called "Hebekunst" but this particular construction failed miserably. Here you see it still mounted to the marble run but for the show I took it out and then scrapped it.

The intention was to have the marbles arriving from the left. A distributor sends them to the back and front alternating. Then they are entering the mechanism and get lifted upwards.