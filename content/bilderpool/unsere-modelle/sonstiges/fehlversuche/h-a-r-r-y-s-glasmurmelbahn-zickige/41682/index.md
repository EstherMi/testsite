---
layout: "image"
title: "Verschleiß - ein Grund für die Unzuverlässigkeit"
date: "2015-07-31T17:07:41"
picture: "glasmurmelbahnhebekunst14.jpg"
weight: "14"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/41682
imported:
- "2019"
_4images_image_id: "41682"
_4images_cat_id: "3106"
_4images_user_id: "1557"
_4images_image_date: "2015-07-31T17:07:41"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41682 -->
Auch an den Seiten zum Aluminium hin gibt es Scheuerstellen. Nur das an den 45er Alus kein Verhaken möglich war.

Ebenfalls haben die Kunststoffachsen und deren Lager im Getriebe Spuren der Belastung - hiervon gibt es allerdings keine brauchbaren Fotos.


--------------

Some more wear. Any new contruction needs to address this.