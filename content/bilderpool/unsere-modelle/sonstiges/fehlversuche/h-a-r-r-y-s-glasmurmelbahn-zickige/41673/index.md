---
layout: "image"
title: "Klemmnabe extrafest"
date: "2015-07-31T17:07:35"
picture: "glasmurmelbahnhebekunst05.jpg"
weight: "5"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["Glasmurmelbahn"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/41673
imported:
- "2019"
_4images_image_id: "41673"
_4images_cat_id: "3106"
_4images_user_id: "1557"
_4images_image_date: "2015-07-31T17:07:35"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41673 -->
Im Forum gab es mal die dezenten Hinweise auf "Papier" und "Bindfaden" um die Klemmwirkung einer Nabe zu verbessern. Auch in Hobbybüchern soll davon schon die Rede gewesen sein. Hier sieht man das endlich mal wieder!

------------------

ft is a nice construction toy but has some issues getting enough torque through the construction. Relatively early there were some tricks around: one or two layers of paper or a piece string put into the clamping hub increases the friction there.

Um das nötige Drehmoment von der Lenkwelle auf das Z40 zu übertragen, ging es nicht ohne beide Kniffe. Die am engsten sitzende zweiflügelige Klemmnabe aus meiner Sammlung ist mit zwei Lagen Zeitungspapier noch enger gemacht, im Schraubgewinde der Nabenmutter ist noch etwas Nähgarn untergebracht um die Klemmwirkung ebenfalls zu unterstützen. Erstaunlicherweise wird die Verbindung "über Nacht" noch etwas fester als direkt nach dem Anziehen der Nabenmutter. TST-Schlüssel habe ich hier absichtlich nicht verwendet um das Gewinde nicht zu ruinieren. Im Endergebnis reicht die Klemmwirkung sogar um einen S-Motor mit U-Getriebe und zwischengeschalteter Untersetzung 2:1 bis zur Blockade zu belasten.

Für Härtefälle gibt es bei TST noch seine Version der Klemmnabe, die dann ohne Tricks die nötige Klemmwirkung erzielt - allerdings unter der Rubrik "modding" laufen müßte.