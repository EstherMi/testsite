---
layout: "overview"
title: "H.A.R.R.Y.'s Glasmurmelbahn - die zickige Hebekunst"
date: 2019-12-17T19:35:14+01:00
legacy_id:
- categories/3106
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3106 --> 
Eine kleine Dokumentation über die zickige Hebekunst. An sich funktionierte sie, aber sie war einfach nicht zuverlässig genug für eine Ausstellung. Und deswegen landet sie hier, auf dem "Schrottplatz" der ft-Geschichte.
Das Prinzip zum Kugeltransport könnt ihr Euch hier anschauen: www.kugelbahn.info/deutsch/bergwerk/dusyma.html
Es ist die kleine Animation am Anfang des ersten Absatzes, die zeigt wie das so vor sich gehen sollte. In dem Fall hier sogar doppelseitig auf Vorder- und Rückseite gleichzeitig.

Zu Problemen führte eindeutig der Verschleiß (Abrieb) und das Getriebe mit den vielen Kunststoffachsen war zu instabil. Auch an der Geometrie der Hubplattformen war das Optimum wohl noch nicht erreicht. Die Durchmessertoleranzen der Murmeln sind nicht ausreichend berücksichtigt.

Die Pneumatik - ohne Kompressor - allerdings lief absolut perfekt.