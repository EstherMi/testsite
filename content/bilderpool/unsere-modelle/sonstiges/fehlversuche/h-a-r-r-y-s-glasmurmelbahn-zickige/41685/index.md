---
layout: "image"
title: "Historie - wie es auch nicht so recht funktionieren wollte"
date: "2015-07-31T17:07:41"
picture: "glasmurmelbahnhebekunst17.jpg"
weight: "17"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/41685
imported:
- "2019"
_4images_image_id: "41685"
_4images_cat_id: "3106"
_4images_user_id: "1557"
_4images_image_date: "2015-07-31T17:07:41"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41685 -->
Hier noch die Krone mit der Anlenkung per Lenkhebel aus der Nähe.

-----------

A close up of the top with the connectiong rod and a lever for the drive.