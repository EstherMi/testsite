---
layout: "comment"
hidden: true
title: "20963"
date: "2015-08-01T07:25:49"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Du hast die Antwort ja schon gefunden.

Hier noch der Hinweis an alle:
Da gibt es noch ein Rückschlagventil, versteckt hinter der I-Strebe.

Grüße
H.A.R.R.Y.