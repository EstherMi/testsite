---
layout: "comment"
hidden: true
title: "20983"
date: "2015-08-05T09:08:38"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ginge es vielleicht, die Drehscheibe nur auf einer Seite stabil zu lagern, um dann doch ihre exzentrischen Löcher für eine Achse des Pendelantriebs zu verwenden?
Gruß,
Stefan