---
layout: "image"
title: "Historie - wie es auch nicht so recht funktionieren wollte"
date: "2015-07-31T17:07:41"
picture: "glasmurmelbahnhebekunst16.jpg"
weight: "16"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/41684
imported:
- "2019"
_4images_image_id: "41684"
_4images_cat_id: "3106"
_4images_user_id: "1557"
_4images_image_date: "2015-07-31T17:07:41"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41684 -->
Hier eine Gesamtansicht, etwas andere Kronenkonstruktion. Die sah zwar ganz nett aus, aber die vielen Winkelsteine haben sich zu gern verschoben. Und die Seildehnung...

--------

Some more example of how things would not work. But at least looks nice.