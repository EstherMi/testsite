---
layout: "image"
title: "Gleitführung der Hubstangen"
date: "2015-07-31T17:07:41"
picture: "glasmurmelbahnhebekunst11.jpg"
weight: "11"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/41679
imported:
- "2019"
_4images_image_id: "41679"
_4images_cat_id: "3106"
_4images_user_id: "1557"
_4images_image_date: "2015-07-31T17:07:41"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41679 -->
Damit sich die Hubstangen auch nur schön auf und ab bewegen und nicht noch seitlich wegpendeln gibt es jeweils zwei Gleitführungen. Zwei Kufen sind auf einen BS7,5 montiert und laufen in den Nuten der Alus. Seitlich sind sie auf die Hubstangen aufgesetzt.