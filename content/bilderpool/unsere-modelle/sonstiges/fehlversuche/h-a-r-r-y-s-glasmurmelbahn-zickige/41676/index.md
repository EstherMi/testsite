---
layout: "image"
title: "Das Getriebe - alte Version"
date: "2015-07-31T17:07:35"
picture: "glasmurmelbahnhebekunst08.jpg"
weight: "8"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/41676
imported:
- "2019"
_4images_image_id: "41676"
_4images_cat_id: "3106"
_4images_user_id: "1557"
_4images_image_date: "2015-07-31T17:07:35"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41676 -->
Hier ist noch die kompakte ursprüngliche Version des Getriebes zu sehen, der S-Motor war nach den letzten Modifikationen nicht mehr kräftig genug und mußte gegen einen Encodermotor getauscht werden. Nicht wegen des Encoders, sondern weil dessen Abgabeleistung und Abgangsdrehzahl bei 6V halbwegs zur Mechanik paßten.

------------

The gear 'box' fromt he side. It is just a photo with the intended motor but this was not strong enough.