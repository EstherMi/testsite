---
layout: "overview"
title: "Segway"
date: 2019-12-17T19:35:09+01:00
legacy_id:
- categories/2438
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2438 --> 
Dies ist ein erster fehlgeschlagener Versuch eines Segways. Er konnte sich zwar mal für 2 Sekunden halten, aber mehr auch nicht. Auch die Verlagerung des Schwerpunktes nach oben half nichts.