---
layout: "image"
title: "Motoren"
date: "2011-10-05T11:04:35"
picture: "segway5.jpg"
weight: "7"
konstrukteure: 
- "FischerPapa"
fotografen:
- "FischerPapa"
keywords: ["Segway"]
uploadBy: "FischerPapa"
license: "unknown"
legacy_id:
- details/33105
imported:
- "2019"
_4images_image_id: "33105"
_4images_cat_id: "2438"
_4images_user_id: "1127"
_4images_image_date: "2011-10-05T11:04:35"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33105 -->
Ich habe zwei 50:1 Powermotoren (rote Kappe) verwendet, da ich keine anderen hatte. Ich hätte auch Minimotoren einbauen können, aber ich dachte die seien zu schwach, vielleicht sind sie sogar besser dazu geeignet, weil sie schneller sind.