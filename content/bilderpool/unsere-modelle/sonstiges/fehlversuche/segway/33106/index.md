---
layout: "image"
title: "Abstandsensor"
date: "2011-10-05T11:04:35"
picture: "segway6.jpg"
weight: "8"
konstrukteure: 
- "FischerPapa"
fotografen:
- "FischerPapa"
keywords: ["Segway"]
uploadBy: "FischerPapa"
license: "unknown"
legacy_id:
- details/33106
imported:
- "2019"
_4images_image_id: "33106"
_4images_cat_id: "2438"
_4images_user_id: "1127"
_4images_image_date: "2011-10-05T11:04:35"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33106 -->
Mit diesem Ultraschallsensor misst der Segway den Abstand zum Boden. Diese Methode hat den Vorteil, dass der Untergrund jede beliebige Farbe haben kann, aber hat auch den Nachteil, das der Segway nur horinzintale Strecken fahren kann, d.h. er kann nicht bergauf oder bergab fahren.