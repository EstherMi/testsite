---
layout: "image"
title: "Zweiter Prototyp"
date: "2014-10-05T20:00:43"
picture: "druckluftballbalanciererdreiduesen4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/39676
imported:
- "2019"
_4images_image_id: "39676"
_4images_cat_id: "2974"
_4images_user_id: "104"
_4images_image_date: "2014-10-05T20:00:43"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39676 -->
Das Grundprinzip funktioniert mit meinem Kompressor mit zwei Düsen und einem Ball ganz wunderbar. Der Ball kann, wenn er seitlich übergeben wird, sauber auf der nächste Düse platziert werden, ohne jemals mit etwas anderem als strömender Luft in Berührung zu kommen. Da aber selbst mein Kompressor mit 3,5 bar Druck und 15 L/min Durchsatz nicht genügen Luft für drei Düsen lieferte, wollte ich die Düsen näher beieinander unterbringen und auf diese Art Weg sparen. Leider reicht's auch dafür einfach nicht, auch mit Air-Tanks, Rückschlageventilen und allen möglichen Tricks. Zwei Düsen gehen wunderbar, aber für drei genügt der Druck einfach nicht.