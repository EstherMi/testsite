---
layout: "image"
title: "Prototyp 1"
date: "2014-10-05T20:00:43"
picture: "druckluftballbalanciererdreiduesen1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/39673
imported:
- "2019"
_4images_image_id: "39673"
_4images_cat_id: "2974"
_4images_user_id: "104"
_4images_image_date: "2014-10-05T20:00:43"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39673 -->
Die Idee ist, kräftig Druckluft aus drei Düsen zu blasen, und auf zweien davon je einen Tischtennisball balancieren zu lassen. Dann soll sich eine Düse so bewegen, dass der Ball in der Luft schwebend weit raus getragen und schließlich von der Seite auf die freie Düse platziert wird. Auf diese Weise soll sozusagen das Loch (der eine fehlende Ball) immer herumgereicht werden.