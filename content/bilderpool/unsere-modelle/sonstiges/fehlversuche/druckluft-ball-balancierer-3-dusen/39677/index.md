---
layout: "image"
title: "Bewegung (1)"
date: "2014-10-05T20:00:43"
picture: "druckluftballbalanciererdreiduesen5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/39677
imported:
- "2019"
_4images_image_id: "39677"
_4images_cat_id: "2974"
_4images_user_id: "104"
_4images_image_date: "2014-10-05T20:00:43"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39677 -->
Hier sieht man die linke Düse des vorigen Bildes auf halbem Wege gedreht.