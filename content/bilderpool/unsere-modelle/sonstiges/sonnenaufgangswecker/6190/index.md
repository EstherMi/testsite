---
layout: "image"
title: "Sonnen-aufgangs-wecker"
date: "2006-05-01T14:20:13"
picture: "Jakob_ft-Bilder1_2.jpg"
weight: "1"
konstrukteure: 
- "Jakob Wolf"
fotografen:
- "Jakob Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FFBBII"
license: "unknown"
legacy_id:
- details/6190
imported:
- "2019"
_4images_image_id: "6190"
_4images_cat_id: "534"
_4images_user_id: "420"
_4images_image_date: "2006-05-01T14:20:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6190 -->
Klein aber fein!
Sobald die Sonne auf die Lupe strahlt ertönt ein lautes summen.