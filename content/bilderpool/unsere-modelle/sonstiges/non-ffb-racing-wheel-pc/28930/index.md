---
layout: "image"
title: "(Update 05.10.10) Die Mechanik des Lenkrades"
date: "2010-10-05T16:42:50"
picture: "IMG_9393.jpg"
weight: "5"
konstrukteure: 
- "Simixus"
fotografen:
- "Simixus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Simixus"
license: "unknown"
legacy_id:
- details/28930
imported:
- "2019"
_4images_image_id: "28930"
_4images_cat_id: "2033"
_4images_user_id: "986"
_4images_image_date: "2010-10-05T16:42:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28930 -->
