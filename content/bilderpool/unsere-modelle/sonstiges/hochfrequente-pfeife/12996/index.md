---
layout: "image"
title: "Hochfrequente Pfeife"
date: "2007-12-04T16:58:09"
picture: "pfeife3.jpg"
weight: "3"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/12996
imported:
- "2019"
_4images_image_id: "12996"
_4images_cat_id: "1177"
_4images_user_id: "558"
_4images_image_date: "2007-12-04T16:58:09"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12996 -->
Ich habe das bild so bearbeitet das man so gut wie möglich erkennt wie rum das rad sein muss: die vertiefung muss jetzt zu sehen sein.