---
layout: "image"
title: "Kettenfertiger"
date: "2016-10-26T19:22:35"
picture: "kettenf1.jpg"
weight: "1"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/44685
imported:
- "2019"
_4images_image_id: "44685"
_4images_cat_id: "3327"
_4images_user_id: "558"
_4images_image_date: "2016-10-26T19:22:35"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44685 -->
Der Fertiger eignet sich am besten für schwarze Kettenglieder. Rote klappen auch, jedoch nicht so flüssig. Schiebt man die einzelnen Kettenglieder hinen, liegen die dünnen Bügel an der breiteren Seite tiefer und werden durch Anschieben von selbst passend unter das vorherige Kettenglied positioniert. Ablauf:
Kettenglied einlegen
Alu drücken
Kettenglied einlegen
Alu drücken
repeat
Spart unheimlich Zeit und Aufwand!