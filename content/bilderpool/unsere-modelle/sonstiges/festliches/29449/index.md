---
layout: "image"
title: "Advent Elf #2"
date: "2010-12-11T16:27:55"
picture: "elf_v2b.jpg"
weight: "27"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Advent", "Elf"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/29449
imported:
- "2019"
_4images_image_id: "29449"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2010-12-11T16:27:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29449 -->
My second attempt at an Advent Elf.