---
layout: "image"
title: "Yule Reindeer"
date: "2010-12-08T12:29:16"
picture: "reindeerB.jpg"
weight: "34"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Yule", "Reindeer", "Advent", "Calendar"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/29437
imported:
- "2019"
_4images_image_id: "29437"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2010-12-08T12:29:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29437 -->
A Yule Reindeer for the Advent Calendar