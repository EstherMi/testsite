---
layout: "image"
title: "Sculpture"
date: "2010-06-20T12:18:04"
picture: "ft_lady_1.jpg"
weight: "55"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Burning", "Man"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/27530
imported:
- "2019"
_4images_image_id: "27530"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2010-06-20T12:18:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27530 -->
In celebration of Burning man, I built this sculpture.