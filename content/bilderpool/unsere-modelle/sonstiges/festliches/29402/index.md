---
layout: "image"
title: "Menorah"
date: "2010-12-03T08:55:02"
picture: "menorah_b.jpg"
weight: "42"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Advent", "Calendar"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/29402
imported:
- "2019"
_4images_image_id: "29402"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2010-12-03T08:55:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29402 -->
This is a menorah model for the ft advent calendar.