---
layout: "image"
title: "Christmas Fireplace with Yeloow Stockings"
date: "2010-12-05T15:10:58"
picture: "ft_fireplace.jpg"
weight: "39"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["fireplace", "advent", "calendar"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/29413
imported:
- "2019"
_4images_image_id: "29413"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2010-12-05T15:10:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29413 -->
A Christmastime fireplace. This is a model for an Advent Calendar. (Note-I will render these models when I get a chance).