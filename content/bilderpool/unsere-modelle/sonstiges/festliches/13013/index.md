---
layout: "image"
title: "Weihnachsschmuck"
date: "2007-12-06T21:59:23"
picture: "Weihnachtsbaum.jpg"
weight: "69"
konstrukteure: 
- "Werwex"
fotografen:
- "Werwex"
keywords: ["Weihnachtsbaum", "Adventskalender", "Weihnachsschmuck"]
uploadBy: "werwex"
license: "unknown"
legacy_id:
- details/13013
imported:
- "2019"
_4images_image_id: "13013"
_4images_cat_id: "1180"
_4images_user_id: "689"
_4images_image_date: "2007-12-06T21:59:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13013 -->
Eine Idee vom Fischertechnik Adventskalender.
Der Weihnachtsbaum wächst langsam von 1 bis 24