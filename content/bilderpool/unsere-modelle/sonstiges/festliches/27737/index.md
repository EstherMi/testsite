---
layout: "image"
title: "Wolf"
date: "2010-07-09T08:53:48"
picture: "wolf_pic.jpg"
weight: "54"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Wolf", "Instructables"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/27737
imported:
- "2019"
_4images_image_id: "27737"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2010-07-09T08:53:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27737 -->
This is an alternative model from a set of ft components I created for the Instructables Gift Exchange! Does it look like a wolf?