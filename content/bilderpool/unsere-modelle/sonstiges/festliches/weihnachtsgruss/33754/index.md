---
layout: "image"
title: "Frohe Weihnachten!"
date: "2011-12-24T13:24:10"
picture: "schwibbogen1.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- details/33754
imported:
- "2019"
_4images_image_id: "33754"
_4images_cat_id: "739"
_4images_user_id: "791"
_4images_image_date: "2011-12-24T13:24:10"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33754 -->
