---
layout: "image"
title: "Santa"
date: "2012-03-23T00:54:36"
picture: "ftsanta2.jpg"
weight: "2"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Santa"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/34674
imported:
- "2019"
_4images_image_id: "34674"
_4images_cat_id: "739"
_4images_user_id: "585"
_4images_image_date: "2012-03-23T00:54:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34674 -->
Late this year. My cubist Santa