---
layout: "image"
title: "Weihnachtsgruß 2"
date: "2006-12-12T08:54:27"
picture: "Weihnachtsschlitten_4.jpg"
weight: "7"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/7878
imported:
- "2019"
_4images_image_id: "7878"
_4images_cat_id: "739"
_4images_user_id: "328"
_4images_image_date: "2006-12-12T08:54:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7878 -->
