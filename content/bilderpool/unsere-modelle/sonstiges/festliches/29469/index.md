---
layout: "image"
title: "Advent Baby Stroller"
date: "2010-12-18T14:02:46"
picture: "advent_buggie1.jpg"
weight: "10"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Advent", "baby", "stroller"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/29469
imported:
- "2019"
_4images_image_id: "29469"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2010-12-18T14:02:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29469 -->
This is  a micro model of a baby stroller