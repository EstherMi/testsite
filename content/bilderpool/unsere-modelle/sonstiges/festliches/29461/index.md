---
layout: "image"
title: "Advent Sled"
date: "2010-12-15T17:19:45"
picture: "sm_sledA.jpg"
weight: "20"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Advent", "Sled"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/29461
imported:
- "2019"
_4images_image_id: "29461"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2010-12-15T17:19:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29461 -->
This is a different view of the Advent Sled micro build.