---
layout: "image"
title: "Weihnachten"
date: "2011-12-25T14:29:38"
picture: "schmuck3.jpg"
weight: "3"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Fredy"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/33807
imported:
- "2019"
_4images_image_id: "33807"
_4images_cat_id: "1507"
_4images_user_id: "453"
_4images_image_date: "2011-12-25T14:29:38"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33807 -->
