---
layout: "image"
title: "Leprechaun"
date: "2012-04-04T21:27:20"
picture: "sm_lep_edited-2.jpg"
weight: "6"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Leprechaun"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/34749
imported:
- "2019"
_4images_image_id: "34749"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2012-04-04T21:27:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34749 -->
My cubist leprechaun. For St Patrick's Day.