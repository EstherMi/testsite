---
layout: "image"
title: "Kugel blau"
date: "2012-12-13T19:31:22"
picture: "kugelblau2.jpg"
weight: "3"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/36267
imported:
- "2019"
_4images_image_id: "36267"
_4images_cat_id: "2691"
_4images_user_id: "1"
_4images_image_date: "2012-12-13T19:31:22"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36267 -->
