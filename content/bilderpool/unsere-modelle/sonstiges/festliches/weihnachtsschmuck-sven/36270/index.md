---
layout: "image"
title: "Kugel blau"
date: "2012-12-13T19:31:22"
picture: "kugel5.jpg"
weight: "6"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/36270
imported:
- "2019"
_4images_image_id: "36270"
_4images_cat_id: "2691"
_4images_user_id: "1"
_4images_image_date: "2012-12-13T19:31:22"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36270 -->
Die blaue Kugel von oben.