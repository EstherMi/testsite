---
layout: "image"
title: "Krippe"
date: "2012-12-12T15:41:00"
picture: "weihnachten5.jpg"
weight: "17"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/36259
imported:
- "2019"
_4images_image_id: "36259"
_4images_cat_id: "2691"
_4images_user_id: "1"
_4images_image_date: "2012-12-12T15:41:00"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36259 -->
Weihnachtskrippen gibt es viele, aber bisher noch keine aus ft.
Hier nun eine einfache Weihnachtskrippe aus ft mit Beleuchtung.
Bestehend aus:
3x Bauplatte 500, 6x Winkelstein 60°, 2x Leuchsteine mit Lampe, Kabel, Netzteil und Power Controller (zum dimmen der Beleuchtung)