---
layout: "image"
title: "großer Stern"
date: "2012-12-12T16:16:39"
picture: "weihnachstsschmuck2.jpg"
weight: "13"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/36262
imported:
- "2019"
_4images_image_id: "36262"
_4images_cat_id: "2691"
_4images_user_id: "1"
_4images_image_date: "2012-12-12T16:16:39"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36262 -->
