---
layout: "image"
title: "Einen Stern ..."
date: "2012-12-13T20:16:12"
picture: "sternausachsen1.jpg"
weight: "2"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/36272
imported:
- "2019"
_4images_image_id: "36272"
_4images_cat_id: "2691"
_4images_user_id: "1"
_4images_image_date: "2012-12-13T20:16:12"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36272 -->
... der Deinen äh, mal ein Stern ganz anderer Art.
Hat doch auch irgendwie was, oder?