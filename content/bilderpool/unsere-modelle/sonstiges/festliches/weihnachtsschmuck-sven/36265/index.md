---
layout: "image"
title: "riesen Stern - beleuchtet"
date: "2012-12-12T17:11:59"
picture: "riesenstern1_2.jpg"
weight: "11"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/36265
imported:
- "2019"
_4images_image_id: "36265"
_4images_cat_id: "2691"
_4images_user_id: "1"
_4images_image_date: "2012-12-12T17:11:59"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36265 -->
Hier noch mal der riesen Stern.
Jetzt aber mit Beleuchtung.