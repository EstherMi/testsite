---
layout: "image"
title: "Weihnachtskugel andere Ansicht"
date: "2012-12-12T15:41:00"
picture: "weihnachten2.jpg"
weight: "12"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/36256
imported:
- "2019"
_4images_image_id: "36256"
_4images_cat_id: "2691"
_4images_user_id: "1"
_4images_image_date: "2012-12-12T15:41:00"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36256 -->
