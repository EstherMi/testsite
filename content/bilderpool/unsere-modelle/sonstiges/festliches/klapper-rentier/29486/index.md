---
layout: "image"
title: "Klapper-Rentier 2"
date: "2010-12-18T14:56:51"
picture: "Klapper-Rentier_2.jpg"
weight: "2"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/29486
imported:
- "2019"
_4images_image_id: "29486"
_4images_cat_id: "2146"
_4images_user_id: "328"
_4images_image_date: "2010-12-18T14:56:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29486 -->
Detailansicht vom Rentier-Kopf. An der Stange sieht man gut die Ratter-Spuren, die sich nach einer Weile einstellen.

Die Grundplatte braucht zwingend die vier Füße, weil ein fester Stand der Stange erforderlich ist, damit es wie gewünscht funktioniert.