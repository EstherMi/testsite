---
layout: "image"
title: "Klapper-Rentier 1"
date: "2010-12-18T14:56:51"
picture: "Klapper-Rentier_1.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/29485
imported:
- "2019"
_4images_image_id: "29485"
_4images_cat_id: "2146"
_4images_user_id: "328"
_4images_image_date: "2010-12-18T14:56:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29485 -->
Hier habe ich ein traditionelles weihnachtliches Spielzeug nachgebaut. Man zieht das Rentier an der Stange nach oben, und es bleibt zunächst haften. Durch leichtes Anstupsen der Nase rattert es sich dann langsam nach unten.