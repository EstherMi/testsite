---
layout: "image"
title: "Jaialdi 2010!"
date: "2010-08-01T11:23:54"
picture: "sm_jaialdi_cut_copy.jpg"
weight: "50"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Jaialdi"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/27790
imported:
- "2019"
_4images_image_id: "27790"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2010-08-01T11:23:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27790 -->
A model celebrating Jaialdi 2010!