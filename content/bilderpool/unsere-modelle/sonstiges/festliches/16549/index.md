---
layout: "image"
title: "Christmas Tree"
date: "2008-12-06T12:02:38"
picture: "sm_yt3.jpg"
weight: "60"
konstrukteure: 
- "Richard Mussler-Wright, Laura Baran, David Chase"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Christmas", "Tree", "PCS", "Brain"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/16549
imported:
- "2019"
_4images_image_id: "16549"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2008-12-06T12:02:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16549 -->
A Christmas Tree with lights programmed with the PCS Brain!