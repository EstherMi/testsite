---
layout: "image"
title: "Basque Stone Lifting"
date: "2010-08-01T11:23:54"
picture: "ft_stome2.jpg"
weight: "49"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Jaialdi", "harrijasotzaile"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/27789
imported:
- "2019"
_4images_image_id: "27789"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2010-08-01T11:23:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27789 -->
HAPPY JAIALDI 2010!
A harrijasotzaile lifting the stone.