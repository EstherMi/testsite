---
layout: "image"
title: "Christmas Ornaments"
date: "2010-12-28T22:19:29"
picture: "ft_resin2.jpg"
weight: "8"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["resn"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/29554
imported:
- "2019"
_4images_image_id: "29554"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2010-12-28T22:19:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29554 -->
This is a building block 30 inside a resin tree ornament.