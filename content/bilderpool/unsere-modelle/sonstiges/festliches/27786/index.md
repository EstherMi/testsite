---
layout: "image"
title: "Ontzi Eramatea"
date: "2010-07-29T09:24:39"
picture: "ft_churn_carry.jpg"
weight: "52"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Churn", "Carrying", "Ontzi", "Eramatea"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/27786
imported:
- "2019"
_4images_image_id: "27786"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2010-07-29T09:24:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27786 -->
This is the churn carrying event that takes place during Jaialdi! Thuoght to share.