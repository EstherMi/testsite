---
layout: "image"
title: "Advent Snowflake (White)"
date: "2010-12-10T00:52:54"
picture: "snowflake1B.jpg"
weight: "29"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Snowflake", "Advent", "Calendar"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/29447
imported:
- "2019"
_4images_image_id: "29447"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2010-12-10T00:52:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29447 -->
This is a Snowflake Model for an Advent Calendar. (I colored this one white)