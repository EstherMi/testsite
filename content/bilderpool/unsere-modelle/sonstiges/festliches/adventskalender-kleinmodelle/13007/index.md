---
layout: "image"
title: "Kran mit Handkurbel"
date: "2007-12-06T19:07:02"
picture: "adv4.jpg"
weight: "12"
konstrukteure: 
- "stefanft"
fotografen:
- "stefanft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stefanft"
license: "unknown"
legacy_id:
- details/13007
imported:
- "2019"
_4images_image_id: "13007"
_4images_cat_id: "1179"
_4images_user_id: "672"
_4images_image_date: "2007-12-06T19:07:02"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13007 -->
Ein Klassiker, immer wieder gerne gesehen ;-)