---
layout: "image"
title: "Karussell mit Handkurbel-Antrieb"
date: "2007-12-07T18:24:22"
picture: "karussell.jpg"
weight: "7"
konstrukteure: 
- "Stefan Schilling"
fotografen:
- "Stefan Schilling"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stefanft"
license: "unknown"
legacy_id:
- details/13016
imported:
- "2019"
_4images_image_id: "13016"
_4images_cat_id: "1179"
_4images_user_id: "672"
_4images_image_date: "2007-12-07T18:24:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13016 -->
Die Sitze mit Stangen sind per Kardangelenk befestigt. Und mit der 1:1 Übersetzung von Handkurbel zu Umdrehung des Karussells heben sich die Sitze beim Drehen fliehkraftbedingt an - halt in etwa so wie beim echten (Ketten-)Karussell.