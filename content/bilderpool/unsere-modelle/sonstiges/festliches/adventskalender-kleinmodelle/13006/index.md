---
layout: "image"
title: "Fussgängerampel"
date: "2007-12-06T19:07:02"
picture: "adv3.jpg"
weight: "11"
konstrukteure: 
- "stefanft"
fotografen:
- "stefanft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stefanft"
license: "unknown"
legacy_id:
- details/13006
imported:
- "2019"
_4images_image_id: "13006"
_4images_cat_id: "1179"
_4images_user_id: "672"
_4images_image_date: "2007-12-06T19:07:02"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13006 -->
Es darf auch mal was Kleines mit Elektrik sein... Als Basis dient die Batterie-Box mit 9 V Batterie/Akku, der Polwendeschalter schaltet entweder die grüne oder die rote Leuchte (oder gar nichts, in Mittenstellung).