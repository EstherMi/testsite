---
layout: "image"
title: "Bollerwagen"
date: "2007-12-07T18:24:22"
picture: "bollerwagen.jpg"
weight: "6"
konstrukteure: 
- "Stefan Schilling"
fotografen:
- "Stefan Schilling"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stefanft"
license: "unknown"
legacy_id:
- details/13015
imported:
- "2019"
_4images_image_id: "13015"
_4images_cat_id: "1179"
_4images_user_id: "672"
_4images_image_date: "2007-12-07T18:24:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13015 -->
