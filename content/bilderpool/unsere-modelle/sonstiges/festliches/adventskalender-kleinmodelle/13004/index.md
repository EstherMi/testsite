---
layout: "image"
title: "Trike mit Lenkung"
date: "2007-12-06T19:07:01"
picture: "adv1.jpg"
weight: "9"
konstrukteure: 
- "stefanft"
fotografen:
- "stefanft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stefanft"
license: "unknown"
legacy_id:
- details/13004
imported:
- "2019"
_4images_image_id: "13004"
_4images_cat_id: "1179"
_4images_user_id: "672"
_4images_image_date: "2007-12-06T19:07:01"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13004 -->
