---
layout: "overview"
title: "Adventskalender - Kleinmodelle für die Vorweihnachtszeit"
date: 2019-12-17T19:36:35+01:00
legacy_id:
- categories/1179
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1179 --> 
Die Idee zu einem ft-Adventskalender entstand im November 2007 in den fischertechnik-fanclub-foren. Zielsetzung: Aus ft viele Klein(st)modelle zu konstruieren, damit Kind jeden Tag etwas zu spielen hat. Je kleiner und einfacher, desto besser![br][br]Ich stelle mal ein paar Kleinmodelle ein, die mir dazu auf die Schnelle eingefallen sind. Wäre schön, wenn andere auch ein paar Ideen hätten und die Ergebnisse zeigen würden - damit der Adventskalender 2008 für jeden Tag etwas zu bieten hat ;-)[br][br]Viele Grüße, [br]Stefan