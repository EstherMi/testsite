---
layout: "image"
title: "Halterung des Eies von vorne"
date: "2013-03-24T21:51:36"
picture: "IMG_4649.jpg"
weight: "6"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lukas99h."
license: "unknown"
legacy_id:
- details/36814
imported:
- "2019"
_4images_image_id: "36814"
_4images_cat_id: "2730"
_4images_user_id: "1631"
_4images_image_date: "2013-03-24T21:51:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36814 -->
Hier sieht man nun die halterung wo das Ei eingespannt wird.....Die geleben schaumstoff kreise dienen dazu das man das Ei richtig fest einspannen kann ohne das es zerbricht...