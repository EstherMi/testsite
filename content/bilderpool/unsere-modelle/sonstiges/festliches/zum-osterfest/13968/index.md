---
layout: "image"
title: "Eierausblasvorrichtung4"
date: "2008-03-20T10:00:57"
picture: "Bild4.jpg"
weight: "10"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Andreas Tacke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/13968
imported:
- "2019"
_4images_image_id: "13968"
_4images_cat_id: "1508"
_4images_user_id: "182"
_4images_image_date: "2008-03-20T10:00:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13968 -->
Ansicht von oben