---
layout: "comment"
hidden: true
title: "6387"
date: "2008-05-03T11:06:06"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
@ft_idaho: Wow! Now that's great Idea! Congratulations!

@Johannes: "would" heißt "würde".

Handgemachte Übersetzung von ft_idahos Erklärung: Das Roboter-Osterei summt (mit Pausen darin, wenn ich das richtig verstehe), damit sehbehinderte Schüler/Studenten es finden können. Sobald sie näher kommen, wird das Summen schneller (die Pausen kleiner). Durch das Drücken einer der beiden Taster wird der Summer abgeschaltet.

Gruß,
Stefan