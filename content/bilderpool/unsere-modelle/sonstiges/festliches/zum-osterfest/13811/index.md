---
layout: "image"
title: "Eiermalvorrichtung"
date: "2008-03-01T17:35:32"
picture: "Bild_Upload.jpg"
weight: "12"
konstrukteure: 
- "FamilieTacke"
fotografen:
- "Andreas Tacke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/13811
imported:
- "2019"
_4images_image_id: "13811"
_4images_cat_id: "1508"
_4images_user_id: "182"
_4images_image_date: "2008-03-01T17:35:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13811 -->
Es ist ja bald Ostern. Hier eine Vorrichtung zum Anmalen von Ostereiern. Eine recht einfache aber wirkungsvolle Vorrichtung. Zum Trocknen wird dann ein Minnimot mit Propeller benutzt. Meine Kinder haben einen riesen Spaß damit :))