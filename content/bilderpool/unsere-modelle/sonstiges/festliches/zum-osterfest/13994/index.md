---
layout: "image"
title: "Easter Rabbit -2"
date: "2008-03-22T07:45:09"
picture: "ft-rabbit_f.jpg"
weight: "5"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Easter", "Rabbit", "egg", "basket"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/13994
imported:
- "2019"
_4images_image_id: "13994"
_4images_cat_id: "1508"
_4images_user_id: "585"
_4images_image_date: "2008-03-22T07:45:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13994 -->
The Easter Rabbit with egg basket!

(google translation: Die Oster-Ei-Hase mit Korb!)