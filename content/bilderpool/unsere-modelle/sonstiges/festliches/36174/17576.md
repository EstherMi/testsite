---
layout: "comment"
hidden: true
title: "17576"
date: "2012-11-24T23:14:45"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Ok, ich muss gestehen, dass mir die Modellidee heute morgen beim Radiohören (SWR3) kam: "Frage: Woran erkennt man einen schwäbischen Adventskranz? Antwort: Eine Kerze, drei Spiegel." Daraufhin meinte Magnus ganz trocken: Die Kerze kann man auch noch sparen...
Gruß, Dirk