---
layout: "image"
title: "Snowman"
date: "2010-12-01T22:17:00"
picture: "snowman2.jpg"
weight: "46"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Snowman", "Advent", "Calendar"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/29390
imported:
- "2019"
_4images_image_id: "29390"
_4images_cat_id: "1180"
_4images_user_id: "585"
_4images_image_date: "2010-12-01T22:17:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29390 -->
This is the first model in the ft advent calendar series. I am going to post these on instructables.