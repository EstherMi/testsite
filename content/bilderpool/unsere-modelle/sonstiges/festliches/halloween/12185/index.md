---
layout: "image"
title: "ft-Zombie"
date: "2007-10-12T18:18:33"
picture: "ft-zombie.jpg"
weight: "20"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Halloween", "Zombie"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/12185
imported:
- "2019"
_4images_image_id: "12185"
_4images_cat_id: "1100"
_4images_user_id: "585"
_4images_image_date: "2007-10-12T18:18:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12185 -->
A ft Zombie for Halloween! (A ft Zombie für Halloween!)