---
layout: "image"
title: "ft Skeleton (Halloween Theme)"
date: "2007-10-18T08:57:05"
picture: "ft-skeleton2b.jpg"
weight: "19"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["skeleton", "Halloween"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/12265
imported:
- "2019"
_4images_image_id: "12265"
_4images_cat_id: "1100"
_4images_user_id: "585"
_4images_image_date: "2007-10-18T08:57:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12265 -->
A skeleton for Halloween! (Ein Skelett für Halloween! - google translation)