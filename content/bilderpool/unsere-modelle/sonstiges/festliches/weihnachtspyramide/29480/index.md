---
layout: "image"
title: "Weihnachtspyramide 5"
date: "2010-12-18T14:56:50"
picture: "Weihnachtspyramide_5.jpg"
weight: "8"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/29480
imported:
- "2019"
_4images_image_id: "29480"
_4images_cat_id: "2145"
_4images_user_id: "328"
_4images_image_date: "2010-12-18T14:56:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29480 -->
Hier habe ich einen Träger demontiert, um das drehbare Innenleben zu zeigen. Auf dieser Seite der Drehscheibe steht ein Rentier.