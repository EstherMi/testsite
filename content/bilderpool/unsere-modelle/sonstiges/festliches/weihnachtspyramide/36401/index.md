---
layout: "image"
title: "Weihnachtspyramide anderer Antrieb 3"
date: "2013-01-04T12:50:57"
picture: "weihnachtspyramideandererantrieb3.jpg"
weight: "3"
konstrukteure: 
- "thomas004, Jan Werner"
fotografen:
- "Jan Werner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "werner"
license: "unknown"
legacy_id:
- details/36401
imported:
- "2019"
_4images_image_id: "36401"
_4images_cat_id: "2145"
_4images_user_id: "1196"
_4images_image_date: "2013-01-04T12:50:57"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36401 -->
Dieser Antrieb für die Weihnachtspyramide von thomas004 läuft etwas langsamer als der Antrieb durch zwei Zahnräder in einer - wie ich finde - realistischeren Geschwindigkeit