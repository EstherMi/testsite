---
layout: "overview"
title: "FT Tannenbaum mit Kerzen zum auspusten"
date: 2019-12-17T19:36:47+01:00
legacy_id:
- categories/2822
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2822 --> 
Am FT-Tannenbaum brennen 6 Kerzen. Schaffst du es, sie alle aus zu pusten???
Wenn ja, dann erwartet dich eine Überraschung.
