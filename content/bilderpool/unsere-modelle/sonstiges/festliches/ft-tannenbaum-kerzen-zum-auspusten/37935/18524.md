---
layout: "comment"
hidden: true
title: "18524"
date: "2013-12-24T16:00:03"
uploadBy:
- "Phil"
license: "unknown"
imported:
- "2019"
---
Hallo,

könnte man nicht anstatt der Papp-Flamme eine LED als Kerzenflamme verwenden, die dann beim Pusten ausgeht?

Grüße,
Phil