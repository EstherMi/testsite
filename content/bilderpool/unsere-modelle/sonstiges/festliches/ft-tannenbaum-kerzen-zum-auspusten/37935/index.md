---
layout: "image"
title: "PappKerze-PusteSensor von Vorne"
date: "2013-12-21T16:45:42"
picture: "fttannenbaummitkerzenzumauspusten4.jpg"
weight: "4"
konstrukteure: 
- "DasKasperle"
fotografen:
- "DasKasperle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- details/37935
imported:
- "2019"
_4images_image_id: "37935"
_4images_cat_id: "2822"
_4images_user_id: "1677"
_4images_image_date: "2013-12-21T16:45:42"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37935 -->
Der "PappKerze-PusteSensor" besteht aus bunter Pappe, 3 Fahrwerksfeder, Flachstein 30, V-Riegel, Kabel und Alufolie.
Am Flachstein haben wir eine Aluflolienkugel befestigt, diese wurde zusammen mit den  Fahrwerksfedern mit Alufolie umwickelt und an einem Kabelende befestigt.