---
layout: "image"
title: "Roboter 4"
date: "2008-10-11T22:48:04"
picture: "OLD11.jpg"
weight: "11"
konstrukteure: 
- "HLGR"
fotografen:
- "HLGR"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "HLGR"
license: "unknown"
legacy_id:
- details/15953
imported:
- "2019"
_4images_image_id: "15953"
_4images_cat_id: "1448"
_4images_user_id: "832"
_4images_image_date: "2008-10-11T22:48:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15953 -->
Roboter mit Lochstreifensteuerung
Alter ca. 30 Jahre