---
layout: "comment"
hidden: true
title: "7540"
date: "2008-10-12T06:18:36"
uploadBy:
- "HLGR"
license: "unknown"
imported:
- "2019"
---
Bei dieser Variante liegt die Trommel mit den Lampen schräg, so dass die Lichter in einem Bogen über den Bildschirm aus Pergamentpapier liefen.
Auch hier mußte man einen Fotowiderstand so nach links und rechts steuern, dass er nicht getroffen wurde.