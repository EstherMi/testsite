---
layout: "image"
title: "Schule"
date: "2009-04-10T19:57:04"
picture: "bild2.jpg"
weight: "2"
konstrukteure: 
- "Lara"
fotografen:
- "Sven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/23661
imported:
- "2019"
_4images_image_id: "23661"
_4images_cat_id: "1615"
_4images_user_id: "1"
_4images_image_date: "2009-04-10T19:57:04"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23661 -->
Hier eine kleine Schule