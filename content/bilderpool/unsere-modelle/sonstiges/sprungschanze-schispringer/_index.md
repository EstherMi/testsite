---
layout: "overview"
title: "Sprungschanze für Schispringer"
date: 2019-12-17T19:40:28+01:00
legacy_id:
- categories/3480
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3480 --> 
Um meine Lego und Playmobil spielenden Kinder auch für FischerTechnik zu begeistern habe ich ihnen vor ein paar Jahren eine Schisprungschanze aus FischerTechnik gebaut. Insbesondere die Playmobil-Schifahrer springen auf dieser Schanze sehr gut und wir haben richtige Wettbewerbe veranstaltet. Ich habe dazu ein paar Fotos begelegt.