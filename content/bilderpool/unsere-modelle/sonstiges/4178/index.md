---
layout: "image"
title: "Notebook-Ständer"
date: "2005-05-23T19:23:18"
picture: "Laptop-Stnder.jpg"
weight: "27"
konstrukteure: 
- "user"
fotografen:
- "user"
keywords: ["Laptop", "Notebook"]
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/4178
imported:
- "2019"
_4images_image_id: "4178"
_4images_cat_id: "323"
_4images_user_id: "104"
_4images_image_date: "2005-05-23T19:23:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4178 -->
Dieser hier wurde für einen Fujitu-Siemens Amilo gebaut und leistet mir täglich seit einem guten Jahr unentbehrliche Dienste. Für den Transportfall ist er zusammenklappbar (das kommt dann zum Tragen, wenn ich nach ebay- und Neukäufen meine gesamte Sammlung mal wieder einräumen und fotografieren will). Rückfrage beim Hersteller ergab übrigens, dass es heutigen Festplatten nichts mehr ausmacht, schräg betrieben zu werden.