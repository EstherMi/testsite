---
layout: "overview"
title: "Beschleunigung auf einer schiefen Ebene"
date: 2019-12-17T19:40:04+01:00
legacy_id:
- categories/3181
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3181 --> 
Anlässlich des Themas "Beschleunigung einer Kugel auf einer schiefen Ebene" http://forum.ftcommunity.de/viewtopic.php?f=8&t=3333 habe ich einen Versuchsaufbau erstellt, mit dem man die Beschleunigung einer Kugel und deren Endgeschwindigkeit berechnen kann. Der Versuchsaufbau nutzt die Bewegungsgleichungen, um die Endgeschwindigkeit zu berechnen, ohne sehr kleine Zeiten messen zu müssen.