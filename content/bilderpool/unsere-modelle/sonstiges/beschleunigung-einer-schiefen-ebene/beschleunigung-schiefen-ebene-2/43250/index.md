---
layout: "image"
title: "Verdunklungszeit"
date: "2016-04-04T21:54:43"
picture: "badsema5.jpg"
weight: "5"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/43250
imported:
- "2019"
_4images_image_id: "43250"
_4images_cat_id: "3212"
_4images_user_id: "2228"
_4images_image_date: "2016-04-04T21:54:43"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43250 -->
Die Momentangeschwindigkeit wird annäherungsweise bestimmt, indem die Verdunklungszeit der Kugel gemessen wird. Die Geschwindigkeit ergibt sich aus Weg pro Zeit.