---
layout: "image"
title: "Mechanik"
date: "2016-04-04T21:54:43"
picture: "badsema3.jpg"
weight: "3"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/43248
imported:
- "2019"
_4images_image_id: "43248"
_4images_cat_id: "3212"
_4images_user_id: "2228"
_4images_image_date: "2016-04-04T21:54:43"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43248 -->
So wird die Neigung eingestellt: Ein Powermotor treibt über eine Schnecke den Drehkranz an. Als Referenzpunkt (0° Rampenneigung) dient der Taster links. Der Taster rechts zählt die Impulse des Impulszahnrades. Damit kann die Rampe mittels Soll-Ist Abgleich geneigt werden. Die Genauigkeit beträgt etwa 1°.