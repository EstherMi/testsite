---
layout: "image"
title: "Vergleich Schienen"
date: "2016-04-04T21:54:43"
picture: "badsema6.jpg"
weight: "6"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/43251
imported:
- "2019"
_4images_image_id: "43251"
_4images_cat_id: "3212"
_4images_user_id: "2228"
_4images_image_date: "2016-04-04T21:54:43"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43251 -->
Oben: Schiene aus zwei parallel verlaufenden Edelstahlschienen
Unten: Fischertechnik Flexschienen
