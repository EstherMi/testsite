---
layout: "image"
title: "TX Display"
date: "2016-01-13T14:57:44"
picture: "vbse2.jpg"
weight: "2"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/42722
imported:
- "2019"
_4images_image_id: "42722"
_4images_cat_id: "3181"
_4images_user_id: "2228"
_4images_image_date: "2016-01-13T14:57:44"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42722 -->
hier wird die Beschleunigung der Kugel sowie deren Endgeschwindigkeit angezeigt