---
layout: "image"
title: "in Aktion - Nahaufnahme"
date: "2007-01-28T17:07:10"
picture: "in_Aktion_-_Detail_1.jpg"
weight: "2"
konstrukteure: 
- "jko"
fotografen:
- "jko"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jko"
license: "unknown"
legacy_id:
- details/8715
imported:
- "2019"
_4images_image_id: "8715"
_4images_cat_id: "798"
_4images_user_id: "540"
_4images_image_date: "2007-01-28T17:07:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8715 -->
