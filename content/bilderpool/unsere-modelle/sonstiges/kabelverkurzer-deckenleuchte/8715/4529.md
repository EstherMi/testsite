---
layout: "comment"
hidden: true
title: "4529"
date: "2007-11-08T22:51:14"
uploadBy:
- "jko"
license: "unknown"
imported:
- "2019"
---
Zur Beruhigung aller, die sich Sorgen um die armen Steine machen: das Teil ist nicht deshalb so krumm, weil es extrem belastet wird, sondern weil ich es absichtlich krumm gebaut habe - mit Winkelsteinen 7,5° über, zwischen und unter den roten Bausteinen 15. Bei ganz genauem Hinsehen kann man das auf dem Bild oben erkennen.

Ich bin doch kein Steinequäler!