---
layout: "image"
title: "in Aktion"
date: "2007-01-28T17:07:10"
picture: "in_Aktion.jpg"
weight: "1"
konstrukteure: 
- "jko"
fotografen:
- "jko"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jko"
license: "unknown"
legacy_id:
- details/8714
imported:
- "2019"
_4images_image_id: "8714"
_4images_cat_id: "798"
_4images_user_id: "540"
_4images_image_date: "2007-01-28T17:07:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8714 -->
Dank dieser Konstruktion war unsere schöne Hängelampe nicht unmittelbar den Dünsten des Silvester-Fondues ausgesetzt.
Das ft-Rot harmonierte auch hervorragend mit den Resten der Weihnachtsdekoration.