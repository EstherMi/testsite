---
layout: "image"
title: "Alziend Oog :   -Op afstand bestuurd draai- en kantelbaar."
date: "2014-11-16T15:34:20"
picture: "alziendoog04.jpg"
weight: "4"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/39829
imported:
- "2019"
_4images_image_id: "39829"
_4images_cat_id: "2988"
_4images_user_id: "22"
_4images_image_date: "2014-11-16T15:34:20"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39829 -->
De OpenBeam Corner Cubes 15x15x15mm zijn zeer handig voor diverse zwaardere constructie toepassingen.

Alziend Oog weegt totaal 7,8 kg

http://www.makerbeam.eu/epages/63128753.sf/en_GB/?ObjectPath=/Shops/63128753/Products/OB-CC-151515C-P12

OpenBeam Projects and FischerTechnik : 
http://www.makerbeam.eu/epages/63128753.sf/en_GB/?ObjectPath=/Shops/63128753/Categories/OpenBeam_Projects