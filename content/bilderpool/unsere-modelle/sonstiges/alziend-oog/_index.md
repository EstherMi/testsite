---
layout: "overview"
title: "Alziend Oog"
date: 2019-12-17T19:38:16+01:00
legacy_id:
- categories/2988
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2988 --> 
Alziend Oog :   -Op afstand bestuurd draai- en kantelbaar.
Ophanging gemaakt uit combinatie van Fischertechnik + 15x15mm Open-Beams.