---
layout: "image"
title: "Alziend Oog :   -Op afstand bestuurd draai- en kantelbaar."
date: "2014-11-16T15:34:20"
picture: "alziendoog02.jpg"
weight: "2"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/39827
imported:
- "2019"
_4images_image_id: "39827"
_4images_cat_id: "2988"
_4images_user_id: "22"
_4images_image_date: "2014-11-16T15:34:20"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39827 -->
Beneden-positie ver weg  kijkend..........