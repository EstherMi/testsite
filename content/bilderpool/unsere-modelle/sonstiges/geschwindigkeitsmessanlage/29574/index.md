---
layout: "image"
title: "Geschwindigkeitsmessanlage- Gesamtansicht"
date: "2011-01-01T17:32:47"
picture: "sdfsfs1.jpg"
weight: "1"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/29574
imported:
- "2019"
_4images_image_id: "29574"
_4images_cat_id: "2154"
_4images_user_id: "1162"
_4images_image_date: "2011-01-01T17:32:47"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29574 -->
So, hier nun mein Modell und zwar ist es eine Geschwindigkeitsmessanlage. Sie misst die Geschwindikeit meiner Märklin Züge. Sie besteht aus zwei Lichtschranken, einer Extension und einem Taster.

Ein Video von der Anlage gibt es hier: http://www.youtube.com/watch?v=hTprAwfvemI