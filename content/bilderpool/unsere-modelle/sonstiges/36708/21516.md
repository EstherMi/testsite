---
layout: "comment"
hidden: true
title: "21516"
date: "2016-01-06T17:32:50"
uploadBy:
- "Getriebesand"
license: "unknown"
imported:
- "2019"
---
Hier ist die neue Version, zumindest die Hardware ist nahezu fertig: https://drive.google.com/folder/d/0B5wpVMcAlelmVUFONjF3bHRsSG8/edit

Leider musste ich das Modell vor einiger Zeit abbauen, ich werde es aber irgendwann weiterentwickeln.

Grüße Getriebesand