---
layout: "comment"
hidden: true
title: "21365"
date: "2015-11-30T21:39:03"
uploadBy:
- "JaSpiel"
license: "unknown"
imported:
- "2019"
---
Hallo,

- Die richtige Info ist Binarzahler 2.3
- Die Spass fur Kinder ist ausprobieren wenn genau die richtige Birnen brennen
- Es ist ein Spiel zu tun.
- 1 taster Taktsteuerung, 1 taster Reset
- Das ganze Programm, 16 klicks:

1. birne 1
2. birne 2
3. birne 1 und 2
4. birne 3
5. birne 1 und 3
6. birne 3 und 4
7. birne 1, 2, und 3
8. birne 4
9. birne 1 und 4
10. birne 2 und 4
11. birne 1, 2 und 4
12. birne 3 und 4
13. birne 1, 3 und 4
14. birne 2, 3 und 4
15. birne 1, 2, 3 und 4
16. kein birne brennt

Gruss, Jack