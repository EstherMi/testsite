---
layout: "image"
title: "LCD-Box mit LCD2004 (von vorne)"
date: "2014-01-06T08:31:03"
picture: "ftboxfuervierzeiligeslcdisplay5.jpg"
weight: "5"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/38022
imported:
- "2019"
_4images_image_id: "38022"
_4images_cat_id: "2828"
_4images_user_id: "1126"
_4images_image_date: "2014-01-06T08:31:03"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38022 -->
Frontansicht der Box inklusive Display.