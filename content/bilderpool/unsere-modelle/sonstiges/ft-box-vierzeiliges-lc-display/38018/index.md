---
layout: "image"
title: "LC-Display LCD2004"
date: "2014-01-06T08:31:03"
picture: "ftboxfuervierzeiligeslcdisplay1.jpg"
weight: "1"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/38018
imported:
- "2019"
_4images_image_id: "38018"
_4images_cat_id: "2828"
_4images_user_id: "1126"
_4images_image_date: "2014-01-06T08:31:03"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38018 -->
I²C-LC-Display LCD2004 (4 x 20 Zeichen) zum direkten Betrieb am TX Controller (mehr in ft:pedia 1/2014). Der zugehörige Robo Pro-Treiber findet sich im Download-Bereich der ft:c.