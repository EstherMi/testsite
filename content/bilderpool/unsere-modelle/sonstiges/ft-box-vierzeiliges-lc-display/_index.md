---
layout: "overview"
title: "ft-Box für vierzeiliges LC-Display"
date: 2019-12-17T19:38:11+01:00
legacy_id:
- categories/2828
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2828 --> 
ft-fremde Í²C-Sensoren und -Aktoren lassen sich meist nicht ohne Weiteres stabil und dezent in ft-Modellen verbauen.
Für das vierzeilige LC-Display LCD2004 lässt sich jedoch eine Box in den Maßen der Grundplatte 120 x 60 konstruieren.