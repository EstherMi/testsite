---
layout: "image"
title: "LCD-Box (leer, von schräg oben)"
date: "2014-01-06T08:31:03"
picture: "ftboxfuervierzeiligeslcdisplay2.jpg"
weight: "2"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/38019
imported:
- "2019"
_4images_image_id: "38019"
_4images_cat_id: "2828"
_4images_user_id: "1126"
_4images_image_date: "2014-01-06T08:31:03"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38019 -->
Box zum Verbauen des Displays in fischertechnik-Modellen im Format der Grundplatte 120 x 60 (ohne Deckel) .