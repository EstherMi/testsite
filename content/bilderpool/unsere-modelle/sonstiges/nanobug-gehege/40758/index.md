---
layout: "image"
title: "Krabbelalarm!!!!"
date: "2015-04-12T13:29:03"
picture: "kak1.jpg"
weight: "1"
konstrukteure: 
- "Jens Hannes & Jonas"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/40758
imported:
- "2019"
_4images_image_id: "40758"
_4images_cat_id: "3062"
_4images_user_id: "1359"
_4images_image_date: "2015-04-12T13:29:03"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40758 -->
die Kakerlaken sind los . . .   
Bekannt aus dem Kinderspiel "Kakerlakak" vom Hersteller Ravensburger  sind diese kleinen Krabbelroboter fast schon als inteligent-anmutend einzustufen ;-)
die (bei uns) orange Variante ist ein sog. "Nanobug v2" - danach kann man mal googeln - die Viecher können auch Steigungen und unter gewissen Vorausetzungen  auch in senkrechten Röhren hochklettern. Finden kann man die lieben Kleinen auch als "HexBug" - das ist der Markenname des Herstellers..
die "V1" - in unserem Fall grün, kann kaum Steigungen überwinden, und auch nicht senkrecht hoch bzw. auf Kopf krabbeln.
Das jetzt gebaute Habitat ist ein erster Versuch auf die Schnelle mit reichlich Ideen der beiden Jungs. Die TAge werde ich mal systematisch herausarbeiten, wie man die Viecher in die Senkrechte leiten kann, und was sonst noch so geht, ich denke da an eine Art Rückschlagventil für die Krabbler, evtl auch Aufzüge, Wippen, Lichtschranken-bewehrte Durchgänge, Tunnel .. usw.