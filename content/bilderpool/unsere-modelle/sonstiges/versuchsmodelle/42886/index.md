---
layout: "image"
title: "U-Träger-Brückentragwerk 3"
date: "2016-02-15T22:41:37"
picture: "IMG_3292.jpg"
weight: "10"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- details/42886
imported:
- "2019"
_4images_image_id: "42886"
_4images_cat_id: "3189"
_4images_user_id: "740"
_4images_image_date: "2016-02-15T22:41:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42886 -->
Wie stabil die Gesamtkonstruktion im "Brückenbetrieb" ist zeigt diese Ansicht.

Die 2010 mm lange, freistehende, Brücken-Konstruktion biegt sich mittig nur einige wenige Millimeter durch.