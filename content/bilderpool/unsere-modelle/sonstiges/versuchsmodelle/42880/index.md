---
layout: "image"
title: "U-Träger-Tragwerk 4"
date: "2016-02-15T22:41:37"
picture: "IMG_3273.jpg"
weight: "4"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- details/42880
imported:
- "2019"
_4images_image_id: "42880"
_4images_cat_id: "3189"
_4images_user_id: "740"
_4images_image_date: "2016-02-15T22:41:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42880 -->
Ansicht eines inneren Knotenpunkts.