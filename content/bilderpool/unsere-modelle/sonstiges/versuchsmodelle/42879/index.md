---
layout: "image"
title: "U-Träger-Tragwerk 3"
date: "2016-02-15T22:41:37"
picture: "IMG_3266.jpg"
weight: "3"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- details/42879
imported:
- "2019"
_4images_image_id: "42879"
_4images_cat_id: "3189"
_4images_user_id: "740"
_4images_image_date: "2016-02-15T22:41:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42879 -->
Ansicht einer kompletten Ebene samt eingearbeiteter Diagonale.

Die Länge der zu konstruierenden Diagonale konnte mittels Pythagoras über das Innenmaß des senkrechten und waagerechten Hauptträgers (jeweils 2 x U 150 = 300 mm) ermittelt werden. Sie beträgt rechnerisch somit 424,26 mm.
Die Länge der jeweiligen Winkel-Eck-Stein-Konstruktion (eine oben rechts und eine unten links) ergibt sich ebenso aus Rechnung des  Pythagoras. Sie beträgt jeweils 25,98 mm.
Somit verbleibt dabei eine Restlänge von 372,3 mm, also rund 372,5 mm. Dieses wird erreicht durch 2 x U 150 (300 mm) + 1 x WT 60 (60 mm) + 1 x BS 7,5 (7,5 mm) oben + 1 x BS 5 (5 mm) unten. Alles klar? :)