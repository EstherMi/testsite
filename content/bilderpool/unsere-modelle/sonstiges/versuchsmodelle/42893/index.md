---
layout: "image"
title: "Träger/Kabelkanal 3"
date: "2016-02-15T22:41:37"
picture: "IMG_3329.jpg"
weight: "17"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- details/42893
imported:
- "2019"
_4images_image_id: "42893"
_4images_cat_id: "3189"
_4images_user_id: "740"
_4images_image_date: "2016-02-15T22:41:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42893 -->
Kabelführung vom Kanal zum Abzweigkasten (Batteriegehäuse).