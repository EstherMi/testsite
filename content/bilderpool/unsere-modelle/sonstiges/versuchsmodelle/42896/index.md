---
layout: "image"
title: "Großes Rad"
date: "2016-02-15T22:41:37"
picture: "IMG_3347.jpg"
weight: "20"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- details/42896
imported:
- "2019"
_4images_image_id: "42896"
_4images_cat_id: "3189"
_4images_user_id: "740"
_4images_image_date: "2016-02-15T22:41:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42896 -->
Ziel dieses Versuchsaufbaus bzw. dieser Konstruktion ist die Schaffung eines großen Rades mittels vorhandener originaler ft-Teile.

Der Innendurchmesser von 49 Winkelsteinen 7,5° entspricht dem Außendurchmesser einer Drehscheibe. Als Haftreifen dient das ältere Raupenband 31057.