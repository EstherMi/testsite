---
layout: "image"
title: "7"
date: "2010-07-09T08:53:32"
picture: "solarventilator07.jpg"
weight: "7"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27730
imported:
- "2019"
_4images_image_id: "27730"
_4images_cat_id: "1997"
_4images_user_id: "1082"
_4images_image_date: "2010-07-09T08:53:32"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27730 -->
So wird er an der Kopfstütze befestigt.