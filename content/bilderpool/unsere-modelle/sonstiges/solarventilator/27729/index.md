---
layout: "image"
title: "6"
date: "2010-07-09T08:53:32"
picture: "solarventilator06.jpg"
weight: "6"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27729
imported:
- "2019"
_4images_image_id: "27729"
_4images_cat_id: "1997"
_4images_user_id: "1082"
_4images_image_date: "2010-07-09T08:53:32"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27729 -->
Hier einmal im Stillstand. Ich habe zwei vierflügelige Luftschrauben übereinander auf den Adapter gesteckt.