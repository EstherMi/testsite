---
layout: "image"
title: "1"
date: "2010-07-09T08:53:31"
picture: "solarventilator01.jpg"
weight: "1"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27724
imported:
- "2019"
_4images_image_id: "27724"
_4images_cat_id: "1997"
_4images_user_id: "1082"
_4images_image_date: "2010-07-09T08:53:31"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27724 -->
Die Solarzellen hinter der Windschutzscheibe.