---
layout: "image"
title: "11"
date: "2010-07-09T08:53:48"
picture: "solarventilator11.jpg"
weight: "11"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27734
imported:
- "2019"
_4images_image_id: "27734"
_4images_cat_id: "1997"
_4images_user_id: "1082"
_4images_image_date: "2010-07-09T08:53:48"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27734 -->
Dann habe ich das Messingstück soweit es ging in den Luftschraubenadapter gesteckt. In dieser Position ist er etwas festgeklemmt.