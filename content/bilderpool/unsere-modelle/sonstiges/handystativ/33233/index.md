---
layout: "image"
title: "Stativ2"
date: "2011-10-19T14:25:45"
picture: "Stativ2.jpg"
weight: "2"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: ["Kamerahalterung"]
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- details/33233
imported:
- "2019"
_4images_image_id: "33233"
_4images_cat_id: "2459"
_4images_user_id: "1322"
_4images_image_date: "2011-10-19T14:25:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33233 -->
Hier sieht man die bei Bild1 erwähnte Schnecke zur (Handy-)Kameraneigung im Detail.