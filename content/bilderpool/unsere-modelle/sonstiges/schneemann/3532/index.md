---
layout: "image"
title: "Schneemann"
date: "2005-01-19T13:58:17"
picture: "Schneemann2.jpg"
weight: "2"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/3532
imported:
- "2019"
_4images_image_id: "3532"
_4images_cat_id: "536"
_4images_user_id: "130"
_4images_image_date: "2005-01-19T13:58:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3532 -->
Der Schneemann aus einer etwas anderen Ansicht.