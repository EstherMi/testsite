---
layout: "image"
title: "Schneemann"
date: "2005-01-19T13:58:17"
picture: "Schneemann.jpg"
weight: "1"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/3531
imported:
- "2019"
_4images_image_id: "3531"
_4images_cat_id: "536"
_4images_user_id: "130"
_4images_image_date: "2005-01-19T13:58:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3531 -->
Mein Schneemann, den ich extra für meine Frau gebastelt habe. Könnte aber etwas stabiler sein. Musste halt schnell gehen.