---
layout: "image"
title: "Überschlagauto"
date: "2003-08-04T12:38:50"
picture: "ft_Auto.jpg"
weight: "30"
konstrukteure: 
- "stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/1320
imported:
- "2019"
_4images_image_id: "1320"
_4images_cat_id: "323"
_4images_user_id: "130"
_4images_image_date: "2003-08-04T12:38:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1320 -->
Mein altes Überschlagauto. Krabbelte mühelos überall rauf und wenn es doch mal zu steil war und er umkippte fuhr er einfach rückwärts wieder weg. Stammt noch aus meinen Anfangszeiten mit ft. Daherr auch ohne Lenkung.