---
layout: "image"
title: "Getriebe Spritzenpresse"
date: "2017-09-30T11:52:18"
picture: "keksdrucker05.jpg"
weight: "5"
konstrukteure: 
- "Fabian, Max, Christian & Stefan"
fotografen:
- "Christian & Stefan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ftpi"
license: "unknown"
legacy_id:
- details/46535
imported:
- "2019"
_4images_image_id: "46535"
_4images_cat_id: "3438"
_4images_user_id: "2611"
_4images_image_date: "2017-09-30T11:52:18"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46535 -->
Getriebe um die beiden Spindeln gleichmäßig zu verfahren. Durch die Untersetzung der Spindel reicht ein S-Motor für den Antrieb aus.