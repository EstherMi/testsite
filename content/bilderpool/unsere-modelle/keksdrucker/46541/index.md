---
layout: "image"
title: "Verfahreinheit"
date: "2017-09-30T11:52:18"
picture: "keksdrucker11.jpg"
weight: "11"
konstrukteure: 
- "Fabian, Max, Christian & Stefan"
fotografen:
- "Christian & Stefan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ftpi"
license: "unknown"
legacy_id:
- details/46541
imported:
- "2019"
_4images_image_id: "46541"
_4images_cat_id: "3438"
_4images_user_id: "2611"
_4images_image_date: "2017-09-30T11:52:18"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46541 -->
Die Laufkatze zwischen den einzelnen Reihen des Trockenlagers läuft auf Laufschienen die oben und unten an dem Lager angebaut sind. Ein Mini-Motor mit U-Getriebe treibt die Laufkatze an einer Zahnstange an.