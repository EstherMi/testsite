---
layout: "image"
title: "Druckkopf"
date: "2017-09-30T11:52:18"
picture: "keksdrucker03.jpg"
weight: "3"
konstrukteure: 
- "Fabian, Max, Christian & Stefan"
fotografen:
- "Christian & Stefan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ftpi"
license: "unknown"
legacy_id:
- details/46533
imported:
- "2019"
_4images_image_id: "46533"
_4images_cat_id: "3438"
_4images_user_id: "2611"
_4images_image_date: "2017-09-30T11:52:18"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46533 -->
Der Druckkopf besteht aus drei Reedkontakthaltern / Kabelführungen in die 4 mm Plastikschläuche aus dem Baumarkt gesteckt werden. Kurze Messingröhrchen und etwas Tesa-Film um die Schläuche sorgen für den perfekten Sitz.