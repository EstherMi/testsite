---
layout: "image"
title: "Spritzenpresse"
date: "2017-09-30T11:52:18"
picture: "keksdrucker04.jpg"
weight: "4"
konstrukteure: 
- "Fabian, Max, Christian & Stefan"
fotografen:
- "Christian & Stefan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ftpi"
license: "unknown"
legacy_id:
- details/46534
imported:
- "2019"
_4images_image_id: "46534"
_4images_cat_id: "3438"
_4images_user_id: "2611"
_4images_image_date: "2017-09-30T11:52:18"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46534 -->
Um die Farbe auf den Keks zu spritzen, werden die Kolben der Injektionsspritzen mit einem Spundelantrieb in die Spritze gedrückt. Da der Zuckerguß recht zäh ist, muss die Spindel doppelt ausgeführt werden.