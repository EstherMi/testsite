---
layout: "image"
title: "Druckwerk"
date: "2017-09-30T11:52:18"
picture: "keksdrucker02.jpg"
weight: "2"
konstrukteure: 
- "Fabian, Max, Christian & Stefan"
fotografen:
- "Christian & Stefan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ftpi"
license: "unknown"
legacy_id:
- details/46532
imported:
- "2019"
_4images_image_id: "46532"
_4images_cat_id: "3438"
_4images_user_id: "2611"
_4images_image_date: "2017-09-30T11:52:18"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46532 -->
Das Druckwerk besteht aus zwei Encodermotoren, die mit drei Spindeln den Druckkopf frei in x- und y-Richtung verfahren können. Die untere Ebene ist mit zwei Spindeln ausgeführt, die über eine Steuerkette verbunden sind.