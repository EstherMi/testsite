---
layout: "image"
title: "8"
date: "2009-01-17T13:23:39"
picture: "rhvonterex07.jpg"
weight: "6"
konstrukteure: 
- "lil-mike"
fotografen:
- "lil-mike"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lil-mike"
license: "unknown"
legacy_id:
- details/17024
imported:
- "2019"
_4images_image_id: "17024"
_4images_cat_id: "1530"
_4images_user_id: "822"
_4images_image_date: "2009-01-17T13:23:39"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17024 -->
