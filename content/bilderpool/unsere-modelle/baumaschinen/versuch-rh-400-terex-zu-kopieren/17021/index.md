---
layout: "image"
title: "4"
date: "2009-01-17T13:23:39"
picture: "rhvonterex04.jpg"
weight: "3"
konstrukteure: 
- "lil-mike"
fotografen:
- "lil-mike"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lil-mike"
license: "unknown"
legacy_id:
- details/17021
imported:
- "2019"
_4images_image_id: "17021"
_4images_cat_id: "1530"
_4images_user_id: "822"
_4images_image_date: "2009-01-17T13:23:39"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17021 -->
