---
layout: "image"
title: "Rij en stuur motoren"
date: "2013-02-21T18:56:48"
picture: "P2180004.jpg"
weight: "2"
konstrukteure: 
- "Ruurd"
fotografen:
- "Chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/36656
imported:
- "2019"
_4images_image_id: "36656"
_4images_cat_id: "2718"
_4images_user_id: "838"
_4images_image_date: "2013-02-21T18:56:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36656 -->
De beide rij en stuur motoren. In totaal 4 diff's in het model (nu nog maar 3 had tandwielen te weinig maar het diff kan er zo weer in)