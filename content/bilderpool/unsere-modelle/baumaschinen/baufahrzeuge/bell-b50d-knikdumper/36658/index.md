---
layout: "image"
title: "Voorste aandrijving"
date: "2013-02-21T18:56:48"
picture: "P2180006.jpg"
weight: "4"
konstrukteure: 
- "Ruurd"
fotografen:
- "Chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/36658
imported:
- "2019"
_4images_image_id: "36658"
_4images_cat_id: "2718"
_4images_user_id: "838"
_4images_image_date: "2013-02-21T18:56:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36658 -->
Aandrijving naar de vooras (ligt een oud type diff onder, die met die stalen assen)