---
layout: "image"
title: "Bell_B30D_28.JPG"
date: "2007-03-30T17:20:25"
picture: "Bell_B30D_28.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/9855
imported:
- "2019"
_4images_image_id: "9855"
_4images_cat_id: "860"
_4images_user_id: "4"
_4images_image_date: "2007-03-30T17:20:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9855 -->
