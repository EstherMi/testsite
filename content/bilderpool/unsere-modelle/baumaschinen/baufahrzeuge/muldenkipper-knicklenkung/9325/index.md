---
layout: "image"
title: "Bell-B30D-55.JPG"
date: "2007-03-06T19:47:35"
picture: "Bell-B30D-55.JPG"
weight: "7"
konstrukteure: 
- "Fa. Bell"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/9325
imported:
- "2019"
_4images_image_id: "9325"
_4images_cat_id: "860"
_4images_user_id: "4"
_4images_image_date: "2007-03-06T19:47:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9325 -->
Hier das Zentralgelenk. Auffällig ist, dass die Drehgelenke hintereinander und nicht umeinander herum angeordnet sind: in Fahrtrichtung vorn ist das Lenk-Gelenk (mit Hydraulikzylindern), ein ganzes Stück dahinter kommt das Gelenk, das den Achsen das Verwinden im Raum erlaubt.


Dazwischen liegt die Kardanwelle mit zwei Gelenken (homokinetischer Aufbau, der Kardanfehler des einen Gelenks hebt denjenigen des anderen auf).


Was noch auffällt: das Fahrzeug hat kein Zentraldifferenzial. Die Antriebswelle geht von vorn her geradewegs durch, die hintere Achse wird über einen Durchtrieb durch die zweite Achse hindurch angetrieben.


Was außerdem noch auffällt: das Fahrzeug hat keinen Auspuff, wie man ihn so kennt. Die Abgase des Motors werden über den Schlauch links oben in die Hohlräume der doppelwandigen Mulde geleitet und können erst ganz hinten an der oberen Kante wieder austreten. Das dient dazu, den Muldenboden frostfrei zu halten, damit die Ladung im Winter (also, das was man eigentlich so als Winter kennt, mit Kälte und so...) nicht anfriert.