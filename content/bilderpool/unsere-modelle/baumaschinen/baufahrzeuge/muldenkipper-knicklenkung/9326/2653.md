---
layout: "comment"
hidden: true
title: "2653"
date: "2007-03-11T14:48:44"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Ja, richtig: RTFM. Die PDF-Datei lag auch schon lange auf meiner Festplatte rum, nur bis ins Kleingedruckte hat's erst nach Fertigstellung des Autos gereicht.

Gruß,
Harald