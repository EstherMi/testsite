---
layout: "image"
title: "Bell_B30D_03.JPG"
date: "2007-03-06T19:54:50"
picture: "Bell_B30D_03.JPG"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/9329
imported:
- "2019"
_4images_image_id: "9329"
_4images_cat_id: "860"
_4images_user_id: "4"
_4images_image_date: "2007-03-06T19:54:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9329 -->
