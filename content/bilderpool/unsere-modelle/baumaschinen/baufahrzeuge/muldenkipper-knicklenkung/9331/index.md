---
layout: "image"
title: "Bell_B30D_07.JPG"
date: "2007-03-06T20:00:15"
picture: "Bell_B30D_07.JPG"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Gleichlaufgetriebe", "Knicklenkung", "Allrad"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/9331
imported:
- "2019"
_4images_image_id: "9331"
_4images_cat_id: "860"
_4images_user_id: "4"
_4images_image_date: "2007-03-06T20:00:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9331 -->
Ein Blick unter die Haube. Man sucht vergeblich nach einem Lenkgestänge. Da habe ich dran herumprobiert, aber nichts hat die Kräfte am Knickgelenk auch nur annähernd bändigen können. Also (in Abweichung vom Original) lenkt dieser Laster mittels Gleichlaufgetriebe. Der Motor in Bildmitte treibt die Vorderräder gleichsinnig an, und über die Antriebswelle auch das (einzelne) Differenzial in der Mitte der Hinterachskonstruktion. Der Motor im Bild unten treibt die Vorderräder gegensinnig an, das Gleichlaufgetriebe überlagert beide Drehbewegungen.