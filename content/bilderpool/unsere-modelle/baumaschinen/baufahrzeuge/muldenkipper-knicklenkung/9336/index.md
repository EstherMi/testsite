---
layout: "image"
title: "Bell_B30D_21.JPG"
date: "2007-03-06T20:18:35"
picture: "Bell_B30D_21.JPG"
weight: "18"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/9336
imported:
- "2019"
_4images_image_id: "9336"
_4images_cat_id: "860"
_4images_user_id: "4"
_4images_image_date: "2007-03-06T20:18:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9336 -->
Der Minimot treibt die linke Schnecke der Kippvorrichtung direkt an, die rechte Schnecke wird über den Kettentrieb am anderen Ende der linken Schnecke angetrieben. Als Gelenk (unten drunter) dient ein Statik-Scharnier aus alten ft-Tagen.