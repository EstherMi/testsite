---
layout: "image"
title: "Bell_B30D_04.JPG"
date: "2007-03-06T19:56:13"
picture: "Bell_B30D_04.JPG"
weight: "12"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/9330
imported:
- "2019"
_4images_image_id: "9330"
_4images_cat_id: "860"
_4images_user_id: "4"
_4images_image_date: "2007-03-06T19:56:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9330 -->
Mangels ft-Hydraulik arbeitet die Kippvorrichtung hier mit Schneckenantrieb. Der Mini-Mot schwenkt mit dem Aufbau mit, was eine fehlerträchtige Gelenkverbindung einspart.