---
layout: "image"
title: "Bell_B30D_27.JPG"
date: "2007-03-30T17:19:58"
picture: "Bell_B30D_27.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/9854
imported:
- "2019"
_4images_image_id: "9854"
_4images_cat_id: "860"
_4images_user_id: "4"
_4images_image_date: "2007-03-30T17:19:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9854 -->
Die Hinterachse nochmal genauer und bei besserem Licht. Vor und hinter der Antriebsschnecke sitzen je eine "Hülse mit Scheibe". Nach vorn ist die Welle in einer Schneckenmutter und einer Spannzange gelagert, nach hinten ebenfalls in einer Schneckenmutter, aber darin steckt eine Lagerhülse 36819. 
Die Schneckenmuttern habe ich verwendet, weil sie auf einer Seite runde Zapfen haben und die Antriebswelle schräg zum Rahmen verläuft.