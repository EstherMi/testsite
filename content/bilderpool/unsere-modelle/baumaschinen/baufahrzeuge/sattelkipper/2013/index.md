---
layout: "image"
title: "Zweiter Versuch"
date: "2003-12-07T19:10:29"
picture: "IMG_0449.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- details/2013
imported:
- "2019"
_4images_image_id: "2013"
_4images_cat_id: "145"
_4images_user_id: "6"
_4images_image_date: "2003-12-07T19:10:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2013 -->
So kann man das Getriebe schalten wenn man keinen Servo zur Verfügung hat.