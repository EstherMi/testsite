---
layout: "image"
title: "Zij aanzicht"
date: "2011-06-22T08:18:43"
picture: "TD24_024.jpg"
weight: "4"
konstrukteure: 
- "Chef8"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/30891
imported:
- "2019"
_4images_image_id: "30891"
_4images_cat_id: "2307"
_4images_user_id: "838"
_4images_image_date: "2011-06-22T08:18:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30891 -->
Geheel weer dicht gebouwd plus bediening voor de cilinders