---
layout: "image"
title: "Cilinder"
date: "2011-06-22T08:18:44"
picture: "TD24_026.jpg"
weight: "6"
konstrukteure: 
- "Chef8"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/30893
imported:
- "2019"
_4images_image_id: "30893"
_4images_cat_id: "2307"
_4images_user_id: "838"
_4images_image_date: "2011-06-22T08:18:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30893 -->
Cilinder aan 2 kanten aangesloten want gewicht zakt niet door zijn eigen gewicht :-)