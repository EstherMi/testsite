---
layout: "image"
title: "International TD24 Pipe Layer"
date: "2011-06-20T21:57:10"
picture: "TD24_001.jpg"
weight: "7"
konstrukteure: 
- "Chef8"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/30868
imported:
- "2019"
_4images_image_id: "30868"
_4images_cat_id: "2307"
_4images_user_id: "838"
_4images_image_date: "2011-06-20T21:57:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30868 -->
Mijn nieuwste project 
Een pijp legger van vroeger met gelijkloop aandrijving