---
layout: "image"
title: "Voorste klep"
date: "2011-02-13T17:51:43"
picture: "pivot_016.jpg"
weight: "19"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/29968
imported:
- "2019"
_4images_image_id: "29968"
_4images_cat_id: "2210"
_4images_user_id: "838"
_4images_image_date: "2011-02-13T17:51:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29968 -->
Arm omhoog de klep is dicht, zand kan er niet uit (Transport stand)