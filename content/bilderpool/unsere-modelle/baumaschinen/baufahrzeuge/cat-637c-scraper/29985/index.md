---
layout: "image"
title: "Vooraanzicht"
date: "2011-02-14T14:45:47"
picture: "pivot_030.jpg"
weight: "3"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/29985
imported:
- "2019"
_4images_image_id: "29985"
_4images_cat_id: "2210"
_4images_user_id: "838"
_4images_image_date: "2011-02-14T14:45:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29985 -->
Motorkap plus cabine