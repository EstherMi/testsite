---
layout: "image"
title: "Losschuif"
date: "2011-02-13T17:51:44"
picture: "pivot_018.jpg"
weight: "21"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/29970
imported:
- "2019"
_4images_image_id: "29970"
_4images_cat_id: "2210"
_4images_user_id: "838"
_4images_image_date: "2011-02-13T17:51:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29970 -->
Losschuif halverwege terug