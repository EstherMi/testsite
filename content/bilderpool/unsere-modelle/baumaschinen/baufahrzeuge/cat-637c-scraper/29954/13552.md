---
layout: "comment"
hidden: true
title: "13552"
date: "2011-02-13T19:36:36"
uploadBy:
- "robvanbaal"
license: "unknown"
imported:
- "2019"
---
Vet model Ruurd! Ziet er erg goed en doordacht uit.  
Ik hoop het model eens live te mogen zien. Kom je mogelijk langs in Hoofddorp (http://www.fischertechnikclub.nl/index.php?option=com_content&view=article&id=292&catid=13&Itemid=57).
Groet, rob