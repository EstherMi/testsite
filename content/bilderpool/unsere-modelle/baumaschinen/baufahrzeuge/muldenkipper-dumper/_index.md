---
layout: "overview"
title: "Muldenkipper 'Dumper'"
date: 2019-12-17T19:15:10+01:00
legacy_id:
- categories/2929
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2929 --> 
Dies ist mein Muldenkipper oder auf Englisch mein "Dumper". Der ein oder andere kennt das Modell vielleicht schon von der Modellaustellung des ft-Fanclubtags 2014. Sowohl das Design als auch technische Funktionen wie der Allradantrieb oder die pneumatische (naja in echt hydraulische) Lenkung orientieren sich an dem Volvo Muldenkipper A30F.