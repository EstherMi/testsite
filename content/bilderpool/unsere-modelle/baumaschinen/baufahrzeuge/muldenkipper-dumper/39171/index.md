---
layout: "image"
title: "Ansicht von oben"
date: "2014-08-08T21:21:23"
picture: "dumper05.jpg"
weight: "5"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/39171
imported:
- "2019"
_4images_image_id: "39171"
_4images_cat_id: "2929"
_4images_user_id: "2228"
_4images_image_date: "2014-08-08T21:21:23"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39171 -->
Führerhaus und Mulde von oben