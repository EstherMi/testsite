---
layout: "image"
title: "Lenkung"
date: "2014-08-08T21:21:23"
picture: "dumper03.jpg"
weight: "3"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/39169
imported:
- "2019"
_4images_image_id: "39169"
_4images_cat_id: "2929"
_4images_user_id: "2228"
_4images_image_date: "2014-08-08T21:21:23"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39169 -->
Hier sieht man den Lenkeinschlag. Das Fahrzeug wird elektropneumatisch gelenkt. Dazu sind zwei Magnetventile an den Ausgang M2 des Empfängers angeschlossen. Bewegt man den Joystick am Control Set nach links oder rechts, schaltet je eines der beiden Ventile. Dadurch drückt der Zylinder das Fahrzeug nach rechts oder links.
Um diese Schaltung zu ermöglichen, habe ich zwei Dioden verwendet (siehe Schaltplan Lenkung). Obwohl die Lenkung nicht proportional ist, bewährt sich diese Technik bei meinen Knicklenkern schon seit drei Jahren.