---
layout: "image"
title: "Schaltplan Lenkung"
date: "2014-08-08T21:21:23"
picture: "dumper04.jpg"
weight: "4"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/39170
imported:
- "2019"
_4images_image_id: "39170"
_4images_cat_id: "2929"
_4images_user_id: "2228"
_4images_image_date: "2014-08-08T21:21:23"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39170 -->
Wie schon im Bild "Lenkung" erwähnt habe ich für die pneumatische Lenkung eine Diodenschaltung verwendet. Die Schaltung ist recht simpel und bewirkt. Sie bewirkt, dass zwei Magnetventile an einen Motorausgang angeschlossen werden können.
In der Praxis funktioniert die Lenkung dann wie folgt: Bewegt man den linken Joystick nach links, schaltet Ventil 1 und das Fahrzeug lenkt nach links. Umgekehrt kippt man den linken Joystick nach rechts, schaltet Ventil 2 und der Dumper lenkt nach rechts. Durch kurzes Antippen der Joysticks sind auch Feineinstellungen der Lenkung möglich. Auch das Geradeausfahren erfolgt nach diesem Prinzip.