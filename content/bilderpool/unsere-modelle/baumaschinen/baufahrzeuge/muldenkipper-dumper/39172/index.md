---
layout: "image"
title: "Mulde gekippt"
date: "2014-08-08T21:21:23"
picture: "dumper06.jpg"
weight: "6"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/39172
imported:
- "2019"
_4images_image_id: "39172"
_4images_cat_id: "2929"
_4images_user_id: "2228"
_4images_image_date: "2014-08-08T21:21:23"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39172 -->
Die Mulde kann pneumatisch gekippt werden. Beim Original befinden sich die Zylinder an der Seite der Mulde. Dies ist mit den vergleichsweise kurzen ft-Zylindern nicht möglich.
Ich habe einen Zylinder mit und einen ohne Feder verwendet. Dies ermöglicht den richtigen Widerstand für das Ablassen der Mulde.