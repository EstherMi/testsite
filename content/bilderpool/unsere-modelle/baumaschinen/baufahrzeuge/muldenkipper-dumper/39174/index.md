---
layout: "image"
title: "Allradantrieb"
date: "2014-08-08T21:21:23"
picture: "dumper08.jpg"
weight: "8"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/39174
imported:
- "2019"
_4images_image_id: "39174"
_4images_cat_id: "2929"
_4images_user_id: "2228"
_4images_image_date: "2014-08-08T21:21:23"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39174 -->
Hier sieht man, wie die Kraft auf die letze Achse übertragen wird. Ich habe das so umständlich gelöst, da ich zu wenig Kegelzahnräder habe.