---
layout: "image"
title: "Kabine"
date: "2009-11-02T21:41:42"
picture: "pneumatischerkipper10.jpg"
weight: "10"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/25643
imported:
- "2019"
_4images_image_id: "25643"
_4images_cat_id: "1801"
_4images_user_id: "845"
_4images_image_date: "2009-11-02T21:41:42"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25643 -->
In der Kabine ist das Control Set und der Schalter um es ein und auszuschalten.