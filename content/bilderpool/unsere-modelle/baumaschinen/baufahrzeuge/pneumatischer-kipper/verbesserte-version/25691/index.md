---
layout: "image"
title: "Klappe"
date: "2009-11-07T20:23:46"
picture: "verbesserteversion10.jpg"
weight: "10"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/25691
imported:
- "2019"
_4images_image_id: "25691"
_4images_cat_id: "1802"
_4images_user_id: "845"
_4images_image_date: "2009-11-07T20:23:46"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25691 -->
