---
layout: "image"
title: "Klappe"
date: "2009-11-07T20:23:46"
picture: "verbesserteversion08.jpg"
weight: "8"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/25689
imported:
- "2019"
_4images_image_id: "25689"
_4images_cat_id: "1802"
_4images_user_id: "845"
_4images_image_date: "2009-11-07T20:23:46"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25689 -->
Das ist die Klappe, damit die Ladung rausrutscht.