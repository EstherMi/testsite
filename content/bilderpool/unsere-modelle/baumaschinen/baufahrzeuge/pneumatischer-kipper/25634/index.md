---
layout: "image"
title: "Gesamtansicht"
date: "2009-11-02T21:41:41"
picture: "pneumatischerkipper01.jpg"
weight: "1"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/25634
imported:
- "2019"
_4images_image_id: "25634"
_4images_cat_id: "1801"
_4images_user_id: "845"
_4images_image_date: "2009-11-02T21:41:41"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25634 -->
