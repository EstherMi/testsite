---
layout: "image"
title: "Pneumatikzylinder"
date: "2009-11-02T21:41:42"
picture: "pneumatischerkipper03.jpg"
weight: "3"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/25636
imported:
- "2019"
_4images_image_id: "25636"
_4images_cat_id: "1801"
_4images_user_id: "845"
_4images_image_date: "2009-11-02T21:41:42"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25636 -->
Mit diesem Zylinder wird die Laderampe gekippt.