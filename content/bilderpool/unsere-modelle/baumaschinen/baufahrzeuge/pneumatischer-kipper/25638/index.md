---
layout: "image"
title: "Schläuche"
date: "2009-11-02T21:41:42"
picture: "pneumatischerkipper05.jpg"
weight: "5"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/25638
imported:
- "2019"
_4images_image_id: "25638"
_4images_cat_id: "1801"
_4images_user_id: "845"
_4images_image_date: "2009-11-02T21:41:42"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25638 -->
Hier sieht man wie die Schläuche verlegt sind.