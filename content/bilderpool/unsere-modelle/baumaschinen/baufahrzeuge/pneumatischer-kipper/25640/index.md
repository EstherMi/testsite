---
layout: "image"
title: "pneumatischerkipper07.jpg"
date: "2009-11-02T21:41:42"
picture: "pneumatischerkipper07.jpg"
weight: "7"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/25640
imported:
- "2019"
_4images_image_id: "25640"
_4images_cat_id: "1801"
_4images_user_id: "845"
_4images_image_date: "2009-11-02T21:41:42"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25640 -->
