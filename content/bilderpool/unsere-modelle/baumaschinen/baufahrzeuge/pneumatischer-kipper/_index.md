---
layout: "overview"
title: "pneumatischer Kipper"
date: 2019-12-17T19:14:57+01:00
legacy_id:
- categories/1801
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1801 --> 
Die Laderampe kann pneumatisch gekippt werden. Außerdem hat er eine einfache Federung und ist fernsteuerbar. Ich habe die verkleinerte Version meines Powerkompressors genommen.