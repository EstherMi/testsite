---
layout: "image"
title: "Telesc-49-lr"
date: "2015-07-09T07:25:27"
picture: "telescopmobilkran29.jpg"
weight: "29"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/41426
imported:
- "2019"
_4images_image_id: "41426"
_4images_cat_id: "3096"
_4images_user_id: "2449"
_4images_image_date: "2015-07-09T07:25:27"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41426 -->
Die Lenkung soll eigentlich noch verbessert werden, weil es mit den Reifen vom 80'er Jahren so nicht besonders gut Lenkt.