---
layout: "image"
title: "Telesc-01-lr"
date: "2015-07-09T07:25:27"
picture: "telescopmobilkran01.jpg"
weight: "1"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/41398
imported:
- "2019"
_4images_image_id: "41398"
_4images_cat_id: "3096"
_4images_user_id: "2449"
_4images_image_date: "2015-07-09T07:25:27"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41398 -->
Meine Version des Telescopmobilkrans der Achziger Jahren. Weil Ich nur 2 lange Alus hatte, musste Ich das anders lösen.