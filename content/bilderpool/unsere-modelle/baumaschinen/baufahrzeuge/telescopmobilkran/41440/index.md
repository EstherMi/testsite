---
layout: "image"
title: "Telesc-lr-84"
date: "2015-07-09T07:25:27"
picture: "telescopmobilkran43.jpg"
weight: "43"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/41440
imported:
- "2019"
_4images_image_id: "41440"
_4images_cat_id: "3096"
_4images_user_id: "2449"
_4images_image_date: "2015-07-09T07:25:27"
_4images_image_order: "43"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41440 -->
