---
layout: "overview"
title: "Meißelbagger"
date: 2019-12-17T19:15:38+01:00
legacy_id:
- categories/1653
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1653 --> 
Dieses Gerät ist mit einem riesigen Meißel ausgestattet. Der Arm ist ähnlich wie im Pneumatik-2-Baukasten und pneumatisch gesteuert. Der Meißel wird durch einen Exzenter angetrieben. Mit so einem Gerät werden Straßen aufgebrochen.