---
layout: "image"
title: "heruntergelassener Arm"
date: "2009-05-27T18:14:02"
picture: "meiselbagger09.jpg"
weight: "9"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/24113
imported:
- "2019"
_4images_image_id: "24113"
_4images_cat_id: "1653"
_4images_user_id: "845"
_4images_image_date: "2009-05-27T18:14:02"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24113 -->
Hier ist der Arm runtergefahren