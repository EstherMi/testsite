---
layout: "image"
title: "Zylinder"
date: "2009-05-27T18:14:02"
picture: "meiselbagger07.jpg"
weight: "7"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/24111
imported:
- "2019"
_4images_image_id: "24111"
_4images_cat_id: "1653"
_4images_user_id: "845"
_4images_image_date: "2009-05-27T18:14:02"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24111 -->
