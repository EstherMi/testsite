---
layout: "image"
title: "Kompressor"
date: "2009-05-27T18:14:02"
picture: "meiselbagger08.jpg"
weight: "8"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/24112
imported:
- "2019"
_4images_image_id: "24112"
_4images_cat_id: "1653"
_4images_user_id: "845"
_4images_image_date: "2009-05-27T18:14:02"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24112 -->
Der Komperssor ist die verkleinerte Version von dem hier:
http://www.ftcommunity.de/categories.php?cat_id=1648