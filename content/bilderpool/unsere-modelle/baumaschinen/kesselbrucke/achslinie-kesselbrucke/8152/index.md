---
layout: "image"
title: "diverse Achskonstruktion für die Kesselbrücke"
date: "2006-12-26T15:40:18"
picture: "achsliniefuerdiekesselbruecke07.jpg"
weight: "7"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/8152
imported:
- "2019"
_4images_image_id: "8152"
_4images_cat_id: "753"
_4images_user_id: "389"
_4images_image_date: "2006-12-26T15:40:18"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8152 -->
