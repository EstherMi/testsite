---
layout: "image"
title: "Dikke kont"
date: "2013-02-17T23:44:00"
picture: "P2160003.jpg"
weight: "3"
konstrukteure: 
- "Ruurd"
fotografen:
- "Chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/36646
imported:
- "2019"
_4images_image_id: "36646"
_4images_cat_id: "2716"
_4images_user_id: "838"
_4images_image_date: "2013-02-17T23:44:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36646 -->
Model is voor 90% nieuw opgebouwd. Alleen het onderstel is hetzelfde als de Liebherr R9400.
Contra gewicht weegt 2kg had eigenlijk 3kg moeten zijn. Meerdere foto's volgen nog.