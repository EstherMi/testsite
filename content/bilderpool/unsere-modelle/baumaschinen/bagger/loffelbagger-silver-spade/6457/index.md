---
layout: "image"
title: "Werkzeug"
date: "2006-06-20T21:35:52"
picture: "DSCN0813.jpg"
weight: "26"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/6457
imported:
- "2019"
_4images_image_id: "6457"
_4images_cat_id: "566"
_4images_user_id: "184"
_4images_image_date: "2006-06-20T21:35:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6457 -->
Löffel in Arbeitsstellung