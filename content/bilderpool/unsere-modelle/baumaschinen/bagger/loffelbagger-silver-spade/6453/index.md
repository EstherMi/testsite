---
layout: "image"
title: "komletter Unterbau"
date: "2006-06-20T21:35:52"
picture: "DSCN0808.jpg"
weight: "22"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/6453
imported:
- "2019"
_4images_image_id: "6453"
_4images_cat_id: "566"
_4images_user_id: "184"
_4images_image_date: "2006-06-20T21:35:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6453 -->
Der obere Drehkranz hat eine UK Höhe von 20,5 cm.