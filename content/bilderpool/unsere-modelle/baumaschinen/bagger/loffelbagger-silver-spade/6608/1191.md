---
layout: "comment"
hidden: true
title: "1191"
date: "2006-07-12T12:32:45"
uploadBy:
- "ludger-ftc"
license: "unknown"
imported:
- "2019"
---
ich sage mal so, wenn man eine größere Menge kauft, kann man handeln...... ;-))

Das sind genau die, die ich auch in die Schneckenmuttern 37925 eingebaut habe.

Die genaue Bezeichnung ist:
Kugellager für den Modellbau usw. aussen 12mm, innen 4mm, Breite 4mm mit seitlichen Staubschutz versehen.
Typ KLM 12; 2 Stück in Blisterverpackung.