---
layout: "image"
title: "Drehkranz Unterteil"
date: "2006-07-10T17:38:39"
picture: "DSCN0859.jpg"
weight: "1"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/6608
imported:
- "2019"
_4images_image_id: "6608"
_4images_cat_id: "566"
_4images_user_id: "184"
_4images_image_date: "2006-07-10T17:38:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6608 -->
Hier habe ich die Räder 14 gegen die Oppermann - Kugellager getauscht. 72 Stück ergeben einen Super-Leichtlauf des Baggeroberbaues.