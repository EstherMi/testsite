---
layout: "comment"
hidden: true
title: "1185"
date: "2006-07-10T19:01:38"
uploadBy:
- "ludger-ftc"
license: "unknown"
imported:
- "2019"
---
An diese Variante mit den Rollenböcken hatte ich auch zuerst gedacht. Aber dann gefiel mir der Baustein 15 besser. Da braucht man nur bohren und die Niete reinstecken. Und von diesen Bausteinen habe ich mehr als genug. Da kann man schon mal 10 Stück "opfern".