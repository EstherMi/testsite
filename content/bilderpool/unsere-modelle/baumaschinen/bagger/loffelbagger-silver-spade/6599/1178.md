---
layout: "comment"
hidden: true
title: "1178"
date: "2006-07-08T19:22:45"
uploadBy:
- "ludger-ftc"
license: "unknown"
imported:
- "2019"
---
Heute habe ich die schwarzen Räder 14 vom Drehkranz durch die Kugellager von Oppermann ersetzt und eine dicke Pappe als Lauffläche zwischengelegt. Ihr könnt euch gar nicht vorstellen wie gut das jetzt läuft.
Dann habe ich noch den kompletten Kabelbaum für die Fahrwerke installiert. Auch das läuft super (und das bei dem Gewicht).