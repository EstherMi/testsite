---
layout: "image"
title: "Drehkranz Unterteil"
date: "2006-07-10T17:38:39"
picture: "DSCN0861.jpg"
weight: "2"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/6609
imported:
- "2019"
_4images_image_id: "6609"
_4images_cat_id: "566"
_4images_user_id: "184"
_4images_image_date: "2006-07-10T17:38:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6609 -->
eine Detail Aufnahme