---
layout: "image"
title: "die Lenkung"
date: "2006-06-20T21:35:52"
picture: "DSCN0801.jpg"
weight: "12"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/6443
imported:
- "2019"
_4images_image_id: "6443"
_4images_cat_id: "566"
_4images_user_id: "184"
_4images_image_date: "2006-06-20T21:35:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6443 -->
In dem mittig eingebauten Baustein 15 mit Bohrung wird eine Metallachse eingeschoben und lenkt damit das Modell