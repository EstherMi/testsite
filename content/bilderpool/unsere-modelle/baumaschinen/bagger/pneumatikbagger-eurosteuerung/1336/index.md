---
layout: "image"
title: "Neue Zylinder"
date: "2003-08-17T20:16:35"
picture: "IMG_0363.jpg"
weight: "25"
konstrukteure: 
- "Michael Orlik"
fotografen:
- "Michael Orlik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- details/1336
imported:
- "2019"
_4images_image_id: "1336"
_4images_cat_id: "23"
_4images_user_id: "6"
_4images_image_date: "2003-08-17T20:16:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1336 -->
