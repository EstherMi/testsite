---
layout: "image"
title: "Hebemechanik"
date: "2006-03-09T23:20:10"
picture: "IMG_0287.jpg"
weight: "9"
konstrukteure: 
- "Michael Orlik//Sannchen90"
fotografen:
- "Michael Orlik//Sannchen90"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- details/5847
imported:
- "2019"
_4images_image_id: "5847"
_4images_cat_id: "23"
_4images_user_id: "6"
_4images_image_date: "2006-03-09T23:20:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5847 -->
Zu sehen ist die Hebemechanik für das Schiebeschild, es handelt sich um eine etwas abgewandelte Version des Krafthebers von Harald