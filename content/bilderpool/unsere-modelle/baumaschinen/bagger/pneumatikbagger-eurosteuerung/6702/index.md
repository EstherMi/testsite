---
layout: "image"
title: "Schleifring"
date: "2006-08-20T20:05:17"
picture: "Bagger_2_2.jpg"
weight: "6"
konstrukteure: 
- "Michael Orlik//Sannchen90"
fotografen:
- "Michael Orlik//Sannchen90"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- details/6702
imported:
- "2019"
_4images_image_id: "6702"
_4images_cat_id: "23"
_4images_user_id: "6"
_4images_image_date: "2006-08-20T20:05:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6702 -->
Im Drehkranz sitzt der Schleifring von Harald, der kann 6 Kabel übertragen. Der ist sehr praktikabel und auch leiocht einbaubar.

Vielen Dank dafür Harald.