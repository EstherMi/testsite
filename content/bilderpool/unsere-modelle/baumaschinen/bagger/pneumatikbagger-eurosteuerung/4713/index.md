---
layout: "image"
title: "Löffelstiel"
date: "2005-09-11T19:11:38"
picture: "IMG_1479.jpg"
weight: "14"
konstrukteure: 
- "Michael Orlik"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- details/4713
imported:
- "2019"
_4images_image_id: "4713"
_4images_cat_id: "23"
_4images_user_id: "6"
_4images_image_date: "2005-09-11T19:11:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4713 -->
