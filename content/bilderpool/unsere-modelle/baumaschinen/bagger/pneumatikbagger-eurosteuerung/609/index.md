---
layout: "image"
title: "Ausleger mit den Zylindern von H.Steinhaus"
date: "2003-04-24T01:54:44"
picture: "IMG_0414.jpg"
weight: "26"
konstrukteure: 
- "Michael Orlik"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- details/609
imported:
- "2019"
_4images_image_id: "609"
_4images_cat_id: "23"
_4images_user_id: "6"
_4images_image_date: "2003-04-24T01:54:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=609 -->
Die Zylinder sind eine Eigenkonstruktion von Harald Steinhaus.