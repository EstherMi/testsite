---
layout: "image"
title: "IMG 0405"
date: "2003-04-21T19:48:18"
picture: "IMG_0405.jpg"
weight: "50"
konstrukteure: 
- "Michael Orlik"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/213
imported:
- "2019"
_4images_image_id: "213"
_4images_cat_id: "23"
_4images_user_id: "1"
_4images_image_date: "2003-04-21T19:48:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=213 -->
