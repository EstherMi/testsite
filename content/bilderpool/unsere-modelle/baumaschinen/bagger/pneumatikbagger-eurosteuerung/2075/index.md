---
layout: "image"
title: "Fahrwerk"
date: "2004-01-25T16:35:29"
picture: "IMG_0513.jpg"
weight: "20"
konstrukteure: 
- "Michael Orlik"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- details/2075
imported:
- "2019"
_4images_image_id: "2075"
_4images_cat_id: "23"
_4images_user_id: "6"
_4images_image_date: "2004-01-25T16:35:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2075 -->
Fahrweksverkabelung und Umbau auf Powermotoren 50:1. Gesteuert über Infrarotsteuerung . Wird dann ein wenig schwierig zum laden aber das brauche ich nicht so oft.