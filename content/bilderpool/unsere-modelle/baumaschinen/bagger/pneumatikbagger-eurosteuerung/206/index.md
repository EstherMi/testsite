---
layout: "image"
title: "IMG 0394"
date: "2003-04-21T19:48:18"
picture: "IMG_0394.jpg"
weight: "43"
konstrukteure: 
- "Michael Orlik"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/206
imported:
- "2019"
_4images_image_id: "206"
_4images_cat_id: "23"
_4images_user_id: "1"
_4images_image_date: "2003-04-21T19:48:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=206 -->
