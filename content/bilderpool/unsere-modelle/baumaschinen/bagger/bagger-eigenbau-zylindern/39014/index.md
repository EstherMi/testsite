---
layout: "image"
title: "Oberwagen von unten"
date: "2014-07-11T14:18:05"
picture: "pbagger4.jpg"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/39014
imported:
- "2019"
_4images_image_id: "39014"
_4images_cat_id: "2920"
_4images_user_id: "4"
_4images_image_date: "2014-07-11T14:18:05"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39014 -->
Ober- und Unterwagen sind nur mechanisch verbunden, d.h. das Kabel zum Antriebsmotor (und dem nicht vorhandenen Lenkmotor) wird durch den Drehkranz gefädelt und zwirbelt sich mit der Zeit auf.