---
layout: "overview"
title: "Bagger mit Eigenbau-Zylindern"
date: 2019-12-17T19:14:24+01:00
legacy_id:
- categories/2920
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2920 --> 
Ein Werk aus alten Tagen (man schrieb das Jahr 2002): ein Bagger mit selbst gebauten Pneumatikzylindern.