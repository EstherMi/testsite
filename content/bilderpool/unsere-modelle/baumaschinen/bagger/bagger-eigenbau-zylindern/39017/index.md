---
layout: "image"
title: "Innereien"
date: "2014-07-11T14:18:06"
picture: "pbagger7.jpg"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/39017
imported:
- "2019"
_4images_image_id: "39017"
_4images_cat_id: "2920"
_4images_user_id: "4"
_4images_image_date: "2014-07-11T14:18:06"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39017 -->
Die Innereien bei abgenommenem Deckel, Oben links im Bild befindet sich ein P-Betätiger, der bei vollem Druckluftbehälter über die gelbe Strebe und den grauen Taster darunter den Kompressor still setzt.