---
layout: "image"
title: "Raupenfahrwerk (2)"
date: "2014-07-11T14:18:05"
picture: "pbagger2.jpg"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/39012
imported:
- "2019"
_4images_image_id: "39012"
_4images_cat_id: "2920"
_4images_user_id: "4"
_4images_image_date: "2014-07-11T14:18:05"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39012 -->
Das wäre ein Fall für das "geteilte Gleichlaufgetriebe" von thomas004 gewesen:
http://ftcommunity.de/categories.php?cat_id=1094

Nur, da war der Bagger schon längst wieder zerlegt.
