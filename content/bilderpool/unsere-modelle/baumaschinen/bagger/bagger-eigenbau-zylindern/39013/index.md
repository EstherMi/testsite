---
layout: "image"
title: "Raupenfahrwerk (3)"
date: "2014-07-11T14:18:05"
picture: "pbagger3.jpg"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/39013
imported:
- "2019"
_4images_image_id: "39013"
_4images_cat_id: "2920"
_4images_user_id: "4"
_4images_image_date: "2014-07-11T14:18:05"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39013 -->
von unten. Die weißen Streben sind selbst gemacht.