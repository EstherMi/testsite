---
layout: "image"
title: "Dreierlei"
date: "2014-07-11T14:18:06"
picture: "pbagger5.jpg"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/39015
imported:
- "2019"
_4images_image_id: "39015"
_4images_cat_id: "2920"
_4images_user_id: "4"
_4images_image_date: "2014-07-11T14:18:06"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39015 -->
Da sind dreierlei Zylinder verbaut. Der dicke ganz unten ist einfach wirkend (nur drücken) und lässt sich von Hand nicht mehr bändigen. Die anderen wirken zweifach (ziehen und drücken).