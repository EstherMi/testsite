---
layout: "overview"
title: "O&K Ketten - Seilbagger R18"
date: 2019-12-17T19:14:32+01:00
legacy_id:
- categories/3203
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3203 --> 
Der O&K Seilbagger R18 ist in dieser Ausführung von 1964 bis1969 hergestellt worden. Die Entscheidung für das Modell unterlag folgenden Gründen: keine Hydraulik, ein Kettenlaufwerk mit Rollenwagen,ein Aufbau (Oberwagen) mit großem Raumangebot und eine perfekte Frabgebung von rot,grau u. weiß für Fischertechnik.Es wurden drei Ausführungen angeboten :Tieflöffel, Hochlöffel u.Schlepp- oder Klappschaufel. Von letzteren habe ich schon mehr Modelle gesehen, aber von einem Tieflöffel sogut wie nichts. Auß diesem Grund wurde es eine Tieflöffelausführung.