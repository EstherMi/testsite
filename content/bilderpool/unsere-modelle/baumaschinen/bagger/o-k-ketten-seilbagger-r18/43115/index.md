---
layout: "image"
title: "O&K Bagger R18 (13) Der Drehkranz 3"
date: "2016-03-13T14:35:15"
picture: "seilbaggerr14.jpg"
weight: "14"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- details/43115
imported:
- "2019"
_4images_image_id: "43115"
_4images_cat_id: "3203"
_4images_user_id: "946"
_4images_image_date: "2016-03-13T14:35:15"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43115 -->
Hier der fertige Drehkranz. Er erfüllt alles, was gefordert wurde: leichtgängig, hoch belastbar, geringe Bauhöhe und durch das Planetengtriebe ein hervoragender Antrieb.Aber auch diese Konstruktion hat einen Haken.. das Drucklager kostet im Handel mal locker über 80,00 Euro und kann damit so manche Portokasse zu hoch belasten.