---
layout: "image"
title: "O&K Bagger R18 (2)"
date: "2016-03-13T14:35:14"
picture: "seilbaggerr02.jpg"
weight: "2"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- details/43103
imported:
- "2019"
_4images_image_id: "43103"
_4images_cat_id: "3203"
_4images_user_id: "946"
_4images_image_date: "2016-03-13T14:35:14"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43103 -->
siehe Bild 1