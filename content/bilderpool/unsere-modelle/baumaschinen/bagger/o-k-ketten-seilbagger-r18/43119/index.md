---
layout: "image"
title: "O&K Bagger R18 (18) Frontansicht"
date: "2016-03-13T14:35:15"
picture: "seilbaggerr18.jpg"
weight: "18"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- details/43119
imported:
- "2019"
_4images_image_id: "43119"
_4images_cat_id: "3203"
_4images_user_id: "946"
_4images_image_date: "2016-03-13T14:35:15"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43119 -->
Die Kinematik des Baggerarmes, des Unterarmers  sowie der Schaufel werden, wie im Orginal, mit 3 Seilwinden betätigt, die durch die richtige Übersetzung auch die pefekte Geschwindigkeit ereichen, um einen guten Baggerbetrieb wie beim Orginal zu erreichen. Leider hat er nicht, wie im Orginal, Rutschkupplungen an den Seilwinden.Aber das kann man perfekt über die feinfühlige Fernsteuerung von Ft. ausgleichen.