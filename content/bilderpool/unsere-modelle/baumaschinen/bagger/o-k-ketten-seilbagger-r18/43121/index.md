---
layout: "image"
title: "O&K Bagger R18 (20) Heckansicht"
date: "2016-03-13T14:35:15"
picture: "seilbaggerr20.jpg"
weight: "20"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- details/43121
imported:
- "2019"
_4images_image_id: "43121"
_4images_cat_id: "3203"
_4images_user_id: "946"
_4images_image_date: "2016-03-13T14:35:15"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43121 -->
Die unteren Rückleuten sind rot, sehen auf dem Bild aber aus wie orange. Die O&K Firmenlogos habe ich mit dem Computer auf weißem, selbstklebendem Papier gerduckt u. aufgeklebt. Der weiße Streifen rund um das Fahrzeug ist mit den weißen Bauplatten 1 x1 38263 gemacht; die Winkelsteine im Heckteil habe ich mit weißem Klebepapier beklebt.