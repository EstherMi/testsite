---
layout: "image"
title: "O&K Bagger R18 (19) Seiten u. Heckansicht"
date: "2016-03-13T14:35:15"
picture: "seilbaggerr19.jpg"
weight: "19"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- details/43120
imported:
- "2019"
_4images_image_id: "43120"
_4images_cat_id: "3203"
_4images_user_id: "946"
_4images_image_date: "2016-03-13T14:35:15"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43120 -->
Ich habe keine Bilder vom Innenaufbau eingestellt, weil das sonst hier den Rahmen sprengen würde.