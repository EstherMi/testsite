---
layout: "image"
title: "O&K Bagger R18 (23) Transport"
date: "2016-03-13T14:35:15"
picture: "seilbaggerr23.jpg"
weight: "23"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- details/43124
imported:
- "2019"
_4images_image_id: "43124"
_4images_cat_id: "3203"
_4images_user_id: "946"
_4images_image_date: "2016-03-13T14:35:15"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43124 -->
Nun zuletzt habe ich noch ein Transportgestell ausHolz gefertigt, um den Transport ohne Schaden am Modell vollziehen zu können. Der Bagger R18 (14,3 kg Gewicht)  fährt selbstständig über eine Rampe auf das Tragegestell und kann mit den zwei Griffen ganz leicht angehoben werden.