---
layout: "image"
title: "O&K Bagger R18 (21) Vollfunktion des Uterwagens"
date: "2016-03-13T14:35:15"
picture: "seilbaggerr21.jpg"
weight: "21"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- details/43122
imported:
- "2019"
_4images_image_id: "43122"
_4images_cat_id: "3203"
_4images_user_id: "946"
_4images_image_date: "2016-03-13T14:35:15"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43122 -->
Hier noch ein Bild von einer Videoaufnahme, welche ich später noch einstellen werde. Der Unterwagen fährt mit den 4 Powermotoren 50:1 104574 spielend leicht ohne Rucken über den Pappkarton. Hier sieht man auch, dass die Konstruktion mit den Rollenwagen von O&K mächtige Vorteile hat, was mir bei der Konstuktion mit Ft - Teilen sehr zugute kam, weil die Belastung vom Unterwagengestell dadurch sehr gering ist.