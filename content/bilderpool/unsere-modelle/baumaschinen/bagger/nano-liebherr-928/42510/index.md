---
layout: "image"
title: "Nanobagger mit allen Funktionen"
date: "2015-12-09T12:11:32"
picture: "nanobagger1.jpg"
weight: "1"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "dito"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- details/42510
imported:
- "2019"
_4images_image_id: "42510"
_4images_cat_id: "3160"
_4images_user_id: "1582"
_4images_image_date: "2015-12-09T12:11:32"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42510 -->
Einem Liebherr 928 nachempfunden kann dieser Bagger alle Funktionen ausführen. Mit den Nylon-Schnüren sind alle Funktionen vom Bagger aus steuerbar: Schaufel, Beugen., senken. Der Bagger kann lenken und sich drehen und ist sehr klein. Er ist einem Liebherr 928 nachempfunden. Vielleicht geht die Schaufelansteuerung noch kleiner (nur 2 Löcher und Schnur an der Schaufenkopplung) und die roten Reifen sind irgendwie auch noch unbefriedigend. Aber sonst gfallts mer scho!