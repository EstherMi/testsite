---
layout: "overview"
title: "Nano-Liebherr 928"
date: 2019-12-17T19:14:31+01:00
legacy_id:
- categories/3160
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3160 --> 
Wie klein kann man einen voll funktionsfähigen Bagger bauen? Die Nylon-Seilsteuerrung ist der Schlüssel zu dem Nano-Liebherr 928.