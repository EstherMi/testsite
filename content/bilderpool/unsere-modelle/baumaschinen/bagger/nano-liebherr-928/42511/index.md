---
layout: "image"
title: "Nylonsteuerung"
date: "2015-12-09T12:11:32"
picture: "nanobagger2.jpg"
weight: "2"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "dito"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- details/42511
imported:
- "2019"
_4images_image_id: "42511"
_4images_cat_id: "3160"
_4images_user_id: "1582"
_4images_image_date: "2015-12-09T12:11:32"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42511 -->
Die Fäden sind doppelt aufgewickelt, damit sie in beiden Drehrichtungen einen Zug entwickeln können.