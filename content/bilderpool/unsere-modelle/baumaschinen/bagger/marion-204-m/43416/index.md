---
layout: "image"
title: "Voor aanzicht"
date: "2016-05-25T10:12:52"
picture: "P3190006_-_kopie.jpg"
weight: "2"
konstrukteure: 
- "Chef8"
fotografen:
- "Chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/43416
imported:
- "2019"
_4images_image_id: "43416"
_4images_cat_id: "3226"
_4images_user_id: "838"
_4images_image_date: "2016-05-25T10:12:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43416 -->
Dit model is mee geweest naar de Modelbouw beurs in Ede 2016. Tijdens de terugreis is er wat gebroken in het onderstel en daarom helt de machine achterover :(