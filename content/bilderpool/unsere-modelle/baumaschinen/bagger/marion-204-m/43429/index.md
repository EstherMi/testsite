---
layout: "image"
title: "Draaikrans"
date: "2016-05-25T10:12:53"
picture: "P3280062.jpg"
weight: "15"
konstrukteure: 
- "Chef8"
fotografen:
- "Chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/43429
imported:
- "2019"
_4images_image_id: "43429"
_4images_cat_id: "3226"
_4images_user_id: "838"
_4images_image_date: "2016-05-25T10:12:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43429 -->
Aandrijving van de draaikrans. Deze beide tandwielen grijpen aan op het vaste tandwiel in het midden.