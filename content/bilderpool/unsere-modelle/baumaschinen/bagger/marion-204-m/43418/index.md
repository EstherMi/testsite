---
layout: "image"
title: "Arm naar voren"
date: "2016-05-25T10:12:52"
picture: "P3190010_-_kopie.jpg"
weight: "4"
konstrukteure: 
- "Chef8"
fotografen:
- "Chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/43418
imported:
- "2019"
_4images_image_id: "43418"
_4images_cat_id: "3226"
_4images_user_id: "838"
_4images_image_date: "2016-05-25T10:12:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43418 -->
Machine heeft de beweging naar voren gemaakt