---
layout: "image"
title: "Lier"
date: "2016-05-25T10:12:52"
picture: "P3280058.jpg"
weight: "11"
konstrukteure: 
- "Chef8"
fotografen:
- "Chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/43425
imported:
- "2019"
_4images_image_id: "43425"
_4images_cat_id: "3226"
_4images_user_id: "838"
_4images_image_date: "2016-05-25T10:12:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43425 -->
Lier met zijdelingse kabel geleiding