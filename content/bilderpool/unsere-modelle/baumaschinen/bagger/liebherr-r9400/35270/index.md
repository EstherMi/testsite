---
layout: "image"
title: "Contra gewicht"
date: "2012-08-07T08:03:14"
picture: "PICT3783.jpg"
weight: "39"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/35270
imported:
- "2019"
_4images_image_id: "35270"
_4images_cat_id: "2614"
_4images_user_id: "838"
_4images_image_date: "2012-08-07T08:03:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35270 -->
Als je goed kijkt zie je door de spleet een bak knikkers met een gewicht van 1,5 kg.

Totaal aan motoren
2x xm motor
2x power motor 20:1
2x s-motor (aandrijving draakrans)

Totale lengte met gestrekte arm 85 cm