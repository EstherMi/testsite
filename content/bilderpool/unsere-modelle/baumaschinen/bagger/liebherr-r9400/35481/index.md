---
layout: "image"
title: "Liebherr R9400"
date: "2012-09-10T17:20:04"
picture: "P9100059.jpg"
weight: "11"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/35481
imported:
- "2019"
_4images_image_id: "35481"
_4images_cat_id: "2614"
_4images_user_id: "838"
_4images_image_date: "2012-09-10T17:20:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35481 -->
Aandrijving rupsen hebben het nogal zwaar ivm totale gewicht machine (ik schat 8 a 9 kg)