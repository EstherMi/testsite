---
layout: "comment"
hidden: true
title: "17216"
date: "2012-09-10T20:41:14"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Prachtige solide constructie met schitterende cilinder-oplossing.

Grüss,

Peter
Poederoyen NL