---
layout: "image"
title: "Liebherr R9400"
date: "2012-09-12T21:32:11"
picture: "P9120058.jpg"
weight: "5"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: ["Hubspindel"]
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/35506
imported:
- "2019"
_4images_image_id: "35506"
_4images_cat_id: "2614"
_4images_user_id: "838"
_4images_image_date: "2012-09-12T21:32:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35506 -->
De cilinderbodem is ongeveer 5 mm dik.  Diameter 14 mm. Het moeilijkste is het gat in het midden te krijgen en de schroefdraad er recht in te tappen. Vooral als je een draaibank van 1943 gebruikt met speling op echt alles.