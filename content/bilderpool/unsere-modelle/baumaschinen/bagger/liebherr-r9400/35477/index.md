---
layout: "image"
title: "Fertig"
date: "2012-09-10T17:20:03"
picture: "P9100055.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/35477
imported:
- "2019"
_4images_image_id: "35477"
_4images_cat_id: "2614"
_4images_user_id: "838"
_4images_image_date: "2012-09-10T17:20:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35477 -->
Eindelijk klaar het wachten was op de joysticks voor de besturing