---
layout: "image"
title: "Bagger 5"
date: "2008-03-24T10:35:18"
picture: "baggerstefanl05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/14073
imported:
- "2019"
_4images_image_id: "14073"
_4images_cat_id: "1291"
_4images_user_id: "502"
_4images_image_date: "2008-03-24T10:35:18"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14073 -->
