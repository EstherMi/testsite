---
layout: "image"
title: "Bagger 13"
date: "2008-03-24T10:35:18"
picture: "baggerstefanl13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/14081
imported:
- "2019"
_4images_image_id: "14081"
_4images_cat_id: "1291"
_4images_user_id: "502"
_4images_image_date: "2008-03-24T10:35:18"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14081 -->
Neue Verkleidung.