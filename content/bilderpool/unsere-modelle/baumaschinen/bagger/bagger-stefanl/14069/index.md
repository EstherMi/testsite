---
layout: "image"
title: "Bagger 1"
date: "2008-03-24T10:35:18"
picture: "baggerstefanl01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/14069
imported:
- "2019"
_4images_image_id: "14069"
_4images_cat_id: "1291"
_4images_user_id: "502"
_4images_image_date: "2008-03-24T10:35:18"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14069 -->
Hier mal ein Bagger von mir. Alle Funktionen sind motorisiert und werden ferngesteuert. Zur Sicherheit habe ich außerdem noch Endtaster eingebaut damit der Motor automatisch stoppt.