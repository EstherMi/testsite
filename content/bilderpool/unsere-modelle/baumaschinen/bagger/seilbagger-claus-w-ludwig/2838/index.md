---
layout: "image"
title: "Seilbagger 019"
date: "2004-11-03T12:57:55"
picture: "Seilbagger_019.JPG"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/2838
imported:
- "2019"
_4images_image_id: "2838"
_4images_cat_id: "275"
_4images_user_id: "5"
_4images_image_date: "2004-11-03T12:57:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2838 -->
von Claus-W. Ludwig