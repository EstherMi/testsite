---
layout: "image"
title: "Seilbagger 016"
date: "2004-11-03T12:57:55"
picture: "Seilbagger_016.JPG"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/2835
imported:
- "2019"
_4images_image_id: "2835"
_4images_cat_id: "275"
_4images_user_id: "5"
_4images_image_date: "2004-11-03T12:57:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2835 -->
von Claus-W. Ludwig