---
layout: "image"
title: "Fahrwerk"
date: "2011-06-04T15:48:54"
picture: "schaufelradbagger4.jpg"
weight: "4"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- details/30780
imported:
- "2019"
_4images_image_id: "30780"
_4images_cat_id: "2298"
_4images_user_id: "791"
_4images_image_date: "2011-06-04T15:48:54"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30780 -->
Hier sieht man das Fahrwerk.