---
layout: "comment"
hidden: true
title: "14391"
date: "2011-06-04T18:19:07"
uploadBy:
- "Sebastian"
license: "unknown"
imported:
- "2019"
---
Hallo,
wie du sicher gesehen hast passt sie perfekt in den Stein, den du auf dem Nächsten Bild siehst. Ich bin per Zufall drauf gestoßen als ich nach anderen Möglichkeiten suchte die Webcam zu befestigen. Normalerweise hat sie eine Art Klammer unten dran. Die Webcam an sich ist von Hercules. Sie besitzt ein USB-Kabel. 
Link zur Website: http://www.hercules.com/de/webcam/bdd/p/18/hercules-deluxe-webcam/
Ich besitze allerdings ein Vorgängermodell.
Gruß Sebastian