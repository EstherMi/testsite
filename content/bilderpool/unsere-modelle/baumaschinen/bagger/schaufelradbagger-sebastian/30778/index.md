---
layout: "image"
title: "Kabine"
date: "2011-06-04T15:48:54"
picture: "schaufelradbagger2.jpg"
weight: "2"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- details/30778
imported:
- "2019"
_4images_image_id: "30778"
_4images_cat_id: "2298"
_4images_user_id: "791"
_4images_image_date: "2011-06-04T15:48:54"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30778 -->
Am Ausleger sind zwei Kabinen angebracht. In einem der beiden ist eine Webcam befestigt und sendet Bilder an den Bildschirm.