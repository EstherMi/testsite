---
layout: "image"
title: "Beleuchtung"
date: "2011-06-04T15:48:54"
picture: "schaufelradbagger3.jpg"
weight: "3"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- details/30779
imported:
- "2019"
_4images_image_id: "30779"
_4images_cat_id: "2298"
_4images_user_id: "791"
_4images_image_date: "2011-06-04T15:48:54"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30779 -->
Am Schaufelradbagger sind 10 Lampen befestigt. Außerdem ist die Warnblinkanlage mit vier weiteren Lampen ausgestattet.