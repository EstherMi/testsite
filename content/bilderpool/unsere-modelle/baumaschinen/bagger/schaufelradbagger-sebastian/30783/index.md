---
layout: "image"
title: "Kranhacken"
date: "2011-06-04T15:48:54"
picture: "schaufelradbagger7.jpg"
weight: "7"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- details/30783
imported:
- "2019"
_4images_image_id: "30783"
_4images_cat_id: "2298"
_4images_user_id: "791"
_4images_image_date: "2011-06-04T15:48:54"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30783 -->
Die Gewichte sind wichtig, damit sich das Seil abrollt.