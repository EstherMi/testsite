---
layout: "image"
title: "Kettenbagger, oben"
date: "2011-07-14T11:58:24"
picture: "kettenbagger08.jpg"
weight: "8"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/31250
imported:
- "2019"
_4images_image_id: "31250"
_4images_cat_id: "2324"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T11:58:24"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31250 -->
