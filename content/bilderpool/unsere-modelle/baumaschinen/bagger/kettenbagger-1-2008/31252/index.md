---
layout: "image"
title: "Fahrwerk von unten"
date: "2011-07-14T11:58:24"
picture: "kettenbagger10.jpg"
weight: "10"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/31252
imported:
- "2019"
_4images_image_id: "31252"
_4images_cat_id: "2324"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T11:58:24"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31252 -->
Die Antriebstechnik stammt noch weitestgehen aus dem "Powerbulldozer" Baukasten