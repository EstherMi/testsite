---
layout: "image"
title: "Kettenbagger"
date: "2011-07-14T11:58:24"
picture: "kettenbagger24.jpg"
weight: "23"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/31266
imported:
- "2019"
_4images_image_id: "31266"
_4images_cat_id: "2324"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T11:58:24"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31266 -->
ein Polwendeschalter, dient als An- und Ausschalter