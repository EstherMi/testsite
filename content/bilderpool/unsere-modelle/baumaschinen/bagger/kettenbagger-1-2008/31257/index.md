---
layout: "image"
title: "Baggerarm"
date: "2011-07-14T11:58:24"
picture: "kettenbagger15.jpg"
weight: "14"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/31257
imported:
- "2019"
_4images_image_id: "31257"
_4images_cat_id: "2324"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T11:58:24"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31257 -->
