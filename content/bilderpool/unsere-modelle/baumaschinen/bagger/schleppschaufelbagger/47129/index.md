---
layout: "image"
title: "bagger"
date: "2018-01-17T18:01:36"
picture: "bagegr22.jpg"
weight: "21"
konstrukteure: 
- "nicolas kurz"
fotografen:
- "nicolas kurz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nicolas kurz"
license: "unknown"
legacy_id:
- details/47129
imported:
- "2019"
_4images_image_id: "47129"
_4images_cat_id: "3485"
_4images_user_id: "2819"
_4images_image_date: "2018-01-17T18:01:36"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47129 -->
der antrieb ist übrigens ein direktantrieb. keine getriebe oder untersetzungen. die neuen encoder sind echt gut