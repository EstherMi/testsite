---
layout: "image"
title: "bagger"
date: "2018-01-17T18:01:36"
picture: "bagegr24.jpg"
weight: "23"
konstrukteure: 
- "nicolas kurz"
fotografen:
- "nicolas kurz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nicolas kurz"
license: "unknown"
legacy_id:
- details/47131
imported:
- "2019"
_4images_image_id: "47131"
_4images_cat_id: "3485"
_4images_user_id: "2819"
_4images_image_date: "2018-01-17T18:01:36"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47131 -->
gegengewicht (leider ein fremdteil)

passt aber trotzdem gut in die FT optik