---
layout: "image"
title: "bagger"
date: "2018-01-17T18:01:36"
picture: "bagegr12.jpg"
weight: "11"
konstrukteure: 
- "nicolas kurz"
fotografen:
- "nicolas kurz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nicolas kurz"
license: "unknown"
legacy_id:
- details/47119
imported:
- "2019"
_4images_image_id: "47119"
_4images_cat_id: "3485"
_4images_user_id: "2819"
_4images_image_date: "2018-01-17T18:01:36"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47119 -->
die Winde ist doppelt bespannt. eine Wicklung links die andere rechts. die beiden enden werden zusammengeknotet und um eine rolle geführt.

dadurch wird ein gleichmäßiger zug links und rechts erreicht bei gleichzeiter flexibilität und anpassungsfähigkeit von schlechter wicklung bzw. falschwicklung auf der winde.