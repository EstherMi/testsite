---
layout: "image"
title: "Konstruktion Kettenantrieb"
date: "2012-08-11T20:38:30"
picture: "liebherrr08.jpg"
weight: "8"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- details/35314
imported:
- "2019"
_4images_image_id: "35314"
_4images_cat_id: "2617"
_4images_user_id: "1122"
_4images_image_date: "2012-08-11T20:38:30"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35314 -->
Links eine Ft-Männchen zum Größenvergleich.
Im Fahrgestell sind keine Motoren eingebaut, die hatte der Rest des Baggers verschlungen,