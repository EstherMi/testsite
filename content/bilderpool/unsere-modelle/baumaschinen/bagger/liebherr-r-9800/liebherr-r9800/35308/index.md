---
layout: "image"
title: "Ansicht von vorne"
date: "2012-08-11T20:38:30"
picture: "liebherrr02.jpg"
weight: "2"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- details/35308
imported:
- "2019"
_4images_image_id: "35308"
_4images_cat_id: "2617"
_4images_user_id: "1122"
_4images_image_date: "2012-08-11T20:38:30"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35308 -->
Zum Original:

Der Mining-Bagger Liebherr r 9800, mit einem Gewicht von 800.000 kg, verfügt  über eine Löffelkapazität von 42 m3.
Er wird von zwei Motoren mit 2000 PS und 16 Zylindern angetrieben. Die Länge des Baggers beträgt 25,330 m.
Die maximale Höhe ist 19 m.