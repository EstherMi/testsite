---
layout: "image"
title: "Bagger Unterwagen von vorne"
date: "2014-02-21T20:18:35"
picture: "raupenbagger08.jpg"
weight: "10"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38323
imported:
- "2019"
_4images_image_id: "38323"
_4images_cat_id: "2851"
_4images_user_id: "1729"
_4images_image_date: "2014-02-21T20:18:35"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38323 -->
Nicht nur ein Abbildungsfehler...der Oberwagen hängt wegen dem Gewicht des Bagger-Arms wirklich etwas nach links.
Im aufgebockten Zustand ist auch das V zu sehen, welches die Unterwagen-Streben (die aus Motor+Getriebe bestehen) bilden.