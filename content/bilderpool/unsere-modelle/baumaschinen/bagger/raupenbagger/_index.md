---
layout: "overview"
title: "Raupenbagger"
date: 2019-12-17T19:14:16+01:00
legacy_id:
- categories/2851
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2851 --> 
Schon seit längerem denke ich über ein Raupenbagger-Projekt nach. Allerdings bin ich es nie konkret angegangen, da ich ein einigermaßen realitätsgetreues Modell für nicht machbar hielt.
Nachdem ich meine Schleifringe endlich fertig habe, die man fast vollständig im Drehkranz versenken kann, kam mir dann wenigstens für den Unterwagen eine super Idee, welche ich darstellen möchte.
Das Oberteil des Baggers ist allerdings erstmal nur ein rein optisches Modell ohne Funktion.
Dieses Mal mache ich Bilder gleich während der Bauphase und poste diese. Da wird es die eine oder andere Variante und auch Sackgassen geben.
Wenn man damit wartet, bis das Modell fertig ist, möchte man eigentlich nicht extra für Detail-Photos das Modell wieder auseinanderlegen.

Die Schleifringe habe ich übrigens hier bereits vorgestellt: http://ftcommunity.de/categories.php?cat_id=2843


