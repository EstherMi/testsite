---
layout: "image"
title: "schräg von oben"
date: "2014-02-21T20:18:35"
picture: "raupenbagger02.jpg"
weight: "4"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38317
imported:
- "2019"
_4images_image_id: "38317"
_4images_cat_id: "2851"
_4images_user_id: "1729"
_4images_image_date: "2014-02-21T20:18:35"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38317 -->
