---
layout: "image"
title: "Vorderansicht"
date: "2014-02-21T20:18:35"
picture: "raupenbagger03.jpg"
weight: "5"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38318
imported:
- "2019"
_4images_image_id: "38318"
_4images_cat_id: "2851"
_4images_user_id: "1729"
_4images_image_date: "2014-02-21T20:18:35"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38318 -->
