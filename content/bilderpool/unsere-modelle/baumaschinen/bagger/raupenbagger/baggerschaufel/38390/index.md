---
layout: "image"
title: "Ur-Version"
date: "2014-02-26T08:17:56"
picture: "Initial.jpg"
weight: "3"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38390
imported:
- "2019"
_4images_image_id: "38390"
_4images_cat_id: "2855"
_4images_user_id: "1729"
_4images_image_date: "2014-02-26T08:17:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38390 -->
Die Urversion hab ich nur schnell gemacht, damit der Bagger überhaupt eine Schaufel hat. Diese entspricht weder meinen Ansprüchen noch der Realität