---
layout: "image"
title: "Konzept"
date: "2014-06-10T06:55:49"
picture: "Schaufelvariante.jpg"
weight: "1"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: ["Raupenbagger", "Baggerschaufel", "Klemmringe", "Stellringe"]
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38929
imported:
- "2019"
_4images_image_id: "38929"
_4images_cat_id: "2855"
_4images_user_id: "1729"
_4images_image_date: "2014-06-10T06:55:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38929 -->
so ähnlich soll die neue Schaufel werden.
Der Schwung ist schon ziemlich gelungen. Leider habe ich für die Seitenwände noch keine richtige Idee.
Außerdem ist die Schaufel ziemlich schwer.

Noch besteht alles aus Originalteilen. (Außer ein paar Metall-Teile der Kinematik), aber die Fräse läuft schon warm .... ;-)