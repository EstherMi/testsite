---
layout: "image"
title: "Kettenfahrwerk komplett"
date: "2014-02-26T08:17:48"
picture: "raupenbaggerunterwagen09.jpg"
weight: "16"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38387
imported:
- "2019"
_4images_image_id: "38387"
_4images_cat_id: "2854"
_4images_user_id: "1729"
_4images_image_date: "2014-02-26T08:17:48"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38387 -->
Das Kettenfahrwerk mit aufgelegter Kette.
Diese liegt plan auf dem Boden auf. Außerdem ist die Kettenspannung perfekt. Sie sitzt nicht zu stramm (sonst wird es schwergängig, hängt aber noch nicht durch)
Das ist leider gar nicht so einfach hinzubekommen. Dabei geht es um wenige Millimeter. 
Bei anderen Fahrwerks-Varianten mit unterschiedlichen Gesamtlängen habe ich es nicht so schön hinbekommen. Die verfügbaren Bausteine 5mm und 7,5 Millimeter als Zwischenstücke sind oft nicht feingranular genug. Man kann dann lange basteln, aber entweder ist die Kette zu stramm oder hängt unschön durch.

Die aufgesetzten Statikstreben sind überwiegend wegen der Optik. Sie fixieren aber auch die Laufräder, so daß ich von der Gegenseite die Achsen nicht kontern muß