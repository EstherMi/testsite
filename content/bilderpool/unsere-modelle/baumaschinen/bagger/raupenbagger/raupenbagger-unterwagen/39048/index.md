---
layout: "image"
title: "Raupenbagger Unterwagen komplett"
date: "2014-07-26T11:32:13"
picture: "Unterwagen_komplett_funktionsfhig.jpg"
weight: "3"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: ["Raupenbagger", "Unterwagen", "Schleifring", "Raupenfunktion"]
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/39048
imported:
- "2019"
_4images_image_id: "39048"
_4images_cat_id: "2854"
_4images_user_id: "1729"
_4images_image_date: "2014-07-26T11:32:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39048 -->
Der komplette Unterwagen, alles zusammengebaut, motorisiert und verkabelt.
Zum Testen habe ich oben eine kleine Plattform draufgebaut, die Akku, Empfänger und Schalter trägt.
Die zwei Ketten werden zwar unabhängig mit jeweils eigenen Motoren angetrieben, aber mit der Raupenfunktion der Fischertechnik Fernbedienung lässt sich das Gefährt über einen Joystick komfortabel vorwärts und rückwärts fahren sowie stufenlos lenken.