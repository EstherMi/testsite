---
layout: "image"
title: "Unterwagen Übersicht"
date: "2014-02-26T08:17:48"
picture: "raupenbaggerunterwagen01.jpg"
weight: "8"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38379
imported:
- "2019"
_4images_image_id: "38379"
_4images_cat_id: "2854"
_4images_user_id: "1729"
_4images_image_date: "2014-02-26T08:17:48"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38379 -->
Der Unterwagen im Entwicklungsstadium; die beiden Kettenfahrwerke sind verschiedene Versionen