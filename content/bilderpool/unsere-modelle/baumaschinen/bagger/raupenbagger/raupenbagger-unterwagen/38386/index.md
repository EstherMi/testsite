---
layout: "image"
title: "Kettenfahrwerk Aufbau"
date: "2014-02-26T08:17:48"
picture: "raupenbaggerunterwagen08.jpg"
weight: "15"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38386
imported:
- "2019"
_4images_image_id: "38386"
_4images_cat_id: "2854"
_4images_user_id: "1729"
_4images_image_date: "2014-02-26T08:17:48"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38386 -->
Ein gesamtes Kettenfahrwerk mit bestückten Zahnrädern. Man beachte, daß die Unterseite aller Räder auf einer Linie liegen muß, damit die Kette plan auf dem Untergrund aufliegt.
Deswegen haben die Achsen der Z20 und der Z15 (welche die Läufräder für die Kette sind) einen Höhenversatz zueinander.
Für die Laufräder nehme ich Zahnräder, weil die Kette dadurch eine viel bessere Führung hat. Dies entspricht allerdings nicht ganz der Realität eines echten Baggers.

Die roten Platten dienen zur Aufnahme von Statikadaptern.