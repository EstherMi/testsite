---
layout: "image"
title: "Unteransicht Drehkranz"
date: "2014-02-26T08:17:48"
picture: "raupenbaggerunterwagen10.jpg"
weight: "17"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38388
imported:
- "2019"
_4images_image_id: "38388"
_4images_cat_id: "2854"
_4images_user_id: "1729"
_4images_image_date: "2014-02-26T08:17:48"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38388 -->
Unten am Drehkranz sind die ganzen Motoren befestigt.
Für den Antrieb sind 4 S-Motoren vorgesehen, wobei ich im Moment nur 2 angeschlossen habe.
Die Motoren sind nicht nur der Antrieb, sondern haben auch eine tragende Funktion. Sie bilden das V, über die die Kettenfahrwerke befestigt sind.

Auch der Drehkranz wird von unten angetrieben. Der Micro/Minimotor, den ich hier einsetze, hat einen Grund:
Das Rastritzel Z28 (31082) trägt auf der anderen Seite ein Z10. Durch den Minmotor hat dieses Z10 genau die Höhe vom Drehkranz und kann diesen direkt antreiben.
Allerdings schafft dieser Motor es nicht immer, den Drehkranz zu drehen (mein Drehkranz-Exemplar ist allerdings auch etwas schwergängig).
Ich könnte entweder 2 Minimotoren rechts und links einbauen, oder ich nehme einen weiteren S-Motor, für den der Platz ausreichen würde.
Allerdings muß ich in diesem Fall ein Problem lösen: Da der S-Motor größer ist, ist mit Rastachsen das Z10 dann nicht mehr auf der Höhe des Drehkranzes.
Die Alternative Metallachse mit Z10 35112 bringt ein anderes Problem. Die Zähne dieses Z10 haben eine andere Form als die des Z10 für Rastachsen (35945) und greifen nicht richtig in den Drehkranz.