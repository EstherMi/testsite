---
layout: "image"
title: "Versteifungen"
date: "2014-02-26T08:17:48"
picture: "raupenbaggerunterwagen07.jpg"
weight: "14"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38385
imported:
- "2019"
_4images_image_id: "38385"
_4images_cat_id: "2854"
_4images_user_id: "1729"
_4images_image_date: "2014-02-26T08:17:48"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38385 -->
Um das Fahrwerk stabil zu bekommen, habe ich Achsen in die Bausteine eingeschoben.
Es sind immer 3 Stück 15er Bausteine, die eine zusammenhängende Nut bilden, in die eine Achse eingeschoben werden kann. Das ganze übelappt sich jeweils in der Unter- und in der Innenseite, so daß eine vernünftige Gesamtsteifigkeit entsteht, obwohl es keine durchgängige Achse gibt.
Es sind jeweils 30mm Achsen. Leider gibt es keine 45 mm Achsen, aber wahrscheinlich werde ich mir noch welche passgenau ablängen.
Außerdem haben die schwarzen Bausteine selbst eine sehr gute Klemmung (anders als manche Jahrgänge der grauen Bausteine).