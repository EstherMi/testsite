---
layout: "image"
title: "Ohne Verkabelung"
date: "2014-06-10T06:55:41"
picture: "OhneVerkabelung.jpg"
weight: "6"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: ["Raupenbagger", "Unterwagen", "Drehkranz", "Schleifring"]
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38927
imported:
- "2019"
_4images_image_id: "38927"
_4images_cat_id: "2854"
_4images_user_id: "1729"
_4images_image_date: "2014-06-10T06:55:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38927 -->
Ich muß den Unterwagen noch verkabeln. Die Stromversorgung/Ansteuerung erfolgt vom Oberwagen über diesen Schleifring. Der passt genau in den Drehkranz.
Je Adernpaar werden die Motoren rechts und links jeweils paarweise angesteuert und der Mini-Motor, um den Drehkranz zu drehen.
Damit kann man endlos in eine Richtung drehen, ohne daß sich die Kabel verdrillen.
Am Ende soll man fast nichts von den Kabeln sehen.