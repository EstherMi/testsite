---
layout: "image"
title: "Detail Kraftübertragung zur Kette"
date: "2014-02-21T20:18:35"
picture: "raupenbagger05.jpg"
weight: "7"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38320
imported:
- "2019"
_4images_image_id: "38320"
_4images_cat_id: "2851"
_4images_user_id: "1729"
_4images_image_date: "2014-02-21T20:18:35"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38320 -->
In dem Modell habe  ich zum ersten Mal Rast-Achsen eingesetzt, da man damit viel filigraner bauen kann als mit Metallachsen. Die Gesamtoptik hätte ich mit Metallachsen nicht hinbekommen. Vor allem die Kardangelenke und die Kegelzahnräder würden alles verderben!
Es werden hier auch nicht so hohe Kräfte übertragen, so daß Rastachsen ausreichend sind.