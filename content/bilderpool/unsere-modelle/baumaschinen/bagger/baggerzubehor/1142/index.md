---
layout: "image"
title: "FT-Baggergreifer"
date: "2003-05-30T16:33:33"
picture: "FT-bagger-Greifer.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/1142
imported:
- "2019"
_4images_image_id: "1142"
_4images_cat_id: "134"
_4images_user_id: "22"
_4images_image_date: "2003-05-30T16:33:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1142 -->
