---
layout: "image"
title: "Ausleger unten"
date: "2014-07-22T21:31:21"
picture: "schaufelradbagger11.jpg"
weight: "11"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- details/39035
imported:
- "2019"
_4images_image_id: "39035"
_4images_cat_id: "229"
_4images_user_id: "1635"
_4images_image_date: "2014-07-22T21:31:21"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39035 -->
Theoretisch geht er auch noch tiefer, aber der Fußboden war im weg ;-)