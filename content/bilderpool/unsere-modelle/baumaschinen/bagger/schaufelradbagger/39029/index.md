---
layout: "image"
title: "Rutsche"
date: "2014-07-22T21:31:21"
picture: "schaufelradbagger05.jpg"
weight: "5"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- details/39029
imported:
- "2019"
_4images_image_id: "39029"
_4images_cat_id: "229"
_4images_user_id: "1635"
_4images_image_date: "2014-07-22T21:31:21"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39029 -->
... aber trotzdem kommt irgendwann etwas Erde die Rutsche herunter.