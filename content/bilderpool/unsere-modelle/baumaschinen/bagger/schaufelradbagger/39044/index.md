---
layout: "image"
title: "Drehkranz-Antrieb"
date: "2014-07-22T21:31:21"
picture: "schaufelradbagger20.jpg"
weight: "20"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- details/39044
imported:
- "2019"
_4images_image_id: "39044"
_4images_cat_id: "229"
_4images_user_id: "1635"
_4images_image_date: "2014-07-22T21:31:21"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39044 -->
Der Minimotor reicht tatsächlich aus, um den gesamten Bagger (langsam) zu drehen.