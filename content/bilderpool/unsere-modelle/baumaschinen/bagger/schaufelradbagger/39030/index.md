---
layout: "image"
title: "Schaufelrad und Motor"
date: "2014-07-22T21:31:21"
picture: "schaufelradbagger06.jpg"
weight: "6"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- details/39030
imported:
- "2019"
_4images_image_id: "39030"
_4images_cat_id: "229"
_4images_user_id: "1635"
_4images_image_date: "2014-07-22T21:31:21"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39030 -->
