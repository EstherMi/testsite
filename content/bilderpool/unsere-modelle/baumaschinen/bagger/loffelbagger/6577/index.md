---
layout: "image"
title: "löffel 2"
date: "2006-06-24T19:38:11"
picture: "ft_lffelbagger_007.jpg"
weight: "4"
konstrukteure: 
- "trucker4164"
fotografen:
- "trucker4164"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "trucker4164"
license: "unknown"
legacy_id:
- details/6577
imported:
- "2019"
_4images_image_id: "6577"
_4images_cat_id: "567"
_4images_user_id: "368"
_4images_image_date: "2006-06-24T19:38:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6577 -->
