---
layout: "image"
title: "löffelbagger mit muldenkipper 2"
date: "2006-06-24T19:38:46"
picture: "ft_lffelbagger3_003.jpg"
weight: "6"
konstrukteure: 
- "trucker4164"
fotografen:
- "trucker4164"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "trucker4164"
license: "unknown"
legacy_id:
- details/6580
imported:
- "2019"
_4images_image_id: "6580"
_4images_cat_id: "567"
_4images_user_id: "368"
_4images_image_date: "2006-06-24T19:38:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6580 -->
