---
layout: "image"
title: "Kettenbagger"
date: "2011-07-14T12:04:42"
picture: "kettenbagger03.jpg"
weight: "3"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/31276
imported:
- "2019"
_4images_image_id: "31276"
_4images_cat_id: "2325"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T12:04:42"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31276 -->
Unten sind die Powermot.s zu sehen, die für den Antrieb zuständig sind. Sie treiben die Z30 Zahnräder mit einem direkt aufgesetzten Z10 Zahnrad an.