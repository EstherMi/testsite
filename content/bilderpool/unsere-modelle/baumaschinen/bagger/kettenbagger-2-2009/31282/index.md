---
layout: "image"
title: "Baggerarm"
date: "2011-07-14T12:04:42"
picture: "kettenbagger09.jpg"
weight: "9"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/31282
imported:
- "2019"
_4images_image_id: "31282"
_4images_cat_id: "2325"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T12:04:42"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31282 -->
Der Powermot, treibt über eine alte Kardanwelle die Schnecke für das Baggerarmgelek an. (beim Vorgänger war es noch ein Minimot.)