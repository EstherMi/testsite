---
layout: "image"
title: "Schaufelradbagger"
date: "2011-11-26T12:03:36"
picture: "schaufelradbagger14.jpg"
weight: "14"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/33558
imported:
- "2019"
_4images_image_id: "33558"
_4images_cat_id: "2181"
_4images_user_id: "968"
_4images_image_date: "2011-11-26T12:03:36"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33558 -->
Eines der fünf Hauptfahrwerke.

