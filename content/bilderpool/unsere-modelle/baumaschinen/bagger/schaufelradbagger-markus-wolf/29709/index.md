---
layout: "image"
title: "Schaufelradbagger"
date: "2011-01-17T21:34:57"
picture: "schaufelradbagger07.jpg"
weight: "27"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/29709
imported:
- "2019"
_4images_image_id: "29709"
_4images_cat_id: "2181"
_4images_user_id: "968"
_4images_image_date: "2011-01-17T21:34:57"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29709 -->
Das ganze Teil wiegt knappe 13 Kilo. Maßstäblich ist es nicht.Ich habe mich an den bekannten Vorbildern orientiert und das Teil im" FT Männlein" Maßstab gehalten.