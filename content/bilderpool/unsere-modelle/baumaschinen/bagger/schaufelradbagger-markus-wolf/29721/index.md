---
layout: "image"
title: "Seilspanner"
date: "2011-01-18T23:46:32"
picture: "schaufelradbagger1.jpg"
weight: "20"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/29721
imported:
- "2019"
_4images_image_id: "29721"
_4images_cat_id: "2181"
_4images_user_id: "968"
_4images_image_date: "2011-01-18T23:46:32"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29721 -->
Hier ein Detailbild vom Seilspanner. Unten sind die Achsen mit etwas Tesa in den Baustein gesteckt.das obere Teil mit der Rolle kann sich bewegen.Das Gummiband dazwischen ist mit Federnocken befestigt.
Das funktioniert ganz gut. Besser wäre.das Seil würde sauber auf die Trommel laufen.Das ist aber technisch sehr aufwändig  und die Mittel dazu habe ich schlicht nicht.