---
layout: "image"
title: "Schaufelradbagger"
date: "2011-11-26T12:03:36"
picture: "schaufelradbagger07_2.jpg"
weight: "7"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/33551
imported:
- "2019"
_4images_image_id: "33551"
_4images_cat_id: "2181"
_4images_user_id: "968"
_4images_image_date: "2011-11-26T12:03:36"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33551 -->
Hier die Stelle wo das Material aus dem Bagger auf die
Baggerbrücke beförtdert wird .