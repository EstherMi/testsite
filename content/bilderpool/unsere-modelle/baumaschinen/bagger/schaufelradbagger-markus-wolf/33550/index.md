---
layout: "image"
title: "Schaufelradbagger"
date: "2011-11-26T12:03:36"
picture: "schaufelradbagger06_2.jpg"
weight: "6"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/33550
imported:
- "2019"
_4images_image_id: "33550"
_4images_cat_id: "2181"
_4images_user_id: "968"
_4images_image_date: "2011-11-26T12:03:36"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33550 -->
In der Mitte unterm Powermotor befindet sich der Trichter.