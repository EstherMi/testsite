---
layout: "image"
title: "Schaufelradbagger"
date: "2011-01-17T21:34:57"
picture: "schaufelradbagger10.jpg"
weight: "30"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/29712
imported:
- "2019"
_4images_image_id: "29712"
_4images_cat_id: "2181"
_4images_user_id: "968"
_4images_image_date: "2011-01-17T21:34:57"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29712 -->
Hier einer von zwei Hilfskränen im abgebauten Zustand. Hierzu habe ich mal eine Frage,da ich in elektrischen Dingen nicht so bewandert bin. Ich möchte den Ausleger mit einem Endabschalter für beide Positionen haben. Taster habe ich dafür vorgesehen.....finde aber ums verrecken die Schaltung nicht raus . Kann mir da wer weiterhelfen?

Danke und Gruß