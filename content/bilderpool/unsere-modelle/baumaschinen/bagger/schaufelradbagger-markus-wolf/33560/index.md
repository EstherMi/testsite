---
layout: "image"
title: "Schaufelradbagger"
date: "2011-11-26T12:03:36"
picture: "schaufelradbagger16.jpg"
weight: "16"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/33560
imported:
- "2019"
_4images_image_id: "33560"
_4images_cat_id: "2181"
_4images_user_id: "968"
_4images_image_date: "2011-11-26T12:03:36"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33560 -->
Hier eins von fünf Förderbändern von unten gesehen.
Dieses hier saß unterm Trichter und hat einwandfrei funktioniert.
Das Band ist ein dünnes Gummituch. Die gummierte Seit läuft innen
Der Antrieb erfolgt über eine mit Schleifpapier beklebte Rolle.
