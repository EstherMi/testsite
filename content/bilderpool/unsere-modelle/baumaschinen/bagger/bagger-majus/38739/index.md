---
layout: "image"
title: "Bagger von unten"
date: "2014-05-02T16:40:08"
picture: "Bagger_2.jpg"
weight: "2"
konstrukteure: 
- "majus"
fotografen:
- "majus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "majus"
license: "unknown"
legacy_id:
- details/38739
imported:
- "2019"
_4images_image_id: "38739"
_4images_cat_id: "2893"
_4images_user_id: "1239"
_4images_image_date: "2014-05-02T16:40:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38739 -->
Wie schon im Bild vorher beschrieben sieht man hier den Antriebsstang des Baggers für die Vorwärts und Rückwärtsbewegung. Den Antrieb bilden zwei Power-Motoren.