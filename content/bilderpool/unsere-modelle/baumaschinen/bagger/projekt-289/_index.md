---
layout: "overview"
title: "Projekt 289"
date: 2019-12-17T19:14:26+01:00
legacy_id:
- categories/2946
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2946 --> 
Ein Kindheitstraum wird wahr... ich plane und baue ein Großgerät der RWE-Power im Maßstab 1:40. Es soll der 289er sein, weil er prima in das FT-Raster passt. Ich werde einige Jahre daran arbeiten und ab und an darüber berichten...