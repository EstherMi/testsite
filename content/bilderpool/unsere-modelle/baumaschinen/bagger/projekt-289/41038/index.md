---
layout: "image"
title: "Ballastausleger nach langer Pause"
date: "2015-05-26T14:41:38"
picture: "WP_20141101_001.jpg"
weight: "4"
konstrukteure: 
- "Dietmar Krauss"
fotografen:
- "Dietmar Krauss"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rivarama1968"
license: "unknown"
legacy_id:
- details/41038
imported:
- "2019"
_4images_image_id: "41038"
_4images_cat_id: "2946"
_4images_user_id: "2231"
_4images_image_date: "2015-05-26T14:41:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41038 -->
Ballastausleger von oben (noch liegend im Bau)