---
layout: "image"
title: "E-Halle von unten"
date: "2015-05-26T14:41:38"
picture: "WP_20141130_010.jpg"
weight: "6"
konstrukteure: 
- "Dietmar Krauss"
fotografen:
- "Dietmar Krauss"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rivarama1968"
license: "unknown"
legacy_id:
- details/41040
imported:
- "2019"
_4images_image_id: "41040"
_4images_cat_id: "2946"
_4images_user_id: "2231"
_4images_image_date: "2015-05-26T14:41:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41040 -->
EXTREM schwer zu bauen...Flachstücke mit S Dübel auf Aluprofil bekommen bei einer Länge von 65 cm.