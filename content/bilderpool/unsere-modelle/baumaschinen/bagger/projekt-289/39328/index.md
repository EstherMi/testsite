---
layout: "image"
title: "289.1"
date: "2014-09-04T11:04:21"
picture: "test.jpg"
weight: "10"
konstrukteure: 
- "Dietmar Krauss"
fotografen:
- "Dietmar Krauss"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rivarama1968"
license: "unknown"
legacy_id:
- details/39328
imported:
- "2019"
_4images_image_id: "39328"
_4images_cat_id: "2946"
_4images_user_id: "2231"
_4images_image_date: "2014-09-04T11:04:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39328 -->
Der Ballastausleger entsteht gerade - es ist also nur der Anfang. Hier ein Knotenpunkt.