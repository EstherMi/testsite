---
layout: "image"
title: "bagger-detail-8"
date: "2003-05-14T17:11:57"
picture: "FT-bagger-8.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/1125
imported:
- "2019"
_4images_image_id: "1125"
_4images_cat_id: "115"
_4images_user_id: "22"
_4images_image_date: "2003-05-14T17:11:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1125 -->
