---
layout: "image"
title: "Bagger komplett (Version 2)"
date: "2010-06-06T21:36:58"
picture: "baggerfishv2.jpg"
weight: "2"
konstrukteure: 
- "fish"
fotografen:
- "fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- details/27401
imported:
- "2019"
_4images_image_id: "27401"
_4images_cat_id: "1952"
_4images_user_id: "1113"
_4images_image_date: "2010-06-06T21:36:58"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27401 -->
Der ganze Bagger in neuer Version mit Ketten.