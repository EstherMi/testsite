---
layout: "overview"
title: "Baggerroboter (fish"
date: 2019-12-17T19:13:46+01:00
legacy_id:
- categories/1952
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1952 --> 
Pneumatischer Baggerroboter mit vier Zylindern, drei Magnetventielen, zwei Kompressoren, zwei Lampen, einem Farbsensor, einem Fahrgestell und einem Robo Interface.