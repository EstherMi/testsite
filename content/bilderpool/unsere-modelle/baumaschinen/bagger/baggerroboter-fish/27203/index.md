---
layout: "image"
title: "Bagger vorne (Version 1)"
date: "2010-05-08T20:05:20"
picture: "bagger2.jpg"
weight: "8"
konstrukteure: 
- "fish"
fotografen:
- "fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- details/27203
imported:
- "2019"
_4images_image_id: "27203"
_4images_cat_id: "1952"
_4images_user_id: "1113"
_4images_image_date: "2010-05-08T20:05:20"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27203 -->
Man sieht im Vordergrund die Schaufel, die über Zylinder bewegt wird, weiter hinten kann man das rote Lämpchen des Farbsensors erkennen.