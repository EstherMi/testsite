---
layout: "image"
title: "Kompressor"
date: "2010-06-06T21:36:59"
picture: "baggerfishv6.jpg"
weight: "6"
konstrukteure: 
- "fish"
fotografen:
- "fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- details/27405
imported:
- "2019"
_4images_image_id: "27405"
_4images_cat_id: "1952"
_4images_user_id: "1113"
_4images_image_date: "2010-06-06T21:36:59"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27405 -->
Hier ist der Kompressor mit dem Lufttank.