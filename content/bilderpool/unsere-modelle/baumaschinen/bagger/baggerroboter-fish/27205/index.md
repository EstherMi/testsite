---
layout: "image"
title: "Ventile"
date: "2010-05-08T20:05:20"
picture: "bagger4.jpg"
weight: "10"
konstrukteure: 
- "fish"
fotografen:
- "fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- details/27205
imported:
- "2019"
_4images_image_id: "27205"
_4images_cat_id: "1952"
_4images_user_id: "1113"
_4images_image_date: "2010-05-08T20:05:20"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27205 -->
Das Bild zeigt die drei Ventile zur Steuerung des Baggerarms. Leider sind meine FT Akkus schon älter, sodass der Strom zur Ansteuerung der Ventile oft nicht reicht. Ich muss immer ein Netzteil verwenden