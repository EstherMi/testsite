---
layout: "image"
title: "Befestigung XS-Motor"
date: "2013-07-14T15:29:48"
picture: "schaufelradbagger1.jpg"
weight: "2"
konstrukteure: 
- "sven"
fotografen:
- "sven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/37162
imported:
- "2019"
_4images_image_id: "37162"
_4images_cat_id: "2758"
_4images_user_id: "1"
_4images_image_date: "2013-07-14T15:29:48"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37162 -->
Nach dem ich den Schaufelradbagger aus Power Machines voll motorisiert aufgebaut hatte musste ich leider feststellen das es Problem mit den Antrieb der Drehscheibe gibt,.
Die Schnecke drückte sich immer von der Drehscheibe weg, wenn man den Motor so wie in der Anleitung beschrieben anbaut.
Ich habe dann die Befestigung des XS Motors abgeändert und schon dreht sich der Bagger absolut sauber.
