---
layout: "image"
title: "Power Machines - Schaufelradbagger"
date: "2013-07-16T21:24:15"
picture: "schaufelrad1.jpg"
weight: "1"
konstrukteure: 
- "Martin W. (Masked)"
fotografen:
- "Martin W. (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/37165
imported:
- "2019"
_4images_image_id: "37165"
_4images_cat_id: "2758"
_4images_user_id: "373"
_4images_image_date: "2013-07-16T21:24:15"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37165 -->
Wie man sieht, ungefähr Tisch-hoch (80cm) und knapp 140cm lang. Wiegt knapp 5kg.
Das Bild ist nur schnell mit dem Handy geknipst, also bitte die Qualität entschuldigen.