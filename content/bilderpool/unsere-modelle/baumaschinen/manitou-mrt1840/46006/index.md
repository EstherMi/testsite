---
layout: "image"
title: "Manitou MRT1840"
date: "2017-07-03T17:16:44"
picture: "IMG_0908.jpg"
weight: "9"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/46006
imported:
- "2019"
_4images_image_id: "46006"
_4images_cat_id: "3420"
_4images_user_id: "838"
_4images_image_date: "2017-07-03T17:16:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46006 -->
4x powermotor aandrijving
2x powermotor besturing (vooras achteras)
2x powermotor steunpoten
1x powermotor voor de draaikrans
2x powermotor voor het heffen van de mast
2x powermotor voor het uitschuiven van de mast
1x eigengemaakte cilinder voor het neigen van het vorkenbord.