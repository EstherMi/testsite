---
layout: "image"
title: "Manitou MRT1840"
date: "2017-07-03T17:16:44"
picture: "IMG_0917.jpg"
weight: "16"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/46013
imported:
- "2019"
_4images_image_id: "46013"
_4images_cat_id: "3420"
_4images_user_id: "838"
_4images_image_date: "2017-07-03T17:16:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46013 -->
De draaikrans is een groot rollager. En alles is uit elkaar te halen d.m.v.grote componenten.