---
layout: "image"
title: "Manitou MRT1840"
date: "2017-07-03T17:16:44"
picture: "IMG_0900.jpg"
weight: "2"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/45999
imported:
- "2019"
_4images_image_id: "45999"
_4images_cat_id: "3420"
_4images_user_id: "838"
_4images_image_date: "2017-07-03T17:16:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45999 -->
Vooraanzicht met de giek omhoog.