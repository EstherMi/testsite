---
layout: "image"
title: "aandrijving rechts"
date: "2010-02-19T21:58:18"
picture: "P2190234.jpg"
weight: "12"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/26480
imported:
- "2019"
_4images_image_id: "26480"
_4images_cat_id: "1883"
_4images_user_id: "838"
_4images_image_date: "2010-02-19T21:58:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26480 -->
deze kant voorzien van 4 tandwielen tussen diffs