---
layout: "image"
title: "Caterpillar D11R Schaufel"
date: "2006-04-27T13:15:47"
picture: "11_Caterpillar_D11R_Schaufel.jpg"
weight: "11"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/6152
imported:
- "2019"
_4images_image_id: "6152"
_4images_cat_id: "530"
_4images_user_id: "162"
_4images_image_date: "2006-04-27T13:15:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6152 -->
