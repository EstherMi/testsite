---
layout: "image"
title: "Caterpillar D11R Vorne"
date: "2006-04-27T13:15:36"
picture: "01_Caterpillar_D11R_vorne.jpg"
weight: "1"
konstrukteure: 
- "Arjen Neijsen"
fotografen:
- "Arjen Neijsen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/6142
imported:
- "2019"
_4images_image_id: "6142"
_4images_cat_id: "530"
_4images_user_id: "162"
_4images_image_date: "2006-04-27T13:15:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6142 -->
