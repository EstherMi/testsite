---
layout: "image"
title: "Caterpillar D11R Seite"
date: "2006-04-27T13:15:36"
picture: "02_Caterpillar_D11R_seite.jpg"
weight: "2"
konstrukteure: 
- "Arjen Neijsen (jmn)"
fotografen:
- "Arjen Neijsen (jmn)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/6143
imported:
- "2019"
_4images_image_id: "6143"
_4images_cat_id: "530"
_4images_user_id: "162"
_4images_image_date: "2006-04-27T13:15:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6143 -->
Diese Modell ist ausgestattet mit:
2 M-motoren (antrieb Ketten)
5 S-Motoren (3 Hydraulik und 2 fur den Schaufel).
Insgesammt gibt es 10 Hydraulik Zylinder drin.
Lange = 57 cm
Breite = 33 cm
Hohe = 28 cm