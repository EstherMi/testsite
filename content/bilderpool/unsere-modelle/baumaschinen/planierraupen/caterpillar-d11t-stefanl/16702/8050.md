---
layout: "comment"
hidden: true
title: "8050"
date: "2008-12-24T11:17:11"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
@StefanL
Hallo Stefan,
wieder ein tolles Modell von dir und mit vielen Funktionen. Ein solches Modell verdient aber bessere Fotos. Ich habe deine Fotos mal runtergeladen und im SF tfC Publisher nachbearbeitet. Wenn du interessiert bist, würde ich sie mal mit den Werten der Nachbearbeitung in die ftC hochladen. Das könnte für mit einer solchen Nachbearbeitung Ungeübte auch eine Hilfe sein.
Gruß, Udo2