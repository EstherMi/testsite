---
layout: "image"
title: "Caterpillar D11T"
date: "2008-12-23T15:26:44"
picture: "caterpillardt3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/16704
imported:
- "2019"
_4images_image_id: "16704"
_4images_cat_id: "1512"
_4images_user_id: "502"
_4images_image_date: "2008-12-23T15:26:44"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16704 -->
