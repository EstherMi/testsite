---
layout: "image"
title: "Raupe seitlich"
date: "2009-01-22T21:34:22"
picture: "raupemitkran6.jpg"
weight: "6"
konstrukteure: 
- "sebastian g."
fotografen:
- "sebastian g."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sebastian g."
license: "unknown"
legacy_id:
- details/17127
imported:
- "2019"
_4images_image_id: "17127"
_4images_cat_id: "1537"
_4images_user_id: "904"
_4images_image_date: "2009-01-22T21:34:22"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17127 -->
Das ist die komplett zusammengebaute Raupe von der Seite!