---
layout: "image"
title: "Raupenhälfte seitlich"
date: "2009-01-22T21:34:22"
picture: "raupemitkran3.jpg"
weight: "3"
konstrukteure: 
- "sebastian g."
fotografen:
- "sebastian g."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sebastian g."
license: "unknown"
legacy_id:
- details/17124
imported:
- "2019"
_4images_image_id: "17124"
_4images_cat_id: "1537"
_4images_user_id: "904"
_4images_image_date: "2009-01-22T21:34:22"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17124 -->
Die Raupe besteht aus 2 dieser Hälften (eine dvon spiegelverkehrt)!