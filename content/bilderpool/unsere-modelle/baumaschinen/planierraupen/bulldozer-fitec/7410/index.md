---
layout: "image"
title: "Rückseite Detailansicht"
date: "2006-11-05T14:39:25"
picture: "Bulldozer8.jpg"
weight: "8"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/7410
imported:
- "2019"
_4images_image_id: "7410"
_4images_cat_id: "700"
_4images_user_id: "456"
_4images_image_date: "2006-11-05T14:39:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7410 -->
Hier sieht man die Rückseite im Detail.
Eine durchgehende Achs wäre am stabilsten gewesen, aber weil man dann nicht mehr lenken kann hab ich ein Differenzial genommen.