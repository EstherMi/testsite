---
layout: "image"
title: "Seitenansicht"
date: "2006-11-05T14:39:25"
picture: "Bulldozer3.jpg"
weight: "3"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/7405
imported:
- "2019"
_4images_image_id: "7405"
_4images_cat_id: "700"
_4images_user_id: "456"
_4images_image_date: "2006-11-05T14:39:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7405 -->
Hier sieht man den Bulldozer von der Seite.