---
layout: "image"
title: "grosserbulldozer49.jpg"
date: "2011-07-13T14:32:41"
picture: "grosserbulldozer49.jpg"
weight: "49"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/31097
imported:
- "2019"
_4images_image_id: "31097"
_4images_cat_id: "2319"
_4images_user_id: "833"
_4images_image_date: "2011-07-13T14:32:41"
_4images_image_order: "49"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31097 -->
von vorne, ohne Planierschild