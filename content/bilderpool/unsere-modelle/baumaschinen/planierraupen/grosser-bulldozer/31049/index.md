---
layout: "image"
title: "grosserbulldozer01.jpg"
date: "2011-07-13T14:32:41"
picture: "grosserbulldozer01.jpg"
weight: "1"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/31049
imported:
- "2019"
_4images_image_id: "31049"
_4images_cat_id: "2319"
_4images_user_id: "833"
_4images_image_date: "2011-07-13T14:32:41"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31049 -->
Bulldozer im Einsatz