---
layout: "image"
title: "Schaufel3"
date: "2006-05-07T15:16:34"
picture: "IMG_4575.jpg"
weight: "6"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/6239
imported:
- "2019"
_4images_image_id: "6239"
_4images_cat_id: "543"
_4images_user_id: "389"
_4images_image_date: "2006-05-07T15:16:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6239 -->
