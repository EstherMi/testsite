---
layout: "image"
title: "Fischertechnik Trilblok met instelbaar variabel excentrisch moment."
date: "2009-08-30T09:28:51"
picture: "fttrilblokmetinstelbaarvariabelexcentrischmoment05.jpg"
weight: "5"
konstrukteure: 
- "Peter Damen"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/24853
imported:
- "2019"
_4images_image_id: "24853"
_4images_cat_id: "1708"
_4images_user_id: "22"
_4images_image_date: "2009-08-30T09:28:51"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24853 -->
Beim Deicherhöhung mussen entlang der Niederrijn / Lek sehr viele Spundwande genutzt werden wegen die viele Gebäude. 

Hochfrequenzrüttler mit variablem Moment 
sollen angewendet werden. 

Prinzip :
Bilder die dass Funktionieren zeigen gibt es jetzt bei 
http://www.pve-holland.com/content/217/309/Technology/Vibratory-Hammers/Principle-of-a-vibratory-hammer.html 




Trilblok met instelbaar variabel excentrisch moment.

Kritische (eigen-) frequentie, ook bij het opstarten en stoppen, kunnen hiermee vermeden worden.

Voordelen: 

minder of geen bouwkundige schade + geringere werkafstanden tot belendingen zijn mogelijk