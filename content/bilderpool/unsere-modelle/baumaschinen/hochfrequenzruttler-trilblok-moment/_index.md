---
layout: "overview"
title: "Hochfrequenzrüttler ('Trilblok') mit einstellbarem Moment"
date: 2019-12-17T19:15:41+01:00
legacy_id:
- categories/1708
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1708 --> 
Trilblok met instelbaar variabel excentrisch moment.[br][br]Kritische (eigen-) frequentie, ook bij het opstarten en stoppen, kunnen hiermee vermeden worden.[br][br]Voordelen: [br][br]minder of geen bouwkundige schade + geringere werkafstanden tot belendingen zijn mogelijk