---
layout: "image"
title: "Raupenbagger III"
date: "2014-07-29T07:12:28"
picture: "raupenbaggerversion3.jpg"
weight: "3"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/39096
imported:
- "2019"
_4images_image_id: "39096"
_4images_cat_id: "2922"
_4images_user_id: "1729"
_4images_image_date: "2014-07-29T07:12:28"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39096 -->
