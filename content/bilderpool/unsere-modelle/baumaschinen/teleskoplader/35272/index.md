---
layout: "image"
title: "Teleskoplader 002"
date: "2012-08-10T17:56:37"
picture: "teleskoplader02.jpg"
weight: "20"
konstrukteure: 
- "ft-sigi"
fotografen:
- "ft-sigi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-sigi"
license: "unknown"
legacy_id:
- details/35272
imported:
- "2019"
_4images_image_id: "35272"
_4images_cat_id: "2615"
_4images_user_id: "1536"
_4images_image_date: "2012-08-10T17:56:37"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35272 -->
Der Teleskoplader wird über die beiden IR-Empfänger (Code I + II) gesteuert.

In diesem Modell eingebaut sind:

-1x Mini-Motor
-1x Power-Motor
-1x XM-Motor
-2x XS-Motor
-5x Lampen
-1x Blinkelektronik
-2x IR-Empfänger
-1x Akku
- und viele Stunden Arbeit