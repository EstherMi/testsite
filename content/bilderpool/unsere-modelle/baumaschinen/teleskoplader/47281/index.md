---
layout: "image"
title: "'Gerippe'"
date: "2018-02-14T16:38:04"
picture: "telbly13.jpg"
weight: "13"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/47281
imported:
- "2019"
_4images_image_id: "47281"
_4images_cat_id: "2615"
_4images_user_id: "2228"
_4images_image_date: "2018-02-14T16:38:04"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47281 -->
Fahrzeug ohne Räder, Teleskopausleger und Elektronik