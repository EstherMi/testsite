---
layout: "image"
title: "Teleskoplader 014"
date: "2012-08-10T17:56:38"
picture: "teleskoplader14.jpg"
weight: "32"
konstrukteure: 
- "ft-sigi"
fotografen:
- "ft-sigi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-sigi"
license: "unknown"
legacy_id:
- details/35284
imported:
- "2019"
_4images_image_id: "35284"
_4images_cat_id: "2615"
_4images_user_id: "1536"
_4images_image_date: "2012-08-10T17:56:38"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35284 -->
5 Schneckenteile bewegen die Statikstreben und heben so den Teleskoparm.