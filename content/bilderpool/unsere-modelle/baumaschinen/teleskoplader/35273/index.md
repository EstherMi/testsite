---
layout: "image"
title: "Telskoplader 003"
date: "2012-08-10T17:56:37"
picture: "teleskoplader03.jpg"
weight: "21"
konstrukteure: 
- "ft-sigi"
fotografen:
- "ft-sigi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-sigi"
license: "unknown"
legacy_id:
- details/35273
imported:
- "2019"
_4images_image_id: "35273"
_4images_cat_id: "2615"
_4images_user_id: "1536"
_4images_image_date: "2012-08-10T17:56:37"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35273 -->
Ansicht von Vorne