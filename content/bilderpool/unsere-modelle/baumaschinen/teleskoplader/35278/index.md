---
layout: "image"
title: "Teleskoplader 008"
date: "2012-08-10T17:56:37"
picture: "teleskoplader08.jpg"
weight: "26"
konstrukteure: 
- "ft-sigi"
fotografen:
- "ft-sigi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-sigi"
license: "unknown"
legacy_id:
- details/35278
imported:
- "2019"
_4images_image_id: "35278"
_4images_cat_id: "2615"
_4images_user_id: "1536"
_4images_image_date: "2012-08-10T17:56:37"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35278 -->
Lenkung:

Im Z40 steckt eine Rastachse, welche in die Statikstrebe greift. 
Diese Konstruktion war nötig, damit das ganze Fahrzeug vorne nicht zu hoch wurde
und der Teleskoparm gut aufliegt...