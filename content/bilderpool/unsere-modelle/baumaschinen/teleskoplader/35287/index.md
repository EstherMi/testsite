---
layout: "image"
title: "Teleskoplader 017"
date: "2012-08-10T17:56:38"
picture: "teleskoplader17.jpg"
weight: "35"
konstrukteure: 
- "ft-sigi"
fotografen:
- "ft-sigi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-sigi"
license: "unknown"
legacy_id:
- details/35287
imported:
- "2019"
_4images_image_id: "35287"
_4images_cat_id: "2615"
_4images_user_id: "1536"
_4images_image_date: "2012-08-10T17:56:38"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35287 -->
So ist der Teleskoparm ganz hochgefahren...