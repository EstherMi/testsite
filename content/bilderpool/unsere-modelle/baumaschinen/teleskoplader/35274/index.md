---
layout: "image"
title: "Teleskoplader 004"
date: "2012-08-10T17:56:37"
picture: "teleskoplader04.jpg"
weight: "22"
konstrukteure: 
- "ft-sigi"
fotografen:
- "ft-sigi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-sigi"
license: "unknown"
legacy_id:
- details/35274
imported:
- "2019"
_4images_image_id: "35274"
_4images_cat_id: "2615"
_4images_user_id: "1536"
_4images_image_date: "2012-08-10T17:56:37"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35274 -->
Ansicht von Hinten