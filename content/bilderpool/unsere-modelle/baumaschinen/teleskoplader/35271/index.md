---
layout: "image"
title: "Teleskoplader 001"
date: "2012-08-10T17:56:37"
picture: "teleskoplader01.jpg"
weight: "19"
konstrukteure: 
- "ft-sigi"
fotografen:
- "ft-sigi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-sigi"
license: "unknown"
legacy_id:
- details/35271
imported:
- "2019"
_4images_image_id: "35271"
_4images_cat_id: "2615"
_4images_user_id: "1536"
_4images_image_date: "2012-08-10T17:56:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35271 -->
Mein Ziel bei diesem Projekt war, alle Funktionen wie in der Wirklichkeit einzubauen.
Dieses Modell hat folgende Funktionen:

- Antrieb der Hinterachse
- Lenkung der Vorderachse (Leider nicht ganz originalgetreu, da die meisten Teleskoplader auch die Hinterachse lenken können)
- Frontscheinwerfer
- Rücklichter
- Blinklicht auf dem Kabinendach
- Heb- und ausfahrbarer Teleskoparm
- Kippmechanik für Anschlussgeräte


Etwas später werde ich noch einen Arbeitsscheinwerfer und wenn der Platz reicht auch noch einen Kompressor einbauen.