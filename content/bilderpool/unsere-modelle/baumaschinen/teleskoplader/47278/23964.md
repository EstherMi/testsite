---
layout: "comment"
hidden: true
title: "23964"
date: "2018-02-15T12:26:27"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Hallo Stefan,

Die Kraft der Pneumatik reicht gerade aus, um den Teleskoparm (eingefahren) mit Gabel anzuheben. Sie reicht außerdem, um den ausgefahrenen Teleskoparm oben zu halten.

Auf den Fotos ist die erste Version des Teleskoparms zu sehen: Ein Verbinder 30 in den Bauplatten 90x30 dient als Führung, man kann den Teleskoparm von Hand ausziehen. In der zweiten Version lässt er sich elektrisch ausfahren: https://ftcommunity.de/details.php?image_id=44069
Da wird das Gewicht des Auslegers aber kritisch für die Pneumatikzylinder.

Gruß,
David