---
layout: "image"
title: "Teleskoplader 019"
date: "2012-08-10T17:56:38"
picture: "teleskoplader19.jpg"
weight: "37"
konstrukteure: 
- "ft-sigi"
fotografen:
- "ft-sigi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-sigi"
license: "unknown"
legacy_id:
- details/35289
imported:
- "2019"
_4images_image_id: "35289"
_4images_cat_id: "2615"
_4images_user_id: "1536"
_4images_image_date: "2012-08-10T17:56:38"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35289 -->
Hier erkennt man die Teleskopmechanik!!!
Der Arm wird über ein Hubgetriebe und 4 Zahnstangen ausgefahren.