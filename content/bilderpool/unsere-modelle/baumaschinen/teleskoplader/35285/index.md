---
layout: "image"
title: "Teleskoplader 015"
date: "2012-08-10T17:56:38"
picture: "teleskoplader15.jpg"
weight: "33"
konstrukteure: 
- "ft-sigi"
fotografen:
- "ft-sigi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-sigi"
license: "unknown"
legacy_id:
- details/35285
imported:
- "2019"
_4images_image_id: "35285"
_4images_cat_id: "2615"
_4images_user_id: "1536"
_4images_image_date: "2012-08-10T17:56:38"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35285 -->
Weil die Statikstreben beim Hochfahren des Arms stark belastet
werden, habe ich sie oben doppelt verstärkt.