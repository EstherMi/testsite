---
layout: "image"
title: "Teleskoplader 009"
date: "2012-08-10T17:56:38"
picture: "teleskoplader09.jpg"
weight: "27"
konstrukteure: 
- "ft-sigi"
fotografen:
- "ft-sigi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-sigi"
license: "unknown"
legacy_id:
- details/35279
imported:
- "2019"
_4images_image_id: "35279"
_4images_cat_id: "2615"
_4images_user_id: "1536"
_4images_image_date: "2012-08-10T17:56:38"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35279 -->
Oben im Bild sieht man einen Mini-Mot und sein Getriebe.
er treibt über 3 Zahnräder die Lenkung an.

