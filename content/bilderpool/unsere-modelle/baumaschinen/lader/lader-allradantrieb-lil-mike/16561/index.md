---
layout: "image"
title: "teleskoplader"
date: "2008-12-07T01:33:30"
picture: "DSC01018.jpg"
weight: "2"
konstrukteure: 
- "lil mike"
fotografen:
- "lil mike"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lil-mike"
license: "unknown"
legacy_id:
- details/16561
imported:
- "2019"
_4images_image_id: "16561"
_4images_cat_id: "1554"
_4images_user_id: "822"
_4images_image_date: "2008-12-07T01:33:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16561 -->
