---
layout: "image"
title: "Radlader-2"
date: "2003-08-04T09:17:36"
picture: "radl2.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thkais"
license: "unknown"
legacy_id:
- details/1311
imported:
- "2019"
_4images_image_id: "1311"
_4images_cat_id: "190"
_4images_user_id: "41"
_4images_image_date: "2003-08-04T09:17:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1311 -->
Features:
- Große Conrad-Reifen (s. Forum)
- Allradantrieb
- Hinterachse als Pendelachse ausgeführt
- Knicklenkung
- alle Funktionen motorisiert
geplant: Ferngesteuert.