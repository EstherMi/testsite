---
layout: "comment"
hidden: true
title: "4235"
date: "2007-10-06T13:02:24"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Sehr hübsch, das Teil!

Deine Massivbauweise (ich nenn's mal so) beweist, dass auch bei ft jeder seine eigene "Handschrift" hat. Ich hätte auch ohne hinzusehen auf dich als den Konstrukteur getippt :-)

Gruß,
Harald