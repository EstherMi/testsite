---
layout: "image"
title: "Umbau"
date: "2007-10-05T22:34:52"
picture: "DSCN1654.jpg"
weight: "5"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/12138
imported:
- "2019"
_4images_image_id: "12138"
_4images_cat_id: "1079"
_4images_user_id: "184"
_4images_image_date: "2007-10-05T22:34:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12138 -->
