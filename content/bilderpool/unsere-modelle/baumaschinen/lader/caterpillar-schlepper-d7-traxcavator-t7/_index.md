---
layout: "overview"
title: "Caterpillar Schlepper D7 mit Traxcavator Schaufel Modell T7"
date: 2019-12-17T19:10:10+01:00
legacy_id:
- categories/1079
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1079 --> 
Das Original wurde von Caterpillar 1937 so gebaut und war eine Vorstufe der heutigen Hydraulik Schlepper.