---
layout: "image"
title: "Modell"
date: "2007-10-02T08:32:54"
picture: "DSCN1631.jpg"
weight: "20"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/12069
imported:
- "2019"
_4images_image_id: "12069"
_4images_cat_id: "1079"
_4images_user_id: "184"
_4images_image_date: "2007-10-02T08:32:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12069 -->
Seitenansicht