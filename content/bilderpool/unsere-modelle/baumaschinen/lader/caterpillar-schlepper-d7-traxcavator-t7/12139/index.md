---
layout: "image"
title: "fast fertig"
date: "2007-10-05T22:34:52"
picture: "DSCN1662.jpg"
weight: "6"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/12139
imported:
- "2019"
_4images_image_id: "12139"
_4images_cat_id: "1079"
_4images_user_id: "184"
_4images_image_date: "2007-10-05T22:34:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12139 -->
Nur die Kabelfersteuerung fehlt noch. Und natürlich ein paar Schönheitskorrekturen.