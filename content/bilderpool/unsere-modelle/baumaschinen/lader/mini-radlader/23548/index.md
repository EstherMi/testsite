---
layout: "image"
title: "Mini-Radlader 6"
date: "2009-03-29T19:07:18"
picture: "Radlader_06.jpg"
weight: "6"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/23548
imported:
- "2019"
_4images_image_id: "23548"
_4images_cat_id: "1608"
_4images_user_id: "328"
_4images_image_date: "2009-03-29T19:07:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23548 -->
Diese Ansicht finde ich besonders gelungen... ;o)