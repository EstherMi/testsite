---
layout: "image"
title: "Mini-Radlader 4"
date: "2009-03-29T19:07:18"
picture: "Radlader_04.jpg"
weight: "4"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/23546
imported:
- "2019"
_4images_image_id: "23546"
_4images_cat_id: "1608"
_4images_user_id: "328"
_4images_image_date: "2009-03-29T19:07:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23546 -->
Ansicht von hinten. Akku-Pack, Kompressor und Empfänger bilden eine kompakte Einheit hinter dem Fahrerhaus. Unten im Bild die Lenkung der Hinterachse per Servo.