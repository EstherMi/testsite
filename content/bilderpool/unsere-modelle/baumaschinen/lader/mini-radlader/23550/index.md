---
layout: "image"
title: "Mini-Radlader 8"
date: "2009-03-29T19:07:18"
picture: "Radlader_08.jpg"
weight: "8"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/23550
imported:
- "2019"
_4images_image_id: "23550"
_4images_cat_id: "1608"
_4images_user_id: "328"
_4images_image_date: "2009-03-29T19:07:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23550 -->
Hier sind der "Starthebel" (rechts vom Sitz) und das rechte motorisierte Handventil gut zu erkennen.