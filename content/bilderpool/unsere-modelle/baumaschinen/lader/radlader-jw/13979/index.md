---
layout: "image"
title: "000677"
date: "2008-03-20T15:18:00"
picture: "BILD0677.jpg"
weight: "12"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- details/13979
imported:
- "2019"
_4images_image_id: "13979"
_4images_cat_id: "1105"
_4images_user_id: "107"
_4images_image_date: "2008-03-20T15:18:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13979 -->
Leider verzerrt das Weitwinkelobjektiv das ganze Modell etwas.
Im Original ist die Schaufel noch weiter nach unten kippbar.