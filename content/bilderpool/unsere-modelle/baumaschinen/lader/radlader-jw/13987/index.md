---
layout: "image"
title: "000702"
date: "2008-03-20T15:23:41"
picture: "BILD0702.jpg"
weight: "20"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- details/13987
imported:
- "2019"
_4images_image_id: "13987"
_4images_cat_id: "1105"
_4images_user_id: "107"
_4images_image_date: "2008-03-20T15:23:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13987 -->
Fast hätte ich die andere Seite vergessen. Der VOLVO hat nur eine Tür.