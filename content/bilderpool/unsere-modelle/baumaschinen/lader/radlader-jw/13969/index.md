---
layout: "image"
title: "000653"
date: "2008-03-20T14:53:54"
picture: "BILD0653.jpg"
weight: "2"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: ["Knicklenkung", "Allrad", "Vorgelege"]
uploadBy: "jw"
license: "unknown"
legacy_id:
- details/13969
imported:
- "2019"
_4images_image_id: "13969"
_4images_cat_id: "1105"
_4images_user_id: "107"
_4images_image_date: "2008-03-20T14:53:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13969 -->
