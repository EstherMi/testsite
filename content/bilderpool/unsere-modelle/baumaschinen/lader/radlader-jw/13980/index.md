---
layout: "image"
title: "000685"
date: "2008-03-20T15:18:00"
picture: "BILD0685.jpg"
weight: "13"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- details/13980
imported:
- "2019"
_4images_image_id: "13980"
_4images_cat_id: "1105"
_4images_user_id: "107"
_4images_image_date: "2008-03-20T15:18:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13980 -->
Die Hubmotore sind unabhängig voneinander und nicht drehbar gelagert. Sicherlich tauchen etwas Gleichlaufschwankungen auf. Diese werden in beiden Endanschlägen korrigiert