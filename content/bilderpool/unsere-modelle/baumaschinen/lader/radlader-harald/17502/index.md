---
layout: "image"
title: "ExplorerMk2-28.JPG"
date: "2009-02-24T11:24:47"
picture: "ExplorerMk2-28.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/17502
imported:
- "2019"
_4images_image_id: "17502"
_4images_cat_id: "1572"
_4images_user_id: "4"
_4images_image_date: "2009-02-24T11:24:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17502 -->
Die Mechanik zum Anheben der Schaufel beruht auf der Hubvorrichtung von hier:
http://www.ftcommunity.de/categories.php?cat_id=507 . Geändert ist die Zahnradbestückung, und die zweite Kette ist auch weg, dafür ist das hintere Z20 jetzt mittels Verbinder 30 blockiert.