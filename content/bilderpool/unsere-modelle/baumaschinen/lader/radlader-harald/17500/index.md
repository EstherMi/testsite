---
layout: "image"
title: "ExplorerMk2-24.JPG"
date: "2009-02-24T11:18:58"
picture: "ExplorerMk2-24.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Allrad", "Knicklenkung"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/17500
imported:
- "2019"
_4images_image_id: "17500"
_4images_cat_id: "1572"
_4images_user_id: "4"
_4images_image_date: "2009-02-24T11:18:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17500 -->
Der "Explorer" (von 
http://www.ftcommunity.de/categories.php?cat_id=1555 ) ist auf dem Weg, sich zum Radlader zu mausern.