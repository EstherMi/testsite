---
layout: "comment"
hidden: true
title: "8593"
date: "2009-02-25T15:54:35"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Hallo und danke Udo!

Du brauchst keine Befürchtungen zu haben, dass das Gleiche herauskommt, wenn zwei ft-Bastler eine Modellidee umsetzen. Es hat auch bei ft jeder seine eigene Handschrift. Das fängt bei der Teileauswahl an (mehr BS30 oder weniger davon) und geht über die Farben (grau-rot, schwarz-rot, mit oder ohne Schema dahinter) bis hin zum Gesamtaufbau (massiv und steif mit Winkelsteinen oder luftig-leicht mit S-Streben). Es kommen also mit ziemlicher Sicherheit bei zwei Tüftlern zweierlei Realisierungen heraus.

Ja mei, und vorher ein bisschen in der ftcommunity stöbern und Inspiration holen ist auch nicht verboten ;-)

Gruß,
Harald