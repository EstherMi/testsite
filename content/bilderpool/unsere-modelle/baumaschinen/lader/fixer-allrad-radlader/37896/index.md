---
layout: "image"
title: "Schaufel unten"
date: "2013-12-02T12:57:36"
picture: "fixerradlader2.jpg"
weight: "2"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- details/37896
imported:
- "2019"
_4images_image_id: "37896"
_4images_cat_id: "2817"
_4images_user_id: "1582"
_4images_image_date: "2013-12-02T12:57:36"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37896 -->
Die Schaufel in der unteren Position: der Mini-Mot hat genug Platz zwischen den beiden Motoren, die zusammen stark genug sind, die Schaufel sehr fix zu kippen. Man kann erahnen, wie die Querliegenden, großen M-Motoren über ein Ritzel und ein 30er-Zahnrad gleichermaßen vorne unten hinten die beiden Achsen antreiben. Sorry - der Kabelverhau und der mangelnde Aufbau ist unseren Kids geschuldet: da soll das Teil so schnell wie möglich einsatzbereit sein!