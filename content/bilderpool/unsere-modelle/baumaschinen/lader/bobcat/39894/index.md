---
layout: "image"
title: "Bobcat"
date: "2014-12-08T17:05:15"
picture: "PC080195.jpg"
weight: "1"
konstrukteure: 
- "chef8"
fotografen:
- "chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/39894
imported:
- "2019"
_4images_image_id: "39894"
_4images_cat_id: "2997"
_4images_user_id: "838"
_4images_image_date: "2014-12-08T17:05:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39894 -->
Mijn nieuwste model voorzien van:

4x rijmotor
2x hefmotor
1x neigmotor

en voorzien van parallel constructie die de bak verder naar voren brengt om te kunnen legen.