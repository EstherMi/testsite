---
layout: "image"
title: "Radlader 13"
date: "2007-07-15T17:49:00"
picture: "radladerstefanl12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/11095
imported:
- "2019"
_4images_image_id: "11095"
_4images_cat_id: "1005"
_4images_user_id: "502"
_4images_image_date: "2007-07-15T17:49:00"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11095 -->
