---
layout: "image"
title: "Radlader (Detail)"
date: "2007-01-24T20:37:35"
picture: "Radlader35b.jpg"
weight: "25"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- details/8652
imported:
- "2019"
_4images_image_id: "8652"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2007-01-24T20:37:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8652 -->
Detailaufnahme des hinteren Antiebs, Ersatz für das seltsame Gebilde, das vorher ganz hinten oben auf der Verstrebung saß