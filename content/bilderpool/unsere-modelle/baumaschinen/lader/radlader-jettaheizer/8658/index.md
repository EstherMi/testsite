---
layout: "image"
title: "Radlader (Detail)"
date: "2007-01-24T20:37:35"
picture: "Radlader41b.jpg"
weight: "31"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- details/8658
imported:
- "2019"
_4images_image_id: "8658"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2007-01-24T20:37:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8658 -->
Gleiche Stelle, andere Perspektive. Die Axiallager habe ich erstmal wieder rausgeworfen, die haben mehr Reibung verursacht als verhindert