---
layout: "image"
title: "Pendelhinterachse (Detail)"
date: "2006-12-19T21:20:02"
picture: "Radlader17b.jpg"
weight: "53"
konstrukteure: 
- "Franz Osten (Jettaheizer"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- details/7959
imported:
- "2019"
_4images_image_id: "7959"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2006-12-19T21:20:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7959 -->
Detailaufnahme des "Radmitnehmers", bestehend aus einem Drehkranz. Die 10mm-Winkel sind mit Federnocken am Drehkranz zusätzlich befestigt.