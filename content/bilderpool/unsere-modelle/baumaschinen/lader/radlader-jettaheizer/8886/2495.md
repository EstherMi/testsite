---
layout: "comment"
hidden: true
title: "2495"
date: "2007-02-24T19:23:30"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Uff! Wenn die Kamera sooo viel Licht braucht, um etwas zu abbilden zu können, sollte sie vielleicht mal auf Star (grau/grün/...) untersucht werden?

Gruß,
Harald