---
layout: "comment"
hidden: true
title: "2275"
date: "2007-02-05T18:31:18"
uploadBy:
- "Jettaheizer"
license: "unknown"
imported:
- "2019"
---
Hallo Harald,

das "bißchen Licht" ist eine 160W Philips ML Lampe, die bei diesem Bild ca. 50cm neben dem Teil steht... 
Tageslicht ist ein sch***dreck dagegen. Dieselbe Lampe nehme ich für alle Fotos, aber mit mehr Abstand reicht das schon nicht mehr. Außerdem hat die Lampe den Nachteil, daß sie ein leicht bläuliches Licht erzeugt, was dem Blaustich meiner Cam noch mehr Futter gibt.
Leider hab ich keine weiße Unterlage, auf die der ganze Trümmer draufpaßt...

Gruß,
Franz