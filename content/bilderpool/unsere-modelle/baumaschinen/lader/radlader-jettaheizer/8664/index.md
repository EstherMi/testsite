---
layout: "image"
title: "Radlader (Detail)"
date: "2007-01-24T20:38:07"
picture: "Radlader47b.jpg"
weight: "37"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- details/8664
imported:
- "2019"
_4images_image_id: "8664"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2007-01-24T20:38:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8664 -->
Schaufel in mittlerer Hubhöhe, aber diesmal maximal abgekippt