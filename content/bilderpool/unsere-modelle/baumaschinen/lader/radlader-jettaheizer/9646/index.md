---
layout: "image"
title: "Radlader (Detail)"
date: "2007-03-22T17:16:53"
picture: "Radlader69b.jpg"
weight: "6"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- details/9646
imported:
- "2019"
_4images_image_id: "9646"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2007-03-22T17:16:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9646 -->
Das Heck von unten. Den Teil ganz unten (graue Bausteine) werd ich vermutlich noch etwas verstärken, wenn ich das Modell wieder zusammenbaue. Diese Aufhängung macht es leider erforderlich, die Karosserie nach hinten nochmals zu verlängern, da sonst die untere Kante nach hinten aus dem geplanten Unterfahrschutz herausschaut.