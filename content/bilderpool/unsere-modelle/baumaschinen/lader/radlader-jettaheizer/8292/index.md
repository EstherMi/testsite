---
layout: "image"
title: "Radlader (Detail)"
date: "2007-01-03T19:25:23"
picture: "Radlader23b.jpg"
weight: "41"
konstrukteure: 
- "Franz Osten"
fotografen:
- "Franz Osten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- details/8292
imported:
- "2019"
_4images_image_id: "8292"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2007-01-03T19:25:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8292 -->
Die Frontpartie. Die beiden Grundplatten 180x90 dienen nicht nur der Optik, sie haben hier tragende und stabilisierende Funktion.