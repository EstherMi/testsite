---
layout: "image"
title: "Radlader (Detail)"
date: "2007-01-03T19:25:23"
picture: "Radlader27b.jpg"
weight: "45"
konstrukteure: 
- "Franz Osten (Jettaheizer"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- details/8296
imported:
- "2019"
_4images_image_id: "8296"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2007-01-03T19:25:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8296 -->
Detailaufnahme des Vorderachsantriebes, vordere Welle. Wird von oben (im übernächsten Bild zu sehen) per Kardan angetrieben. Gefällt mir aber auch noch nicht, weil zwischen Kardan und Differential keinerlei Verbindung der Antriebswelle mit dem Fahrgestell besteht. Werd ich auch noch ändern.