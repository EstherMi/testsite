---
layout: "image"
title: "Radlader Schaufel"
date: "2006-12-07T22:48:31"
picture: "Radlader03b.jpg"
weight: "68"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- details/7715
imported:
- "2019"
_4images_image_id: "7715"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2006-12-07T22:48:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7715 -->
Hier mal eine nähere Aufnahme der Schaufel. Gefällt mir eigentlich so sehr gut.
Nachteil: das Ding ist biestig schwer. Hoffentlich machen das die Arme mit...