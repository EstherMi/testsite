---
layout: "image"
title: "Radlader01"
date: "2006-12-07T22:48:31"
picture: "Radlader01b.jpg"
weight: "66"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- details/7713
imported:
- "2019"
_4images_image_id: "7713"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2006-12-07T22:48:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7713 -->
Mein erster Versuch. Momentan sind die Schaufel (bis auf die seitliche Verkleidung), das Führerhaus und die Lenkmechanik fertig, die Arme sind fast fertig.