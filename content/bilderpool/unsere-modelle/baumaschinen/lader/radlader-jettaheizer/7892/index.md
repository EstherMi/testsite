---
layout: "image"
title: "Lenkmechanik und Antrieb"
date: "2006-12-12T21:57:02"
picture: "Radlader07b.jpg"
weight: "62"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- details/7892
imported:
- "2019"
_4images_image_id: "7892"
_4images_cat_id: "729"
_4images_user_id: "488"
_4images_image_date: "2006-12-12T21:57:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7892 -->
Leider spielen weder der Powermot noch die Differentiale so richtig im Raster mit.