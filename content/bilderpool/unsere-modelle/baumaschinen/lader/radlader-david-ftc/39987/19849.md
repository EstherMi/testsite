---
layout: "comment"
hidden: true
title: "19849"
date: "2014-12-27T12:36:30"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Für die Schaufelmechanik habe ich zwei 4mm Stangen aus Messing (erhältlich im Baumarkt) auf eine Länge von 15 mm abesägt. Eine der beiden ist auf diesem Bild erkennbar, sie verbindet den Rollenblock mit der schwarzen Statikstrebe. Die andere ist auf einem der anderen Bilder zu erkennen.