---
layout: "image"
title: "Kp_version2_1"
date: "2005-07-23T13:02:14"
picture: "Kp2_001.jpg"
weight: "1"
konstrukteure: 
- "ffcoe"
fotografen:
- "ffcoe"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ffcoe"
license: "unknown"
legacy_id:
- details/4520
imported:
- "2019"
_4images_image_id: "4520"
_4images_cat_id: "368"
_4images_user_id: "332"
_4images_image_date: "2005-07-23T13:02:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4520 -->
Als ich die Bilder des ersten Kompaktladers hoch geladen hatte, wurde mir im Chat erklärt, dass ein Kompaktlader die Schaufel kippen könne. Ich hab mich also gleich an die Arbeit gemacht, um dieses "Upgrate" einzubauen.
Und hier ist das Ergebnis.