---
layout: "image"
title: "Kp2"
date: "2005-07-05T21:50:45"
picture: "Kompaktlader_003.jpg"
weight: "8"
konstrukteure: 
- "Christopher Wecht"
fotografen:
- "ffcoe"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ffcoe"
license: "unknown"
legacy_id:
- details/4498
imported:
- "2019"
_4images_image_id: "4498"
_4images_cat_id: "368"
_4images_user_id: "332"
_4images_image_date: "2005-07-05T21:50:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4498 -->
