---
layout: "image"
title: "Kp_version2_4"
date: "2005-07-23T13:02:14"
picture: "Kp2_004.jpg"
weight: "4"
konstrukteure: 
- "ffcoe"
fotografen:
- "ffcoe"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ffcoe"
license: "unknown"
legacy_id:
- details/4523
imported:
- "2019"
_4images_image_id: "4523"
_4images_cat_id: "368"
_4images_user_id: "332"
_4images_image_date: "2005-07-23T13:02:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4523 -->
