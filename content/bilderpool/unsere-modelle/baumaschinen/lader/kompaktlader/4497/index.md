---
layout: "image"
title: "Kp1"
date: "2005-07-05T21:50:44"
picture: "Kompaktlader_001.jpg"
weight: "7"
konstrukteure: 
- "Christopher Wecht"
fotografen:
- "ffcoe"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ffcoe"
license: "unknown"
legacy_id:
- details/4497
imported:
- "2019"
_4images_image_id: "4497"
_4images_cat_id: "368"
_4images_user_id: "332"
_4images_image_date: "2005-07-05T21:50:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4497 -->
Dieses Modell soll einen Kompaktlader der Firma Case darstellen. Wie der Name schon sagt, ist es ein sehr kompakter und wendiger Lader. Ich denke, er wird gerne auf Baustellen benutzt (Habe in unserem Neubaugebiet immerhin einen gesehen).