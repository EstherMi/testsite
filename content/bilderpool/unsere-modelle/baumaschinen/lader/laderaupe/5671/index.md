---
layout: "image"
title: "End-Schalter für für den Hebearm"
date: "2006-01-25T16:43:07"
picture: "DSCN0589.jpg"
weight: "28"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/5671
imported:
- "2019"
_4images_image_id: "5671"
_4images_cat_id: "488"
_4images_user_id: "184"
_4images_image_date: "2006-01-25T16:43:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5671 -->
Dieser Schalter hat mich richtig geärgert. Es war sehr schwer den richtigen Platz für ihn zu finden. Vorne unter der "Motorhaube" ist sehr wenig Platz.