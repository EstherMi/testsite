---
layout: "image"
title: "Hebe Motor (2)"
date: "2006-01-25T16:42:58"
picture: "DSCN0587.jpg"
weight: "19"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/5662
imported:
- "2019"
_4images_image_id: "5662"
_4images_cat_id: "488"
_4images_user_id: "184"
_4images_image_date: "2006-01-25T16:42:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5662 -->
... gut, bei den Zahnrädern habe ich ein bisschen gemogelt ...