---
layout: "image"
title: "Aufreißer"
date: "2006-01-25T14:42:52"
picture: "DSCN0597.jpg"
weight: "12"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/5655
imported:
- "2019"
_4images_image_id: "5655"
_4images_cat_id: "488"
_4images_user_id: "184"
_4images_image_date: "2006-01-25T14:42:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5655 -->
Diese Zähne können zupacken