---
layout: "image"
title: "Schaufelsteuerung (1)"
date: "2006-01-26T16:04:19"
picture: "DSCN0603.jpg"
weight: "4"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/5679
imported:
- "2019"
_4images_image_id: "5679"
_4images_cat_id: "488"
_4images_user_id: "184"
_4images_image_date: "2006-01-26T16:04:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5679 -->
Habe noch ´ne kleine Änderung vorgenommen. So sieht es besser aus.