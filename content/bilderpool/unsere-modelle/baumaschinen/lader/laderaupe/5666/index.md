---
layout: "image"
title: "Ladearm in Aktion (2)"
date: "2006-01-25T16:42:58"
picture: "DSCN0567.jpg"
weight: "23"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/5666
imported:
- "2019"
_4images_image_id: "5666"
_4images_cat_id: "488"
_4images_user_id: "184"
_4images_image_date: "2006-01-25T16:42:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5666 -->
Hier ist die Schaufel angekippt