---
layout: "image"
title: "Ladearm in Aktion (2)"
date: "2006-01-25T16:42:58"
picture: "DSCN0560.jpg"
weight: "24"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/5667
imported:
- "2019"
_4images_image_id: "5667"
_4images_cat_id: "488"
_4images_user_id: "184"
_4images_image_date: "2006-01-25T16:42:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5667 -->
Hier ist der Arm oben