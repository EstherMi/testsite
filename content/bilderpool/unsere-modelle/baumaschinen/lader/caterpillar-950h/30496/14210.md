---
layout: "comment"
hidden: true
title: "14210"
date: "2011-05-01T14:02:04"
uploadBy:
- "Marten70"
license: "unknown"
imported:
- "2019"
---
Sehr schönes Modell... Ich muss mal weiter arbeiten an meine (viel kleinere) Knicklader.

Aber..... immer wieder verwendet man die (zugegeben sehr kompakte ) Lösung mit dem Kardan beim Knickpunkt. Die Folge: ungleichmässiger Rundlauf der angetriebene Asche beim Lenken. 
Fur ein Ausgleich sollte man zwei Kardangelenken benutzen, symetrisch zur Lenkasche. Problem: beim Lenken sollte diese Asche kurzer werden. Leider gibt es bei Fischertechnik keine Originalteile um dieses Problem zu lösen.


Ofwel: prachtig model, inspireert mij om verder te gaan met mijn veel kleinere shovel. 

Ik zie opnieuw het toepassen van een kardan op de knik-as. Hoewel een zeer compacte oplossing geeft dit bij het sturen een ongelijkmatig  toerental op de uitgaande as. De oplossing in de "real world" is twee kardangewrichten toe te passen, min of meer symmetrisch geplaatst ten opzichte van de knikas. Helaas verandert de afstand tussen de twee kardangewrichten bij het sturen, die door een "splined as" worden opgevangen. Helaas heeft Fischertechnik deze niet in het programma.
Zelf ben ik aan het vogelen met een overbrenging zoals in de sturende vooras wordt toegepast, met twee kegelwielen op de horizontale (aangedreven) as , welke ingrijpen op een kegelwiel dat op de verticale as is geplaatst. Nadele: de draairichting van de uitgaande as is tegengesteld aan de ingaande as..... uitdagingen uitdagingen....

Maar, los van deze kritiek: puik model!!