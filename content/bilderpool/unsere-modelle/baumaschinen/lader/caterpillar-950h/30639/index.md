---
layout: "image"
title: "950H"
date: "2011-05-27T17:01:18"
picture: "950h_016.jpg"
weight: "1"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/30639
imported:
- "2019"
_4images_image_id: "30639"
_4images_cat_id: "2270"
_4images_user_id: "838"
_4images_image_date: "2011-05-27T17:01:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30639 -->
De 950H aangepast naar servo besturing. Rijdt veel relaxter en gaat niet kapot.