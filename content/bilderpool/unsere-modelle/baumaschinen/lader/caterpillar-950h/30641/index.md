---
layout: "image"
title: "servo"
date: "2011-05-27T17:01:18"
picture: "950h_018.jpg"
weight: "3"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/30641
imported:
- "2019"
_4images_image_id: "30641"
_4images_cat_id: "2270"
_4images_user_id: "838"
_4images_image_date: "2011-05-27T17:01:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30641 -->
Hoe ruig je ook stuurt de servo zal nooit beschadigen en de stuurmotor corrigeert er keurig achteraan zelf van extreem recht naar extreem links.