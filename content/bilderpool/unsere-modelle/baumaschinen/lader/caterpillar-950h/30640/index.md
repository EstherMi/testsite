---
layout: "image"
title: "servo"
date: "2011-05-27T17:01:18"
picture: "950h_017.jpg"
weight: "2"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/30640
imported:
- "2019"
_4images_image_id: "30640"
_4images_cat_id: "2270"
_4images_user_id: "838"
_4images_image_date: "2011-05-27T17:01:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30640 -->
Het schroefje houdt het elastiekje op zijn plek daar het anders naar het schanier toeschuift.