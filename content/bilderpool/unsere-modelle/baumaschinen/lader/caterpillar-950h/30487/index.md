---
layout: "image"
title: "Bovenaanzicht"
date: "2011-04-27T22:02:30"
picture: "pivot_006.jpg"
weight: "23"
konstrukteure: 
- "Chef8"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/30487
imported:
- "2019"
_4images_image_id: "30487"
_4images_cat_id: "2270"
_4images_user_id: "838"
_4images_image_date: "2011-04-27T22:02:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30487 -->
Bovenaanzicht van het geheel 1,8 kg op achteras en 1,6 kg op de vooras