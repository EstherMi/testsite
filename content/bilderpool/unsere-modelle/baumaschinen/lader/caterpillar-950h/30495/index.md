---
layout: "image"
title: "achteraanzicht"
date: "2011-04-27T22:02:43"
picture: "pivot_014.jpg"
weight: "31"
konstrukteure: 
- "Chef8"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/30495
imported:
- "2019"
_4images_image_id: "30495"
_4images_cat_id: "2270"
_4images_user_id: "838"
_4images_image_date: "2011-04-27T22:02:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30495 -->
Achterkant van de 950H Zitten 2 rijmotoren in gekoppeld met een diff voor extra rijpower