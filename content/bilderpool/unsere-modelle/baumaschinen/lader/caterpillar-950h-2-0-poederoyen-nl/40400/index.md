---
layout: "image"
title: "Caterpillar-950H-2.0 mit Dreh-Zylinder  -Oben 1"
date: "2015-01-24T21:42:56"
picture: "caterpillar04.jpg"
weight: "4"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/40400
imported:
- "2019"
_4images_image_id: "40400"
_4images_cat_id: "3028"
_4images_user_id: "22"
_4images_image_date: "2015-01-24T21:42:56"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40400 -->
M1 = Drehzylinder- Antrieb -Laadbak  dmv 10 x 12 x 26mm Getriebemotor Pololu + M5 zum kippen
M2 = Drehzylinder-Antrieb -Giek  dmv 2x Schwarze XM-Motor + Ketten-Antrieb + 2x M5 zum hoch und nach unten
M3 = Lenk-Antrieb dmv 10 x 12 x 26mm Getriebemotor Pololu + M4  zum Links und Rechts
Servo =  Fahr-Antrieb (2x Powermotor 20:1) mit Thor4 am servo-Ausgang zum Vor- und -Ruckwärts-gang
