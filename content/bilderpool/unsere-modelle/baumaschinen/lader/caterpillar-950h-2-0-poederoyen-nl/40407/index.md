---
layout: "image"
title: "Caterpillar-950H-2.0  Antrieb mit 2x Powermotoren 20:1"
date: "2015-01-24T21:42:56"
picture: "caterpillar11.jpg"
weight: "11"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/40407
imported:
- "2019"
_4images_image_id: "40407"
_4images_cat_id: "3028"
_4images_user_id: "22"
_4images_image_date: "2015-01-24T21:42:56"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40407 -->
pendelende achteras en 4 wielaandrijving

Voor de pendelende achteras (1), de laadbak (2) en de stuur-unit -onder en boven (3) heb ik totaal 4st Bodenplatte Führerhaus 75x90 (31889) gebruikt.

Fahr-Antrieb (2x Powermotor 20:1) mit Thor4 am servo-Ausgang zum Vor- und -Ruckwärts-gang