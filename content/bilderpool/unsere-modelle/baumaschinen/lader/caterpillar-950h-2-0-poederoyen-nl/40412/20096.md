---
layout: "comment"
hidden: true
title: "20096"
date: "2015-01-25T14:41:30"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Der sieht ja vollständig Baustellen-tauglich aus, super! Nur die Räder sind dafür viel zu sauber und glänzend ;-)

Gruß,
Stefan