---
layout: "image"
title: "Caterpillar-950H-2.0  Antrieb mit 2x Powermotor 20:1   -Oben"
date: "2015-01-24T21:42:56"
picture: "caterpillar13.jpg"
weight: "13"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/40409
imported:
- "2019"
_4images_image_id: "40409"
_4images_cat_id: "3028"
_4images_user_id: "22"
_4images_image_date: "2015-01-24T21:42:56"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40409 -->
