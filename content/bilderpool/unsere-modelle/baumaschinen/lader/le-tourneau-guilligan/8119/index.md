---
layout: "image"
title: "Radlader Le Tourneau 2350"
date: "2006-12-26T15:39:46"
picture: "baumaschinen05.jpg"
weight: "5"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/8119
imported:
- "2019"
_4images_image_id: "8119"
_4images_cat_id: "751"
_4images_user_id: "389"
_4images_image_date: "2006-12-26T15:39:46"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8119 -->
Vorderbauansicht