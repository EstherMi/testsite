---
layout: "image"
title: "Radlader Le Tourneau 2350"
date: "2006-12-26T15:39:45"
picture: "baumaschinen02.jpg"
weight: "2"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/8116
imported:
- "2019"
_4images_image_id: "8116"
_4images_cat_id: "751"
_4images_user_id: "389"
_4images_image_date: "2006-12-26T15:39:45"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8116 -->
Gesamtansicht 2