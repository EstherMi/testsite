---
layout: "image"
title: "Radlader Le Tourneau 2350"
date: "2006-12-26T15:40:01"
picture: "baumaschinen15.jpg"
weight: "15"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/8129
imported:
- "2019"
_4images_image_id: "8129"
_4images_cat_id: "751"
_4images_user_id: "389"
_4images_image_date: "2006-12-26T15:40:01"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8129 -->
Moter quer eingebaut für den Vorderantrieb