---
layout: "image"
title: "Atlas Copco Scooptram"
date: "2014-01-13T13:02:34"
picture: "P1130036.jpg"
weight: "1"
konstrukteure: 
- "Ruurd"
fotografen:
- "Chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/38058
imported:
- "2019"
_4images_image_id: "38058"
_4images_cat_id: "2830"
_4images_user_id: "838"
_4images_image_date: "2014-01-13T13:02:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38058 -->
Mijn nieuwste project is bijna klaar alleen de afstandsbediening moet er nog in. Verder is hij volledig functioneel.