---
layout: "image"
title: "Detail foto lader frame"
date: "2014-01-13T13:02:35"
picture: "P1130038.jpg"
weight: "3"
konstrukteure: 
- "Ruurd"
fotografen:
- "Chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/38060
imported:
- "2019"
_4images_image_id: "38060"
_4images_cat_id: "2830"
_4images_user_id: "838"
_4images_image_date: "2014-01-13T13:02:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38060 -->
Ik had een probleem met het wegwerken van de motor voor de neig cilinder. Zodoende eindigde deze motor in het achterstuk.