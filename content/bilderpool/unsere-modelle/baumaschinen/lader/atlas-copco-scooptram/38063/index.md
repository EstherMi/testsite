---
layout: "image"
title: "Hoogste stand"
date: "2014-01-13T13:02:35"
picture: "P1130041.jpg"
weight: "6"
konstrukteure: 
- "Ruurd"
fotografen:
- "Chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/38063
imported:
- "2019"
_4images_image_id: "38063"
_4images_cat_id: "2830"
_4images_user_id: "838"
_4images_image_date: "2014-01-13T13:02:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38063 -->
Het heffen gebeurde eerst met een 9:1 powermotor, maar die had het te zwaar daarom gebeurt het nu met een 20:1. Dit gaat veel beter.