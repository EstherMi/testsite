---
layout: "image"
title: "Liebherr 26 K"
date: "2012-01-08T23:29:56"
picture: "IM003396.jpg"
weight: "10"
konstrukteure: 
- "Rheingauer01"
fotografen:
- "Rheingauer01"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rheingauer01"
license: "unknown"
legacy_id:
- details/33874
imported:
- "2019"
_4images_image_id: "33874"
_4images_cat_id: "2505"
_4images_user_id: "1428"
_4images_image_date: "2012-01-08T23:29:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33874 -->
Kran in Arbeitsstellung. Die Hauptfunktionen (Heben/Senken/Drehen/Laufkatze vor/zurück) sind über die IR-Steuerung bedienbar.