---
layout: "image"
title: "Liebherr 26 K"
date: "2012-01-08T23:29:55"
picture: "IM003410.jpg"
weight: "3"
konstrukteure: 
- "Rheingauer01"
fotografen:
- "Rheingauer01"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rheingauer01"
license: "unknown"
legacy_id:
- details/33867
imported:
- "2019"
_4images_image_id: "33867"
_4images_cat_id: "2505"
_4images_user_id: "1428"
_4images_image_date: "2012-01-08T23:29:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33867 -->
Der Turm wurde mit 2 Bolzen verriegelt und der Turmauszug frei gegeben.