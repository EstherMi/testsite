---
layout: "image"
title: "Hebebühne aus dem Baukasten Car tuning Center"
date: "2009-10-01T19:18:51"
picture: "dersupercranemitmotorenundderirfernsteuerung1.jpg"
weight: "1"
konstrukteure: 
- "Dieter Meckel"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/25443
imported:
- "2019"
_4images_image_id: "25443"
_4images_cat_id: "1781"
_4images_user_id: "374"
_4images_image_date: "2009-10-01T19:18:51"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25443 -->
Hebebühne mit Rennwagen