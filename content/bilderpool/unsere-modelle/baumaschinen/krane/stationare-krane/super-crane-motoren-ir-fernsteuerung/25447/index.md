---
layout: "image"
title: "Super Crane mit Drehkranz"
date: "2009-10-01T19:18:52"
picture: "dersupercranemitmotorenundderirfernsteuerung5.jpg"
weight: "5"
konstrukteure: 
- "Dieter Meckel"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/25447
imported:
- "2019"
_4images_image_id: "25447"
_4images_cat_id: "1781"
_4images_user_id: "374"
_4images_image_date: "2009-10-01T19:18:52"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25447 -->
