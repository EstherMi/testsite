---
layout: "image"
title: "Laufkatzenmotor"
date: "2003-11-03T22:26:36"
picture: "IMG_0430.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- details/1856
imported:
- "2019"
_4images_image_id: "1856"
_4images_cat_id: "200"
_4images_user_id: "6"
_4images_image_date: "2003-11-03T22:26:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1856 -->
Laufkatzenmotor mit Seilrolle die die Katze vor und zurückfährt.