---
layout: "overview"
title: "Classic-Kran (2m)"
date: 2019-12-17T19:12:27+01:00
legacy_id:
- categories/3290
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3290 --> 
Der Classic Kran besteht zu 98% nur aus Classic-Teilen (grau/rot).
Höhe ca. 200cm / Ausleger ca. 150cm / max. Last bei voller Auslage ca. 400g
Gebaut im Frühjahr 2015 zur Ausstellung Dreieich 2015
Bauzeit ca. 3 Monate in Freizeit. Statik durch Jan (6 Jahre)!
Besonderheiten
- frei drehbar durch 3 Schleifringkontakte
- Kabelfernsteuerung mit Feedback-Lichtern
- voll bespielbar