---
layout: "image"
title: "Antriebsblock"
date: "2007-10-12T21:26:45"
picture: "gittermastkran7.jpg"
weight: "11"
konstrukteure: 
- "Masked (Martin)"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/12193
imported:
- "2019"
_4images_image_id: "12193"
_4images_cat_id: "1091"
_4images_user_id: "373"
_4images_image_date: "2007-10-12T21:26:45"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12193 -->
Nochmal in der Übersicht. Wenn ihr euch jetzt fragt, was denn die Schrauben da in den Statikträgern sollen, dann schaut mal 2 Fotos weiter.