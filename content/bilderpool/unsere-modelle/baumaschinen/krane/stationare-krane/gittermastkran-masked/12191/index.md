---
layout: "image"
title: "Gelenk"
date: "2007-10-12T21:26:45"
picture: "gittermastkran5.jpg"
weight: "9"
konstrukteure: 
- "Masked (Martin)"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/12191
imported:
- "2019"
_4images_image_id: "12191"
_4images_cat_id: "1091"
_4images_user_id: "373"
_4images_image_date: "2007-10-12T21:26:45"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12191 -->
Das Gelenk in der Mitte des Kranes