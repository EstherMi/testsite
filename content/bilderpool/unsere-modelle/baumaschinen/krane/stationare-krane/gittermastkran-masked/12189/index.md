---
layout: "image"
title: "Erster Versuch, Teil 2"
date: "2007-10-12T21:26:45"
picture: "gittermastkran3.jpg"
weight: "7"
konstrukteure: 
- "Masked (Martin)"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/12189
imported:
- "2019"
_4images_image_id: "12189"
_4images_cat_id: "1091"
_4images_user_id: "373"
_4images_image_date: "2007-10-12T21:26:45"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12189 -->
Immer noch die volle Länge, diesmal aber mit neuer Abspannung