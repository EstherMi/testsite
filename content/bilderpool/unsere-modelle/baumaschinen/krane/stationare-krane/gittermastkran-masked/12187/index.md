---
layout: "image"
title: "Erster Versuch"
date: "2007-10-12T21:26:45"
picture: "gittermastkran1.jpg"
weight: "5"
konstrukteure: 
- "Masked (Martin)"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/12187
imported:
- "2019"
_4images_image_id: "12187"
_4images_cat_id: "1091"
_4images_user_id: "373"
_4images_image_date: "2007-10-12T21:26:45"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12187 -->
Das war der erste Versuch eines größeren Gittermastkranes. Wie man sieht hatte ich mich bei der Abspannung etwas verschätzt ;-)