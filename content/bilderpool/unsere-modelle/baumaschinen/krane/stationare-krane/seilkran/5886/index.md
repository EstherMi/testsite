---
layout: "image"
title: "Station 2"
date: "2006-03-12T18:38:51"
picture: "Station_2.jpg"
weight: "5"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/5886
imported:
- "2019"
_4images_image_id: "5886"
_4images_cat_id: "508"
_4images_user_id: "373"
_4images_image_date: "2006-03-12T18:38:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5886 -->
