---
layout: "image"
title: "IR- Fernsteuerung"
date: "2012-03-02T14:24:50"
picture: "yantairafflesinkleinerversion09.jpg"
weight: "9"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- details/34502
imported:
- "2019"
_4images_image_id: "34502"
_4images_cat_id: "2548"
_4images_user_id: "1122"
_4images_image_date: "2012-03-02T14:24:50"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34502 -->
Alle drei Motoren des Kranes sind mit der Fischertechnik-IR-Fernsteuerung ansteuerbar.
