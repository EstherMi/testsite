---
layout: "image"
title: "Von Oben"
date: "2012-03-02T14:24:50"
picture: "yantairafflesinkleinerversion07.jpg"
weight: "7"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- details/34500
imported:
- "2019"
_4images_image_id: "34500"
_4images_cat_id: "2548"
_4images_user_id: "1122"
_4images_image_date: "2012-03-02T14:24:50"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34500 -->
Die Seilwinde für die Hacken.