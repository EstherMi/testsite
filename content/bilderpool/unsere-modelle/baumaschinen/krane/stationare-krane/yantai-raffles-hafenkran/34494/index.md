---
layout: "image"
title: "Gesamt Ansicht"
date: "2012-03-02T14:24:50"
picture: "yantairafflesinkleinerversion01.jpg"
weight: "1"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- details/34494
imported:
- "2019"
_4images_image_id: "34494"
_4images_cat_id: "2548"
_4images_user_id: "1122"
_4images_image_date: "2012-03-02T14:24:50"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34494 -->
Im oberen Drehteil sind 3 Mini-Motoren verbaut.