---
layout: "image"
title: "Ir-Fernbedinung"
date: "2012-03-02T14:24:50"
picture: "yantairafflesinkleinerversion10.jpg"
weight: "10"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- details/34503
imported:
- "2019"
_4images_image_id: "34503"
_4images_cat_id: "2548"
_4images_user_id: "1122"
_4images_image_date: "2012-03-02T14:24:50"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34503 -->
Das Steuermodul und der Akku sind im Krangehäuse untergebracht.