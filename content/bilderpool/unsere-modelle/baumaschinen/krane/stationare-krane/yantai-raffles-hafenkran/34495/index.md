---
layout: "image"
title: "Von hinten"
date: "2012-03-02T14:24:50"
picture: "yantairafflesinkleinerversion02.jpg"
weight: "2"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- details/34495
imported:
- "2019"
_4images_image_id: "34495"
_4images_cat_id: "2548"
_4images_user_id: "1122"
_4images_image_date: "2012-03-02T14:24:50"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34495 -->
Hier gut zu sehen, das Gegengewicht.