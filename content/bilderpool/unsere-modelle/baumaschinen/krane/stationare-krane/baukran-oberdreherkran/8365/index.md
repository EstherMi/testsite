---
layout: "image"
title: "Baukran, Oberdreher umgebaut"
date: "2007-01-11T22:55:57"
picture: "Baukran_2.jpg"
weight: "66"
konstrukteure: 
- "Alphawolf"
fotografen:
- "Alphawolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Alphawolf"
license: "unknown"
legacy_id:
- details/8365
imported:
- "2019"
_4images_image_id: "8365"
_4images_cat_id: "770"
_4images_user_id: "522"
_4images_image_date: "2007-01-11T22:55:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8365 -->
