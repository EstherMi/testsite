---
layout: "image"
title: "baukran29.jpg"
date: "2017-02-27T17:37:26"
picture: "baukran29.jpg"
weight: "29"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/45309
imported:
- "2019"
_4images_image_id: "45309"
_4images_cat_id: "770"
_4images_user_id: "2449"
_4images_image_date: "2017-02-27T17:37:26"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45309 -->
