---
layout: "image"
title: "Baukran, Oberdreherkran"
date: "2007-01-08T20:17:24"
picture: "Baukran_1.jpg"
weight: "67"
konstrukteure: 
- "Alphawolf"
fotografen:
- "Alphawolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Alphawolf"
license: "unknown"
legacy_id:
- details/8358
imported:
- "2019"
_4images_image_id: "8358"
_4images_cat_id: "770"
_4images_user_id: "522"
_4images_image_date: "2007-01-08T20:17:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8358 -->
Hier seht ihr meinen Baukran als Oberdreherkran gebaut...
Derzeit baue ich ihn gerade zu einem Giganten um, der mit über 2,00m Höhe und über 1,80m Längebestimmt beeindruckend wirkt...

Bis dann

Euer Alphawolf