---
layout: "image"
title: "Oberdrehermontage01"
date: "2007-12-18T17:32:58"
picture: "kranmontage01.jpg"
weight: "57"
konstrukteure: 
- "Alphawolf"
fotografen:
- "Alphawolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Alphawolf"
license: "unknown"
legacy_id:
- details/13101
imported:
- "2019"
_4images_image_id: "13101"
_4images_cat_id: "770"
_4images_user_id: "522"
_4images_image_date: "2007-12-18T17:32:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13101 -->
Hier wir mein neuer Fischertechnik Oberdreher Baustellenkran Stück für Stück montiert, siehe Fotos...

Euer Alphawolf