---
layout: "image"
title: "baukran25.jpg"
date: "2017-02-27T17:37:26"
picture: "baukran25.jpg"
weight: "25"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/45305
imported:
- "2019"
_4images_image_id: "45305"
_4images_cat_id: "770"
_4images_user_id: "2449"
_4images_image_date: "2017-02-27T17:37:26"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45305 -->
