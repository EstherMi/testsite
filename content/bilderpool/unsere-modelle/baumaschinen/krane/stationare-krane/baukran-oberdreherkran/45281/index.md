---
layout: "image"
title: "Baukran"
date: "2017-02-27T17:37:26"
picture: "baukran01.jpg"
weight: "1"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/45281
imported:
- "2019"
_4images_image_id: "45281"
_4images_cat_id: "770"
_4images_user_id: "2449"
_4images_image_date: "2017-02-27T17:37:26"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45281 -->
Zwar nicht wirklich Etwas Besonderes, einen Nächsten Baukran. Ich habe Ihn jedenfalls gebaut, weil Ich versuchen wollte den Ausleger sehr schlicht und leicht zu machen und das Motorhaus compact zu halten. Das ist wohl einigermaßen gelungen, glaube Ich.