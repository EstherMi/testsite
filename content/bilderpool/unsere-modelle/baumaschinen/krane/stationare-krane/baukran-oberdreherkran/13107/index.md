---
layout: "image"
title: "Kranmontage 06"
date: "2007-12-18T17:32:59"
picture: "kranmontage06.jpg"
weight: "63"
konstrukteure: 
- "Alphawolf"
fotografen:
- "Alphawolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Alphawolf"
license: "unknown"
legacy_id:
- details/13107
imported:
- "2019"
_4images_image_id: "13107"
_4images_cat_id: "770"
_4images_user_id: "522"
_4images_image_date: "2007-12-18T17:32:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13107 -->
