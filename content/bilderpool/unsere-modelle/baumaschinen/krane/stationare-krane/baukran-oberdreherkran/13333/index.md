---
layout: "image"
title: "Der fertige Oberdreher Baustellenkran"
date: "2008-01-16T16:52:09"
picture: "kran1.jpg"
weight: "56"
konstrukteure: 
- "Alexander Eckrich"
fotografen:
- "Alexander Eckrich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Alphawolf"
license: "unknown"
legacy_id:
- details/13333
imported:
- "2019"
_4images_image_id: "13333"
_4images_cat_id: "770"
_4images_user_id: "522"
_4images_image_date: "2008-01-16T16:52:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13333 -->
