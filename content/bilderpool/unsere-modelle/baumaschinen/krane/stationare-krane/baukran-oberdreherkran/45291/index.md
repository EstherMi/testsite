---
layout: "image"
title: "baukran11.jpg"
date: "2017-02-27T17:37:26"
picture: "baukran11.jpg"
weight: "11"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/45291
imported:
- "2019"
_4images_image_id: "45291"
_4images_cat_id: "770"
_4images_user_id: "2449"
_4images_image_date: "2017-02-27T17:37:26"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45291 -->
