---
layout: "image"
title: "Gegengewichte"
date: "2007-04-03T17:33:56"
picture: "Kleiner_Kran13.jpg"
weight: "8"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/9917
imported:
- "2019"
_4images_image_id: "9917"
_4images_cat_id: "890"
_4images_user_id: "456"
_4images_image_date: "2007-04-03T17:33:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9917 -->
Das ist mein Kran. Weil er jetzt vergrößert wurde hat er 2 Gegengewichte. 1xBlei und 1xAkku.