---
layout: "image"
title: "Kran"
date: "2007-04-03T17:33:56"
picture: "Kleiner_Kran12.jpg"
weight: "7"
konstrukteure: 
- "timtech"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/9916
imported:
- "2019"
_4images_image_id: "9916"
_4images_cat_id: "890"
_4images_user_id: "456"
_4images_image_date: "2007-04-03T17:33:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9916 -->
Die Laufschiene vom Kran von timtech.