---
layout: "image"
title: "Kleiner Kran"
date: "2007-03-31T18:08:13"
picture: "Kleiner_Kran4.jpg"
weight: "15"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/9862
imported:
- "2019"
_4images_image_id: "9862"
_4images_cat_id: "890"
_4images_user_id: "456"
_4images_image_date: "2007-03-31T18:08:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9862 -->
Hier sieht man die Laufschiene. Sie besteht aus einem Hubgetriebe mit Zahnstangen.