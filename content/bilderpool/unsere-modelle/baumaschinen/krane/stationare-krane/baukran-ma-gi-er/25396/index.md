---
layout: "image"
title: "Rolle für Seil"
date: "2009-09-27T23:59:14"
picture: "kranvonmagier06.jpg"
weight: "6"
konstrukteure: 
- "Martin Giger"
fotografen:
- "Martin Giger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/25396
imported:
- "2019"
_4images_image_id: "25396"
_4images_cat_id: "1779"
_4images_user_id: "445"
_4images_image_date: "2009-09-27T23:59:14"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25396 -->
Diese Rolle ist etwas über der Rolle am Wägelchen für das Seil des Hackens montiert.