---
layout: "image"
title: "Antrieb für die Drehung"
date: "2009-09-27T23:59:14"
picture: "kranvonmagier08.jpg"
weight: "8"
konstrukteure: 
- "Martin Giger"
fotografen:
- "Martin Giger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/25398
imported:
- "2019"
_4images_image_id: "25398"
_4images_cat_id: "1779"
_4images_user_id: "445"
_4images_image_date: "2009-09-27T23:59:14"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25398 -->
Diese Kette ist etwas überspannt, wobei dies nicht sehr schlimm ist. Dafür kann man sehr genau platzieren (bis auf leichtwes nachschwenken)