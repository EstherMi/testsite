---
layout: "image"
title: "Antrieb des Seils"
date: "2009-09-27T23:59:14"
picture: "kranvonmagier07.jpg"
weight: "7"
konstrukteure: 
- "Martin Giger"
fotografen:
- "Martin Giger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/25397
imported:
- "2019"
_4images_image_id: "25397"
_4images_cat_id: "1779"
_4images_user_id: "445"
_4images_image_date: "2009-09-27T23:59:14"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25397 -->
Diesen habe ich simpel nach Beispiel Bauanleitung von ft gelöst.