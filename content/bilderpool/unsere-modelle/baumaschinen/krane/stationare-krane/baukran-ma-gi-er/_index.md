---
layout: "overview"
title: "Baukran von Ma-gi-er"
date: 2019-12-17T19:12:21+01:00
legacy_id:
- categories/1779
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1779 --> 
Dieser Kran ist 117.5 cm hoch und mit dem Gewichtsausgleich 98 cm breit. Der Ausleger sleber ist 66.5cm lang. Gearbeitet habe ich an dem Kran etwa 2 Tage, danach war er fertig. Der Mast ist so stabil, dass ich mich darauf setzten kann, ohne, dass er einknickt oder so.Er steht nicht super Stabil, da er auf vier Platten steht, welche eine leichte Rundung haben. P.S.:Alle Bilder habe ich mit meinem Handy geknipst!