---
layout: "image"
title: "Gesamtansicht Mitte und Hinten"
date: "2009-09-27T23:59:14"
picture: "kranvonmagier05.jpg"
weight: "5"
konstrukteure: 
- "Martin Giger"
fotografen:
- "Martin Giger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/25395
imported:
- "2019"
_4images_image_id: "25395"
_4images_cat_id: "1779"
_4images_user_id: "445"
_4images_image_date: "2009-09-27T23:59:14"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25395 -->
