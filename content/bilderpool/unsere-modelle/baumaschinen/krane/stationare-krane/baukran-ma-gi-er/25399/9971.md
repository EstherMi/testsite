---
layout: "comment"
hidden: true
title: "9971"
date: "2009-09-28T21:12:25"
uploadBy:
- "Triceratops"
license: "unknown"
imported:
- "2019"
---
Hallo

Je größer der Kran insgesamt (und damit auch stetig schwerer), desto höher
wird der Schneckenantrieb der Drehmechanik belastet. Ich sehe das bei dem
Molenkran, dessen drehbarer Teil m. E. viel zu groß und damit zu schwer ist.
Immerhin ist das auch "nur" die elektrifizierte Weiterentwicklung eines origina-
len FT-Modells. Nach einer gewissen Zeit und mehreren Drehungen ist die
Schnecke oft "aus der Spur", so daß der Antrieb überspringt. Am Motor liegt
es jedenfalls nicht; es liegt halt oft an begrenzten Anbau-/ Befestigungsmög-
lichkeiten.

Gruß, Thomas