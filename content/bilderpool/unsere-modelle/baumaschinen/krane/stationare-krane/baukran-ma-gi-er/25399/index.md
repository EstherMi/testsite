---
layout: "image"
title: "Drehantrieb"
date: "2009-09-27T23:59:15"
picture: "kranvonmagier09.jpg"
weight: "9"
konstrukteure: 
- "Martin Giger"
fotografen:
- "Martin Giger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/25399
imported:
- "2019"
_4images_image_id: "25399"
_4images_cat_id: "1779"
_4images_user_id: "445"
_4images_image_date: "2009-09-27T23:59:15"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25399 -->
Schnekenantrieb zum drehenm des Krans. Dieser Antrieb ist ein bisschen ein Gebastel und auch nicht gerade eine Augenweide. Eine bessere Lösung habe ich aber nicht gefunden. Sobald der Motor jedoch gestoppt wird, schwankt er noch etwas vor und zurück, da die Schnecke noch etwas Spielraum hat.