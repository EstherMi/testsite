---
layout: "image"
title: "Hafenkran Bild 2"
date: "2003-08-12T19:29:59"
picture: "RIMG0054.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MaLie"
license: "unknown"
legacy_id:
- details/1332
imported:
- "2019"
_4images_image_id: "1332"
_4images_cat_id: "144"
_4images_user_id: "26"
_4images_image_date: "2003-08-12T19:29:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1332 -->
selbstgebauter Hafenkran

mit Interface und Joystick gesteuert