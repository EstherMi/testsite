---
layout: "image"
title: "Wippkran, Steuerungseinheit"
date: "2012-01-11T18:34:32"
picture: "wippkran5.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- details/33884
imported:
- "2019"
_4images_image_id: "33884"
_4images_cat_id: "144"
_4images_user_id: "1361"
_4images_image_date: "2012-01-11T18:34:32"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33884 -->
Mit 2 Joysticks lässt sich der Wippkran punktgenau steuern. Die Regler drosseln die Motorgeschwindigkeiten.