---
layout: "comment"
hidden: true
title: "16072"
date: "2012-01-13T16:51:24"
uploadBy:
- "Ixer"
license: "unknown"
imported:
- "2019"
---
An McDoofi,
da ich die Originalstecker für die Stromversorgung der Power-Controller nicht immer praktisch fand, habe ich mir mittels zweier Buchsen, passend zu den ft-Steckern, und interner Kabelzuführung diese alternative Art der Stromzuführung gebaut.
Gruß Ixer