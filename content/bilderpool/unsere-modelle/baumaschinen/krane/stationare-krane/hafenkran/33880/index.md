---
layout: "image"
title: "Wippkran"
date: "2012-01-11T18:34:32"
picture: "wippkran1.jpg"
weight: "1"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- details/33880
imported:
- "2019"
_4images_image_id: "33880"
_4images_cat_id: "144"
_4images_user_id: "1361"
_4images_image_date: "2012-01-11T18:34:32"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33880 -->
Fuktionen: Fahrt vor - zurück, Drehung im - und gegen den Uhrzeigersinn, Ausleger vor - zurück, Haken auf - ab, E-Magnet an - aus, Beleuchtung an - aus