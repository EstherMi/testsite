---
layout: "image"
title: "Hafenkran Bild 3"
date: "2003-08-12T19:29:59"
picture: "RIMG0063.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MaLie"
license: "unknown"
legacy_id:
- details/1333
imported:
- "2019"
_4images_image_id: "1333"
_4images_cat_id: "144"
_4images_user_id: "26"
_4images_image_date: "2003-08-12T19:29:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1333 -->
selbstgebauter Hafenkran

mit Interface und Joystick gesteuert