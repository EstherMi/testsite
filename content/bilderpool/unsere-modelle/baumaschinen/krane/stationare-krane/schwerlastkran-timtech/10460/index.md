---
layout: "image"
title: "Kran"
date: "2007-05-19T09:12:25"
picture: "PICT0055.jpg"
weight: "1"
konstrukteure: 
- "timtech"
fotografen:
- "timtech"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "timtech"
license: "unknown"
legacy_id:
- details/10460
imported:
- "2019"
_4images_image_id: "10460"
_4images_cat_id: "953"
_4images_user_id: "590"
_4images_image_date: "2007-05-19T09:12:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10460 -->
Hier sieht man meinen Schwerlastkran.Ich habe bis zu 0,5kg am Hacken, das geht aber nur in  
der höchsten Stellung.