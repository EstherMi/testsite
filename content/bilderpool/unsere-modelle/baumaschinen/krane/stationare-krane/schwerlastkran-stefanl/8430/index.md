---
layout: "image"
title: "Schwerlastkran 12"
date: "2007-01-13T20:16:26"
picture: "schwerlastkran12.jpg"
weight: "15"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/8430
imported:
- "2019"
_4images_image_id: "8430"
_4images_cat_id: "774"
_4images_user_id: "502"
_4images_image_date: "2007-01-13T20:16:26"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8430 -->
