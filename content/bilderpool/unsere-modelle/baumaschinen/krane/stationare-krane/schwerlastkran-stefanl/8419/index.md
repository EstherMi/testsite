---
layout: "image"
title: "Schwerlastkran 1"
date: "2007-01-13T20:16:26"
picture: "schwerlastkran01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/8419
imported:
- "2019"
_4images_image_id: "8419"
_4images_cat_id: "774"
_4images_user_id: "502"
_4images_image_date: "2007-01-13T20:16:26"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8419 -->
Höhe: 2 meter