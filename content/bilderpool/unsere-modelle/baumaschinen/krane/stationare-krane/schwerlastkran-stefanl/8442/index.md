---
layout: "image"
title: "Schwerlastkran 15"
date: "2007-01-13T22:12:46"
picture: "schwerlastkran3.jpg"
weight: "6"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/8442
imported:
- "2019"
_4images_image_id: "8442"
_4images_cat_id: "774"
_4images_user_id: "502"
_4images_image_date: "2007-01-13T22:12:46"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8442 -->
