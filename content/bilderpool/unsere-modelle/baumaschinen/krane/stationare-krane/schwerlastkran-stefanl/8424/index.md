---
layout: "image"
title: "Schwerlastkran 6"
date: "2007-01-13T20:16:26"
picture: "schwerlastkran06.jpg"
weight: "9"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/8424
imported:
- "2019"
_4images_image_id: "8424"
_4images_cat_id: "774"
_4images_user_id: "502"
_4images_image_date: "2007-01-13T20:16:26"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8424 -->
