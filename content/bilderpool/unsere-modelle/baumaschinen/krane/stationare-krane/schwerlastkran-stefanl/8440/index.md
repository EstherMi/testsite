---
layout: "image"
title: "Schwerlastkran 13"
date: "2007-01-13T22:12:46"
picture: "schwerlastkran1.jpg"
weight: "2"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/8440
imported:
- "2019"
_4images_image_id: "8440"
_4images_cat_id: "774"
_4images_user_id: "502"
_4images_image_date: "2007-01-13T22:12:46"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8440 -->
