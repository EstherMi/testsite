---
layout: "image"
title: "Steuerung"
date: "2016-09-03T11:07:00"
picture: "baustellenkran4.jpg"
weight: "4"
konstrukteure: 
- "Jori"
fotografen:
- "Jori"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jori"
license: "unknown"
legacy_id:
- details/44332
imported:
- "2019"
_4images_image_id: "44332"
_4images_cat_id: "3271"
_4images_user_id: "2465"
_4images_image_date: "2016-09-03T11:07:00"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44332 -->
Gesteuert wird der Kran über drei Schalter und einen Taster. Die zwei großen Schalter sind für die Drehung des Krans und die Bewegung des Auslegers, mit dem Taster(ganz rechts) wird die Seilwinde für den Kranhaken gesteuert, über den (Um)Schalter schräg links daneben kann man die Drehrichtung der Seilwinde (für den Kranhaken) einstellen.
Links davon sieht man nochmal den Mast des Krans, unten wird er von vier zusätzlichen U-Trägern stabilisiert.