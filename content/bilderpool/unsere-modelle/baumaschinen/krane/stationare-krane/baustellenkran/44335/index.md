---
layout: "image"
title: "Seilwinden, Flaschenzug und Motoren"
date: "2016-09-03T11:07:00"
picture: "baustellenkran7.jpg"
weight: "7"
konstrukteure: 
- "Jori"
fotografen:
- "Jori"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jori"
license: "unknown"
legacy_id:
- details/44335
imported:
- "2019"
_4images_image_id: "44335"
_4images_cat_id: "3271"
_4images_user_id: "2465"
_4images_image_date: "2016-09-03T11:07:00"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44335 -->
Die zwei Seilwinden:
->Die rechts bewegt den Kranhaken.
->Die links bewegt über den Flaschenzug den Ausleger. 

Der Flaschenzug achtelt(?) die Geschwindigkeit und verstärkt das Drehmoment entsprechend. Die Rollen sind nicht mit Absicht unterschiedlich groß, ich hatte einfach nicht genug Große :-/
Unter der blauen Platte ist noch die Lagerung der Achsen von den Seilwinden.