---
layout: "image"
title: "Gesamtansicht mit nach unten gefahrenem Ausleger"
date: "2016-09-03T11:07:00"
picture: "baustellenkran2.jpg"
weight: "2"
konstrukteure: 
- "Jori"
fotografen:
- "Jori"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jori"
license: "unknown"
legacy_id:
- details/44330
imported:
- "2019"
_4images_image_id: "44330"
_4images_cat_id: "3271"
_4images_user_id: "2465"
_4images_image_date: "2016-09-03T11:07:00"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44330 -->
Wenn der Ausleger ganz nach unten gefharen ist, ist er fast waagrecht.