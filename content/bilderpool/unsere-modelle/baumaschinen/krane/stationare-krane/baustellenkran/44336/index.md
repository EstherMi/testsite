---
layout: "image"
title: "Der Kran von der Spitze aus gesehen"
date: "2016-09-03T11:07:00"
picture: "baustellenkran8.jpg"
weight: "8"
konstrukteure: 
- "Jori"
fotografen:
- "Jori"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jori"
license: "unknown"
legacy_id:
- details/44336
imported:
- "2019"
_4images_image_id: "44336"
_4images_cat_id: "3271"
_4images_user_id: "2465"
_4images_image_date: "2016-09-03T11:07:00"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44336 -->
Hier sieht man auch die Flugwarnleuchte und das ich schon wieder vergessen habe sie zu verkabeln :D