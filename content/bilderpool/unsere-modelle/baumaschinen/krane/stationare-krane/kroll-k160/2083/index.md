---
layout: "image"
title: "kroll k 160 turmkran"
date: "2004-01-27T14:00:44"
picture: "honjo_kroll_160_5.jpg"
weight: "2"
konstrukteure: 
- "honjo1"
fotografen:
- "honjo1"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "honjo1"
license: "unknown"
legacy_id:
- details/2083
imported:
- "2019"
_4images_image_id: "2083"
_4images_cat_id: "227"
_4images_user_id: "14"
_4images_image_date: "2004-01-27T14:00:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2083 -->
