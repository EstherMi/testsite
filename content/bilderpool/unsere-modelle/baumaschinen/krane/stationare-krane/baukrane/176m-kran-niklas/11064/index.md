---
layout: "image"
title: "Kran 1,76m"
date: "2007-07-15T17:48:59"
picture: "kran2.jpg"
weight: "2"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- details/11064
imported:
- "2019"
_4images_image_id: "11064"
_4images_cat_id: "1002"
_4images_user_id: "557"
_4images_image_date: "2007-07-15T17:48:59"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11064 -->
Hier der Ausleger in der mitte