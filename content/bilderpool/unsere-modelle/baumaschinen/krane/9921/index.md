---
layout: "image"
title: "Obendreher, Seite"
date: "2007-04-03T17:33:56"
picture: "Kran01.jpg"
weight: "17"
konstrukteure: 
- "Paul und Tobias"
fotografen:
- "Paul"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ROBO Paul"
license: "unknown"
legacy_id:
- details/9921
imported:
- "2019"
_4images_image_id: "9921"
_4images_cat_id: "609"
_4images_user_id: "459"
_4images_image_date: "2007-04-03T17:33:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9921 -->
Unser neuer Obendreher von der Seite.