---
layout: "image"
title: "Lenkung (4)"
date: "2016-08-01T19:00:30"
picture: "mlkn05.jpg"
weight: "7"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/44073
imported:
- "2019"
_4images_image_id: "44073"
_4images_cat_id: "3263"
_4images_user_id: "2228"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44073 -->
Lenkmodus: Straßenlenkung, zweite Achse schlägt nicht ein, die dritte nur verzögert