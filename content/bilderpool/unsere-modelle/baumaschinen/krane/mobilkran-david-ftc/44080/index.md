---
layout: "image"
title: "Kabine"
date: "2016-08-01T19:00:30"
picture: "mlkn12.jpg"
weight: "14"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/44080
imported:
- "2019"
_4images_image_id: "44080"
_4images_cat_id: "3263"
_4images_user_id: "2228"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44080 -->
Führerhaus mit LEDs: https://ftcommunity.de/categories.php?cat_id=3161