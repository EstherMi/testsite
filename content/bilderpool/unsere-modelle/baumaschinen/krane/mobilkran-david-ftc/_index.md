---
layout: "overview"
title: "Mobilkran (david-ftc)"
date: 2019-12-17T19:13:12+01:00
legacy_id:
- categories/3263
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3263 --> 
Modell eines Liebherr LTM 1060 Mobilkran
Funktionen:
- Allradlenkung
- Allradantrieb
- Drehbarer Aufbau mit Teleskopausleger