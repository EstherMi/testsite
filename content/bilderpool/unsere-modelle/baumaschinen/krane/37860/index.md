---
layout: "image"
title: "Detail Kran mit Greifer"
date: "2013-11-29T21:55:24"
picture: "IMG_2908.jpg"
weight: "13"
konstrukteure: 
- "Lukas Graber"
fotografen:
- "Lukas Graber"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "LuGra"
license: "unknown"
legacy_id:
- details/37860
imported:
- "2019"
_4images_image_id: "37860"
_4images_cat_id: "609"
_4images_user_id: "1767"
_4images_image_date: "2013-11-29T21:55:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37860 -->
Teil der Übersetzung des Mechanismus der Turmdrehung.