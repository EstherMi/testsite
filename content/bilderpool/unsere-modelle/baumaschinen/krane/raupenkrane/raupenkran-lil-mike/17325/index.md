---
layout: "image"
title: "gesamt8"
date: "2009-02-07T23:09:43"
picture: "raupenkran008.jpg"
weight: "3"
konstrukteure: 
- "lil-mike"
fotografen:
- "lil-mike"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lil-mike"
license: "unknown"
legacy_id:
- details/17325
imported:
- "2019"
_4images_image_id: "17325"
_4images_cat_id: "1559"
_4images_user_id: "822"
_4images_image_date: "2009-02-07T23:09:43"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17325 -->
