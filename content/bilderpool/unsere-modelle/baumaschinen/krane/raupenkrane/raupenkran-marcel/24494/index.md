---
layout: "image"
title: "Raupenkran 2005"
date: "2009-07-03T09:11:46"
picture: "20050328-134201-Maximal.jpg"
weight: "3"
konstrukteure: 
- "Marcel"
fotografen:
- "Marcel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marcel"
license: "unknown"
legacy_id:
- details/24494
imported:
- "2019"
_4images_image_id: "24494"
_4images_cat_id: "1682"
_4images_user_id: "979"
_4images_image_date: "2009-07-03T09:11:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24494 -->
in maximaler Auslegung