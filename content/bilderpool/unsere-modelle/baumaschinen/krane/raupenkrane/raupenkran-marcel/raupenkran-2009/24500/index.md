---
layout: "image"
title: "Windenantrieb"
date: "2009-07-07T16:43:40"
picture: "20090628-135638-Antrieb-Winden.jpg"
weight: "5"
konstrukteure: 
- "Marcel"
fotografen:
- "Marcel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marcel"
license: "unknown"
legacy_id:
- details/24500
imported:
- "2019"
_4images_image_id: "24500"
_4images_cat_id: "1684"
_4images_user_id: "979"
_4images_image_date: "2009-07-07T16:43:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24500 -->
Der Antrieb der drei Winden für die Masten erfolgt über Powermotoren 1:50 (von Conrad).
Die Winde für den Haken erfolgt noch mit dem grauen mini-Motor. Der packt das aber nicht, daher werde ich das noch überarbeiten.