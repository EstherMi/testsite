---
layout: "image"
title: "Balast und Stützrad"
date: "2009-07-07T16:43:39"
picture: "20090628-135622-Balast.jpg"
weight: "4"
konstrukteure: 
- "Marcel"
fotografen:
- "Marcel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marcel"
license: "unknown"
legacy_id:
- details/24499
imported:
- "2019"
_4images_image_id: "24499"
_4images_cat_id: "1684"
_4images_user_id: "979"
_4images_image_date: "2009-07-07T16:43:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24499 -->
Ich hab das bisher nicht ohne diese Räder hinbekommen. Bisher hab ich noch keine Aluprofile und auch keinen richtigen Drehkranz hinbekommen, ganz ohne Fremdteile.