---
layout: "image"
title: "Fahrgestell mit Oberwagen"
date: "2009-07-07T15:56:19"
picture: "20090628-135616-Aufbau.jpg"
weight: "3"
konstrukteure: 
- "Marcel"
fotografen:
- "Marcel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marcel"
license: "unknown"
legacy_id:
- details/24498
imported:
- "2019"
_4images_image_id: "24498"
_4images_cat_id: "1684"
_4images_user_id: "979"
_4images_image_date: "2009-07-07T15:56:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24498 -->
