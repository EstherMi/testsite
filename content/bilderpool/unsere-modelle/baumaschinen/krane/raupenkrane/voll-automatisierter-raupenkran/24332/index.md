---
layout: "image"
title: "Ein weiterer Seilzug"
date: "2009-06-12T19:41:21"
picture: "cn17.jpg"
weight: "20"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- details/24332
imported:
- "2019"
_4images_image_id: "24332"
_4images_cat_id: "1665"
_4images_user_id: "969"
_4images_image_date: "2009-06-12T19:41:21"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24332 -->
Der Arm wird über einen Seilzug mit 4 bzw. 3 Windungen gezogen, damit der Motor nicht allzuviel Kraft braucht (für genaueres siehe Wikipedia -> Flaschenzug)