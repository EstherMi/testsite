---
layout: "image"
title: "Kabelwirrwar"
date: "2009-06-12T19:42:06"
picture: "cn24.jpg"
weight: "27"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- details/24339
imported:
- "2019"
_4images_image_id: "24339"
_4images_cat_id: "1665"
_4images_user_id: "969"
_4images_image_date: "2009-06-12T19:42:06"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24339 -->
Bei 16 Leitungen nach oben und 3 Motoren unten kommt etwas zusammen...