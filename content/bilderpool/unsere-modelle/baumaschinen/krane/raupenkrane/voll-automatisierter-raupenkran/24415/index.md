---
layout: "image"
title: "Spiegel-Update"
date: "2009-06-18T14:10:58"
picture: "update3.jpg"
weight: "3"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- details/24415
imported:
- "2019"
_4images_image_id: "24415"
_4images_cat_id: "1665"
_4images_user_id: "969"
_4images_image_date: "2009-06-18T14:10:58"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24415 -->
Diese Konstruktion hängt wie zuvor die Lampe allein an zwei Gelenken.
Die Lampe ist schräg (im 30° Winkel) aufgebaut, da bekanntlich der Einfallswinkel dem Ausfallwinkel gleicht. Wäre sie Senkrecht eingebaut, wäre das ziemlich Witzlos. Zuvor habe ich versucht mit 15° hin zu kommen, aber da hat der Fototransistor immer zu spät reagiert (7,5° Steine habe ich leider momentan keine...).

So funktioniert es einwandfrei und der Haken häng gerade bei rauf, bzw. runter, Bewegung, da er kein schweres Kabel mehr mit ziehen muss.