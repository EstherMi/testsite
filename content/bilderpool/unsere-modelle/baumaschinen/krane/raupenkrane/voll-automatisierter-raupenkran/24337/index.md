---
layout: "image"
title: "Der Kran aus Zwergenperspektive"
date: "2009-06-12T19:42:06"
picture: "cn22.jpg"
weight: "25"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- details/24337
imported:
- "2019"
_4images_image_id: "24337"
_4images_cat_id: "1665"
_4images_user_id: "969"
_4images_image_date: "2009-06-12T19:42:06"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24337 -->
Der Kran von unten. Sichtbar sind weiterhin die Kabel, welche Motoren und Sensoren mit Strom versorgen (insgesamt gehen 16 Leitungen nach oben, es werden wohl noch mehr werden...).