---
layout: "image"
title: "Die Laufkatze"
date: "2009-06-12T19:41:20"
picture: "cn12.jpg"
weight: "15"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- details/24327
imported:
- "2019"
_4images_image_id: "24327"
_4images_cat_id: "1665"
_4images_user_id: "969"
_4images_image_date: "2009-06-12T19:41:20"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24327 -->
Die Katze. Sie läuft und kann heben (greifen soll sie auch noch "lernen", aber dafür fehlt mir momentan die Zeit). Das Heben zu motorisieren war garnicht so einfach, da die alten Motoren nicht so viele Anbaumöglichkeiten haben. Geklappt hat es trotzdem :)