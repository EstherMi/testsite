---
layout: "image"
title: "Die Laufkatze"
date: "2009-06-12T19:41:20"
picture: "cn10.jpg"
weight: "13"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- details/24325
imported:
- "2019"
_4images_image_id: "24325"
_4images_cat_id: "1665"
_4images_user_id: "969"
_4images_image_date: "2009-06-12T19:41:20"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24325 -->
Dies ist der Antrieb der Laufkatze. Sie wird über einen Seilzug bewegt, da dies am leichtesten für mich umsetzbar war.