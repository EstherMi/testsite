---
layout: "image"
title: "Der Arm"
date: "2009-06-12T19:41:12"
picture: "cn07.jpg"
weight: "10"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- details/24322
imported:
- "2019"
_4images_image_id: "24322"
_4images_cat_id: "1665"
_4images_user_id: "969"
_4images_image_date: "2009-06-12T19:41:12"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24322 -->
Eine kleine Gesamtansicht des Arms (die Gewichte haben nicht mehr auf das Bild gepasst).