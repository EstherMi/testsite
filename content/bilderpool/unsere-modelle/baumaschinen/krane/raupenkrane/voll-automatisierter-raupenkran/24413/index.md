---
layout: "image"
title: "Spiegel-Update"
date: "2009-06-18T14:10:57"
picture: "update1.jpg"
weight: "1"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- details/24413
imported:
- "2019"
_4images_image_id: "24413"
_4images_cat_id: "1665"
_4images_user_id: "969"
_4images_image_date: "2009-06-18T14:10:57"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24413 -->
In dieser Position bleibt der Haken in etwa stehen. Dann wird das von der Lampe emittierte Licht so reflektiert, dass es den Fototransistor trifft.