---
layout: "image"
title: "Seiten-Stabilisierung"
date: "2009-06-12T19:41:11"
picture: "cn03.jpg"
weight: "6"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- details/24318
imported:
- "2019"
_4images_image_id: "24318"
_4images_cat_id: "1665"
_4images_user_id: "969"
_4images_image_date: "2009-06-12T19:41:11"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24318 -->
Damit der Kran auch einen leichten "Anschubser" übersteht und nicht gleich umfällt, sind Seilwinden unten und oben befestigt.