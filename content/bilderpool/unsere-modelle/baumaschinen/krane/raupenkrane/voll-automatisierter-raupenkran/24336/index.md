---
layout: "image"
title: "Motorisierung des Arms"
date: "2009-06-12T19:42:06"
picture: "cn21.jpg"
weight: "24"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- details/24336
imported:
- "2019"
_4images_image_id: "24336"
_4images_cat_id: "1665"
_4images_user_id: "969"
_4images_image_date: "2009-06-12T19:42:06"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24336 -->
Man sieht einen weiteren Motor. Dieser ist für die Stellung des Armes zuständig