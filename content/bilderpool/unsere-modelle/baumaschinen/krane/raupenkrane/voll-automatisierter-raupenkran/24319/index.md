---
layout: "image"
title: "Kabelwirrwar"
date: "2009-06-12T19:41:12"
picture: "cn04.jpg"
weight: "7"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- details/24319
imported:
- "2019"
_4images_image_id: "24319"
_4images_cat_id: "1665"
_4images_user_id: "969"
_4images_image_date: "2009-06-12T19:41:12"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24319 -->
Da ein Großteil der Sensoren und Motoren sich im Arm des Krans befinden, herrscht hier unten ein bisschen Chaos.