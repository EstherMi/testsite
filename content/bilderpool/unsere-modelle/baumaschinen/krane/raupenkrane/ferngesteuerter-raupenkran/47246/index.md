---
layout: "image"
title: "Draufsicht Raupenkran"
date: "2018-02-05T21:19:01"
picture: "Raupenkran-1.jpg"
weight: "1"
konstrukteure: 
- "Birne"
fotografen:
- "Birne"
keywords: ["Raupenkran", "Fernsteuerung"]
uploadBy: "Birne"
license: "unknown"
legacy_id:
- details/47246
imported:
- "2019"
_4images_image_id: "47246"
_4images_cat_id: "3497"
_4images_user_id: "1142"
_4images_image_date: "2018-02-05T21:19:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47246 -->
Sohnmann (2,5 J.) fragte nach einem Kran und so wurde es endlich Zeit, mal wieder etwas größeres zu bauen. Entstanden ist dieser Raupenkran, der zwar nicht bis in Detail ausgetüftelt ist, uns aber viel Freude bereitete. Bevor er nun bald etwas neuem weichen muss, soll er hier dokumentiert werden...