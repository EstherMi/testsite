---
layout: "image"
title: "Ferngesteuerter Raupenkran (Detailansicht Krantechnik)"
date: "2018-02-05T21:19:01"
picture: "Raupenkran-3.jpg"
weight: "3"
konstrukteure: 
- "Birne"
fotografen:
- "Birne"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Birne"
license: "unknown"
legacy_id:
- details/47248
imported:
- "2019"
_4images_image_id: "47248"
_4images_cat_id: "3497"
_4images_user_id: "1142"
_4images_image_date: "2018-02-05T21:19:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47248 -->
Das Gegengewicht besteht aus zwei FT-Akkus und einer Box mit Murmeln. Ein altersschwacher Akku übernimmt dabei die Beleuchtung, welche nicht ferngesteuert werden kann und der andere Akku übernimmt Fernsteuerung und die Motoren.
Ein alter M-Motor ist für die Seilwinde der Kippvorrichtung zuständig und ein XS-Motor wickelt das eigentliche Kranseil auf.
Direkt am Kippgelenk unterhalb der Ausleger sind Taster zu sehen, welche die jeweiligen maximalen Positionen des Auslegers markieren (senkrecht wie horizontal). Werden die Taster durch den Ausleger gedrückt, leuchten die direkt am Gelenk befindlichen Lampen und ein Summer ertönt.