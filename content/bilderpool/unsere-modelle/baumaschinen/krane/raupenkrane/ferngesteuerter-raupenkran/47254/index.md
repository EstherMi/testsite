---
layout: "image"
title: "Ferngesteuerter Raupenkran (Unterseite)"
date: "2018-02-05T22:39:09"
picture: "Raupenkran-9.jpg"
weight: "9"
konstrukteure: 
- "Birne"
fotografen:
- "Birne"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Birne"
license: "unknown"
legacy_id:
- details/47254
imported:
- "2019"
_4images_image_id: "47254"
_4images_cat_id: "3497"
_4images_user_id: "1142"
_4images_image_date: "2018-02-05T22:39:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47254 -->
Übersicht über die recht simple und leere Fahr- und Unterkonstruktion des Kranes.