---
layout: "image"
title: "Ferngesteuerter Raupenkran (Rückansicht)"
date: "2018-02-05T22:39:08"
picture: "Raupenkran-6.jpg"
weight: "6"
konstrukteure: 
- "Birne"
fotografen:
- "Birne"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Birne"
license: "unknown"
legacy_id:
- details/47251
imported:
- "2019"
_4images_image_id: "47251"
_4images_cat_id: "3497"
_4images_user_id: "1142"
_4images_image_date: "2018-02-05T22:39:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47251 -->
Gut zu sehen sind das beschriebene Gegengewicht, sowie die Motoren mit Winden und Seilführung. Als Signalleuchten (oben) dienen Rainbow LEDs unter orangenen Leuchtkappen.