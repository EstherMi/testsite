---
layout: "image"
title: "Raupenkran und Rechner"
date: "2007-10-25T16:46:27"
picture: "DKelek.jpg"
weight: "14"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- details/12309
imported:
- "2019"
_4images_image_id: "12309"
_4images_cat_id: "834"
_4images_user_id: "438"
_4images_image_date: "2007-10-25T16:46:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12309 -->
Der Drehkranz mit den darunterliegen Treiberboards für 8 Motorkanäle und seinen 2 Antriebsmotoren.