---
layout: "image"
title: "Belastungsproben"
date: "2007-11-03T12:52:36"
picture: "021107A.jpg"
weight: "8"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- details/12381
imported:
- "2019"
_4images_image_id: "12381"
_4images_cat_id: "834"
_4images_user_id: "438"
_4images_image_date: "2007-11-03T12:52:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12381 -->
Hier ist das Gerät mit 1,25 Kg am Haken, Gesamthöhe 1,98m
Zu Zeit hängt er noch an einem Kabel über das die Steuerdaten übertragen werden, dies sollte jedoch in Kürze einem Funkmodul weichen.
Die Stromversorgung übernimmt ein 10,8V Akku (9 Zellen) mit 1,2Ah