---
layout: "image"
title: "Raupenkran mit Teleskopausleger"
date: "2007-01-08T16:53:44"
picture: "DSC03564.jpg"
weight: "23"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- details/8333
imported:
- "2019"
_4images_image_id: "8333"
_4images_cat_id: "834"
_4images_user_id: "438"
_4images_image_date: "2007-01-08T16:53:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8333 -->
