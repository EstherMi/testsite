---
layout: "image"
title: "Belastungstests"
date: "2007-11-11T08:33:12"
picture: "DSC03920.jpg"
weight: "2"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- details/12623
imported:
- "2019"
_4images_image_id: "12623"
_4images_cat_id: "834"
_4images_user_id: "438"
_4images_image_date: "2007-11-11T08:33:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12623 -->
Die Devise heisst nach wie vor, fahren bis zum Materialbruch.
Hier mit dem neuen Gittermastausleger und 0.5Kg in 2,1m Entfernung vom Drehpunkt am Haken.