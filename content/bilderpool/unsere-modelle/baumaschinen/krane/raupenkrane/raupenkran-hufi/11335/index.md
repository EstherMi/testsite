---
layout: "image"
title: "Raupenkran"
date: "2007-08-10T17:07:04"
picture: "new2.jpg"
weight: "19"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- details/11335
imported:
- "2019"
_4images_image_id: "11335"
_4images_cat_id: "834"
_4images_user_id: "438"
_4images_image_date: "2007-08-10T17:07:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11335 -->
0.25Kg in 1.55m Entfernung