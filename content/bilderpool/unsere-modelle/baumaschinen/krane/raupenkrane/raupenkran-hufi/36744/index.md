---
layout: "image"
title: "Raupenkran  5Kg Hub Videolink"
date: "2013-03-10T17:46:49"
picture: "DSC00004.jpg"
weight: "1"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- details/36744
imported:
- "2019"
_4images_image_id: "36744"
_4images_cat_id: "834"
_4images_user_id: "438"
_4images_image_date: "2013-03-10T17:46:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36744 -->
Das Modell macht so richtig Freude.
Link zu einem 5 Kg Hub 
http://youtu.be/L6LLoVWLyrw
Eigengewicht des Modells 5Kg 
Zusatzballast bis 2.5Kg 
Hubleistung Teleskop eingefahren Höhe 70cm: bis 6Kg
Teleskop voll ausgefahren Höhe 125cm bis 3Kg (Teleskop bleibt mit dem Gewicht verfahrbar)
Mit Zusatzausleger Höhe 200cm bis 1.25Kg
9 Motoren, 4 x im Fahrwerk, 2 x für die Drehung, 3 x Seilwinden, 1 x Teleskopantrieb
Alle Funktionen sind via 4 Stk. XY Joysticks steuerbar, die Daten werden seriell zum Modell übertragen. Am Kran werden alle Motoren via PWM von 0-100% entsprechend der Joystickbewegung gefahren.