---
layout: "image"
title: "Raupenkran & Rechner"
date: "2007-10-25T22:06:54"
picture: "DSC03843.jpg"
weight: "15"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- details/12315
imported:
- "2019"
_4images_image_id: "12315"
_4images_cat_id: "834"
_4images_user_id: "438"
_4images_image_date: "2007-10-25T22:06:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12315 -->
