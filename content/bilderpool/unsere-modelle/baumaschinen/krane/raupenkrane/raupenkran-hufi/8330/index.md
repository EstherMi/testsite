---
layout: "image"
title: "Raupenkran mit teleskop Ausleger"
date: "2007-01-08T16:53:44"
picture: "DSC03576.jpg"
weight: "20"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- details/8330
imported:
- "2019"
_4images_image_id: "8330"
_4images_cat_id: "834"
_4images_user_id: "438"
_4images_image_date: "2007-01-08T16:53:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8330 -->
Eigengewicht 3,8Kg
max Hubhöhe 1m
max Hubkraft 4,3Kg
6 Motoren
Schubkraft des Auslegers 1kg