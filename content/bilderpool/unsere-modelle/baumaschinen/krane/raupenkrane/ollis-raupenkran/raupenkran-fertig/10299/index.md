---
layout: "image"
title: "Fahrgestell"
date: "2007-05-05T21:03:30"
picture: "raupenkranolli07.jpg"
weight: "15"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/10299
imported:
- "2019"
_4images_image_id: "10299"
_4images_cat_id: "936"
_4images_user_id: "504"
_4images_image_date: "2007-05-05T21:03:30"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10299 -->
Das ist das Fahrgestell.