---
layout: "image"
title: "Hauptmast"
date: "2007-05-05T21:04:15"
picture: "raupenkranolli18.jpg"
weight: "26"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/10310
imported:
- "2019"
_4images_image_id: "10310"
_4images_cat_id: "936"
_4images_user_id: "504"
_4images_image_date: "2007-05-05T21:04:15"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10310 -->
Der Hauptmast ist ungefähr 2,20m lang.