---
layout: "image"
title: "Mast"
date: "2007-05-19T09:12:25"
picture: "raupenkranolli3.jpg"
weight: "4"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/10457
imported:
- "2019"
_4images_image_id: "10457"
_4images_cat_id: "936"
_4images_user_id: "504"
_4images_image_date: "2007-05-19T09:12:25"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10457 -->
Hier der Mast aus der Luft.