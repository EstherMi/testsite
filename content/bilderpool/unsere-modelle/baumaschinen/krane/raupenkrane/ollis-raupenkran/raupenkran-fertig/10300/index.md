---
layout: "image"
title: "Gesamtansicht"
date: "2007-05-05T21:03:30"
picture: "raupenkranolli08.jpg"
weight: "16"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/10300
imported:
- "2019"
_4images_image_id: "10300"
_4images_cat_id: "936"
_4images_user_id: "504"
_4images_image_date: "2007-05-05T21:03:30"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10300 -->
