---
layout: "image"
title: "Kran"
date: "2007-05-05T21:03:30"
picture: "raupenkranolli05.jpg"
weight: "13"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/10297
imported:
- "2019"
_4images_image_id: "10297"
_4images_cat_id: "936"
_4images_user_id: "504"
_4images_image_date: "2007-05-05T21:03:30"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10297 -->
Das hier ist der Kran draußen aufgebaut. Wenn man ihn so aufbaut ist der Ausleger 3,30m lang.