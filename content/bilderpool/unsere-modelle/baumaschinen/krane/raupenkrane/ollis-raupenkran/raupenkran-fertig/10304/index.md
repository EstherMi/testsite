---
layout: "image"
title: "Wippspitze"
date: "2007-05-05T21:04:14"
picture: "raupenkranolli12.jpg"
weight: "20"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/10304
imported:
- "2019"
_4images_image_id: "10304"
_4images_cat_id: "936"
_4images_user_id: "504"
_4images_image_date: "2007-05-05T21:04:14"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10304 -->
Wippspitze.