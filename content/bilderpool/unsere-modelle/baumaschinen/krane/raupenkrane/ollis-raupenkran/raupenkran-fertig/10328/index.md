---
layout: "image"
title: "Kran 2. Version"
date: "2007-05-06T21:37:11"
picture: "raupenkran1.jpg"
weight: "6"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/10328
imported:
- "2019"
_4images_image_id: "10328"
_4images_cat_id: "936"
_4images_user_id: "504"
_4images_image_date: "2007-05-06T21:37:11"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10328 -->
Das hier ist der gleiche Kran. Der einzige Unterschied ist das der Hauptmast nur halb so lang ist.