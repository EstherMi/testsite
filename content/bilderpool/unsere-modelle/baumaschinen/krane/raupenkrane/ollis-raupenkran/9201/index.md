---
layout: "image"
title: "Neue Strebe"
date: "2007-03-01T19:40:30"
picture: "DSCI0004.jpg"
weight: "1"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: ["Raupenkran"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/9201
imported:
- "2019"
_4images_image_id: "9201"
_4images_cat_id: "832"
_4images_user_id: "504"
_4images_image_date: "2007-03-01T19:40:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9201 -->
Das hier ist die neue Strebe zum Halten des Hauptauslegers. Da bei der alten Strebe die langen S-Riegel sehr einseitg belastet waren habe ich diese neue Konstruktion die man lustigerweise wie eine Ziehamonika zusammneschieben kann gebaut.