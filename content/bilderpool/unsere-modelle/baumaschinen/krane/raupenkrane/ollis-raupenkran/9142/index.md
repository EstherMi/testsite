---
layout: "image"
title: "Wippspitze von Raupenkran"
date: "2007-02-24T13:52:51"
picture: "DSCI0059.jpg"
weight: "5"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: ["Raupenkran"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/9142
imported:
- "2019"
_4images_image_id: "9142"
_4images_cat_id: "832"
_4images_user_id: "504"
_4images_image_date: "2007-02-24T13:52:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9142 -->
Das ist die Wippspitze. Einen Kranhaken muss ich erstnochmal konstruieren. Die Seilwinde für die Steuerung der Wippspitze ist über dem Anlenkstück untergebracht.