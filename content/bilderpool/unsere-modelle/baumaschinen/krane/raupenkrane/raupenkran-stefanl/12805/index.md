---
layout: "image"
title: "Raupenkran 28"
date: "2007-11-24T12:18:23"
picture: "raupenkran07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/12805
imported:
- "2019"
_4images_image_id: "12805"
_4images_cat_id: "1137"
_4images_user_id: "502"
_4images_image_date: "2007-11-24T12:18:23"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12805 -->
