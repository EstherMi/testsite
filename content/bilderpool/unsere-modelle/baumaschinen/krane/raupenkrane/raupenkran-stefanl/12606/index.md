---
layout: "image"
title: "Raupenkran 6"
date: "2007-11-10T16:43:58"
picture: "raupenkranstefanl06.jpg"
weight: "16"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/12606
imported:
- "2019"
_4images_image_id: "12606"
_4images_cat_id: "1137"
_4images_user_id: "502"
_4images_image_date: "2007-11-10T16:43:58"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12606 -->
In dieser Position ist er 1,8m hoch.