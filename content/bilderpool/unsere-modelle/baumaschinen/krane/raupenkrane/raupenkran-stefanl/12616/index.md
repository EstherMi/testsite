---
layout: "image"
title: "Raupenkran 16"
date: "2007-11-10T16:43:58"
picture: "raupenkranstefanl16.jpg"
weight: "26"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/12616
imported:
- "2019"
_4images_image_id: "12616"
_4images_cat_id: "1137"
_4images_user_id: "502"
_4images_image_date: "2007-11-10T16:43:58"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12616 -->
