---
layout: "image"
title: "Raupenkran 31"
date: "2007-11-24T12:18:23"
picture: "raupenkran10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/12808
imported:
- "2019"
_4images_image_id: "12808"
_4images_cat_id: "1137"
_4images_user_id: "502"
_4images_image_date: "2007-11-24T12:18:23"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12808 -->
Hier die überarbeiteten Seiltrommeln.