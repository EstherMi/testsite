---
layout: "image"
title: "XXL Kran"
date: "2011-02-21T21:29:21"
picture: "Aufzeichnen.jpg"
weight: "14"
konstrukteure: 
- "Luca97"
fotografen:
- "Luca97"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Luca97"
license: "unknown"
legacy_id:
- details/30101
imported:
- "2019"
_4images_image_id: "30101"
_4images_cat_id: "609"
_4images_user_id: "1276"
_4images_image_date: "2011-02-21T21:29:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30101 -->
Maße
 Hauptmast: ca.1,10m
Gegenmast: ca. 60cm