---
layout: "image"
title: "Oberweagen, oben"
date: "2011-07-14T10:50:29"
picture: "kleinergittermastkran11.jpg"
weight: "11"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/31222
imported:
- "2019"
_4images_image_id: "31222"
_4images_cat_id: "2321"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31222 -->
