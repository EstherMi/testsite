---
layout: "image"
title: "vorne"
date: "2011-07-14T10:50:29"
picture: "kleinergittermastkran05.jpg"
weight: "5"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/31216
imported:
- "2019"
_4images_image_id: "31216"
_4images_cat_id: "2321"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31216 -->
