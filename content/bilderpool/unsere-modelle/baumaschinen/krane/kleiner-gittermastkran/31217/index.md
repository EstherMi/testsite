---
layout: "image"
title: "Kabine"
date: "2011-07-14T10:50:29"
picture: "kleinergittermastkran06.jpg"
weight: "6"
konstrukteure: 
- "Thilo"
fotografen:
- "Thilo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/31217
imported:
- "2019"
_4images_image_id: "31217"
_4images_cat_id: "2321"
_4images_user_id: "833"
_4images_image_date: "2011-07-14T10:50:29"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31217 -->
