---
layout: "image"
title: "Kran 05"
date: "2010-07-13T15:40:04"
picture: "kran05.jpg"
weight: "5"
konstrukteure: 
- "Endlich und sein Bruder"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/27744
imported:
- "2019"
_4images_image_id: "27744"
_4images_cat_id: "1998"
_4images_user_id: "1162"
_4images_image_date: "2010-07-13T15:40:04"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27744 -->
