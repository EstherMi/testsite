---
layout: "image"
title: "Kran 02"
date: "2010-07-13T15:40:04"
picture: "kran02.jpg"
weight: "2"
konstrukteure: 
- "Endlich & sein Bruder"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/27741
imported:
- "2019"
_4images_image_id: "27741"
_4images_cat_id: "1998"
_4images_user_id: "1162"
_4images_image_date: "2010-07-13T15:40:04"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27741 -->
