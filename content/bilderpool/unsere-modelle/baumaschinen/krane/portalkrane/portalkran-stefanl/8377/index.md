---
layout: "image"
title: "Portalkran 4"
date: "2007-01-12T17:00:32"
picture: "portalkran4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/8377
imported:
- "2019"
_4images_image_id: "8377"
_4images_cat_id: "772"
_4images_user_id: "502"
_4images_image_date: "2007-01-12T17:00:32"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8377 -->
