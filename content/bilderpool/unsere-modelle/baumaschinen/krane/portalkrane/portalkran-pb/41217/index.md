---
layout: "image"
title: "Containerkr-lr-18"
date: "2015-06-24T14:11:15"
picture: "containerkran13.jpg"
weight: "13"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/41217
imported:
- "2019"
_4images_image_id: "41217"
_4images_cat_id: "3086"
_4images_user_id: "2449"
_4images_image_date: "2015-06-24T14:11:15"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41217 -->
Das feste teil des Bahn für den Laufkatze.