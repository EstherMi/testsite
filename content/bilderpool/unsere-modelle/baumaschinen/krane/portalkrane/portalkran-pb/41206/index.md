---
layout: "image"
title: "Containerkr-lr-02"
date: "2015-06-24T14:11:15"
picture: "containerkran02.jpg"
weight: "2"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/41206
imported:
- "2019"
_4images_image_id: "41206"
_4images_cat_id: "3086"
_4images_user_id: "2449"
_4images_image_date: "2015-06-24T14:11:15"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41206 -->
Der Kran ist ungefähr 1m hoch und 1.50m Breit met gesenkter Ausleger.