---
layout: "image"
title: "Containerkr-lr-42"
date: "2015-06-24T14:11:15"
picture: "containerkran30.jpg"
weight: "30"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/41234
imported:
- "2019"
_4images_image_id: "41234"
_4images_cat_id: "3086"
_4images_user_id: "2449"
_4images_image_date: "2015-06-24T14:11:15"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41234 -->
Die Container brauchen Angfriffspunkte an die Obenseite.