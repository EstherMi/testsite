---
layout: "image"
title: "Containerkr-lr-11"
date: "2015-06-24T14:11:15"
picture: "containerkran08.jpg"
weight: "8"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/41212
imported:
- "2019"
_4images_image_id: "41212"
_4images_cat_id: "3086"
_4images_user_id: "2449"
_4images_image_date: "2015-06-24T14:11:15"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41212 -->
Ein kleinen Knick in den Säulen am Vorseite, so das er nicht umkippt.