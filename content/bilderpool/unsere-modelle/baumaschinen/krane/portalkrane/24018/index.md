---
layout: "image"
title: "Molenkran Seilsicherung"
date: "2009-05-14T16:34:27"
picture: "FTC-Seilsicherung.jpg"
weight: "4"
konstrukteure: 
- "Thomas Habig"
fotografen:
- "Thomas Habig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Triceratops"
license: "unknown"
legacy_id:
- details/24018
imported:
- "2019"
_4images_image_id: "24018"
_4images_cat_id: "837"
_4images_user_id: "59"
_4images_image_date: "2009-05-14T16:34:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24018 -->
Mit zwei Federgelenkbausteinen an der
Seilrolle wird die maximale Hebekraft
des Krans exakt definiert und ein Über-
heben - und damit die Gefahr des Um-
kippens - generell ausgeschlossen.