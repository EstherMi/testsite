---
layout: "image"
title: "Hochbett-Kran_005"
date: "2003-10-14T11:35:27"
picture: "Portalkran_Inter_002.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Kran", "Laufkran"]
uploadBy: "mworks"
license: "unknown"
legacy_id:
- details/1836
imported:
- "2019"
_4images_image_id: "1836"
_4images_cat_id: "195"
_4images_user_id: "61"
_4images_image_date: "2003-10-14T11:35:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1836 -->
Steuerung wahlweise per IR oder Interface. Wird mittels 2. IR und 5 Kleinrelais umgeschaltet.