---
layout: "image"
title: "Hochbett-Kran_003"
date: "2003-10-14T11:35:27"
picture: "Portalkran_IR_003.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Kran", "Laufkran"]
uploadBy: "mworks"
license: "unknown"
legacy_id:
- details/1834
imported:
- "2019"
_4images_image_id: "1834"
_4images_cat_id: "195"
_4images_user_id: "61"
_4images_image_date: "2003-10-14T11:35:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1834 -->
Ein Kran der per IR Kekse und andere Sachen ins Hochbett hebt.