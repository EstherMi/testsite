---
layout: "image"
title: "Molenkran Verladeszene"
date: "2009-05-14T16:34:27"
picture: "FTC-Verladen.jpg"
weight: "5"
konstrukteure: 
- "Thomas Habig"
fotografen:
- "Thomas Habig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Triceratops"
license: "unknown"
legacy_id:
- details/24019
imported:
- "2019"
_4images_image_id: "24019"
_4images_cat_id: "837"
_4images_user_id: "59"
_4images_image_date: "2009-05-14T16:34:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24019 -->
Dieses Bild zeigt eine mögliche Variante hier in Kombination
mit dem Tieflader aus "Supertrucks". Die verwendete Traverse
ist übrigens frei drehbar* und erleichtert so das Laden großer
Objekte.

*) Kombiniert mit einer abgeschliffenen Gelenkwürfel-Zunge,
die so ins Lagerstück 2 paßt (mit Lagerstück 1 ohne Rastnase
drehbar). Die abgeschliffene (begradigte) Gelenkwürfel-Zunge
paßt übrigens auch ins Rollenlager bzw. in den Rollenbock.