---
layout: "image"
title: "Portalkran von unten"
date: "2018-10-05T20:15:39"
picture: "Portalkran_Treppenhaus-1.jpg"
weight: "1"
konstrukteure: 
- "Birne"
fotografen:
- "Birne"
keywords: ["Portalkran", "Treppenhaus", "ferngesteuert"]
uploadBy: "Birne"
license: "unknown"
legacy_id:
- details/48197
imported:
- "2019"
_4images_image_id: "48197"
_4images_cat_id: "3536"
_4images_user_id: "1142"
_4images_image_date: "2018-10-05T20:15:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48197 -->
Ansicht von unten. Der Schlitten wird über ein Hubgetriebe angetrieben. Die gesamte Elektronik ist im Schlitten selber untergebracht. Nur der Akku und Schalter sind aus Gewichtsgründen auf der Treppe.