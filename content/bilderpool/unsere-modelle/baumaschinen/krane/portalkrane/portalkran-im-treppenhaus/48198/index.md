---
layout: "image"
title: "Portalkran von vorne/unten"
date: "2018-10-05T20:15:39"
picture: "Portalkran_Treppenhaus-2.jpg"
weight: "2"
konstrukteure: 
- "Birne"
fotografen:
- "Birne"
keywords: ["Portalkran", "Treppenhaus", "ferngesteuert"]
uploadBy: "Birne"
license: "unknown"
legacy_id:
- details/48198
imported:
- "2019"
_4images_image_id: "48198"
_4images_cat_id: "3536"
_4images_user_id: "1142"
_4images_image_date: "2018-10-05T20:15:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48198 -->
