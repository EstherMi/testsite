---
layout: "image"
title: "Portalkran - Schlitten von unten"
date: "2018-10-05T20:15:39"
picture: "Portalkran_Treppenhaus-5.jpg"
weight: "5"
konstrukteure: 
- "Birne"
fotografen:
- "Birne"
keywords: ["Portalkran", "Treppenhaus", "ferngesteuert"]
uploadBy: "Birne"
license: "unknown"
legacy_id:
- details/48201
imported:
- "2019"
_4images_image_id: "48201"
_4images_cat_id: "3536"
_4images_user_id: "1142"
_4images_image_date: "2018-10-05T20:15:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48201 -->
Ansicht der Winde und der Befestigung der Stromzuführung