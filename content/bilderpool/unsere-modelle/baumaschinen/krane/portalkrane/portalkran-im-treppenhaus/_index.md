---
layout: "overview"
title: "Portalkran im Treppenhaus"
date: 2019-12-17T19:12:45+01:00
legacy_id:
- categories/3536
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3536 --> 
Bevor der Kran völlig im Staub versinkt, wird er nun endlich abgebaut. Evtl. sollte ich mir angewöhnen Fotos direkt nach dem Bau zu machen...

Ziel war es einen einfach Kran zu haben, mit dem der Sohnemann kleine "Lasten" durch das Treppenhaus heben kann. Herausgekommen ist dieser ferngesteuerte Portalkran. Wir hatten viel Spaß, doch nach fast einem Jahr muss er etwas neuem weichen.