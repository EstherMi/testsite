---
layout: "image"
title: "Portalkran von oben"
date: "2018-10-05T20:15:39"
picture: "Portalkran_Treppenhaus-4.jpg"
weight: "4"
konstrukteure: 
- "Birne"
fotografen:
- "Birne"
keywords: ["Portalkran", "Treppenhaus", "ferngesteuert"]
uploadBy: "Birne"
license: "unknown"
legacy_id:
- details/48200
imported:
- "2019"
_4images_image_id: "48200"
_4images_cat_id: "3536"
_4images_user_id: "1142"
_4images_image_date: "2018-10-05T20:15:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48200 -->
Rechts der Akku mit Schalter. Die LED sind so angeschlossen, dass beim Ablassen des Seiles die rote leuchtet und beim Aufwickeln die grüne.