---
layout: "overview"
title: "Portalkran (baufischertechnik)"
date: 2019-12-17T19:12:38+01:00
legacy_id:
- categories/2931
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2931 --> 
Hier habe ich den Portalkran aus Super Cranes modifiziert und motorisiert.
Modifikationen: Neue Seilzüge, Gewichtskontolle für Lasten, Energieketten zur Kabelführung