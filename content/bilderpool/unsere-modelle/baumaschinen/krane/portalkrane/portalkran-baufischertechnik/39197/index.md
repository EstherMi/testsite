---
layout: "image"
title: "Tster mit Eigendiagnose"
date: "2014-08-08T21:21:23"
picture: "DSC00042.jpg"
weight: "9"
konstrukteure: 
- "Pascal Jan"
fotografen:
- "Pascal Jan"
keywords: ["Controller", "bauFischertechnik", "Robo", "computergesteuert", "Taster", "Portalkran", "Fischertechnik", "Ft", "Kugelsortierung", "Motor", "Roboter", "Automatisch", "Tx", "RoboPro", "Computer"]
uploadBy: "bauFischertechnik"
license: "unknown"
legacy_id:
- details/39197
imported:
- "2019"
_4images_image_id: "39197"
_4images_cat_id: "2931"
_4images_user_id: "2086"
_4images_image_date: "2014-08-08T21:21:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39197 -->
Alle Taster werden analog ausgewertet, so kann man Schnell einen Kabelbruch ausmachen, da man durch die Programmierung eine Fehlermeldung auf dem Bedienfeld angezeigt bekommt.