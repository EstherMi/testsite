---
layout: "comment"
hidden: true
title: "19352"
date: "2014-08-09T19:48:20"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Da hätte ich Fragen, Euer Ehren!
- Hat diese Sortieranlage etwas mit dem Portalkran zu tun oder sollte das in eine eigene Kategorie?
- Wer oder was entscheidet wie, ob eine Kugel "Müll" ist?
- Sind die BS30 vom Zylinder direkt beim Bandanfang geführt, oder muss der Zylinder die Querkräfte von ankommenden Kugeln auffangen?
Gruß,
Stefan