---
layout: "image"
title: "Zahnstange"
date: "2010-06-08T16:27:42"
picture: "portalkranfish2.jpg"
weight: "2"
konstrukteure: 
- "fish"
fotografen:
- "fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- details/27437
imported:
- "2019"
_4images_image_id: "27437"
_4images_cat_id: "1969"
_4images_user_id: "1113"
_4images_image_date: "2010-06-08T16:27:42"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27437 -->
Die Zahnstange an der die Laufkatze läuft.