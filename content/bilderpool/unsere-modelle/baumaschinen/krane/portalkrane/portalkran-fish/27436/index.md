---
layout: "image"
title: "Portalkran gesamt"
date: "2010-06-08T16:27:42"
picture: "portalkranfish1.jpg"
weight: "1"
konstrukteure: 
- "fish"
fotografen:
- "fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- details/27436
imported:
- "2019"
_4images_image_id: "27436"
_4images_cat_id: "1969"
_4images_user_id: "1113"
_4images_image_date: "2010-06-08T16:27:42"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27436 -->
Der Portalkran mit Ferbedienung und Modelleisenbahnschienen.