---
layout: "image"
title: "containerbrücke von jan Knobbe - greifer (2)"
date: "2008-02-01T17:44:12"
picture: "containerbruecke5.jpg"
weight: "5"
konstrukteure: 
- "jan knobbe"
fotografen:
- "simon knobbe"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jan"
license: "unknown"
legacy_id:
- details/13496
imported:
- "2019"
_4images_image_id: "13496"
_4images_cat_id: "1234"
_4images_user_id: "713"
_4images_image_date: "2008-02-01T17:44:12"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13496 -->
