---
layout: "image"
title: "containerbrücke von jan Knobbe - greifer (1)"
date: "2008-02-01T17:44:12"
picture: "containerbruecke4.jpg"
weight: "4"
konstrukteure: 
- "jan knobbe"
fotografen:
- "simon knobbe"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jan"
license: "unknown"
legacy_id:
- details/13495
imported:
- "2019"
_4images_image_id: "13495"
_4images_cat_id: "1234"
_4images_user_id: "713"
_4images_image_date: "2008-02-01T17:44:12"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13495 -->
