---
layout: "image"
title: "containerbrücke von jan Knobbe - plan des greifers"
date: "2008-02-01T17:44:25"
picture: "containerbruecke9.jpg"
weight: "9"
konstrukteure: 
- "jan knobbe"
fotografen:
- "simon knobbe"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jan"
license: "unknown"
legacy_id:
- details/13500
imported:
- "2019"
_4images_image_id: "13500"
_4images_cat_id: "1234"
_4images_user_id: "713"
_4images_image_date: "2008-02-01T17:44:25"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13500 -->
