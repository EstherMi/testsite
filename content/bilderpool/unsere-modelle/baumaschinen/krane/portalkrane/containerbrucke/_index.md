---
layout: "overview"
title: "Containerbrücke"
date: 2019-12-17T19:12:31+01:00
legacy_id:
- categories/1234
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1234 --> 
Containerbrücke von Jan Knobbe[br][br]Ausgangspunkt: Die Arbeit am Greifer. Ich bin fasziniert von den Lösungen von Dirk Kutsch, Juergen Warwel und IM000806, die auf dieser homepage zu besichtigen sind. Hut ab! Von Anfang an strebte ich allerdings einen möglichst kleinen Greifer an, um die eigentliche Brücke dann nicht zu groß werden zu lassen...[br]Der Greifer sieht zwar etwas klobig aus, bietet aber durch die halbautomatische Steuerung mit zwei Aus-Tastern einigen Komfort. Ohne Taster oder mit Mini-Tastern wäre es noch eleganter.[br]Die Brücke ist etwa im Maßstab 1 : 60 gebaut. Kleiner Wermutstropfen. Sie ist nur für 20-Fuß-Container geeignet.[br]Das Modell soll allen Mut machen, die von den faszinierenden Fotos auf dieser web-site begeistert sind, aber vielleicht nur über \"geringfügig\" weniger Bauteile verfügen...[br]