---
layout: "image"
title: "containerbrücke von jan Knobbe - katze"
date: "2008-02-01T17:44:12"
picture: "containerbruecke7.jpg"
weight: "7"
konstrukteure: 
- "jan knobbe"
fotografen:
- "simon knobbe"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jan"
license: "unknown"
legacy_id:
- details/13498
imported:
- "2019"
_4images_image_id: "13498"
_4images_cat_id: "1234"
_4images_user_id: "713"
_4images_image_date: "2008-02-01T17:44:12"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13498 -->
