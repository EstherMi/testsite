---
layout: "image"
title: "containerbrücke von jan Knobbe - antrieb"
date: "2008-02-01T17:44:12"
picture: "containerbruecke2.jpg"
weight: "2"
konstrukteure: 
- "jan knobbe"
fotografen:
- "simon knobbe"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jan"
license: "unknown"
legacy_id:
- details/13493
imported:
- "2019"
_4images_image_id: "13493"
_4images_cat_id: "1234"
_4images_user_id: "713"
_4images_image_date: "2008-02-01T17:44:12"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13493 -->
über eine Zahnkette wird der Antrieb auch auf die andere Seite der Brücke übertragen