---
layout: "image"
title: "Portalkran"
date: "2009-05-23T12:17:23"
picture: "Krane_2_010.jpg"
weight: "7"
konstrukteure: 
- "Marspau"
fotografen:
- "Andree"
keywords: ["Detail", "of", "the", "Power", "switch", "for", "the", "Robo", "Interface", "and", "the", "carriage", "motor."]
uploadBy: "marspau"
license: "unknown"
legacy_id:
- details/24090
imported:
- "2019"
_4images_image_id: "24090"
_4images_cat_id: "1651"
_4images_user_id: "416"
_4images_image_date: "2009-05-23T12:17:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24090 -->
Detail of the Power switch for the Robo Interface and the carriage motor.

Detail des Power-Schalters für die Robo-Schnittstelle und der Motor Carriage.