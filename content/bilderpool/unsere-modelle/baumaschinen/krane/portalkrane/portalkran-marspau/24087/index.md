---
layout: "image"
title: "PortalKrane"
date: "2009-05-22T20:56:48"
picture: "Krane_2_006_2.jpg"
weight: "24"
konstrukteure: 
- "Marspau"
fotografen:
- "Andree"
keywords: ["Detail", "of", "the", "lamp", "support."]
uploadBy: "marspau"
license: "unknown"
legacy_id:
- details/24087
imported:
- "2019"
_4images_image_id: "24087"
_4images_cat_id: "1651"
_4images_user_id: "416"
_4images_image_date: "2009-05-22T20:56:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24087 -->
