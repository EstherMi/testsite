---
layout: "image"
title: "PortalKrane"
date: "2009-05-22T20:56:47"
picture: "Krane_2_002_2.jpg"
weight: "20"
konstrukteure: 
- "Marspau"
fotografen:
- "Andree"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "marspau"
license: "unknown"
legacy_id:
- details/24083
imported:
- "2019"
_4images_image_id: "24083"
_4images_cat_id: "1651"
_4images_user_id: "416"
_4images_image_date: "2009-05-22T20:56:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24083 -->
Details of the opposite side of the 
kran traveling mechanism.

Details zu der gegenüberliegenden Seite des der Kran Reisen Mechanismus