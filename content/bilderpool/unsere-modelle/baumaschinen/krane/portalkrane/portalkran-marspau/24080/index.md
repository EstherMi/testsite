---
layout: "image"
title: "PortalKran"
date: "2009-05-22T14:40:02"
picture: "Krane_2_008.jpg"
weight: "17"
konstrukteure: 
- "Marspau"
fotografen:
- "Andree"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "marspau"
license: "unknown"
legacy_id:
- details/24080
imported:
- "2019"
_4images_image_id: "24080"
_4images_cat_id: "1651"
_4images_user_id: "416"
_4images_image_date: "2009-05-22T14:40:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24080 -->
