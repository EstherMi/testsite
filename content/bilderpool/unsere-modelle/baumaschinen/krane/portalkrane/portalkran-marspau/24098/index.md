---
layout: "image"
title: "Portalkran"
date: "2009-05-23T12:17:24"
picture: "Kranr_3_001.jpg"
weight: "15"
konstrukteure: 
- "Marspau"
fotografen:
- "Andree"
keywords: ["View", "of", "underside", "of", "the", "travelling", "carriage", "with", "pulleys", "for", "the", "hook"]
uploadBy: "marspau"
license: "unknown"
legacy_id:
- details/24098
imported:
- "2019"
_4images_image_id: "24098"
_4images_cat_id: "1651"
_4images_user_id: "416"
_4images_image_date: "2009-05-23T12:17:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24098 -->
View of underside of the travelling carriage with pulleys for the hook.

Ansicht der Unterseite der fahrenden Beförderung mit Riemenscheiben für den Haken