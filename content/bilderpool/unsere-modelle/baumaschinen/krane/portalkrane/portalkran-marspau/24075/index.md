---
layout: "image"
title: "PortalKrane"
date: "2009-05-21T22:29:48"
picture: "Krane_2_003.jpg"
weight: "29"
konstrukteure: 
- "Marspau"
fotografen:
- "Andree"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "marspau"
license: "unknown"
legacy_id:
- details/24075
imported:
- "2019"
_4images_image_id: "24075"
_4images_cat_id: "1651"
_4images_user_id: "416"
_4images_image_date: "2009-05-21T22:29:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24075 -->
