---
layout: "image"
title: "Portalkran"
date: "2009-05-23T12:17:24"
picture: "Krane_2_022.jpg"
weight: "14"
konstrukteure: 
- "Marspau"
fotografen:
- "Andree"
keywords: ["Kran", "traveling", "mechanism."]
uploadBy: "marspau"
license: "unknown"
legacy_id:
- details/24097
imported:
- "2019"
_4images_image_id: "24097"
_4images_cat_id: "1651"
_4images_user_id: "416"
_4images_image_date: "2009-05-23T12:17:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24097 -->
Kran traveling mechanism.


Kran Reisen Mechanismus