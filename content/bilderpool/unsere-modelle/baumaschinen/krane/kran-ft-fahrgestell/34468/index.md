---
layout: "image"
title: "08 Unten Gesamt"
date: "2012-02-27T18:13:21"
picture: "kranaufftfahrgestell08.jpg"
weight: "15"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/34468
imported:
- "2019"
_4images_image_id: "34468"
_4images_cat_id: "2547"
_4images_user_id: "860"
_4images_image_date: "2012-02-27T18:13:21"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34468 -->
der untere Teil mit gedrehtem Mast