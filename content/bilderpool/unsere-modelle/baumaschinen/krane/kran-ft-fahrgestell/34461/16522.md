---
layout: "comment"
hidden: true
title: "16522"
date: "2012-02-27T20:54:20"
uploadBy:
- "-Matthias-"
license: "unknown"
imported:
- "2019"
---
Der Akku hinten dient als Gewicht, ggf. noch die beiden Seilmotoren. Das reicht, um nicht allzu schwere Dinge anzuheben.