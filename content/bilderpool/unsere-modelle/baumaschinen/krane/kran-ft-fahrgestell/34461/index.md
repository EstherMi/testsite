---
layout: "image"
title: "01 Kran auf ft-Fahrgestell"
date: "2012-02-27T18:13:21"
picture: "kranaufftfahrgestell01.jpg"
weight: "8"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/34461
imported:
- "2019"
_4images_image_id: "34461"
_4images_cat_id: "2547"
_4images_user_id: "860"
_4images_image_date: "2012-02-27T18:13:21"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34461 -->
Mit diesem Kran habe ich viele meiner Ideen und Wünsche in ein Modell umgesetzt:

- Ein vernünftiger Kran auf dem original ft-Fahrgestell aus dem Baukasten Bulldozer

- Ein Mast aus nur 3 "Rippen" habe ich schon öfters gesehen, mit ft allerdings noch nicht. Die Umsetzung war auch nicht ganz einfach, dazu später aber mehr.

- Die Verwendung meiner neuen Schalter zur Steuerung (auf dem Steuerpult)

- Und die Verwendung der LEDs, die ich mir letztens gebaut hatte