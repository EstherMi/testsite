---
layout: "image"
title: "32 Baustein"
date: "2012-02-27T18:13:21"
picture: "kranaufftfahrgestell32.jpg"
weight: "39"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/34492
imported:
- "2019"
_4images_image_id: "34492"
_4images_cat_id: "2547"
_4images_user_id: "860"
_4images_image_date: "2012-02-27T18:13:21"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34492 -->
von der Seite - er ist genauso dick und doppelt so lang wie ein BS 7,5