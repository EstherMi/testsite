---
layout: "image"
title: "38 Drehkranz"
date: "2012-04-01T17:12:16"
picture: "kran5.jpg"
weight: "5"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/34740
imported:
- "2019"
_4images_image_id: "34740"
_4images_cat_id: "2547"
_4images_user_id: "860"
_4images_image_date: "2012-04-01T17:12:16"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34740 -->
.