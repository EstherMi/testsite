---
layout: "image"
title: "19 Beleuchtung"
date: "2012-02-27T18:13:21"
picture: "kranaufftfahrgestell19.jpg"
weight: "26"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/34479
imported:
- "2019"
_4images_image_id: "34479"
_4images_cat_id: "2547"
_4images_user_id: "860"
_4images_image_date: "2012-02-27T18:13:21"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34479 -->
So hell sind die LEDs nicht, das lässt sich nur leider nicht so gut fotografieren.
Man wird nicht blind, wenn man frontal reinschaut, aber gesund ist es nicht.
Oben am Mast hängen noch zwei weitere.