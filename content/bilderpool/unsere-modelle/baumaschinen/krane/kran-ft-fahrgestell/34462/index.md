---
layout: "image"
title: "02 Gesamt"
date: "2012-02-27T18:13:21"
picture: "kranaufftfahrgestell02.jpg"
weight: "9"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/34462
imported:
- "2019"
_4images_image_id: "34462"
_4images_cat_id: "2547"
_4images_user_id: "860"
_4images_image_date: "2012-02-27T18:13:21"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34462 -->
- max. Höhe: 1,15m
- 5 Motoren + 8 LEDs
- hebt locker einen ft-Akku
