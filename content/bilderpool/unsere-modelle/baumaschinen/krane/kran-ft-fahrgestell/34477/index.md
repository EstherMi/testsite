---
layout: "image"
title: "17 Drehkranz"
date: "2012-02-27T18:13:21"
picture: "kranaufftfahrgestell17.jpg"
weight: "24"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/34477
imported:
- "2019"
_4images_image_id: "34477"
_4images_cat_id: "2547"
_4images_user_id: "860"
_4images_image_date: "2012-02-27T18:13:21"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34477 -->
Der Moter zum Drehen