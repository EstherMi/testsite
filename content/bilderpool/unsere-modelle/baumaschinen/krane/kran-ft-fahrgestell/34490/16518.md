---
layout: "comment"
hidden: true
title: "16518"
date: "2012-02-27T19:38:47"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Das ist ein Unterteil zum Ur-Mini-Motor (in Baustein-30-Größe von 1969!). Der Motor konnte in die Nut eingeschoben und mit den Zapfen irgendwo befestigt werden, so dass man den Motor dann längs verschieben und verdrehen konnte, um ihn genau zu justieren. So eine ähnliche Platte hatte der graue Ur-ft-Motor mot 1 gleich fest angebaut.

Gruß,
Stefan