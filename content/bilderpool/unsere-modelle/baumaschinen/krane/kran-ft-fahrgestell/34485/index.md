---
layout: "image"
title: "25 Steuerpult"
date: "2012-02-27T18:13:21"
picture: "kranaufftfahrgestell25.jpg"
weight: "32"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/34485
imported:
- "2019"
_4images_image_id: "34485"
_4images_cat_id: "2547"
_4images_user_id: "860"
_4images_image_date: "2012-02-27T18:13:21"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34485 -->
Das ist das Steuerpult, bestehend aus:

- Interface + Extension

- Joystick zum fahren

- Schalter zum steuern des Krans + Lichter