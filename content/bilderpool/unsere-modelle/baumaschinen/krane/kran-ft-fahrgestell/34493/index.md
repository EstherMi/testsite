---
layout: "image"
title: "33 Gelenk im Joystick"
date: "2012-02-28T15:41:19"
picture: "Gelenkstein_800x315.jpg"
weight: "7"
konstrukteure: 
- "Matthias"
fotografen:
- "Matthias"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/34493
imported:
- "2019"
_4images_image_id: "34493"
_4images_cat_id: "2547"
_4images_user_id: "860"
_4images_image_date: "2012-02-28T15:41:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34493 -->
dieser Gelenkstein befindet sich im Joystick