---
layout: "comment"
hidden: true
title: "16539"
date: "2012-02-29T09:03:31"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Vielen Dank für das Bild!

Das ist doch das neue (alte) Kardangelenk, dass jetzt auch im Pneumatic 3 kommt. Wir hatten's im Forum davon:

http://forum.ftcommunity.de/viewtopic.php?f=4&t=1126

Mir gefällt's! Kann man super gebrauchen.

Gruß, Thomas