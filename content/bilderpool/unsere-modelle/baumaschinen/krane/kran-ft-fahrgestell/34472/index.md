---
layout: "image"
title: "12 Oben"
date: "2012-02-27T18:13:21"
picture: "kranaufftfahrgestell12.jpg"
weight: "19"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/34472
imported:
- "2019"
_4images_image_id: "34472"
_4images_cat_id: "2547"
_4images_user_id: "860"
_4images_image_date: "2012-02-27T18:13:21"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34472 -->
so sieht es oben aus