---
layout: "image"
title: "21 Mast"
date: "2012-02-27T18:13:21"
picture: "kranaufftfahrgestell21.jpg"
weight: "28"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/34481
imported:
- "2019"
_4images_image_id: "34481"
_4images_cat_id: "2547"
_4images_user_id: "860"
_4images_image_date: "2012-02-27T18:13:21"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34481 -->
Ein Akku hebt er ohne Mühen.