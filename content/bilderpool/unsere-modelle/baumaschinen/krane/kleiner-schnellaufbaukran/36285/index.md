---
layout: "image"
title: "Gesamtansicht"
date: "2012-12-16T18:21:03"
picture: "kleinerschnellaufbaukran01.jpg"
weight: "1"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- details/36285
imported:
- "2019"
_4images_image_id: "36285"
_4images_cat_id: "2694"
_4images_user_id: "1122"
_4images_image_date: "2012-12-16T18:21:03"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36285 -->
Als Vorlage hat der Mk80 von Liebherr gedient.
Verbaut sind 7 Motoren:
-5 Mini Motoren
-2 Power Motoren

Ich hab die gleiche Fernbedienung verwendet, wie bei meinem kleinen Raupenkran.