---
layout: "image"
title: "Gesamtansicht"
date: "2012-12-16T18:21:03"
picture: "kleinerschnellaufbaukran19.jpg"
weight: "19"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- details/36303
imported:
- "2019"
_4images_image_id: "36303"
_4images_cat_id: "2694"
_4images_user_id: "1122"
_4images_image_date: "2012-12-16T18:21:03"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36303 -->
-