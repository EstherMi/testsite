---
layout: "image"
title: "Gesamtansicht vollständig aufgebaut"
date: "2012-12-16T18:21:03"
picture: "kleinerschnellaufbaukran20.jpg"
weight: "20"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- details/36304
imported:
- "2019"
_4images_image_id: "36304"
_4images_cat_id: "2694"
_4images_user_id: "1122"
_4images_image_date: "2012-12-16T18:21:03"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36304 -->
-