---
layout: "image"
title: "der Ausleger ausgefahren"
date: "2011-03-15T18:50:59"
picture: "minikranwagen8.jpg"
weight: "8"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- details/30260
imported:
- "2019"
_4images_image_id: "30260"
_4images_cat_id: "2249"
_4images_user_id: "1122"
_4images_image_date: "2011-03-15T18:50:59"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30260 -->
Der Motor fährt den Ausleger über eine Zahnstange hoch.