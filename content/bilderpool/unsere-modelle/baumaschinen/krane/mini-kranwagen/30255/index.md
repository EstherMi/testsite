---
layout: "image"
title: "von vorne"
date: "2011-03-15T18:50:59"
picture: "minikranwagen3.jpg"
weight: "3"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- details/30255
imported:
- "2019"
_4images_image_id: "30255"
_4images_cat_id: "2249"
_4images_user_id: "1122"
_4images_image_date: "2011-03-15T18:50:59"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30255 -->
Die Steuerung sitzt in der Fahrerkabine