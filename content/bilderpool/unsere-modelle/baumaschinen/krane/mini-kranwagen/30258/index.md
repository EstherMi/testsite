---
layout: "image"
title: "Hinterteil von oben (ohne Gegengewicht)"
date: "2011-03-15T18:50:59"
picture: "minikranwagen6.jpg"
weight: "6"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- details/30258
imported:
- "2019"
_4images_image_id: "30258"
_4images_cat_id: "2249"
_4images_user_id: "1122"
_4images_image_date: "2011-03-15T18:50:59"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30258 -->
Rechts sieht man den Motor der für das Drehen des Hinterenteils zuständig ist.