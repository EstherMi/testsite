---
layout: "comment"
hidden: true
title: "18487"
date: "2013-11-30T18:22:46"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Verstehe ich das richtig? Da gehst Du mit den feinen Zähnen nach dem U-Getriebe auf mehrere Zwischenzahnräder? Von einem mot-2? Und das letzte ist mit seinen Zähnen irgendwie festgeklemmt, sodass die Drehbewegung erzwungen wird? Ganz große Klasse, so kompakt - da wäre ein Detailbild und etwas Text noch toll!

Gruß,
Stefan