---
layout: "image"
title: "Steuerpult"
date: "2013-11-29T21:55:24"
picture: "IMG_2903.jpg"
weight: "8"
konstrukteure: 
- "Lukas Graber"
fotografen:
- "Lukas Graber"
keywords: ["Steuerpult"]
uploadBy: "LuGra"
license: "unknown"
legacy_id:
- details/37855
imported:
- "2019"
_4images_image_id: "37855"
_4images_cat_id: "609"
_4images_user_id: "1767"
_4images_image_date: "2013-11-29T21:55:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37855 -->
Steuerpult mit integriertem Netzteil. Obige Schalterreihe sind ein-aus Schalter für Licht etc. welches noch nicht im Modell eingebaut ist....