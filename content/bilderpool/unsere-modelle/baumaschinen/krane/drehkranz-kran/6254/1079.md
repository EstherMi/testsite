---
layout: "comment"
hidden: true
title: "1079"
date: "2006-05-11T16:17:26"
uploadBy:
- "remadus"
license: "unknown"
imported:
- "2019"
---
Das sieht schon mächtig nach was aus.

Als kleine Anregung empfehle ich noch zwischen oberem Kranz und den Rollen eine sehr feste Pappe zu legen, dann rollen die kleinen Räder nicht über Fugen. Es 'holpert' dann weniger.

Wenn die Reibung noch kleiner werden soll, dann benutze lose Rollen, wie in einem Kugellager. Die Räder rollen dann zwischen oberem und unteren Kranz und werden nur geführt, damit sie nicht herausfallen. Das erfordert allerdings einen zusätzlichen Kranz zwischen oberem und unteren, der die Rollen festhält, sonst sich aber unabhängig dreht. Ein solcher Kranz dreht sich auffällig leicht. (vgl. Montierung für Teleobjektiv)