---
layout: "image"
title: "Gesamtansicht"
date: "2006-05-11T15:01:30"
picture: "DSCN0729.jpg"
weight: "12"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/6254
imported:
- "2019"
_4images_image_id: "6254"
_4images_cat_id: "214"
_4images_user_id: "184"
_4images_image_date: "2006-05-11T15:01:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6254 -->
So wird der Drehkranz in den "Silver Spade" eingebaut. An den vier Ausbuchtungen am unteren Ring werden die Raupenfahrwerke befestigt (wenn´s denn klappt). Über dem Ring der sich direkt oberhalb der Rollenböcke mit den Rädern 14 befindet, wird eine Kette befestigt. Diese wiederum wird dann durch ein anliegendes Zahnrad angetrieben.
Das gleiche Modell ist im Big Muskie verbaut und funktioniert dort super.