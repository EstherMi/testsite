---
layout: "image"
title: "Oberer Teil"
date: "2006-05-11T15:01:30"
picture: "DSCN0732.jpg"
weight: "15"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/6257
imported:
- "2019"
_4images_image_id: "6257"
_4images_cat_id: "214"
_4images_user_id: "184"
_4images_image_date: "2006-05-11T15:01:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6257 -->
