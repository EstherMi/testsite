---
layout: "comment"
hidden: true
title: "1573"
date: "2006-11-15T17:31:52"
uploadBy:
- "Jettaheizer"
license: "unknown"
imported:
- "2019"
---
Wassertiefe

Naja, ich würde mal so auf 1,10m Pegelstand tippen...

Nein, mal im Ernst, ich schaffe es mit meiner Cam beim besten Willen nicht, bei Kunstlicht gescheite Fotos zu machen. Sowohl, was die Farbe als auch die Schärfe angeht, hat der Sensor da arge Probleme. Ich kann die Cam nichtmal auf ein Stativ schrauben, weil dafür kein Gewinde vorhanden ist. Ich werde aber versuchen, bessere Fotos zu machen und diese dann ersetzen.

Gruß,
Franz