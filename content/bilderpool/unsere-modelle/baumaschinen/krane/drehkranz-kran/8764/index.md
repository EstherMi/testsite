---
layout: "image"
title: "Innenteil"
date: "2007-01-30T19:24:52"
picture: "drehkranzmartin4.jpg"
weight: "4"
konstrukteure: 
- "Masked (Martin)"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/8764
imported:
- "2019"
_4images_image_id: "8764"
_4images_cat_id: "214"
_4images_user_id: "373"
_4images_image_date: "2007-01-30T19:24:52"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8764 -->
In der Mitte erkennt man die Führung der Gewindestange durch einen Baustein 15 rot.