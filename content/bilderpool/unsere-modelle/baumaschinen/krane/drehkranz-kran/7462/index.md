---
layout: "image"
title: "Drehkranz klein Gesamtansicht"
date: "2006-11-14T22:58:19"
picture: "Drehkranz01.jpg"
weight: "5"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- details/7462
imported:
- "2019"
_4images_image_id: "7462"
_4images_cat_id: "214"
_4images_user_id: "488"
_4images_image_date: "2006-11-14T22:58:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7462 -->
Mein erster Versuch, einen sehr kompakten Drehkranz für den Gittermastkran (siehe auch hier http://www.ftcommunity.de/categories.php?cat_id=698 ) zu bauen.
Der Außendurchmesser beträgt einschließlich der Kette ca. 110mm.
An den 30°-Winkelsteinen soll dann der Oberwagen befestigt werden.

Leider gibt die Cam einfach keine bessere Qualität her bei Kunstlicht. Und scharf krieg ich das von Hand auch nicht...