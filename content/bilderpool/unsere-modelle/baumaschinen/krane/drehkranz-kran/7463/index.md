---
layout: "image"
title: "Drehkranz klein Gesamtansicht ohne Drehscheibe"
date: "2006-11-14T22:58:19"
picture: "Drehkranz02.jpg"
weight: "6"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- details/7463
imported:
- "2019"
_4images_image_id: "7463"
_4images_cat_id: "214"
_4images_user_id: "488"
_4images_image_date: "2006-11-14T22:58:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7463 -->
Hier jetzt ohne Drehscheibe. Die gelbe Scheibe ist aus Platinenmaterial und eines von nur zwei Fremdteilen. Ich habe ein 4mm-Loch hineingebohrt, die Drehscheibe mit Achse reingesteckt und als Schablone zum anreissen benutzt. Das habe ich von beiden Seiten gemacht. Danach hab ich den Kreis mit der Anreissnadel solange nachgezogen, bis ich die Scheibe aus der Platte brechen konnte.
Allerdings werde ich das Loch noch auf 30mm aufbohren, so daß die Scheibe um die Rohrhülse paßt (siehe auch nächstes Bild), dann hat der Drehkranz weniger Spiel.