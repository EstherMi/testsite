---
layout: "image"
title: "Drehkranz klein"
date: "2004-03-07T15:21:25"
picture: "drehkranz_klein_2.jpg"
weight: "24"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "honjo1"
license: "unknown"
legacy_id:
- details/2301
imported:
- "2019"
_4images_image_id: "2301"
_4images_cat_id: "214"
_4images_user_id: "14"
_4images_image_date: "2004-03-07T15:21:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2301 -->
