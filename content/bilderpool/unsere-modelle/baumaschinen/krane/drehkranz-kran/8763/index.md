---
layout: "image"
title: "Plexiglas"
date: "2007-01-30T19:24:52"
picture: "drehkranzmartin3.jpg"
weight: "3"
konstrukteure: 
- "Masked (Martin)"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/8763
imported:
- "2019"
_4images_image_id: "8763"
_4images_cat_id: "214"
_4images_user_id: "373"
_4images_image_date: "2007-01-30T19:24:52"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8763 -->
Die Idee kam mir als ich das Plexiglas unten im Keller gesehen habe. Alu/Metall hatte ich nicht und stabil genug ist Plexiglas auch.