---
layout: "image"
title: "16"
date: "2012-03-03T21:35:39"
picture: "halbportalkran16.jpg"
weight: "16"
konstrukteure: 
- "Rolf B"
fotografen:
- "Rolf B"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rolf B"
license: "unknown"
legacy_id:
- details/34544
imported:
- "2019"
_4images_image_id: "34544"
_4images_cat_id: "2550"
_4images_user_id: "1419"
_4images_image_date: "2012-03-03T21:35:39"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34544 -->
Zugstrebe zwischen den beinen der Stütze.