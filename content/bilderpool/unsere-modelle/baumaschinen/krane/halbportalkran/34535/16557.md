---
layout: "comment"
hidden: true
title: "16557"
date: "2012-03-04T10:50:51"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hallo Rolf,
das ist ja eine feine Idee - die Bausteine 30 als Schienenstrang für zwei Spurkränze.
Macht einen ziemlich stabilen Eindruck.
Gruß, Dirk