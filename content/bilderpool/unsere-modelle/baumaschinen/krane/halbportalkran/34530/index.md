---
layout: "image"
title: "2"
date: "2012-03-03T21:35:26"
picture: "halbportalkran02.jpg"
weight: "2"
konstrukteure: 
- "Rolf B"
fotografen:
- "Rolf B"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rolf B"
license: "unknown"
legacy_id:
- details/34530
imported:
- "2019"
_4images_image_id: "34530"
_4images_cat_id: "2550"
_4images_user_id: "1419"
_4images_image_date: "2012-03-03T21:35:26"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34530 -->
Stirnansicht mit Unterflasche.