---
layout: "image"
title: "Hilfsausleger"
date: "2005-03-22T17:56:59"
picture: "DSC00022.jpg"
weight: "26"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chevyfahrer"
license: "unknown"
legacy_id:
- details/3854
imported:
- "2019"
_4images_image_id: "3854"
_4images_cat_id: "334"
_4images_user_id: "103"
_4images_image_date: "2005-03-22T17:56:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3854 -->
Damit auch die letzten Statikteile mal die Sonne sehen hab ich noch einen Hilfsausleger draufgesetzt.