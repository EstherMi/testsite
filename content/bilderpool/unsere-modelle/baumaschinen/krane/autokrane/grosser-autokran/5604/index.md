---
layout: "image"
title: "kran mit begleitfahrzeugen"
date: "2006-01-21T08:51:54"
picture: "kran_mit_begleit_2.jpg"
weight: "1"
konstrukteure: 
- "trucker4164"
fotografen:
- "trucker4164"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "trucker4164"
license: "unknown"
legacy_id:
- details/5604
imported:
- "2019"
_4images_image_id: "5604"
_4images_cat_id: "334"
_4images_user_id: "368"
_4images_image_date: "2006-01-21T08:51:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5604 -->
hier ein bild vom kran mit begleitfahrzeugen