---
layout: "image"
title: "Ausleger04"
date: "2007-06-17T22:19:51"
picture: "kran5.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/10877
imported:
- "2019"
_4images_image_id: "10877"
_4images_cat_id: "984"
_4images_user_id: "389"
_4images_image_date: "2007-06-17T22:19:51"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10877 -->
Hier ist die Unterseite des Auslegers mit der Laufkatze zu erkennen