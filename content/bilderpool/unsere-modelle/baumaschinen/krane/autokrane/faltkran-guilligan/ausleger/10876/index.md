---
layout: "image"
title: "Ausleger03"
date: "2007-06-17T22:19:51"
picture: "kran4.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/10876
imported:
- "2019"
_4images_image_id: "10876"
_4images_cat_id: "984"
_4images_user_id: "389"
_4images_image_date: "2007-06-17T22:19:51"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10876 -->
Hier noch mal ein Draufblick.
Der graue Minimotor mit dem vorgesetzten Getriebe und der Schnecke(Sonderanfertigung) für den Vorlauf der Laufkatze ist auf dem Ausleger befestigt.