---
layout: "image"
title: "Faltkran beim Aufbau"
date: "2007-06-09T14:58:57"
picture: "faltkranguilligan18.jpg"
weight: "24"
konstrukteure: 
- "Guilligan"
fotografen:
- "Guilligan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/10754
imported:
- "2019"
_4images_image_id: "10754"
_4images_cat_id: "974"
_4images_user_id: "389"
_4images_image_date: "2007-06-09T14:58:57"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10754 -->
