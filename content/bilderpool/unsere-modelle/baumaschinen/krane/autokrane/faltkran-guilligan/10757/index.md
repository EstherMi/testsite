---
layout: "image"
title: "Faltkran beim Aufbau"
date: "2007-06-09T14:59:04"
picture: "faltkranguilligan21.jpg"
weight: "27"
konstrukteure: 
- "Guilligan"
fotografen:
- "Guilligan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/10757
imported:
- "2019"
_4images_image_id: "10757"
_4images_cat_id: "974"
_4images_user_id: "389"
_4images_image_date: "2007-06-09T14:59:04"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10757 -->
