---
layout: "image"
title: "Draufsicht Oberwagen"
date: "2007-06-09T14:58:44"
picture: "faltkranguilligan07.jpg"
weight: "13"
konstrukteure: 
- "Guilligan"
fotografen:
- "Guilligan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/10743
imported:
- "2019"
_4images_image_id: "10743"
_4images_cat_id: "974"
_4images_user_id: "389"
_4images_image_date: "2007-06-09T14:58:44"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10743 -->
