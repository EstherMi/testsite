---
layout: "image"
title: "Faltkran04"
date: "2007-06-09T22:56:32"
picture: "faltkran6.jpg"
weight: "12"
konstrukteure: 
- "Guilligan"
fotografen:
- "Guilligan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/10783
imported:
- "2019"
_4images_image_id: "10783"
_4images_cat_id: "974"
_4images_user_id: "389"
_4images_image_date: "2007-06-09T22:56:32"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10783 -->
