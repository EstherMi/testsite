---
layout: "image"
title: "Oberteil"
date: "2007-06-09T14:59:04"
picture: "faltkranguilligan22.jpg"
weight: "28"
konstrukteure: 
- "Guilligan"
fotografen:
- "Guilligan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/10758
imported:
- "2019"
_4images_image_id: "10758"
_4images_cat_id: "974"
_4images_user_id: "389"
_4images_image_date: "2007-06-09T14:59:04"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10758 -->
