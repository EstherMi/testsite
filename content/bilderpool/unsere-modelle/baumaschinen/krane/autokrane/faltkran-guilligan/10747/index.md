---
layout: "image"
title: "Aufbau Oberwagen"
date: "2007-06-09T14:58:57"
picture: "faltkranguilligan11.jpg"
weight: "17"
konstrukteure: 
- "Guilligan"
fotografen:
- "Guilligan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/10747
imported:
- "2019"
_4images_image_id: "10747"
_4images_cat_id: "974"
_4images_user_id: "389"
_4images_image_date: "2007-06-09T14:58:57"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10747 -->
