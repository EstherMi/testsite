---
layout: "image"
title: "Faltkran Model 2-20"
date: "2005-09-25T14:02:35"
picture: "Faltkran_2-28.jpg"
weight: "27"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/4802
imported:
- "2019"
_4images_image_id: "4802"
_4images_cat_id: "383"
_4images_user_id: "162"
_4images_image_date: "2005-09-25T14:02:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4802 -->
