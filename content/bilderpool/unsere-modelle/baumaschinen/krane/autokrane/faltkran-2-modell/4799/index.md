---
layout: "image"
title: "Faltkran Model 2-17"
date: "2005-09-25T14:02:34"
picture: "Faltkran_2-20.jpg"
weight: "24"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/4799
imported:
- "2019"
_4images_image_id: "4799"
_4images_cat_id: "383"
_4images_user_id: "162"
_4images_image_date: "2005-09-25T14:02:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4799 -->
