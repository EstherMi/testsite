---
layout: "image"
title: "Faltkran Model 2-16"
date: "2005-09-25T14:02:34"
picture: "Faltkran_2-19.jpg"
weight: "23"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/4798
imported:
- "2019"
_4images_image_id: "4798"
_4images_cat_id: "383"
_4images_user_id: "162"
_4images_image_date: "2005-09-25T14:02:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4798 -->
