---
layout: "image"
title: "Oberwagen 1"
date: "2006-10-29T19:02:13"
picture: "Kran08a.jpg"
weight: "7"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- details/7279
imported:
- "2019"
_4images_image_id: "7279"
_4images_cat_id: "698"
_4images_user_id: "488"
_4images_image_date: "2006-10-29T19:02:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7279 -->
Hier das Gelenk des Hauptmastes. Das war schon etwas komplizierter. Hatte zuerst weniger Verstärkung drin, da hat´s mir ständig das Gelenk zerlegt. Zwei Zapfen sind einfach zuwenig Halt für einen ganzen Mast...