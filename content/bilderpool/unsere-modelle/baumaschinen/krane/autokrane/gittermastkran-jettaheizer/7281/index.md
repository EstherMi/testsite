---
layout: "image"
title: "Oberwagen 3"
date: "2006-10-29T19:02:13"
picture: "Kran11a.jpg"
weight: "9"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- details/7281
imported:
- "2019"
_4images_image_id: "7281"
_4images_cat_id: "698"
_4images_user_id: "488"
_4images_image_date: "2006-10-29T19:02:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7281 -->
Hier noch eine Detailansicht des Drehkranzes. Wenn ich den Mast so weit hoch fahre wie auf dem ersten Bild zu sehen, steht die ganze Konstruktion tatsächlich nur auf 60 x 60 mm (!!) Grundfläche! Okay, sobald ich auch nur die Wippspitze ein Stück senke, kippt die Sache nach vorne, aber das gibt sich, wenn ein Fahrgestell drunter ist und mehr Gegengewicht auf dem Oberwagen liegt.

Das Fahrgestell ist derzeit in Arbeit, weitere Fotos gibt´s, sobald sich etwas nennenswertes getan hat.