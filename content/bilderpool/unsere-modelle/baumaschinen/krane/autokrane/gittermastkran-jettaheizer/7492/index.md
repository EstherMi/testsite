---
layout: "image"
title: "Oberwagen neu Gesamtansicht"
date: "2006-11-19T13:24:58"
picture: "Oberwagenrahmen02b.jpg"
weight: "1"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- details/7492
imported:
- "2019"
_4images_image_id: "7492"
_4images_cat_id: "698"
_4images_user_id: "488"
_4images_image_date: "2006-11-19T13:24:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7492 -->
Nachdem ich den ersten Oberwagen wieder zerlegt habe, fange ich nun von vorne an. Hier erstmal die Gesamtansicht des neuen Rahmens. 
Nachdem der erste Oberwagen aus U-Trägern bestand, die nachträglich verstärkt wurden, habe ich hier von vornherein einen richtigen Rahmen gebaut. In allen Dimensionen gewachsen, hat er jetzt auch genug Platz für die Winden und alles andere, was notwendig ist. Die Breite beträgt jetzt 120mm gegenüber 90mm des ersten Versuchs.
Nachteil des neuen Konstruktion ist, daß sehr viel Material draufgeht. Ich werde wohl über kurz oder lang noch einiges zukaufen müssen, weil sonst nichts mehr für das Fahrgestell über ist...
Ich werde auch noch einige Verstrebungen einbauen, wenn die Antriebe etc. an ihrem Platz sind, damit der Rahmen torsionssteifer wird.
Man sieht auf diesem Bild schon deutlich, daß der originale ft-Drehkranz der Belastung jetzt schon nicht mehr gewachsen ist. Ich hoffe, daß sich auch das erledigt hat, wenn ich den modifizierten ft-Drehkranz fertig habe.