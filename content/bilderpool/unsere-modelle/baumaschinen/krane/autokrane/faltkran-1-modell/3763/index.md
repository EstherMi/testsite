---
layout: "image"
title: "Faltkran01"
date: "2005-03-12T19:48:41"
picture: "Faltkran01.JPG"
weight: "28"
konstrukteure: 
- "Arjen Neijsen"
fotografen:
- "Arjen Neijsen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/3763
imported:
- "2019"
_4images_image_id: "3763"
_4images_cat_id: "336"
_4images_user_id: "5"
_4images_image_date: "2005-03-12T19:48:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3763 -->
