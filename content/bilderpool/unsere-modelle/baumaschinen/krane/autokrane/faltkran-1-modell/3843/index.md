---
layout: "image"
title: "Faltkran32"
date: "2005-03-14T14:45:05"
picture: "Faltkran32.JPG"
weight: "17"
konstrukteure: 
- "Arjen Neijsen"
fotografen:
- "Arjen Neijsen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/3843
imported:
- "2019"
_4images_image_id: "3843"
_4images_cat_id: "336"
_4images_user_id: "5"
_4images_image_date: "2005-03-14T14:45:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3843 -->
