---
layout: "image"
title: "Faltkran27"
date: "2005-03-14T14:45:05"
picture: "Faltkran27.JPG"
weight: "12"
konstrukteure: 
- "Arjen Neijsen"
fotografen:
- "Arjen Neijsen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/3838
imported:
- "2019"
_4images_image_id: "3838"
_4images_cat_id: "336"
_4images_user_id: "5"
_4images_image_date: "2005-03-14T14:45:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3838 -->
