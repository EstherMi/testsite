---
layout: "image"
title: "Autokran mit 15x15mm Alu-Bauprofil4"
date: "2003-12-08T15:17:19"
picture: "FT-efteling0004.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/2014
imported:
- "2019"
_4images_image_id: "2014"
_4images_cat_id: "117"
_4images_user_id: "22"
_4images_image_date: "2003-12-08T15:17:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2014 -->
