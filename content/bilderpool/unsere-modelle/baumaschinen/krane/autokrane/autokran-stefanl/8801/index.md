---
layout: "image"
title: "Autokran 9"
date: "2007-02-03T12:18:38"
picture: "autokran01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/8801
imported:
- "2019"
_4images_image_id: "8801"
_4images_cat_id: "835"
_4images_user_id: "502"
_4images_image_date: "2007-02-03T12:18:38"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8801 -->
Gewicht: 4,5 kilo, Gesamthöhe mit ausgefahrenem Ausleger: 95cm.