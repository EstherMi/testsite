---
layout: "image"
title: "Autokran 1"
date: "2007-02-02T21:23:13"
picture: "autokran1.jpg"
weight: "15"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/8781
imported:
- "2019"
_4images_image_id: "8781"
_4images_cat_id: "835"
_4images_user_id: "502"
_4images_image_date: "2007-02-02T21:23:13"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8781 -->
Er ist noch nicht ganz fertig da der Ausleger noch nicht dran ist den muss ich noch bauen aber sonst lässt sich alles über die Fernsteuerung steuern. Ein paar Daten: Länge 50cm, Breite:25cm, Höhe: 20cm.