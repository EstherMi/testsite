---
layout: "image"
title: "Autokran 15"
date: "2007-02-03T12:18:38"
picture: "autokran07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/8807
imported:
- "2019"
_4images_image_id: "8807"
_4images_cat_id: "835"
_4images_user_id: "502"
_4images_image_date: "2007-02-03T12:18:38"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8807 -->
