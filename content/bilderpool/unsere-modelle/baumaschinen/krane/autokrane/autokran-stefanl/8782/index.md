---
layout: "image"
title: "Autokran 2"
date: "2007-02-02T21:23:13"
picture: "autokran2.jpg"
weight: "16"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/8782
imported:
- "2019"
_4images_image_id: "8782"
_4images_cat_id: "835"
_4images_user_id: "502"
_4images_image_date: "2007-02-02T21:23:13"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8782 -->
