---
layout: "image"
title: "Autokran 3"
date: "2007-02-02T21:23:13"
picture: "autokran3.jpg"
weight: "17"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/8783
imported:
- "2019"
_4images_image_id: "8783"
_4images_cat_id: "835"
_4images_user_id: "502"
_4images_image_date: "2007-02-02T21:23:13"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8783 -->
