---
layout: "image"
title: "Haken"
date: "2007-01-12T17:00:21"
picture: "gittermastkran8.jpg"
weight: "8"
konstrukteure: 
- "Masked (Martin)"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/8373
imported:
- "2019"
_4images_image_id: "8373"
_4images_cat_id: "705"
_4images_user_id: "373"
_4images_image_date: "2007-01-12T17:00:21"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8373 -->
Mit je 3 Rollen am Haken und am Ausleger ist der Kran ganz gut bestückt. Am Haken muss ich allerdings noch Arbeiten, der geht bei mehr Last als 3,5kg (Hand auflegen....) immer als erstes Auseinander.