---
layout: "image"
title: "Die Gelenke"
date: "2006-11-18T23:52:39"
picture: "Gittermastkran04.jpg"
weight: "12"
konstrukteure: 
- "Martin W."
fotografen:
- "Martin W."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/7479
imported:
- "2019"
_4images_image_id: "7479"
_4images_cat_id: "705"
_4images_user_id: "373"
_4images_image_date: "2006-11-18T23:52:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7479 -->
aus Rollenlager Nr. 37636