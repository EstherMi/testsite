---
layout: "image"
title: "Antriebe+Gegengewicht"
date: "2007-01-12T17:00:20"
picture: "gittermastkran2.jpg"
weight: "2"
konstrukteure: 
- "Masked (Martin)"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/8367
imported:
- "2019"
_4images_image_id: "8367"
_4images_cat_id: "705"
_4images_user_id: "373"
_4images_image_date: "2007-01-12T17:00:20"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8367 -->
Der Antrieb erfolgt über je einen PowerMotor mit Getriebe.
Als Gegengewicht dient ein alter Trafokern, Gewicht etwa 6kg.