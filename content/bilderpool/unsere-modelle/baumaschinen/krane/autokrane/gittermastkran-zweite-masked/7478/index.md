---
layout: "image"
title: "Fahrwerk 1"
date: "2006-11-18T23:52:39"
picture: "Gittermastkran03.jpg"
weight: "11"
konstrukteure: 
- "Martin W."
fotografen:
- "Martin W."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/7478
imported:
- "2019"
_4images_image_id: "7478"
_4images_cat_id: "705"
_4images_user_id: "373"
_4images_image_date: "2006-11-18T23:52:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7478 -->
Das Fahrwerk mit den Stützen, insgesamt angelehnt an das Fahrwerk aus dem Fanclubmodell Nr. 19. Allerdings ist kaum noch was davon übriggeblieben.
Das Fahrwerk wird angetrieben von 2 PowerMotoren, hat aber (noch?) keine Lenkung...schaun mer ma