---
layout: "image"
title: "Unterbau"
date: "2007-01-12T17:00:21"
picture: "gittermastkran7.jpg"
weight: "7"
konstrukteure: 
- "Masked (Martin)"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/8372
imported:
- "2019"
_4images_image_id: "8372"
_4images_cat_id: "705"
_4images_user_id: "373"
_4images_image_date: "2007-01-12T17:00:21"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8372 -->
Um die mehr als 6 kg Gegengewicht zu halten musste eine deutlich stabilere Konstruktion her, wie man sieht ist sie gelungen. Es darf nur dem Kran nichts im Weg stehen beim Drehen sonst nimmt er es mit.