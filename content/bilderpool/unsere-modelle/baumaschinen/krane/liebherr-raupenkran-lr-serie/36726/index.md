---
layout: "image"
title: "Kran in seine Basiskomponenten zerlegt"
date: "2013-03-07T13:30:38"
picture: "20111213_170545_Apple_iPhone_4_IMG_1587.jpeg"
weight: "7"
konstrukteure: 
- "Marc Stephan Tauchert"
fotografen:
- "Marc Stephan Tauchert"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mst"
license: "unknown"
legacy_id:
- details/36726
imported:
- "2019"
_4images_image_id: "36726"
_4images_cat_id: "2724"
_4images_user_id: "1621"
_4images_image_date: "2013-03-07T13:30:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36726 -->
