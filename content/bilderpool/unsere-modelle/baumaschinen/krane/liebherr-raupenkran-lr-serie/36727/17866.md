---
layout: "comment"
hidden: true
title: "17866"
date: "2013-03-12T13:08:07"
uploadBy:
- "Marten70"
license: "unknown"
imported:
- "2019"
---
Wow! Das sieht gut aus! Sehr interessant. Ich frage mir aber wie stabil dieser Drehkranz ist. Denn die das Kippmoment des Krahns wird durch Vertikalkrafte im Drehkranz aufgenommen. Leider exact in der Richtung der Bohrungen der Winkelsteinen. 

NB, sehe dies nicht als negative Kritik! Ich ringe auch mit die Herausforderung eines stabilen Drehkranzes fur ein Grosskran. Wenn dein Drehkranz funzt nehme ich dein Idee gerne mit in meinem Projekt.