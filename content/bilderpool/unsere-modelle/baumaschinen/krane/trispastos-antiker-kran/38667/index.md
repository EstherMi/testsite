---
layout: "image"
title: "Trispastos - mit Wellrad"
date: "2014-04-26T10:28:38"
picture: "Trispastos_mit_Wellrad_ftc.jpg"
weight: "1"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/38667
imported:
- "2019"
_4images_image_id: "38667"
_4images_cat_id: "2875"
_4images_user_id: "1126"
_4images_image_date: "2014-04-26T10:28:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38667 -->
Und zum Schluss noch eine Variante mit Wellrad. (Mehr dazu in ft:pedia 2/2014).