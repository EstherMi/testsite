---
layout: "overview"
title: "Trispastos (antiker Kran)"
date: 2019-12-17T19:13:11+01:00
legacy_id:
- categories/2875
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2875 --> 
Griechisch-römischer Kran mit 3-Rollen-Flaschenzug, der etwa 400-300 v.Chr. entwickelt und von dem römischen Ingenieur Vitruv ca. 100 v.Chr. dokumentiert wurde.