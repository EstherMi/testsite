---
layout: "image"
title: "sicht der Kamera"
date: "2011-02-03T19:50:41"
picture: "lkwmittiefladerundgabelstabler06.jpg"
weight: "6"
konstrukteure: 
- "Tim_Fischer"
fotografen:
- "Tim_Fischer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Tim_Fischer"
license: "unknown"
legacy_id:
- details/29847
imported:
- "2019"
_4images_image_id: "29847"
_4images_cat_id: "2198"
_4images_user_id: "1244"
_4images_image_date: "2011-02-03T19:50:41"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29847 -->
