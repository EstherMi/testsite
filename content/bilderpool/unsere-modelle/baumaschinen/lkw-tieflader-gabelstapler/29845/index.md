---
layout: "image"
title: "Kamerahalterung"
date: "2011-02-03T19:50:41"
picture: "lkwmittiefladerundgabelstabler04.jpg"
weight: "4"
konstrukteure: 
- "Tim_Fischer"
fotografen:
- "Tim_Fischer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Tim_Fischer"
license: "unknown"
legacy_id:
- details/29845
imported:
- "2019"
_4images_image_id: "29845"
_4images_cat_id: "2198"
_4images_user_id: "1244"
_4images_image_date: "2011-02-03T19:50:41"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29845 -->
Die Motorisierte Kamerahalterung. Sie ist drehba und beleuchtet.