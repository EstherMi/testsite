---
layout: "image"
title: "Abräumbagger_13"
date: "2012-04-06T23:17:42"
picture: "abraumbagger13.jpg"
weight: "13"
konstrukteure: 
- "Michael (coini)"
fotografen:
- "Michael (coini)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "coini"
license: "unknown"
legacy_id:
- details/34762
imported:
- "2019"
_4images_image_id: "34762"
_4images_cat_id: "2568"
_4images_user_id: "1476"
_4images_image_date: "2012-04-06T23:17:42"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34762 -->
Hier der Antrieb des Schaufelrades.