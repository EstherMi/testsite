---
layout: "image"
title: "Abräumbagger_01"
date: "2012-04-06T23:17:29"
picture: "abraumbagger01.jpg"
weight: "1"
konstrukteure: 
- "Michael (coini)"
fotografen:
- "Michael (coini)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "coini"
license: "unknown"
legacy_id:
- details/34750
imported:
- "2019"
_4images_image_id: "34750"
_4images_cat_id: "2568"
_4images_user_id: "1476"
_4images_image_date: "2012-04-06T23:17:29"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34750 -->
Bei diesem Modell handelt es sich um das Schaufenstermodell "Abraumbagger", das zu Weihnachten 1978 in einigen wenigen Schaufenstern von Spielwarenläden zu finden war. Ich habe das Modell nur anhand von Fotos mit meinen damaligen fischertechnik-Bauteilen originalgetreu wieder aufgebaut. Der eine Minimotor treibt das Schaufelrad an. Der andere Minimotor dreht den Bagger oberhalb des Kettengestells um ca. 180 Grad in beide Richtungen. Der Polwendeschalter ändert bei Erreichen des Zielpunktes jeweils die Drehrichtung. Durch die Seilwinden kann der Ausleger auf- und abgesenkt werden.
