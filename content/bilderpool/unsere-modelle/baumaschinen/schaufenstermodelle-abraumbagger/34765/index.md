---
layout: "image"
title: "Abräumbagger_16"
date: "2012-04-06T23:17:43"
picture: "abraumbagger16.jpg"
weight: "16"
konstrukteure: 
- "Michael (coini)"
fotografen:
- "Michael (coini)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "coini"
license: "unknown"
legacy_id:
- details/34765
imported:
- "2019"
_4images_image_id: "34765"
_4images_cat_id: "2568"
_4images_user_id: "1476"
_4images_image_date: "2012-04-06T23:17:43"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34765 -->
Ein weiterer Blick auf den Antrieb des Drehkranzes.