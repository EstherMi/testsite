---
layout: "image"
title: "Abräumbagger_17"
date: "2012-04-06T23:17:43"
picture: "abraumbagger17.jpg"
weight: "17"
konstrukteure: 
- "Michael (coini)"
fotografen:
- "Michael (coini)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "coini"
license: "unknown"
legacy_id:
- details/34766
imported:
- "2019"
_4images_image_id: "34766"
_4images_cat_id: "2568"
_4images_user_id: "1476"
_4images_image_date: "2012-04-06T23:17:43"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34766 -->
Ein weiterer Blick auf die beiden Seilwinden und auf die Statikstreben.