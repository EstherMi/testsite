---
layout: "image"
title: "Druckwerk Keksdrucker"
date: "2017-09-30T11:52:18"
picture: "02_Druckwerk.jpg"
weight: "2"
konstrukteure: 
- "Fabian, Max, Christian & Stefan"
fotografen:
- "Christian und Stefan"
keywords: ["Keksdrucker", "Druckwerk"]
uploadBy: "ftpi"
license: "unknown"
legacy_id:
- details/46509
imported:
- "2019"
_4images_image_id: "46509"
_4images_cat_id: "10"
_4images_user_id: "2611"
_4images_image_date: "2017-09-30T11:52:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46509 -->
Das Druckwerk besteht aus zwei Encodermotoren, die mit drei Spindeln den Druckkopf frei in x- und y-Richtung verfahren können. Die untere Ebene ist mit zwei Spindeln ausgeführt, die über eine Steuerkette verbunden sind.