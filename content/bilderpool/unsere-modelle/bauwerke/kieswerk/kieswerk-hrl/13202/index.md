---
layout: "image"
title: "Bahnverlad"
date: "2008-01-02T21:29:31"
picture: "kieswerk2.jpg"
weight: "2"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/13202
imported:
- "2019"
_4images_image_id: "13202"
_4images_cat_id: "1192"
_4images_user_id: "424"
_4images_image_date: "2008-01-02T21:29:31"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13202 -->
Hier können zwei Bahnwagen ent- oder beladen werden