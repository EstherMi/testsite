---
layout: "image"
title: "Zulieferbahn"
date: "2007-11-28T21:04:56"
picture: "kieswerk02.jpg"
weight: "5"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/12862
imported:
- "2019"
_4images_image_id: "12862"
_4images_cat_id: "1165"
_4images_user_id: "424"
_4images_image_date: "2007-11-28T21:04:56"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12862 -->
Die Lok wird über ein Doppel Flip-Flop noch von Hand bedient. Sie haben folgende Funktionen Ein/ Aus und Vor/ Zurück. Sie könnten jedoch ohne
weiteres durch Reed-Kontakte oder Fototransistoren ersetzt werden.