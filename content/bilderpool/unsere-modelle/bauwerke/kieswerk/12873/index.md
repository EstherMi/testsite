---
layout: "image"
title: "Sortieranlage 3"
date: "2007-11-28T21:05:13"
picture: "kieswerk13.jpg"
weight: "16"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/12873
imported:
- "2019"
_4images_image_id: "12873"
_4images_cat_id: "1165"
_4images_user_id: "424"
_4images_image_date: "2007-11-28T21:05:13"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12873 -->
Die zwei Kisten im Vordergrund fangen die fliegenden Riegel auf und spucken Sie unten aus. Mit den S-Riegel 4 habe ich keine Probleme, Sie fallen am Ende 
vom Förderband.