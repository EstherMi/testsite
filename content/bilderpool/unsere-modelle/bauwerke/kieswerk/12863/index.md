---
layout: "image"
title: "Aufzug 1"
date: "2007-11-28T21:04:56"
picture: "kieswerk03.jpg"
weight: "6"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/12863
imported:
- "2019"
_4images_image_id: "12863"
_4images_cat_id: "1165"
_4images_user_id: "424"
_4images_image_date: "2007-11-28T21:04:56"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12863 -->
Wird mit einem Flaschenzug hochgezogen. Leider ein bisschen langsam, doch es hat seinen Grund. Siehe die nächsten zwei Bilder.