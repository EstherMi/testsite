---
layout: "image"
title: "Förderbänder 3"
date: "2007-11-28T21:04:56"
picture: "kieswerk10.jpg"
weight: "13"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/12870
imported:
- "2019"
_4images_image_id: "12870"
_4images_cat_id: "1165"
_4images_user_id: "424"
_4images_image_date: "2007-11-28T21:04:56"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12870 -->
Hier erfolgt die Übergabe an die Sortieranlage.