---
layout: "image"
title: "Gesamtansicht"
date: "2007-11-28T21:04:55"
picture: "kieswerk01.jpg"
weight: "4"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/12861
imported:
- "2019"
_4images_image_id: "12861"
_4images_cat_id: "1165"
_4images_user_id: "424"
_4images_image_date: "2007-11-28T21:04:55"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12861 -->
Hier die ersten Bilder meines neuen Projekts.
Kurze Beschreibung der Anlage.
Mit dem Zug wird das Kies (in meinem Fall S-Riegelsteine 4, 6 und 8) angeliefert, mit dem Aufzug hochgezogen und in einen Rüttler gekippt.
Dann geht es über die Förderbänder, damit die Riegelsteine einen genügenden Abstand für die Sortieranlage bekommen. Dort werden zuerst die S-Riegel 8 aussortiert, dann die S-Riegel 6, die S-Riegel 4 werden durchgereicht. Dies ist der momentane Stand der Anlage. In arbeit sind die Silos von wo die S-Riegel abgezählt in Kassetten 60/60 abgefühlt werden. Später folgt noch ein HRL wo das ganze eingelagert wird.