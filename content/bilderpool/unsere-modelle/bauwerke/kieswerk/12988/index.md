---
layout: "image"
title: "Sortieranlage Detail"
date: "2007-12-03T23:30:27"
picture: "kieswerk1.jpg"
weight: "1"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/12988
imported:
- "2019"
_4images_image_id: "12988"
_4images_cat_id: "1165"
_4images_user_id: "424"
_4images_image_date: "2007-12-03T23:30:27"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12988 -->
Das Förderband ist so schmal, damit die Riegel nur  längs hindurch kommen.