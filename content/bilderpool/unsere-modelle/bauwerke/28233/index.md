---
layout: "image"
title: "Eiffelturm fertig"
date: "2010-09-25T20:38:37"
picture: "Vastgelegd_2008-10-10_00001A.jpg"
weight: "7"
konstrukteure: 
- "Ben Halverkamps"
fotografen:
- "Ben Halverkamps"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Benji"
license: "unknown"
legacy_id:
- details/28233
imported:
- "2019"
_4images_image_id: "28233"
_4images_cat_id: "656"
_4images_user_id: "764"
_4images_image_date: "2010-09-25T20:38:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28233 -->
Höhe ist 2,18 mtr.