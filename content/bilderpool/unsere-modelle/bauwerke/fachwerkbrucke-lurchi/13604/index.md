---
layout: "image"
title: "Mittlerer Tragpfeiler"
date: "2008-02-08T23:17:03"
picture: "Neues_Bild3.jpg"
weight: "10"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- details/13604
imported:
- "2019"
_4images_image_id: "13604"
_4images_cat_id: "1249"
_4images_user_id: "740"
_4images_image_date: "2008-02-08T23:17:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13604 -->
