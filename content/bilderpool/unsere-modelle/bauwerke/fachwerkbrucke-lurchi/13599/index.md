---
layout: "image"
title: "Seitliche Ansicht der Fachwerkbrücke"
date: "2008-02-08T23:17:03"
picture: "IMG_0607.jpg"
weight: "6"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- details/13599
imported:
- "2019"
_4images_image_id: "13599"
_4images_cat_id: "1249"
_4images_user_id: "740"
_4images_image_date: "2008-02-08T23:17:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13599 -->
