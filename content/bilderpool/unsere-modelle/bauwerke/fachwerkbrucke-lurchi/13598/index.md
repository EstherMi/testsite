---
layout: "image"
title: "Gesamtansicht der Fachwerkbrücke"
date: "2008-02-08T23:17:03"
picture: "IMG_0604.jpg"
weight: "5"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- details/13598
imported:
- "2019"
_4images_image_id: "13598"
_4images_cat_id: "1249"
_4images_user_id: "740"
_4images_image_date: "2008-02-08T23:17:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13598 -->
Vorderansicht der gesamten Brückenkonstruktion.
Die Gesamtlänge beträgt 3,60 m, die Höhe 1,43 m