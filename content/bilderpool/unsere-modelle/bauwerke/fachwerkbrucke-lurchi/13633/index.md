---
layout: "image"
title: "Seitliche Ansicht auf Tragwerk und Bögen"
date: "2008-02-09T22:49:06"
picture: "IMG_0648.jpg"
weight: "4"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- details/13633
imported:
- "2019"
_4images_image_id: "13633"
_4images_cat_id: "1249"
_4images_user_id: "740"
_4images_image_date: "2008-02-09T22:49:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13633 -->
