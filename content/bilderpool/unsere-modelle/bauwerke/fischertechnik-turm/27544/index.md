---
layout: "image"
title: "Turm 2"
date: "2010-06-23T22:18:57"
picture: "fischertechnikturm02.jpg"
weight: "2"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27544
imported:
- "2019"
_4images_image_id: "27544"
_4images_cat_id: "1978"
_4images_user_id: "1082"
_4images_image_date: "2010-06-23T22:18:57"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27544 -->
In der Kiste liegen zwei Steine mit insgesamt ca 6 Kilo Gewicht, gibt zusätzliche Stabilität. Am Anfang hatte ich nähmlich keine Abspannungen, jetzt wären die Steine wohl nicht mehr nötig.