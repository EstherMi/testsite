---
layout: "image"
title: "Turm 14"
date: "2010-06-23T22:18:58"
picture: "fischertechnikturm14.jpg"
weight: "14"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27556
imported:
- "2019"
_4images_image_id: "27556"
_4images_cat_id: "1978"
_4images_user_id: "1082"
_4images_image_date: "2010-06-23T22:18:58"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27556 -->
Da die Seilbahn nicht ganz bis zum Boden geht, gibts am Ende auch noch einen Fahrstuhl mit dem man noch mal ungefähr zwei Meter fährt