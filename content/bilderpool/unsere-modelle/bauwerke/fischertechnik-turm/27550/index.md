---
layout: "image"
title: "Turm 8"
date: "2010-06-23T22:18:57"
picture: "fischertechnikturm08.jpg"
weight: "8"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27550
imported:
- "2019"
_4images_image_id: "27550"
_4images_cat_id: "1978"
_4images_user_id: "1082"
_4images_image_date: "2010-06-23T22:18:57"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27550 -->
Von unten nach oben fotografiert. Man sieht, er ist leicht gebogen :-/