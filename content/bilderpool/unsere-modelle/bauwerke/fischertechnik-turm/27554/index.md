---
layout: "image"
title: "Turm 12"
date: "2010-06-23T22:18:58"
picture: "fischertechnikturm12.jpg"
weight: "12"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27554
imported:
- "2019"
_4images_image_id: "27554"
_4images_cat_id: "1978"
_4images_user_id: "1082"
_4images_image_date: "2010-06-23T22:18:58"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27554 -->
Gesamtansicht des erhöten Turmes. Das FT-Männchen liegt schon irgendwo unten. Ist aber weich im Grass gelandet