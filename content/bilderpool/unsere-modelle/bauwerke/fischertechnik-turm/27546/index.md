---
layout: "image"
title: "Turm 4"
date: "2010-06-23T22:18:57"
picture: "fischertechnikturm04.jpg"
weight: "4"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27546
imported:
- "2019"
_4images_image_id: "27546"
_4images_cat_id: "1978"
_4images_user_id: "1082"
_4images_image_date: "2010-06-23T22:18:57"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27546 -->
Der Ganze Turm, oben drauf kam noch mein Solar-Karussel.