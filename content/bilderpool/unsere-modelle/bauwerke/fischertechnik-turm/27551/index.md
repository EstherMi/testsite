---
layout: "image"
title: "Turm 9"
date: "2010-06-23T22:18:57"
picture: "fischertechnikturm09.jpg"
weight: "9"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27551
imported:
- "2019"
_4images_image_id: "27551"
_4images_cat_id: "1978"
_4images_user_id: "1082"
_4images_image_date: "2010-06-23T22:18:57"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27551 -->
Noch mal eine Fotografie von schräg unten.