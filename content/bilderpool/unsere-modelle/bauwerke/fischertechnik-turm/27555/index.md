---
layout: "image"
title: "Turm 13"
date: "2010-06-23T22:18:58"
picture: "fischertechnikturm13.jpg"
weight: "13"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27555
imported:
- "2019"
_4images_image_id: "27555"
_4images_cat_id: "1978"
_4images_user_id: "1082"
_4images_image_date: "2010-06-23T22:18:58"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27555 -->
Beim zweiten Aufbau hab´ ich dann auch noch ne Seilbahn dran gebaut. Im Wind schaukelt die enorm!