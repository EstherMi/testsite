---
layout: "image"
title: "haussiemens01.jpg"
date: "2010-08-22T13:21:07"
picture: "haussiemens01.jpg"
weight: "1"
konstrukteure: 
- "Sascha Lipka"
fotografen:
- "Sascha Lipka"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "saschalipka"
license: "unknown"
legacy_id:
- details/27874
imported:
- "2019"
_4images_image_id: "27874"
_4images_cat_id: "2017"
_4images_user_id: "635"
_4images_image_date: "2010-08-22T13:21:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27874 -->
Das Haus wird die folgenden funktionen koennen: 
Automatisiertes Tor und Garagentor 
Aussenbeleuchtung mit Lichtsensor 
Alarm aussen und innen (Tuer mit Magnetsensor) Alarmsimulation durch rote Lampen und einem Buzzer. 
Poolbeleuchtung und Pumpensimulation durch blaue Lampe 
Innenbeleuchtung 
Deckenventilator 
Boiler simulation durch LED's (rot=heiss orange=wird erhitzt) 
Sprinklersystem fuer Garten (gruene LED Simulation) 
Solarzellen auf dem Dach, die den Boiler erhitzen 

Steuerung wird durch eine Siemens LOGO! (SPS) erziehlt.

Solarzellen muessen noch auf dem Dach montiert werden sobald das Plexiglassdach montiert wurde.