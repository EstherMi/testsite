---
layout: "comment"
hidden: true
title: "11988"
date: "2010-08-22T18:15:28"
uploadBy:
- "saschalipka"
license: "unknown"
imported:
- "2019"
---
Moin Udo

Das Modell habe ich fuer Siemens gebaut. Es dient dazu Hausautomatik vor zustellen. Alle Motoren und Lampen sind 24V so das diese von einer SPS angesteuert werden koennen.
Die Aluprofile habe ich als Meterware bestellt und zu ft-Laengen und freien Laengen zurecht geschnitten, wie es auch immer passte oder passen musste.

Das Haus wird die folgenden funktionen koennen:
Automatisiertes Tor und Garagentor
Aussenbeleuchtung mit Lichtsensor
Alarm aussen und innen (Tuer mit Magnetsensor) Alarmsimulation durch rote Lampen und einem Buzzer.
Poolbeleuchtung und Pumpensimulation durch blaue Lampe
Innenbeleuchtung
Deckenventilator
Boiler simulation durch LED's (rot=heiss  orange=wird erhitzt)
Sprinklersystem fuer Garten (gruene LED Simulation)
Solarzellen auf dem Dach, die den Boiler erhitzen

Gruss
Sascha
fischertechnik Suedafrika