---
layout: "image"
title: "Fernbedienung Abdeckung Unterseite"
date: "2018-09-23T13:24:04"
picture: "Bedienung_untere_Abdeckung.jpg"
weight: "32"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/47910
imported:
- "2019"
_4images_image_id: "47910"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47910 -->
Auch die Unterseite der Fernbedienung muss geschlossen werden.
Auf der ft:con in Dreieich 2017 war diese noch offen. Das wurde uns zum Verhängnis, da sich auf der Ausstellung ein Kabel löste, denn ein Besucher blieb mit dem Finger dran hängen, als er die Bedienung zurück gab.

Aber wenn ich schon eine Abdeckung mache, dann darf sie auch beschriftet sein. Humor gehört dazu und wer die Sachenicht lesen kann: auf der ft convention 2018 in Dreieich zeige ich die Unterseite gerne. (Und auf weiteren Ausstellungen.)