---
layout: "image"
title: "Detail: alter Turm Fahrbahnlager"
date: "2016-10-01T22:12:29"
picture: "IMG_20160904_183810a.jpg"
weight: "96"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: ["Statik", "Turm", "Brücke"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/44496
imported:
- "2019"
_4images_image_id: "44496"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-10-01T22:12:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44496 -->
Hier kommt die Fahrbahn (von rechts unten) an den alten Turm heran. Um die zu lagern wurde ein Gelenk benutzt. Nachteil dabei ist, dass in dem Gelenk selbst eine scherspannung auftritt, die später zu Problemen führen kann. Außerdem liegt das ganze Gewicht der Fahrbahn auf nur 2 Zapfen der grauen 30er mit Loch.

Die neue Version (nächstes Bild) ist da schon viel stabiler.