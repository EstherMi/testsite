---
layout: "image"
title: "Plan des Antriebes"
date: "2016-10-01T22:12:29"
picture: "GOPR9189a.jpg"
weight: "88"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: ["Prototyp", "Getriebe", "Hängebahn", "Schrägseilbrücke", "Brücke", "Differential"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/44486
imported:
- "2019"
_4images_image_id: "44486"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-10-01T22:12:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44486 -->
Plan des Seilantriebes (Gedankennotiz).
Der Antriebsmotor (oben) sowie die Motoren für den Auf- und Abbau und die Seilspannung müssen auf 2 Spulen wirken.
Erklärung und Video mit Funktionstest hier: https://youtu.be/-EDMZNxTwsE