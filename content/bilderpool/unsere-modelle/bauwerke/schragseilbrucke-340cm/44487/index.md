---
layout: "image"
title: "Probeaufbau des Funktionsprototyps Hängebahnantrieb"
date: "2016-10-01T22:12:29"
picture: "GOPR9188a.jpg"
weight: "89"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: ["Getriebe", "Differential", "Prototyp"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/44487
imported:
- "2019"
_4images_image_id: "44487"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-10-01T22:12:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44487 -->
Erklärung und Video mit Funktionstest hier: https://youtu.be/-EDMZNxTwsE