---
layout: "image"
title: "Fahrtrichtungsgeber Version 2 eingebaut"
date: "2018-09-23T13:24:04"
picture: "Fahrtrichtungsgeber-alt.jpg"
weight: "25"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: ["Kopggetriebe", "Reedkontakt"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/47903
imported:
- "2019"
_4images_image_id: "47903"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47903 -->
Hier der Geber nun eingebaut. im HIntergrund zu sehen sind die Powermotoren, die über das Gleichlaufgetriebe die Seilspulen für den Antrieb bewegen. Die verlängerte Achse der einen treibt dann über das Z10 ds Kopfritzel des Z40 an. Darunter der Magnet, welcher die Reedkontakte auslöst.
Schön zu sehen hier am Reed rechts unten, wie der grüne Gummiring den Kontakt hält.

Was mich hier ärgert:
- der Geber ist dennoch etwas zu groß und ragt über die Platte raus.
- Kopfgetriebe mag ich nicht.