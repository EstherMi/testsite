---
layout: "image"
title: "Kollisionsleuchte an der hinteren Strebe"
date: "2016-12-28T12:29:46"
picture: "IMG_20161227_110725.jpg"
weight: "63"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: ["Kabel", "Elektrik", "LED", "Lampe"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/44938
imported:
- "2019"
_4images_image_id: "44938"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-12-28T12:29:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44938 -->
Am Turm halten lange Streben die Spitze gegen den Zug des Tragseils der Fahrbahnbeleuchtung und des oberen Schrägseils.

An dieser Strebe ist auf einem kleinen Podest eine LED mit roter Kappe als Killisionswarnleuchte angebracht, etwa auf gleicher Höhe wie die Kollisionswarnleuchten am Tragseil.

Die Kabel von oben seind jene von der Turmspitze und führen nach unten zur Stromversorgung.