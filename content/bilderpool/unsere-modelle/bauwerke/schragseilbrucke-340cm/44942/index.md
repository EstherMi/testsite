---
layout: "image"
title: "Fahrbahnaufhängung"
date: "2016-12-28T12:29:46"
picture: "IMG_20161227_110601.jpg"
weight: "67"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: ["Statik", "schrägseilbrücke"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/44942
imported:
- "2019"
_4images_image_id: "44942"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-12-28T12:29:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44942 -->
Zu sehen ist die Fahrbahn (Statikträger mit Laufschienen).
Die Seile an denen die Fahrbahn hängt sind durch die Löcher geführt und dann um die Strebe wieder herum, so dass die Last sich auf die komplette Strebe und nicht nur auf das Loch verteilt.

Erst danach wurden die Laufschienen angeschraubt.

Der Trick ist nun, dass die Räder der Bahn seitlich an den Seilen vorbei laufen müssen.

Links am Bildrand ist ein kleines Geheimnis zu sehen:
unter derFahrbahn wird ein dickes Kabel (5m serielles Kabel) eingehängt und mit den Drahtstücken an der Fahrbahn befestigt. Dieses Kabal stellt die Stromversorgung der Beleuchtung und einiger Signalgeber (Endlageschalter) sicher, denn die Idee ist es, dass nur die Talstation (Turm 1) mit Trafos und Steuerelementen, sowie Antrieb ausgestattet ist. Die Bergstation soll nur über dieses Kabel versorgt und gesteuert werden.