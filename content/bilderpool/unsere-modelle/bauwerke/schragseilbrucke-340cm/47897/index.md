---
layout: "image"
title: "Traverse 5 Prototyp"
date: "2018-09-23T13:24:04"
picture: "traverse04.jpg"
weight: "19"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: ["Minitaster"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/47897
imported:
- "2019"
_4images_image_id: "47897"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47897 -->
Endversion der Traverse mit Endabschaltung.

Nachdem im vorigen BIld die technische Lösung gefunden wurde, geht es nun um Stabilität, genaue Winkel, Zuverlässigkeit, Eleganz.

Zum Vergleich mit der vorigen Version wandert nun die Feser (Stoßdämpfer) nach hinten und verschwindet innerhalb der bereits vorhandenen Konstruktion. Der gelbe Baustein  entfällt).
Die Winkel der Gabel werden verbessert, schließlich kann die Gabel noch einen Raster nach hinten versetzt werden, was sie unauffälliger macht und die Fahrstrecke um 2 x 15mm verlängert, also auch die Gondel näher an den Turm bringt.

Weiterer Vorteil gegenüber der alten Traverse: die Schalter sind nun direkt am Turm angebracht. So können die Kabel sicher verlegt werden und die Traverse muss nicht auf und abgebaut werden, wenn die Anlage transportiert wird.

Diese Version wird schließlich verbaut.