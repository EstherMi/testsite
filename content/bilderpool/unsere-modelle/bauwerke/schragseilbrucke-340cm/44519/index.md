---
layout: "image"
title: "Lagerblock zur Spannung der Brückenseile"
date: "2016-10-02T17:43:47"
picture: "IMG_20160918_194740a.jpg"
weight: "83"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: ["Hängebrücke", "Seilbahn", "Seilwinde"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/44519
imported:
- "2019"
_4images_image_id: "44519"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-10-02T17:43:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44519 -->
Lagerblock eingebaut.

Jetzt wird langsam klar, was dieser Block später macht. Von rechts werden die Seile der Schrägseilbrücke wie von einem Flaschenzug zugeführt. 4 Halteseile müssen aufgewickelt werden und so blockiewrt, dass sie nicht mehr nachgeben. Dazu benutze ich die Riegel. Sie werden mit den "Anfasserln" (30er Streben) in Position geschoben und greifen in die Zahnräder.

Ziel ist es, 4 blockierbare Spulen auf kleinstem Raum unterzubringen und das ganze möglichst zu versteifen. Die komplette Last der Schrägseilbrücke wird später hier in die Bodenplatte eingeleitet. Die Seile verstellen die Biegung der Brücke exakt.