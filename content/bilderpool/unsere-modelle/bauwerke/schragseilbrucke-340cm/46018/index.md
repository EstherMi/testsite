---
layout: "image"
title: "Antrieb Hängebahn Getriebe (Teil 4)"
date: "2017-07-04T16:25:46"
picture: "a-Gleichlaufgetriebe-5.jpg"
weight: "49"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: ["Gleichlaufgetriebe", "Antrieb", "Motor", "Differenzialetriebe"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/46018
imported:
- "2019"
_4images_image_id: "46018"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2017-07-04T16:25:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46018 -->
Hier von der Abtriebsseite gesehen.
Zu erkennen sind nun die Seilspulen und das Zusammenspiel von Motor, Schneckengetriebe und dem Abtrieb.