---
layout: "image"
title: "Der zweite Turm von oben gesehen (vom Toplicht)"
date: "2016-10-02T17:43:47"
picture: "IMG_20161002_171120a.jpg"
weight: "85"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: ["Statik", "Turm", "Brücke", "Hängebrücke", "Schrägseilbrücke"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/44521
imported:
- "2019"
_4images_image_id: "44521"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-10-02T17:43:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44521 -->
Turm und Lagerblockin von oben.
Von links kommen die Seile vom Turm herunter (die Fahrbahn ist links noch im Ansatz zu erkennen). Alle 4 Halteseile werden im Lagerblock aufgespult. Die Riegel blockieren die Zahnräder und somit die Spulen. Außerdem werden die Kräfte der Haltestreben aufgenommen und in die Grundplatte eingeleitet. Nach tests zeigt sich: das System ist absolut perfekt und arbeitet millimetergenau.