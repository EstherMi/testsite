---
layout: "image"
title: "Kabelchaos 3"
date: "2018-09-23T13:24:04"
picture: "gehuse04.jpg"
weight: "36"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/47914
imported:
- "2019"
_4images_image_id: "47914"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47914 -->
Wenn das Betriebsgebäude von der Fachfirmal fachmännisch abgerissen wird, bleibt ein Haufen Kabel und der serielle Stecker übrig.

Nun beginnt die Arbeit: alle Kabel müssen weg und die komplette Verkabelung des Turms wird neu gemacht.
Wohin damit? Zeige ich auf den nächsten BIldern.