---
layout: "comment"
hidden: true
title: "24200"
date: "2018-09-26T13:09:15"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Junge Junge, da steckt ja echt wahnsinnig viel Technik und Tüftelei drin. Das sieht man dem Modell von außen gar nicht an. Nächstes Mal muss ich mal nur als Besucher zur Convention kommen, damit ich mir sowas auch mal ausführlich zeigen lassen kann! :-)

Gruß,
Stefan