---
layout: "image"
title: "Traverse 4 Prototyp"
date: "2018-09-23T13:24:04"
picture: "traverse03.jpg"
weight: "18"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/47896
imported:
- "2019"
_4images_image_id: "47896"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47896 -->
Nächste Version der Traverse mit Endschaltern.

Zurück zur ersten Variante. Allerdings wurden hier die Lager umgebaut und die Gabel mit Hilfe einer Feder (Stoßdämpfer) nach unten gedrückt. So kann die Gabel nach oben nachgeben und die Betätigung der Taster ist zuverlässig.

Das Prizip hat alle Tests bestanden, auch wenn die Fahrbahn durch den Höhenunterschied von Berg und Tal gekippt sein kann.

Lediglich kleinere Verbesserungen, und Stabilität waren jetzt noch zu machen. ABer das Prinzip war gefunden.