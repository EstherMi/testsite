---
layout: "image"
title: "Seilbefestigung an der Fahrbahn"
date: "2016-10-02T17:43:47"
picture: "IMG_20160921_152336a.jpg"
weight: "77"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: ["Laufschien", "Seil", "Knoten"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/44513
imported:
- "2019"
_4images_image_id: "44513"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-10-02T17:43:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44513 -->
Die Seile wurden als Schlaufe geknotet und schließlich mit einem Doppelten Ankerstich um die Bahn und durch die Löcher fixiert. So ist gewährleistet, dass die Seile genau in der Mitte heraus kommen und die Kräfte nicht auf die Ösen wirken, sondern von dem ganzen Träger aufgenommen werden.