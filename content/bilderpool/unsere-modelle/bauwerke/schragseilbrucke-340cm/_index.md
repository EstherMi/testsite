---
layout: "overview"
title: "Schrägseilbrücke (340cm)"
date: 2019-12-17T19:50:25+01:00
legacy_id:
- categories/3291
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3291 --> 
Die Schrägseilbrücke aus Classic-Teilen 
Breite über alles 510cm / Turmhöhe 140cm / Fahrbahnlänge = Spannweite 340cm / Höhe über Grund ca. 40cm / max. Höhenunterschied der Stationen +- 40cm 
Kabelfernsteuerung allerFunktionen. Voll beleuchtet. Demontierbar für Transport in wenigen Minuten. Voll bespielbar. 
Bauwerk aus ca. 98% Classic-Teilen im Privatbesitz (wird nach der Ausstellung wieder zum spielen benutzt). 
Design und Statik von Jan (7 Jahre). Funktion und Technik von Tilo (46 Jahre)