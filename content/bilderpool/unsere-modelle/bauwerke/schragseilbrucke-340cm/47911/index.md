---
layout: "image"
title: "Fernbedienung Endversion"
date: "2018-09-23T13:24:04"
picture: "Bedienung-fertig.jpg"
weight: "33"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/47911
imported:
- "2019"
_4images_image_id: "47911"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47911 -->
Und hier die Endgültige Version die ab Dreieich 2018 zu sehen sein wird.
Sauber beschriftet - nicht ganz so sauber von mir aufgeklebt.

Der linke Teil ist für den Hand- / Notbetrieb. Sobald die Klappe angehoben wird (sie kann mit der gelben Strebe am linken Rand wie bei einem Auto die Motorhaube blockiert werden), schaltet die Anlage auf Handbetrieb und bleibt stehen. (Man kann die leuchtende gelbe Lampe links oben im Eck unter dem Scharnier erkennen.) Nun kann man Seilspannung und Fahr per Hand steuern.

Der rechte Teil ist für den Normalbetrieb / Automatik, wenn die Klappe geschossen ist.

Bei einem Fehler hält die Anlage auch an und die rote Blink-LED meldet sich. (Ich habe diese Lampe vom ft:Blinkbaustein abgekoppelt und durch eine stand-allone Blink-LED in rot ersetzt. Spart Strom, Nerven und sieht viel toller aus, wenn sie nicht im Takt der Kollisionsleuchten blinkt.
Ebenso die LED "Wartung". Ebenfalls außer Takt und als gelbe Blink-LED ausgeführt. (Ich werde hierzu noch Fotos im Bereich Selbstbau hochladen.)

Jetzt habe ich auch das Arbeitslicht angebracht - sehr vorteilhaft wenn ich bei Dunkelheit damit spiele, denn die Brücke selbst ist toll beleuchtet. Da muss doch der Bediener aus Sicherheitsgründen einen beleuchteten Pult bekommen.

Die Abdeckung ist mit Riegeln an entsprechenden Statikbausteinen befestigt und die ganze Bedienung wurde vorher noch einmal versteift.

Und da ist er wieder: der gehasste Magnet-Baustein. Und ich sage euch: "zeigt der Materie wer Herr im Hause ist!" So habe ich die Nase des Bausteins einfach mit Feile gekürzt. Er lässt sich nun ganz in die Klappe einschieben und schaut nicht mehr raus. (Keine Angst, ich habe noch 4 weitere Hass-Objekte im Original aus den 80ern.)