---
layout: "image"
title: "Seilbefestigung an der Fahrbahn"
date: "2016-10-02T17:43:47"
picture: "IMG_20160921_152023a.jpg"
weight: "78"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: ["Knoten", "Seil", "Laufschiene"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/44514
imported:
- "2019"
_4images_image_id: "44514"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-10-02T17:43:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44514 -->
HIer nach der Montage der zweiten Schiene. So ist das Seil nicht mehr im Weg. (Der Knoten wird nun auf die Unterseite in den Träger geschoben, damit nur noch das saubere Seil heraus kommt.