---
layout: "image"
title: "Turm 1 fertig"
date: "2018-09-23T13:24:04"
picture: "Turm-1-fertig.jpg"
weight: "40"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/47918
imported:
- "2019"
_4images_image_id: "47918"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47918 -->
Die Gesamtansicht des Turm 1 (Talstation) von hinter der Anlage aus gesehen (Rückseite)

Zu sehen:

(gelber Pfeil) Das Maschienenhaus hat ein Dach bekommen - die Story hierzu folgt noch mit Bildern.

(blauer Pfeil) Das untere Ende des Schachtes für das Gewicht hat eine Verkleidung und darauf eine Beschriftung (Fahrpreis und Turmdaten) für die Touristen erhalten. (Auch hierzu kommen noch Fotos.)

(grüner Pfeil) das Gewicht ist eingebaut und funktioniert wie es soll. Die Gondel ist nun am Turm angekommen und löst die Gabel der Traverse aus (magenta-Pfeil).

Der Turm selbst hat seit fast 2 Jahren keine Überarbeitung erhalten, schließlich hat mein Sohn Jan (heute 9 Jahre) ihn damals designed und gebaut und es war seine Bedingung, dass ich die Bahn einbaue ohne den Turm zu zerstüren oder zu verändern.

Die Seile die im Bild nach vorne reichen tragen die Fahrbahn.
Hinter dem Turm gehen die Streben herunter, die die Last in den Boden ableiten.

Zu sehen ist, dass sich die Türme auf Holzplatten befinden um leicht zur Ausstellung transportiert werden zu können. Wenn alles klappt brauche ich 5 Minuten zum Abbau und ca. 10 Minuten zum Aufbau.
Für die ft Convention in Dreieich 2018 ist die Anlage außerdem auf Cola-Kisten gestellt worden um sie höher an die neugierigen Nasen der Besucher zu bringen.

Die Anekdate zu dem Bild:
Man erkennt, dass die Anlage in der Tür steht und sie fast ganz blockiert.
Das ist mein Büro, in der die Brücke steht und das Treppenhaus in welches sie hinein ragt.
Meine holde Frau schimpft jedes Mal wenn sie daran vorbei muss um im Büro etwas zu holen.
Aber sie wollte ja, dass "der Kram" aus ihrem Büro verschwindet, das jahrelang unser gemeinsames Spielzimmer war. 
Also cih kann gut damit im Büro leben. (P.S.: die Ehe hält auch nach 16 Jahren noch.)