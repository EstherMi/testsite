---
layout: "image"
title: "Fahrbahnaufhängung / neue Version"
date: "2016-12-28T12:29:46"
picture: "IMG_20161227_110758.jpg"
weight: "71"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: ["statik", "schrägseilbrücke", "loselager"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/44946
imported:
- "2019"
_4images_image_id: "44946"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-12-28T12:29:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44946 -->
Dies ist die neue Fahrbahnaufhängung.

Nachdem ich eine Anregung zur ersten Version erhalten hatte....
(siehe Kommentare http://www.ftcommunity.de/details.php?image_id=44497 )
.... undsich herausgestellt hatte, dass die Fahrbahn sich bei Belastung durch das Fahrzeug verlängert oder zusammenzieht, habe ich die Konstruktion überarbeitet.

Hier nun die wesentlich verbesserte Version. (vgl. o.g. Bild)
- Die Fahrbahn ist nun am Ende mit zwei Alu-Profilen ausgestattet. Diese hängen in zwei Winkelachsen. 
- dadurch kann sich die Fahrbahn in der Längsachse bewegen
- und die Fahrbahn kan nach oben und unten kippen, um den Spannungsbogen aufzubauen. (Konnte sie bei der alten Version auch)