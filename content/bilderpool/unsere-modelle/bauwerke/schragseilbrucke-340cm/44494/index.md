---
layout: "image"
title: "Neuer Turm in Gesamtansicht"
date: "2016-10-01T22:12:29"
picture: "IMG_20160904_182409a.jpg"
weight: "94"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: ["Turm", "Statik", "Brücke"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/44494
imported:
- "2019"
_4images_image_id: "44494"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-10-01T22:12:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44494 -->
Hier der neue Turm alleine. Natürlich wird auch der 2. Turm genau so umgebaut werden, denn beide Türme der Brücke müssen total identisch sein, bevor die Technik (Antrieb) eingebaut werden kann.