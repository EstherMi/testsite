---
layout: "image"
title: "Größen-Übersicht (fast ganz)"
date: "2016-12-31T17:34:58"
picture: "IMG_20161228_112328.jpg"
weight: "59"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: ["schrägseilbrücke", "statik"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/44989
imported:
- "2019"
_4images_image_id: "44989"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-12-31T17:34:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44989 -->
Das Bauwerk ist so groß, dass es nicht ganz auf die AUfnahme passt.
Jan (7) und ich (46) passen locker unter die Fahrbahn.
Im Hintergrund die Talstation (Turm 1).

Freie Spannweite der Fahrbahn: 340cm
Länge des Bauwerks über alles: 510cm
Turmhöhe: 136cm
Fahrbahnhöhe: ca 40cm