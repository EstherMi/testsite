---
layout: "image"
title: "Turm Version 3 (Vergleich von der Seite)"
date: "2016-10-23T20:57:10"
picture: "IMG_20161021_161851a.jpg"
weight: "73"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: ["Schrägseilbrücke", "hängebrücke", "statik"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/44663
imported:
- "2019"
_4images_image_id: "44663"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-10-23T20:57:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44663 -->
Durch die weiter verlängerte Bahn (jetzt 350cm Spannweite) und das dadurch höhere Bahngewicht mussten die Türme verändert werden. Da sich auch die Angriffspunkte für die Schrägseile verschoben waren die Verhältnisse der Kräfte nicht mehr gewahrt.

Die Lösung war, dass die Anschlagpunkte der Seil ehöhergelegt werden mussten, der obere Turmträger stabiler gemacht. (Nuer Turm, Version 3 auf der linken Seite im Vergleich zur Version 2, rechts.)

Folgende Maßnahmen führten zum Ziel:

1. die Fahrbahn wurde wieder vom oberen Lager in das untere eingebaut. Der Nachreil einer kleineren freien Höhe der Spur wurde durch eine bessere Laufkatzenkonstruktion kompensiert.) = türkise Linie

2. Die Anschlagpunkte (=gelbe, lila, grüne und orangene Linie) wurden deutlich höher gesetzt, damit der Seilwinkel später steiler gestellt um die Reichweite durch die längere Fahrbahn zu kompensieren.

3. der obere Ausleger wurde schlanker und mir 3 Scharnieren gelagert

4. der obere Ausleger wurde steiler gestellt und leitet die Kräfte nun mehr in die Senkrechte des Turms, statt mit seinem Eigengewicht auszugleichen. Dadurch wurden die Anschlagpunkte automatisch weiter erhöht und so die Seitenverhältnisse der Schrägseile verbessert.