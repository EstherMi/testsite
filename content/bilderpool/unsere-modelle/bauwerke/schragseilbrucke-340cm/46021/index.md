---
layout: "image"
title: "Antrieb Hängebahn Getriebe (Teil 6)"
date: "2017-07-04T16:25:46"
picture: "a-Gleichlaufgetriebe-14.jpg"
weight: "52"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: ["Gleichlaufgetriebe", "Antrieb", "Motor", "Differenzialgetriebe"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/46021
imported:
- "2019"
_4images_image_id: "46021"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2017-07-04T16:25:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46021 -->
Und richtigrum - fertig.

Bisher hatte ich verschwiegen, dass das Getriebe auf den anderen Bildern eigentlich immer Kopf steht. Die ganze Mechanik muss in ein "Häuschen" rein (ein wenig Optik muss schon sein.)
Außerdem wird es auf die graue Grundplatte direkt montiert, die das Fundament der Talstation der Schrägseilbrücke darstellt.

Hier also das fertige, sauber aufgebaute (ohne unbenutztem 3. Motor) und richtig herum stehende Getriebe. Gezeigt ist die Hauptantriebsseite. Der Motor ist nun wieder gerade und sauber zu positionieren.