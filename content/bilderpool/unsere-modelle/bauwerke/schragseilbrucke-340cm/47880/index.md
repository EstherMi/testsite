---
layout: "image"
title: "Gipsgewicht"
date: "2018-09-23T13:24:03"
picture: "gewicht-verguss.jpg"
weight: "2"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/47880
imported:
- "2019"
_4images_image_id: "47880"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47880 -->
Was macht man, wenn der Sand rieselt?

In meinem Gewicht, das für die Seilspannung sorgt waren anfänglich nur Kieselsteine aus dem Garten.
Leider war das Gewicht zu leicht. Daher habe ich die Zwischenräume mit Sand aufgefüllt.
Leider rieselt dieser aus dem hängenden Kästchen.

Lösung: vergießen der Steinchen mit Gips.