---
layout: "image"
title: "Neues Gleichlaufgetriebe 3"
date: "2018-09-23T13:24:04"
picture: "gleichlaufgetriebe03.jpg"
weight: "5"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: ["Differenzial", "Powermotor", "Gleichlaufgetriebe"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/47883
imported:
- "2019"
_4images_image_id: "47883"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47883 -->
Das Gleichlaufgetriebe für den Seilantrieb von unten.

Der Motor mit der roten Kappe sperrt die Differenziale, bzw. sorg für deren Gegenlauf, um später die Seilspannung zu regulieren.

Der andere Powermotor treibt die Hauptachsen der Differenziale über die Z20 am rechten Bildrand an. Die schwarze Raststange erwies sich da als einfachste und dennoch stabielste Lösung.

Der Abtrieb des Getriebes (= Antrieb der Seilspulen) erfolgt auf der linken Seite über die zwei Z10, die später wieder in Z10 der Spulen greifen

Das Gestell wurde noch weiter versteift und läuft nun absolut sauber.