---
layout: "image"
title: "Antrieb Hängebahn Getriebe (Teil 1)"
date: "2017-07-04T13:01:19"
picture: "a-Gleichlaufgetriebe-12.jpg"
weight: "47"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: ["Gleichlaufgetriebe", "Antrieb", "Klassik", "Motor", "Getriebe", "Differenzialgetriebe"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/46016
imported:
- "2019"
_4images_image_id: "46016"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2017-07-04T13:01:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46016 -->
OK!
Ich hab' es kapiert!
Das von mir entworfene Getriebe (siehe ältere Fotos hier bei der Schrägseilbrücke: Funktionsprototyp http://www.ftcommunity.de/details.php?image_id=44489 ) ist nichts anderes als ein kompliziertes instabiles Gleichlaufgetriebe (siehe: http://www.ftcommunity.de/categories.php?cat_id=31 )

Ein guter Ingenieur schaut immer erst einmal was andere bereits vor ihm gemacht haben.
Und so habe ich mich von NBGer  inspirieren lassen: http://www.ftcommunity.de/details.php?image_id=38975 

Sein recht kompaktes Gleichlaufgetriebe inspirierte mich zur Anordnung der Differenzialgetriebe in dieser Weise.

(Fortsetzung nächstes Bild)