---
layout: "image"
title: "Kabelchaos 2"
date: "2018-09-23T13:24:04"
picture: "gehuse02.jpg"
weight: "35"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/47913
imported:
- "2019"
_4images_image_id: "47913"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47913 -->
Klappe auf!

Eigentlich gefällt mir schon, wie sich die Klappe des Betriebsgebäudes öffnen lässt.
Darunter verbirgt sich der Stecker für das serielle Kabel und Kabelchaos.

Weg damit!