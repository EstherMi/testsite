---
layout: "image"
title: "Neues Gleichlaufgetriebe 2"
date: "2018-09-23T13:24:04"
picture: "gleichlaufgetriebe02.jpg"
weight: "4"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: ["Gleichlaufgetriebe", "Powermotor"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/47882
imported:
- "2019"
_4images_image_id: "47882"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47882 -->
Ich bin nicht ganz zufrieden mit der Motorenaufhängung. Aber die Powermotoren passen einfach nicht in das ft Raster.
Sei es drum. Wie stark sie wirklich sind werde ich erst sehen, wenn der Seilantrieb eingebaut ist.