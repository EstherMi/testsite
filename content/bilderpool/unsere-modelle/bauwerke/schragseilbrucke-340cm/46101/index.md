---
layout: "image"
title: "Schönheits-OP (Teil 1) nachher"
date: "2017-07-29T19:32:20"
picture: "hintere_Seile_korrigiert01.jpg"
weight: "45"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: ["Schrägseilbrücke"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/46101
imported:
- "2019"
_4images_image_id: "46101"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2017-07-29T19:32:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46101 -->
Nach dem Ordnen der Seile und dem neuen Spannen auf (andere) Spulen sind diese geordnet und ergeben mit den Streben der Türme ein harmonischeres Bild.