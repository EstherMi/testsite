---
layout: "image"
title: "Out-Takes Verstärkt"
date: "2018-09-23T13:24:04"
picture: "ausbruch-verstrkung.jpg"
weight: "28"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/47906
imported:
- "2019"
_4images_image_id: "47906"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47906 -->
Die ausgebrochenen Gelenkbausteine (siehe Foto vorher) wurden nun verstärkt. Mit Hilfe von Winkel 30° und dem Baustein 15 ist nun eine bessere Krafteinleitung möglich.

Statisch gesehen ist es günstig nur 2 der 3 Beine zu verstärken, da sonst der Mittlere als Hebelpunkt dienen kann und die Kräfte sich auf einen Ankerpunkt konzentrieren würden.

In der Endversion ersetze ich noch die Baustein 15 durch Winkel. (Wegen der Optik.)