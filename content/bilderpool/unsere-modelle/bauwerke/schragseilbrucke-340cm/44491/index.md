---
layout: "image"
title: "Neuer Turm (links) und alter"
date: "2016-10-01T22:12:29"
picture: "IMG_20160904_182951a.jpg"
weight: "92"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: ["Brücke", "Hängebahn", "Statik", "Turm", "Laufschiene"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/44491
imported:
- "2019"
_4images_image_id: "44491"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2016-10-01T22:12:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44491 -->
Der neue Turm (links) ist nun größer, stabiler und länger geworden. Die Fahrbahn wurde um ca. 8cm höher gebaut. Die endgültige Version des Turms ist noch ein wenig größer, da gewisse Proportionen stimmen müssen.
Der alte Turm (rechts) wurde von Jan (7 Jahre) gestaltet und die Statik gemacht.

Gut zu sehen ist auch, dass in der alten Variante die Fahrbahn nur eine Schiene hatte und damit nicht so sehr biegestabil war.