---
layout: "image"
title: "Fahrbereit"
date: "2018-09-23T13:24:04"
picture: "turm01.jpg"
weight: "14"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/47892
imported:
- "2019"
_4images_image_id: "47892"
_4images_cat_id: "3291"
_4images_user_id: "2638"
_4images_image_date: "2018-09-23T13:24:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47892 -->
Endlich hängt der Wagen dran.

Unter der Fahrbahn hängt nun die Gondel.
Im HIntergrund der Turm 1 mit seinem deutlich sichtbaren hängenden Schacht für das Gewicht der Seilspannung.

Zu erkennen ist hier noch die alte Version mit den gläsernen Reedkontakten (siehe Bilder vorher). Das untere Ende des Schachts ist mit Statikplatten abgeschirmt. Schließlich sollen die Touristen später nicht in die Maschiene kommen. AUßerdem verbirgt sich dahinter die zwei Z15 zur Umlenkung der Kette und die Endschalter, die das Gewicht überwachen.