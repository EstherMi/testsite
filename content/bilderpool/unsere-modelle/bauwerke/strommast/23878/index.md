---
layout: "image"
title: "strommast05.jpg"
date: "2009-05-05T16:44:02"
picture: "strommast05.jpg"
weight: "5"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- details/23878
imported:
- "2019"
_4images_image_id: "23878"
_4images_cat_id: "1632"
_4images_user_id: "791"
_4images_image_date: "2009-05-05T16:44:02"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23878 -->
