---
layout: "image"
title: "Dachkonstruktion"
date: "2018-10-14T22:40:11"
picture: "kleineshausmitangeberflitzer7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48211
imported:
- "2019"
_4images_image_id: "48211"
_4images_cat_id: "3537"
_4images_user_id: "104"
_4images_image_date: "2018-10-14T22:40:11"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48211 -->
Das Dach ruht auf klassischen Winkelsteinen 30° rechtwinkelig. In deren seitliches Nut-Loch greifen die dreieckigen Giebelteile. Der Rest sind ein paar Bausteine, mit einem Winkelstein 60° verbunden, und in dessen obenliegender Nut sitzt der Dachfirst, der seinerseits wieder den Kaminbaustein trägt. Auch die Innenseiten der Dachträger sind verkleidet.