---
layout: "comment"
hidden: true
title: "24230"
date: "2018-10-15T09:33:48"
uploadBy:
- "geometer"
license: "unknown"
imported:
- "2019"
---
Hallo Stefan,

wunderschön retro. Aber wie hast du die Flachsteine so kratzerlos transparent bekommen? Meine sind trüb und zerkratzt. Die wurden damals heftig bespielt.

Viele Grüße, Thomas