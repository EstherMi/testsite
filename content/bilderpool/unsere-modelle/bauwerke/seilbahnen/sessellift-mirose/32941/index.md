---
layout: "image"
title: "Doppelsessel - Seitenansicht"
date: "2011-09-27T23:24:31"
picture: "Sessellift_Sessel_01.jpg"
weight: "1"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/32941
imported:
- "2019"
_4images_image_id: "32941"
_4images_cat_id: "2421"
_4images_user_id: "765"
_4images_image_date: "2011-09-27T23:24:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32941 -->
