---
layout: "image"
title: "Stütze mit 4 Rollen"
date: "2011-09-27T23:24:31"
picture: "Sessellift_Sttze_4_Rollen_14.jpg"
weight: "25"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/32965
imported:
- "2019"
_4images_image_id: "32965"
_4images_cat_id: "2421"
_4images_user_id: "765"
_4images_image_date: "2011-09-27T23:24:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32965 -->
hier die Holzscheiben, die den Stützenkopf im Rohr verankert