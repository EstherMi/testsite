---
layout: "image"
title: "Stütze mit 8 Rollen"
date: "2011-09-27T23:37:33"
picture: "Sessellift_Sttze_8_Rollen_14.jpg"
weight: "38"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/32978
imported:
- "2019"
_4images_image_id: "32978"
_4images_cat_id: "2421"
_4images_user_id: "765"
_4images_image_date: "2011-09-27T23:37:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32978 -->
