---
layout: "comment"
hidden: true
title: "13633"
date: "2011-02-19T10:56:45"
uploadBy:
- "majus"
license: "unknown"
imported:
- "2019"
---
Tach, 
eigentlich sollten das verschiedene Bilder sein und man sollte auf dem Bild unten noch mehr sehen...
(Letzteres scheint an der Community zu liegen)

Also:
Auf Bild 1 erkennt man die Gondel am Seil (Bin ich nicht ein begnadeter Künstler...?)
Die untere Rolle wird auf Bild 2 über die seitliche Schiene in der Station geführt. Hier sieht man das ganze in echt: http://www.cablecar.ch/images/blausee5.jpg
Auf Bild 3 wird die Bahn dann über die Doppelrolle an beiden Seiten geführt.
Bild 4 ist dann nur noch einmal ein Ausschnitt von Bild 3. 
Bild 5 soll zur Vereinfachung nur noch einmal die Aufhängungen von oben zeigen.
Alle diese Bilder zeigen die Führung des Sessels.
Auf Bild 6 sieht man oben den Stahlträger, der die obere Rolle des Sessels nach unten drückt. (Welche genaue Folge das hat zeigt mein anderes Bild)
Bild 6 zeigt dann, dass das Seil nach unten weggeführt wird, da die Klemme mithilfe der oberen Rolle geöffnet wurde.
Zuletzt Bild 7, dass nur noch einmal zeigt, wie der Antrieb nach der Abkopplung vom Seil
angetrieben wird. (Achtet auch auf die Übersetzung aus Keilriemen)
Diese Skizze verwendete ich einmal für einen Vortrag, daher  sind die Einführungsbilder üppig und daher ist auch ein Bild der Übersetzung beigefügt...

Gruß
majus