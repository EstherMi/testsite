---
layout: "image"
title: "Umlauf - Kuppelseilbahn 2"
date: "2010-12-08T21:53:46"
picture: "Kuppelseilbahn-Web-1.2.jpg"
weight: "12"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "majus"
license: "unknown"
legacy_id:
- details/29441
imported:
- "2019"
_4images_image_id: "29441"
_4images_cat_id: "2141"
_4images_user_id: "1239"
_4images_image_date: "2010-12-08T21:53:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29441 -->
Hier sieht man die Bergstation meiner Umlauf - Kuppelseilbahn. Besonders schwierig erwieß sich der Mast.