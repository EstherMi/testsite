---
layout: "comment"
hidden: true
title: "15908"
date: "2011-12-22T22:20:17"
uploadBy:
- "Ma-gi-er"
license: "unknown"
imported:
- "2019"
---
@majus: nein, leider habe ich keinen funktionstüchtigen Masten. Ich bin an der Federung gescheitert.
Ich habe mit Statikstreben (die mit den vielen Löchern) das Federungssystem gebaut, die "Federglieder" habe ich einer Gondelbahn abgeschaut. Das hauptproblem war dann die Federung, ich habe mit Gummis herumprobiert, jedoch noch keine gut funktionierende Lösung gefunden.