---
layout: "image"
title: "Querschnitt"
date: "2011-02-16T17:10:16"
picture: "Querschnitt_001.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "majus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "majus"
license: "unknown"
legacy_id:
- details/30003
imported:
- "2019"
_4images_image_id: "30003"
_4images_cat_id: "2141"
_4images_user_id: "1239"
_4images_image_date: "2011-02-16T17:10:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30003 -->
Hier sieht man den Querschnitt der original Doppelmayr-Seilbahn.