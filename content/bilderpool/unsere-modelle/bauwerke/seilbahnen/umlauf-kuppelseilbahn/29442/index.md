---
layout: "image"
title: "Umlauf - Kuppelseilbahn 3"
date: "2010-12-08T21:53:46"
picture: "Kuppelseilbahn-Web-1.3.jpg"
weight: "13"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "majus"
license: "unknown"
legacy_id:
- details/29442
imported:
- "2019"
_4images_image_id: "29442"
_4images_cat_id: "2141"
_4images_user_id: "1239"
_4images_image_date: "2010-12-08T21:53:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29442 -->
Hier noch ein Detailbild von der Talstation. Dieses Bild eignet sich´, um die Funktion der Bügel zu verstehen.