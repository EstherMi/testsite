---
layout: "comment"
hidden: true
title: "15815"
date: "2011-12-03T12:29:30"
uploadBy:
- "Marten70"
license: "unknown"
imported:
- "2019"
---
Hallo Majus, tolles Teil! Die Gondeln aus Papier sind sehr gewitzt, das hätte ich mir nie gedacht.

Frage: Warum steht die Antriebsrolle nicht senkrecht auf das Seil? Da wird es doch Abtrieb  des Reifens geben? Das wäre schade. 

mfr Gr. Marten