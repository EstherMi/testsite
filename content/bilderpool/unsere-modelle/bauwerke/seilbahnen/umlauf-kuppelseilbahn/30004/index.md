---
layout: "image"
title: "Übersicht"
date: "2011-02-16T17:10:16"
picture: "bersicht_001.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "majus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "majus"
license: "unknown"
legacy_id:
- details/30004
imported:
- "2019"
_4images_image_id: "30004"
_4images_cat_id: "2141"
_4images_user_id: "1239"
_4images_image_date: "2011-02-16T17:10:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30004 -->
Hier kann man die Technik des Ausklinkens nacheinander sehen.
Es beginnt von oben -> unten