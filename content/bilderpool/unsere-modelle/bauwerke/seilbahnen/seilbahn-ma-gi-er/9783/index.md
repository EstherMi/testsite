---
layout: "image"
title: "Seilbahn 1"
date: "2007-03-25T21:57:05"
picture: "seilbahn1.jpg"
weight: "1"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/9783
imported:
- "2019"
_4images_image_id: "9783"
_4images_cat_id: "882"
_4images_user_id: "445"
_4images_image_date: "2007-03-25T21:57:05"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9783 -->
Variante mit Mast.