---
layout: "image"
title: "Gesamtansicht"
date: "2011-07-15T15:50:01"
picture: "seilbahnlightman02.jpg"
weight: "2"
konstrukteure: 
- "Ingo Koinzer"
fotografen:
- "Ingo Koinzer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lightman"
license: "unknown"
legacy_id:
- details/31289
imported:
- "2019"
_4images_image_id: "31289"
_4images_cat_id: "2326"
_4images_user_id: "842"
_4images_image_date: "2011-07-15T15:50:01"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31289 -->
