---
layout: "image"
title: "Bergstation Ausfahrt"
date: "2011-07-15T15:50:01"
picture: "seilbahnlightman14.jpg"
weight: "14"
konstrukteure: 
- "Ingo Koinzer"
fotografen:
- "Ingo Koinzer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lightman"
license: "unknown"
legacy_id:
- details/31301
imported:
- "2019"
_4images_image_id: "31301"
_4images_cat_id: "2326"
_4images_user_id: "842"
_4images_image_date: "2011-07-15T15:50:01"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31301 -->
Hier "plumst" der Sessel mit seinem Haken einfach auf das Seil, wenn die Schiene zuende ist.
Talfahrt: http://vimeo.com/26469719