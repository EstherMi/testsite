---
layout: "image"
title: "Stromverteiler"
date: "2011-07-15T15:50:01"
picture: "seilbahnlightman05.jpg"
weight: "5"
konstrukteure: 
- "Ingo Koinzer"
fotografen:
- "Ingo Koinzer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lightman"
license: "unknown"
legacy_id:
- details/31292
imported:
- "2019"
_4images_image_id: "31292"
_4images_cat_id: "2326"
_4images_user_id: "842"
_4images_image_date: "2011-07-15T15:50:01"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31292 -->
Dieses Modul versorgt das eine Interface ständig. Wird das Interface vom Programm erkannt, schaltet dieses ein Relais, und mit dem Taster kann das zweite Interface eingeschaltet werden, an dem die Motoren angeschlossen sind. Hier kann man auch einen Not-Aus-Knopf anschließen. Wird das Programm geschlossen, schaltet auch die Versorgungsspannung wieder ab.