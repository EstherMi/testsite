---
layout: "image"
title: "Bergstation Bügelöffner"
date: "2011-07-15T15:50:01"
picture: "seilbahnlightman22.jpg"
weight: "22"
konstrukteure: 
- "Ingo Koinzer"
fotografen:
- "Ingo Koinzer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lightman"
license: "unknown"
legacy_id:
- details/31309
imported:
- "2019"
_4images_image_id: "31309"
_4images_cat_id: "2326"
_4images_user_id: "842"
_4images_image_date: "2011-07-15T15:50:01"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31309 -->
Der Öffner funktioniert wie der Schließer, nur anders herum.
http://vimeo.com/26470358