---
layout: "image"
title: "Bergstation Einfahrt Kollisionsvermeidung"
date: "2011-07-15T15:50:01"
picture: "seilbahnlightman13.jpg"
weight: "13"
konstrukteure: 
- "Ingo Koinzer"
fotografen:
- "Ingo Koinzer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lightman"
license: "unknown"
legacy_id:
- details/31300
imported:
- "2019"
_4images_image_id: "31300"
_4images_cat_id: "2326"
_4images_user_id: "842"
_4images_image_date: "2011-07-15T15:50:01"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31300 -->
