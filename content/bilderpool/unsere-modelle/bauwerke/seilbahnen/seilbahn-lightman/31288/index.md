---
layout: "image"
title: "Gesamtansicht"
date: "2011-07-15T15:50:01"
picture: "seilbahnlightman01.jpg"
weight: "1"
konstrukteure: 
- "Ingo Koinzer"
fotografen:
- "Ingo Koinzer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lightman"
license: "unknown"
legacy_id:
- details/31288
imported:
- "2019"
_4images_image_id: "31288"
_4images_cat_id: "2326"
_4images_user_id: "842"
_4images_image_date: "2011-07-15T15:50:01"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31288 -->
Das ist mein Sessellift. Das Besondere ist, dass die Sessel in den Stationen vom Seil genommen werden, um einen gemütlichen und sicheren Ein- und Ausstieg zu ermöglichen. Ich habe noch keine Seilbahn aus Fischertechnik gesehen, die das kann. Naja, und das macht dieses Exemplar auch etwas kompliziert. Die größte Abweichung von Originalen besteht darin, dass bei mir das Seil in der Station nach aussen abgelenkt wird und über viele Rollen zur anderen Seite gelangt, anstatt dass es innen von einer großen Rolle umgelenkt wird. Ich hab aber keine andere Möglichkeit gefunden. So müssen nun auch die Stützen zwischen den Stationen das Seil von aussen halten, da ja nun auch die Sessel die Seilhalterung an der anderen Seite haben. Die Bahn läuft keineswegs störungsfrei, sondern gerade so, dass man Glück hat, wenn ein Sessel die andere Station erreicht, ohne dass das Seil oder der Sessel herunterfällt. Aber keine Angst: im Original werden die Sessel auf das Seil geklemmt und sollte das Seil von den Rollen fallen, wird es noch von Haken an den Stützen aufgehalten. Aber soweit kommt es sowieso nicht ;).