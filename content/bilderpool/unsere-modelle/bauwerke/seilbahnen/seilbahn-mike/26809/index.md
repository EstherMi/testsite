---
layout: "image"
title: "Seilbahn 3"
date: "2010-03-23T21:06:12"
picture: "seilbahn3.jpg"
weight: "3"
konstrukteure: 
- "Michael Biehl und Fabian"
fotografen:
- "Michael Biehl und Fabian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mike"
license: "unknown"
legacy_id:
- details/26809
imported:
- "2019"
_4images_image_id: "26809"
_4images_cat_id: "1915"
_4images_user_id: "1051"
_4images_image_date: "2010-03-23T21:06:12"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26809 -->
Talstation