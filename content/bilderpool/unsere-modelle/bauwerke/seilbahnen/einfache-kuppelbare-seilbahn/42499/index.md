---
layout: "image"
title: "Detail Seil zur Kette"
date: "2015-12-09T12:11:32"
picture: "personenseilbahn05.jpg"
weight: "5"
konstrukteure: 
- "Dieter, Uli und Agnes"
fotografen:
- "dito"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- details/42499
imported:
- "2019"
_4images_image_id: "42499"
_4images_cat_id: "3159"
_4images_user_id: "1582"
_4images_image_date: "2015-12-09T12:11:32"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42499 -->
Der direkte Antrieb mit dem Motor macht die Sache einfach