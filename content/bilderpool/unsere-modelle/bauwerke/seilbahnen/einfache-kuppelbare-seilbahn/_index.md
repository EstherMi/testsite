---
layout: "overview"
title: "Einfache Kuppelbare Seilbahn"
date: 2019-12-17T19:50:09+01:00
legacy_id:
- categories/3159
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3159 --> 
Dies ist eine Seilbahn, die nach etlichem Tüfteln es schafft, Figuren an der Berg- und Talstation automatisch vom Seil zu heben, damit sie langsam ein- und aussteigen können. Die Idee war, möglichst einfach die Funktion nachzubauen. Der Geschwindigkeitsunterschied ist recht groß, was das an- und abkoppeln zu einer spannenden Geschichte macht. Movies gibts unter: www.dieterb.de/ft/Seite_1.mov, www.dieterb.de/ft/Seite_2.mov und www.dieterb.de/ft/vorne.mov. Viel Spaß beim weiterbauen!