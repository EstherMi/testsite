---
layout: "image"
title: "Antrieb"
date: "2015-12-09T12:11:32"
picture: "personenseilbahn12.jpg"
weight: "12"
konstrukteure: 
- "Dieter, Uli und Agnes"
fotografen:
- "dito"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- details/42506
imported:
- "2019"
_4images_image_id: "42506"
_4images_cat_id: "3159"
_4images_user_id: "1582"
_4images_image_date: "2015-12-09T12:11:32"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42506 -->
Der Antrieb das Hauptseils läuft flott über einen großen M-Motor und einem großen roten Rad auf einer Metallstange.