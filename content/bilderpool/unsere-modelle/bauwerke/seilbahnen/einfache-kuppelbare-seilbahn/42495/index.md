---
layout: "image"
title: "Übersicht"
date: "2015-12-09T12:11:32"
picture: "personenseilbahn01.jpg"
weight: "1"
konstrukteure: 
- "Dieter, Uli und Agnes"
fotografen:
- "dito"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- details/42495
imported:
- "2019"
_4images_image_id: "42495"
_4images_cat_id: "3159"
_4images_user_id: "1582"
_4images_image_date: "2015-12-09T12:11:32"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42495 -->
Die Raupenketten nehmen die Gondel auf, drehen sie um und legen sie wieder auf das Seil