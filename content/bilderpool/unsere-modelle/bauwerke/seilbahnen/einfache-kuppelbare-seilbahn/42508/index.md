---
layout: "image"
title: "Manschgerl von vorne"
date: "2015-12-09T12:11:32"
picture: "personenseilbahn14.jpg"
weight: "14"
konstrukteure: 
- "Dieter, Uli und Agnes"
fotografen:
- "dito"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- details/42508
imported:
- "2019"
_4images_image_id: "42508"
_4images_cat_id: "3159"
_4images_user_id: "1582"
_4images_image_date: "2015-12-09T12:11:32"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42508 -->
Man sieht, dass die Stange zum Seil auf einer Seite ist - damit er sich vom Seil trennen kann.