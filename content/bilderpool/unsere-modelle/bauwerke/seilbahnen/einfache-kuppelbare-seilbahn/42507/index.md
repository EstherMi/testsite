---
layout: "image"
title: "Manschgerl eine Seite"
date: "2015-12-09T12:11:32"
picture: "personenseilbahn13.jpg"
weight: "13"
konstrukteure: 
- "Dieter, Uli und Agnes"
fotografen:
- "dito"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- details/42507
imported:
- "2019"
_4images_image_id: "42507"
_4images_cat_id: "3159"
_4images_user_id: "1582"
_4images_image_date: "2015-12-09T12:11:32"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42507 -->
Der Schwerpunkt des Mannschgerls ist wichtig, damit er gerade unter dem Seil hängt. Er ist recht mutig, weil er doch hin und wieder in die Tiefe stürzt