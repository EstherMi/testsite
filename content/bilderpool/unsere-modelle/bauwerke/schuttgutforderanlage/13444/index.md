---
layout: "image"
title: "Schüttgutförderanlage01"
date: "2008-01-27T17:30:27"
picture: "schuettgutfoerderanlage1.jpg"
weight: "1"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/13444
imported:
- "2019"
_4images_image_id: "13444"
_4images_cat_id: "1223"
_4images_user_id: "729"
_4images_image_date: "2008-01-27T17:30:27"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13444 -->
Schüttgutförderanlage