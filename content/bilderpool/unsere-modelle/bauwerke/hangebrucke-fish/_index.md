---
layout: "overview"
title: "Hängebrücke (fish)"
date: 2019-12-17T19:50:15+01:00
legacy_id:
- categories/2465
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2465 --> 
Die Hängebrücke hat eine Spannweite von 72cm. Sie besitzt 14 Tragseile die von den beiden Brückenköpfen ausgehen. An den beiden Brückenenden gibt es mit Textilklebeband beklebte Rampen über die schmale Fahrzeuge auf die Brücke fahren können. Die Seile werden von den Statik Riegeln 4mm gehalten um die, die Schnur gefädelt wurde. So können die Seile nachgestellt werden. An der Brücke sind die Seile fest gebunden.