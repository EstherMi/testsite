---
layout: "overview"
title: "Eiffelturm"
date: 2019-12-17T19:49:02+01:00
legacy_id:
- categories/2110
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2110 --> 
Der Maßstab ist ca. 1:100,
der Eiffelturm ist somit ca.3 m hoch.
Er hat eine kleine Beleuchtung, aber leider keinen Aufzug.