---
layout: "comment"
hidden: true
title: "12643"
date: "2010-10-29T16:56:27"
uploadBy:
- "stephan"
license: "unknown"
imported:
- "2019"
---
Beim echten ist nur die erste Plattform in der Mitte offen. Alle anderen sind geschlossen. 
Wenn du später mal mehr Platten hast kannst du ihn ja einfach umbauen oder neu bauen.