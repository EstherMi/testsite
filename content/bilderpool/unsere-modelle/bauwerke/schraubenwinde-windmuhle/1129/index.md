---
layout: "image"
title: "Schraubenwinde-Windmuhle"
date: "2003-05-14T18:20:43"
picture: "FT-vijzelwindmolen-13.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/1129
imported:
- "2019"
_4images_image_id: "1129"
_4images_cat_id: "116"
_4images_user_id: "22"
_4images_image_date: "2003-05-14T18:20:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1129 -->
