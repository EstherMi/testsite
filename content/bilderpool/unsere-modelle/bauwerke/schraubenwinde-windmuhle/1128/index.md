---
layout: "image"
title: "Schraubenwinde-Windmuhle-12"
date: "2003-05-14T18:20:43"
picture: "FT-vijzelwindmolen-12.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/1128
imported:
- "2019"
_4images_image_id: "1128"
_4images_cat_id: "116"
_4images_user_id: "22"
_4images_image_date: "2003-05-14T18:20:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1128 -->
