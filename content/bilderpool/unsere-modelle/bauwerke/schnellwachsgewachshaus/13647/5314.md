---
layout: "comment"
hidden: true
title: "5314"
date: "2008-02-15T20:33:05"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Sauber! Wenn du es jetzt noch soweit verkleinerst, dass ich achte davon in den Unterboden von so einem Flieger reinkriege, dann gibt's von mir noch einen Orden obendrauf :-)

Gruß,
Harald