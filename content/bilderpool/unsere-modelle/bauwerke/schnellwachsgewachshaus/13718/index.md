---
layout: "image"
title: "Wassertank"
date: "2008-02-21T20:46:22"
picture: "Schnellwachsgewchshaus86.jpg"
weight: "11"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/13718
imported:
- "2019"
_4images_image_id: "13718"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2008-02-21T20:46:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13718 -->
Das ist der wassertank mit seiner Halterung. Zum Kabel, welches Im Tank hängt, habe ich ein 5K Widerstand parallel geschaltet, so kann man über AX/AY messen, ob noch Wasser im Tank ist. Das funktioniert auch sehr gut. (ich habe mal gemessen, der wasserwiderstand liegt bei etwa 100K)