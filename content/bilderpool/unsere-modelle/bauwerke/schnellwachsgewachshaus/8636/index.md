---
layout: "image"
title: "Schnellwachsgewächshaus"
date: "2007-01-23T17:38:42"
picture: "Schnellwachsgewchshaus13.jpg"
weight: "82"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/8636
imported:
- "2019"
_4images_image_id: "8636"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2007-01-23T17:38:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8636 -->
Das ist ein Handventil, das per M-Mot geöffnet/geschlossen werden kann. Wenn gegossen wird und man macht den Kompressor aus schließt man auch das Ventil, damit kein Wasser mehr nach fließt.