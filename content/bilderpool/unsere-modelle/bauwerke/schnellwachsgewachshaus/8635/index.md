---
layout: "image"
title: "Schnellwachsgewächshaus"
date: "2007-01-23T17:38:42"
picture: "Schnellwachsgewchshaus12.jpg"
weight: "81"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/8635
imported:
- "2019"
_4images_image_id: "8635"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2007-01-23T17:38:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8635 -->
Das ist der Kompressor, der das Wasser in die Töpfe gießt. Den hab ich hier auch schon mal ausgestellt.