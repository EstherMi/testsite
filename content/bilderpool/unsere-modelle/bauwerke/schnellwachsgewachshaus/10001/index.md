---
layout: "image"
title: "Schnellwachsgewächshaus"
date: "2007-04-06T19:01:52"
picture: "Schnellwachsgewchshaus57.jpg"
weight: "34"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/10001
imported:
- "2019"
_4images_image_id: "10001"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2007-04-06T19:01:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10001 -->
Für das pneumatische Öffnen der Lüftungsklappe hab ich einen größeren Tank gebraucht. Deswegen habe ich mir hier einen 0,33L Tank gebaut.