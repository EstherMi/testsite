---
layout: "image"
title: "Schnellwachsgewächshaus"
date: "2007-04-28T17:52:10"
picture: "Schnellwachsgewchshaus63.jpg"
weight: "25"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/10183
imported:
- "2019"
_4images_image_id: "10183"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2007-04-28T17:52:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10183 -->
Das ist eine meiner Paprikapflanzen. Ich habe diese Schilder gebastelt, damit man die verschiedenen Sorten unterscheiden kann.