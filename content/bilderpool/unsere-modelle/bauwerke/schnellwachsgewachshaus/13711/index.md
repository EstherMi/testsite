---
layout: "image"
title: "Oberansicht"
date: "2008-02-20T19:54:17"
picture: "Schnellwachsgewchshaus79.jpg"
weight: "15"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/13711
imported:
- "2019"
_4images_image_id: "13711"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2008-02-20T19:54:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13711 -->
Hier sieht man den Ventilator und den Fotowiderstand. Der Fotowiderstand misst die Helligkeit. Die Lampen zur Beleuchtung werden mit dem Fotowiderstand über den Flip-Flop-Baustein gesteuert.