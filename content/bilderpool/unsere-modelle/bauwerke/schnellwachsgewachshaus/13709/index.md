---
layout: "image"
title: "Gesamt"
date: "2008-02-20T19:54:16"
picture: "Schnellwachsgewchshaus77.jpg"
weight: "13"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/13709
imported:
- "2019"
_4images_image_id: "13709"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2008-02-20T19:54:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13709 -->
Das ist mein Gewächshaus. Ich habe versucht, das meiste an Elektronik und Mechanik unter dem Podest unterzubringen. Nur der Wassertank und die Ventile kommen auf eine Extraplatte.