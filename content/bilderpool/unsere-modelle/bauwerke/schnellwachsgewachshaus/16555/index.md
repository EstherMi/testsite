---
layout: "image"
title: "Ventile"
date: "2008-12-06T12:02:40"
picture: "Gewchshaus9.3.jpg"
weight: "3"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/16555
imported:
- "2019"
_4images_image_id: "16555"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2008-12-06T12:02:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16555 -->
Es werden mithilfe der 4 Ventile wieder 8 Töpfe gegossen.
Diesmal sind die Mini-Motoren 3:1 Untersetzt, die Taster entfallen, die Potis bleiben, da ich mit dem ATMega eine sehr genau Referenzspannung habe. (Tschau IF...)