---
layout: "image"
title: "Innenraum"
date: "2008-12-06T12:02:40"
picture: "Gewchshaus9.4.jpg"
weight: "4"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/16556
imported:
- "2019"
_4images_image_id: "16556"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2008-12-06T12:02:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16556 -->
Das ist der noch relativ leere Innenraum.
Es befindet sich dort ein Kompressor, der das Wasser vom Wassertank in die Töpfe pumpen wird.
Links ist die große Lochrasterplatine befestigt (mit 4 M4-Schrauben).
Sie ist 150x200mm groß.