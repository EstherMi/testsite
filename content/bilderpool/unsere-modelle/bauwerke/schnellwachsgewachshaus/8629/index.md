---
layout: "image"
title: "Schnellwachsgewächshaus"
date: "2007-01-23T17:38:32"
picture: "Schnellwachsgewchshaus6.jpg"
weight: "75"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/8629
imported:
- "2019"
_4images_image_id: "8629"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2007-01-23T17:38:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8629 -->
Die Halterung kann man auch aufklappen.
So kann man die Flasche einfach herausnehmen und nachfüllen.