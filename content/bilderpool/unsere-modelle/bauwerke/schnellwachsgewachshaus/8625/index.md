---
layout: "image"
title: "Schnellwachsgewächshaus"
date: "2007-01-23T17:38:32"
picture: "Schnellwachsgewchshaus2.jpg"
weight: "71"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/8625
imported:
- "2019"
_4images_image_id: "8625"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2007-01-23T17:38:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8625 -->
Hier sieht man das Gewächshaus. Es sollen 8 Paprikas darin wachsen. Gebaut hab ich das ,weil ich letztes Jahr schon mal Paprikas gepflanzt hab und die Ernte war super!!!