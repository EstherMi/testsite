---
layout: "image"
title: "Schnellwachsgewächshaus"
date: "2007-01-23T17:38:32"
picture: "Schnellwachsgewchshaus8.jpg"
weight: "77"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/8631
imported:
- "2019"
_4images_image_id: "8631"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2007-01-23T17:38:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8631 -->
Das ist der Schlauch der in der Flasche nach ganz unten geht. An ihm sind 2 Kabel.
Wenn Wasser in der Flasche ist, ist der Stromkreis geschlossen. Ist das Wasser leer, ist der Stromkreis unterbrochen. So kann man Alarmgeben, wenn das Wasser leer ist.