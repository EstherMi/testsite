---
layout: "image"
title: "Paprika"
date: "2007-08-25T12:44:10"
picture: "Schnellwachsgewchshaus73.jpg"
weight: "23"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/11406
imported:
- "2019"
_4images_image_id: "11406"
_4images_cat_id: "794"
_4images_user_id: "456"
_4images_image_date: "2007-08-25T12:44:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11406 -->
Das ist eine normale Paprika. Die wächst aber noch.