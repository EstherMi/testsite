---
layout: "image"
title: "Kaiser-Wilhelm-Brücke (Gesamtansicht)"
date: "2011-04-16T18:30:46"
picture: "kaiserwilhelmbrueckewilhelmshavenklein6.jpg"
weight: "6"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/30467
imported:
- "2019"
_4images_image_id: "30467"
_4images_cat_id: "2269"
_4images_user_id: "1126"
_4images_image_date: "2011-04-16T18:30:46"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30467 -->
Jetzt fehlt noch die "Kür": Ampel, zeitgesteuerte Drehautomatik, Schranken, ... und natürlich die zweite Brückenhälfte.
Einen tollen Eindruck von der Originalbrücke in Funktion zeigt das Video unter http://www.youtube.com/watch?v=5yF3HJ7sSyk.