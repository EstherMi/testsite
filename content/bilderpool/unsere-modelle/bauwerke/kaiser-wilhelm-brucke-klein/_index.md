---
layout: "overview"
title: "Kaiser-Wilhelm-Brücke (klein)"
date: 2019-12-17T19:50:11+01:00
legacy_id:
- categories/2269
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2269 --> 
Die "kleine Schwester" des Modells der Wilhelmshavener Kaiser-Wilhelm-Brücke von stephan (http://www.ftcommunity.de/categories.php?cat_id=1227).