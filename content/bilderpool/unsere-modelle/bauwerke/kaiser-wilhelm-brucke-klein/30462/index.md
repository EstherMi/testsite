---
layout: "image"
title: "Kaiser-Wilhelm-Brücke (Draufsicht)"
date: "2011-04-16T18:30:45"
picture: "kaiserwilhelmbrueckewilhelmshavenklein1.jpg"
weight: "1"
konstrukteure: 
- "Dirk Fox & Johann Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/30462
imported:
- "2019"
_4images_image_id: "30462"
_4images_cat_id: "2269"
_4images_user_id: "1126"
_4images_image_date: "2011-04-16T18:30:45"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30462 -->
Hier eine etwas kleinere Variante von Stephans Modell der Kaiser-Wilhelm-Brücke von 1907, siehe http://www.ftcommunity.de/categories.php?cat_id=1227. Sie ist wahrscheinlich eher für einspurige Verkehrsführung geeignet... Außerdem haben wir nur eine der beiden (identischen) Teile dieser faszinierenden Drehbrücke realisiert - die grauen 169,6er X-Streben wurden knapp. Aber auch so kommt sie auf 180 cm Länge (10 rote Statikplatten), 73 cm Höhe und ca. 3.000 g Statikteile.