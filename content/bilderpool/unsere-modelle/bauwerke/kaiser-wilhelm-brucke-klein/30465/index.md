---
layout: "image"
title: "Kaiser-Wilhelm-Brücke (Drehpfeiler)"
date: "2011-04-16T18:30:46"
picture: "kaiserwilhelmbrueckewilhelmshavenklein4.jpg"
weight: "4"
konstrukteure: 
- "Dirk Fox & Johann Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/30465
imported:
- "2019"
_4images_image_id: "30465"
_4images_cat_id: "2269"
_4images_user_id: "1126"
_4images_image_date: "2011-04-16T18:30:46"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30465 -->
Der Antrieb im Drehpfeiler erfolgt über einen Drehkranz mit Rastschnecke. Das Gewicht der Brücke stützt sich auf den Drehkranz und vier Seilrollen, die (ähnlich der Originalbrücke, siehe http://www.ftcommunity.de/details.php?image_id=13457) auf einem Kreisbogen aus gebogenen Flachträgern (60°) laufen. (An der Konstruktion hatte Johann wesentlichen Anteil.)