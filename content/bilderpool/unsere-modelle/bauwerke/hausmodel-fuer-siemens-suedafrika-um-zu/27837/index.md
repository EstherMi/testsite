---
layout: "image"
title: "hausemodelfuersiemens04.jpg"
date: "2010-08-21T17:39:42"
picture: "hausemodelfuersiemens04.jpg"
weight: "4"
konstrukteure: 
- "Sascha Lipka"
fotografen:
- "Sascha Lipka"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "saschalipka"
license: "unknown"
legacy_id:
- details/27837
imported:
- "2019"
_4images_image_id: "27837"
_4images_cat_id: "2015"
_4images_user_id: "635"
_4images_image_date: "2010-08-21T17:39:42"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27837 -->
