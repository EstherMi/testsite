---
layout: "image"
title: "hausemodelfuersiemens12.jpg"
date: "2010-08-21T17:40:53"
picture: "hausemodelfuersiemens12.jpg"
weight: "12"
konstrukteure: 
- "Sascha Lipka"
fotografen:
- "Sascha Lipka"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "saschalipka"
license: "unknown"
legacy_id:
- details/27845
imported:
- "2019"
_4images_image_id: "27845"
_4images_cat_id: "2015"
_4images_user_id: "635"
_4images_image_date: "2010-08-21T17:40:53"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27845 -->
