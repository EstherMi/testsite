---
layout: "image"
title: "hausemodelfuersiemens01.jpg"
date: "2010-08-21T17:39:41"
picture: "hausemodelfuersiemens01.jpg"
weight: "1"
konstrukteure: 
- "Sascha Lipka"
fotografen:
- "Sascha Lipka"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "saschalipka"
license: "unknown"
legacy_id:
- details/27834
imported:
- "2019"
_4images_image_id: "27834"
_4images_cat_id: "2015"
_4images_user_id: "635"
_4images_image_date: "2010-08-21T17:39:41"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27834 -->
