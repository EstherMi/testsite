---
layout: "overview"
title: "Hausmodel fuer Siemens Suedafrika um Hausautomatisierung zu zeigen"
date: 2019-12-17T19:48:50+01:00
legacy_id:
- categories/2015
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2015 --> 
Haus wird mit einer Siemens LOGO! angesteuert.

Automatisierung von:
Licht
Alarm
Garagenmotor
Tormotor
Beuler
Solarzellen
Schwimmbeckenbeleuchtung und Pumpe

Alles wird auch steuerbar sein via GPRS Modul