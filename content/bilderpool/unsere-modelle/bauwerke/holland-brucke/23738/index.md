---
layout: "image"
title: "Klappbrücke"
date: "2009-04-15T23:04:05"
picture: "hollandbruecke8.jpg"
weight: "8"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- details/23738
imported:
- "2019"
_4images_image_id: "23738"
_4images_cat_id: "1622"
_4images_user_id: "936"
_4images_image_date: "2009-04-15T23:04:05"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23738 -->
Motorantrieb der Brücke mit bistabilem Relais. Der zweite Schalter ist noch nicht in Benutzung.