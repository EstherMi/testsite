---
layout: "overview"
title: "Holland-Brücke"
date: 2019-12-17T19:48:52+01:00
legacy_id:
- categories/1622
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1622 --> 
Nach fast 31 Jahren habe ich meine Fischertechnik-Bauteile wieder aus der Versenkung geholt :-) und mir auch einiges neues (gebrauchtes) angeschafft. Das saß ich also als alter Herr und wußte nicht so recht was es dann als erstes werden soll. Nachdem ich im ft-Museum die Verpackung der Hobbywelt Box gesehen habe, war mir klar das ich mich an einer Klappbrücke versuche.  Ich hoffe, die Bilder gefallen Euch, ich hatte Spaß am bauen des Modells, und auch so einige Momente wo ich echt grübeln mußte. Aber Üung macht den Meister. Kann also nur besser werden ..... 