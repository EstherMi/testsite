---
layout: "image"
title: "Klappbrücke"
date: "2009-04-15T23:04:04"
picture: "hollandbruecke3.jpg"
weight: "3"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- details/23733
imported:
- "2019"
_4images_image_id: "23733"
_4images_cat_id: "1622"
_4images_user_id: "936"
_4images_image_date: "2009-04-15T23:04:04"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23733 -->
Oberteil der Brücke.