---
layout: "overview"
title: "Bauwerke"
date: 2019-12-17T19:48:14+01:00
legacy_id:
- categories/656
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=656 --> 
Gebäude, Brücken, Türme;  kurz: alles was nicht schwimmt, fährt oder fliegt. Kräne befinden sich aber unter "Baumaschinen".