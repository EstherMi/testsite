---
layout: "image"
title: "Kaiser Wilhelm Brug"
date: "2012-05-06T22:24:00"
picture: "1.jpg"
weight: "2"
konstrukteure: 
- "Jack Steeghs"
fotografen:
- "Jack Steeghs"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "JaSpiel"
license: "unknown"
legacy_id:
- details/34900
imported:
- "2019"
_4images_image_id: "34900"
_4images_cat_id: "2583"
_4images_user_id: "1295"
_4images_image_date: "2012-05-06T22:24:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34900 -->
Geinspireerd door de brug van de heer Pettera uit Stuttgart heb ik deze brug gebouwd (zie: Nederlands fischertechnik clubblad 2008/3). De slagbomen worden pneumatisch bewogen. Het geheel wordt bestuurd via 7 etec-modules. Het draaien van de brug wordt via minitasters in gang gezet (opgeroepen).