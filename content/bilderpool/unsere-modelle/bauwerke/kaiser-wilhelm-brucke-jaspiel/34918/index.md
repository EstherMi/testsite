---
layout: "image"
title: "Kaiser Wilhelm Brug"
date: "2012-05-06T22:24:12"
picture: "17.jpg"
weight: "20"
konstrukteure: 
- "Jack Steeghs"
fotografen:
- "Jack Steeghs"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "JaSpiel"
license: "unknown"
legacy_id:
- details/34918
imported:
- "2019"
_4images_image_id: "34918"
_4images_cat_id: "2583"
_4images_user_id: "1295"
_4images_image_date: "2012-05-06T22:24:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34918 -->
