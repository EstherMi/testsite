---
layout: "image"
title: "Längsansicht"
date: "2008-09-08T17:45:22"
picture: "bruecke1_2.jpg"
weight: "2"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/15210
imported:
- "2019"
_4images_image_id: "15210"
_4images_cat_id: "1227"
_4images_user_id: "130"
_4images_image_date: "2008-09-08T17:45:22"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15210 -->
Hier mal eine Längsansicht der Brücke [b]ohne[/b] Fahrbahn. Ober der Bogen ist neu gemacht. Es fehlen jetzt noch einige senkrechte Streben nach oben und der obere Abschluss. 

Bitte nicht auf das Chaos unter der Brücke achten!!