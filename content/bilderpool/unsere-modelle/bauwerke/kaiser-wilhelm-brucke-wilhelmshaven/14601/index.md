---
layout: "image"
title: "Ansicht von unten"
date: "2008-05-30T22:39:36"
picture: "bruecke02_2.jpg"
weight: "8"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/14601
imported:
- "2019"
_4images_image_id: "14601"
_4images_cat_id: "1227"
_4images_user_id: "130"
_4images_image_date: "2008-05-30T22:39:36"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14601 -->
Hier sieht man mal die Unterseite meines Modells von der KW Brücke.