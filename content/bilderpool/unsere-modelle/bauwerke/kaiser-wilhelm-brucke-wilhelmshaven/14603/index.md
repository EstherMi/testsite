---
layout: "image"
title: "Blick auf die 'Durchfahrt'"
date: "2008-05-30T22:39:36"
picture: "bruecke04_2.jpg"
weight: "10"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/14603
imported:
- "2019"
_4images_image_id: "14603"
_4images_cat_id: "1227"
_4images_user_id: "130"
_4images_image_date: "2008-05-30T22:39:36"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14603 -->
Eine Gesamtansicht von der Seite wo man über die Brücke geht bzw. fährt.