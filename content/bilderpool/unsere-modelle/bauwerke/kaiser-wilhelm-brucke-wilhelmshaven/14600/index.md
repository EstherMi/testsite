---
layout: "image"
title: "Ein Laufwerk"
date: "2008-05-30T22:39:35"
picture: "bruecke01_2.jpg"
weight: "7"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/14600
imported:
- "2019"
_4images_image_id: "14600"
_4images_cat_id: "1227"
_4images_user_id: "130"
_4images_image_date: "2008-05-30T22:39:35"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14600 -->
Hier mal ein Blick auf eines von vier Räderpaaren.