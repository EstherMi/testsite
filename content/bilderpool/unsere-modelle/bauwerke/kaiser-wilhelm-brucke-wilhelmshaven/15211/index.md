---
layout: "image"
title: "Seitenansicht"
date: "2008-09-08T17:45:22"
picture: "bruecke2_2.jpg"
weight: "3"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/15211
imported:
- "2019"
_4images_image_id: "15211"
_4images_cat_id: "1227"
_4images_user_id: "130"
_4images_image_date: "2008-09-08T17:45:22"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15211 -->
Eine Seitenansicht. Hier kann man erkennen das die Seite die über dem Waser ist etwas höher geht als die, die zum Land geht. 

Bis zur Convention ist noch ne Menge zu tun.

Bitte nicht auf das Chaos unter der Brücke achten!