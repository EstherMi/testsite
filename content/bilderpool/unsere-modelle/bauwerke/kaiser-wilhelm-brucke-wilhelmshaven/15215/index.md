---
layout: "image"
title: "Schalttafel der K.W. Brücke"
date: "2008-09-09T17:28:09"
picture: "bruecke1_3.jpg"
weight: "1"
konstrukteure: 
- "Maschinenfabrik - Augsburg - Nürnberg AG (MAN)"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/15215
imported:
- "2019"
_4images_image_id: "15215"
_4images_cat_id: "1227"
_4images_user_id: "130"
_4images_image_date: "2008-09-09T17:28:09"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15215 -->
Hier ein Bild der Schalttafel. Man sieht doch die Unterteilung in beide Brückenhälften und für die Schranken am Ende der Brücke.
Ich durfte leider nicht länger auf der Brücke bleiben weil sie geöffnet wurde und man da nicht auf ihr drauf sein darf. Ich verschaff mir noch einen Besichtigungstermin, dann manche ich noch mehr Bilder. Hoffe das ich auch ins Motorenhaus darf....