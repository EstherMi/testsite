---
layout: "image"
title: "Kaiser-Wilhelm-Brücke"
date: "2008-01-29T16:30:45"
picture: "kwbruecke2.jpg"
weight: "36"
konstrukteure: 
- "u. a. Oberbaurat Ernst Troschel, MAN Nürnberg"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/13456
imported:
- "2019"
_4images_image_id: "13456"
_4images_cat_id: "1227"
_4images_user_id: "130"
_4images_image_date: "2008-01-29T16:30:45"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13456 -->
Blick von unten auf einen Pfeiler.