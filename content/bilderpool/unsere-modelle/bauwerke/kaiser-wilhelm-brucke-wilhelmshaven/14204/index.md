---
layout: "image"
title: "Modell der Kaiser-Wilhelm-Brücke"
date: "2008-04-09T21:09:34"
picture: "bruecke01.jpg"
weight: "18"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/14204
imported:
- "2019"
_4images_image_id: "14204"
_4images_cat_id: "1227"
_4images_user_id: "130"
_4images_image_date: "2008-04-09T21:09:34"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14204 -->
Hier mal die Gesammtansicht. Die aufrechten Träger werden noch verstrebt.