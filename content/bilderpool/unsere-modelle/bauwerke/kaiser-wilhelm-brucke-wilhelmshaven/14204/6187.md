---
layout: "comment"
hidden: true
title: "6187"
date: "2008-04-11T21:51:39"
uploadBy:
- "stephan"
license: "unknown"
imported:
- "2019"
---
Also Mittelteile gibts da ja eigentlich keine. Es sind ja zwei gleiche Teile die sich beide unabhängig voneinander drehen lassen. 

Ein Teil von meinem Modell wird so ca. 1,60m lang, also liegst du mit 3m ganz nah dran. Hoffe jedenfalls das ich das mit der Länge so hinbekomme wie ich es mir vorgestellt habe. Viel länger darf sie nicht werden da ich sie sonst nicht ins Auto bekomme und ich wollte sie eigentlich zu unserer Ausstellung im August und zur Convention transportieren. 

Wenn du wieder mal hier oben Urlaub machst klingel kurz an dann könnte man sich ja treffen.