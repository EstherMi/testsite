---
layout: "image"
title: "Modell der Kaiser-Wilhelm-Brücke"
date: "2008-04-09T21:09:35"
picture: "bruecke02.jpg"
weight: "19"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/14205
imported:
- "2019"
_4images_image_id: "14205"
_4images_cat_id: "1227"
_4images_user_id: "130"
_4images_image_date: "2008-04-09T21:09:35"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14205 -->
Ansicht von Rechts.