---
layout: "image"
title: "Bauplan von der KW Brücke"
date: "2008-02-11T20:34:22"
picture: "bruecke2.jpg"
weight: "29"
konstrukteure: 
- "u.a. Oberbaurat Ernst Troschel; Firma MAN Nürnberg"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/13638
imported:
- "2019"
_4images_image_id: "13638"
_4images_cat_id: "1227"
_4images_user_id: "130"
_4images_image_date: "2008-02-11T20:34:22"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13638 -->
Mit freundlicher Genehmigung von den Stadtwerken Wilhelmshavens!