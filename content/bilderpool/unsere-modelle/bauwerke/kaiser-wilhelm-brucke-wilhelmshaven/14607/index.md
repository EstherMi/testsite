---
layout: "image"
title: "Eine senkrechte Stütze"
date: "2008-05-30T22:39:36"
picture: "bruecke08_2.jpg"
weight: "14"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/14607
imported:
- "2019"
_4images_image_id: "14607"
_4images_cat_id: "1227"
_4images_user_id: "130"
_4images_image_date: "2008-05-30T22:39:36"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14607 -->
Eine von vielen senkrechten Stützen. Auf den schwarzen Achshalter kommt dann demnächst die Fahrbahn zu liegen aber erst nachdem noch ein dicker Querträger eingebaut ist. (siehe Bauzeichnung)