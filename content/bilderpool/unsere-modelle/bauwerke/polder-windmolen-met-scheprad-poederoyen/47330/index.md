---
layout: "image"
title: "Achtkantige Poldermolen constructie-details"
date: "2018-03-09T22:20:59"
picture: "polderwindmolen22.jpg"
weight: "22"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/47330
imported:
- "2019"
_4images_image_id: "47330"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:20:59"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47330 -->
Interessante achtergrondinfo :
http://www.wilcosproductions.nl/poldermill/index.php/opbouw-poldermolen/houten-achtkant