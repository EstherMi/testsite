---
layout: "image"
title: "Polder- Windmolen met scheprad    Poederoyen NL"
date: "2018-03-09T22:20:59"
picture: "polderwindmolen63.jpg"
weight: "63"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/47371
imported:
- "2019"
_4images_image_id: "47371"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:20:59"
_4images_image_order: "63"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47371 -->
Schaal: 1:25,
Modelhoogte:  1,30m 
Modelbreedte: 1,15m 

In de Floris-aflevering "De Vrijbrief" werden de soldaten van Maarten van Rossum met Pestmaskers op molenwieken vast gebonden. Dit met stuntmannen + Loyds-verzekering voor een halfuur filmdraaien. Terug te zien op Youtube na ca. 28 min. : 
https://www.youtube.com/watch?v=R8unaDyOV78&list=FLvBlHQzqD-ISw8MaTccrfOQ&index=1&t=1680s 

In fischertechnik via de link : 
Youtube-link : 

https://www.youtube.com/watch?v=UliZqs6p7HM&t=0s