---
layout: "image"
title: "(Ver-) Nieuwbouw Molens"
date: "2018-03-09T22:20:59"
picture: "polderwindmolen79.jpg"
weight: "79"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/47387
imported:
- "2019"
_4images_image_id: "47387"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:20:59"
_4images_image_order: "79"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47387 -->
Interessante Links mite viel Details : 

https://www.verbij.nl/molens/ 
https://www.verbij.nl/molens/3d-ontwerp

http://www.wilcosproductions.nl/poldermill/index.php/opbouw-poldermolen/houten-achtkant

http://www.molenbiotoop.nl/content.php?page=2.2.1