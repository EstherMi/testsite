---
layout: "image"
title: "Uitstroomzijde met wachtdeur (-bovenstrooms)"
date: "2018-03-09T22:20:59"
picture: "polderwindmolen34.jpg"
weight: "34"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/47342
imported:
- "2019"
_4images_image_id: "47342"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:20:59"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47342 -->
Funktionerend te zien met wachtdeur:

https://youtu.be/AbBXY2oiQCk?list=PLWSSuks9CypF4Ujs3XgsttKqm_jhqS1Cq&t=121