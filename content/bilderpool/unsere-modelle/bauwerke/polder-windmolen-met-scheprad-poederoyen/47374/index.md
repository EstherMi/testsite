---
layout: "image"
title: "Instroomzijde Polder- Windmolen met scheprad    Poederoyen NL"
date: "2018-03-09T22:20:59"
picture: "polderwindmolen66.jpg"
weight: "66"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/47374
imported:
- "2019"
_4images_image_id: "47374"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:20:59"
_4images_image_order: "66"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47374 -->
