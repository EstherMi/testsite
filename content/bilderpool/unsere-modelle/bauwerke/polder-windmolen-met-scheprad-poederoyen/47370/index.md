---
layout: "image"
title: "Staart + lange Schoor + Vangstok"
date: "2018-03-09T22:20:59"
picture: "polderwindmolen62.jpg"
weight: "62"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/47370
imported:
- "2019"
_4images_image_id: "47370"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:20:59"
_4images_image_order: "62"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47370 -->
Kruiwerk kapconstructie in fischertechnik dmv kogels