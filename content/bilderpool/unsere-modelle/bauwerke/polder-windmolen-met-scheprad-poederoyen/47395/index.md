---
layout: "image"
title: "Poldermolens Kinderdijk  UNESCO- Welt Kultur Erbe"
date: "2018-03-09T22:20:59"
picture: "polderwindmolen87.jpg"
weight: "87"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/47395
imported:
- "2019"
_4images_image_id: "47395"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:20:59"
_4images_image_order: "87"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47395 -->
Interessante links met achtergronden :

https://www.kinderdijk.nl/


Instagram account van Peter Paul Klapwijk 
https://www.instagram.com/ppkhm/ 
#lifeatawindmill 


http://www.wilcosproductions.nl/poldermill/index.php/molens-algemeen

http://www.wilcosproductions.nl/poldermill/index.php/kinderdijk