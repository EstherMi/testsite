---
layout: "image"
title: "Achtkantige Polder Windmolen met scheprad te Zuilichem"
date: "2018-03-09T22:20:59"
picture: "polderwindmolen86.jpg"
weight: "86"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/47394
imported:
- "2019"
_4images_image_id: "47394"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:20:59"
_4images_image_order: "86"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47394 -->
