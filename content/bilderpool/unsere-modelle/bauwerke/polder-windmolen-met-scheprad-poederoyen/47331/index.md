---
layout: "image"
title: "Molentaal  + Wiekstanden"
date: "2018-03-09T22:20:59"
picture: "polderwindmolen23.jpg"
weight: "23"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/47331
imported:
- "2019"
_4images_image_id: "47331"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:20:59"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47331 -->
Interessante achtergrondinfo :
http://www.wilcosproductions.nl/poldermill/index.php/molens-algemeen