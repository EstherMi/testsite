---
layout: "image"
title: "Paal-kesp-fundering voor het metselwerk (-onder maaiveld zichtbaar gemaakt)"
date: "2018-03-09T22:20:59"
picture: "polderwindmolen65.jpg"
weight: "65"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/47373
imported:
- "2019"
_4images_image_id: "47373"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:20:59"
_4images_image_order: "65"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47373 -->
