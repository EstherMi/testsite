---
layout: "image"
title: "Achtkantige Poldermolen constructie"
date: "2018-03-09T22:20:59"
picture: "polderwindmolen25.jpg"
weight: "25"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/47333
imported:
- "2019"
_4images_image_id: "47333"
_4images_cat_id: "3501"
_4images_user_id: "22"
_4images_image_date: "2018-03-09T22:20:59"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47333 -->
Interessante achtergrondinfo :
http://www.wilcosproductions.nl/poldermill/index.php/opbouw-poldermolen/houten-achtkant