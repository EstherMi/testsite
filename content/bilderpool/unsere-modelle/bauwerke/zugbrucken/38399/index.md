---
layout: "image"
title: "automatische Zugbrücke - weitere Ansicht der Steuerelektronik"
date: "2014-03-01T15:55:19"
picture: "2050.jpg"
weight: "14"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Relaisbaustein", "Relais", "RBII"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/38399
imported:
- "2019"
_4images_image_id: "38399"
_4images_cat_id: "2856"
_4images_user_id: "579"
_4images_image_date: "2014-03-01T15:55:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38399 -->
Hier sieht man das Relais besser