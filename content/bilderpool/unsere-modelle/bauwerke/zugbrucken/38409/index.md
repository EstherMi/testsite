---
layout: "image"
title: "Umlenkspiegel"
date: "2014-03-02T18:43:51"
picture: "S1040128.jpg"
weight: "1"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Hohlspiegel", "Umlenkspiegel", "Lichtschranke"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/38409
imported:
- "2019"
_4images_image_id: "38409"
_4images_cat_id: "2856"
_4images_user_id: "579"
_4images_image_date: "2014-03-02T18:43:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38409 -->
Hier sind die beiden Umlenkspiegel für die Lichtschranke auf Schiffsebene zu sehen. Rechts der original FT Hohlspiegel, links ein Eigenbau mit Alufolie.