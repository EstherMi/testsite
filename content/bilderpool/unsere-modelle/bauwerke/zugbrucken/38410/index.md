---
layout: "image"
title: "Selbst gebaute Steuerelektronik: Mikrocontroller-Board mit ATTiny26"
date: "2014-03-02T18:43:51"
picture: "S1040133.jpg"
weight: "2"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["ATTiny26", "Atmel", "Mikrocontroller", "Microcontroller", "PWM", "MOSFET", "Pulsgenerierung", "Puls"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/38410
imported:
- "2019"
_4images_image_id: "38410"
_4images_cat_id: "2856"
_4images_user_id: "579"
_4images_image_date: "2014-03-02T18:43:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38410 -->
Hier noch ein detaillierter Blick auf die selbst gebaute Steuerelektronik, ein Mikrocontroller-Board mit ATTiny26. Funktionen:
- Fernbedienbar über Infrarotempfänger
- pwm-gesteuerter Leistungs-MOSFET zur Motoransteuerung
- MOSFET zur Relaisumschaltung für die Fahrtrichtung des Motors
- Pulsgenerierung für zwei Infrarot-LEDs, die mit 50µs-Pulsen auf 1A Spitzenstrom gebracht werden
- Abfrage der Eingänge (Fototransistoren der Lichtschranke über den FT-Schwellwertschalter)
- analoge Messung der Potistellung (Brückenposition)
- direktes Ansteuern / Treiben der LEDs für die Verkehrsampeln