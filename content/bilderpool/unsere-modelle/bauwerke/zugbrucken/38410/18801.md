---
layout: "comment"
hidden: true
title: "18801"
date: "2014-03-02T19:12:42"
uploadBy:
- "Triceratops"
license: "unknown"
imported:
- "2019"
---
Hallo

Bislang habe ich mich nicht getraut, unter einem SMD-Widerstand hindurch auch noch eine (nicht isolierte) Leitung zu führen. Ist sowas denn tatsächlich üblich?

Ich selbst habe mir den h4-FT in DIL-Ausführung noch einmal verkleinert, indem ich jetzt 3 SMD-Widerstände und 1 SMD-Diode verbaut habe. Funktioniert einwandfrei.

Gruß, Thomas