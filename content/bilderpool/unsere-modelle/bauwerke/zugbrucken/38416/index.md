---
layout: "image"
title: "verriegelte Brückenteile"
date: "2014-03-02T18:43:51"
picture: "2061.jpg"
weight: "8"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/38416
imported:
- "2019"
_4images_image_id: "38416"
_4images_cat_id: "2856"
_4images_user_id: "579"
_4images_image_date: "2014-03-02T18:43:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38416 -->
vier Magnete und eine mechanische Verriegelung halten die Brückenteile zusammen