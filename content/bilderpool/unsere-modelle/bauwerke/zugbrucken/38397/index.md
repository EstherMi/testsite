---
layout: "image"
title: "Ampeln für Schiffs- und Autoverkehr, Potentiometer zur Lageerkennung der Brücke"
date: "2014-03-01T15:55:18"
picture: "2046.jpg"
weight: "12"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/38397
imported:
- "2019"
_4images_image_id: "38397"
_4images_cat_id: "2856"
_4images_user_id: "579"
_4images_image_date: "2014-03-01T15:55:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38397 -->
oben die Ampel für Autoverkehr von der Seite
mittig die Ampel für Schiffsverkehr
rechts unten das Potentiometer zur Lageerkennung der Brücke