---
layout: "image"
title: "Tunnel-Boor-Machine(TBM) met Earth Pressure Balance (EPB) -Shield   Peter Damen Poederoyen NL"
date: "2017-08-22T19:52:01"
picture: "tbm88.jpg"
weight: "88"
konstrukteure: 
- "Peter Damen Poederoyen NL"
fotografen:
- "Peter Damen Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/46211
imported:
- "2019"
_4images_image_id: "46211"
_4images_cat_id: "3427"
_4images_user_id: "22"
_4images_image_date: "2017-08-22T19:52:01"
_4images_image_order: "88"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46211 -->
Ubersicht -vorne

Tunnel-Boor-Machine(TBM) met Earth Pressure Balance (EPB) -Shield   
In die Niederlanden wird diese Technik auch manchmal verwendet wegen die weichen, bindigen Ton-Böden.
In weichen, bindigen Böden werden bevorzugt Vortriebsmaschinen mit Erddruckstützung eingesetzt.
Bei den sogenannten Erddruckschilden (engl. Earth Pressure Balance Shield, kurz EPB) dient ein Erdbrei aus
abgebautem Material als plastisches Stützmedium. Dies ermöglicht den nötigen Ausgleich der Druckverhältnisse 
an der Ortsbrust, verhindert ein unkontrolliertes Eindringen des Bodens in die Maschine und schafft die Voraussetzung
für einen schnellen und weitestgehend setzungsfreien Vortrieb.

Interessante + wichtige weblinks :    
https://www.herrenknecht.com/de/produkte/kernprodukte/tunnelling/epb-schild.html

https://www.youtube.com/watch?v=kyXQfxsrymg&t=3s&index=1&list=FLvBlHQzqD-ISw8MaTccrfOQ

https://youtu.be/6EKA2yZ_aic