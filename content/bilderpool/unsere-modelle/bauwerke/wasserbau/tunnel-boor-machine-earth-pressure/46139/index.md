---
layout: "image"
title: "Tunnel-Boor-Machine(TBM) met Earth Pressure Balance (EPB) -Shield    Herrenknecht"
date: "2017-08-22T19:51:30"
picture: "tbm16.jpg"
weight: "16"
konstrukteure: 
- "Peter Damen Poederoyen NL"
fotografen:
- "Peter Damen Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/46139
imported:
- "2019"
_4images_image_id: "46139"
_4images_cat_id: "3427"
_4images_user_id: "22"
_4images_image_date: "2017-08-22T19:51:30"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46139 -->
Stabilität durch Stützdruck
Das besondere Merkmal von Erddruckschilden ist die direkte Nutzung des abgebauten Bodens als Stützmedium. Dieses Verfahren wird insbesondere bei kohäsiven Böden mit hohen Ton- oder Schluffanteilen und geringer Wasserdurchlässigkeit eingesetzt. Ein werkzeugbestücktes, rotierendes Schneidrad wird an die Ortsbrust gedrückt und löst den anstehenden Boden. Über Öffnungen gelangt dieser in die Abbaukammer, wo er sich mit dem bereits vorhandenen Erdbrei vermischt. Mischflügel an Schneidrad und Druckwand kneten die Masse zur gewünschten Konsistenz. Der Druck der Vortriebspressen wird über die Druckwand auf den Erdbrei übertragen. Wenn der äußere Erd- und Wasserdruck dem Druck des stützenden Erdbreis entspricht, ist der notwendige Gleichgewichtszustand erreicht.

Förderrate und Vortriebsgeschwindigkeit regeln Druckverhältnisse
Eine Förderschnecke transportiert das abgebaute Material vom Boden der Abbaukammer auf ein Förderband. Dabei sichert das Zusammenspiel der Förderrate der Schnecke und der Vortriebs­geschwindigkeit die präzise Steuerung des Stützdrucks des Erdbreis. Mittels Erddrucksensoren in der Abbaukammer wird der Gleichgewichtszustand kontinuierlich überwacht. Somit können alle Vortriebsparameter auch bei wechselnden geologischen Bedingungen vom Maschinenfahrer optimal aufeinander abgestimmt werden. Das ermöglicht hohe Vortriebsgeschwindigkeiten und minimiert die Gefahr von Hebungen oder Setzungen an der Oberfläche.

Weblink :
https://www.youtube.com/watch?v=kyXQfxsrymg&index=1&list=FLvBlHQzqD-ISw8MaTccrfOQ&t=9s