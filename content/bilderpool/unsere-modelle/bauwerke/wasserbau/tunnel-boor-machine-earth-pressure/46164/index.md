---
layout: "image"
title: "Ubersicht Tunnel-Boor-Machine(TBM) met Earth Pressure Balance (EPB) -Shield"
date: "2017-08-22T19:52:01"
picture: "tbm41.jpg"
weight: "41"
konstrukteure: 
- "Peter Damen Poederoyen NL"
fotografen:
- "Peter Damen Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/46164
imported:
- "2019"
_4images_image_id: "46164"
_4images_cat_id: "3427"
_4images_user_id: "22"
_4images_image_date: "2017-08-22T19:52:01"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46164 -->
Interessante Weblink :

https://www.youtube.com/watch?v=kyXQfxsrymg&t=3s&index=1&list=FLvBlHQzqD-ISw8MaTccrfOQ