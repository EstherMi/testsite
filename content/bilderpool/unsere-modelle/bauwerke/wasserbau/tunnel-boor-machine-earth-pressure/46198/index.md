---
layout: "image"
title: "Erektor Details + Tubing (segment)"
date: "2017-08-22T19:52:01"
picture: "tbm75.jpg"
weight: "75"
konstrukteure: 
- "Peter Damen Poederoyen NL"
fotografen:
- "Peter Damen Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/46198
imported:
- "2019"
_4images_image_id: "46198"
_4images_cat_id: "3427"
_4images_user_id: "22"
_4images_image_date: "2017-08-22T19:52:01"
_4images_image_order: "75"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46198 -->
