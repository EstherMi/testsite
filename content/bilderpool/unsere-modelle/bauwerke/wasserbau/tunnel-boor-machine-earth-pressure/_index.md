---
layout: "overview"
title: "Tunnel-Boor-Machine + Earth Pressure Balance Shield"
date: 2019-12-17T19:49:40+01:00
legacy_id:
- categories/3427
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3427 --> 
In weichen, bindigen Böden werden bevorzugt Vortriebsmaschinen mit Erddruckstützung eingesetzt. Bei den sogenannten Erddruckschilden (engl. Earth Pressure Balance Shield, kurz EPB) dient ein Erdbrei aus abgebautem Material als plastisches Stützmedium. Dies ermöglicht den nötigen Ausgleich der Druckverhältnisse an der Ortsbrust, verhindert ein unkontrolliertes Eindringen des Bodens in die Maschine und schafft die Voraussetzung für einen schnellen und weitestgehend setzungsfreien Vortrieb.