---
layout: "image"
title: "Erektor Details + Tubing (segment)"
date: "2017-08-22T19:52:01"
picture: "tbm76.jpg"
weight: "76"
konstrukteure: 
- "Peter Damen Poederoyen NL"
fotografen:
- "Peter Damen Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/46199
imported:
- "2019"
_4images_image_id: "46199"
_4images_cat_id: "3427"
_4images_user_id: "22"
_4images_image_date: "2017-08-22T19:52:01"
_4images_image_order: "76"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46199 -->
