---
layout: "image"
title: "Segmentwehr"
date: "2008-07-19T12:03:50"
picture: "Segmentwehr_009.jpg"
weight: "78"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/14917
imported:
- "2019"
_4images_image_id: "14917"
_4images_cat_id: "1361"
_4images_user_id: "22"
_4images_image_date: "2008-07-19T12:03:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14917 -->
2 Segmentwehre in meinem Garten-Fluss Poederoyen NL