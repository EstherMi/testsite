---
layout: "image"
title: "Klepstuw + Maxon-Encoder-Motor"
date: "2009-03-15T13:46:54"
picture: "KlepstuwMaxon-Encoder-Motor_002.jpg"
weight: "24"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/23455
imported:
- "2019"
_4images_image_id: "23455"
_4images_cat_id: "1361"
_4images_user_id: "22"
_4images_image_date: "2009-03-15T13:46:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23455 -->
