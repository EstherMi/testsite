---
layout: "image"
title: "Klepstuw + Maxon-Encoder-Motor"
date: "2009-03-15T13:46:54"
picture: "KlepstuwMaxon-Encoder-Motor_005.jpg"
weight: "29"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/23460
imported:
- "2019"
_4images_image_id: "23460"
_4images_cat_id: "1361"
_4images_user_id: "22"
_4images_image_date: "2009-03-15T13:46:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23460 -->
Technical Data Pin Allocation 
Supply voltage VCC 3.8 - 24 V 

Pin: 
1 Motor+  (rot)
2 VCC   (= +5V mit Conrad 5V Spannungsregler)
3 Channel A   (an A1 Robo-Interface )
4 Channel B 
5 GND   (= - Conrad 5V Spannungsregler)
6 Motor- 

(Wenn man Channel A an A1, und Channel B an A1 der Robo-Interface verbindet, ist es mit einem RoboPro-Programm möglich die Drehrichting fest u stellen)
Pin type DIN 41651 (Typ 3M 89110-0101 HA) flat band cable AWG 28 
Output signal VCC = 5 VDC TTL compatible 
Phase shift 90°e ± 45°e 
Power input at VCC 5 VDC max.8 mA 
Inertia of the magnetic disc 0.03 gcm2 
----------------------------------------------------------------------------------------------------------------------------------------------------
 
Mit dem heutigen Robo Interface funktionieren die Encoder-Getriebemotoren (schon) gut mit "Channel A" oder "Channel B" als analogen A1 Spannungs-eingang und ein Impulszähler. 

Gruss, 

Peter Damen 
Poederoyen NL