---
layout: "image"
title: "Niveau- en spoelregeling binnen 5 peilvakken middels Schuif- , Segment- en Klep- Stuwen"
date: "2009-04-05T22:44:40"
picture: "Schuif-_Segment-_en_Klepstuwen_-Fischertechnik_008.jpg"
weight: "6"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/23609
imported:
- "2019"
_4images_image_id: "23609"
_4images_cat_id: "1361"
_4images_user_id: "22"
_4images_image_date: "2009-04-05T22:44:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23609 -->
