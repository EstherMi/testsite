---
layout: "image"
title: "Klepstuw + Maxon-Encoder-Motor"
date: "2009-03-15T13:46:55"
picture: "KlepstuwMaxon-Encoder-Motor_006.jpg"
weight: "30"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/23461
imported:
- "2019"
_4images_image_id: "23461"
_4images_cat_id: "1361"
_4images_user_id: "22"
_4images_image_date: "2009-03-15T13:46:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23461 -->
