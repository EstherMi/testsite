---
layout: "image"
title: "Eco-Fischertechnik :  Igel beim FT-Wehren........"
date: "2011-09-18T14:48:36"
picture: "FT-StuwenRdam_015.jpg"
weight: "2"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter PoederoyenNL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/31812
imported:
- "2019"
_4images_image_id: "31812"
_4images_cat_id: "1941"
_4images_user_id: "22"
_4images_image_date: "2011-09-18T14:48:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31812 -->
Eco-Fischertechnik :  Igel beim FT-Wehren........