---
layout: "image"
title: "Segment- en Schuifstuwen, naast Robo-Pro-niveau-sturing nu ook met handbediening."
date: "2010-04-25T19:48:52"
picture: "SchuifSegment-Stuwen-AutomHanbediening_009.jpg"
weight: "23"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/26986
imported:
- "2019"
_4images_image_id: "26986"
_4images_cat_id: "1941"
_4images_user_id: "22"
_4images_image_date: "2010-04-25T19:48:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26986 -->
Handbedienung macht die Kinder Antonie & Annemieke viel mehr Spass !