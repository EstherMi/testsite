---
layout: "comment"
hidden: true
title: "6733"
date: "2008-07-19T14:00:17"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Hallo,

Der Wehre vor hat ein FT-S-Motor.
Antrieb uber: 2 FT-Schnecken m=1,5 , Rast-Ritzel Z10 (5mm ausbohren) mit M4-Hulze (Baumarkt, 5mm aussen).

Der Wehre hinter hat eine 12V Modelcraft Getriebemotor (Conradnr. 234350): 1:231, 26rpm.

Die Segmentwehre möchte ich noch mit ein Robopro-Programm oder ein E-TEC Modul steuern.

Ich uberwiege die Wasserstände mit Ultraschall-sensoren (128597) zu messen.

Hat jemand gute erfahrungen mit Wasserdruck-Sensoren ?..... Messbereich 0-10 cm Wasserdruck (0-1 kPa).

[ 100 kPa = 10 mWk = 1 Bar = 1 atm ]

Gruss,

Peter Damen
Poederoyen NL