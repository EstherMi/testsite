---
layout: "image"
title: "Neubau Schiebe- und Segmentwehre für dem Sommer"
date: "2009-02-01T19:35:39"
picture: "SegmentSchuifstuwen-2009_007.jpg"
weight: "41"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/17248
imported:
- "2019"
_4images_image_id: "17248"
_4images_cat_id: "1361"
_4images_user_id: "22"
_4images_image_date: "2009-02-01T19:35:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17248 -->
Neubau Schiebe- und Segmentwehre für dem Sommer