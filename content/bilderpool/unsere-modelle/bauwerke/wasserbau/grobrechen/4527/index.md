---
layout: "image"
title: "krooshekreiniger"
date: "2005-07-30T14:12:03"
picture: "Fischertechnik-krooshekreiniger_002.jpg"
weight: "10"
konstrukteure: 
- "&nbsp;"
fotografen:
- "b"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/4527
imported:
- "2019"
_4images_image_id: "4527"
_4images_cat_id: "358"
_4images_user_id: "22"
_4images_image_date: "2005-07-30T14:12:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4527 -->
