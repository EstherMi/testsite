---
layout: "image"
title: "krooshekreiniger"
date: "2005-08-12T13:08:58"
picture: "Fischertechnik-Krooshekreiniger_in_bedrijf_004.jpg"
weight: "4"
konstrukteure: 
- "Peter Damen (Poederoyen Holland)"
fotografen:
- "Peter Damen (Poederoyen Holland)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/4542
imported:
- "2019"
_4images_image_id: "4542"
_4images_cat_id: "358"
_4images_user_id: "22"
_4images_image_date: "2005-08-12T13:08:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4542 -->
