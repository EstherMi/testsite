---
layout: "overview"
title: "Windmühle"
date: 2019-12-17T19:49:36+01:00
legacy_id:
- categories/2980
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2980 --> 
Diente früher zur Landgewinnung in den Niederlanden.
Eigentlich ist es eine Pumpe, die mit Hilfe von Wind angetrieben wurde.