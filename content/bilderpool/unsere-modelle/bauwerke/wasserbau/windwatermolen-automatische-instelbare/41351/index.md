---
layout: "image"
title: "Bosman B4 Windwatermolen -overzicht (tijdens aanleg / realisatie ontpoldering Noordwaard)"
date: "2015-07-01T18:05:08"
picture: "windwatermolen07.jpg"
weight: "7"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/41351
imported:
- "2019"
_4images_image_id: "41351"
_4images_cat_id: "3090"
_4images_user_id: "22"
_4images_image_date: "2015-07-01T18:05:08"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41351 -->
- Overzicht (tijdens aanleg / realisatie ontpoldering Noordwaard)