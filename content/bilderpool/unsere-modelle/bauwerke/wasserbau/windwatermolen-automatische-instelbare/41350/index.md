---
layout: "image"
title: "Bosman B4 Windwatermolen -werking mech. automatische instelbare peil-beheersing"
date: "2015-07-01T18:05:08"
picture: "windwatermolen06.jpg"
weight: "6"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/41350
imported:
- "2019"
_4images_image_id: "41350"
_4images_cat_id: "3090"
_4images_user_id: "22"
_4images_image_date: "2015-07-01T18:05:08"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41350 -->
Als het water hoog in de sloot staat, staat de hoofdvaan verticaal en de hulpvaan ligt plat. Daarmee draait de molen vanzelf in de wind. 

Zakt het water, dan verstelt de vlotter de vanen, totdat in de laagste stand de hoofdvaan plat ligt en de hulpvaan verticaal staat. Daarmee draait de molen uit de wind.

De aandrijfas van de wieken naar de centrifugaalpomp en de as-koppeling tbv de windvaan-verstelling zijn zichtbaar