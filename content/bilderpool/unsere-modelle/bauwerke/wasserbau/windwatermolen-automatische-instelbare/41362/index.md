---
layout: "image"
title: "Fischertechnik Windwatermolen met automatische windvaan-verstelling door vlotter"
date: "2015-07-01T18:05:08"
picture: "windwatermolen18.jpg"
weight: "18"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/41362
imported:
- "2019"
_4images_image_id: "41362"
_4images_cat_id: "3090"
_4images_user_id: "22"
_4images_image_date: "2015-07-01T18:05:08"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41362 -->
Instroom-opening van centrifugaal-pomp zonder vuilrooster.

Er drijft een vlotter op het water, die via een stang de stand van beide vanen instelt t.b.v. volautomatische peilbeheersing.  
Van de hoofdvaan, die recht achter de wieken zit en van de hulp- of bijvaan, die opzij staat.