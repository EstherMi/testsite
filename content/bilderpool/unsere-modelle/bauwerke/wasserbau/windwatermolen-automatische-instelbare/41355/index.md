---
layout: "image"
title: "Fischertechnik Windwatermolen met automatische windvaan-verstelling door vlotter"
date: "2015-07-01T18:05:08"
picture: "windwatermolen11.jpg"
weight: "11"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/41355
imported:
- "2019"
_4images_image_id: "41355"
_4images_cat_id: "3090"
_4images_user_id: "22"
_4images_image_date: "2015-07-01T18:05:08"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41355 -->
Het grootste gevaar is vuil, dat in de pomp wordt gezogen of de vlotterbeweging blokkeert. 
Daarom staan er altijd vuilroosters in de sloot, vlak voor de molen.

Achter het vuilrooster is de rode vlotter zichtbaar.

De aandrijfas van de wieken naar de centrifugaalpomp en de as-koppeling tbv de windvaan-verstelling zijn zichtbaar