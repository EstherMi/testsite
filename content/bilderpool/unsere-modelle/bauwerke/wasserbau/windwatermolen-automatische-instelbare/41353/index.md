---
layout: "image"
title: "Geïntegreerde  aandrijving centrifugaalpomp door molenwieken + windvaan-verstelling door vlotter"
date: "2015-07-01T18:05:08"
picture: "windwatermolen09.jpg"
weight: "9"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/41353
imported:
- "2019"
_4images_image_id: "41353"
_4images_cat_id: "3090"
_4images_user_id: "22"
_4images_image_date: "2015-07-01T18:05:08"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41353 -->
De draaiende wieken drijven een centrifugaalpomp aan, die het water uit de sloot maalt. 
Als het water genoeg is gezakt, heeft de vlotter inmiddels de vanen zover gedraaid, dat de wieken uit de wind draaien en de bemaling stopt. 

De aandrijfas van de wieken naar de centrifugaalpomp en de as-koppeling tbv de windvaan-verstelling zijn zichtbaar
