---
layout: "image"
title: "Fischertechnik Windwatermolen met automatische windvaan-verstelling door vlotter"
date: "2015-07-01T18:05:08"
picture: "windwatermolen15.jpg"
weight: "15"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/41359
imported:
- "2019"
_4images_image_id: "41359"
_4images_cat_id: "3090"
_4images_user_id: "22"
_4images_image_date: "2015-07-01T18:05:08"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41359 -->
- Detail dubbele geïntegreerde aandrijving :  Aandrijving centrifugaalpomp door molenwieken + windvaan-verstelling door vlotter