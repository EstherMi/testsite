---
layout: "image"
title: "Bosman B4 Windwatermolens in Noordwaard Biesbosch"
date: "2015-07-01T18:05:08"
picture: "windwatermolen02.jpg"
weight: "2"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/41346
imported:
- "2019"
_4images_image_id: "41346"
_4images_cat_id: "3090"
_4images_user_id: "22"
_4images_image_date: "2015-07-01T18:05:08"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41346 -->
In het kader van het Project Ruimte voor de Rivier wordt de Noordwaard ontpolderd.
Hoofddoel is waterstandsverlaging ivm vergrote rivierafvoeren van Rijn en Maas + klimaat-verandering.
Nieuwe overstroombare kleine polders worden aangelegd in combinatie met ruime hoogwatergeulen.

Voor de ontwatering van de kleine nieuwe polders worden vele windwatermolens geplaatst

Externe link Bosman B4 Windwatermolen :
http://www.bosman-water.nl/nl/producten/pompinstallaties/b4-windwatermolen

De toepassing van windmolens voor de aandrijving van pompen is een economisch aantrekkelijk en milieuvriendelijke oplossing voor watertransport.
Dit geldt in het bijzonder voor gebieden waar elektriciteit niet voorhanden is. De windwatermolen werd in Nederland hoofdzakelijk gebruikt voor het bemalen van polders, maar tegenwoordig ook in natuurgebieden met zogeheten plas-dras situaties. 
De molen werkt al met windsnelheden vanaf 3 m/s  (1 Bft.). 
Door een eenvoudig vlottersysteem is volautomatische peilbeheersing mogelijk. Onder normale omstandigheden (met name vlak open terrein), kan deze molen een gebied van 20-25 ha. verzorgen.

Toepassingen 
.	drainage van cultuurgronden in het algemeen 
.	irrigatie van cultuurgronden zoals bijvoorbeeld in deltagebieden 
.	irrigatie van natuurgebieden 
.	waterniveaubeheersing in visvijvers, zoutpannen en natuurgebieden 
.	het rondpompen van stilstaand water met het oog op verversing
 
Kenmerken B4 Windwatermolen
.	standaard modulaire opbouw 
.	milieuvriendelijk 
.	geen energiekosten 
.	lange levensduur 
.	volautomatisch instelbare peilbeheersing 
.	lichtmetalen wieken en staartbladen 
.	torenhoogte van 4 of 7 meter 
.	toren volbad verzinkt 
.	leverbaar met persbuis 
.	HDPE terugslagklep 
.	robuust prefab staalvezel versterkte betonnen onderbouw
