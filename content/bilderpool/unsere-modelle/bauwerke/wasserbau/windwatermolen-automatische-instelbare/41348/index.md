---
layout: "image"
title: "Bosman B4 Windwatermolen -werking mech. automatische instelbare peil-beheersing"
date: "2015-07-01T18:05:08"
picture: "windwatermolen04.jpg"
weight: "4"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/41348
imported:
- "2019"
_4images_image_id: "41348"
_4images_cat_id: "3090"
_4images_user_id: "22"
_4images_image_date: "2015-07-01T18:05:08"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41348 -->
Er drijft een vlotter op het water, die via een stang de stand van beide vanen instelt. Van de hoofdvaan, die recht achter de wieken zit en van de hulp- of bijvaan, die opzij staat. 
Als het water hoog in de sloot staat, staat de hoofdvaan verticaal en de hulpvaan ligt plat. Daarmee draait de molen vanzelf in de wind. 

Zakt het water, dan verstelt de vlotter de vanen, totdat in de laagste stand de hoofdvaan plat ligt en de hulpvaan verticaal staat. Daarmee draait de molen uit de wind.
De draaiende wieken drijven een centrifugaalpomp aan, die het water uit de sloot maalt. Als het water genoeg is gezakt, heeft de vlotter inmiddels de vanen zover gedraaid, dat de wieken uit de wind draaien en de bemaling stopt. 
De pomp biedt voldoende weerstand om bij storm het op hol slaan van de molen te voorkomen. 

Het grootste gevaar is vuil, dat in de pomp wordt gezogen of de vlotterbeweging blokkeert. 
Daarom staan er altijd vuilroosters in de sloot, vlak voor de molen.
