---
layout: "image"
title: "Tor mit Sperrschieber"
date: "2008-07-15T22:35:42"
picture: "0005_Tor_hochkant_mit_Sperrschieber.jpg"
weight: "3"
konstrukteure: 
- "Laserman"
fotografen:
- "Laserman"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/14888
imported:
- "2019"
_4images_image_id: "14888"
_4images_cat_id: "1359"
_4images_user_id: "724"
_4images_image_date: "2008-07-15T22:35:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14888 -->
Der Schieber dient der Wasserregulierung. Hiermit kann der Wasserstand in der Schleusenkammer gehoben oder gesenkt werden. Es gibt jeweils 2 Schieber auf der Bergseite, und 2 Talseits.