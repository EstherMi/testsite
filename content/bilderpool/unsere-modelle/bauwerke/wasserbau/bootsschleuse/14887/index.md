---
layout: "image"
title: "Schleuse von oben"
date: "2008-07-15T22:35:42"
picture: "0002_Schleuse_von_oben.jpg"
weight: "2"
konstrukteure: 
- "Laserman"
fotografen:
- "Laserman"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/14887
imported:
- "2019"
_4images_image_id: "14887"
_4images_cat_id: "1359"
_4images_user_id: "724"
_4images_image_date: "2008-07-15T22:35:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14887 -->
Hier sieht man die Kabelführung