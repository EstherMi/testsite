---
layout: "image"
title: "Schleu01"
date: "2013-01-13T18:39:54"
picture: "schleusenschiebetor1.jpg"
weight: "1"
konstrukteure: 
- "qincym"
fotografen:
- "qincym"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- details/36482
imported:
- "2019"
_4images_image_id: "36482"
_4images_cat_id: "2709"
_4images_user_id: "895"
_4images_image_date: "2013-01-13T18:39:54"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36482 -->
Angeregt durch eine Anfrage im Forum, habe ich meinen Vorschlag zur Lösung des Problems in einer "Baumaßnahme" umgesetzt.

Hier sieht man das Schiebe-Tor mit Zahnstange und Hubgetriebe, die das Tor bewegen, in leicht geöffneten Zustand. Deutlich ist die Schiene, in welcher das Tor mit zwei Kufen geführt wird, zu sehen. Der kleine Stift am der unteren, linken Ecke des Tores betätigt einen Taster im Inneren des linken Pfostens, der den linken Halt des Tores (Tor geschlossen) signalisiert. Auf dem linken Pfosten ist eine Signallampe montiert.