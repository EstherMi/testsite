---
layout: "image"
title: "Schleu03"
date: "2013-01-13T18:39:54"
picture: "schleusenschiebetor3.jpg"
weight: "3"
konstrukteure: 
- "qincym"
fotografen:
- "qincym"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- details/36484
imported:
- "2019"
_4images_image_id: "36484"
_4images_cat_id: "2709"
_4images_user_id: "895"
_4images_image_date: "2013-01-13T18:39:54"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36484 -->
Hier ist die "Vekleidung" am unteren Ende des linken Pfostens zu erkennen, der den Taster für die "Endstellung" (Tor geschlossen) signalisiert. Die oberen Bausplatten an den beiden Pfosten halten die Pfosten zusammen.