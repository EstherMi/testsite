---
layout: "image"
title: "Schleu04"
date: "2013-01-13T18:39:54"
picture: "schleusenschiebetor4.jpg"
weight: "4"
konstrukteure: 
- "qincym"
fotografen:
- "qincym"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- details/36485
imported:
- "2019"
_4images_image_id: "36485"
_4images_cat_id: "2709"
_4images_user_id: "895"
_4images_image_date: "2013-01-13T18:39:54"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36485 -->
Hier ist schön eine untere Reihe roter Bausteine auf der Rückseite des Schleusen-Tores zu sehen. Ist das Tor geschlossen drückt der höhere Wasserdruck von der anderen Seite (Wasserstand ist dort höher) das Tor gegen den roten "Rahmen" und dichtet somit das Tor ab.