---
layout: "image"
title: "Schleu02"
date: "2013-01-13T18:39:54"
picture: "schleusenschiebetor2.jpg"
weight: "2"
konstrukteure: 
- "qincym"
fotografen:
- "qincym"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- details/36483
imported:
- "2019"
_4images_image_id: "36483"
_4images_cat_id: "2709"
_4images_user_id: "895"
_4images_image_date: "2013-01-13T18:39:54"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36483 -->
Am rechten Pfosten ist ein Summer (Signalhorn) befestigt. Die Flügel auf der linken Seite des Schleusen-Tores deuten die Schleusen-Kammer an.