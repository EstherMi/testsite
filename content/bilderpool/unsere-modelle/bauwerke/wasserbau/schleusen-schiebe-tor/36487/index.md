---
layout: "image"
title: "Schleu06"
date: "2013-01-13T18:39:54"
picture: "schleusenschiebetor6.jpg"
weight: "6"
konstrukteure: 
- "qincym"
fotografen:
- "qincym"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- details/36487
imported:
- "2019"
_4images_image_id: "36487"
_4images_cat_id: "2709"
_4images_user_id: "895"
_4images_image_date: "2013-01-13T18:39:54"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36487 -->
Das Schleusen-Tor und der Antrieb allein.