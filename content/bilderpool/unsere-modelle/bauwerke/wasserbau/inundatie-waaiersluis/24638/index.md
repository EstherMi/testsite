---
layout: "image"
title: "Inundatie-Waaiersluis"
date: "2009-07-19T17:30:53"
picture: "Papsluis__Afgedamde_Maastocht_022.jpg"
weight: "57"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poedeoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/24638
imported:
- "2019"
_4images_image_id: "24638"
_4images_cat_id: "1692"
_4images_user_id: "22"
_4images_image_date: "2009-07-19T17:30:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24638 -->
Een waaierdeur bestaat uit twee aan elkaar verbonden delen, die rond kunnen draaien in een komvormige inkassing. De kerende (punt-) deur heeft een breedte van 5/6 van de waaierdeur. Beiden deuren vormen samen een soort waaier. De waaierdeuren kunnen naar beide zijden het water keren. Door de waaierkas via riolen met water te vullen wijzigt zich de druk op de deuren zodanig dat deze zowel tegen de stroom in als met de stroom mee open en dicht gedraaid kunnen worden.