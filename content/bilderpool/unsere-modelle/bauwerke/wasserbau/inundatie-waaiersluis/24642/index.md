---
layout: "image"
title: "Inundatie-waaiersluis"
date: "2009-07-19T17:48:43"
picture: "FT-Inundatie-Waaiersluis_015_2.jpg"
weight: "61"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poedeoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/24642
imported:
- "2019"
_4images_image_id: "24642"
_4images_cat_id: "1692"
_4images_user_id: "22"
_4images_image_date: "2009-07-19T17:48:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24642 -->
Een waaiersluis is een speciale sluis, die als voornaamste eigenschap heeft dat hij tegen de waterdruk in geopend en gesloten kan worden. Dit type sluis is uitgevonden door Jan Blanken (1755-1838), de zoon van een dorpstimmerman uit Bergambacht. Jan Blanken was Inspecteur-Generaal bij de Waterstaat van 1808 tot 1826, ten tijde van Napoleon. 

Van dit type sluis zijn nog maar enkele exemplaren in Nederland te vinden.

Een waaierdeur bestaat uit twee aan elkaar verbonden delen, die rond kunnen draaien in een komvormige inkassing. De kerende (punt-) deur heeft een breedte van 5/6 van de waaierdeur. Beiden deuren vormen samen een soort waaier. De waaierdeuren kunnen naar beide zijden het water keren. Door de waaierkas via riolen met water te vullen wijzigt zich de druk op de deuren zodanig dat deze zowel tegen de stroom in als met de stroom mee open en dicht gedraaid kunnen worden.

Gruss,

Peter Damen
Poederoyen NL