---
layout: "image"
title: "Inundatie-waaiersluis-details"
date: "2009-07-24T17:57:03"
picture: "FT-Inundatie-Waaiersluis-buitenwaterzijde.jpg"
weight: "8"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/24671
imported:
- "2019"
_4images_image_id: "24671"
_4images_cat_id: "1692"
_4images_user_id: "22"
_4images_image_date: "2009-07-24T17:57:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24671 -->
Voorzijde buitenwater