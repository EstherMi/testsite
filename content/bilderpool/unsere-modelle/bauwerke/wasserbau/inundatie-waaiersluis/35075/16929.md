---
layout: "comment"
hidden: true
title: "16929"
date: "2012-06-17T18:33:29"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Eine Waaierschleuse ist eine spezielle Schleuse, die gegen den Wasserdruck geöffnet und geschlossen werden kann. Dieser Schleusentyp wurde von Jan Blanken (1755-1838) erfunden. 

Een waaiersluis is een speciale sluis, die als voornaamste eigenschap heeft dat hij tegen de waterdruk in geopend en gesloten kan worden. 
Een waaierdeur bestaat uit twee aan elkaar verbonden delen, die rond kunnen draaien in een komvormige inkassing. De kerende (punt-) deur heeft een breedte van 5/6 van de waaierdeur. Beiden deuren vormen samen een soort waaier. De waaierdeuren kunnen naar beide zijden het water keren. Door de waaierkas via riolen met water te vullen wijzigt zich de druk op de deuren zodanig dat deze zowel tegen de stroom in als met de stroom mee open en dicht gedraaid kunnen worden.
--------------------------------------------------------------------------------

Mehr info über die (Fischertechnik-)  Inundatie-waaiersluizen Nieuwe Hollandse Waterlinie gibt es unter: 

http://www.ftcommunity.de/data/downloads/beschreibungen/ftinundatiewaaiersluis.doc 

Gruss, 

Peter Damen 
Poederoyen NL