---
layout: "overview"
title: "Inundatie-waaiersluis"
date: 2019-12-17T19:49:27+01:00
legacy_id:
- categories/1692
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1692 --> 
Eine Waaierschleuse ist eine spezielle Schleuse, die gegen den Wasserdruck geöffnet und geschlossen werden kann. Dieser Schleusentyp wurde von Jan Blanken (1755-1838) erfunden.

Een waaiersluis is een speciale sluis, die als voornaamste eigenschap heeft dat hij tegen de waterdruk in geopend en gesloten kan worden.
Een waaierdeur bestaat uit twee aan elkaar verbonden delen, die rond kunnen draaien in een komvormige inkassing. De kerende (punt-) deur heeft een breedte van 5/6 van de waaierdeur. Beiden deuren vormen samen een soort waaier. De waaierdeuren kunnen naar beide zijden het water keren. Door de waaierkas via riolen met water te vullen wijzigt zich de druk op de deuren zodanig dat deze zowel tegen de stroom in als met de stroom mee open en dicht gedraaid kunnen worden.