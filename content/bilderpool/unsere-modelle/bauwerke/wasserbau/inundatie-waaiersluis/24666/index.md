---
layout: "image"
title: "Papsluis in Werkendam"
date: "2009-07-21T20:45:17"
picture: "inundatiewaaiersluis1.jpg"
weight: "13"
konstrukteure: 
- "Peter Damen, Poederoyen NL"
fotografen:
- "Peter Damen, Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/24666
imported:
- "2019"
_4images_image_id: "24666"
_4images_cat_id: "1692"
_4images_user_id: "22"
_4images_image_date: "2009-07-21T20:45:17"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24666 -->
Papsluis in Werkendam gab mir Inspiration zum Nachbau einer Inundatie-waaiersluis.

(erster Versuch mit SF ft Community Publisher)