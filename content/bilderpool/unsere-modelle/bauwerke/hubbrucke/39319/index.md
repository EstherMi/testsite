---
layout: "image"
title: "Hebebrucke 4"
date: "2014-08-29T20:39:02"
picture: "Hebebrucke_4.jpg"
weight: "4"
konstrukteure: 
- "J. Steeghs"
fotografen:
- "J. Steeghs"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "JaSpiel"
license: "unknown"
legacy_id:
- details/39319
imported:
- "2019"
_4images_image_id: "39319"
_4images_cat_id: "2945"
_4images_user_id: "1295"
_4images_image_date: "2014-08-29T20:39:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39319 -->
