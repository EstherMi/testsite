---
layout: "image"
title: "Antrieb"
date: "2018-01-14T18:46:42"
picture: "turmmitaufzug06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47089
imported:
- "2019"
_4images_image_id: "47089"
_4images_cat_id: "3483"
_4images_user_id: "104"
_4images_image_date: "2018-01-14T18:46:42"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47089 -->
Irgendwie alles im Turm ist einfach gebaut.