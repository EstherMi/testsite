---
layout: "image"
title: "Steuermodul vor dem Einbau (1)"
date: "2018-01-14T18:46:42"
picture: "turmmitaufzug20.jpg"
weight: "20"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47103
imported:
- "2019"
_4images_image_id: "47103"
_4images_cat_id: "3483"
_4images_user_id: "104"
_4images_image_date: "2018-01-14T18:46:42"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47103 -->
Das unverkabelte Steuermodul, bevor es in den Turm eingebaut wurde.