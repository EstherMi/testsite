---
layout: "image"
title: "Aufzug ganz oben"
date: "2018-01-14T18:46:42"
picture: "turmmitaufzug15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47098
imported:
- "2019"
_4images_image_id: "47098"
_4images_cat_id: "3483"
_4images_user_id: "104"
_4images_image_date: "2018-01-14T18:46:42"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47098 -->
Hier ist das Männchen höher angekommen als ich groß bin.