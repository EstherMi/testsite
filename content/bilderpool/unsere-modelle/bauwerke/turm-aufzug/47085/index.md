---
layout: "image"
title: "Aufzug im Fuß"
date: "2018-01-14T18:46:42"
picture: "turmmitaufzug02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47085
imported:
- "2019"
_4images_image_id: "47085"
_4images_cat_id: "3483"
_4images_user_id: "104"
_4images_image_date: "2018-01-14T18:46:42"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47085 -->
Das ft-Männchen ist im Aufzug gesichert. Unter dem Aufzug sitzt ein Endlagentaster.