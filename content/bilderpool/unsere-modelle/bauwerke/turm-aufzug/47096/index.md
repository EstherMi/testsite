---
layout: "image"
title: "Steuermodul (2)"
date: "2018-01-14T18:46:42"
picture: "turmmitaufzug13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47096
imported:
- "2019"
_4images_image_id: "47096"
_4images_cat_id: "3483"
_4images_user_id: "104"
_4images_image_date: "2018-01-14T18:46:42"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47096 -->
Die Außenseite der Steuerung.