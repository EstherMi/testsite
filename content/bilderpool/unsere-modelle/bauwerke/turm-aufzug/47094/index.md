---
layout: "image"
title: "Befestigung der Aufzugsschiene (3)"
date: "2018-01-14T18:46:42"
picture: "turmmitaufzug11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47094
imported:
- "2019"
_4images_image_id: "47094"
_4images_cat_id: "3483"
_4images_user_id: "104"
_4images_image_date: "2018-01-14T18:46:42"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47094 -->
In der unteren Hälfte des Turms wird die Schiene so gelagert. Ach ja, und: Kabelhalter... ;-)