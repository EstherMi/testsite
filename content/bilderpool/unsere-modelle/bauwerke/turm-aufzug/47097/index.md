---
layout: "image"
title: "Steuermodul (3)"
date: "2018-01-14T18:46:42"
picture: "turmmitaufzug14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47097
imported:
- "2019"
_4images_image_id: "47097"
_4images_cat_id: "3483"
_4images_user_id: "104"
_4images_image_date: "2018-01-14T18:46:42"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47097 -->
An der rechten Kante sitzen eine Reihe von Leuchtsteinsockeln als Anschlüsse: Von oben nach unten führen die zum Motor, obersten Endlagentaster, Taster der zweitobersten und drittobersten Plattform sowie zum untersten Endlagentaster.