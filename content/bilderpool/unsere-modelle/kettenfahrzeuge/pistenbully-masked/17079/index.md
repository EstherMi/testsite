---
layout: "image"
title: "Ansicht von unten"
date: "2009-01-18T20:08:01"
picture: "pb5.jpg"
weight: "8"
konstrukteure: 
- "Martin (Masked)"
fotografen:
- "Martin (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/17079
imported:
- "2019"
_4images_image_id: "17079"
_4images_cat_id: "1533"
_4images_user_id: "373"
_4images_image_date: "2009-01-18T20:08:01"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17079 -->
Normalerweise sitzt noch eine schwarze Grundbauplatte 60x120 über den Motoren, die habe ich für das Foto entfernt. Der schwarze Baustein unter dem rechten Motor ist das alte Blinkmodul aus Lights, das steuert die Warnlampe auf dem Dach.