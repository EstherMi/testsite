---
layout: "image"
title: "Vor dem Einsatz"
date: "2010-12-18T14:02:47"
picture: "kettenfahrzeugschnee1.jpg"
weight: "1"
konstrukteure: 
- "Martin W. (Masked)"
fotografen:
- "Martin W. (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/29471
imported:
- "2019"
_4images_image_id: "29471"
_4images_cat_id: "2144"
_4images_user_id: "373"
_4images_image_date: "2010-12-18T14:02:47"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29471 -->
Im Prinzip ist es der Pistenbully von vor 2 Jahren, nur ohne Aufbauten und mitFolienabdichtung(und 20:1er Motoren, sonst ginge garnichts). Auch die Motoren (& Anschlüsse) sind nach unten hin mit Klarsichtfolie abgedichtet. Das Räumschild ist nicht wirklich efektiv, aber solche Dinge merkt man ja erst im Praxiseinsatz ;-)