---
layout: "image"
title: "Rückansicht"
date: "2009-01-18T20:08:01"
picture: "pb2.jpg"
weight: "5"
konstrukteure: 
- "Martin (Masked)"
fotografen:
- "Martin (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/17076
imported:
- "2019"
_4images_image_id: "17076"
_4images_cat_id: "1533"
_4images_user_id: "373"
_4images_image_date: "2009-01-18T20:08:01"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17076 -->
