---
layout: "image"
title: "Unten"
date: "2009-01-27T17:08:32"
picture: "pistenbullyunten1.jpg"
weight: "1"
konstrukteure: 
- "Martin (Masked)"
fotografen:
- "Martin (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/17169
imported:
- "2019"
_4images_image_id: "17169"
_4images_cat_id: "1533"
_4images_user_id: "373"
_4images_image_date: "2009-01-27T17:08:32"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17169 -->
So, extra für Harald ein Bild von unten mit der eingebauten Grundplatte als Motorschutz. Diese wird gehalten durch die vier eingekreisten Bausteine 7,5, die mit einer Federnocke in die Platte greifen. Lässt sich einfach wegschieben, wenn man mal an die Motoren dran muss und nicht den kompletten Aufbau auseinander nehmen will.