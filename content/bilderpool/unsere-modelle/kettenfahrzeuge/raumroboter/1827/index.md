---
layout: "image"
title: "Räumroboter_003"
date: "2003-10-14T11:28:35"
picture: "rBot003.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Räumroboter"]
uploadBy: "mworks"
license: "unknown"
legacy_id:
- details/1827
imported:
- "2019"
_4images_image_id: "1827"
_4images_cat_id: "194"
_4images_user_id: "61"
_4images_image_date: "2003-10-14T11:28:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1827 -->
IR-Ferngesteuerter Räumroboter