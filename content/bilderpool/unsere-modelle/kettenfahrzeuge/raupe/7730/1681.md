---
layout: "comment"
hidden: true
title: "1681"
date: "2006-12-08T20:41:19"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Ja, die Differenziale sind noch bekannt. Aber, wozu sind die überhaupt da verbaut, und wozu dienen die Zahnräder dazwischen?

Es ist doch so, dass jeder Motor eine Seite der Ketten direkt antreibt. Da sind doch die Differenziale wirkungslos !?

Gruß,
Harald