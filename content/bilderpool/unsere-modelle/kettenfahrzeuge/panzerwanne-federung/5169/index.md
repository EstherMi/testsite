---
layout: "image"
title: "Pz-Wanne03.JPG"
date: "2005-10-31T21:06:56"
picture: "Pz-Wanne03.jpg"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Panzer", "Wanne", "Fahrgestell"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5169
imported:
- "2019"
_4images_image_id: "5169"
_4images_cat_id: "499"
_4images_user_id: "4"
_4images_image_date: "2005-10-31T21:06:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5169 -->
Das Fahrgestell samt Nutzlast auf etwas unebenem Grund.