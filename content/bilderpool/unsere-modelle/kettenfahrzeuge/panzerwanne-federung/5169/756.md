---
layout: "comment"
hidden: true
title: "756"
date: "2005-11-01T20:44:15"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ganz schön breit, das TeilAber die Federung scheint ja prima zu funktionieren. Ob das ganze auch nicht zu schwer wird, um noch fahren zu können? Nicht dass die Raupenglieder am Antriebsrad durchdrehen oder gar von der Kette gedrückt werden? Das wird noch sehr interessant, denke ich. Freue mich schon auf's fertige Modell.

Gruß,
Stefan