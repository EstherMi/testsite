---
layout: "comment"
hidden: true
title: "16656"
date: "2012-03-25T13:03:28"
uploadBy:
- "fitec"
license: "unknown"
imported:
- "2019"
---
Hallo Harald,
vor einiger Zeit hatte ich bezüglich der Federung eine ähnliche Idee, allerdings schlechter umgesetzt: http://www.ftcommunity.de/categories.php?cat_id=2150
Die Idee war ebenfalls die Aufhängung nicht breiter, als die Kette selbst zu machen, was einen riesen Bodenabstand ermöglicht.

Was mir hier gut gefällt sind die verwendeten Räder, die die Kette führen (und nebenbei ein abspringen bei hoher Belastung verhindern!). Das dürfte um einiges sauberer laufen, als die von mir verwendeten Z20 …

Die Gummis sind sicherlich auch sinnvoller als die normalen FT-Federn. Ich vermute mal die laiern sich halt mit der Zeit aus?

Der Antrieb ist natürlich … traumhaft. :)

VG, Nils