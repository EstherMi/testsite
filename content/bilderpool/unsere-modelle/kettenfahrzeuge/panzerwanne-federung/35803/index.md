---
layout: "image"
title: "bully_8365"
date: "2012-10-07T14:41:49"
picture: "IMG_8365.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/35803
imported:
- "2019"
_4images_image_id: "35803"
_4images_cat_id: "499"
_4images_user_id: "4"
_4images_image_date: "2012-10-07T14:41:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35803 -->
Die Lenkung jetzt komplett. Als Sperrelemente dienen die beiden S-Riegel links und rechts. Im Moment liegt die Welle mit den schwarzen Z15 am linken Anschlag, das Z15 links außen ist durch den S-Riegel blockiert und damit wird auch die rechte Kette blockiert.
Der Geradeauslauf ist sehr hakelig einzustellen. Da fehlt für zuverlässigen Betrieb noch jede Menge "Verblockung", damit alle Teile an ihren Plätzen bleiben.

Als Lenkantrieb dient ein Hubgetriebe, das den Schlitten aus Zahnstange, Metallachse und der oberen S-Strebe seitlich verfährt. Weil meistens Zahn auf Zahn steht und die Zahnräder sich nicht ineinander schieben lassen, werden vom Hubgetriebe nur die Federn vorgespannt (der Lenkwunsch wird sozusagen mechanisch gespeichert) und der Rest ergibt sich, wenn die Zahnräder passend zueinander kommen.

Auf der convention hat die Lenkung besser funktioniert als das Fahrwerk selbst. Durch das Mehrgewicht und fehlende Federn an erster und letzter Laufrolle ist das Fahrwerk ständig "durchgesackt", die Federbeine sind nach vorn geschwungen und dann haben sich die Laufrollen gegenseitig blockiert.