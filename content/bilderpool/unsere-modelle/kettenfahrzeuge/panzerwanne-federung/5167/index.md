---
layout: "image"
title: "Pz-Wanne01.JPG"
date: "2005-10-31T20:56:43"
picture: "Pz-Wanne01.jpg"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Panzer", "Wanne", "Fahrgestell"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5167
imported:
- "2019"
_4images_image_id: "5167"
_4images_cat_id: "499"
_4images_user_id: "4"
_4images_image_date: "2005-10-31T20:56:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5167 -->
Die Wanne für ein Kettenfahrzeug. Alle Laufrollen sind einzeln gefedert. In den blauen Federn vorn und hinten stecken zusätzlich noch Stücke von Silikonschlauch, um diese Rollen etwas härter zu federn.

Die Wanne bietet Platz für ein quer (!)liegendes Robo-Interface, das vorzugsweise in die Mitte kommt, damit Akkus (vorn) und Antriebsaggregat (hinten) gegeneinander ausbalanciert werden können.

Was noch fehlt, ist eine Kettenspannmöglichkeit. Außerdem muss das Antriebsritzel noch etwas nach hinten verlagert werden (oder: alle Laufrollen etwas nach vorn), weil die Kette durch den geringen Umschlingungswinkel leicht überspringt.