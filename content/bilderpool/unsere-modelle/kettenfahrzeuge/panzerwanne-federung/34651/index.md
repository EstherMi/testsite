---
layout: "image"
title: "bully48.jpg"
date: "2012-03-16T09:56:39"
picture: "bully48.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/34651
imported:
- "2019"
_4images_image_id: "34651"
_4images_cat_id: "499"
_4images_user_id: "4"
_4images_image_date: "2012-03-16T09:56:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34651 -->
Die halben Gelenksteine waren nötig, um die Einbaubreite für das Rastdifferenzial zu gewinnen. Drinnen steckt je eine Gelenkmutter 35019. Die Z20 sitzen auf zwei Stahlachsen. Damit diese schön "in Flucht" bleiben, ist ein Messingrohr mit Innendurchmesser 4 mm darüber geschoben.

Die Enden der Gummizüge sind um Federnocken geknotet. Über deren Position (weiter hinten oder vorn) kann die Federspannung eingestellt werden.