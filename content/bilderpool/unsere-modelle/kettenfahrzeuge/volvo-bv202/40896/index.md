---
layout: "image"
title: "von oben"
date: "2015-05-01T22:04:59"
picture: "volvobv03.jpg"
weight: "12"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- details/40896
imported:
- "2019"
_4images_image_id: "40896"
_4images_cat_id: "3073"
_4images_user_id: "1924"
_4images_image_date: "2015-05-01T22:04:59"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40896 -->
