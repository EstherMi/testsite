---
layout: "image"
title: "Verdrehausgleich-Detail1"
date: "2015-08-28T21:20:28"
picture: "volvobv5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- details/41872
imported:
- "2019"
_4images_image_id: "41872"
_4images_cat_id: "3073"
_4images_user_id: "1924"
_4images_image_date: "2015-08-28T21:20:28"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41872 -->
Noch ein paar Fotos des Verdrehausgleiches...

Hier schön zu sehen die drei Gelenke um die das Ganze dreht und durch die auch die Antriebswelle geht.
