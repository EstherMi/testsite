---
layout: "image"
title: "Verdrehausgleich-Detail3"
date: "2015-08-28T21:20:28"
picture: "volvobv7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- details/41874
imported:
- "2019"
_4images_image_id: "41874"
_4images_cat_id: "3073"
_4images_user_id: "1924"
_4images_image_date: "2015-08-28T21:20:28"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41874 -->
Vorder- und Hinterwagen sind zueinander nicht Verdreht.

Zur Besseren Sicht habe ich die Box an der Seite abmontiert.