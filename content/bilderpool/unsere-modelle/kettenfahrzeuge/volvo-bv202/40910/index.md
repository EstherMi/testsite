---
layout: "image"
title: "Lenkung-Gesamtansicht-1"
date: "2015-05-01T22:04:59"
picture: "volvobv17.jpg"
weight: "26"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- details/40910
imported:
- "2019"
_4images_image_id: "40910"
_4images_cat_id: "3073"
_4images_user_id: "1924"
_4images_image_date: "2015-05-01T22:04:59"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40910 -->
Vier Lenkzylinder sorgen dafür, dass auch am Teppichboden oder im Gelände problemlos gelenkt werden kann. Der Druckluftfluss wird durch eine Drossel direkt nach dem Kompressor verlangsamt und man kann recht feine Lenkbewegungen ausführen.

Die Steuerung:
Es werden 4 Magnetventile verwenet. Ein Magnetventil wird mit "blockierendem" Anschluss an den Zylinder geschlossen. Dieses wird dann mit einem Motorausgang der Fernsteuerung verbunden. Ein zweites Magnetventil wird mit dem "blockierenden" Anschluss an die Druckluftversorgung angeschlossen und mit dem "offenen" Aschluss mit dem "offenen" Anschluss des ersten Magnetventiles verbunden. Dieses Ventil wird dann mit einem Kabel an den linken Anschuss des Motorausganges (selber Motoranschluss wie bei 1. Ventil) angeschlossen und mit dem zweiten Kabel an die Masse der Stromversorgung.
Das wird dann für den zweiten Anschluss des Zylinders wiederholt, nur hier wird das Ventil, das an der Druckluftversorgung hängt, mit einem Kabel nicht an den linken Anschluss, sondern an den rechten Anschuss des Motorausganges gehängt.
-->ist alles richtig verbunden, fährt der Zylinder in einer Stromrichtung aus und in der anderen Stromrichtung des Motorausganges wieder ein.