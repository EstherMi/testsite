---
layout: "image"
title: "Motorraum-2"
date: "2015-05-01T22:04:59"
picture: "volvobv14.jpg"
weight: "23"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- details/40907
imported:
- "2019"
_4images_image_id: "40907"
_4images_cat_id: "3073"
_4images_user_id: "1924"
_4images_image_date: "2015-05-01T22:04:59"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40907 -->
Hier gut zu sehen ist das Getriebe (notwendig, damit die Kardangelenke im Mittelteil des Fahrzeugs nicht zu viel Drehmoment übertragen müssen). Das gleiche befindet sich auch im Hinterwagen.