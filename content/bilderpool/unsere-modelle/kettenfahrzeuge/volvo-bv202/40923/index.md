---
layout: "image"
title: "Hebebühne-2"
date: "2015-05-01T22:04:59"
picture: "volvobv30.jpg"
weight: "39"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- details/40923
imported:
- "2019"
_4images_image_id: "40923"
_4images_cat_id: "3073"
_4images_user_id: "1924"
_4images_image_date: "2015-05-01T22:04:59"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40923 -->
Hier die Hebebühne ausgefahren.
Sie ist natürlich stark genug um Passagiere mit hochzuheben.