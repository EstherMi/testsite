---
layout: "image"
title: "Führerhaus-Seitenansicht"
date: "2015-05-01T22:04:59"
picture: "volvobv07.jpg"
weight: "16"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- details/40900
imported:
- "2019"
_4images_image_id: "40900"
_4images_cat_id: "3073"
_4images_user_id: "1924"
_4images_image_date: "2015-05-01T22:04:59"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40900 -->
