---
layout: "comment"
hidden: true
title: "22834"
date: "2016-12-23T20:33:46"
uploadBy:
- "Stefan Rm"
license: "unknown"
imported:
- "2019"
---
Hallo FT-Modellbau,

es wirkt schon etwas seltsam, wenn Du unter einem Foto von MEINEM Modell einen Verweis auf ein Video von DEINEM Nachbau/ DEINER Eigenkreation gibts ohne zu erwähnen, dass es sich dabei NICHT um das oben gezeigte Modell von MIR handelt.
Deshalb möchte ich Dich hiermit bitten solche Kommentare inzukunft zu unterlassen!

Und noch ein kleiner Tipp zu Deinem Modell: 
Wie ich schon in der Beschreibung hier: http://www.ftcommunity.de/details.php?image_id=41868 erleuter habe, halten die Pneumatikzylinder in der Lenkung die Beanspruchung der Kolbenstange auf Biegung nicht sehr lange aus. Daher würde ich Dir empfehlen Deine Lenkung umzubauen, um Deine Pneumatikzylinder nicht kaputt zu machen ;-).

Mit freundlichen Grüßen
Stefan Reinmüller