---
layout: "image"
title: "neue Lenkung-oben"
date: "2015-08-28T21:20:28"
picture: "volvobv3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- details/41870
imported:
- "2019"
_4images_image_id: "41870"
_4images_cat_id: "3073"
_4images_user_id: "1924"
_4images_image_date: "2015-08-28T21:20:28"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41870 -->
An der ganzen Lenkung hat sich eigentlich nicht sehr viel geänder, nur, dass jetzt der Aufhänge- (und Drehpunkt) der Lenkzylinder in der Mitte der Zylinder sitzt.