---
layout: "image"
title: "Unterseite-Vorderwagen"
date: "2015-05-01T22:04:59"
picture: "volvobv38.jpg"
weight: "47"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- details/40931
imported:
- "2019"
_4images_image_id: "40931"
_4images_cat_id: "3073"
_4images_user_id: "1924"
_4images_image_date: "2015-05-01T22:04:59"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40931 -->
Hier zu sehen die "fliegende" Aufhängung des Mitteldifferentilas. Hält aber alles, weil kein goßes Drehmoment übertragen werden muss.