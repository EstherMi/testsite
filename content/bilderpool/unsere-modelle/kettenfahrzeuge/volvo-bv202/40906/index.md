---
layout: "image"
title: "Motorraum-1"
date: "2015-05-01T22:04:59"
picture: "volvobv13.jpg"
weight: "22"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- details/40906
imported:
- "2019"
_4images_image_id: "40906"
_4images_cat_id: "3073"
_4images_user_id: "1924"
_4images_image_date: "2015-05-01T22:04:59"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40906 -->
Hier kann man die vier Magnetventile zur Steuerung der Lenkung erkennen, sowie den Kompressor und den Antriebsmotor.
