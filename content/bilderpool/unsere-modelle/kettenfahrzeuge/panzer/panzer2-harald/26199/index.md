---
layout: "image"
title: "Panzer23.jpg"
date: "2010-02-02T23:21:03"
picture: "Panzer23.jpg"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/26199
imported:
- "2019"
_4images_image_id: "26199"
_4images_cat_id: "1861"
_4images_user_id: "4"
_4images_image_date: "2010-02-02T23:21:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26199 -->
Die vorderen Leiträder sind auch gefedert. Sie halten beim Einfedern die Kettenspannung aufrecht.

Die blaue Platte rechts oben hält den ft-Akku (hier herausgenommen) in seinem Fach, mittig auf der schwarzen Bauplatte. Der Akku liegt überkopf, damit die Stecker nicht mit dem Turm in Konflikt geraten.