---
layout: "image"
title: "Panzer20.jpg"
date: "2010-02-02T23:19:51"
picture: "Panzer20.jpg"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/26198
imported:
- "2019"
_4images_image_id: "26198"
_4images_cat_id: "1861"
_4images_user_id: "4"
_4images_image_date: "2010-02-02T23:19:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26198 -->
Die Räder (beim Panzer heißen sie Laufrollen) sind alle einzeln gefedert. Das geschieht hier mit der ft-Lenkklaue 35998 nebst Lenkwürfel 31843. Die Federung erfolgt durch Gummiringe Marke "Edeka", die immer bis zum übernächsten Nachbarn gespannt sind, aber beim Gesamtgewicht des Teils (stramme 2,4 kg) immer noch zu schwach sind. Fazit: das Fahrwerk sackt bei der ersten Gelegenheit einfach nach unten durch. Die hinterste Achse ist deshalb schon mit einer zusätzlichen Feder versehen; den anderen steht ähnliches noch bevor.

Die Antriebseinheit ist nur über die Antriebsachse fest mit der Wanne verbunden und liegt lose auf den gelben Platten auf. Man kann sie also (bei aufrechter Lage der Wanne) hochklappen, oder, wie hier zu sehen, klappt sie beim Umdrehen einfach nach unten. Damit sie im Fahrbetrieb nicht herumzuckt, fehlt noch eine Verriegelung.