---
layout: "comment"
hidden: true
title: "10874"
date: "2010-02-14T14:23:45"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Hier zeigt sich deutlich der erfahrene FT-Modellbauer, Profi Harald:

Mir ist es gefühlte 100 mal schon passiert, dass ein Modell komplett fertig war, und dann bei der abschließenden Verkabelung die Stecker an Motor, Lampe oder Empfänger nicht mehr reingepasst haben! Was für ein Riesenärger!

Der Bau eines Modells von Beginn an mit Dummy-Steckern ohne Kabel an den elektrischen Komponenten wie hier im Bild vermeidet so etwas von Beginn an. Hut ab!

So mache ich's ab jetzt auch.

Gruß, Thomas