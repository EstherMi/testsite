---
layout: "image"
title: "Panzer31.jpg"
date: "2010-02-14T17:39:43"
picture: "Panzer31.jpg"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/26434
imported:
- "2019"
_4images_image_id: "26434"
_4images_cat_id: "1861"
_4images_user_id: "4"
_4images_image_date: "2010-02-14T17:39:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26434 -->
Und jetzt hochgeklappt. Die Verriegelung fehlt immer noch. Dafür sind jetzt Messingachsen anstelle der ft-Rastachsen drin.