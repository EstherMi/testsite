---
layout: "image"
title: "Panzer30.jpg"
date: "2010-02-14T17:38:07"
picture: "Panzer30.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/26433
imported:
- "2019"
_4images_image_id: "26433"
_4images_cat_id: "1861"
_4images_user_id: "4"
_4images_image_date: "2010-02-14T17:38:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26433 -->
Der Antrieb: jeder Motor treibt seine Seite an.