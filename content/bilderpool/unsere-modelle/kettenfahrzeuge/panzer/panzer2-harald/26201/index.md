---
layout: "image"
title: "Panzer28.jpg"
date: "2010-02-02T23:33:35"
picture: "Panzer28.jpg"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/26201
imported:
- "2019"
_4images_image_id: "26201"
_4images_cat_id: "1861"
_4images_user_id: "4"
_4images_image_date: "2010-02-02T23:33:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26201 -->
Die Innereien des Turms. Das Hubgetriebe (quer eingebaut) bewegt über den Seilzug die Kanone auf und ab. Der Gummiring sorgt dafür, dass die Hubzahnstange aus dem Getriebe herauslaufen kann (es gibt keine Endschalter), aber dann trotzdem im Eingriff mit dem Antrieb bleibt, so dass man wieder in die Gegenrichtung verfahren kann. Beim zweiten Antrieb (Turm links/rechts) sitzt unter dem Klemm-Z15 noch ein Z10, das in den Drehkranz außen eingreift.