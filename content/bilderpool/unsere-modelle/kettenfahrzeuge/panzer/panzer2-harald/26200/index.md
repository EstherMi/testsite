---
layout: "image"
title: "Panzer24.jpg"
date: "2010-02-02T23:24:52"
picture: "Panzer24.jpg"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/26200
imported:
- "2019"
_4images_image_id: "26200"
_4images_cat_id: "1861"
_4images_user_id: "4"
_4images_image_date: "2010-02-02T23:24:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26200 -->
Hier gibt es kein Gleichlaufgetriebe. Jede Kettenseite wird mit einem Motor angetrieben, und das war's.

Der Antrieb geht über Rastkegelzahnrad (am Motor, nicht im Bild zu sehen) aufs Rad 45, über ein Z10 auf das Z20, das Rücken an Rücken zum Z30 auf der Achse sitzt. Das äußere Z30 ist nur Dekoration.