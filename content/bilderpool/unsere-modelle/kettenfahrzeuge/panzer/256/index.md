---
layout: "image"
title: "Panzer02"
date: "2003-04-21T21:15:35"
picture: "Panzer02.jpg"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/256
imported:
- "2019"
_4images_image_id: "256"
_4images_cat_id: "32"
_4images_user_id: "1"
_4images_image_date: "2003-04-21T21:15:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=256 -->
