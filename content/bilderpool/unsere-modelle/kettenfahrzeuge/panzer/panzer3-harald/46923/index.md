---
layout: "image"
title: "Tragarme"
date: "2017-11-18T18:01:36"
picture: "pzr1.jpg"
weight: "1"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/46923
imported:
- "2019"
_4images_image_id: "46923"
_4images_cat_id: "3440"
_4images_user_id: "4"
_4images_image_date: "2017-11-18T18:01:36"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46923 -->
In Dreieich gab es noch durchgehende Metallachsen, auf denen die Laufrollen saßen. Die sind jetzt zu Metallstummeln geschrumpft. Insgesamt ist das jetzt die Fahrwerksanordnung wie bei den "großen", außer natürlich den Federungselementen. Die Tragarme sind unverändert geblieben. 
