---
layout: "image"
title: "Wanne"
date: "2017-09-30T19:42:20"
picture: "pz5.jpg"
weight: "11"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/46561
imported:
- "2019"
_4images_image_id: "46561"
_4images_cat_id: "3440"
_4images_user_id: "4"
_4images_image_date: "2017-09-30T19:42:20"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46561 -->
Der Drehkranz Z75, Außenradius 59,8, ist selbst 3D-gedruckt. Der ist "sauberer" als er hier aussieht, weil ich noch nachträglich an den Zähnen gefeilt hatte, damit auch ein Antriebszahnrad mit horizontal liegender Achse (und entsprechend "von oben" eingreifenden Zähnen) drauf passt. Es fehlt auch ein Wulst, an dem man den Turm festklemmen könnte: derzeit liegt er nur lose auf. 
In der Wanne ist noch reichlich Platz vor und hinter dem Drehkranz. Der Raum innerhalb wird aber gefüllt von Bauteilen, die zum Turm gehören.