---
layout: "image"
title: "von oben"
date: "2017-09-30T19:42:20"
picture: "pz3.jpg"
weight: "9"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/46559
imported:
- "2019"
_4images_image_id: "46559"
_4images_cat_id: "3440"
_4images_user_id: "4"
_4images_image_date: "2017-09-30T19:42:20"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46559 -->
Die Heckplatte ist an den Leo2 angelehnt, der dort zwei mächtig große Luftfilter hat.