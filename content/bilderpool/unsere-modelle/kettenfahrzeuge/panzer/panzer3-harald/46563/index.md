---
layout: "image"
title: "Fahrwerk"
date: "2017-09-30T19:42:20"
picture: "pz7.jpg"
weight: "13"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/46563
imported:
- "2019"
_4images_image_id: "46563"
_4images_cat_id: "3440"
_4images_user_id: "4"
_4images_image_date: "2017-09-30T19:42:20"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46563 -->
nochmal aus etwas anderer Perspektive.