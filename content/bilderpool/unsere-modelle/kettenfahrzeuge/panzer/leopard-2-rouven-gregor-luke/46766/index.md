---
layout: "image"
title: "Panzer Leopard 2 (02)"
date: "2017-10-14T18:51:08"
picture: "leopardrouven2.jpg"
weight: "2"
konstrukteure: 
- "Rouven und Gregor Lüke"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rouven"
license: "unknown"
legacy_id:
- details/46766
imported:
- "2019"
_4images_image_id: "46766"
_4images_cat_id: "3462"
_4images_user_id: "2784"
_4images_image_date: "2017-10-14T18:51:08"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46766 -->
Der Turm lässt sich um 360° drehen und das Kanonenrohr in der Höhe verstellen. Beides wird mit einem Minimotor betrieben.
