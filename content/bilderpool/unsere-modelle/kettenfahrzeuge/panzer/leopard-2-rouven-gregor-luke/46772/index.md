---
layout: "image"
title: "Panzer Leopard 2 (08)"
date: "2017-10-14T18:51:08"
picture: "leopardrouven8.jpg"
weight: "8"
konstrukteure: 
- "Rouven und Gregor Lüke"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rouven"
license: "unknown"
legacy_id:
- details/46772
imported:
- "2019"
_4images_image_id: "46772"
_4images_cat_id: "3462"
_4images_user_id: "2784"
_4images_image_date: "2017-10-14T18:51:08"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46772 -->
Mein selbst gedrehtes Kanonenrohr mit Aufnahmestein, um hier die FT-Teil anzubringen. Maße 30x15x30 mit zwei 4mm Nuten für Drehachse und Schubstange.