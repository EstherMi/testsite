---
layout: "image"
title: "Panzer Leopard 2 (04)"
date: "2017-10-14T18:51:08"
picture: "leopardrouven4.jpg"
weight: "4"
konstrukteure: 
- "Rouven und Gregor Lüke"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rouven"
license: "unknown"
legacy_id:
- details/46768
imported:
- "2019"
_4images_image_id: "46768"
_4images_cat_id: "3462"
_4images_user_id: "2784"
_4images_image_date: "2017-10-14T18:51:08"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46768 -->
Da die weichen Kettenbeläge verwendet wurden, ist das Fahren auf Teppichboden nicht optimal, da es dort zum Abspringen der Kette kommen kann. Hier wären die harten Beläge besser.
Das Fahren auf Fliesen oder glatten Böden geht aber sehr gut, selbst das Drehen auf der Hochachse.