---
layout: "image"
title: "Tank Obenansicht"
date: "2003-07-07T13:58:40"
picture: "tankboven1a.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["panzer", "kettenantrieb", "synchronisierter", "Antrieb", "lankheet", "fischertechnikclub", "nederland"]
uploadBy: "ftjohan"
license: "unknown"
legacy_id:
- details/1206
imported:
- "2019"
_4images_image_id: "1206"
_4images_cat_id: "32"
_4images_user_id: "36"
_4images_image_date: "2003-07-07T13:58:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1206 -->
