---
layout: "image"
title: "Kampfpanzer2"
date: "2008-10-22T18:56:33"
picture: "kampfpanzer2.jpg"
weight: "2"
konstrukteure: 
- "Marius"
fotografen:
- "Marius"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/16031
imported:
- "2019"
_4images_image_id: "16031"
_4images_cat_id: "32"
_4images_user_id: "845"
_4images_image_date: "2008-10-22T18:56:33"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16031 -->
Links am Panzer erkennt man den Motor, welcher ihn antreibt.