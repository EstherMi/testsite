---
layout: "comment"
hidden: true
title: "7644"
date: "2008-10-22T19:21:55"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Hallo Marius,

und wieder ein Neuer hier, schön!

Wenn ich freundlich einen Tipp geben darf: Ich glaube nicht, dass es den Zapfen der Bausteine so gut tut, wenn sie, wie hier an den unteren Kettenläufen, so stark verdreht werden. Vielleicht kannst Du das noch verstärken, indem Du z. B. nach oben hin auch mit Bausteinen weiterbaust, damit sich das Kettenrad unten stabiler abstützen kann.

Gruß,
Stefan