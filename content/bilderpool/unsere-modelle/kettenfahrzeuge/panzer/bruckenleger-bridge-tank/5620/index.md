---
layout: "image"
title: "Bridge Tank TM-5-5420-203-14"
date: "2006-01-21T19:52:16"
picture: "Bridging_Tank_TM-5-5420-203-14_019.jpg"
weight: "31"
konstrukteure: 
- "Peter Damen (alias PeterHolland"
fotografen:
- "Peter Damen (alias PeterHolland)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/5620
imported:
- "2019"
_4images_image_id: "5620"
_4images_cat_id: "486"
_4images_user_id: "22"
_4images_image_date: "2006-01-21T19:52:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5620 -->
