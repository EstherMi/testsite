---
layout: "image"
title: "Bridge Tank TM-5-5420-203-14"
date: "2006-02-18T21:24:57"
picture: "Bridging_Tank_TM-5-5420-203-14_058.jpg"
weight: "12"
konstrukteure: 
- "Peter Damen (Poederoyen Holland)"
fotografen:
- "Peter Damen (Poederoyen Holland)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/5783
imported:
- "2019"
_4images_image_id: "5783"
_4images_cat_id: "486"
_4images_user_id: "22"
_4images_image_date: "2006-02-18T21:24:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5783 -->
