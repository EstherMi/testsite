---
layout: "image"
title: "Bridge Tank TM-5-5420-203-14 mit Antonie + Piratenschip"
date: "2006-01-21T19:52:28"
picture: "Bridging_Tank_TM-5-5420-203-14.jpg"
weight: "48"
konstrukteure: 
- "Peter Damen (alias PeterHolland"
fotografen:
- "Peter Damen (alias PeterHolland)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/5637
imported:
- "2019"
_4images_image_id: "5637"
_4images_cat_id: "486"
_4images_user_id: "22"
_4images_image_date: "2006-01-21T19:52:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5637 -->
