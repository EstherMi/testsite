---
layout: "image"
title: "Heckansicht"
date: "2008-10-22T18:56:33"
picture: "kampfpanzer4.jpg"
weight: "4"
konstrukteure: 
- "Marius"
fotografen:
- "Marius"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/16033
imported:
- "2019"
_4images_image_id: "16033"
_4images_cat_id: "32"
_4images_user_id: "845"
_4images_image_date: "2008-10-22T18:56:33"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16033 -->
Hier sieht man den Motor für die Rohrdrehung und die Seilwinde.