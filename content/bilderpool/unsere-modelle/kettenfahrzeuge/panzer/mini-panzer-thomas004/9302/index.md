---
layout: "image"
title: "Mini-Panzer 2"
date: "2007-03-05T21:52:12"
picture: "Mini-Panzer_2.jpg"
weight: "2"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/9302
imported:
- "2019"
_4images_image_id: "9302"
_4images_cat_id: "858"
_4images_user_id: "328"
_4images_image_date: "2007-03-05T21:52:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9302 -->
Er sieht nicht nur breit aus, sondern ist auch deutlich breiter als lang. Knuffig, wie ich finde...