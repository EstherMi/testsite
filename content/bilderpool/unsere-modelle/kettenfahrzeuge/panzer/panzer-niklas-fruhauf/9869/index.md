---
layout: "image"
title: "Panzer"
date: "2007-03-31T18:08:14"
picture: "panzer6.jpg"
weight: "6"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- details/9869
imported:
- "2019"
_4images_image_id: "9869"
_4images_cat_id: "891"
_4images_user_id: "557"
_4images_image_date: "2007-03-31T18:08:14"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9869 -->
von oben