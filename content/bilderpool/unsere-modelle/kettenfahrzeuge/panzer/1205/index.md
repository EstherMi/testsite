---
layout: "image"
title: "Tank mit Soldaten"
date: "2003-07-07T13:58:40"
picture: "tanksoldaten1a.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["panzer", "kettenantrieb", "synchronisierter", "Antrieb", "lankheet", "fischertechnikclub", "nederland"]
uploadBy: "ftjohan"
license: "unknown"
legacy_id:
- details/1205
imported:
- "2019"
_4images_image_id: "1205"
_4images_cat_id: "32"
_4images_user_id: "36"
_4images_image_date: "2003-07-07T13:58:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1205 -->
Panzer mit synchronisierter Antrieb (garantierten geradeauslauf) dürch 2 Powermotoren; 1x Antrieb, 1x steuerung.
2 IR-Empfänger und Accu eingebaut.
Geschütz 360 Grad drehbar und Lauf auf und nieder bewegbar dürch 2 Minimotoren.
Nür Orginal FT-Teile gebraucht (mit ausname von den Zahnstochern).
Der Antrieb wird in Clubheft nr 3-2003 von Fischertechnikclub Nederland ausfürlich präsentiert.