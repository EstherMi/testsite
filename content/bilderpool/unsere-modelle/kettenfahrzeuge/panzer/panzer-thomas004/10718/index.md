---
layout: "image"
title: "Panzer 5"
date: "2007-06-04T21:22:22"
picture: "Panzer_08.jpg"
weight: "5"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/10718
imported:
- "2019"
_4images_image_id: "10718"
_4images_cat_id: "971"
_4images_user_id: "328"
_4images_image_date: "2007-06-04T21:22:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10718 -->
