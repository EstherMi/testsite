---
layout: "image"
title: "Panzer 1"
date: "2007-06-04T21:22:22"
picture: "Panzer_01.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/10714
imported:
- "2019"
_4images_image_id: "10714"
_4images_cat_id: "971"
_4images_user_id: "328"
_4images_image_date: "2007-06-04T21:22:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10714 -->
Hier mein kompakter Panzer mit Antrieb, Lenkung, Akku-Pack und Kabelfernsteuerung. Im Gegensatz zum häufig verwendeten Gleichlaufgetriebe mit 2 Power-Motoren habe ich zur Abwechslung mal Antrieb und Lenkung nach historischem Vorbild mit nur einem Antriebsmotor (Power-Motor) und Lenkung per einseitigem Bremseneingriff dargestellt. So ungefähr haben die ersten Panzer bzw. Kettenfahrzeuge funktioniert.

Der Power-Motor treibt über ein Differenzial ganz normal die beiden Ketten an. Auf jeder Seitenwelle ist zusätzlich ein Zahnrad Z10 angebracht. Der Mini-Motor verschiebt über den kleinen Schneckentrieb einen Hebelmechanismus im Unterboden, der entweder rechts oder links je eine Bauplatte 15x30 als einseitige Bremse in eines dieser beiden Zahnräder Z10 schiebt. Damit wird einseitig die Halbwelle und die Kette blockiert, und das Differenzial leitet die gesamte Antriebskraft zur anderen Kette.

Mit dieser Technik sind natürlich nur 2 Fahrzustände möglich: Geradeausfahrt (durch die normale innere Reibung des Differenzial ganz ordentlich) und Kurvenfahrt mit konstantem Radius um eine stehende Kette. Bei einem normalen Gleichlaufgetriebe gibt es übrigens auch nur 3 Fahrzustände (Geradeausfahrt, Kurvenfahrt mit konstantem Radius, Drehen auf der Stelle), und historische Panzer konnten auch nur mit einem konstanten Kurvenradius lenken. Somit also ganz brauchbar…

Größter Vorteil dieses Konzepts ist eindeutig die kompakte Bauweise. Der Panzer hat eine sehr hohe Bodenfreiheit bei gleichzeitig sehr niedrigem Aufbau, was für diese Größenverhältnisse eine extreme Geländegängigkeit ermöglicht.