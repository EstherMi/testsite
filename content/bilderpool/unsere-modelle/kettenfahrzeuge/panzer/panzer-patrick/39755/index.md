---
layout: "image"
title: "Panzer"
date: "2014-10-27T17:53:42"
picture: "tank2.jpg"
weight: "2"
konstrukteure: 
- "Patrick"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/39755
imported:
- "2019"
_4images_image_id: "39755"
_4images_cat_id: "2982"
_4images_user_id: "2228"
_4images_image_date: "2014-10-27T17:53:42"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39755 -->
Dank einem Minimotor lässt sich der Aufbau drehen.