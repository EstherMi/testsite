---
layout: "image"
title: "Minipanzer - Einzelteile"
date: "2012-09-16T20:52:47"
picture: "minipanzer3.jpg"
weight: "3"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/35527
imported:
- "2019"
_4images_image_id: "35527"
_4images_cat_id: "2635"
_4images_user_id: "1126"
_4images_image_date: "2012-09-16T20:52:47"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35527 -->
Ganze 13 Bauteile benötigt der Minipanzer.