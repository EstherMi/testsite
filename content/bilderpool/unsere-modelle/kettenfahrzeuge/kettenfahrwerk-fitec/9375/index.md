---
layout: "image"
title: "Kettenfahrwerk"
date: "2007-03-10T19:30:57"
picture: "Kettenfahrwerk6.jpg"
weight: "15"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/9375
imported:
- "2019"
_4images_image_id: "9375"
_4images_cat_id: "865"
_4images_user_id: "456"
_4images_image_date: "2007-03-10T19:30:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9375 -->
Von unten. Man sieht gut die 2x4 Z20 für die Kettenaufhängungen.