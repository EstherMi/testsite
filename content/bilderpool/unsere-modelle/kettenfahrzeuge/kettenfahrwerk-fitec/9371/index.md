---
layout: "image"
title: "Kettenfahrwerk"
date: "2007-03-10T19:30:57"
picture: "Kettenfahrwerk2.jpg"
weight: "11"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/9371
imported:
- "2019"
_4images_image_id: "9371"
_4images_cat_id: "865"
_4images_user_id: "456"
_4images_image_date: "2007-03-10T19:30:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9371 -->
Auf die Z30 kommen die Ketten.