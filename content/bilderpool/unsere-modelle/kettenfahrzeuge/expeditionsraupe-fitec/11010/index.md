---
layout: "image"
title: "Änderung"
date: "2007-07-01T12:32:57"
picture: "Expeditionsraupe9.jpg"
weight: "9"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/11010
imported:
- "2019"
_4images_image_id: "11010"
_4images_cat_id: "995"
_4images_user_id: "456"
_4images_image_date: "2007-07-01T12:32:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11010 -->
Ich habe an den Antriebszahnrädern und an den beiden vorderen Zahnrädern zusätzlich Zahnräder angebracht. So kann die Kette nicht abgehen.