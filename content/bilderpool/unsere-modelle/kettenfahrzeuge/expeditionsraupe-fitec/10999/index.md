---
layout: "image"
title: "Gleichlaufgetriebe"
date: "2007-07-01T12:32:57"
picture: "Expeditionsraupe2.jpg"
weight: "2"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/10999
imported:
- "2019"
_4images_image_id: "10999"
_4images_cat_id: "995"
_4images_user_id: "456"
_4images_image_date: "2007-07-01T12:32:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10999 -->
Hier sieht man das Gleichlaufgetriebe. Es ist schon mal einzeln ausgestellt. Auf der einen Seite sind ein Z10 und ein Z20 mit Kette verbunden, auf der anderen Seite greift ein Z15 in ein Z30.