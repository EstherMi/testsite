---
layout: "image"
title: "Expeditionsraupe"
date: "2007-07-01T12:32:57"
picture: "Expeditionsraupe4.jpg"
weight: "4"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/11002
imported:
- "2019"
_4images_image_id: "11002"
_4images_cat_id: "995"
_4images_user_id: "456"
_4images_image_date: "2007-07-01T12:32:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11002 -->
Hier fährt sie auf einem Weg im Garten. Ich habe mit ihr und mit tim-tech, der auch eine Raupe gebaut hat, eine Tour durch unseren Garten gemacht. Von gepflastertem Weg und Rasen auf mein Paprikabeet mit Erde.