---
layout: "image"
title: "Kettenfahrwerk (linke Seite)"
date: "2005-05-30T20:17:50"
picture: "Kettenfahrwerk_001.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Kettenfahrwerk"]
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/4326
imported:
- "2019"
_4images_image_id: "4326"
_4images_cat_id: "500"
_4images_user_id: "104"
_4images_image_date: "2005-05-30T20:17:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4326 -->
Das Fahrwerk soll natürlich noch einen Aufbau bekommen (einen Greifer oder sowas). Vor allem sollte es aber so kompakt und leicht wie möglich, aber ausschweifend geländefähig sein (siehe entsprechendes Video).