---
layout: "image"
title: "Kettenfahrwerk (rechte Seite)"
date: "2005-05-30T20:17:50"
picture: "Kettenfahrwerk_003.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/4327
imported:
- "2019"
_4images_image_id: "4327"
_4images_cat_id: "500"
_4images_user_id: "104"
_4images_image_date: "2005-05-30T20:17:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4327 -->
Der Antrieb besteht aus zwei 1:50 Powermotoren zum Synchronantrieb der beiden Ketten: Bei Geradeausfahrt laufen beide Ketten laufen exakt gleich schnell in die gleiche Richtung. Beim Drehen auf der Stelle laufen beide Ketten exakt gleich schnell in entgegensetze Richtungen. Der obere Motor ist der zum Lenken, der untere zum Fahren. Gelenkt wird mit einer 1:2-Untersetzung. Dadurch steht beim Drehen auf der Stelle mehr Kraft zur Verfügung. Die braucht es auch, denn dabei sind die Reibungsverluste auf dem Boden natürlich hoch. Außerdem kann man dann durch gleichzeitiges "Fahren" und "Lenken" (sprich: Einschalten beider Motoren gleichzeitig) eine schöne Kurve fahren, denn die kurveninnere Kette dreht sich dann halb so schnell wie normal, die kurvenäußere doppelt so schnell.