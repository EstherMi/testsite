---
layout: "image"
title: "Antrieb Motorenbefestigung links"
date: "2005-10-30T11:29:25"
picture: "Antrieb_006.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/5147
imported:
- "2019"
_4images_image_id: "5147"
_4images_cat_id: "500"
_4images_user_id: "104"
_4images_image_date: "2005-10-30T11:29:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5147 -->
Die Motoren sind alle vierfach aufgehängt. Zwischen ihnen ist gerade genug Platz für einen Flachstein 30 * 60, der zwischen den Alus quer eingebaut war.