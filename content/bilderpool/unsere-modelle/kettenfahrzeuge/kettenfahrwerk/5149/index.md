---
layout: "image"
title: "Antrieb Abstände links"
date: "2005-10-30T11:29:25"
picture: "Antrieb_008.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/5149
imported:
- "2019"
_4images_image_id: "5149"
_4images_cat_id: "500"
_4images_user_id: "104"
_4images_image_date: "2005-10-30T11:29:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5149 -->
Hier sieht man, wie alles zusammen passt. Der Abstand zwischen den Motoren sollte so gering wie möglich ausfallen. Das kleinste, was dazwischen muss, ist ein BS 5. Die von den Zahnrädern befreite Achse ist die Antriebsachse.