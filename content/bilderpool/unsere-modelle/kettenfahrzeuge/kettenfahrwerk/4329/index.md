---
layout: "image"
title: "Kettenfahrzeug (Rückseite)"
date: "2005-05-30T20:17:50"
picture: "Kettenfahrwerk_004.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/4329
imported:
- "2019"
_4images_image_id: "4329"
_4images_cat_id: "500"
_4images_user_id: "104"
_4images_image_date: "2005-05-30T20:17:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4329 -->
Etwas unschaft (bessere Fotos werden nachgereicht). Die Metall-Querstange dient nur zur Versteifung der Aufhängung der Antriebsräder.