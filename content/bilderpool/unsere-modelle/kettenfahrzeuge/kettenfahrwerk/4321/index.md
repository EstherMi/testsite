---
layout: "image"
title: "Kettenfahrwerk (Blick auf den Antrieb)"
date: "2005-05-30T20:17:50"
picture: "Kettenfahrwerk_005.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/4321
imported:
- "2019"
_4images_image_id: "4321"
_4images_cat_id: "500"
_4images_user_id: "104"
_4images_image_date: "2005-05-30T20:17:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4321 -->
Es wurde heftig getrickst, um den Synchronantrieb möglichst klein zu bekommen.