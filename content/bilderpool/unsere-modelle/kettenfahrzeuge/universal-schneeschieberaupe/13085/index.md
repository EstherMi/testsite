---
layout: "image"
title: "Universal-Schneeschieberaupe"
date: "2007-12-17T18:22:07"
picture: "Universal-Schneeschieberaupe1.jpg"
weight: "6"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/13085
imported:
- "2019"
_4images_image_id: "13085"
_4images_cat_id: "1186"
_4images_user_id: "456"
_4images_image_date: "2007-12-17T18:22:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13085 -->
Das ist mein Beitrag zu diesem Thema. Er soll später das Räumschild heben und senken können, wahrscheinlich hydraulisch. Er besitzt ein Gleichlaufgetriebe mit 2 50:1 Power-Motoren, welche auf die ketten nochmals 3:1 untersetzt sind.