---
layout: "image"
title: "Untenansicht"
date: "2007-12-17T18:22:08"
picture: "Universal-Schneeschieberaupe5.jpg"
weight: "10"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/13089
imported:
- "2019"
_4images_image_id: "13089"
_4images_cat_id: "1186"
_4images_user_id: "456"
_4images_image_date: "2007-12-17T18:22:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13089 -->
Von unten. Leider hat es bei mir noch nicht geschneit, ich muss also noch etwas mit dem testen warten, aber er eignet sich hervorragend dazu, Tannennadeln unter dem Christbaum wegzukehren.