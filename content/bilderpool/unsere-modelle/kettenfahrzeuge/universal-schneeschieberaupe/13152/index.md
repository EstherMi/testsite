---
layout: "image"
title: "Hydraulik"
date: "2007-12-23T13:36:17"
picture: "Universal-Schneeschieberaupe8.jpg"
weight: "3"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/13152
imported:
- "2019"
_4images_image_id: "13152"
_4images_cat_id: "1186"
_4images_user_id: "456"
_4images_image_date: "2007-12-23T13:36:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13152 -->
Jawohl, endlich eine richtige Hydraulik. Ich habe in ebay 6 Stück dieser Hydraulikzylinder60 ersteigert. Dieser einzelne Zylinder hat bereits Gewichte gehoben, welche 2 Pneumatikzylinder kaum schafften. Ich benutze destiliertes Wasser, damit die Zylinder nicht so schnell verkalken.