---
layout: "image"
title: "Bauphase kurz vor dem Ende"
date: "2013-08-06T18:51:14"
picture: "IMG_7834.jpg"
weight: "10"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- details/37221
imported:
- "2019"
_4images_image_id: "37221"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2013-08-06T18:51:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37221 -->
Das Modell noch im Bau, aber kurz vor der Vollendung.