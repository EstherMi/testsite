---
layout: "image"
title: "Fertig"
date: "2012-02-19T15:44:28"
picture: "IMG_3870.jpg"
weight: "43"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- details/34292
imported:
- "2019"
_4images_image_id: "34292"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2012-02-19T15:44:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34292 -->
Der Motorblock in seiner endgültigen Form.