---
layout: "overview"
title: "Ein etwas weniger universelles Kettenfahrzeug"
date: 2019-12-17T19:47:39+01:00
legacy_id:
- categories/2537
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2537 --> 
Zeit ist vergangen, und das ursprüngliche Monster, das kaum über ein Blatt Papier fahren konnte, hat sich entwickelt.