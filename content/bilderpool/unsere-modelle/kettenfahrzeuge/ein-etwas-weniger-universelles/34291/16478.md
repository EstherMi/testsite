---
layout: "comment"
hidden: true
title: "16478"
date: "2012-02-26T13:58:10"
uploadBy:
- "Defiant"
license: "unknown"
imported:
- "2019"
---
Denk daran, daß hier jedes Getriebe ordentlich Verluste einfährt, ein Motor mit einem Getriebe ist deswegen oft besser als 2 Motoren mit 2 Getrieben.
Wieviel kg willst du eigentlich bewegen?
Eventuell macht es Sinn auf nicht-ft-Motoren auszuweichen, oder ein Power Motor mit höherer Übersetzung.