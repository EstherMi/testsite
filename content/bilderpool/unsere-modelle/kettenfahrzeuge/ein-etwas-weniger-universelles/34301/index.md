---
layout: "image"
title: "Eingesetzt 4"
date: "2012-02-19T15:44:28"
picture: "IMG_3819.jpg"
weight: "52"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- details/34301
imported:
- "2019"
_4images_image_id: "34301"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2012-02-19T15:44:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34301 -->
Der Motorblock hat in Längrichtung ca. 1mm Spiel.