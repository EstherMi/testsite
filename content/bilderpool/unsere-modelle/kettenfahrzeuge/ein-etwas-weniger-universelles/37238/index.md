---
layout: "image"
title: "Wartung und Erweiterung 2"
date: "2013-08-07T18:39:27"
picture: "IMG_9079.jpg"
weight: "7"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- details/37238
imported:
- "2019"
_4images_image_id: "37238"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2013-08-07T18:39:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37238 -->
Die zweite Antriebswelle läßt sich entnehmen.