---
layout: "image"
title: "Richtig steif"
date: "2012-02-19T15:44:06"
picture: "IMG_3577.jpg"
weight: "21"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- details/34270
imported:
- "2019"
_4images_image_id: "34270"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2012-02-19T15:44:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34270 -->
Diesmal setzte ich auf Alu-Profile. Sie verringern zwar nicht das Gesamtgewicht, sondern sind ca. 30% schwerer als ihre Kollegen aus Plastik, haben aber eine wahnsinnige Festigkeit. Ich habe mich für Drehkränze entschieden, weil sie die größten Zahnräder sind und sie relativ einfach mit der nötigen Festigkeit in der richtigen Position gehalten werden können ( keine achsen - nur zapfen und nuten )