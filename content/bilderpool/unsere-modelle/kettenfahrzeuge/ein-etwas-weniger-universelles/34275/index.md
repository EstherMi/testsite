---
layout: "image"
title: "Mehr Power"
date: "2012-02-19T15:44:06"
picture: "IMG_3610.jpg"
weight: "26"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- details/34275
imported:
- "2019"
_4images_image_id: "34275"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2012-02-19T15:44:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34275 -->
das ding fährt mit vier motoren schon ganz ordentlich. über ein kleines kopfkissen (30x30) klettert es ganz ordentlich. Da das aber nur der nackte rahmen ist, denke ich, daß doppelte power besser ist. also 4x2 motoren.