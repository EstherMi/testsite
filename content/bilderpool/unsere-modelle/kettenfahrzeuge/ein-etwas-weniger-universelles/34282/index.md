---
layout: "image"
title: "Neuer Motorblock"
date: "2012-02-19T15:44:28"
picture: "IMG_3739.jpg"
weight: "33"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- details/34282
imported:
- "2019"
_4images_image_id: "34282"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2012-02-19T15:44:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34282 -->
das ist der prototyp des neuen motorblocks. vier blöcke mit je 2 power-motoren. das ganze möchte ich modular aufbauen, so daß nach lösen der kette der ganze block ausgetauscht wird. an der überstzung muß ich noch arbeiten. in der version kriecht es und eine schnecke wäre wahrscheinlich nicht langsamer. der 8:1 ist wohl die bessere wahl.