---
layout: "image"
title: "Endlich geschafft"
date: "2013-08-06T18:51:14"
picture: "IMG_8713.jpg"
weight: "9"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- details/37220
imported:
- "2019"
_4images_image_id: "37220"
_4images_cat_id: "2537"
_4images_user_id: "859"
_4images_image_date: "2013-08-06T18:51:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37220 -->
Nach einer weiteren Pause und dem Zukauf der benötigten Teile ist das Fahrgestell endlich fertig. Wenn ich dazu komme, schneide ich ein Video und stelle es bei youtube ein