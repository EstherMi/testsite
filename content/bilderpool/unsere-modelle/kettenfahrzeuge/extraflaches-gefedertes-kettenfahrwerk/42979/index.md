---
layout: "image"
title: "Kettenfahrwerk von unten"
date: "2016-03-02T12:54:17"
picture: "federkette3.jpg"
weight: "3"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/42979
imported:
- "2019"
_4images_image_id: "42979"
_4images_cat_id: "3196"
_4images_user_id: "558"
_4images_image_date: "2016-03-02T12:54:17"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42979 -->
Die Kette umschließt das Fahrwerk vollständig, es liegt also, egal in welcher Position, nur die Kette auf.