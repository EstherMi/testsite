---
layout: "image"
title: "Kettenfahrwerk Getriebe"
date: "2016-05-28T18:15:40"
picture: "gefederteskettenfahrwerk09.jpg"
weight: "9"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/43440
imported:
- "2019"
_4images_image_id: "43440"
_4images_cat_id: "3227"
_4images_user_id: "558"
_4images_image_date: "2016-05-28T18:15:40"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43440 -->
Hier lässt sich schön der Gleichlauf sehen. Angetrieben wird das ganze von 2x PM 20:1 für Geradeausfahrt und einen für die Lenkung. Die beiden Empfänger