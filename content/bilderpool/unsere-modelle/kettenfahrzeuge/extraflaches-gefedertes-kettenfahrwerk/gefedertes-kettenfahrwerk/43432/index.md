---
layout: "image"
title: "Kettenfahrwerk mit Geschützturm"
date: "2016-05-28T18:15:40"
picture: "gefederteskettenfahrwerk01.jpg"
weight: "1"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/43432
imported:
- "2019"
_4images_image_id: "43432"
_4images_cat_id: "3227"
_4images_user_id: "558"
_4images_image_date: "2016-05-28T18:15:40"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43432 -->
Die Verkleidung ist zu Anschauungszwecken noch nicht angebaut.