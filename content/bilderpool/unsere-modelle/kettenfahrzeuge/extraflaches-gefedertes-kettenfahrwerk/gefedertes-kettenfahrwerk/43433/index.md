---
layout: "image"
title: "Kettenfahrwerk mit Hauptantrieb und Drehscheibe"
date: "2016-05-28T18:15:40"
picture: "gefederteskettenfahrwerk02.jpg"
weight: "2"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/43433
imported:
- "2019"
_4images_image_id: "43433"
_4images_cat_id: "3227"
_4images_user_id: "558"
_4images_image_date: "2016-05-28T18:15:40"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43433 -->
Verkleidung fehlt noch, Fotos gibts aber jetzt schon um euch das innere ohne großen Aufwand zeigen zu können ;)