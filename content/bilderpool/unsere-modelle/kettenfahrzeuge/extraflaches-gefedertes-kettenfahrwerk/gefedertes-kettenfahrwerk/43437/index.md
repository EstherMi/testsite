---
layout: "image"
title: "Rohrneigung unten"
date: "2016-05-28T18:15:40"
picture: "gefederteskettenfahrwerk06.jpg"
weight: "6"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/43437
imported:
- "2019"
_4images_image_id: "43437"
_4images_cat_id: "3227"
_4images_user_id: "558"
_4images_image_date: "2016-05-28T18:15:40"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43437 -->
Motor unter der Verlängerung