---
layout: "image"
title: "IR Pneumatik Raupen-Löffelbagger von DasKasperle - schräg OBEN"
date: "2013-05-26T09:50:17"
picture: "irpneumatikraupenloeffelbaggervondaskasperle03.jpg"
weight: "3"
konstrukteure: 
- "DasKasperle alias Sushiteck"
fotografen:
- "Kai"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- details/36961
imported:
- "2019"
_4images_image_id: "36961"
_4images_cat_id: "2748"
_4images_user_id: "1677"
_4images_image_date: "2013-05-26T09:50:17"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36961 -->
