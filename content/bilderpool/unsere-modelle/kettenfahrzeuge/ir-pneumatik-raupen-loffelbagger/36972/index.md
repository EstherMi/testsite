---
layout: "image"
title: "IR Pneumatik Raupen-Löffelbagger von DasKasperle - UNTEN"
date: "2013-05-26T09:50:17"
picture: "irpneumatikraupenloeffelbaggervondaskasperle14.jpg"
weight: "14"
konstrukteure: 
- "DasKasperle alias Sushiteck"
fotografen:
- "Kai"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- details/36972
imported:
- "2019"
_4images_image_id: "36972"
_4images_cat_id: "2748"
_4images_user_id: "1677"
_4images_image_date: "2013-05-26T09:50:17"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36972 -->
