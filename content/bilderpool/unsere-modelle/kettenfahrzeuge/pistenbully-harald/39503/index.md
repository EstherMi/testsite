---
layout: "image"
title: "Einzelradfederung"
date: "2014-10-02T21:56:48"
picture: "pistenbully06.jpg"
weight: "6"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/39503
imported:
- "2019"
_4images_image_id: "39503"
_4images_cat_id: "2958"
_4images_user_id: "4"
_4images_image_date: "2014-10-02T21:56:48"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39503 -->
Zarte Gemüter mögen bitte weiter klicken.

Hier wurde gemoddet: die Rollenböcke haben nur 8,5 mm "Luft", und da passt ein BS7,5 nur dann quer hinein, wenn man mit der Säge nach geholfen hat. Das Ganze zweimal pro Laufrad, und schon ist eine Parallelogrammführung fertig. Das federnde Element ist ein Gummizug aus dem Haushaltswarenladen, der einmal der Reihe nach durch alle Radaufhängungen hindurch gefädelt ist.