---
layout: "image"
title: "Gesamtansicht"
date: "2008-12-14T10:53:30"
picture: "industreadozer7.jpg"
weight: "7"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/16617
imported:
- "2019"
_4images_image_id: "16617"
_4images_cat_id: "1504"
_4images_user_id: "845"
_4images_image_date: "2008-12-14T10:53:30"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16617 -->
