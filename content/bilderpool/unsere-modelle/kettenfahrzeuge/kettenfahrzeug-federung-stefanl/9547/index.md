---
layout: "image"
title: "Kettenfahrzeug 8"
date: "2007-03-17T18:15:09"
picture: "kettenfahtzeugmitfederung08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/9547
imported:
- "2019"
_4images_image_id: "9547"
_4images_cat_id: "872"
_4images_user_id: "502"
_4images_image_date: "2007-03-17T18:15:09"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9547 -->
