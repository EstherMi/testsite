---
layout: "image"
title: "Kettenfahrzeug 13"
date: "2007-03-17T18:16:58"
picture: "kettenfahtzeugmitfederung13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/9552
imported:
- "2019"
_4images_image_id: "9552"
_4images_cat_id: "872"
_4images_user_id: "502"
_4images_image_date: "2007-03-17T18:16:58"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9552 -->
Hier sieht man die Einrastfunktion des Deckels.