---
layout: "image"
title: "Frontalansicht"
date: "2010-12-27T16:29:50"
picture: "panzer5.jpg"
weight: "5"
konstrukteure: 
- "Nils"
fotografen:
- "Nils"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/29551
imported:
- "2019"
_4images_image_id: "29551"
_4images_cat_id: "2150"
_4images_user_id: "456"
_4images_image_date: "2010-12-27T16:29:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29551 -->
Sehr großer Bodenabstand!