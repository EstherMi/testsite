---
layout: "image"
title: "Armmitte 2"
date: "2005-08-21T20:54:36"
picture: "Kettenfahrwerk_mit_Arm_008.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/4624
imported:
- "2019"
_4images_image_id: "4624"
_4images_cat_id: "372"
_4images_user_id: "104"
_4images_image_date: "2005-08-21T20:54:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4624 -->
Und hier geht's ab zur nächsten Längsachse. Das Mittelgelenk hätte auch nur mit je einem Gelenk pro Teilarm (also insgesamt drei anstatt sechs) und somit ohne die Z10 ausgeführt werden können. Aber das hätte zur Folge dass der Arm nicht so scharf abgeknickt werden könnte (erster Arm ganz hoch, zweiter ganz herunter), denn das ankommende und abgehende Winkelzahnrad würden sich dann berühren.