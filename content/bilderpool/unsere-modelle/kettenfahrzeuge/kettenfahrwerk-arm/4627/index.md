---
layout: "image"
title: "Armpositionen 1"
date: "2005-08-21T20:54:36"
picture: "Kettenfahrwerk_mit_Arm_011.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/4627
imported:
- "2019"
_4images_image_id: "4627"
_4images_cat_id: "372"
_4images_user_id: "104"
_4images_image_date: "2005-08-21T20:54:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4627 -->
Hier die eine Extremposition: Beide Armsegmente ganz nach oben. Auch in dieser Stellung ist der Greifer voll steuerbar.