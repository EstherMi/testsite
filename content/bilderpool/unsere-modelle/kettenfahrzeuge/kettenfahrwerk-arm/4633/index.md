---
layout: "image"
title: "Die Schaufel 2"
date: "2005-08-21T20:54:36"
picture: "Kettenfahrwerk_mit_Arm_017.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/4633
imported:
- "2019"
_4images_image_id: "4633"
_4images_cat_id: "372"
_4images_user_id: "104"
_4images_image_date: "2005-08-21T20:54:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4633 -->
Hier die Schaufel von der anderen Seite gesehen.