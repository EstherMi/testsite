---
layout: "image"
title: "Armmitte 1"
date: "2005-08-21T20:36:53"
picture: "Kettenfahrwerk_mit_Arm_007.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/4623
imported:
- "2019"
_4images_image_id: "4623"
_4images_cat_id: "372"
_4images_user_id: "104"
_4images_image_date: "2005-08-21T20:36:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4623 -->
Hier kommt die Achse am Mittelgelenk an. Dadurch dass das vom Motor angetriebene Winkelzahnrad auf der ANDEREN Seite liegt als das hier, ergibt sich der schöne Effekt, dass sich beim Heben oder Senken des Armsegmentes die Lage der hier sichtbaren Z10 nicht ändern. Die längs durch den Arm laufende Achse rollt auf dem ersten Winkelzahnrad ab und hält das nächste hier in der selben absoluten Position. Dadurch bleibt der Greifer/die Schaufel von Armbewegungen unbeeinflusst.