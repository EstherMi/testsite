---
layout: "image"
title: "Armantrieb"
date: "2005-08-21T20:36:53"
picture: "Kettenfahrwerk_mit_Arm_003.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/4619
imported:
- "2019"
_4images_image_id: "4619"
_4images_cat_id: "372"
_4images_user_id: "104"
_4images_image_date: "2005-08-21T20:36:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4619 -->
Der Minimot rechts unten im Bild steuert den Greifer bzw. die Schaufel am entfernten Ende des Arms. Der obere rechte Motor (im Bild vorne) steuert das äußere Armsegment, der obere linke (im Bild hinten) das innere.