---
layout: "image"
title: "Seilzüge"
date: "2005-08-21T20:36:53"
picture: "Kettenfahrwerk_mit_Arm_004.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/4620
imported:
- "2019"
_4images_image_id: "4620"
_4images_cat_id: "372"
_4images_user_id: "104"
_4images_image_date: "2005-08-21T20:36:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4620 -->
Die Seilzüge für die Armsegmente sind beliebig primitiv: Ein Stück Faden ist direkt auf die jeweilige Achse geknotet. Ohne weitere Untersetzungsgetriebe ergibt das direkt eine höchst brauchbare Geschwindigkeit und Kraft (siehe das entsprechende Video).