---
layout: "image"
title: "Armsegmente"
date: "2005-08-21T20:29:05"
picture: "Kettenfahrwerk_mit_Arm_002.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/4618
imported:
- "2019"
_4images_image_id: "4618"
_4images_cat_id: "372"
_4images_user_id: "104"
_4images_image_date: "2005-08-21T20:29:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4618 -->
Die beiden Segmente des Arms sind völlig unabhängig voneinander steuerbar. Wiederum völlig unabhängig von der Bewegung bleibt die Stellung des Greifers/der Schaufel völlig gleich.