---
layout: "image"
title: "Gesamtansicht"
date: "2011-10-29T17:04:57"
picture: "fahrstuhlrentnerspass1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "user"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stainless"
license: "unknown"
legacy_id:
- details/33349
imported:
- "2019"
_4images_image_id: "33349"
_4images_cat_id: "2469"
_4images_user_id: "1376"
_4images_image_date: "2011-10-29T17:04:57"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33349 -->
Wieso sollten Rentner eigentlich nur Bücher lesen und Miniatureisenbahnen über ihren Schreibtisch fahren lassen ;o)
Ferngesteuert, über 2 S-Motoren angetrieben und mit Spaßgarantie :-)
Der Stuhl kommt zügig voran, ein gedeckter Kaffeetisch ist zum Leidwesen meiner Mutter kein Hindernis. Auch eine flotte Runde übers Sofa funktioniert super.