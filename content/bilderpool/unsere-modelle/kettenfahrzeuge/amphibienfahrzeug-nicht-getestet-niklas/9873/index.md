---
layout: "image"
title: "Amphibienfahrzeug(nicht getestet)"
date: "2007-04-01T17:49:09"
picture: "amphibienfahrzeug1.jpg"
weight: "1"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- details/9873
imported:
- "2019"
_4images_image_id: "9873"
_4images_cat_id: "892"
_4images_user_id: "557"
_4images_image_date: "2007-04-01T17:49:09"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9873 -->
Front.Antrieb durch zwei Poermotoren, dahinter Interface