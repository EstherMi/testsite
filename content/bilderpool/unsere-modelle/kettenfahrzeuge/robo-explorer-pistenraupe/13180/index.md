---
layout: "image"
title: "Pistenraupe 1"
date: "2008-01-01T09:40:10"
picture: "roboexplorerpistenraupe01.jpg"
weight: "1"
konstrukteure: 
- "water sebi"
fotografen:
- "water sebi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "watersebi"
license: "unknown"
legacy_id:
- details/13180
imported:
- "2019"
_4images_image_id: "13180"
_4images_cat_id: "1191"
_4images_user_id: "642"
_4images_image_date: "2008-01-01T09:40:10"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13180 -->
