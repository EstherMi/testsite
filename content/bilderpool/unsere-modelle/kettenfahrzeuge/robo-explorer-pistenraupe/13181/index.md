---
layout: "image"
title: "Pistenraupe 2"
date: "2008-01-01T09:40:10"
picture: "roboexplorerpistenraupe02.jpg"
weight: "2"
konstrukteure: 
- "water sebi"
fotografen:
- "water sebi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "watersebi"
license: "unknown"
legacy_id:
- details/13181
imported:
- "2019"
_4images_image_id: "13181"
_4images_cat_id: "1191"
_4images_user_id: "642"
_4images_image_date: "2008-01-01T09:40:10"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13181 -->
