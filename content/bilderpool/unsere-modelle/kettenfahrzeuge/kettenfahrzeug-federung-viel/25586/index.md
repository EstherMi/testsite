---
layout: "image"
title: "Erster Entwurf"
date: "2009-10-30T21:05:04"
picture: "kettenfahrzeugmitfederungundvielbodenfreiheit2.jpg"
weight: "2"
konstrukteure: 
- "Gürol Dümrol"
fotografen:
- "Gürol Dümrol"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "guerol"
license: "unknown"
legacy_id:
- details/25586
imported:
- "2019"
_4images_image_id: "25586"
_4images_cat_id: "1798"
_4images_user_id: "891"
_4images_image_date: "2009-10-30T21:05:04"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25586 -->
Hier ist der erste Entwurf. Die acht Federn waren (leider) zu schwach für 1,45 kg Eigengewicht. Wie man oben sieht, hat das Fahrzeug fast Bodenkontakt.