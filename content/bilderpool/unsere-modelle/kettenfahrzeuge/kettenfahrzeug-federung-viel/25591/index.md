---
layout: "image"
title: "Kraft an die Ketten"
date: "2009-10-30T21:05:04"
picture: "kettenfahrzeugmitfederungundvielbodenfreiheit7.jpg"
weight: "7"
konstrukteure: 
- "Gürol Dümrol"
fotografen:
- "Gürol Dümrol"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "guerol"
license: "unknown"
legacy_id:
- details/25591
imported:
- "2019"
_4images_image_id: "25591"
_4images_cat_id: "1798"
_4images_user_id: "891"
_4images_image_date: "2009-10-30T21:05:04"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25591 -->
Die Kraftübertragung hat mir am Ende doch mehr Kopfschmerzen zubereitet, als ich erwartet habe. Mein erster Entwurf (oben) lief zwar ohne Last prima, aber mit Last überhaupt nicht. :-(
Dann habe ich aber den passenden Forumsartikel dazu gefunden (http://www.fischertechnik.de/de/fanclub/forum/default.aspx?g=posts&t=2077).
Anschließend war die Lösung nicht mehr fern.