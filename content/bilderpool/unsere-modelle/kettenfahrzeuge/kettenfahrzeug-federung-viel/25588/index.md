---
layout: "image"
title: "Gewicht sparen"
date: "2009-10-30T21:05:04"
picture: "kettenfahrzeugmitfederungundvielbodenfreiheit4.jpg"
weight: "4"
konstrukteure: 
- "Gürol Dümrol"
fotografen:
- "Gürol Dümrol"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "guerol"
license: "unknown"
legacy_id:
- details/25588
imported:
- "2019"
_4images_image_id: "25588"
_4images_cat_id: "1798"
_4images_user_id: "891"
_4images_image_date: "2009-10-30T21:05:04"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25588 -->
Es ist ja nicht so, daß ich nicht versucht hätte Gewicht zu sparen. Leider musste ich einiges davon (aus Stabilitätsgründen) wieder rückgängig machen.