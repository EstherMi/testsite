---
layout: "image"
title: "Bodenfreiheit pur"
date: "2009-10-30T21:05:04"
picture: "kettenfahrzeugmitfederungundvielbodenfreiheit5.jpg"
weight: "5"
konstrukteure: 
- "Gürol Dümrol"
fotografen:
- "Gürol Dümrol"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "guerol"
license: "unknown"
legacy_id:
- details/25589
imported:
- "2019"
_4images_image_id: "25589"
_4images_cat_id: "1798"
_4images_user_id: "891"
_4images_image_date: "2009-10-30T21:05:04"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25589 -->
Wie man (an dem Messbalken sowie an dem ft-Mann) sieht, beträgt die Bodenfreiheit rekordverdächtige 8,5 cm. (Lediglich einige Teile der Getriebe liegen etwas tiefer.) Da ich damit nicht in die Wildnis ziehen will, sind die Antriebszahnräder nicht ummantelt. Wer damit in die Natur will, kann das ja gerne nachholen. ;-)