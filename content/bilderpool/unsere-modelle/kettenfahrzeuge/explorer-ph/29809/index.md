---
layout: "image"
title: "Minimodel"
date: "2011-01-26T15:11:17"
picture: "explorer08.jpg"
weight: "8"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- details/29809
imported:
- "2019"
_4images_image_id: "29809"
_4images_cat_id: "2193"
_4images_user_id: "1275"
_4images_image_date: "2011-01-26T15:11:17"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29809 -->
Hat alle funktionen des Roboters