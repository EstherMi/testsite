---
layout: "comment"
hidden: true
title: "19724"
date: "2014-11-23T18:21:21"
uploadBy:
- "NBGer"
license: "unknown"
imported:
- "2019"
---
Hallo Harald,
hab jetzt gerade eben erst Deine Getriebevariante entdeckt. Sieht sehr kompakt aus. Schön, daß du es so flexibel für unterschiedliche Einbau-Anforderungen durchdacht hast.
Es freut mich sehr, daß jemand meine Idee aufgegriffen hat! Gbt es denn schon ein fertiges Modell zu bestaunen?
Leider habe ich momentan gar keine Zeit für fischertechnik. Habe sowohl eine Idee für einen Pistenbully als auch für einen Panzer im Kopf, natürlich mit weiteren technischen Lösungen, die wahrscheinlich neu sind. Aber mangels Zeit habe ich bisher noch nichts in die Realität umsetzen können (außer ein paar Versuchsaufbauten).