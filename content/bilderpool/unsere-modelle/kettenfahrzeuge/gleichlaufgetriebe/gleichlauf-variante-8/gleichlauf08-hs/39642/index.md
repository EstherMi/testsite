---
layout: "image"
title: "Gleichlauf08_6"
date: "2014-10-04T16:26:16"
picture: "gleichlaufhs6.jpg"
weight: "6"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/39642
imported:
- "2019"
_4images_image_id: "39642"
_4images_cat_id: "2967"
_4images_user_id: "4"
_4images_image_date: "2014-10-04T16:26:16"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39642 -->
Variante 1x15°, zusammengefaltet und von der Ausgangsseite gesehen. Der "gegegsinnige" Motor sitzt 7,5 mm weiter außen, daher die Unterbrechung der Geometrie.