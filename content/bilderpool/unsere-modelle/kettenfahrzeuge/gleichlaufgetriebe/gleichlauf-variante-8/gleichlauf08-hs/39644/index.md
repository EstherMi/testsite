---
layout: "image"
title: "Gleichlauf08_8"
date: "2014-10-04T16:26:16"
picture: "gleichlaufhs8.jpg"
weight: "8"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/39644
imported:
- "2019"
_4images_image_id: "39644"
_4images_cat_id: "2967"
_4images_user_id: "4"
_4images_image_date: "2014-10-04T16:26:16"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39644 -->
In der Variante 0x15° liegt der "gleichsinnige" Motor (hier verdeckt) direkt am Mittelteil an. Deshalb kann er direkt (ohne Kette), das Z20 unten mitte antreiben.