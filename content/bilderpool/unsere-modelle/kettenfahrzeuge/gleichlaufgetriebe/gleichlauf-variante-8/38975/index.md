---
layout: "image"
title: "Gleichlaufgetriebe motorisiert"
date: "2014-06-23T18:54:09"
picture: "gleichlaufvariante4.jpg"
weight: "4"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38975
imported:
- "2019"
_4images_image_id: "38975"
_4images_cat_id: "2917"
_4images_user_id: "1729"
_4images_image_date: "2014-06-23T18:54:09"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38975 -->
Hier habe ich den Aufbau vervollständigt, um das Getriebe zu testen,
Für den Hauptantrieb verwende ich 2 Motoren 20:1. Der Panzer soll zügig unterwegs sein (im Vergleich ein Kampfpanzer Leopard hat eine Höchstgeschwindigkeit bis ca. 70 km/h)
Diese sind direkt mit einem Achsverbinder mit dem Gleichlaufgetriebe verbunden. Durch die beiden Z20 laufen die Motoren immer gleich schnell.
Der Lenkmotor (rot) hat eine Übersetzung 50:1 und ist der Einfachheit halber über Kardangelenke angeflanscht. 
Durch die Übersetzung laufen beim reinen Lenken die Ketten also deutlich langsamer, denn man benötigt ziemlich viel Drehmoment, um einen Panzer auf der Stelle drehen zu können.

Der Aufbau wird auch der Kern meiner Panzerwanne sein. Durch die Lage der Motoren ist der Antriebsstrang sehr schmal und auch sehr niedrig. In der Gesamtlänge sollte ich den nötigen Platz haben.
Je nach Anwendungsfall kann man auch andere Anordnungen wählen:
- Bei genügend Platz in der Höhe die Motoren übereinander bauen
- Bei genügend Platz in der Breite mit zusätzlichen Zahnrädern die Antriebsmotoren neben dem Getriebe platzieren
- Auf den zweiten Antriebsmotor verzichten und die beiden Motoren für Lenkung und Antrieb nebeneinander platzieren.

Antrieb und Lenkung liegen übrigens auf dem linken Knüppel der Fernsteuerung. Der rechte Knüppel wäre noch frei zum Rotieren des Turms.