---
layout: "image"
title: "Funktionsweise Gleichlaufgetriebe"
date: "2014-06-23T18:54:09"
picture: "gleichlaufvariante3.jpg"
weight: "3"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: ["Gleichlaufgetriebe", "Überlagerungslenkgetriebe", "Panzer", "Lenkung", "Kettenantrieb"]
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38974
imported:
- "2019"
_4images_image_id: "38974"
_4images_cat_id: "2917"
_4images_user_id: "1729"
_4images_image_date: "2014-06-23T18:54:09"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38974 -->
Die Ketten rechts und links werden über die Kegelzahnräder angetrieben.
Der Hauptantrieb für Vorwärts/Rückwärtsfahrt erfolgt durch die Achsen Antrieb (A) oder (B). Man kann auch zwei gleiche Motoren an beide Antriebsachsen anschliessen, um das Drehmoment zu erhöhen.
Die Lenkungsüberlagerung erfolgt über die mittlere Achse. 

Funktionsweise:
- Steht die mittlere Achse still, so fährt man über die Hauptantriebsachsen nur Vorwärts und Rückwärts. Die Ketten laufen dabei synchron, es ist also eine exakte Geradeausfahrt
- Stehen die Antriebsachsen (A und B) still, dann wird das Raupenfahrzeug im Stand gelenkt. Eine Raupe fährt rückwärts, die andere vorwärts, je nach Geschwindigkeit und Drehrichtung des Lenkmotors
-  Dreht der Lenkmotor gleichzeitig mit den Antriebsachsen, so wird dem Antrieb eine Lenkung überlagert. Die beiden Ketten laufen dann mit unterschiedlichen Geschwindigkeiten, was je nach Verhältnis der Geschwindigkeiten der Antriebsachse und Lenkachse zueinander zu unterschiedlichen Lenkradien führt.