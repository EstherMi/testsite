---
layout: "overview"
title: "Gleichlauf (Variante 8)"
date: 2019-12-17T19:45:49+01:00
legacy_id:
- categories/2917
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2917 --> 
Aktuell will ich einen Panzer bauen. Deswegen habe ich mich mit Gleichlaufgetrieben beschäftigt.
Im Übrigen hab ich erst mit Fischertechnik bzw. durch die Postings hier in der Community über die Existenz und Funktionsweise von Gleichlaufgetrieben erfahren.
Vorher war ich ein Ahnungsloser! ;-)
Komischerweise scheint der Begriff "Gleichlaufgetriebe" nicht so verbreitet zu sein und wird hauptsächlich hier in der Community verwendet. 
Allgemein üblich ist wohl eher der Begriff "Überlagerungslenkgetriebe" z.B. bei http://de.wikipedia.org/wiki/%C3%9Cberlagerungslenkgetriebe

Die Lösungen, die hier schon zur Genüge vorgestellt wurden, sind alle sehr interessant, sind aber für mich nicht so sehr geeignet.
Entweder sie sind sehr komplex mit vielen Teilen aufgebaut, was auch viel Platz braucht und die Effektivität verschlechtert , oder sie passen von der Geometrie nicht in mein Vorhaben.

Nach einigem Überlegen bin ich auf eine Lösung gekommen, die ähnlich, aber doch ein bisschen anders funktioniert und sehr kompakt, mit wenigen Teilen und guter Kraftübertragung funktioniert.