---
layout: "image"
title: "Kettenfahrwerk mit Gleichlaufgetriebe (ft designer)"
date: "2010-12-30T13:19:23"
picture: "Gesamtansicht_Gleichlaufgetriebe_ft_designer_I.jpg"
weight: "8"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/29555
imported:
- "2019"
_4images_image_id: "29555"
_4images_cat_id: "2148"
_4images_user_id: "1126"
_4images_image_date: "2010-12-30T13:19:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29555 -->
Hier die Konstruktionsdarstellung aus dem ft designer. Zur Vereinfachung des Nachbaus habe ich es in vier Baugruppen gegliedert; bei Interesse stelle ich die Datei gerne zum Download bereit.