---
layout: "image"
title: "Steuerprogramm Wall-e"
date: "2011-03-27T14:58:48"
picture: "Steuerprogramm_Wall-e.jpg"
weight: "5"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/30333
imported:
- "2019"
_4images_image_id: "30333"
_4images_cat_id: "2148"
_4images_user_id: "1126"
_4images_image_date: "2011-03-27T14:58:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30333 -->
Hier das (zugegeben etwas simple) Robo-Pro-Steuerprogramm. Nähert sich Wall-e einem Hindernis auf 20 cm, geht die Sirene an und er dreht sich auf der Stelle, bis kein Hindernis mehr "sichtbar" ist. Dann gibt er wieder Vollgas. Mit leichten Veränderungen ließe sich daraus ein Labyrinth-Roboter bauen...

Praktische Tests haben gezeigt, dass Wall-e sich in realen Wohnsituationen gelegentlich in Sackgassen manövriert - da wäre eine "Option zur Frontbegradigung" (Rückwärtsgang) hilfreich. Auch ist der vom Utraschall-Sensor ausgeleuchtete Bereich etwas schmal - ein bisserl weniger "Scheuklappenblick" würde ihm ebenfalls gut tun.