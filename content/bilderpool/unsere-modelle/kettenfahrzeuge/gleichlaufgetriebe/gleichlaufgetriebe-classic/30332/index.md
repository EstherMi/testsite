---
layout: "image"
title: "Wall-e mit Selbststeuerung"
date: "2011-03-27T14:58:48"
picture: "Wall-e_mit_Steuerung.jpg"
weight: "4"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/30332
imported:
- "2019"
_4images_image_id: "30332"
_4images_cat_id: "2148"
_4images_user_id: "1126"
_4images_image_date: "2011-03-27T14:58:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30332 -->
Jetzt gibt es das Raupenfahrzeug auch mit Selbststeuerung: Der Ultraschallsensor erkennt Hindernisse und dreht den kleinen Wall-e dann, bis der Weg frei ist.
Zur großen Begeisterung der jüngeren (und geringeren der älteren) Familienmitglieder habe ich ihm noch ein Sounds-and-Lights-Modul spendiert, das das Blinklicht steuert und mit wechselnden Alarmsignalen auf plötzlich auftretende Hindernisse reagiert.