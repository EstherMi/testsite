---
layout: "comment"
hidden: true
title: "12998"
date: "2010-12-22T07:50:25"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Ein großartiges Funktionsmodell, klasse!!!

Es ist mir eine Ehre, dass mein "Geteiltes Gleichlaufgetriebe" der Pistenraupe von 2007 weiterentwickelt bzw. in diesem Fall in die graue Ära "zurückentwickelt" wurde:

http://www.ftcommunity.de/details.php?image_id=12243#col3

Ich bin gespannt auf das fertige Kettenfahrzeug!

Gruß, Thomas