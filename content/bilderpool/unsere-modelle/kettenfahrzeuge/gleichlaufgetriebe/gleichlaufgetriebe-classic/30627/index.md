---
layout: "image"
title: "Wall-e 2.0 mit neuen Differentialen (ohne Motor)"
date: "2011-05-23T19:39:54"
picture: "Raupenfahrzeug_mit_Gleichlaufgetriebe_neue_Differentiale_ohne_Motor.jpg"
weight: "1"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/30627
imported:
- "2019"
_4images_image_id: "30627"
_4images_cat_id: "2148"
_4images_user_id: "1126"
_4images_image_date: "2011-05-23T19:39:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30627 -->
Hier ist Wall-e 2.0: Da die M-Motoren mit Schneckengetriebe dazu neigten, gelegentlich zu verrutschen und "durchzudrehen", und die Kette in unfreundlicher Umgebung Raupenglieder verlor, wurde ein Upgrade auf 2.0 erforderlich. Dazu ersetzte ich die "klassischen" Differentiale durch neue und die roten durch schwarze Raupenketten (darauf halten die Raupenglieder bombig).