---
layout: "comment"
hidden: true
title: "13100"
date: "2011-01-04T18:08:52"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Freut mich, dass der kleine Wall-E Euch gefällt...

@Endlich: Stimmt, das dürfte funktionieren - aber beim nächsten Versuch, das Hindernis zu bewältigen, wird er wieder kippen. Daher ist es vermutlich geschickter, den Schwerpunkt möglichst tief zu legen (z.B. den Akku und die Motoren horizontal zu montieren - das habe ich nur nicht stabil genug hinbekommen).

Gruß, Dirk