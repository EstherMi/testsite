---
layout: "comment"
hidden: true
title: "14028"
date: "2011-04-04T18:53:18"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Au weia! 
Ich bin mir jetzt ziemlich sicher, dass die Forumssoftware hin und wieder Texte verliert! Ich hatte das vor einiger Zeit schon mal geschrieben, aber jetzt ist es weg: Das Getriebe hier hat sich nach Fertigstellung als unbrauchbar erwiesen. Man kann zwar damit fahren, aber ... es ist kein Gleichlaufgetriebe. Beide Motoren arbeiten gleichsinnig auf die Antriebe und Kurvenfahren "ist nicht" :-((

Anstelle der beiden BS30 gehörte da eine Richtungsumkehr zwischen die beiden Z10, dann wäre die Welt wieder in Ordnung.

bedauernde Grüße,
Harald