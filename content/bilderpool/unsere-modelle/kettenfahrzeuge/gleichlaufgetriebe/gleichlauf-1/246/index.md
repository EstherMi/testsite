---
layout: "image"
title: "kette01"
date: "2003-04-21T21:14:41"
picture: "kette01.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/246
imported:
- "2019"
_4images_image_id: "246"
_4images_cat_id: "616"
_4images_user_id: "4"
_4images_image_date: "2003-04-21T21:14:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=246 -->
