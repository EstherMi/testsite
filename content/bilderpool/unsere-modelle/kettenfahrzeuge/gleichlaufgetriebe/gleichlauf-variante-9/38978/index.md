---
layout: "image"
title: "3 Gang Schaltgetriebe"
date: "2014-06-26T22:46:49"
picture: "gleichlaufvariante3.jpg"
weight: "4"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38978
imported:
- "2019"
_4images_image_id: "38978"
_4images_cat_id: "2918"
_4images_user_id: "1729"
_4images_image_date: "2014-06-26T22:46:49"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38978 -->
Ein einfaches 3-Gang Schaltgetriebe.
Verwendet werden Z10, Z15 und Z20. Um zu schalten, werden die Achsen zueinander verschoben.
Es ergeben sich die Übersetzungsverhältnisse 1:2, 1:1 und 2:1. 
Der Bereich ist nicht allzu groß, ist aber dem fischertechnik angemessen. Größere Übersetzungsverhältnisse überfordern entweder die Mechanik oder den Motor.
Der Abstand der Zahnräder ist so gewählt, daß sich zwischen den Gängen immer ein kleiner Leerlauf-Bereich ergibt. Damit blockiert das Getriebe beim Schalten nicht!
 
Was hat jetzt ein Schaltgetriebe im Zusammenhang hier zu suchen?
Die Erklärung erfolgt im nächsten Bild.....