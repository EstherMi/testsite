---
layout: "comment"
hidden: true
title: "19214"
date: "2014-07-07T13:06:35"
uploadBy:
- "NBGer"
license: "unknown"
imported:
- "2019"
---
Hallo Harald,
ich hab mal folgendes gelesen (ich bin mir aber nicht mehr sicher, ob das für den Leopard galt oder für einen anderen Panzer wie den M1 Abrams):

- es gibt einen Hauptmotor
- dieser arbeitet über ein Sfufen/Schaltgetriebe als Hauptantrieb für beide Ketten
- der Hauptantrieb treibt aber auch eine Hydraulikpumpe an
- Ein Hydraulikmotor fungiert als Lenkantrieb. (also quasi ein zweiter Motor)
- Der Lenkantrieb wird dann dem Hauptantrieb über das Überlagerungslenkgetriebe überlagert
- Die Kraft des Lenkmotors und damit die Lenkgeschwindigkeit hängt vom Öldruck der Pumpe und damit von der Drehzahl des Hauptantriebes ab

Das scheint das Konzept in modernen Panzern zu sein.

Grüße
Richard