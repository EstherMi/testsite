---
layout: "image"
title: "Gleichlauf05-02.JPG"
date: "2006-05-05T19:14:02"
picture: "Gleichlauf05-02.jpg"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/6203
imported:
- "2019"
_4images_image_id: "6203"
_4images_cat_id: "620"
_4images_user_id: "4"
_4images_image_date: "2006-05-05T19:14:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6203 -->
Die Achsen, über die die Ketten laufen, müssen noch stabil auf Abstand gehalten werden, sonst ratterts und die Kette springt über.