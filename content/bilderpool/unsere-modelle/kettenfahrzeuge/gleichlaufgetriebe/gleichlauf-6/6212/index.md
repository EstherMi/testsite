---
layout: "image"
title: "Gleichlauf06-07.JPG"
date: "2006-05-05T19:28:34"
picture: "Gleichlauf06-07.jpg"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/6212
imported:
- "2019"
_4images_image_id: "6212"
_4images_cat_id: "621"
_4images_user_id: "4"
_4images_image_date: "2006-05-05T19:28:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6212 -->
