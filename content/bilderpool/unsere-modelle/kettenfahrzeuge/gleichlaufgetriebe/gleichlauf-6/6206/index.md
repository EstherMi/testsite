---
layout: "image"
title: "Gleichlauf06-01.JPG"
date: "2006-05-05T19:20:53"
picture: "Gleichlauf06-01.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/6206
imported:
- "2019"
_4images_image_id: "6206"
_4images_cat_id: "621"
_4images_user_id: "4"
_4images_image_date: "2006-05-05T19:20:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6206 -->
Hier noch eine andere Variante. Ohne "Teile-Modding" (c) geht's auch hier nicht:
- Das Z15 muss mit der Achse verstiftet oder verklebt werden.
- Die rote Schnecke ebenfalls.
- Die S-Riegel zur Befestigung der Laschen 23,2 zwischen den Motoren sind oben und unten abgeflacht. Am Griff (oben) hätte auch weniger Schnippelei ausgereicht, aber die Teile hab ich nun mal von früheren Projekten so da.