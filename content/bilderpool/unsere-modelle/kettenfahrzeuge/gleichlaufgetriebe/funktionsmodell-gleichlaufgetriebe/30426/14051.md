---
layout: "comment"
hidden: true
title: "14051"
date: "2011-04-10T22:37:09"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hallo Harald,

das allerbeste Licht ist immer wieder Sonnenlicht - das schränkt zwar die Modell-Ablicht-Zeiten ein wenig ein, ist aber unschlagbar (wenn man beim Schatten ein bisserl aufpasst). Mit Stativ stimmt dann auch die (Tiefen-)Schärfe...

Das Funktionsmodell diente so erstmal (selbst-)didaktischen Zwecken und ist motiviert durch einen (lesenswerten!) Text von W.E. Johns über die Steuerung von Raupengetrieben (http://www.gizmology.net/tracked.htm). Verstehen geht bei mir manchmal einfacher über die Finger ;-)

Ich baue es gerade für einen Schaufelradantrieb um - da ist, wie ich feststellen muss, sowohl die Anordnung der Motoren, als auch die stabile Konstruktion äußerst trickreich... Mein Respekt an alle Raupenantriebbastler hier... 
Wenn ich es hinbekomme, folgen Fotos.

Gruß,
Dirk