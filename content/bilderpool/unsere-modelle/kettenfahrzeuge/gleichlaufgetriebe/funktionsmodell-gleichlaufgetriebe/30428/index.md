---
layout: "image"
title: "Funktionsmodell Gleichlaufgetriebe - Lenkung"
date: "2011-04-04T00:13:27"
picture: "funktionsmodellgleichlaufgetriebe3.jpg"
weight: "4"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/30428
imported:
- "2019"
_4images_image_id: "30428"
_4images_cat_id: "2262"
_4images_user_id: "1126"
_4images_image_date: "2011-04-04T00:13:27"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30428 -->
Diese Kurbel überlagert die Lenkung.