---
layout: "image"
title: "Gleichlauf04-03.JPG"
date: "2005-10-31T20:56:43"
picture: "Gleichlauf04-03.jpg"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Sigma-Delta", "Gleichlauf"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5166
imported:
- "2019"
_4images_image_id: "5166"
_4images_cat_id: "619"
_4images_user_id: "4"
_4images_image_date: "2005-10-31T20:56:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5166 -->
Nochmal das gleiche, mit dem Lenk-Motor oben.

Das ganze Getriebe schluckt (je nach Einbaulage) nur 60 bis 75 mm Baulänge, fordert aber dafür ein Chassis mit ziemlicher Breite. Hierzu siehe Kategorie http://www.ftcommunity.de/categories.php?cat_id=499