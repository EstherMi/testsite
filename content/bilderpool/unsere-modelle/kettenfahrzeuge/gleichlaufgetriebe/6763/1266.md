---
layout: "comment"
hidden: true
title: "1266"
date: "2006-08-31T23:49:55"
uploadBy:
- "remadus"
license: "unknown"
imported:
- "2019"
---
Wenn ich das Getriebe richtig verstehe, dann macht ein Motor den Vortrieb und ein Motor überlagert eine Richtungsänderung.

Was soll ich sagen: Maschinenbau reinsten Wassers.