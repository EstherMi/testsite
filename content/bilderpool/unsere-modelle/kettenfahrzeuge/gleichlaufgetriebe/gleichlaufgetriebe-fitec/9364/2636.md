---
layout: "comment"
hidden: true
title: "2636"
date: "2007-03-10T19:18:09"
uploadBy:
- "wahsager"
license: "unknown"
imported:
- "2019"
---
Das Fahrzeug dreht aber nicht exakt auf der Stelle, wenn nur der Lenkmotor läuft -- man muss mischen, richtig? 

Ansonsten eine sehr kompakte Kontruktion, gefällt mir.