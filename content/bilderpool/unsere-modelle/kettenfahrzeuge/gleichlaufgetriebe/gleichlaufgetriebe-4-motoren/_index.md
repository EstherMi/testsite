---
layout: "overview"
title: "Gleichlaufgetriebe mit 4 Motoren"
date: 2019-12-17T19:45:44+01:00
legacy_id:
- categories/1623
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1623 --> 
Das ganze soll noch in ein Kettenfahrwerk eingebaut werden. Durch das Differenzial zwischen 2 Motoren können diese sich nicht gegenseitig blockieren.