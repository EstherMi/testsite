---
layout: "image"
title: "Kettenfahrzeug"
date: "2007-03-24T15:06:00"
picture: "Kettenfahrwerk21.jpg"
weight: "1"
konstrukteure: 
- "Umgebaute Federung: fitec /Rest: StefanL"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/9778
imported:
- "2019"
_4images_image_id: "9778"
_4images_cat_id: "879"
_4images_user_id: "456"
_4images_image_date: "2007-03-24T15:06:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9778 -->
Das ist das Kettenfahrzeug von StefanL nur mit viel mehr Bodenabstand, weil ich gerne auf Rasen und Erde fahre. Jetzt fragt man sich wieso der fitec das einfach nachbaut. Weil ich keine bessere Lösung gefunden habe und weil ich die Federung super finde.