---
layout: "overview"
title: "Kettenfahrzeug mit Federung (fitec)"
date: 2019-12-17T19:46:48+01:00
legacy_id:
- categories/879
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=879 --> 
Dieses Kettenfahrzeug hat eine Federung. Ich habe hauptsächlich von der Federung Bilder gemacht, weil das Kettenfahrzeug dem von StefanL sehr ähnelt. Das einzig andere ist, dass die Federung so gebaut wurde dass das Kettenfahrzeug mehr Bodenabstand hat.