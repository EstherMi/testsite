---
layout: "image"
title: "KF 3.8"
date: "2008-12-30T18:22:36"
picture: "IMG_2909.jpg"
weight: "10"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- details/16811
imported:
- "2019"
_4images_image_id: "16811"
_4images_cat_id: "1502"
_4images_user_id: "859"
_4images_image_date: "2008-12-30T18:22:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16811 -->
Zusätzliche innere Verstrebungen.