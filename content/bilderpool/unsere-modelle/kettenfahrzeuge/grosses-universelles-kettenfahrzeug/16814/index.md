---
layout: "image"
title: "KF 3.11"
date: "2008-12-30T19:18:00"
picture: "IMG_2931.jpg"
weight: "13"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- details/16814
imported:
- "2019"
_4images_image_id: "16814"
_4images_cat_id: "1502"
_4images_user_id: "859"
_4images_image_date: "2008-12-30T19:18:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16814 -->
Die ersten drei kugelgelagerten Achsen. Mit je drei Kugellagern. Vorgesehen für die unteren Laufräder. Bei insgesamt 20 Achsen im Fahrwerk erhoffe ich mir eine Reduzierung der Reibung von min. 50% unter Verwendung von Kugellagern.