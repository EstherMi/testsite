---
layout: "image"
title: "KF 2.5"
date: "2008-12-12T22:54:12"
picture: "IMG_2808.jpg"
weight: "20"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- details/16590
imported:
- "2019"
_4images_image_id: "16590"
_4images_cat_id: "1502"
_4images_user_id: "859"
_4images_image_date: "2008-12-12T22:54:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16590 -->
Da ich mit ca.10Kg Gewicht rechne, habe ich die Federung etwas straffer gestaltet. Zwei Kunststofffedern und ein Federgelenkstein federn ab. Der innere Gelenkstein ist ohne Feder.