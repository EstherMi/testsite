---
layout: "image"
title: "KF 2.2"
date: "2008-12-12T22:54:12"
picture: "IMG_2787.jpg"
weight: "17"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- details/16586
imported:
- "2019"
_4images_image_id: "16586"
_4images_cat_id: "1502"
_4images_user_id: "859"
_4images_image_date: "2008-12-12T22:54:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16586 -->
Als obere Laufrollen dient dieses Modell und paßt perfekt.