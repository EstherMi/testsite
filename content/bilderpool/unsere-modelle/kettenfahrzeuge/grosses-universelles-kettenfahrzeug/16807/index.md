---
layout: "image"
title: "KF 3.4"
date: "2008-12-30T18:22:36"
picture: "IMG_2924.jpg"
weight: "6"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- details/16807
imported:
- "2019"
_4images_image_id: "16807"
_4images_cat_id: "1502"
_4images_user_id: "859"
_4images_image_date: "2008-12-30T18:22:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16807 -->
Die Revision 3 in der Seitenansicht. Angetrieben wird das Vehikel durch vier Power-Motoren mit 50:1 Übersetzung. Was die Festigkeit der Naben angeht, so halte ich Sägen und Bohren bei einer harten, 4 mm dicken Stahlstange für ein verletzungsträchtiges Unterfangen. Da bin ich eher für's Körnen.