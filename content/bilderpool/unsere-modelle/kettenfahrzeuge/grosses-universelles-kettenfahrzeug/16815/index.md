---
layout: "image"
title: "KF 3.12"
date: "2008-12-30T19:18:00"
picture: "IMG_2932.jpg"
weight: "14"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- details/16815
imported:
- "2019"
_4images_image_id: "16815"
_4images_cat_id: "1502"
_4images_user_id: "859"
_4images_image_date: "2008-12-30T19:18:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16815 -->
Mit drei Motoren fährt es schon. Für den vierten fehlen mir mal wieder Bauteile. Allerdings stellt ein dünnes Heftchen schon eine Schwierigkeit dar. Dabei blockieren aber nicht die Motoren, sondern die Achsen drehen halt durch.