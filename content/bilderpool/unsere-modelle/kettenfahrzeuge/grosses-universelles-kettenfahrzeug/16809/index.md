---
layout: "image"
title: "KF 3.6"
date: "2008-12-30T18:22:36"
picture: "IMG_2927.jpg"
weight: "8"
konstrukteure: 
- "michael stumberger"
fotografen:
- "michael stumberger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mikelsofast"
license: "unknown"
legacy_id:
- details/16809
imported:
- "2019"
_4images_image_id: "16809"
_4images_cat_id: "1502"
_4images_user_id: "859"
_4images_image_date: "2008-12-30T18:22:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16809 -->
Da ich nicht nur gerne frickl, sondern noch dazu ein Computer-Fuzzi bin, habe ich doppelten Spaß - das Ding hinterher auch noch zu programmieren. Geplant ist eine möglichst autonome Einheit, die aber wahlweise auch ferngesteuert werden können soll.