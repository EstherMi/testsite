---
layout: "image"
title: "Pneumatischer Greifer"
date: "2008-03-11T15:40:57"
picture: "raupenfahrzeugmitpneumatischemgreiferfer5.jpg"
weight: "7"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- details/13898
imported:
- "2019"
_4images_image_id: "13898"
_4images_cat_id: "1275"
_4images_user_id: "731"
_4images_image_date: "2008-03-11T15:40:57"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13898 -->
Den Greifer hab ich vom Profi Pneumatik-Kasten übernommen