---
layout: "image"
title: "Raupenfahrzeug1"
date: "2008-03-11T15:40:56"
picture: "raupenfahrzeugmitpneumatischemgreiferfer1.jpg"
weight: "3"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- details/13894
imported:
- "2019"
_4images_image_id: "13894"
_4images_cat_id: "1275"
_4images_user_id: "731"
_4images_image_date: "2008-03-11T15:40:56"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13894 -->
Dies ist die Konstruktion eines ziemlich einfachen, aber durchaus wirkungsvollen und gut einsetzbaren Raupenfahrzeugs mit einem pneumatischen Greifer. Der Kompressor ist an Bord und der Antrieb wird per IR-Control-Set ferngesteuert.