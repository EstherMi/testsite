---
layout: "overview"
title: "Rasenmäher Kettwiesl"
date: 2019-12-17T19:47:46+01:00
legacy_id:
- categories/2696
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2696 --> 
Der erste funktionsfähige Rasenmäher (fast) nur aus ft -Teilen. Gebaut als Hilfe für Mäharbeiten in steilem Gelände.
 - Kettenfahrwerk voll gefedert
 - automatische Kettenspanner
 - höhenverstellbares Mähwerk (Schneidteller)