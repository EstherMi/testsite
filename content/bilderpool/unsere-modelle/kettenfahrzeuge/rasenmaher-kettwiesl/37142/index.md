---
layout: "image"
title: "Kettwiesl 2013 - Bodenfreiheit"
date: "2013-07-06T16:01:17"
picture: "P1010288.jpg"
weight: "4"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["Rasenmäher", "Kettwiesl", "Mähwerk", "Roboter", "Kettenfahrwerk"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/37142
imported:
- "2019"
_4images_image_id: "37142"
_4images_cat_id: "2696"
_4images_user_id: "1557"
_4images_image_date: "2013-07-06T16:01:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37142 -->
Satte 75mm Bodenfreiheit. Im Vergleich zur alten Konstruktion ist der jetzt noch etwa 20mm höher.

Und außerdem auch 30mm breiter; das hängt mit den Steckanschlüssen der Powermotoren zusammen. Da war früher die linke Kette haargenau im Weg.