---
layout: "image"
title: "Kettwiesl 2013 - Federbeine"
date: "2013-07-06T16:01:17"
picture: "P1010270.jpg"
weight: "9"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["Rasenmäher", "Kettwiesl", "Mähwerk", "Roboter", "Kettenfahrwerk"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/37147
imported:
- "2019"
_4images_image_id: "37147"
_4images_cat_id: "2696"
_4images_user_id: "1557"
_4images_image_date: "2013-07-06T16:01:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37147 -->
Ja, die neuen Federbeine. Jetzt verklemmt sich nichts mehr, die federn schön ein und aus. Ich bin echt zufrieden. Die Federn stammen von der Fa. Gutekunst (www.federnshop.com) und bieten die notwendigen Kenndaten für die fertige Maschine. Der Federweg ist etwa 22mm von unbelastet bis an den Anschlag. ft-Federn sind leider zu "weich" für die Anwendung. Das mittlere Federbein ist absichtlich etwa 5mm länger gewählt (die beiden Klemmbuchsen), das verbessert die Kurvenfahrt.

Auf dem Bild sind die Federbeine übrigens fast nicht eingefedert. Wenn alles montiert ist, schätze ich auf etwa 5mm bis 7mm Einfederung.