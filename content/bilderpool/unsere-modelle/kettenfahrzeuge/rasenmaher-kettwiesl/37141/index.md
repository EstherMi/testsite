---
layout: "image"
title: "Kettwiesl 2013 - etwas anderer Sichtwinkel"
date: "2013-07-06T16:01:17"
picture: "P1010287.jpg"
weight: "3"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["Rasenmäher", "Kettwiesl", "Mähwerk", "Roboter", "Kettenfahrwerk"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/37141
imported:
- "2019"
_4images_image_id: "37141"
_4images_cat_id: "2696"
_4images_user_id: "1557"
_4images_image_date: "2013-07-06T16:01:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37141 -->
Die Aufhängung des Mähwerks ist jetzt - bei gleicher Hubhöhe - flacher geraten, der Antriebsmotor (für die Höheneinstellung des Mähwerks) ist gut versteckt.