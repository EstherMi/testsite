---
layout: "image"
title: "Kettwiesl 2013 - mittlere Laufrolle"
date: "2013-07-06T16:01:17"
picture: "P1010276.jpg"
weight: "12"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["Rasenmäher", "Kettwiesl", "Mähwerk", "Roboter", "Kettenfahrwerk"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/37150
imported:
- "2019"
_4images_image_id: "37150"
_4images_cat_id: "2696"
_4images_user_id: "1557"
_4images_image_date: "2013-07-06T16:01:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37150 -->
Hier ein Detail der mittleren Laufrolle. Die Befestigung der Federn gegen Herausfallen ist ebenso einfach wie effizient: eine kleiner O-Ring (Gummiring) wird über die Feder gezogen und dann die Feder mit dem Ring auf eine Metallachse gesteckt - hält.