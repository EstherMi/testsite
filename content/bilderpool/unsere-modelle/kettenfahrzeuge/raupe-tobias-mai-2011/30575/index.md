---
layout: "image"
title: "Raupe im 'Berchele'"
date: "2011-05-18T14:47:35"
picture: "raupe6.jpg"
weight: "6"
konstrukteure: 
- "Tobias"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/30575
imported:
- "2019"
_4images_image_id: "30575"
_4images_cat_id: "2278"
_4images_user_id: "1162"
_4images_image_date: "2011-05-18T14:47:35"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30575 -->
So, hier die Raupe in unserem "Berchele". Sie fährt nur ein paar Zentimeter und dann rutsch schon die Kette runter, weiß jemand da vielleicht eine Lösung?

MfG