---
layout: "image"
title: "Raupe im 'Berchele'"
date: "2011-05-18T14:47:35"
picture: "raupe5.jpg"
weight: "5"
konstrukteure: 
- "Tobias"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/30574
imported:
- "2019"
_4images_image_id: "30574"
_4images_cat_id: "2278"
_4images_user_id: "1162"
_4images_image_date: "2011-05-18T14:47:35"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30574 -->
