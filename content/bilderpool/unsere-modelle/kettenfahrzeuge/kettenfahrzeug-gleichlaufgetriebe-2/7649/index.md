---
layout: "image"
title: "Antrieb 1"
date: "2006-12-03T13:49:35"
picture: "kettenfahrzeugmitgleichlaufgetriebe05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/7649
imported:
- "2019"
_4images_image_id: "7649"
_4images_cat_id: "717"
_4images_user_id: "502"
_4images_image_date: "2006-12-03T13:49:35"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7649 -->
Übergang vom Gleilaufgetriebe zu den Ketten.