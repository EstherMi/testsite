---
layout: "image"
title: "Schnecke"
date: "2011-11-07T18:53:07"
picture: "bombenentschaerfungsroboter07.jpg"
weight: "7"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- details/33435
imported:
- "2019"
_4images_image_id: "33435"
_4images_cat_id: "2478"
_4images_user_id: "1275"
_4images_image_date: "2011-11-07T18:53:07"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33435 -->
..