---
layout: "image"
title: "Von Oben"
date: "2011-11-07T18:53:08"
picture: "bombenentschaerfungsroboter25.jpg"
weight: "25"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- details/33453
imported:
- "2019"
_4images_image_id: "33453"
_4images_cat_id: "2478"
_4images_user_id: "1275"
_4images_image_date: "2011-11-07T18:53:08"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33453 -->
..