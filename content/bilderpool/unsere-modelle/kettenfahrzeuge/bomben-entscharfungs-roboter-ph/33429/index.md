---
layout: "image"
title: "Gesamt"
date: "2011-11-07T18:53:07"
picture: "bombenentschaerfungsroboter01.jpg"
weight: "1"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- details/33429
imported:
- "2019"
_4images_image_id: "33429"
_4images_cat_id: "2478"
_4images_user_id: "1275"
_4images_image_date: "2011-11-07T18:53:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33429 -->
Mein "Bomben-Entschärfungs-Roboter" mit 3 TX´s auf dem Roboter und 1 Interface mit 2 Erwiterungen.