---
layout: "image"
title: "Rups-02"
date: "2015-06-26T19:36:39"
picture: "raupen02.jpg"
weight: "2"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/41280
imported:
- "2019"
_4images_image_id: "41280"
_4images_cat_id: "3088"
_4images_user_id: "2449"
_4images_image_date: "2015-06-26T19:36:39"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41280 -->
