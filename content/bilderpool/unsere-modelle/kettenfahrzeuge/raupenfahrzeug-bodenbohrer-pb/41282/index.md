---
layout: "image"
title: "Rups-04"
date: "2015-06-26T19:36:39"
picture: "raupen04.jpg"
weight: "4"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/41282
imported:
- "2019"
_4images_image_id: "41282"
_4images_cat_id: "3088"
_4images_user_id: "2449"
_4images_image_date: "2015-06-26T19:36:39"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41282 -->
