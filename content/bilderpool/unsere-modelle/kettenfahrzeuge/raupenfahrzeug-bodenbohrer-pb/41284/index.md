---
layout: "image"
title: "Rups-07"
date: "2015-06-26T19:36:39"
picture: "raupen06.jpg"
weight: "6"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/41284
imported:
- "2019"
_4images_image_id: "41284"
_4images_cat_id: "3088"
_4images_user_id: "2449"
_4images_image_date: "2015-06-26T19:36:39"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41284 -->
