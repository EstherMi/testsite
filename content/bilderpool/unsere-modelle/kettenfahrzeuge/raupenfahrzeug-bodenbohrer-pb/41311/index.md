---
layout: "image"
title: "Rups-34"
date: "2015-06-26T19:36:40"
picture: "raupen33.jpg"
weight: "33"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/41311
imported:
- "2019"
_4images_image_id: "41311"
_4images_cat_id: "3088"
_4images_user_id: "2449"
_4images_image_date: "2015-06-26T19:36:40"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41311 -->
