---
layout: "image"
title: "Rups-30"
date: "2015-06-26T19:36:40"
picture: "raupen29.jpg"
weight: "29"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/41307
imported:
- "2019"
_4images_image_id: "41307"
_4images_cat_id: "3088"
_4images_user_id: "2449"
_4images_image_date: "2015-06-26T19:36:40"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41307 -->
Aufbau des Greifer-teils.