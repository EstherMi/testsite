---
layout: "image"
title: "Rups-24"
date: "2015-06-26T19:36:40"
picture: "raupen23.jpg"
weight: "23"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/41301
imported:
- "2019"
_4images_image_id: "41301"
_4images_cat_id: "3088"
_4images_user_id: "2449"
_4images_image_date: "2015-06-26T19:36:40"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41301 -->
Obenseite