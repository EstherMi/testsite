---
layout: "image"
title: "Rups-40"
date: "2015-06-26T19:36:40"
picture: "raupen39.jpg"
weight: "39"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/41317
imported:
- "2019"
_4images_image_id: "41317"
_4images_cat_id: "3088"
_4images_user_id: "2449"
_4images_image_date: "2015-06-26T19:36:40"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41317 -->
Den Empfänger vom Greifer hat noch ein freie Ausgang. wenn Ich die benutzt hätte für die Drehscheibe, Hätte Ich hier noch eine Freie gehabt. Die hätte Ich vielleicht nutzen sollen für Wechselung oder Kippen von das Kästchen, aber Ich sah so schnell keine Methode dafür.