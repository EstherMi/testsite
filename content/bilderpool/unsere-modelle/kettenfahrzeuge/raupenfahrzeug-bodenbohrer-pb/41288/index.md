---
layout: "image"
title: "Rups-11"
date: "2015-06-26T19:36:39"
picture: "raupen10.jpg"
weight: "10"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/41288
imported:
- "2019"
_4images_image_id: "41288"
_4images_cat_id: "3088"
_4images_user_id: "2449"
_4images_image_date: "2015-06-26T19:36:39"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41288 -->
