---
layout: "image"
title: "Rups-10"
date: "2015-06-26T19:36:39"
picture: "raupen09.jpg"
weight: "9"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/41287
imported:
- "2019"
_4images_image_id: "41287"
_4images_cat_id: "3088"
_4images_user_id: "2449"
_4images_image_date: "2015-06-26T19:36:39"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41287 -->
Die Kleine Hülse fällt teilweise ins Rad hinein. So steht das Leitungs-Zahnrad Z15 auf gleichen Ebene mit den Z30's unter.