---
layout: "image"
title: "Geländewagen mit Kettenantrieb 4"
date: "2009-07-19T17:13:36"
picture: "gelaendewagenmitkettenantrieb04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/24588
imported:
- "2019"
_4images_image_id: "24588"
_4images_cat_id: "1691"
_4images_user_id: "502"
_4images_image_date: "2009-07-19T17:13:36"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24588 -->
