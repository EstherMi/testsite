---
layout: "comment"
hidden: true
title: "4325"
date: "2007-10-17T16:16:45"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Ohne Kegelzahnräder?!?

Nun ja, in die Welle zwischen dem Lenk-Differenzial und dem entsprechenden Kettenzahnrad gehört eine Drehrichtungsumkehr! Um das ganze Konzept mit der eingesparten einen Wellenebene nicht mit einer zusätzlichen Wellenebene zur Drehrichtungsumkehr zu zerstören, ist mir nur der Kegelradsatz eingefallen. Und der fuktioniert perfekt! Ohne Drehrichtungsumkehr funktioniert es natürlich nicht...

Was gefällt Dir an dieser Stelle nicht am Kegelradsatz?

Gruß, Thomas