---
layout: "image"
title: "Pistenraupe 8"
date: "2007-10-16T21:15:18"
picture: "Pistenraupe_10.jpg"
weight: "8"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/12242
imported:
- "2019"
_4images_image_id: "12242"
_4images_cat_id: "1094"
_4images_user_id: "328"
_4images_image_date: "2007-10-16T21:15:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12242 -->
Eine Kette im Detail.