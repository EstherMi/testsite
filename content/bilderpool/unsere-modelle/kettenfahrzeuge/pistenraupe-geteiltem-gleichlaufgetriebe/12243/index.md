---
layout: "image"
title: "Pistenraupe 9"
date: "2007-10-16T21:29:41"
picture: "Pistenraupe_11.jpg"
weight: "9"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: ["Gleichlaufgetriebe"]
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/12243
imported:
- "2019"
_4images_image_id: "12243"
_4images_cat_id: "1094"
_4images_user_id: "328"
_4images_image_date: "2007-10-16T21:29:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12243 -->
Hier nun des Rätsels Lösung:

Im Gegensatz zu einem konventionellen Gleichlaufgetriebe sind die beiden Differenzialachsen nicht direkt über Zahnräder miteinander verbunden, sondern über die beiden Antriebsketten! Auf jeder Seite ist ein Motor, der aber nicht die jeweilige Kette daneben antreibt, sondern jeweils ein Differenzial.

Der Aufbau pro Seite ist absolut symmetrisch. Nur in einer der beiden Differenzialachsen ist eine Kegelradsatz eingebaut, der als Drehrichtungsumkehr dient.

Das Ergebnis des "geteilten Gleichlaufgetriebes" ist wie bei einem konventionellem Gleichlaufgetriebe: Ein Motor treibt beide Ketten absolut symmetrisch an (der Motor im Bild unten), und ein Motor sorgt für das Lenken (im Bild oben).

Die Fahrzustände sind auch wie immer:

- absolut perfekte Geradeausfahrt
- Lenken um eine stehende Kette
- Lenken auf der Stelle

Die Vorteile gegenüber einem konventionellem Gleichlaufgetriebe liegen auf der Hand:

- extrem flacher Aufbau, da die Motoren nebeneinander angeordnet werden können
- man spart eine komplette Wellenebene, da die beiden Achswellen auch als Antrieb dienen

Nachteile:

- die Ketten müssen straff gespannt sein, damit der Gleichlauf funktioniert
- das Lenken ist nicht ganz so präsize wie bei einem konventionellem Gleichlaufgetriebe mit Zahnradübertragung

Klar, die beiden Mini-Moteren sind in schwerem Gelände nicht sooo der Hit, aber es funktioniert eigentlich besser als ich es erwartet hatte. Das Ganze lässt sich sicher auch wunderbar mit 2 Power-Motoren aufbauen. Ich habe leider gerade nur einen...