---
layout: "image"
title: "Pistenbully 2"
date: "2007-10-28T12:45:39"
picture: "pistenbully02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/12354
imported:
- "2019"
_4images_image_id: "12354"
_4images_cat_id: "1103"
_4images_user_id: "502"
_4images_image_date: "2007-10-28T12:45:39"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12354 -->
