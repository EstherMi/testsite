---
layout: "image"
title: "4"
date: "2010-08-08T17:09:26"
picture: "herberthindernis4.jpg"
weight: "4"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27814
imported:
- "2019"
_4images_image_id: "27814"
_4images_cat_id: "2005"
_4images_user_id: "1082"
_4images_image_date: "2010-08-08T17:09:26"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27814 -->
Hier sieht man ihn nochmal von der Seite. Der rote Kasten ist der Akku, man sieht, dass Herbert nur ca. 20 cm lang ist. Der TX ist das flache schwarze ganz oben.