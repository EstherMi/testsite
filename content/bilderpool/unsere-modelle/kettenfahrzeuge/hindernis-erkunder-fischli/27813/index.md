---
layout: "image"
title: "3"
date: "2010-08-08T17:09:26"
picture: "herberthindernis3.jpg"
weight: "3"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27813
imported:
- "2019"
_4images_image_id: "27813"
_4images_cat_id: "2005"
_4images_user_id: "1082"
_4images_image_date: "2010-08-08T17:09:26"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27813 -->
Hier sieht man den hinteren Fühler. Wenn er betätigt wird, wird die Rückwärtsfahrt unterbrochen, und er fährt ein ganz kleines Stück vor und dreht anschließend um ca 90 Grad.