---
layout: "image"
title: "1"
date: "2010-08-08T17:09:25"
picture: "herberthindernis1.jpg"
weight: "1"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27811
imported:
- "2019"
_4images_image_id: "27811"
_4images_cat_id: "2005"
_4images_user_id: "1082"
_4images_image_date: "2010-08-08T17:09:25"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27811 -->
Hier sieht man HERBERT-Hindernis von oben. Vorne befinden sich zwei Fühler, hinten einer, und an der Seite des TX-Controllers befinden sich zwei Taster mit denen man ihn losfahren und anhalten lassen kann.