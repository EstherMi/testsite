---
layout: "comment"
hidden: true
title: "18684"
date: "2014-02-06T20:04:05"
uploadBy:
- "Stefan Rm"
license: "unknown"
imported:
- "2019"
---
Danke für die Komplimente,
der Mittelteil ist mit ft machbar (bis auf die "Gewindezylinder" und die Kardangelenke im Antrieb). Das ganze wird dann aber nicht sehr stabil und die Lenkung hat sehr viel "Spiel".

Mein Modell ist mehr dem Bandvagn 202 nachempfunden ( hat eine lange Schnauze) .

Gruß,
Stefan