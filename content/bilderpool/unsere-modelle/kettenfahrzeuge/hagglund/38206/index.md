---
layout: "image"
title: "Steigfägigkeit"
date: "2014-02-05T12:24:18"
picture: "haegglund40.jpg"
weight: "40"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- details/38206
imported:
- "2019"
_4images_image_id: "38206"
_4images_cat_id: "2841"
_4images_user_id: "1924"
_4images_image_date: "2014-02-05T12:24:18"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38206 -->
Er schafft aus eigener Kraft 30°.