---
layout: "image"
title: "Hinterwagen"
date: "2014-02-05T12:24:18"
picture: "haegglund29.jpg"
weight: "29"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- details/38195
imported:
- "2019"
_4images_image_id: "38195"
_4images_cat_id: "2841"
_4images_user_id: "1924"
_4images_image_date: "2014-02-05T12:24:18"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38195 -->
Der Antriebsstrang zum Differential hin...