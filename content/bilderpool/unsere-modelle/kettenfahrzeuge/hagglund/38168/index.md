---
layout: "image"
title: "Gesamtansicht 2"
date: "2014-02-05T12:24:18"
picture: "haegglund02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- details/38168
imported:
- "2019"
_4images_image_id: "38168"
_4images_cat_id: "2841"
_4images_user_id: "1924"
_4images_image_date: "2014-02-05T12:24:18"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38168 -->
