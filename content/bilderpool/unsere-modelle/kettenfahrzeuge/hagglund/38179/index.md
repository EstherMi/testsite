---
layout: "image"
title: "Vorderwagen-Seite"
date: "2014-02-05T12:24:18"
picture: "haegglund13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- details/38179
imported:
- "2019"
_4images_image_id: "38179"
_4images_cat_id: "2841"
_4images_user_id: "1924"
_4images_image_date: "2014-02-05T12:24:18"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38179 -->
