---
layout: "image"
title: "Spritzenpresse Keksdrucker"
date: "2017-09-30T11:52:18"
picture: "04_Spritzenpresse.jpg"
weight: "4"
konstrukteure: 
- "Fabian, Max, Christian & Stefan"
fotografen:
- "Christian und Stefan"
keywords: ["Spritze", "Keksdrucker", "Spindelantrieb"]
uploadBy: "ftpi"
license: "unknown"
legacy_id:
- details/46511
imported:
- "2019"
_4images_image_id: "46511"
_4images_cat_id: "10"
_4images_user_id: "2611"
_4images_image_date: "2017-09-30T11:52:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46511 -->
Um die Farbe auf den Keks zu spritzen werden die Kolben der Injektionsspritzen mit einem Spindelantrieb in die Spritze gedrückt. Da der Zuckerguß recht zäh ist, muss die Spindel doppelt ausgeführt werden.