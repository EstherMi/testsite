---
layout: "image"
title: "Keksdrucker"
date: "2017-09-30T11:52:18"
picture: "01_Keksdrucker.jpg"
weight: "1"
konstrukteure: 
- "Fabian, Max, Christian & Stefan"
fotografen:
- "Christian und Stefan"
keywords: ["Keksdrucker", "Weihnachten"]
uploadBy: "ftpi"
license: "unknown"
legacy_id:
- details/46508
imported:
- "2019"
_4images_image_id: "46508"
_4images_cat_id: "10"
_4images_user_id: "2611"
_4images_image_date: "2017-09-30T11:52:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46508 -->
Weihnachten steht vor der Tür. Obwohl 3D Drucker Schon in vielen fischertechnikerhaushalten stehen, müssen Kekse immer noch manuell verziert werden. Hier hilft Magic's Cookie Printer - die nächsten Fotos zeigen wie er funktioniert.