---
layout: "image"
title: "Druckkopf Keksdrucker"
date: "2017-09-30T11:52:18"
picture: "03_Druckkopf.jpg"
weight: "3"
konstrukteure: 
- "Fabian, Max, Christian & Stefan"
fotografen:
- "Christian und Stefan"
keywords: ["Druckkopf", "Keksdrucker"]
uploadBy: "ftpi"
license: "unknown"
legacy_id:
- details/46510
imported:
- "2019"
_4images_image_id: "46510"
_4images_cat_id: "10"
_4images_user_id: "2611"
_4images_image_date: "2017-09-30T11:52:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46510 -->
Der Druckkopf besteht aus drei Reedkontakthaltern / Kabelführungen in die 4 mm Plastikschläuche aus dem Baumarkt gesteckt werden. Kurze Messingröhrchen und etwas Tesa-Film um die Schläuche sorgen für den perfekten Sitz.Druckkopf Keksdrucker