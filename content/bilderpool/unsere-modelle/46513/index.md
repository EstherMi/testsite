---
layout: "image"
title: "Antrieb Förderband"
date: "2017-09-30T11:52:18"
picture: "06_Bandantrieb.jpg"
weight: "6"
konstrukteure: 
- "Fabian, Max, Christian & Stefan"
fotografen:
- "Christian und Stefan"
keywords: ["Förderband", "M-Motor"]
uploadBy: "ftpi"
license: "unknown"
legacy_id:
- details/46513
imported:
- "2019"
_4images_image_id: "46513"
_4images_cat_id: "10"
_4images_user_id: "2611"
_4images_image_date: "2017-09-30T11:52:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46513 -->
Ein M-Motor zieht das Förderband. Es wird in einer Nut zwischen zwei Winkelträger geführt. Achtung: Die Kette nicht zu straff wählen!