---
layout: "image"
title: "2 traps cilinder"
date: "2016-10-17T21:02:42"
picture: "IMG_0731.jpg"
weight: "5"
konstrukteure: 
- "Chef8"
fotografen:
- "Chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/44625
imported:
- "2019"
_4images_image_id: "44625"
_4images_cat_id: "3317"
_4images_user_id: "838"
_4images_image_date: "2016-10-17T21:02:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44625 -->
Motor bevestiging met een 4 m inbus bout.