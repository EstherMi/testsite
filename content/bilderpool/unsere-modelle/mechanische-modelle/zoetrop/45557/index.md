---
layout: "image"
title: "Zoetrop - Gesamtansicht"
date: "2017-03-18T19:27:39"
picture: "Zoetrop_01.jpeg"
weight: "1"
konstrukteure: 
- "rito"
fotografen:
- "rito"
keywords: ["Zoetrop", "Film", "Optik", "Geschichte"]
uploadBy: "rito"
license: "unknown"
legacy_id:
- details/45557
imported:
- "2019"
_4images_image_id: "45557"
_4images_cat_id: "3386"
_4images_user_id: "2179"
_4images_image_date: "2017-03-18T19:27:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45557 -->
Bei einem Zoetrop blickt man entspannt durch die Sichtschlitze auf einen kurzen Filmstreifen (hier 24 Bilder) und dreht die obere Trommel. Durch den Stroboskopeffekt entsteht, je nachdem wie zügig gedreht wird eine fließende Abfolge. Die Einzelbilder verschwimmen zu einem Film, ähnlich einem Daumenkino.