---
layout: "image"
title: "Zoetrop - Seitenansicht"
date: "2017-03-18T19:27:39"
picture: "Zoetrop_02.jpeg"
weight: "2"
konstrukteure: 
- "rito"
fotografen:
- "rito"
keywords: ["Zoetrop", "Film", "Optik", "Geschichte"]
uploadBy: "rito"
license: "unknown"
legacy_id:
- details/45558
imported:
- "2019"
_4images_image_id: "45558"
_4images_cat_id: "3386"
_4images_user_id: "2179"
_4images_image_date: "2017-03-18T19:27:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45558 -->
Die Obere Trommel wurde mit einfachem schwarzem Bastelkartonpapier ausgekleidet und mit Sichtschlitzen für den Stroboskobeffekt versehen.