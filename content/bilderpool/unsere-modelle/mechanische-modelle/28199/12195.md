---
layout: "comment"
hidden: true
title: "12195"
date: "2010-09-19T22:41:24"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ich kenne das Ellipsenzeichnen mit der "Gärtnermethode": Zwei Pflöcke einschlagen, ein längeres Seil drumrum, und mit einem dritten Pflock bei immer komplett gespanntem Seil eine Kurve beschreiben. Per Definition (Summe der Entfernungen von den beiden Pflöcken konstant, jedenfalls bei Pflöcken der Dicke 0) ergibt das eine Ellipse. Ein Beweis, das dies hier eine Ellipse ergibt, wäre interessant. :-)

Gruß,
Stefan