---
layout: "image"
title: "04-gedrehtes Target"
date: "2006-02-11T14:18:07"
picture: "04-Ziel_in_Schrglage.jpg"
weight: "4"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/5751
imported:
- "2019"
_4images_image_id: "5751"
_4images_cat_id: "494"
_4images_user_id: "46"
_4images_image_date: "2006-02-11T14:18:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5751 -->
Das Target kann gedreht werden, um festzustellen, ob der Winkel auf das Sensorsignal einen Einfluß hat. Hier die Stellung -45 Grad.