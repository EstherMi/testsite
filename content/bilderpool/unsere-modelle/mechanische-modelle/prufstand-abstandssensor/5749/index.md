---
layout: "image"
title: "02-Steuerung"
date: "2006-02-11T14:07:58"
picture: "02-Steuerung_und_Sensorstativ.jpg"
weight: "2"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/5749
imported:
- "2019"
_4images_image_id: "5749"
_4images_cat_id: "494"
_4images_user_id: "46"
_4images_image_date: "2006-02-11T14:07:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5749 -->
Eine kleine Extraelektronik steuert den Schrittmotor im Viertelschrittbetrieb und wertet auch gleich die Endschalter aus.