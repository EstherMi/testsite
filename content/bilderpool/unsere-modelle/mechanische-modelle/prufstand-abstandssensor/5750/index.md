---
layout: "image"
title: "03-Ausgangslage"
date: "2006-02-11T14:18:07"
picture: "03-Ausgangslage.jpg"
weight: "3"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/5750
imported:
- "2019"
_4images_image_id: "5750"
_4images_cat_id: "494"
_4images_user_id: "46"
_4images_image_date: "2006-02-11T14:18:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5750 -->
Der Wagen ist gegen den unteren Endschalter gefahren. Dort ist der Abstand des Sensors zum Target praktisch null. Von hier aus fährt der Wagen los, bis er den gegenüberliegenden Endschalter erreicht. Während der Fahrt zeichnet der Computer das Analogsignal auf.