---
layout: "image"
title: "ft-Druckerei"
date: "2008-11-16T23:15:13"
picture: "PB160034.jpg"
weight: "2"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- details/16285
imported:
- "2019"
_4images_image_id: "16285"
_4images_cat_id: "1469"
_4images_user_id: "381"
_4images_image_date: "2008-11-16T23:15:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16285 -->
Die dazu gehörenden Buchstaben fehlen rechts