---
layout: "image"
title: "Fahrzeug 2 - Rennwagen"
date: "2015-10-24T21:46:36"
picture: "kettenzugfahrzeugbahn13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42119
imported:
- "2019"
_4images_image_id: "42119"
_4images_cat_id: "3136"
_4images_user_id: "104"
_4images_image_date: "2015-10-24T21:46:36"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42119 -->
Der Fahrer wurde seiner Beine beraubt. In den BS7,5 stecken Rastachsen, die ihn halten. Nach hinten kann er wegen des Spoilers nicht herausfallen. Aber Hauptsache 'n Helm ;-)