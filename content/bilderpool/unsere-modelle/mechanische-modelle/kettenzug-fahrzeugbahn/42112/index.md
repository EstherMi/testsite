---
layout: "image"
title: "Schrägaufzüge"
date: "2015-10-24T21:46:24"
picture: "kettenzugfahrzeugbahn06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42112
imported:
- "2019"
_4images_image_id: "42112"
_4images_cat_id: "3136"
_4images_user_id: "104"
_4images_image_date: "2015-10-24T21:46:24"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42112 -->
Die schräg nach oben ziehenden Ketten werden über die Kombination von je zwei alten großen Kardangelenken, Schnecke auf Z20 und schließlich Z10 auf Kette angetrieben. Auf der Antriebsseite ist die Kette jeweils von einem zweiten Z10 geführt, damit die überall identischen Abstände zwischen den Mitnehmern realisiert werden kann.