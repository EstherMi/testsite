---
layout: "image"
title: "Gesamtansicht"
date: "2015-10-24T21:46:24"
picture: "kettenzugfahrzeugbahn01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42107
imported:
- "2019"
_4images_image_id: "42107"
_4images_cat_id: "3136"
_4images_user_id: "104"
_4images_image_date: "2015-10-24T21:46:24"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42107 -->
Auf der inneren Bahn werden fünf verschiedene Fahrzeuge per Kettenzug sowohl schräg hoch als auch um 180°-Kurven gezogen. Oben angekommen, rollen sie von alleine auf der äußeren Bahn herunter und landen wieder beim ersten Kettenzug innen.

Dieses Modell verlangte leider zu viel Baby Sitting. Im Laufe eines ganzen Convention-Tages verhakte sich doch öfter mal etwas, oder jemand hatte seine Hand drin. Das ganze Modell ist nur minimal stabil, und die Fahrzeuge fahren in dieser Konfiguration auch ein bisschen langsam hoch. Das könnte man also durchaus besser machen.

Das Modell im Hintergrund gehört nicht zur Bahn (das ist Sylvias Autolackieranlage).