---
layout: "comment"
hidden: true
title: "21142"
date: "2015-10-25T20:14:59"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Ah, endlich! Meine Waldachtal-Fotos sind nicht wirklich gut genug für den Bilderpool. Klasse Idee, insbesondere mit den Übergängen von Kettensegment zu Kettensegment.

Grüße
H.A.R.R.Y.