---
layout: "image"
title: "Kurven (1)"
date: "2015-10-24T21:46:24"
picture: "kettenzugfahrzeugbahn07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42113
imported:
- "2019"
_4images_image_id: "42113"
_4images_cat_id: "3136"
_4images_user_id: "104"
_4images_image_date: "2015-10-24T21:46:24"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42113 -->
In den Kurven liegen die Ketten waagerecht und werden von unten per Achse angetrieben. Die Schnecke/Zahnrad-Kombination liegt jeweils am Fuß des Modells.