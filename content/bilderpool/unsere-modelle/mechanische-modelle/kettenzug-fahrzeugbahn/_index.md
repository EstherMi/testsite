---
layout: "overview"
title: "Kettenzug-Fahrzeugbahn"
date: 2019-12-17T19:25:58+01:00
legacy_id:
- categories/3136
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3136 --> 
Eine Bahn, auf der mehrere Fahrzeuge eine längere Strecke hochgezogen werden und wieder hinabrollen.