---
layout: "image"
title: "Fahrzeug 3 - Abschleppwagen"
date: "2015-10-24T21:46:36"
picture: "kettenzugfahrzeugbahn14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42120
imported:
- "2019"
_4images_image_id: "42120"
_4images_cat_id: "3136"
_4images_user_id: "104"
_4images_image_date: "2015-10-24T21:46:36"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42120 -->
Mit grünen Reifen, ganz wie in echt ;-)