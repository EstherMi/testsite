---
layout: "image"
title: "Kurven (2)"
date: "2015-10-24T21:46:24"
picture: "kettenzugfahrzeugbahn08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42114
imported:
- "2019"
_4images_image_id: "42114"
_4images_cat_id: "3136"
_4images_user_id: "104"
_4images_image_date: "2015-10-24T21:46:24"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42114 -->
Hier sieht man die Lagerung der waagerechten Ketten, fotografiert auf der Seite, auf der zwei Kurvenstecken übereinanderliegen.