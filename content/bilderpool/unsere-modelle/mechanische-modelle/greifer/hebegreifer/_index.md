---
layout: "overview"
title: "Hebegreifer"
date: 2019-12-17T19:22:59+01:00
legacy_id:
- categories/2952
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2952 --> 
Dieser Greifer hebt das Objekt leicht an, solbald es fest im Greifer ist. Das ganze benötigt nur eine Drehbewegung. Diese Ausführung des Greifers ist nicht sehr stark, aber zum heben von leichten Gegenständen reicht es trotzdem. Die Idee für die Funktionsweise kam mir in einer Diskussion mit Masked.