---
layout: "image"
title: "Hebegreifer von Unten"
date: "2014-09-27T18:57:33"
picture: "hebegreifer4.jpg"
weight: "4"
konstrukteure: 
- "Martin Giger"
fotografen:
- "Martin Giger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/39370
imported:
- "2019"
_4images_image_id: "39370"
_4images_cat_id: "2952"
_4images_user_id: "445"
_4images_image_date: "2014-09-27T18:57:33"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39370 -->
Die beiden BS15 mit Loch haben einen etwas grösseren Abstand damit die Bewegungsmechanik sauber dazwischen Platz hat, oder in anderen Worten, um den Greifer maximal schliessen zu können.