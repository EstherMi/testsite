---
layout: "image"
title: "Seitenansicht"
date: "2009-08-14T21:37:27"
picture: "fingergreifer6.jpg"
weight: "27"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- details/24797
imported:
- "2019"
_4images_image_id: "24797"
_4images_cat_id: "1516"
_4images_user_id: "920"
_4images_image_date: "2009-08-14T21:37:27"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24797 -->
