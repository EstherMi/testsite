---
layout: "image"
title: "Greifer (2) Hand - Ansicht von schräg/hinten."
date: "2013-02-24T16:00:37"
picture: "DSCN5043.jpg"
weight: "14"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/36684
imported:
- "2019"
_4images_image_id: "36684"
_4images_cat_id: "1516"
_4images_user_id: "184"
_4images_image_date: "2013-02-24T16:00:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36684 -->
Und nun der letze. Die Hand ist geschlossen.