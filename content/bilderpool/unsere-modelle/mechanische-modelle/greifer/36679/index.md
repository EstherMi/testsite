---
layout: "image"
title: "Greifer (2) Hand"
date: "2013-02-24T16:00:37"
picture: "DSCN5038.jpg"
weight: "9"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/36679
imported:
- "2019"
_4images_image_id: "36679"
_4images_cat_id: "1516"
_4images_user_id: "184"
_4images_image_date: "2013-02-24T16:00:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36679 -->
der obere Finger kommt hinzu.