---
layout: "image"
title: "Greifer für Schüttgut (pneumatisch)"
date: "2008-12-30T16:07:44"
picture: "DSCN2590.jpg"
weight: "49"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/16798
imported:
- "2019"
_4images_image_id: "16798"
_4images_cat_id: "1516"
_4images_user_id: "184"
_4images_image_date: "2008-12-30T16:07:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16798 -->
Greifer offen.
Ansicht von unten.
Für den Greifer selbst habe ich die Kesselhalter 31592 genutzt.