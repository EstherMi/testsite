---
layout: "image"
title: "Greifer für Schüttgut (pneumatisch)"
date: "2008-12-30T16:07:44"
picture: "DSCN2587.jpg"
weight: "45"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/16794
imported:
- "2019"
_4images_image_id: "16794"
_4images_cat_id: "1516"
_4images_user_id: "184"
_4images_image_date: "2008-12-30T16:07:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16794 -->
Nächster Versuch.
Funktioniert sehr gut.
Hier mal ein Greifer der auch kleinste Granulate (2mm) heben kann.