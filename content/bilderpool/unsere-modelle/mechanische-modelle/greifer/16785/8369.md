---
layout: "comment"
hidden: true
title: "8369"
date: "2009-01-30T12:20:55"
uploadBy:
- "ludger-ftc"
license: "unknown"
imported:
- "2019"
---
Hi Robert,

ich sage mal jain....
Tragen kann er genau so viel wie der mechanische Greifer.
Das Problem an der Sache ist das Greifen der Gegenstände.
Wenn diese zu groß oder zu schwer sind reicht die Kraft des Zylinders nicht aus um den Greifer zu schließen.
Aber an und für sich klappt das schon ganz gut.

Gruß ludger