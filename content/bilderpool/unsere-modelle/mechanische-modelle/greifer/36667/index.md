---
layout: "image"
title: "Greifer (1)"
date: "2013-02-23T18:00:41"
picture: "DSCN5023.jpg"
weight: "21"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/36667
imported:
- "2019"
_4images_image_id: "36667"
_4images_cat_id: "1516"
_4images_user_id: "184"
_4images_image_date: "2013-02-23T18:00:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36667 -->
etwas andere Befestigung