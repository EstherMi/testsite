---
layout: "image"
title: "Greifer (2) Hand V2.0"
date: "2013-02-26T17:01:43"
picture: "DSCN5100.jpg"
weight: "4"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/36699
imported:
- "2019"
_4images_image_id: "36699"
_4images_cat_id: "1516"
_4images_user_id: "184"
_4images_image_date: "2013-02-26T17:01:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36699 -->
Auf diesem Bild kann man die um 20mm geringere Breite gut erkennen.