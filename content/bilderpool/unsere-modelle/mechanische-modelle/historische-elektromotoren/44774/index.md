---
layout: "image"
title: "Jacobi Motor 1834 Funktionsmodell"
date: "2016-11-21T17:35:48"
picture: "Jacobi_Motor_1834_Funktionsmodell.jpg"
weight: "10"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: ["Jacobi", "Motor", "Elektromotor"]
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- details/44774
imported:
- "2019"
_4images_image_id: "44774"
_4images_cat_id: "3337"
_4images_user_id: "2635"
_4images_image_date: "2016-11-21T17:35:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44774 -->
Jacobi Motor 1834 Funktionsmodell

Der Stator besteht aus 4 Elektromagneten des älteren Typs 31324, der Rotor aus 4 der neueren Magneten 32363. Dahinter steckt keine Philisophie sondern mein Bestand. 
Beim Nachbau ist zu beachten, dass sich die Pole von Stator und Rotor alle 45° gegenüberstehen müssen, deswegen der gedrängte Aufbau. Die Magnete sind jeweils parallel geschaltet. Je Umdrehung muss die Stromrichtung achtmal umgeschaltet werden. Der Stromwender (Kommutator, Kollektor) besteht aus dem Stufenschalter-Unterteil 31311, dessen Kontakte mit dem Spurkranz 35756 fixiert werden. Die Schleifkontakte bilden 2 Achsen, die von je einem Scharnier 36329 an die Kontakte gedrückt werden. Mit 14 V Versorgungsspannung wird eine Drehzahl von etwa 80 U/min erreicht. 
Im Original wurden 4 Stromwender mit je 1 Schleifkontakt verwendet, jeder Stromwender bestand aus 4 leitenden und 4 nichtleitenden Sektoren. 2 dieser Stromwender mit je 2 Schleifkontakten hätten die gleiche Funktion ausüben können. 
Mein Versuch mit 2 Paar Schleifringen und je 4 Unterbrecherstücken ist am hohen Reibungswiderstand der Unterbrecherstücke gescheitert.