---
layout: "image"
title: "Jacobi Motor 1834, elektrische Schaltung des Funktionsmodells"
date: "2016-11-26T21:54:29"
picture: "Schaltung_Jacobi_Motor.jpg"
weight: "5"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- details/44776
imported:
- "2019"
_4images_image_id: "44776"
_4images_cat_id: "3337"
_4images_user_id: "2635"
_4images_image_date: "2016-11-26T21:54:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44776 -->
Dargestellt sind die 4 parallel geschalteten Rotormagnete mit der Verbindung zum Stromwender, die Verbindungen der Kontakte des Stromwenders und die beiden Schleifkontakte.
Die 4 ebenfalls parallelgeschalteten Magnete des Stators sind nicht dargestellt.
Bei 10 V dreht der Motor mit 60 &#8211; 80 U/min und nimmt etwa 1,4 A auf.