---
layout: "image"
title: "Jacobi Motor Funktionsmodell halblinks"
date: "2016-11-21T17:35:48"
picture: "Jacobi_Motor_Funktionsmodell_halblinks.jpg"
weight: "9"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- details/44773
imported:
- "2019"
_4images_image_id: "44773"
_4images_cat_id: "3337"
_4images_user_id: "2635"
_4images_image_date: "2016-11-21T17:35:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44773 -->
