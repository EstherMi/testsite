---
layout: "overview"
title: "Historische Elektromotoren"
date: 2019-12-17T19:26:10+01:00
legacy_id:
- categories/3337
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3337 --> 
Vor fast 200 Jahren begann die Entwicklung des Elektromotors, siehe auch das Buch "Technikgeschichte mit fischertechnik" von Dirk Fox und Thomas Püttmann. Aus heutiger Sicht muten viele der ersten Versuche sehr seltsam an. Wegen der physikalischen Zusammenhänge ist ein Nachbau mit Fischertechnik äußerst interessant und sehr lehrreich.