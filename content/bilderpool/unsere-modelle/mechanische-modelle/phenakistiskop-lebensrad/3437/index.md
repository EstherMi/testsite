---
layout: "image"
title: "Antrieb per P-Mot"
date: "2005-01-02T15:36:50"
picture: "Antrieb.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chevyfahrer"
license: "unknown"
legacy_id:
- details/3437
imported:
- "2019"
_4images_image_id: "3437"
_4images_cat_id: "318"
_4images_user_id: "103"
_4images_image_date: "2005-01-02T15:36:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3437 -->
Der Antrieb mit dem P-Mot 1:8 ist ein wenig unglücklich gewählt da er eine zu hohe Drehzahl liefert.Ich denke der 1:50 ist die bessere Wahl um die Drehzahl feiner regulieren zu können.Wenn ich genügend 7,5er Winkelsteine eingekauft habe versuche ich mich mal am großen Rad welches allerdings mit ca1,70m Umfang recht groß ausfallen wird.