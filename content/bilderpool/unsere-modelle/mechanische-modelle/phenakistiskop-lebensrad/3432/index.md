---
layout: "image"
title: "Gesamtansicht"
date: "2005-01-02T15:36:50"
picture: "Gesamtansicht.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chevyfahrer"
license: "unknown"
legacy_id:
- details/3432
imported:
- "2019"
_4images_image_id: "3432"
_4images_cat_id: "318"
_4images_user_id: "103"
_4images_image_date: "2005-01-02T15:36:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3432 -->
Ein Nachbau eines Phenakistiskops oder Lebensrades(erfunden glaub ich ca.1830).Vorläufer der modernen Kinoprojektoren.Prinzip beruht auf dem Effekt daß ein kurz gezeigtes Bild im Gehirn noch einige ms erhalten bleibt und somit die Lücken zwischen den Bildern füllt.