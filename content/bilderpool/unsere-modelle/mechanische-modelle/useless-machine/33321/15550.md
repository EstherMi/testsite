---
layout: "comment"
hidden: true
title: "15550"
date: "2011-10-26T00:18:14"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
(...den-Thomas-vom-Kabel-schieb...)
Habe auch einen kurzen Moment gestutzt - aber klar: "Schalter" ist der Polwender, der die vom taster unterbrochene Verbindung überbrückt, wenn er manuell eingeschaltet wird. Der Exzenter dreht sich vom Taster weg (zweiter Stromkreis geschlossen) und kann dann problemlos den Polwender ausschalten; er dreht weiter, bis der Exzenter den Taster wieder betätigt (und die Leitung unterbricht).
Gruß, Dirk