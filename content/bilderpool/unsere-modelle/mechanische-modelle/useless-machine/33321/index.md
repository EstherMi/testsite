---
layout: "image"
title: "Schaltplan"
date: "2011-10-25T15:58:37"
picture: "Foto_46.jpg"
weight: "1"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- details/33321
imported:
- "2019"
_4images_image_id: "33321"
_4images_cat_id: "2461"
_4images_user_id: "1322"
_4images_image_date: "2011-10-25T15:58:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33321 -->
Hallo! Hier seht ihr den Schaltplan. Ich denke das dieser Selbsterklärend ist. Falls Ihr fragen habt, schreibt einen Kommentar.

Gruß
Lukas