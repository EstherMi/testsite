---
layout: "image"
title: "useless3"
date: "2011-10-19T16:49:06"
picture: "DSC01619.jpg"
weight: "4"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- details/33247
imported:
- "2019"
_4images_image_id: "33247"
_4images_cat_id: "2461"
_4images_user_id: "1322"
_4images_image_date: "2011-10-19T16:49:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33247 -->
Die Nockenscheibe, der Austaster und der Motor.