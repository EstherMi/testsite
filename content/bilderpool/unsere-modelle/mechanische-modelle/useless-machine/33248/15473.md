---
layout: "comment"
hidden: true
title: "15473"
date: "2011-10-20T10:21:25"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
So richtig verstehe ich die Schaltung am Ende Deiner Beschreibung nicht. Wie kann der Arm wieder einfahren, wenn er selbst vorher die Maschine auf  "Aus" geschaltet hat? Hast Du vielleicht einen Schaltplan (Handskizze)?

Gruß, Thomas