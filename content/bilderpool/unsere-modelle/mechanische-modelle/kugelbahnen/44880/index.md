---
layout: "image"
title: "Kugelbahn Verteilermechanismus"
date: "2016-12-10T21:29:01"
picture: "P1040710.jpg"
weight: "8"
konstrukteure: 
- "Matthias"
fotografen:
- "Matthias"
keywords: ["Kugelbahn", "Tischtennisbälle"]
uploadBy: "MatzeIsar"
license: "unknown"
legacy_id:
- details/44880
imported:
- "2019"
_4images_image_id: "44880"
_4images_cat_id: "1030"
_4images_user_id: "2522"
_4images_image_date: "2016-12-10T21:29:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44880 -->
Auslauf und Einlauf der Kugelbahn liegen übereinander.

Die ankommenden Bälle werden in der Auffangbox, rechts am Bildrand zu sehen, gesammelt. Von dort laufen sie langsam auf die Verteilerdrehscheibe zu, die die Bälle abwechselnd nach rechts und links auf die Abrollschienen zum Schieber, der die Bälle unter den Turm befördert, verteilt.