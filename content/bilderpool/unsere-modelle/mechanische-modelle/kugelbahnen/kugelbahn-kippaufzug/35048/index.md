---
layout: "image"
title: "Stufe 7"
date: "2012-06-07T13:08:18"
picture: "kippaufzug09.jpg"
weight: "9"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/35048
imported:
- "2019"
_4images_image_id: "35048"
_4images_cat_id: "2595"
_4images_user_id: "453"
_4images_image_date: "2012-06-07T13:08:18"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35048 -->
