---
layout: "image"
title: "Beschriftung der Eingabeschalter"
date: "2016-12-10T21:29:02"
picture: "P1040717.jpg"
weight: "15"
konstrukteure: 
- "Matthias"
fotografen:
- "Matthias"
keywords: ["Kugelbahn"]
uploadBy: "MatzeIsar"
license: "unknown"
legacy_id:
- details/44887
imported:
- "2019"
_4images_image_id: "44887"
_4images_cat_id: "1030"
_4images_user_id: "2522"
_4images_image_date: "2016-12-10T21:29:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44887 -->
Schieber auf Position LINKS. Schalter über Analogeingang A1 mit Arduino verbunden.
In der Software ist dieses Signal der Variablen E4_SCH zugeordnet.