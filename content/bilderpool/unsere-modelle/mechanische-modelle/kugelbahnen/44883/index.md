---
layout: "image"
title: "Beladung des Schiebers"
date: "2016-12-10T21:29:02"
picture: "P1040713.jpg"
weight: "11"
konstrukteure: 
- "Matthias"
fotografen:
- "Matthias"
keywords: ["Kugelbahn", "Tischtennisbälle"]
uploadBy: "MatzeIsar"
license: "unknown"
legacy_id:
- details/44883
imported:
- "2019"
_4images_image_id: "44883"
_4images_cat_id: "1030"
_4images_user_id: "2522"
_4images_image_date: "2016-12-10T21:29:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44883 -->
Im Bild unten links eine Box zur Aufbewahrung der Tischtennisbälle.