---
layout: "image"
title: "Geöffnete Umlenkung"
date: "2012-02-26T12:53:08"
picture: "kugelbahnmitschraegaufzug11.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "RitterRost"
license: "unknown"
legacy_id:
- details/34430
imported:
- "2019"
_4images_image_id: "34430"
_4images_cat_id: "2544"
_4images_user_id: "1129"
_4images_image_date: "2012-02-26T12:53:08"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34430 -->
Geöffnete Umlenkung