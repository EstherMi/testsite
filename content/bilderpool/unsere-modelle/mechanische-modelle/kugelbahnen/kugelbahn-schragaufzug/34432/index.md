---
layout: "image"
title: "Kugelführung auf den Schrägaufzug"
date: "2012-02-26T12:53:08"
picture: "kugelbahnmitschraegaufzug13.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "RitterRost"
license: "unknown"
legacy_id:
- details/34432
imported:
- "2019"
_4images_image_id: "34432"
_4images_cat_id: "2544"
_4images_user_id: "1129"
_4images_image_date: "2012-02-26T12:53:08"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34432 -->
Kugelführung auf den Schrägaufzug