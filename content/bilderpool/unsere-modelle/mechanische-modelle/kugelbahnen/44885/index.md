---
layout: "image"
title: "Aufzug"
date: "2016-12-10T21:29:02"
picture: "P1040715.jpg"
weight: "13"
konstrukteure: 
- "Matthias"
fotografen:
- "Matthias"
keywords: ["Kugelbahn", "Aufzug"]
uploadBy: "MatzeIsar"
license: "unknown"
legacy_id:
- details/44885
imported:
- "2019"
_4images_image_id: "44885"
_4images_cat_id: "1030"
_4images_user_id: "2522"
_4images_image_date: "2016-12-10T21:29:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44885 -->
Der Aufzug arbeitet mit dem klassischen Spindelantrieb von Fischertechnik

Beim Hochfahren werden die Schritte über einen Impulszähler gezählt um die beiden Positionen Mitte und Oben zu finden. Auf dem Rückweg fährt der Aufzug gegen einen Endschalter.

Wenn die mittlere Position erreicht ist, wird der Halter (zwei Stahlstangen) nach rechts zurückgezogen. Alle Kugeln des Kugelturms müssen jetzt vom Spindelaufzug gehalten werden. Mit letzter Kraft hebt der Minimotor den gesamten Kugelturm auf die oberste Position.

An dieser Stelle kommt der Halter zurück und übernimmt die Sicherung des Kugelturms. Der Aufzug ist wieder frei, fährt zurück nach unten und holt eine neue Kugel.