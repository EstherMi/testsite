---
layout: "image"
title: "Kugelbahn mit Pendelaufzug"
date: "2014-06-19T19:13:42"
picture: "kugelbahnmitpendelaufzug11.jpg"
weight: "11"
konstrukteure: 
- "Ritterrost"
fotografen:
- "Ritterrost"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "RitterRost"
license: "unknown"
legacy_id:
- details/38960
imported:
- "2019"
_4images_image_id: "38960"
_4images_cat_id: "2916"
_4images_user_id: "1129"
_4images_image_date: "2014-06-19T19:13:42"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38960 -->
Pendelaufzug in unterer Endstellung
Hier erfolgt die Umsteuerung per Taster
Der Bodern der Etagen wird mechanich nach unten gedrückt und die Kugeln rollen in den Aufzug