---
layout: "image"
title: "Kugelbahn für Golfbällen"
date: "2008-09-09T22:43:32"
picture: "DSC01570klein.jpg"
weight: "1"
konstrukteure: 
- "Rob van Oostenbrugge"
fotografen:
- "Rob van Oostenbrugge"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Tovenaar"
license: "unknown"
legacy_id:
- details/15216
imported:
- "2019"
_4images_image_id: "15216"
_4images_cat_id: "2342"
_4images_user_id: "814"
_4images_image_date: "2008-09-09T22:43:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15216 -->
Eine Kugelbahn für Golfbällen