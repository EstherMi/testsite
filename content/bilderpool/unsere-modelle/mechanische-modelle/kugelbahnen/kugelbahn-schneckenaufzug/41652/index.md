---
layout: "image"
title: "Kugel während des hebens"
date: "2015-07-29T11:02:34"
picture: "kbmsa05.jpg"
weight: "5"
konstrukteure: 
- "cpuetter"
fotografen:
- "cpuetter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- details/41652
imported:
- "2019"
_4images_image_id: "41652"
_4images_cat_id: "3104"
_4images_user_id: "298"
_4images_image_date: "2015-07-29T11:02:34"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41652 -->
Einmal eingefädelt läuft die Kugel immer weiter in der Nut hoch.
Eine Abdeckung nach vorne ist dabei nicht erforderlich, da die Rotation der Schnecke die Kugel nach hinten drückt.
Durch das präzise einlegen in die Nut bräuche man hier nichtmal die hinteren Bausteine.