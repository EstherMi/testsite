---
layout: "image"
title: "Bahn als ganzes"
date: "2015-07-29T11:02:34"
picture: "kbmsa01.jpg"
weight: "1"
konstrukteure: 
- "cpuetter"
fotografen:
- "cpuetter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- details/41648
imported:
- "2019"
_4images_image_id: "41648"
_4images_cat_id: "3104"
_4images_user_id: "298"
_4images_image_date: "2015-07-29T11:02:34"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41648 -->
Ein Video gibt's hier: [url=http://onedrive.live.com/?cid=3bb1725198a49fbc&id=3BB1725198A49FBC%2173431&ithint=video,mp4&authkey=!AFKyL7k63on1URc]hier[/url]
Die ganze Fördertechnik muss sehr präzise ausgerichtet werden, die Kugel muss von alleine reinlaufen können und muss andererseits genug Druck bekommen, dass sie in der feinen Schnecke halt findet.
Eine Einschränkung ist, dass in meinen Versuchen nur eine Kugel zuverlässig gefördert wurde, bei zweien funktioniert es noch ohne größere Probleme.
Die Schnecke muss zudem mit einer Mindesdrehzahl laufen, da sonst der Auswurf nicht funktioniert. Ich habe den Motor mit 6V betrieben.
Die Achse in den Bauseinen dient zur Stabilisierung, da sich die Bausteine sonst zu leicht wegdrücken.