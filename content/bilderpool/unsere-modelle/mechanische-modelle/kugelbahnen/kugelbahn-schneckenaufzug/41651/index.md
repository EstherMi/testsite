---
layout: "image"
title: "Kugel in Aufmahme"
date: "2015-07-29T11:02:34"
picture: "kbmsa04.jpg"
weight: "4"
konstrukteure: 
- "cpuetter"
fotografen:
- "cpuetter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- details/41651
imported:
- "2019"
_4images_image_id: "41651"
_4images_cat_id: "3104"
_4images_user_id: "298"
_4images_image_date: "2015-07-29T11:02:34"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41651 -->
Durch die Abdeckplatte wird sichergestellt, dass die Kugel in der Nut von den BS 30 läuft.
Es geht zwar auch ohne, aber es läuft so einfach sauberer.