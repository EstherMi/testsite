---
layout: "image"
title: "Kugeltreppe"
date: "2013-10-22T16:40:22"
picture: "kugeltreppe3.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/37746
imported:
- "2019"
_4images_image_id: "37746"
_4images_cat_id: "2803"
_4images_user_id: "162"
_4images_image_date: "2013-10-22T16:40:22"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37746 -->
