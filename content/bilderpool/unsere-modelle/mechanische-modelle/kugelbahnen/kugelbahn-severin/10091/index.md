---
layout: "image"
title: "Kugelbahn"
date: "2007-04-19T20:05:30"
picture: "severinkugelbahn1.jpg"
weight: "1"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/10091
imported:
- "2019"
_4images_image_id: "10091"
_4images_cat_id: "914"
_4images_user_id: "558"
_4images_image_date: "2007-04-19T20:05:30"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10091 -->
Seitenansicht