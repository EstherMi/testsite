---
layout: "image"
title: "Kugelabgabe"
date: "2012-03-02T15:36:26"
picture: "kugelbahn7.jpg"
weight: "7"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/34513
imported:
- "2019"
_4images_image_id: "34513"
_4images_cat_id: "2549"
_4images_user_id: "453"
_4images_image_date: "2012-03-02T15:36:26"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34513 -->
Die Kugel ist aus dem Fach und macht sich wieder auf den weg nach unten.