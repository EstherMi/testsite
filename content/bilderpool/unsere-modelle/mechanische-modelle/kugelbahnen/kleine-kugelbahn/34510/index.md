---
layout: "image"
title: "Kugelaufnahme"
date: "2012-03-02T15:36:26"
picture: "kugelbahn4.jpg"
weight: "4"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/34510
imported:
- "2019"
_4images_image_id: "34510"
_4images_cat_id: "2549"
_4images_user_id: "453"
_4images_image_date: "2012-03-02T15:36:26"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34510 -->
Hier wird die Strebe von dem Aufzugsarm herrunter gedrückt, so das eine Kugel in den Aufzug rollen kann. Es fehlt noch ein mm, dann rollt die Kugel rüber.