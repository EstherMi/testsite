---
layout: "image"
title: "Die Unterfunktionen"
date: "2016-12-11T10:30:59"
picture: "PRG_Unterfunktion01.png"
weight: "2"
konstrukteure: 
- "Matthias"
fotografen:
- "Matthias"
keywords: ["Arduino", "Kugelbahn", "Software"]
uploadBy: "MatzeIsar"
license: "unknown"
legacy_id:
- details/44891
imported:
- "2019"
_4images_image_id: "44891"
_4images_cat_id: "1030"
_4images_user_id: "2522"
_4images_image_date: "2016-12-11T10:30:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44891 -->
Diese Datei zeigt die beiden Unterfunktionen.