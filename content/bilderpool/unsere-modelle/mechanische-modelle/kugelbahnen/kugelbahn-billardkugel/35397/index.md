---
layout: "image"
title: "06 Kugelbahn"
date: "2012-08-28T22:30:01"
picture: "kugelbahn06.jpg"
weight: "6"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/35397
imported:
- "2019"
_4images_image_id: "35397"
_4images_cat_id: "2626"
_4images_user_id: "860"
_4images_image_date: "2012-08-28T22:30:01"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35397 -->
Eigentlich viel zu viel Technik für eine simple Kugelbahn, aber ich konnte nicht anders ;-)