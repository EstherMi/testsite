---
layout: "image"
title: "05 Kugelbahn"
date: "2012-08-28T22:30:01"
picture: "kugelbahn05.jpg"
weight: "5"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/35396
imported:
- "2019"
_4images_image_id: "35396"
_4images_cat_id: "2626"
_4images_user_id: "860"
_4images_image_date: "2012-08-28T22:30:01"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35396 -->
Das ist der "Looping". Da die Kugel eine ganze Drehung nicht schaffen würde (und um Platz zu sparen), habe ich nur einen halben Looping gebaut. 

Technisch gesehen ist das vielleicht ein bisschen unsauber, da die Eigenrotation der Kugel nicht der Rollrichtung entspricht, aber was solls ;-)