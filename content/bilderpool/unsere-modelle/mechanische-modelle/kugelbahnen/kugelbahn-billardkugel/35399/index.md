---
layout: "image"
title: "08 Kugelbahn"
date: "2012-08-28T22:30:01"
picture: "kugelbahn08.jpg"
weight: "8"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/35399
imported:
- "2019"
_4images_image_id: "35399"
_4images_cat_id: "2626"
_4images_user_id: "860"
_4images_image_date: "2012-08-28T22:30:01"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35399 -->
Das ist der Wagen. Die Lichtschranke prüft, ob die Kugel auf dem Wagen liegt, der Endtaster ist auf dem Boden.