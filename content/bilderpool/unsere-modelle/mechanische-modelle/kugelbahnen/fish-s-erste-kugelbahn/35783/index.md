---
layout: "image"
title: "Verladestation"
date: "2012-10-04T20:21:08"
picture: "fishserstekugelbahn2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- details/35783
imported:
- "2019"
_4images_image_id: "35783"
_4images_cat_id: "2645"
_4images_user_id: "1113"
_4images_image_date: "2012-10-04T20:21:08"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35783 -->
Unten liegen die Kugeln (die Bahn ist gerade nicht in Betrieb), selbst mit nur meinen zwei Mitnehmern geht es richtig rund.