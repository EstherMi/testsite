---
layout: "image"
title: "Murmelbahn mit ft-Sitzen"
date: "2011-10-16T20:34:57"
picture: "MurmelnMitSitzen.jpg"
weight: "22"
konstrukteure: 
- "Jutta Püttmann"
fotografen:
- "Jutta Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- details/33185
imported:
- "2019"
_4images_image_id: "33185"
_4images_cat_id: "1030"
_4images_user_id: "1088"
_4images_image_date: "2011-10-16T20:34:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33185 -->
Eine Mini-Murmelbahn mit Richtungsumkehr durch fischertechnik-Sitze.