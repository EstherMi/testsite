---
layout: "image"
title: "Gesamtansicht von vorne"
date: "2011-08-02T19:13:39"
picture: "murmelbahnfastslow04.jpg"
weight: "4"
konstrukteure: 
- "Tobias Horst"
fotografen:
- "Tobias Horst"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- details/31506
imported:
- "2019"
_4images_image_id: "31506"
_4images_cat_id: "2340"
_4images_user_id: "1007"
_4images_image_date: "2011-08-02T19:13:39"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31506 -->
