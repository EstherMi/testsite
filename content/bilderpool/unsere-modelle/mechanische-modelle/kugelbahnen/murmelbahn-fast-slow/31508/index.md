---
layout: "image"
title: "Streckenansicht von oben"
date: "2011-08-02T19:13:39"
picture: "murmelbahnfastslow06.jpg"
weight: "6"
konstrukteure: 
- "Tobias Horst"
fotografen:
- "Tobias Horst"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- details/31508
imported:
- "2019"
_4images_image_id: "31508"
_4images_cat_id: "2340"
_4images_user_id: "1007"
_4images_image_date: "2011-08-02T19:13:39"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31508 -->
