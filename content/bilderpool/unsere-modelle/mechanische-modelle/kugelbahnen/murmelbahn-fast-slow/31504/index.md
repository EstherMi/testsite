---
layout: "image"
title: "Die Weiche"
date: "2011-08-02T19:13:39"
picture: "murmelbahnfastslow02.jpg"
weight: "2"
konstrukteure: 
- "Tobias Horst"
fotografen:
- "Tobias Horst"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- details/31504
imported:
- "2019"
_4images_image_id: "31504"
_4images_cat_id: "2340"
_4images_user_id: "1007"
_4images_image_date: "2011-08-02T19:13:39"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31504 -->
Hier kann man zwischen zwei unterschiedlichen Steckenfürungen wählen.