---
layout: "image"
title: "Kugelbahn Version 1 Ansicht rechte Seite"
date: "2015-04-22T16:44:16"
picture: "kugelbahnv04.jpg"
weight: "4"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- details/40862
imported:
- "2019"
_4images_image_id: "40862"
_4images_cat_id: "3070"
_4images_user_id: "936"
_4images_image_date: "2015-04-22T16:44:16"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40862 -->
Hier sieht man, das die Endabschaltung der BSB rechts etwas übersteht.