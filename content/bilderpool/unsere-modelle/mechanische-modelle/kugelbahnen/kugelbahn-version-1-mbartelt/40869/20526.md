---
layout: "comment"
hidden: true
title: "20526"
date: "2015-04-22T23:24:29"
uploadBy:
- "mbartelt"
license: "unknown"
imported:
- "2019"
---
Hi Stefan,

der Reedkontakt löst den Monoflop aus der mit dem Schwellwertschalter realisiert ist. Nicht Nachtriggerbar, dauer rund 2 Sekunden. Rechts daneben ist mit dem SN 7402 ein Flipflop geschaltet, der vom Schwellwertschalter gestartet wird. Entladung beginnt und am Ende wird der Motor abgeschaltet. Dann wird der Monoflop mit dem SN 74121 gestartet der den Aufzug anlaufen läßt. Die Leistungsstufe schaltet die beiden Motoren anstelle eines Relais. Beim Start des Monoflop wird gleichzeitig ein FlipFlop ausgelöst (oben linker Baustein mit 7400), das obere Relais auslöst, dass als Polwender arbeitet. Der Zug fährt nun wieder zurück. Die Steuerung des Zuges ist per Dioden und Entschalter gelöst.

Gruß

Manfred