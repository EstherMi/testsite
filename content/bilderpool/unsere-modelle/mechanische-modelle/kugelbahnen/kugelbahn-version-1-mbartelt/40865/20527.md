---
layout: "comment"
hidden: true
title: "20527"
date: "2015-04-22T23:26:41"
uploadBy:
- "mbartelt"
license: "unknown"
imported:
- "2019"
---
Hi Stefan,

eine Lichtschranke steuert den Grundbaustein an, der den Monoflop triggert und den 2. Aufzug in Bewegung setzt. Ein zweites Relais betätigt das Zählwerk.

Manfred