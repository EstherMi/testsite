---
layout: "image"
title: "Kugelbahn Version 1 Ansicht mittlerer Teil"
date: "2015-04-22T16:44:16"
picture: "kugelbahnv05.jpg"
weight: "5"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- details/40863
imported:
- "2019"
_4images_image_id: "40863"
_4images_cat_id: "3070"
_4images_user_id: "936"
_4images_image_date: "2015-04-22T16:44:16"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40863 -->
Wärend der Bauphase ist der Dynamic M Kasten auf den Markt gekommen und meine Tochter fand die Klangelemente ganz toll. Kurzentschlossen gekauft und in etwas modifizierter Fassung mit eingebaut.