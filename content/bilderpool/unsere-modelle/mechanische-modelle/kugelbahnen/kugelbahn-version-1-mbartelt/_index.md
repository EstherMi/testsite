---
layout: "overview"
title: "Kugelbahn Version 1 (mbartelt)"
date: 2019-12-17T19:20:09+01:00
legacy_id:
- categories/3070
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3070 --> 
Nachdem ich das Video Schoonhooven 2014 vom Fischertechnik Club NL gesehen habe, dachte ich, es wird an der Zeit meine erste eigene Kugelbahn zu bauen. Ich wollte wie in Video auch die Bau-Spiel-Bahn integrieren. Einiges sollte automatisiert werden, das Verladen der Kugeln und das Anfordern des Zuges werden allerdings über 2 Taster ausgelöst, damit meine Tochter beim Spielen auch etwas zu tun hat. Es gibt hier ja Bilder von Ihr als sie mit 15 Monaten staunend ihren ersten Kontakt mit Fischertechnik hatte. Nun wird sie in knapp 2 Monaten schon 5 Jahre und Fischertechnik findet sie noch immer ganz toll.