---
layout: "image"
title: "Kugelbahn Version 1 Ende auf der rechten Seite"
date: "2015-04-22T16:44:16"
picture: "kugelbahnv15.jpg"
weight: "15"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- details/40873
imported:
- "2019"
_4images_image_id: "40873"
_4images_cat_id: "3070"
_4images_user_id: "936"
_4images_image_date: "2015-04-22T16:44:16"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40873 -->
Hier kommen die Kugeln an, und ein Lichtschanke erkennt ob mindestens 4 Kugeln da sind.