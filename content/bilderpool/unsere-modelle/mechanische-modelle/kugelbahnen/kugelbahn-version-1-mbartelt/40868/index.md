---
layout: "image"
title: "Kugelbahn Version 1 Druchblick von der linken hinteren Seite"
date: "2015-04-22T16:44:16"
picture: "kugelbahnv10.jpg"
weight: "10"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- details/40868
imported:
- "2019"
_4images_image_id: "40868"
_4images_cat_id: "3070"
_4images_user_id: "936"
_4images_image_date: "2015-04-22T16:44:16"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40868 -->
Aufnahme mit Durchblick der Dynamic M Kugelbahn von der linken Seite aus.