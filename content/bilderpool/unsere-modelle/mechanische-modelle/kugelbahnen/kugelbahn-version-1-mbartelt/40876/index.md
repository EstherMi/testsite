---
layout: "image"
title: "Kugelbahn Version 1 Elektronik rechte Seite"
date: "2015-04-22T16:44:16"
picture: "kugelbahnv18.jpg"
weight: "18"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- details/40876
imported:
- "2019"
_4images_image_id: "40876"
_4images_cat_id: "3070"
_4images_user_id: "936"
_4images_image_date: "2015-04-22T16:44:16"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40876 -->
Die verbaute Elektronik auf der rechten Seite. Hier wird durch ein dreifach AND-Gatter abgefragt, ob der Zug da ist, ob mindestens 4 Kugeln bereit liegen und ob der Verladetaster gedrückt wurde. Dann werden 3 Kugeln verladen und die Bühne bewegt sich zur anderen Seite. Nun kann der Verladetaster erneut gedrückt werden (sofern wieder mindestens 4 Kugen da sind) und der zweite Wagen kann beladen werden. Mit den Anforderungstaster auf der linken Seite kann der Zug losfahren.