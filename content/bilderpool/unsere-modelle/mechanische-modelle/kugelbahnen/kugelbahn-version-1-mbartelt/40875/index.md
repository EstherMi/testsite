---
layout: "image"
title: "Kugelbahn Version 1 Verladung der Kugel rechte Seite #1"
date: "2015-04-22T16:44:16"
picture: "kugelbahnv17.jpg"
weight: "17"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- details/40875
imported:
- "2019"
_4images_image_id: "40875"
_4images_cat_id: "3070"
_4images_user_id: "936"
_4images_image_date: "2015-04-22T16:44:16"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40875 -->
Die Verladerampe an der rechten Seite. Endabschaltung über 2 Minitaster sowie 2 Dioden zum stoppen und 2 Dioden zur Schnellabschaltung. Die Umkehr des Motors erfolgt durch den Taster auf einen CMOS 4013 IC (Toggle Flip-Flop mit Entprellung), der ein Polwende-Relais ansteuert.