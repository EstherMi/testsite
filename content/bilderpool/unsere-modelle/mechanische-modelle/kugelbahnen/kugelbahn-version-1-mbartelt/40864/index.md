---
layout: "image"
title: "Kugelbahn Version 1 Ansicht linke Seite"
date: "2015-04-22T16:44:16"
picture: "kugelbahnv06.jpg"
weight: "6"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- details/40864
imported:
- "2019"
_4images_image_id: "40864"
_4images_cat_id: "3070"
_4images_user_id: "936"
_4images_image_date: "2015-04-22T16:44:16"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40864 -->
Hier nochmal die Entladung der BSB und der Aufzug in groß.