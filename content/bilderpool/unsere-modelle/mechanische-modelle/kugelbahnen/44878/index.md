---
layout: "image"
title: "Kugelbahn mit Tischtennisbällen"
date: "2016-12-10T11:17:30"
picture: "P1040708.jpg"
weight: "6"
konstrukteure: 
- "Matthias"
fotografen:
- "Matthias"
keywords: ["Kugelbahn", "Förderung", "Arduino"]
uploadBy: "MatzeIsar"
license: "unknown"
legacy_id:
- details/44878
imported:
- "2019"
_4images_image_id: "44878"
_4images_cat_id: "1030"
_4images_user_id: "2522"
_4images_image_date: "2016-12-10T11:17:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44878 -->
Die Tischtennisbälle werden in einem Turm übereinander gestapelt. Jeder neue Ball wird unter den bestehenden Ballstapel gedrückt und um die Größe eines Balldurchmessers angehoben, so lange bis der oberste Ball am oberen Ende des Turms auf die Bahn kullert und abrollt.
Ein Fördermechanismus führt die Kugeln von zwei Seiten unter den Turm.

Die Anlage besteht aus 4 Motoren, 7 Endschaltern und einem Impulszähler.
Gesteuert wird die Anlage mit dem Arduino Uno R3 und dem Motor Shield von Adafruit.

Die ganze Anlage steht in sich stabil auf Fischertechnikplatten.
Für den sicheren Transport habe ich sie aber auf ein passendes Holzbrett, inklusive Hebegriffe, geschraubt.