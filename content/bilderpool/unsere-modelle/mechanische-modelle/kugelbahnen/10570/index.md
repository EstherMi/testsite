---
layout: "image"
title: "Kugelbahn mit Greifer"
date: "2007-05-31T09:43:06"
picture: "diverse2.jpg"
weight: "24"
konstrukteure: 
- "-?-"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- details/10570
imported:
- "2019"
_4images_image_id: "10570"
_4images_cat_id: "1030"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:43:06"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10570 -->
