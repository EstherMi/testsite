---
layout: "overview"
title: "Tischtennisballstrecke"
date: 2019-12-17T19:20:19+01:00
legacy_id:
- categories/3175
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3175 --> 
Was hat denn eine Tischtennisballbahn mit Glasmurmeln zu tun?

Nichts, es sein denn Glasmurmeln treiben sie an.

Prinzipiell könnten die Glasmurmeln auch ein Hebeaggregat für ihresgleichen betreiben. Aber Tischtennisbälle sind leichter und damit etwas besser als Versuchsobjekte im zweiten Stadium geeignet.. Und - und etwas Abwechslung schadet auch einer Kugelbahn nicht.