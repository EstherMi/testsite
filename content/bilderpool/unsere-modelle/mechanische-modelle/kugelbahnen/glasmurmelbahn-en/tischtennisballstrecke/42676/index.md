---
layout: "image"
title: "Das Getriebe - Nahaufnahme"
date: "2016-01-07T18:43:03"
picture: "tischtennisballbahn3.jpg"
weight: "3"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/42676
imported:
- "2019"
_4images_image_id: "42676"
_4images_cat_id: "3175"
_4images_user_id: "1557"
_4images_image_date: "2016-01-07T18:43:03"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42676 -->
Hier sieht man wie das Z15 unter 45° mit dem Z40 kämmt.

Dieses Getriebe mit Untersetzung 15/40 = 3/8 ist das einzige Zusatzelement in der Transmission. Es gibt sonst nur noch Wellenlager. Eine Rücklaufhemmung ist nirgendwo eingbaut, trotzdem dreht sich das Heberad der Tischtennisbälle auch im ungünstigsten Lastfall nicht zurück. Dafür sind alleine die Lagerreibungen auf beiden Seiten des Getriebes verantwortlich. Ein weiteres erstaunliches Detail der Anlage.

----

A close-up of the gears

You see here how the 15 teeth spur gear meshes with the 40 teeth spur gear at an angle of 45°.

This gear mechanism is the one and only gear element in the drive train from the mill to the ping-pong-ball lifter. Its reduction ratio is 15/40 = 3/8. Remaining elements are only the bearings. There is explicitly no non-return device used. Only coulomb friction of the bearings hinders the ping-pong-ball lifter to turn backwards if there is no driving force from the mill. A very astonishing aspect of the machinery.