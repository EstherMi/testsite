---
layout: "image"
title: "Das Getriebe"
date: "2016-01-07T18:43:03"
picture: "tischtennisballbahn2.jpg"
weight: "2"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/42675
imported:
- "2019"
_4images_image_id: "42675"
_4images_cat_id: "3175"
_4images_user_id: "1557"
_4images_image_date: "2016-01-07T18:43:03"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42675 -->
Etwas arg luftig aber dennoch erstaunlich zuverlässig. Auf der Mühlradwelle sitzt ein Klemm-Z15 und kämmt mit dem Z40 unter einem Winkel von 45°. Leider gibt es von ft keine Lösung für solch einen Winkeltrieb. Dennoch arbeitet die Kraftübertragung noch ohne allzuviel rattern. Die Welle mit dem Z40 ist an einem benachbarten Turm aufgehängt. Dabei ist auf möglichst leichtgängige und spielfreie Lagerung geachtet worden.

----

The "gear box"

The gear mechanism is quite simple and looks very instable. In fact it works reliably. A special 15 teeth gear is clamped on the mill shaft and drives a larger 40 teeth gear. Both shafts have an angle of 45°. fischertechnik does not provide such a solution by genuine parts but the setup works without much rattling. The shaft with the 40 teeth spur gear is mounted at a nearby tower construction. Its bearings are selected to give leastmost coulomb friction.