---
layout: "image"
title: "Die Gelenkwelle"
date: "2016-01-07T18:43:03"
picture: "tischtennisballbahn4.jpg"
weight: "4"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/42677
imported:
- "2019"
_4images_image_id: "42677"
_4images_cat_id: "3175"
_4images_user_id: "1557"
_4images_image_date: "2016-01-07T18:43:03"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42677 -->
Vom Getriebe muß die Energie zum Heberad der Tischtennisbälle. Bedingt durch die Geometrie und den Wunsch nicht noch extra Wellenlager einbauen zu müssen, geht es per Gelenkwelle vom Z40 (hinten links im Bild) von schräg nach quer zum TTB-Heberad (unten mittig am Bildrand).

Für den Transport der Glasmurmelbahn ist das hintere Kardangelenk nicht direkt mit der Welle verschraubt sondern steckbar per V-Stein mit der Kardanhälfte formschlüsig verbunden. Da gibt es eine Nahaufnahme von, als "Beifang" quasi: http://www.ftcommunity.de/details.php?image_id=42216

----

The drive shaft

The drive shaft uses the already available supports to forward the rotational movement from the mill (upper left of the picture) to the ping-pong-ball lifter (bottom center of the picture). Due to the geometry it runs diagonally through the construction.

A simple combination of fischertechnik elements provide a removable joint for transport.