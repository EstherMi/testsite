---
layout: "image"
title: "Das ist der sekundäre Antrieb für die Tischtennisballbahn"
date: "2016-01-07T18:43:02"
picture: "tischtennisballbahn1.jpg"
weight: "1"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/42674
imported:
- "2019"
_4images_image_id: "42674"
_4images_cat_id: "3175"
_4images_user_id: "1557"
_4images_image_date: "2016-01-07T18:43:02"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42674 -->
Die Glasmurmeln rollen über die Aluschienen ins Mühlrad und treiben dieses durch ihre Gewichtskraft an. Die Drehbewegung wird über ein Getriebe zum Heberad der Tischtennisballbahn (TTBB) geleitet. Tatsächlich ist an der Tischtennisballstrecke kein Antriebsmotor verbaut.

Der eigentliche (primäre) Antrieb ist dann doch ein XS-Motor, der das Heberad der Glasmurmelbahn antreibt. Von dort gelangen die Murmeln zum Mühlrad.

Die Mühle war auch schon 2014 im Einsatz, allerdings etwas anders aufgebaut (http://www.ftcommunity.de/details.php?image_id=39560). Aufgrund eines Designfehlers mußte für 2015 die Strecke rund 30mm tiefergelegt werden, dadurch war ein radikaler Umbau, auch der Mühle, nötig.

----

[b] Secondary drive for the ping-pong-ball track [/b]

The glas marbles are guided by the aluminium rods towards the mill wheel and drive it just by their weight. The resulting movement drives the lifter of the ping-pong-ball track throug a gear mechanism. Their is really no motor driving the ping-pong-ball lifter directly.

The primary energy source is a motor, of course. It drives a huge lifting wheel for the glas marbles and those marbles are arriving at the mill.

A mill already has been used in a fromer track system in 2014. Due to a design flaw there the tracks had to be lowered 30mm and thus the mill got a reconstrcution.