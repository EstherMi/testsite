---
layout: "image"
title: "Der Tischtennisballheber - Auslaßseite"
date: "2016-01-07T18:43:03"
picture: "tischtennisballbahn7.jpg"
weight: "7"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/42680
imported:
- "2019"
_4images_image_id: "42680"
_4images_cat_id: "3175"
_4images_user_id: "1557"
_4images_image_date: "2016-01-07T18:43:03"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42680 -->
Unten rollen die TTB rein, oben quer dazu wieder raus. Der TTB-Heber steht unter knapp 37° gegen das Raster der Grundplatte verdreht und paßt trotzdem genau ins Raster. Hoch leben die BS15 mit rundem Zapfen (31059) und die pythagoräischen Zahlen.

----

The ping-pong-ball lifter outlet side

At the bottom track the balls roll in and at the top the balls leave transversal. The lifter is mounted against the traditional direction at about 37° but still fits exactly into the grid given. Shout hooray at special elements with round tongues (31059) and the pythagorean numbers.