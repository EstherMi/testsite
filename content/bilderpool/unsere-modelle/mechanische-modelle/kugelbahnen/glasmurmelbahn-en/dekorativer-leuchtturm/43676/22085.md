---
layout: "comment"
hidden: true
title: "22085"
date: "2016-06-03T20:52:10"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Die Orginalkonstruktion stammt aus dem ec3 und war eines meiner Lieblingsmodelle:
https://ft-datenbank.de/web_document.php?id=baad8b10-bc1c-481c-9c16-906a003b94eb
Seite 16

In der Anleitung ist auch eine Abbildung des festsitzenden Lämpchens.

Gruß, David