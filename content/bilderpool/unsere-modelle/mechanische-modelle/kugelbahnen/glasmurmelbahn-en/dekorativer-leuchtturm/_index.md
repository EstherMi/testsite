---
layout: "overview"
title: "Dekorativer Leuchtturm"
date: 2019-12-17T19:20:23+01:00
legacy_id:
- categories/3232
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3232 --> 
Es gab schon lange keinen Leuchtturm mehr im Bilderpool. Also war es Zeit mal einen zu bauen. Inspiriert vom Original aus 1976 (http://ft-datenbank.de/details.php?ArticleVariantId=0f71a422-cf0f-4bfe-a1f3-b0f2ed40b181) kommt hier meine Interpretation des Themas.