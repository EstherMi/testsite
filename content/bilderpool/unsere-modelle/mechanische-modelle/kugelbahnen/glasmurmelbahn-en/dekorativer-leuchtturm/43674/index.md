---
layout: "image"
title: "Übergang von Turmschaft und Oktogon"
date: "2016-06-03T15:50:44"
picture: "leuchtturm4.jpg"
weight: "4"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/43674
imported:
- "2019"
_4images_image_id: "43674"
_4images_cat_id: "3232"
_4images_user_id: "1557"
_4images_image_date: "2016-06-03T15:50:44"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43674 -->
Der Leuchtturmsockel ist gegenüber den Schienenstützen des Oktogons um 22,5° verdreht. So passen die Bogenträger aneinander vorbei ohne sich gegenseitig zu behindern.

----

The foundation of the lighthouse has an angular offset of 22.5° against the track supports. This way everything passes side-by-side without interference.