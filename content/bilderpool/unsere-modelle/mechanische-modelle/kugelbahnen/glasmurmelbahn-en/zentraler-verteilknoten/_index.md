---
layout: "overview"
title: "Zentraler Verteilknoten"
date: 2019-12-17T19:20:19+01:00
legacy_id:
- categories/3148
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3148 --> 
Zwei Teilkreisläufe treffen im zentralen Verteiler zusammen und werden dort auch wieder aufgetrennt.