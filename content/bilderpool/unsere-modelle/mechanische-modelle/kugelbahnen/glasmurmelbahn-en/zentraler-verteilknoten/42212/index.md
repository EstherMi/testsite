---
layout: "image"
title: "Die Version aus Waldachtal 2015"
date: "2015-11-03T19:35:57"
picture: "pic4.jpg"
weight: "4"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/42212
imported:
- "2019"
_4images_image_id: "42212"
_4images_cat_id: "3148"
_4images_user_id: "1557"
_4images_image_date: "2015-11-03T19:35:57"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42212 -->
Und hier ein seltener Schnappschuss eines gar nicht so seltenen Fehlers bei der Version, die in Waldachtal zu sehen war. Eine Murmel ist  - wie auch immer - am Eingangsportal neben den Schienen hängengeblieben. Sie liegt auf der Schienenstütze, wird von den nachdrängenden Murmeln gegen das Portal gedrückt und blockiert die Strecke. Einer der Gründe warum der Verteiler verbessert werden mußte.

----

Here you see an issue of the old distributor. One marble got - however it happened - aside the tracks and blocks all following ones. One of the issues for the reconstruction.