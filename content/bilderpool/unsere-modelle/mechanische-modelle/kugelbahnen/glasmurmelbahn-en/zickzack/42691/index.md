---
layout: "image"
title: "Fundament - linker Turm"
date: "2016-01-08T13:30:54"
picture: "zickzack3.jpg"
weight: "3"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/42691
imported:
- "2019"
_4images_image_id: "42691"
_4images_cat_id: "3177"
_4images_user_id: "1557"
_4images_image_date: "2016-01-08T13:30:54"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42691 -->
Dank BS15 mit rundem Zapfen (31059 - http://ft-datenbank.de/details.php?ArticleVariantId=ab340c5c-49ec-4679-b6b0-627ce5a03d30) lassen sich die Turmecken zerstörungsfrei in der Grundplatte verankern. Der Nutenmittenabstand beträgt exakt 42,4mm. Man beachte die Kennzeichnung der grauen Streben! Gemäß Herrn Pythagoras beträgt die korrekte Länge der schrägen Streben nahzu 45mm. So "nahezu", daß der Fehler locker in der Fertigungstoleranz der Teile untergeht.

Der rechte Turm ist gegengleich aufgebaut.

WARNUNG: Die verdrehten Geometrien lassen sich nur mittels 31059 oder anderen Bauteilen mit RUNDEM Zapfen zerstörungsfrei irgendwo verankern. Werden Standardbauteile mit eckigen Zapfen derart verdreht, so sind hinterher wenigstens die jeweiligen Nuten der Grundplatten zerstört.

----

Foundation - left side tower

Thanks to special ft elements 31059 having rounded tongue (http://ft-datenbank.de/details.php?ArticleVariantId=ab340c5c-49ec-4679-b6b0-627ce5a03d30) it is possible to mount the towers to the base plates without any damages. The distance of the groove centers is exactly 42.4mm. Note the markings on the grey struts. Applying the law of Pythagoras the diagonal struts need to be nearly 45mm - so nearly that the remaining error is well inside the manufacturing tolerances of the struts.

The right side tower is made mirror-like.

WARNING: Those rotated geometries need to be constructed with 31059 or other elements with ROUND tongues. Standard elements with square-shaped tongues getting rotated will definitely damage the respective notches of the base plates.