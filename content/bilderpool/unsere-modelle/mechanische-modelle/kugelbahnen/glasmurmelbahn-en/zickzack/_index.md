---
layout: "overview"
title: "ZickZack"
date: 2019-12-17T19:20:22+01:00
legacy_id:
- categories/3177
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3177 --> 
Für Kugelbahnen ein ziemlich einfacher Effekt. Die Kugeln rollen auf der obersten schiefen Ebene zur einen Seite, fallen ein kleines Stück zur nächstieferen Etage um deren Neigung zurück zur Eingangsseite zu folgen. Und so geht es von Etage zu Etage immer weiter hinunter. Die Besonderheit dieses ZickZacks ist der Aufbau der Türme. Diese sind um 45° im Raster der Grundplatte verdreht. So ergibt sich eine für ft-Statik außergewöhnliche und bislang einmalige Geometrie.

----

[b] ZigZag [/b]

On marble runs this is a relatively simple effect. The balls run at the topmost level to the other side, fall down to the next level and then roll back to the entry side. Each level is slightly tilted. This downward movement is repeated agian and again. This setup here has a special construction of the towers. Both are rotated 45° against the basic grid resulting in a strange geometry first shown here.