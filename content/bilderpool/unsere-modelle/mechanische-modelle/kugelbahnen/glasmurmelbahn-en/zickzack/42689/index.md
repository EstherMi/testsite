---
layout: "image"
title: "ZickZack 2015"
date: "2016-01-08T13:30:54"
picture: "zickzack1.jpg"
weight: "1"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/42689
imported:
- "2019"
_4images_image_id: "42689"
_4images_cat_id: "3177"
_4images_user_id: "1557"
_4images_image_date: "2016-01-08T13:30:54"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42689 -->
Das ZickZack ist ein größeres Element, meist schon eine eigene Kugelbahn. Im Bild ist es als Element in einer größere Anlage integriert.

----

ZigZag of 2015

The ZigZag is a larger element and most typically a mable run on its own. In the pictrue it is integrated as an element into a larger mable run.