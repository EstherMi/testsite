---
layout: "image"
title: "Ausstieg - Rückseite"
date: "2018-02-11T21:48:50"
picture: "heberad5.jpg"
weight: "5"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/47265
imported:
- "2019"
_4images_image_id: "47265"
_4images_cat_id: "3498"
_4images_user_id: "1557"
_4images_image_date: "2018-02-11T21:48:50"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47265 -->
Das Rad fördert die Murmeln im Uhrzeigersinn (hier allerdings von rechts kommend weil das Foto von der Rückseite aus aufgenommen ist) nach oben. Die Kante der Bauplatte (90 x 30)  führt sie bis knapp vor den höchsten Punkt. Die Murmeln rollen dann über diese Bauplatte auf die Schienen und nach links weg. Holzstäbe dämpfen das Klacken beim Einspuren in die Schienenführung besser als Metallachsen.

Die Böden der Kammern im Rad (Bauplatte 15 x 15) sind per Winkelstein 7,5° geneigt, das läßt die Murmeln freiwillig herausrollen wenn sie oben sind.
Während der Fahrt bieten die seitlich angebrachten Gelenkwürfelklauen den Murmeln ausreichend Halt in jeder Kammer. Erstaunlicherweise klappt das sogar bis kurz vor den Zenith. Also nochmal zusammengefaßt:
* Unten rollen die Murmeln auf die aussen liegende Kette.
* Die Gelenkwürfelklauen übernehmen während der Fahrt die Murmeln von der Kette.
* Die geneigten Bauplatten in jeder Kammer "werfen" die Murmeln im Bereich des Zeniths seitlich heraus.