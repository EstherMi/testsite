---
layout: "image"
title: "Schaltstation 2015"
date: "2016-09-10T14:26:54"
picture: "schaltstation1.jpg"
weight: "1"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: ["Glasmurmelbahn", "Energiezentrale", "Verteilung", "Elektroverteilung", "Netzteilanschluß"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/44349
imported:
- "2019"
_4images_image_id: "44349"
_4images_cat_id: "3275"
_4images_user_id: "1557"
_4images_image_date: "2016-09-10T14:26:54"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44349 -->
Zunächst einmal die alte Schaltstation. 4 Schalter, Netzteilanschluß und die Steckbuchsen für die Anschlüsse zur Anlage. Obendrüber ein Schutzdach, nicht nur für die Optik.

---

First the older version of the switching station. 4 switches, low voltage receptacle and plugs for the loads. A roof is above that not only serves optical purposes.