---
layout: "image"
title: "Schaltstation 2016"
date: "2016-09-10T14:26:54"
picture: "schaltstation8.jpg"
weight: "8"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: ["Glasmurmelbahn", "Energiezentrale", "Verteilung", "Elektroverteilung", "Netzteilanschluß"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/44356
imported:
- "2019"
_4images_image_id: "44356"
_4images_cat_id: "3275"
_4images_user_id: "1557"
_4images_image_date: "2016-09-10T14:26:54"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44356 -->
Die Netzteilbuchse ist jetzt ziemlich "versteckt". Das Bild stammt noch aus der Zeit als der Schlepplift ohne Kettenspanner lief.

---

The power plug (low voltage receptacle) now is hardly visible. The picture has been taken before the drag lift got its chain tensioner.