---
layout: "image"
title: "Schaltstation 2015 - der Kern"
date: "2016-09-10T14:26:54"
picture: "schaltstation2.jpg"
weight: "2"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: ["Glasmurmelbahn", "Energiezentrale", "Verteilung", "Elektroverteilung", "Netzteilanschluß"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/44350
imported:
- "2019"
_4images_image_id: "44350"
_4images_cat_id: "3275"
_4images_user_id: "1557"
_4images_image_date: "2016-09-10T14:26:54"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44350 -->
Hier ist die eigentliche Technik, die anderen 2 Schalter sitzen auf der Rückseite. Dort gibt es dann keine Netzteilbuchse und auch keine Steckbuchsen.

---

The switching staton itself, the 2 other switches are at the back. There are no low voltage receptacle and no plugs, of course.