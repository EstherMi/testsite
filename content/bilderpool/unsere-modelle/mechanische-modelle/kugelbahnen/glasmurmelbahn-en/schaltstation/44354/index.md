---
layout: "image"
title: "Schaltstation 2016"
date: "2016-09-10T14:26:54"
picture: "schaltstation6.jpg"
weight: "6"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: ["Glasmurmelbahn", "Energiezentrale", "Verteilung", "Elektroverteilung", "Netzteilanschluß"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/44354
imported:
- "2019"
_4images_image_id: "44354"
_4images_cat_id: "3275"
_4images_user_id: "1557"
_4images_image_date: "2016-09-10T14:26:54"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44354 -->
Die Seite mit den Ausgangsbuchsen. Ausser den 4 geschalteten Stromkreisen gibt es noch zweimal Dauerstrom.

---

This is the side with the plugs. Beside the 4 switched circuits now we have 2 unswitched circuits in addition.