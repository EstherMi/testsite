---
layout: "image"
title: "Schaltstation 2016"
date: "2016-09-10T14:26:54"
picture: "schaltstation5.jpg"
weight: "5"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: ["Glasmurmelbahn", "Energiezentrale", "Verteilung", "Elektroverteilung", "Netzteilanschluß"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/44353
imported:
- "2019"
_4images_image_id: "44353"
_4images_cat_id: "3275"
_4images_user_id: "1557"
_4images_image_date: "2016-09-10T14:26:54"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44353 -->
Die neue Schaltstation. Immer noch 4 Schalter in der gleichen Anordnung wie früher. Aber sonst ist alles neu; die Farbgebung, das Dach, die Position des Netzteilanschlusses und der Ausgangsbuchsen, die umlaufende Arbeitsbühne mit Geländer und Treppen und die Verdrahtung im inneren.

---

The new power station. Still 4 switches with the same arrangement as before. But the remainings are new: colours, the roof, positions of low voltage receptacle and plugs, operating platform with handrail and the stairs as well as the wiring inside.