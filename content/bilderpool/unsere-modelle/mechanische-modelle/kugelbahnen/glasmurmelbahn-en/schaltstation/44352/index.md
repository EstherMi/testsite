---
layout: "image"
title: "Schaltstation 2015 - Netzteilanschluß"
date: "2016-09-10T14:26:54"
picture: "schaltstation4.jpg"
weight: "4"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/44352
imported:
- "2019"
_4images_image_id: "44352"
_4images_cat_id: "3275"
_4images_user_id: "1557"
_4images_image_date: "2016-09-10T14:26:54"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44352 -->
Die Netzteilbuchse. Siehe auch "Pumpenhalter als Elektroanschluß" (https://www.ftcommunity.de/categories.php?cat_id=3149).

---

This is the DIY low voltage receptacle. See "Pumpenhalter als Elektroanschluß" (https://www.ftcommunity.de/categories.php?cat_id=3149) for construction details.