---
layout: "image"
title: "Schaltstation 2016"
date: "2016-09-10T14:26:54"
picture: "schaltstation7.jpg"
weight: "7"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: ["Glasmurmelbahn", "Energiezentrale", "Verteilung", "Elektroverteilung", "Netzteilanschluß"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/44355
imported:
- "2019"
_4images_image_id: "44355"
_4images_cat_id: "3275"
_4images_user_id: "1557"
_4images_image_date: "2016-09-10T14:26:54"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44355 -->
Die Schalter sind noch wie früher.

---

The switches are still the same old version.