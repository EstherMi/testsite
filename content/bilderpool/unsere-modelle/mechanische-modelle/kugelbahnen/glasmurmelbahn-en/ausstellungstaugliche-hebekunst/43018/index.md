---
layout: "image"
title: "Pneumatikschaltplan"
date: "2016-03-07T14:34:20"
picture: "Schleuse_klein.jpg"
weight: "1"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/43018
imported:
- "2019"
_4images_image_id: "43018"
_4images_cat_id: "3147"
_4images_user_id: "1557"
_4images_image_date: "2016-03-07T14:34:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43018 -->
"Der Federzylinder 133027 wird von einer der beiden Motorkurbeln angetrieben und der Kolben synchron zur Hubstangenbewegung herausgezogen und wieder losgelassen. Während des Kolbenhubes verhindert das Rückschlagventil 36970 (32061 geht auch), daß sich ein Vakuum bildet. Ein klitzekleinwenig Unterdruck entsteht dann doch und hilft auch dem widerborstigsten P-Betätiger 36075 dabei die Membran einzustülpen. Wenn der Kolben durch die Feder zurückgedrückt wird, entsteht ein geringer Druck, der die Membran der P-Betätiger ausstülpt. An einem bestimmten Punkt bleibt der Kolben stehen, da die Druckkraft auf die Kolbenfläche der Rückstellkraft der Feder entspricht - die Details mit der Reibung des Kolbens an der Zylinderwand sind natürlich auch noch im Spiel. An diesem Punkt knickt dann die Pleuelstange des Kolbens aus - siehe auch die Getriebebeschreibung.

Allfällige geringste Undichtigkeiten sorgen dafür, daß sich Schwankungen des äußeren Luftdrucks nicht auswirken können. Steht das Pneumatiksystem unter Druck entweicht die enthaltene Luft spätestens nach ein paar Minuten von alleine. Über das Rückschlagventil wird der Arbeitspunkt immer automatisch richtig eingestellt sobald der Antrieb anläuft."

Nachdem sich nun im Winter herausgestellt hat, daß die "allfälligen geringsten Undichtigkeiten" nun doch nicht zu einer ausreichenden Verbindung mit der Umgebung führen, gibt es eine Erweiterung in Form der Drossel (FC1). Fast ganz zugedreht dauert es einige Minuten bis sich ein eventueller Überdruck an den Umgebungsdruck angepaßt hat. Auf die Art kann sich ein möglicher Überdruck abbauen und das Rückschlagventil stellt den Arbeitspunkt ein.

Seit die Drossel eingebaut ist, hängt kein Betätiger mehr.

https://www.ftcommunity.de/details.php?image_id=41680

----

This is the schematic of the pneumatic circuit. Hope it is understandable without too much explanations - at least for the insiders.