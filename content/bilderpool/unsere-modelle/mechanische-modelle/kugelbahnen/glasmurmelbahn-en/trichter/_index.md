---
layout: "overview"
title: "Trichter"
date: 2019-12-17T19:20:21+01:00
legacy_id:
- categories/3176
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3176 --> 
Kein gewöhnlicher Trichter sondern ein sogenannter Exponential- oder Potentialtrichter. Manchmal muß es eben ein Fremdteil sein.

----

[b] Funnel [/b]

Not an ordinary funnel but a very special one. A so called exponential or potential funnel. Sometimes it has to be a non-ft part.