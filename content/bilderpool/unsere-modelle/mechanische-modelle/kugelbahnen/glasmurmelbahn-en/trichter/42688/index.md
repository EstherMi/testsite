---
layout: "image"
title: "Trichterauslauf"
date: "2016-01-08T13:30:54"
picture: "trichter4.jpg"
weight: "4"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/42688
imported:
- "2019"
_4images_image_id: "42688"
_4images_cat_id: "3176"
_4images_user_id: "1557"
_4images_image_date: "2016-01-08T13:30:54"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42688 -->
Das untere Ende des Trichters mündet in eine Art Auffangwanne. Dort werden die Murmeln, die den Trichter verlassen, auf Schienen zurückgeleitet. Viel ist im Bild davon nicht zu sehen, das markanteste Merkmal der Auffangkonstruktion ist das 'T' aus grauen BS30.

Die Schienen im Vordergrund gehören zur Tischtennisballstrecke.

----

Funnel outlet

The lower end of the funnel leaves the marbles into a special kind of collecting tray. The tray guides any marble to the tracks leading away. In the picture there is not much of this tray visible but the grey parts resembling a 'T'.

The tracks in the foreground belong to the ping-pong-ball track system.