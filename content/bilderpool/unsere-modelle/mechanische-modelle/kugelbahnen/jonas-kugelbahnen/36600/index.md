---
layout: "image"
title: "Muldenkipper-Aufzug Foto1 der Kugelbahn V5"
date: "2013-02-11T20:43:14"
picture: "Aufzug_mit_Muldenkipper.jpg"
weight: "1"
konstrukteure: 
- "Jonas"
fotografen:
- "Jonas"
keywords: ["Aufzug", "Mulde", "Jonas", "Kugelbahn"]
uploadBy: "x coaster"
license: "unknown"
legacy_id:
- details/36600
imported:
- "2019"
_4images_image_id: "36600"
_4images_cat_id: "2713"
_4images_user_id: "1608"
_4images_image_date: "2013-02-11T20:43:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36600 -->
Hier ist ein kleiner Aufzug. Die Mulde wird nach oben befördert und der Zylinder stupst die Mulde dann um und die Kugel fliegt raus 8siehe Foto 2).