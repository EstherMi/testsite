---
layout: "image"
title: "Muldenkipper-Aufzug Foto 2 der Kugelbahn V5"
date: "2013-02-11T20:43:15"
picture: "Aufzug_mit_Muldenkipper_Foto_2.jpg"
weight: "2"
konstrukteure: 
- "Jonas"
fotografen:
- "Jonas"
keywords: ["Kugelbahn", "Aufzug", "Mulde", "Jonas"]
uploadBy: "x coaster"
license: "unknown"
legacy_id:
- details/36601
imported:
- "2019"
_4images_image_id: "36601"
_4images_cat_id: "2713"
_4images_user_id: "1608"
_4images_image_date: "2013-02-11T20:43:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36601 -->
Beschreibung siehe Foto 1.

Die Mulde geht wieder automatisch in die Grundposition, wenn sie nach unten befördert wird.