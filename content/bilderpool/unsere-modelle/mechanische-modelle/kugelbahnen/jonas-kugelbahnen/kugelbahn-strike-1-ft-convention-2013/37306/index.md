---
layout: "image"
title: "Armaufzug der Kugelbahn STRIKE 1"
date: "2013-09-07T12:59:47"
picture: "Arm-Aufzug_2.jpg"
weight: "5"
konstrukteure: 
- "Jonas"
fotografen:
- "Jonas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "x coaster"
license: "unknown"
legacy_id:
- details/37306
imported:
- "2019"
_4images_image_id: "37306"
_4images_cat_id: "2777"
_4images_user_id: "1608"
_4images_image_date: "2013-09-07T12:59:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37306 -->
Der Armaufzug der Kugelbahn STRIKE 1.
Die Kugeln werden immer einen "Arm" weiter nach oben transportiert. Die "Arme" drehen sich immer hin und her.
Video zum "Armaufzug ": http://youtu.be/WnrhKpwymj8