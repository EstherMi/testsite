---
layout: "image"
title: "Weiche 3 der Kugelbahn Strike 1"
date: "2013-09-07T12:59:47"
picture: "Weiche-3.jpg"
weight: "14"
konstrukteure: 
- "Jonas"
fotografen:
- "Jonas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "x coaster"
license: "unknown"
legacy_id:
- details/37315
imported:
- "2019"
_4images_image_id: "37315"
_4images_cat_id: "2777"
_4images_user_id: "1608"
_4images_image_date: "2013-09-07T12:59:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37315 -->
Weiche 3 der Kugelbahn Strike 1.
Dort fliegen die Kugeln entweder rüber auf die eine Strecke oder sind zu langsam und fallen in die andere Strecke. Die Kugeln die nicht rüber fliegen werden vorher mit einer Pneumatiksperre verlangsamt.