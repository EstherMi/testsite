---
layout: "image"
title: "Drehaufzug der Kugelbahn STRIKE 1"
date: "2013-09-07T12:59:46"
picture: "Drehaufzug.jpg"
weight: "3"
konstrukteure: 
- "Jonas"
fotografen:
- "Jonas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "x coaster"
license: "unknown"
legacy_id:
- details/37304
imported:
- "2019"
_4images_image_id: "37304"
_4images_cat_id: "2777"
_4images_user_id: "1608"
_4images_image_date: "2013-09-07T12:59:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37304 -->
Der Drehaufzug der Kugelbahn STRIKE 1. 
Die Kugeln kommen in die "Kapsel", die Kapsel dreht sich nach oben und die Kugel fliegt raus auf die Bahn.