---
layout: "image"
title: "Loren-Aufzug mit Schieber der Kugelbahn STRIKE 1"
date: "2013-09-07T12:59:47"
picture: "Schieber-Lore-Aufzug.jpg"
weight: "8"
konstrukteure: 
- "Jonas"
fotografen:
- "Jonas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "x coaster"
license: "unknown"
legacy_id:
- details/37309
imported:
- "2019"
_4images_image_id: "37309"
_4images_cat_id: "2777"
_4images_user_id: "1608"
_4images_image_date: "2013-09-07T12:59:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37309 -->
Der Loren-Aufzug mit Schieber der Kugelbahn STRIKE 1.
Die Kugeln kommen in die Lore, werden dann nach oben transportiert und bleiben an einem Magneten hängen. Von dort werden sie dann mit einem Schieber zurück auf die Bahn  geschoben.

Video: http://youtu.be/Rl1LMT8sZ34