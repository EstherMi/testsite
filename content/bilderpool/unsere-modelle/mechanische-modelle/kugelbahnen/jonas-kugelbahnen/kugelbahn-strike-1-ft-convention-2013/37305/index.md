---
layout: "image"
title: "Armaufzug der Kugelbahn STRIKE 1"
date: "2013-09-07T12:59:47"
picture: "Arm-Aufzug.jpg"
weight: "4"
konstrukteure: 
- "Jonas"
fotografen:
- "Jonas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "x coaster"
license: "unknown"
legacy_id:
- details/37305
imported:
- "2019"
_4images_image_id: "37305"
_4images_cat_id: "2777"
_4images_user_id: "1608"
_4images_image_date: "2013-09-07T12:59:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37305 -->
Der Armaufzug der Kugelbahn STRIKE 1.
Die Kugeln werden immer einen "Arm" weiter nach oben transportiert. Die "Arme" drehen sich immer hin und her.
Video zum "Armaufzug ":  Armaufzug der Kugelbahn STRIKE 1