---
layout: "image"
title: "Kippaufzug der Kugelbahn STRIKE 1"
date: "2013-09-07T12:59:47"
picture: "Kippaufzug.jpg"
weight: "6"
konstrukteure: 
- "Jonas"
fotografen:
- "Jonas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "x coaster"
license: "unknown"
legacy_id:
- details/37307
imported:
- "2019"
_4images_image_id: "37307"
_4images_cat_id: "2777"
_4images_user_id: "1608"
_4images_image_date: "2013-09-07T12:59:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37307 -->
Der Kippaufzug der Kugelbahn STRIKE 1.
Die Kugeln kommen in die "Röhre" rein und ein Pneumatikzylinder drückt die "Röhre" nach oben. So rollt die Kugel zurück auf die Bahn.