---
layout: "image"
title: "1ste Pneumatikweiche---der Kugelbahn V5"
date: "2013-02-09T11:50:06"
picture: "Kugelbahn3.jpg"
weight: "9"
konstrukteure: 
- "Jonas"
fotografen:
- "Jonas"
keywords: ["Kugelbahn", "Jonas", "Weiche"]
uploadBy: "x coaster"
license: "unknown"
legacy_id:
- details/36591
imported:
- "2019"
_4images_image_id: "36591"
_4images_cat_id: "2713"
_4images_user_id: "1608"
_4images_image_date: "2013-02-09T11:50:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36591 -->
Hier ist die erste Pneumatikweiche.
Gerade eben habe ich eine Motorisirte Weiche gebaut, aber noch nicht fotografiert.