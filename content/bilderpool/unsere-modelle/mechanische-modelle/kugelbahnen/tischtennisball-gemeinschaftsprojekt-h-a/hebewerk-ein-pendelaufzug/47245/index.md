---
layout: "image"
title: "Hebewerk - Schaltplan"
date: "2018-02-04T20:58:46"
picture: "hebewerk4.jpg"
weight: "4"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: ["Gemeinschaftsprojekt", "Tischtennisballmaschine", "Hebewerk", "Pendelaufzug"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/47245
imported:
- "2019"
_4images_image_id: "47245"
_4images_cat_id: "3496"
_4images_user_id: "1557"
_4images_image_date: "2018-02-04T20:58:46"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47245 -->
Ja, das ist alles - keine Silberlinge, kein Interface, kein Controller oder sonstiges Brimborium - nur zwei Motoren, 4 Taster und ein Schwung Litze:
* M2 läuft kontinuierlich und steuert die Bestromung von M1.
* M1 dreht abwechselnd links oder rechts.
* M1 stoppt in der jeweiligen Endlage.
* M1 erreicht die jeweilige Endlage bevor via M2 die Drehrichtung wechselt und so gibt es auch ein wenig Aufenthalt an den Endpunkten.
Dazu kommen noch ein paar Zahnräder und ein Maltesersatz (6:1) veredelt das Ganze.
Alles zusammen ist so ausgesucht und eingestellt, dass ein kompletter Zyklus ziemlich genau 12 s benötigt. Im Mittel kommt alle 2 s ein Ball herausgerollt. Tatsächlich sind es 3er Gruppen alle 6 s. Das nachfolgende Modul muss damit dann halt zu Rande kommen, war aber in Dreieich kein Problem.

Und wie zuverlässig diese Steinzeitsteuerungstechnik funktioniert, zeigt Euch dieses kleine Video (Danke an Sisyphos für's Schneiden und Hosten):
https://youtu.be/ZYH144aMy8Y

=====================================

This is all the stuff needed - no "Silberlinge", no interfaces, no micro controller or other fancy stuff - just 2 motors, 4 pushbuttons and some wiring:
* M2 turns continuously and controls M1.
* M1 alternately turns clockwise or counterclockwise.
* M1 stops at the respective end postition.
* M1 reaches the end positions some time before M2 commands the directional change. Thus there is some resting time at the end positions.
In addition we have some gears and the geneva drive (6:1) finalizes the design. All together is selected and tuned to get one complete cycle close to 12 s. As an average there is one ball every 2 s. Practically 3 balls grouped together leave every 6 s. The next module downstream needs to handle this property. In Dreieich there was no issue.

How fine this 'stone age control system' works you can see at the clip:
https://youtu.be/ZYH144aMy8Y
Courtesy applies to Sisyphos for cutting and hosting.

Vocabulary ;)
----------
rot = red
grün = green
gelb = yellow
schwarz = black
blau = blue
grau = grey

Linkslauf = counterclockwise rotation
Rechtslauf = clockwise rotation
Endtaster Links = stop position pushbutton counterclockwise
Endtaster Rechts = stop position pushbutton clockwise