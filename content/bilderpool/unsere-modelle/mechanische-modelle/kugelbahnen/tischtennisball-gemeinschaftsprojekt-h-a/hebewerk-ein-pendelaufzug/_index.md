---
layout: "overview"
title: "Hebewerk - ein Pendelaufzug"
date: 2019-12-17T19:20:31+01:00
legacy_id:
- categories/3496
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3496 --> 
Inspiriert von den grossen Schiffshebewerken (daher der Name) hebt dieser Aufzug wechselseitig bis zu 3 Tischtennisbälle hoch. Die Steuerung erfolgt dabei elektromechanisch und die restliche Mechanik ist recht minimalistisch gehalten.