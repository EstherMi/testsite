---
layout: "image"
title: "Einlauf"
date: "2015-07-04T21:56:09"
picture: "kugelaufnahme10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41389
imported:
- "2019"
_4images_image_id: "41389"
_4images_cat_id: "3075"
_4images_user_id: "104"
_4images_image_date: "2015-07-04T21:56:09"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41389 -->
Schließlich kommen sie hier mit hinreichend gleichmäßiger und geringer Geschwindigkeit in der Kugelaufnahmestation an. (Die Teile auf der Bauplatte 500 rechts im Bild gehören nicht zum Brunnen.)