---
layout: "comment"
hidden: true
title: "20619"
date: "2015-05-10T23:46:37"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Toll - ein Wasserrad, endlich! Früher mussten wir dafür an den Bach im Wald, heute darf der Zierbrunnen herhalten (hoffentlich wird diese technische Aufwertung von der besseren Hälfte nicht als ästhetische Freveltat eingestuft...).
Gruß, Dirk