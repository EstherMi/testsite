---
layout: "image"
title: "Antriebsseite"
date: "2015-05-08T17:37:28"
picture: "brunnenkugelbahn3.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/40950
imported:
- "2019"
_4images_image_id: "40950"
_4images_cat_id: "3075"
_4images_user_id: "104"
_4images_image_date: "2015-05-08T17:37:28"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40950 -->
Das primitiv aufgebaute Wasserrad (ein gedachterweise schlauer gebautes mit Schaufelform funktionierte nicht besser) geht über eine 1:3-Untersetzung auf den Aufzug.

Wenn man aber mit dem Gartenschlauf aufs Wasserrad geht, dann rennt das Teil wie Hölle ;-)