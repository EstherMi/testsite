---
layout: "image"
title: "Eine Kugel auf zwei Laufschienen"
date: "2014-03-07T10:45:14"
picture: "ftmaennchenspieltkugelbahn4.jpg"
weight: "4"
konstrukteure: 
- "Thorste_n"
fotografen:
- "Thorste_n"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thorste_n"
license: "unknown"
legacy_id:
- details/38438
imported:
- "2019"
_4images_image_id: "38438"
_4images_cat_id: "2864"
_4images_user_id: "2138"
_4images_image_date: "2014-03-07T10:45:14"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38438 -->
http://www.youtube.com/watch?v=3fEk4EjfYXA
