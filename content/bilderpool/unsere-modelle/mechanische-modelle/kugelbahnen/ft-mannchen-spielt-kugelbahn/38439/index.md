---
layout: "image"
title: "Kugel - Mitnehmer"
date: "2014-03-07T10:45:14"
picture: "ftmaennchenspieltkugelbahn5.jpg"
weight: "5"
konstrukteure: 
- "Thorste_n"
fotografen:
- "Thorste_n"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thorste_n"
license: "unknown"
legacy_id:
- details/38439
imported:
- "2019"
_4images_image_id: "38439"
_4images_cat_id: "2864"
_4images_user_id: "2138"
_4images_image_date: "2014-03-07T10:45:14"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38439 -->
Die Kugel liegt sehr stabil zwischen Eckstein 15 und Bogenstück. 
