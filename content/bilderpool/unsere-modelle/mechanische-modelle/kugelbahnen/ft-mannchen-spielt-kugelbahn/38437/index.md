---
layout: "image"
title: "ft Männchen bringt die Kugeln"
date: "2014-03-07T10:45:14"
picture: "ftmaennchenspieltkugelbahn3.jpg"
weight: "3"
konstrukteure: 
- "Thorste_n"
fotografen:
- "Thorste_n"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thorste_n"
license: "unknown"
legacy_id:
- details/38437
imported:
- "2019"
_4images_image_id: "38437"
_4images_cat_id: "2864"
_4images_user_id: "2138"
_4images_image_date: "2014-03-07T10:45:14"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38437 -->
Das ft Männchen darf - wenn alle Kinder schlafen - auch Kugelbahn spielen.

www.youtube.com/watch?v=3fEk4EjfYXA


