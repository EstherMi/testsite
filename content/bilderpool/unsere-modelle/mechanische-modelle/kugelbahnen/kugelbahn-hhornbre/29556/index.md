---
layout: "image"
title: "Kugelbahn_HH_01"
date: "2010-12-31T10:59:39"
picture: "FT-Kugelbahn_HH_01.jpg"
weight: "1"
konstrukteure: 
- "Holger Horn"
fotografen:
- "Holger Horn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "HHornBre"
license: "unknown"
legacy_id:
- details/29556
imported:
- "2019"
_4images_image_id: "29556"
_4images_cat_id: "2151"
_4images_user_id: "1254"
_4images_image_date: "2010-12-31T10:59:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29556 -->
Mein "Wiedereinstieg" in das Bauen mit Fischertechnik.