---
layout: "image"
title: "Pneumatik-Schieber (6)"
date: "2017-10-16T19:45:36"
picture: "ttballmaschinen08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46808
imported:
- "2019"
_4images_image_id: "46808"
_4images_cat_id: "3465"
_4images_user_id: "104"
_4images_image_date: "2017-10-16T19:45:36"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46808 -->
Hier sieht man links auch den PowerBlock, mit dem man die Arbeitsgeschwindigkeit in weiten Grenzen justieren kann. Als schnellstes habe ich eine Zykluszeit von 1,3 Sekunden gemessen - da kamen die Kompressoren gerade noch nach.