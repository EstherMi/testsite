---
layout: "image"
title: "Pneumatik-Schieber (2)"
date: "2017-10-16T19:45:36"
picture: "ttballmaschinen04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46804
imported:
- "2019"
_4images_image_id: "46804"
_4images_cat_id: "3465"
_4images_user_id: "104"
_4images_image_date: "2017-10-16T19:45:36"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46804 -->
Hier ist der erste Zylinder eingefahren, was bewirkt, dass der erste Schieber ausgefahren ist. Die ganzen "Hilfsbleche" dienen alle der "Unfallverhütung", falls doch mal ein Ball mit ungünstigem Timing ankommen und irgendwie überspringen sollte. Er fällt damit immer wieder auf die Bahn zurück. Die Maschine verträgt es sogar, mit Bällen von oben überschüttet zu werden, und räumt alles schön nacheinander wieder weg. Die gelben Streben verhindern, dass ein Ball unter die schwarzen Streben gelangt, wenn die gerade ausgefahren sind. Deshalb gibt es auch auf zwei Seiten die blauen "Schutzbleche", damit möglichst kein Convention-Besucher da seine Wand in den Weg hält, wenn die gelbe Strebe zurück und damit etwas aus dem Modell herausfährt.