---
layout: "image"
title: "Korbwerfer (1)"
date: "2017-10-16T19:45:36"
picture: "ttballmaschinen09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46809
imported:
- "2019"
_4images_image_id: "46809"
_4images_cat_id: "3465"
_4images_user_id: "104"
_4images_image_date: "2017-10-16T19:45:36"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46809 -->
Hier kommen die Bälle in den Eingang des Korbwerfer-Moduls. All die Führungsbleche etc. dienen wieder dazu, auch in widrigsten Situationen ja keinen Ball aus der Maschine zu feuern oder in die Mechanik hinein zu verlieren. Der waagerecht liegende Hebel mit dem WS30° drauf in Bildmitte wird per Motorkraft so gedreht, dass der WS30° nach unten wandert, und dann schlagartig losgelassen. Gummizüge lassen ihn dann wieder hochschnalzen, und der WS30° kickt die Bälle hoch durch die Luft in den Korb oder zumindest in die große Auffang-Wanne.