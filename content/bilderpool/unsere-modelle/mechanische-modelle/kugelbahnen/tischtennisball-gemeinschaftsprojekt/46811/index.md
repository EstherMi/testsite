---
layout: "image"
title: "Korbwerfer (3)"
date: "2017-10-16T19:45:36"
picture: "ttballmaschinen11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46811
imported:
- "2019"
_4images_image_id: "46811"
_4images_cat_id: "3465"
_4images_user_id: "104"
_4images_image_date: "2017-10-16T19:45:36"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46811 -->
Keines der Führungsbleche ist überflüssig ;-) Es kam beim Test vor, dass nach einer Stunde problemlosen Dauerbetriebs sich plötzlich ein Ball auf der Bauplatte 1000 fand - und ich nicht gesehen hatte, in welcher Situation der da hin gelangte. Das hat ein paar graue Haare mehr gebracht ;-)