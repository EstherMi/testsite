---
layout: "image"
title: "Pneumatik-Schieber (5)"
date: "2017-10-16T19:45:36"
picture: "ttballmaschinen07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46807
imported:
- "2019"
_4images_image_id: "46807"
_4images_cat_id: "3465"
_4images_user_id: "104"
_4images_image_date: "2017-10-16T19:45:36"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46807 -->
Zwei ft-Kompressoren und ein Lufttank versorgen die Anlage.