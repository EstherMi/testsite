---
layout: "image"
title: "Korbwerfer (6)"
date: "2017-10-16T19:45:36"
picture: "ttballmaschinen14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46814
imported:
- "2019"
_4images_image_id: "46814"
_4images_cat_id: "3465"
_4images_user_id: "104"
_4images_image_date: "2017-10-16T19:45:36"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46814 -->
Vom Z40 geht es - wieder kraftschlüssig über drei Metallachsen) auf ein Z30, was schließlich das Z40 des Wurfarms antreibt. Oberhalb der Kurvenscheibe ist der Endschalter für das Feststellen der Umdrehung der Kurvenscheibe.