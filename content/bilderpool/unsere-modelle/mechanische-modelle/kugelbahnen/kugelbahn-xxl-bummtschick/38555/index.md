---
layout: "image"
title: "05 basket"
date: "2014-04-16T15:10:50"
picture: "05.jpg"
weight: "5"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- details/38555
imported:
- "2019"
_4images_image_id: "38555"
_4images_cat_id: "2880"
_4images_user_id: "2106"
_4images_image_date: "2014-04-16T15:10:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38555 -->
This shows the basket mentioned with picture 03, which empties itself when three balls have piled up in it. All three balls are then dropped onto the red plane visible at the front right. The plane is slightly tilted so the balls then run slowly towards the back, where another green track guides them to red plane no. 2 on the left (visible in the back).