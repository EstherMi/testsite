---
layout: "image"
title: "03 grand curves"
date: "2014-04-16T15:10:50"
picture: "03.jpg"
weight: "3"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- details/38553
imported:
- "2019"
_4images_image_id: "38553"
_4images_cat_id: "2880"
_4images_user_id: "2106"
_4images_image_date: "2014-04-16T15:10:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38553 -->
View of the grand curves at the top. The idea was to drop the ball at extreme speed onto the blue track, which goes uphill again and then round and round in a slow descent until the balls are dropped into the basket at the front center. That basket is balanced on a joint so that it tips over when three balls have been dropped (see picture 5).