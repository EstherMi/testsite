---
layout: "image"
title: "12 barrier"
date: "2014-04-16T15:10:50"
picture: "12.jpg"
weight: "11"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- details/38561
imported:
- "2019"
_4images_image_id: "38561"
_4images_cat_id: "2880"
_4images_user_id: "2106"
_4images_image_date: "2014-04-16T15:10:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38561 -->
Here the barrier / release construct from the original ft model is clearly visible. The difference is the additional switch B added before it, at the side of the model (visible on the right, and on the previous picture 11).