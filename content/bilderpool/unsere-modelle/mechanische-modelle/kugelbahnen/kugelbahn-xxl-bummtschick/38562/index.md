---
layout: "image"
title: "13 other side"
date: "2014-04-16T15:10:50"
picture: "13.jpg"
weight: "12"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- details/38562
imported:
- "2019"
_4images_image_id: "38562"
_4images_cat_id: "2880"
_4images_user_id: "2106"
_4images_image_date: "2014-04-16T15:10:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38562 -->
View of the other side. The track at the bottom is the continuation of the steep track coming from switch A (see picture 09).