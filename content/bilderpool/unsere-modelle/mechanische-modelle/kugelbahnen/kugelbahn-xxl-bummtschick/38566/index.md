---
layout: "image"
title: "17 finale"
date: "2014-04-16T15:10:50"
picture: "17.jpg"
weight: "16"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- details/38566
imported:
- "2019"
_4images_image_id: "38566"
_4images_cat_id: "2880"
_4images_user_id: "2106"
_4images_image_date: "2014-04-16T15:10:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38566 -->
The finale (bottom right from the front). Here all three tracks come together again, and the balls are queuing to be transported again.