---
layout: "overview"
title: "Kugelbahn XXL (bummtschick)"
date: 2019-12-17T19:20:02+01:00
legacy_id:
- categories/2880
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2880 --> 
My own Kugelbahn which has grown over the last ten months or so. This is originally model 3 from the ft "Dynamic" kit, some bits of which are left at the front right, but most has been replaced and much enlarged. Enjoy!