---
layout: "image"
title: "16 loop"
date: "2014-04-16T15:10:50"
picture: "16.jpg"
weight: "15"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- details/38565
imported:
- "2019"
_4images_image_id: "38565"
_4images_cat_id: "2880"
_4images_user_id: "2106"
_4images_image_date: "2014-04-16T15:10:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38565 -->
Front view of the bottom. On the top, the steep track from switch A continues to the right (and leads to the release of the barrier construct from the original ft "Dynamics" model). In the middle is the loop-the-loop also left over from the original model. At the bottom is the end third track, coming from the catchment container at the back (see picture 16).