---
layout: "image"
title: "15 jump"
date: "2014-04-16T15:10:50"
picture: "15.jpg"
weight: "14"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- details/38564
imported:
- "2019"
_4images_image_id: "38564"
_4images_cat_id: "2880"
_4images_user_id: "2106"
_4images_image_date: "2014-04-16T15:10:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38564 -->
The blue track is the fast track coming from switch B, and the ball is so fast that it is possible to make it jump into the air and have it land on the yellow box on the right with a loud click. There is a photo sensor in the yellow pillar on the left so that every ball passing by will trigger a signal in the "Silberlinge" visible in a front view (see picture 07). This is a classic light barrier ("Lichtschranke") with an added Monoflop so that every time a ball comes by, a light is switched on for a couple of seconds to illuminate the yellow box for special effect.