---
layout: "image"
title: "14 side release"
date: "2014-04-16T15:10:50"
picture: "14.jpg"
weight: "13"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- details/38563
imported:
- "2019"
_4images_image_id: "38563"
_4images_cat_id: "2880"
_4images_user_id: "2106"
_4images_image_date: "2014-04-16T15:10:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38563 -->
Close up of the track coming from the "release" part of the original ft barrier / release construct. This kind of track was not in the original model though.