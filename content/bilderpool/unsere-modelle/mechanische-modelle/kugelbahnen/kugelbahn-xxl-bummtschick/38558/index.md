---
layout: "image"
title: "08 switch A"
date: "2014-04-16T15:10:50"
picture: "08.jpg"
weight: "8"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- details/38558
imported:
- "2019"
_4images_image_id: "38558"
_4images_cat_id: "2880"
_4images_user_id: "2106"
_4images_image_date: "2014-04-16T15:10:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38558 -->
This is a rear view of switch A, visible in the background. The track from plane 1 (mentioned with picture 07) arrives in the back from the right. The track on the right goes into another steep descent, the track on the left leads to switch B.