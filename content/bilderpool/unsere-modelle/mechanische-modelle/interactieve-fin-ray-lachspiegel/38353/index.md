---
layout: "image"
title: "Transmissie-motor-aandrijving"
date: "2014-02-23T12:10:17"
picture: "finraylachspiegel09.jpg"
weight: "9"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/38353
imported:
- "2019"
_4images_image_id: "38353"
_4images_cat_id: "2852"
_4images_user_id: "22"
_4images_image_date: "2014-02-23T12:10:17"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38353 -->
Transmissie-motor-aandrijving van Pollin. 
Deze heeft ca. 7 omw/min. en een zwaai-arm van ca. 35 cm waarvan de draaihoek-positie middels een 5K Potmeter (= I1) continue wordt gemeten.  

Vanwege het benodigde koppel functioneert (helaas) alleen een V=8 snelheid-instelling goed. 
Zie p29:  http://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2012-4.pdf

