---
layout: "image"
title: "zwaai-arm waarvan draaihoek-positie middels een 5K Potmeter wordt gemeten"
date: "2014-02-23T12:10:17"
picture: "finraylachspiegel08.jpg"
weight: "8"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/38352
imported:
- "2019"
_4images_image_id: "38352"
_4images_cat_id: "2852"
_4images_user_id: "22"
_4images_image_date: "2014-02-23T12:10:17"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38352 -->
In verband met de eenvoud en compactheid ben ik uiteindelijk uitgekomen op een tussen de spiegels opgestelde transmissie-motor-aandrijving.  
Deze heeft ca. 7 omw/min. en een zwaai-arm van ca. 35 cm waarvan de draaihoek-positie middels een 5K Potmeter (= I1) continue wordt gemeten.