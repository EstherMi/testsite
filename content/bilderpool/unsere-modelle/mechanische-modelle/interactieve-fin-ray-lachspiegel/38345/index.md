---
layout: "image"
title: "Interactieve Fin-Ray Lachspiegel"
date: "2014-02-23T12:10:16"
picture: "finraylachspiegel01.jpg"
weight: "1"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/38345
imported:
- "2019"
_4images_image_id: "38345"
_4images_cat_id: "2852"
_4images_user_id: "22"
_4images_image_date: "2014-02-23T12:10:16"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38345 -->
Voor m'n lachspiegel heb ik 2 spiegelplaten van 0,5 x 1m middels Fischertechnik I-spanten op regelmatige afstanden aan elkaar verbonden ten behoeve van het Fin-Ray-principe. 

Afhankelijk van de instel-positie en/of het gekozen RoboPro-Programma van de Fin-Ray Lachtspiegel wordt aan de ene zijde de bolling en aan de andere zijde de holling van de spiegel groter, kleiner of omgekeerd.  

Youtube-link:
http://www.youtube.com/watch?v=hk6pu6vlWG8&list=UUjudqZ8_PVUpZRRLsSIJCPA&feature=share


Pijlstaartrog met Fin-Ray-principe :
http://www.ftcommunity.de/categories.php?cat_id=2825

Adaptive Gripper met Fin-Ray-principe :
http://www.ftcommunity.de/categories.php?cat_id=2775

Fischertechnik-Qualle mit Fin-Ray-Effect :
http://www.ftcommunity.de/categories.php?cat_id=2772