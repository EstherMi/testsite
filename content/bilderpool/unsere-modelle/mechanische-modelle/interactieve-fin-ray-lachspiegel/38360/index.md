---
layout: "image"
title: "Spiegeltje, spiegeltje aan de wand, wie is het schoonste van heel het land ?...."
date: "2014-02-23T12:10:17"
picture: "finraylachspiegel16.jpg"
weight: "16"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/38360
imported:
- "2019"
_4images_image_id: "38360"
_4images_cat_id: "2852"
_4images_user_id: "22"
_4images_image_date: "2014-02-23T12:10:17"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38360 -->
Bolle spiegel......