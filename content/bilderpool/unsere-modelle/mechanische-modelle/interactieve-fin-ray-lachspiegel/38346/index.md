---
layout: "image"
title: "Spiegelplaten met I-spanten op regelmatige afstanden t.b.v. Fin-Ray-principe"
date: "2014-02-23T12:10:17"
picture: "finraylachspiegel02.jpg"
weight: "2"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/38346
imported:
- "2019"
_4images_image_id: "38346"
_4images_cat_id: "2852"
_4images_user_id: "22"
_4images_image_date: "2014-02-23T12:10:17"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38346 -->
Polystyreen Spiegelplaten 1000x500x1,5mm en andere interessante zaken zijn verkrijgbaar bij : 
Kunststofshop.nl , Didamseweg 150, 6902 PE Zevenaar NL    
info@kunststofshop.nl

