---
layout: "image"
title: "Balkenwaage in Eighties-Classic-Line (Classic-80-Line)"
date: "2009-07-09T15:39:46"
picture: "IMG_1329b.jpg"
weight: "2"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- details/24523
imported:
- "2019"
_4images_image_id: "24523"
_4images_cat_id: "1687"
_4images_user_id: "740"
_4images_image_date: "2009-07-09T15:39:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24523 -->
Modelle, die nach der „Eighties-Classic-Line“ aufgebaut werden, dürfen neben den unveränderten Bauteilen der 1. Baukasten-Generation auch aus neueren oder veränderten Bauteilen der 2. bis 4. Baukasten-Generation (gemäß ft-History, von ca. 1974 bis ca. 1989) bestehen. 


Haupterkennungsmerkmale:
Haupterkennungsmerkmal des Linie ist die ist die ebenfalls noch grau-rote Modellfarbgebung.
Grundbausteine und Statikteile sind immer noch in Grau gehalten. Auch fast alle anderen Bauteile sind noch in Rot gehalten. Umfangreiche Bauteilneuheiten, die überwiegend in Rot gehalten waren, runden das Erscheinungsbild ab.


Markante Besonderheiten der Bauteile:

- Die roten Flachsteine 30 und 60 besitzen ab ca. 1972 eine V-förmige Anschlussnut (vorher T-förmig)
- Der graue Baustein 30 mit Bohrung besitzt ab ca. 1972 eine eckige Kreuzbohrung (vorher rund)
- Alle Zahnräder (auch die an den grauen Motor-Getrieben) werden ab 1977 nur noch in Schwarz hergestellt.
- Die Anschlussfeder (Clip-Anschluss) an den roten Bauplatten (Verkleidungsplatten) werden ab 1977 durch Anschlusszapfen ersetzt
- Viele neue Bauteile erhalten Einzug ins Programm z.B.: Reifen 60, Baustein 5 und 7,5, Winkelbaustein, Baustein V15 Eck, kleine Grundplatten (45x45/90), gelochte graue Statik-Streben, mehrere verschiedene rote Kupplungsstücken, u.v.a.
- Grundplatten, Naben, Drehscheibe und Winkelsteine sind von nun an in einer neuen matten, roten Farbgebung, zusätzlich erhalten die Grundplatten ab 1977 mehrere mittig angebrachte Zusatzbohrungen.
- Ab 1977 erhalten graue Kunststoffachsen Einzug ins Programm
- Die ersten roten Statik-Anbauteile ersetzen die grauen (Kreuz-, Eck- und Doppelknotenplatte, Lasche 15 und 21,3)
- Bereits Ende der 80er-Jahre kommen die ersten gelben Statikstreben (erst nur als Ergänzung, ausnahmslos in den Modellbaukästen) auf den Markt
- Ein absolutes No-Go, wie ich finde, war die kurzzeitige Einführung von dunkelroten Teilen Ende der 80er-Jahre.

Leider sind auch einige Bauteile in der 2. bis 4. Generation verschwunden:

- Der lange Seilhaken (31025) wird durch den kürzeren Haken (38225) ersetzt
- Die Statikknotenplatte (36328) wird durch die etwas größere Doppelknotenplatte (35797) ersetzt
- Der Winkelstein (31012), die Achskupplung (31036) und der Statik-Drehflügel (36681) verschwinden komplett aus dem Programm