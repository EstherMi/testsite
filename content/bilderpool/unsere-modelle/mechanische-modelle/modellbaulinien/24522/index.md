---
layout: "image"
title: "Balkenwaage in Ur-Classic-Line (Classic-70-Line)"
date: "2009-07-09T15:24:36"
picture: "IMG_1326b.jpg"
weight: "1"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- details/24522
imported:
- "2019"
_4images_image_id: "24522"
_4images_cat_id: "1687"
_4images_user_id: "740"
_4images_image_date: "2009-07-09T15:24:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24522 -->
Modelle die nach der „Ur-Classic-Line“ aufgebaut werden, bestehen ausschließlich aus Bauteilen der 1. Baukasten-Generation (gemäß ft-History, 1966 bis ca. 1974) 

Haupterkennungsmerkmale:

Das Haupterkennungsmerkmal dieser Linie ist die klassisch grau-rote Modellfarbgebung. Grundbausteine und Statikteile der Modelle sind in Grau gehalten, fast alle anderen Bauteile sind in Rot gehalten.
 

Markante Besonderheiten der Bauteile:
- Der graue Baustein 30 mit Bohrung besitzt noch eine runde Kreuzbohrung (später eckig)
- Der rote Flachstein 30 und 60 besitzt jeweils noch zwei T-förmige Anschlussnuten (später V-förmig)
- Die roten Verbindungsstücke 15, 30 und 45 besitzen noch auf beiden Seiten gleichförmige Anschlussnuten. (später wurde eine Anschlussnut schwalbenschwanzförmig ausgearbeitet)
- Alle Zahnräder sind noch in Rot gefertigt (später schwarz)
- Die ansetzbaren Motorgetriebeteile waren noch ausnahmslos in Grau gefertigt und mit roten Zahnräder bestückt (später schwarz)
- Grundplatten, Naben, Drehscheibe und Winkelsteine besitzen noch ihre markante rot-glänzende Farbgebung (später matt-rot)

Auf welche Bauteile sollte in dieser Linie verzichtet werden?
- Graue Kunststoffachsen (wurden erst 1977 eingeführt)
- Schwarze Zahnräder (wurden erst 1977 eingeführt)
- Graue Motorgetriebeteile, die mit schwarzen Zahnräder bestückt sind (wurden auch erst um 1977 eingeführt)