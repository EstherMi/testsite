---
layout: "overview"
title: "Modellbaulinien"
date: 2019-12-17T19:24:25+01:00
legacy_id:
- categories/1687
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1687 --> 
Mit jeder Baukasten-Generation kamen stets auch neue oder veränderte Bauteile sowie neue Bauteilfarben auf den Markt.
Die dadurch gewonnene Teilevielfalt verhalf, gewisse Problemstellungen leichter bzw. überhaupt erst lösen zu können. Zudem brachten die neuen Bauteilfarben Abwechselung und neue Farbschemen in die Modelle.

Eine Modellbaulinie ist nun ein ausgeglichenes Zusammenspiel zwischen dem Bauteil-Farbschema und einer epochengerechten Bauteilauswahl.

Ein ganz besonderer Reiz kann darin bestehen, Modelle nach vorgegeben Modellbaulinien zu erstellen oder nachzubauen.

In den folgenden Modellen versuche ich Euch mal die wichtigsten Modellbaulinien, anhand einer 
Balkenwaage, die ursprünglich aus der Bauanleitung eines Universal-II-Baukastens stammt, vorzustellen.