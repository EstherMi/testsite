---
layout: "image"
title: "Balkenwaage in New-Classic-Line"
date: "2009-07-09T15:50:32"
picture: "IMG_1349b.jpg"
weight: "5"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- details/24526
imported:
- "2019"
_4images_image_id: "24526"
_4images_cat_id: "1687"
_4images_user_id: "740"
_4images_image_date: "2009-07-09T15:50:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24526 -->
In der New-Classic-Line kommen Bauteile mit klassischer Farbprägung, also graue Grundbausteine und Statikteile, sowie alles was rot ist (egal aus welcher Generation) zum Einsatz.
Wenn nicht anders möglich sind auch schwarze Kunststoffachsen (auch Rastmaterial) und schwarze Zahnräder und sonstige nur in Schwarz gefertigte Spezialteile (z.B. Reedkontakt) erlaubt.

No-Go: Schwarze und gelbe Grund- und Statikbauteile.