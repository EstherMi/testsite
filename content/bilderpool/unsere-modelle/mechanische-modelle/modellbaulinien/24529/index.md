---
layout: "image"
title: "Alle Baulinien im Überblick"
date: "2009-07-09T17:00:33"
picture: "IMG_1365b.jpg"
weight: "8"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- details/24529
imported:
- "2019"
_4images_image_id: "24529"
_4images_cat_id: "1687"
_4images_user_id: "740"
_4images_image_date: "2009-07-09T17:00:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24529 -->
Alle Baulinien in Form einer Balkenwaage (von links nach rechts) auf einen Blick:

1. Ur-Classic-Line
2. Eighties-Classic-Line
3. Actual-Line
4. Professional-Line
5. Color-Line
6. Freestyle-Line
7. New-Classic-Line