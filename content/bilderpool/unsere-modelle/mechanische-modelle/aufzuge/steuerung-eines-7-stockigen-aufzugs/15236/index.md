---
layout: "image"
title: "Die Fahrkabine mit Tastern"
date: "2008-09-15T16:39:11"
picture: "DSCF1692.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "me"
keywords: ["Fahrkabine", "Aufzug", "Taster"]
uploadBy: "Strohi"
license: "unknown"
legacy_id:
- details/15236
imported:
- "2019"
_4images_image_id: "15236"
_4images_cat_id: "1394"
_4images_user_id: "763"
_4images_image_date: "2008-09-15T16:39:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15236 -->
Das ist die Fahrstuhl-Kabine, in ihr sind Taster für jedes Stockwerk angebracht.

http://jugendforscht.fiz-karlsruhe.de/cgi-bin/ih?id=1486.1.0.3430743190&action=filter&+param=loadpage+1+1