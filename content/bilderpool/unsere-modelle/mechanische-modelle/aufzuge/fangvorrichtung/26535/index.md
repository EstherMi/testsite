---
layout: "image"
title: "PICT4869"
date: "2010-02-24T21:35:37"
picture: "fangvorrichtung1.jpg"
weight: "1"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/26535
imported:
- "2019"
_4images_image_id: "26535"
_4images_cat_id: "1890"
_4images_user_id: "729"
_4images_image_date: "2010-02-24T21:35:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26535 -->
Fangvorrichtung im ausgebauten Zustand.