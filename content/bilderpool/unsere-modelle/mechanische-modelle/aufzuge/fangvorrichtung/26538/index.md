---
layout: "image"
title: "PICT4872"
date: "2010-02-24T21:35:37"
picture: "fangvorrichtung4.jpg"
weight: "4"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/26538
imported:
- "2019"
_4images_image_id: "26538"
_4images_cat_id: "1890"
_4images_user_id: "729"
_4images_image_date: "2010-02-24T21:35:37"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26538 -->
Ansicht von der anderen Seite.