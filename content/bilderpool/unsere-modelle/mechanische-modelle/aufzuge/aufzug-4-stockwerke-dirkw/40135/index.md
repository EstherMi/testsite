---
layout: "image"
title: "Führungsschienen Gegengewicht"
date: "2015-01-02T15:55:46"
picture: "aufzug40.jpg"
weight: "40"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/40135
imported:
- "2019"
_4images_image_id: "40135"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40135 -->
Führungsschienen für das Gegengewicht