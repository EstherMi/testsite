---
layout: "image"
title: "Ruftaster Stockwerk"
date: "2015-01-02T15:55:46"
picture: "aufzug16.jpg"
weight: "16"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/40111
imported:
- "2019"
_4images_image_id: "40111"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40111 -->
Die Beleuchtung des Ruftasters habe ich folgendermaßen umgesetzt. 
Ich habe auf ein rundes 4 mm Acrylstück ein kurzes rundes 6 mm  Acrylstück geklebt.
