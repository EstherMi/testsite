---
layout: "image"
title: "Kabine vorn"
date: "2015-01-02T15:55:46"
picture: "aufzug30.jpg"
weight: "30"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/40125
imported:
- "2019"
_4images_image_id: "40125"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40125 -->
Teleskoptür ist geschlossen.