---
layout: "image"
title: "Kabine seitlich"
date: "2015-01-02T15:55:46"
picture: "aufzug33.jpg"
weight: "33"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/40128
imported:
- "2019"
_4images_image_id: "40128"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40128 -->
Man kann die 4 Ruftaster und die 4 Führungsrollen der Kabine erkennen.