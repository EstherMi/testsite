---
layout: "image"
title: "Flachbandkabel für die Stockwerke"
date: "2015-01-02T15:55:46"
picture: "aufzug24.jpg"
weight: "24"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/40119
imported:
- "2019"
_4images_image_id: "40119"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40119 -->
Die Ansteuerung aller Stockwerke über ein 26-adriges Flachbandkabel.