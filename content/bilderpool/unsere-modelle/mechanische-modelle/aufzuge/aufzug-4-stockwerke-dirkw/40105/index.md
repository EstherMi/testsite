---
layout: "image"
title: "Antrieb"
date: "2015-01-02T15:55:46"
picture: "aufzug10.jpg"
weight: "10"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/40105
imported:
- "2019"
_4images_image_id: "40105"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40105 -->
Der Antrieb der Kabine erfolgt über einen Encoder-Motor mit Seilen über Drehscheiben. 
Dies erwies sich als sehr praktisch, wenn der Fahrstuhl mal in einem Stockwerk hängt, 
der Encodermotor weiter durchdrehen kann.



