---
layout: "image"
title: "Ruftaster Stockwerk"
date: "2015-01-02T15:55:46"
picture: "aufzug17.jpg"
weight: "17"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/40112
imported:
- "2019"
_4images_image_id: "40112"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40112 -->
Dazwischen habe ich eine grüne LED gebaut. Dadurch wird das Licht über das Acrylstück nach außen übertragen.