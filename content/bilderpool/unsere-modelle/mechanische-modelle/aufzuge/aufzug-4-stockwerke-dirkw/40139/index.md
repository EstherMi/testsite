---
layout: "image"
title: "Fahrstuhl Außenverkleidung vorn"
date: "2015-01-02T15:55:46"
picture: "aufzug44.jpg"
weight: "44"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/40139
imported:
- "2019"
_4images_image_id: "40139"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "44"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40139 -->
Man kann die Elektrik der Stockwerke erkennen.