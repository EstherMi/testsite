---
layout: "overview"
title: "Aufzug 4 Stockwerke (DirkW)"
date: 2019-12-17T19:22:03+01:00
legacy_id:
- categories/3014
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3014 --> 
Hallo zusammen,

ich möchte euch mein Aufzug mit 4 Stockwerken vorstellen.

Einen Film zum Aufzug findet ihr auf Youtube:

https://www.youtube.com/watch?v=dCneCQCMcaM&list=UU4xME8CsjPQxq7E6JSkrfuw

Hier die Facts:

*Stockwerke*
4x Stockwerke
4x Ruftaster beleuchtet
4x Lichtschranke außen 
4x 7-Segment Anzeige (wo befindet sich der Fahrstuhl)
8x Auf- und Ab Lichter als Pfeil.
1x Encoder-Motor für Auf und Abwärtsfahrt
1x Endschalter unten
2x Robo TX Controller für die Auf- und Ab-Steuerung, Rufverarbeitung und Lichtschranken
1x SAA1064 Treiber für 7- Segmentanzeige 

*Kabine*
1x S-Motor mit Hubgetriebe für Teleskoptür
2x Reedkontakte als Endschalter für Öffnen und Schließen 
1x Innnenbeleuchtung
4x Ruftaster in der Kabine
1x 7-Segment  Anzeige im Fahrstuhl für Stockwerkanzeige
2x Federkontakte als Stromversorgung, für den Robo TX Controller der Kabine, über Stromschienen 
1x  Robo TX für die Teleskoptür, Lichtschranke und das Soundmodul
Sound Modul:: 
- öffen und schließen der Kabinentür  (Zischen)
- Stockwerk erreicht (Gong)
- Auf und Ab Kabine (Fahrgeräusch)