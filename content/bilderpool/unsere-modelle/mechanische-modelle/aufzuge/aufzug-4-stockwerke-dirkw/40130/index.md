---
layout: "image"
title: "Kabine oben schräg"
date: "2015-01-02T15:55:46"
picture: "aufzug35.jpg"
weight: "35"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/40130
imported:
- "2019"
_4images_image_id: "40130"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40130 -->
S-Motor mit Hubgetriebe für Teleskoptür