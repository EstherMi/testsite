---
layout: "image"
title: "Gegengewicht"
date: "2015-01-02T15:55:46"
picture: "aufzug25.jpg"
weight: "25"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/40120
imported:
- "2019"
_4images_image_id: "40120"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40120 -->
