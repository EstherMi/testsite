---
layout: "image"
title: "Kabine hinten"
date: "2015-01-02T15:55:46"
picture: "aufzug34.jpg"
weight: "34"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/40129
imported:
- "2019"
_4images_image_id: "40129"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40129 -->
Kabine hinten, der Robo TX Controller für die Auf- und Ab-Steuerung, Lichtschranke und das Soundmodul.
EXT 2 Anschluß über Schleppkabel an die anderen Controller