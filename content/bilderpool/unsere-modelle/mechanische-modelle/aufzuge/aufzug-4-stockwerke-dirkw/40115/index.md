---
layout: "image"
title: "Kabine aus oberen Stockwerk"
date: "2015-01-02T15:55:46"
picture: "aufzug20.jpg"
weight: "20"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/40115
imported:
- "2019"
_4images_image_id: "40115"
_4images_cat_id: "3014"
_4images_user_id: "2303"
_4images_image_date: "2015-01-02T15:55:46"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40115 -->
Hier sind die Reed-Endschalter der Teleskoptür zu erkennen.