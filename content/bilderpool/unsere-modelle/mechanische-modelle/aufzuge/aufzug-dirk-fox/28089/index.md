---
layout: "image"
title: "Blick auf den Antrieb des Aufzugs (von hinten)"
date: "2010-09-13T14:37:23"
picture: "aufzug05.jpg"
weight: "6"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/28089
imported:
- "2019"
_4images_image_id: "28089"
_4images_cat_id: "2040"
_4images_user_id: "1126"
_4images_image_date: "2010-09-13T14:37:23"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28089 -->
Angetrieben wird er Aufzug über die Kette, an der Aufzugkabine und Gegengewicht befestigt sind.