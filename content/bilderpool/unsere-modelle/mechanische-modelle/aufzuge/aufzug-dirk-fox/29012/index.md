---
layout: "image"
title: "Aktivierung Lichtschranke und Stockwerkanzeige"
date: "2010-10-16T21:50:49"
picture: "Taster_fr_Lichtschranke_und_Stockwerkanzeige.jpg"
weight: "1"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/29012
imported:
- "2019"
_4images_image_id: "29012"
_4images_cat_id: "2040"
_4images_user_id: "1126"
_4images_image_date: "2010-10-16T21:50:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29012 -->
Da die Anschlüsse des TX Controllers alle belegt sind, werden die Stockwerkanzeige und die Lampe der jeweiligen Lichtschranke von der geöffneten Tür über einen Minitaster (im Bild rechts unten) aktiviert.