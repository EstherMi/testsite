---
layout: "image"
title: "Aufzugkabine im Aufzugschacht (von hinten)"
date: "2010-09-13T14:37:23"
picture: "aufzug06.jpg"
weight: "7"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/28090
imported:
- "2019"
_4images_image_id: "28090"
_4images_cat_id: "2040"
_4images_user_id: "1126"
_4images_image_date: "2010-09-13T14:37:23"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28090 -->
Die Kabine gleitet an zwei seitlichen Laufschienen über jeweils vier Seilrollen 21. 
Damit das stabil funktioniert, habe ich die Laufschienen an Flachträgern mit je zwei seitlichen Flachstücken befestigen müssen - die Winkelträger waren zu instabil.