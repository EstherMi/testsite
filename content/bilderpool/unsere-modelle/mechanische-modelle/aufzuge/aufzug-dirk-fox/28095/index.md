---
layout: "image"
title: "Geschlossene Aufzugtüre (ohne Verkleidung)"
date: "2010-09-13T14:37:24"
picture: "aufzug11.jpg"
weight: "12"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/28095
imported:
- "2019"
_4images_image_id: "28095"
_4images_cat_id: "2040"
_4images_user_id: "1126"
_4images_image_date: "2010-09-13T14:37:24"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28095 -->
Unmittelbar hinter den Schiebetüren habe ich eine Lichtschranke (Fototransistor, links) montiert, damit die Türe nicht irrtümlich einen ein- oder aussteigenden Fahrgast einklemmt.