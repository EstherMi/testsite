---
layout: "image"
title: "Seitenblick in den Aufzugschacht (ohne Verkleidung)"
date: "2010-09-13T14:37:23"
picture: "aufzug08.jpg"
weight: "9"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/28092
imported:
- "2019"
_4images_image_id: "28092"
_4images_cat_id: "2040"
_4images_user_id: "1126"
_4images_image_date: "2010-09-13T14:37:23"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28092 -->
Hier sind die Laufschienen für Gegengewicht und Aufzugkabine noch einmal von der Seite zu sehen. Beide bewegen sich in einem Abstand von wenigen Millimetern aneinander vorbei.
Vorne im Bild sieht man die (schwarzen) Reed-Kontakte für die Stockwerkerkennung.