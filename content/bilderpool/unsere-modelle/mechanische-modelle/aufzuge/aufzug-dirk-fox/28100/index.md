---
layout: "image"
title: "Programm Aufzugsteuerung: Unterprogramm Stockwerk-Halt"
date: "2010-09-13T14:37:24"
picture: "aufzug16.jpg"
weight: "17"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/28100
imported:
- "2019"
_4images_image_id: "28100"
_4images_cat_id: "2040"
_4images_user_id: "1126"
_4images_image_date: "2010-09-13T14:37:24"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28100 -->
Das Unterprogramm "Stockwerk Halt" stoppt zunächst den Aufzugmotor. Anschließend wird die Schiebetür geöffnet und drei Sekunden nachdem der letzte Fahrgast die Kabine betreten oder verlassen hat, wieder geschlossen. Anschließend wird der Aufzugmotor in der ursprünglichen Fahrtrichtung gestartet.
(Das Programm kann im Downloadbereich unter "Robo Pro" heruntergeladen werden.)