---
layout: "image"
title: "Programm Aufzugsteuerung: Hauptprozess"
date: "2010-09-13T14:37:24"
picture: "aufzug14.jpg"
weight: "15"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/28098
imported:
- "2019"
_4images_image_id: "28098"
_4images_cat_id: "2040"
_4images_user_id: "1126"
_4images_image_date: "2010-09-13T14:37:24"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28098 -->
Die Aufzugsteuerung arbeitet mit vier parallelen Prozessen: Dem Hauptprozess, der die Auf- und Abbewegung des Aufzugs steuert, und drei Nebenprozessen, die den Halt auf einer Etage kontrollieren.
Der Hauptprozess bewegt die Aufzugkabine abwechselnd nach oben und nach unten; ist keine Ruftaste betätigt, geht der Aufzug unten in Wartestellung.
(Das Programm kann im Downloadbereich unter "Robo Pro" heruntergeladen werden.)