---
layout: "image"
title: "Das Gegengewicht"
date: "2010-09-13T14:37:22"
picture: "aufzug04.jpg"
weight: "5"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/28088
imported:
- "2019"
_4images_image_id: "28088"
_4images_cat_id: "2040"
_4images_user_id: "1126"
_4images_image_date: "2010-09-13T14:37:22"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28088 -->
Damit die erforderliche Motorleistung bei Auf- und Abfahrt möglichst gleich ist, benötigt der Aufzug ein ebenso schweres Gegengewicht. 
Nach einigen Experimenten mit einer gefüllten Kassette, die sich aber nur mühsam halbwegs stabil in der Führung hielt, habe ich mich für drei miteinander verbundene Grundplatten entschieden (gewissermaßen eine "Kreuzung" der Gegengewichte aus den Aufzugmodellen von Stefan Falk und Frank Jakob). Zur Sicherheit ist das Gewicht nicht nur über die Zugkette, sondern auch durch zwei zusätzliche Halteseile mit der Aufzugkabine verbunden.