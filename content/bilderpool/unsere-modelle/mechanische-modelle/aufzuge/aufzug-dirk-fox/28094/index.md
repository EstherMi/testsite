---
layout: "image"
title: "Funktionsweise der Aufzugtüren von oben (ohne Verkleidung)"
date: "2010-09-13T14:37:23"
picture: "aufzug10.jpg"
weight: "11"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/28094
imported:
- "2019"
_4images_image_id: "28094"
_4images_cat_id: "2040"
_4images_user_id: "1126"
_4images_image_date: "2010-09-13T14:37:23"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28094 -->
Hier sieht man die Funktionsweise der Schiebetüren noch einmal von schräg oben: 
Die Türflügel werden mit einem (alten) Raupenkettenglied und einem Winkelstein befestigt, die Kette von einem Minimotor (rechts) angetrieben.
Sofern die Stangen nicht verbogen sind, gleiten die Türflügel überraschend leichtgängig auf und zu.