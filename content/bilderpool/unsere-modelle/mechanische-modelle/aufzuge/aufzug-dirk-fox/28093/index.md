---
layout: "image"
title: "Geöffnete Aufzugtüre (ohne Verkleidung)"
date: "2010-09-13T14:37:23"
picture: "aufzug09.jpg"
weight: "10"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/28093
imported:
- "2019"
_4images_image_id: "28093"
_4images_cat_id: "2040"
_4images_user_id: "1126"
_4images_image_date: "2010-09-13T14:37:23"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28093 -->
Der "Clou" des Modells, die auf zwei parallelen Achsen an einem Baustein 30 gleitenden Aufzugtüren, die von einer Kette auseinander gezogen werden, geht auf eine (im Detail ein wenig optimierte) Idee von Dirk Uffmann zurück. Von seinem Aufzug-Modell (http://home.arcor.de/uffmann/Elevator.html) habe ich mich auch bei der Gestaltung der "Stockwerkebenen" inspirieren lassen. (Stefan Falks Aufzug arbeitet mit einer ähnlichen Türaufhängung.)