---
layout: "image"
title: "Hauptschalter"
date: "2011-10-16T20:34:57"
picture: "stoeckigeraufzugftfan03.jpg"
weight: "4"
konstrukteure: 
- "FT-FAN"
fotografen:
- "FT-FAN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FT-Fan"
license: "unknown"
legacy_id:
- details/33188
imported:
- "2019"
_4images_image_id: "33188"
_4images_cat_id: "2454"
_4images_user_id: "1371"
_4images_image_date: "2011-10-16T20:34:57"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33188 -->
Der Hauptschalter mit einem Voltmeter und Amperemeter.
