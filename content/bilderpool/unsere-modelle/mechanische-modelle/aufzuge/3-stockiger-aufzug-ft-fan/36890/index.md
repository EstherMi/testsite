---
layout: "image"
title: "Bedienpanel auf dem PC"
date: "2013-05-11T22:13:40"
picture: "Liftpanel_PC.jpg"
weight: "1"
konstrukteure: 
- "FT-FAN"
fotografen:
- "FT-FAN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FT-Fan"
license: "unknown"
legacy_id:
- details/36890
imported:
- "2019"
_4images_image_id: "36890"
_4images_cat_id: "2454"
_4images_user_id: "1371"
_4images_image_date: "2013-05-11T22:13:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36890 -->
Hier kann man den Aufzug im Online modus steuern zurzeit noch mit USB-Kabel