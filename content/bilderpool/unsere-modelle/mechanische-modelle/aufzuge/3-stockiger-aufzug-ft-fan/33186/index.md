---
layout: "image"
title: "Gesamtansicht"
date: "2011-10-16T20:34:57"
picture: "stoeckigeraufzugftfan01.jpg"
weight: "2"
konstrukteure: 
- "FT-FAN"
fotografen:
- "FT-FAN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FT-Fan"
license: "unknown"
legacy_id:
- details/33186
imported:
- "2019"
_4images_image_id: "33186"
_4images_cat_id: "2454"
_4images_user_id: "1371"
_4images_image_date: "2011-10-16T20:34:57"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33186 -->
Der Aufzug in der Gesamtansicht

Ein Elektroschema des Aufzuges findet ihr im Downloadbereich

Hier ist noch ein Video des Aufzugs:

https://youtu.be/e6l7MbIMdR8