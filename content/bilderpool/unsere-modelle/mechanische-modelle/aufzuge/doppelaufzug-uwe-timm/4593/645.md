---
layout: "comment"
hidden: true
title: "645"
date: "2005-08-16T21:53:16"
uploadBy:
- "Chemikus"
license: "unknown"
imported:
- "2019"
---
Vorderseite des DoppelaufzugsMan erkennt den sechsstöckigen Aufbau mit 6 Ruftastern und 6 Stockwerkslämpchen. Rechts daneben 2 I/O Extensions mit je 6 Tastern und 6 Lämpchen für Kabine 1 und 2.