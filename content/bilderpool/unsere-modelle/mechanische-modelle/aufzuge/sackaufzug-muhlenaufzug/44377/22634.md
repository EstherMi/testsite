---
layout: "comment"
hidden: true
title: "22634"
date: "2016-10-22T18:25:51"
uploadBy:
- "DasKasperle"
license: "unknown"
imported:
- "2019"
---
Hallo Konrad, ein wirklich spitzenmäßiges Model, die technischen Elemente hast du sehr gute nachgebaut. Mir sind die Säcke und Stelzen mit weißem Fuß ins Auge gesprungen. Sie geben deinem Model den letzten Schliff.

Ich freue mich für dich und deine Geschwister, dass der Papa sich so viel Zeit fürs Konstruieren nimmt.

Konstruktive Grüße
Kai