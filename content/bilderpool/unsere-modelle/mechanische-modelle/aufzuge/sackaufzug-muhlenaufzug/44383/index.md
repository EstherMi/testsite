---
layout: "image"
title: "Alternative Konstruktion mit Reibrad"
date: "2016-09-18T09:26:13"
picture: "sackaufzugmuehlenaufzug7.jpg"
weight: "8"
konstrukteure: 
- "Konrad Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/44383
imported:
- "2019"
_4images_image_id: "44383"
_4images_cat_id: "3278"
_4images_user_id: "1126"
_4images_image_date: "2016-09-18T09:26:13"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44383 -->
In Windmühlen wurden, wie wir bei den anschließenden Recherchen festgestellt haben, statt eines losen Transmissionsriemens üblicherweise Reibräder verwendet, die durch einen Seilzug abgesenkt wurden, um den Sackaufzug "einzukuppeln". Das "riecht" geradezu nach einem Folgemodell...