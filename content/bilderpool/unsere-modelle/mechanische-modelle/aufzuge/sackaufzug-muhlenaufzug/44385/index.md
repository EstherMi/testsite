---
layout: "image"
title: "Kupplungsmässig steuerbarer Riementrieb"
date: "2016-09-21T16:47:03"
picture: "Riemenkupplung_EP0036198A.jpg"
weight: "1"
konstrukteure: 
- "Konrad Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/44385
imported:
- "2019"
_4images_image_id: "44385"
_4images_cat_id: "3278"
_4images_user_id: "1126"
_4images_image_date: "2016-09-21T16:47:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44385 -->
Eine solche Riemenkupplung (mit spezieller Spannvorrichtung) wurde am 23.09.1981 für DEERE & Company als EP 0036198 A1 patentiert. Aus der Patentschrift: "Ein Beispiel einer solchen Anwendung ist eine selbstfahrende landwirtschaftliche Maschine, bei der der Steuerbare Riementrieb die Antriebskraft von der Maschine des Gerätes oder der Maschine auf Werkzeuge oder Einrichtungen überträgt."