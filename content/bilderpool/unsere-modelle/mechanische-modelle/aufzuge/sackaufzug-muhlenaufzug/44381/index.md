---
layout: "image"
title: "Sackaufzug - Detailansicht"
date: "2016-09-18T09:26:13"
picture: "sackaufzugmuehlenaufzug5.jpg"
weight: "6"
konstrukteure: 
- "Konrad Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/44381
imported:
- "2019"
_4images_image_id: "44381"
_4images_cat_id: "3278"
_4images_user_id: "1126"
_4images_image_date: "2016-09-18T09:26:13"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44381 -->
Detailansicht der "Kupplung"