---
layout: "overview"
title: "Sackaufzug (Mühlenaufzug)"
date: 2019-12-17T19:22:12+01:00
legacy_id:
- categories/3278
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3278 --> 
Angeregt durch die Besichtigung einer original erhaltenen, 500 Jahre alten und nach wie vor Mehl produzierenden Wassermühle in Lothringen konstruierte Konrad nach unserer Rückkehr diesen Sackaufzug.