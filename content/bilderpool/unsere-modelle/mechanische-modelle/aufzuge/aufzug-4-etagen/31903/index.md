---
layout: "image"
title: "Stockwerksanzeige"
date: "2011-09-23T11:45:36"
picture: "etagenaufzug32.jpg"
weight: "32"
konstrukteure: 
- "Unbekannt"
fotografen:
- "Unbekannt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- details/31903
imported:
- "2019"
_4images_image_id: "31903"
_4images_cat_id: "2378"
_4images_user_id: "298"
_4images_image_date: "2011-09-23T11:45:36"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31903 -->
Antriebsmotor für die Tür und Endtaster für Tür geöffnet