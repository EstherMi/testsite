---
layout: "image"
title: "Aufzug Gesamtansicht von rechts"
date: "2011-09-23T11:45:14"
picture: "etagenaufzug20.jpg"
weight: "20"
konstrukteure: 
- "Unbekannt"
fotografen:
- "Unbekannt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- details/31891
imported:
- "2019"
_4images_image_id: "31891"
_4images_cat_id: "2378"
_4images_user_id: "298"
_4images_image_date: "2011-09-23T11:45:14"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31891 -->
