---
layout: "comment"
hidden: true
title: "21483"
date: "2015-12-31T18:32:00"
uploadBy:
- "Martin Wanke"
license: "unknown"
imported:
- "2019"
---
In http://www.ftcommunity.de/details.php?image_id=31905#col3 sieht man, dass dann der Abstand der Seile besser zu den Seiltrommeln passt. Aber so ganz erschließt sich mir die Seilführung trotzdem nicht, auch in Zusammenhang mit dem Gegengewicht, welches scheinbar an einer dritten Seiltrommel hängt.