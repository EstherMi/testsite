---
layout: "comment"
hidden: true
title: "23105"
date: "2017-02-25T13:31:28"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
[i]Stefan: Ich stimme mit H.A.A.R.Y. überein und hätte die Rollenböcke wohl auch um 90° verdreht angebaut[/i]
Nein, Stefan, nicht so. Die Rollenböcke genau in der Lage wie auf dem Foto. Die Zapfenseite nach unten und kein BS15 dazwischen sondern press aneinander. Die Achsen aber nicht oben sondern seitlich rein - also im Rechten rechts und im Linken links. Die ganze Seilaufhängung ist tatsächlich nur mit den zwei Zapfen der Rollenböcke in der Kabine verankert, daran ändert auch mein Vorschlag nichts.

Bei (einigen oder allen?) Aufzügen gibt es auch noch ein zusätzliches Umlaufseil. Das hat eine Sicherheitsfunktion. Wenn das Hauptseil reißt, dann beißt die "Otis-Bremse" zu. Wenn aber der Antrieb zu schnell wird, trägt das Hauptseil immer noch die Last und dann kommt das Zusatzseil ins Spiel. Das treibt eine drehzahlgesteuerte Fangvorrichtung an. Wird es zu schnell, dann beißt diese Bremse zu. Eventuell hat der Konstrukteur zu einen Aufzug nachbauen wollen, sich aber vertan? Wirklich wissen wir es erst, wenn sich der Erbauer dazu äußert.

Grüße
H.A.R.R.Y.