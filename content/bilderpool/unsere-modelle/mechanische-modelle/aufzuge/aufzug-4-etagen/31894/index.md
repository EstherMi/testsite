---
layout: "image"
title: "Teilansicht von hinten, Gegengewicht mit Führung"
date: "2011-09-23T11:45:25"
picture: "etagenaufzug23.jpg"
weight: "23"
konstrukteure: 
- "Unbekannt"
fotografen:
- "Unbekannt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- details/31894
imported:
- "2019"
_4images_image_id: "31894"
_4images_cat_id: "2378"
_4images_user_id: "298"
_4images_image_date: "2011-09-23T11:45:25"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31894 -->
