---
layout: "image"
title: "Fahrkorb (links)"
date: "2011-09-23T11:45:14"
picture: "etagenaufzug15.jpg"
weight: "15"
konstrukteure: 
- "Unbekannt"
fotografen:
- "Unbekannt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- details/31886
imported:
- "2019"
_4images_image_id: "31886"
_4images_cat_id: "2378"
_4images_user_id: "298"
_4images_image_date: "2011-09-23T11:45:14"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31886 -->
