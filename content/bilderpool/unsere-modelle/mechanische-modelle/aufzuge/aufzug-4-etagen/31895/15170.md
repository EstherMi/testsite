---
layout: "comment"
hidden: true
title: "15170"
date: "2011-09-23T12:45:20"
uploadBy:
- "Masked"
license: "unknown"
imported:
- "2019"
---
Das sind doch die Teile aus dem Styropor-Sortiment, etwas gemoddet, oder? Also 37196, Hobbywelt Verbinder 30 rot: http://www.ft-datenbank.de/details.php?ArticleVariantId=fd396a52-72d0-482d-b23e-9765725d5267
Oben abgeschnitten, die Trennstege zwischendrin rausgeschnitzt und dann Kabel durchgezogen.