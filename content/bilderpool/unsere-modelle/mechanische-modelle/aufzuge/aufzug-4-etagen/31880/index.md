---
layout: "image"
title: "Gegengewicht für Fahrkorb"
date: "2011-09-23T11:44:56"
picture: "etagenaufzug09.jpg"
weight: "9"
konstrukteure: 
- "Unbekannt"
fotografen:
- "Unbekannt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- details/31880
imported:
- "2019"
_4images_image_id: "31880"
_4images_cat_id: "2378"
_4images_user_id: "298"
_4images_image_date: "2011-09-23T11:44:56"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31880 -->
