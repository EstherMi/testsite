---
layout: "image"
title: "Ansicht 3. OG"
date: "2011-09-23T11:45:25"
picture: "etagenaufzug28.jpg"
weight: "28"
konstrukteure: 
- "Unbekannt"
fotografen:
- "Unbekannt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- details/31899
imported:
- "2019"
_4images_image_id: "31899"
_4images_cat_id: "2378"
_4images_user_id: "298"
_4images_image_date: "2011-09-23T11:45:25"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31899 -->
