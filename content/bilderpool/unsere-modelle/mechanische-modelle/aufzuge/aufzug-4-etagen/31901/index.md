---
layout: "image"
title: "Teilansicht der Stockwerke von links vorne"
date: "2011-09-23T11:45:25"
picture: "etagenaufzug30.jpg"
weight: "30"
konstrukteure: 
- "Unbekannt"
fotografen:
- "Unbekannt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- details/31901
imported:
- "2019"
_4images_image_id: "31901"
_4images_cat_id: "2378"
_4images_user_id: "298"
_4images_image_date: "2011-09-23T11:45:25"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31901 -->
