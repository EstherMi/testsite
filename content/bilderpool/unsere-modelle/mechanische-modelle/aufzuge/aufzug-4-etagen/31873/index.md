---
layout: "image"
title: "Beispielmodell 2"
date: "2011-09-23T11:44:56"
picture: "etagenaufzug02.jpg"
weight: "2"
konstrukteure: 
- "Unbekannt"
fotografen:
- "Unbekannt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- details/31873
imported:
- "2019"
_4images_image_id: "31873"
_4images_cat_id: "2378"
_4images_user_id: "298"
_4images_image_date: "2011-09-23T11:44:56"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31873 -->
