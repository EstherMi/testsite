---
layout: "image"
title: "Fahrkorb (rechts vorne)"
date: "2011-09-23T11:45:14"
picture: "etagenaufzug11.jpg"
weight: "11"
konstrukteure: 
- "Unbekannt"
fotografen:
- "Unbekannt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- details/31882
imported:
- "2019"
_4images_image_id: "31882"
_4images_cat_id: "2378"
_4images_user_id: "298"
_4images_image_date: "2011-09-23T11:45:14"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31882 -->
