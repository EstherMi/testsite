---
layout: "image"
title: "Seiltrommeln Fahrkorbantrieb (rechts)"
date: "2011-09-23T11:45:36"
picture: "etagenaufzug37.jpg"
weight: "37"
konstrukteure: 
- "Unbekannt"
fotografen:
- "Unbekannt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- details/31908
imported:
- "2019"
_4images_image_id: "31908"
_4images_cat_id: "2378"
_4images_user_id: "298"
_4images_image_date: "2011-09-23T11:45:36"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31908 -->
