---
layout: "image"
title: "2. und 3. OG mit Antrieb"
date: "2011-09-23T11:45:25"
picture: "etagenaufzug21.jpg"
weight: "21"
konstrukteure: 
- "Unbekannt"
fotografen:
- "Unbekannt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- details/31892
imported:
- "2019"
_4images_image_id: "31892"
_4images_cat_id: "2378"
_4images_user_id: "298"
_4images_image_date: "2011-09-23T11:45:25"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31892 -->
