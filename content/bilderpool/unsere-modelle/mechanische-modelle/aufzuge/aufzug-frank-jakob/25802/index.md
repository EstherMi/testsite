---
layout: "image"
title: "PICT4853"
date: "2009-11-20T17:55:36"
picture: "PICT4853.jpg"
weight: "2"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/25802
imported:
- "2019"
_4images_image_id: "25802"
_4images_cat_id: "1809"
_4images_user_id: "729"
_4images_image_date: "2009-11-20T17:55:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25802 -->
Falls ein Seil reißt, stellt sich die Seilwippe schräg und löst den Fangkontakt aus.