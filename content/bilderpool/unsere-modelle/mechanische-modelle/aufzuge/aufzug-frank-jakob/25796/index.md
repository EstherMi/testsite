---
layout: "image"
title: "PICT4844"
date: "2009-11-17T21:32:24"
picture: "aufzugfrankjakob10.jpg"
weight: "12"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/25796
imported:
- "2019"
_4images_image_id: "25796"
_4images_cat_id: "1809"
_4images_user_id: "729"
_4images_image_date: "2009-11-17T21:32:24"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25796 -->
Kabinendach von hinten