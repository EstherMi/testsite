---
layout: "image"
title: "PICT4845"
date: "2009-11-17T21:32:27"
picture: "aufzugfrankjakob11.jpg"
weight: "13"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/25797
imported:
- "2019"
_4images_image_id: "25797"
_4images_cat_id: "1809"
_4images_user_id: "729"
_4images_image_date: "2009-11-17T21:32:27"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25797 -->
Kabinendach von hinten