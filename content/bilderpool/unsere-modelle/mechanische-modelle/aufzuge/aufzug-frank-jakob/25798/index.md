---
layout: "image"
title: "PICT4846"
date: "2009-11-17T21:32:28"
picture: "aufzugfrankjakob12.jpg"
weight: "14"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/25798
imported:
- "2019"
_4images_image_id: "25798"
_4images_cat_id: "1809"
_4images_user_id: "729"
_4images_image_date: "2009-11-17T21:32:28"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25798 -->
Türantrieb