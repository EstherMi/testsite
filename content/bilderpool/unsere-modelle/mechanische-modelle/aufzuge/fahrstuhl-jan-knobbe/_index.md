---
layout: "overview"
title: "Fahrstuhl (Jan Knobbe)"
date: 2019-12-17T19:21:35+01:00
legacy_id:
- categories/1200
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1200 --> 
Nach dem geglückten Versuch, einen Fahrstuhl mit drei Stockwerken zu bauen, reizte mich nun die Idee, mit \"Bordmitteln\" (ein paar Taster und einige \"Silberlinge\") einen Aufzug zu konstruieren, der vier Stockwerke ansteuert. Verzichtet habe ich auf Stockwerk-Wahltaster in der Kabine. Aus Gründen der Übersicht beim Verdrahten habe ich die Taster neben das Modell zusammen mit den E-Bausteinen auf eine Extra-Platte verlagert.[br][br]Wichtig war mir nur: Die Kabine wird irgendwo angefordert und fährt genau dahin. Nach einiger Zeit des Tüftelns merkte ich, dass ich als Besitzer von nur einem Flip-Flop einen weiteren Kompromiss eingehen musste. Die Fahrt zu den mittleren beiden Geschossen wird über Mono-Flops gesteuert: Das \"Ein\"-Signal des Mono-Flops muss mindestens anhalten, bis die Fahrt über den jeweiligen Stop-Taster beendet wird. Solange das Mono-Flop noch \"Ein\"-Signal hat, kann leider kein neuer Fahrbefehl erteilt werden...[br]Naja, die Computer-Freaks werden sagen, wie primitiv! aber mir hat es eben Spaß gemacht, mit den \"guten, alten\" Silberlingen zu arbeiten...[br][br]Zu den Bildern:[br][br]Gesamt: Die unregelmäßige Anordnung der Etagen erklärt sich daraus, dass der Fahrstuhl die Stockwerke der Puppenstube meiner Tochter ansteuern sollte.[br][br]Kabine: Die Kabine hält im \"Hochparterre\".[br][br]Oben: Oben sind zwei Stop-Taster nötig.[br][br]Taster: Ein Stoptaster in einer der mittleren Etagen (mit Federgelenkbaustein).