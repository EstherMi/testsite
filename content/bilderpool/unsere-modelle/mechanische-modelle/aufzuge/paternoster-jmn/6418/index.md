---
layout: "image"
title: "Hinten ansicht"
date: "2006-06-04T12:56:41"
picture: "paternoster_4.jpg"
weight: "5"
konstrukteure: 
- "Arjen Neijsen"
fotografen:
- "Arjen Neijsen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/6418
imported:
- "2019"
_4images_image_id: "6418"
_4images_cat_id: "556"
_4images_user_id: "162"
_4images_image_date: "2006-06-04T12:56:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6418 -->
