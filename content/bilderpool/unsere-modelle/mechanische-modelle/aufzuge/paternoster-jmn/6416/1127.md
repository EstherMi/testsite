---
layout: "comment"
hidden: true
title: "1127"
date: "2006-06-06T10:37:15"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
Du prüfst in RoboPro sicher, ob der Wert in einem bestimmten Bereich ist. Bis 705 Ohm (470 + 470/2) Stellung 1, bis 1175 Ohm Stellung 2 u.s.w würde ich mal probieren.

Zum Merken: Wenn du einen 2. mechanisch über das Interface angetriebenen Drehschalter nimmst, kannst du dort die aktuelle Position speichern und auch nach einem Stromausfall wieder auslesen.

Gruß,
Thomas