---
layout: "image"
title: "Aufzug"
date: "2004-04-24T14:51:25"
picture: "Aufzug_005F.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/2381
imported:
- "2019"
_4images_image_id: "2381"
_4images_cat_id: "221"
_4images_user_id: "104"
_4images_image_date: "2004-04-24T14:51:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2381 -->
