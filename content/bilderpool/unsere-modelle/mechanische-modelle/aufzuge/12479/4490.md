---
layout: "comment"
hidden: true
title: "4490"
date: "2007-11-05T18:34:30"
uploadBy:
- "Eins"
license: "unknown"
imported:
- "2019"
---
Beim abgebildeten Aufzug handelt es sich um einen 8-geschossigen Personenaufzug mit einer Modellhöhe von rund. 2,20 m.

Inneres und äußeres Tragwerk ist im klassischen ft-Statik-Grau gehalten.

Die Aufzugskabine wird (bei dieser Modellvariante) mittels Hubgetriebe auf Hubzahnstangen im inneren Tragwerk gehalten bzw. geführt. Der Antriebsmotor (Mini-Motor) sitzt hierbei direkt auf dem Hubgetriebe (hier noch sichtbar) hinter der Aufzugskabinenrückwand.

Am äußeren Tragwerk sind die einzelnen Etagenbalkons befestigt.
Inneres- und äußeres Tragwerk sind im Abstand von 180mm (in Höhe der Etagenbalkons) miteinander verbunden.
Das Gesamttragwerk wirkt trotz der nicht eingesetzten Aluprofile (wurde aus Kostengründen darauf verzichtet) recht massiv.

Gesteuert wird diese Aufzugsanlage per Speicherprogrammierbarer Steuerung, Simatic S5 (nicht sichtbar).

An jedem Stockwerk befindet sich jeweils ein Mini-Taster zur Aufzugsanforderung und eine gelbe Kontrollleuchte (Aufzug kommt), diese leuchtet sobald der Aufzug angefordert wird und erlischt wenn die entsprechende Etage angefahren wurde.
Die Rückmeldung der Etagenankunft wird über acht Reedkontakte an die SPS gemeldet.
Der dazu benötige Magnet ist in die Aufzugskabine eingearbeitet (dies ist auf einem anderen Foto gut erkennbar).
An der Spitze des Tragwerks befindet sich eine rote Blinkleuchte.

Bauzeit ca. 100 Stunden