---
layout: "image"
title: "PICT4861"
date: "2010-02-24T21:35:37"
picture: "sargaufzug09.jpg"
weight: "9"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/26529
imported:
- "2019"
_4images_image_id: "26529"
_4images_cat_id: "1889"
_4images_user_id: "729"
_4images_image_date: "2010-02-24T21:35:37"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26529 -->
Detail der Bandbremse