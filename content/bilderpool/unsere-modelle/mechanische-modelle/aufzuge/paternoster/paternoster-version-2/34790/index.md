---
layout: "image"
title: "IMG_0001_Paternoster-Aufzug.JPG"
date: "2012-04-09T22:33:49"
picture: "IMG_0001_Paternoster-Aufzug.jpg"
weight: "6"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: ["Umlauf-Aufzug", "lift", "Fahrstuhl"]
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/34790
imported:
- "2019"
_4images_image_id: "34790"
_4images_cat_id: "2571"
_4images_user_id: "724"
_4images_image_date: "2012-04-09T22:33:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34790 -->
Hier gibt es ein Video:
http://youtu.be/-dysMNrfvLE