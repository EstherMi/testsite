---
layout: "image"
title: "Kabine auf der 1. Ebene"
date: "2008-01-30T17:58:11"
picture: "fahrstuhl03.jpg"
weight: "3"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/13468
imported:
- "2019"
_4images_image_id: "13468"
_4images_cat_id: "1229"
_4images_user_id: "636"
_4images_image_date: "2008-01-30T17:58:11"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13468 -->
