---
layout: "image"
title: "Rückansicht"
date: "2008-01-30T17:58:11"
picture: "fahrstuhl12.jpg"
weight: "12"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/13477
imported:
- "2019"
_4images_image_id: "13477"
_4images_cat_id: "1229"
_4images_user_id: "636"
_4images_image_date: "2008-01-30T17:58:11"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13477 -->
