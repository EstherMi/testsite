---
layout: "image"
title: "Tür geöffnet"
date: "2008-01-30T17:58:11"
picture: "fahrstuhl04.jpg"
weight: "4"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/13469
imported:
- "2019"
_4images_image_id: "13469"
_4images_cat_id: "1229"
_4images_user_id: "636"
_4images_image_date: "2008-01-30T17:58:11"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13469 -->
