---
layout: "image"
title: "Antrieb Kabine"
date: "2008-01-30T17:58:11"
picture: "fahrstuhl19.jpg"
weight: "19"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/13484
imported:
- "2019"
_4images_image_id: "13484"
_4images_cat_id: "1229"
_4images_user_id: "636"
_4images_image_date: "2008-01-30T17:58:11"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13484 -->
