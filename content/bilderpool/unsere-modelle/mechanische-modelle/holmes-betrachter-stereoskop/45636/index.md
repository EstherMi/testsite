---
layout: "image"
title: "Vorderansicht"
date: "2017-03-23T11:41:10"
picture: "Stereoskop_01.jpg"
weight: "1"
konstrukteure: 
- "rito"
fotografen:
- "rito"
keywords: ["Optik", "stereo", "Stereogramm", "3D"]
uploadBy: "rito"
license: "unknown"
legacy_id:
- details/45636
imported:
- "2019"
_4images_image_id: "45636"
_4images_cat_id: "3390"
_4images_user_id: "2179"
_4images_image_date: "2017-03-23T11:41:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45636 -->
Holmes-Betrachter
Mit Hilfe dieses Gerätes kann man Stereogramme einspannen (6x6) und damit einen 3D Eindruck erhalten. Sieht noch nicht hübsch aus, funktioniert aber ganz gut.