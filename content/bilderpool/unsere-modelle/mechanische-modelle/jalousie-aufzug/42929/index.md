---
layout: "image"
title: "Jalousie-Aufzug"
date: "2016-02-21T23:03:44"
picture: "jalousie2.jpg"
weight: "2"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/42929
imported:
- "2019"
_4images_image_id: "42929"
_4images_cat_id: "3191"
_4images_user_id: "558"
_4images_image_date: "2016-02-21T23:03:44"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42929 -->
Dieser ist mit üblicher Diodenverschaltung versehen  und ermöglicht so noch die andere Drehrichtung fürs Jalousie herablassen. Bis jetzt muss beim Herablassen allerdings der Schalter im Tiefpunkt manuell ausgestellt werden.