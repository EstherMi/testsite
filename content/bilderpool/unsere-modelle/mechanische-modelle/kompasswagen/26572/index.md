---
layout: "image"
title: "Antiverdriller - Ansicht von hinten"
date: "2010-03-01T16:45:06"
picture: "Antiverdriller2.jpg"
weight: "7"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- details/26572
imported:
- "2019"
_4images_image_id: "26572"
_4images_cat_id: "1891"
_4images_user_id: "1088"
_4images_image_date: "2010-03-01T16:45:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26572 -->
