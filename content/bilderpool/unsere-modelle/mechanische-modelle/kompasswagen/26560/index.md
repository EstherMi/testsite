---
layout: "image"
title: "Hohlrad-Kompasswagen 1"
date: "2010-02-27T10:39:08"
picture: "HohlradKompass.jpg"
weight: "9"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- details/26560
imported:
- "2019"
_4images_image_id: "26560"
_4images_cat_id: "1891"
_4images_user_id: "1088"
_4images_image_date: "2010-02-27T10:39:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26560 -->
Hier ein Kompasswagen mit Planetengetriebe statt Differential. Wenn s die Umdrehungen des Sonnenrads, t die des Stegs und h die des Hohlrads bezeichnen, so genügen diese Variablen bei den ft-Zahnrädern der linearen Gleichung s  - 4t = -3h. Wenn man somit den Steg an ein Z40 koppelt und dies mit einem Z10 antreibt, dividiert man t durch 4 und subtrahiert daher. Die Länge geht genau auf: Wegen des Faktors 3 müssen die beiden Laufräder 3*3,3 cm = 10 cm auseinander sein. (3,3 cm = Radius der Drehscheibe + O-Ring). Das Modell läuft wegen der Assymmetrie nicht so gut wie der erste Kompassanhänger hier, kann aber vielleicht ganz gut die Anwendung von Planetengetrieben veranschaulichen.