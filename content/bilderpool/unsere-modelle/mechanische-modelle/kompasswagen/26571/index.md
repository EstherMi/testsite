---
layout: "image"
title: "Anwendung: Antiverdriller"
date: "2010-03-01T16:45:05"
picture: "Antiverdriller.jpg"
weight: "6"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- details/26571
imported:
- "2019"
_4images_image_id: "26571"
_4images_cat_id: "1891"
_4images_user_id: "1088"
_4images_image_date: "2010-03-01T16:45:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26571 -->
Wenn man ein Fahrzeug über ein herunterhängendes Kabel mit Strom versorgt, hat man oft das Problem, dass sich das Kabel nach mehreren Drehungen verdrillt. Hier eine Lösung: Das Kabel wird an den Schleifring mit Buchsen angeschlossen. Der Kompassmechanismus sorgt dafür, dass sich das Kabel nicht aufwickelt.