---
layout: "image"
title: "Hohlrad-Kompasswagen 3"
date: "2010-02-27T10:39:09"
picture: "HohlradKompass3.jpg"
weight: "11"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- details/26562
imported:
- "2019"
_4images_image_id: "26562"
_4images_cat_id: "1891"
_4images_user_id: "1088"
_4images_image_date: "2010-02-27T10:39:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26562 -->
