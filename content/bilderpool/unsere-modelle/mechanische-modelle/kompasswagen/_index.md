---
layout: "overview"
title: "Kompasswagen"
date: 2019-12-17T19:24:39+01:00
legacy_id:
- categories/1891
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1891 --> 
Englisch: South pointing chariot. Zeigt trotz Bewegungen immer in die gleiche Richtung. Zur Funktionsweise siehe z.B.

http://www.odts.de/southptr/

http://www.aip.de/%7Elie/Publikationen/342.Kompasswagen.pdf