---
layout: "comment"
hidden: true
title: "11205"
date: "2010-03-15T18:54:41"
uploadBy:
- "geometer"
license: "unknown"
imported:
- "2019"
---
Beim Nachbau von Frank Jakobs Variante ist mir aufgefallen, wie man die Teileanzahl verkleinern kann, wenn man das Differential untypisch einsetzt: Ein Rad treibt die Mitte des Differentials an. Das war mit dem alten Differential wegen der Länge der Achsen schlecht zu machen. Beim alten Differential gibt es übrigens zwei Achslängen. Ich habe die kurze Variante nur einmal. Hier weiß doch bestimmt jemand, aus welchem Kasten die stammt?