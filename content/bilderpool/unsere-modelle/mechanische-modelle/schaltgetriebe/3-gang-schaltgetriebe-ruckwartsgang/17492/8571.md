---
layout: "comment"
hidden: true
title: "8571"
date: "2009-02-21T18:43:12"
uploadBy:
- "fitec"
license: "unknown"
imported:
- "2019"
---
Ein gut durchdachtes Getriebe! Was mir besonders gefällt, ist, dass es auch einen Rückwärtsgang [b]und[/b] einen Leerlaufgang gibt. Die Kunst wäre es jetzt, das ganze geschickt in ein Fahrzeug einzubauen. Wenn du das schaffst, das wäre die Krönung.

Gruß fitec