---
layout: "image"
title: "3-Gang Wechselgetriebe mit Rückwärtsgang 24"
date: "2009-02-21T16:56:56"
picture: "porschemakus24.jpg"
weight: "24"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- details/17492
imported:
- "2019"
_4images_image_id: "17492"
_4images_cat_id: "1570"
_4images_user_id: "327"
_4images_image_date: "2009-02-21T16:56:56"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17492 -->
Leergang eingelegt