---
layout: "image"
title: "20090223_3-Gang_Schaltgetriebe_mit_Rückwärtsgang_kompakter_01"
date: "2009-02-23T20:21:39"
picture: "ganggetriebekompakter1.jpg"
weight: "1"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- details/17493
imported:
- "2019"
_4images_image_id: "17493"
_4images_cat_id: "1571"
_4images_user_id: "327"
_4images_image_date: "2009-02-23T20:21:39"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17493 -->
Ich konnte das Getriebe in der Version II durch verwenden von Abstandsringen anstatt Klemmbüchsen um einen Baustein 15 in der Breite vermindern. Ausserdem hat der geringere Abstand zwischen den Z20 den Vorteil, dass jetzt beim Schalten schneller auf den nächsten Gang geweselt werden kann.