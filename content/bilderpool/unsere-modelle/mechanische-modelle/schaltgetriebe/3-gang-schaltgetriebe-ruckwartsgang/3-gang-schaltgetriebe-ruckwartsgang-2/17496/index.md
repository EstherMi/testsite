---
layout: "image"
title: "20090223_3-Gang_Schaltgetriebe_mit_Rückwärtsgang_kompakter_04"
date: "2009-02-23T20:21:40"
picture: "ganggetriebekompakter4.jpg"
weight: "4"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- details/17496
imported:
- "2019"
_4images_image_id: "17496"
_4images_cat_id: "1571"
_4images_user_id: "327"
_4images_image_date: "2009-02-23T20:21:40"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17496 -->
Das Getriebe in Aktion