---
layout: "image"
title: "3-Gang Wechselgetriebe mit Rückwärtsgang 18"
date: "2009-02-21T16:56:56"
picture: "porschemakus18.jpg"
weight: "18"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- details/17486
imported:
- "2019"
_4images_image_id: "17486"
_4images_cat_id: "1570"
_4images_user_id: "327"
_4images_image_date: "2009-02-21T16:56:56"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17486 -->
Rückwärtsgang eingelegt (Z20 ganz rechts)