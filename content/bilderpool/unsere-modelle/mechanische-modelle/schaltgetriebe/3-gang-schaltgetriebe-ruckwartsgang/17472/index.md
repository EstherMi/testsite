---
layout: "image"
title: "3-Gang Wechselgetriebe mit Rückwärtsgang 04"
date: "2009-02-21T16:56:56"
picture: "porschemakus04.jpg"
weight: "4"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- details/17472
imported:
- "2019"
_4images_image_id: "17472"
_4images_cat_id: "1570"
_4images_user_id: "327"
_4images_image_date: "2009-02-21T16:56:56"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17472 -->
Die beiden roten Z40 und Z20 bilden die Übersetzung für den zweiten Gang.