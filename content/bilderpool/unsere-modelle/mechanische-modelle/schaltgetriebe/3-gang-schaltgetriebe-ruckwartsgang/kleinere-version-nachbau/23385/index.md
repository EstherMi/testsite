---
layout: "image"
title: "von oben"
date: "2009-03-06T19:34:34"
picture: "g1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- details/23385
imported:
- "2019"
_4images_image_id: "23385"
_4images_cat_id: "1581"
_4images_user_id: "920"
_4images_image_date: "2009-03-06T19:34:34"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23385 -->
Hier noch einmal eine Ansicht von oben