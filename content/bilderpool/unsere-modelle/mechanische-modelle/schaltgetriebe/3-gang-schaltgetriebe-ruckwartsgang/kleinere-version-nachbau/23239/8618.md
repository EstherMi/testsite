---
layout: "comment"
hidden: true
title: "8618"
date: "2009-02-28T21:54:02"
uploadBy:
- "Mr Smith"
license: "unknown"
imported:
- "2019"
---
Die Achsen sind quasi identisch.
Bei deinem Modell waren die Kraftübertragungsachsen im Kreis um die ganze Mittelachse angeordnet. Ich hab jetzt die Achsen für den Rückwärtsgang und Gang 1 beide oben draufgebaut. die Eingangswelle ließe sich auch noch dorthin setzten. 

Das schwere war vorallem, die Achsen so unterzubringen dass sich die falschen Achsen/Zahnräder/Streben nicht berühren und dass das ganze auch noch bei hohen Kräften stabil bleibt. 
Der Rahmen lässt sich aber noch kompakter bauen, das werde ich als nächstes angehen-