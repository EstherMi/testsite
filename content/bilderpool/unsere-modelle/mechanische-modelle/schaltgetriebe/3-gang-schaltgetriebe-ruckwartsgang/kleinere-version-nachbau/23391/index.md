---
layout: "image"
title: "Gesamtansicht"
date: "2009-03-06T19:34:34"
picture: "g7.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- details/23391
imported:
- "2019"
_4images_image_id: "23391"
_4images_cat_id: "1581"
_4images_user_id: "920"
_4images_image_date: "2009-03-06T19:34:34"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23391 -->

Das 3-Gang-Getriebe mit integriertem Antriebsmotor und Gangschaltung ist 9,5 cm breit, 14cm tief und 12cm hoch (an den höchsten Stellen)