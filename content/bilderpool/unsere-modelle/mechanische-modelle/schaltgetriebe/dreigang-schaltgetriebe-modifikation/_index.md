---
layout: "overview"
title: "Dreigang Schaltgetriebe Modifikation vermeidet blockieren"
date: 2019-12-17T19:21:04+01:00
legacy_id:
- categories/2779
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2779 --> 
Kleine Anpassungen des Getriebes von Dirk Fox: http://www.ftcommunity.de/categories.php?cat_id=2106
Dieses Getriebe kann beim Schalten kurzzeitig blockieren, da beim Schaltvorgang die Ritzel von 2 verschiedenen Gängen kurzzeitig ineinandergreifen können.
Ich habe, aufbauend auf dem Getriebe von Dirk, die Abstände der Ritzel ein bisschen modifiziert und mit Abstandshaltern fixiert. So kann sich auch im Betrieb nichts verschieben.