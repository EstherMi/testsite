---
layout: "image"
title: "3. Gang"
date: "2013-09-13T17:16:34"
picture: "3terGang.jpg"
weight: "4"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/37390
imported:
- "2019"
_4images_image_id: "37390"
_4images_cat_id: "2779"
_4images_user_id: "1729"
_4images_image_date: "2013-09-13T17:16:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37390 -->
