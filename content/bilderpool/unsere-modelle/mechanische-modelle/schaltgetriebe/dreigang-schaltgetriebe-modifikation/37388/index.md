---
layout: "image"
title: "1.Gang"
date: "2013-09-13T17:16:33"
picture: "1terGang.jpg"
weight: "2"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/37388
imported:
- "2019"
_4images_image_id: "37388"
_4images_cat_id: "2779"
_4images_user_id: "1729"
_4images_image_date: "2013-09-13T17:16:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37388 -->
