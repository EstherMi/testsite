---
layout: "overview"
title: "Dreigang-Schaltgetriebe (Dirk Fox)"
date: 2019-12-17T19:20:56+01:00
legacy_id:
- categories/2106
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2106 --> 
Funktionsmodell eines Dreigang-Schiebeschaltgetriebes mit geringer Bauhöhe und hoher Stabilität (daher auch für den Einsatz in Fahrzeugen geeignet)