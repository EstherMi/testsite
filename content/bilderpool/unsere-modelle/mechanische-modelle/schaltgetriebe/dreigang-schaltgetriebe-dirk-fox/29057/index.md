---
layout: "image"
title: "Dreigang-Schaltgetriebe: Konstruktionszeichnung (ft designer)"
date: "2010-10-26T22:49:40"
picture: "Dreigang-Schaltgetriebe_Entwurf_ft-Designer_klein.jpg"
weight: "1"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: ["Dreigang-Schaltgetriebe", "Schaltgetriebe", "Schaltung"]
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/29057
imported:
- "2019"
_4images_image_id: "29057"
_4images_cat_id: "2106"
_4images_user_id: "1126"
_4images_image_date: "2010-10-26T22:49:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29057 -->
Hier die mit dem ft designer erstellte Konstruktionszeichnung. Die Originaldatei (inklusive Animation und Bauteilliste) gibt es im Download-Bereich.