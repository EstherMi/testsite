---
layout: "image"
title: "Dreigang-Schaltgetriebe: 3. Gang"
date: "2010-10-11T18:08:07"
picture: "dreigangschaltgetriebe4.jpg"
weight: "6"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/28984
imported:
- "2019"
_4images_image_id: "28984"
_4images_cat_id: "2106"
_4images_user_id: "1126"
_4images_image_date: "2010-10-11T18:08:07"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28984 -->
Im dritten Gang erreicht das Getriebe eine 1:3-Übersetzung (Z30 auf Z10). Die Reifen haben einen Umfang von 21 cm; damit liegt die Entfaltung bei 63 cm. 
Auf die Räder gestellt schaffen Akku und XM-Motor diese Übersetzung nur nach einer vorausgegangenen Beschleunigung in den ersten beiden Gängen.