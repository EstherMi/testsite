---
layout: "comment"
hidden: true
title: "16158"
date: "2012-01-18T12:04:51"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Hallo Dirk,

Eine möglichkeit für Version 2.0 mit Differential an der Hinterachse wäre Antriebmotor + Getriebe 90 Grad zu drehen.
Damit wird das Modell vielleicht länger aber nicht breiter.

Grüss,

Peter
Poederoyen NL