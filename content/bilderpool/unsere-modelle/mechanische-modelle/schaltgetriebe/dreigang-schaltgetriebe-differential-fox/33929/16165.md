---
layout: "comment"
hidden: true
title: "16165"
date: "2012-01-18T18:11:10"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Hallo Dirk,

Sehr schick, das Getriebe! Von der Machart her kommt richtig "Retro-Feeling" auf, trotz schwarzer Steine. Hach ja, damals, mit dem Kasten 400... 

Für Anzeige oder Stillsetzen des Schaltmotors könnte man vielleicht etwas mit mitdrehenden Magneten und feststehenden Reedkontakten machen.

Was mir nicht so besonders gefällt, ist der große Schwenkradius der Vorderräder. 

Du hättest doch in der Höhe noch Spielraum, um eine zusätzliche Achse unter zu bringen. Oder halt nur eins der Hinterräder fest anziehen.

Gruß,
Harald