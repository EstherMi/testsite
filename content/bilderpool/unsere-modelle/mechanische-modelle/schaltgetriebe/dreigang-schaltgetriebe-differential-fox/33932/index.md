---
layout: "image"
title: "Gesamtansicht"
date: "2012-01-14T22:26:04"
picture: "dreigangschaltgetriebemitdifferentialdirkfox13.jpg"
weight: "13"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/33932
imported:
- "2019"
_4images_image_id: "33932"
_4images_cat_id: "2510"
_4images_user_id: "1126"
_4images_image_date: "2012-01-14T22:26:04"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33932 -->
Das Lenken und Schalten mit der Fernsteuerung ist etwas gewöhnungsbedürftig - der Antriebsmotor liegt auf dem rechten Hebel, Schaltung und Lenkung auf dem linken, um während der Fahrt lenken und schalten zu können. Das gefällt auch dem Getriebe besser. Sauber hochgeschaltet, erreicht das Fahrzeug eine ordentliche Geschwindigkeit... nichts für kurze Flure und überfüllte Kinderzimmer.
Ein kleines Video habe ich in Arbeit...