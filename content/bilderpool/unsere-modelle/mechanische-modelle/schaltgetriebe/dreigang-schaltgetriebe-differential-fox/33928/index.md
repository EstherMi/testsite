---
layout: "image"
title: "Lenkung (Detail)"
date: "2012-01-14T22:26:04"
picture: "dreigangschaltgetriebemitdifferentialdirkfox09.jpg"
weight: "9"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/33928
imported:
- "2019"
_4images_image_id: "33928"
_4images_cat_id: "2510"
_4images_user_id: "1126"
_4images_image_date: "2012-01-14T22:26:04"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33928 -->
Die Lenkung mit zwei Gelenkbausteinen ist eine Variante einer Konstruktionsidee aus den ft-Club-Nachrichten 2/1976.
Natürlich kann man hier auch einen Servo einbauen - das erschien mir aber zu "billig". Beim Aufbau muss man darauf achten, dass der Baustein 7,5 stramm auf der Zahnstange 30 des Hubgetriebes sitzt. 
Die Gelenksteine sorgen für einen natürlichen "Anschlag" der Lenkung, allerdings ist der XS-Motor sehr stark, daher muss man beim Steuern darauf achten, den Motor rechtzeitig zu stoppen. 
(Da das Fahrzeug mit der Fernsteuerung betrieben wird, konnte ich keine Endlagenschalter einsetzen; ein Polwendeschalter wäre eine Möglichkeit, sah mir aber zu klobig aus.)