---
layout: "overview"
title: "Dreigang-Schaltgetriebe mit Differential (Dirk Fox)"
date: 2019-12-17T19:20:59+01:00
legacy_id:
- categories/2510
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2510 --> 
Sehr kompaktes Dreigang-Schaltgetriebe mit geringer Bauhöhe unter Verwendung eines Differentialgetriebes in einem lenkbaren, ferngesteuerten Fahrzeug-Chassis auf der Grundplatte 90x45