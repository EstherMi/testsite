---
layout: "image"
title: "Kompaktes Dreiganggetriebe"
date: "2012-01-14T22:26:03"
picture: "dreigangschaltgetriebemitdifferentialdirkfox02.jpg"
weight: "2"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/33921
imported:
- "2019"
_4images_image_id: "33921"
_4images_cat_id: "2510"
_4images_user_id: "1126"
_4images_image_date: "2012-01-14T22:26:03"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33921 -->
Thomas Püttmanns (geometer) Erläuterung der Getriebekonstruktion mit Differentialen in der ft:pedia 3/2011 (http://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2011-3.pdf) ließ mich über die Weihnachtsfeiertage nicht los. Das Ergebnis ist ein ausreichend kompaktes Dreigang-Getriebe für den Einbau in ein ft-Fahrzeug.