---
layout: "image"
title: "Neu"
date: "2007-09-26T15:07:38"
picture: "2-Gang-Getriebeneu1.jpg"
weight: "1"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/12016
imported:
- "2019"
_4images_image_id: "12016"
_4images_cat_id: "972"
_4images_user_id: "456"
_4images_image_date: "2007-09-26T15:07:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12016 -->
Gleiches Prinzip nur ein wenig kompakter.