---
layout: "comment"
hidden: true
title: "3386"
date: "2007-06-07T10:28:14"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
Kleiner Tipp: Schau mal bitte bei deiner Kamera, da gibt es für solche Nahaufnahmen üblicherweise einen Makromodus (meist eine Blume als Symbol) :)