---
layout: "overview"
title: "2-Gang-Schaltgetriebe (fitec)"
date: 2019-12-17T19:20:41+01:00
legacy_id:
- categories/972
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=972 --> 
Besitzt 2 Gänge (2:1/1:2) und kann angetrieben sowie per Minimotor geschalten werden.