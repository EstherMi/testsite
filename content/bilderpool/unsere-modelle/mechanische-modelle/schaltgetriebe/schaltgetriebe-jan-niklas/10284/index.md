---
layout: "image"
title: "Getriebe mit Bremse"
date: "2007-05-05T08:38:34"
picture: "getriebe2.jpg"
weight: "6"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- details/10284
imported:
- "2019"
_4images_image_id: "10284"
_4images_cat_id: "935"
_4images_user_id: "557"
_4images_image_date: "2007-05-05T08:38:34"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10284 -->
Schalthebel mit Taster