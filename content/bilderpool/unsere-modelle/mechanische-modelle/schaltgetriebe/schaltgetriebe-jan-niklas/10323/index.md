---
layout: "image"
title: "Getriebe"
date: "2007-05-06T16:50:21"
picture: "getriebeneu1.jpg"
weight: "1"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- details/10323
imported:
- "2019"
_4images_image_id: "10323"
_4images_cat_id: "935"
_4images_user_id: "557"
_4images_image_date: "2007-05-06T16:50:21"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10323 -->
scheibenbremse