---
layout: "image"
title: "Rechtes Getriebe1"
date: "2009-10-03T20:57:27"
picture: "minizahnraedergetriebe4.jpg"
weight: "4"
konstrukteure: 
- "Martin Giger"
fotografen:
- "Martin Giger"
keywords: ["m05"]
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/25486
imported:
- "2019"
_4images_image_id: "25486"
_4images_cat_id: "1783"
_4images_user_id: "445"
_4images_image_date: "2009-10-03T20:57:27"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25486 -->
Hier ist die Schaltung auf das rechte getriebe geschoben. Leider habe ich nicht das ganze auf ein Bild gebracht --> nächstes Bild