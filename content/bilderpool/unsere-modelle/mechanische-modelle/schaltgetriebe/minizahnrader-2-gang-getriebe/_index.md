---
layout: "overview"
title: "Minizahnräder 2 Gang Getriebe"
date: 2019-12-17T19:20:55+01:00
legacy_id:
- categories/1783
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1783 --> 
Dieses Getriebe ist nicht klein, wurde jedoch mit den kleinen ft-Zahnrädchen gebaut. Es kann verschoben werden (auf einem Hubgetriebe) und hat dadurch zwei verschiedene Geschwindigkeiten. Leider habe ich nicht mehr scharfe Fotos und es schon lange abgebrochen.