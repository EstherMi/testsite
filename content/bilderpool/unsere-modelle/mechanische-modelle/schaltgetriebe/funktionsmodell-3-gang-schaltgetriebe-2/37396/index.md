---
layout: "image"
title: "Seitenansicht Getrieberahmen"
date: "2013-09-15T15:47:05"
picture: "Getrieberahmen.jpg"
weight: "4"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/37396
imported:
- "2019"
_4images_image_id: "37396"
_4images_cat_id: "2781"
_4images_user_id: "1729"
_4images_image_date: "2013-09-15T15:47:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37396 -->
Die Seitenansicht ist von rechts.
Im Bild habe ich die Achsen durchnumeriert. Die Kraftübertragung erfolgt von Achse 
1->2->3->4.
1 und 2 gehören zur ersten Getriebestufe; 3 und 4 zur zweiten.
1 und 4 sind fest; 2 und 3 gehören zum Schaltschlitten und sind in der Längsachse verschiebbar.
Wichtig ist der unterschiedliche Abstand von Achse 1 zu 4 im Vergleich zu 2 und 3.
Die Ritzel der Achsen von 1 und 4 dürfen sich nicht berühren, deshalb ist der Abstand größer.
Der Abstand von 2 zu 3 ist genau so gewählt, daß die schwarzen Z20 Ritzel ineinandergreifen, um die Kraft zu übertragen.