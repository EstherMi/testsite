---
layout: "overview"
title: "Funktionsmodell 3 Gang Schaltgetriebe '2-stufig'"
date: 2019-12-17T19:21:04+01:00
legacy_id:
- categories/2781
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2781 --> 
Aufbau eines Funktionsmodells für ein 3-Gang Getriebe, welches 2 stufig aufgebaut ist.

An einem einfachen 3-Gang Getriebe wie unter:
http://www.ftcommunity.de/categories.php?cat_id=2779
bzw.
http://www.ftcommunity.de/categories.php?cat_id=2106
störte mich, daß man für die Kraftübertragung zum Getriebe hin sowas wie eine Walze braucht, da sich die Antriebswelle je nach Gang in einer unterschiedlichen Position befindet.

Dieses Getriebe habe ich intern verdoppelt in 2 ähnliche Stufen. Zum Schalten wird dann nur der "Getriebeschlitten" (gibt es da einen Fachausdruck?) geschoben. Die Antriebswellen für Krafteinleitung (vom Motor) und Kraftausleitung (zu den Rädern) sind fest in Ihrer Position.

Ich weiß nicht, ob ich dieses Funktionsmodell mal in ein reales Fahrzeug umsetzen werde, denn es hat auch Nachteile:
- baut ziemlich groß
- deutlich höhere Übersetzungsverhältnisse, die eventuell nicht praxistauglich sind.
- beim Schalten müssen sich beide Achsen drehen, sonst geht der Gang manchmal nicht rein
  (Könnte Probleme beim stehenden Fahrzeug geben)