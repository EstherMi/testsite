---
layout: "image"
title: "3. Gang"
date: "2013-09-15T15:47:05"
picture: "3terGang.jpg"
weight: "7"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/37399
imported:
- "2019"
_4images_image_id: "37399"
_4images_cat_id: "2781"
_4images_user_id: "1729"
_4images_image_date: "2013-09-15T15:47:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37399 -->
im dritten Gang (Getriebestellung ganz rechts) sind folgende Zahnräder beteiligt:
1: Z30->Z10
2: Z20->Z20
3: Z30->Z10

in dieser Stellung kann es dann leider auch schon passieren, daß Zahnräder (bzw. die Klemmnabe) auf der Achse durchrutschen, da bei dieser Übersetzung ordentliche Kräfte entstehen.