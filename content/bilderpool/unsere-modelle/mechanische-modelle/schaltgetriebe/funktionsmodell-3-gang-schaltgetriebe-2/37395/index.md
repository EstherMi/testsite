---
layout: "image"
title: "Ansicht von Hinten/2.Stufe"
date: "2013-09-15T15:47:05"
picture: "VonHinten.jpg"
weight: "3"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/37395
imported:
- "2019"
_4images_image_id: "37395"
_4images_cat_id: "2781"
_4images_user_id: "1729"
_4images_image_date: "2013-09-15T15:47:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37395 -->
Von Hinten sieht man die 2. Getriebestufe. Die Übersetzungen sind genau anders herum angeordnet als bei Stufe 1. Die Abstände zwischen den Ritzeln sind so ausgetüftelt, daß die Ritzel der beiden Stufen in sich verschränkt sind (platzsparend), sich aber nie berühren (außer die schwarzen Z20 für die Kraftübertragung von Stufe 1 auf Stufe2)
Insgesamt besteht das Getriebe ja aus 12 Zahnrädern!