---
layout: "image"
title: "Drehimpuls-Prüfstand (6/11)"
date: "2008-09-25T17:47:41"
picture: "pruefstanddrehimpulse06.jpg"
weight: "6"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/15600
imported:
- "2019"
_4images_image_id: "15600"
_4images_cat_id: "1430"
_4images_user_id: "723"
_4images_image_date: "2008-09-25T17:47:41"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15600 -->
Impulstaster:
Erst mit der den Schaltimpuls ausreichend verlängernden ft-Steuerscheibe zählt auch die Impulsbaugruppe "Taster" ebenfalls ohne mechanische Übersetzung 1:1 zuverlässig bis 1000 U/min.
In Synchronlaufversuchen bis knapp 1000 U/min zählten so die Drehzahlzähler Mechanik, Lichtschranke und Impulstaster übereinstimmend die Drehzahlen.
Vielleicht kommt jetzt ein schlauer ft-Fan daher und rechnet mir ohne Versuche die erforderliche Impulslänge vor. Das wäre nicht schlecht, dann wüsste ich es auch wieder und könnte damit die Grenzen obiger Anordnung schon abstecken.