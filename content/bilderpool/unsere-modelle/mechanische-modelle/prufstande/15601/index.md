---
layout: "image"
title: "Drehimpuls-Prüfstand (7/11)"
date: "2008-09-25T17:47:41"
picture: "pruefstanddrehimpulse07.jpg"
weight: "7"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2 / 2D aus 3D"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/15601
imported:
- "2019"
_4images_image_id: "15601"
_4images_cat_id: "1430"
_4images_user_id: "723"
_4images_image_date: "2008-09-25T17:47:41"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15601 -->
Mechanischer Drehzahlzähler:
Draufsicht von vorn mit ft-Schrittmotor, 2D-Ansicht aus 3D mit Bauteilkanten.
Hiermit denke ich mal, sieht man die Mechanik besser ein als auf einem Foto. Es gibt keine zu dunkle oder zu helle Partien und alles ist gleichbleibend scharf abgebildet.