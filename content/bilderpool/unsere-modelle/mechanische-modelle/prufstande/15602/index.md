---
layout: "image"
title: "Drehimpuls-Prüfstand (8/11)"
date: "2008-09-25T17:47:41"
picture: "pruefstanddrehimpulse08.jpg"
weight: "8"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2 / 2D aus 3D"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/15602
imported:
- "2019"
_4images_image_id: "15602"
_4images_cat_id: "1430"
_4images_user_id: "723"
_4images_image_date: "2008-09-25T17:47:41"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15602 -->
Mechanischer Drehzahlzähler:
Draufsicht von hinten mit Mini-Motor und U-Getriebe, 2D-Ansicht aus 3D mit Bauteilkanten
Die farbigen Flachstecker sind bei mir beliebt, sie ersparen mir im ftD die Kabelverlegung.