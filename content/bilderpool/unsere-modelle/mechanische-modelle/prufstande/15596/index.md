---
layout: "image"
title: "Drehimpuls-Prüfstand (2/11)"
date: "2008-09-25T17:47:40"
picture: "pruefstanddrehimpulse02.jpg"
weight: "2"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/15596
imported:
- "2019"
_4images_image_id: "15596"
_4images_cat_id: "1430"
_4images_user_id: "723"
_4images_image_date: "2008-09-25T17:47:40"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15596 -->
Mechanischer Prüfstand:
Modellbeispiele mit Klinken fand ich nicht gut, weil sie nur in einer Drehrichtung arbeiten und bei Eingriff aller Klinken gleichzeitig nicht ganz unkritisch sind. Ausserdem wünschte ich mir eine kontinuierliche Anzeige. Auch sollte es eine Lösung für beide Drehrichtungen sein, wenn ich mal einen Verkabelungsfehler mache. Also fanden sich 7 Kegelräder und 5 Schneckentriebe zusammen in meinem Prüfstand ein. Der Drehzahlmessstand kann damit mechanisch addierend oder subtrahierend im Bereich von 0 bis 9.999,9 zählen. Etwas Sorgfalt bei der Auswahl der Teile und bei der Justierung ist schon angebracht. Die Wellen müssen gerade sein und eine Schmierung der Lager bringt erkennbare Vorteile. Eine Reserve hat er noch, die Zahnräder werden noch nicht geschmiert!