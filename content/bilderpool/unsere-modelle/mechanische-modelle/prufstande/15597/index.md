---
layout: "image"
title: "Drehimpuls-Prüfstand (3/11)"
date: "2008-09-25T17:47:40"
picture: "pruefstanddrehimpulse03.jpg"
weight: "3"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/15597
imported:
- "2019"
_4images_image_id: "15597"
_4images_cat_id: "1430"
_4images_user_id: "723"
_4images_image_date: "2008-09-25T17:47:40"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15597 -->
Robo Interface:
Das ist die aktuelle Beschaltung in der Konfiguration mit Robo Interface. Eine ablehnende Haltung im Fanclub-Forum scheint hierbei zur Nutzung der analogen Anschlüsse A1 und A2 zu bestehen unter dem Thema "RoboPro+Interface: DCV-Messung mit Kommastellen?". Erste Programme in LLWin und RoboPro habe ich mir zum Prüfstand schon erstellt bzw. ich arbeite auch damit. Für mich als Anfänger in der Anwendung dieser grafischen Programmiersprache ist der Weg aber noch weit, bis meine "Kreationen" das öffentliche Interesse suchen sollten. Also fragt mir dazu bitte vorläufig keine Löcher in den Bauch und lasst mir die noch stille Freude an meinen Anzeigefeldern.
Die Farbcodierung der Flachstecker habe ich vom Universalinterface übernommen.