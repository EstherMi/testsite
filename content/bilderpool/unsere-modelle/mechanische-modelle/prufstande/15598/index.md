---
layout: "image"
title: "Drehimpuls-Prüfstand (4/11)"
date: "2008-09-25T17:47:41"
picture: "pruefstanddrehimpulse04.jpg"
weight: "4"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/15598
imported:
- "2019"
_4images_image_id: "15598"
_4images_cat_id: "1430"
_4images_user_id: "723"
_4images_image_date: "2008-09-25T17:47:41"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15598 -->
Das ist die Stelle, wo drei Kegelräder zusammenlaufen. Die Baugruppe "Impulsrad" ist aber hier mal zurückgestellt.