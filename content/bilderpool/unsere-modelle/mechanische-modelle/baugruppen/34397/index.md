---
layout: "image"
title: "Gelenk 30"
date: "2012-02-25T16:58:27"
picture: "Gelenk_30.jpg"
weight: "3"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/34397
imported:
- "2019"
_4images_image_id: "34397"
_4images_cat_id: "462"
_4images_user_id: "328"
_4images_image_date: "2012-02-25T16:58:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34397 -->
Benötigt man ein Gelenk auf 30 mm Länge, kann die "normale" Lösung mit dem Gelenkstein, 2 Bausteinen 7,5 und insgesamt 3 Verbindern möglicherweise zu instabil sein, und die Bausteine verschieben sich unter Last (im Bild oben).

Mein Gelenk 30 besteht aus 4 Kupplungsstücken 38253, 3 Scheiben 105195 und einem Klemmstift D4,1 107356 und ist in alle Richtungen wesentlich stabiler (im Bild in der Mitte). Der Stift hält die Scheiben bombenfest! Auf der rechten Seite ist zu sehen, dass das Gelenk exakt 15 mm hoch baut und somit auch prima zusätzlich gegen Verrutschen gesichert werden kann (hier 2 Bausteine 5).

Die mittlere Scheibe kann man durch ein weiteres Kupplungsstück 38253 ersetzen, was das Gelenk noch ein wenig stabiler macht (im Bild unten). Allerdings überschreitet es dann das Rastermaß von 15 mm. Es kann aber auch noch gut gegen Verschieben gesichert werden, z.B. wie im Bild durch 2 Bausteine 5.

Mangels kurzer Kupplungsstücke 38253 musste ich unten 2 lange Kupplungsstücke verbauen. Aber das Prinzip sollte erkennbar sein ... ;o)