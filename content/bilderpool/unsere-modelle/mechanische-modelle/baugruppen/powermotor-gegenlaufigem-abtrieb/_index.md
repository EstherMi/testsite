---
layout: "overview"
title: "PowerMotor mit gegenläufigem Abtrieb"
date: 2019-12-17T19:16:48+01:00
legacy_id:
- categories/1882
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1882 --> 
Hier wird ein PowerMotor so verbaut, dass er extrem robust zwei gegenläufig drehende Achsen antreibt.