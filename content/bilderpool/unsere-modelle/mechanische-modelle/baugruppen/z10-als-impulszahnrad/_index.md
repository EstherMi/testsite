---
layout: "overview"
title: "Z10 als Impulszahnrad"
date: 2019-12-17T19:16:38+01:00
legacy_id:
- categories/1696
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1696 --> 
Wenn das normale Impulszahnrad nicht hoch genug auflöst, kann man das Auslenken der Zähne eines Z10 mit einem Hebel so verstärken, dass es einen Taster betätigen kann.