---
layout: "image"
title: "Lange schmale Variante (3)"
date: "2009-08-03T22:51:44"
picture: "zalsimpulszahnrad3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/24696
imported:
- "2019"
_4images_image_id: "24696"
_4images_cat_id: "1696"
_4images_user_id: "104"
_4images_image_date: "2009-08-03T22:51:44"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24696 -->
Hier sieht man ganz gut, wie der Statikadapter als Hebelgelenk eingesetzt ist.