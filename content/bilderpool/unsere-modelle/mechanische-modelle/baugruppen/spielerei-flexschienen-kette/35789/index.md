---
layout: "image"
title: "Antrieb"
date: "2012-10-04T23:36:35"
picture: "spielereimitflexschienenundkette2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/35789
imported:
- "2019"
_4images_image_id: "35789"
_4images_cat_id: "2646"
_4images_user_id: "104"
_4images_image_date: "2012-10-04T23:36:35"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35789 -->
Der Antrieb ist gegenüber der Fassung des Videos um ein zusätzliches Z10 ergänzt, um ein Überspringen der Kette zu verhindern. Die roten BS15 des Motors sind dazu nicht mehr bis zum Anschlag in die Nut der Bauplatte eingeschoben.