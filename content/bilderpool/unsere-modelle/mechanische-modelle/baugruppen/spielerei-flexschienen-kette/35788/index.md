---
layout: "image"
title: "Gesamtansicht"
date: "2012-10-04T23:36:35"
picture: "spielereimitflexschienenundkette1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/35788
imported:
- "2019"
_4images_image_id: "35788"
_4images_cat_id: "2646"
_4images_user_id: "104"
_4images_image_date: "2012-10-04T23:36:35"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35788 -->
Die fischertechnik-Kette passt, wie man sieht, recht gut in die Flexschiene. Jetzt fehlt nur noch das Problem, was man damit lösen kann - die Anwendungsmöglichkeit.

Ein Video gibt's unter http://www.youtube.com/watch?v=Wb4iCmLEX_o