---
layout: "image"
title: "Antrieb8871.JPG"
date: "2013-10-19T16:46:11"
picture: "IMG_8871.JPG"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/37730
imported:
- "2019"
_4images_image_id: "37730"
_4images_cat_id: "1855"
_4images_user_id: "4"
_4images_image_date: "2013-10-19T16:46:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37730 -->
Der Motor sitzt unten auf zwei Federnocken. Die WInkelsteine rund herum müssen ihn trotzdem in der Lage fixieren.