---
layout: "image"
title: "IMG_8879.JPG"
date: "2013-10-19T16:47:59"
picture: "IMG_8879.JPG"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/37731
imported:
- "2019"
_4images_image_id: "37731"
_4images_cat_id: "1855"
_4images_user_id: "4"
_4images_image_date: "2013-10-19T16:47:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37731 -->
Hier ein Antrieb auf ein Rast-Z10 (nicht ganz original). Die angetriebene Achse ist nicht mehr im Raster.