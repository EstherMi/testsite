---
layout: "image"
title: "Antrieb0017.JPG"
date: "2013-10-19T17:14:10"
picture: "IMG_0017.JPG"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/37735
imported:
- "2019"
_4images_image_id: "37735"
_4images_cat_id: "1855"
_4images_user_id: "4"
_4images_image_date: "2013-10-19T17:14:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37735 -->
Zwei Exoten vereint: das Getriebe 31069 und der Drehschieber 31070, die sich bisher standhaft geweigert haben, sinnvolles zu einem Modell beizutragen. Manchmal geht's aber :-)