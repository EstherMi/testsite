---
layout: "image"
title: "Antrieb2099c.jpg"
date: "2010-01-27T19:00:34"
picture: "IMG_2100b.jpg"
weight: "25"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["31069"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/26161
imported:
- "2019"
_4images_image_id: "26161"
_4images_cat_id: "1855"
_4images_user_id: "4"
_4images_image_date: "2010-01-27T19:00:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26161 -->
hier von der Rückseite her. Die Winkel 60 sorgen für den richtigen Abstand zwischen Schnecke und Eingangszahnrad des Stufengetriebes.