---
layout: "image"
title: "Antrieb9512.JPG"
date: "2013-10-19T17:26:55"
picture: "IMG_9512.JPG"
weight: "16"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/37738
imported:
- "2019"
_4images_image_id: "37738"
_4images_cat_id: "1855"
_4images_user_id: "4"
_4images_image_date: "2013-10-19T17:26:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37738 -->
Drei Schnecken hintereinander, davon zwei kleine, aber diesmal mit dem S-Motor. Am 31069 sind zwei Kanten abgeschrägt. Damit da auch "Power" drüber geht, müssen die beiden Getriebe miteinander verstiftet werden (das ist so beim Tarnkappenbomber geschehen).