---
layout: "image"
title: "Antrieb9511.JPG"
date: "2013-10-19T17:05:09"
picture: "IMG_9511mit.JPG"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/37733
imported:
- "2019"
_4images_image_id: "37733"
_4images_cat_id: "1855"
_4images_user_id: "4"
_4images_image_date: "2013-10-19T17:05:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37733 -->
Drei Schnecken, davon zwei kleine. Der "Getriebehalter grau mit Schnecke 0,5" 31069 fristete so ein Schattendasein in seiner Magazinbox, da musste doch was zu machen sein. Etwas Modding musste aber sein: zwei Kanten wurden angeschrägt (eine sieht man hier glänzen), damit das zweite Getriebe drüber passt.