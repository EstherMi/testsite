---
layout: "image"
title: "S-Motor mit Untersetzung, Aufbau im FT-Raster"
date: "2015-05-03T19:52:12"
picture: "S-Motor_mit_Untersetzung_02.jpg"
weight: "2"
konstrukteure: 
- "pinkepunk"
fotografen:
- "pinkepunk"
keywords: ["S-Motor", "Getriebe", "Z44", "Untersetzung"]
uploadBy: "pinkepunk"
license: "unknown"
legacy_id:
- details/40943
imported:
- "2019"
_4images_image_id: "40943"
_4images_cat_id: "1855"
_4images_user_id: "2431"
_4images_image_date: "2015-05-03T19:52:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40943 -->
S-Motor mit Getriebe und zusätzlicher Z44 Untersetzung, Aufbau im FT-Raster