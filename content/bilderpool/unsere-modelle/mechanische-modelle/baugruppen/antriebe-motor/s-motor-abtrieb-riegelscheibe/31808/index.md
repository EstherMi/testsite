---
layout: "image"
title: "Von der Seite"
date: "2011-09-17T16:48:52"
picture: "smotorabtriebmitriegelscheibe3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/31808
imported:
- "2019"
_4images_image_id: "31808"
_4images_cat_id: "2374"
_4images_user_id: "104"
_4images_image_date: "2011-09-17T16:48:52"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31808 -->
Das passt alles wie dafür gemacht. Verwendet wurden die aktuellen Winkelsteine 30° (es gibt eine eine Spur kleinere Fassung aus den Ur-ft-Zeiten; findet man auch hier im Bilderpool).