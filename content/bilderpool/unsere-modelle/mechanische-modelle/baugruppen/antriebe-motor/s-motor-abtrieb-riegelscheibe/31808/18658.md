---
layout: "comment"
hidden: true
title: "18658"
date: "2014-02-03T20:28:58"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Die älteren sind eine Spur kleiner, da greift die Schnecke dann nicht sauber in das kleine Zahnrädchen. Evtl. reibt letzteres dann sogar am Baustein - das habe ich aber nicht probiert.

Zum Unterschied zwischen beiden Varianten siehe "Perlentauchen (1)" in der ft:pedia 3/2012.

Gruß,
Stefan