---
layout: "image"
title: "Antrieb1601b.jpg"
date: "2013-10-19T20:21:38"
picture: "IMG_0035.JPG"
weight: "20"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/37743
imported:
- "2019"
_4images_image_id: "37743"
_4images_cat_id: "1855"
_4images_user_id: "4"
_4images_image_date: "2013-10-19T20:21:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37743 -->
Antrieb mit dem kleinen Schneckengetriebe auf Z44. Es ist dieselbe Anordnung wie im vorigen Bild, nur auf "wundersame" Weise ist jetzt die Achse im Raster (und dafür der Motor nicht mehr).