---
layout: "image"
title: "xm_2112.JPG"
date: "2010-01-27T19:20:19"
picture: "xm_2112.JPG"
weight: "36"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/26172
imported:
- "2019"
_4images_image_id: "26172"
_4images_cat_id: "1855"
_4images_user_id: "4"
_4images_image_date: "2010-01-27T19:20:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26172 -->
Höhö. Noch simpler geht's nimmer. Damit bau ich gerade ein Kettenfahrzeug: einer links, einer rechts (nix mit Gleichlaufgetriebe) und ab geht die Fahrt.