---
layout: "image"
title: "Antrieb2099b.jpg"
date: "2010-01-27T18:59:23"
picture: "IMG_2099b.jpg"
weight: "24"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["31069"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/26160
imported:
- "2019"
_4images_image_id: "26160"
_4images_cat_id: "1855"
_4images_user_id: "4"
_4images_image_date: "2010-01-27T18:59:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26160 -->
Extra langsam, durch Hintereinanderschalten von Schnecke und Stufengetriebe.