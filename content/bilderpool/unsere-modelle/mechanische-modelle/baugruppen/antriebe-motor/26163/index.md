---
layout: "image"
title: "Antrieb2102b.jpg"
date: "2010-01-27T19:03:58"
picture: "IMG_2102b.jpg"
weight: "27"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/26163
imported:
- "2019"
_4images_image_id: "26163"
_4images_cat_id: "1855"
_4images_user_id: "4"
_4images_image_date: "2010-01-27T19:03:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26163 -->
Noch eine sehr starke Untersetzung. Die roten Steine rechts sorgen nur dafür, dass die Schnecke unter Last nicht abhaut.