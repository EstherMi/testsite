---
layout: "image"
title: "Antrieb0027.JPG"
date: "2013-10-19T16:57:35"
picture: "IMG_0027.JPG"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/37732
imported:
- "2019"
_4images_image_id: "37732"
_4images_cat_id: "1855"
_4images_user_id: "4"
_4images_image_date: "2013-10-19T16:57:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37732 -->
Hier geht es vom Stufengetriebe auf ein Z44, mit verschiedenen Übersetzungen. Im Raster ist keins davon, aber es kann nirgendwo etwas verrutschen, weil die Nuten der Bausteine immer quer zur Kraft liegen, die sie verschieben würde.