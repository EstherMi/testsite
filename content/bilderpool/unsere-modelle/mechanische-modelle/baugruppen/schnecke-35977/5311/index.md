---
layout: "image"
title: "Winkelantrieb03.JPG"
date: "2005-11-11T12:29:35"
picture: "Winkelantrieb03.JPG"
weight: "14"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5311
imported:
- "2019"
_4images_image_id: "5311"
_4images_cat_id: "456"
_4images_user_id: "4"
_4images_image_date: "2005-11-11T12:29:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5311 -->
Mit dem Differential-Abtriebsrad 31413 bleibt man mit allen Teilen im ft-Rastermaß :-)