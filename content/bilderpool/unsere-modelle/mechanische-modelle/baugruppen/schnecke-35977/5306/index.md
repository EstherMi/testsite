---
layout: "image"
title: "Querruder02.JPG"
date: "2005-11-11T12:20:16"
picture: "Querruder02.JPG"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5306
imported:
- "2019"
_4images_image_id: "5306"
_4images_cat_id: "456"
_4images_user_id: "4"
_4images_image_date: "2005-11-11T12:20:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5306 -->
Der Antrieb war zuerst über das Z10 links außen vorgesehen, da kam aber später noch eine Untersetzung 1:3 dazwischen.