---
layout: "comment"
hidden: true
title: "8808"
date: "2009-03-21T20:41:26"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Nein, sowas hatte ich nicht. Muss aber nichts heißen, denn ich habe mit diesem Antrieb bisher noch keine großartigen Kräfte übertragen. 
Ich habe einige Schnecken, deren beide Hälften minimal versetzt zusammengeschweißt sind. Diese Stoßkante dürfte bei dir auch "zugeschlagen" haben.

Aber - hast du auch [b]zwei[/b] gegenläufige Schnecken verwendet? Mit nur einer Schnecke kommt keine große Freude auf.

Gruß,
Harald