---
layout: "image"
title: "Kraftheber2-03.JPG"
date: "2005-11-11T12:10:36"
picture: "Kraftheber2-03.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5302
imported:
- "2019"
_4images_image_id: "5302"
_4images_cat_id: "456"
_4images_user_id: "4"
_4images_image_date: "2005-11-11T12:10:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5302 -->
