---
layout: "image"
title: "Kraftheber2-02.JPG"
date: "2005-11-11T12:09:51"
picture: "Kraftheber2-02.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5301
imported:
- "2019"
_4images_image_id: "5301"
_4images_cat_id: "456"
_4images_user_id: "4"
_4images_image_date: "2005-11-11T12:09:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5301 -->
Das Z20 hat keinen Klemmring, die Zapfwelle (mit Kardan) läuft ungehindert hindurch.