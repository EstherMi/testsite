---
layout: "image"
title: "Querruder04.JPG"
date: "2005-11-11T12:22:48"
picture: "Querruder04.JPG"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5308
imported:
- "2019"
_4images_image_id: "5308"
_4images_cat_id: "456"
_4images_user_id: "4"
_4images_image_date: "2005-11-11T12:22:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5308 -->
So ist es dann im Flieger eingebaut. Der Antrieb kommt vom Klemm-Z10 über das Z30 über das zweite Rast-Z10.