---
layout: "comment"
hidden: true
title: "15367"
date: "2011-10-06T00:07:49"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Das ist doch Haralds Lieblingsteil, die http://www.ft-datenbank.de/search.php?keyword=32455 "E-Magnet Führungsplatte 15x20".

Elegant ist das. Ich rieche schon förmlich die schönen Modelle, zu denen Du da kommen wirst! :-)

Gruß,
Stefan