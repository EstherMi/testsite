---
layout: "image"
title: "(2/8) Drehkranz, Ringansicht"
date: "2008-10-20T21:35:34"
picture: "drehkranz2.jpg"
weight: "2"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/16021
imported:
- "2019"
_4images_image_id: "16021"
_4images_cat_id: "1442"
_4images_user_id: "723"
_4images_image_date: "2008-10-20T21:35:34"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16021 -->
Die bislang verwendete Stabilisierung der Bogenteilverbindungen mit I-Strebe 15 wurde durch die I-Strebe 45 ersetzt. Damit wird nicht nur eine Stabilisierung radial sondern auch axial erreicht. Der Lagerring (unteres Ringpaar) ist jetzt viermal auf der Arbeitsplatte befestigt. Die Kette wird axial von den I-Steben geführt und sitzt mit Vorspannung auf dem Zylindermantel des Drehrings fest gegen Verrutschen. In den Spalt zwischen den beiden Drehringhälften kann das Z30 mit seinen Zähnen ohne Behinderung eintauchen.