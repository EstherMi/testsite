---
layout: "image"
title: "(3/8) Drehkranz, Tauchantrieb"
date: "2008-10-20T21:35:34"
picture: "drehkranz3.jpg"
weight: "3"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/16022
imported:
- "2019"
_4images_image_id: "16022"
_4images_cat_id: "1442"
_4images_user_id: "723"
_4images_image_date: "2008-10-20T21:35:34"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16022 -->
Der unverzichtbare Tauchantrieb. Im Rahmen seiner Geometrie können Radialhub und Federvorspannung stufenlos eingestellt werden. Die Griffhöhe der S-Riegel erfordert eine größere Tauchtiefe. Deshalb mußte hier ein Z30 eingesetzt werden.