---
layout: "comment"
hidden: true
title: "7612"
date: "2008-10-17T17:47:21"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Stefan Falk und Stephan Wenkers,
auch bei einer auf den Umfang des Bogenzylinders gelegten Kette wird korrekterweise ein solcher "Tauchantrieb" benötigt, da die Bögen nicht rund sind. Der Durchmesser dieser Bögen ist jeweils in Höhe der Bogenstückmitten am größten und an den Verbindungen am kleinsten, die sonstigen Unrundheiten jetzt mal nicht mit berücksichtigt.
Gruß, Ingo