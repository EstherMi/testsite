---
layout: "image"
title: "(5/6) Radialführung, Gesamtansicht"
date: "2008-10-13T23:09:48"
picture: "waelzlageraxialradialfuehrung5.jpg"
weight: "5"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/15965
imported:
- "2019"
_4images_image_id: "15965"
_4images_cat_id: "1450"
_4images_user_id: "723"
_4images_image_date: "2008-10-13T23:09:48"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15965 -->
Anforderungen:
Ausgehend von den mit dem Nachbau des Originals gesammelten Erkenntnissen ergaben sich an die konstruktive Weiterentwicklung folgende Anforderungen:
1. Der Wälzkörperring muß radial elastisch bleiben, damit seine Wälzkörper (Räder 23 schwarz und weiß) die Durchmesserdifferenz der Laufringe zwischen den Stoßverbindungen und der Bogenstückmitten von 0,5mm auf den Durchmesser ohne Laufspiel überwinden können.
2. Die Zapfen der axialen Radachsen, BS 7,5 und WS 15° sind gegen Verdrehen zu stabilisieren.
Lösungsprinzip:
1. Das Kippmoment der Radachsen wird über angesetzte Achsverschraubungen von Metallachsen aufgenommen
2. Ein "schwimmender" Führungsring gleicht die Momente aus und hält dabei den Wälzkörperring radial elastisch.
Funktion:
1. Die Metallachsen der schwarzen Wälzkörper wollen axial nach oben und die der weißen nach unten kippen. Der Führungsring gleicht das aus und findet dabei seine axiale Lage. 
2. Die Metallachsen bewegen sich dabei gleichzeitig radial mit den Unterschieden der inneren Durchmesser der Laufringe frei in den Bohrungen der BS15. Der Führungsring findet dabei seine radiale Zentrierung durch ihre 12-teilige Anordnung. 
3. Die Ortslage des oberen Laufringes bleibt über dem unteren zentriert.