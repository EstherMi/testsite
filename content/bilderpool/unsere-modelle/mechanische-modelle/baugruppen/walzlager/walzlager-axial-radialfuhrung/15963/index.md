---
layout: "image"
title: "(3/6) Kettenführung"
date: "2008-10-13T23:09:47"
picture: "waelzlageraxialradialfuehrung3.jpg"
weight: "3"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/15963
imported:
- "2019"
_4images_image_id: "15963"
_4images_cat_id: "1450"
_4images_user_id: "723"
_4images_image_date: "2008-10-13T23:09:47"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15963 -->
Eine endlose ft-Kette außen an den Zylindermantel des oberen Laufringes angelegt läßt sich ohne nach oben überstehende Teile nicht verwirklichen. Deshalb habe ich als Behelf zur Erprobung der angestrebten konstruktiven Lösung eine Befestigung mit einer straff aufgezogenen ft-Kette als 24-Eck realisiert.