---
layout: "image"
title: "Nachbau Axiallager (6/7) Lager komplett"
date: "2008-10-03T09:42:12"
picture: "axiallagernachbau6.jpg"
weight: "14"
konstrukteure: 
- "Remadus, Nachbau Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/15806
imported:
- "2019"
_4images_image_id: "15806"
_4images_cat_id: "1442"
_4images_user_id: "723"
_4images_image_date: "2008-10-03T09:42:12"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15806 -->
Das Aufsetzen des oberen Laufrings auf den Wälzkäfig erfolgt äquvalent wie das Aufsetzen des Wälzkäfigs auf den unteren Laufring. Hierbei werden die Bogenstückmitten des oberen Laufrings auf die weißen Räder ausgerichtet.