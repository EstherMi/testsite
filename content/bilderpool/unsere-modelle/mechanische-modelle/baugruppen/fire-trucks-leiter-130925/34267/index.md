---
layout: "image"
title: "Leiter mit Getriebe V1.2"
date: "2012-02-19T13:45:05"
picture: "DSCN4599.jpg"
weight: "7"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: ["Leiter", "130925"]
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/34267
imported:
- "2019"
_4images_image_id: "34267"
_4images_cat_id: "2529"
_4images_user_id: "184"
_4images_image_date: "2012-02-19T13:45:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34267 -->
Hier der genaue Aufbau:
Die Klemmbuchse, die innerhalb der Leiterseitenwand liegt, muss sich leicht drehen
lassen. Das geht nicht mit allen Klemmbuchsen. Ich musste da ein bischen suchen.
Die Riegelscheiben und die anderen Klemmbuchsen sollten sehr fest sitzen.