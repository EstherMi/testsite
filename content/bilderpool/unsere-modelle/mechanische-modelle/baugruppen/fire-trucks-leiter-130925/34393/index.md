---
layout: "image"
title: "zwei Leitern als ausfahrbare Rampe"
date: "2012-02-24T18:19:39"
picture: "DSCN4611.jpg"
weight: "4"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: ["Leiter", "130925"]
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/34393
imported:
- "2019"
_4images_image_id: "34393"
_4images_cat_id: "2529"
_4images_user_id: "184"
_4images_image_date: "2012-02-24T18:19:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34393 -->
Am unteren Ende der Leiter ist eine kleine Auffahrrampe.