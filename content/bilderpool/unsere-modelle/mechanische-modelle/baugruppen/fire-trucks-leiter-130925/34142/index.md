---
layout: "image"
title: "als Schranke für einen Bahnübergang"
date: "2012-02-12T14:48:21"
picture: "Schranke_0.jpg"
weight: "15"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: ["Leiter", "130925"]
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/34142
imported:
- "2019"
_4images_image_id: "34142"
_4images_cat_id: "2529"
_4images_user_id: "184"
_4images_image_date: "2012-02-12T14:48:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34142 -->
Dieses Modell hatte ich in einer Modellschau in Münster ausgestellt.