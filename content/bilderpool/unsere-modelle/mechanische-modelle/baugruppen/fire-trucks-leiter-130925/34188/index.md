---
layout: "image"
title: "Befestigung"
date: "2012-02-15T14:35:41"
picture: "DSCN4563.jpg"
weight: "14"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: ["Leiter", "130925"]
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/34188
imported:
- "2019"
_4images_image_id: "34188"
_4images_cat_id: "2529"
_4images_user_id: "184"
_4images_image_date: "2012-02-15T14:35:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34188 -->
Die Befestigung ist genau so wie bei den Bildern vorher.