---
layout: "image"
title: "Leiter mit Getriebe V1.2"
date: "2012-02-19T13:45:05"
picture: "DSCN4581.jpg"
weight: "6"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: ["Leiter", "130925"]
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/34266
imported:
- "2019"
_4images_image_id: "34266"
_4images_cat_id: "2529"
_4images_user_id: "184"
_4images_image_date: "2012-02-19T13:45:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34266 -->
Die Lagerung der kurzen Achse war mit den Hülsen 15 nicht optimal.
Ich habe hier eine andere Lösung gefunden.