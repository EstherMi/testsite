---
layout: "image"
title: "Leiter mit Getriebe V1.2"
date: "2012-02-19T13:45:05"
picture: "DSCN4588.jpg"
weight: "5"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: ["Leiter", "130925"]
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/34265
imported:
- "2019"
_4images_image_id: "34265"
_4images_cat_id: "2529"
_4images_user_id: "184"
_4images_image_date: "2012-02-19T13:45:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34265 -->
Hier habe ich die BS15 gegen andere Halterungen getauscht.
Sieht doch so etwas dezenter aus. Durch einfügen des BS7,5 wird die K-Achse
etwas nach außen gebracht und man kann eine Achse (hier die Metallachse)
zur Lagerung (Drehung) einbauen.