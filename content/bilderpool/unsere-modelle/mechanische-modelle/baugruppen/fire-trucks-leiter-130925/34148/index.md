---
layout: "image"
title: "Befestigungsmöglichkeiten"
date: "2012-02-12T14:48:34"
picture: "Befestigung_Strebe_45.jpg"
weight: "20"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: ["Leiter", "130925"]
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/34148
imported:
- "2019"
_4images_image_id: "34148"
_4images_cat_id: "2529"
_4images_user_id: "184"
_4images_image_date: "2012-02-12T14:48:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34148 -->
Zwei Leitern verbindet man am besten so. Weil die Achsen nicht dicker als die Sprossen sind liegen beide Leitern flach auf.