---
layout: "image"
title: "35998-03.JPG"
date: "2006-01-16T17:58:09"
picture: "35998-03.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Gleitführung", "Schiebetür", "Gabelstapler"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5592
imported:
- "2019"
_4images_image_id: "5592"
_4images_cat_id: "485"
_4images_user_id: "4"
_4images_image_date: "2006-01-16T17:58:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5592 -->
Das wäre was für Schiebetüren und Gabelstapler oder wo auch immer eine Gleitführung gebraucht wird. Die Bauplatte 15x90 steckt lose drinnen und hält einmal die Statikelement im richtigen Abstand und gleichzeitig verhindert sie ein Kippen der Lenkwürfel.