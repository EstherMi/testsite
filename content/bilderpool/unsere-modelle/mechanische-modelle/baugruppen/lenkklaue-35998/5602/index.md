---
layout: "image"
title: "Container-B02.JPG"
date: "2006-01-16T18:14:26"
picture: "Container-B02.JPG"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5602
imported:
- "2019"
_4images_image_id: "5602"
_4images_cat_id: "485"
_4images_user_id: "4"
_4images_image_date: "2006-01-16T18:14:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5602 -->
