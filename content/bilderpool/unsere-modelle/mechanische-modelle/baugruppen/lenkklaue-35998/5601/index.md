---
layout: "image"
title: "Container-B01.JPG"
date: "2006-01-16T18:13:59"
picture: "Container-B01.JPG"
weight: "12"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5601
imported:
- "2019"
_4images_image_id: "5601"
_4images_cat_id: "485"
_4images_user_id: "4"
_4images_image_date: "2006-01-16T18:13:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5601 -->
Der Winkelstein 38423 passt auch (rechts eingebaut), man muss aber genau treffen.