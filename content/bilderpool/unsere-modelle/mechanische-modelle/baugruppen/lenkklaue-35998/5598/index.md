---
layout: "image"
title: "Container-A02.JPG"
date: "2006-01-16T18:11:28"
picture: "Container-A02.JPG"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5598
imported:
- "2019"
_4images_image_id: "5598"
_4images_cat_id: "485"
_4images_user_id: "4"
_4images_image_date: "2006-01-16T18:11:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5598 -->
Klack-Klack, Container am Hubgeschirr angedockt.