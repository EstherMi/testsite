---
layout: "image"
title: "Pneumatisch abschaltbare Handkurbel (Mechanik)"
date: "2009-12-06T19:40:51"
picture: "pneukurbl2.jpg"
weight: "2"
konstrukteure: 
- "Marius"
fotografen:
- "Marius"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- details/25914
imported:
- "2019"
_4images_image_id: "25914"
_4images_cat_id: "1822"
_4images_user_id: "430"
_4images_image_date: "2009-12-06T19:40:51"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25914 -->
Hier sieht man, wie das Z10 in das Z20 greift. Das Z10 ist über ein Kegelrad mit der Kurbel verbunden. Das Z20 überträgt dann die Drehbewegung auf die Welle, die dann das Modell bewegt.