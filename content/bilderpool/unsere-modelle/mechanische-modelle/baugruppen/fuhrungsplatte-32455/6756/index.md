---
layout: "image"
title: "32455-Lenk02.JPG"
date: "2006-08-29T21:14:35"
picture: "32455-Lenk02.JPG"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/6756
imported:
- "2019"
_4images_image_id: "6756"
_4images_cat_id: "649"
_4images_user_id: "4"
_4images_image_date: "2006-08-29T21:14:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6756 -->
Das Alternativ-Hubgetriebe passt prima in eine Vorderachse hinein.