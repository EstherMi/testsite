---
layout: "image"
title: "Vorderachse02.JPG"
date: "2007-08-05T17:17:06"
picture: "Vorderachse02.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/11303
imported:
- "2019"
_4images_image_id: "11303"
_4images_cat_id: "649"
_4images_user_id: "4"
_4images_image_date: "2007-08-05T17:17:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11303 -->
Die Lenkung erfolgt durch die gleiche Achse, um die auch die ganze Achse pendelt. Das hier ist die Ansicht vom Fahrzeugheck her.