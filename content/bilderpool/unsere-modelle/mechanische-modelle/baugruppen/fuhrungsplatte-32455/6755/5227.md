---
layout: "comment"
hidden: true
title: "5227"
date: "2008-02-06T20:55:25"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Ja genau. Und dann kann man sogar auf den Stein und die Zahnradkonstruktion in der Mitte verzichten. Das wird dann eine schön klein bauende Laufkatze (so geschehen im Portalkran vom Transportflieger).