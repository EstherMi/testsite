---
layout: "image"
title: "32455-Twin02.JPG"
date: "2006-08-29T21:19:06"
picture: "32455-Twin02.JPG"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/6759
imported:
- "2019"
_4images_image_id: "6759"
_4images_cat_id: "649"
_4images_user_id: "4"
_4images_image_date: "2006-08-29T21:19:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6759 -->
Die Alternativ-Hubgetriebe lassen sich prima parallel schalten (mit dem Original müsste man zwei Motoren synchronisieren und mehr Platz spendieren).