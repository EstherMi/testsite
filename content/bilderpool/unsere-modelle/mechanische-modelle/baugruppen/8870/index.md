---
layout: "image"
title: "Gelenk"
date: "2007-02-04T12:35:30"
picture: "gelenk1.jpg"
weight: "4"
konstrukteure: 
- "Michael K."
fotografen:
- "Michael K."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Wert"
license: "unknown"
legacy_id:
- details/8870
imported:
- "2019"
_4images_image_id: "8870"
_4images_cat_id: "462"
_4images_user_id: "366"
_4images_image_date: "2007-02-04T12:35:30"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8870 -->
Falls  einmal ein Gelenk fehlen sollte, kann man es auch so nachbauen. oben sieht man die benötigten Bauteile, unten das fertige Gelenk.