---
layout: "image"
title: "Greifer"
date: "2010-10-01T14:51:17"
picture: "greifer3.jpg"
weight: "3"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: ["35977"]
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/28795
imported:
- "2019"
_4images_image_id: "28795"
_4images_cat_id: "2096"
_4images_user_id: "373"
_4images_image_date: "2010-10-01T14:51:17"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28795 -->
von unten