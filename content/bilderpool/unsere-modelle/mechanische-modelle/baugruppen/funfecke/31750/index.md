---
layout: "image"
title: "5Eck-B_5715m.JPG"
date: "2011-09-05T20:00:46"
picture: "5eck-B_5715m.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Vieleck"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/31750
imported:
- "2019"
_4images_image_id: "31750"
_4images_cat_id: "2368"
_4images_user_id: "4"
_4images_image_date: "2011-09-05T20:00:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31750 -->
Eine andere Ausführung des "großen" Fünfecks. Diesmal gleiten die Speichenräder in den Nuten von BS5. Damit die Abstände stimmen, muss innen drin ziemlich gefummelt werden.