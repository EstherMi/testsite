---
layout: "image"
title: "5Eck_A_5722"
date: "2011-09-05T19:53:26"
picture: "5eck-A_5722m.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Speichenrad", "19317", "36916"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/31747
imported:
- "2019"
_4images_image_id: "31747"
_4images_cat_id: "2368"
_4images_user_id: "4"
_4images_image_date: "2011-09-05T19:53:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31747 -->
Das sollte einmal das Kernstück eines Fahrgeschäfts mit fünf Gondelträgern werden. Das Vorbild hat auch nur fünf, und sechse hätten in zusammengeklapptem Zustand nicht mehr gepasst.

Die "Zwischenstücke" 38424 haben zwei durchgehende vollrunde Nuten, in denen der Kranz des Speichenrades 19317 / 36916 gleitet. Lagerung und Antrieb sind räumlich getrennt. 
Das untere Speichenrad wird von meinem Lieblingsteil 32455 geführt (deswegen hat es ja auch das "Führungsplatte" im Namen). Als Antrieb dient ein Z15 in der Lenkklaue. Damit das passt, ist nur noch eine Klemmbuchse 5 dazu gepackt, die die Führung am anderen Innenzapfen der Lenkklaue übernimmt.