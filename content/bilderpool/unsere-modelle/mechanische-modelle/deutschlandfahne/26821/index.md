---
layout: "image"
title: "Deutschland-Fahne 3"
date: "2010-03-27T21:54:39"
picture: "Fahne_3.jpg"
weight: "3"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/26821
imported:
- "2019"
_4images_image_id: "26821"
_4images_cat_id: "1916"
_4images_user_id: "328"
_4images_image_date: "2010-03-27T21:54:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26821 -->
Eine Detailansicht der Rückseite. Man sieht gut den Antrieb der Schnecke, den unteren (betätigten) Endschalter und die dafür notwendige aufwendige Verkabelung.