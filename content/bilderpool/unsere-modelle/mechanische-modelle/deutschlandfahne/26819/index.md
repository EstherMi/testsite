---
layout: "image"
title: "Deutschland -Fahne 1"
date: "2010-03-27T21:54:39"
picture: "Fahne_1.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/26819
imported:
- "2019"
_4images_image_id: "26819"
_4images_cat_id: "1916"
_4images_user_id: "328"
_4images_image_date: "2010-03-27T21:54:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26819 -->
Passend zur diesjährigen Fußball-WM oder auch zur aktuellen Formel-1-Saison mit Mercedes-Benz-Werksteam und Michael Schumacher gibt es hier von mir eine elektrisch hissbare Deutschland-Fahne.

Mit Schneckentrieb und Endschaltern vielleicht etwas zu viel Technik für eine einfache Fahne, aber es funktioniert super!

Und immer schön aufstehen beim Fahne Hissen! ;o)