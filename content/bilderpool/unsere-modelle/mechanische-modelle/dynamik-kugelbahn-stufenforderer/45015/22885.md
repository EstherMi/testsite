---
layout: "comment"
hidden: true
title: "22885"
date: "2017-01-08T18:47:50"
uploadBy:
- "allsystemgmbh"
license: "unknown"
imported:
- "2019"
---
... ja den Kasten 30861 habe ich mit Düsenbaustein unbespielt...
https://www.ftcommunity.de/detahttps://ft-datenbank.de/details.php?ArticleVariantId=a8d9b915-2c7d-442c-ba93-2ea179a27a8eils.php?image_id=44229

... aber beim CVK  30862  fehlt mir leider genau dieser Düsenbaustein...
https://ft-datenbank.de/details.php?ArticleVariantId=cae47e02-315a-4c28-ae8d-da1aec3bdd93

Also dann schauen wir mal wer den Düsenbaustein schneller ergattert ;-)