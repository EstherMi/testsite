---
layout: "image"
title: "Doppelfalttor  9"
date: "2007-08-10T17:07:41"
picture: "doppelfalttor09.jpg"
weight: "13"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/11344
imported:
- "2019"
_4images_image_id: "11344"
_4images_cat_id: "1019"
_4images_user_id: "502"
_4images_image_date: "2007-08-10T17:07:41"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11344 -->
Da die 3. Tür extern gesteuert wird kann sie geöfftnet werden ohne dass das ganze Tor aufgeht.