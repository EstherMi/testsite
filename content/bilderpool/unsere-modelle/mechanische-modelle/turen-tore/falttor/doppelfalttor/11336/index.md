---
layout: "image"
title: "Doppelfalttor  1"
date: "2007-08-10T17:07:04"
picture: "doppelfalttor01.jpg"
weight: "5"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/11336
imported:
- "2019"
_4images_image_id: "11336"
_4images_cat_id: "1019"
_4images_user_id: "502"
_4images_image_date: "2007-08-10T17:07:04"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11336 -->
Hier mal die ersten Bilder von meinem neuen Tor. Das gelbe Gestänge ist noch labbrig und muss noch besser justiert werden.