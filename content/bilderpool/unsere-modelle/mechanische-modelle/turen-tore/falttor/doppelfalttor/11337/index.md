---
layout: "image"
title: "Doppelfalttor  2"
date: "2007-08-10T17:07:05"
picture: "doppelfalttor02.jpg"
weight: "6"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/11337
imported:
- "2019"
_4images_image_id: "11337"
_4images_cat_id: "1019"
_4images_user_id: "502"
_4images_image_date: "2007-08-10T17:07:05"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11337 -->
