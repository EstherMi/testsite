---
layout: "image"
title: "Falttor 11"
date: "2007-02-28T19:26:00"
picture: "falttor11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/9168
imported:
- "2019"
_4images_image_id: "9168"
_4images_cat_id: "846"
_4images_user_id: "502"
_4images_image_date: "2007-02-28T19:26:00"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9168 -->
Hier die Führung des Tors und der Endtaster beim schließen.