---
layout: "image"
title: "Falttor 6"
date: "2007-02-28T19:25:41"
picture: "falttor06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/9163
imported:
- "2019"
_4images_image_id: "9163"
_4images_cat_id: "846"
_4images_user_id: "502"
_4images_image_date: "2007-02-28T19:25:41"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9163 -->
Hier die ganze Rückseite der Öffnungsmechanik.