---
layout: "image"
title: "Luke04-auf2.JPG"
date: "2007-01-13T14:55:41"
picture: "Luke04-auf2.JPG"
weight: "32"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/8411
imported:
- "2019"
_4images_image_id: "8411"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T14:55:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8411 -->
