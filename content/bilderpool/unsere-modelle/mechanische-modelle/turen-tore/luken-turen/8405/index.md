---
layout: "image"
title: "Luke02-auf.JPG"
date: "2007-01-13T14:50:08"
picture: "Luke02-auf.JPG"
weight: "26"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/8405
imported:
- "2019"
_4images_image_id: "8405"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T14:50:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8405 -->
Das ist eine symmetrisch aufgebaute Luke, z.B. für ein Flugzeug-Fahrwerk. Die beiden Deckel sind über die Riegelscheiben gekoppelt. Beim Öffnen schwenken die Deckel nach unten, damit links und rechts davon die Fahrwerksbeine herausfahren können.