---
layout: "image"
title: "Luke07-zu.JPG"
date: "2007-01-13T15:06:35"
picture: "Luke07-zu.JPG"
weight: "38"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/8417
imported:
- "2019"
_4images_image_id: "8417"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T15:06:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8417 -->
Das ist noch ein Viergelenk-Getriebe mit der Gelenkkurbel 35088. Den rechten Deckel muss man sich dazudenken.