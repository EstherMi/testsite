---
layout: "image"
title: "Luke04-zu.JPG"
date: "2007-01-13T14:54:45"
picture: "Luke04-zu.JPG"
weight: "30"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/8409
imported:
- "2019"
_4images_image_id: "8409"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T14:54:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8409 -->
