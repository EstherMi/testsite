---
layout: "image"
title: "Luke03-zu.JPG"
date: "2007-01-13T14:53:11"
picture: "Luke03-zu.JPG"
weight: "27"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/8406
imported:
- "2019"
_4images_image_id: "8406"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T14:53:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8406 -->
Die Kurbel 38447 leistet bei solchen Viergelenk-Getrieben unschätzbare Dienste.
Die Deckel klappen nicht einfach nur hinunter, sondern werden gleichzeitig nach außen und oben geführt. Ähnliche Klapp-Mechanismen findet man auch bei Küchen- und Wohnzimmerschränken.