---
layout: "image"
title: "Luke05-auf.JPG"
date: "2007-01-13T15:01:41"
picture: "Luke05-auf.JPG"
weight: "34"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/8413
imported:
- "2019"
_4images_image_id: "8413"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T15:01:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8413 -->
Hier sind Teile quer durchs ft-Sortiment verbaut. Die Zahnstange wird durch "Führungsplatten E-Magnet 32455" gehalten. Die Scharniere sind aus der Zahnspurstange 38472 und zugehörigen Lenkhebeln aufgebaut. Die beiden Streben I-30 sind oben in einem Statikadapter 35975 gelagert.