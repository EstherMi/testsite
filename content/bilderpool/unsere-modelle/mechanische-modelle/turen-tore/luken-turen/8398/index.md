---
layout: "image"
title: "Tuer09-zu1.JPG"
date: "2007-01-13T14:34:10"
picture: "Tuer09-zu1.JPG"
weight: "21"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/8398
imported:
- "2019"
_4images_image_id: "8398"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T14:34:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8398 -->
Diese Tür hat 75 mm Öffnungsweite. Sie kann durch Drehen an der Achse geöffnet und geschlossen werden. Als Mitnehmer für die Drehbewegung dient die Konstruktion mit den beiden Rastadaptern mittendrin.