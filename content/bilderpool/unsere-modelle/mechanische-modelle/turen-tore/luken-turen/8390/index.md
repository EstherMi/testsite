---
layout: "image"
title: "Tuer05-auf.JPG"
date: "2007-01-13T14:18:47"
picture: "Tuer05-auf.JPG"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/8390
imported:
- "2019"
_4images_image_id: "8390"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T14:18:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8390 -->
Das Bild täuscht: diese Tür öffnet 60 mm weit (es fehlt eine V-Platte 15x90 links).
Die Rollenlager 37636 sitzen je auf zwei Zapfen, aber immer nur halb.
Zwischen den beiden Radachsen 36586 sorgt eine K-Achse 30 für Stabilität.