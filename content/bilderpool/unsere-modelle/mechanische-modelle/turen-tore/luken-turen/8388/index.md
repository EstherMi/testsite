---
layout: "image"
title: "Tuer04-auf.JPG"
date: "2007-01-13T14:13:33"
picture: "Tuer04-auf.JPG"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/8388
imported:
- "2019"
_4images_image_id: "8388"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T14:13:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8388 -->
So sieht es aus, wenn man auf den Winkel verzichtet: diese Tür braucht die Aussparung im Winkelträger 60, um richtig weit aufzugehen.