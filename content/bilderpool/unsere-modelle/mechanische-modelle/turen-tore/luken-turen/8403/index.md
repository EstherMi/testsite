---
layout: "image"
title: "Luke01-auf.JPG"
date: "2007-01-13T14:46:35"
picture: "Luke01-auf.JPG"
weight: "24"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/8403
imported:
- "2019"
_4images_image_id: "8403"
_4images_cat_id: "773"
_4images_user_id: "4"
_4images_image_date: "2007-01-13T14:46:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8403 -->
Das ist eine symmetrisch aufgebaute Luke, z.B. für ein Flugzeug-Fahrwerk. Die beiden Deckel sind über die Riegelscheiben gekoppelt. Beim Öffnen schwenken die Deckel nach unten, damit links und rechts davon die Fahrwerksbeine herausfahren können.

Die Kurbel 38447 (oder auch 38235, 38239, 38445, die aber *nml* sind) leistet unschätzbare Dienste. In Bildmitte unten sieht man, wie die Kurbel zwischen Verkleidungsplatte und Federnocken stirnseitig am BS7,5 festgeklemmt wird.