---
layout: "image"
title: "Teleskoptür09"
date: "2008-02-07T21:08:18"
picture: "teleskoptuernachlinksoeffnend9.jpg"
weight: "9"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/13596
imported:
- "2019"
_4images_image_id: "13596"
_4images_cat_id: "1248"
_4images_user_id: "729"
_4images_image_date: "2008-02-07T21:08:18"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13596 -->
Offne Tür von innen