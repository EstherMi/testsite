---
layout: "image"
title: "Teleskoptür08"
date: "2008-02-07T21:08:18"
picture: "teleskoptuernachlinksoeffnend8.jpg"
weight: "8"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/13595
imported:
- "2019"
_4images_image_id: "13595"
_4images_cat_id: "1248"
_4images_user_id: "729"
_4images_image_date: "2008-02-07T21:08:18"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13595 -->
Offne Tür