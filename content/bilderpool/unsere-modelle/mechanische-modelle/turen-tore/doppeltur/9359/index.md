---
layout: "image"
title: "Tür 7"
date: "2007-03-09T19:00:22"
picture: "doppeltuer07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/9359
imported:
- "2019"
_4images_image_id: "9359"
_4images_cat_id: "863"
_4images_user_id: "502"
_4images_image_date: "2007-03-09T19:00:22"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9359 -->
