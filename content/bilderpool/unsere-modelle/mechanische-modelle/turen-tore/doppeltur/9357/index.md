---
layout: "image"
title: "Tür 5"
date: "2007-03-09T19:00:21"
picture: "doppeltuer05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/9357
imported:
- "2019"
_4images_image_id: "9357"
_4images_cat_id: "863"
_4images_user_id: "502"
_4images_image_date: "2007-03-09T19:00:21"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9357 -->
Ganz offen.