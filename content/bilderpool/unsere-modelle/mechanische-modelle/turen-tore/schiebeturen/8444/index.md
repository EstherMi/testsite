---
layout: "image"
title: "Schiebetuer01.JPG"
date: "2007-01-14T12:50:53"
picture: "Schiebetuer01.jpg"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/8444
imported:
- "2019"
_4images_image_id: "8444"
_4images_cat_id: "1151"
_4images_user_id: "4"
_4images_image_date: "2007-01-14T12:50:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8444 -->
Das ist der erste Versuch, und taugt nicht viel. Die Führung ist zu wabbelig.