---
layout: "image"
title: "Schiebetuer02-zu.JPG"
date: "2007-01-14T12:52:15"
picture: "Schiebetuer02-zu.jpg"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/8445
imported:
- "2019"
_4images_image_id: "8445"
_4images_cat_id: "1151"
_4images_user_id: "4"
_4images_image_date: "2007-01-14T12:52:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8445 -->
Der U-Träger-Adapter liefert eine wunderbar saubere Führung. Auf die Achsen sind Lagerhülsen 36819 aufgeschoben.