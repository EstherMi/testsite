---
layout: "image"
title: "Schiebetuer03-zu.JPG"
date: "2007-01-14T12:53:50"
picture: "Schiebetuer03-zu.jpg"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/8447
imported:
- "2019"
_4images_image_id: "8447"
_4images_cat_id: "1151"
_4images_user_id: "4"
_4images_image_date: "2007-01-14T12:53:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8447 -->
Die "Führungsplatte E-Magnet" 32455 ist wieder mit von der Partie.