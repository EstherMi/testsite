---
layout: "image"
title: "Schiebetuer04-auf.JPG"
date: "2007-01-14T12:56:49"
picture: "Schiebetuer04-auf.jpg"
weight: "12"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/8450
imported:
- "2019"
_4images_image_id: "8450"
_4images_cat_id: "1151"
_4images_user_id: "4"
_4images_image_date: "2007-01-14T12:56:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8450 -->
