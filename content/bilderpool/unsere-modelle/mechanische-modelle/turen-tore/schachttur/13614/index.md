---
layout: "image"
title: "Schachttür06"
date: "2008-02-09T13:45:47"
picture: "schachttuer6.jpg"
weight: "6"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/13614
imported:
- "2019"
_4images_image_id: "13614"
_4images_cat_id: "1251"
_4images_user_id: "729"
_4images_image_date: "2008-02-09T13:45:47"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13614 -->
Detail der Mechanik