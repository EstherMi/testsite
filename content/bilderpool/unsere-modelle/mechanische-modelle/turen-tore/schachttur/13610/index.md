---
layout: "image"
title: "Schachttür02"
date: "2008-02-09T13:45:47"
picture: "schachttuer2.jpg"
weight: "2"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/13610
imported:
- "2019"
_4images_image_id: "13610"
_4images_cat_id: "1251"
_4images_user_id: "729"
_4images_image_date: "2008-02-09T13:45:47"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13610 -->
Ansicht vom "Schacht"