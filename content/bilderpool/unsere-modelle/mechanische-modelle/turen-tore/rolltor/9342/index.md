---
layout: "image"
title: "Rolltor 1"
date: "2007-03-07T16:00:50"
picture: "rolltor1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/9342
imported:
- "2019"
_4images_image_id: "9342"
_4images_cat_id: "862"
_4images_user_id: "502"
_4images_image_date: "2007-03-07T16:00:50"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9342 -->
