---
layout: "image"
title: "Tür 4"
date: "2007-03-10T19:31:36"
picture: "zweifachdoppeltuer04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/9384
imported:
- "2019"
_4images_image_id: "9384"
_4images_cat_id: "866"
_4images_user_id: "502"
_4images_image_date: "2007-03-10T19:31:36"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9384 -->
