---
layout: "image"
title: "Tür 10"
date: "2007-03-10T19:31:36"
picture: "zweifachdoppeltuer10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/9390
imported:
- "2019"
_4images_image_id: "9390"
_4images_cat_id: "866"
_4images_user_id: "502"
_4images_image_date: "2007-03-10T19:31:36"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9390 -->
