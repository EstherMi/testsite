---
layout: "image"
title: "Tor"
date: "2007-08-09T22:02:25"
picture: "lamellentor6.jpg"
weight: "6"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/11329
imported:
- "2019"
_4images_image_id: "11329"
_4images_cat_id: "1017"
_4images_user_id: "504"
_4images_image_date: "2007-08-09T22:02:25"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11329 -->
Das mit den Rollenböcken hat (glaub ich) Harald irgendwann mal reingestellt. Ziemlich gut Idee.