---
layout: "image"
title: "Von innen"
date: "2007-08-09T22:02:25"
picture: "lamellentor7.jpg"
weight: "7"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/11330
imported:
- "2019"
_4images_image_id: "11330"
_4images_cat_id: "1017"
_4images_user_id: "504"
_4images_image_date: "2007-08-09T22:02:25"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11330 -->
Hier ist die Garage mal von innen zu sehen. Wobei diese Garage nicht zu verwechseln ist mit der Garage aus den FANCLUB NEWS Ausgabe 01/07 (eigentlich ein computergesteuertes Terminal zum Beladen von LKWs...).