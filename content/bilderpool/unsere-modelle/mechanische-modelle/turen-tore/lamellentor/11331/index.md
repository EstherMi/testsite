---
layout: "image"
title: "Taster (außen)"
date: "2007-08-09T22:02:25"
picture: "lamellentor8.jpg"
weight: "8"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/11331
imported:
- "2019"
_4images_image_id: "11331"
_4images_cat_id: "1017"
_4images_user_id: "504"
_4images_image_date: "2007-08-09T22:02:25"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11331 -->
Das ist der Taster mit dem man das Tor von außen öffnen kann.