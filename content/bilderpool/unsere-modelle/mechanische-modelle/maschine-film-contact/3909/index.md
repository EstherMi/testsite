---
layout: "image"
title: "Antrieb des äußeren Rings"
date: "2005-03-28T23:50:43"
picture: "Contact-Maschine_012.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/3909
imported:
- "2019"
_4images_image_id: "3909"
_4images_cat_id: "339"
_4images_user_id: "104"
_4images_image_date: "2005-03-28T23:50:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3909 -->
Wie gesagt: Eine Stahlachse wird durch die beiden fest am Turm angebauten Drehscheiben fix gehalten. Das rote Z40 sitzt lose auf der Achse (mittels "Freilaufnaben" sogar, nicht nur durch nur leicht zugedrehte Flachnabe) und wird vom Z20 angetrieben. Dadurch wird auch die rechte Drehscheibe und dadurch der ganze äußere Ring gedreht. Das schwarze Z40 sitzt fest auf der festen Achse, dreht sich also nicht, sondern bleibt stabil stehen. Erst auf diesem rollt das mit dem äußeren Ring verbundene Z10 ab.