---
layout: "image"
title: "Übergang auf den mittleren Ring"
date: "2005-03-28T23:31:27"
picture: "Contact-Maschine_006.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/3903
imported:
- "2019"
_4images_image_id: "3903"
_4images_cat_id: "339"
_4images_user_id: "104"
_4images_image_date: "2005-03-28T23:31:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3903 -->
... Das zweite Z10 auf dieser Achse treibt wieder ein Z40 an, welches wiederum lose auf einer relativ zum äußeren Ring festgehaltenen Achse sitzt und den mittleren Ring dreht. Dadurch bleibt auch das von oben kommende Rast-Winkelzahnrad stehen und sein Gegenstück rollt wiederum auf diesem ab. Dadurch wird die Kette im Hintergrund angetrieben, um 60 Grad weiter über ein paar Winkelgetriebe den inneren Ring antreibt. Das ganze Modell kommt mit einem einzigen, außen montierten Power-Motor aus.