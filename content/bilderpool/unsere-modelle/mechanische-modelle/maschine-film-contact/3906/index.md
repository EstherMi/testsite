---
layout: "image"
title: "Gesamtansicht linke Seite"
date: "2005-03-28T23:37:36"
picture: "Contact-Maschine_009.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/3906
imported:
- "2019"
_4images_image_id: "3906"
_4images_cat_id: "339"
_4images_user_id: "104"
_4images_image_date: "2005-03-28T23:37:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3906 -->
Der PowerMotor treib ein Differential an. Ansonsten ist der Antrieb symmetrisch auf beiden Seiten eingerichtet. Die Z15 am oberen Ende der Kettentriebe sind selbstklemmende. Dadurch kann man den Motor ruhig hart einschalten, ohne dass sich das Material quält - das Z15 dreht einfach einen Moment (kaum merkbar) ein bisschen durch. Außerdem sieht man links unten am äußeren Ring, wie die Fixierung der Achse zum mittleren Ring realisiert ist: Der Mittlere Achsadapter trägt diese Achse, die äußeren beiden je eine Rastachse 30, die keine weitere Funktion als "Festhalten" haben.