---
layout: "comment"
hidden: true
title: "7691"
date: "2008-11-03T16:51:30"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Würden den normale (halblange) Klemmringe nicht auch hinreichend gegen Abspringen der Radialräder genügen?

Gruß,
Stefan