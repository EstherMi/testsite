---
layout: "image"
title: "Futter01.JPG"
date: "2004-11-30T19:55:42"
picture: "Futter01.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/3360
imported:
- "2019"
_4images_image_id: "3360"
_4images_cat_id: "299"
_4images_user_id: "4"
_4images_image_date: "2004-11-30T19:55:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3360 -->
Der Lothar sucht nach einem Bohr/Spannfutter für Muttern M13. Das hier ist mal ein Anfang; als wichtigstes Teil fehlt noch der Antriebsmotor für die Klemmvorrichtung.