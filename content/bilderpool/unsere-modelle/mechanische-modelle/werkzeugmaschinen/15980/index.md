---
layout: "image"
title: "Bohr- und Fräsmaschine BF1 (14/15)"
date: "2008-10-14T09:00:14"
picture: "bohrundfraesmaschinebf14.jpg"
weight: "14"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/15980
imported:
- "2019"
_4images_image_id: "15980"
_4images_cat_id: "1451"
_4images_user_id: "723"
_4images_image_date: "2008-10-14T09:00:14"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15980 -->
Der Aufsatzdrehtisch mit einem Spannteller für rotationssymmetrische (runde) Werkstücke. Mit einem Motorantrieb des Aufsatzdrehtisches wird das Modell mit Einschränkungen zu einer Karusseldrehfräse.