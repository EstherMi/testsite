---
layout: "image"
title: "08/53"
date: "2008-11-06T21:41:21"
picture: "drehmaschinevonclaus03.jpg"
weight: "3"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig, Fotonachbearbeitung Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/16176
imported:
- "2019"
_4images_image_id: "16176"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:21"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16176 -->
Werkzeugschlitten (Support) in der Linearachse Z auf dem Bett verfahrbar
mit dem in der rechtwinkligen Linearachse X verfahrbaren Planschlitten und dem
um die Drehachse B parallel zur senkrechten Linearachse Y drehbaren Oberschlitten.