---
layout: "image"
title: "06/53"
date: "2008-11-06T21:41:20"
picture: "drehmaschinevonclaus01.jpg"
weight: "1"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig, Fotonachbearbeitung Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/16174
imported:
- "2019"
_4images_image_id: "16174"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:20"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16174 -->
Gesamtansicht der zur Convention 2008 vorgestellten Drehmaschine.
Die Vorstellung ist eine Auswahl aus 53 Fotos.
Der Erklärungstext wird noch schrittweise ergänzt.