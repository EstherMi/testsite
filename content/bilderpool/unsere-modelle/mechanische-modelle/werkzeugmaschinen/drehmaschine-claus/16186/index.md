---
layout: "image"
title: "19/53"
date: "2008-11-06T21:41:21"
picture: "drehmaschinevonclaus13.jpg"
weight: "13"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig, Fotonachbearbeitung Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/16186
imported:
- "2019"
_4images_image_id: "16186"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:21"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16186 -->
Spindelgetriebe, Teilansicht von oben