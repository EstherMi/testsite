---
layout: "image"
title: "14/53"
date: "2008-11-06T21:41:21"
picture: "drehmaschinevonclaus08.jpg"
weight: "8"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig, Fotonachbearbeitung Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/16181
imported:
- "2019"
_4images_image_id: "16181"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:21"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16181 -->
Spindelstock- und Getriebegehäuse mit 3-Backen-Futter, Diagonalansicht von rechts hinten