---
layout: "image"
title: "49/53"
date: "2008-11-06T21:41:22"
picture: "drehmaschinevonclaus39.jpg"
weight: "39"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/16212
imported:
- "2019"
_4images_image_id: "16212"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:22"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16212 -->
Spindelgetriebe, Teilansicht