---
layout: "image"
title: "44/53"
date: "2008-11-06T21:41:22"
picture: "drehmaschinevonclaus34.jpg"
weight: "34"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig, Fotonachbearbeitung Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/16207
imported:
- "2019"
_4images_image_id: "16207"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:22"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16207 -->
Oberschlitten, Bedienung von links:
- Rastritzel: Winkeleinstellung zur Bettführung, dient z.B. zum Kegel- oder Konusdrehen
- Rastkegelrad: Werkzeugspannung
- Rastkurbel: Schlittenzustellung von Hand