---
layout: "comment"
hidden: true
title: "7708"
date: "2008-11-06T21:56:48"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ein Traum von fischertechnik-Maschine. Dieses Modell ist ein so fantastisches Paradebeispiel für fischertechnik in Bestform, ich kann gar nicht aufhören, mich an den vielen Zahnrädern sattzusehen.

Gruß,
Stefan