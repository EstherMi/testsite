---
layout: "image"
title: "24/53"
date: "2008-11-06T21:41:22"
picture: "drehmaschinevonclaus18.jpg"
weight: "18"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig, Fotonachbearbeitung Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/16191
imported:
- "2019"
_4images_image_id: "16191"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:22"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16191 -->
3-Backen-Futter selbstzentrierend
Vorschubgetriebe
Von links die 2 Schneckenspindeln unterschiedlicher Geschwindigkeit für die Vorschübe Werkzeugschlitten längs und Planschlitten.