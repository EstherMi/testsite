---
layout: "image"
title: "09/53"
date: "2008-11-06T21:41:21"
picture: "drehmaschinevonclaus04.jpg"
weight: "4"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig, Fotonachbearbeitung Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/16177
imported:
- "2019"
_4images_image_id: "16177"
_4images_cat_id: "1462"
_4images_user_id: "723"
_4images_image_date: "2008-11-06T21:41:21"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16177 -->
Reitstock