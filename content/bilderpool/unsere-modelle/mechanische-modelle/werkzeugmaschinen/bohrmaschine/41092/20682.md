---
layout: "comment"
hidden: true
title: "20682"
date: "2015-05-29T09:05:34"
uploadBy:
- "uhen"
license: "unknown"
imported:
- "2019"
---
Also bei den Teilen, die für eine Bohrung relevant sind, wackelt nichts. Die schwarze Grundplatte habe ich mit einer darunter verschraubten Holzplatte gestärkt (1. Foto), da diese sonst nachgegeben hat. Lediglich die Aluminiumträger kann man mit Druck nach hinten oder vorne etwas bewegen. Deshalb ist es besonders wichtig schön langsam in das Material reinzubohren. Dann bleibt die Bohrung auch genau.