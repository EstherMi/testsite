---
layout: "image"
title: "Bohrmaschine (Beispiel Trennscheibe)"
date: "2015-05-28T10:04:20"
picture: "bohrmaschine7.jpg"
weight: "7"
konstrukteure: 
- "uhen"
fotografen:
- "uhen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- details/41092
imported:
- "2019"
_4images_image_id: "41092"
_4images_cat_id: "3081"
_4images_user_id: "1112"
_4images_image_date: "2015-05-28T10:04:20"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41092 -->
Hier sieht man die Kürzung der Stange eines Potentiometers. Durch die Einstellung der langsamem Geschwindigkeit der Bewegung des Koordinatentisches erfolgt die Abtrennung sehr genau.