---
layout: "image"
title: "Bohrmaschine (Antrieb Koordinatentisch vor/zurück)"
date: "2015-05-28T10:04:20"
picture: "bohrmaschine3.jpg"
weight: "3"
konstrukteure: 
- "uhen"
fotografen:
- "uhen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- details/41088
imported:
- "2019"
_4images_image_id: "41088"
_4images_cat_id: "3081"
_4images_user_id: "1112"
_4images_image_date: "2015-05-28T10:04:20"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41088 -->
Mit diesem Motor wird der Tisch vor und zurück bewegt.