---
layout: "image"
title: "Bohrmaschine (Schiebeschalter)"
date: "2015-05-28T10:04:20"
picture: "bohrmaschine6.jpg"
weight: "6"
konstrukteure: 
- "uhen"
fotografen:
- "uhen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- details/41091
imported:
- "2019"
_4images_image_id: "41091"
_4images_cat_id: "3081"
_4images_user_id: "1112"
_4images_image_date: "2015-05-28T10:04:20"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41091 -->
Hier kann man zwischen zwei Geschwindigkeiten wählen. Die Spannung wird einfach halbiert durch Abgreifen der Spannung nach bereits drei Zellen. Die anderen beiden Taster sind für die Vorwärts/Rückwärts Bewegung des Tisches.