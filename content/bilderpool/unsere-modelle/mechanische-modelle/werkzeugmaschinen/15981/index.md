---
layout: "image"
title: "Bohr- und Fräsmaschine BF1 (15/15)"
date: "2008-10-14T09:00:14"
picture: "bohrundfraesmaschinebf15.jpg"
weight: "15"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/15981
imported:
- "2019"
_4images_image_id: "15981"
_4images_cat_id: "1451"
_4images_user_id: "723"
_4images_image_date: "2008-10-14T09:00:14"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15981 -->
Auf dem Arbeitstisch des Kreuzschlittens das "Sonderzubehör" Teilkopf mit Stützlager. Der Teilkopf dreht sich um die Drehachse A, die parallel zur Linearachse X liegt. Mit einem Direktantrieb der Teilspindel wird das Modell mit Einschränkungen zu einer Drehbankfräse.
17.10.2008 Nachtrag:
In der Praxis kann der Teilkopf auch mit senkrechter Achse (Drehachse C parallel zur Linearachse Z) aufgespannt werden.