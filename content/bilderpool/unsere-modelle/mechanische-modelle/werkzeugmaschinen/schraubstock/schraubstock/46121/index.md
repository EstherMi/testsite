---
layout: "image"
title: "Schraubstock schräg"
date: "2017-08-16T20:50:57"
picture: "schraubstock1.jpg"
weight: "1"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/46121
imported:
- "2019"
_4images_image_id: "46121"
_4images_cat_id: "3426"
_4images_user_id: "2303"
_4images_image_date: "2017-08-16T20:50:57"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46121 -->
