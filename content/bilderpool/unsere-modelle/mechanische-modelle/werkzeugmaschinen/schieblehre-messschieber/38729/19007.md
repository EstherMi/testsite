---
layout: "comment"
hidden: true
title: "19007"
date: "2014-05-02T00:20:34"
uploadBy:
- "Thorste_n"
license: "unknown"
imported:
- "2019"
---
Hallo Dirk,

das Ende: die Halter passen.
Nachdem ich nur ein paar BS30 holen wollte, fiel mir die Laufschiene + die Rollen in die Hände und die Version war eine fest einstellbare Lehre mit der ich die Außenmaße im Hellen!!! nehmen konnte, alles andere kam im Dunkeln dazu.

Bei kleineren Kindern verrutscht manchmal durch zufiel Messkraft der Schlitten &#8211; ein Kippfehler 1. Ordnung. http://www.mw-import.de/werkzeug/messschieber-kippfehler.html

Mit Metallachsen statt Laufschiene + Anbauwinkel 32615 und einer besseren Skala sollte auch eine stabilere Version < 1 mm möglich sein.
Uns reicht aber diese Version.

Gruß
Thorsten

PS: Für Kinder zum Messen und Erkunden von Gegenständen zum Nachbau empfohlen &#8211; macht denen echt Spaß.