---
layout: "overview"
title: "Schieblehre  Messschieber"
date: 2019-12-17T19:22:53+01:00
legacy_id:
- categories/2892
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2892 --> 
Mit Innenmaß, Außenmaß und Tiefenmaß + Feststellschraube.
Ohne Nonius da nur im mm Bereich genau.