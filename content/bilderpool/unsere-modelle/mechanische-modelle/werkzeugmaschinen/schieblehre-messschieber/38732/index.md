---
layout: "image"
title: "Rückseite - Schieblehre  / Messschieber"
date: "2014-04-30T12:23:43"
picture: "schieblehremessschieber4.jpg"
weight: "4"
konstrukteure: 
- "Thorste_n"
fotografen:
- "Thorste_n"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thorste_n"
license: "unknown"
legacy_id:
- details/38732
imported:
- "2019"
_4images_image_id: "38732"
_4images_cat_id: "2892"
_4images_user_id: "2138"
_4images_image_date: "2014-04-30T12:23:43"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38732 -->
Rückseite: Verstärkung duch Strebe 120 + Anschlag durch Riegel.

Für FT Puristen die Skala in Zahnstangenunterteilung (ca. 19 a 16 mm?)
(Modding d.h. cm Skala siehe vorher) 
