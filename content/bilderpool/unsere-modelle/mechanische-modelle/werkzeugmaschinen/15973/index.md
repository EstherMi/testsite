---
layout: "image"
title: "Bohr- und Fräsmaschine BF1 (8/15)"
date: "2008-10-14T08:59:54"
picture: "bohrundfraesmaschinebf07.jpg"
weight: "7"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/15973
imported:
- "2019"
_4images_image_id: "15973"
_4images_cat_id: "1451"
_4images_user_id: "723"
_4images_image_date: "2008-10-14T08:59:54"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15973 -->
Hier im Vordergrund die querliegende Reversierwelle, die mit dem Handhebel am rechten Bildrand axial verstellt werden kann. Die rechte axiale Stellung der Welle ermöglicht den Handvorschub und die linke die Fräszustellung. Beim Handvorschub ist so das linke Rastkegelradpaar entkuppelt und bei der Fräszustellung eingekuppelt. Auf der Reversierwelle sind noch zwei Rastritzel angeordnet, die dann wechselseitig in die Zahnstangen der Pinole eingreifen. Für die ordnungsgemäße Funktion der beiden Bedienfunktionen sind die zwei Gelenkwürfel-Zungen zweidimensional einstellbar.