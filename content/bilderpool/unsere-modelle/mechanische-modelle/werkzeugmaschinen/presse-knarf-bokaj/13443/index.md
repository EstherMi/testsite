---
layout: "image"
title: "Presse03"
date: "2008-01-27T17:30:27"
picture: "presse3.jpg"
weight: "3"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/13443
imported:
- "2019"
_4images_image_id: "13443"
_4images_cat_id: "1222"
_4images_user_id: "729"
_4images_image_date: "2008-01-27T17:30:27"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13443 -->
Presse von der Seite