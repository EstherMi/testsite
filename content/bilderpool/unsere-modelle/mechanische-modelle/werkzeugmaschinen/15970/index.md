---
layout: "image"
title: "Bohr- und Fräsmaschine BF1 (4/15)"
date: "2008-10-14T08:59:54"
picture: "bohrundfraesmaschinebf04.jpg"
weight: "4"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/15970
imported:
- "2019"
_4images_image_id: "15970"
_4images_cat_id: "1451"
_4images_user_id: "723"
_4images_image_date: "2008-10-14T08:59:54"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15970 -->
Bei dem Spindelkopfgewicht besteht mit Zapfenverbindungen aus Nylon an der Säule keine Chance diese ohne Krümmung hinzubekommen. Es sei denn, man wählt einen Säulenquerschnitt, der mit den modellinternen Proportionen nichts mehr zu tun hat. So habe ich 2 Alu-Profile hinten angesetzt und zusätzlich den Drehkranz des Spindelkopfes mit einer Einlage versehen, damit der Spindelkopf nicht nach vorn ankippt.