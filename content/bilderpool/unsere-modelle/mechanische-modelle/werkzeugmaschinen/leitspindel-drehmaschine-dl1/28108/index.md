---
layout: "image"
title: "[8/11] Vierstahlhalter"
date: "2010-09-13T14:37:25"
picture: "leitspindeldrehmaschinedl08.jpg"
weight: "8"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/28108
imported:
- "2019"
_4images_image_id: "28108"
_4images_cat_id: "2041"
_4images_user_id: "723"
_4images_image_date: "2010-09-13T14:37:25"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28108 -->
Wird die Spannspindel nach links gelöst, kann der Halterblock um ihre Achse beidseitig um 360° geschwenkt werden. Von einer 90°-Rasterung habe ich abgesehen, die dann doch in ihrer Grösse "fipsig" werden würde. Entsprechend dem Vorbild sind die Spannschrauben Inbusschrauben, hier M3x16.
Der Werkzeughalter ist ausgelegt für 4mm hohe Drehmeissel, wofür ich mir ein 8-fach-Formset HSS-Drehmeissel der Abmessung 4x4x63 gegönnt habe. Diese Werkzeuge sind einsatzfertig allseitig ! feingeschliffen und müssen nur noch am Schaftende etwas gekürzt werden, damit hier auch 4 Drehmeissel im Halter gleichzeitig angeordnet werden können.
Natürlich muss hier der Halter noch 3,75mm :o) nach vorn versetzt werden, damit seine Aussenkante mit der nachträglich eingehaussten Stirnseite des Oberschlittens abschliesst.