---
layout: "image"
title: "[2/11] Spindelstockgetriebe"
date: "2010-09-13T14:37:25"
picture: "leitspindeldrehmaschinedl02.jpg"
weight: "2"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/28102
imported:
- "2019"
_4images_image_id: "28102"
_4images_cat_id: "2041"
_4images_user_id: "723"
_4images_image_date: "2010-09-13T14:37:25"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28102 -->
Das gemäss den Vorbildern einfach gehaltene Spindelstockgetriebe für Varioantrieb besteht aus nur 3 Achsen mit 2-stufiger (High 1:1,5 und Low 1:0,375) Zahnstangenschaltung und Schwenkhebelbedienung. Das Foto zeigt die Einstellung Leerlauf. Von der Vorgelegekette des längs auf der Rückseite neben dem Spindelstock angeordneten Power-Motors 8:1 ist auch ein Stück zu sehen.