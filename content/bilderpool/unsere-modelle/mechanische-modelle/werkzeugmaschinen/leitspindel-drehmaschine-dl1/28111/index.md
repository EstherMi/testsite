---
layout: "image"
title: "[11/11] Sichtschutz"
date: "2010-09-13T14:37:26"
picture: "leitspindeldrehmaschinedl11.jpg"
weight: "11"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/28111
imported:
- "2019"
_4images_image_id: "28111"
_4images_cat_id: "2041"
_4images_user_id: "723"
_4images_image_date: "2010-09-13T14:37:26"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28111 -->
Die neuen Kassetten-Deckel gewähren noch eine ungetrübte Sicht ...
Gut zu sehen sind die beiden Seitendrehmeissel, hier genutzt oben ein Rechter zum Langdrehen und rechts daneben ein Linker allerdings dann 90° geschwenkt zum Plandrehen. Beide sind zwischen Haupt- und Nebenschneide (Eckenwinkel kleiner 90°) ohne Eckenradius fast scharfkantig. Dennoch haben die Oberflächen des Werkstücks durch die Nebenschneide eine erfreulich glatte Oberfläche mit geringer Rauhtiefe. Die Zylindermäntel wurden mit dem mechanischen Längsvorschub gedreht.

Der Schwerpunkt der Modellentwicklung bis zum  hier gezeigten Zwischenstand war der gefundene Beweis, dass es mit den Stabilitäten und Drehmomenten eines ft-Modells möglich ist, ausgesuchte nichtmetallische Werkstoffe mit niedriger Spantiefe zu drehen. Hier sind im Gegensatz zu meinem Bohr- und Fräsmaschinenmodell BF1 die erforderlichen Drehmomente durch die möglichen Werkstückdurchmesser wesentlich größer. Das war auch der Grund, warum ich bei diesem Modell einer Drehmaschine DL1 mit Zerspanungsversuchen begonnen habe, weil die Durchmesser von Bohr- und Fräswerkzeugen im Vergleich sichtbar kleiner sind.

Viel ist noch in der Bautetappe 1 zu tun. Die Arbeitspindel z.B. läuft "fachlich" betreut immer noch stumm und ohne Wälzlager. Die Bearbeitungsversuche werden mir sicher noch Notwendigkeiten und Anregungen aufzeigen.
Mein unter dem Foto [1/11] geoutetes Modellfernziel halte ich jedenfalls fest im Visier.

Wenn meine Teilnahme an der Convention 2010 klappt, bringe ich das Modell mit. Gedreht wird dort aber nicht, die Halle muss sauber bleiben !
Da ich sicher wenig am Platz sein werde, lege ich speziell für interessierte Besucher ein Infoblatt aus :o)

Fazit:
Diese DL1 mit ihren Funktionen war im Gegensatz zu meiner BF1 in der Näherung zu einer noch realistischeren Reflektion der Vorbilder nur mit dem ft-Standard sowie mit ft-Umgehungslösungen nicht mehr zu bewältigen. Mehrere ausgesuchte Teileverbindungen mit baukastenvergleichbarer Belastung hielten nur kurze Zeit bevor sie sich lockerten und verschoben. Das ist allerdings bei einem ft-Modell ein permanenter Vorgang, bis man es wieder rückbaut :o) Statik und Mechanik sind dadurch auch nur niedrig belastbar. Für die Wechselräder und die Schlossmutter waren teilweise deutliche Modifizierungen an Originalteilen erforderlich. Für die Lagerhülsen der Freilaufwechselräderpaare wurde Fremdmaterial benötigt.