---
layout: "image"
title: "[7/11] Reitstockverstellung X plus"
date: "2010-09-13T14:37:25"
picture: "leitspindeldrehmaschinedl07.jpg"
weight: "7"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/28107
imported:
- "2019"
_4images_image_id: "28107"
_4images_cat_id: "2041"
_4images_user_id: "723"
_4images_image_date: "2010-09-13T14:37:25"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28107 -->
... hier in der Stellung  X +5 mm.

Dieser Reitstock ist in jeder Längsstellung und innerhalb des Bereichs -5 ... 0 ... +5 mm quer zur Bettachse Z selbstsichernd verstellbar.
Wenn man so will ein neuartiger? 2D-Kreuzschlitten mit der Bauhöhe Null :o)  2D deshalb weil der Reitstock auch gleichzeitig in X und Z verstellbar ist.

Taugt das Modellkonzept zum Erreichen meiner Ziele, kann ich hier immer noch allerdings dann aufwendiger diese XZ-Funktionen traditionell konstruiert umsetzen.