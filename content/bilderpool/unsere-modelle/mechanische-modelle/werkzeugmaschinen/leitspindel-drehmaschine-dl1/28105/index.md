---
layout: "image"
title: "[5/11] Schlossmutter"
date: "2010-09-13T14:37:25"
picture: "leitspindeldrehmaschinedl05.jpg"
weight: "5"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/28105
imported:
- "2019"
_4images_image_id: "28105"
_4images_cat_id: "2041"
_4images_user_id: "723"
_4images_image_date: "2010-09-13T14:37:25"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28105 -->
In der Schlossplatte des Supports (Werkzeugträger mit Bett-, Plan- und schwenkbarem Oberschlitten) befindet sich neben der Baugruppe zum  manuellen Verschieben des Bettschlittens auch die Baugruppe Leitspindel-Schlossmutter vorbildgerecht für den Bettschlittenvorschub zum Lang- und Gewindedrehen.

Diese "echte" Schlossmutter fotografiert in der Stellung ausgekuppelt (offen) habe ich aus einer ft-Schneckenmutter m1,5 modifiziert, die dazu bei einer ft-Schneckenspindel nicht nur geteilt werden muss. Links vorn der Schwenkebel zur Betätigung.

Grössere und umfangreicher ausgestattete Drehmaschinen der technischen Vorbilder haben noch zusätzlich eine Zugspindel (Mehrkantwelle mit Verschieberad), die unter der Leitspindel angeordnet ist. Von der Zugspindel wird dort der Bettschlittenvorschub längs und quer des Planschlittens in der Schlossplatte abgeleitet. Der Längsvorschub wälzt sich dabei an der Bettzahnstange ab. Die Leitspindel allerdings mit Trapezgewinde wird dann dort nur z.B. für das Gewindedrehen eingesetzt.