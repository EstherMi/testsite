---
layout: "comment"
hidden: true
title: "12162"
date: "2010-09-13T23:25:48"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Was für ein schmuckes Kleinod an Mechanik verbirgt sich denn hier und bewegt die beiden Halbschalen um die Schnecke? Da wäre ein Detailfoto noch nett. Auch wenn hier ein ft-Teil geteilt wurde, ist das ein tolles Detail Deiner Drehbank!

Gruß,
Stefan