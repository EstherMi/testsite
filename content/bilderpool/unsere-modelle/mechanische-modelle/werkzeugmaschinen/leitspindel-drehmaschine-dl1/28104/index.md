---
layout: "image"
title: "[4/11] Wechselrädergetriebe II"
date: "2010-09-13T14:37:25"
picture: "leitspindeldrehmaschinedl04.jpg"
weight: "4"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/28104
imported:
- "2019"
_4images_image_id: "28104"
_4images_cat_id: "2041"
_4images_user_id: "723"
_4images_image_date: "2010-09-13T14:37:25"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28104 -->
Hier kann man den Wechselräderstab (und auch das Z15) erkennen, an dem die Freilaufradpaare gelagert sind. Der Stab ist unten über einen BS15mB um die Leitspindelachse schwenkbar und oben je nach Räderanordnung über eine gegebenenfalls austauschbare Strebe 30 mit Loch und zwei Strebenadapter stufenlos einstellbar. Die Ms-Rohrstücke mit den Freilaufradpaaren sind auf Rastachsen mit Platte aufgesteckt. Damit können diese Achsabstände für andere Räderkonfigurationen stufenlos geändert werden.