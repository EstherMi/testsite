---
layout: "image"
title: "Baumstammsäge"
date: "2013-06-29T20:52:03"
picture: "baumstammsaege2.jpg"
weight: "2"
konstrukteure: 
- "Fredy"
fotografen:
- "Fredy"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/37121
imported:
- "2019"
_4images_image_id: "37121"
_4images_cat_id: "2755"
_4images_user_id: "453"
_4images_image_date: "2013-06-29T20:52:03"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37121 -->
