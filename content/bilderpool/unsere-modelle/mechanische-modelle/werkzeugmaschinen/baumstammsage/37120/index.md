---
layout: "image"
title: "Baumstammsäge"
date: "2013-06-29T20:52:03"
picture: "baumstammsaege1.jpg"
weight: "1"
konstrukteure: 
- "Fredy"
fotografen:
- "Fredy"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/37120
imported:
- "2019"
_4images_image_id: "37120"
_4images_cat_id: "2755"
_4images_user_id: "453"
_4images_image_date: "2013-06-29T20:52:03"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37120 -->
http://youtu.be/aNmoj_HOD98