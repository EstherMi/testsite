---
layout: "image"
title: "Plattform mit Tuschestift"
date: "2004-05-31T19:50:19"
picture: "H2-07-Plattform_mit_Tuschestift.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/2441
imported:
- "2019"
_4images_image_id: "2441"
_4images_cat_id: "181"
_4images_user_id: "46"
_4images_image_date: "2004-05-31T19:50:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2441 -->
Als Werkzeug ist der Plattform ein Tuschestift eingesetzt. Für die Fotos habe ich das Modell abgekabelt, deshalb ist die Sache hier mehr symbolisch. Das Plotten in beliebigen Ebenen ist aber tatsächlich nicht allzu schwierig.