---
layout: "image"
title: "Rahmen und Plattform"
date: "2004-05-31T19:50:19"
picture: "H2-03-Rahmen_und_Plattform.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/2444
imported:
- "2019"
_4images_image_id: "2444"
_4images_cat_id: "181"
_4images_user_id: "46"
_4images_image_date: "2004-05-31T19:50:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2444 -->
Sicht auf einen Teil des Rahmens mit eingehängter Tandemseilwinde