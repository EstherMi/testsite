---
layout: "image"
title: "Plattform über Drehteller"
date: "2004-05-31T19:50:19"
picture: "H2-06-Plattform_ber_Drehteller.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/2442
imported:
- "2019"
_4images_image_id: "2442"
_4images_cat_id: "181"
_4images_user_id: "46"
_4images_image_date: "2004-05-31T19:50:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2442 -->
Als siebte Achse habe ich noch einen großen Drehteller hinzugefügt. Die Plattform kann nur eingeschränkt um die Hochachse rotieren.