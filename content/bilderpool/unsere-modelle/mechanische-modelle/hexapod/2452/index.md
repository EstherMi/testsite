---
layout: "image"
title: "Tandemseilwinde1"
date: "2004-05-31T19:50:20"
picture: "H2-09-Tandemseilwinde.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/2452
imported:
- "2019"
_4images_image_id: "2452"
_4images_cat_id: "181"
_4images_user_id: "46"
_4images_image_date: "2004-05-31T19:50:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2452 -->
Hier der Blick auf eine der drei Tandemseilwinden, die jetzt in den oberen Rahmenteil gewandert sind.