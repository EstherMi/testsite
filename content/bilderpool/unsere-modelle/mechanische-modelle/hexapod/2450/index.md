---
layout: "image"
title: "Steuerelektronik"
date: "2004-05-31T19:50:20"
picture: "H2-15-Steuerelektronik.jpg"
weight: "14"
konstrukteure: 
- "irgendwer"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/2450
imported:
- "2019"
_4images_image_id: "2450"
_4images_cat_id: "181"
_4images_user_id: "46"
_4images_image_date: "2004-05-31T19:50:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2450 -->
Was auf den ersten Blick wie ein Kabelverhau aussieht, funktioniert in Wirklichkeit. Rechts sind die 6 Schrittmotortreiber zu erkennen, in der Mitte sind drei Treiber für die Signale Enable, Schrittauflösung Bit 1 und Schrittauflösung Bit 2. Danach kommen drei Tasten, mit denen die o. a. Signale manuell eingegeben werden.