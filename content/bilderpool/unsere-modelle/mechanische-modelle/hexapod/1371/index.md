---
layout: "image"
title: "Hexapod5"
date: "2003-09-07T14:28:44"
picture: "Hexapod5-Oberer_Knoten.jpg"
weight: "24"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/1371
imported:
- "2019"
_4images_image_id: "1371"
_4images_cat_id: "181"
_4images_user_id: "46"
_4images_image_date: "2003-09-07T14:28:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1371 -->
Oberer Knoten, der die gewinkelten, 2-fach Rollenblöcke trägt