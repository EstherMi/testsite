---
layout: "image"
title: "Oberer Knoten"
date: "2004-05-31T19:50:19"
picture: "H2-04-Oberer_Knoten.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/2440
imported:
- "2019"
_4images_image_id: "2440"
_4images_cat_id: "181"
_4images_user_id: "46"
_4images_image_date: "2004-05-31T19:50:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2440 -->
Seitenansicht einer der drei Knoten des oberen Rahmens