---
layout: "image"
title: "Unterer Knoten"
date: "2004-05-31T19:50:20"
picture: "H2-05-Unterer_Knoten.jpg"
weight: "18"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/2454
imported:
- "2019"
_4images_image_id: "2454"
_4images_cat_id: "181"
_4images_user_id: "46"
_4images_image_date: "2004-05-31T19:50:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2454 -->
Die Aufstellfüße sind umkonstruiert und jetzt viel kleiner geworden.