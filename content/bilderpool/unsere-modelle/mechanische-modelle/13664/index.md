---
layout: "image"
title: "Erdnussknack-Automat"
date: "2008-02-17T14:16:08"
picture: "DSC03938.jpg"
weight: "7"
konstrukteure: 
- "Raphael"
fotografen:
- "Raphael"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "baustein"
license: "unknown"
legacy_id:
- details/13664
imported:
- "2019"
_4images_image_id: "13664"
_4images_cat_id: "127"
_4images_user_id: "727"
_4images_image_date: "2008-02-17T14:16:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13664 -->
Erdnussknack-Automat