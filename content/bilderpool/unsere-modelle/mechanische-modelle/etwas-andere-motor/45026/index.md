---
layout: "image"
title: "Der Propeller um die Ecke"
date: "2017-01-13T13:00:50"
picture: "Propeller.jpg"
weight: "1"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- details/45026
imported:
- "2019"
_4images_image_id: "45026"
_4images_cat_id: "3352"
_4images_user_id: "2635"
_4images_image_date: "2017-01-13T13:00:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45026 -->
Den Propeller treibt er &#8222;um die Ecke&#8220;. Zum Start muss man ihn manchmal anwerfen, wegen des Trägheitsmomentes.