---
layout: "image"
title: "Der 'etwas andere Motor' mit Knick"
date: "2017-01-13T13:00:50"
picture: "Der_etwas_andere_Motor_mit_Knickl.jpg"
weight: "4"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- details/45029
imported:
- "2019"
_4images_image_id: "45029"
_4images_cat_id: "3352"
_4images_user_id: "2635"
_4images_image_date: "2017-01-13T13:00:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45029 -->
Der Motor funktioniert wie in ft:pedia 3-2016 beschrieben. Er besitzt (von links nach rechts)  einen Stabmagneten 4x10 mm, dann eine Achse 30 und mit der Klemmbuchse 10 ist ein diametral magnetisierter Stabmagnet 4x10 mm befestigt. Die zweite Welle besitzt ebenfalls einen diametral magnetisierten Stabmagneten und eine Achse 30. Mit 6 V Wechselstrom betrieben läuft der Motor mit ca. 14 Hz (&#8793; 840 U/min) und die Welle hinter dem Knick läuft synchron mit bis zu den maximalen Knickwinkeln +- 90°.