---
layout: "image"
title: "zwei"
date: "2006-03-26T14:53:33"
picture: "fischertechnik_030.jpg"
weight: "16"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/5938
imported:
- "2019"
_4images_image_id: "5938"
_4images_cat_id: "1593"
_4images_user_id: "371"
_4images_image_date: "2006-03-26T14:53:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5938 -->
