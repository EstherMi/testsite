---
layout: "image"
title: "Förderband"
date: "2009-01-06T17:20:57"
picture: "foerderband03.jpg"
weight: "15"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/16906
imported:
- "2019"
_4images_image_id: "16906"
_4images_cat_id: "1522"
_4images_user_id: "1"
_4images_image_date: "2009-01-06T17:20:57"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16906 -->
