---
layout: "image"
title: "Förderband"
date: "2009-01-06T17:20:58"
picture: "foerderband13.jpg"
weight: "25"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/16916
imported:
- "2019"
_4images_image_id: "16916"
_4images_cat_id: "1522"
_4images_user_id: "1"
_4images_image_date: "2009-01-06T17:20:58"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16916 -->
