---
layout: "image"
title: "Förderband"
date: "2009-01-07T15:12:39"
picture: "foerderband06_2.jpg"
weight: "6"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/16935
imported:
- "2019"
_4images_image_id: "16935"
_4images_cat_id: "1522"
_4images_user_id: "1"
_4images_image_date: "2009-01-07T15:12:39"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16935 -->
