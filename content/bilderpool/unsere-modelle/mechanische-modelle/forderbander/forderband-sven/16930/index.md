---
layout: "image"
title: "Förderband"
date: "2009-01-07T15:12:38"
picture: "foerderband01_2.jpg"
weight: "1"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/16930
imported:
- "2019"
_4images_image_id: "16930"
_4images_cat_id: "1522"
_4images_user_id: "1"
_4images_image_date: "2009-01-07T15:12:38"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16930 -->
Ich habe das ganze noch weiter verbessert.
Hier sieht man die Spitzen zum zentrieren des Fördergutes.