---
layout: "image"
title: "FB12_01.JPG"
date: "2006-03-12T16:06:12"
picture: "FB12_01.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Rolltor"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5879
imported:
- "2019"
_4images_image_id: "5879"
_4images_cat_id: "1591"
_4images_user_id: "4"
_4images_image_date: "2006-03-12T16:06:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5879 -->
Naja, als Förderband ist das vielleicht nicht so der Bringer, denn die Platten kommen unter Last schnell ins Kippeln.

Aber als Rolltor für eine Garage geht es ganz bestimmt.