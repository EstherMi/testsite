---
layout: "image"
title: "FoeBa2_01.JPG"
date: "2004-11-15T17:29:03"
picture: "FoeBa2_01.jpg"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/3152
imported:
- "2019"
_4images_image_id: "3152"
_4images_cat_id: "1591"
_4images_user_id: "4"
_4images_image_date: "2004-11-15T17:29:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3152 -->
