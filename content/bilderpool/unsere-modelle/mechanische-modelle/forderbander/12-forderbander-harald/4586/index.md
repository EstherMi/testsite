---
layout: "image"
title: "FB10_01.JPG"
date: "2005-08-12T14:48:44"
picture: "FB10_01.jpg"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4586
imported:
- "2019"
_4images_image_id: "4586"
_4images_cat_id: "1591"
_4images_user_id: "4"
_4images_image_date: "2005-08-12T14:48:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4586 -->
Ohne die Noppen käme dieses Förderband auch auf eine Höhe von ganzen 15 mm.