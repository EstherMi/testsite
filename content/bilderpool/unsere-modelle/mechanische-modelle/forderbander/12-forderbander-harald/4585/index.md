---
layout: "image"
title: "FB09_03.JPG"
date: "2005-08-12T14:48:44"
picture: "FB09_03.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Förderband", "conveyor", "belt"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4585
imported:
- "2019"
_4images_image_id: "4585"
_4images_cat_id: "1591"
_4images_user_id: "4"
_4images_image_date: "2005-08-12T14:48:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4585 -->
Die ganze Mechanik passt durch einen 15mm hohen Schlitz ( = 1 ft-Bausteinraster) hindurch. Ich weiß zwar nicht, wozu man das verwenden kann, aber da findet sich sicher noch eine Anwendung.