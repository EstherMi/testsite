---
layout: "image"
title: "FoeBa4_02.JPG"
date: "2004-11-15T17:29:03"
picture: "FoeBa4_02.jpg"
weight: "20"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Förderband"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/3159
imported:
- "2019"
_4images_image_id: "3159"
_4images_cat_id: "1591"
_4images_user_id: "4"
_4images_image_date: "2004-11-15T17:29:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3159 -->
Das hier ist der minimalistische Ansatz. Mit WENIGER Teilen wird es kaum gehen.

Den schwarzen Statikteilen wurden die Zapfen abgeschnitten.