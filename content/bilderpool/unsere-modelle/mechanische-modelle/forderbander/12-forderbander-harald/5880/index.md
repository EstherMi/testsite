---
layout: "image"
title: "FB12_02.JPG"
date: "2006-03-12T16:08:38"
picture: "FB12_02.jpg"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5880
imported:
- "2019"
_4images_image_id: "5880"
_4images_cat_id: "1591"
_4images_user_id: "4"
_4images_image_date: "2006-03-12T16:08:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5880 -->
Anstelle der "alten" Kettenglieder kann man natürlich auch nehmen:
- Baustein 15x30x5, 35049
- Bauplatte 15x30x3, 32330
- BS30 (na ja...)