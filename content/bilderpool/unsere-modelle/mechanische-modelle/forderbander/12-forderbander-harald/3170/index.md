---
layout: "image"
title: "FoeBa8_01.JPG"
date: "2004-11-15T17:29:22"
picture: "FoeBa8_01.jpg"
weight: "30"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Förderband"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/3170
imported:
- "2019"
_4images_image_id: "3170"
_4images_cat_id: "1591"
_4images_user_id: "4"
_4images_image_date: "2004-11-15T17:29:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3170 -->
