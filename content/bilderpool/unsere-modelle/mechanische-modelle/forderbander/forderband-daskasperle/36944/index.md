---
layout: "image"
title: "Förderband von DasKasperle - VORNE DETAILE 1"
date: "2013-05-26T09:50:17"
picture: "foerderbandvondaskasperle08.jpg"
weight: "8"
konstrukteure: 
- "DasKasperle alias Sushitechnik"
fotografen:
- "Kai"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- details/36944
imported:
- "2019"
_4images_image_id: "36944"
_4images_cat_id: "2746"
_4images_user_id: "1677"
_4images_image_date: "2013-05-26T09:50:17"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36944 -->
