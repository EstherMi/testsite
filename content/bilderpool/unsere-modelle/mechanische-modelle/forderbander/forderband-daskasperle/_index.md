---
layout: "overview"
title: "Förderband von DasKasperle"
date: 2019-12-17T19:16:19+01:00
legacy_id:
- categories/2746
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2746 --> 
Das Förderband ist für Kastaniene ausgelegt. Die Neigung des Förderbandes wird per Motor und IR Control eingestellt. Dank der alten Controler lässt sich auch die Geschindigkeit in zwei Stufen einstellen.
Mit dem Förderband beladen wir unseren Pneumatik-Kipplaster sowie unser ft-Ships & More Rhein-Frachter. Befüllt wird das Förderband von einem IR-Pneumatik Radlader mit Frontladeschaufel oder vom IR-Pneumatik Löffelbagger.

IR Pneumatik Radlader belädt ein Förderband mit Kastanien
LINK: http://www.youtube.com/watch?v=SF-P_u1B4_Q&feature=youtu.be