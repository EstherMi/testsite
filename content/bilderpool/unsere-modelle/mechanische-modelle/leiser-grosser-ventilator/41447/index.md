---
layout: "image"
title: "Schwenkantrieb (1)"
date: "2015-07-18T14:54:50"
picture: "leisergrosserventilator3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41447
imported:
- "2019"
_4images_image_id: "41447"
_4images_cat_id: "3098"
_4images_user_id: "104"
_4images_image_date: "2015-07-18T14:54:50"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41447 -->
Über ein normales Haushaltsgummi geht's auf zweifach gelagerte die Drehscheibe...