---
layout: "image"
title: "Gesamtansicht"
date: "2015-07-18T14:54:50"
picture: "leisergrosserventilator1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41445
imported:
- "2019"
_4images_image_id: "41445"
_4images_cat_id: "3098"
_4images_user_id: "104"
_4images_image_date: "2015-07-18T14:54:50"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41445 -->
Dieser Ventilator schwenkt hin und her, erzeugt mit 3 V ein gerade angenehmes sanftes Lüftchen oder auch mit 9 V ziemlich Wind. Aber vor allem: Er läuft so leise, dass man außer dem Windrauschen praktisch nichts hört.