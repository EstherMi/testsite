---
layout: "image"
title: "Vollgas Ventilator"
date: "2016-07-26T10:47:34"
picture: "ventilatorgross1.jpg"
weight: "1"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/44015
imported:
- "2019"
_4images_image_id: "44015"
_4images_cat_id: "3258"
_4images_user_id: "558"
_4images_image_date: "2016-07-26T10:47:34"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44015 -->
Unten rechts der Schiebeschalter. Betrieben wird das ganze bei 12V vom Netzteil das wegen des Jalousie Aufzugs eh immer Steckt. Die Motoren werden dabei knapp Handwarm.