---
layout: "image"
title: "automatische Ballzuführung für ft KBEM"
date: "2017-02-12T12:32:19"
picture: "IMG_2091.jpg"
weight: "1"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/45194
imported:
- "2019"
_4images_image_id: "45194"
_4images_cat_id: "3358"
_4images_user_id: "1359"
_4images_image_date: "2017-02-12T12:32:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45194 -->
