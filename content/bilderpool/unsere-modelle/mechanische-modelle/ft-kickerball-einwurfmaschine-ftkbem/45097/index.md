---
layout: "image"
title: "Pneumatischer Stössel"
date: "2017-01-29T14:06:53"
picture: "IMG_1080.jpg"
weight: "5"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/45097
imported:
- "2019"
_4images_image_id: "45097"
_4images_cat_id: "3358"
_4images_user_id: "1359"
_4images_image_date: "2017-01-29T14:06:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45097 -->
Zylinder, um den Ball ins Spiel zu werfen. wer keine blauen Zylinder der alten Form hat, kann natürlich auch aktuelle schwarze verwenden. das Ventil kann natürlich gegen ein aktuelles Modell oder wahlweise ein Magnetventil ersetzt werden.
Man den Ring gut erkennen, durch den der Ball mit dem Aufzug hochgefahren wird, um dann auf Höhe des Zylinders eingeworen zu werden.