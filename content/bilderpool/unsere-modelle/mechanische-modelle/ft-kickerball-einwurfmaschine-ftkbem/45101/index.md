---
layout: "image"
title: "Ball in Einwurfposition"
date: "2017-01-29T14:06:53"
picture: "IMG_1084.jpg"
weight: "9"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: ["ft", "Kickerball", "Einwurfmaschine"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/45101
imported:
- "2019"
_4images_image_id: "45101"
_4images_cat_id: "3358"
_4images_user_id: "1359"
_4images_image_date: "2017-01-29T14:06:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45101 -->
