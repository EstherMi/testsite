---
layout: "image"
title: "Aufgelegter Kickerball, in unterer Position"
date: "2017-01-29T14:06:53"
picture: "IMG_1077.jpg"
weight: "3"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: ["fischertechnik", "Kickerball", "Einwurfmaschine", "(ftKBEM)"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/45095
imported:
- "2019"
_4images_image_id: "45095"
_4images_cat_id: "3358"
_4images_user_id: "1359"
_4images_image_date: "2017-01-29T14:06:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45095 -->
