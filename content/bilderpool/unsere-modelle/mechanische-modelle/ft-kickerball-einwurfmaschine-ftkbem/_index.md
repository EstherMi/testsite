---
layout: "overview"
title: "Ft Kickerball Einwurfmaschine (ftKBEM)"
date: 2019-12-17T19:26:17+01:00
legacy_id:
- categories/3358
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3358 --> 
Die Maschine kann an einen Kickertisch eingehängt werden und wirft den Ball ein, um Spielmanipulation beim Einwurf zu verhindern. Eine Ablaufsteuerung und Ballzuführung befindet sich noch im Bau)