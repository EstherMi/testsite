---
layout: "image"
title: "Consul, The Educated Monkey - der 'Kopf'"
date: "2015-01-11T06:37:05"
picture: "consultheeducatedmonkey5.jpg"
weight: "5"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/40312
imported:
- "2019"
_4images_image_id: "40312"
_4images_cat_id: "3023"
_4images_user_id: "1126"
_4images_image_date: "2015-01-11T06:37:05"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40312 -->
Hier sieht man rechts die Unterseite des "Affenkopfes": Ich habe eine Schwungscheibe (39006) auf einer Metallachse 60 verwendet - alternativ tun es auch ein Rad 23 (36581), ein Walzenrad (35386) oder eine Drehscheibe 60 (31019). Darauf habe ich mit Klebeband das Bild eines Affenkopfes angebracht. Links daneben sieht man im Bild den "Ellenbogen": eine Gelenkklaue mit zwei Winkelsteinen 30°.