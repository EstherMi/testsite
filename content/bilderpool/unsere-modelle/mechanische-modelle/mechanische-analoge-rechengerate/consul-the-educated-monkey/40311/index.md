---
layout: "image"
title: "Consul, The Educated Monkey - das 'Kopfgelenk'"
date: "2015-01-11T06:37:05"
picture: "consultheeducatedmonkey4.jpg"
weight: "4"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/40311
imported:
- "2019"
_4images_image_id: "40311"
_4images_cat_id: "3023"
_4images_user_id: "1126"
_4images_image_date: "2015-01-11T06:37:05"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40311 -->
Das Kopfgelenk ist entscheidend für die Funktionsfähigkeit der Mechanik: Die beiden "Arme" und "Beine" des Affen werden paarweise fest miteinander verbunden (Bauplatte 15x30x5 mit Nuten, 38428) und beide Seiten über einen Winkelstein 60° und eine Gelenkklaue verknüpft. Die "Arme" stehen in einem Winkel von 30° (Winkelstein 30) seitlich ab.