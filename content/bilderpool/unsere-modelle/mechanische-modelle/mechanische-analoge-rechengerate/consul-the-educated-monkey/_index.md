---
layout: "overview"
title: "Consul, The Educated Monkey"
date: 2019-12-17T19:25:41+01:00
legacy_id:
- categories/3023
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3023 --> 
Im Jahr 1916 erschien in den USA die Multiplikationshilfe "Consul, The Educated Monkey". Ein "mechanischer Taschenrechner" - und eine tolle 1x1-Lernhilfe. 
Nachbauten aus Blech können heute noch für ca. 15 ? erworben werden - lassen sich aber auch elegant mit fischertechnik konstruieren.