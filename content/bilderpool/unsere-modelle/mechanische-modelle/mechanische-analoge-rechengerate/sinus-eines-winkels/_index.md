---
layout: "overview"
title: "Sinus eines Winkels"
date: 2019-12-17T19:25:39+01:00
legacy_id:
- categories/2948
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2948 --> 
Bestimmung des Sinus eines Drehwinkels.