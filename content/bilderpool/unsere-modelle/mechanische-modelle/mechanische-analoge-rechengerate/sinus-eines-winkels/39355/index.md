---
layout: "image"
title: "Sinus Variante 5 - horizonte Geradführung"
date: "2014-09-14T12:48:39"
picture: "Sinus_Variante_5_Bild_2_publish.jpg"
weight: "7"
konstrukteure: 
- "MickyW"
fotografen:
- "MickyW"
keywords: ["Sinus", "Geradführung", "Gleitführung"]
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- details/39355
imported:
- "2019"
_4images_image_id: "39355"
_4images_cat_id: "2948"
_4images_user_id: "1806"
_4images_image_date: "2014-09-14T12:48:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39355 -->
Da es immer wieder Blockaden auf der Geradführung gegeben hat, war die Überlegung, die Kontaktstellen zwischen den Steinen und den Achsen möglichst weit auseinander zu legen, in der Hoffnung, dass die Reibkraft geringer wird, weil der Winkel zwischen Stein und Achse in der Nut geringer wird. Ebenso wurde die "Einspannung" des Hebels verlängert.