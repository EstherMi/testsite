---
layout: "image"
title: "Sinus Variante 3 - Dreiviertel-Ansicht"
date: "2014-09-14T12:48:39"
picture: "Sinus_Variante_3_Bild_3_publish.jpg"
weight: "5"
konstrukteure: 
- "MickyW"
fotografen:
- "MickyW"
keywords: ["Sinus", "Gleitführung"]
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- details/39353
imported:
- "2019"
_4images_image_id: "39353"
_4images_cat_id: "2948"
_4images_user_id: "1806"
_4images_image_date: "2014-09-14T12:48:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39353 -->
Hier nochmal von der Seite. Die rotatorische Achse wurde länger gelagert um mehr Stabillität zu erreichen.