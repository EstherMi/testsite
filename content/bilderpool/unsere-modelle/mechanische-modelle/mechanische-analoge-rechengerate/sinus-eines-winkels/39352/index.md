---
layout: "image"
title: "Sinus Variante 3"
date: "2014-09-14T12:48:39"
picture: "Sinus_Variante_3_Bild_1_publish.jpg"
weight: "4"
konstrukteure: 
- "MickyW"
fotografen:
- "MickyW"
keywords: ["Sinus", "Gleitführung"]
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- details/39352
imported:
- "2019"
_4images_image_id: "39352"
_4images_cat_id: "2948"
_4images_user_id: "1806"
_4images_image_date: "2014-09-14T12:48:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39352 -->
Die nächste Variante sollte die horizontale Führung durch eine vertikale ersetzen, da ich vermutete, das der Lauf dann etwas leichter wäre. Durch den Lochstein sollte mehr Hebelwirkung da sein.