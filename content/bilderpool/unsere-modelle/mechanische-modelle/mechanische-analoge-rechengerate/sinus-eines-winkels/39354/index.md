---
layout: "image"
title: "Sinus Variante 4 - Gelenk"
date: "2014-09-14T12:48:39"
picture: "Sinus_Variante_4_Bild_2_publish.jpg"
weight: "6"
konstrukteure: 
- "MickyW"
fotografen:
- "MickyW"
keywords: ["Sinus", "Gleitführung"]
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- details/39354
imported:
- "2019"
_4images_image_id: "39354"
_4images_cat_id: "2948"
_4images_user_id: "1806"
_4images_image_date: "2014-09-14T12:48:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39354 -->
Hier ist Variante 3 dadurch modifiziert worden, dass das Kurbelgelenk kein Stein mehr ist, sondern ein auf die Achse festklemmbares Dings (wie heisst das nur gerade?)