---
layout: "comment"
hidden: true
title: "19511"
date: "2014-09-14T19:54:48"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Sehr schön!

Das geht um so leichter, je länger die Gleitführung (hier: das Alu) ist, weil sich dann immer weniger Verkantungsneigung ergibt.

Das könnte man doch noch um einen Stift ergänzen, der auf einer Papierrolle genau einen Sinus zeichnen? Vielleicht mit noch einem zweiten Stift, der einfach konstant die x-Achse dazu malt?

Gruß,
Stefan