---
layout: "image"
title: "Addierer/Subtraihierer mit Raederkurbelgetriebe - Gesamtansicht"
date: "2014-03-02T18:43:51"
picture: "AddiererRaederkurbelgetriebeGesamtansicht.jpg"
weight: "2"
konstrukteure: 
- "MickyW"
fotografen:
- "MickyW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- details/38402
imported:
- "2019"
_4images_image_id: "38402"
_4images_cat_id: "2859"
_4images_user_id: "1806"
_4images_image_date: "2014-03-02T18:43:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38402 -->
Falls mal die Batterie des Taschenrechners leer ist... :-)