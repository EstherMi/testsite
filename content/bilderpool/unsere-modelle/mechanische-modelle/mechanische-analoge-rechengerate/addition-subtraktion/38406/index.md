---
layout: "image"
title: "Addierer/Subtrahierer mit Raederkurbelgetriebe (erste Baustufe)"
date: "2014-03-02T18:43:51"
picture: "AddiererRaedkurbelgetriebeErsteBaustufe.jpg"
weight: "6"
konstrukteure: 
- "MickyW"
fotografen:
- "MickyW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- details/38406
imported:
- "2019"
_4images_image_id: "38406"
_4images_cat_id: "2859"
_4images_user_id: "1806"
_4images_image_date: "2014-03-02T18:43:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38406 -->
Hier die primitiven Elemente. Die beiden einzelnen Bausteine mit Loch gehören auf die Scheiben, so das die jeweilige Achse durch das Loch führt.