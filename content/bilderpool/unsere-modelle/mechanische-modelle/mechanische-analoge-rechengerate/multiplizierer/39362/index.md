---
layout: "image"
title: "Multiplizierer - Variante 1 - Seitenansicht"
date: "2014-09-14T20:24:06"
picture: "Multiplizierer_Variante_1_Bild_2_publish.jpg"
weight: "2"
konstrukteure: 
- "MickyW"
fotografen:
- "MickyW"
keywords: ["Multplizierer", "Gleitführung"]
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- details/39362
imported:
- "2019"
_4images_image_id: "39362"
_4images_cat_id: "2950"
_4images_user_id: "1806"
_4images_image_date: "2014-09-14T20:24:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39362 -->
HIer das ganze nochmal von der Seite.

Eine kitzlige Sache ist auch die einseitge Lagerung der einen Eingangsachse. (Hier ein wenig gepfuscht). Es gibt noch viel Verbesserungspotential.