---
layout: "comment"
hidden: true
title: "991"
date: "2006-04-13T15:17:02"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Viel einfacher: einfach die Klemmbuchse auf die andere Seite setzen! Das Ritzel bekommt Druck von der Seite und neigt dazu, sich vom Stufengetriebe weg zu bewegen. Eben das soll die Clips-Geschichte verhindern.