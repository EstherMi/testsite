---
layout: "image"
title: "verseilmaschine 1"
date: "2005-11-13T12:11:19"
picture: "verseilmaschine_1.jpg"
weight: "1"
konstrukteure: 
- "Sven Engelke & Ralf Gerken"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/5325
imported:
- "2019"
_4images_image_id: "5325"
_4images_cat_id: "457"
_4images_user_id: "1"
_4images_image_date: "2005-11-13T12:11:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5325 -->
