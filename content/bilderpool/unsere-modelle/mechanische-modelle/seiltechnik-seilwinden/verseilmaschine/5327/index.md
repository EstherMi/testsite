---
layout: "image"
title: "verseilmaschine 11"
date: "2005-11-13T12:11:20"
picture: "verseilmaschine_11.jpg"
weight: "3"
konstrukteure: 
- "Sven Engelke & Ralf Gerken"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/5327
imported:
- "2019"
_4images_image_id: "5327"
_4images_cat_id: "457"
_4images_user_id: "1"
_4images_image_date: "2005-11-13T12:11:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5327 -->
