---
layout: "image"
title: "'Abstauber'"
date: "2007-09-09T15:10:00"
picture: "d_bilder_014.jpg"
weight: "3"
konstrukteure: 
- "dragon"
fotografen:
- "dragon"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dragon"
license: "unknown"
legacy_id:
- details/11441
imported:
- "2019"
_4images_image_id: "11441"
_4images_cat_id: "1031"
_4images_user_id: "637"
_4images_image_date: "2007-09-09T15:10:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11441 -->
Das ist schon fast die Endstation von meiner maschine die Lichtschranke erkennt den Gegenstand und hält das förderband an. Anschließend wird der drehteller zur seite gedreht und der gegenstand ausgeworfen