---
layout: "image"
title: "'Abstauber'"
date: "2007-09-09T15:10:00"
picture: "d_bilder_001.jpg"
weight: "4"
konstrukteure: 
- "dragon"
fotografen:
- "dragon"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dragon"
license: "unknown"
legacy_id:
- details/11442
imported:
- "2019"
_4images_image_id: "11442"
_4images_cat_id: "1031"
_4images_user_id: "637"
_4images_image_date: "2007-09-09T15:10:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11442 -->
ein gegenstand läuft durch die maschine und wird von dem Ventilator "abgestaubt" danach gehts auf den drehteller und dann in ein gefäß