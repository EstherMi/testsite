---
layout: "image"
title: "Bild 08 - Anzeige FT"
date: "2008-05-16T07:15:06"
picture: "Bild_08_-_Anzeige_FT.jpg"
weight: "8"
konstrukteure: 
- "Laserman"
fotografen:
- "Laserman"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/14521
imported:
- "2019"
_4images_image_id: "14521"
_4images_cat_id: "1337"
_4images_user_id: "724"
_4images_image_date: "2008-05-16T07:15:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14521 -->
