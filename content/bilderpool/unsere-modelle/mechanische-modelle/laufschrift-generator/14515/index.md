---
layout: "image"
title: "Bild 02 - Detail Schleifring"
date: "2008-05-16T07:15:05"
picture: "Bild_02_-_Detail_Schleifring.jpg"
weight: "2"
konstrukteure: 
- "Laserman"
fotografen:
- "Laserman"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/14515
imported:
- "2019"
_4images_image_id: "14515"
_4images_cat_id: "1337"
_4images_user_id: "724"
_4images_image_date: "2008-05-16T07:15:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14515 -->
Über einen 4-poligen Klinkenstecker, der sich mitdreht, bekomme ich meine Daten zu den LEDs.
Darüber die Buchse dreht sich nicht mit.