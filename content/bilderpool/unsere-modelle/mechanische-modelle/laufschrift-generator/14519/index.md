---
layout: "image"
title: "Bild 06 - Detail Motoraufhängung"
date: "2008-05-16T07:15:06"
picture: "Bild_06_-_Detail_Motoraufhngung.jpg"
weight: "6"
konstrukteure: 
- "Laserman"
fotografen:
- "Laserman"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/14519
imported:
- "2019"
_4images_image_id: "14519"
_4images_cat_id: "1337"
_4images_user_id: "724"
_4images_image_date: "2008-05-16T07:15:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14519 -->
Ein ausgeschlachteter Motor aus einem alten HP-Drucker dient als Antrieb. Ehemals diente er als Antrieb für den Druckkopf. Hier zu sehen die Art der Befestigung. Es könnte natürlich auch ein Powermotor von FT sein...   ;c)