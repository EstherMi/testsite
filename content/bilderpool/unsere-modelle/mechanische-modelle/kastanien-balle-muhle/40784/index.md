---
layout: "image"
title: "Bälle- & Kastanienmühle Vorne 01 Fischertechnik & Lego Duplo bzw."
date: "2015-04-16T22:04:42"
picture: "kastanienbaellemuehle17.jpg"
weight: "17"
konstrukteure: 
- "Kai B aus D"
fotografen:
- "Kai B aus D"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- details/40784
imported:
- "2019"
_4images_image_id: "40784"
_4images_cat_id: "3064"
_4images_user_id: "1677"
_4images_image_date: "2015-04-16T22:04:42"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40784 -->
Ich hoffe ich werde nicht ge(lego)steinigt,
aber nur so konnte ich die Anzahl von verschluckbaren Kleinteile vermindern.