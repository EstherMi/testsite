---
layout: "image"
title: "Kastanienmühle Vorne 01"
date: "2015-04-16T22:04:42"
picture: "kastanienbaellemuehle07.jpg"
weight: "7"
konstrukteure: 
- "Kai B aus D"
fotografen:
- "Kai B aus D"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- details/40774
imported:
- "2019"
_4images_image_id: "40774"
_4images_cat_id: "3064"
_4images_user_id: "1677"
_4images_image_date: "2015-04-16T22:04:42"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40774 -->
Auf der Achse des Schaufelrades sitzt ein Scheibe. Mit ihr könne ander Konstruktionen angetrieben werden.
Allerdings sind die Drehimpulse sehr kurz.