---
layout: "image"
title: "Wer findet alle FT Bauten und Teile :)"
date: "2015-04-16T22:04:42"
picture: "kastanienbaellemuehle16.jpg"
weight: "16"
konstrukteure: 
- "Kai B aus D"
fotografen:
- "Kai B aus D"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- details/40783
imported:
- "2019"
_4images_image_id: "40783"
_4images_cat_id: "3064"
_4images_user_id: "1677"
_4images_image_date: "2015-04-16T22:04:42"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40783 -->
In diesem Bild haben sich mehrere fischertechnik Projekte versteckt. 

Finde sie (die Stellen) und Vermute was es sein könnte. :-)