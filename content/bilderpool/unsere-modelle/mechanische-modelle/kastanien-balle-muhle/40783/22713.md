---
layout: "comment"
hidden: true
title: "22713"
date: "2016-10-29T23:04:28"
uploadBy:
- "TiloRust"
license: "unknown"
imported:
- "2019"
---
Challenge accepted:
- die "Flügel" bestehen aus Statikteilen mit gebogenen Einsätzen
- das Lager aus einem 15er mit Loch, daneben jeweils ein 15er und ein 7,5er
- Achse
- Im Hintergrund (rechter Rand hinter dem Leogo-Gewirr) ist ein "Gewächshaus" mit Alu-Profilen und einer Klappe