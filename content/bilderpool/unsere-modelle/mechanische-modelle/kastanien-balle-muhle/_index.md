---
layout: "overview"
title: "Kastanien- & Bälle-Mühle"
date: 2019-12-17T19:25:52+01:00
legacy_id:
- categories/3064
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3064 --> 
Im Sandkasten hatte bestimmt der Eine oder Andere im Kindesalter eine Sandmühle.
Oben wird rieselfähiger Sand eingefüllt. Dieser  fließt durch einen Trichter.
Der herunterfallende Sand treibt ein oder zwei Mühlräder an.

Das Prinzip, wurde einfach auf Kastanien und Bälle angewandt.

Anforderungen die erfüllt werden sollten:

+ es soll ein "Lern(äahhh) Spielzeug" für Kinder im Alter von 1-99 Jahren sein
+ hohe Stabilität
+ Unfallsicher (verschluckbare Kleinteile)
+ Umfallsicher (umsturzsicher)