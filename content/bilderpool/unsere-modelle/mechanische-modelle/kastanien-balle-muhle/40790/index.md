---
layout: "image"
title: "kastanienbaellemuehle23.jpg"
date: "2015-04-16T22:04:42"
picture: "kastanienbaellemuehle23.jpg"
weight: "23"
konstrukteure: 
- "Kai B aus D"
fotografen:
- "Kai B aus D"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- details/40790
imported:
- "2019"
_4images_image_id: "40790"
_4images_cat_id: "3064"
_4images_user_id: "1677"
_4images_image_date: "2015-04-16T22:04:42"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40790 -->
