---
layout: "image"
title: "Kastanienmühle Vorne 02"
date: "2015-04-16T22:04:42"
picture: "kastanienbaellemuehle08.jpg"
weight: "8"
konstrukteure: 
- "Kai B aus D"
fotografen:
- "Kai B aus D"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- details/40775
imported:
- "2019"
_4images_image_id: "40775"
_4images_cat_id: "3064"
_4images_user_id: "1677"
_4images_image_date: "2015-04-16T22:04:42"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40775 -->
In unserem Kastanienbunker sind Kastanien viele verschiedener Größen.
Sind die Kastanien zu groß, verstopfen sie den Trichter. 
