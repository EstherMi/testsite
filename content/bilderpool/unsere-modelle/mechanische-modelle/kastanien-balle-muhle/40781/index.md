---
layout: "image"
title: "Kastanienmühle Seite 01"
date: "2015-04-16T22:04:42"
picture: "kastanienbaellemuehle14.jpg"
weight: "14"
konstrukteure: 
- "Kai B aus D"
fotografen:
- "Kai B aus D"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- details/40781
imported:
- "2019"
_4images_image_id: "40781"
_4images_cat_id: "3064"
_4images_user_id: "1677"
_4images_image_date: "2015-04-16T22:04:42"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40781 -->
Von der Seite kann man den Vorgang auch "begreifen" und verkanntete Kastanien entfernen.