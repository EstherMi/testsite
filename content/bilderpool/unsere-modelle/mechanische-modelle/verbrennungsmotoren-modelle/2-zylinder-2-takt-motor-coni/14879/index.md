---
layout: "image"
title: "Vorderansicht 3"
date: "2008-07-15T22:17:21"
picture: "zylindermotor05.jpg"
weight: "6"
konstrukteure: 
- "Constantin N."
fotografen:
- "Constantin N."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Coni"
license: "unknown"
legacy_id:
- details/14879
imported:
- "2019"
_4images_image_id: "14879"
_4images_cat_id: "1358"
_4images_user_id: "653"
_4images_image_date: "2008-07-15T22:17:21"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14879 -->
