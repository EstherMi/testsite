---
layout: "image"
title: "Zylinder"
date: "2008-07-15T22:17:21"
picture: "zylindermotor08.jpg"
weight: "9"
konstrukteure: 
- "Constantin N."
fotografen:
- "Constantin N."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Coni"
license: "unknown"
legacy_id:
- details/14882
imported:
- "2019"
_4images_image_id: "14882"
_4images_cat_id: "1358"
_4images_user_id: "653"
_4images_image_date: "2008-07-15T22:17:21"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14882 -->
