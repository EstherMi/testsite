---
layout: "image"
title: "2-Takt-Motor 7"
date: "2007-01-08T17:16:10"
picture: "taktmotor7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/8349
imported:
- "2019"
_4images_image_id: "8349"
_4images_cat_id: "768"
_4images_user_id: "502"
_4images_image_date: "2007-01-08T17:16:10"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8349 -->
Die Druckluftherstellung ist bei mir noch echte Handarbeit :-)