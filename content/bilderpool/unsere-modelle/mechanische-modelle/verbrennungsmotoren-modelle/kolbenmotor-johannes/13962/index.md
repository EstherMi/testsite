---
layout: "image"
title: "Auslassventil"
date: "2008-03-19T16:00:27"
picture: "kolbenmotor7.jpg"
weight: "7"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/13962
imported:
- "2019"
_4images_image_id: "13962"
_4images_cat_id: "1282"
_4images_user_id: "747"
_4images_image_date: "2008-03-19T16:00:27"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13962 -->
Auf diesem Bild sieht man das Auslassventil, das gerade betätigt wird. Es ist offen, damit  die durch die Explosion enstandenen Abgase wieder heraus können. Da der Kolben sich gerade mit Schwung in Richtung Zündkerze bewegt, presst er die Abgase heraus.