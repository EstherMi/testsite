---
layout: "image"
title: "Zündung"
date: "2008-03-19T16:00:27"
picture: "kolbenmotor5.jpg"
weight: "5"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/13960
imported:
- "2019"
_4images_image_id: "13960"
_4images_cat_id: "1282"
_4images_user_id: "747"
_4images_image_date: "2008-03-19T16:00:27"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13960 -->
Auf diesem Bild sieht man das Einlassventil (gerade geöffnet) und die Zündkerze (gerade angeschaltet). Da das Einlassventil geöffnet ist, läuft Sprit an die Zündkerze, die gerade ein kleinen Funken macht (der elektronisch gesteuert und hergestellt wird) und dadurch explodiert der Sprit und es entsteht eine Druckwelle die den Kolben nach hinten drückt.