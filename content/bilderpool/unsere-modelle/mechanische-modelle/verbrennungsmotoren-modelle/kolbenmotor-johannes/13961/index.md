---
layout: "image"
title: "Zündkerzenfunktion"
date: "2008-03-19T16:00:27"
picture: "kolbenmotor6.jpg"
weight: "6"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/13961
imported:
- "2019"
_4images_image_id: "13961"
_4images_cat_id: "1282"
_4images_user_id: "747"
_4images_image_date: "2008-03-19T16:00:27"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13961 -->
Hier sieht man die von dem Kolben angetriebenen Zahnräder. Einmal pro Umdrehung betätigen sie den Taster, der die Zündkerze anschaltet. Das Zahnrad, das den Taster betätigt, nennt man auch Zündspule.