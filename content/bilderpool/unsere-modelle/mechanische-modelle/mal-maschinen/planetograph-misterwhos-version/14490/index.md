---
layout: "image"
title: "Ergebnis"
date: "2008-05-08T17:07:59"
picture: "planetergebnisse3.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- details/14490
imported:
- "2019"
_4images_image_id: "14490"
_4images_cat_id: "1336"
_4images_user_id: "8"
_4images_image_date: "2008-05-08T17:07:59"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14490 -->
- Alle Drehkränze drehen sich gleichschnell.
- Excenter haben eine unterschiedliche Drehrichtung wie Drehkränze der Excenter