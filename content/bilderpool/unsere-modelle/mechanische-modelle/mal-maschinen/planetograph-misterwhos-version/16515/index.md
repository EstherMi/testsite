---
layout: "image"
title: "Planetograph Bildergalerie"
date: "2008-11-27T21:38:03"
picture: "bildergalerie08.jpg"
weight: "8"
konstrukteure: 
- "Joachim Jacobi"
fotografen:
- "Joachim Jacobi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- details/16515
imported:
- "2019"
_4images_image_id: "16515"
_4images_cat_id: "1336"
_4images_user_id: "8"
_4images_image_date: "2008-11-27T21:38:03"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16515 -->
Drei innenliegende Ellipsen.