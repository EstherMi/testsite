---
layout: "image"
title: "Planetograph Bildergalerie"
date: "2008-11-27T21:38:02"
picture: "bildergalerie03.jpg"
weight: "3"
konstrukteure: 
- "Joachim Jacobi"
fotografen:
- "Joachim Jacobi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- details/16510
imported:
- "2019"
_4images_image_id: "16510"
_4images_cat_id: "1336"
_4images_user_id: "8"
_4images_image_date: "2008-11-27T21:38:02"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16510 -->
Blattfüllendes Bild. Die andersfarbigen Flecken stammen von der Rückseite.
Die unterschiedlichen Figuren kommen nur durch Änderung der Übersetzungen zu stande. Meistens ist die Drehzahl des Tisches geändert worden.