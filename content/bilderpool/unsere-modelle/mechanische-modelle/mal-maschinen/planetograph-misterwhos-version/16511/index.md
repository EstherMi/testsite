---
layout: "image"
title: "Planetograph Bildergalerie"
date: "2008-11-27T21:38:02"
picture: "bildergalerie04.jpg"
weight: "4"
konstrukteure: 
- "Joachim Jacobi"
fotografen:
- "Joachim Jacobi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- details/16511
imported:
- "2019"
_4images_image_id: "16511"
_4images_cat_id: "1336"
_4images_user_id: "8"
_4images_image_date: "2008-11-27T21:38:02"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16511 -->
Zweimal die gleiche Figur in verschiedenen Farben. Das Blatt wurde um 90 ° gedreht.
Die andersfarbigen Flecken stammen von der Rückseite.
Die unterschiedlichen Figuren kommen nur durch Änderung der Übersetzungen zu stande. Meistens ist die Drehzahl des Tisches geändert worden.