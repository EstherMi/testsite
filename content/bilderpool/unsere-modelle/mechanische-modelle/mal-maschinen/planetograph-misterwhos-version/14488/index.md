---
layout: "image"
title: "Ergebnis"
date: "2008-05-08T17:07:59"
picture: "planetergebnisse1.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- details/14488
imported:
- "2019"
_4images_image_id: "14488"
_4images_cat_id: "1336"
_4images_user_id: "8"
_4images_image_date: "2008-05-08T17:07:59"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14488 -->
Tisch dreht sich doppelt so schnell wie die Drehkränze der Excenter