---
layout: "image"
title: "Ergebniss"
date: "2008-05-06T16:31:09"
picture: "planetographmwversion6.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- details/14485
imported:
- "2019"
_4images_image_id: "14485"
_4images_cat_id: "1336"
_4images_user_id: "8"
_4images_image_date: "2008-05-06T16:31:09"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14485 -->
Hier wurde der Tisch mitgedreht. Ausgangsstellung war eigentlich die gleiche wie bei Knarfs Planetograph II (Eines der letzten Bilder)