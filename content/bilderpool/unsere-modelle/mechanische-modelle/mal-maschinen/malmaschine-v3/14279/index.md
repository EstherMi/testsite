---
layout: "image"
title: "Malmaschine V3"
date: "2008-04-18T21:08:54"
picture: "malmaschinevderkompaktograph03.jpg"
weight: "10"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/14279
imported:
- "2019"
_4images_image_id: "14279"
_4images_cat_id: "1322"
_4images_user_id: "729"
_4images_image_date: "2008-04-18T21:08:54"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14279 -->
von rechts