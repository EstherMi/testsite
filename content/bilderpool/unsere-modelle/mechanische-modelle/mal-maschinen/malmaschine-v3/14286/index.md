---
layout: "image"
title: "Ergebnis"
date: "2008-04-18T21:08:55"
picture: "malmaschinevderkompaktograph10.jpg"
weight: "17"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/14286
imported:
- "2019"
_4images_image_id: "14286"
_4images_cat_id: "1322"
_4images_user_id: "729"
_4images_image_date: "2008-04-18T21:08:55"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14286 -->
Ergebnis04