---
layout: "image"
title: "PICT3919 Kopie"
date: "2008-04-19T14:36:42"
picture: "PICT3919_Kopie.jpg"
weight: "2"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/14302
imported:
- "2019"
_4images_image_id: "14302"
_4images_cat_id: "1322"
_4images_user_id: "729"
_4images_image_date: "2008-04-19T14:36:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14302 -->
Einen Drehpunkt verschoben.