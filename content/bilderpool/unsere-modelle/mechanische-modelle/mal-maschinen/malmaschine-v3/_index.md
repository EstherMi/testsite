---
layout: "overview"
title: "Malmaschine V3"
date: 2019-12-17T19:23:14+01:00
legacy_id:
- categories/1322
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1322 --> 
Ähnlich wie V2, aber kompakter. Kleinere Bauplatte 500, nur ein Powermotor, geändertes Malmodul.