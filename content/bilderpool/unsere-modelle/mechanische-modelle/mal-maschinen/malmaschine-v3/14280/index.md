---
layout: "image"
title: "Malmaschine V3"
date: "2008-04-18T21:08:54"
picture: "malmaschinevderkompaktograph04.jpg"
weight: "11"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/14280
imported:
- "2019"
_4images_image_id: "14280"
_4images_cat_id: "1322"
_4images_user_id: "729"
_4images_image_date: "2008-04-18T21:08:54"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14280 -->
von hinten