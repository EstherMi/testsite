---
layout: "comment"
hidden: true
title: "6406"
date: "2008-05-05T09:59:34"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Einspruch, Euer Ehren ;-) Bei so kurzen Ketten ist ein Kettenglied mehr oft gleichbedeutend mit einer labberigen Kette, und eines weniger gleichbedeutend mit "es reicht einfach nicht". Damit das schön sauber läuft halte ich ein so wie hier oder federnd angebrachtes Spannrad für durchaus angebracht ;-)

Gruß,
Stefan