---
layout: "comment"
hidden: true
title: "6392"
date: "2008-05-03T21:25:20"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
@Frank
Gemeint war das Spiel zwischen den Drehkranzteilen Unter- und Oberteil. Beseitigst du das nicht, beschreiben die beiden Achsen annähernd einen Kegelmantel. Deine Grundplatte benötigt eine absolut ebene Auflage, auf die sie notfalls zu fixieren ist, wenn die Aufbauten sie nicht gleichmäßig auf die Unterlage aufdrücken. Alle senkrechten Achsen müssen zueinander absolut parallel sein. Die beiden Lager und das Gelenk der Zeichenarme müssen spielfrei sein. Mit den für Lagerungen gedachten ft-Teilen ist aber eine spielfreie Lagerung nicht erreichbar, besonders dann wenn lange Hebelarme an den Lagern angreifen. Da helfen auch nicht die Gegengewichte. Es gibt aber dazu ein "fremd" eingesetztes ft-Teil, mit dem man das kann! Es ist der Klemmstift mit seinem Ø4,1mm. Da er nur 15mm lang ist, muß das über je zwei Lagerzapfen umgesetzt werden. Tüftle mal etwas, es lohnt sich ... Ich darf dazu aus praktischer Erfahrung raten. Die Statikteile mit ihren Strebenfixierlöchern sind überhaupt nicht geeignet zum Lagern von Metallwellen. Mit dem U-Träger 150 verbessert sich lediglich die "Leibungslänge der Lagerung" von 15 auf 30mm. Vergiß das also schnell. Ansonsten schau dir die Meccanobilder genau an, die heute ergänzt wurden. Sie sind ein Lehrstück für so eine Mechanik. Noch ein Tipp heute zum Schluß, nimm als Schreibstift den Roller 97. Das nicht, weil heute im Kommentar zum "Meccano" von Rollschreibern gesprochen wird, sondern weil ich dir aus längerer Erfahrung damit dazu raten kann.
Gruß Udo2