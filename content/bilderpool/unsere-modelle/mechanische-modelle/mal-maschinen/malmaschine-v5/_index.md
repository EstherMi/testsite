---
layout: "overview"
title: "Malmaschine V5"
date: 2019-12-17T19:23:19+01:00
legacy_id:
- categories/1332
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1332 --> 
Malmaschine mit 2 Planetengetrieben, 3 Drehkränze Z58 sorgen für Regelmäßigkeit :-) , alles modular.