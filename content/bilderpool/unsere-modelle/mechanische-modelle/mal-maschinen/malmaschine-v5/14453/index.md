---
layout: "image"
title: "Detail Getriebe und Kupplung"
date: "2008-05-04T12:53:13"
picture: "PICT4089.jpg"
weight: "4"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/14453
imported:
- "2019"
_4images_image_id: "14453"
_4images_cat_id: "1332"
_4images_user_id: "729"
_4images_image_date: "2008-05-04T12:53:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14453 -->
Detail Getriebe und Kupplung