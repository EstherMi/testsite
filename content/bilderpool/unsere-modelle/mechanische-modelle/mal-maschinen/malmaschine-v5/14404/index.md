---
layout: "image"
title: "Malmaschine V5"
date: "2008-04-28T22:27:56"
picture: "malmaschinevplaneto15.jpg"
weight: "21"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/14404
imported:
- "2019"
_4images_image_id: "14404"
_4images_cat_id: "1332"
_4images_user_id: "729"
_4images_image_date: "2008-04-28T22:27:56"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14404 -->
Ergebnis03