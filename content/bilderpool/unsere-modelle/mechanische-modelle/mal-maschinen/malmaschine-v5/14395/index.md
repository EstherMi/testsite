---
layout: "image"
title: "Malmaschine V5"
date: "2008-04-28T22:27:55"
picture: "malmaschinevplaneto06.jpg"
weight: "12"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/14395
imported:
- "2019"
_4images_image_id: "14395"
_4images_cat_id: "1332"
_4images_user_id: "729"
_4images_image_date: "2008-04-28T22:27:55"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14395 -->
Planetengetriebe-Ablenkmodul