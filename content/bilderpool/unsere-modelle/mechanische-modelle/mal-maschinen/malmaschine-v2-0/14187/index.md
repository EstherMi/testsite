---
layout: "image"
title: "Malmaschine2-08"
date: "2008-04-07T07:56:04"
picture: "malmaschinev08.jpg"
weight: "8"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/14187
imported:
- "2019"
_4images_image_id: "14187"
_4images_cat_id: "1313"
_4images_user_id: "729"
_4images_image_date: "2008-04-07T07:56:04"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14187 -->
Zeichentisch-Antrieb
