---
layout: "image"
title: "Malmaschine2-09"
date: "2008-04-07T07:56:04"
picture: "malmaschinev09.jpg"
weight: "9"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/14188
imported:
- "2019"
_4images_image_id: "14188"
_4images_cat_id: "1313"
_4images_user_id: "729"
_4images_image_date: "2008-04-07T07:56:04"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14188 -->
Zeichentisch-Antrieb