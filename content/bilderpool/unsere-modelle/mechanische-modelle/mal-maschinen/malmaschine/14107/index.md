---
layout: "image"
title: "Malmaschine05"
date: "2008-03-25T17:56:41"
picture: "malmaschine03.jpg"
weight: "13"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/14107
imported:
- "2019"
_4images_image_id: "14107"
_4images_cat_id: "1294"
_4images_user_id: "729"
_4images_image_date: "2008-03-25T17:56:41"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14107 -->
Ansicht von hinten links