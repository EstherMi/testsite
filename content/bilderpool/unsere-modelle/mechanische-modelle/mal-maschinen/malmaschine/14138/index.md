---
layout: "image"
title: "Malmaschine19"
date: "2008-03-28T16:39:51"
picture: "malmaschine05_2.jpg"
weight: "5"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/14138
imported:
- "2019"
_4images_image_id: "14138"
_4images_cat_id: "1294"
_4images_user_id: "729"
_4images_image_date: "2008-03-28T16:39:51"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14138 -->
Neue Stifthalterung