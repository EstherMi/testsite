---
layout: "image"
title: "Malmaschine17"
date: "2008-03-28T16:39:51"
picture: "malmaschine03_2.jpg"
weight: "3"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/14136
imported:
- "2019"
_4images_image_id: "14136"
_4images_cat_id: "1294"
_4images_user_id: "729"
_4images_image_date: "2008-03-28T16:39:51"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14136 -->
Neuer Zeichenteller