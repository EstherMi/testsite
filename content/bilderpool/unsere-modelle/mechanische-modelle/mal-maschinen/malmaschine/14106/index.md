---
layout: "image"
title: "Malmaschine04"
date: "2008-03-25T17:56:41"
picture: "malmaschine02.jpg"
weight: "12"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: ["Lissajous"]
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/14106
imported:
- "2019"
_4images_image_id: "14106"
_4images_cat_id: "1294"
_4images_user_id: "729"
_4images_image_date: "2008-03-25T17:56:41"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14106 -->
Ansicht von vorne rechts