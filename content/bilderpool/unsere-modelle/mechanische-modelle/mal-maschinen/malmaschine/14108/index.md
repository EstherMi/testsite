---
layout: "image"
title: "Malmaschine06"
date: "2008-03-25T17:56:41"
picture: "malmaschine04.jpg"
weight: "14"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/14108
imported:
- "2019"
_4images_image_id: "14108"
_4images_cat_id: "1294"
_4images_user_id: "729"
_4images_image_date: "2008-03-25T17:56:41"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14108 -->
Getriebe X-Richtung von oben