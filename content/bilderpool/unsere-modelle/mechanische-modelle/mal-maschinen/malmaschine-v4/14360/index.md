---
layout: "image"
title: "Malmaschine V4"
date: "2008-04-23T17:06:41"
picture: "malmaschinevderplanetograph01.jpg"
weight: "1"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/14360
imported:
- "2019"
_4images_image_id: "14360"
_4images_cat_id: "1329"
_4images_user_id: "729"
_4images_image_date: "2008-04-23T17:06:41"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14360 -->
Gesamtansicht von oben