---
layout: "comment"
hidden: true
title: "9065"
date: "2009-04-26T22:34:29"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Mir fällt gerade, das könnte ja auch eine ausfahrbare Stütze sein, die nur in Druckrichtung belastet wird. Vielleicht für Kranstützen oder sowas. Naja, irgendwo, wo's keiner vermutet, wirst Du das bestimmt in einem Modell mal verbauen. :-)

Gruß,
Stefan