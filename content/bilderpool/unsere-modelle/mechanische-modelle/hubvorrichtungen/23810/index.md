---
layout: "image"
title: "hv04.jpg"
date: "2009-04-25T10:32:22"
picture: "Hubvorrichtung04.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/23810
imported:
- "2019"
_4images_image_id: "23810"
_4images_cat_id: "1244"
_4images_user_id: "4"
_4images_image_date: "2009-04-25T10:32:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23810 -->
Fertig montiert und ganz ausgefahren. Die Zahnstange bringt in beiden Richtungen volle Kraft, weil sie in allen Richtungen über die Schnecken auf den BS15-Loch abgestützt wird. Beim Ziehen fehlt allerdings eine formschlüssige Verbindung zum gezogenen Objekt. Drücken geht immer.

(Die Klemmbuchse oben hat einen Schmelz/Pressfehler - ich war das nicht!)