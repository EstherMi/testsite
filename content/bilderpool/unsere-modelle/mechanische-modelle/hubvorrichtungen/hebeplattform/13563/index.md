---
layout: "image"
title: "Hebeplatform 2"
date: "2008-02-06T17:15:05"
picture: "DSCN0008.jpg"
weight: "2"
konstrukteure: 
- "bodo42"
fotografen:
- "bodo42"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guest"
license: "unknown"
legacy_id:
- details/13563
imported:
- "2019"
_4images_image_id: "13563"
_4images_cat_id: "1245"
_4images_user_id: "-1"
_4images_image_date: "2008-02-06T17:15:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13563 -->
