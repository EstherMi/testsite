---
layout: "overview"
title: "Hebeplattform"
date: 2019-12-17T19:22:18+01:00
legacy_id:
- categories/1245
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1245 --> 
Hier ist mein Versuch, eine möglichst mobile Hebeplattform zu bauen. Leider ist sie im zusammengefalteten Zustand noch viel zu groß. Auch die zu hebende Last kann nicht sehr groß sein, weil das untere Schneckengetriebe sonst nicht mehr mitspielt.