---
layout: "image"
title: "Gesamtansicht"
date: "2009-02-14T09:50:29"
picture: "hebebuehne01.jpg"
weight: "1"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/17392
imported:
- "2019"
_4images_image_id: "17392"
_4images_cat_id: "1565"
_4images_user_id: "845"
_4images_image_date: "2009-02-14T09:50:29"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17392 -->
