---
layout: "image"
title: "mit Auto"
date: "2009-02-14T09:50:30"
picture: "hebebuehne08.jpg"
weight: "8"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/17399
imported:
- "2019"
_4images_image_id: "17399"
_4images_cat_id: "1565"
_4images_user_id: "845"
_4images_image_date: "2009-02-14T09:50:30"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17399 -->
Hier sieht man die Hebebühne mit meinem Rennwagen:
http://www.ftcommunity.de/categories.php?cat_id=1560