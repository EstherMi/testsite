---
layout: "image"
title: "Scherenbühne 2"
date: "2008-02-06T17:15:24"
picture: "DSCN0020.jpg"
weight: "2"
konstrukteure: 
- "bodo42"
fotografen:
- "bodo42"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guest"
license: "unknown"
legacy_id:
- details/13572
imported:
- "2019"
_4images_image_id: "13572"
_4images_cat_id: "1246"
_4images_user_id: "-1"
_4images_image_date: "2008-02-06T17:15:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13572 -->
