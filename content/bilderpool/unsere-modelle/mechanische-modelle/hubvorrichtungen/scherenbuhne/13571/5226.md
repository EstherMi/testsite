---
layout: "comment"
hidden: true
title: "5226"
date: "2008-02-06T20:53:01"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Oder dafür sorgen, dass das Seil gleich beides tut: am untersten oder am zweiten Scherengelenk heben und gleichzeitig die Scherengriffe zusammenziehen. Ein paar Umlenkrollen und eine nach beiden Seiten verlängerte Achse sollten schon ausreichen.

Gruß,
Harald