---
layout: "image"
title: "Hub06.JPG"
date: "2006-03-12T13:49:16"
picture: "Hub06.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5876
imported:
- "2019"
_4images_image_id: "5876"
_4images_cat_id: "507"
_4images_user_id: "4"
_4images_image_date: "2006-03-12T13:49:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5876 -->
