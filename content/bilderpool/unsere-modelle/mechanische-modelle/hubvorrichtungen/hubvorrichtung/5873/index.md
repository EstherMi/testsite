---
layout: "image"
title: "Hub03.JPG"
date: "2006-03-12T13:45:58"
picture: "Hub03.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5873
imported:
- "2019"
_4images_image_id: "5873"
_4images_cat_id: "507"
_4images_user_id: "4"
_4images_image_date: "2006-03-12T13:45:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5873 -->
Das Getriebe funktioniert nur, wenn auch eine Zahnstange dazwischen ist. Ohne diese gehen die Differenziale den Weg des kleineren Widerstands und drehen leer durch.
Mit Zahnstange ergibt sich eine ziemlich hohe Untersetzung: 1 Umdrehung des  Zahnrad Z10 links oben schiebt die Zahnstange um 1 Zahn weiter.