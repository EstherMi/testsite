---
layout: "overview"
title: "Hubvorrichtungen"
date: 2019-12-17T19:22:15+01:00
legacy_id:
- categories/1244
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1244 --> 
Maschinen zum Anheben von Sachen. Anders als bei einem Kran soll dabei die Sache nicht nach oben gezogen werden, sondern von unten nach oben gedrückt werden.