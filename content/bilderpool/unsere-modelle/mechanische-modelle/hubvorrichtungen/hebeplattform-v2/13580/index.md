---
layout: "image"
title: "Hebeplatform 1"
date: "2008-02-07T09:48:06"
picture: "DSCN0030.jpg"
weight: "1"
konstrukteure: 
- "bodo42"
fotografen:
- "bodo42"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guest"
license: "unknown"
legacy_id:
- details/13580
imported:
- "2019"
_4images_image_id: "13580"
_4images_cat_id: "1247"
_4images_user_id: "-1"
_4images_image_date: "2008-02-07T09:48:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13580 -->
Die zweite Version der Hebeplatform. Im zusammengefalteten Zustand liegen die Arme direkt aufeinander. Die Höher der roten Platte ist somit nur noch 11.5cm.