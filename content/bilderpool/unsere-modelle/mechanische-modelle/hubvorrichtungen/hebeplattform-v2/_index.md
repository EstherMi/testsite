---
layout: "overview"
title: "Hebeplattform v2"
date: 2019-12-17T19:22:21+01:00
legacy_id:
- categories/1247
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1247 --> 
Die zweite Version der Hebeplattform, die sich schon wesentlich besser zusammenfaltet. Das wurde möglich durch einen modifizierten Mini-Motor, bei dem ich die Feder an der Seite abgeschnitten habe. Dadurch ist er genau 15mm breit und kann in den Träger versenkt werden.