---
layout: "image"
title: "Hebeplatform 4"
date: "2008-02-07T09:48:06"
picture: "DSCN0033.jpg"
weight: "4"
konstrukteure: 
- "bodo42"
fotografen:
- "bodo42"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guest"
license: "unknown"
legacy_id:
- details/13583
imported:
- "2019"
_4images_image_id: "13583"
_4images_cat_id: "1247"
_4images_user_id: "-1"
_4images_image_date: "2008-02-07T09:48:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13583 -->
