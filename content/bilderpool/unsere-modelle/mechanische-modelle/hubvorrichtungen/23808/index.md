---
layout: "image"
title: "hv01"
date: "2009-04-25T10:27:07"
picture: "Hubvorrichtung01.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/23808
imported:
- "2019"
_4images_image_id: "23808"
_4images_cat_id: "1244"
_4images_user_id: "4"
_4images_image_date: "2009-04-25T10:27:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23808 -->
Noch so 'ne Hubvorrichtung, diesmal mit Schnecken auf Zahnstange. Der Verfahrweg ist nicht so berauschend, aber dafür kann man richtig Kräfte aufbringen.