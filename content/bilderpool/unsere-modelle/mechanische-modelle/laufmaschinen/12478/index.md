---
layout: "image"
title: "Bionic-Modell 1"
date: "2007-11-05T15:54:00"
picture: "IMG_0125.jpg"
weight: "1"
konstrukteure: 
- "Burkhard Eins"
fotografen:
- "Burkhard Eins"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Burkhard Eins"
license: "unknown"
legacy_id:
- details/12478
imported:
- "2019"
_4images_image_id: "12478"
_4images_cat_id: "1576"
_4images_user_id: "611"
_4images_image_date: "2007-11-05T15:54:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12478 -->
Das bekannte 'Herbie'-Bionic-Modell in einer leicht modifizierten Ausführung im klassischen ft-Grau.