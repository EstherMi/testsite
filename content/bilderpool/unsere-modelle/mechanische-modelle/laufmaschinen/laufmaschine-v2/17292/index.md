---
layout: "image"
title: "LMv2-02"
date: "2009-02-03T00:59:29"
picture: "laufmaschinev2.jpg"
weight: "2"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/17292
imported:
- "2019"
_4images_image_id: "17292"
_4images_cat_id: "1551"
_4images_user_id: "729"
_4images_image_date: "2009-02-03T00:59:29"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17292 -->
Ansicht von links vorne