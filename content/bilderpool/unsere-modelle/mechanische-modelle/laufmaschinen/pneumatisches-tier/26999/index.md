---
layout: "image"
title: "Pneumatisches Tier 1"
date: "2010-04-26T10:13:20"
picture: "P1080541_-_Kopie.jpg"
weight: "6"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/26999
imported:
- "2019"
_4images_image_id: "26999"
_4images_cat_id: "1942"
_4images_user_id: "1082"
_4images_image_date: "2010-04-26T10:13:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26999 -->
Dieses Tier läuft pneumatisch. Bislang aber leider nur gerade aus, wird sich noch ändern. Im Hintergrund ein Teil des Kompressors.