---
layout: "image"
title: "Pneumatisches Tier 6"
date: "2010-04-27T12:02:09"
picture: "P1080561_-_Kopie.jpg"
weight: "3"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27004
imported:
- "2019"
_4images_image_id: "27004"
_4images_cat_id: "1942"
_4images_user_id: "1082"
_4images_image_date: "2010-04-27T12:02:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27004 -->
Hier noch einmal von vorne/unten.