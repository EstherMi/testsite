---
layout: "image"
title: "Pneumatisches Tier 2"
date: "2010-04-26T10:13:20"
picture: "P1080542_-_Kopie.jpg"
weight: "7"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27000
imported:
- "2019"
_4images_image_id: "27000"
_4images_cat_id: "1942"
_4images_user_id: "1082"
_4images_image_date: "2010-04-26T10:13:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27000 -->
Hier noch einmal mit Kompressor, der leider noch nicht auf das Tier drauf passte...