---
layout: "image"
title: "lm03"
date: "2009-02-03T00:59:29"
picture: "laufmaschinebein3.jpg"
weight: "3"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/17289
imported:
- "2019"
_4images_image_id: "17289"
_4images_cat_id: "1550"
_4images_user_id: "729"
_4images_image_date: "2009-02-03T00:59:29"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17289 -->
von rechts vorne