---
layout: "image"
title: "Steuereinheit (3)"
date: "2017-06-19T19:47:06"
picture: "spinne09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/45980
imported:
- "2019"
_4images_image_id: "45980"
_4images_cat_id: "3417"
_4images_user_id: "104"
_4images_image_date: "2017-06-19T19:47:06"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45980 -->
Der Aufbau ist denkbar simpel.