---
layout: "image"
title: "Steuereinheit (1)"
date: "2017-06-19T19:47:06"
picture: "spinne07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/45978
imported:
- "2019"
_4images_image_id: "45978"
_4images_cat_id: "3417"
_4images_user_id: "104"
_4images_image_date: "2017-06-19T19:47:06"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45978 -->
Da ein Akku in der Spinne zu schwer geworden wäre, wird von außen gesteuert. Dieser Joystick fährt das Modell nach vorne (wo die grünen Augen hinschauen). Das ist die Pfeilrichtung: Man muss den Joystick in Richtung des WS30° schieben, damit die Spinne nach vorne kommt.