---
layout: "image"
title: "Blick auf die Unterseite"
date: "2017-06-19T19:47:06"
picture: "spinne04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/45975
imported:
- "2019"
_4images_image_id: "45975"
_4images_cat_id: "3417"
_4images_user_id: "104"
_4images_image_date: "2017-06-19T19:47:06"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45975 -->
So sieht das Teil von unten aus. Die 8 Antriebe sind alle genau gleich aufgebaut.