---
layout: "image"
title: "Von vorne"
date: "2007-04-26T15:36:22"
picture: "humanoid3.jpg"
weight: "4"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/10170
imported:
- "2019"
_4images_image_id: "10170"
_4images_cat_id: "920"
_4images_user_id: "445"
_4images_image_date: "2007-04-26T15:36:22"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10170 -->
Die Schwarzen Platten sind jetzt gelb und etwas kleiner...