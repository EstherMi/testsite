---
layout: "image"
title: "Von der Seite"
date: "2007-04-26T15:36:22"
picture: "humanoid2.jpg"
weight: "3"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/10169
imported:
- "2019"
_4images_image_id: "10169"
_4images_cat_id: "920"
_4images_user_id: "445"
_4images_image_date: "2007-04-26T15:36:22"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10169 -->
Die Zwei Achsen wurden durch ein Lagerstück 1+2 ersetzt.