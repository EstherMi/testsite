---
layout: "image"
title: "Funktionsweise"
date: "2007-04-26T15:36:22"
picture: "humanoid4.jpg"
weight: "5"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/10171
imported:
- "2019"
_4images_image_id: "10171"
_4images_cat_id: "920"
_4images_user_id: "445"
_4images_image_date: "2007-04-26T15:36:22"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10171 -->
Der untere Beinteil wird durch eine Verbindung zwischen ihm und dem Rad bewegt. Damit eine Bewegung entsteht, muss die Verbindung irgendwo einen Drehpunkt haben, der aber die Verbindung nicht festhält.