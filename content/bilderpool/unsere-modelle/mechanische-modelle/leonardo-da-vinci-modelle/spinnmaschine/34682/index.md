---
layout: "image"
title: "Spinnmaschine von Leonardo da Vinci"
date: "2012-03-25T13:03:14"
picture: "Spinnmaschine_Leonardo.jpg"
weight: "1"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/34682
imported:
- "2019"
_4images_image_id: "34682"
_4images_cat_id: "2560"
_4images_user_id: "1126"
_4images_image_date: "2012-03-25T13:03:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34682 -->
Diese von Leonardo da Vinci Ende des 15. Jahrhunderts entworfene Spinnmaschine war ein sehr komplexer Mechanismus: Angetrieben von einer einfachen Handkurbel drehten sich Spindel und Schnecke, und gleichzeitig wurde die Spindel von einem raffinierten Getriebe hin- und herbewegt. In weiteren Zeichnungen hat er bis zu vier dieser Spinndel-Antriebe zu einer großen Maschine kombiniert. Auf diesen Entwürfen basierten die ersten mechanischen Spinnmaschinen in England - dreihundert Jahre später.