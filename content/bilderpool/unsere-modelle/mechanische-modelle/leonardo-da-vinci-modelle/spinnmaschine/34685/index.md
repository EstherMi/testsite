---
layout: "image"
title: "Rückansicht Kronrad und Triebstöcke"
date: "2012-03-25T14:40:07"
picture: "Rckansicht_Kronrad_und_Triebstcke.jpg"
weight: "4"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/34685
imported:
- "2019"
_4images_image_id: "34685"
_4images_cat_id: "2560"
_4images_user_id: "1126"
_4images_image_date: "2012-03-25T14:40:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34685 -->
Nachdem die fünf Zapfen die Achse mit den Triebstöcken um ca. 330° gedreht haben, greifen die Zapfen auf der anderen Seite in die Triebstöcke und drehen die Achse um 330° zurück - damit dreht sich die Achse mit den Triebstöcken je Kurbelumdrehnung einmal in die eine und einmal in die andere Richtung. Eine sehr elegante mechanische Lösung - z.B. für die Bewegung des Stempels in Elmars Stempelmaschine (http://www.ftcommunity.de/details.php?image_id=34262).