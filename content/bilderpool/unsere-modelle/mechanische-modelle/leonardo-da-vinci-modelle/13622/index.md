---
layout: "image"
title: "Originalskizzen Leonardo Da Vinci"
date: "2008-02-09T13:45:48"
picture: "Skizzen.jpg"
weight: "6"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- details/13622
imported:
- "2019"
_4images_image_id: "13622"
_4images_cat_id: "1252"
_4images_user_id: "731"
_4images_image_date: "2008-02-09T13:45:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13622 -->
Dies sind die Originalskizzen von Leonardo Da Vinci von seinen beiden Sichelwägen