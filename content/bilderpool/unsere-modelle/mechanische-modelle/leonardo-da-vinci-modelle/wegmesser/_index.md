---
layout: "overview"
title: "Wegmesser"
date: 2019-12-17T19:22:25+01:00
legacy_id:
- categories/2559
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2559 --> 
Von Leonardo da Vinci entwickelter Wegmesser (zur Vermessung von Siedlungen)