---
layout: "image"
title: "Neuer alter Kompressor :-)"
date: "2011-05-08T11:43:57"
picture: "Neuer_Kompressor1.jpg"
weight: "2"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- details/30537
imported:
- "2019"
_4images_image_id: "30537"
_4images_cat_id: "613"
_4images_user_id: "426"
_4images_image_date: "2011-05-08T11:43:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30537 -->
durch zufall bei E-Bay entdeckt und zum Schnäppchenpreis von 20,-€ ersteigert.
Und noch dazu in einem 1A zustand, nun kann ich meine alte Kompressorstation abbauen.