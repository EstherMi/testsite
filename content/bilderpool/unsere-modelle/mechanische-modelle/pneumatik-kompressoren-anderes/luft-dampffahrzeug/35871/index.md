---
layout: "image"
title: "Übersicht"
date: "2012-10-10T15:13:52"
picture: "luft2.jpg"
weight: "2"
konstrukteure: 
- "Fredy"
fotografen:
- "Fredy"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/35871
imported:
- "2019"
_4images_image_id: "35871"
_4images_cat_id: "2679"
_4images_user_id: "453"
_4images_image_date: "2012-10-10T15:13:52"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35871 -->
