---
layout: "image"
title: "kompressor2.jpg"
date: "2010-04-07T12:40:31"
picture: "kompressor2.jpg"
weight: "2"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- details/26884
imported:
- "2019"
_4images_image_id: "26884"
_4images_cat_id: "1926"
_4images_user_id: "1112"
_4images_image_date: "2010-04-07T12:40:31"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26884 -->
