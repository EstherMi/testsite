---
layout: "overview"
title: "Kompressor (uhen)"
date: 2019-12-17T19:17:43+01:00
legacy_id:
- categories/1926
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1926 --> 
Ich habe hier einen Standard Motor modifiziert. Ich habe die Schnecke vorsichtig mit einer Mini-Trennscheibe gelöst und durch ein Zahnrad ersetzt. Die Kraftübertragung ist beeindruckend. Die Luftmenge reicht aus, um ein \"Zylinder Kolben Pneumatik Motor\" zu betreiben.