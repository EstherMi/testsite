---
layout: "image"
title: "Kompressor 4"
date: "2010-04-07T12:40:31"
picture: "kompressor6.jpg"
weight: "6"
konstrukteure: 
- "Udo Henkel"
fotografen:
- "uhen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- details/26888
imported:
- "2019"
_4images_image_id: "26888"
_4images_cat_id: "1926"
_4images_user_id: "1112"
_4images_image_date: "2010-04-07T12:40:31"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26888 -->
Hier sieht man das Zahnrad für die ersetzte Schnecke gut.