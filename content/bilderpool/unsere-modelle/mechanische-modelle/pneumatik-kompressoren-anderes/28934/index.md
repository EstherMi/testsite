---
layout: "image"
title: "Antrieb des Drehtisches"
date: "2010-10-06T17:12:55"
picture: "Antrieb.jpg"
weight: "6"
konstrukteure: 
- "werner"
fotografen:
- "werner"
keywords: ["Drehtisch", "500883"]
uploadBy: "werner"
license: "unknown"
legacy_id:
- details/28934
imported:
- "2019"
_4images_image_id: "28934"
_4images_cat_id: "613"
_4images_user_id: "1196"
_4images_image_date: "2010-10-06T17:12:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28934 -->
Zunächst nur zum Verlinken