---
layout: "image"
title: "Kompressoranhänger 1"
date: "2009-11-20T17:55:37"
picture: "kompressoranhaenger1.jpg"
weight: "1"
konstrukteure: 
- "Michael W."
fotografen:
- "Michael W."
keywords: ["Kompressor", "Pneumatik", "TST", "Betätiger", "Anhänger"]
uploadBy: "Arsamenes"
license: "unknown"
legacy_id:
- details/25805
imported:
- "2019"
_4images_image_id: "25805"
_4images_cat_id: "1810"
_4images_user_id: "1027"
_4images_image_date: "2009-11-20T17:55:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25805 -->
Eines meiner Lieblingsmodelle ist der Bausatz "Kompressoranhänger" von 1984. Leider passt dieser maßstäblich nicht so recht zu den aktuellen Modellen. Ich habe daher eine kleinere Alternative gebaut, die wie ich finde gut zu den Baumaschinen aus Profi Pneumatic II passt (der bei diesen Modellen hinten angebrachte Kompressor ist meiner Meinung nach nicht so schön).