---
layout: "image"
title: "Kompressor vorn Gesamtansicht"
date: "2015-02-08T12:54:27"
picture: "IMG_0009.jpg"
weight: "4"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: ["Kompressor", "Manometer", "Druckschalter"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/40461
imported:
- "2019"
_4images_image_id: "40461"
_4images_cat_id: "3035"
_4images_user_id: "1359"
_4images_image_date: "2015-02-08T12:54:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40461 -->
Der Kompressor in Gesamtansicht. Bestehend aus Druckschalter (irgendwo bestellt), Manometer, 2 Tanks aus dem KFZ-Bereich (Ebay) und einem Membran-Kompressor (aus einem ausgeschlachtetem Blutdruckmonitor; er kann auch Vakkum ziehen (ca. -0,8 bar), und liefert eigentlich ganz gut Durchfluss. Betrieb mit 12V-Netzteil. das Netzteil steht unten drunter und wurde während meiner Ausbildung von mir selbst gebaut.