---
layout: "image"
title: "Manometer und Druckschalter"
date: "2015-02-08T12:54:27"
picture: "IMG_0010.jpg"
weight: "6"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/40463
imported:
- "2019"
_4images_image_id: "40463"
_4images_cat_id: "3035"
_4images_user_id: "1359"
_4images_image_date: "2015-02-08T12:54:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40463 -->
Der Druckschalter unterbricht direkt die Spannungsversorgung des Kompressors und ist sehr fein einstellbar. Ich hatte den Druckschalter irgendwo mal bestellt, kann leider im Moment aber nicht nachvollziehen, wo genau.