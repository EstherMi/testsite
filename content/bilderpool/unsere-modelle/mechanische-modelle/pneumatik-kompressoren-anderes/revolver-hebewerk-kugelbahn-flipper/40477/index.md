---
layout: "image"
title: "Schritt 2"
date: "2015-02-08T12:54:27"
picture: "IMG_0072.jpg"
weight: "17"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/40477
imported:
- "2019"
_4images_image_id: "40477"
_4images_cat_id: "3035"
_4images_user_id: "1359"
_4images_image_date: "2015-02-08T12:54:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40477 -->
die Kugel wird Richtung Blockier-Mechanismus angehoben