---
layout: "image"
title: "Flipper gesamt"
date: "2015-02-08T12:54:27"
picture: "IMG_0006.jpg"
weight: "1"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/40458
imported:
- "2019"
_4images_image_id: "40458"
_4images_cat_id: "3035"
_4images_user_id: "1359"
_4images_image_date: "2015-02-08T12:54:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40458 -->
Wir haben den anhand von Prospekt-Fotos mal mit vorhandenem Material nachkonstruiert.
Die Lampen blinken durch den Taktgeber, ansonsten gibt es keine Sonder-Funktionen. Spiel-Spass ist trotzdem genug vorhanden ;-)