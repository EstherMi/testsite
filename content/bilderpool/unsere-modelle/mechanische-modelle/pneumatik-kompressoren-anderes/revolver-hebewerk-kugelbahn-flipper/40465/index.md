---
layout: "image"
title: "Rovolver - Hebewerk Gesamt"
date: "2015-02-08T12:54:27"
picture: "IMG_0048.jpg"
weight: "8"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: ["Kugelbahn", "Revolver-Hebewerk", "Kugel-Förderer"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/40465
imported:
- "2019"
_4images_image_id: "40465"
_4images_cat_id: "3035"
_4images_user_id: "1359"
_4images_image_date: "2015-02-08T12:54:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40465 -->
Das Hebewerk kann in sehr schneller Folge durch einen Revolver-Lader die Kugeln nach oben transportieren. Das Ganze wird durch einen Zylinder angetrieben. die Kugeln werden oben einzeln ausgeworfen und rollen dann über die 2 Flex-Schienen zurück in den Revolver.