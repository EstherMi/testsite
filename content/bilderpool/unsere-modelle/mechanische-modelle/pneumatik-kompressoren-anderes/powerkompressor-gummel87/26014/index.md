---
layout: "image"
title: "Powerkompressor"
date: "2010-01-06T12:06:43"
picture: "powerkompressor1.jpg"
weight: "1"
konstrukteure: 
- "gummel97"
fotografen:
- "gummel97"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "gummel97"
license: "unknown"
legacy_id:
- details/26014
imported:
- "2019"
_4images_image_id: "26014"
_4images_cat_id: "1835"
_4images_user_id: "1052"
_4images_image_date: "2010-01-06T12:06:43"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26014 -->
Kompakter, starker Kompressor mit Powermotor für anspruchsvolle Modelle.