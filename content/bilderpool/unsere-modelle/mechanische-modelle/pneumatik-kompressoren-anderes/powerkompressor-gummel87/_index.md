---
layout: "overview"
title: "Powerkompressor (gummel87)"
date: 2019-12-17T19:17:38+01:00
legacy_id:
- categories/1835
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1835 --> 
Kompakter, starker Kompressor mit Powermotor für anspruchsvolle Modelle.