---
layout: "image"
title: "ft-Arm mit Hand Gesamt"
date: "2013-03-07T13:30:37"
picture: "bild01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36713
imported:
- "2019"
_4images_image_id: "36713"
_4images_cat_id: "2723"
_4images_user_id: "1624"
_4images_image_date: "2013-03-07T13:30:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36713 -->
Diesen Arm habe ich mal für den Tag der offenen Tür meiner Schule gebaut. Es stellt den menschlichen Ellenbogen dar, der Rest ist unbeweglich.