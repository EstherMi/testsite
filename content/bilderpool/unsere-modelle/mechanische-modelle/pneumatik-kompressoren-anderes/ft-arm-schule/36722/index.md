---
layout: "image"
title: "ft-Arm Beschriftung"
date: "2013-03-07T13:30:38"
picture: "bild10.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36722
imported:
- "2019"
_4images_image_id: "36722"
_4images_cat_id: "2723"
_4images_user_id: "1624"
_4images_image_date: "2013-03-07T13:30:38"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36722 -->
Dieses Bild habe ich auf A4 ausgedruckt, laminiert und über dem Arm aufgehängt.