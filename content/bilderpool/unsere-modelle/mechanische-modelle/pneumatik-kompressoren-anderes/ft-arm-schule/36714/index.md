---
layout: "image"
title: "ft-Arm Stellung unten"
date: "2013-03-07T13:30:37"
picture: "bild02.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36714
imported:
- "2019"
_4images_image_id: "36714"
_4images_cat_id: "2723"
_4images_user_id: "1624"
_4images_image_date: "2013-03-07T13:30:37"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36714 -->
Ohne Hand, in der unteren Stellung.