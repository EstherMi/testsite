---
layout: "image"
title: "ft-Arm Durcheinander"
date: "2013-03-07T13:30:38"
picture: "bild09.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36721
imported:
- "2019"
_4images_image_id: "36721"
_4images_cat_id: "2723"
_4images_user_id: "1624"
_4images_image_date: "2013-03-07T13:30:38"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36721 -->
Ein *äußerst platzsparendes* Gehäuse, in dem sich zwei Magnetventile und ganz viele Kabel und Schläuche befinden.