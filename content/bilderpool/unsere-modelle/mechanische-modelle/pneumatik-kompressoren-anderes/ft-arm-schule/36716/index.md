---
layout: "image"
title: "ft-Arm mit Hand 1"
date: "2013-03-07T13:30:37"
picture: "bild04.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36716
imported:
- "2019"
_4images_image_id: "36716"
_4images_cat_id: "2723"
_4images_user_id: "1624"
_4images_image_date: "2013-03-07T13:30:37"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36716 -->
Mit Hand in der unteren Stellung