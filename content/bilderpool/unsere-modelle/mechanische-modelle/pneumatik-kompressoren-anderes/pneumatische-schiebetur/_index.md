---
layout: "overview"
title: "Pneumatische Schiebetür"
date: 2019-12-17T19:17:45+01:00
legacy_id:
- categories/1947
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1947 --> 
Die Schiebetür pneumatische Schiebetür wird über ein Robo Interface gesteuert. Die Tür öffnet sich wenn eine Lichtschranke unterbrochen wird. Nach einer bestimmten Zeit schließt sich die Tür wieder. Steht dabei ein Hindernis im Weg springt die Tür sofort wieder auf.