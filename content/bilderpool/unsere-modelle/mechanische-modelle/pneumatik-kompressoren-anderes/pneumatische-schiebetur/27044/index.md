---
layout: "image"
title: "Schiebetür von hinten"
date: "2010-05-02T22:08:20"
picture: "schiebetuer4.jpg"
weight: "5"
konstrukteure: 
- "fish"
fotografen:
- "fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- details/27044
imported:
- "2019"
_4images_image_id: "27044"
_4images_cat_id: "1947"
_4images_user_id: "1113"
_4images_image_date: "2010-05-02T22:08:20"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27044 -->
In der Mitte sieht man die Ventile, rechts davon den Luftspeicher und daneben den Kompressor.