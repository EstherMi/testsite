---
layout: "image"
title: "Getriebe"
date: "2013-04-29T18:39:19"
picture: "druckluftballbalancierer3.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/36875
imported:
- "2019"
_4images_image_id: "36875"
_4images_cat_id: "2740"
_4images_user_id: "104"
_4images_image_date: "2013-04-29T18:39:19"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36875 -->
Die Segmentscheibe dreht also zu einem Teil der Zeit am Z40, das über ein Z20 au das rechte Z40 geht und somit die Düse (hier: nach links) verdreht. Im Hintergrund sieht man ein Gummi, was die Düse beim Ausdrehen der Segmentscheibe aus dem Z40 sofort wieder zurück in die Senkrechte stellt.