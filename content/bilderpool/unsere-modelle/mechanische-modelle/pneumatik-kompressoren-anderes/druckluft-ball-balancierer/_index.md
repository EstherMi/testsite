---
layout: "overview"
title: "Druckluft-Ball-Balancierer"
date: 2019-12-17T19:17:57+01:00
legacy_id:
- categories/2740
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2740 --> 
Angeregt durch ein Festo-Modell im Technikmuseum Sinsheim, das einen Tischtennisball auf einem senkrecht nach oben zeigenden Luftstrahl balancierend transportierte, entstand dieses Modell.