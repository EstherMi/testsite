---
layout: "image"
title: "Vorderansicht"
date: "2013-04-29T18:39:19"
picture: "druckluftballbalancierer1.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/36873
imported:
- "2019"
_4images_image_id: "36873"
_4images_cat_id: "2740"
_4images_user_id: "104"
_4images_image_date: "2013-04-29T18:39:19"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36873 -->
Durch die blaue Pneumatikdüse rechts im Bild wird ordentlich Druckluft geleitet. Der blaue ft-Kompressor reicht dafür leider nicht aus, hier musste mein Kompressor mit Maximaldruck 3,5 bar und Durchfluss 15 L/min ran. Obendrauf legt man einen Tischtennisball. Der Luftstrom drückt ihn nicht nur einfach hoch, sondern er umströmt den Ball ja allseitig. Das ergibt den Coanda-Effekt: Die strömende Luft saugt sich am Ball fest. Immer, wenn er zur Seite wegfallen möchte, sorgt der dann stärkere Luftstrom an der gegenüberliegenden Seite dafür, dass er zurückgezogen wird. Der Ball tanzt also auf dem Luftstrahl hin und her und schwingt auf und ab, aber er fällt nicht herunter.

Den Coanda-Effekt kennt wohl jedermann vom Waschbecken: Man kann seine Hand senkrecht unter einen Strahl halten und diesen durch die Saugwirkung der Strömung des Wassers über der Haut durch verdrehen der Hand ablenken.