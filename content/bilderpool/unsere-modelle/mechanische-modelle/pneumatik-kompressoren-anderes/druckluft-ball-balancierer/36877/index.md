---
layout: "image"
title: "Momentaufnahme 1"
date: "2013-04-29T18:39:19"
picture: "druckluftballbalancierer5.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/36877
imported:
- "2019"
_4images_image_id: "36877"
_4images_cat_id: "2740"
_4images_user_id: "104"
_4images_image_date: "2013-04-29T18:39:19"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36877 -->
Hier sieht man den Ball frei in der Luft schweben. Die Düse hat gerade begonnen, sich nach links zu drehen.