---
layout: "image"
title: "Ventilantrieb (2)"
date: "2011-01-24T19:23:40"
picture: "ventilantrieb2.jpg"
weight: "2"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/29792
imported:
- "2019"
_4images_image_id: "29792"
_4images_cat_id: "2190"
_4images_user_id: "729"
_4images_image_date: "2011-01-24T19:23:40"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29792 -->
Ansicht