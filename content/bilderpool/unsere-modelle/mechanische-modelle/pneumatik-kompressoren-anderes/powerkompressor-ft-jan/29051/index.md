---
layout: "image"
title: "Gesamtansicht"
date: "2010-10-25T19:15:03"
picture: "pkomp1.jpg"
weight: "1"
konstrukteure: 
- "Jan Hils"
fotografen:
- "Jan Hils"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-Jan"
license: "unknown"
legacy_id:
- details/29051
imported:
- "2019"
_4images_image_id: "29051"
_4images_cat_id: "2111"
_4images_user_id: "1181"
_4images_image_date: "2010-10-25T19:15:03"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29051 -->
Hier sieht man den kompletten Kompressor
Ich habe ihn extra auf die kleine Platte gebaut da er nur umständlich auf das Raster der anderen Platten passen würde