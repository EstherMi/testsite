---
layout: "image"
title: "Die Kraftübertragung"
date: "2010-10-25T19:15:03"
picture: "pkomp2.jpg"
weight: "2"
konstrukteure: 
- "Jan Hils"
fotografen:
- "Jan Hils"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-Jan"
license: "unknown"
legacy_id:
- details/29052
imported:
- "2019"
_4images_image_id: "29052"
_4images_cat_id: "2111"
_4images_user_id: "1181"
_4images_image_date: "2010-10-25T19:15:03"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29052 -->
Hier sieht man die die 1:1 über setzung vom Powermotor (8:1) auf die Kurbelwelle