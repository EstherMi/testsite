---
layout: "image"
title: "2 - PneumaCube geöffnet"
date: "2009-05-08T23:35:29"
picture: "2_-_PneumaCube_geffnet_2.jpg"
weight: "2"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/23938
imported:
- "2019"
_4images_image_id: "23938"
_4images_cat_id: "1643"
_4images_user_id: "724"
_4images_image_date: "2009-05-08T23:35:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23938 -->
Pneumatisch öffnen sich nach und nach die 5 Seiten des Würfels. 
Das war vielleicht ne Tüfteley... Bestimmt 10 Versuche habe ich gebraucht, bis es endlich geklappt hat. Es gibt nur exakt 1 Möglichkeit, wie man die Pneumatik-Ventile so befestigen kann, daß man 90° klappen kann.
Für das Heben des "Doppelarms" brauche ich 1 bar!!! Mein Kompressor ist der Pari Inhalierboy. Bei einer Zylinderfläche von etwa 1 cm² sind das 1 kg, die aufgrund des kurzen Hebelarms nötig sind!!!