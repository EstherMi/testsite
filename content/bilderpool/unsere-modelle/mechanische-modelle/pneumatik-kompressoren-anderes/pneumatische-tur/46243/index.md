---
layout: "image"
title: "Frontansicht"
date: "2017-09-17T18:02:22"
picture: "Frontansicht_community.jpg"
weight: "2"
konstrukteure: 
- "Alwin"
fotografen:
- "Alwin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischertechniker"
license: "unknown"
legacy_id:
- details/46243
imported:
- "2019"
_4images_image_id: "46243"
_4images_cat_id: "3430"
_4images_user_id: "2770"
_4images_image_date: "2017-09-17T18:02:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46243 -->
Hier sieht man die pneumatische Tür von vorne