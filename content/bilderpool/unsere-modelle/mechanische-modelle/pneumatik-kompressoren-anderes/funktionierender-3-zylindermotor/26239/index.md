---
layout: "image"
title: "Kompressor 1"
date: "2010-02-08T23:27:50"
picture: "funktionierenderzylindermotor05.jpg"
weight: "5"
konstrukteure: 
- "gummel97"
fotografen:
- "gummel97"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "gummel97"
license: "unknown"
legacy_id:
- details/26239
imported:
- "2019"
_4images_image_id: "26239"
_4images_cat_id: "1868"
_4images_user_id: "1052"
_4images_image_date: "2010-02-08T23:27:50"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26239 -->
Die 2 Kurbeln laufen Parallel um zu verhindern dass der eine Zylinder die luft in den anderen pumpt.