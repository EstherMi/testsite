---
layout: "image"
title: "Pleuelstange von der neuen Version"
date: "2010-02-08T23:28:06"
picture: "funktionierenderzylindermotor15.jpg"
weight: "15"
konstrukteure: 
- "gummel97"
fotografen:
- "gummel97"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "gummel97"
license: "unknown"
legacy_id:
- details/26249
imported:
- "2019"
_4images_image_id: "26249"
_4images_cat_id: "1868"
_4images_user_id: "1052"
_4images_image_date: "2010-02-08T23:28:06"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26249 -->
