---
layout: "image"
title: "ein kleiner Film..."
date: "2010-02-08T23:28:06"
picture: "funktionierenderzylindermotor11.jpg"
weight: "11"
konstrukteure: 
- "gummel97"
fotografen:
- "gummel97"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "gummel97"
license: "unknown"
legacy_id:
- details/26245
imported:
- "2019"
_4images_image_id: "26245"
_4images_cat_id: "1868"
_4images_user_id: "1052"
_4images_image_date: "2010-02-08T23:28:06"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26245 -->
die neue version kann man im speedmode und im slowmo laufen lassen, daher 2 Filme