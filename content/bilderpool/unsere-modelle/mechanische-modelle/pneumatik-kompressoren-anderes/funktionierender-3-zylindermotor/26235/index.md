---
layout: "image"
title: "Draufsicht"
date: "2010-02-08T23:27:49"
picture: "funktionierenderzylindermotor01.jpg"
weight: "1"
konstrukteure: 
- "gummel97"
fotografen:
- "gummel97"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "gummel97"
license: "unknown"
legacy_id:
- details/26235
imported:
- "2019"
_4images_image_id: "26235"
_4images_cat_id: "1868"
_4images_user_id: "1052"
_4images_image_date: "2010-02-08T23:27:49"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26235 -->
Version 1