---
layout: "overview"
title: "3-Zylinder Druckluftmotor"
date: 2019-12-17T19:17:39+01:00
legacy_id:
- categories/2515
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2515 --> 
Hier das Modell eines 3-Zylinder Druckluftmotors, dessen Zylinder über einen Verteiler, ganz links, und Magnetventile, im Vordergrund, gesteuert werden. Als Druckluftquelle benutze ich eine sehr kräftige Aquarienpumpe, wie sie von Profis zum gleichzeitigen Belüften mehrerer Aquarien eingesetzt wird. Der Motor läuft nur dann selbständig an, wenn eine der drei Kontrolllampen leuchtet, ansonsten muss er angeworfen werden. Man hätte hier auch noch einen elektrischen Anlasser einbauen können. Ein Video, das den Motor in Betrieb zeigt, kann in Youtube unter "fischertechnik 3ZylDruckluftmotor.MTS" angesehen werden. 