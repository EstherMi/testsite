---
layout: "image"
title: "Pleuelstange"
date: "2010-02-08T23:27:50"
picture: "funktionierenderzylindermotor03.jpg"
weight: "3"
konstrukteure: 
- "gummel97"
fotografen:
- "gummel97"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "gummel97"
license: "unknown"
legacy_id:
- details/26237
imported:
- "2019"
_4images_image_id: "26237"
_4images_cat_id: "1868"
_4images_user_id: "1052"
_4images_image_date: "2010-02-08T23:27:50"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26237 -->
Version 1