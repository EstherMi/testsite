---
layout: "image"
title: "Kompressor PICT5008"
date: "2011-01-09T15:18:50"
picture: "doppelkolbenpowerkompressorknarfbokaj4.jpg"
weight: "5"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/29643
imported:
- "2019"
_4images_image_id: "29643"
_4images_cat_id: "2169"
_4images_user_id: "729"
_4images_image_date: "2011-01-09T15:18:50"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29643 -->
Ansicht hinten links