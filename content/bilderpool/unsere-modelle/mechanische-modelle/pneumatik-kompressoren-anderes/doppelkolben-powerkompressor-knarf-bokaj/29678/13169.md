---
layout: "comment"
hidden: true
title: "13169"
date: "2011-01-12T22:18:32"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Frank,
dein Kompressor ist wirklich eine "Artenbereicherung". Vergibst du auch Nachbaulizenzen?
Warum aber diese systemfremde Pilotventilfeder? Fehlten dir etwa P-Betätiger und ft-Druckfeder?
Gruß, Ingo