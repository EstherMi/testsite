---
layout: "overview"
title: "Doppelkolben-Powerkompressor (Knarf Bokaj)"
date: 2019-12-17T19:17:47+01:00
legacy_id:
- categories/2169
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2169 --> 
Doppelkolben Kompressor mit 8:1 Powermotor und Endabschaltung auf 120*60 Grundplatte.