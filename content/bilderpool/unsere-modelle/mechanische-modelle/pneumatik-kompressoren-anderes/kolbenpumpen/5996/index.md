---
layout: "image"
title: "Kompressor_alt_2.jpg"
date: "2006-03-30T07:52:34"
picture: "dsc00618_resize.jpg"
weight: "5"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich (schnaggels)"
keywords: ["manometer", "pumpe", "kompressor", "pneumatik"]
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- details/5996
imported:
- "2019"
_4images_image_id: "5996"
_4images_cat_id: "520"
_4images_user_id: "120"
_4images_image_date: "2006-03-30T07:52:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5996 -->
In späteren Versionen wurde das Chassis durch den Kurbelwellen Antrieb ersetzt. Die Übersetzung ist etwas geringer, was den Motor noch stärker belastet. Der große Hub der Kolben im Kompressorzylinder wirkt sich aber sehr positiv auf den Druck aus :)