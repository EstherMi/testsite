---
layout: "image"
title: "Kompressor_neu_1.jpg"
date: "2006-03-30T07:52:28"
picture: "dsc00608_resize.jpg"
weight: "1"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich (schnaggels)"
keywords: ["manometer", "pumpe", "kompressor", "pneumatik"]
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- details/5992
imported:
- "2019"
_4images_image_id: "5992"
_4images_cat_id: "520"
_4images_user_id: "120"
_4images_image_date: "2006-03-30T07:52:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5992 -->
Original FT-Kompressor aus Pneumatic Robots oder Pneumatik I + II schafft etwa 0,3 bar, 2 Lufttanks sind ratsam.