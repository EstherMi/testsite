---
layout: "comment"
hidden: true
title: "964"
date: "2006-04-02T16:33:08"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Ich habe dasselbe erreicht, indem ich die Propellernabe flach abgeschnitten habe, aber das Aufbohren der Seilrollen hat was!


Ich habe aber noch eine Anmerkung zum Wirkungsgrad bei diesem Aufbau: ich würde dafür sorgen, dass der Kolben möglichst weit eingedrückt wird (also bis kurz vor dem Punkt, wo er beim Komprimieren am Boden aufschlägt). Nur dann hat es sich gelohnt, die Luft zusammenzudrücken. Wenn man mehr Platz lässt, wird die Luft (mit entsprechendem Aufwand an physikalischer Arbeit) zusammengedrückt, nur um hinter dem unteren Totpunkt die Schwungscheibe zu beschleunigen. Dieser Aufwand ist verschenkt. 

Ich würde also die BS7,5 weglassen und die BS5 etwas aus der Bildebene heraus schieben, gerade soweit, dass der Kolben am unteren Ende des Arbeitsweges nicht aufschlägt.