---
layout: "image"
title: "Ventilinsel (1)"
date: "2011-01-22T16:48:48"
picture: "ventilinselfrankjakob1.jpg"
weight: "1"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/29740
imported:
- "2019"
_4images_image_id: "29740"
_4images_cat_id: "2185"
_4images_user_id: "729"
_4images_image_date: "2011-01-22T16:48:48"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29740 -->
Ansicht von vorne links