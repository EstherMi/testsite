---
layout: "image"
title: "Ventilinsel (5)"
date: "2011-01-22T16:48:49"
picture: "ventilinselfrankjakob5.jpg"
weight: "5"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/29744
imported:
- "2019"
_4images_image_id: "29744"
_4images_cat_id: "2185"
_4images_user_id: "729"
_4images_image_date: "2011-01-22T16:48:49"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29744 -->
Ansicht von unten auf die I/O-Extension