---
layout: "comment"
hidden: true
title: "13305"
date: "2011-01-22T20:13:38"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Herrlich finde ich die versetzten Ebenen der Zahnräder! Man anderer hätte die einfach nebeneinander (und damit breiter) gebaut. Gefällt mir gut!

Gruß, Thomas