---
layout: "comment"
hidden: true
title: "13310"
date: "2011-01-22T21:14:54"
uploadBy:
- "Knarf Bokaj"
license: "unknown"
imported:
- "2019"
---
Danke Thomas

Das ist der Entwicklung geschuldet. Das Handventil hatte ich erst 1:1
angetrieben, dann 2:1 und jetzt 3:1. So wurde alles immer größer und
breiter. Der Plan war alles auf eine kleine Bauplatte oder I/O-Erweiterung
unterzubringen. 

Gruß,
Frank