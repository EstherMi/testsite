---
layout: "image"
title: "Ungleichförmig übersetzende Getriebe"
date: "2015-10-03T16:23:03"
picture: "druckluftballbalancierermitduesen4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42034
imported:
- "2019"
_4images_image_id: "42034"
_4images_cat_id: "3125"
_4images_user_id: "104"
_4images_image_date: "2015-10-03T16:23:03"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42034 -->
Nennen wir die Zeit, in der ein Ball sowohl von einer Düse auf die zweite als auch wieder zurück übergeben wird, 100%. Dann muss die Bewegung einer einzelnen Düse in weniger als 50% vonstattengehen. Die übernehmende Düse muss noch hinreichend lange stillstehend warten, bis die ankommende Düse zurückgeschnalzt ist (wie das geschieht, sehen wir noch) und bis der Ball sich hinreichend auf der neuen Düse stabilisiert hat.

Nach einem Blick ins hobby-1-Begleitbuch Band 2 ab Seite 48 ("Umlaufende Kurbelscheibe") war wieder alles klar. In diesem Modell wird, da sich der Motor einfach konstant in einer Richtung dreht, nur die Drehmomentübertragung in eine Drehrichtung (im Uhrzeigersinn nämlich) benötigt.

Auf den unteren, sich gleichmäßig schnell drehenden Drehscheiben ist ein kleines Rad befestigt und mit der S-Strebe gegen herausrutschen gesichert. Die oberen beiden Drehscheiben, deren Achsen zu den Düsen führen, sind exzentrisch dazu angeordnet. Sie tragen auf ihrer Unterseite eine lange Platte, die von den Rädchen unten verdreht werden. Dreht das Rädchen nun auf der Innenseite dieses Bildes, wirkt ein langer Hebel, und die obere Drehscheibe dreht langsam. Liegt das Rädchen gerade auf der Außenseite, wirkt ein kurzer Hebel und die obere Drehscheibe dreht deutlich schneller.

Insgesamt drehen sich damit alle Drehscheiben gleich oft pro Zeiteinheit, aber die oberen drehen sich eben mit ungleichförmig Geschwindigkeit. Die schnelle Hälfte der Drehung ist die, die die Düsen oben auf die Reise schicken, und die andere Düse wartet geduldig, bis sie an der Reihe ist.

Alles ist richtig justiert, wenn die Rädchen beide genau in dieselbe Richtung zeigen, gesehen von den Drehachsen ihrer jeweiligen Z40 aus.