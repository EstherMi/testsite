---
layout: "image"
title: "Schrägstehende Drehachsen (2)"
date: "2015-10-03T16:23:03"
picture: "druckluftballbalancierermitduesen9.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42039
imported:
- "2019"
_4images_image_id: "42039"
_4images_cat_id: "3125"
_4images_user_id: "104"
_4images_image_date: "2015-10-03T16:23:03"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42039 -->
Die Düsen führen aber bei der Drehung eine schöne, weit ausladende Bewegung aus, um den Ball in einem großen Bogen zur anderen Düse zu bewegen, wo sie den Ball schließlich von der Seite übergeben. Der andere Luftstrahl erfasst den Ball, der ankommende Luftstrahl wird scharf abgedeckt, die eben angekommene Düse wird vom Rückstellgummi schnell in ihre Ausgangsposition bewegt, wo der Schlauch zur hinreichend genauen Positionierung an einem Anschlag landet. Erst dann (wegen des ungleichförmig übersetzenden Getriebes im Fuß des Modells) fängt die andere Düse an, sich zu bewegen.