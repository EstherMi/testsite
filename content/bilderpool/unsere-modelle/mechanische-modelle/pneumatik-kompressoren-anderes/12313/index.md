---
layout: "image"
title: "Pelamis4"
date: "2007-10-25T18:14:17"
picture: "Pelamis_0051.jpg"
weight: "12"
konstrukteure: 
- "Paulchen"
fotografen:
- "Paulchen"
keywords: ["Pelamis", "Seeschlange"]
uploadBy: "Paulchen"
license: "unknown"
legacy_id:
- details/12313
imported:
- "2019"
_4images_image_id: "12313"
_4images_cat_id: "613"
_4images_user_id: "599"
_4images_image_date: "2007-10-25T18:14:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12313 -->
