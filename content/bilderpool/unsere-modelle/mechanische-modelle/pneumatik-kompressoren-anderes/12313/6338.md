---
layout: "comment"
hidden: true
title: "6338"
date: "2008-04-27T21:19:02"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Schau mal hier (im ft-Forum gesucht nach "Pelamis") :
http://www.fischertechnik.de/de/fanclub/forum/default.aspx?g=posts&t=2178

Es wäre halt schön, wenn jeder zu seinen Fotos eine ordentliche Beschreibung verfasste, auf verwandte Themen und Threads im ft-Forum verlinkte und die Bilder mit aussagekräftigen Schlagwörtern für die Suche innerhalb der ftc versähe. Naja, man träumt halt so. Man muss ja schon dankbar sein, wenn die Bildnamen etwas über das Modell sagen.

Gruß,
Harald