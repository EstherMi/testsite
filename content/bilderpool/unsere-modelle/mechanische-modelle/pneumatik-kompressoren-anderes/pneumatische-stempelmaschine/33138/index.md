---
layout: "image"
title: "Hier werden die Siegel aufgestempelt"
date: "2011-10-13T20:13:30"
picture: "pneumatischestempelmaschine03.jpg"
weight: "3"
konstrukteure: 
- "FT-FAN"
fotografen:
- "FT-FAN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FT-Fan"
license: "unknown"
legacy_id:
- details/33138
imported:
- "2019"
_4images_image_id: "33138"
_4images_cat_id: "2450"
_4images_user_id: "1371"
_4images_image_date: "2011-10-13T20:13:30"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33138 -->
Die Werkstücke "bekommen" hier ein Siegel, welches auf die Bestandene Qualitätsprüfung hin weist.