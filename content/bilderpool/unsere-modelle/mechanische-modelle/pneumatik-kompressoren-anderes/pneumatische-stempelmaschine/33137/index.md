---
layout: "image"
title: "Der Kompressor"
date: "2011-10-13T20:13:30"
picture: "pneumatischestempelmaschine02.jpg"
weight: "2"
konstrukteure: 
- "FT-FAN"
fotografen:
- "FT-FAN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FT-Fan"
license: "unknown"
legacy_id:
- details/33137
imported:
- "2019"
_4images_image_id: "33137"
_4images_cat_id: "2450"
_4images_user_id: "1371"
_4images_image_date: "2011-10-13T20:13:30"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33137 -->
Der Kompressor, davor die Druckabschaltung.