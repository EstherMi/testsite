---
layout: "overview"
title: "Pneumatische Stempelmaschine"
date: 2019-12-17T19:17:53+01:00
legacy_id:
- categories/2450
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2450 --> 
Diese Maschine stempelt allen bereits vorsortierten Werkstücken ein Qualitätssiegel auf.