---
layout: "image"
title: "Druckabschaltung"
date: "2011-10-13T20:13:30"
picture: "pneumatischestempelmaschine10.jpg"
weight: "10"
konstrukteure: 
- "FT-FAN"
fotografen:
- "FT-FAN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FT-Fan"
license: "unknown"
legacy_id:
- details/33145
imported:
- "2019"
_4images_image_id: "33145"
_4images_cat_id: "2450"
_4images_user_id: "1371"
_4images_image_date: "2011-10-13T20:13:30"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33145 -->
Der Taster unterbricht die Stromleitung des Motor, (welche ständig Spannung hat) wenn er gedrückt wird.