---
layout: "image"
title: "Umgebauter Mini-Komp1"
date: "2007-01-21T14:05:17"
picture: "Umgebauter_Mini-Komp1.jpg"
weight: "6"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/8583
imported:
- "2019"
_4images_image_id: "8583"
_4images_cat_id: "578"
_4images_user_id: "456"
_4images_image_date: "2007-01-21T14:05:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8583 -->
Ich habe den Mini-Kompressor umgebaut, sodass er das doppelte Luftvolumen liefert. Alles mit original ft-Teilen. Man braut lediglich einen zweiten Kompressorzylinder und noch ein Rückschlagventil.
Hier ist das Ergebnis: