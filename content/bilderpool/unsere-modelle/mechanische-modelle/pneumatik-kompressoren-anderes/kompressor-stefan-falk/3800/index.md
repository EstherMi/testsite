---
layout: "image"
title: "Kompressor - Endabschaltung"
date: "2005-03-13T13:21:34"
picture: "Kompressor_005.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/3800
imported:
- "2019"
_4images_image_id: "3800"
_4images_cat_id: "578"
_4images_user_id: "104"
_4images_image_date: "2005-03-13T13:21:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3800 -->
Hier noch ein Detailblick auf die Druckabschaltung. Der selbstrückstellende Zylinder muss gegen vier Federfüße (aus dem alten Elektromechanik-Programm) andrücken, um den Taster für die Abschaltung des Motors zu drücken, wenn und solange genug Druck vorhanden ist.