---
layout: "image"
title: "Kompressor - Flip-Flop (1)"
date: "2005-03-13T13:21:34"
picture: "Kompressor_003.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/3798
imported:
- "2019"
_4images_image_id: "3798"
_4images_cat_id: "578"
_4images_user_id: "104"
_4images_image_date: "2005-03-13T13:21:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3798 -->
Die guten alten einfachen und doppelten Betätiger und die blauen (Öffner) und ein rotes (Schließer) Ventile steuern den Zylinder. Die Betätiger links und rechts vom schwenkbaren BS15 schieben einen halben Gelenkstein (das zweitneueste Modell) an ein anderes Ventil (im Bild hinten). Egal ob links oder rechts zuletzt "betätigt" wurde: Das Gelenk bleibt stehen, bis es wieder umgeschaltet wird. Das damit gesteuerte Ventil geht auf den Doppelbetätiger, der wiederum je ein blaues und ein rotes Ventil steuert. das bewirkt, dass immer genau einer der beiden Anschlüsse des letztlich gesteuerten Zylinders mit Druckluft beaufschlagt wird. (Für alle die, die die alten Pneumatikteile nie kennengelernt haben.)