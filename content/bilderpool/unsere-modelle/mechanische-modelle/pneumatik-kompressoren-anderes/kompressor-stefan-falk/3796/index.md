---
layout: "image"
title: "Kompressor"
date: "2005-03-13T13:21:34"
picture: "Kompressor_001.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/3796
imported:
- "2019"
_4images_image_id: "3796"
_4images_cat_id: "578"
_4images_user_id: "104"
_4images_image_date: "2005-03-13T13:21:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3796 -->
Ein Powermotor treibt zwei Zylinder gegentaktig an. Das ergibt ganz brauchbare Leistungen. Der Rest ist eine kleine Flip-Flop-Schaltung zum Lasttest: Kurzer Druck auf eines der beiden Ventile vorne schaltet den Zylinder auf "ausgefahren" oder "eingefahren".