---
layout: "image"
title: "Umgebauter Minikomp3"
date: "2007-01-21T14:05:17"
picture: "Umgebauter_Mini-Komp3.jpg"
weight: "8"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/8585
imported:
- "2019"
_4images_image_id: "8585"
_4images_cat_id: "578"
_4images_user_id: "456"
_4images_image_date: "2007-01-21T14:05:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8585 -->
