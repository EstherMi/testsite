---
layout: "image"
title: "Kompressorstation"
date: "2006-12-17T16:23:48"
picture: "Kompressorstation1.jpg"
weight: "11"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/7914
imported:
- "2019"
_4images_image_id: "7914"
_4images_cat_id: "578"
_4images_user_id: "456"
_4images_image_date: "2006-12-17T16:23:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7914 -->
Das ist eine Kompressorstation. Sie besteht aus:Mini-Kompressor, Kompressorabschaltung, 2 Magnetventilen und einem Zylinder.
Gebaut habe ich sie, weil man damit Zylinder und den Kompressor testen kann bevor man eine Maschine baut.