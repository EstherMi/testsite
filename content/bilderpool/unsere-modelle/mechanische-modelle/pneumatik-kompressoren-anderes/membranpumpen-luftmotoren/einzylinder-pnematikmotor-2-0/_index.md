---
layout: "overview"
title: "Einzylinder-Pnematikmotor 2.0"
date: 2019-12-17T19:17:13+01:00
legacy_id:
- categories/3173
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3173 --> 
Ziel war nach wie vor einen möglichst kompakten Pneumatikmotor zu konstruieren.

Motiviert von den tollen Motoren von Stefan Falk die durch das Abknicken von den Schläuchen gesteuert werden, probierte ich auch etwas mit dieser Ventilmethode herum.
Herrausgekommen ist das hier ;-)
Video: https://youtu.be/JPZpj14pj4w

Des weiteren lässt sich dieser Motor mit beliebig vielen Zylindern erweritern. Wie ich das vorhabe werdet ihr natürlich bald zu sehen bekommen ;-)