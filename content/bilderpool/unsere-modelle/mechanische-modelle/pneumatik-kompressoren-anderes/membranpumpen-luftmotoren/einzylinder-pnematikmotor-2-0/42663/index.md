---
layout: "image"
title: "Ventil 2"
date: "2016-01-03T14:41:06"
picture: "pneumatikmotor13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- details/42663
imported:
- "2019"
_4images_image_id: "42663"
_4images_cat_id: "3173"
_4images_user_id: "1924"
_4images_image_date: "2016-01-03T14:41:06"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42663 -->
Die Kurbelwelle ist um 180° gedreht und somit ist der Abluftschlauch (rechte Schlauchschleife) offen und der Drucklftschlauch (linke Schlauchschleife) abgeknickt.