---
layout: "image"
title: "Schwungrad Seite"
date: "2016-01-03T14:41:06"
picture: "pneumatikmotor05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- details/42655
imported:
- "2019"
_4images_image_id: "42655"
_4images_cat_id: "3173"
_4images_user_id: "1924"
_4images_image_date: "2016-01-03T14:41:06"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42655 -->
Wegen der Leichtgängigkeit des Motors reicht der Traktorreifen als Schwungmasse aus.