---
layout: "image"
title: "Gesamtansicht"
date: "2016-01-03T14:41:06"
picture: "pneumatikmotor01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- details/42651
imported:
- "2019"
_4images_image_id: "42651"
_4images_cat_id: "3173"
_4images_user_id: "1924"
_4images_image_date: "2016-01-03T14:41:06"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42651 -->
Ziel war nach wie vor einen möglichst kompakten Pneumatikmotor zu konstruieren.

Motiviert von den tollen Motoren von Stefan Falk die durch das Abknicken von den Schläuchen gesteuert werden, probierte ich auch etwas mit dieser Ventilmethode herum.
Herrausgekommen ist das hier ;-)
Video: https://youtu.be/JPZpj14pj4w

Des weiteren lässt sich dieser Motor mit beliebig vielen Zylindern erweritern. Wie ich das vorhabe werdet ihr natürlich bald zu sehen bekommen ;-)