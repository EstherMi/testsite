---
layout: "image"
title: "Umschalter und Handventil (Dampfmaschine)"
date: "2012-10-07T19:47:31"
picture: "dampfmaschinenmodellnachjameswattdirkfox8.jpg"
weight: "9"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/35833
imported:
- "2019"
_4images_image_id: "35833"
_4images_cat_id: "2676"
_4images_user_id: "1126"
_4images_image_date: "2012-10-07T19:47:31"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35833 -->
Hier sieht man das Handventil und den Gelenkbaustein ohne "Verkleidung". Der Baustein 7,5 wird von oben auf den Verbinder 15 geschoben und "klemmt" so das Handventil ein.