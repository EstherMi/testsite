---
layout: "comment"
hidden: true
title: "17405"
date: "2012-10-08T08:14:45"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Du hast offensichtlich keinerlei Maßnahmen unternommen, die Reibung im System (hauptsächlich an den Wellenlagern) gering zu halten. Das war damals bei meiner Dampfmaschine mit dem alten FT-Selbstbau-Kompressor das größte Problem - sonst lief sie nicht.

Liefert der neue Kompressor hier wirklich so viel "Bumms", dass man das getrost ignorieren kann?

Gruß, Thomas