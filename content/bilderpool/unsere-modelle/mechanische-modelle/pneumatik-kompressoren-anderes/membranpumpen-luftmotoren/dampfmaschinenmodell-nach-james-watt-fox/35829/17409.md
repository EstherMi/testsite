---
layout: "comment"
hidden: true
title: "17409"
date: "2012-10-08T09:46:44"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hallo Thomas,
mit keineren und leichteren Schwungrädern habe ich experimentiert - den toten Punkt des Schubkurbelgetriebes konnte ich damit nicht überwinden. Das kritische scheint mir zu sein, dass das Handventil einen relativ langen Umschaltzeitraum hat; daher muss die Welle ein ganzes Stück "alleine" laufen. Und das leistet eben nur ein hinreichend schweres Schwungrad (sprich: ein ausreichend großer "Energiespeicher")...
Gruß, Dirk