---
layout: "image"
title: "Kolben und Pleuel der Dampfmaschine"
date: "2012-10-07T19:47:31"
picture: "dampfmaschinenmodellnachjameswattdirkfox5.jpg"
weight: "6"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/35830
imported:
- "2019"
_4images_image_id: "35830"
_4images_cat_id: "2676"
_4images_user_id: "1126"
_4images_image_date: "2012-10-07T19:47:31"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35830 -->
Der Kolben wird von zwei parallelen Stangen gerade geführt, damit er nicht verkantet. Die Pleuelstange ist mit einer Rastachse 20 an einem Rollenlager befestigt.