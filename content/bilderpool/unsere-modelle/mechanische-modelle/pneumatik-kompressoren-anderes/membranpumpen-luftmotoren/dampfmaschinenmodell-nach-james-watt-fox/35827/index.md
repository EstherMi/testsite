---
layout: "image"
title: "Originale Dampfmaschine"
date: "2012-10-07T19:47:31"
picture: "dampfmaschinenmodellnachjameswattdirkfox2.jpg"
weight: "3"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/35827
imported:
- "2019"
_4images_image_id: "35827"
_4images_cat_id: "2676"
_4images_user_id: "1126"
_4images_image_date: "2012-10-07T19:47:31"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35827 -->
Hier ein altes Foto einer orignalen Dampfmaschine (Copyright abgelaufen).