---
layout: "image"
title: "Dampfmaschine nach James Watt (Gesamtansicht)"
date: "2012-10-07T19:47:31"
picture: "dampfmaschinenmodellnachjameswattdirkfox1.jpg"
weight: "2"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/35826
imported:
- "2019"
_4images_image_id: "35826"
_4images_cat_id: "2676"
_4images_user_id: "1126"
_4images_image_date: "2012-10-07T19:47:31"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35826 -->
Die Demonstration der Funktionsweise einer Dampfmaschine gelingt mit fischertechnik nahezu perfekt, und in der ft:c finden sich auch zahlreiche schöne "Luftmotor"-Modelle (wie das von thomas004, das komplett mit den Teilen aus dem Kasten "Profi Pneumatik II" nachgebaut werden kann: http://www.ftcommunity.de/categories.php?cat_id=1580).
Ich wollte nun ein Dampfmaschinen-Modell konstruieren, das den originalen Dampfmaschinen von James Watt möglichst nahe kommt. Vor knapp einem Jahr konstruierte ich diese Dampfmaschine hier, für die das Kompressor-Modell aus dem Pneumatik-Kasten aber nicht genug "Wumms" lieferte. Mit dem neuen ft-Kompressor läuft die Maschine jetzt jedoch einwandfrei.