---
layout: "image"
title: "Vorderseite"
date: "2016-03-21T13:33:37"
picture: "pneumatikmotor02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- details/43195
imported:
- "2019"
_4images_image_id: "43195"
_4images_cat_id: "3208"
_4images_user_id: "1924"
_4images_image_date: "2016-03-21T13:33:37"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43195 -->
Der Reifen dient als Schwungscheibe, der Motor würde aber auch ohne diese laufen!