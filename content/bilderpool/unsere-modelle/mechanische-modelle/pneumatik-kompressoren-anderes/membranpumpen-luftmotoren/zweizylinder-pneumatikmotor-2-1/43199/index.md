---
layout: "image"
title: "Unterseite"
date: "2016-03-21T13:33:37"
picture: "pneumatikmotor06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- details/43199
imported:
- "2019"
_4images_image_id: "43199"
_4images_cat_id: "3208"
_4images_user_id: "1924"
_4images_image_date: "2016-03-21T13:33:37"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43199 -->
Die neue Kurbelwelle kann die Kräfte von den Zylindern deutlich besser aufnehmen... Dafür ist der Anschluss an die Schwungseibe ect. nicht ganz so einfach: Die Verbindung wird daher durch die schwarze Kette und die Zahnräder übertragen.