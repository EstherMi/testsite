---
layout: "overview"
title: "Zweizylinder-Pneumatikmotor 2.1"
date: 2019-12-17T19:17:15+01:00
legacy_id:
- categories/3208
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3208 --> 
Die verbesserte Version des Einzylinder-Pneumatikmotors 2.0 (http://ftcommunity.de/categories.php?cat_id=3173).

Verbesserungen:
stabilere Kurbelwelle
bessere Ventile...
und zwei Zylinder

Dank der Zylinder mit Federr ist der Motor selbsanlaufend.

Ein kurzes Video gibt es unter: https://youtu.be/OiWAKAY1sT0