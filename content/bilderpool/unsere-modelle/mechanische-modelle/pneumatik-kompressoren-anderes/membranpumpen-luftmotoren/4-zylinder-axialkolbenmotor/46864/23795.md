---
layout: "comment"
hidden: true
title: "23795"
date: "2017-11-07T17:52:33"
uploadBy:
- "Stefan Rm"
license: "unknown"
imported:
- "2019"
---
Der vom Schwungrad ausgehende BS 15, an dessen Ende die zwei Winkel und die Rastachse mit Platte sitzen, ist exzentrisch angebracht --> das wäre in diesem Fall der Hebel. Die Kraft, die an diesem Hebel wirkt und somit das Drehmoment um die Hauptachse ergibt, ergibt sich daraus, dass die Lage der Taumelscheibe von den Zylindern bestimmt wird und somit die Schwungscheibe der Bewegung folgen muss (Kraftwirkung um die Drehachse)... Die Erklärung ist jetzt nicht perfekt, aber ich hoffe du kannst meinen grundlegenden Ansatz nachvollziehen.

Schöne Grüße 
Stefan