---
layout: "comment"
hidden: true
title: "23767"
date: "2017-11-05T14:24:20"
uploadBy:
- "Stefan Rm"
license: "unknown"
imported:
- "2019"
---
Hallo Peter,

vielen Dank für das Lob!!

Ich bin auf dein Modell gespannt, mit den Festo-Ventilen müsste der Motor noch besser laufen (ich besitze leider keine...).

Alle Zylinder sind entweder beim Aus- oder beim Einfahren. Es gibt keine Wartezeit, die zwei Zylinderpaare haben eine "Ansteuerungsverschiebung/Phasenverschiebung" von 90° (dies ergibt sich aus der 90° versetzten Anordung der Zylinderpaare um die Haupdrehachse. Sprich ein Zylinderpaar läuft dem anderen Zylinderpaar mit einer verschiebung von 90° hinterher.

Was genau meinst du mit "Öffsungs-Winkel" der Ventile? Meinst du damit wie lange das Ventil offen bzw. geschlossen ist?

Schöne Grüße
Stefan