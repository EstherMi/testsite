---
layout: "image"
title: "Drehbewegung2"
date: "2017-11-04T12:19:22"
picture: "axialkolbenmotor17.jpg"
weight: "17"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- details/46869
imported:
- "2019"
_4images_image_id: "46869"
_4images_cat_id: "3469"
_4images_user_id: "1924"
_4images_image_date: "2017-11-04T12:19:22"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46869 -->
Alle Zylinder werden sowol beim Ein-, als auch beim Ausfahren mit Druckluft beaufschlagt. 
Die Ansteuerung erfolgt jeweils paarweise: 
                     + der obere und untere Zylinder bilden ein Zylinderpaar
                     + der vordere und hintere Zylinder bilden ein Zylinderpaar
Fährt der obere Zylinder aus, fährt der untere ein.