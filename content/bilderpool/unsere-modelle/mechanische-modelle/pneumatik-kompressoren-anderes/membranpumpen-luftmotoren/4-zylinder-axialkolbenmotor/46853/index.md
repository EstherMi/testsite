---
layout: "image"
title: "Gesamtansicht"
date: "2017-11-04T12:19:22"
picture: "axialkolbenmotor01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- details/46853
imported:
- "2019"
_4images_image_id: "46853"
_4images_cat_id: "3469"
_4images_user_id: "1924"
_4images_image_date: "2017-11-04T12:19:22"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46853 -->
Fasziniert von Alfred Petteras 6-Zylinder-Axialkolbenmotor ( http://www.ftcommunity.de/details.php?image_id=1040#col3 ) habe ich mich entschlossen meinen eigenen Axialkolbenmotor zu bauen. Was dabei herausgekommen ist könnt ihr auf den nächsten Fotos genauer unter die Lupe nehmen ;-).

Allgemeines:
+Anzahl Zylinder: 4
+Gewicht: ca. 3,5 kg
+Bauzeit: ca. 2 Wochen

Video: https://youtu.be/6ziPNIWrqGw