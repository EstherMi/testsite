---
layout: "image"
title: "Gesamtansicht"
date: "2017-11-04T12:19:22"
picture: "axialkolbenmotor05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- details/46857
imported:
- "2019"
_4images_image_id: "46857"
_4images_cat_id: "3469"
_4images_user_id: "1924"
_4images_image_date: "2017-11-04T12:19:22"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46857 -->
Rechts unten sieht man deutlich eine der 8 Senkschrauben zur Befestigung des Modells auf der Spanholzplatte.