---
layout: "image"
title: "Schwungrad"
date: "2017-11-04T12:19:22"
picture: "axialkolbenmotor25.jpg"
weight: "25"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- details/46877
imported:
- "2019"
_4images_image_id: "46877"
_4images_cat_id: "3469"
_4images_user_id: "1924"
_4images_image_date: "2017-11-04T12:19:22"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46877 -->
Für einen rhuigen Lauf sorgt eine gefühlte Tonne an Grundbausteinen.