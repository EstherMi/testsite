---
layout: "overview"
title: "4 Zylinder Axialkolbenmotor"
date: 2019-12-17T19:17:17+01:00
legacy_id:
- categories/3469
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3469 --> 
Fasziniert von Alfred Petteras 6-Zylinder-Axialkolbenmotor ( http://www.ftcommunity.de/details.php?image_id=1040#col3 ) habe ich mich entschlossen meinen eigenen Axialkolbenmotor zu bauen. Was dabei herausgekommen ist könnt ihr auf den nächsten Fotos genauer unter die Lupe nehmen ;-).

Allgemeines:
+Anzahl Zylinder: 4
+Gewicht: ca. 3,5 kg
+Bauzeit: ca. 2 Wochen

Video: https://youtu.be/6ziPNIWrqGw