---
layout: "image"
title: "Drehbewegung6"
date: "2017-11-04T12:19:22"
picture: "axialkolbenmotor21.jpg"
weight: "21"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- details/46873
imported:
- "2019"
_4images_image_id: "46873"
_4images_cat_id: "3469"
_4images_user_id: "1924"
_4images_image_date: "2017-11-04T12:19:22"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46873 -->
