---
layout: "image"
title: "Seitenansicht"
date: "2015-10-26T14:47:12"
picture: "einzylinderpneumatikmotor3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- details/42146
imported:
- "2019"
_4images_image_id: "42146"
_4images_cat_id: "3140"
_4images_user_id: "1924"
_4images_image_date: "2015-10-26T14:47:12"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42146 -->
Der blaue Schlauch im bild unten ist die Druckluftversorgung.