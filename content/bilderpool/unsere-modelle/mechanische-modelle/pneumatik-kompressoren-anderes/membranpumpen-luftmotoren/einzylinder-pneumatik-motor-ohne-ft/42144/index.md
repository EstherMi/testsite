---
layout: "image"
title: "Gesamtansicht"
date: "2015-10-26T14:47:12"
picture: "einzylinderpneumatikmotor1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- details/42144
imported:
- "2019"
_4images_image_id: "42144"
_4images_cat_id: "3140"
_4images_user_id: "1924"
_4images_image_date: "2015-10-26T14:47:12"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42144 -->
Die meisten kennen sicherlich die "Lego switchless pneumatic engines" auf YouTube... solch einen Motor wollte ich nun auch aus fischertechnik bauen.
Nach vielen Fehlversuchen und langen Pausen ist es nun doch zu einem voll funktionsfähigen Pneumatikmotor gekommen.
Ein Vorteil dieser Variante ohne richtige Ventile ist, dass sie "relativ" kompakt baut und auch weniger Drehmoment benötigt wird, weil der große Widerstand zum Schalten der eher schwergängigen fischertechnik Ventile weckfällt. Aus diesem Grund kann auch die Schwungscheibe recht klein gehalten werden.

Ein Video zum Funktionsbeweis gibt es hier: https://youtu.be/0luH46fEqSE

Der Motor läuft mit dem normalen fischertechnik Kompressor (ca. 0,8 bar) etwas langsam, aber tadellos. Wenn man den Druck auf ca. 1,5 bar erhöht läuft er wirklich perfekt.
