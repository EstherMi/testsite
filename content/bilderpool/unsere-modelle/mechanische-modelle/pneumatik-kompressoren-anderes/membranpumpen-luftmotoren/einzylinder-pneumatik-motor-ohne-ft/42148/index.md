---
layout: "image"
title: "Innenleben-Oben"
date: "2015-10-26T14:47:12"
picture: "einzylinderpneumatikmotor5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- details/42148
imported:
- "2019"
_4images_image_id: "42148"
_4images_cat_id: "3140"
_4images_user_id: "1924"
_4images_image_date: "2015-10-26T14:47:12"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42148 -->
Um die Lage der Lenkklauen (siehe nächtes Bild) zu gewährleisten und die Schwingungen des Motors gut aufzunehmen ist das Gehäuse stabil aus Grundbausteinen aufgebaut und nimmt somit auch recht viel Plaz ein.