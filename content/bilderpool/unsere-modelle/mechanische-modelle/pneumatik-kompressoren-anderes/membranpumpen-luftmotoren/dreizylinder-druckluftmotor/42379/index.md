---
layout: "image"
title: "Zylinder und Mechanik (3)"
date: "2015-11-12T16:33:23"
picture: "dreizylinderdruckluftmotormitschlauchlogik6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42379
imported:
- "2019"
_4images_image_id: "42379"
_4images_cat_id: "3153"
_4images_user_id: "104"
_4images_image_date: "2015-11-12T16:33:23"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42379 -->
Praktisch der ganze Hub der Zylinder wird ausgenutzt. Auf dem Video sieht man es dann in Aktion.