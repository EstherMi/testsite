---
layout: "image"
title: "Zylinder und Mechanik (2)"
date: "2015-11-12T16:33:23"
picture: "dreizylinderdruckluftmotormitschlauchlogik5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42378
imported:
- "2019"
_4images_image_id: "42378"
_4images_cat_id: "3153"
_4images_user_id: "104"
_4images_image_date: "2015-11-12T16:33:23"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42378 -->
Der Motor dreht von diesem Punkt aus gesehen gegen den Uhrzeigersinn. Die Zylinder drücken die Winkellaschen und damit die Paare von (gelochten und ungelochten) I-Streben 45.