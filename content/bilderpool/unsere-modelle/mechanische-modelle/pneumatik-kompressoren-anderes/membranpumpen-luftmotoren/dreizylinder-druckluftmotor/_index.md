---
layout: "overview"
title: "Dreizylinder-Druckluftmotor mit Schlauchlogik"
date: 2019-12-17T19:17:11+01:00
legacy_id:
- categories/3153
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3153 --> 
Dieser Motor wird von selbstgebauten Pneumatikventilen in "Schlauch-Logik" (siehe ft:pedia) gesteuert.