---
layout: "image"
title: "Dampfmaschine 7"
date: "2009-02-27T20:00:21"
picture: "Dampfmaschine_09.jpg"
weight: "7"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/23232
imported:
- "2019"
_4images_image_id: "23232"
_4images_cat_id: "1580"
_4images_user_id: "328"
_4images_image_date: "2009-02-27T20:00:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23232 -->
Die Ansteuerung des Steuerventils. An der Drehscheibe 60 kann man die Länge der Kurbel stufenlos justieren, bis das Ventil optimal öffnet und schließt.