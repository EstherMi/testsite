---
layout: "image"
title: "Dampfmaschine 4"
date: "2009-02-27T20:00:21"
picture: "Dampfmaschine_06.jpg"
weight: "4"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/23229
imported:
- "2019"
_4images_image_id: "23229"
_4images_cat_id: "1580"
_4images_user_id: "328"
_4images_image_date: "2009-02-27T20:00:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23229 -->
