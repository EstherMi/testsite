---
layout: "image"
title: "Dampfmaschine 8"
date: "2009-02-27T20:00:21"
picture: "Dampfmaschine_10.jpg"
weight: "8"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/23233
imported:
- "2019"
_4images_image_id: "23233"
_4images_cat_id: "1580"
_4images_user_id: "328"
_4images_image_date: "2009-02-27T20:00:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23233 -->
Die Lagerung des Schwungrades und der Abtrieb der Dampfmaschine (Z20).

Die Befestigung des Schwungrades ist etwas unkonventionell, funktioniert aber gut. Mir fehlt leider gerade eine weitere Drehscheibe 60... ;o)