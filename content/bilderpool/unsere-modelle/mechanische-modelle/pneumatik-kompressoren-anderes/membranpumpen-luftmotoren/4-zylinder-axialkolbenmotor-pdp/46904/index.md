---
layout: "image"
title: "4-Zylinder-Axialkolbenmotor + Festo -Ventilen"
date: "2017-11-06T16:08:42"
picture: "axiaalkolbenmotor1.jpg"
weight: "1"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/46904
imported:
- "2019"
_4images_image_id: "46904"
_4images_cat_id: "3471"
_4images_user_id: "22"
_4images_image_date: "2017-11-06T16:08:42"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46904 -->
Ich habe die 4-Zylinder-Axialkolbenmotor nachgebaut  mit  eine: Antriebseinheit (4 pneumatikzylinder) + "Taumelkreuz" + Schwungrad. 
Statt das Ventilinsel habe ich die Festo 3/2-Wegeventilen + Nocken genutzt.
