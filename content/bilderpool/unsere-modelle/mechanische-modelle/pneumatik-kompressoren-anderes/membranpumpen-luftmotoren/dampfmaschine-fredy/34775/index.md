---
layout: "image"
title: "Dampfmaschine 04"
date: "2012-04-08T11:08:42"
picture: "dampfmaschine4.jpg"
weight: "4"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/34775
imported:
- "2019"
_4images_image_id: "34775"
_4images_cat_id: "2569"
_4images_user_id: "453"
_4images_image_date: "2012-04-08T11:08:42"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34775 -->
Kurbelwelle