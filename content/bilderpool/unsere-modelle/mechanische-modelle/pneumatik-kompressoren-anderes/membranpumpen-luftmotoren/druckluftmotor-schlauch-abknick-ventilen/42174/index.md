---
layout: "image"
title: "Gesamtansicht"
date: "2015-10-29T21:09:24"
picture: "druckluftmotormitschlauchabknickventilen1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42174
imported:
- "2019"
_4images_image_id: "42174"
_4images_cat_id: "3144"
_4images_user_id: "104"
_4images_image_date: "2015-10-29T21:09:24"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42174 -->
Der Antriebszylinder nutzt den kompletten Hub aus und wird beidseitig angesteuert. Das dazu nötige 4/2-Wegeventil ist ähnlich wie in der ft:pedia ab Ausgabe 2014-1 beschrieben mit abknickenden Schläuchen realisiert, die die Zylinderhälften abwechselnd mit Druckluft beaufschlagen und mit den offenen Abluftausgängen verbinden.