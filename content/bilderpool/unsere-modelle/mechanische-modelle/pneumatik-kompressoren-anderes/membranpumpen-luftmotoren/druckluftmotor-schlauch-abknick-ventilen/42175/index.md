---
layout: "image"
title: "Ventilsteuerung"
date: "2015-10-29T21:09:24"
picture: "druckluftmotormitschlauchabknickventilen2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42175
imported:
- "2019"
_4images_image_id: "42175"
_4images_cat_id: "3144"
_4images_user_id: "104"
_4images_image_date: "2015-10-29T21:09:24"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42175 -->
Der bewegliche Teil des Ventils ist einfach ein BS30 auf einem 15-mm-Gelenkstein. Die Zentralausgänge gehen zu den Zylindern. Je ein seitlicher Eingang ist mit Druckluft versorgt, der andere geht ins Freie (Abluft). Die Ansteuerung kann gut justiert werden. Angenehm ist, dass wenig Hub und vor allem nur wenig Kraft notwendig ist um das Ventil zu betätigen. Dadurch gehen an dieser Stelle kaum Energie verloren.