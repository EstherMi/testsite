---
layout: "image"
title: "Die kettengesteuerte 'DOHC'"
date: "2003-06-09T17:14:51"
picture: "V-Mot3.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- details/1182
imported:
- "2019"
_4images_image_id: "1182"
_4images_cat_id: "30"
_4images_user_id: "27"
_4images_image_date: "2003-06-09T17:14:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1182 -->
Fast wie ein Originalmotor :-)