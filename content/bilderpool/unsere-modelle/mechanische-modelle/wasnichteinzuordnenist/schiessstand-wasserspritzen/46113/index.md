---
layout: "image"
title: "Von hinten"
date: "2017-08-06T13:16:34"
picture: "schiessstandfuerwasserspritzen4.jpg"
weight: "4"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/46113
imported:
- "2019"
_4images_image_id: "46113"
_4images_cat_id: "3425"
_4images_user_id: "1557"
_4images_image_date: "2017-08-06T13:16:34"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46113 -->
Die ganze Konstruktion besteht hauptsächlich aus Statikelementen und ist absolut frei von Metallteilen! Wasserfest ist hier ein Muss und die Konstruktion funktioniert auch noch tropfnass.
2 Fallziele sind auf einer Grundplatte montiert. Winkelträger 15 (WT15) sorgen als Anschlag dafür, dass die Gelenke beim Herunterklappen nicht  ausreissen. Das sind im Bild die kleinen grauen Vierecke am unteren Ende der linken Hälfte.

Stückliste:
4 x BS5
4 x WS7,5°
4 x Gelenkwürfelklaue
4 x Gelenkwürfelzunge
10 x WT15 (davon 2 x gelb)
4 x WT30
2 x Statikbauplatte 180x90 mm²
2 x X-Strebe 169,6
2 x L-Lasche
2 x Winkellasche
16 x S-Riegel 4
1 x Grundplatte 500

Die Farbauswahl richtet sich nach dem persönlichen Geschmack und / oder dem Material im Fundus.