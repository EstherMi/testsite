---
layout: "overview"
title: "Schießstand für Wasserspritzen"
date: 2019-12-17T19:25:26+01:00
legacy_id:
- categories/3425
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3425 --> 
Der Renner bei der Gartenparty. Man nehme sommerliches Wetter, eine Wasserspritze und eine handvoll ft, schon sind die Kids beschäftigt.

Für Innenräume eiget sich trockene Munition (zum Beispiel Tennisbälle) besser.