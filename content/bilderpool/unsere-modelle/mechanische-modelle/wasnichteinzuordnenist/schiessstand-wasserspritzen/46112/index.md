---
layout: "image"
title: "Erwischt!"
date: "2017-08-06T13:16:34"
picture: "schiessstandfuerwasserspritzen3.jpg"
weight: "3"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/46112
imported:
- "2019"
_4images_image_id: "46112"
_4images_cat_id: "3425"
_4images_user_id: "1557"
_4images_image_date: "2017-08-06T13:16:34"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46112 -->
Gewonnen. Und damit das Spielchen etwas kniffliger wird, müssen beide Fallziele mit nur einer Wasserladung getroffen werden - gar nicht so einfach. Die Wasserspritze stammt aus dem Spielzeugladen. Hier haben wir das Ensemble komplett.

Sinnvollerweise steht vorne an den Zielscheiben ein Wassergefäss zum Nachtanken und der Schütze richtet die Zielscheiben wieder auf bevor er die Spritze weiter gibt - sonst sind die Kids hinterher triefnass; aber das kann ja auch erwünscht sein.

Die Entfernung zum Abschuß kann mit ein paar zusammengesteckten WT120 prima festgelegt werden.