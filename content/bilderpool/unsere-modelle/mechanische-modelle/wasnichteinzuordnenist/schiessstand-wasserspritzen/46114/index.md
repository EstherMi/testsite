---
layout: "image"
title: "Scharnier nach dem Treffer"
date: "2017-08-06T13:16:34"
picture: "schiessstandfuerwasserspritzen5.jpg"
weight: "5"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/46114
imported:
- "2019"
_4images_image_id: "46114"
_4images_cat_id: "3425"
_4images_user_id: "1557"
_4images_image_date: "2017-08-06T13:16:34"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46114 -->
Gelenkwürfel auf BS5 tragen WT30 und daran ist eine 180x90 mm² Statik-Bauplatte befestigt. Davor sitzt ein WT15 und obendrauf ein WS7,5 - mit der flacheren Seite nach hinten. Das ist der Anschlag für ein aufgerichtetes Ziel.

Die Gelenkwürfel müssen leichtgängig sein.