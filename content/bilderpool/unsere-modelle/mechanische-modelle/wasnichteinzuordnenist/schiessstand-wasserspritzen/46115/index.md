---
layout: "image"
title: "Scharnier in Lauerstellung"
date: "2017-08-06T13:16:34"
picture: "schiessstandfuerwasserspritzen6.jpg"
weight: "6"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/46115
imported:
- "2019"
_4images_image_id: "46115"
_4images_cat_id: "3425"
_4images_user_id: "1557"
_4images_image_date: "2017-08-06T13:16:34"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46115 -->
Die Kante der Bauplatte liegt auf der schlankeren Seite des WS7,5° auf. Dadurch steht die hochgeklappte Bauplatte nicht senkrecht sondern ist etwas weiter zum Schützen hin geneigt. Nur deswegen ist die Kombination BS5 plus WS7,5° verbaut.