---
layout: "image"
title: "9/9"
date: "2013-11-24T09:40:43"
picture: "mechsignal9.jpg"
weight: "9"
konstrukteure: 
- "Ironhorse406"
fotografen:
- "Ironhorse406"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ironhorse406"
license: "unknown"
legacy_id:
- details/37842
imported:
- "2019"
_4images_image_id: "37842"
_4images_cat_id: "2811"
_4images_user_id: "497"
_4images_image_date: "2013-11-24T09:40:43"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37842 -->
Außenspannwerk