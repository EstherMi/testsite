---
layout: "image"
title: "5/9"
date: "2013-11-24T09:40:27"
picture: "mechsignal5.jpg"
weight: "5"
konstrukteure: 
- "Ironhorse406"
fotografen:
- "Ironhorse406"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ironhorse406"
license: "unknown"
legacy_id:
- details/37838
imported:
- "2019"
_4images_image_id: "37838"
_4images_cat_id: "2811"
_4images_user_id: "497"
_4images_image_date: "2013-11-24T09:40:27"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37838 -->
mech. Signal mit Außenspannwerk