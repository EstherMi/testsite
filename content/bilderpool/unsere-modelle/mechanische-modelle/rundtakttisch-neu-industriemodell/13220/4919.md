---
layout: "comment"
hidden: true
title: "4919"
date: "2008-01-04T16:49:02"
uploadBy:
- "sven"
license: "unknown"
imported:
- "2019"
---
Hallo!

@steffalk:
Die Abmessungen des Klemmstiftes habe ich in meinem letzten Posting schon angegeben.
4mm Durchmesser und 1cm lang.
In welchem Baukasten die drinne waren weiß ich grad nicht.

Besser wären für die Verbindung Spannhülsen aus dem Metallbau mit 4mm Durchmesser und 1cm Länge.

Danach habe ich heute morgen schon im Geschäft ausschau gehalten.
Die hatten allerdings nur nur 4mm Durchmesser 40mm lang.
Durchschneiden ist schlecht weil das Hartmetall ist.
Aber diese Hülsen würden sicherlich besser halten wie die ft Klemmstifte.

Gruß
Sven