---
layout: "image"
title: "Moving Head seitlich"
date: "2004-06-06T10:30:08"
picture: "MH10-Seitenansicht.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/2478
imported:
- "2019"
_4images_image_id: "2478"
_4images_cat_id: "234"
_4images_user_id: "46"
_4images_image_date: "2004-06-06T10:30:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2478 -->
Beide Achsen können beliebig rotiert werden, ohne daß sich dabei Kabel aufwickeln.