---
layout: "image"
title: "Moving Head Frontal"
date: "2004-06-06T10:30:08"
picture: "MH09-Frontansicht.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/2475
imported:
- "2019"
_4images_image_id: "2475"
_4images_cat_id: "234"
_4images_user_id: "46"
_4images_image_date: "2004-06-06T10:30:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2475 -->
Modell im Überblick, dabei erkennbar mein Faible für schräge Winkel in der Stützkonstruktion. Aber die Schleifringe brauchen den Platz.