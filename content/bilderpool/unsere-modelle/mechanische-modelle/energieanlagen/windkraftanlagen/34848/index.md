---
layout: "image"
title: "Windrad 1"
date: "2012-04-30T20:47:43"
picture: "Windrad1.jpg"
weight: "1"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/34848
imported:
- "2019"
_4images_image_id: "34848"
_4images_cat_id: "2579"
_4images_user_id: "46"
_4images_image_date: "2012-04-30T20:47:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34848 -->
Bisher hatte ich es noch nicht geschafft, dem Wind etwas Energie abzugewinnen. Aber da mich gerade Belinda besucht und wir gerade Zeit haben, konstruieren wir gemeinsam eine kleine Windkraftanlage und setzen sie draussen dem Wind aus.

Naja, starker Wind geht gerade nicht, aber es reicht tatsächlich aus, den Generator zu drehen. Unsere Ausbeute: so ca. 10 mA Strom bei knapp 5V.

Reicht vielleicht für eine Leuchtdiode.