---
layout: "image"
title: "Windrad 2"
date: "2012-04-30T20:47:43"
picture: "Windrad2.jpg"
weight: "2"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/34849
imported:
- "2019"
_4images_image_id: "34849"
_4images_cat_id: "2579"
_4images_user_id: "46"
_4images_image_date: "2012-04-30T20:47:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34849 -->
Hier nochmals der Generator: ein Schrittmotor, dessen gewaltige Induktion bei kleinsten Drehzahlen bereits spürbar Strom macht.

Und eine Einrichtung zum Drehen des Rotors fehlt nicht. Allerdings haben wir das von Hand gekurbelt, weil die Energie zum Drehen des Rotors von der Windkraftanlage leider nicht bereitgestellt wurde.

Jedenfalls nicht heute.