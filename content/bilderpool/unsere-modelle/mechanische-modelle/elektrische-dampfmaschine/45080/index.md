---
layout: "image"
title: "Das vierte Bild der 'Dampfmaschine'"
date: "2017-01-21T19:05:25"
picture: "bkl4.jpg"
weight: "4"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- details/45080
imported:
- "2019"
_4images_image_id: "45080"
_4images_cat_id: "3356"
_4images_user_id: "2635"
_4images_image_date: "2017-01-21T19:05:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45080 -->
