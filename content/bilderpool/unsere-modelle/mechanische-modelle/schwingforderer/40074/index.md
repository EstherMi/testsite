---
layout: "image"
title: "Detail Rinne 2"
date: "2014-12-30T07:14:11"
picture: "schwingfoerderer07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- details/40074
imported:
- "2019"
_4images_image_id: "40074"
_4images_cat_id: "3013"
_4images_user_id: "1924"
_4images_image_date: "2014-12-30T07:14:11"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40074 -->
