---
layout: "comment"
hidden: true
title: "24211"
date: "2018-10-01T10:38:19"
uploadBy:
- "Rüdiger Riedel"
license: "unknown"
imported:
- "2019"
---
Hallo Thomas,
wie soll ich die E-Magnete kippen, in Rollrichtung? Dann müsste ich den Abstand zur Kugel vergrößern, das wäre schlecht. Oder seitlich, dann würde nur eine zusätzliche Querkraft erzeugt.
Gruß
Rüdiger