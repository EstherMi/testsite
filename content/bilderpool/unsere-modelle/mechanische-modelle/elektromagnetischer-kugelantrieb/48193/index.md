---
layout: "image"
title: "Direkt geschalteter Linerarbeschleuniger 1"
date: "2018-09-30T20:40:29"
picture: "kl_Direkt_geschalteter_Linerarbeschleuniger_1.jpg"
weight: "2"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- details/48193
imported:
- "2019"
_4images_image_id: "48193"
_4images_cat_id: "3535"
_4images_user_id: "2635"
_4images_image_date: "2018-09-30T20:40:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48193 -->
Am 22. Sept. auf der Convention in Dreieich unterhielt ich mich mit einem ft-Freund, seinen Namen weiß ich leider nicht. Beim Betrachten meines Ringbeschleunigers (ft:pedia 2017-3) erzählte er mir von seiner Idee, keinen Dauermagneten sondern eine ft-Stahlkugel mit den Elektromagneten zu beschleunigen. Der E-Magnet sollte von der Kugel eingeschaltet werden, indem diese zwei stromführende Achsen beim Darüberrollen überbrückt.
Diese Idee musste ich natürlich ausprobieren!
Die Kontaktstelle besteht aus zwei Winkelsteinen 7,5° und zwei Winkelsteinen 60° sowie zwei darin geführten Achsen 30.
Anschließend rollt die Kugel auf Kunstoffachsen weiter zur nächsten Kontaktstelle.