---
layout: "image"
title: "Direkt geschalteter Linerarbeschleuniger 4"
date: "2018-10-05T20:15:39"
picture: "kl_Direkt_geschalteter_Linerarbeschleuniger_4.jpg"
weight: "1"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: ["Kugelantrieb"]
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- details/48196
imported:
- "2019"
_4images_image_id: "48196"
_4images_cat_id: "3535"
_4images_user_id: "2635"
_4images_image_date: "2018-10-05T20:15:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48196 -->
Zwei E-Magnete stehen sich gegenüber und sind parallel geschaltet. Die Beschleunigung der Kugel ist deutlich besser!