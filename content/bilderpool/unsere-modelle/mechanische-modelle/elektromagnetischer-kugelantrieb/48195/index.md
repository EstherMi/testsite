---
layout: "image"
title: "Direkt geschalteter Linerarbeschleuniger 3"
date: "2018-09-30T20:40:30"
picture: "kl_Direkt_geschalteter_Linerarbeschleuniger_3.jpg"
weight: "4"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: ["Elektromagnet", "Kugel", "Beschleunigung"]
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- details/48195
imported:
- "2019"
_4images_image_id: "48195"
_4images_cat_id: "3535"
_4images_user_id: "2635"
_4images_image_date: "2018-09-30T20:40:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48195 -->
Die Magnete sind abgenommen, die gesamte Strecke ist sichtbar. Der elektrische Anschluss ist polunabhängig weil keine Dauermagnete verwendet werden.
Eine bescheidene Beschleunigung erreiche ich erst mit Spannungen von 20 &#8211; 25 V! Für die Elektromagnete ist das unschädlich, da sie nur ganz kurz eingeschaltet werden.