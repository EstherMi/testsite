---
layout: "image"
title: "Dynamo in grau"
date: "2013-07-19T22:26:13"
picture: "ft_tech800.jpg"
weight: "5"
konstrukteure: 
- "mir nicht bekannter Meister"
fotografen:
- "unbekannter Meister"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "xbach"
license: "unknown"
legacy_id:
- details/37168
imported:
- "2019"
_4images_image_id: "37168"
_4images_cat_id: "2631"
_4images_user_id: "427"
_4images_image_date: "2013-07-19T22:26:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37168 -->
das Original