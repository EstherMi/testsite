---
layout: "image"
title: "Drehzahlmessung"
date: "2012-09-16T20:52:47"
picture: "Drehzahlmessung.jpg"
weight: "1"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/35523
imported:
- "2019"
_4images_image_id: "35523"
_4images_cat_id: "2632"
_4images_user_id: "1126"
_4images_image_date: "2012-09-16T20:52:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35523 -->
Mit einem Baustein 15 mit Magnet (108278, E-Tec) und einem Reed-Kontakt (36120) lässt sich über den Zählereingang des TX die Drehzahl des Motors bestimmen. Der E-Motor erreicht bei guter Justierung über 600 U/min.