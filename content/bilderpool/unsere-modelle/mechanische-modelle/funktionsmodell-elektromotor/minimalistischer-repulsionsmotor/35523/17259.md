---
layout: "comment"
hidden: true
title: "17259"
date: "2012-09-18T18:48:24"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hallo Stefan,
zunächst wollte ich optisch messen, habe dann aber angesichts der Erfahrungen von Ingo (http://www.ftcommunity.de/categories.php?cat_id=1430) nach einer Alternative gesucht.
Mein Messaufbau ist hinsichtlich der maximal messbaren Drehzahl sicher begrenzter, denn der Reed-Schalter reagiert nur in einem Winkel von ca. 30°, daher muss der Zählereingang mindestens 12 mal pro Umdrehung abgefragt werden, damit ich keine Umdrehung verpasse. Bei 600 Umdrehungen pro Minute sind das, wenn ich richtig rechne, eine Abfrage alle 0,0083 Sekunden.
Weiß denn jemand, wie schnell die Zähler C1 und C2 des TX maximal zählen können? Dazu habe ich in der Dokumentation und im Forum nichts finden können...
Gruß, Dirk