---
layout: "image"
title: "Nahansicht der Achse mit den Magneten"
date: "2012-09-09T20:58:19"
picture: "Dokumentation3.jpg"
weight: "5"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/35474
imported:
- "2019"
_4images_image_id: "35474"
_4images_cat_id: "2632"
_4images_user_id: "1126"
_4images_image_date: "2012-09-09T20:58:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35474 -->
Zwei Rastadapter an einem Baustein 15 mit zwei Zapfen, an dessen vier Außenseiten die Magneten befestigt werden, bilden das Herzstück des Motors. Achtung: Die Magneten müssen alle in derselben "Polung" montiert werden. Ich habe sie mit dem ft-Doppelklebeband auf vier Bauplatten 15x15 geklebt. Links und rechts werden zwei Elektromagneten so platziert, dass ihre Mitte auf Achshöhe liegt.