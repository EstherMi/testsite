---
layout: "image"
title: "Gesamtansicht des Elektromotors"
date: "2012-09-09T20:58:19"
picture: "Dokumentation1.jpg"
weight: "3"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/35472
imported:
- "2019"
_4images_image_id: "35472"
_4images_cat_id: "2632"
_4images_user_id: "1126"
_4images_image_date: "2012-09-09T20:58:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35472 -->
Mein Ziel war die Konstruktion eines Funktionsmodells für einen Elektromotor mit möglichst wenigen Bauteilen, das die Funktionsweise eines Repulsionsmotors veranschaulicht und sich leicht nachbauen lässt.

Das Modell aus dem Clubheft 1/1973 (http://www.ft-datenbank.de/web_document.php?id=ffdf0c2c-bb68-4e4a-9763-22d759eb0d0c; von Fredy für die Convention 2009 nachgebaut: http://www.ftcommunity.de/details.php?image_id=25317) kam meiner Vorstellung schon ziemlich nahe, allerdings erschien mir das Herzstück etwas "monströs". Fredy hatte es in seiner Variante schon deutich eleganter gestaltet. Noch kleiner geht es mit vier der starken Dauermagneten aus dem Kasten "Technical Revolutions" (139251).