---
layout: "image"
title: "Gleichrichter und Relais"
date: "2012-09-09T20:58:19"
picture: "Dokumentation4.jpg"
weight: "6"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/35475
imported:
- "2019"
_4images_image_id: "35475"
_4images_cat_id: "2632"
_4images_user_id: "1126"
_4images_image_date: "2012-09-09T20:58:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35475 -->
Das Relais macht nichts anderes, als bei jeder Betätigung des Tasters die Polung der beiden Elektromagnete zu vertauschen (Schaltplan siehe http://www.ft-datenbank.de/web_document.php?id=ffdf0c2c-bb68-4e4a-9763-22d759eb0d0c, S. 22). Das ginge - ganz ohne Elektronik - prinzipiell auch mit zwei direkt hintereinander montierten Mini-Tastern, die von den Unterbrecherstücken des Schleifrings betätigt werden. Zwei Mini-Taster sind allerdings so schwergängig, dass mein Motor nicht startete; außerdem schalten sie nicht exakt gleichzeitig, sodass die Gefahr eines Kurzschlusses besteht.