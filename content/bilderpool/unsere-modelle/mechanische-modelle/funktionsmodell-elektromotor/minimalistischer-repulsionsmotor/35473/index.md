---
layout: "image"
title: "Herzstück des Motors"
date: "2012-09-09T20:58:19"
picture: "Dokumentation2.jpg"
weight: "4"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/35473
imported:
- "2019"
_4images_image_id: "35473"
_4images_cat_id: "2632"
_4images_user_id: "1126"
_4images_image_date: "2012-09-09T20:58:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35473 -->
Hier das Herzstück des Motors. Der Schleifring entspricht der Originalkonstruktion des Clubheft-Modells; in gleichen Abständen werden darauf vier Unterbrecherstücke montiert, die den großen Taster viermal je Umdrehung betätigen. Der Taster ist deutlich leichtgängiger als die Mini-Taster; wer keinen dieser Alt-Schalter besitzt, kann alternativ einen "Selbstbau-Taster" verwenden. Durch "Verdrehen" des Schleifrings gegen die Magneten und Verschieben des Tasters lässt sich die Geschwindigkeit und die Laufruhe des Motors optimieren.