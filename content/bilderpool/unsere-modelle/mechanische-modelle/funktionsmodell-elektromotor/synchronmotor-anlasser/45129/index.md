---
layout: "image"
title: "Rotor"
date: "2017-02-06T17:22:59"
picture: "smma3.jpg"
weight: "3"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/45129
imported:
- "2019"
_4images_image_id: "45129"
_4images_cat_id: "3363"
_4images_user_id: "2228"
_4images_image_date: "2017-02-06T17:22:59"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45129 -->
Die Magnet sind etwa 22mm lang und haben eine Durchmesser von 5mm. Damit sie in die Gelenkklauen passen, wurden sie mit einem Gummiband fixiert.