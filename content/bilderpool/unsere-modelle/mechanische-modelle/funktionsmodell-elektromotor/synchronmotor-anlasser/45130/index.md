---
layout: "image"
title: "Rotor und Stator"
date: "2017-02-06T17:22:59"
picture: "smma4.jpg"
weight: "4"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/45130
imported:
- "2019"
_4images_image_id: "45130"
_4images_cat_id: "3363"
_4images_user_id: "2228"
_4images_image_date: "2017-02-06T17:22:59"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45130 -->
Die Dauermagnete sind Teil des Rotors, der Elektromagnet Teil des Stators.