---
layout: "image"
title: "Anlasser eingekuppelt"
date: "2017-02-06T17:22:59"
picture: "smma5.jpg"
weight: "5"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/45131
imported:
- "2019"
_4images_image_id: "45131"
_4images_cat_id: "3363"
_4images_user_id: "2228"
_4images_image_date: "2017-02-06T17:22:59"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45131 -->
Der Encodermotor treibt, solange er eingekoppelt ist, den Rotor an, um ihn auf die Zieldrehzahl zu beschleunigen. Damit der Encodermotor eine Drehzahl von 500 U/min erreichen kann, wurde er mit einer 3:1 Übersetzung eingebaut.