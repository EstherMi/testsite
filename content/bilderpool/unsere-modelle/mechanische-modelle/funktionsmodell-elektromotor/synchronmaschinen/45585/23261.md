---
layout: "comment"
hidden: true
title: "23261"
date: "2017-03-21T19:44:20"
uploadBy:
- "geometer"
license: "unknown"
imported:
- "2019"
---
Tja, jetzt rate einmal, was mich letzte Nacht um 0:35 Uhr auf so etwas gebracht hat. Ich sage nur "Winkelsteine".

Was mir an diesen Drehscheiben-Motoren besonders gefällt, ist, dass beim Drehen ein transparenter Kreisring durch alle Drehscheiben hindurch entsteht. Die Drehscheiben scheinen also in einen Kern und einen äußeren Ring zu zerfallen. Das sieht schon gut aus.