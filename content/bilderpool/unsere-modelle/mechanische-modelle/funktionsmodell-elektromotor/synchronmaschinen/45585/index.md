---
layout: "image"
title: "Synchronmotor mit 600 U/min aus drei Drehscheiben"
date: "2017-03-21T17:35:36"
picture: "Synchron600UproMin1.jpg"
weight: "12"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- details/45585
imported:
- "2019"
_4images_image_id: "45585"
_4images_cat_id: "3374"
_4images_user_id: "1088"
_4images_image_date: "2017-03-21T17:35:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45585 -->
Hier werden 10 Neodym-Magneten in einem bestimmten Muster in die radialen Bohrungen dreier Drehscheiben gesteckt. Das liefert einen Synchronmotor mit 600 U/min mit sehr einfachem Aufbau.

Empfohlen werden zur Sicherheit kräftige Gummiringe/-bänder auf den Drehscheiben. Nachbau auf EIGENE GEFAHR!