---
layout: "image"
title: "Synchronmotor mit 600 U/min aus drei Drehscheiben - Konzept"
date: "2017-03-21T17:35:36"
picture: "Synchron600UproMin3.jpg"
weight: "14"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- details/45587
imported:
- "2019"
_4images_image_id: "45587"
_4images_cat_id: "3374"
_4images_user_id: "1088"
_4images_image_date: "2017-03-21T17:35:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45587 -->
Benachbarte Magneten haben nicht den gewünschten Winkel von 360°/10 = 36° zueinander, sondern einige Nachbarn stecken in einem Winkel von 30° und andere 45°. Wie man sieht, kommt man aber den idealen Steckpositionen recht nahe. Der Motor läuft auch sehr gut.