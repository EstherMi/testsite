---
layout: "image"
title: "Fast-Zehneck"
date: "2017-02-26T16:49:31"
picture: "Fast-Zehneck.jpg"
weight: "18"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- details/45278
imported:
- "2019"
_4images_image_id: "45278"
_4images_cat_id: "3374"
_4images_user_id: "2635"
_4images_image_date: "2017-02-26T16:49:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45278 -->
Das ist das Fast-Zehneck, aus dem eine 10-polige Synchronmaschine werden soll, mit einer Drehzahl von 600 U/min.
Fast-Zehneck: 10x30° + 10x15°-10x7,5° = 375°, also 15° zu viel. Aber es geht.