---
layout: "image"
title: "Synchronmaschine mit 600 U/min weiterentwickelt aus Rüdiger Riedels Fast-Zehneck"
date: "2018-02-23T19:37:47"
picture: "P1240997kl.jpg"
weight: "1"
konstrukteure: 
- "Reiner Merz"
fotografen:
- "Reiner Merz"
keywords: ["Synchronmaschine", "Synchronmotor"]
uploadBy: "ReinerMerz"
license: "unknown"
legacy_id:
- details/47301
imported:
- "2019"
_4images_image_id: "47301"
_4images_cat_id: "3374"
_4images_user_id: "2838"
_4images_image_date: "2018-02-23T19:37:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47301 -->
Aus Rüdiger Riedels Fast-Zehneck habe ich einen Synchronmotor mit außenliegenden Magneten gebaut. Ich habe dazu 4x20 mm NeoDym verwendet, die lassen sich mit je einer S-Riegelscheibe gut festmachen. Der Motor läuft ganz gut.