---
layout: "comment"
hidden: true
title: "23375"
date: "2017-04-26T09:52:02"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Spitze! Vor allem die 600 U/min macht das für Uhrengetriebe interessant, weil wir wohl keine 1:5 brauchen, wenn ich das richtig sehe.

Gruß,
Stefan