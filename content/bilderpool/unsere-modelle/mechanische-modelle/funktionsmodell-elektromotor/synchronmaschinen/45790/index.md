---
layout: "image"
title: "10-polige Synchronmaschine im Betrieb"
date: "2017-04-25T16:54:26"
picture: "10-polige_Synchronmaschine_im_Betrieb.jpg"
weight: "10"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- details/45790
imported:
- "2019"
_4images_image_id: "45790"
_4images_cat_id: "3374"
_4images_user_id: "2635"
_4images_image_date: "2017-04-25T16:54:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45790 -->
