---
layout: "image"
title: "6-poliger Synchronläufer mit 1/3 Nennfrequenz im Stand / andere Seite"
date: "2017-04-27T14:37:56"
picture: "Bild3.jpg"
weight: "8"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Synchronläufer", "1/3-Nennfrequenz", "333Hz"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/45794
imported:
- "2019"
_4images_image_id: "45794"
_4images_cat_id: "3374"
_4images_user_id: "579"
_4images_image_date: "2017-04-27T14:37:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45794 -->
Er läuft sehr ruhig mit 1/3 seiner Nennfrequenz (1000 Umdrehungen / Minute), also mit 333 Umdrehungen pro Minute. Das liegt wohl an der Geometrie, also dem Abstand der Dauer-Magneten untereinander auf dem Umfang der Scheibe im Verhältnis zum Abstand der Elektromagnetpole und deren Abstand zur Drehscheibe.  
Damit lässt sich die 50 Hz-Synchronuhr mit Schrittschaltwerk von Dirk Fox betreiben, wenn man die Getriebeuntersetzung bis zur Minutenachse ändert von 1000:1 und dann 1:2 auf 1000:1 und dann 1:3, um auf eine Umdrehung pro Minute zu kommen.