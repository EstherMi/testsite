---
layout: "image"
title: "2-polige Synchronmaschine mit 'Trommelanker'"
date: "2017-10-11T16:59:09"
picture: "Trommelanker_p1.jpg"
weight: "5"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: ["Synchron", "Synchronmotor", "Synchronmaschine", "Polpaarzahl1", "2-polig"]
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- details/46759
imported:
- "2019"
_4images_image_id: "46759"
_4images_cat_id: "3374"
_4images_user_id: "2635"
_4images_image_date: "2017-10-11T16:59:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46759 -->
Mit 2 Stabmagneten, lose eingelegt mit wechselnder Polung, erhält man die Polpaarzahl 1. Die Maschine läuft mit dem alten, grauen Netzgerät an 6 V Wechselstrom mit 3000 Umdrehungen pro Minute.