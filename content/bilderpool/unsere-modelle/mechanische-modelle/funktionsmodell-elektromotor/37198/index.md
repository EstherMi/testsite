---
layout: "image"
title: "Permanetmagnetmotor mit Hallsensor"
date: "2013-07-28T13:59:39"
picture: "ft_motor800.jpg"
weight: "1"
konstrukteure: 
- "xbach"
fotografen:
- "xbach"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "xbach"
license: "unknown"
legacy_id:
- details/37198
imported:
- "2019"
_4images_image_id: "37198"
_4images_cat_id: "2631"
_4images_user_id: "427"
_4images_image_date: "2013-07-28T13:59:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37198 -->
- noch ein Motor ...