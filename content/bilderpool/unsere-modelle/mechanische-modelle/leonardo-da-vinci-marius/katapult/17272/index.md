---
layout: "image"
title: "katapult3.jpg"
date: "2009-02-03T00:59:28"
picture: "katapult3.jpg"
weight: "3"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/17272
imported:
- "2019"
_4images_image_id: "17272"
_4images_cat_id: "1548"
_4images_user_id: "845"
_4images_image_date: "2009-02-03T00:59:28"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17272 -->
