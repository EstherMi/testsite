---
layout: "overview"
title: "Sturmleiter"
date: 2019-12-17T19:23:07+01:00
legacy_id:
- categories/1549
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1549 --> 
Diese Version der Sturmleiter von Leonardo Da Vinci ist die \"Luxus\"-Ausführung. Die Leiter wird durch den Mini-Motor hoch-und runtergefahren.