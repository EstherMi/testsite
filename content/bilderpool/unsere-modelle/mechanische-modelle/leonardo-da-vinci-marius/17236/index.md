---
layout: "image"
title: "Gesamtansicht"
date: "2009-02-01T19:35:38"
picture: "streitwagen1.jpg"
weight: "1"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/17236
imported:
- "2019"
_4images_image_id: "17236"
_4images_cat_id: "1546"
_4images_user_id: "845"
_4images_image_date: "2009-02-01T19:35:38"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17236 -->
