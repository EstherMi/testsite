---
layout: "image"
title: "50Hz-Motor"
date: "2011-07-01T23:32:34"
picture: "Motor.jpg"
weight: "1"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- details/30981
imported:
- "2019"
_4images_image_id: "30981"
_4images_cat_id: "615"
_4images_user_id: "1088"
_4images_image_date: "2011-07-01T23:32:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30981 -->
Ein vibrationsfreier, geräuscharmer 50Hz-Motor. Innen im Rad befinden sich ein Außen- und ein Innenzahnrad Z30. In den zwölf äußeren Bohrungen des Innenzahnrads befinden sich zwölf Magnete S-04-10-AN von supermagnete.de mit abwechselnden Polaritäten. Die beiden Elektromagnete arbeiten gegentaktig. In axialer Richtung ergibt sich somit keine Kraft auf das Rad. Das Rad braucht für eine Umdrehung genau 120ms, das sind also 25/3 Umdrehungen pro Sekunde. Beide Elektromagnete sind in Reihe geschaltet. Für den Antrieb einer Uhr ist das völlig ausreichend, das Anwerfen erfordert aber mehrere Versuche.