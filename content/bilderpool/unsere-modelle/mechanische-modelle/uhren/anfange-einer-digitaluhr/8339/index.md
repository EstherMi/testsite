---
layout: "image"
title: "Ein Segment von hinten"
date: "2007-01-08T16:53:45"
picture: "digitaluhrprototyp5.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/8339
imported:
- "2019"
_4images_image_id: "8339"
_4images_cat_id: "767"
_4images_user_id: "104"
_4images_image_date: "2007-01-08T16:53:45"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8339 -->
Durch "Anfassen" des zwischen den beiden BS15 sichtbaren Klemmrings und Schieben soll die Vor-/Zurückbewegung erreicht werden.