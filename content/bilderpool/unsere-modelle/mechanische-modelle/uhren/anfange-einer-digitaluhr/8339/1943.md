---
layout: "comment"
hidden: true
title: "1943"
date: "2007-01-10T09:29:20"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ganz anders ;-) Ich habe 7 E-Magnete verbaut, gerade genug für 1 Ziffer. Ich habe insgesamt genug Achsen für alle 28 Segmente, aber nicht 28 Metallachsen. Ein paar der Achsen sind glatte Kunststoffachsen. Das ist aber egal und funktioniert genausogut. Die Mechanik mit den 7 E-Magneten zur Umstellung einer Ziffer soll zu den 4 Ziffern verfahren werden können. Ok?

Gruß,
Stefan