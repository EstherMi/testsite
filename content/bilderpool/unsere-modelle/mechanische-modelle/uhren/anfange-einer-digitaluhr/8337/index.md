---
layout: "image"
title: "Von hinten"
date: "2007-01-08T16:53:45"
picture: "digitaluhrprototyp3.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/8337
imported:
- "2019"
_4images_image_id: "8337"
_4images_cat_id: "767"
_4images_user_id: "104"
_4images_image_date: "2007-01-08T16:53:45"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8337 -->
Auf dieser Seite müssen noch die Vorschubmechanik und die Elektronik untergebracht werden.