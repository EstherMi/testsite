---
layout: "image"
title: "Die Schiebemechanik von hinten"
date: "2007-01-08T17:16:10"
picture: "digitaluhrprototyp7.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/8341
imported:
- "2019"
_4images_image_id: "8341"
_4images_cat_id: "767"
_4images_user_id: "104"
_4images_image_date: "2007-01-08T17:16:10"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8341 -->
Auf den Metallachsen sitzen auf der Vorderseite BS30, mit denen die Lagerböcke verbunden sind.