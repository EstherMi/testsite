---
layout: "image"
title: "Detailblick von hinten"
date: "2007-07-16T16:20:57"
picture: "digitaluhr5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/11101
imported:
- "2019"
_4images_image_id: "11101"
_4images_cat_id: "1006"
_4images_user_id: "104"
_4images_image_date: "2007-07-16T16:20:57"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11101 -->
Hier kann man von hinten bis nach vorne auf die Rückseite der anhebbaren Segmentsteller durchgucken.