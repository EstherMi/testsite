---
layout: "image"
title: "Von Vorne"
date: "2007-01-08T16:53:45"
picture: "digitaluhrprototyp2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/8336
imported:
- "2019"
_4images_image_id: "8336"
_4images_cat_id: "767"
_4images_user_id: "104"
_4images_image_date: "2007-01-08T16:53:45"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8336 -->
Die Segmente sollen hinter einer matten Transparentfolie o. ä. stehen. Jedes Segment kann ca. 3 mm vorgeschoben bzw. zurückgezogen werden. Wenn das Segment vorgeschoben wurde, soll es an der Transparentfolie anliegen und deshalb deutlich sichtbar sein. Wird es zurückgezogen, soll es wegen der diffusen Wirkung der matten Folie fast unsichtbar sein. Auf diese Weise sollten alle Ziffern ordentlich erkennbar dargestellt werden können.