---
layout: "image"
title: "Backside (3)"
date: "2016-03-07T12:45:30"
picture: "segmentclock08.jpg"
weight: "8"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- details/43009
imported:
- "2019"
_4images_image_id: "43009"
_4images_cat_id: "3199"
_4images_user_id: "1505"
_4images_image_date: "2016-03-07T12:45:30"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43009 -->
All 4 displays together, 2 mirrored ones on the left and two mirrored ones on the right. 

Two TX controllers are built-in to the bottom to control the motors. 6 Motors in total, so need two TXs. One motor to turn levers on each display, one motor to run the carriage up and down, and one to drive the seconds indicator in the middle.