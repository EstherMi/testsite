---
layout: "image"
title: "Carriage (3)"
date: "2016-03-07T12:45:30"
picture: "segmentclock11.jpg"
weight: "11"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- details/43012
imported:
- "2019"
_4images_image_id: "43012"
_4images_cat_id: "3199"
_4images_user_id: "1505"
_4images_image_date: "2016-03-07T12:45:30"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43012 -->
Carriage in top position. When changing segments, the carriage always starts from the top, where it is positioned over the top segment. So it turns the levers for that row first. Then the carriage is lowered to the 2nd segment, it changes those levers, when needed, and it descends further, until it reaches the bottom position, where it can change the bottom segments. Then it moves back to the top in order to be ready for the next change.

The carriage makes the 6 stops on the way down and not on the way up, as the Encoder Motor sometimes doesn't have enough power to position properly on the way up.