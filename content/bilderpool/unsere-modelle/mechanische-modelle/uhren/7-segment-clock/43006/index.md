---
layout: "image"
title: "Puzzle"
date: "2016-03-07T12:45:30"
picture: "segmentclock05.jpg"
weight: "5"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- details/43006
imported:
- "2019"
_4images_image_id: "43006"
_4images_cat_id: "3199"
_4images_user_id: "1505"
_4images_image_date: "2016-03-07T12:45:30"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43006 -->
It was quite a puzzle to fit everything in.