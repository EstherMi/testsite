---
layout: "image"
title: "Backside (2)"
date: "2016-03-07T12:45:30"
picture: "segmentclock07.jpg"
weight: "7"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- details/43008
imported:
- "2019"
_4images_image_id: "43008"
_4images_cat_id: "3199"
_4images_user_id: "1505"
_4images_image_date: "2016-03-07T12:45:30"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43008 -->
The shafts on the backside have a 'Abstandsring 4mm' and a 'Achsadapter'. The Achsadapter of course doesn't have a real Rastkupplung, but it fits snugly on the outer end of a Rastachse. However, in order to make it last, I had to stick a little piece of plastic in to give it better hold. 
The Achsadapter is small and flat, and functions as a lever that can be turned 90 degrees left or right.
In the picture you can see two rows shafts for two 7 segment displays, with a Zahnstange in the middle. The 2 displays are mirrored, so they can both be close to the Zahnstange. The Zahnstange is used to slide a motor up and down to move the levers.