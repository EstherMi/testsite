---
layout: "image"
title: "Segment"
date: "2016-03-07T12:45:30"
picture: "segmentclock02.jpg"
weight: "2"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- details/43003
imported:
- "2019"
_4images_image_id: "43003"
_4images_cat_id: "3199"
_4images_user_id: "1505"
_4images_image_date: "2016-03-07T12:45:30"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43003 -->
Each segment consists of a red and a yellow 'Bauplatte' 4 x 1, connected to a regular 'Baustein'. It can turn 90 degrees around a lateral shaft ('Rastachse mit Platte' for the ones shown in the picture) that is fixed using a 'Gelenkwürfel-Klaue mit Hülse', to show either the red or yellow side. In order to turn the segment from the back, two Kegelzahnräder are used. The 'Klemmbuchse 5' on the Hülse is to provide a little bit of friction, so that the segment stays in position, and doesn't fall back to its 'red' state.