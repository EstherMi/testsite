---
layout: "comment"
hidden: true
title: "21770"
date: "2016-03-08T09:46:44"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hi Willem,
this is a 	marvelous solution. I had been working on such a clock some years ago, but I could not find a method small enough to adjust the segments. The mechanism you invented is really amazing...
Very well done!
Regards, Dirk