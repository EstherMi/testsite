---
layout: "image"
title: "ROBOPro Program"
date: "2016-03-07T12:45:30"
picture: "segmentclock15.jpg"
weight: "15"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- details/43016
imported:
- "2019"
_4images_image_id: "43016"
_4images_cat_id: "3199"
_4images_user_id: "1505"
_4images_image_date: "2016-03-07T12:45:30"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43016 -->
The clock driven by a ROBOPro program running on the TX controllers. 60610 milliseconds on the internal TX clock is about a minute, and every minute, the program calculates the segments that need to change, and lowers the carriage to make the changes.