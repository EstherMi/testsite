---
layout: "image"
title: "Segments"
date: "2016-03-07T12:45:30"
picture: "segmentclock04.jpg"
weight: "4"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- details/43005
imported:
- "2019"
_4images_image_id: "43005"
_4images_cat_id: "3199"
_4images_user_id: "1505"
_4images_image_date: "2016-03-07T12:45:30"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43005 -->
By having the segment drives on the back in a vertical row and at equal distances, the Kegelzahnräder end up in different places, so there are in fact 4 different segments: 2 left, 2 right, top/bottom and the middle one. The right ones in the picture require a bit of glue to glue a piece of 'Verbindungsstück' on the inside of an 'Eckstein 15'.