---
layout: "image"
title: "Carriage"
date: "2016-03-07T12:45:30"
picture: "segmentclock09.jpg"
weight: "9"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- details/43010
imported:
- "2019"
_4images_image_id: "43010"
_4images_cat_id: "3199"
_4images_user_id: "1505"
_4images_image_date: "2016-03-07T12:45:30"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43010 -->
This is the carriage that slides up and down the Zahnstange to move the levers. The carriage is driven by a single encoder motor, which drives one long shaft that is connected to two 'U-Getriebeachse', one for each Zahnstange. The carriage has an S-motor on each side of the Zahnstangem with an Achsadapter and a piece of a 'Federnocken'.