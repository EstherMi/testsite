---
layout: "overview"
title: "Akkubetriebene Mini-Uhr"
date: 2019-12-17T19:19:29+01:00
legacy_id:
- categories/3546
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3546 --> 
Diese Uhr wird von einem normalen fischertechnik-Akku gespeist, ist mit einem Netduino in .net (C#) auf dem nanoFramework programmiert, verwendet einen Schrittmotor und ist durch manuelles Drehen an einem Z25 (Danke, Roland!) ganz leicht und schnell stellbar.