---
layout: "image"
title: "Blick von oben"
date: "2018-11-23T21:15:00"
picture: "akkubetriebeneminiuhr4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48413
imported:
- "2019"
_4images_image_id: "48413"
_4images_cat_id: "3546"
_4images_user_id: "104"
_4images_image_date: "2018-11-23T21:15:00"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48413 -->
Ein zweiter Blick auf Antriebsstrang, Getriebe und Controller.