---
layout: "image"
title: "Rückseite"
date: "2017-10-22T19:43:34"
picture: "geraeuschlosehzuhrmitneodymsekundenwelle03.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46831
imported:
- "2019"
_4images_image_id: "46831"
_4images_cat_id: "3468"
_4images_user_id: "104"
_4images_image_date: "2017-10-22T19:43:34"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46831 -->
Die schwarze Frontplattenkonstruktion ist mit dem schweren älteren fischertechnik-Trafo verbunden. Dessen Gewicht genügt völlig für einen sicheren Stand. Von dessen Wechselspannungsausgang geht es zum h4-Gleichrichter links. Der macht daraus knapp 9 V Gleichspannung für die beiden Electronics-Moduln, die ansonsten die 50 Hz zählen und die vier-Phasen-Ansteuerung der beiden Elektromagnete (unten im Bild) realisieren.

Das schicke Z30-Paar ist mit 30 Neodym-Magneten vom fischerfriendsman bestückt. Die sitzen immer abwechselnd herum gepolt zwischen den Zähnen und werden von einem Haushaltsgummi gesichert. Die Magnete ziehen das ganze präzise jede Sekunde um 1/60 Vollkreis weiter - damit ist die Sekundenachse schonmal versorgt.

Den Rest besorgt der Getriebekomplex in der Mitte, der die richtigen Untersetzungsverhältnisse für Minuten- und Stundenzeiger liefert.