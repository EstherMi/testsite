---
layout: "image"
title: "Blicke ins Getriebe (2)"
date: "2017-10-22T19:43:34"
picture: "geraeuschlosehzuhrmitneodymsekundenwelle09.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46837
imported:
- "2019"
_4images_image_id: "46837"
_4images_cat_id: "3468"
_4images_user_id: "104"
_4images_image_date: "2017-10-22T19:43:34"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46837 -->
Hier das obere Z10 von oben. Und ist gerade noch das Z10 im Bild zu sehen, welches auf der Achse des Z20 sitzt, das von der Kette rechts im Bild angetrieben wird.