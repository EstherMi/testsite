---
layout: "image"
title: "Führung der drei Zeiger"
date: "2017-10-22T19:43:34"
picture: "geraeuschlosehzuhrmitneodymsekundenwelle02.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46830
imported:
- "2019"
_4images_image_id: "46830"
_4images_cat_id: "3468"
_4images_user_id: "104"
_4images_image_date: "2017-10-22T19:43:34"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46830 -->
Die lange Achse selbst trägt den Sekundenzeiger, der über einen Statik-Mitnehmer (31712) und Klemmringen darauf befestigt ist.

Der Minutenzeiger wird durch das Loch im Z30 durch die Kombination folgender Teile geführt: Z15, Zangenmutter (31915), Hülse 15 (31983), wieder eine Zangenmutter und wieder ein Z15. Die Hülse klemmt prima in den beiden Zangenmuttern (mit denen man auch die großen Schneckenendstücke auf Achsen schraubt), und das ganze dreht sich noch wunderbar leichtgängig auf der Achse.

Der Stundenzeiger ist mittels eines Seilklemmstifts (107356) mit dem Z30 verbunden. Das Z30 wird unten vom Z10 getragen und angetrieben, und es wird ansonsten einfach nur von den Führungsplatten links und rechts in Position gehalten.