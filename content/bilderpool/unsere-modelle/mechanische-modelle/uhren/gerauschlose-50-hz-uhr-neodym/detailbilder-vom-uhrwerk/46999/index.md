---
layout: "image"
title: "Durchführung für den Minutenzeiger"
date: "2017-12-12T13:21:34"
picture: "detailbildervomuhrwerk13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46999
imported:
- "2019"
_4images_image_id: "46999"
_4images_cat_id: "3478"
_4images_user_id: "104"
_4images_image_date: "2017-12-12T13:21:34"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46999 -->
Hier die Einzelteile der Durchführung durch das Loch des Z30: Je zwei Z15 sitzen auf den Klemmhülsen. Die stecken auf einem Abstandshalter 15. Das klemmt prima und gibt eine kompakte Durchführung. Am ausgehenden Z15 kann sich dann irgendwas einklinken, z.B. der Minutenzeiger mit einem S-Riegel.