---
layout: "image"
title: "Trafo herausgenommen"
date: "2017-12-12T13:21:34"
picture: "detailbildervomuhrwerk02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46988
imported:
- "2019"
_4images_image_id: "46988"
_4images_cat_id: "3478"
_4images_user_id: "104"
_4images_image_date: "2017-12-12T13:21:34"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46988 -->
Den Trafo kann man nach unten aus der Uhr herausschieben.