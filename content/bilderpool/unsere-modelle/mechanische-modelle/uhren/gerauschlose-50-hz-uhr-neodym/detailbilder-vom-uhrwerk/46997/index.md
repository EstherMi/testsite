---
layout: "image"
title: "Detailblick in die Zeigeraufhängung"
date: "2017-12-12T13:21:34"
picture: "detailbildervomuhrwerk11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46997
imported:
- "2019"
_4images_image_id: "46997"
_4images_cat_id: "3478"
_4images_user_id: "104"
_4images_image_date: "2017-12-12T13:21:34"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46997 -->
Hier sieht man gut, wie die Zeiger aufgehängt sind.

Das Z30 vom Stundenzeiger wird zwischen BS5x15x30 und E-Magnet-Halteplatte geführt und gegen Kippen geschützt. Unten sitzt es einfach auf dem Z10 auf, das es antreibt.

Der Minutenzeiger wird von der Kette über Z15, Durchführung, Z15, S-Riegel im Minutenzeiger, Minutenzeiger angetrieben. Man sieht auch gut das nachträglich angebrachte Hemmungs-Gummi, das um die BS5x15x30 gezogen wurde. Es liegt an der Unterseite der Durchführung an und verhindert so recht wirkungsvoll ein Wackeln des Minutenzeigers, das ansonsten durch die Reibung auf der pulsierenden Sekundenachse entstehen konnte.

Der Sekundenzeiger sitzt auf der durchgehenden Achse und ist mit einem Statik-Halter, der in die Streben und Klemmringe passt, befestigt.