---
layout: "image"
title: "Kegelgetriebe abgenommen"
date: "2017-12-12T13:21:34"
picture: "detailbildervomuhrwerk10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46996
imported:
- "2019"
_4images_image_id: "46996"
_4images_cat_id: "3478"
_4images_user_id: "104"
_4images_image_date: "2017-12-12T13:21:34"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46996 -->
Hier sind auch die Kegelgetriebe abgebaut, die vom Z40 aus angetrieben wurden.