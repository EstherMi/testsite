---
layout: "image"
title: "Schrägansicht (2)"
date: "2017-10-22T19:43:34"
picture: "geraeuschlosehzuhrmitneodymsekundenwelle05.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46833
imported:
- "2019"
_4images_image_id: "46833"
_4images_cat_id: "3468"
_4images_user_id: "104"
_4images_image_date: "2017-10-22T19:43:34"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46833 -->
Vom Z40 geht es weiter über ein paar Rast-Winkelzahnräder. Nach vorne über ein Z10 per Kette auf das hintere Z15 des Minutenzeiger-Trägers (der, wie weiter vorne beschrieben, lose drehend auf der Sekundenachse sitzt). Damit haben wir 1:40 x 10:15 = 1:60, und der Minutenzeiger hat, was er braucht.

Nach hinten geht ein zweites Winkelzahnrad auf Z10, Kette Z20 (also 1:2), von da auf Z10 direkt auf ein Rast-Z20 (das sieht man später noch besser), und von dort nach vorne auf ein Z10 nach Z15, und von da aufs Z10 unter dem Z30 des Stundenzeigers. Damit haben wir 1:40 x 1:2 x 1:2 x 10:15 x 1:3 = 1:720 = 1:60 x 1:12 - richtig für die Übersetzung zwischen Sekunden- und Stundenzeiger.

Und, ich sag nur: Kabelhalter. Was ich ja gar nicht leiden kann, sind aus lauter Bequemlichkeit unordentlich verkabelter Modelle *hust* *räusper* *hust*... Nix will ich hören ;-)