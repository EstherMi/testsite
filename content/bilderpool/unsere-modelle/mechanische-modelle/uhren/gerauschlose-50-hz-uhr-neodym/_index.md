---
layout: "overview"
title: "Geräuschlose 50-Hz-Uhr mit Neodym-Sekundenwelle"
date: 2019-12-17T19:19:22+01:00
legacy_id:
- categories/3468
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3468 --> 
Eine kompakte Variante einer Uhr mit Direktantrieb des Sekundenzeigers unter Verwendung kleiner Neodym-Magnete.