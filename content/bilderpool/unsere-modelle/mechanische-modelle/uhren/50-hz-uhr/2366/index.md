---
layout: "image"
title: "50-Hz-Uhr"
date: "2004-04-23T18:19:01"
picture: "50-Hz-Uhr_006F.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/2366
imported:
- "2019"
_4images_image_id: "2366"
_4images_cat_id: "235"
_4images_user_id: "104"
_4images_image_date: "2004-04-23T18:19:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2366 -->
