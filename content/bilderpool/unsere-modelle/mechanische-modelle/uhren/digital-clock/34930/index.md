---
layout: "image"
title: "Digital Clock"
date: "2012-05-12T17:08:07"
picture: "digitalclock03.jpg"
weight: "3"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- details/34930
imported:
- "2019"
_4images_image_id: "34930"
_4images_cat_id: "2586"
_4images_user_id: "1505"
_4images_image_date: "2012-05-12T17:08:07"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34930 -->
A ring. The rope pulley 21 nicely fits in the triangle formed by the angular block 60 and the building block 7,5.