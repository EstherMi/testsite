---
layout: "image"
title: "Digital Clock v1"
date: "2012-05-12T17:08:07"
picture: "digitalclock11.jpg"
weight: "11"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- details/34938
imported:
- "2019"
_4images_image_id: "34938"
_4images_cat_id: "2586"
_4images_user_id: "1505"
_4images_image_date: "2012-05-12T17:08:07"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34938 -->
These motors turn the axles with the triangular rings. The gear wheel T30 has 3 convenient holes to put (custom cut) short axles with clips in, that push the mini switch after exactly 1/3 turn.