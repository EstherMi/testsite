---
layout: "image"
title: "Digital Clock"
date: "2012-05-12T17:08:07"
picture: "digitalclock06.jpg"
weight: "6"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- details/34933
imported:
- "2019"
_4images_image_id: "34933"
_4images_cat_id: "2586"
_4images_user_id: "1505"
_4images_image_date: "2012-05-12T17:08:07"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34933 -->
Layout of the digits and ring positions to form them. The RoboPro program has a list of these positions, and calculates the difference between the current position and the next one. That determines which rings need to turn, either one or two steps.