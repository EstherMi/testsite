---
layout: "image"
title: "Digital Clock v2"
date: "2012-05-12T17:08:08"
picture: "digitalclock16.jpg"
weight: "16"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- details/34943
imported:
- "2019"
_4images_image_id: "34943"
_4images_cat_id: "2586"
_4images_user_id: "1505"
_4images_image_date: "2012-05-12T17:08:08"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34943 -->
Not much cleaner on the back...