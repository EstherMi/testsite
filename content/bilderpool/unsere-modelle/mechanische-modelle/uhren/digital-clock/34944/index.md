---
layout: "image"
title: "Digital Clock v2"
date: "2012-05-12T17:08:08"
picture: "digitalclock17.jpg"
weight: "17"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- details/34944
imported:
- "2019"
_4images_image_id: "34944"
_4images_cat_id: "2586"
_4images_user_id: "1505"
_4images_image_date: "2012-05-12T17:08:08"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34944 -->
The bottom. The same driving and control as the v1 clock, but now hidden from view.