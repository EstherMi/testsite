---
layout: "image"
title: "Ziffernblatt"
date: "2015-07-25T14:07:40"
picture: "IMG_1298_-_Kopie.jpg"
weight: "1"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/41462
imported:
- "2019"
_4images_image_id: "41462"
_4images_cat_id: "3100"
_4images_user_id: "724"
_4images_image_date: "2015-07-25T14:07:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41462 -->
