---
layout: "image"
title: "50 Hz-Motor"
date: "2015-07-25T14:07:40"
picture: "IMG_1871_-_Kopie.jpg"
weight: "3"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/41464
imported:
- "2019"
_4images_image_id: "41464"
_4images_cat_id: "3100"
_4images_user_id: "724"
_4images_image_date: "2015-07-25T14:07:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41464 -->
Hier gibts einen Zeitraffer:
https://www.youtube.com/watch?v=VQf2YRXs3rI