---
layout: "image"
title: "Uhrwerk (2)"
date: "2018-05-20T17:43:49"
picture: "leichtstellbareminianaloguhrmitschrittmotor6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47680
imported:
- "2019"
_4images_image_id: "47680"
_4images_cat_id: "3515"
_4images_user_id: "104"
_4images_image_date: "2018-05-20T17:43:49"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47680 -->
Hier sieht man das bis dahin beschriebene Getriebe von der anderen Seite.

----------

This image shows the gear, as described so far, from the other side.