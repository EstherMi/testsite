---
layout: "image"
title: "Frontansicht"
date: "2018-05-20T17:43:49"
picture: "leichtstellbareminianaloguhrmitschrittmotor1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47675
imported:
- "2019"
_4images_image_id: "47675"
_4images_cat_id: "3515"
_4images_user_id: "104"
_4images_image_date: "2018-05-20T17:43:49"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47675 -->
Die Uhr ist recht kompakt - sie hat ein Getriebe mit feiner Verzahnung. Angetrieben wird sie von einem Schrittmotor. Da das nur alle 60 Sekunden geschieht, ist sie geräuschlos und im Umschaltmoment immer noch flüsterleise.

----------

The clock is relatively compact - it uses a gear with the fine fischertechnik gearings. It gets driven using a stepper motor. Because this happens every 60 seconds only, the clock is totally quiet, and even in the switching moment whisper-quiet.