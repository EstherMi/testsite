---
layout: "overview"
title: "Leicht stellbare Mini-Analoguhr mit Schrittmotor"
date: 2019-12-17T19:19:27+01:00
legacy_id:
- categories/3515
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3515 --> 
Diese kleine Uhr wird von einem Schrittmotor gedreht, der alle 60 Sekunden von einem Netduino 3, der in C# (.net) programmiert ist, angesteuert wird. Man kann die Uhrzeit auch während des Laufs einfach einstellen.