---
layout: "image"
title: "Unterseite"
date: "2018-05-20T17:43:49"
picture: "leichtstellbareminianaloguhrmitschrittmotor3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47677
imported:
- "2019"
_4images_image_id: "47677"
_4images_cat_id: "3515"
_4images_user_id: "104"
_4images_image_date: "2018-05-20T17:43:49"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47677 -->
Auf der Unterseite sitzt ein Netduino 3 Microcontroller, auf dem das nanoFramework (https://nanoframework.net/) läuft und das also komfortabel mit Visual Studio in C# (.net) programmiert werden kann. Ganz unten sitzt ein Adafruit Motor Shield V2.3, an dem der Schrittmotor angeschlossen ist. Da ich meine Adafruits alle ständig verkabelt habe, laufen alle unbenutzten Kabel einfach in unbestückte Leuchtbausteine. Die Uhr steht auf Federn, damit auch nicht der Hauch einer Erschütterung auf die Tischplatte gelangt und die ungewollt zum Resonanzkörper wird.

----------

At the bottom side there is a Netduino 3 micro controller running nanoFramework (https://nanoframework.net/) and so is most comfortably programmable using Visual Studio and C# (.net). Underneath there is an Adafruit Motor Shield V2.3 driving the stepper motor. Because I have pre-cabled all my Adafruits, the unused cables just end in empty lamp sockets. The clock sits on springs so that not even a small vibration gets transferred to the tabletop as a potential resonating body.