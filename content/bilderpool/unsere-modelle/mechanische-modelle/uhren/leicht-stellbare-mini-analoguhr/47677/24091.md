---
layout: "comment"
hidden: true
title: "24091"
date: "2018-05-26T09:31:14"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
So ungefähr ;-) Deshalb läuft auch der Schrittmotor nur mit 20% PWM-Kraft. Das genügt für den zuverlässigen Lauf völlig, und macht die Sache halt noch leiser, weil die "Erschütterung" beim Schritt viel kleiner ist.


Gruß,
Stefan