---
layout: "image"
title: "Rückansicht"
date: "2018-05-20T17:43:49"
picture: "leichtstellbareminianaloguhrmitschrittmotor2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47676
imported:
- "2019"
_4images_image_id: "47676"
_4images_cat_id: "3515"
_4images_user_id: "104"
_4images_image_date: "2018-05-20T17:43:49"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47676 -->
Der Schrittmotor ist einer vom Ur-Plotter-Baukasten von fischertechnik. Er treibt über ein Z10 das schwarze Z25 (Danke, Roland!) an. An diesem kann man auch einfach mit dem Daumen drehen, um die Uhr jederzeit - auch während des Laufs - schnell zu stellen.

----------

The stepper motor is from the first fischertechnik plotter kit. It has a Z10 (a gear wheel with 10 tooth) attached, which drives the black Z25 (Thanks, Roland, for this rarity!). One can just turn on this wheel using a thumb to set the clock at any time - including when it is running.