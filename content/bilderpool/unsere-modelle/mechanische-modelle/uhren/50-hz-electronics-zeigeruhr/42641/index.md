---
layout: "image"
title: "Arbeitsweise (2) - Elektronik"
date: "2016-01-02T14:25:48"
picture: "hzelectronicszeigeruhrmitsekundenzeiger07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42641
imported:
- "2019"
_4images_image_id: "42641"
_4images_cat_id: "3172"
_4images_user_id: "104"
_4images_image_date: "2016-01-02T14:25:48"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42641 -->
Das flachere Modul ist ein 2015er Electronics-Modul. Das ist so verschaltet, dass von zwei Zählern 1:5 jeweils die verdoppelten Ausgänge 1:10 verwendet und hintereinandergeschaltet werden. Letztlich wird also das 50-Hz-Eingangssignal zwei Mal durch 10, insgesamt also durch 100 geteilt. Am Ausgang gibt es also ein Rechtecksignal mit einer Periodenlänge von zwei (!) Sekunden. Eine ganze Sekunde lang ist der Ausgang auf +9V und dann eine ganze Sekunde auf 0V. Das wird ans zweite Modul übergeben, einem als ODER-Glied eingestellten E-Tec-Modul. Das wird nur deshalb benötigt, weil beim Zählermodul kein Ausgang das negierte Signal ausgibt (alle Ausgänge dienen bei der Zähleinstellung des Electronics-Moduls als verschiedene Teilerausgänge). Die beiden Leitungen der gegeneinander inversen Ausgänge des E-Tecs gehen in die andere Hälfte des Modells zu den Tastern.