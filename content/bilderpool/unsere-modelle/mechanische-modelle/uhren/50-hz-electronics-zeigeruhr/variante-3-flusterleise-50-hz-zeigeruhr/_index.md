---
layout: "overview"
title: "Variante 3: Flüsterleise 50-Hz-Zeigeruhr mit Sekundenzeiger und Schrittmotor"
date: 2019-12-17T19:19:05+01:00
legacy_id:
- categories/3186
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3186 --> 
In dieser Variante wurde der Antrieb nochmal umgestellt, und zwar auf einen Selbstbau-Schrittmotor, der ein Mal pro Sekunde getaktet wird. So läuft die Uhr praktisch völlig geräuschlos.