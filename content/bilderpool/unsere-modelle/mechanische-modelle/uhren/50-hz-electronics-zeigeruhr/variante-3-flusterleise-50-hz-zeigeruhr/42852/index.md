---
layout: "image"
title: "Schrittmotor"
date: "2016-01-30T17:54:49"
picture: "fluesterleisehzzeigeruhrmitsekundenzeiger2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42852
imported:
- "2019"
_4images_image_id: "42852"
_4images_cat_id: "3186"
_4images_user_id: "104"
_4images_image_date: "2016-01-30T17:54:49"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42852 -->
Sechs Dauermagnete aus dem alten fischertechnik-Elektromechanik-Programm werden von zwei Elektromagneten wie ein Motor angesteuert. Allerdings macht dieser Motor nur genau einen Schritt pro Sekunde, und ein Schritt ist auch nur 1/24 Umdrehung.

Vorne ist etwas für Roland Enzenhofer: Ein echtes fischertechnik Z25, was leider nie auf den Markt kam, ist hier verbaut. Wenn der Motor nämlich 1/24 Umdrehung pro Sekunde macht (wie und warum er das macht, sehen wir später), eine Minute aber 60 Sekunden hat, brauchen wir - wiedermal! - eine Untersetzung 2,5:1 (24 * 2,5 = 60). Da passt ein Z10 auf ein Z25 so wunderbar! Danke, Roland, für die Raritäten!

Es ginge aber auch anders: a) ein Selbstbau-Z50 wie in http://www.ftcommunity.de/details.php?image_id=27016, b) viel eleganter geometers 2:5-Planetengetriebe in http://www.ftcommunity.de/details.php?image_id=42476, oder c) wenn man es schafft (was ich noch probieren will), fünf Magnete im Kreis anzuordnen. Dann würde nämlich ein Z30 anstatt ein Z25 passen. Ich habe mit Haralds Fünfecken in http://www.ftcommunity.de/categories.php?cat_id=2368 und der ft:pedia experimentiert, aber noch ohne Erfolg. Die Abstände zwischen den Magneten dürfen nicht zu groß werden dafür.