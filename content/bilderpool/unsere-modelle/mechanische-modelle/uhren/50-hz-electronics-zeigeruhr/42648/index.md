---
layout: "image"
title: "Arbeitsweise (8) - Übergang zum Stundenrad"
date: "2016-01-02T14:25:48"
picture: "hzelectronicszeigeruhrmitsekundenzeiger14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42648
imported:
- "2019"
_4images_image_id: "42648"
_4images_cat_id: "3172"
_4images_user_id: "104"
_4images_image_date: "2016-01-02T14:25:48"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42648 -->
Die vorhin schon erwähnte, gegenüber der Sekundenwelle 1:40 untersetzte Achse des Z40 links trägt auch ein Z10. Man beachte, dass die Achse des Z40 nicht den Minutenzeiger repräsentiert, denn der ist dieser Achse gegenüber ja vorne bei den Zeigern nochmal per Z20:Z30 um einen Faktor 1,5 untersetzt. Wir brauchen also zwischen der Z40-Welle nicht etwa eine Untersetzung 1:12 für den Stundenzeiger, sondern 1:12 * 2:3 = 2:36 = 1:18. Von den 1:18 erhalten wir durch die hier gezeigte zweifache Untersetzung Z10:Z30 schon mal 1:3 * 1:3 = 1:9. Es fehlt also noch eine Untersetzung 1:2.