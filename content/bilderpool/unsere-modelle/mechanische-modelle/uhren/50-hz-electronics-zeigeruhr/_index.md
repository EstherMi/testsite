---
layout: "overview"
title: "50-Hz-Electronics-Zeigeruhr mit Sekundenzeiger"
date: 2019-12-17T19:19:02+01:00
legacy_id:
- categories/3172
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3172 --> 
Dieses Modell benutzt die Zähler-Funktionen des 2015er Electronics-Moduls zum Takten einer relativ kompakten Zeigeruhr mit Sekundenzeiger.