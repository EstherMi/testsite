---
layout: "image"
title: "Frontansicht"
date: "2016-01-02T14:25:48"
picture: "hzelectronicszeigeruhrmitsekundenzeiger01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42635
imported:
- "2019"
_4images_image_id: "42635"
_4images_cat_id: "3172"
_4images_user_id: "104"
_4images_image_date: "2016-01-02T14:25:48"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42635 -->
Die Uhr und ihre gesamte Mechanik werden von einem doppelt ausgeführten senkrechten Bausteinstrang in der Mitte getragen. Die Elektronik findet auf einer Grundplatte 120 x 60 Platz. Das Netzteil - ein altes fischertechnik-Netzgerät mit 50-Hz-Wechselspannungsausgang - steht separat.