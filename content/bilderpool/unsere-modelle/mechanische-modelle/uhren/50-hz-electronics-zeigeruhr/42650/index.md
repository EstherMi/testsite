---
layout: "image"
title: "Arbeitsweise (10) - Gleitführung des Stundenrades"
date: "2016-01-02T14:25:48"
picture: "hzelectronicszeigeruhrmitsekundenzeiger16.jpg"
weight: "16"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42650
imported:
- "2019"
_4images_image_id: "42650"
_4images_cat_id: "3172"
_4images_user_id: "104"
_4images_image_date: "2016-01-02T14:25:48"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42650 -->
Die rechte, tragende Gleitführung ist mit zwei Winkelsteinen um 60° geneigt und greift an genau der richtigen Stelle in die hintere Drehscheibe des Stundenrades ein. Die obere Gleitführung hat nur die Aufgabe, das Stundenrad gegen Kippen nach vorne oder hinten zu sichern. Die Uhr lief bereits einige Tage lang wunderbar zuverlässig durch.

That's it :-)