---
layout: "image"
title: "Variante mit optischer Abtastung - Halbschritt 'Zahn'"
date: "2016-01-06T18:24:13"
picture: "optischeabtastung03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42666
imported:
- "2019"
_4images_image_id: "42666"
_4images_cat_id: "3174"
_4images_user_id: "104"
_4images_image_date: "2016-01-06T18:24:13"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42666 -->
Hier unterbricht gerade ein Zahn des Z30 die Lichtschranke.