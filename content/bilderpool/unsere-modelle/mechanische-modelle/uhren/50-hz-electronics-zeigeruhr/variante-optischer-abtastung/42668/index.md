---
layout: "image"
title: "Variante mit optischer Abtastung - Bauweise"
date: "2016-01-06T18:24:13"
picture: "optischeabtastung05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42668
imported:
- "2019"
_4images_image_id: "42668"
_4images_cat_id: "3174"
_4images_user_id: "104"
_4images_image_date: "2016-01-06T18:24:13"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42668 -->
Die Befestigung der Lampe und des Fototransistors ist einfach, alles muss aber genau justiert werden.