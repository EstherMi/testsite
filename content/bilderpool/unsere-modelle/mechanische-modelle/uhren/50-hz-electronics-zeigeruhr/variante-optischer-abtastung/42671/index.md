---
layout: "image"
title: "Variante mit optischer Abtastung - Zusätzliche Untersetzung"
date: "2016-01-06T18:24:13"
picture: "optischeabtastung08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42671
imported:
- "2019"
_4images_image_id: "42671"
_4images_cat_id: "3174"
_4images_user_id: "104"
_4images_image_date: "2016-01-06T18:24:13"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42671 -->
Ein anderer Blickwinkel auf die Getriebeerweiterung.