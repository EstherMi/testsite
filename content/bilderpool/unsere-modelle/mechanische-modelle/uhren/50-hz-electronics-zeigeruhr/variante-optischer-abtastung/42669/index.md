---
layout: "image"
title: "Variante mit optischer Abtastung - Doppelter Vorwiderstand"
date: "2016-01-06T18:24:13"
picture: "optischeabtastung06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42669
imported:
- "2019"
_4images_image_id: "42669"
_4images_cat_id: "3174"
_4images_user_id: "104"
_4images_image_date: "2016-01-06T18:24:13"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42669 -->
Die E-Tecs reagierten zu träge und es wurden Halbschritte übersprungen. Eine Maßnahme dagegen war die Serienschaltung von zwei anstatt nur einer Lampe mit dem Motor, um ihn noch langsamer (und nebenbei noch leiser) laufen zu lassen.