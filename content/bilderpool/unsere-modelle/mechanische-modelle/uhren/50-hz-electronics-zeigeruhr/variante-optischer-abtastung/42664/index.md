---
layout: "image"
title: "Variante mit optischer Abtastung - Überblick"
date: "2016-01-06T18:24:13"
picture: "optischeabtastung01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42664
imported:
- "2019"
_4images_image_id: "42664"
_4images_cat_id: "3174"
_4images_user_id: "104"
_4images_image_date: "2016-01-06T18:24:13"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42664 -->
Da das Klicken des Tasters auf Dauer von manchen Mitbewohnern als störend empfunden wurde, gibt es hier noch eine Variante mit optischer Abtastung des Z30.