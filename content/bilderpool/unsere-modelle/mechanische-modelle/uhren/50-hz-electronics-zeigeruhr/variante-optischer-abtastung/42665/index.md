---
layout: "image"
title: "Variante mit optischer Abtastung - Lichtschranke"
date: "2016-01-06T18:24:13"
picture: "optischeabtastung02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42665
imported:
- "2019"
_4images_image_id: "42665"
_4images_cat_id: "3174"
_4images_user_id: "104"
_4images_image_date: "2016-01-06T18:24:13"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42665 -->
Eine Glühlampe strahlt am Rand des Z30 entlang auf einen Fototransistor. Eine ft-LED war übrigens nicht hell genug.