---
layout: "image"
title: "Variante mit optischer Abtastung - Halbschritt 'Zwischenraum'"
date: "2016-01-06T18:24:13"
picture: "optischeabtastung04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42667
imported:
- "2019"
_4images_image_id: "42667"
_4images_cat_id: "3174"
_4images_user_id: "104"
_4images_image_date: "2016-01-06T18:24:13"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42667 -->
Im nächsten Halbschritt (also der nächsten Sekunde, 1/60 Umdrehung) wird die Lichtschranke durch den Zahnzwischenraum freigegeben.