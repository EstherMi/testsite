---
layout: "image"
title: "Rückseite"
date: "2016-01-02T14:25:48"
picture: "hzelectronicszeigeruhrmitsekundenzeiger04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42638
imported:
- "2019"
_4images_image_id: "42638"
_4images_cat_id: "3172"
_4images_user_id: "104"
_4images_image_date: "2016-01-02T14:25:48"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42638 -->
Die Achse direkt über dem Z40 trägt den Sekundenzeiger. Der Motor treibt das Z30 oben an. Zahn für Zahn wird mit einem Verlängerungshebel vom großen Taster detektiert.