---
layout: "image"
title: "Arbeitsweise (7) - Durchführung der Minutenwelle"
date: "2016-01-02T14:25:48"
picture: "hzelectronicszeigeruhrmitsekundenzeiger13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42647
imported:
- "2019"
_4images_image_id: "42647"
_4images_cat_id: "3172"
_4images_user_id: "104"
_4images_image_date: "2016-01-02T14:25:48"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42647 -->
Die beiden Rastachsen stecken vorne wiederum in einem BS15 mit Bohrung. Die sind einfach völlig lose in zwei der vier Öffnungen eingeschoben, die diese Bausteine seitlich haben. Danach wurde der hier sichtbare BS15 nach links gedreht, bis die Rastachsen an den Wänden des BS15 anschlugen. Der wird dann mit zwei Klemmringen stabil dagegen gesichert, nach vorne herausgeschoben zu werden. Das ergibt eine kraftschlüssige Verbindung zwischen den beiden BS15, die durch die Öffnung der Drehscheiben und des Z40 komfortabel hindurch passt. Auf dem vorderen BS15 ist mittels einer S-Adapterlasche (31674) der Minutenzeiger angebracht.