---
layout: "image"
title: "Mechanik - Übertrag zu den Stundenlampen"
date: "2015-11-27T12:13:59"
picture: "hzklartextuhr08.jpg"
weight: "17"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42394
imported:
- "2019"
_4images_image_id: "42394"
_4images_cat_id: "3155"
_4images_user_id: "104"
_4images_image_date: "2015-11-27T12:13:59"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42394 -->
Links unten im Bild sieht man die Lasche, die ganz leicht exzentrisch im BS 7,5 steckt. Genau alle Stunde dreht sie das Z30 um genau einen Zahn weiter.

Der Antrieb hat dafür ja nur 1/12 Umdrehung zur Verfügung, kann also auch das Z30 um bestenfalls 1/12 weiterdrehen. Und das Drehen muss ja in einem Schritt erfolgen. In dieser Anordnung braucht der Mitnehmer das Z30 also nur um 1/30 Umdrehung zu drehen, und das ist wenig genug, dass das beim Wechsel der richtigen 5 Minuten erfolgt.

Erst beim Bau des Modells bemerkte ich übrigens, dass der Wechsel der Stundenlampen ja gar nicht zur vollen Stunde geschehen darf, sondern beim Wechsel von "viertel nach" (einer Stunde) auf "zehn vor halb" (der nächsten Stunde nämlich).

Die Sperrklinke in Form der zweiten Lasche, rechts vom Z30, sorgt dafür, dass das Getriebe nicht etwa durch Federkraft (siehe später) zurück verstellt wird.

Wir haben somit also 1/30 Umdrehung, die wir auf zwölf Lampen verteilen müssen. Wir müssen also aus 1/30 Umdrehung 1/12 Umdrehung machen. Das geht über ein selbstgebautes Z50 (Ur-ft-Raupengummi um Innenzahnrad, darum eine Kette mit 50 Gliedern geschwungen, siehe auch http://www.ftcommunity.de/details.php?image_id=27016). 1:30 / 1:12 = 0,4 = 2/5. Also gibt es hier das Getriebe Z50 auf Z20. Das weitere Z30 ist nur ein Zwischenzahnrad, um genug Platz für den Stunden-Stromverteiler zu bekommen.

Ich hatte versucht, die 2:5-Übersetzung mit einem Differential zu realisieren. Da hier aber "ins Schnelle" übersetzt werden muss, war das zu schwergängig und hakte. Also musste wieder das Selbstbau-Z50 herhalten. Ich wünsche mir ein Z25 von fischertechnik.