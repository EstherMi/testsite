---
layout: "overview"
title: "50-Hz-Klartext-Uhr"
date: 2019-12-17T19:18:56+01:00
legacy_id:
- categories/3155
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3155 --> 
Diese Uhr geht zwar genau (getaktet durch das 50-Hz-Stromnetz), zeigt aber die Uhrzeit sehr entspannend nur auf fünf Minuten genau und in ganzen Worten an.