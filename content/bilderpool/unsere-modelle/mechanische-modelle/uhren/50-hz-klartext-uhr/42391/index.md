---
layout: "image"
title: "Elektronik - Electronics-Modul und E-Tecs"
date: "2015-11-27T12:13:59"
picture: "hzklartextuhr05.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42391
imported:
- "2019"
_4images_image_id: "42391"
_4images_cat_id: "3155"
_4images_user_id: "104"
_4images_image_date: "2015-11-27T12:13:59"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42391 -->
Das Electronics-Modul links zählt also die 50 Hz und teilt sie auch gleich durch 24 und nochmal durch 5. Die linken drei der vier E-Tecs bilden die drei höchstwertigen Bits des Zählers bis 125. Das rechte E-Tec-Modul wird vom Monoflop angesteuert und ist als ODER-Glied eingestellt. Damit haben wir einen feinen, geräuschlosen Treiberbaustein für den Motor (das hobby-4-Relais würde ja alle 5 Minuten "klacken"). Zusätzlich ist an diesem Oder-Glied ein zweiter Taster angeschlossen. Mit dem kann man die Uhr im Dauerlauf auf die richtige Stunde stellen.