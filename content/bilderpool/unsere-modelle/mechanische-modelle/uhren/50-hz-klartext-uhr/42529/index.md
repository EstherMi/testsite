---
layout: "image"
title: "Display hinter Acrylglas im Alu-Rahmen"
date: "2015-12-20T13:46:57"
picture: "ft_ZeitwortUhr_front.jpg"
weight: "5"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Acryl", "Glas", "Display", "Alu"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/42529
imported:
- "2019"
_4images_image_id: "42529"
_4images_cat_id: "3155"
_4images_user_id: "579"
_4images_image_date: "2015-12-20T13:46:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42529 -->
