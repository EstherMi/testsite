---
layout: "image"
title: "Entwurf Display mit Hintergrundbild"
date: "2015-12-20T13:46:57"
picture: "ft_Display_ZeitwortUhr.jpg"
weight: "4"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Uhr", "Zeitwort", "Display", "WS2812B", "WS2811", "WS2812", "leuchtschwarz"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/42528
imported:
- "2019"
_4images_image_id: "42528"
_4images_cat_id: "3155"
_4images_user_id: "579"
_4images_image_date: "2015-12-20T13:46:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42528 -->
Stefans tolle Idee hat mich zu einem Weihnachtsgeschenk inspiriert. Meine Partnerin hat sie leuchtschwarze Zeitwort-Uhr genannt.