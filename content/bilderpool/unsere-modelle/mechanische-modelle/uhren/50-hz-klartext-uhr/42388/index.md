---
layout: "image"
title: "Frontansicht"
date: "2015-11-27T12:13:59"
picture: "hzklartextuhr02.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42388
imported:
- "2019"
_4images_image_id: "42388"
_4images_cat_id: "3155"
_4images_user_id: "104"
_4images_image_date: "2015-11-27T12:13:59"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42388 -->
18 Lampen stecken in dem Gitter-Gerippe, welches vorne mit einem simplen bedruckten Blatt Papier bespannt ist. Die jeweils gerade benötigten Lampen leuchten auf und wechseln nur alle fünf Minuten.