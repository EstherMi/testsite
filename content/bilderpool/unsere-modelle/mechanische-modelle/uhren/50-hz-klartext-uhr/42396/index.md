---
layout: "image"
title: "Mechanik - Stundenverteiler"
date: "2015-11-27T12:13:59"
picture: "hzklartextuhr10.jpg"
weight: "19"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42396
imported:
- "2019"
_4images_image_id: "42396"
_4images_cat_id: "3155"
_4images_user_id: "104"
_4images_image_date: "2015-11-27T12:13:59"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42396 -->
Das hier ist nicht etwa das Raumschiff aus "Interstellar", sondern der Stromverteiler für die Stundenlampen. Die Feder greift den der Drehachse zugeführten Strom ab und verteilt ihn auf zwölf Metallachsen 30. An jedem hängt ein Klemmkontakt mit einer Leitung zu je einer der zwölf Stundenlampen.

Jede Stunde (immer beim Wechsel von "viertel nach" auf "zehn vor halb") wird die Feder also durch das Getriebe um genau 1/12 Umdrehung weitergeschaltet. Die Feder liegt dann mit leichtem Druck an der jeweiligen Kontaktachse und stellt so zuverlässig Kontakt her.

Die weiteren 12 Kunststoffachsen und die zwölf BS7,5 dienen als Isolator, damit die Klemmkontakte sich nicht gegenseitig berühren können.