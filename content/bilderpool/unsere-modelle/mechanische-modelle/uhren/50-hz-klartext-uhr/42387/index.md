---
layout: "image"
title: "Nachtansicht"
date: "2015-11-27T12:13:59"
picture: "hzklartextuhr01.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42387
imported:
- "2019"
_4images_image_id: "42387"
_4images_cat_id: "3155"
_4images_user_id: "104"
_4images_image_date: "2015-11-27T12:13:59"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42387 -->
Die jeweils gerade "angesagten" Wörter leuchten auf.