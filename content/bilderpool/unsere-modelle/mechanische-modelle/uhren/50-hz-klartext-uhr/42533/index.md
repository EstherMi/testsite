---
layout: "image"
title: "Rückseite Detail Arduino Nano"
date: "2015-12-20T13:46:57"
picture: "ft_ZeitwortUhr_nano.jpg"
weight: "9"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Nano", "Controller", "Mikrocontroller", "AVR", "ATMEGA328P"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/42533
imported:
- "2019"
_4images_image_id: "42533"
_4images_cat_id: "3155"
_4images_user_id: "579"
_4images_image_date: "2015-12-20T13:46:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42533 -->
Die Durchkontaktierung durch die Rückseite läuft über 3 Pins