---
layout: "image"
title: "Absenkmechanik"
date: "2010-05-17T21:02:54"
picture: "prototypeinerhaengedachuhr4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/27283
imported:
- "2019"
_4images_image_id: "27283"
_4images_cat_id: "1959"
_4images_user_id: "104"
_4images_image_date: "2010-05-17T21:02:54"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27283 -->
Die Schnecke rechts unten sitzt auf der Achse des Z10 des vorherigen Bildes. Die Ziffern hängen alle mittels Verbindern 45 zwischen zwei Bausteinschienen. An einer Stelle ist die Schiene ein kurzes Stück absenkbar, geführt an den langen senkrechten Metallachsen. Die Drehscheibe senkt und hebt die gerade in der Absenkeinheit befindliche Ziffer mittels der Streben, die es auf der anderen Seite symmetrisch nochmal gibt.