---
layout: "image"
title: "Motor von hinten"
date: "2011-09-10T17:53:45"
picture: "Motor3.jpg"
weight: "5"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- details/31782
imported:
- "2019"
_4images_image_id: "31782"
_4images_cat_id: "2371"
_4images_user_id: "1088"
_4images_image_date: "2011-09-10T17:53:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31782 -->
Der Kranz mit 5 Flachträgern trägt 38 Supermagnete mit alternierenden Polaritäten. Zwei Positionen im Kranz sind unbesetzt wegen der Montierung. Wegen der 40 Magnetpositionen dreht sich der Motor mit einer Umdrehung pro 0,4 Sekunden: In 10 ms ändern die Elektromagneten ihre Polarität. In dieser Zeit macht der Motor 1/40 einer Umdrehung. Durch die Wahl dieser Drehgeschwindigkeit kann man eine Schnecke im Getriebe sparen. Wenn man die Sekundenanzeige dieser Uhr mit der einer Quarzuhr vergleicht, kann man sehr gut die Schwankungen der Netzfrequenz während eines Tages beobachten, die sich nach Ablauf eines Tages aber wieder sehr gut herausgemittelt haben.