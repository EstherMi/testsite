---
layout: "image"
title: "Rechte Seite"
date: "2011-09-10T17:53:45"
picture: "RechteSeite.jpg"
weight: "2"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- details/31779
imported:
- "2019"
_4images_image_id: "31779"
_4images_cat_id: "2371"
_4images_user_id: "1088"
_4images_image_date: "2011-09-10T17:53:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31779 -->
Das Modell läßt sich in der Mitte in die eigentliche Uhr und den 50Hz-Motor teilen.