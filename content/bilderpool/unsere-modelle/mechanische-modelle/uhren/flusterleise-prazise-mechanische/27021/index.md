---
layout: "image"
title: "Übertrag von der Zehn-Minuten-Achse auf die Stunden"
date: "2010-04-30T00:11:59"
picture: "fluesterleisepraezisemechanischedigitaluhr14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: ["complication"]
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/27021
imported:
- "2019"
_4images_image_id: "27021"
_4images_cat_id: "1943"
_4images_user_id: "104"
_4images_image_date: "2010-04-30T00:11:59"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27021 -->
Hier sieht man die Rückseite des Uhrwerks. In Bildmitte das eben beschriebene Z36, jetzt links davon das Z30, was nach oben abgeht zur vorletzten Walze.

Bleibt noch die Stundenwalze. Da wird's jetzt ein bisschen eklig mit den Verhältnissen. Nach den wenigen kurzen Sätzen bislang bleibt also jetzt eine etwas ausführlichere Beschreibung dem nicht erspart, der wissen will, was da so unangenehm ist. ;-)

Wir brauchen alle 60 Minuten genau 1/24 Umdrehung des Stundenrades. Immer wenn die Zehn-Minuten-Walze von 5 (der 59 nämlich) zurück auf 0 gedreht wird, muss die Stundenwalze um genau eine der 24 Zahlen weiter gedreht werden. Im ersten Ansatz wollte ich besonders schlau sein und habe einfach zwei gegenüberliegende Förderkettenglieder als Mitnehmer eines Z30 verwendet. Zwei Mal pro Umdrehung des Zehn-Minuten-Rades (wie gewünscht, sie trägt die Ziffern 0 - 5 ja doppelt) wird das Z30 um drei Zähne weiter gedreht. Noch eine passende Untersetzung dahinter, und fertig. Dachte ich jedenfalls.

Pustekuchen. Is nich. Der Mitnehmer hätte nämlich 12 Sekunden benötigt, um die drei Zähne weiter zu schalten (denkt's mal durch - es würde sich zwei Mal pro Stunde drehen, legt also in den 6 Schaltsekunden nur die Hälfte des benötigten Winkels zurück).

Die Lösung dafür ist aber recht billig: Eine Übersetzung 1:2 davor, nur ein Mitnehmer wie bei den anderen Überträgen, und in genau den 6 Sekunden vor Vollendung der Stunde wird das hier ganz rechts außen sitzende Z30 um genau drei Zähne weiter geschaltet.

Wir haben jetzt also 1/10 Umdrehung jede Stunde. Also brauchen wir eine Untersetzung 24:10, oder, ums noch hässlicher auszudrücken, 2,4:1, um aus der 1/10 Umdrehung 1/24 Umdrehung zu machen - genau um eine der 24 Stundennummern weiter.

Auch hier hilft das schon bekannte Z36: Ein Z15 treibt es an. 36:15 kann man mit 3 kürzen, das ergibt 12:5, und mit 2 erweitern, was die erwünschten 24:10 ergibt. Das Z36 ist nur ganz außen in dem roten BS15 mit Loch, aber stabil, gelagert und trägt ein Z30, welches das Z30 der Stundenwalze antreibt.

Geschafft. Die Stundenwalze dreht sich damit in den 6 Sekunden vor Vollendung jeder Stunde um genau eine Zahl weiter, was ein mehr oder weniger einfältiges Grinsen ins Gesicht des Konstrukteurs getrieben haben dürfte.

Verlässlich überliefert ist dazu aber nichts. Die Welt wird sich wohl auch ohne Kenntnis dieses Details irgendwie weiterdrehen.