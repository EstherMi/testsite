---
layout: "comment"
hidden: true
title: "11447"
date: "2010-04-30T23:50:07"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Tach Thomas,

so wie hier, mit gemeinsamen Minuspol, addiert sich ja nichts (ich nehme an, da sind wir einig, oder verstand ich Dich falsch?), und dann ist die 0/1-Sache ja, wie Du ja auch sagst, einfach Definitionssache. Was für eine Pufferschaltung braucht man denn für den Anschluss von TTLs? Reicht da nicht ein komfortabel hochohmiger Spannungsteiler?

Gruß,
Stefan