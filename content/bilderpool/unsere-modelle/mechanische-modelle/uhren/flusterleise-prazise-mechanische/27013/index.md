---
layout: "image"
title: "Schaltplan"
date: "2010-04-30T00:11:58"
picture: "fluesterleisepraezisemechanischedigitaluhr06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/27013
imported:
- "2019"
_4images_image_id: "27013"
_4images_cat_id: "1943"
_4images_user_id: "104"
_4images_image_date: "2010-04-30T00:11:58"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27013 -->
Tja, von nix kommt nix, also kommt hier mal Butter bei die Fische: Hier die vollständige Schaltung der Uhr. Ein paar der vielleicht interessanteren Schaltungstricks sind:

- Der Selbstbau-Trafo links oben versorgt die Eingänge E1 (über das Potentiometer als Schutzwiderstand) und E2 des Grundbausteins mit potentialfreier 50-Hz-Wechselspannung.

- Da, wie sich herausstellte, das Tastverhältnis leider ziemlich unsymmetrisch ist, lieferten A1 und A2 des Grundbausteins nicht beide genügend Signalstärke zur Ansteuerung der weiteren Elektronikbausteine. Deshalb wurde ein OR-NOR zwischengeschaltet, welches nur der Verstärkung dient.

- Erste Versuche mit einem für die späteren Getriebe geeigneten Teiler aus Flipflops ergaben, dass eine Teilung durch 5 angenehm ist - aber letztlich leider eine zu geringe Drehzahl des Motors ergab. Der drehte sich dann zwar besonders gemächlich mit nur 12,5 Umdrehungen pro Minute, lieferte aber leider nicht mehr genügend Drehmoment (siehe http://www.youtube.com/watch?v=z3g7mUdsoxQ für diesen Prototypen). Deshalb wird die Frequenz hier zunächst verdoppelt: Sowohl vom normalen als auch vom invertierten Ausgang des OR-NOR-Bausteins werden über den Dyn. AND-Baustein die 0-1-Flanken durchgelassen und - das ist der Trick - beide dem CP-Eingang des ersten Flipflops zugeführt. Es ist übrigens offiziell erlaubt, die Ausgänge der Dyn.-AND-Bausteine auf diese Weise einfach zusammen zu schalten. Bei jedem Wechsel des 50-Hz-Signals, also 100 Mal pro Sekunde, gibt's also einen Puls am Flipflop.

- Die linken drei Flipflops sind zunächst mal als ganz normaler Binärteiler geschaltet und würden also durch 8 teilen. Wir wollen aber 5. Er soll also die Folge der Binärzahlen (mit den Ziffern wie üblich von rechts nach links zu lesen, also umgekehrt wie die Flipflop-Schaltung) 000-001-010-011-100-000... erzeugen. Nach der 4 (100) muss also zurück auf die 0 (000) geschaltet werden. Nun könnte man die gewaltsame Methode nehmen und einen AND-Baustein an die beiden äußeren Flipflops anschließen, der, sobald von 4 auf 5 (also von 100 auf 101) geschaltet wird, alle Flipflops auf 000 zurücksetzt. Die Sache hat nur einen Haken: Durch das Zurücksetzen des ersten Flipflops würde das zweite wieder getriggert. Die Schaltung würde nämlich von 101 nicht auf 000, sondern auf 010 springen. Es geht aber auch viel eleganter, nämlich direkt von 100 auf 000. Wir müssen ja nur dafür sorgen, dass der nächste Impuls nach 100 a) das erste Flipflop nicht triggert, aber b) das letzte zurücksetzt. Das wird durch die beiden roten Verbindungen erreicht. Das dritte Flipflop steht ja nur in diesem letzten Takt (100) auf 1, so dass durch Verbinden seines negierten Ausgangs Q- mit dem J-Vorbereitungseingang des ersten Flipflops dieses erste nur dann von 0 auf 1 schalten kann, wenn das letzte Flipflop auf 0 und nicht auf 1 steht. Damit hätten wir Aufgabe a) erfüllt - das erste Flipflop kann beim Stand 100 nicht auf 1 wechseln, sondern bleibt auf 0. Schließlich wird das dritte Flipflop einfach bei jedem Takt durch den Anschluss an seinem Puls-Rücksetzeingang RP, der auf 0-1-Flanken reagiert, zwangsweise zurückgesetzt. Da es ja sowieso nur im letzten Takt (bei 4, also 100 nämlich) überhaupt gesetzt ist, wird es also dort wie gewünscht beim nächsten Takt zurückgesetzt. Voilà, wir haben einen Teiler durch 5, der von 4 = 100b direkt (ohne den kurzzeitigen Zwischenstand 5 = 101b) wieder auf 0 = 000b zurückfällt.

- Allerdings haben wir jetzt ein extrem ungleichmäßiges Tastverhältnis an den Ausgängen des letzten Flipflops. In 4 von 5 Takten ist es zurückgesetzt, und nur in einem von 5 gesetzt. Um das wieder zu einem hinreichend gleichmäßigen Takt zu machen, teilt ein viertes Flipflop dieses Signal in seiner Frequenz nochmal durch 2. Alle 5 Takte wechselt es also seinen Zustand. Das letzte bisschen Unregelmäßigkeit kommt nur noch durch das ungleichmäßige Tastverhältnis direkt am Eingang des Operationsverstärkers zustande und tut der Sache keinen Abbruch.

- Der Motor muss ja sehr langsam, aber immer noch hinreichend kräftig drehen. Es darf ja kein Takt in der Mechanik verloren gehen, sonst liefe die Uhr nicht genau. Deshalb bedient sich der Motor zweier Elektromagnete (ganz rechts im Schaltbild), die immer abwechselnd umgeschaltet werden. Das wird durch Anschluss je eines als D-Flipflop eingestellten und als Binärzähler verdrahteten E-Tec-Moduls erreicht. Diese 4-Phasen-Ansteuerung wird auf dem nächsten Bild beschrieben.

- Damit sich die Silberlinge und die E-Tec-Moduln überhaupt verstehen, müssen sie sich einen Pol ihrer Stromversorgung teilen, um ein gemeinsames Potenzial zu verwenden. Die Silberlinge werden wie üblich von einem hobby-4-Gleichrichterbaustein versorgt, der am Wechselspannungsausgang eines fischertechnik-Trafos hängt. Die E-Tec-Moduln werden von ihrem eigenen Gleichspannungsnetzteil versorgt. Das gemeinsame Bezugspotenzial wird durch Verbinden der Minuspole der beiden Stromversorgungen erreicht. Bei den Silberlingen denkt man zwar üblicherweise in negativer Logik (-9 V ist die logische1 und 0 V ist die logische 0) und bei den E-Tecs in positiver Logik (0 V ist die logische 0 und +9 V die logische 1), aber das ist ja alles nur Definitionssache. Hauptsache, beide verwenden kompatible Spannungspegel bezogen auf ein gemeinsames Basispotenzial.

Mehrere tausend Dank gehen an Thomas Habig/Triceratops für seine hervorragenden Schaltbilder der Silberlinge, die hier unter http://www.ftcommunity.de/data/downloads/ebausteine/silberlinge.zip ladbar sind und die ich hier verwendet habe.