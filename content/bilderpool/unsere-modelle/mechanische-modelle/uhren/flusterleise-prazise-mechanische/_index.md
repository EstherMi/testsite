---
layout: "overview"
title: "Flüsterleise präzise mechanische Digitaluhr"
date: 2019-12-17T19:18:39+01:00
legacy_id:
- categories/1943
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1943 --> 
Eine fast unhörbar leise und deshalb absolut wohnzimmertaugliche mechanische Digitaluhr, die ihre Präzision aus dem 50-Hz-Takt des Stromnetzes bezieht - ganz ohne Computerei.