---
layout: "comment"
hidden: true
title: "11530"
date: "2010-05-11T21:55:21"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Stefan,
sehr interessant hier dein Kettentrieb, der den Motor nur? wie von dir beschrieben mit dem Drehmoment des Z30 beaufschlagt. Das ist hier mit dem Trum auch nur möglich bei einer! Drehrichtung des Motors hier im Uhrzeigersinn. Soll ja schliesslich keiner für zwei Drehrichtungen drauf reinfallen ... :o)
Gruss Ingo