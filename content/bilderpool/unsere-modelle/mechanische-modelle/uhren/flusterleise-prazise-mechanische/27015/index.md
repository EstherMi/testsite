---
layout: "image"
title: "Übergang zum Getriebe"
date: "2010-04-30T00:11:59"
picture: "fluesterleisepraezisemechanischedigitaluhr08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/27015
imported:
- "2019"
_4images_image_id: "27015"
_4images_cat_id: "1943"
_4images_user_id: "104"
_4images_image_date: "2010-04-30T00:11:59"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27015 -->
Bis hierher dreht sich der Motor also wie folgt: 50 Hz, aufgeputscht zu 100 Hz, geteilt durch 5 (20 Hz), geteilt durch 4 (5 Hz), auf die Magnete und den Rotor mit 12 Dauermagneten, der also mit 5 Hz / 12, also 5 * 60 / 12 = 25 Umdrehungen pro Minute dreht (eine Umdrehung ganz gemütlich in 2,4 Sekunden). Der gedämpft aufgehängte Motor überträgt sein Drehmoment über die hier sichtbare ganz locker angebrachte Kette auf ein fest gelagertes Z30, welches sich also auch 25 Mal pro Minute dreht.