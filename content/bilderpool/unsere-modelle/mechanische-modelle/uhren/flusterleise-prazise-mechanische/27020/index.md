---
layout: "image"
title: "Übertrag von der Einer- zur Zehnerstelle der Minuten"
date: "2010-04-30T00:11:59"
picture: "fluesterleisepraezisemechanischedigitaluhr13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/27020
imported:
- "2019"
_4images_image_id: "27020"
_4images_cat_id: "1943"
_4images_user_id: "104"
_4images_image_date: "2010-04-30T00:11:59"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27020 -->
Das Z30 rechts im Bild ist das der Einerstelle der Minuten vom letzten Bild. Auf seiner Achse sitzt wieder die gleiche Mimik mit dem (hier gerade auf der entfernten Seite befindlichen) einzelnen Förderkettenglied als Mitnehmer. Das nimmt das dahinter, in Bildmitte, sichtbare Z30 wieder um genau drei Zähne mit und dreht es also um 1/10 Umdrehung. Und schon legen sich die Sorgenfalten tief in die Stenkerdirne, äh, Denkerstirne: Für die Zehnerstelle der Minuten gibt es ja zwölf Ziffern (zwei Mal 0 - 5), nicht zehn. Wir brauchen also eine Untersetzung 12:10, um aus den 10 Schritten je Umdrehung des Z30 12 Schritte für die Zehn-Minuten-Walze zu machen.

Triceratops macht's in seiner Trickfibel (http://www.ftcommunity.de/data/downloads/beschreibungen/trickfibel.pdf) vor, wie man so was machen kann: Eine Kette mit 36 Gliedern passt genau und stramm genug um einen jüngeren fischertechnik-Reifen, den man im Bildhintergrund vor den Silberlingen erahnen kann. 36 Zähne, von einem Z30 angetrieben, ergibt also ein Untersetzungsverhältnis von 36:30, was nichts anderes ist als 12:10. Wunderbar - Dank Thomas Habigs genialer Tüfteleien.

Auf der Achse dieses Reifens sitzt wieder, hier rechts daneben, hinten, zu sehen, ein Z30, welches über ein weiteres Z40-Zwischenzahnrad glücklich das Z30 der Zehn-Minuten-Walze antreibt. Die schaltet also bei jedem Takt - genau in den 6 Sekunden vor Vollendung aller 10 Minuten! - um 1/12 Umdrehung weiter. Also genau wie gewollt um eine der zwölf Ziffern 0 - 5 und 0 - 5.