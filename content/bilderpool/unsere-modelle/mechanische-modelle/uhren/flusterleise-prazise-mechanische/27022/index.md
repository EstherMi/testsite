---
layout: "image"
title: "Abschluss"
date: "2010-04-30T00:12:00"
picture: "fluesterleisepraezisemechanischedigitaluhr15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: ["complication"]
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/27022
imported:
- "2019"
_4images_image_id: "27022"
_4images_cat_id: "1943"
_4images_user_id: "104"
_4images_image_date: "2010-04-30T00:12:00"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27022 -->
Hier nochmal ein letzter Querblick auf das ganze Zeugs. Und endlich sieht man mal den genauen Aufbau der Mitnehmer. Die Achse der Drehscheibe ragt in den BS15 mit Loch hinein. Nein, nicht weil sie ob ihrer langweiligen ab-und-zu-mal-ein-bisschen-Dreherei frieren würde, sondern natürlich weil das die Sache so schön zentriert.

Was noch zu bemerken wäre: Die Justage ist, ähm, "reizvoll". Der Reiz ist allerdings dem Brechreiz nicht unentfernt ähnlich. Was bin ich froh, dass fischertechnik Nuten hat, in denen man Bausteine auf irgendwas in der Größenordnung von 1/10 Millimeter justieren kann. Sonst nehmen die Mitnehmer nämlich nicht drei, sondern nur zwei Zähne mit, oder sie blockieren, wenn sie den Z30 zu nah angebracht wurden.

Wenn sie dann aber läuft, läuft sie, die Uhr. Zwei Tage Dauerlauf, praktisch nicht hörbar (außer wenn's ganz still ist und man genau hin hört, dann knistert der Motor ein ganz klein wenig), und die Zeit stimmt immer noch. Ich bin gespannt, wie lange sie durchhält.

Ach ja - sollte irgendjemand tatsächlich diesen ganzen Sermon durchgelesen haben (bei fischertechnikerns sollen ja öfters ein paar Irre rumlaufen) - DANKE!