---
layout: "overview"
title: "'Bahnhofs'-Uhr (Dirk Fox)"
date: 2019-12-17T19:18:51+01:00
legacy_id:
- categories/2580
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2580 --> 
Uhr mit separatem Sekundenzeiger, die - wie eine Bahnhofsuhr - den Sekunden- und Minutenzeiger nicht kontinuierlich, sondern diskret weiterschaltet.