---
layout: "image"
title: "Gesamtansicht der Uhr"
date: "2012-05-02T08:22:28"
picture: "bahnhofsuhr1.jpg"
weight: "1"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/34850
imported:
- "2019"
_4images_image_id: "34850"
_4images_cat_id: "2580"
_4images_user_id: "1126"
_4images_image_date: "2012-05-02T08:22:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34850 -->
Schon lange faszinieren mich die tollen Uhrenmodelle von Steffalk, Remadus und geometer. Die ft-Zahnräder sind einfach wie gemacht für ein Uhrwerk - mit einer Einschränkung: das Verhältnis 1:60 (Sekunden- zu Minutenzeiger) lässt sich nicht mit einem Getriebe aus Z10, Z15, Z20, Z30 und Z40 darstellen. Dafür benötigt man entweder ein Schneckengetriebe (1:x) - oder geometers Trick mit dem Differential (siehe ft:pedia 3/2011). Ein Schneckengetriebe hat zudem den Nachteil, dass die Achsen orthogonal zueinander stehen.

Beim Lesen von Steffalks Beitrag in ft:pedia 1/2012 kam mir eine andere Idee: Sein genialer Zähler-Antrieb (Rast-Kurbel auf Z10) eignet sich auch als 1:x-Antrieb für ein Uhrgetriebe ohne Schnecke - und mit parallelen Achsen. Ergänzt um eine Sperre wird daraus eine "tickende" Uhr, deren Sekundenzeiger sich nicht kontinuierlich bewegt, sondern diskret im Sekundentakt weiterschaltet - und nach 60 Sekunden den Minutenzeiger "betätigt". Damit "tickt" die Uhr wie eine Bahnhofsuhr: Alle 60 Sekunden bewegen sich Sekunden- und Minutenzeiger gleichzeitig einen Schritt weiter.