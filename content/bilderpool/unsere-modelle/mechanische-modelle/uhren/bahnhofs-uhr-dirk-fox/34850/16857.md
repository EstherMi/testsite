---
layout: "comment"
hidden: true
title: "16857"
date: "2012-05-20T21:40:01"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Im Download-Bereich gibt es jetzt auch eine ft-Designer-Datei der Uhr - mit Kinematik-Animation: http://www.ftcommunity.de/data/downloads/ftdesignerdateien/uhr05.zip
Gruß, Dirk