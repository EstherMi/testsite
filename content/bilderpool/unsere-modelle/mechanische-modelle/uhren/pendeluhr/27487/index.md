---
layout: "image"
title: "15-Antrieb"
date: "2010-06-13T13:46:59"
picture: "15-Antrieb.jpg"
weight: "10"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/27487
imported:
- "2019"
_4images_image_id: "27487"
_4images_cat_id: "1886"
_4images_user_id: "46"
_4images_image_date: "2010-06-13T13:46:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27487 -->
Unter dem Uhrwerk hängen an einem 320 cm langen Endlosriemen zwei Gewichte von 850g und 147 g. Hier ist gerade das schwerere Gewicht erkennbar. Riemen, Schnurrollen und Gewichte bilden einen sogenannten Huygens'schen Antrieb, der es gestattet, die Uhr im Betrieb aufzuziehen, ohne daß das Uhrwerk davon etwas bemerkt.

Wenn sich der Aufzugsmotor bewegt und das große Gewicht nach oben zieht, bleibt das Antriebsdrehmoment auf der im Uhrwerk rechten Schnurrolle unverändert.

An dieser Stelle möchte ich mich besonders bei den Firmen habasit in Eppertshausen und BEHAbelt in Glottertal bedanken, die mir freundlicherweise je einen Endlosriemen zur Verfügung gestellt haben. Diese Riemen eignen sich vorzüglich für fischertechnik-Anwendungen. Sie haben einen Durchmesser von 2 mm und von 3 mm. In der Uhr befindet sich gerade der dünnere der beiden Riemen, der mit einer Zugkraft von 4 N beaufschlagt wird und diesem aber noch gewachsen ist. Der 3 mm Riemen hält deutlich mehr.

Die verfügbare Fallhöhe des großen Gewichts beträgt 112 cm. In jeder Stunde senkt sich das Gewicht um 4,71 cm, so daß die Uhr nach dem Aufziehen eine Gangreserve von knapp 24 h hat. Die Leistung der Uhr beträgt somit 90 Mikrowatt, wobei die Hemmung selbst in der aktuellen Bauart gerade mal 25 Mikrowatt verbraucht. Aber das Zeigerwerk kommt ja noch hinzu.