---
layout: "image"
title: "20-Zeigerwerk"
date: "2010-07-04T14:04:17"
picture: "20-Zeigerwerk.jpg"
weight: "6"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/27603
imported:
- "2019"
_4images_image_id: "27603"
_4images_cat_id: "1886"
_4images_user_id: "46"
_4images_image_date: "2010-07-04T14:04:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27603 -->
Endlich erhält die Uhr Zeiger, damit die Zeit ablesbar wird.

Von links hinten nach rechts am Aufzugsmotor vorbei läuft die Minutenwelle, die jedoch absichtlich entgegengesetzt zum Uhrzeigersinn läuft. Das Uhrwerk steht ja koaxial zum Zeigerwerk hinter dem Pendel und an diesem muss ich vorbei. Hier helfen mir zwei Zahnräder Z20, die mir den Drehsinn zum Zeigerwerk umkehren und die mir den notwendigen Achsabstand am Pendel vorbei liefern.