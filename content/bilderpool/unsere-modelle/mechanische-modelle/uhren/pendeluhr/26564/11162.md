---
layout: "comment"
hidden: true
title: "11162"
date: "2010-03-07T17:43:44"
uploadBy:
- "remadus"
license: "unknown"
imported:
- "2019"
---
Hallo zusammen,

die umgesetzte Leistung der Maschine ist angehängtes Gewicht (kg) mal Erdbeschleunigung (9,81 m/s^2) mal Weg des Gewichts nach unten (m) dividiert durch die dafür gebrauchte Zeit (s). Das ergibt Watt.

Wo die Leistung verbraten wird, weiß ich dann leider noch nicht, aber es sind ja nur sehr wenige Komponenten an der ganzen Sache beteiligt.

Als dann
Remadus