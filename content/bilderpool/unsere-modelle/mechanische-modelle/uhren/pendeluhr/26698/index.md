---
layout: "image"
title: "13-Pendelaufhängung"
date: "2010-03-15T16:56:39"
picture: "13-Pendelaufhngung.jpg"
weight: "20"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/26698
imported:
- "2019"
_4images_image_id: "26698"
_4images_cat_id: "1886"
_4images_user_id: "46"
_4images_image_date: "2010-03-15T16:56:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26698 -->
Hier noch das Haus mit der Doppelfeder der Pendelaufhängung. Damit ist das Pendel jetzt auch reibungsfrei gelagert. Leider ist die Sache etwas verbaut, so daß man es nicht so toll sehen kann, aber am oberen Ende ragen die beiden Enden der ft-Schwingfeder heraus.