---
layout: "image"
title: "02-Hemmung"
date: "2010-02-21T22:18:18"
picture: "02-Hemmung.jpg"
weight: "23"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/26501
imported:
- "2019"
_4images_image_id: "26501"
_4images_cat_id: "1886"
_4images_user_id: "46"
_4images_image_date: "2010-02-21T22:18:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26501 -->
Hier der nächste Versuch. Das sieht schon fast aus wie echt.

Jetzt greifen die kleinen Rollen mit größerem Abstand in die Bolzen. Das macht den Pendelwinkel kleiner. Aus 10 Grad werden so 4 Grad.

Das macht sich auch prompt in der Energiebilanz bemerkbar. Der Leistungshunger dieser Hemmung geht runter auf 430 Mikrowatt.