---
layout: "image"
title: "08-Hemmung"
date: "2010-03-15T16:56:36"
picture: "08-Hemmung.jpg"
weight: "15"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/26693
imported:
- "2019"
_4images_image_id: "26693"
_4images_cat_id: "1886"
_4images_user_id: "46"
_4images_image_date: "2010-03-15T16:56:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26693 -->
Die Beobachtung der Hemmung im Betrieb zeigt, daß das Pendel nach links stärker angestoßen wird, als nach rechts. Das wird offensichtlich, wenn man sich die Berührsituation zwischen Hemmungsbolzen und Ankerrollen genauer ansieht.

Ganz oben ist die oszillierende Welle des Pendelantriebs gezeichnet, darunter die 12 Bolzen des rotierenden Hemmungstriebes. Drehrichtung ist im Uhrzeigersinn.

Die linke Seite (rot dargestellt) hat kein Problem: die Verbindungslinie zwischen Wellenmittelpunkt Pendelantrieb und Mittelpunkt Hemmungsrolle sowie die Verbindungslinie zwischen Mittelpunkt Hemmungsbolzen und Mittelpunkt Hemmungsrolle bilden ungefähr einen rechten Winkel. In dieser Situation fällt es dem Hemmungsantrieb leicht, ein nennenswertes Drehmoment auf die Welle des Pendelantriebs zu übertragen.

Ganz anders auf der rechten Seite (grün dargestellt): der Winkel ist hier deutlich ungünstiger. Das übertragene Drehmoment vom Hemmungsrad auf den Pendelantrieb und wesentlich kleiner oder fast null.

Damit ist klar, daß diese Hemmung für eine Pendeluhr nicht gut geeignet ist, wenn eine gewisse Ganggenauigkeit erzielt werden soll.