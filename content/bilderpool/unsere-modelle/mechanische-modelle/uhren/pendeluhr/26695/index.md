---
layout: "image"
title: "10-Hemmung"
date: "2010-03-15T16:56:38"
picture: "10-Hemmung.jpg"
weight: "17"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/26695
imported:
- "2019"
_4images_image_id: "26695"
_4images_cat_id: "1886"
_4images_user_id: "46"
_4images_image_date: "2010-03-15T16:56:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26695 -->
Die Hemmung kommt gleich in einen neuen Teststand.

Die beiden Ausleger vorne an der Hemmungsmaschine fassen das Pendel rechts und links an und führen es. Dabei ist der Drehpunkt der Pendelaufhängung und der Drehpunkt des Hemmungsankers dicht beieinander, um Reibungseffekte durch unterschiedliche Wege beim Pendeln zu vermeiden. Der Hemmungsanker könnte eigentlich direkt mit dem Pendel verbunden werden, das spart noch mehr Masse, aber dann wird mir die Mechanik zu steif und die Justage zu schwierig.

Die Hemmungsmaschine steht nur lose in dem Gebälk drin und kann leicht entnommen werden, wenn man sie zuvor etwas nach hinten schiebt.

Überhaupt Justage: Pendel, Hemmungsanker und Hemmrad müssen akkurat aufeinander justiert sein, damit die Hemmung gleichmäßig arbeitet. Wenige Zehntelmillimeter Unterschied sind da schon zu merken. Außerdem muß der Rahmen mit der Wasserwaage ausgerichtet werden, sonst ist die Justage futsch.