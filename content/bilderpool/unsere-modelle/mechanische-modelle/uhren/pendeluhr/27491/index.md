---
layout: "image"
title: "19-Gangabweichungsdiagramm"
date: "2010-06-13T14:58:59"
picture: "19-Diagramm.jpg"
weight: "14"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/27491
imported:
- "2019"
_4images_image_id: "27491"
_4images_cat_id: "1886"
_4images_user_id: "46"
_4images_image_date: "2010-06-13T14:58:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27491 -->
Diese Sensation hätte ich nie und nimmer geglaubt: Die letzte Justage war der Volltreffer. Nach 3 h Beruhigungslauf, damit das Pendel gleichmäßig arbeitet, habe ich die Aufzeichnung der Pendelschwingungen gestartet. Alle 384 Pendelschwingungen (soll 15 min sein) hat der Rechner seine interne Uhr mit der Pendeluhr verglichen und die Abweichung aufgeschrieben.

Die interne Uhr kann leider nur 1/60-Sekungen, die als Hundertstel ausgegeben werden. Das erklärt die Sprünge im Diagramm.

Die Abweichung betrug maximal 0,41 Sekunden. Fantastisch. Gestartet wurde die Aufzeichnung gegen 19 Uhr. Erst war die Uhr zu langsam, dann wurde sich allmählich schneller. Da sich der Raum, in dem die Uhr steht über Nacht leicht abgekühlt hat und damit das Pendel kürzer und schneller wird, bin ich wohl im Bereich der thermischen Stabilität angelangt.

Sollte dem so sein, dann müßte die Uhr in einem halbwegs temperaturstabilen Raum auf Dauer mit einer Gangabweichung von wenigen Sekunden am Tag laufen können. Der Beweis steht aber noch aus.

Im nächsten Schritt erhält sie noch ein Zeigerwerk und geht dann in den Dauerbetrieb. Dann werden wir ja sehen...