---
layout: "image"
title: "25-Making of"
date: "2011-08-07T17:42:21"
picture: "25-Making_of.jpg"
weight: "1"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/31545
imported:
- "2019"
_4images_image_id: "31545"
_4images_cat_id: "1886"
_4images_user_id: "46"
_4images_image_date: "2011-08-07T17:42:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31545 -->
Es gibt ein Video.

Für die Nahaufnahme habe ich die Uhr stützen müssen, denn aufgrund des Pendelgewichts taumelt der Turm um gut einen Millimeter. Das sieht im Film nicht gut aus.