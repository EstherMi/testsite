---
layout: "image"
title: "06-Profihemmung"
date: "2010-02-21T22:18:19"
picture: "Hemmung2.jpg"
weight: "27"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/26505
imported:
- "2019"
_4images_image_id: "26505"
_4images_cat_id: "1886"
_4images_user_id: "46"
_4images_image_date: "2010-02-21T22:18:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26505 -->
Dieses Bild muß man sich ganz langsam auf der Zunge zergehen lassen. (Quelle: Sattler Präzisionspendeluhren)

Ich bin mir noch nicht schlüssig, ob ich diesen kraftkonstanten Pendelantrieb in fischertechnik umsetze.

Schön wär's schon.