---
layout: "overview"
title: "Selbstaufziehende Pendeluhr"
date: 2019-12-17T19:18:25+01:00
legacy_id:
- categories/1463
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1463 --> 
Eine Pendeluhr mit zwei Gewichten, von denen jeweils eines die Uhr antreibt, während das andere entweder motorisch hochgezogen wird oder oben in Bereitschaft steht, um einzuspringen, wenn das andere unten angekommen ist.