---
layout: "image"
title: "Umschaltmechanismus von hinten"
date: "2008-11-07T16:44:36"
picture: "selbstaufziehendependeluhr04.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "flyingcat"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- details/16220
imported:
- "2019"
_4images_image_id: "16220"
_4images_cat_id: "1463"
_4images_user_id: "853"
_4images_image_date: "2008-11-07T16:44:36"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16220 -->
Der Motor in Bildmitte kurbelt bei Bedarf das Gewicht rechts nach oben. in der oberen Bildhälfte sind mittleres und oberes Differential zu erkennen. Die Kabel am unteren Bildrand gehen zu den Tastern, die die obere Endlage der Gewichte erkennen.