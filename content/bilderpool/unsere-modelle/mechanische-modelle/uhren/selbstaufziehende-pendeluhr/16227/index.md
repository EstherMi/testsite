---
layout: "image"
title: "Untere Endlage"
date: "2008-11-07T16:44:36"
picture: "selbstaufziehendependeluhr11.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "flyingcat"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- details/16227
imported:
- "2019"
_4images_image_id: "16227"
_4images_cat_id: "1463"
_4images_user_id: "853"
_4images_image_date: "2008-11-07T16:44:36"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16227 -->
Die beiden Taster, die erkennen wenn ein Gewicht unten angekommen ist.