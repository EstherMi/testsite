---
layout: "image"
title: "Hemmung"
date: "2008-11-07T16:44:35"
picture: "selbstaufziehendependeluhr02.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "flyingcat"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- details/16218
imported:
- "2019"
_4images_image_id: "16218"
_4images_cat_id: "1463"
_4images_user_id: "853"
_4images_image_date: "2008-11-07T16:44:35"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16218 -->
Dieses Prinzip funktioniert erstaunlich stabil - mittlerweile läuft die Uhr seit 36 Stunden. Für eine Umdrehung des Rades werden 12 Pendelschwingungen ausgeführt, eine Schwingung ist auf 1,25 Sekunden einreguliert - weiter hinten gibt es noch ein Bild, dass die Reguliermöglichkeit zeigt.