---
layout: "image"
title: "Steuerung"
date: "2008-11-07T16:44:36"
picture: "selbstaufziehendependeluhr10.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "flyingcat"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- details/16226
imported:
- "2019"
_4images_image_id: "16226"
_4images_cat_id: "1463"
_4images_user_id: "853"
_4images_image_date: "2008-11-07T16:44:36"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16226 -->
Kompressor und Steuerung der Ablauflogik. Der Kompressor läuft etwa alle 15 Minuten für 8 Sekunden an.