---
layout: "image"
title: "Umschaltmechanismus"
date: "2008-11-07T16:44:35"
picture: "selbstaufziehendependeluhr03.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "flyingcat"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- details/16219
imported:
- "2019"
_4images_image_id: "16219"
_4images_cat_id: "1463"
_4images_user_id: "853"
_4images_image_date: "2008-11-07T16:44:35"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16219 -->
Mein Ziel war es, die Uhr so zu konstruieren, dass die beiden Gewichte als Antrieb der Uhr an- und abgeschaltet werden und motorisch hochgezogen werden können. Dies geschieht über 3 Differentiale, die übereinander angeordnet sind. Das Z20 direkt über der sichtbaren Seilrolle gehört zum mittleren Differential, das untere liegt verdeckt hinter der Seilrolle und das oberste sichtbare Z20 hängt am oberen Differential. Vom mittleren Differential wird zentral der Abtrieb für die Hemmung getrieben, die beiden Seiten gehen jeweils über ein Z20 an unteres bzw. oberes Differential. An den oberen und unteren Differentialen hängt jeweils in der Mitte ein Motor zum Aufzug, an einem Ende der Abtrieb zum mittleren Differential über ein Z20 und am anderen Ende über eine Untersetzung eine Seilrolle. Zwischen die beiden Z20-Paare zwischen je einer Seite des mittleren Differentials und oberem/unterem Differential kann über einen Pneumatikzylinder ein drittes Z20 geschoben werden (hier das Z20 rechts im Bild), dass dann die Kraftübertragung auf der entsprechenden Seite des mittleren Differentials blockiert.

Auf diese Art kann jedes Gewicht umgeschalten werden zwischen
- Antrieb Pendeluhr (Motor aus, Blockier-Z20 nicht im Weg)
- Aufzug (Motor an, Blockier-Z20 verhindert Kraftübertragung zur Hemmung)
- Standby (Motor aus, Blockier-Z20 verhindert Kraftübertragung zur Hemmung).

Die Blockier-Z20 werden durch die Federkraft von roten Pneumatik-Zylindern in Blockierstellung gehalten und durch Lufdruck bei Bedarf aus dem Weg geräumt. Taster kontrollieren, ob das Blockier-Z20 auch wirklich aus dem Weg ist - wenn nicht, läuft der Kompressor an um den Luftdruck zu erhöhen. Somit läuft der Kompressor nur, wenn auch wirklich mehr Luftdruck gebraucht wird.