---
layout: "image"
title: "Zeigerantrieb"
date: "2008-11-07T16:44:36"
picture: "selbstaufziehendependeluhr06.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "flyingcat"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- details/16222
imported:
- "2019"
_4images_image_id: "16222"
_4images_cat_id: "1463"
_4images_user_id: "853"
_4images_image_date: "2008-11-07T16:44:36"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16222 -->
Die Untersetzung für Minuten- und Stundenzeiger.