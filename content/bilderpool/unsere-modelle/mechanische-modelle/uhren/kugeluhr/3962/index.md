---
layout: "image"
title: "Kugeluhr Kugelmaschine"
date: "2005-04-08T20:54:59"
picture: "02-Dispenserantrieb.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/3962
imported:
- "2019"
_4images_image_id: "3962"
_4images_cat_id: "341"
_4images_user_id: "46"
_4images_image_date: "2005-04-08T20:54:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3962 -->
Dieses Gerät vereinzelt die Kugeln aus dem Vorrat. Die Maschine läuft solange, bis eine herausfallende Kugel, die über die Metallachsen rollt, dort einen Schaltkontakt betätigt.