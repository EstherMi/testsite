---
layout: "image"
title: "Kugeluhr Wippe"
date: "2005-04-08T20:54:59"
picture: "03-Wippe.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/3963
imported:
- "2019"
_4images_image_id: "3963"
_4images_cat_id: "341"
_4images_user_id: "46"
_4images_image_date: "2005-04-08T20:54:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3963 -->
Die Wippe läßt die Kugeln entweder nach vorne auf die Minutenschiene laufen oder schwenkt zur vollen Stunde nach hinten um, damit dort die Stundenkugeln gesammelt werden.