---
layout: "image"
title: "Kugeluhr Gesamtmodell"
date: "2005-04-08T20:54:59"
picture: "01-Gesamtmodell.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/3961
imported:
- "2019"
_4images_image_id: "3961"
_4images_cat_id: "341"
_4images_user_id: "46"
_4images_image_date: "2005-04-08T20:54:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3961 -->
Die Seitenansicht zeigt die einzelnen Baugruppen der Uhr: Kugelmaschine, um einzelne Kugeln herzugeben, dann nach rechts die Wippe, die Minuten- von Stundenkugeln unterscheidet, nach links verlaufend die Kugelschienen, dann der Rücklauf nach unten in den Förderkorb.