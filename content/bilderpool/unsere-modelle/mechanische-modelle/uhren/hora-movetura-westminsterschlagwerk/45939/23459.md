---
layout: "comment"
hidden: true
title: "23459"
date: "2017-06-17T12:56:38"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Hallo,

ich finde die Uhr ebenfalls prima! Der Klang deines Schlagwerks passt sehr gut zum Stil der Uhr, da wären meiner Meinung nach eher die ft Klangstäbe Stilbruch.

FTbyIST schrieb: "[...] einem Microprozessor mit einer bis auf Millisekunden exakten internen Uhr die Stunde vorzugeben irgendwie lustig."

Zwar kann der Arduino Milli- und Mikrosekunden zählen, allerdings ist die Zeitmessung ohne Real Time Clock auf einen Tag (oder noch schlimmer eine Woche) betrachtet sehr ungenau (mit einigen Minuten Abweichung) und damit für ein Stundenschlagwerk ungeeignet. Deinen Ansatz, dass die mechanische Uhr dem Mikrocontroller die exakte Stunde mitteilt, halte ich für eine hervorragende Lösung für dieses Problem.

Gruß,
David