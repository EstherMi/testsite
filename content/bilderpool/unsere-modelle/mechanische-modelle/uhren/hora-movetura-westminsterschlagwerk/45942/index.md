---
layout: "image"
title: "Hora Movetura 04"
date: "2017-06-14T20:18:47"
picture: "IMG_7955_2.jpg"
weight: "5"
konstrukteure: 
- "ist"
fotografen:
- "ist"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FTbyIST"
license: "unknown"
legacy_id:
- details/45942
imported:
- "2019"
_4images_image_id: "45942"
_4images_cat_id: "3412"
_4images_user_id: "2145"
_4images_image_date: "2017-06-14T20:18:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45942 -->
Durch die Gabellichtschranke wird zur vollen Stunde durch die mechanische Uhr das Schlagwerk ausgelöst.
Sehen und hören kann man das Ganze hier: https://youtu.be/xm9n1IMXBu0