---
layout: "overview"
title: "Hora Movetura mit Westminsterschlagwerk"
date: 2019-12-17T19:19:17+01:00
legacy_id:
- categories/3412
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3412 --> 
Klassik meets Moderne

Ich habe vor einigen Jahren die Standuhr von M. Rohmann nachgebaut. Inzwischen habe ich ein Westminsterschlagwerk hinzugefügt. Dieses wird von einem Arduino Uno gesteuert. Der Stundenschlag wird durch ein Stück Papier, welches durch eine Gabellichtschranke fährt, ausgelöst. Dieses Papierstück ist rückwärtig an der Hora Movetura angebracht. Ansonsten ist das Schlagwerk unabhängig von der Uhr.
 Die jeweiligen Viertelstundenschläge löst der Arduino anschliesend selbstständig aus und wartet nach der dritten Viertelstunde wieder auf das Signal der Lichtschranke.