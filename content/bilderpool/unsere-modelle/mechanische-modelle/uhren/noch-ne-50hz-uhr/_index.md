---
layout: "overview"
title: "Und noch 'ne 50Hz Uhr"
date: 2019-12-17T19:18:53+01:00
legacy_id:
- categories/2710
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2710 --> 
Inspiriert on Andreas Püttmanns wunderschönen 50Hz-Uhr hab ich mich auch an einer Uhr mit Sekundenzeiger versucht. Sie ist ein wenig kompakter geraten und hat eine alternative Mechanik für die drei Zeiger. Die Mechanik erreicht nicht ganz die geniale Einfachheit von Andreas Uhr, manche Lösungen sind mir für meinen Geschmack etwas zu komplex geraten. Bin aber trotzdem recht stolz darauf. 