---
layout: "image"
title: "Rechte Seite"
date: "2013-01-25T19:32:47"
picture: "althzuhr03.jpg"
weight: "3"
konstrukteure: 
- "Helmut"
fotografen:
- "Helmut"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hamlet"
license: "unknown"
legacy_id:
- details/36509
imported:
- "2019"
_4images_image_id: "36509"
_4images_cat_id: "2710"
_4images_user_id: "1327"
_4images_image_date: "2013-01-25T19:32:47"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36509 -->
