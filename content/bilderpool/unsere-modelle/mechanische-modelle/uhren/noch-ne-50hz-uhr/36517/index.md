---
layout: "image"
title: "Rückseite"
date: "2013-01-25T21:53:44"
picture: "althzuhr11.jpg"
weight: "11"
konstrukteure: 
- "Helmut"
fotografen:
- "Helmut"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hamlet"
license: "unknown"
legacy_id:
- details/36517
imported:
- "2019"
_4images_image_id: "36517"
_4images_cat_id: "2710"
_4images_user_id: "1327"
_4images_image_date: "2013-01-25T21:53:44"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36517 -->
