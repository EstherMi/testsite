---
layout: "overview"
title: "Geräuschlose 50-Hz-Uhr mit Direktantrieb des Minutenzeigers"
date: 2019-12-17T19:19:18+01:00
legacy_id:
- categories/3416
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3416 --> 
Diese Zeigeruhr hat keinen Sekundenzeiger, läuft aber absolut geräuschlos und ist präzise 50-Hz-getaktet.