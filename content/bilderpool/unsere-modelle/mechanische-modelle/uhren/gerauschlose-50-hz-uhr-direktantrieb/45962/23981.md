---
layout: "comment"
hidden: true
title: "23981"
date: "2018-03-07T12:18:55"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Hallo Stefan,

läuft deine Synchronuhr noch? Ich bin kürzlich auf folgenden Artikel gestoßen: http://www.faz.net/aktuell/wirtschaft/schwankungen-im-stromnetz-in-europa-gehen-uhren-nach-15481078.html

Der Artikel beschreibt, weshalb viele Uhren, die die Netzfrequenz als Zeitgeber erhalten, derzeit nachgehen. Tritt bei dir das Problem auch auf? Es wäre nämlich besonders interessant, wenn man den Fehler mit einem Fischertechnik Modell veranschaulichen könnte.

Gruß
David