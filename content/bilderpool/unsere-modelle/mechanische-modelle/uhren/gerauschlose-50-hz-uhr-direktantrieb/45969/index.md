---
layout: "image"
title: "Stromversorgung und Taktsignal"
date: "2017-06-18T23:03:34"
picture: "hzuhrmitdirektantrieb08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/45969
imported:
- "2019"
_4images_image_id: "45969"
_4images_cat_id: "3416"
_4images_user_id: "104"
_4images_image_date: "2017-06-18T23:03:34"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45969 -->
Vom 50-Hz-Wechselspannungsausgang des alten ft-Trafos, der gleichzeitig als schwerer Unterbau zum stabilen Stand des Modells beiträgt, geht es zu den unteren Anschlüssen des hier sichtbaren ft-h4-Gleichrichterbausteins. Man sieht am linken der beiden Stecker einen weiteren - der geht zur Elektronik und führt also das 50-Hz-Signal. Der obere Steckerverhau dient der Stromversorgung der vier Elektronikmoduln und der Lampe.