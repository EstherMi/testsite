---
layout: "image"
title: "Rotor"
date: "2017-06-18T23:03:34"
picture: "hzuhrmitdirektantrieb05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/45966
imported:
- "2019"
_4images_image_id: "45966"
_4images_cat_id: "3416"
_4images_user_id: "104"
_4images_image_date: "2017-06-18T23:03:34"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45966 -->
Durch die 4-Phasen-Ansteuerung braucht die Uhr also vier Takte, um den Rotor um einen Dauermagneten weiter zu schalten. Da 15 Magnete auf dem Rotor stecken, ergibt das also 4 * 15 = 60 Schritte - perfekt für den Sekunden- oder Minutenzeiger einer Uhr. Deshalb kann diese Uhr einen "Direktantrieb des Minutenzeigers" verwenden - der Rotor *ist* der Minutenzeiger.

Für ein perfektes 15-Eck bräuchte man Winkelsteine 24°. Die gibt es leider nicht. Also wurden immer WS15° + WS7,5° verwendet, aber an drei Stellen WS30°. Das ergibt dann zusammen wieder (15 - 3) * (15° + 7,5°) + 3 * 30° = 360° sauber. Allerdings ist der Rotor eben kein perfekter Kreis, und die Magnete sitzen nicht alle exakt in gleichen Abständen. An drei Stellen wird der Rotor genau passend von der Drehscheibe auf einer Freilaufnabe getragen, an der Stelle mit dem WS30° in der Mitte zwischen zwei Aufhängungen hat der "Kreis" seinen größten Radios. Magnete und Ziffernblatt sind ausgelegt, mit diesem etwas ungleichmäßigen Radius klar zu kommen.