---
layout: "image"
title: "Getriebe und Sekundenlampe"
date: "2017-06-18T23:03:34"
picture: "hzuhrmitdirektantrieb07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/45968
imported:
- "2019"
_4images_image_id: "45968"
_4images_cat_id: "3416"
_4images_user_id: "104"
_4images_image_date: "2017-06-18T23:03:34"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45968 -->
Hier sieht man die andere Seite des Getriebes.

Die Lampe oben wird mit einer Frequenz von 1/2 Hz (also zwei Sekunden pro Schwingung) von einem der Elektronik-Moduln mit einem Ihrer Anschlüsse angesteuert. Der andere Pol der Lampe geht, in Serie geschaltet mit dem 1000-µF-Kondensator im zweiten ft-Gleichrichterbaustein, auf die Stromversorgung (hier muss die Polung des Elkos beachtet werden). Das bewirkt, dass der Elko mit der Lampe als Vorwiderstand zu einer Sekunde auf- und zur nächsten wieder entladen wird. Die Lampe als Vorwiderstand bewirkt, dass das ein paar 100 ms benötigt, und die Lampe leuchtet also sekündlich durch den Lade-/Entladestrom auf, geht aber langsam (parallel zur Lade-/Entladekurve des Kondensators) aus. Das ergibt ein ganz reizvolles Aufleuchten.