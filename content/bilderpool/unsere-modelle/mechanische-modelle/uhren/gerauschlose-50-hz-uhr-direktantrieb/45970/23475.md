---
layout: "comment"
hidden: true
title: "23475"
date: "2017-06-19T21:05:40"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Hallo,

das optionale Abschalten der Entprellung und die sinnvollen Teilerverhältnisse sind super. Das sind Funktionen, die in den Kästen vermutlich nicht unbedingt gebraucht werden und trotzdem in den Modulen vorkommen: Da schließe ich mich deinem Dank an die Entwickler natürlich an!

Gruß,
David