---
layout: "image"
title: "Elektronik 2 - Vier-Phasen-Ansteuerung"
date: "2017-06-18T23:03:34"
picture: "hzuhrmitdirektantrieb10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/45971
imported:
- "2019"
_4images_image_id: "45971"
_4images_cat_id: "3416"
_4images_user_id: "104"
_4images_image_date: "2017-06-18T23:03:34"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45971 -->
Die weitere Ansteuerung ist praktisch identisch zu der von früheren Uhren (siehe https://www.ftcommunity.de/categories.php?cat_id=1943 mit Schaltung https://www.ftcommunity.de/details.php?image_id=27013 und Ansteuerungsdiagramm https://www.ftcommunity.de/details.php?image_id=27014 und die drei Varianten unter https://www.ftcommunity.de/categories.php?cat_id=3172).

Rechts sieht man ein E-Tec-Modul als OR-Glied, was hier als einfaches NOT-Glied (Negation) verwendet wird. Das Minibots-Modul links ist als zwei D-Flip-Flops eingestellt und bildet den für die 4-Phasen-Ansteuerung notwendigen Rhythmus. Eines der Flip-Flops schaltet bei ansteigender Flanke (z.B. bei einer ungeraden Minute), das andere durch die Negation bei absteigender Flanke (z.B. bei einer geraden Minute). Durch Anschluss je eines der Pole der Magnete an den Ausgängen (vom E-Tec links die oberen beiden Ausgänge, normaler und invertierter also) und gemeinsamem Anschluss an einem einzigen Ausgang des anderen Flip-Flops (Ausgang ganz links oben) ergibt sich die 4-Phasen-Steuerung. Nach vier Minuten ist ein Zyklus beendet, und die nächsten vier Minuten können kommen.

Bis die Uhr irgendwann wieder zerlegt wird.