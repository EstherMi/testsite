---
layout: "image"
title: "Schnelles Stellen (2)"
date: "2009-06-23T16:02:24"
picture: "selbststellendeanaloguhr11.jpg"
weight: "17"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/24446
imported:
- "2019"
_4images_image_id: "24446"
_4images_cat_id: "1676"
_4images_user_id: "104"
_4images_image_date: "2009-06-23T16:02:24"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24446 -->
Hier eine Draufsicht auf das schnelle Stellen. Die Freilaufkupplung ist ab http://www.ftcommunity.de/details.php?image_id=24410 detailliert beschrieben. Sie wirkt so, dass per Motor schnell vorwärts gestellt, bei Stillstand des Motors aber immer noch minutenweise weitergeschaltet werden kann.