---
layout: "comment"
hidden: true
title: "13270"
date: "2011-01-21T00:51:17"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Jetzt verstehe ich schnaggels' Frage erst richtig: Ja, die *Spalten* 4/5 und 8/9 sind gleich im untersten Bild. Dennoch ändert sich von 4 auf 5 bzw. 8 auf 9 etwas, weil ja Taster um 6 Spalten verstetzt angebracht sind. Zwischen 4/5 ändert sich nichts, aber auf der gegenüberliegenden Schaltwalzenseite findet dort der Übergang von 4+6 = 10  auf 5+6 = 11 statt, und da ändert sich genau ein Tasterzustand. Genauso bei Spalte 8/9: Dem entsrpicht zeitgleich gegenüber 8-6 = 2 nach 9-6 = 3, und ebenfalls ändert sich was.

Gruß,
Stefan