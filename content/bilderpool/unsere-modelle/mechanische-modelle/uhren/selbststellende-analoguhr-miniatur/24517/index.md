---
layout: "image"
title: "Details zum Uhrwerk (2)"
date: "2009-07-08T21:11:44"
picture: "detailszumuhrwerk2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/24517
imported:
- "2019"
_4images_image_id: "24517"
_4images_cat_id: "1676"
_4images_user_id: "104"
_4images_image_date: "2009-07-08T21:11:44"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24517 -->
Ebenfalls von vorne unten, mit dem Stunden-Z30 und der Baugruppe um den vordersten BS30 abgenommen.