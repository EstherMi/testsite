---
layout: "image"
title: "Details zum Uhrwerk (6)"
date: "2009-07-08T21:11:44"
picture: "detailszumuhrwerk6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/24521
imported:
- "2019"
_4images_image_id: "24521"
_4images_cat_id: "1676"
_4images_user_id: "104"
_4images_image_date: "2009-07-08T21:11:44"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24521 -->
Hier von hinten rechts unten aufgenommen.