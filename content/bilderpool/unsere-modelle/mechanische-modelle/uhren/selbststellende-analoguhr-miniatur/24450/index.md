---
layout: "image"
title: "Stellen auf die volle Stunde (2)"
date: "2009-06-23T16:02:25"
picture: "selbststellendeanaloguhr15.jpg"
weight: "21"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/24450
imported:
- "2019"
_4images_image_id: "24450"
_4images_cat_id: "1676"
_4images_user_id: "104"
_4images_image_date: "2009-06-23T16:02:25"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24450 -->
Es ist tatsächlich der kleine Klemmring, der letztlich den Taster betätigt. Der dreht sich wie der Minutenzeiger von hier (hinten) aus gesehen nach links und lässt der Kufe genau zur Minute 00 Platz, in seinen Ausschnitt zu rutschen. Der Taster drückt nämlich von oben auf den zweiarmigen Hebel und ist froh, pünktlich zur vollen Stunde mal losgelassen zu werden.