---
layout: "comment"
hidden: true
title: "9523"
date: "2009-07-06T18:30:47"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Doch, aber da kann man ja max. 6 Unterbrecher aufstecken. Ich muss ja aber 12 Stellungen unterscheiden und wüsste nicht, wie ich das mit einem Schleifring machen könnte. Oder gibts da doch einen Weg?

Gruß,
Stefan