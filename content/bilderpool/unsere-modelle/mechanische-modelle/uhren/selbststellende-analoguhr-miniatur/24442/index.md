---
layout: "image"
title: "Stundenmessung (2)"
date: "2009-06-23T16:02:18"
picture: "selbststellendeanaloguhr07.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/24442
imported:
- "2019"
_4images_image_id: "24442"
_4images_cat_id: "1676"
_4images_user_id: "104"
_4images_image_date: "2009-06-23T16:02:18"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24442 -->
Hier sieht man das letzte Bit, welches 6 Stunden hintereinander gesetzt und die anderen 6 Stunden nicht gesetzt hat. Die Bestückung der Drehscheiben wird noch ausführlich im übernächsten Bild beschrieben.