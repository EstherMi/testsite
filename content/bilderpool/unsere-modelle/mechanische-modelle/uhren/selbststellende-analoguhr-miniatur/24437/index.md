---
layout: "image"
title: "Überblick"
date: "2009-06-23T16:02:17"
picture: "selbststellendeanaloguhr02.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/24437
imported:
- "2019"
_4images_image_id: "24437"
_4images_cat_id: "1676"
_4images_user_id: "104"
_4images_image_date: "2009-06-23T16:02:17"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24437 -->
Das Uhrwerk, also das 12:1-Getriebe zwischen Stunden- und Minutenzeiger ist nur ganz winzig vorne. Rechts wird festgestellt, auf welcher Stunde die Uhr gerade steht, mit dem Taster in Bildmitte wird festgestellt, ob der Minutenzeiger gerade auf 0 steht, links wird die Uhr per Motor und Freilaufkupplung schnell auf die richtige Stunde und per E-Magneten minutenweise weitergestellt.