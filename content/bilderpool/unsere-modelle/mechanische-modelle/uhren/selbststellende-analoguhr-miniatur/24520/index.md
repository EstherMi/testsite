---
layout: "image"
title: "Details zum Uhrwerk (5)"
date: "2009-07-08T21:11:44"
picture: "detailszumuhrwerk5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/24520
imported:
- "2019"
_4images_image_id: "24520"
_4images_cat_id: "1676"
_4images_user_id: "104"
_4images_image_date: "2009-07-08T21:11:44"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24520 -->
Dasselbe Motiv wie im vorigen Bild von direkt vorne aufgenommen.