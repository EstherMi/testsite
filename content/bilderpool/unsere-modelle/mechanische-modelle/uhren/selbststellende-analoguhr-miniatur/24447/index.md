---
layout: "image"
title: "Minutenweises Stellen (1)"
date: "2009-06-23T16:02:24"
picture: "selbststellendeanaloguhr12.jpg"
weight: "18"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/24447
imported:
- "2019"
_4images_image_id: "24447"
_4images_cat_id: "1676"
_4images_user_id: "104"
_4images_image_date: "2009-06-23T16:02:24"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24447 -->
Hier werden zwei Elektromagnete parallel angesteuert, um mehr Kraft zu bekommen. Bei einem Schaltvorgang in die nächste Minute werden diese Magnete umgepolt, also von aus auf "rechts", dann nach einer kleinen Pause nach "links" gepolt, dann nach einer Pause wieder ausgeschaltet. Die Pausen sind kleiner als eine Sekunde und geben der Mechanik Zeit, dem Willen der Magnete zu folgen.

Der darunter befindliche Dauermagnet wird also zunächst nach rechts und dann wieder nach links gezwungen. Die dunkelrote Platte 15x15 links davon ist ein Anschlag.