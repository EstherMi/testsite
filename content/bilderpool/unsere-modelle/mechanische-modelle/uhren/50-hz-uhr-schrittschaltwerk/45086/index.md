---
layout: "image"
title: "Schrittschaltwerk"
date: "2017-01-28T19:34:14"
picture: "uhrrmitschrittschaltwerk04.jpg"
weight: "5"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/45086
imported:
- "2019"
_4images_image_id: "45086"
_4images_cat_id: "3357"
_4images_user_id: "1126"
_4images_image_date: "2017-01-28T19:34:14"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45086 -->
Die Minutenachse (in der Bildmitte mit Segmentscheibe) treibt das Schrittschaltwerk an.