---
layout: "image"
title: "Funktionsweise des Uhrgetriebes"
date: "2017-01-28T19:34:14"
picture: "uhrrmitschrittschaltwerk03.jpg"
weight: "4"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/45085
imported:
- "2019"
_4images_image_id: "45085"
_4images_cat_id: "3357"
_4images_user_id: "1126"
_4images_image_date: "2017-01-28T19:34:14"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45085 -->
Die 500 U/min werden auf eine Minutenachse 1:500 untersetzt (Achse in der Bildmitte mit Segmentscheibe). Das gelingt mit drei Schneckengetrieben (Rastschnecke auf Z10) und einer Übersetzung 2:1 (Z20 auf Z10).