---
layout: "image"
title: "Detailansicht Schrittschaltwerk"
date: "2017-01-28T19:34:14"
picture: "uhrrmitschrittschaltwerk05.jpg"
weight: "6"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/45087
imported:
- "2019"
_4images_image_id: "45087"
_4images_cat_id: "3357"
_4images_user_id: "1126"
_4images_image_date: "2017-01-28T19:34:14"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45087 -->
Der Minutenzeiger wird einmal pro Minute mit diesem Schrittschaltwerk aus einer Drehscheibe 60 und einer Segmentscheibe Z12 um 1/60stel Umdrehung weiterbewegt. Dabei greift der rückseitige Pin der Segmentscheibe in eine Nut der Drehscheibe 60. Diese Bewegung wird dann noch einmal 1:10 untersetzt.
Der unten im Bild erkennbare Rollenhebel greift in die Nut der Drehscheibe 60 und verhindert ein ungewolltes Weiterdrehen der zugehörigen Achse, durch das das Schrittschaltwerk blockieren kann.