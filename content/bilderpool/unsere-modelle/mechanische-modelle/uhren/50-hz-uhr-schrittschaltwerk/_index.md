---
layout: "overview"
title: "50-Hz-Uhr mit Schrittschaltwerk"
date: 2019-12-17T19:19:13+01:00
legacy_id:
- categories/3357
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3357 --> 
Viele Analoguhren bewegen den Minutenzeiger nicht kontinuierlich, sondern schalten ihn schrittweise Minute für Minute weiter. Besonders gut gefiel mir das immer an Bahnhofsuhren...
Daher möchte ich die Sammlung der 50-Hz-Uhren im Bilderpool um eine mit Synchronuhr Schrittschaltwerk ergänzen. Bei der Konstruktion des Synchronmotors ließ ich mich von Hamlets Uhr mit sechs Neodym-Stabmagneten inspirieren.