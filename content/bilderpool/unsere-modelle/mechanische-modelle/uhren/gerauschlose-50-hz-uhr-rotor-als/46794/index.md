---
layout: "image"
title: "Linke Seite"
date: "2017-10-15T13:56:27"
picture: "geraeuschlosehzuhrmitrotoralssekundenwelle05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46794
imported:
- "2019"
_4images_image_id: "46794"
_4images_cat_id: "3464"
_4images_user_id: "104"
_4images_image_date: "2017-10-15T13:56:27"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46794 -->
Die waagerechte Achse vorne im Bild wird vor der linken (bzw. hinteren) Bauplatte 500 mit einer Freilaufnabe plus Klemmstücken beidseitig justiert. Ohne die Freilaufnabe würde der Klemmring sich evtl. in den Stegen der Platte verhaken.

Unten sieht man einen der beiden Elektromagnete glänzen.