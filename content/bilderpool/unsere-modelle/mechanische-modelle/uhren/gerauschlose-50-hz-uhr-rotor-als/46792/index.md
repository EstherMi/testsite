---
layout: "image"
title: "Ansicht von oben"
date: "2017-10-15T13:56:27"
picture: "geraeuschlosehzuhrmitrotoralssekundenwelle03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46792
imported:
- "2019"
_4images_image_id: "46792"
_4images_cat_id: "3464"
_4images_user_id: "104"
_4images_image_date: "2017-10-15T13:56:27"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46792 -->
Hier sieht man den großen Rotor, der von zwei darunterliegenden Elektromagneten angetrieben wird und den grünen Sekundenzeiger direkt antreibt. Von da geht es vom Z10 per Kette auf ein Z20, auf ein Winkelgetriebe und per Schnecke auf ein Z10 (hier verdeckt). Nach vorne geht's über Kette von Z10 auf Z30 auf den Minutenzeiger, der auf einer Freilaufnabe sitzt. Nach hinten geht's über Kette Z10-Z40, Kette Z10-Z30 nach vorne auf Kette Z10-Z30 zum ebenfalls auf einer Freilaufnabe befindlichen Stundenzeiger.