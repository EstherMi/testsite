---
layout: "image"
title: "Elektronik für die 4-Phasen-Ansteuerung"
date: "2017-10-15T13:56:27"
picture: "geraeuschlosehzuhrmitrotoralssekundenwelle06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46795
imported:
- "2019"
_4images_image_id: "46795"
_4images_cat_id: "3464"
_4images_user_id: "104"
_4images_image_date: "2017-10-15T13:56:27"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46795 -->
Die Elektronik macht dieselbe 4-Phasen-Steuerung der beiden Elektromagnete wie bei https://www.ftcommunity.de/categories.php?cat_id=3416 und bei https://www.ftcommunity.de/categories.php?cat_id=3186 und bei https://www.ftcommunity.de/categories.php?cat_id=1943 (wo die Steuerung auch ausführlich beschrieben ist).

Sie ist hier aber etwas vereinfacht: Da wir ja Sekunden und nicht Minuten zählen, brauchen wir nur ein Electronics-Modul, was als 1:50-Teiler geschaltet ist. Das Minibots-Modul rechts (ein Electronics würde auch gehen) bildet die beiden benötigten Flip-Flops. Das NOT-Glied dazwischen wird hier aber nicht mehr mit einem E-Tec-Modul realisiert, sondern einfach mit zwei Widerständen und einem Transistor, die sich ja eh im Electronics-Kasten mit dem Electronics-Modul befinden. Zum Nachbau der Steuerung genügt also ein Electronics-Kasten und entweder ein MiniBots- oder noch ein Electronics-Modul (oder zwei E-Tecs als FlipFlops). Nur die 50 Hz muss man wo herbekommen.