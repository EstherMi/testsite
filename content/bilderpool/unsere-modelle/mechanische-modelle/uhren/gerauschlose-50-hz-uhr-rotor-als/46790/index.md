---
layout: "image"
title: "Frontansicht"
date: "2017-10-15T13:56:27"
picture: "geraeuschlosehzuhrmitrotoralssekundenwelle01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46790
imported:
- "2019"
_4images_image_id: "46790"
_4images_cat_id: "3464"
_4images_user_id: "104"
_4images_image_date: "2017-10-15T13:56:27"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46790 -->
Die Uhr läuft geräuschlos mit 50-Hz-Präzision. Der Sekundenzeiger wird direkt vom selben Rotor angetrieben wie in https://www.ftcommunity.de/categories.php?cat_id=3416. Diese Fassung hier ist aber, außer dass sie einen Sekundenzeiger besitzt, auch wegen des schönen Kontrasts der Farben besser ablesbar.