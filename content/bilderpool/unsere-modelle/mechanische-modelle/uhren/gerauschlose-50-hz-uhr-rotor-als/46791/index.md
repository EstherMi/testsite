---
layout: "image"
title: "Rückseite"
date: "2017-10-15T13:56:27"
picture: "geraeuschlosehzuhrmitrotoralssekundenwelle02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46791
imported:
- "2019"
_4images_image_id: "46791"
_4images_cat_id: "3464"
_4images_user_id: "104"
_4images_image_date: "2017-10-15T13:56:27"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46791 -->
Man sieht den alten ft-Trafo, der an seinem seitlichen Wechselspannungsausgang die benötigten 50 Hz liefert, den Ein-/Ausschalter, den h4-Gleichrichterbaustein für die Stromversorgung der Elektronik-Module, eine Kettenuntersetzung zwischen Minuten- und Stundenzeiger sowie die Elektronische Steuerung.