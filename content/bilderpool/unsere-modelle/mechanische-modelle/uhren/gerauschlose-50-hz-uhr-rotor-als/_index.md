---
layout: "overview"
title: "Geräuschlose 50-Hz-Uhr mit Rotor als Sekundenwelle"
date: 2019-12-17T19:19:21+01:00
legacy_id:
- categories/3464
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3464 --> 
Genau wie ihr Vorgänger https://www.ftcommunity.de/categories.php?cat_id=3416 gibt es einen Direktantrieb eines Zeigers, aber hier ist es der Sekundenzeiger.