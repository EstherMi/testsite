---
layout: "image"
title: "Noch ein Blick von hinten auf die Anzeige"
date: "2008-09-16T21:35:35"
picture: "digitaluhrv08.jpg"
weight: "21"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/15275
imported:
- "2019"
_4images_image_id: "15275"
_4images_cat_id: "1396"
_4images_user_id: "104"
_4images_image_date: "2008-09-16T21:35:35"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15275 -->
Hier die Ziffern aus einer etwas anderen Perspektive.