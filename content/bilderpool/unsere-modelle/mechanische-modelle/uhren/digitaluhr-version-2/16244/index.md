---
layout: "image"
title: "Wagen (2)"
date: "2008-11-09T17:53:53"
picture: "digitaluhrvwagen02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/16244
imported:
- "2019"
_4images_image_id: "16244"
_4images_cat_id: "1396"
_4images_user_id: "104"
_4images_image_date: "2008-11-09T17:53:53"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16244 -->
Der Powermotor treibt über sein Ritzel einen "Getriebehalter mit Schnecke" an, der auf das Z15 links unten geht.