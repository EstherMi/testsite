---
layout: "image"
title: "Wagen (9)"
date: "2008-11-09T17:53:54"
picture: "digitaluhrvwagen09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/16251
imported:
- "2019"
_4images_image_id: "16251"
_4images_cat_id: "1396"
_4images_user_id: "104"
_4images_image_date: "2008-11-09T17:53:54"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16251 -->
Hier ein Detailblick auf eines der elektropneumatischen Ventile. Der Elektromagnet zieht an der Rückschlussplatte, die über den BS5 am hier oberen BS15 sitzt. An dessen anderer Seite steckt der auf einer roten Platte 15x45, die unter dem Pneumatikventil hindurch nach rechts geht, wo schließlich ein Winkelstein 60 drauf steckt. Das Ventil sitzt mit etwas Spiel, sodass die Anordnung leicht gezogen werden kann. Durch Einschalten des Magneten wird der Winkelstein gegen den Stößel des Ventils gedrückt - es schaltet durch. Der untere BS15 dient wegen der senkrechten Einbaulage als Stütze gegen Verkanten der Platte 15x45, genauso der ebenfalls mit etwas Spiel oben angesetzte BS5. Die Federkraft des Pneumatikventils genügt vollauf, die bewegliche Anordnung vom Winkelstein bis zur Rückschlussplatte beim Ausschalten des Magneten wieder so weit zurückzustoßen, dass das Ventil vollständig unbetätigt bleibt und keine Luft verliert, wenn dazu auch ein bisschen Feinjustage nötig ist.