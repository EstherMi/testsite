---
layout: "overview"
title: "Digitaluhr Version 2"
date: 2019-12-17T19:18:18+01:00
legacy_id:
- categories/1396
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1396 --> 
Nachdem die Digitaluhr für die Convention 2007 ja nur \"fast\" funktionierte (man konnte die Ziffern nicht lesen), gibt es jetzt hier Version 2 mit lesbarer Anzeige.