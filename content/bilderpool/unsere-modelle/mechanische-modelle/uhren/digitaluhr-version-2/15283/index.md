---
layout: "image"
title: "Verschieben des Geräteträgers (1)"
date: "2008-09-16T21:35:35"
picture: "digitaluhrv16.jpg"
weight: "29"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/15283
imported:
- "2019"
_4images_image_id: "15283"
_4images_cat_id: "1396"
_4images_user_id: "104"
_4images_image_date: "2008-09-16T21:35:35"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15283 -->
Hier sieht man den Gerätewagen von rechts hinten unten. Man sieht die Hinterteile der Taster für das Erreichen der Position ganz rechts (im Bild vorne links), für das Erreichen einer Ziffer (im Bild rechts) und - nur an den Steckern erkennbar) etwas über der Bildmitte für den Endanschlag "vorne" des Geräteträgers.

Letzterer wird von der roten Doppelklemmbuchse etwas rechts daneben betätigt. Die sitzt nämlich auf der Kurbel, die das Exzenter bildet, und schiebt die graue in Bildmitte sichtbare Statikstrebe. Deren anderes Ende schiebt den Geräteträger.