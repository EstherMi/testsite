---
layout: "image"
title: "Die Stellmechanik (1)"
date: "2008-09-16T21:35:35"
picture: "digitaluhrv09.jpg"
weight: "22"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/15276
imported:
- "2019"
_4images_image_id: "15276"
_4images_cat_id: "1396"
_4images_user_id: "104"
_4images_image_date: "2008-09-16T21:35:35"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15276 -->
Die Stellmechanik ist so aufgebaut:

- Ganz unten liegt die Plattform, die auf insgesamt 12 Vorstuferädern ("Rad 23") via Power-Motor und Kette nach links und rechts verschoben werden kann. Dieser Motor befindet sich bei der Elektronik und ist dort fest angebracht.

- Darauf wiederum kann der Geräteträger auf insgesamt 8 Vorstuferädern nach vorne und hinten verschoben werden. Der Motor dafür ist Teil des Wagens.

- Rechts kann man die elektropneumatische Baugruppe erahnen.

- Die steuert die sieben Pneumatikzylinder. Einen sieht man ganz oben, die anderen sind analog im Inneren des Geräteträgers angebracht.

- Die Pneumatikzylinder betätigen je eine Kipphebelmechanik.

- Die letztlich schiebt die Segmente in die gewünschte Position.