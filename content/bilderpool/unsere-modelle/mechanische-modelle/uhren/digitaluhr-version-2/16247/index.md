---
layout: "image"
title: "Wagen (5)"
date: "2008-11-09T17:53:53"
picture: "digitaluhrvwagen05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/16247
imported:
- "2019"
_4images_image_id: "16247"
_4images_cat_id: "1396"
_4images_user_id: "104"
_4images_image_date: "2008-11-09T17:53:53"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16247 -->
Rechts die indirekt vom PowerMotor angetriebene Schnecke. Die Achse des Z15 endet links in einer Rastkurbel, die über zwei Streben die Achse schiebt oder zieht, die quer zwischen den beiden schwarzen Bauplatten steckt, die das Fundament für den Geräteträger bilden. Die Metallachse dort gibt es vorne auch und hat auch den Zweck, die beiden schwarzen Bauplatten gegen Verkanten zu bewahren. Immerhin drückt das ganze Geräteträgergewicht von oben etwa mittig nach unten, die Räder, auf denen er nach vorne und hinten fährt, liegen aber außen (hier ganz rechts und ganz links, nur ganz links oben sieht man auf diesem Foto eines dieser Räder). Die Kurbel betätigt hier auch gerade den Endschalter für die hintere Position. Die ebenfalls auf der Kurbel aufgesteckte rote Achskupplung betätigt den hier links zu sehenden Taster für die vordere Endlage. Die Kurbel könnte endlos drehen, ohne dass sie an einen echten Anschlag gelangte und evtl. etwas beschädigen könnte.