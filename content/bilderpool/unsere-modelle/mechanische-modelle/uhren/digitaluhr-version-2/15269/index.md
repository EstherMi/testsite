---
layout: "image"
title: "Überblick"
date: "2008-09-16T21:35:34"
picture: "digitaluhrv02.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/15269
imported:
- "2019"
_4images_image_id: "15269"
_4images_cat_id: "1396"
_4images_user_id: "104"
_4images_image_date: "2008-09-16T21:35:34"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15269 -->
Das Ganze funktioniert so (Details werden bei den nachfolgenden Bildern beschrieben):

1. Zeitbasis sind die sehr genauen 50 Hz aus dem normalen Wechselstromnetz, die von einigen Elektronik-Bausteinen (links vorne) in eine Robo-Interface-taugliche Form gebracht werden.

2. Ein Robo-Interface mit zwei I/O-Extensions beschäftigt sich mit dem Mitzählen der Zeit und dem Steuern der Anlage.

3. Das große Teil rechts wird mittels der unten sichtbaren Kette von Ziffer zu Ziffer sowie nach vorne in die Ziffern hinein bzw. nach hinten aus den Ziffern hinaus verschoben.

4. Ein dreißigadriges Flachbandkabel geht von der Elektronik links zu diesem Wagen und steuert sieben Elektromagnete (einen pro Segment einer Ziffer). Die betätigen eines der leider nicht mehr hergestellten ft-Festo-Ventile, die von einem Kompressor (genauer: einem Inhaliergerät) mit Luft versorgt werden. Die so ein/ausschaltbare Druckluft der 7 Ventile geht auf je einen Pneumatik-Doppelbetätiger, der wiederum je zwei Ventile (einen Öffner, blau, und einen Schließer, rot) ansteuern. Damit kann per E-Magnet je ein Zylinder ein- oder ausgefahren werden.

5. Die Zylinder stellen durch eine kleine Kipphebelmechanik je ein Segment um, indem Sie es hinter graue Statikträger schieben (um es zu verstecken und damit unsichtbar zu machen) bzw. indem sie es wieder hervorschieben, wodurch es deutlich sichtbar wird. Die Segmente werden um je 15 mm verschoben.

6. Damit sich die gelben Segmente bei Blick von vorne möglichst abheben, ist eine schwarze Rückwand eingebaut. Die ist außerdem als Versteifung - vor allem beim Transportieren des Modells - wichtig.

Das war's im Wesentlichen auch schon.