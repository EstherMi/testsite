---
layout: "image"
title: "Elektropneumatische Ventile"
date: "2008-09-16T21:35:35"
picture: "digitaluhrv13.jpg"
weight: "26"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/15280
imported:
- "2019"
_4images_image_id: "15280"
_4images_cat_id: "1396"
_4images_user_id: "104"
_4images_image_date: "2008-09-16T21:35:35"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15280 -->
Hier sieht man eines der der sieben Elektro-Ventile: Es ist fast genau so aufgebaut wie in der Anleitung zum älteren Pneumatik-Kasten: Ein Elektromagnet zieht an einer rechteckigen Rückschlussplatte (aus dem älteren ft-Elektromechanik-Programm). Die hängt über einen BS5 an einem BS15. Der ist mit einer Platte 15x45 mit zwei Zapfen mit dem Winkelstein 60 rechts verbunden. (In der Originalanleitung ist anstelle des Winkelsteins ein BS7,5 mit Klebestreifen verwendet worden.) Die Platte wird von dem in der Mitte sitzenden Pneumatikventil (das durchsichtige 15x15x15-Teil in der Mitte) locker in Position gehalten.

Letztlich zieht der E-Magnet also den Winkelstein gegen das Pneumatikventil (man sieht dessen blauen Stößel). Das lässt dann Luft durch (wohin, zeigt das nächste Bild).

Diese Mimik gibt es sieben Mal: Dies hier ist "rechts vorne oben". Links davon sieht man dasselbe (spiegelverkehrt angebracht). Auf der Rückseite der Trägerplatten 45x90 befindet sich noch eines. Und das Ganze gibt's ein Stockwerk tiefer nochmal. Nur "links hinten oben" befindet sich kein Ventil (sonst wären es ja 8), sondern die Pneumatik-Ventil-Gruppe, die auf dem nächsten Bild beschrieben wird.