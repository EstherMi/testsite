---
layout: "image"
title: "Wagen (10)"
date: "2008-11-09T17:53:54"
picture: "digitaluhrvwagen10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/16252
imported:
- "2019"
_4images_image_id: "16252"
_4images_cat_id: "1396"
_4images_user_id: "104"
_4images_image_date: "2008-11-09T17:53:54"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16252 -->
Hier die rechte Seite des Wagens.