---
layout: "image"
title: "Die Anzeige"
date: "2008-09-16T21:35:34"
picture: "digitaluhrv03.jpg"
weight: "16"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/15270
imported:
- "2019"
_4images_image_id: "15270"
_4images_cat_id: "1396"
_4images_user_id: "104"
_4images_image_date: "2008-09-16T21:35:34"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15270 -->
Für diese Aufnahme wurden die Klarsichtfolien, die der Kontrasterhöhung dienen, abgenommen, damit man die Details besser erkennen kann. Die Segmente gleiten an folgenden Achsen entlang:

- Alle senkrecht stehenden Segmente gleiten auf insgesamt vier 50-cm-Achsen (von Conrad) entlang: Je zwei Achsen für die oberen und zwei für die unteren senkrecht stehenden Segmente. Auf diesen Achsen können die Segmente um je 15 mm nach links oder rechts verschoben werden.

- Alle waagerecht liegende Segmente gleiten an je zwei senkrecht stehenden Achsen je Ziffer auf und ab. Diese Segmente werden durch Klemmen daran gehindert, immer wieder herunter zu rutschen - sie sollen ja in der Stellung bleiben, in der sie von der Stellmechanik gebracht wurden.