---
layout: "image"
title: "Der Wagen von rechts"
date: "2008-09-16T21:35:35"
picture: "digitaluhrv12.jpg"
weight: "25"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/15279
imported:
- "2019"
_4images_image_id: "15279"
_4images_cat_id: "1396"
_4images_user_id: "104"
_4images_image_date: "2008-09-16T21:35:35"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15279 -->
Loriot würde jetzt wohl sagen: "Das sieht sehr übersichtlich aus!" ;-)

Die von rechts oben kommende Luftversorgung wird von vier Lufttanks gepuffert und geht dann auf jede Menge Ventile.