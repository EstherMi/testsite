---
layout: "image"
title: "Schräger Blick auf Anzeige und Steuermechanik"
date: "2008-09-16T21:35:34"
picture: "digitaluhrv05.jpg"
weight: "18"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/15272
imported:
- "2019"
_4images_image_id: "15272"
_4images_cat_id: "1396"
_4images_user_id: "104"
_4images_image_date: "2008-09-16T21:35:34"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15272 -->
Hier ein Blick schräg von hinten auf die Anzeige.