---
layout: "image"
title: "Gesamtansicht von links"
date: "2008-09-16T21:35:34"
picture: "digitaluhrv04.jpg"
weight: "17"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/15271
imported:
- "2019"
_4images_image_id: "15271"
_4images_cat_id: "1396"
_4images_user_id: "104"
_4images_image_date: "2008-09-16T21:35:34"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15271 -->
Man sieht ganz gut die Baugruppen:

- Elektronik im Vordergrund
- Anzeige, im Bild rechts
- Steuermechanik - der große Wagen da hinten
- Rückwand

Der senkrecht aus der Rückwand kommende Stab hält den Luftversorgungs-Schlauch so, dass er beim Hin- und Herfahren des Wagens nirgendwo hängt.