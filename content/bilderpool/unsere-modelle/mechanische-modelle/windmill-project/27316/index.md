---
layout: "image"
title: "Windmill Project"
date: "2010-05-28T14:22:08"
picture: "ft_windmill_a.jpg"
weight: "1"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/27316
imported:
- "2019"
_4images_image_id: "27316"
_4images_cat_id: "1962"
_4images_user_id: "585"
_4images_image_date: "2010-05-28T14:22:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27316 -->
Had a request to build a wind powered oil derrick with ft. Thought to share.