---
layout: "image"
title: "Stirnraddifferential mit Zangenmutter und Spannzange - zerlegt"
date: "2014-07-21T20:31:28"
picture: "Stirnraddifferential_V7_Bild_5_bearbeitet.jpg"
weight: "3"
konstrukteure: 
- "MickyW"
fotografen:
- "MickyW"
keywords: ["Stirnraddifferential", "Zangenmutter", "Spannzange"]
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- details/39024
imported:
- "2019"
_4images_image_id: "39024"
_4images_cat_id: "2850"
_4images_user_id: "1806"
_4images_image_date: "2014-07-21T20:31:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39024 -->
Hier das Differential noch einmal in Teilzerlegung.

Die Herausforderung bei diesen Stirnraddifferentialen besteht auch darin, eine Anordnung zu finden, bei der drei Achsen mit ihren Zahnrädern ineinandergreifen. Nicht ganz simpel.