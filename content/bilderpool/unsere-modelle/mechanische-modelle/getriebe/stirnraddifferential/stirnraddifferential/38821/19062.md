---
layout: "comment"
hidden: true
title: "19062"
date: "2014-05-18T23:00:29"
uploadBy:
- "geometer"
license: "unknown"
imported:
- "2019"
---
Hallo Dirk und Harald,

sehr schön. "Bis man sich im Klaren darüber ist, wieso das jetzt ein Differential ist..." Das ist einfach zu sehen: Wenn man den Steg festhält, findet offensichtlich eine Richtungsumkehr statt. Das ist genau das, was ein mit dem Steg mitbewegter Beobachter immer sieht. Jetzt muss man nur noch die Gleichung x' + y' = 0 im mit dem Steg bewegten System durch die Transformation x' = x - z, y' = y - z ins ruhende System zurückrechnen und siehe da x + y = 2z. Didaktisch aufbereitet stelle ich solche Überlegungen in nächster Zeit einmal zur Verfügung. Differentiale und Planetengetriebe sind ideal, um das wichtige Thema Bezugssystemwechsel zu erlernen. Sich in andere hereinversetzten zu können, ist nicht nur im zwischenmenschlichen Bereich wichtig ;-).

Wenn man nicht symmetrisch baut und die Rolle von Steg und Sonne vertauscht, braucht man nur zwei Klemm-Z15 (mehr habe ich leider auch nicht und bei ffm steht immer 0 als vorhandene Stückzahl). Besser sind drei. Ich lade mal eine Version hoch, die ich vor drei Jahren gebaut habe. Damals habe ich richtig viele Planetengetriebe mit ft durchgespielt, aber technisch überzeugen konnte mich außer der Standardvariante mit Innenzahnrad nichts. Die Trägheitsmomente sind alle zu groß.

Viele Grüße

Thomas