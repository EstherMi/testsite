---
layout: "image"
title: "noch kompaktere Variante eines Stirnraddifferentials"
date: "2014-05-23T08:33:42"
picture: "StirnradNeu.jpg"
weight: "1"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: ["Differential", "Planetengetriebe"]
uploadBy: "geometer"
license: "unknown"
legacy_id:
- details/38833
imported:
- "2019"
_4images_image_id: "38833"
_4images_cat_id: "2900"
_4images_user_id: "1088"
_4images_image_date: "2014-05-23T08:33:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38833 -->
