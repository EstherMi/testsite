---
layout: "image"
title: "Stirnraddifferential - Detailansicht (II)"
date: "2014-05-18T19:01:36"
picture: "stirnraddifferential4.jpg"
weight: "11"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/38824
imported:
- "2019"
_4images_image_id: "38824"
_4images_cat_id: "2900"
_4images_user_id: "1126"
_4images_image_date: "2014-05-18T19:01:36"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38824 -->
Das Bild zeigt die Drehscheibe 60 mit dem Rollenbock zur Befestigung der beiden Stirnräder-Achsen (rechts oben), den Lochstein für die Achse in der Mitte (dahinter liegt die Freilaufnabe) und die Gelenkwürfel-Klaue für die Stabilisierungsstange.