---
layout: "image"
title: "Andere Variante eines Stirnraddifferentials - linke Sonne"
date: "2014-05-19T18:34:26"
picture: "SonneLinks.jpg"
weight: "6"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: ["Differential", "Planetengetriebe"]
uploadBy: "geometer"
license: "unknown"
legacy_id:
- details/38828
imported:
- "2019"
_4images_image_id: "38828"
_4images_cat_id: "2900"
_4images_user_id: "1088"
_4images_image_date: "2014-05-19T18:34:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38828 -->
