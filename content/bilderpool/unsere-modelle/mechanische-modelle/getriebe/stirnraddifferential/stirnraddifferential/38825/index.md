---
layout: "image"
title: "Andere Variante eines Stirnraddifferentials"
date: "2014-05-19T11:29:26"
picture: "Stirnraddifferential.jpg"
weight: "3"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- details/38825
imported:
- "2019"
_4images_image_id: "38825"
_4images_cat_id: "2900"
_4images_user_id: "1088"
_4images_image_date: "2014-05-19T11:29:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38825 -->
Variante mit zwei Klemm-Z15. Die Rolle von Sonne und Steg ist gegenüber Dirks Variante vertauscht: Hält man den Steg fest, dreht sich die linke Achse halb so schnell wie die rechte. Genau das passiert bei einem "normalen" Differential, wenn man eine der seitlichen Achsen festhält und die andere dreht: der Käfig dreht sich dann halb so schnell. Im gezeigten Aufbau braucht der Steg natürlich noch einen An-/Abtrieb. Den könnte man realisieren, indem man BS 15 mit zwei Zapfen verwendet und eine zweite Drehscheibe mit aufgesetztem Z40 mit Freilaufnabe anbaut.