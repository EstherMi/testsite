---
layout: "image"
title: "Stirnraddifferential - geometers finale Variante"
date: "2014-05-23T08:33:42"
picture: "StirnradFinal.jpg"
weight: "2"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: ["Differential", "Planetengetriebe"]
uploadBy: "geometer"
license: "unknown"
legacy_id:
- details/38834
imported:
- "2019"
_4images_image_id: "38834"
_4images_cat_id: "2900"
_4images_user_id: "1088"
_4images_image_date: "2014-05-23T08:33:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38834 -->
Ebenso wie Dirks Variante, die die Kategorie eröffnet hat, ist diese Variante KFZ tauglich (genügend große Räder vorausgesetzt): das rote Z15 kann angetrieben und die beiden Räder an die linke und rechte Achse angeschlossen werden. Um Verwinden zu vermeiden, schiebt man am besten noch symmetrisch je eine Bauplatte 5 (35049) mit dem Zapfen in die Nuten der Bausteine 15 mit Ansenkung (32321).