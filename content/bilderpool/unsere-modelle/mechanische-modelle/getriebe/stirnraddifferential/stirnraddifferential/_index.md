---
layout: "overview"
title: "Stirnraddifferential"
date: 2019-12-17T19:24:02+01:00
legacy_id:
- categories/2900
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2900 --> 
Der geniale Entwurf von MickyW hat mich auf die Idee dieses Stirnraddifferentials gebracht.
Es ist stabil und lediglich etwas breiter als das [url=http://www.ftcommunity.de/categories.php?cat_id=2873]Selbstbaudifferential[/url].