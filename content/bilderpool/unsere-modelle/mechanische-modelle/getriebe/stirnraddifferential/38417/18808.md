---
layout: "comment"
hidden: true
title: "18808"
date: "2014-03-02T21:45:02"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Probiers doch mal mit Rastachsen, da geht recht viel recht kompakt und gewichtssparend, und sie halten überraschend viel aus. Dann könnte man noch die feinen Zahnräder probieren (Z10-Ritzel, Statik-Riegelscheiben, mot-2-Aufsteckgetriebeachsen und dergleichen), um Radius und damit Unwucht zu sparen. Die 130593 Rastaufnahmeachse http://ft-datenbank.de/details.php?ArticleVariantId=6e42b090-57f4-49cf-b7db-67cedcac15ac könnte da auch interessant sein.

Gruß,
Stefan