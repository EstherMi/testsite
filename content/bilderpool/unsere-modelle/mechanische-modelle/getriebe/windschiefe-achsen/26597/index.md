---
layout: "image"
title: "In Bewegung"
date: "2010-03-05T21:54:13"
picture: "InBewegung.jpg"
weight: "8"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- details/26597
imported:
- "2019"
_4images_image_id: "26597"
_4images_cat_id: "1897"
_4images_user_id: "1088"
_4images_image_date: "2010-03-05T21:54:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26597 -->
In der Bewegung erkennt man das Hyperboloid gut.