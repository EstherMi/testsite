---
layout: "image"
title: "Tangentiales Aufsetzen"
date: "2010-03-05T21:54:13"
picture: "TangentialesAufsetzen.jpg"
weight: "9"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- details/26598
imported:
- "2019"
_4images_image_id: "26598"
_4images_cat_id: "1897"
_4images_user_id: "1088"
_4images_image_date: "2010-03-05T21:54:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26598 -->
Dass das Hyperboloid aus Geraden besteht, kann zur Kraftübertragung zwischen windschiefen Achsen benutzt werden. Dazu lässt man zwei tangential berührende Hyperboloide aneinander abrollen. Ich hatte gerade nicht genügend lange Gummibänder für ein zweites Hyperboloid da. Daher habe ich hier nur zwei Reifen genommen, zwischen denen man sich das Hyperboloid eingespannt denken muß. Die Haltung ist auch nicht 100% korrekt, sondern etwas verwackelt beim Auslösen.