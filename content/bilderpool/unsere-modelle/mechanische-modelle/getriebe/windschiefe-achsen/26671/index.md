---
layout: "image"
title: "Aufsetzen"
date: "2010-03-11T19:52:42"
picture: "Aufsetzen2.jpg"
weight: "3"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- details/26671
imported:
- "2019"
_4images_image_id: "26671"
_4images_cat_id: "1897"
_4images_user_id: "1088"
_4images_image_date: "2010-03-11T19:52:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26671 -->
Hier das korrekte Aufsetzen des kleineren Hyperboloids auf das größere. Die beiden berühren sich längs der eingezeichneten Geraden. Die aneinander abrollenden Kreise (zwei sind eingezeichnet) sind jeweils gleich groß.