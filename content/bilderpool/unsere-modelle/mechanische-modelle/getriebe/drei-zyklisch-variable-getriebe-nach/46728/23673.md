---
layout: "comment"
hidden: true
title: "23673"
date: "2017-10-02T21:56:38"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Das ist wirklich ganz großes Kino, Stefan!
Klopmeier-Getriebe ganz ohne Fräsen, Kleben, 3D-Druck...
Eine neue ft-Getriebe-Dimension.
Herzlicher Gruß,
Dirk