---
layout: "image"
title: "Übergang zur Pumpe"
date: "2017-10-02T17:56:58"
picture: "dreizyklischvariablegetriebenachklopmeier06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46733
imported:
- "2019"
_4images_image_id: "46733"
_4images_cat_id: "3461"
_4images_user_id: "104"
_4images_image_date: "2017-10-02T17:56:58"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46733 -->
Das Exzenter der Pumpe ist selbst ganz klassisch aufgebaut. Der Gag ist, dass sich die ft-Drehscheibe nicht gleichförmig dreht, sondern während der Aufwärtsbewegung der Pumpe (beim Pumpen also) recht gleichförmig gemächlich, und bei der Abwärtsbewegung (die ja kein Öl fördert) sehr schnell.