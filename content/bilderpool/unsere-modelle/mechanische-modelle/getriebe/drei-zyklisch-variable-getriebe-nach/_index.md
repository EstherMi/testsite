---
layout: "overview"
title: "Drei zyklisch variable Getriebe nach Klopmeier"
date: 2019-12-17T19:24:15+01:00
legacy_id:
- categories/3461
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3461 --> 
Dieses Modell stand auf der Convention 2017 in Dreieich und stellt drei Varianten einfach aufgebauter ungleichförmig übersetzender Getriebe in Anwendung vor.