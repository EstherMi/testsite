---
layout: "image"
title: "[2/4] Getriebeteile m0,5"
date: "2009-10-13T19:55:01"
picture: "getriebeteilem2.jpg"
weight: "2"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2 / 2D aus 3D"
keywords: ["m05"]
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/25549
imported:
- "2019"
_4images_image_id: "25549"
_4images_cat_id: "1794"
_4images_user_id: "723"
_4images_image_date: "2009-10-13T19:55:01"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25549 -->
Leider sind viele Getriebeteile m0,5 nicht mehr lieferbar. Mit etwas Ausdauer kann man aber auf verschiedenen Gebrauchtmärkten Teile daraus erwerben.

Hier zunächst eine noch kleine Auswahl von "nml"-Teilen:

31062 Mini-Motor grau mit Schnecke m0,5
32117 Drehschalter Oberteil mit Z80m0,5
32915 Zahnrad Z44/18m0,5
32916 Zahnrad Z44/14m0,5
32912 Ritzel Z14m0,5
32917 Getriebeachse mit Ritzel Z14m0,5 und Vierkant zur Aufnahme von 32914 
32914 Schneckenrad Schrägzahnrad Z10m1,5
31322 Dauermagnet grün mit beids. Zahnstange 20Zm0,5 
31323 Dauermagnet rot mit beids. Zahnstange 20Zm0,5
31325 em Rückschlußplatte mit beids. Zahnstange 20Zm0,5

Eine Aufreihung der Zähnezahlen in m0,5 (Zahnstangen in Klammern) ergibt hier:
Z14, Z18, (Z20), Z44, Z80