---
layout: "comment"
hidden: true
title: "14809"
date: "2011-08-09T12:27:47"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Tolle Aufstellung!

Ist es eigentlich gedacht, dass man die hier weiß dargestellten Ritzel und Schnecken von den Getriebewellen lösen kann? Als Einzelteil kann man die ja nicht kaufen, richtig?

Die (mit Gewalt) gelösten Teile nützen mir dann ja auch nichts, weil ich ja eine Metallachse benötige, die seitlich axial Rillen aufweist, damit die Zahnräder/Schnecken  nicht rutschen, oder?

Gruß, Thomas