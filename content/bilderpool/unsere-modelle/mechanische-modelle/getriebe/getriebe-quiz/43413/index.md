---
layout: "image"
title: "Hauptkomponenten"
date: "2016-05-22T21:51:25"
picture: "pic2.jpg"
weight: "2"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/43413
imported:
- "2019"
_4images_image_id: "43413"
_4images_cat_id: "3224"
_4images_user_id: "1729"
_4images_image_date: "2016-05-22T21:51:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43413 -->
Das sind die wichtigsten Komponenten. Dazu kommen dann "nur noch" Zahnräder, Achsen und diverse Bausteine.
Angetrieben wird das Ganze über 3 Motoren und gesteuert über eine Remote-Control. Die Motoren sind jetzt nur exemplarisch. Die optimalen Übersetzungen weiß ich selbst noch nicht..
Es wird kein Controller benötigt und es muß nichts programmiert werden. Im Grossen und Ganzen eine rein mechanische Lösung