---
layout: "image"
title: "Getriebe 2 (5)"
date: "2015-01-19T07:10:38"
picture: "getriebemmitminimot11.jpg"
weight: "12"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/40394
imported:
- "2019"
_4images_image_id: "40394"
_4images_cat_id: "3027"
_4images_user_id: "2321"
_4images_image_date: "2015-01-19T07:10:38"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40394 -->
