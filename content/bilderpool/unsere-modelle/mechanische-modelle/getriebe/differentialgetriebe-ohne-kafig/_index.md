---
layout: "overview"
title: "Differentialgetriebe ohne Käfig"
date: 2019-12-17T19:24:08+01:00
legacy_id:
- categories/2895
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2895 --> 
Um zu verstehen und sichtbar zu machen, wie und warum ein Differentialgetriebe funktioniert, vertauscht man am besten die Rollen des Käfigs und der zentraler Achse(n). Die hier vorgestellten zwei Varianten sind sehr leichtgängig und haben nur wenig Spiel.