---
layout: "image"
title: "Innen-Diff0603.jpg"
date: "2014-05-07T20:35:16"
picture: "Innen-Diff0603.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Kronenrad"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/38751
imported:
- "2019"
_4images_image_id: "38751"
_4images_cat_id: "2895"
_4images_user_id: "4"
_4images_image_date: "2014-05-07T20:35:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38751 -->
mit den freilaufenden Kettenrädern Z20 (39164) (links und rechts innen), zwei nomalen Z20, schwarzen Kettengliedern und Freilaufnabe auf der rechten Seite geht es auch eine Nummer kompakter :-)