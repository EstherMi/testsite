---
layout: "image"
title: "Gesamtansicht des Selbstbau-Differentials"
date: "2014-03-24T10:28:22"
picture: "differentialfunktionsmodell1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/38484
imported:
- "2019"
_4images_image_id: "38484"
_4images_cat_id: "2873"
_4images_user_id: "1126"
_4images_image_date: "2014-03-24T10:28:22"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38484 -->
Das Differential besteht aus vier Rast-Kegelzahnrädern und wird auf einer Drehscheibe 60 mit verbundenem Z40 aufgebaut. Der Antrieb erfolgt per Kronenrad am Z40 (15:32). Die Kegelzahnräder sind über Achsen mit den Rädern verbunden, laufen aber frei (Freilaufnabe im Z40).