---
layout: "overview"
title: "Differential (Funktionsmodell)"
date: 2019-12-17T19:24:06+01:00
legacy_id:
- categories/2873
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2873 --> 
Angeregt durch die "Monster-Differentiale" von Ludger haben wir ein Selbstbau-Differential konstruiert, das sich als Funktionsmodell eignet, aber auch in einem Fahrzeug eingesetzt werden kann - die Bauhöhe durfte daher zumindest den derzeit größten ft-Reifendurchmesser nicht übersteigen.