---
layout: "comment"
hidden: true
title: "5089"
date: "2008-01-20T12:46:50"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Wilhelm Klopmeier schrieb: (zu mir Peter Damen Poederoyen NL)

Ich habe jetzt ein Schema und ein paar Fotos zum Pedalantrieb
zusammengestellt. Gerade zum diesem Thema hat es bei den bisherigen
Präsentationen stets spannende Diskussionen gegeben. Nun bin ich auf die
Reaktion beim FTC gespannt.

Schau auch mal unter :
ftCommunity / Downloads / Dokumente / Technische Informationen 

http://www.ftcommunity.de/data/downloads/dokumente/technischeinformationen/pedalantriebfotosschema.pdf

Mit dem Getriebe soll die Leistungsabgabe des menschlichen Körpers
verbessert werden. Nach meinen Überlegungen und nach einer Recherche
sportmedizinischer Untersuchungen kann eine Verbesserung von ca. 10%
erreicht werden. Problematisch ist aber noch eine praxistaugliche
Konstruktion - die auftretenden Kräfte sind erheblich. Die Unterlagen halte
ich jedoch zunächst zurück, damit eine Diskussion nicht beeinflußt wird.

Am Treffen in Veghel würde ich gerne teilnehmen. Es gibt aber noch Probleme
wegen eines anderen Termins, ich kann deshalb noch nicht zusagen.


Mit freundlichen Grüßen

Wilhelm Klopmeier

Goethestr. 30
47877 Willich
Tel.: 02154 / 1550
Fax: 02154 / 484789