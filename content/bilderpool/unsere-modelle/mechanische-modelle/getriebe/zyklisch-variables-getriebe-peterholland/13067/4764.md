---
layout: "comment"
hidden: true
title: "4764"
date: "2007-12-15T12:40:39"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Wegen ihres einfachen und robusten Aufbaues sind sogenannte Pferdekopfpumpen
weitverbreitet in der Ölförderung.
Nachteilig bei diesem Pumpentyp ist der sinusähnliche Hubverlauf. Der Förderstrom der Pumpe muss über die Drehzahl so eingestellt werden, dass das Verdrängungsvolumen
niemals das Bereitstellungsvolumen der Quelle überschreitet. Der kurzzeitige
Spitzenwert der Fördergeschwindigkeit ist daher bei weniger ergiebigen Quellen die
maßgebende Grenze für die Förderkapazität der Pumpe.

Weitere Info gibt es unter Downloads, Documenten, Technische Informationen:
http://www.ftcommunity.de/downloads.php?kategorie=Dokumente