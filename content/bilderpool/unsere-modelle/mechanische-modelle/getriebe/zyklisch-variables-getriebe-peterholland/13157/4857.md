---
layout: "comment"
hidden: true
title: "4857"
date: "2007-12-27T09:36:02"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Das Innenzahnrad Z30 (35694) habe beim Schiedepresse geklemmt (statt geklebt) an die Kugellagerung 6810 ZZ - 50x65x7 mm. (dxDxB).