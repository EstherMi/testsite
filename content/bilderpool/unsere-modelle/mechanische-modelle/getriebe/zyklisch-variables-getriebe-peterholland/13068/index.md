---
layout: "image"
title: "Ölpumpe mit Zyklisch variables Getriebe"
date: "2007-12-15T12:14:02"
picture: "lpumpe_mit_zyklisch_variablen_Getriebes_Peter_Damen_002_2.jpg"
weight: "29"
konstrukteure: 
- "Peter Damen"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/13068
imported:
- "2019"
_4images_image_id: "13068"
_4images_cat_id: "1185"
_4images_user_id: "22"
_4images_image_date: "2007-12-15T12:14:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13068 -->
