---
layout: "image"
title: "Stufenloses Getriebe 4"
date: "2007-06-04T15:48:00"
picture: "Stufenloses_Getriebe_4.jpg"
weight: "4"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/10689
imported:
- "2019"
_4images_image_id: "10689"
_4images_cat_id: "970"
_4images_user_id: "328"
_4images_image_date: "2007-06-04T15:48:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10689 -->
Hier das Getriebe im ausgekuppelten Zustand an einem Endanschlag des Schneckengetriebes (Leerlauf). Das Zwischenrad ist geringfügig nach hinten gekippt, da durch den Winkelstein 30 kein Druck mehr auf die Achse ausgeübt wird.