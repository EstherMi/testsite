---
layout: "image"
title: "Stufenloses Getriebe 1"
date: "2007-06-04T15:47:59"
picture: "Stufenloses_Getriebe_1.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: ["CVT", "continuous"]
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/10686
imported:
- "2019"
_4images_image_id: "10686"
_4images_cat_id: "970"
_4images_user_id: "328"
_4images_image_date: "2007-06-04T15:47:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10686 -->
Eigenlich wollte ich diese Idee mal in einem fertigen Fahrzeug präsentieren.  Aber die Umsetzung wird noch einige Zeit in Anspruch nehmen, so dass ich das Grundprinzip meines stufenlosen Getriebes hier mal zur Diskussion stelle... ;o)

Klar, die Idee mit dem verschiebbaren Reibrad ist nicht neu, aber in FT hab ich das noch nicht gesehen.

Der Mini-Motor ist der Antrieb, das Zahnrad außen der Abtrieb. Beide Achsen enthalten ein Rad mit Reifen und sind fest installiert. Hier muss also nix verschoben werden.

Das große rote Zwischenrad (Drehscheibe 60) überträgt die Drehbewegung vom Antrieb zum Abtrieb. Über das Schneckengetriebe und die Kurbel ist es verschiebbar, so dass das Übersetzungsverhältnis zwischen An- und Abtrieb stufenlos verstellt werden kann.

Die Achse, auf der sich das Zwischenrad dreht, gleitet hinten an dem quer liegenden Grundbaustein 30 mit der Bauplatte 15x30. Dadurch wird der nötige Anpressdruck auf die Gummireifen erzeugt.

Die beiden quer liegenden schwarzen Grundbausteine stellen die Anschläge des Schneckentriebs dar, damit man nicht übers Ziel hinausschiesst.

Zusätzlich wurde noch eine Einkuppelfunktion integriert: An einem Ende der Bauplatte 15x30 ermöglicht ein Winkelstein 30, dass ab dort der Anpressdruck auf die Achse und damit auf den Reibradantrieb nachlässt. Das Zwischenrad kippt dann um die Schneckenachse nach hinten, und der Antrieb ist ausgekuppelt. Startet man in dieser Position den Motor, dreht sich der Abtrieb zunächst nicht. Kuppelt man dann den Schneckentrieb ein, dreht sich der Abtrieb mit sehr geringer Geschwindigkeit (Untersetzung). Dreht man weiter, gelangt man in symmetrischer Lage des Zwischenrads zum Übersetzungsverhältnis 1:1. Ab dann dreht sich der Abtrieb schneller als der Antrieb, bis zum Endanschlag des Schneckentriebs.

Die axialen Anschläge des Schneckentriebs sowie der Anpressdruck durch die Bauplatte 15x30 müssen sehr genau justiert werden, damit es funktioniert. Die Reibung der Gummireifen ist grundsätzlich ganz ordentlich, so dass einiges an Drehmoment übertragen werden kann. Klar, irgendwann rutscht es, aber mir geht's hier erstmal um die Prinzipdarstellung.