---
layout: "image"
title: "Huygens-Getriebe"
date: "2011-12-27T15:29:10"
picture: "Huygens-Getriebe.jpg"
weight: "6"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- details/33813
imported:
- "2019"
_4images_image_id: "33813"
_4images_cat_id: "1575"
_4images_user_id: "1088"
_4images_image_date: "2011-12-27T15:29:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33813 -->
Funktionsmodell des ungleichförmig übersetzenden Getriebes von Christiaan Huygens. Benutzt werden hier zwei Rastachsen mit Platten 130593. Dadurch wird die Exzentrizität e zwischen 0 und 0,25 einstellbar. Das Verhältnis aus maximaler und minimaler Winkelgeschwindigkeit am Abtrieb (im Bild oben) bei konstanter Winkelgeschwindigkeit am Antrieb beträgt (1+e)^2 / (1-e)^2. Siehe auch ft:pedia 4/2011.