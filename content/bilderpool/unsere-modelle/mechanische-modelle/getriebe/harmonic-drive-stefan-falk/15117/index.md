---
layout: "image"
title: "Gesamtansicht (2)"
date: "2008-08-27T23:17:56"
picture: "harmonicdrive2.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: ["complication"]
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/15117
imported:
- "2019"
_4images_image_id: "15117"
_4images_cat_id: "1382"
_4images_user_id: "104"
_4images_image_date: "2008-08-27T23:17:56"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15117 -->
Das schwarze Zahnrad ist die schwarze Hälfte eines großen ft-Drehkranzes. Der hat 58 Zähne. Die darum gelegte Kette hat zwei Zähne mehr, 60 also. Das schwarze Rad steht still! Es ist fest auf der unbeweglich angebrachten Achse angebracht. Das rote Z40 wird angetrieben und dreht damit auch die Drehscheibe mit dem Hebeträger (das ist auf diesem Bild die insgesamt zweite Drehscheibe von links gesehen). Diese Drehscheibe sitzt auf einer ft-Freilaufnabe (es würde aber wohl auch mit einer nur locker angezogenen normalen Nabe funktionieren). An zwei gegenüberliegenden Stellen wird nun die Kette, die ja etwas "zu lang" ist, von den Zähnen abgehoben und - das ist der Gag - um einen Zahn versetzt wieder aufgelegt, wenn der Hebeträger sich weiterdreht.