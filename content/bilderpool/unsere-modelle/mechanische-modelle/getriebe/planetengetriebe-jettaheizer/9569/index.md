---
layout: "image"
title: "Planetengetriebe04"
date: "2007-03-18T16:24:02"
picture: "Planetengetriebe04.jpg"
weight: "4"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- details/9569
imported:
- "2019"
_4images_image_id: "9569"
_4images_cat_id: "873"
_4images_user_id: "488"
_4images_image_date: "2007-03-18T16:24:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9569 -->
Und hier mal auseinandergebaut. Der Aufbau ist sehr einfach, funktioniert aber. Ein Versuch mit 4 Z10 als Verbindung zum Innen-Z30 hat leider nicht funktioniert, da klemmt das Innen-Z30 zu stark.