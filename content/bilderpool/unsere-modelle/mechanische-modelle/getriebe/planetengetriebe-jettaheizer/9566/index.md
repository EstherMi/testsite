---
layout: "image"
title: "Planetengetriebe01"
date: "2007-03-18T16:24:02"
picture: "Planetengetriebe01.jpg"
weight: "1"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- details/9566
imported:
- "2019"
_4images_image_id: "9566"
_4images_cat_id: "873"
_4images_user_id: "488"
_4images_image_date: "2007-03-18T16:24:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9566 -->
Durch eine Diskussion im Forum angeregt habe ich mal versucht, ein Planetengetriebe zu bauen. Dies ist der erste Versuch.