---
layout: "image"
title: "Noch kleiner und noch leichtgängiger (1)"
date: "2009-06-17T23:54:14"
picture: "freilaufnochkleinerundleichtgaengiger4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/24410
imported:
- "2019"
_4images_image_id: "24410"
_4images_cat_id: "1620"
_4images_user_id: "104"
_4images_image_date: "2009-06-17T23:54:14"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24410 -->
Diese hier funktioniert bei nicht all zu hoher Drehzahl, denn die ist dermaßen leichtgängig, dass die Fliehkraft die 30x15-Platte nach außen drücken kann. Die Platte ist eine 30x15 mit zwei richtigen 4-mm-Löchern (die neueren haben nur die halbtiefen Nuten).

Vorteile:
- Sehr klein
- Sehr leichtgängig
- Kein Gummi, was reißen kann

Nachteile:
- Nicht für sehr hohe Drehzahlen geeignet (oder man muss die Federspannung durch weiter Herausschieben des BS5 mit dem Federkontakt erhöhen, was aber auf die Leichtgängigkeit geht)
- Man braucht einen Federkontakt aus dem alten Elektromechanikprogramm