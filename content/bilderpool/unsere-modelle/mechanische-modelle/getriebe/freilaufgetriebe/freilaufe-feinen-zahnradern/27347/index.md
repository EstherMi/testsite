---
layout: "image"
title: "Variante 1"
date: "2010-06-03T12:49:58"
picture: "freilaeufemitfeinenzahnraedern03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/27347
imported:
- "2019"
_4images_image_id: "27347"
_4images_cat_id: "1964"
_4images_user_id: "104"
_4images_image_date: "2010-06-03T12:49:58"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27347 -->
Gelenkbausteine werden zum Rasten verwendet. Das Spanngummi ist eines vom älteren ft-Reifen 45. Beide Achsen stecken teilweise im Grundbaustein mit Bohrung. Hier und in allen weiteren Vorschlägen ist das ein BS30 mit runder Bohrung, nicht mit dem eckigen Loch. Dadurch werden die Achsen besser geführt und beide Achsen fluchten genau.

Direkt rechts vom BS30 auf der Achse des mot-2-Zahnrads sitzt ein Abstandsring, der die Reibung mit dem Gummi verringert. Ganz rechts sitzt eine I-Strebe 30 mit Loch in der Mitte, die durch die Seilrollenachsen die Gelenkte nochmals fixiert.

Vorteile:

- Sehr leichter Lauf.
- Sehr feiner Lauf durch das Z44.
- Ganz ordentliches Drehmoment übertragbar.