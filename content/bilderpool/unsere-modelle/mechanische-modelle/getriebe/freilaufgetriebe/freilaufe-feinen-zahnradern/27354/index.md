---
layout: "image"
title: "Variante 6"
date: "2010-06-03T12:49:59"
picture: "freilaeufemitfeinenzahnraedern10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/27354
imported:
- "2019"
_4images_image_id: "27354"
_4images_cat_id: "1964"
_4images_user_id: "104"
_4images_image_date: "2010-06-03T12:49:59"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27354 -->
Es geht so einfach und gut: BS30 mit Loch, die Statik-Winkellaschen, und oben die flachen Statikriegel. Die halten in ihrer Nut sogar das Zahnrad in Position.

Vorteile:

- Ganz wenig Aufwand. Sehr leicht.
- Schön leichtgängig, und immer noch gutes Drehmoment
- Gut auf Leichtgängigkeit oder hohes Drehmoment zu justieren.
- Symmetrisch
- Garantiert fluchtende Achsen.