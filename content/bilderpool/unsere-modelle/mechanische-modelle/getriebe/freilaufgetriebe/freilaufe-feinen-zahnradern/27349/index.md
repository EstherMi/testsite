---
layout: "image"
title: "Variante 3"
date: "2010-06-03T12:49:59"
picture: "freilaeufemitfeinenzahnraedern05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/27349
imported:
- "2019"
_4images_image_id: "27349"
_4images_cat_id: "1964"
_4images_user_id: "104"
_4images_image_date: "2010-06-03T12:49:59"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27349 -->
Noch einfacher: Zwei BS5 als Abstandshalter, BS7,5 und das Statikteil als Raste. Das federnde Element ist hier im Wesentlichen der Verbinder 30.

Vorteile:

- Leicht
- Extrem leichtgängig
- Ordentliches Drehmoment

Nachteile:
- Asymmetrisch (ungewollte Kräfte auf den Achsen, Unwucht bei zu hohen Drehzahlen)