---
layout: "image"
title: "6 Vorschläge"
date: "2010-06-03T12:49:58"
picture: "freilaeufemitfeinenzahnraedern02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/27346
imported:
- "2019"
_4images_image_id: "27346"
_4images_cat_id: "1964"
_4images_user_id: "104"
_4images_image_date: "2010-06-03T12:49:58"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27346 -->
Hier also von links nach rechts immer kleiner werdende Freiläufe. Das linke verwendet die vom mot-2 stammende Aufsteckachse mit einem Z44 und kurzem Achsstummel, die rechten 5 verwenden eine Einsteckachse vom MiniMot-U-Getriebe. In den folgenden Bildern werden diese Freiläufe in der Reihenfolge von links nach rechts in diesem Bild gezeigt.