---
layout: "image"
title: "Etwas mehr Bauteile"
date: "2009-04-14T22:21:40"
picture: "freilaufgetriebe2.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/23719
imported:
- "2019"
_4images_image_id: "23719"
_4images_cat_id: "1620"
_4images_user_id: "104"
_4images_image_date: "2009-04-14T22:21:40"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23719 -->
Diese Variante verwendet Federgelenksteine, ebenfalls aus dem 1970er Elektromechanikprogramm.

Vorteile:
- Sehr leichter Lauf
- Feine Auflösung (alle 18° rastet der Freilauf ein)
- Recht hohes Drehmoment übertragbar.

Nachteile:
- Die Federgelenksteine werden leider nicht mehr hergestellt (man bekommt sie aber noch gebraucht).
- Deutlich größer als ein Z20.