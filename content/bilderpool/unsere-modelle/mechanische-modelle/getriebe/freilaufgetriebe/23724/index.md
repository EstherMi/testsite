---
layout: "image"
title: "Noch eine Variante"
date: "2009-04-14T23:33:26"
picture: "freilaufgetriebeb1.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/23724
imported:
- "2019"
_4images_image_id: "23724"
_4images_cat_id: "1620"
_4images_user_id: "104"
_4images_image_date: "2009-04-14T23:33:26"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23724 -->
Diese Variante funktioniert auch sehr gut, baut noch kompakter und kommt völlig ohne Spezialteile aus.

Vorteile:
- Keine Spezialteile
- Kompakt

Nachteile:
- Der Freilauf rastet nur 10 Mal pro Umdrehung (alle 36 °) ein