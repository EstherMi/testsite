---
layout: "overview"
title: "Freilaufgetriebe"
date: 2019-12-17T19:23:43+01:00
legacy_id:
- categories/1620
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1620 --> 
Hier ein paar Varianten, wie man Freilaufgetriebe (Getriebe, die Drehmoment nur in eine Richtung übertragen, in die andere aber frei laufen) bauen kann.