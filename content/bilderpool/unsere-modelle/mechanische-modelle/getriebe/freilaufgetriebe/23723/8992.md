---
layout: "comment"
hidden: true
title: "8992"
date: "2009-04-14T22:34:29"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Noch ein Nachtrag: Je nach Gummi (mit dem abgebildeten geht's) kann man die Winkelsteine auch einfach weglassen. Dadurch läuft der Freilauf noch leichter.

Gruß,
Stefan