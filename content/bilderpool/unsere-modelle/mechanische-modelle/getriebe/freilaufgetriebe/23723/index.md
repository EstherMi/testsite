---
layout: "image"
title: "Variante"
date: "2009-04-14T22:21:40"
picture: "freilaufgetriebe6.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/23723
imported:
- "2019"
_4images_image_id: "23723"
_4images_cat_id: "1620"
_4images_user_id: "104"
_4images_image_date: "2009-04-14T22:21:40"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23723 -->
Diese Variante von Max Buitings Konstruktion kommt mit noch weniger Teilen und vor allem ohne Spezialteile aus.

Vorteile:
- Keine Spezialteile benötigt (dafür aber ein Gummi, auf dem Foto eines der alten Gummis, die für einen ft-Reifen 45 gedacht sind)
- Kein Teil ragt über den Radius der Drehscheibe hinaus.
- Recht hohes Drehmoment übertragbar.

Nachteile:
- Der Freilauf rastet nur 10 Mal pro Umdrehung (alle 36 °) ein.