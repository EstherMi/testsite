---
layout: "image"
title: "Kleiner und leichtgängiger (3)"
date: "2009-06-17T23:54:14"
picture: "freilaufnochkleinerundleichtgaengiger3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/24409
imported:
- "2019"
_4images_image_id: "24409"
_4images_cat_id: "1620"
_4images_user_id: "104"
_4images_image_date: "2009-06-17T23:54:14"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24409 -->
Hier ist die Abtriebsachse abgenommen. Die steckt nur lose im BS30 mit Loch zwecks Zentrierung und muss in einem Modell außerhalb des Freilaufs nochmal gelagert werden.