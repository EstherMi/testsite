---
layout: "image"
title: "Gesamtansicht"
date: "2015-12-28T19:08:43"
picture: "rasterkonformeruebertrag1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/42627
imported:
- "2019"
_4images_image_id: "42627"
_4images_cat_id: "3169"
_4images_user_id: "104"
_4images_image_date: "2015-12-28T19:08:43"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42627 -->
Dieses Zählwerk zählt theoretisch über 4 oder 5 (je nach Sichtweise) Dekaden. Der XS-Motor schaft das bis zum letzten Glied.