---
layout: "image"
title: "Prototyp 1 - nicht so toll"
date: "2010-04-14T20:11:09"
picture: "z2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: ["complication"]
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/26937
imported:
- "2019"
_4images_image_id: "26937"
_4images_cat_id: "1932"
_4images_user_id: "104"
_4images_image_date: "2010-04-14T20:11:09"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26937 -->
Das Problem ist aber, dass das nicht rund ist. Deshalb muss die Achse gefedert aufgehängt werden. Alles in allem nicht wirklich prickelnd.