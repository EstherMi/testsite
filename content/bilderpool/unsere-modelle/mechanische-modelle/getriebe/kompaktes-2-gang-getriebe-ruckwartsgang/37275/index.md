---
layout: "image"
title: "Anfänge (1)"
date: "2013-08-27T23:15:00"
picture: "bild1.jpg"
weight: "3"
konstrukteure: 
- "lukas99h. / Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/37275
imported:
- "2019"
_4images_image_id: "37275"
_4images_cat_id: "2773"
_4images_user_id: "1624"
_4images_image_date: "2013-08-27T23:15:00"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37275 -->
kleines 1-Gang-Getriebe mit Rückwärtsgang
Gebaut von lukas99h. und phil