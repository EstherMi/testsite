---
layout: "image"
title: "Kompaktes 2-Gang-Getriebe + Rückwärtsgang verbessert (2)"
date: "2013-09-18T21:59:56"
picture: "bild2_2.jpg"
weight: "2"
konstrukteure: 
- "lukas99h. / Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/37413
imported:
- "2019"
_4images_image_id: "37413"
_4images_cat_id: "2773"
_4images_user_id: "1624"
_4images_image_date: "2013-09-18T21:59:56"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37413 -->
Hier sieht man das Problem, welches ich beim voherigen Bild angesprochen hab. Die beiden Z15 greifen nicht ganz ineinander, sie sind etwas versetzt, aber es funktioniert.
Gebaut von lukas99h. und Phil