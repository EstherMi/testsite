---
layout: "image"
title: "Anfänge (2)"
date: "2013-08-27T23:15:00"
picture: "bild2.jpg"
weight: "4"
konstrukteure: 
- "lukas99h. / Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/37276
imported:
- "2019"
_4images_image_id: "37276"
_4images_cat_id: "2773"
_4images_user_id: "1624"
_4images_image_date: "2013-08-27T23:15:00"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37276 -->
Hier haben wir ein 2-Gang-Getriebe mit Rückwärtsgang.
Gebaut von lukas99h. und phil