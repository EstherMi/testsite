---
layout: "image"
title: "hypozyk582.jpg"
date: "2014-04-21T22:11:30"
picture: "IMG_0582mit.jpg"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/38585
imported:
- "2019"
_4images_image_id: "38585"
_4images_cat_id: "2883"
_4images_user_id: "4"
_4images_image_date: "2014-04-21T22:11:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38585 -->
Die Exzenterwelle des kleinen Mannes verwendet zwei Kabelbinder, um Rastachsen mit den flachen Seiten der Rastclips zusammen zu fügen. Die Verbindung lässt sich lösen und mit etwas Geschick auch wieder zusammen setzen. Soll heißen: man braucht beim Experimentieren nicht jedesmal neue Kabelbinder.