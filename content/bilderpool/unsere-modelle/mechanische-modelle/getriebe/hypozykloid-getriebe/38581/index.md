---
layout: "image"
title: "hypozyk596.JPG"
date: "2014-04-21T21:53:26"
picture: "IMG_0596.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/38581
imported:
- "2019"
_4images_image_id: "38581"
_4images_cat_id: "2883"
_4images_user_id: "4"
_4images_image_date: "2014-04-21T21:53:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38581 -->
Die Einzelteile des Prototypen. Auf der Exzenterwelle sitzen ein Z5 und ein Z4, beide lose, aber miteinander gekoppelt durch den BS7,5, der von der Seilrolle im Vordergrund halb verdeckt wird. Das Z5 rollt im Z6 (der Drehscheibe links) ab und dreht sich dabei langsam und gegensinnig. Das Z4 (aus 4 Winkelsteinen 60°) bildet mit dem rechten Z5 die zweite Stufe. Dadurch, dass das rechte Z5 gegenläufig auf dem Z4 eiert, ist seine Achse wieder ortsfest, und der Ausgang kann direkt von diesem Z5 abgenommen werden. Dafür ist diese Konstruktion dann doch zu grob.