---
layout: "image"
title: "hypozyk587.JPG"
date: "2014-04-21T22:22:01"
picture: "IMG_0587mit.JPG"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/38586
imported:
- "2019"
_4images_image_id: "38586"
_4images_cat_id: "2883"
_4images_user_id: "4"
_4images_image_date: "2014-04-21T22:22:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38586 -->
Ein Exkurs: für das Getriebe braucht man Zahnräder, deren Zähnezahlen möglichst dicht beieinander liegen. Das geht nicht mit dem, was man bei ft unter dem Suchbegriff "Zahnrad" so findet. Die "krummen Zähnezahlen" muss man sich irgendwie zusammen tricksen; als Verschärfung werden auch "krumme" Innenzahnräder gebraucht. Da hilft zum Einen die 

fischertechnik Trickfibel von Triceratops: http://www.ftcommunity.de/data/downloads/beschreibungen/trickfibel.pdf

und zum Anderen eine Kette aus schwarzen Rastkettengliedern. Deren Anschlüsse ragen so weit heraus, dass sie sauber in die ft-Zahnräder eingreifen. Das Bild zeigt eine Auswahl der Möglichkeiten (weitere siehe Trickfibel). Rechts außen sieht man zwei Teile vom schwarzen Differenzial, mit einem Z15, das fest am Z20 dran sitzt, aber beide laufen ohne Klemmung auf der Achse. Die anderen Objekte sind auch entweder lose sitzend, oder können als Stirnkupplung herhalten. 

Was hier nicht gezeigt ist, ist ein Z28, das entsteht, wenn man eine Kette um das Drehschalter-Oberteil 31311 herum wickelt.