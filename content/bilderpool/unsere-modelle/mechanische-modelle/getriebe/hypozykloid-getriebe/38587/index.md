---
layout: "image"
title: "hypozyk561.jpg"
date: "2014-04-21T22:28:21"
picture: "IMG_0561.JPG"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/38587
imported:
- "2019"
_4images_image_id: "38587"
_4images_cat_id: "2883"
_4images_user_id: "4"
_4images_image_date: "2014-04-21T22:28:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38587 -->
Der Anfang, noch auf der kurzen Bank montiert: das fest stehende Z42, darin die Eingangswelle mit Übergang (Kabelbinder) auf den Exzenter (Rastachse 30 in der Mitte). Ein paar Stücke Klebeband sorgen nachher für den richtigen Abstand zwischen Z42 und Z40. Das Z15 auf dem Exzenter dient als Freilauf"nabe": es passt in die innere Aufnahme (da wo eigentlich ft-Naben hin gehören), aber es baut kürzer.