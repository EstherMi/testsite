---
layout: "comment"
hidden: true
title: "18955"
date: "2014-04-23T21:16:11"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Jetzt ist der Harald offenbar mit allem Symmetrischen durch, und nun wird das Universum des Asymmetrischen durchforstet... da kommt noch was auf uns zu!
(Demnächst dann die Uhr-Übersetzung auf einer Achslänge?)
Gruß, Dirk