---
layout: "image"
title: "hypozyk594"
date: "2014-04-21T21:46:41"
picture: "IMG_0594.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/38580
imported:
- "2019"
_4images_image_id: "38580"
_4images_cat_id: "2883"
_4images_user_id: "4"
_4images_image_date: "2014-04-21T21:46:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38580 -->
Als "Technologie-Demonstrator" diente dieser Aufbau, ein zweistufiges Hypozykloidgetriebe mit recht grobschlächtigem Verhalten - aber was will man auch anderes von den grob nachgebildeten Z4, Z5 und Z6 erwarten.