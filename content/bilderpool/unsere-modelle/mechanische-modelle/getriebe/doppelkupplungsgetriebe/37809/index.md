---
layout: "image"
title: "Doppelkupplungsgetriebe 04"
date: "2013-11-03T15:29:10"
picture: "doppelkupplungsgetriebe4.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/37809
imported:
- "2019"
_4images_image_id: "37809"
_4images_cat_id: "2810"
_4images_user_id: "502"
_4images_image_date: "2013-11-03T15:29:10"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37809 -->
