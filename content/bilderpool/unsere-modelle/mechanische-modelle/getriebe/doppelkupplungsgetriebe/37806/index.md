---
layout: "image"
title: "Doppelkupplungsgetriebe 01"
date: "2013-11-03T15:29:10"
picture: "doppelkupplungsgetriebe1.jpg"
weight: "13"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/37806
imported:
- "2019"
_4images_image_id: "37806"
_4images_cat_id: "2810"
_4images_user_id: "502"
_4images_image_date: "2013-11-03T15:29:10"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37806 -->
Also, die zwei unteren schwarzen Power-Motoren treiben das rechte Schwungrad an, dieses treibt die zwei darunter liegenden Kupplungen an (Moosgummibelag). Die Kupplungen werden über einen Seilzug durch die zwei roten Power-Motoren unten links betätigt. Die Motoren können sich auf der Grundplatte verschieben, durch die blaue Feder wird die Anpresskraft aufgebaut. Von den Kupplungen geht es in der Mitte hoch zu den beiden Teilgetrieben mit je drei Gängen (5 Gänge plus Rückwärtsgang). Das vordere Teilgetriebe besitzt die ungeraden Gänge, das hintere Teilgetriebe die geraden Gänge und den Rückwärtsgang. Momentan ist der erste Gang und der Rückwärtsgang eingelegt. Die einzelnen Gänge werden durch die zwei oberen grauen Power-Motoren von außen nach innen hineingedrückt (rattert schön). Wenn z.B. der dritte Gang eingelegt werden soll, fährt die Betätigungseinheit eine Position nach rechts (angetrieben durch die oberen schwarzen Power-Motoren). Die Getriebeausgangswelle sitzt zentral zwischen den beiden Eingangswellen und treibt schließlich das linke Schwungrad an.