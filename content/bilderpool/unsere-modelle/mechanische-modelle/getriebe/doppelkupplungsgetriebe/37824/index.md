---
layout: "image"
title: "Doppelkupplungsgetriebe 19"
date: "2013-11-11T09:44:44"
picture: "DSCN1043.jpg"
weight: "12"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/37824
imported:
- "2019"
_4images_image_id: "37824"
_4images_cat_id: "2810"
_4images_user_id: "502"
_4images_image_date: "2013-11-11T09:44:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37824 -->
