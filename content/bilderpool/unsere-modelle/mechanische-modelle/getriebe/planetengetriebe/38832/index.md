---
layout: "image"
title: "Großaufnahme des Stegs"
date: "2014-05-22T14:33:36"
picture: "Steg.jpg"
weight: "4"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: ["Planetengetriebe", "Mini-Zahnräder"]
uploadBy: "geometer"
license: "unknown"
legacy_id:
- details/38832
imported:
- "2019"
_4images_image_id: "38832"
_4images_cat_id: "2901"
_4images_user_id: "1088"
_4images_image_date: "2014-05-22T14:33:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38832 -->
