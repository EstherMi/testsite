---
layout: "image"
title: "Centrifugaal Getriebe mit 3 Gänge und eine Freilauf"
date: "2010-01-07T08:22:39"
picture: "autotechnik10.jpg"
weight: "11"
konstrukteure: 
- "Peter, Poederoyen NL"
fotografen:
- "Peter, Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/26024
imported:
- "2019"
_4images_image_id: "26024"
_4images_cat_id: "1836"
_4images_user_id: "22"
_4images_image_date: "2010-01-07T08:22:39"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26024 -->
Das Centrifugaal Getriebe hat 3 Gänge und eine Freilauf