---
layout: "image"
title: "Centrifugaal Getriebe mit 3 Gänge und eine Freilauf"
date: "2010-01-07T08:22:38"
picture: "autotechnik07.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/26021
imported:
- "2019"
_4images_image_id: "26021"
_4images_cat_id: "1836"
_4images_user_id: "22"
_4images_image_date: "2010-01-07T08:22:38"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26021 -->
Detail des Centrifugaal Getriebe mit 3 Gänge und eine Freilauf
Jetzt beim niedrigen Geschwindigkeit.