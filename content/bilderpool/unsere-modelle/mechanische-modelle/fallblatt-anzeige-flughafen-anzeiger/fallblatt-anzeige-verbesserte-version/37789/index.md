---
layout: "image"
title: "Fallblatt-Anzeige Antrieb"
date: "2013-11-02T14:06:04"
picture: "Fallblatt_Anzeige_Antrieb.jpg"
weight: "2"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: ["Fallblatt-Anzeige", "Klappertafel", "Flughafen", "Bahnhof", "Split", "flap", "display", "Airport"]
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/37789
imported:
- "2019"
_4images_image_id: "37789"
_4images_cat_id: "2808"
_4images_user_id: "724"
_4images_image_date: "2013-11-02T14:06:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37789 -->
Funktionsweise des Antriebs der Fallblatt-Anzeige.
Man findet sie auf Flughäfen und in Bahnhöfen.
Eine faszinierende Technik.
Hier gibt es das Video:
http://youtu.be/eib9LJUmIOM