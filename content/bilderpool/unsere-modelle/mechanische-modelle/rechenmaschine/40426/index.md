---
layout: "image"
title: "Rechenmaschine mit fließendem Zehnerübertrag"
date: "2015-01-26T18:34:25"
picture: "Rechenmaschine.jpg"
weight: "1"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- details/40426
imported:
- "2019"
_4images_image_id: "40426"
_4images_cat_id: "3030"
_4images_user_id: "1088"
_4images_image_date: "2015-01-26T18:34:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40426 -->
Ein Video findet sich unter http://youtu.be/9a_us8rMczw. Details werden in ft:pedia 1/2015 beschrieben.