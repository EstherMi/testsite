---
layout: "comment"
hidden: true
title: "20105"
date: "2015-01-27T12:29:33"
uploadBy:
- "geometer"
license: "unknown"
imported:
- "2019"
---
Hallo Harry,

das sind 27mm lange Zahnstocherabschnitte, die ich an den Enden mit einer Flachzange ein ganz kleines bisschen flach gequetscht habe. So lassen sie sich sehr schnell setzen und wieder löschen. Die Geschwindigkeit ist wichtig, denn man stellt damit ja Zahlen zwischen 00 und 99 ein. Zuerst hatte ich Drahtstifte verwendet - das sieht natürlich technischer aus.

Viele Grüße

Thomas