---
layout: "image"
title: "Abschussrampe mit Rädern"
date: "2009-08-12T09:44:29"
picture: "abschussrampe02.jpg"
weight: "2"
konstrukteure: 
- "Laurens Wagner"
fotografen:
- "Laurens Wagner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Laurens"
license: "unknown"
legacy_id:
- details/24739
imported:
- "2019"
_4images_image_id: "24739"
_4images_cat_id: "1703"
_4images_user_id: "987"
_4images_image_date: "2009-08-12T09:44:29"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24739 -->
Abschussräder