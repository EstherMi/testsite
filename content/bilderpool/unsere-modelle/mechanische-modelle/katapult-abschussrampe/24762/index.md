---
layout: "image"
title: "Zylinder"
date: "2009-08-12T09:44:30"
picture: "abschussrampe25.jpg"
weight: "25"
konstrukteure: 
- "Laurens Wagner"
fotografen:
- "Laurens Wagner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Laurens"
license: "unknown"
legacy_id:
- details/24762
imported:
- "2019"
_4images_image_id: "24762"
_4images_cat_id: "1703"
_4images_user_id: "987"
_4images_image_date: "2009-08-12T09:44:30"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24762 -->
Sicherheitsnetz Teil 2