---
layout: "image"
title: "Von Oben"
date: "2009-08-12T09:44:30"
picture: "abschussrampe17.jpg"
weight: "17"
konstrukteure: 
- "Laurens Wagner"
fotografen:
- "Laurens Wagner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Laurens"
license: "unknown"
legacy_id:
- details/24754
imported:
- "2019"
_4images_image_id: "24754"
_4images_cat_id: "1703"
_4images_user_id: "987"
_4images_image_date: "2009-08-12T09:44:30"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24754 -->
Das ist die Steuerung von Oben