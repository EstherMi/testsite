---
layout: "image"
title: "Die ersten begeisterten und hoch konzentrierten'Convention' Besucher testen den....."
date: "2015-11-01T15:32:09"
picture: "heisserdrahtdklingeldrahtreloaded01.jpg"
weight: "1"
konstrukteure: 
- "Kai B aus D"
fotografen:
- "DasKasperle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- details/42176
imported:
- "2019"
_4images_image_id: "42176"
_4images_cat_id: "3145"
_4images_user_id: "1677"
_4images_image_date: "2015-11-01T15:32:09"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42176 -->
Der Heiße Draht bzw. Klingeldraht neu aufgelegt.
https://www.youtube.com/watch?v=u2X5_4yOIuc

Im Ursprungsspiel ist der Kontaktdraht (KD) NICHT bewegt.
Beim der Erweiterung der Spielidee bewegt sich der KD in verschiedenen Geschwindigkeiten und es gibt Richtung´s Änderungen
(Standard/Level1=Stillstand; Level 2=Langsam; Level3=Schnell; Level4=CrazyHorse[Verschiedene Geschwindigkeiten und Richtungswechsel] ). Level4 ist ein Hiddenpart. *lach&freu

Am Anfang des Spiels wählt man den Schwierigkeitsgrad durch drücken der entsprechenden Taste. Der gewählte Level wird durch das entsprechende Licht angezeigt. Sobald die Anzeige erloschen ist,  kann man das Spiel durch schließen des Stromkreises mittels Handstück und der Startkontaktfläche(Erster Alu-Abschnitt auf dem KD ) starten. (Je nach Level bewegt sich jetzt der KD)
Zu Beginn des Spiels hat man drei Leben. (angedeutet durch drei Lampen)
Bei jedem Kontakt zwischen Handstück und KD (Schließen des Stromkreislaufs), wird einem ein Leben abgezogen und es erklingt ein Summer als akustisches Signal.
Sind alle drei Leben verbraucht endet das Spiel und ein Audiomodul gibt eine "bedauernde & ermunternde" Ansage aus.
Erreicht man die ZielKontaktfläche vor dem Verbrauch aller Leben erkling eine Beglückwünschung. Hat sich der KD während des Spiels gedreht, wird er jetzt für die nächste Runde wieder in Ausgangs Situation gedreht.

Der Schwierigkeitsgrad kann durch die Form und Aufbau des KD und die Schlingengröße des Handstücks variiert werden.

Im Laufe der Zeit korrodieren der KupferKD und die Kupferschlaufe des Handstücks. Zwei rotierende ftRadnaben beklebt mit einem Stück von einem Schwammrücken auf einer Achse dienen als Polierhilfe. So kann man die bessere Leitfähigkeit der Kupferteile wieder herstellen. 

Idee & Umsetzung: Kai Baumgart

Konstruktionsmaterial von fischertechnik, MisterWho (Drehkranz 3DDruck mit Kugellager), TST (Wellenkupplung , Stellring 4mm & Verteilerplatte), Bauhaus (Kupferdraht/Erdungskabel, Kabelbinder & Schrumpfschlauch ) 
Musik: Mozart Requiem in d minor (https://www.youtube.com/watch?v=sPlhKP0nZII)
