---
layout: "image"
title: "Aufhängung des Kontaktdrahts Links & Endlagentaster"
date: "2015-11-01T15:32:09"
picture: "heisserdrahtdklingeldrahtreloaded10.jpg"
weight: "10"
konstrukteure: 
- "Kai B aus D"
fotografen:
- "DasKasperle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- details/42185
imported:
- "2019"
_4images_image_id: "42185"
_4images_cat_id: "3145"
_4images_user_id: "1677"
_4images_image_date: "2015-11-01T15:32:09"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42185 -->
mit Kontaktzone:
Rot=Start (bei berühren mit dem Handdraht (Öse) beginnt das Spiel)
Gelb=Verlust eines "Lebens" 

Auf einem Stellring besfestigter Kabelbinder drückt den Taster in Endlage bzw. Ausgangsposition.