---
layout: "image"
title: "Motorantrieb des Kontakdrahts - oben -"
date: "2015-11-01T15:32:09"
picture: "heisserdrahtdklingeldrahtreloaded12.jpg"
weight: "12"
konstrukteure: 
- "Kai B aus D"
fotografen:
- "DasKasperle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- details/42187
imported:
- "2019"
_4images_image_id: "42187"
_4images_cat_id: "3145"
_4images_user_id: "1677"
_4images_image_date: "2015-11-01T15:32:09"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42187 -->
Ein Powermotor 9 Volt, Getriebe 50:1 treibt über Zahnräder und Schnecke den Minidrehkranz, und somit den KD an.