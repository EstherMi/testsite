---
layout: "image"
title: "Aufhängung & Führung des Kontaktdrahts rechte Seite von - hinten -"
date: "2015-11-01T15:32:09"
picture: "heisserdrahtdklingeldrahtreloaded14.jpg"
weight: "14"
konstrukteure: 
- "Kai B aus D"
fotografen:
- "DasKasperle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- details/42189
imported:
- "2019"
_4images_image_id: "42189"
_4images_cat_id: "3145"
_4images_user_id: "1677"
_4images_image_date: "2015-11-01T15:32:09"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42189 -->
