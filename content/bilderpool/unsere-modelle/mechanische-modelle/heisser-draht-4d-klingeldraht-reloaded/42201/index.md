---
layout: "image"
title: "Bedienfeld von  - Vorderseite 2 -"
date: "2015-11-01T15:32:09"
picture: "heisserdrahtdklingeldrahtreloaded26.jpg"
weight: "26"
konstrukteure: 
- "Kai B aus D"
fotografen:
- "DasKasperle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- details/42201
imported:
- "2019"
_4images_image_id: "42201"
_4images_cat_id: "3145"
_4images_user_id: "1677"
_4images_image_date: "2015-11-01T15:32:09"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42201 -->
Die Kabel wurden platzsparend mit QTip-Röhrchen anstatt mit ftSteckern befestigt.