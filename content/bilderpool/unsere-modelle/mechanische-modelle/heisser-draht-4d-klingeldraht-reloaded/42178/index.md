---
layout: "image"
title: "HeißerDraht4D Gesamtansicht - oben -"
date: "2015-11-01T15:32:09"
picture: "heisserdrahtdklingeldrahtreloaded03.jpg"
weight: "3"
konstrukteure: 
- "Kai B aus D"
fotografen:
- "DasKasperle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- details/42178
imported:
- "2019"
_4images_image_id: "42178"
_4images_cat_id: "3145"
_4images_user_id: "1677"
_4images_image_date: "2015-11-01T15:32:09"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42178 -->
