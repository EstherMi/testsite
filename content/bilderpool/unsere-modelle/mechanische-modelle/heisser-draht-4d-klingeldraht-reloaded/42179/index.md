---
layout: "image"
title: "HeißerDraht4D Gesamtansicht mit Schwerpunkt Kontaktdraht-'Labyrinth'"
date: "2015-11-01T15:32:09"
picture: "heisserdrahtdklingeldrahtreloaded04.jpg"
weight: "4"
konstrukteure: 
- "Kai B aus D"
fotografen:
- "DasKasperle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- details/42179
imported:
- "2019"
_4images_image_id: "42179"
_4images_cat_id: "3145"
_4images_user_id: "1677"
_4images_image_date: "2015-11-01T15:32:09"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42179 -->
Der Kontaktdrath ist dank der Wellenkupplung 4 mm von TST austauschbar.

Dieses "Labyrint" gehört in die Kategorie: schwer