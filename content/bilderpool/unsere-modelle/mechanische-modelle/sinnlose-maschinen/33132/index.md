---
layout: "image"
title: "Hebel ohne Abdeckung"
date: "2011-10-12T19:38:24"
picture: "sinnlosemaschineftfan07.jpg"
weight: "7"
konstrukteure: 
- "FT-FAN"
fotografen:
- "FT-FAN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FT-Fan"
license: "unknown"
legacy_id:
- details/33132
imported:
- "2019"
_4images_image_id: "33132"
_4images_cat_id: "2449"
_4images_user_id: "1371"
_4images_image_date: "2011-10-12T19:38:24"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33132 -->
Hier in ON-Stellung
Die Lichtschranke meldet die Stellung an das Interface 1= OFF   0=ON