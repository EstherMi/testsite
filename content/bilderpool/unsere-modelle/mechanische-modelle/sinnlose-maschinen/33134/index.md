---
layout: "image"
title: "Innenansicht"
date: "2011-10-12T19:38:24"
picture: "sinnlosemaschineftfan09.jpg"
weight: "9"
konstrukteure: 
- "FT-FAN"
fotografen:
- "FT-FAN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FT-Fan"
license: "unknown"
legacy_id:
- details/33134
imported:
- "2019"
_4images_image_id: "33134"
_4images_cat_id: "2449"
_4images_user_id: "1371"
_4images_image_date: "2011-10-12T19:38:24"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33134 -->
Hier kann man den Antriebsmotor, darunter den Summer und vor dem Summer den Auschaltarm sehen.
