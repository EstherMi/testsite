---
layout: "image"
title: "Containergreifer 10"
date: "2008-07-26T16:23:13"
picture: "containergreifer10.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/14956
imported:
- "2019"
_4images_image_id: "14956"
_4images_cat_id: "1365"
_4images_user_id: "389"
_4images_image_date: "2008-07-26T16:23:13"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14956 -->
Hier ist nun der geschlossene Zustand zu sehen.
Dabei muß die rote Lampe aus und die grüne an gehen.