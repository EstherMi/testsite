---
layout: "image"
title: "Containergreifer 07"
date: "2008-07-26T16:23:13"
picture: "containergreifer07.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/14953
imported:
- "2019"
_4images_image_id: "14953"
_4images_cat_id: "1365"
_4images_user_id: "389"
_4images_image_date: "2008-07-26T16:23:13"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14953 -->
Der Greifer hat zugepakt und die rote Lampe leuchtet.