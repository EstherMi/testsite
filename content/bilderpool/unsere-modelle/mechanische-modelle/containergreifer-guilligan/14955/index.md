---
layout: "image"
title: "Containergreifer 09"
date: "2008-07-26T16:23:13"
picture: "containergreifer09.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/14955
imported:
- "2019"
_4images_image_id: "14955"
_4images_cat_id: "1365"
_4images_user_id: "389"
_4images_image_date: "2008-07-26T16:23:13"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14955 -->
Das Ganze noch weiter gedreht um den offen stehenden Greifer und die rote Lampe zu sehen.