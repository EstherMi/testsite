---
layout: "image"
title: "Kompasswagen vom Typ Lanchester"
date: "2014-05-06T12:31:40"
picture: "Lancaster1c.jpg"
weight: "1"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- details/38750
imported:
- "2019"
_4images_image_id: "38750"
_4images_cat_id: "2896"
_4images_user_id: "1088"
_4images_image_date: "2014-05-06T12:31:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38750 -->
