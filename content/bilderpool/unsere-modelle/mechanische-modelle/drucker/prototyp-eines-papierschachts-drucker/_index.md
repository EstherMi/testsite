---
layout: "overview"
title: "Prototyp eines Papierschachts für einen Drucker"
date: 2019-12-17T19:24:47+01:00
legacy_id:
- categories/2094
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2094 --> 
Dies soll ein Papierfach für einen Drucker mit automatischer Einzelblattzufuhr werden. Dies ist die nachträgliche Dokumentation über den Prototypen, den ich auf der Convention 2010 dabei hatte.