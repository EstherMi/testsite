---
layout: "image"
title: "Gesamtansicht"
date: "2010-09-29T20:02:40"
picture: "prototypeinespapierschachtsfuereinendrucker1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/28756
imported:
- "2019"
_4images_image_id: "28756"
_4images_cat_id: "2094"
_4images_user_id: "104"
_4images_image_date: "2010-09-29T20:02:40"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28756 -->
Die Bauplatte 500 ist der Boden, auf den der Papierstapel gelegt wird. Er ist hinten (hier: links) beweglich gelagert (die Verkleidungsplatten 15 * 60 sind ja leicht biegbar). Vorne wird er von unten mittels Federkraft nach oben gedrückt. Rechts kommt das Papier heraus.