---
layout: "image"
title: "Von unten (1)"
date: "2010-09-29T20:02:41"
picture: "prototypeinespapierschachtsfuereinendrucker3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/28758
imported:
- "2019"
_4images_image_id: "28758"
_4images_cat_id: "2094"
_4images_user_id: "104"
_4images_image_date: "2010-09-29T20:02:41"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28758 -->
Hier blickt man auf die Unterseite. Die Vorstuferollen werden also mitsamt der Bauplatte 500 nach oben gegen die Räder mit den Gummireifen gedrückt. Dadurch können die sich frei drehen, wenn kein Papier mehr im Fach ist, und reiben nicht.