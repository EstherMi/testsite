---
layout: "image"
title: "Funktionsweise (2)"
date: "2010-09-29T20:02:41"
picture: "prototypeinespapierschachtsfuereinendrucker6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/28761
imported:
- "2019"
_4images_image_id: "28761"
_4images_cat_id: "2094"
_4images_user_id: "104"
_4images_image_date: "2010-09-29T20:02:41"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28761 -->
Der noch zu ergänzende Motor muss über den Freilauf die Gummiräder antreiben. Solange die nicht zu verstaubt sind, wird dadurch ein und nur ein Blatt Papier nach vorne hinausgeschoben. Alle anderen Blätter bleiben nämlich unten hängen, während das oberste Blatt wie hier sichtbar an den Ecken für einen kurzen Moment einknickt, bevor es das Fach verlässt.