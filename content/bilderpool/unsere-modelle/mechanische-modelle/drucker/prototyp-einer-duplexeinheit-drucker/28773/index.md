---
layout: "image"
title: "Lichtschranke (2)"
date: "2010-09-29T20:02:43"
picture: "prototypeinerduplexeinheitfuereinendrucker07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/28773
imported:
- "2019"
_4images_image_id: "28773"
_4images_cat_id: "2095"
_4images_user_id: "104"
_4images_image_date: "2010-09-29T20:02:43"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28773 -->
Das rote 30x15-Teil auf dem BS30 ist dieser Spiegel, der das Licht wieder durch die Papierwege reflektiert. Es landet auf der auf der anderen Seite befindlichen Fotozelle (man sieht deren feine Streifen).