---
layout: "image"
title: "Der Dreiecksbalken (2)"
date: "2010-09-29T20:02:43"
picture: "prototypeinerduplexeinheitfuereinendrucker11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/28777
imported:
- "2019"
_4images_image_id: "28777"
_4images_cat_id: "2095"
_4images_user_id: "104"
_4images_image_date: "2010-09-29T20:02:43"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28777 -->
So ist der Balken von innen aufgebaut:

- Außen nur durch zwei Winkel gehalten (das genügt tatsächlich völlig).

- Innen bewirken Winkelsteine 60° mit drei Nuten (also ohne Zapen, gibt's einzeln bestellbar) und eine versetzte Anordnung der Verkleidungsplatten, dass der Balken außen glatt und innen selbsttragend wird.