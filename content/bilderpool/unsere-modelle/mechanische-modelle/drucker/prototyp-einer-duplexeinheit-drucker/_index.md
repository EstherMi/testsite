---
layout: "overview"
title: "Prototyp einer Duplexeinheit für einen Drucker"
date: 2019-12-17T19:24:48+01:00
legacy_id:
- categories/2095
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2095 --> 
Für einen Drucker, der Vorder- und Rückseite bedrucken können soll: Dies ist die nachträgliche Dokumentation für das Modell, das ich auf der Convention 2010 dabei hatte.