---
layout: "image"
title: "Stempelmaschine"
date: "2012-02-19T00:46:22"
picture: "stempelmaschine15.jpg"
weight: "15"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- details/34259
imported:
- "2019"
_4images_image_id: "34259"
_4images_cat_id: "2536"
_4images_user_id: "1361"
_4images_image_date: "2012-02-19T00:46:22"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34259 -->
Blick von oben auf den Stempeltisch.