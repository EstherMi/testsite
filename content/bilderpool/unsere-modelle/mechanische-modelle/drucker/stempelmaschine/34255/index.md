---
layout: "image"
title: "Stempelmaschine"
date: "2012-02-19T00:46:21"
picture: "stempelmaschine11.jpg"
weight: "11"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- details/34255
imported:
- "2019"
_4images_image_id: "34255"
_4images_cat_id: "2536"
_4images_user_id: "1361"
_4images_image_date: "2012-02-19T00:46:21"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34255 -->
Der Stempler beim Schwenk zum Stempeltisch.