---
layout: "image"
title: "Stempelmaschine"
date: "2012-02-19T00:46:10"
picture: "stempelmaschine01.jpg"
weight: "1"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- details/34245
imported:
- "2019"
_4images_image_id: "34245"
_4images_cat_id: "2536"
_4images_user_id: "1361"
_4images_image_date: "2012-02-19T00:46:10"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34245 -->
Man erkennt rechts das ausgeklappte Magazin.
Ein Video, das die Stempelmaschine in Betrieb zeigt, kann auf YuoTube unter "fischertechnik-Stempelmaschine" angesehen werden.