---
layout: "image"
title: "Stempelmaschine"
date: "2012-02-19T00:46:22"
picture: "stempelmaschine14.jpg"
weight: "14"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- details/34258
imported:
- "2019"
_4images_image_id: "34258"
_4images_cat_id: "2536"
_4images_user_id: "1361"
_4images_image_date: "2012-02-19T00:46:22"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34258 -->
Der Stempeltisch mit dem Farbsensor. Das rote Filzrechteck auf dem Stempeltisch sorgt für saubere Stempelabdrücke.