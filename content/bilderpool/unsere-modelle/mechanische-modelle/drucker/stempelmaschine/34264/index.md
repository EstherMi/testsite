---
layout: "image"
title: "Stempelmaschine"
date: "2012-02-19T00:46:22"
picture: "stempelmaschine20.jpg"
weight: "20"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- details/34264
imported:
- "2019"
_4images_image_id: "34264"
_4images_cat_id: "2536"
_4images_user_id: "1361"
_4images_image_date: "2012-02-19T00:46:22"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34264 -->
Die Auswurfrollen.