---
layout: "image"
title: "Stempelmaschine"
date: "2012-02-19T00:46:10"
picture: "stempelmaschine05.jpg"
weight: "5"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- details/34249
imported:
- "2019"
_4images_image_id: "34249"
_4images_cat_id: "2536"
_4images_user_id: "1361"
_4images_image_date: "2012-02-19T00:46:10"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34249 -->
Das ausgeklappte Magazin mit den 9 x9 cm großen Notizzetteln-