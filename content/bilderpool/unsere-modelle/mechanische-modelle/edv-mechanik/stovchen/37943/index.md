---
layout: "image"
title: "Temperatursensor"
date: "2013-12-26T15:23:58"
picture: "stoevchenIRsensor_800x.jpg"
weight: "3"
konstrukteure: 
- "axel"
fotografen:
- "axel"
keywords: ["IR", "Temperatursensor", "Bricklet"]
uploadBy: "axel"
license: "unknown"
legacy_id:
- details/37943
imported:
- "2019"
_4images_image_id: "37943"
_4images_cat_id: "2824"
_4images_user_id: "2056"
_4images_image_date: "2013-12-26T15:23:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37943 -->
Montage des IR Temperatursensor Bricklets