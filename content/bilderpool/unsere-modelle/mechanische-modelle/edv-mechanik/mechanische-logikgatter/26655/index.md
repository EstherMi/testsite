---
layout: "image"
title: "XOR aus drei NAND-Bausteinen 4"
date: "2010-03-07T11:12:24"
picture: "XOR11.jpg"
weight: "11"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: ["Logikgatter", "UND", "NAND", "NOR", "ODER", "XOR"]
uploadBy: "geometer"
license: "unknown"
legacy_id:
- details/26655
imported:
- "2019"
_4images_image_id: "26655"
_4images_cat_id: "1900"
_4images_user_id: "1088"
_4images_image_date: "2010-03-07T11:12:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26655 -->
Hier sind A=B=1 und die Mechanik bildet A XOR B = 0.