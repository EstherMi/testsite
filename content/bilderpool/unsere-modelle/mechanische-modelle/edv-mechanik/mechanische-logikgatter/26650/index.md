---
layout: "image"
title: "Funktionsweise NAND"
date: "2010-03-07T10:57:05"
picture: "Funktionsweise.jpg"
weight: "6"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- details/26650
imported:
- "2019"
_4images_image_id: "26650"
_4images_cat_id: "1900"
_4images_user_id: "1088"
_4images_image_date: "2010-03-07T10:57:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26650 -->
Hier sieht man die Zusammensetzung von NAND aus Mittelwertbildung, Multiplikation mit 3/2 (nicht -2 !!) und Schwellwertbildung.