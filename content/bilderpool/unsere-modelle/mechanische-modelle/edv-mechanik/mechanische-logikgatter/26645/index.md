---
layout: "image"
title: "Einzelteile für ein Gatter"
date: "2010-03-07T10:57:04"
picture: "Einzelteile.jpg"
weight: "1"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- details/26645
imported:
- "2019"
_4images_image_id: "26645"
_4images_cat_id: "1900"
_4images_user_id: "1088"
_4images_image_date: "2010-03-07T10:57:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26645 -->
Hier sieht man die benötigten Einzelteile für ein Gatter.