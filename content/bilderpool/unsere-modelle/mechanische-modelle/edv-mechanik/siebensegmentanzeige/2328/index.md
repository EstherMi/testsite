---
layout: "image"
title: "Siebensegmentanzeige - Ziffer b"
date: "2004-04-05T18:47:45"
picture: "13_-_ZifferB.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/2328
imported:
- "2019"
_4images_image_id: "2328"
_4images_cat_id: "233"
_4images_user_id: "104"
_4images_image_date: "2004-04-05T18:47:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2328 -->
