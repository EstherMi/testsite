---
layout: "image"
title: "Siebensegmentanzeige - Anzeige von der Seite"
date: "2004-04-05T18:47:45"
picture: "20_-_Schaltwalze_1.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/2335
imported:
- "2019"
_4images_image_id: "2335"
_4images_cat_id: "233"
_4images_user_id: "104"
_4images_image_date: "2004-04-05T18:47:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2335 -->
Das Herz des Ganzen: Die Schaltwalze. Für die linken vier Taster in einem Gray-Code (zwischen zwei Stellungen ändert sich nur *ein* Taster), für die restlichen 7 der Form der jeweiligen Ziffer angepasst kodiert.