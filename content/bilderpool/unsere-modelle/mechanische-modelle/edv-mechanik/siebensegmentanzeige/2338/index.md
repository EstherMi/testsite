---
layout: "image"
title: "Siebensegmentanzeige - Schaltwalze (4)"
date: "2004-04-05T18:47:54"
picture: "23_-_Schaltwalze_4.jpg"
weight: "22"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/2338
imported:
- "2019"
_4images_image_id: "2338"
_4images_cat_id: "233"
_4images_user_id: "104"
_4images_image_date: "2004-04-05T18:47:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2338 -->
Gegenüber dem ersten Bild um 270 Grad  nach hinten gedreht.