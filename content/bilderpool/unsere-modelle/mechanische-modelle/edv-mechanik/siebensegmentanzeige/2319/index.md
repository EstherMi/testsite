---
layout: "image"
title: "Siebensegmentanzeige - Ziffer 1"
date: "2004-04-05T18:47:26"
picture: "03_-_Ziffer1.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/2319
imported:
- "2019"
_4images_image_id: "2319"
_4images_cat_id: "233"
_4images_user_id: "104"
_4images_image_date: "2004-04-05T18:47:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2319 -->
