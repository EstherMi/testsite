---
layout: "image"
title: "Siebensegmentanzeige - Ziffer 2"
date: "2004-04-05T18:47:26"
picture: "04_-_Ziffer2.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/2320
imported:
- "2019"
_4images_image_id: "2320"
_4images_cat_id: "233"
_4images_user_id: "104"
_4images_image_date: "2004-04-05T18:47:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2320 -->
