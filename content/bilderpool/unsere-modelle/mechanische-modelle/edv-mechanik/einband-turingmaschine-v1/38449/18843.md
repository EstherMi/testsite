---
layout: "comment"
hidden: true
title: "18843"
date: "2014-03-13T14:57:38"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Sitzen da die Achsen links und rechts des Motors nur in einem einzigen Statikträger geführt? Wackelt das nicht zu sehr?
Gruß,
Stefan