---
layout: "comment"
hidden: true
title: "18845"
date: "2014-03-13T16:48:43"
uploadBy:
- "Thorste_n"
license: "unknown"
imported:
- "2019"
---
Hallo Stefan,

da wackelt im Betrieb gar nichts. 
Das &#8222;Turingband&#8220; ist halt sehr lang und steif &#8211; vorne Zahnradstangen und wegen dem Überhang unten mit Metallstangen drin. 
Hat mich auch überrascht &#8211; ich hab ja längere Achsen eingesetzt um die noch unten zu lagern. Grade die Achse auf dem das Antriebszahnrad sitzt sollte ursprünglich unten unbedingt noch gelagert werden.
Ist aber gar nicht nötig &#8230;
Wenn ich mal eine zweite Version baue wird das trotzdem schön gelagert.

Gruß
Thorsten