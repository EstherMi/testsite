---
layout: "image"
title: "Turingmaschine Übersicht"
date: "2014-03-13T14:46:23"
picture: "einbandturingmaschinev1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thorste_n"
license: "unknown"
legacy_id:
- details/38448
imported:
- "2019"
_4images_image_id: "38448"
_4images_cat_id: "2867"
_4images_user_id: "2138"
_4images_image_date: "2014-03-13T14:46:23"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38448 -->
Oben:
Turingband - Bandalphabet aus BS30 {0,1} = {BS30 nach hinten oder vorn} 
Eingabealphabet E = {1} = BS30 nach vorn.

Rechts: 
Motor zur Bandbewegung  ( = Turing Schreib-Lesekopf Bewegung)
{links, halt, rechts}

Links:
Lese-/Schreibkopf {BS30 Stellung lesen und nach Regeln setzen {0,1}}

Turing Tabelle (momentan nur Addition) ist im RoboPro gespeichert.
Das Lesen (Fotowiderstände) und die Ansteuerung M1 / M2 erfolgt über das ft-LT Modul.