---
layout: "image"
title: "Testprogramm"
date: "2014-03-13T14:46:23"
picture: "einbandturingmaschinev8.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thorste_n"
license: "unknown"
legacy_id:
- details/38455
imported:
- "2019"
_4images_image_id: "38455"
_4images_cat_id: "2867"
_4images_user_id: "2138"
_4images_image_date: "2014-03-13T14:46:23"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38455 -->
Testprogramm  + Berechnung Addition ist in RoboPro umgesetzt.