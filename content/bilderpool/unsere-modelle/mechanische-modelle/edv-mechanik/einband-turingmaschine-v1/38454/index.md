---
layout: "image"
title: "LT Controller und Verkabelung"
date: "2014-03-13T14:46:23"
picture: "einbandturingmaschinev7.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thorste_n"
license: "unknown"
legacy_id:
- details/38454
imported:
- "2019"
_4images_image_id: "38454"
_4images_cat_id: "2867"
_4images_user_id: "2138"
_4images_image_date: "2014-03-13T14:46:23"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38454 -->
- Verkabelung ist nur Version 0.978alpha  :-)

Eingänge LT:

AX Messung an I1 für Bandvorschub.
AY Messung an I2  für Band lesen.
I3 für alle Endschalter (parallel) am Schreibkopf .

Motoren: 
M1 = Bandbewegung  
M2 = Bewegung Schreibkopf.