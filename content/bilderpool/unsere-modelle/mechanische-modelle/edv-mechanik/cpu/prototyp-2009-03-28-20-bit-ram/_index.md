---
layout: "overview"
title: "Prototyp 2009-03-28 - 20 Bit RAM"
date: 2019-12-17T19:24:57+01:00
legacy_id:
- categories/1606
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1606 --> 
Das sind die ersten zaghaften Versuche, Teile der geplanten fischertechnik \"NANO\" CPU zu realisieren. Hier ein Stück Arbeitsspeicher mit sage und schreibe 20 Bit Kapazität. Siehe auch im Forum unter http://www.fischertechnik.de/de/fanclub/forum/default.aspx?g=posts&t=3409.