---
layout: "image"
title: "20 Bit RAM - Das erste Prototypenteil für die fischertechnik NANO CPU"
date: "2009-03-28T16:15:45"
picture: "ram1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/23526
imported:
- "2019"
_4images_image_id: "23526"
_4images_cat_id: "1606"
_4images_user_id: "104"
_4images_image_date: "2009-03-28T16:15:45"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23526 -->
Das sind die ersten zaghaften Versuche, Teile der geplanten fischertechnik "NANO" CPU tatsächlich wenigstens als Prototyp zu realisieren. Hier ein Stück Arbeitsspeicher mit sage und schreibe 20 Bit Kapazität.

Im Forum gibts bereits eine Diskussion dazu unter http://www.fischertechnik.de/de/fanclub/forum/default.aspx?g=posts&t=3409