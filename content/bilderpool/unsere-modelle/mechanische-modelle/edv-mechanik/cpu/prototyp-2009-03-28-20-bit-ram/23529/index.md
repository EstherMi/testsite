---
layout: "image"
title: "Schreib-/Leseeinheit (2)"
date: "2009-03-28T16:15:47"
picture: "ram4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/23529
imported:
- "2019"
_4images_image_id: "23529"
_4images_cat_id: "1606"
_4images_user_id: "104"
_4images_image_date: "2009-03-28T16:15:47"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23529 -->
Hier sieht man die Lichtschrankenanordnung ganz gut. Der Taster oben ist die Endlage des Bit-Setzers (des "Wedels").