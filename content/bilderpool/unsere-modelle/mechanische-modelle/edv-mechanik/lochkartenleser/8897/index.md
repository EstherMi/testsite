---
layout: "image"
title: "Lochstreifenleser"
date: "2007-02-10T15:13:34"
picture: "lochkartenleser2.jpg"
weight: "2"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/8897
imported:
- "2019"
_4images_image_id: "8897"
_4images_cat_id: "807"
_4images_user_id: "453"
_4images_image_date: "2007-02-10T15:13:34"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8897 -->
Gesamtansicht