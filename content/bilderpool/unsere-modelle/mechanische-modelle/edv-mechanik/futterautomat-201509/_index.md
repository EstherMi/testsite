---
layout: "overview"
title: "Futterautomat 201509"
date: 2019-12-17T19:25:05+01:00
legacy_id:
- categories/3126
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3126 --> 
POC (Machbarkeitsnachweis) eines Futterautomaten für Haustiere.

Aufgabe:
Wenn ein Tier in die Nähe des Bewegungssensors kommt soll eine Portion Trockenfutter bereitgestellt werden. Die maximale Anzahl der ausgeführten Futteranforderungen je Zeiteinheit soll begrenzt werden. 

Lösung:
Ein Fischertechnikmodell das über einen Drehteller gelbe Kunststoff Futterportionen aus einem Magazin abgibt.
Steuerung / Antrieb:
Verwendete Bauteile (FT = Fischertechnik, TF = Tinkerforge)
- FT Motor
- TF Distance IR Bricklet
- TF Dual Relay Bricklet
- TF Master Brick
- TF Industrial Digital In 4 Bricklet
- Conrad Relaisplatine REL-PCB3
- TF Step-Down Power Supply (ohne Bild)
- FT Reed Kontakt (2St., ohne Bild)
- FT Akku (ohne Bild)
- Magnet (ohne Bild)
Die TF Bricklets werden mit dem TF Master Brick verbunden. Eines der beiden Relais auf dem TF  Dual Relay Bricklet steuert die Conrad Relaisplatine. An das TF Industrial Digital In 4 Bricklet werden die beiden FT Reed Kontakte angeschlossen. Die FT Reed Kontakte sprechen an, wenn der, am Drehteller befestigte, Magnet die Ladepostion erreicht, bzw. die Auswurfposition überschritten hat. 
Die, wie sicher schon bemerkt wurde, eigentlich überflüssige Conrad Relaisplatine ist nötig um den FT Motor von der TF Elektronik zu entkoppeln. Es kam hier immer wieder zu unerwünschten Resets. Versuche die Störungen durch Schutzbeschaltungen zu unterdrücken waren nicht erfolgreich.
Da mir z.Zt. kein Bewegungssensor zur Verfügung steht wurde ein TF Distance IR Bricklet verwendet, dass nun bei jedem Objekt, das sich in einer definierten Entfernung vor dem Futterautomaten aufhält, eine Futteranforderung auslöst. Eine, für einen POC, sicher vertretbare Alternative.  

Software:
Ein sehr kleines JEE (Java Enterprise) Projekt, das aus einer JSF Startseite, einer &#8222;Managed Bean&#8220; und einer &#8222;Singleton Session Bean&#8220; besteht. Die Singleton Session Bean kapselt alle Zugriffe auf die Tinkerforge Komponenten. Es war mir nicht möglich die Tinkerforge Komponenten in normalen Session Beans zur reibungslosen Zusammenarbeit zu bewegen.
Nach dem Start dreht der Drehteller bis der Event &#8222;Ausswurfposition überschritten&#8220; ausgelöst wird. Hierbei kann es im Augenblick zu einer ungewollten Ausgabe einer Futterposition kommen.
Nach einer Futteranforderung (ausgelöst durch ein Objekt in der Nähe des TF Distance IR Bricklet ) erfolgt die Abfrage ob sich der Drehteller auf der Position &#8222;Auswurfposition überschritten&#8220; befindet und ob die letzte Futteranforderung länger als der gewünschte Zeitraum zurück liegt. Nur wenn beide Bedingungen erfüllt sind wird die Anforderung ausgeführt. Hier sind jederzeit unterschiedlichste Bedingungen ergänzbar.
Es wird, während eine Futteranforderung ausgeführt wird, auch ein Event bei der Ladepostion des Drehtellers ausgelöst. Dieses Event wird aber in der aktuellen Version nicht berücksichtigt. Wenn statt gelber Kunstoff Futterportionen tatsächlich Trockenfutter abgegeben wird, ist dieser Event sicherlich nützlich, um den Drehteller an der Ladeposition kurz anzuhalten.
Die Software wurde mit dem Containern Glassfish 4.1 Web und Payara Micro 4.1.153 getestet.

Anmerkungen:
Warum wurde für den POC nicht der aktuelle Fischertechnik ROBOTICS TXT Kontroller verwendet?
Um die Ergebnisse des POC später einfacher in einen echten Futterautomaten zu übernehmen. Ein netter Nebeneffekt: die Steuerung war so, mit knapp 85¤, günstiger als der  ROBOTICS TXT Kontroller.
Statt eines Bewegungs- oder Entfernungssensor könnte zur Auslösung einer Futteranforderung auch eine Lösung mit RFID Chips zum Einsatz kommen. Dann könnte man die Ausgabe evtentuell &#8220;personalisieren&#8221; (Stichwort TASSO Registrierung).

Links: 
http://www.tinkerforge.com/de
http://www.payara.co.uk/home
https://glassfish.java.net/
http://www.fischertechnik.de/