---
layout: "image"
title: "Drehtisch oben"
date: "2007-02-11T12:23:26"
picture: "drehtisch3.jpg"
weight: "4"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/8928
imported:
- "2019"
_4images_image_id: "8928"
_4images_cat_id: "810"
_4images_user_id: "1"
_4images_image_date: "2007-02-11T12:23:26"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8928 -->
