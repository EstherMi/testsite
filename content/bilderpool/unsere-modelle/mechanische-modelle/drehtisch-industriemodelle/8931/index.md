---
layout: "image"
title: "Drehtisch Werkstückaufnahme"
date: "2007-02-11T12:41:27"
picture: "Drehtisch_Aufsatz.jpg"
weight: "1"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/8931
imported:
- "2019"
_4images_image_id: "8931"
_4images_cat_id: "810"
_4images_user_id: "1"
_4images_image_date: "2007-02-11T12:41:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8931 -->
Nun ist auch eine Aufnahme für Werkstück angebracht, bzw. sagen wir der Anfang dafür.