---
layout: "comment"
hidden: true
title: "10320"
date: "2009-12-02T12:49:15"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo,
hier macht Philip allerdings etwas deutlich. Durch die neuen dünnwandigen? Gehäuse der Motoren (Encodermotor und XM-Motor) geraten kleinflächige Befestigungen an ihren Nuten nicht sehr stabil. Beim XS-Motor ist es am V-Zapfen auch nicht anders.
Gruß, Udo2