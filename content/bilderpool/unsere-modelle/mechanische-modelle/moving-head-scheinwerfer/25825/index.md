---
layout: "image"
title: "von hinten"
date: "2009-11-28T14:30:56"
picture: "movingheadmitscheinwerfer08.jpg"
weight: "8"
konstrukteure: 
- "Philip Lawall"
fotografen:
- "Philip Lawall"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fueller"
license: "unknown"
legacy_id:
- details/25825
imported:
- "2019"
_4images_image_id: "25825"
_4images_cat_id: "1812"
_4images_user_id: "1026"
_4images_image_date: "2009-11-28T14:30:56"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25825 -->
ansicht von hinten