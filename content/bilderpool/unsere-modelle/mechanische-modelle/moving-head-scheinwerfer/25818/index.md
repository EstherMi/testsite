---
layout: "image"
title: "überblick"
date: "2009-11-28T14:30:55"
picture: "movingheadmitscheinwerfer01.jpg"
weight: "1"
konstrukteure: 
- "Philip Lawall"
fotografen:
- "Philip Lawall"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fueller"
license: "unknown"
legacy_id:
- details/25818
imported:
- "2019"
_4images_image_id: "25818"
_4images_cat_id: "1812"
_4images_user_id: "1026"
_4images_image_date: "2009-11-28T14:30:55"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25818 -->
Gesamt überblick