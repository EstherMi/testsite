---
layout: "image"
title: "Steuerkreuz"
date: "2009-11-28T14:30:56"
picture: "movingheadmitscheinwerfer09.jpg"
weight: "9"
konstrukteure: 
- "Philip Lawall"
fotografen:
- "Philip Lawall"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fueller"
license: "unknown"
legacy_id:
- details/25826
imported:
- "2019"
_4images_image_id: "25826"
_4images_cat_id: "1812"
_4images_user_id: "1026"
_4images_image_date: "2009-11-28T14:30:56"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25826 -->
Steuerkreuz zum manuellen steuern