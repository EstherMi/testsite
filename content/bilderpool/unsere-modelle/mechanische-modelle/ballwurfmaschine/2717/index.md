---
layout: "image"
title: "Elektromagnet"
date: "2004-10-16T18:51:55"
picture: "05-Elektromagnet.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Elektromagnet"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/2717
imported:
- "2019"
_4images_image_id: "2717"
_4images_cat_id: "245"
_4images_user_id: "46"
_4images_image_date: "2004-10-16T18:51:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2717 -->
Der soll die Kugel halten, bis die Nenndrehzahl erreicht ist. Gar nicht so einfach, bei der herrschenden Fliehkraft.