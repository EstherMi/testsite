---
layout: "image"
title: "Abweichungen"
date: "2004-11-21T13:37:35"
picture: "Diverse_Spannungen.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/3304
imported:
- "2019"
_4images_image_id: "3304"
_4images_cat_id: "245"
_4images_user_id: "46"
_4images_image_date: "2004-11-21T13:37:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3304 -->
Bei unterschiedlicher Einstellung der Motorspannung ergeben sich unterschiedliche Wurfparabeln. Je höher die Spannung und damit die Drehzahl, desto steiler der Abwurfwinkel.