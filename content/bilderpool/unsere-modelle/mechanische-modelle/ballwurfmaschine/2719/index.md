---
layout: "image"
title: "Gabellichtschranke Detail"
date: "2004-10-16T19:05:44"
picture: "Gabellichtschranke.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Lichtschranke"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/2719
imported:
- "2019"
_4images_image_id: "2719"
_4images_cat_id: "245"
_4images_user_id: "46"
_4images_image_date: "2004-10-16T19:05:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2719 -->
Klein und fein: in der Nut befindet sich der Vorwiederstand und die restliche Verkabelung, so daß diese Lichtschranke an 5 Volt betrieben werden kann.