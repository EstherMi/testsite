---
layout: "image"
title: "Antriebsmotor"
date: "2004-10-16T18:51:55"
picture: "02-Antriebsmotor.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Gleichspannungsmotor", "Inkrementalgeber", "4mmWelle"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/2714
imported:
- "2019"
_4images_image_id: "2714"
_4images_cat_id: "245"
_4images_user_id: "46"
_4images_image_date: "2004-10-16T18:51:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2714 -->
Ein glücklicher Fund: starker Gleichspannungsmotor mit Inkrementalgeber und FT-kompatibler 4 mm Welle.