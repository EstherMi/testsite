---
layout: "image"
title: "Signalübergabe"
date: "2004-10-16T18:51:55"
picture: "03-Antrieb_und_Kontakt.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Steckkontakte", "SubD9"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/2715
imported:
- "2019"
_4images_image_id: "2715"
_4images_cat_id: "245"
_4images_user_id: "46"
_4images_image_date: "2004-10-16T18:51:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2715 -->
Das 9 Volt Batteriefach wäre bei mir sonst zu nichts nütze. Die Kontakte reichen gerade hin: Antriebsmotor, Inkrementalgeber, Elektromagnet und Lichtschranke sind angeschlossen.