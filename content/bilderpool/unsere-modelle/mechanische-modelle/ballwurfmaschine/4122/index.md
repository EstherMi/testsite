---
layout: "image"
title: "Wurfmaschine Viertelkreis"
date: "2005-05-10T23:27:34"
picture: "Viertelkreis.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Gabllichtschranke", "Zähler"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/4122
imported:
- "2019"
_4images_image_id: "4122"
_4images_cat_id: "245"
_4images_user_id: "46"
_4images_image_date: "2005-05-10T23:27:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4122 -->
Um die Drehzahl exakt zu messen und die Lage des Schwungrads festzulegen, ist auf der äußeren Seite ein wesentlich größerer Viertelkreis angebracht worden, der von einer Gabellichtschranke (unten mittig) abgetastet wird. Die Drehzahlmessung geschieht dadurch, daß während einer Dreivierteldrehung ein rechnerinterner Zähler mit 100 kHz abwärts zählt. Die Lichtschranke gibt den Zähler frei und hält ihn auch an. Während der folgenden Vierteldrehung wird dann ausgelesen und gerechnet.