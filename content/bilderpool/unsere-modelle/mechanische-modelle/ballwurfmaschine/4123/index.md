---
layout: "image"
title: "Wurfmaschine Welle"
date: "2005-05-10T23:27:34"
picture: "Welle.jpg"
weight: "5"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: ["Silikonkupplung"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/4123
imported:
- "2019"
_4images_image_id: "4123"
_4images_cat_id: "245"
_4images_user_id: "46"
_4images_image_date: "2005-05-10T23:27:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4123 -->
Hier nochmals die durchgehende Welle mit (vlnr) Drehgeber, weiche Kupplung, Schleifring, Kettentrieb und Schwungrad. Darunter der Motor.