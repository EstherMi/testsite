---
layout: "image"
title: "Wurfergebnis Kurzstrecke"
date: "2004-11-21T13:37:35"
picture: "Kurzstrecke.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/3302
imported:
- "2019"
_4images_image_id: "3302"
_4images_cat_id: "245"
_4images_user_id: "46"
_4images_image_date: "2004-11-21T13:37:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3302 -->
40, 45 und 50 cm werden ganz ordentlich und reproduzierbar getroffen. Die Punkte entstehen als Durchschläge von Kohlepapier auf Papier, das auf einer Stahlplatte liegt.