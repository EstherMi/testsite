---
layout: "image"
title: "PICT4954"
date: "2010-03-13T17:40:46"
picture: "kompasswagenfrankjakob4.jpg"
weight: "4"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/26678
imported:
- "2019"
_4images_image_id: "26678"
_4images_cat_id: "1901"
_4images_user_id: "729"
_4images_image_date: "2010-03-13T17:40:46"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26678 -->
Ansicht von hinten links