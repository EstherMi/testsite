---
layout: "image"
title: "PICT4953"
date: "2010-03-13T17:40:46"
picture: "kompasswagenfrankjakob3.jpg"
weight: "3"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/26677
imported:
- "2019"
_4images_image_id: "26677"
_4images_cat_id: "1901"
_4images_user_id: "729"
_4images_image_date: "2010-03-13T17:40:46"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26677 -->
Ansicht von hinten rechts