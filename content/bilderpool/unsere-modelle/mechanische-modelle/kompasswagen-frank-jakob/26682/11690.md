---
layout: "comment"
hidden: true
title: "11690"
date: "2010-06-17T22:16:53"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Wir pflegen hier einen respektvollen Umgang miteinander. Lies bitte die Beschreibungen und folge den Links, und frag gerne, wenn Du wo nachhaken möchtest. Aber bitte in einem höflicheren Ton. Danke sehr.

Der Witz bei dieser Art Wagen ist, dass unabhängig von der "Bahn", die der Wagen abfährt, ein Rad immer in dieselbe Himmelsrichtung zeigt - wie ein Kompass. Das wurde schon vor langer Zeit - lange bevor es Kompasse gab - in Navigationswagen ausgenutzt.

Gruß,
Stefan