---
layout: "image"
title: "PICT4955"
date: "2010-03-13T17:40:46"
picture: "kompasswagenfrankjakob5.jpg"
weight: "5"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/26679
imported:
- "2019"
_4images_image_id: "26679"
_4images_cat_id: "1901"
_4images_user_id: "729"
_4images_image_date: "2010-03-13T17:40:46"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26679 -->
Ansicht von oben