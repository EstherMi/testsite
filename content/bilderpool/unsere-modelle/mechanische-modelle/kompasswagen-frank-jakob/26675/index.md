---
layout: "image"
title: "PICT4951"
date: "2010-03-13T17:40:46"
picture: "kompasswagenfrankjakob1.jpg"
weight: "1"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/26675
imported:
- "2019"
_4images_image_id: "26675"
_4images_cat_id: "1901"
_4images_user_id: "729"
_4images_image_date: "2010-03-13T17:40:46"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26675 -->
Ansicht von vorne links