---
layout: "image"
title: "Erde und Mond"
date: "2006-06-18T19:44:25"
picture: "planetarium5.jpg"
weight: "5"
konstrukteure: 
- "Michael Samek"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/6438
imported:
- "2019"
_4images_image_id: "6438"
_4images_cat_id: "565"
_4images_user_id: "104"
_4images_image_date: "2006-06-18T19:44:25"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6438 -->
Die Welle unterhalb des Arms wird durch das große Zahnrad unter der Sonne hindurch angetrieben. Dadurch, dass für die Drehung des Mondes (der hier von der Erde verdeckt wird) um die Erde auch wieder das gleiche große Zahnrad verwendet wird, heben sich die etwas krummen Untersetzungsverhältnisse zwischen der Drehung der Erde um die Sonne sowie der Drehung des Mondes um die Erde auf elegante Weise auf. Der Michael Samek, der sich dieses Getriebe wohl ausgedacht hatte (ich war es nicht!), hatte da eine pfiffige Idee!