---
layout: "image"
title: "Gesamtansicht 1"
date: "2006-06-18T19:44:25"
picture: "planetarium1.jpg"
weight: "1"
konstrukteure: 
- "Michael Samek"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/6434
imported:
- "2019"
_4images_image_id: "6434"
_4images_cat_id: "565"
_4images_user_id: "104"
_4images_image_date: "2006-06-18T19:44:25"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6434 -->
Gleich vorweg: Dieses Modell habe nicht ich mir ausgedacht, sondern jemand mit Namen Michael Samek! Ich hoffe, er hat nichts dagegen, wenn ich sein Modell nachbaue und hier zeige. Ich finde seine Idee einfach sehr schön.

Auf www.3dprofi.de (http://www.3dprofi.de/modelle/planetarium/planetarium.htm) gibt es sein schönes SnapCon-Modell eines Planetariums mit Sonne, Erde und Mond, die sich bis auf die Größen und Entfernungen realistisch umeinander drehen. Dies ist ein Nachbau mit realer fischertechnik, weil mir das Modell so gut gefallen hat.