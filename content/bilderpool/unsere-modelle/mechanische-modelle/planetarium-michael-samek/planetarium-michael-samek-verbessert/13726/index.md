---
layout: "image"
title: "Planetarium verbessert"
date: "2008-02-23T17:21:53"
picture: "Planetarium.jpg"
weight: "6"
konstrukteure: 
- "equester/Michael Samek"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- details/13726
imported:
- "2019"
_4images_image_id: "13726"
_4images_cat_id: "1263"
_4images_user_id: "731"
_4images_image_date: "2008-02-23T17:21:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13726 -->
Dies ist die vorerst verbesserte Variante des Planetariums von Michael Samek. Außer ein paar Verstärkungen habe ich nur das Getriebe neu entworfen, das es dem Mond nun ermöglicht innerhalb 30 Tagen um die Erde zu drehen, statt zuvor 12 Tagen.