---
layout: "image"
title: "Planetarium 2. Verbesserung"
date: "2008-02-25T21:06:32"
picture: "1.jpg"
weight: "1"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- details/13792
imported:
- "2019"
_4images_image_id: "13792"
_4images_cat_id: "1263"
_4images_user_id: "731"
_4images_image_date: "2008-02-25T21:06:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13792 -->
Dies ist die 2. Verbesserung. Der Fehler des überflüssigen Z10 beim Mond-Getriebe ist behoben und eine Umrundung der Erde um die Sonne dauert 365,4 Tage! Im Getriebe habe ich daher das von Triceratops entwickelte Z21-Zahnrad verbaut!