---
layout: "image"
title: "Getriebe Mondumlauf"
date: "2008-02-23T17:21:53"
picture: "Planetarium_Getriebe_Mond.jpg"
weight: "7"
konstrukteure: 
- "equester/Michael Samek"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- details/13727
imported:
- "2019"
_4images_image_id: "13727"
_4images_cat_id: "1263"
_4images_user_id: "731"
_4images_image_date: "2008-02-23T17:21:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13727 -->
Hier das Getriebe in der Nahaufnahme