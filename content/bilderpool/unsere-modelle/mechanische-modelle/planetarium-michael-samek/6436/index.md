---
layout: "image"
title: "Gesamtansicht 3"
date: "2006-06-18T19:44:25"
picture: "planetarium3.jpg"
weight: "3"
konstrukteure: 
- "Michael Samek"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/6436
imported:
- "2019"
_4images_image_id: "6436"
_4images_cat_id: "565"
_4images_user_id: "104"
_4images_image_date: "2006-06-18T19:44:25"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6436 -->
Hier die Rückseite.