---
layout: "image"
title: "Gesamtansicht 2"
date: "2006-06-18T19:44:25"
picture: "planetarium2.jpg"
weight: "2"
konstrukteure: 
- "Michael Samek"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/6435
imported:
- "2019"
_4images_image_id: "6435"
_4images_cat_id: "565"
_4images_user_id: "104"
_4images_image_date: "2006-06-18T19:44:25"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6435 -->
Über Michaels raffiniert gemachtes Getriebe werden die Drehbewegungen auf Sonne, Erde um die Sonne, Erde um sich selbst und Mond um Erde und Sonne realistisch wiedergegeben.