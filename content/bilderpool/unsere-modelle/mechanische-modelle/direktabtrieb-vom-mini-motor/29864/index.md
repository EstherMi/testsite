---
layout: "image"
title: "Direktabtrieb 1"
date: "2011-02-06T14:56:12"
picture: "Direktabtrieb_1.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/29864
imported:
- "2019"
_4images_image_id: "29864"
_4images_cat_id: "2202"
_4images_user_id: "328"
_4images_image_date: "2011-02-06T14:56:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29864 -->
Oft wünsche ich mir, die hohe Drehzahl vom Mini-Motor (oder neuerdings auch XS-Motor) direkt vorn am Ritzel abzugreifen, ohne über ein Getriebe zu gehen. Die einzige offizielle FT-Lösung ist da bislang die Spitze der Luftschraube und ein Gummiriemen, der eine Riemenscheibe antreibt, wie z.B. beim Kompressor.

Nun habe ich festgestellt, dass man das Klemm-Z10 (35112) direkt auf die Schnecke schrauben kann. Zuerst fädelt man nur den schwarzen Teil auf die Schnecke. Dann drückt man den roten Teil mit etwas Kraft über die Schnecke und schraubt am Schluss den schwarzen Teil fest. Man kann natürlich nicht so fest zuschrauben, wie bei einer normalen 4 mm-Achse, aber es hält bombenfest.

Und mittels meiner kleinen Konstruktion landet man dann perfekt im Raster und kann Rastachsen usw. anschließen.

Entschuldigt bitte, wenn es das schon mal gab, aber ich habe es nicht gefunden ...