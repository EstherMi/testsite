---
layout: "image"
title: "Direktabtrieb 2"
date: "2011-02-06T14:57:28"
picture: "Direktabtrieb_2.jpg"
weight: "2"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/29865
imported:
- "2019"
_4images_image_id: "29865"
_4images_cat_id: "2202"
_4images_user_id: "328"
_4images_image_date: "2011-02-06T14:57:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29865 -->
Ein Abstandsring rot (31597) sorgt für den richtigen Abstand vom unteren Z10 zum Lager.