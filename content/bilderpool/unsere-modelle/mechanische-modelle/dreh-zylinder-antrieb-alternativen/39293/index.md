---
layout: "image"
title: "Drehzylinder M4-Antrieb+ 4-5mm-Rohr"
date: "2014-08-24T22:29:34"
picture: "drehzylinderantrieb4.jpg"
weight: "4"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/39293
imported:
- "2019"
_4images_image_id: "39293"
_4images_cat_id: "2940"
_4images_user_id: "22"
_4images_image_date: "2014-08-24T22:29:34"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39293 -->
M4-Antrieb+ 4-5mm-Rohr