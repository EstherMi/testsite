---
layout: "image"
title: "Drehzylinder M4-Antrieb+ 7-8mm-Rohr"
date: "2014-08-24T22:29:34"
picture: "drehzylinderantrieb5.jpg"
weight: "5"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/39294
imported:
- "2019"
_4images_image_id: "39294"
_4images_cat_id: "2940"
_4images_user_id: "22"
_4images_image_date: "2014-08-24T22:29:34"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39294 -->
M4-Antrieb+ 7-8mm-Rohr