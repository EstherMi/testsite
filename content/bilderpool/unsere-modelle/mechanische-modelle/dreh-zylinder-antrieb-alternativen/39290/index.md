---
layout: "image"
title: "Dreh-Zylinder Antrieb Alternativen  -Übersicht"
date: "2014-08-24T22:29:34"
picture: "drehzylinderantrieb1.jpg"
weight: "1"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/39290
imported:
- "2019"
_4images_image_id: "39290"
_4images_cat_id: "2940"
_4images_user_id: "22"
_4images_image_date: "2014-08-24T22:29:34"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39290 -->
Conrad-art.nrs:

297194-62 Messing-Rohr-Profil 4 mm buiten + 3.1 mm binnen,   500mm 


297313 Messing buis buitenØ5,0 binnenØ4,1 500mm
221795 Messing buis buitenØ6,0 binnenØ4,0 500mm

297321-62 Messing-Rohr-Profil  6 mm buiten + 5.1 mm binnen , 500 mm


221796 Messing buis buitenØ8,0 binnenØ6,0 500mm
297330 Messing buis buitenØ8,0 binnenØ7,1 500mm

225410 Stelringset  4mm (10 stuks)
225428 Stelringset  5mm (10 stuks)
225436 Stelringset  6mm (10 stuks)
225550 Stelringset  8 mm (10 stuks)

214515 Axiale groefkogellager 4/10/4mm
214523 Axiale groefkogellager 5/12/4mm
214531 Axiale groefkogellager 6/13/5mm
214558 Axiale groefkogellager 8/19/7mm

295612 KOGELLAGER inw. 6mm x 12 x 4
295639 KOGELLAGER inw. 8mm x 12 x 4
233572 Kugellager mit Keramikkugeln inw. 5 mm x 9 mm x 3 mm

RVS-Draadeinden :
https://www.rvspaleis.nl/


221786 Draadeind 500mm  M4 Messing
221787 Draadeind 500mm  M5 Messing
221789 Draadeind 500 mm M6 Messing

183745 Mentor Askoppelingen 720.64 (Ø) 6 mm naar 4 mm

Adapter 4mm auf 5mm Andreas Tacke   Münster.
Ich habe Andreas gefragt Adapter 3,3mm auf 5mm zu machen. Ich mache dann selbst davon M4 auf 5mm

http://www.conrad.nl/nl/search.html?search=schroefdraadreparatie

1224547 Gewindereparatursatz Eventus 40304  M4
1224612 Gewindeeinsatz Eventus 40504  M4

815528 Gewindereparatursatz 19teilig Exact 40305  M5
1224578 Gewindeeinsatz Eventus 40405  M5

815530  Gewindereparatursatz 19teilig Exact 40306  M6
1224579 Gewindeeinsatz Eventus 40406  M6

Ook elders zoeken onder: "schroefdraadreparatie"
Losse Gewindeeinsätze in NL verkrijgbaar bij:
http://www.mechashop.nl/gereedschap/verspanendgereedschap/tappen/v-coil-m4x4-540502120#variatieGroep=V-Coil%2520M4x6%2520540502140


219827 Pashulzen-set:  Ø: 3 naar 4 mm (2 st.),  Ø: 4 naar 5 mm (2 st.), Ø: 5 naar 6 mm (2 st.), Ø: 6 naar 8 mm (2 st.)

275162 Passchijven 4x8x0,3 DIN 988 (20st)
275165 Passchijven 8x14x0,3 DIN 988 (20st)
275163 Passchijven 5x10x0,3 DIN 988 (20st)