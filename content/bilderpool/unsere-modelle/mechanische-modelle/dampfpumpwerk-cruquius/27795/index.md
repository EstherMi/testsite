---
layout: "image"
title: "Steuerpneumatik innen"
date: "2010-08-06T18:43:08"
picture: "Cruquius_Steuerpneumatik_innen.jpg"
weight: "3"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/27795
imported:
- "2019"
_4images_image_id: "27795"
_4images_cat_id: "2003"
_4images_user_id: "724"
_4images_image_date: "2010-08-06T18:43:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27795 -->
Die Steuerpneumatik teilt sich in 2 Teile auf:

Der untere Teil gibt den Druck auf die Hauptzylinder, die dann ausfahren, bis...

das obere Handventil umgeschaltet wird. Dann stellen die 2 unteren Zylinder das Handventil um, und die Hauptzylinder fahren wieder ein.

Unten angekommen wird vom oberen Ventil druck auf die 2 unteren Ventile der Steuerung gegeben, welche das Handventil wieder umstellen, und der Vorgang beginnt von neuem - völlig automatisch und ohne Computer-Steuerung!!!

Video der Steuerpneumatik:
http://www.youtube.com/watch?v=BD4fe6sZcgY

Video der Original-Steuerung:
http://www.cruquiusmuseum.nl/englishsite/engine.html

Auf den Nachfolgenden Bildern sind die 2 Steuerungen noch mal getrennt abgebildet.