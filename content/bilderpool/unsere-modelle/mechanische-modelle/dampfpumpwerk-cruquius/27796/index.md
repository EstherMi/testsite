---
layout: "image"
title: "Pneumatik-Steuerung unten"
date: "2010-08-06T18:43:09"
picture: "Pneumatik-Steuerung.jpg"
weight: "4"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/27796
imported:
- "2019"
_4images_image_id: "27796"
_4images_cat_id: "2003"
_4images_user_id: "724"
_4images_image_date: "2010-08-06T18:43:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27796 -->
Dieser Teil der Steuerung ist für das ein- und ausfahren der Hauptzylinder verantwortlich (also das Handventil).