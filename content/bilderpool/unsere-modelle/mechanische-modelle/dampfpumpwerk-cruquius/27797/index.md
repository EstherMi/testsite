---
layout: "image"
title: "Steuerpneumatik oben"
date: "2010-08-06T18:43:09"
picture: "Steuerpneumatik.jpg"
weight: "5"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/27797
imported:
- "2019"
_4images_image_id: "27797"
_4images_cat_id: "2003"
_4images_user_id: "724"
_4images_image_date: "2010-08-06T18:43:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27797 -->
Dieser Teil der Steuerung ist für den automatischen Ablauf verantwortlich.

Zur besseren Übersichtlichkeit (das das Original sehr stark verbaut ist), hier noch mal als Modell.

Ich habe das Handventil unten mit einem 2er Bohrer angebohrt und eine Schubstange (Art-Nr. 37276) eingeschoben.

Wenn die Hauptzylinder oben angekommen sind, gibt das Handventil Druck auf die 2 unteren Zylinder (hier nicht zu sehen), die dann das Handventil für den Hauptzylinder umschalten.

Unten angekommen, passiert das gleiche, nur andersrum.