---
layout: "image"
title: "Cruquius-Modell von oben"
date: "2010-08-06T18:43:08"
picture: "Cruquius_von_oben.jpg"
weight: "2"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/27794
imported:
- "2019"
_4images_image_id: "27794"
_4images_cat_id: "2003"
_4images_user_id: "724"
_4images_image_date: "2010-08-06T18:43:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27794 -->
Man sieht einen Teil der Pneumatik-Steuerung.