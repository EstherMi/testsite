---
layout: "image"
title: "Drehkranzantrieb1"
date: "2003-08-27T15:27:45"
picture: "Drehkranz-Getriebe.jpg"
weight: "3"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/1352
imported:
- "2019"
_4images_image_id: "1352"
_4images_cat_id: "182"
_4images_user_id: "46"
_4images_image_date: "2003-08-27T15:27:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1352 -->
Blick auf den Schrittmotor mit Kettenuntersetzung