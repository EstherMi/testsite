---
layout: "image"
title: "Höhenwiege-aufgezogen"
date: "2003-08-27T15:27:45"
picture: "Hhenwiege-hochgezogenB.jpg"
weight: "7"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/1357
imported:
- "2019"
_4images_image_id: "1357"
_4images_cat_id: "182"
_4images_user_id: "46"
_4images_image_date: "2003-08-27T15:27:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1357 -->
Die hochgezogene Höhenwiege erlaubt den Blick auf den Antrieb und die großen Bögen.