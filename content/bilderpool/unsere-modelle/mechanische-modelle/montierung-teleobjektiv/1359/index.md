---
layout: "image"
title: "Flugzeug"
date: "2003-08-27T15:27:45"
picture: "Flugzeug2.jpg"
weight: "9"
konstrukteure: 
- "Airbus (?)"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/1359
imported:
- "2019"
_4images_image_id: "1359"
_4images_cat_id: "182"
_4images_user_id: "46"
_4images_image_date: "2003-08-27T15:27:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1359 -->
Die Montierung verfolgt das Flugzeug, das sonst das Bild in 0,4 sec durchquert hätte.