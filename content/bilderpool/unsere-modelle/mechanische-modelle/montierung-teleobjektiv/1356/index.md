---
layout: "image"
title: "Höhenwiege-Antrieb"
date: "2003-08-27T15:27:45"
picture: "Hhenwiege-ZugschnurB.jpg"
weight: "6"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/1356
imported:
- "2019"
_4images_image_id: "1356"
_4images_cat_id: "182"
_4images_user_id: "46"
_4images_image_date: "2003-08-27T15:27:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1356 -->
Zugschnur aus Kevlar für die Höhenverstellung. Ergibt ein spielfreies Getriebe.