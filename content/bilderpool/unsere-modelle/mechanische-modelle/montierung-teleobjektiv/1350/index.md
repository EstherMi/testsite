---
layout: "image"
title: "Rollenkranz"
date: "2003-08-27T14:50:04"
picture: "Rollenkranz.jpg"
weight: "1"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/1350
imported:
- "2019"
_4images_image_id: "1350"
_4images_cat_id: "182"
_4images_user_id: "46"
_4images_image_date: "2003-08-27T14:50:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1350 -->
Rollenlager für reibungsarmen Lauf. Über und unter den Rollen sind Kunststoffolien, damit die Rollen glatt laufen.