---
layout: "overview"
title: "Montierung fürs Teleobjektiv"
date: 2019-12-17T19:16:03+01:00
legacy_id:
- categories/182
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=182 --> 
Auf wenige Bogensekunden genaue Drehung eines Teleobjektivs um zwei Achsen.