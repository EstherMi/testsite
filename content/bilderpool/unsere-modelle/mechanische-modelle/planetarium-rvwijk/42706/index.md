---
layout: "image"
title: "Sun Orbit connect / deconnect 1"
date: "2016-01-10T17:44:13"
picture: "planetarium07.jpg"
weight: "7"
konstrukteure: 
- "Richard van Wijk"
fotografen:
- "Richard van Wijk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rvwijk"
license: "unknown"
legacy_id:
- details/42706
imported:
- "2019"
_4images_image_id: "42706"
_4images_cat_id: "3179"
_4images_user_id: "2539"
_4images_image_date: "2016-01-10T17:44:13"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42706 -->
Detail of the mechanism to connect and deconnect the rotation around the sun from / to the motor. 
In this detail all is connected (wormwheel up)