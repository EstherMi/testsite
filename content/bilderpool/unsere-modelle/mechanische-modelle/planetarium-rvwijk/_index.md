---
layout: "overview"
title: "Planetarium - rvwijk"
date: 2019-12-17T19:26:04+01:00
legacy_id:
- categories/3179
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3179 --> 
Yet another model of a planetarium with Sun, Earth and Moon. The design was partially copied from Triceratops, however, I gave myself some additional challenges to come to a new design.