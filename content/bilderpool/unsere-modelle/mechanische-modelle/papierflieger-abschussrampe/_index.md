---
layout: "overview"
title: "Papierflieger-Abschußrampe"
date: 2019-12-17T19:22:28+01:00
legacy_id:
- categories/1266
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1266 --> 
Nuja, eigentlich mein Einstand (wenn man den Hund ned mitzählt *G*)[br][br]Vorgabe war, die Mechanik auf relativ wenig Platz zu verwirklichen. Könnte man wohl noch besser machen.[br][br]Die Rampe verfügt über einen Schlitten (1 Kettenglied) der über eine Schnur und Gummizug gespannt wird.[br]Version 2.0 verfügt über eine drehbare und kippbare Rampe, dessen Drehpunkt genau über der Seilrolle liegt.[br][br]Seilspannung und Abschuß ist Programmgesteuert. Die Ausrichtung der Rampe über Joystick.[br][br]Je nach Fliegermodell sind 7 bis 8 Meter drin.[br][br]Versuche mit Alternativen laufen (evtl Kinder-Dartpfeil (die mit dem Klettverschluß *G*)