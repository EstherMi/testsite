---
layout: "image"
title: "Mit eingelegtem Flieger"
date: "2008-02-27T18:12:59"
picture: "papierfliegerabschussrampe2.jpg"
weight: "2"
konstrukteure: 
- "Aki-kun"
fotografen:
- "Aki-kun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Aki-kun"
license: "unknown"
legacy_id:
- details/13798
imported:
- "2019"
_4images_image_id: "13798"
_4images_cat_id: "1266"
_4images_user_id: "508"
_4images_image_date: "2008-02-27T18:12:59"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13798 -->
