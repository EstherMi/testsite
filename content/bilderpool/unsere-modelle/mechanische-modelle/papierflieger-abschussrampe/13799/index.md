---
layout: "image"
title: "Detail Spannmechanik"
date: "2008-02-27T18:12:59"
picture: "papierfliegerabschussrampe3.jpg"
weight: "3"
konstrukteure: 
- "Aki-kun"
fotografen:
- "Aki-kun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Aki-kun"
license: "unknown"
legacy_id:
- details/13799
imported:
- "2019"
_4images_image_id: "13799"
_4images_cat_id: "1266"
_4images_user_id: "508"
_4images_image_date: "2008-02-27T18:12:59"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13799 -->
Minimot mit Hubgetriebe fährt einen Schlitten bis zum Anschlagtaster, der so eingestellt ist, dass das Zahnrad des Powermotors in das Zahnrad der Seilwinde greift.
Powermotor ist auch an einem Impuszähler angeschlossen, um damit die Spannung zu kontrollieren.