---
layout: "image"
title: "Gedrehter Turm auf maximaler Höhe"
date: "2008-02-27T18:12:59"
picture: "papierfliegerabschussrampe4.jpg"
weight: "4"
konstrukteure: 
- "Aki-kun"
fotografen:
- "Aki-kun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Aki-kun"
license: "unknown"
legacy_id:
- details/13800
imported:
- "2019"
_4images_image_id: "13800"
_4images_cat_id: "1266"
_4images_user_id: "508"
_4images_image_date: "2008-02-27T18:12:59"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13800 -->
der Turm wäre mit ein paar kleinen Modifikationen mehrmals um 360 Grad drehbar. Hatte aber noch keine Lust, das umzubaun