---
layout: "image"
title: "Strobel-Brücke_06"
date: "2012-04-08T15:54:35"
picture: "Strob_Brueck_Klappe_Alt.jpg"
weight: "90"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- details/34778
imported:
- "2019"
_4images_image_id: "34778"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-04-08T15:54:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34778 -->
Nochmal zur "Strobel-Brücke":
Die "Statikscharniere" lassen sich wie abgebildet recht einfach ersetzen.