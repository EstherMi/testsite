---
layout: "image"
title: "LKW-Zugmaschine 3"
date: "2011-07-16T09:08:15"
picture: "LKW-Zugmasch_3.jpg"
weight: "194"
konstrukteure: 
- "-?-"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- details/31314
imported:
- "2019"
_4images_image_id: "31314"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2011-07-16T09:08:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31314 -->
