---
layout: "comment"
hidden: true
title: "14681"
date: "2011-07-19T19:16:12"
uploadBy:
- "heiko"
license: "unknown"
imported:
- "2019"
---
Ein bisschen kindisch, oder? Ich hab keine Lust, hier ein Jugendfreiheits-Etikett dranzukleistern. Bitte, lasst uns lieber über Technik diskutieren als über Repräsentation von Menschen durch Polygone.