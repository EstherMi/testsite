---
layout: "image"
title: "Strobel-Brücke_01"
date: "2012-03-25T10:32:05"
picture: "Strob_Brueck_Bew_unt.jpg"
weight: "91"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- details/34678
imported:
- "2019"
_4images_image_id: "34678"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-03-25T10:32:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34678 -->
Die "Strobel-Brücke" aus Hobby 1/3 S.77ff in drei verschiedenen Phasen