---
layout: "image"
title: "Federhammer 2 (aus Hobby 2 Band 1 S. 65)"
date: "2011-08-01T20:18:56"
picture: "Federhammer_LPub_page_4.jpg"
weight: "174"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- details/31487
imported:
- "2019"
_4images_image_id: "31487"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2011-08-01T20:18:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31487 -->
