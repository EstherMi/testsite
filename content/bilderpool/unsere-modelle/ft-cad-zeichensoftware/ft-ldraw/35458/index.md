---
layout: "image"
title: "Super Loop 2"
date: "2012-09-04T19:52:53"
picture: "Superloop_02.jpg"
weight: "68"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- details/35458
imported:
- "2019"
_4images_image_id: "35458"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-09-04T19:52:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35458 -->
Das Fan-Club Modell Nr. 4 mit kleinen Modifikationen bei Antrieb und Kabine.