---
layout: "image"
title: "Spiel mit Lichtreflexen 3"
date: "2012-12-24T11:46:45"
picture: "Effektreflektor_I3.jpg"
weight: "37"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- details/36345
imported:
- "2019"
_4images_image_id: "36345"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-12-24T11:46:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36345 -->
Kleines optisches Spielzeug, passend für die dunkle Jahreszeit.
(aus: Elektronik Zusatzkasten ec3, S. 44)