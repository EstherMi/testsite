---
layout: "image"
title: "Ranger 02"
date: "2012-11-30T21:52:44"
picture: "Ranger_2.jpg"
weight: "47"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- details/36240
imported:
- "2019"
_4images_image_id: "36240"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-11-30T21:52:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36240 -->
Ranger. Fan-Club Modell 1988/2 Beweglicher Teil.
Bestimmt einige Abweichungen vom Original, da manche Einzelheiten auf den Scans schlecht erkennbar.