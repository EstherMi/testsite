---
layout: "image"
title: "MB Traktor 1"
date: "2011-06-08T16:55:51"
picture: "MB_Trac.jpg"
weight: "201"
konstrukteure: 
- "ft"
fotografen:
- "barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- details/30806
imported:
- "2019"
_4images_image_id: "30806"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2011-06-08T16:55:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30806 -->
Bis auf die gebogene Platte an der "Schnauze" wie's Orig