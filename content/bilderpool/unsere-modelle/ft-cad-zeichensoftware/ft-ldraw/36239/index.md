---
layout: "image"
title: "Ranger 01"
date: "2012-11-30T21:52:43"
picture: "Ranger_1.jpg"
weight: "46"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- details/36239
imported:
- "2019"
_4images_image_id: "36239"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-11-30T21:52:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36239 -->
Ranger. Fan-Club Modell 1988/2 Unbeweglicher Teil.
Bestimmt einige Abweichungen vom Original, da manche Einzelheiten auf den Scans schlecht erkennbar.