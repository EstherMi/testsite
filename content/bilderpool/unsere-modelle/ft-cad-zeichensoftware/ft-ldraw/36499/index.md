---
layout: "image"
title: "Traktor 3"
date: "2013-01-22T17:34:38"
picture: "Traktor_Inet_03.jpg"
weight: "21"
konstrukteure: 
- "-?-"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- details/36499
imported:
- "2019"
_4images_image_id: "36499"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2013-01-22T17:34:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36499 -->
Modell und einige Bauschritte, nach einem dieser Tage gesehenen Bild.