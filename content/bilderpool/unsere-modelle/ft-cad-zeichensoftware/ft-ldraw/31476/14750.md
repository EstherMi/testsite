---
layout: "comment"
hidden: true
title: "14750"
date: "2011-07-30T18:12:58"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Manfred,
die Justierung des erforderlichen Abstandes ist so natürlich mit einem zusätzlichen Anschlag an der Oberseite der Stifthalterung möglich.
Über die 3D-Konstruktion von Wendelfedern schweigen wir lieber ...
Ich "rette" mich mit dem ftD bei der Darstellung gedrückter Federn stets mit mehreren in der Achse versetzt, die den Eindruck verkürzter Wendelsteigungen vermitteln sofern die Enden beiderseitig "versteckbar" sind.
Es soll ja mal die 3D-Biegbarkeit von Teilen kommen. Da könnten die Druck- und Zugfedern ruhig dabei sein :o) ...
Gruß Ingo