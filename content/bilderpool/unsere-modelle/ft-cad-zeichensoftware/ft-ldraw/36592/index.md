---
layout: "image"
title: "Geldeinwurf"
date: "2013-02-10T15:48:12"
picture: "Gewinnauswurf_Inet1.jpg"
weight: "10"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- details/36592
imported:
- "2019"
_4images_image_id: "36592"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2013-02-10T15:48:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36592 -->
Der Geldeinwurf zum Nachbau des Spielautomaten "Club-Modell 3/1979".