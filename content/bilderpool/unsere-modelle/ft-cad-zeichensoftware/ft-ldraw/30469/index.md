---
layout: "image"
title: "Test Model"
date: "2011-04-22T11:16:34"
picture: "sextic_final.jpg"
weight: "210"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Richard"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/30469
imported:
- "2019"
_4images_image_id: "30469"
_4images_cat_id: "2243"
_4images_user_id: "585"
_4images_image_date: "2011-04-22T11:16:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30469 -->
Thought to share.