---
layout: "image"
title: "Kartengeber 01"
date: "2012-11-15T23:20:59"
picture: "Kartgeb_01.jpg"
weight: "51"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- details/36081
imported:
- "2019"
_4images_image_id: "36081"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-11-15T23:20:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36081 -->
Fan-Club Modell 1976/3 Erweiterung Kartengeber

Der schon bekannte Kartenmischer, motorisiert, aufgebaut auf großer Bauplatte und gegenüber dem Original um einen Fotowiderstand zur Erkennung der durchgeschobenen Karten erweitert.