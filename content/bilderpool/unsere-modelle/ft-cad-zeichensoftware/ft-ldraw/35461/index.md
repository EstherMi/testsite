---
layout: "image"
title: "Super Loop Montage"
date: "2012-09-08T10:53:00"
picture: "SL-Montage.jpg"
weight: "66"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- details/35461
imported:
- "2019"
_4images_image_id: "35461"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-09-08T10:53:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35461 -->
Details zur Montage des Super Loop.
Man hatte früher bei FT schon feine Ideen. Und die Bauanleitungen dafür gab's umsonst.