---
layout: "image"
title: "Warenautomat 05"
date: "2012-12-27T18:34:49"
picture: "Warenautomat_I5.jpg"
weight: "35"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- details/36353
imported:
- "2019"
_4images_image_id: "36353"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-12-27T18:34:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36353 -->
Der Warenautomat aus "Club Modell 1977/04".

Bauphase Münzeinwurf.