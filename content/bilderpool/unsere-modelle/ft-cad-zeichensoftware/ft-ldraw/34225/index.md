---
layout: "image"
title: "Buggy 01"
date: "2012-02-18T15:08:07"
picture: "Buggy_01.jpg"
weight: "118"
konstrukteure: 
- "-?-"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- details/34225
imported:
- "2019"
_4images_image_id: "34225"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-02-18T15:08:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34225 -->
Wie könnte so ein Buggy aufgebaut sein?
(graue Steine wegen besserer Erkennbarkeit, Aufbau der Kabine erspart.)