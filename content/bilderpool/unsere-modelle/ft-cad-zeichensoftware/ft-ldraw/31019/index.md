---
layout: "image"
title: "LKW-Zugmaschine 1"
date: "2011-07-09T18:53:35"
picture: "LKW-Zugmasch_1.jpg"
weight: "195"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- details/31019
imported:
- "2019"
_4images_image_id: "31019"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2011-07-09T18:53:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31019 -->
Ein paar neue Bauteile