---
layout: "image"
title: "Streifenvorschub-Einrichtung 01"
date: "2012-12-20T17:12:15"
picture: "Streifenvorschub_Inet_01.jpg"
weight: "40"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- details/36330
imported:
- "2019"
_4images_image_id: "36330"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-12-20T17:12:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36330 -->
aus: Licht - Elektronik Bd. 1 S.54f