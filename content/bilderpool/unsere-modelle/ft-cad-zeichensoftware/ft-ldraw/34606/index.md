---
layout: "image"
title: "Buggy komplett"
date: "2012-03-07T12:44:35"
picture: "FT_Buggy_Fernsteu_LPub.jpg"
weight: "96"
konstrukteure: 
- "-?-"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- details/34606
imported:
- "2019"
_4images_image_id: "34606"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-03-07T12:44:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34606 -->
Bild des fertigen Fahrzeuges.

Unter "Downloads/Bauanleitungen/Eigene" habe ich eine mehrteilige komplett Bauanleitung (mit LPub erstellt) hochgeladen.