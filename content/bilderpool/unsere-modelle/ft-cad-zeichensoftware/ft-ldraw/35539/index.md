---
layout: "image"
title: "Selbststeuerndes Fahrzeug 1"
date: "2012-09-26T16:30:42"
picture: "SelbFahrz_1.jpg"
weight: "60"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- details/35539
imported:
- "2019"
_4images_image_id: "35539"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-09-26T16:30:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35539 -->
Was heute "Spursucher" genannt wird, war 1976 (vor 36 Jahren) ein "sich selbststeuerndes Fahrzeug" (aus Clubheft 29/1976).

Wichtigste Modifikation: Als Fahrmotor ist im Original ein Mini-Motor vorgesehen. Der bewegt das Fahrzeug aber nur sehr mühsam, wenn überhaupt.

Weitere Änderungen: Befestigungsmöglichkeiten für Akku und TX, Notausschalter und Statuslampe sowie die Verwendung neuerer/kleinerer LDRs.

Wie sich die Zeiten doch nicht ändern.