---
layout: "image"
title: "Spielautomat 5"
date: "2013-02-10T15:48:12"
picture: "Spielautomat_Inet5.jpg"
weight: "11"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- details/36593
imported:
- "2019"
_4images_image_id: "36593"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2013-02-10T15:48:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36593 -->
Nachbau des Spielautomaten "Club-Modell 3/1979" mit einigen Modifikationen insbesondere beim Antrieb.