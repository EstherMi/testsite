---
layout: "image"
title: "Kartengeber 03"
date: "2012-11-15T23:20:59"
picture: "Kartgeb_03.jpg"
weight: "53"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- details/36083
imported:
- "2019"
_4images_image_id: "36083"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-11-15T23:20:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36083 -->
Fan-Club Modell 1976/3 Erweiterung Kartengeber

Baustufe. Drehscheibe 60 und der noch folgende Drehteller nur aufgesteckt (abnehmbar).