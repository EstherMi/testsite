---
layout: "image"
title: "Kipper 3"
date: "2012-08-04T18:07:54"
picture: "Kipper_03.jpg"
weight: "74"
konstrukteure: 
- "ft, con.barriga"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- details/35240
imported:
- "2019"
_4images_image_id: "35240"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-08-04T18:07:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35240 -->
Der modifizierte Hydraulik-Kipper aus den '80ern .
Lenkung mit IR-Fernsteuerung und Antrieb.