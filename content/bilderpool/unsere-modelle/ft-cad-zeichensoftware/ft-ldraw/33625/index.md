---
layout: "image"
title: "KnickArm_04"
date: "2011-12-11T13:57:08"
picture: "KnickArm_04.jpg"
weight: "133"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- details/33625
imported:
- "2019"
_4images_image_id: "33625"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2011-12-11T13:57:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33625 -->
Änderungen Oberarm