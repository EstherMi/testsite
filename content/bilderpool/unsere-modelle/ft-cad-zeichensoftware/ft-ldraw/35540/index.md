---
layout: "image"
title: "Selbststeuerndes Fahrzeug 2"
date: "2012-09-26T16:30:42"
picture: "SelbFahrz_2.jpg"
weight: "61"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- details/35540
imported:
- "2019"
_4images_image_id: "35540"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-09-26T16:30:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35540 -->
Was heute "Spursucher" genannt wird, war 1976 (vor 36 Jahren) ein "sich selbststeuerndes Fahrzeug" (aus Clubheft 29/1976).