---
layout: "image"
title: "Kartenmischer 2"
date: "2012-10-29T20:05:16"
picture: "Kartenmisch_2.jpg"
weight: "58"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- details/36078
imported:
- "2019"
_4images_image_id: "36078"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-10-29T20:05:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36078 -->
Fan-Club Modell 1976/3

Sind die 4 originalen "gerändelten" Achsen 50 nicht vorhanden, normale Achsen 50
verwenden und oben mit etwas Papier, Gummi o.ä. festklemmen.