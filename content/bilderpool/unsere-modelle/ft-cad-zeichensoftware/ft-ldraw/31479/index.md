---
layout: "image"
title: "Variante des Stifthalters für ft-Scanner 5"
date: "2011-07-30T00:52:45"
picture: "Schreibkopf_LPub_page_5.jpg"
weight: "187"
konstrukteure: 
- "-?-"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- details/31479
imported:
- "2019"
_4images_image_id: "31479"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2011-07-30T00:52:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31479 -->
