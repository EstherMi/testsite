---
layout: "image"
title: "Zentrifuge 1"
date: "2012-09-13T12:15:21"
picture: "Zentrifuge_1.jpg"
weight: "63"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- details/35515
imported:
- "2019"
_4images_image_id: "35515"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-09-13T12:15:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35515 -->
Das Fan-Club Modell 1988/1 mit Änderungen beim Antrieb.