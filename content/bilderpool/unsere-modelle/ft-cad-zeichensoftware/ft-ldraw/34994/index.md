---
layout: "image"
title: "Geheimschloss 1"
date: "2012-05-22T16:10:24"
picture: "Geheimschloss_1.jpg"
weight: "84"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- details/34994
imported:
- "2019"
_4images_image_id: "34994"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-05-22T16:10:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34994 -->
aus "Elektromechanik" S.48ff