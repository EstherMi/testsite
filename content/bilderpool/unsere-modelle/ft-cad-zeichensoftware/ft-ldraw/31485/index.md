---
layout: "image"
title: "Stirnradgetriebe 2 (aus Hobby 2 Band 1 S.13)"
date: "2011-07-30T20:48:16"
picture: "Stirnradgetriebe_02.jpg"
weight: "193"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- details/31485
imported:
- "2019"
_4images_image_id: "31485"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2011-07-30T20:48:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31485 -->
