---
layout: "image"
title: "Warenautomat 03"
date: "2012-12-27T18:34:49"
picture: "Warenautomat_I3.jpg"
weight: "33"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- details/36351
imported:
- "2019"
_4images_image_id: "36351"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-12-27T18:34:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36351 -->
Der Warenautomat aus "Club Modell 1977/04".

Bauphase Münzeinwurf.