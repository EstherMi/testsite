---
layout: "image"
title: "Kartenmischer 3"
date: "2012-10-29T20:05:16"
picture: "Kartenmisch_3.jpg"
weight: "59"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- details/36079
imported:
- "2019"
_4images_image_id: "36079"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2012-10-29T20:05:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36079 -->
Fan-Club Modell 1976/3

Der Mischer ohne Kartenablage.