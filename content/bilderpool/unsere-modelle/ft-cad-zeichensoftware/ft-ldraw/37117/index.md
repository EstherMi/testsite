---
layout: "image"
title: "Fahrsimulator 4"
date: "2013-06-20T09:44:07"
picture: "Fahrsimulator_Inet_4.jpg"
weight: "4"
konstrukteure: 
- "ft"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- details/37117
imported:
- "2019"
_4images_image_id: "37117"
_4images_cat_id: "2243"
_4images_user_id: "1284"
_4images_image_date: "2013-06-20T09:44:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37117 -->
Verschiedene Baustufen des "Fahrsimulators" aus dem Clubheft 1/78.

Veränderungen (durch neuere Bauteile ersetzt) wurden an der Lenkung vorgenommen.
Außerdem wurde die Elektronik ersetzt durch einen TX-Controller. So konnten auch noch zwei Taster für Bremse und Gas hinzugefügt werden.
Die 4 Fototransistoren wurden in RoboPro als analoge 5kOhm Eingänge eingestellt.
Was fehlt ist die Fahrbahn. Sie lässt sich aber anhand des o.g. Clubheftes mit Papier und Filzstift herstellen.