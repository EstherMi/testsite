---
layout: "comment"
hidden: true
title: "22477"
date: "2016-08-28T17:06:36"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Der Cycles Render ist der Render Modus, den man bevorzugt wählen sollte, um fotorealistische Darstellungen zu erhalten. Allerdings sollte man die Sättigung der Farben etwas anpassen, damit das rot natürlicher erscheint.

Gruß
David