---
layout: "image"
title: "Radlader"
date: "2016-08-26T22:05:40"
picture: "fdub3.jpg"
weight: "4"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/44324
imported:
- "2019"
_4images_image_id: "44324"
_4images_cat_id: "3270"
_4images_user_id: "2228"
_4images_image_date: "2016-08-26T22:05:40"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44324 -->
Modell: https://ftcommunity.de/categories.php?cat_id=3006

Im Modus "Cycles Render" gerendert, erweitert um Licht und diffuse Reflexionen am Boden