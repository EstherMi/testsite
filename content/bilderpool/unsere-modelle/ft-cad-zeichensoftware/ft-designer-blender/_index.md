---
layout: "overview"
title: "FT Designer und Blender"
date: 2019-12-17T19:52:16+01:00
legacy_id:
- categories/3270
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3270 --> 
Mit der Version 1.2 des Fischertechnik Designers können die im Designer erstellten Modelle in das Extensible 3D Format exportiert werden. Dies ermöglicht u.a. die Nachbearbeitung (und vor allem das Rendern) der Modelle im OpenSource Programm Blender.