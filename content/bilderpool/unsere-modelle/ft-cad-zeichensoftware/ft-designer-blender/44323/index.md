---
layout: "image"
title: "Bausteine fürs Leben"
date: "2016-08-26T22:05:40"
picture: "fdub2.jpg"
weight: "3"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/44323
imported:
- "2019"
_4images_image_id: "44323"
_4images_cat_id: "3270"
_4images_user_id: "2228"
_4images_image_date: "2016-08-26T22:05:40"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44323 -->
Experimente mit Licht und Schatten