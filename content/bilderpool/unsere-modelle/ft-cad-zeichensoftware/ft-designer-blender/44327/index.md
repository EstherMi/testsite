---
layout: "image"
title: "Radlader"
date: "2016-08-28T19:25:40"
picture: "Radlader1-3.png"
weight: "1"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: ["Blender", "Designer", "Bauanleitung"]
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/44327
imported:
- "2019"
_4images_image_id: "44327"
_4images_cat_id: "3270"
_4images_user_id: "2228"
_4images_image_date: "2016-08-28T19:25:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44327 -->
Im Modus Cycles Render gerendert, zusätzlich wurde die Option "Freestile" gewählt, sodass auch der Cycles Render die Bauteilkanten zeichnet. Betrachtet man die Grafik in hoher Auflösung, ist sie nicht mehr von einer Fischertechnik Bauanleitung unterscheidbar.