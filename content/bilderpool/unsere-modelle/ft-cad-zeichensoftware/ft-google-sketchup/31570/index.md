---
layout: "image"
title: "Winkelsteine [2/5]"
date: "2011-08-12T22:54:27"
picture: "ftundgooglesketchup2.jpg"
weight: "2"
konstrukteure: 
- "fischertechnik, 3D-Nachkonstruktion Udo2"
fotografen:
- "Udo2, 2D-Screenshot aus 3D"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/31570
imported:
- "2019"
_4images_image_id: "31570"
_4images_cat_id: "2351"
_4images_user_id: "723"
_4images_image_date: "2011-08-12T22:54:27"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31570 -->
Die 5 roten Winkelsteine des ft-Systems in 3D zur Prüfung auf ihre Winkelgenauigkeit zu Ringen montiert. Die CAD-Anwendung ermöglicht ein automatisiertes Andocken weiterer hinzuimportierter Teile, wenn die Anlagekonturen deckungsgleich sind.
Kräftiges Rot kommt im SF ft Community Publisher in der Ansicht bis zur Bildbearbeitung noch quellengerecht (bmp-Format) sauber rüber um dann so wie hier in der Veröffentlichung leider fleckig auszusehen trotz allen Wissens bei Versuchen mit seinen verfügbaren Einstellfunktionen. Mit einer Quelldatei im jpg-Format wird das Ergebnis der Veröffentlichung noch schlechter ...