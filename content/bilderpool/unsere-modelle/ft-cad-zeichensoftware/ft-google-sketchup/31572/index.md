---
layout: "image"
title: "Zwei BSB-Teile [4/5]"
date: "2011-08-12T22:54:27"
picture: "ftundgooglesketchup4.jpg"
weight: "4"
konstrukteure: 
- "fischertechnik, 3D-Nachkonstruktion Udo2"
fotografen:
- "Udo2, 2D-Screenshot aus 3D"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/31572
imported:
- "2019"
_4images_image_id: "31572"
_4images_cat_id: "2351"
_4images_user_id: "723"
_4images_image_date: "2011-08-12T22:54:27"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31572 -->
Diese beiden BSB-Teile z.B. fehlen u.a. noch im fischertechnik Designer: 31603 Führerstand rot Diesellok und die Wanne rot der Getriebewannen 36241 E-Lok und 36242 Dampflok 120.