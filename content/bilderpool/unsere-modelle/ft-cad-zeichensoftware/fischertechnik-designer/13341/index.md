---
layout: "image"
title: "Retro2007 ft30571 Scanner-Plotter 1985"
date: "2008-01-17T22:48:18"
picture: "Retro_ft30571_Scanner-Plotter_1985.jpg"
weight: "4"
konstrukteure: 
- "Original ft, Retro Udo2"
fotografen:
- "BS-Kopie der 3D-Ansicht"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/13341
imported:
- "2019"
_4images_image_id: "13341"
_4images_cat_id: "1213"
_4images_user_id: "723"
_4images_image_date: "2008-01-17T22:48:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13341 -->
Modellansicht mit Bauteilkantenmodus des elektromechanischen Modellteiles in der Version NC-Bohrmaschine. Retrostudie mit dem ft-Designer unter der Zielstellung des Austausches von "nml"-Bauteilen durch aktuelle. Das RETRO zeigt eine Alternative zum nml-Bauteil Acrylplatte 300mm x 420mm. Einzelne schwarze Bauteile sind zur besseren Transparenz grau eingefärbt.