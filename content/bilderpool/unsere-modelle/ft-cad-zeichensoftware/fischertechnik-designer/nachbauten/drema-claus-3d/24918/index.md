---
layout: "image"
title: "[4/9] Support 1/3"
date: "2009-09-11T21:40:48"
picture: "dremavonclausind4.jpg"
weight: "4"
konstrukteure: 
- "Original Claus, 3D-Nachbau Udo2"
fotografen:
- "Udo2 / 2D aus 3D-Sichtfenster"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/24918
imported:
- "2019"
_4images_image_id: "24918"
_4images_cat_id: "1718"
_4images_user_id: "723"
_4images_image_date: "2009-09-11T21:40:48"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24918 -->
Baugruppe 1 des Supports: Bettschlitten mit manuellem und automatischem Längsvorschub
Bildeinstellung manuelle Betätigung (Profilkantendarstellung)