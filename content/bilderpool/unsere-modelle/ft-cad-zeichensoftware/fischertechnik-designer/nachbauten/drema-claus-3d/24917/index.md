---
layout: "image"
title: "[3/9] Rädergetriebe 2/2"
date: "2009-09-11T21:40:48"
picture: "dremavonclausind3.jpg"
weight: "3"
konstrukteure: 
- "Original Claus, 3D-Nachbau Udo2"
fotografen:
- "Udo2 / 2D aus 3D-Sichtfenster"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/24917
imported:
- "2019"
_4images_image_id: "24917"
_4images_cat_id: "1718"
_4images_user_id: "723"
_4images_image_date: "2009-09-11T21:40:48"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24917 -->
Baugruppe Vorschubgetriebe mit insgesamt 3 mechanischen Vor- und Rückwärtsgängen (3D-Schattierung ohne Bauteilkanten, Bauteile der Baugruppe in Originalfarbe), Einstellung im Bild 2-Motoren-Antrieb
Das Verschieben des Schieberades Z30 nach links (rechter Hebel) kuppelt den Power-Motor 20:1 (grau) für den Antrieb der Vorschübe ein und den Antrieb über das Spindelgetriebe aus. Das ist hier bei den höheren Spindeldrehzahlen notwendig, weil im Modell von zwei langsamen Motoren 50:1 (rot) auf die hohen Spindeldrehzahlen übersetzt wird. Ein 3-stufiges Schieberad (linker Hebel und Synchronrad) ermöglicht 3 mechanische Vor- und Rückwärtsgänge für die Längs- und Planvorschübe des Supports (Werkzeugschlitten).