---
layout: "comment"
hidden: true
title: "9854"
date: "2009-09-12T19:43:11"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo,
das Kompliment gehört Claus dem Konstrukteur des Modells. Ich habe es ja nur nachgebaut :o)
Der 3D-Nachbau wurde in 11 "Sitzungen" mit einem Teilefortschritt je nach
verfügbarer Zeit zwischen 50 bis 350 Teile erstellt. Der Zeitanteil des Durchdenkens des Modells für eine sinnvolle Struktur in Baugruppen und Bauphasen beträgt etwa 1/10.
Gruß Udo