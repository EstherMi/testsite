---
layout: "comment"
hidden: true
title: "11526"
date: "2010-05-11T21:17:33"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Micha,
schätze mal, dass dein Vorschlag auf kein grosses Echo stösst.
Der Gebrauch des ftDesigners ist stets mit einem erweitertem Zeitaufwand verbunden.
Neukonstruktionen von Modellen z.B. erfordern hierbei zusätzlich eine gute Kenntnis der Einzelteil- und Verbundeigenschaften der Teile im konstruktivem Aufbau und
ein weit vorauschauendes Denken über Bauphasen und Baugruppen hinweg.
Da dürfte die Zahl der Anwender vermutlich noch keine grosse Fangemeinde darstellen ?
Die hier in der ftC vorgestellten Fotos sind auch nur 2D-Bildschirmkopien aus dem 3D-Arbeitsfenster des ftC z.B. als bmp- oder jpg-Datei, also nicht das ftm-Format des ftD.
Die neue Homepage vom Programmautor Michael Samek ist ja noch im Aufbau. Sie ermöglicht aber bereits wie die vorhergehende die Möglichkeit des Runterladens im ftm-Format. Das Hochladen (Einstellen Abbildungen und Link) ist gegenwärtig noch nur über den Administrator möglich.
Gruss, Ingo