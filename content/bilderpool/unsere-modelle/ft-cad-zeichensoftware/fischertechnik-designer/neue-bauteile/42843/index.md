---
layout: "image"
title: "500995 Robo TX Controller"
date: "2016-01-30T17:19:51"
picture: "ftdesigner1.jpg"
weight: "1"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/42843
imported:
- "2019"
_4images_image_id: "42843"
_4images_cat_id: "3185"
_4images_user_id: "2303"
_4images_image_date: "2016-01-30T17:19:51"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42843 -->
neues Bauteil für den ftdesigner

Download unter:
https://ftcommunity.de/data/downloads/ftdesignerdateien/ftdesigner_bauteile_teil_1.zip