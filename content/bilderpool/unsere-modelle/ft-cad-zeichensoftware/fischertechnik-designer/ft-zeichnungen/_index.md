---
layout: "overview"
title: "FT-Zeichnungen"
date: 2019-12-17T19:51:35+01:00
legacy_id:
- categories/2120
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2120 --> 
Ein paar Versuche, Fischertechnik perspektivisch zu zeichnen. Haben wir nähmlich gerade in der Schule, mit Fluchtpunkten und so weiter. Natürlich haben wir da etwas anderes alsfischertechnik gezeichnet. Aber als mir vorhin langweilig war, ist mir die Idee gekommen, es mal mit FT zu versuchen. Grundbausteine sind eigentlich recht einfach zu zeichnen, schwieriger wird es bei  Winkeln oder BS 15 mit Loch. Mal ganz abgesehen von irgendwelchen Getrieben. 