---
layout: "image"
title: "Material-Aufzug"
date: "2008-03-07T07:03:15"
picture: "Materialaufzug_web.jpg"
weight: "8"
konstrukteure: 
- "Laserman, Original von fischertechnik Materialaufzug aus Experimenta Computing Schulprogramm"
fotografen:
- "Fischertechnik-Designer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/13869
imported:
- "2019"
_4images_image_id: "13869"
_4images_cat_id: "1217"
_4images_user_id: "724"
_4images_image_date: "2008-03-07T07:03:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13869 -->
Mit den 3 Tastern (rechts) kann der Aufzug in das jeweilige Stockwerk gerufen werden. Dieser funktioniert mit Hubgetriebe. Ist er im Stockwerk angekommen, wird der Motor über den jeweils zweiten Taster (links) ausgeschaltet.

.
.
.