---
layout: "image"
title: "Retro2008 50-Hz-Uhr (1/4)"
date: "2008-01-24T16:53:33"
picture: "retrohzuhr1.jpg"
weight: "21"
konstrukteure: 
- "Original Stefan Falk, 3D-Retro Udo2"
fotografen:
- "Udo2, 2D-Kopie aus 3D-Ansicht"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/13376
imported:
- "2019"
_4images_image_id: "13376"
_4images_cat_id: "1217"
_4images_user_id: "723"
_4images_image_date: "2008-01-24T16:53:33"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13376 -->
Das Originalmodell 2004 von Stefan Falk in einer Retro-Version mit dem 3D-Konstruktionsprogramm als Kaminuhr in der Perspektive von Klein-Hänschen. Die Aufgabenstellung war möglichst alle nml- und sVr-Bauteile durch aktuelle der Bauteilliste 2008 zu ersetzen. Für die Bauteile Wellenkupplung und Dauermagnet fand sich leider kein Ersatz. Hier hätte sich eine Neuentwicklung des Motors notwendig gemacht. Die Frontverkleidung des Originals wurde weggelassen, weil eine Uhr erst interessant wird, wenn man so in ihr "gläsernes" Uhrwerk schauen und es bei seiner Arbeit beobachten kann.
(Darstellung in 2D aus 3D-Schattierung)