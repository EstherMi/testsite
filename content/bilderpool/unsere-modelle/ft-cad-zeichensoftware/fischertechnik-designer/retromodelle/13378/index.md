---
layout: "image"
title: "Retro2008 50-Hz-Uhr (3/4)"
date: "2008-01-24T16:53:33"
picture: "retrohzuhr3.jpg"
weight: "23"
konstrukteure: 
- "Original Stefan Falk, 3D-Retro Udo2"
fotografen:
- "Udo2, 2D-Kopie aus 3D-Ansicht"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/13378
imported:
- "2019"
_4images_image_id: "13378"
_4images_cat_id: "1217"
_4images_user_id: "723"
_4images_image_date: "2008-01-24T16:53:33"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13378 -->
Ansicht des Uhrwerkes unter Teilausblendung des Zifferblattringes mit dem Untersetzungsgetriebe und einer Übersetzung 45.000:1 für den Minutenzeiger. Der 50-Hz-Motor macht 750 rpm. Der Pfiff des denkbar einfach gebauten Motors ist die Beaufschlagung des Magneten mit Wechselspannung, der damit einen Rotor mit 4 Dauermagneten zum Drehen bringt. Das Bauteil Dauermagnet ist im 3D-Konstruktionsprogramm z.Zt. noch nicht vorrätig. Es wurde aus dem Bauteil Rückschlussplatte simuliert. 
(2D-Ansicht aus 3D-Schattierung mit Kantenstil Profile)