---
layout: "image"
title: "Roller gerendert"
date: "2011-12-05T21:21:27"
picture: "roller1.jpg"
weight: "1"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/33613
imported:
- "2019"
_4images_image_id: "33613"
_4images_cat_id: "1217"
_4images_user_id: "1"
_4images_image_date: "2011-12-05T21:21:27"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33613 -->
Hallo,

diesen Roller habe ich mit dem ftDesigner erstellt.
Mein Cousin der professinel mit Blender arbeitet hat das mal für mich gerendert.
Sowas wird ja der Designer auch bald können.
Ist übrigens nach Anleitung des alten 50er ft Kastens erstellt.