---
layout: "image"
title: "Codekarte"
date: "2008-03-07T07:03:14"
picture: "Codekarte_2.jpg"
weight: "3"
konstrukteure: 
- "Laserman, Original von fischertechnik Profi-Computing"
fotografen:
- "Fischertechnik-Designer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/13864
imported:
- "2019"
_4images_image_id: "13864"
_4images_cat_id: "1217"
_4images_user_id: "724"
_4images_image_date: "2008-03-07T07:03:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13864 -->
Codekarte für Lesegerät