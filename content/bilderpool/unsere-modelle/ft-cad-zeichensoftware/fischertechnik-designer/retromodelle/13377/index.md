---
layout: "image"
title: "Retro2008 50-Hz-Uhr (2/4)"
date: "2008-01-24T16:53:33"
picture: "retrohzuhr2.jpg"
weight: "22"
konstrukteure: 
- "Original Stefan Falk, 3D-Retro Udo2"
fotografen:
- "Udo2, 2D-Kopie aus 3D-Ansicht"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/13377
imported:
- "2019"
_4images_image_id: "13377"
_4images_cat_id: "1217"
_4images_user_id: "723"
_4images_image_date: "2008-01-24T16:53:33"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13377 -->
Rückansicht des Uhrwerkes bei ausgeblendeter Rückwand.
(Darstellung in 2D aus 3D-Schattierung)