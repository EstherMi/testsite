---
layout: "image"
title: "Verstellbares Riemengetriebe"
date: "2008-03-07T07:03:15"
picture: "Riemengetriebe_web.jpg"
weight: "16"
konstrukteure: 
- "Laserman, Original von fischertechnik Hobby 1_1 Maschinenkunde 1"
fotografen:
- "Fischertechnik-Designer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/13877
imported:
- "2019"
_4images_image_id: "13877"
_4images_cat_id: "1217"
_4images_user_id: "724"
_4images_image_date: "2008-03-07T07:03:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13877 -->
Wenn man links an der Kurbel dreht, dreht sich rechts das Ras. Man kann das Rad über den "Griff" nach links und rechts verstellen.

Anwendung:
Zahnarztbohrer.