---
layout: "comment"
hidden: true
title: "5488"
date: "2008-03-07T17:57:50"
uploadBy:
- "laserman"
license: "unknown"
imported:
- "2019"
---
Stimmt! Stefan, Du hast recht! Es langt, wenn die Klemmbuchse innen ist. Dann hat man zwar nicht mehr den gesamten Fahrweg. Aber die Stange kann wenigstens nicht mehr rausrutschen...

Tja... Oft sieht man den Wald vor lauter Bäumen nicht...   ;c)

Ich werde die Konstruktion entsprechend abändern...

Danke und Gruß, Andreas.