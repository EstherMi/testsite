---
layout: "image"
title: "Tresor mit Codeschloß"
date: "2008-03-07T07:03:15"
picture: "Tresor_mit_Codeschlo_web.jpg"
weight: "18"
konstrukteure: 
- "Laserman, Original von fischertechnik Profi Computing"
fotografen:
- "Fischertechnik-Designer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/13879
imported:
- "2019"
_4images_image_id: "13879"
_4images_cat_id: "1217"
_4images_user_id: "724"
_4images_image_date: "2008-03-07T07:03:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13879 -->
Man kann einen (richtigen) Code eingeben, und der Tresor öffnet sich.