---
layout: "comment"
hidden: true
title: "5492"
date: "2008-03-07T21:20:52"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Andreas,
damit wir das auch an Hand von Beispielen "2D aus 3D" erschließen bzw. aus der Arbeit mit 3D etwas vorstellen, habe ich die Kategorie fischertechnik-Designer mit den Unterkategorien Retro und Neuentwicklungen erstellt. Es fehlt noch die angedachte Unterkategorie Nachbauten mit vorlagegetreuen Originalbauteilen.
Wir sollten aber dabei auch bedenken, daß 3D so sehr nicht beliebt ist, auch abgelehnt wird und sich viele am Preis des ft-D stoßend dafür lieber ft-Teile und -Komponenten kaufen. Dieser Kreis wird sich die etwas leichtere oder mehr erhellende 3D-Ansicht als Bauanleitung dann nicht erschließen. Wenn du die von dir vorgestellten Modelle auch aufgebaut hast, solltest du davon auch Fotos in die entsprechenden Kategorien hochladen. Hier geht die Einordnung der Modelle aber sonst unter und ein Parallelunternehmen zum praktischen Modellbau war nicht angedacht.
Gruß Udo2
Gruß Udo2