---
layout: "comment"
hidden: true
title: "5497"
date: "2008-03-08T14:18:25"
uploadBy:
- "sven"
license: "unknown"
imported:
- "2019"
---
Hallo!

@jw:
Also MarMac und ich haben entschieden das solche Modelle hier hochgeladen werden dürfen. Die gehören hier genauso hin wie Fotos.

Natürlich sind auch ftDesigner Modelle hier willkommen.
Dafür haben wir die Kategorie ftDesigner erstellt. Da können diese Modelle rein.

Gruß
Sven
(Admin)