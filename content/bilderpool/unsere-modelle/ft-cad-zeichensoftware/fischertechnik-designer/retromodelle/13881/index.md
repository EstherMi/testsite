---
layout: "image"
title: "Zähler"
date: "2008-03-07T07:03:15"
picture: "Zhler_web.jpg"
weight: "20"
konstrukteure: 
- "Laserman, Original von fischertechnik Hobby 1_1 Maschinenkunde 1"
fotografen:
- "Fischertechnik-Designer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/13881
imported:
- "2019"
_4images_image_id: "13881"
_4images_cat_id: "1217"
_4images_user_id: "724"
_4images_image_date: "2008-03-07T07:03:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13881 -->
Vorne auf die 2 Scheiben kommen im Kreis jeweils die Zahlen 0 bis 9.

Hier gibts ein Programm, um die Zahlen auszudrucken. Auch zum drucken von Skalen für Drehknöpfe:
http://home.arcor.de/sportschuetzen-wiesbaden/fischertechnik/skalendruck.exe

Einstellungen hier:
Durchmesser 2400
Dicke 1200
Teiler Gesamt 10
Teiler Klein 1
Teiler Groß 1
Länge Klein 300
Länge Groß 300
Große beschriften x
Rotieren x

Wenn man die Kurbel dreht, wird erst die rechte Scheibe (1er) jeweils 1 Zahl weitergedreht.
Nach jeweils 10 Umläufen dreht der Mitnehmer die linke Scheibe (10er) 1 weiter.

Das große Ding rechts oben ist ein Gegengewicht.