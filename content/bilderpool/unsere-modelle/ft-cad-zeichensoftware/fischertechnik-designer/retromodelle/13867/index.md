---
layout: "image"
title: "Gleichlauf-Gelenk"
date: "2008-03-07T07:03:14"
picture: "Gleichlauf-Gelenk_web.jpg"
weight: "6"
konstrukteure: 
- "Laserman, Original von fischertechnik Hobby 2_3 Mechanik und KFZ-Technik"
fotografen:
- "Fischertechnik-Designer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/13867
imported:
- "2019"
_4images_image_id: "13867"
_4images_cat_id: "1217"
_4images_user_id: "724"
_4images_image_date: "2008-03-07T07:03:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13867 -->
Auch wenn es nicht so aussieht. Aber dieses Gelenk funktioniert! Und zwar so faszinierend, daß man stundenlang zusehen kann...   ;c)
Man muß nur darauf achten, daß das Rad auf der linken Seite um den Stangendurchmesser (also 4mm) nach oben verschoben ist.