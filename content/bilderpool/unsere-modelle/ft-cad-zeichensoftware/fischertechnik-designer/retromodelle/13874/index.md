---
layout: "image"
title: "Pneumatik-Roboter 4"
date: "2008-03-07T07:03:15"
picture: "Pneumatik_Roboter_4_web.jpg"
weight: "13"
konstrukteure: 
- "Laserman, Original von fischertechnik Experimenta Schulprogramm"
fotografen:
- "Fischertechnik-Designer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/13874
imported:
- "2019"
_4images_image_id: "13874"
_4images_cat_id: "1217"
_4images_user_id: "724"
_4images_image_date: "2008-03-07T07:03:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13874 -->
Hier kann man z.B. die gelbe Tonne (Durchmesser 2,9 cm) auf die Plattform stellen.