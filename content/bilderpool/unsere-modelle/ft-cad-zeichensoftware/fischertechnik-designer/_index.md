---
layout: "overview"
title: "fischertechnik-Designer"
date: 2019-12-17T19:51:17+01:00
legacy_id:
- categories/1213
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1213 --> 
Nachbauten, Retros und Neuentwicklungen von ft-Modellen mit dem Konstruktionsprogramm ft-Designer