---
layout: "image"
title: "[2/5] Bestückter ROBO Explorer mini von links vorn"
date: "2008-11-23T12:49:04"
picture: "roboexplorermini2.jpg"
weight: "2"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2 / 2D aus 3D"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/16473
imported:
- "2019"
_4images_image_id: "16473"
_4images_cat_id: "1472"
_4images_user_id: "723"
_4images_image_date: "2008-11-23T12:49:04"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16473 -->
Auf dieser Seite kann man die Klebestelle [Bauplatte 15x30x3,75 mit Nut] sehen, wenn der Ultraschallsensor in der Version Tunnelroboter auch oder auf der linken Seite des Fahrzeugs angebracht werden soll. Zur Befestigung kann ebenso wie für den Summer ein Stück aus 37034 Doppelklebestreifen 14x60 dienen. Die Anordnung des Tasters für die Wegmessung signalisiert, daß ich mich zunächst zur schnellen Fahrzeugerprobung für eine Übergangslösung unter Nutzung der greifbaren Steuerprogramme für das klemmbare Impulsrad 5 entschieden habe. Da werde ich also mal zwei Ritzel Z10 "opfern" ...