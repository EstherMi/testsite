---
layout: "image"
title: "[3/5] ROBO Interface neu befestigt"
date: "2008-11-23T12:49:04"
picture: "roboexplorermini3.jpg"
weight: "3"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2 / 2D aus 3D"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/16474
imported:
- "2019"
_4images_image_id: "16474"
_4images_cat_id: "1472"
_4images_user_id: "723"
_4images_image_date: "2008-11-23T12:49:04"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16474 -->
So ist die Befestigung des ROBO Interface gefälliger. Sie ersetzt die unter <= (6/7) vorgestellte Lösung.