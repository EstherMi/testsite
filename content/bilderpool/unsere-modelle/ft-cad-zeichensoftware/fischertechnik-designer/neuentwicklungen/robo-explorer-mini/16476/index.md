---
layout: "image"
title: "[5/5] Aktuelles Chassis"
date: "2008-11-23T12:49:04"
picture: "roboexplorermini5.jpg"
weight: "5"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2 / 2D aus 3D"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/16476
imported:
- "2019"
_4images_image_id: "16476"
_4images_cat_id: "1472"
_4images_user_id: "723"
_4images_image_date: "2008-11-23T12:49:04"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16476 -->
Das ist jetzt das aktuelle Chassis (Kfz-Fahrgestell) für den Gesamtaufbau des Modells. Jetzt bin ich auch mit der Stabilität der oberen hinteren Achslagerung zufrieden.
Die Motoren sind bereits montiert ...