---
layout: "image"
title: "[6/7] 3D-XYZ-G"
date: "2008-03-07T10:20:43"
picture: "nachtragzudxyz1.jpg"
weight: "2"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2 / 2D-Kopie aus 3D-Ansicht"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/13882
imported:
- "2019"
_4images_image_id: "13882"
_4images_cat_id: "1272"
_4images_user_id: "723"
_4images_image_date: "2008-03-07T10:20:43"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13882 -->
Hier die Z-Achse am unteren Endpunkt. Der Z-Linearweg beträgt gemäß der Bestimmung dieser universellen Modellbasis immerhin 94mm. Dann muß auch das Y-Portal den Z-Weg in seiner Durchlaßhöhe ermöglichen. Das Modell ist kein Einzweckmodell sondern als Mehrzweckmodell für 3D-Projekte gedacht. 2D geplottet wurde bei der Erprobung zur optischen Kontrolle der Qualität der Linearführungen X und Y.