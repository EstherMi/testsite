---
layout: "comment"
hidden: true
title: "5480"
date: "2008-03-06T20:23:28"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Martin,
noch eine Ergänzung. Der Leertrum der Ketten kann bei Bedarf noch zusätzlich mit einem Spannrad "gezähmt" werden. Der Platz ist dafür fallweise schon vorgesehen. Werde noch eine Abbildung mit Z im unterem Totpunkt bringen ...
Gruß Ingo