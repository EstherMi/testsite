---
layout: "comment"
hidden: true
title: "10224"
date: "2009-11-09T23:48:15"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo
Der Encodermotor als eine der ft-Neuheiten 2009 macht es mir nun möglich, die Arbeiten an meiner Modellbasis nach 1,5-jähriger Unterbrechnung fortsetzen zu können. Also habe ich den statisch/mechanischen Modellaufbau aus dem Regal geholt. Das Modell ist nun zunächst 4-fach motorisiert. Die 3 Linearachsen wurden mechanisch erfolgreich erprobt und in der Stromaufnahme passend zum ROBO TX Controller eingestellt. Hoffe, dass ich mein 3D-XYZ-GES mit automatischer Steuerung noch 2009 vorstellen kann.
Grüsse euch