---
layout: "comment"
hidden: true
title: "7393"
date: "2008-09-30T20:00:38"
uploadBy:
- "remadus"
license: "unknown"
imported:
- "2019"
---
Hallo zusammen,

vielen Dank Udo, für den Nachbau und die Analyse. Eine sehr interessante Veröffentlichung.

Eine sehr wichtige Ergänzung sei aber noch gemacht: der Drehkranz ist ohne den Antrieb, den ich dazugebaut hatte, nichts wert. Ohne die seitlichen Druckrollen, die den gespannten Zahnriemen ableiten, verliert der Drehkranz seine Ortsgenauigkeit. Wer den Drehkranz mal baut, wird gleich die radiale Nachgiebigkeit bemerken. Axial ist der Kranz steif, radial nur zusammen mit den schon erwähnten Stützrollen.

Schöne Grüße
Remadus