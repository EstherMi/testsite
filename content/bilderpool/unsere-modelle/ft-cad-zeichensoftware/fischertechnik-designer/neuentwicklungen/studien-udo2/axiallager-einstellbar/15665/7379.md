---
layout: "comment"
hidden: true
title: "7379"
date: "2008-09-30T18:09:56"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Hallo Udo,

- 23,4 cm Admr. (Außendurchmesser, vermute ich mal) gilt für das real Gebaute oder für das "Designerte"?

- Welches Teil genau meinst Du mit dem "aufgebauten Wälzkörper"? Das mittlere Teil, an dem Du außen an den flach liegenden Rädern gemessen hast?

- Im Designer hast Du dieselben Stellen vermessen?

- Was genau ist nun die Schlussfolgerung? Was beweist Du damit?

Gruß,
Stefan