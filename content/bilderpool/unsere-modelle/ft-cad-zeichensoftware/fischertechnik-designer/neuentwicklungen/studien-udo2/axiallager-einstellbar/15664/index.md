---
layout: "image"
title: "Wälzlager, axial (3/4)"
date: "2008-09-30T09:58:22"
picture: "axiallagereinstellbar3.jpg"
weight: "3"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2 / 2D aus 3D"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/15664
imported:
- "2019"
_4images_image_id: "15664"
_4images_cat_id: "1439"
_4images_user_id: "723"
_4images_image_date: "2008-09-30T09:58:22"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15664 -->
Bei diesem Lösungsansatz ist der erste Fehler bei der Kraftübertragung beseitigt.