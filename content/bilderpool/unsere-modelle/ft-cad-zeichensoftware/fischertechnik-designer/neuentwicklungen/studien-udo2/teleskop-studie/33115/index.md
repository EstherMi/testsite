---
layout: "image"
title: "[2/4] Blockstellung, Ansicht Führungselemente"
date: "2011-10-11T21:38:50"
picture: "teleskopstudie2.jpg"
weight: "2"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2, 2D-Screenshot aus 3D-Fenster"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/33115
imported:
- "2019"
_4images_image_id: "33115"
_4images_cat_id: "2447"
_4images_user_id: "723"
_4images_image_date: "2011-10-11T21:38:50"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33115 -->
Als Führungselemente zwischen den Alu-Profilen dienen 107355 Verbinder sz. Die beiden linken Verbinder sind axial jeweils im oberen Alu-Profil an ihrer Unterseite fixiert und die beiden rechten jeweils am unteren auf der Oberseite. Man kann dazu die Kulissensteine Dmr. 4x11 von Andreas Tacke (TST) nehmen. Wer ft-Reinheitspurist ist kann auch dafür 107356 Seilklemmstift rt nehmen sofern an den Alu-Profilen dazu brauchbare Toleranzen vorliegen.