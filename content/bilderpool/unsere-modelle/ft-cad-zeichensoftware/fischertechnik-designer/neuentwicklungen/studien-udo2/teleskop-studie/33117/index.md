---
layout: "image"
title: "[4/4] Ausfahrstellung 2x30mm, Ansicht Führungselemente"
date: "2011-10-11T21:38:50"
picture: "teleskopstudie4.jpg"
weight: "4"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2, 2D-Screenshot aus 3D-Fenster"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/33117
imported:
- "2019"
_4images_image_id: "33117"
_4images_cat_id: "2447"
_4images_user_id: "723"
_4images_image_date: "2011-10-11T21:38:50"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33117 -->
Ansicht der Lage der Führungs- und Fixierteile im Ausfahrbeispiel 2x30 mm.

Von der mechanischen Funktion habe ich mich natürlich vorher im praktischen Aufbau mit 2 Alu-Profilen 120 mm überzeugt :o)