---
layout: "image"
title: "[1/4] Blockstellung, Gesamtansicht"
date: "2011-10-11T21:38:50"
picture: "teleskopstudie1.jpg"
weight: "1"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2, 2D-Screenshot aus 3D-Fenster"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/33114
imported:
- "2019"
_4images_image_id: "33114"
_4images_cat_id: "2447"
_4images_user_id: "723"
_4images_image_date: "2011-10-11T21:38:50"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33114 -->
Aufbaubeispiel mit 3 Alu-Profilen 210 mm hier in einer der möglichen Blockstellungen mit Stirnflucht.