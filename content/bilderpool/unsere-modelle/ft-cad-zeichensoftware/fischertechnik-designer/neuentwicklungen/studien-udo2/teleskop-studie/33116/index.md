---
layout: "image"
title: "[3/4] Ausfahrstellung 2x30mm, Gesamtansicht"
date: "2011-10-11T21:38:50"
picture: "teleskopstudie3.jpg"
weight: "3"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2, 2D-Screenshot aus 3D-Fenster"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/33116
imported:
- "2019"
_4images_image_id: "33116"
_4images_cat_id: "2447"
_4images_user_id: "723"
_4images_image_date: "2011-10-11T21:38:50"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33116 -->
Ausfahrbeispiel 2x30 mm = 60 mm. In einer ausgefahrenen Endlage überdecken sich die Alu-Profile noch jeweils 60mm. Mit 3 Alu-Profilen 210 mm wäre das z.B. dann eine Gesamtausfahrlänge von 2x(210-60) = 300 mm.

Das Ausfahren geht natürlich auch in beide Richtungen. Nach rechts wie gezeigt vom unteren Alu-Profil und nach links in umgekehrter Weise vom oberen. Dreht man den Teleskopblock um seine senkrechte Achse um 180°, dann wie gezeigt wieder vom unteren Alu-Profil aber nach links usw.