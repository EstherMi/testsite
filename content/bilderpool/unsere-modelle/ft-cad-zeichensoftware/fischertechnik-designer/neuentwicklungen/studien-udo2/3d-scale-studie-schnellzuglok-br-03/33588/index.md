---
layout: "image"
title: "[7/7] BR 003 131-1 Innenansicht Führerhaus"
date: "2011-11-29T11:56:36"
picture: "dscalestudieschnellzuglokbrwitte7.jpg"
weight: "7"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2, 2D-Screenshot aus 3D-Arbeitsfenster"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/33588
imported:
- "2019"
_4images_image_id: "33588"
_4images_cat_id: "2487"
_4images_user_id: "723"
_4images_image_date: "2011-11-29T11:56:36"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33588 -->
Die Feuerbüchse ist innen auch mit Ascherost angedacht :o)

Da ich mir die erforderlichen schwarzen Bauplatten auch mal  für andere Dampfrösser schon gekauft habe - die Fa. Knobloch mußte nachliefern :o) - werde ich das Projekt auch weiter vorantreiben. Es ist eben so, wenn man mehr Ideen als Zeit und auch Geld zum Umsetzen hat. Nachdem so denke ich mal optisch eine maßstäbliche Machbarkeit erwiesen sein dürfte, werden die nächsten Schwerpunkte die Umsetzung verschiedener Funktionen einschließlich der Schienenfahrtüchtigkeit sein ...