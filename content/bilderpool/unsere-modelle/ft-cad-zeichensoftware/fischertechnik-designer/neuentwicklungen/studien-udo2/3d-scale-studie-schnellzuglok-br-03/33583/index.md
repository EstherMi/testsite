---
layout: "image"
title: "[2/7] BR 003 131-1 Rückansicht"
date: "2011-11-29T11:56:35"
picture: "dscalestudieschnellzuglokbrwitte2.jpg"
weight: "2"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2, 2D-Screenshot aus 3D-Arbeitsfenster"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/33583
imported:
- "2019"
_4images_image_id: "33583"
_4images_cat_id: "2487"
_4images_user_id: "723"
_4images_image_date: "2011-11-29T11:56:35"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33583 -->
Mit Details wie hier bei Radlagerung/-federung am Schlepptender bin ich noch nicht am Ziel meiner Vorstellungen.