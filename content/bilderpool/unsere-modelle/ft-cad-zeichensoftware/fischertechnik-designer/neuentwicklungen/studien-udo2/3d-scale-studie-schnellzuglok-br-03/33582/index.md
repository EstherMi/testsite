---
layout: "image"
title: "[1/7] BR 003 131-1 Vorderansicht"
date: "2011-11-29T11:56:35"
picture: "dscalestudieschnellzuglokbrwitte1.jpg"
weight: "1"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2, 2D-Screenshot aus 3D-Arbeitsfenster"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/33582
imported:
- "2019"
_4images_image_id: "33582"
_4images_cat_id: "2487"
_4images_user_id: "723"
_4images_image_date: "2011-11-29T11:56:35"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33582 -->
2D-Screenshots mit Bauteilprofilkanten - das tiefe Schwarz in Fotoschwarz aufgehellt - aus meiner 3D-Studie vom November 2007. Ich wollte wissen und dabei auch Erfahrungen sammeln ob man mit ft eine zehnachsige Dampflok - hier die Schnellzuglokomotive BR 003 131-1 mit der Achsfolge Lok 2'C1'h2 / Schlepptender 2'2'T30 und Witte-Windleitblechen - als maßstäbliches Modell bauen kann. Schnell stellte sich heraus, daß die Drehscheibe 60 hier den Maßstab 1:32 bestimmt. Damit werden viele wichtige Modelldetails kleiner als das ft-Raster 15 (7,5/5) mm. Es steht dann auch die Frage im Raum, ob sich unsere ft-Reinheitspuristen mit solchen ft-Bauteilmodifizierungen anfreunden können. Meine Erfahrung sagt ohne Modifizierungen und systemfremde Ergänzungen sind hier mit ft hinsichtlich Geometrietreue, Maßhaltigkeit und Funktionen keine realistischen Scale-Modelle in wirtschaftlich sinnvoll vertretbarer Baugröße möglich.
Die eingefärbten Laufschienen 3x240 mm veranschaulichen die Länge des modellierten Scale-Objekts. Ich habe vom Modell auch eine Vermessungsliste erstellt.