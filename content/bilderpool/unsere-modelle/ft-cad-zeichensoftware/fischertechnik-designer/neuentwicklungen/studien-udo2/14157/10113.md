---
layout: "comment"
hidden: true
title: "10113"
date: "2009-10-22T08:01:08"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Nachtrag:

Du sprachst davon, dass Dein Entwurf einer der wenigen korrekten Blattfederungen in der ftc sind.

Dass diese hier kinematisch und funktional nicht korrekt ist, ist klar:

http://www.ftcommunity.de/details.php?image_id=14049

Aber diese Darstellung hier sollte technisch in Ordnung sein, oder? Da passt meiner Meinung nach alles:

http://www.ftcommunity.de/details.php?image_id=3780

Oder bist Du anderer Meinung?

Gruß, Thomas