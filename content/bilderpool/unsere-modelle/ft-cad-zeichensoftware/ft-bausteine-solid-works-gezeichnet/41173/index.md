---
layout: "image"
title: "Grundbaustein 15 Gelb"
date: "2015-06-08T21:30:57"
picture: "teilekalti22.jpg"
weight: "22"
konstrukteure: 
- "fischertechnik/Kalti"
fotografen:
- "Kalti"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kalti"
license: "unknown"
legacy_id:
- details/41173
imported:
- "2019"
_4images_image_id: "41173"
_4images_cat_id: "2906"
_4images_user_id: "1342"
_4images_image_date: "2015-06-08T21:30:57"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41173 -->
