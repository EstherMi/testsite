---
layout: "image"
title: "Stecker Grün"
date: "2015-06-08T21:30:57"
picture: "teilekalti04.jpg"
weight: "4"
konstrukteure: 
- "fischertechnik/Kalti"
fotografen:
- "Kalti"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kalti"
license: "unknown"
legacy_id:
- details/41155
imported:
- "2019"
_4images_image_id: "41155"
_4images_cat_id: "2906"
_4images_user_id: "1342"
_4images_image_date: "2015-06-08T21:30:57"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41155 -->
