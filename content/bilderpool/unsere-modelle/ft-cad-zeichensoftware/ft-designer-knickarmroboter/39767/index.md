---
layout: "image"
title: "Knickarmroboter 6"
date: "2014-11-04T17:59:37"
picture: "ftdesignerknickarmroboter6.jpg"
weight: "6"
konstrukteure: 
- "M. Bäter"
fotografen:
- "Ft - Designer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischmike"
license: "unknown"
legacy_id:
- details/39767
imported:
- "2019"
_4images_image_id: "39767"
_4images_cat_id: "2984"
_4images_user_id: "2291"
_4images_image_date: "2014-11-04T17:59:37"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39767 -->
Von unten
 erstellt mit Ft-Designer