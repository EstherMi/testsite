---
layout: "comment"
hidden: true
title: "19697"
date: "2014-11-05T18:49:13"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Mir fällt gerade noch etwas ein...

Screenshots aus dem ftDesigner haben leider nur eine eingeschränkte Qualtiät. Das liegt daran, dass das Programm den aktuellen Bildschirminhalt mit der aktuellen Auflösung des Bildschirm ausgibt.
Allerdings bietet der Designer auch den Export ins pov-ray Format an. Nach der Installation und Einrichtung des Pov-Raytracers ist es möglich, diese Dateien weiter zu bearbeiten und in einer höheren Qualtiät zu rendern (Diese ist nahezu unbegrenzt, die Geschwindikeit des Renderns hängt selbstverständlich mit der Prozessorleistung zusammen). Leider ist die Einrichtung nicht ganz unkompliziert. Dieser "Verfahren" eignet sich aber dennoch für all diejenigen, die etwas Spaß am rumprobieren mit dem Programm haben: http://www.povray.org/

Zusätzlich bietet der Designer auch den Export in weitere CAD Formate an. Mit diesen sind mir allerdings noch keine anständigen Ergebnisse gelungen.

Gruß, David