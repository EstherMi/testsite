---
layout: "image"
title: "IMG_0006_Detail Antrieb.JPG"
date: "2012-04-09T22:33:49"
picture: "IMG_0006_Detail_Antrieb.jpg"
weight: "6"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/34784
imported:
- "2019"
_4images_image_id: "34784"
_4images_cat_id: "2570"
_4images_user_id: "724"
_4images_image_date: "2012-04-09T22:33:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34784 -->
Hier gibt es ein Video mit allen 4 Takten:

http://www.youtube.com/watch?v=fJXfWJa6FfU

1. Ansaugen
2. Verdichten
3. Zünden / Arbeiten
4. Ausstoßen