---
layout: "image"
title: "IMG_0005_Detail Kurbelwelle und Pleuel.JPG"
date: "2012-04-09T22:33:48"
picture: "IMG_0005_Detail_Kurbelwelle_und_Pleuel.jpg"
weight: "1"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/34779
imported:
- "2019"
_4images_image_id: "34779"
_4images_cat_id: "2570"
_4images_user_id: "724"
_4images_image_date: "2012-04-09T22:33:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34779 -->
