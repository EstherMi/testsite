---
layout: "image"
title: "IMG_0003_Detail Ventile.JPG"
date: "2012-04-09T22:33:49"
picture: "IMG_0003_Detail_Ventile.jpg"
weight: "3"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/34781
imported:
- "2019"
_4images_image_id: "34781"
_4images_cat_id: "2570"
_4images_user_id: "724"
_4images_image_date: "2012-04-09T22:33:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34781 -->
