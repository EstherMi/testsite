---
layout: "comment"
hidden: true
title: "22236"
date: "2016-07-14T18:22:56"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Schönes Modell,

aber der Fotohintergrund ist ja grausamer als die Bastmatte von Peter ;-)
Das arme JPG muss soo viel Hintergrunddetails verarbeiten - einfarbiger Hintergrund wäre da besser.

Gruß,
Harald