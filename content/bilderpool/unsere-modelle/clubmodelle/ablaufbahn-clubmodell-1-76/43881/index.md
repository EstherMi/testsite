---
layout: "image"
title: "Aboaufbahn"
date: "2016-07-14T18:10:27"
picture: "image_3.jpeg"
weight: "2"
konstrukteure: 
- "Fred Spies"
fotografen:
- "Fred Spies"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredspies"
license: "unknown"
legacy_id:
- details/43881
imported:
- "2019"
_4images_image_id: "43881"
_4images_cat_id: "3250"
_4images_user_id: "2620"
_4images_image_date: "2016-07-14T18:10:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43881 -->
