---
layout: "image"
title: "Anhänger"
date: "2009-04-13T11:49:11"
picture: "trikemitanhaenger4.jpg"
weight: "4"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/23691
imported:
- "2019"
_4images_image_id: "23691"
_4images_cat_id: "1635"
_4images_user_id: "845"
_4images_image_date: "2009-04-13T11:49:11"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23691 -->
