---
layout: "comment"
hidden: true
title: "9102"
date: "2009-04-30T09:27:30"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Ooops, und ich war schon drauf und dran, die Kategorie wieder auseinander zu pflücken, damit "Sorte zu Sorte" gesteckt wird =8-o

Schön wärs, wenn die Forensoftware Bilder als Links erlauben würde, d.h. dass ein einziges Bild gespeichert ist, aber in mehreren Kategorien angezeigt werden kann. Seufz.


Gruß,
Harald