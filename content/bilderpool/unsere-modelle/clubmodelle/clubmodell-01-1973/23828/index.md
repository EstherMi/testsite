---
layout: "image"
title: "Elektronik Bausteine"
date: "2009-04-29T17:24:20"
picture: "clubmodell6.jpg"
weight: "6"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- details/23828
imported:
- "2019"
_4images_image_id: "23828"
_4images_cat_id: "1628"
_4images_user_id: "936"
_4images_image_date: "2009-04-29T17:24:20"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23828 -->
Elektronik Bausteine mit Elko 2200 uF/16V.