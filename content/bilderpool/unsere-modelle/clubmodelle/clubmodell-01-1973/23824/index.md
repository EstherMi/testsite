---
layout: "image"
title: "Elektronisch gesteuerte Uhr"
date: "2009-04-29T17:24:19"
picture: "clubmodell2.jpg"
weight: "2"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- details/23824
imported:
- "2019"
_4images_image_id: "23824"
_4images_cat_id: "1628"
_4images_user_id: "936"
_4images_image_date: "2009-04-29T17:24:19"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23824 -->
Ansicht schräg von hinten.