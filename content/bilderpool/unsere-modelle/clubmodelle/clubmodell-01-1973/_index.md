---
layout: "overview"
title: "Clubmodell 01/1973"
date: 2019-12-17T19:50:59+01:00
legacy_id:
- categories/1628
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1628 --> 
Im Clubmagazin 01/1973 wird eine elektronisch gesteuerte Uhr beschrieben. Bei der Schaltung habe ich allerdings den 22 KOhm Widerstand durch eine Drahtbrücke ersetzt und einen 2200 uF/16V Elko genommen. Seit der Justage des Taktebers läuft die Uhr nun seit 2 Stunden ohne Abweichung. Wollen wir mal sehen wie es mit der Genauigkeit weitergeht.