---
layout: "image"
title: "Elektronisch gesteuerte Uhr"
date: "2009-04-29T17:24:19"
picture: "clubmodell1.jpg"
weight: "1"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- details/23823
imported:
- "2019"
_4images_image_id: "23823"
_4images_cat_id: "1628"
_4images_user_id: "936"
_4images_image_date: "2009-04-29T17:24:19"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23823 -->
Gesamtansicht von vorne.