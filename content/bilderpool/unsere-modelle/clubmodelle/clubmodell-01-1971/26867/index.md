---
layout: "image"
title: "Clubmodell"
date: "2010-04-02T23:27:45"
picture: "clubmodell7.jpg"
weight: "7"
konstrukteure: 
- "Fischertechnik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/26867
imported:
- "2019"
_4images_image_id: "26867"
_4images_cat_id: "1923"
_4images_user_id: "453"
_4images_image_date: "2010-04-02T23:27:45"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26867 -->
