---
layout: "image"
title: "Ballroboter (b)"
date: "2008-02-09T12:07:19"
picture: "Ballroboter_c.jpg"
weight: "2"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- details/13606
imported:
- "2019"
_4images_image_id: "13606"
_4images_cat_id: "1250"
_4images_user_id: "731"
_4images_image_date: "2008-02-09T12:07:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13606 -->
Hier sieht man den Greifmechanismus und den Tastsporn, mit dem der Roboter erkennt, wenn er bei den Stationen ankommt.