---
layout: "image"
title: "Ballroboter"
date: "2008-02-09T12:07:19"
picture: "Ballroboter_April_07_a.jpg"
weight: "1"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- details/13605
imported:
- "2019"
_4images_image_id: "13605"
_4images_cat_id: "1250"
_4images_user_id: "731"
_4images_image_date: "2008-02-09T12:07:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13605 -->
Dies ist das Clubmodell Nr. 22 - "Mobile Pneumatic Robot" aus der News 01/03.
Ich habe den Ballroboter über RoboPro so programmiert, dass er bei einer Station immer einen Tischtennisball aufnimmt, einer schwarzen Linie zur Station 2 folgt, dort den Tischtennisball in einen "Korb" fallen lässt, umkehrt und wieder einen Ball holt,...