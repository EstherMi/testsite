---
layout: "overview"
title: "Tempotaschentücherautomat (Club-Modell aus 2004)"
date: 2019-12-17T19:50:56+01:00
legacy_id:
- categories/1188
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1188 --> 
Zunächst zeigt die grüne LED/Lampe an, dass sich der Automat im Status \"bereit\" befindet. Wirft man nun eine Münze ein, wechselt der Status nach \"in Betrieb\" (rote LED/Lampe), der Schieber geht nach vorne und schmeißt eine Tempopackung aus. Anschließend fährt der Schieber wieder zurück und der Status springt wieder auf \"bereit\". Mein Sohn (5) findet den Automaten total toll. Er hat sogar rausgefunden, dass man die Maschine mit einem Paierstreifen betuppen kann, sprich Tempopackung ohne Münze.