---
layout: "image"
title: "Ansicht von vorne"
date: "2007-12-18T17:32:59"
picture: "tempotaschentuecherautomatclubmodellaus1.jpg"
weight: "1"
konstrukteure: 
- "Nachgebaut franky (Frank Hanke)"
fotografen:
- "franky"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "franky"
license: "unknown"
legacy_id:
- details/13110
imported:
- "2019"
_4images_image_id: "13110"
_4images_cat_id: "1188"
_4images_user_id: "666"
_4images_image_date: "2007-12-18T17:32:59"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13110 -->
Hier der Automat in der Frontansicht