---
layout: "image"
title: "Antrieb vorne"
date: "2007-12-18T17:32:59"
picture: "tempotaschentuecherautomatclubmodellaus8.jpg"
weight: "8"
konstrukteure: 
- "Nachgebaut franky (Frank Hanke)"
fotografen:
- "franky"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "franky"
license: "unknown"
legacy_id:
- details/13117
imported:
- "2019"
_4images_image_id: "13117"
_4images_cat_id: "1188"
_4images_user_id: "666"
_4images_image_date: "2007-12-18T17:32:59"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13117 -->
Hier sieht man den Antrieb, mit dem Schieber nach vorne