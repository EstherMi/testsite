---
layout: "image"
title: "Schaltbild"
date: "2017-10-18T10:21:21"
picture: "schraegaufzugausprospekt09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46826
imported:
- "2019"
_4images_image_id: "46826"
_4images_cat_id: "3466"
_4images_user_id: "104"
_4images_image_date: "2017-10-18T10:21:21"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46826 -->
Das ist mein Vorschlag zur Schaltung. Der Anschluss der Taster hier hat zur Folge, dass jeglicher Druck auf einen oder beide Taster während der Fahrt wirkungslos bleibt - die Fahrt wird zum Ende fortgesetzt. Am Ende bewirkt auch nur der jeweils andere Taster eine Fahrt zum entgegengesetzten Ende.

Außerdem kann man z.B. mit dem em-Stufenschalter mit 8 Schaltstellungen realisieren, dass je nach Schaltstellung keiner, einer oder beide Taster überbrückt werden und so ein halber oder ganzer Automatikbetrieb ("Convention-Modus" ;-) aktiviert werden kann.

Ich bin für Verbesserungsvorschläge und Diskussionen zur Schaltung dankbar.