---
layout: "image"
title: "Kettenantrieb (1)"
date: "2017-10-18T10:21:21"
picture: "schraegaufzugausprospekt04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46821
imported:
- "2019"
_4images_image_id: "46821"
_4images_cat_id: "3466"
_4images_user_id: "104"
_4images_image_date: "2017-10-18T10:21:21"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46821 -->
Das Vorbild verwendete einen klassischen grauen ft-Motor. Hier ist ein M-Motor verbaut, aber der sieht praktisch genauso aus. Man sieht den Antrieb, untersetzt über das mot-2 Stufengetriebe, und die aufgewickelte Kette. Zwischen dem Aufwickel-Z20 und dem Wagen befindet sich die obere Lichtschranke, die einfach durch die Räder (Spurkränze) des Wagens unterbrochen wird.