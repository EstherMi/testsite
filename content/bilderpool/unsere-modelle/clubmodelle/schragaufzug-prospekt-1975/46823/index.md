---
layout: "image"
title: "Befestigung der Kette am Wagen"
date: "2017-10-18T10:21:21"
picture: "schraegaufzugausprospekt06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46823
imported:
- "2019"
_4images_image_id: "46823"
_4images_cat_id: "3466"
_4images_user_id: "104"
_4images_image_date: "2017-10-18T10:21:21"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46823 -->
Die Kette ist mit ihrem anderen Ende einfach in einen quer im Wagen verbauten BS15 eingehängt. Damit sie seitlich nicht herausrutscht, ergänzte ich die beiden Bausteine 5x15x30 3N.