---
layout: "image"
title: "Clubmodell"
date: "2010-04-04T17:28:02"
picture: "clubmodell2.jpg"
weight: "2"
konstrukteure: 
- "Fischertechnik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/26872
imported:
- "2019"
_4images_image_id: "26872"
_4images_cat_id: "1924"
_4images_user_id: "453"
_4images_image_date: "2010-04-04T17:28:02"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26872 -->
