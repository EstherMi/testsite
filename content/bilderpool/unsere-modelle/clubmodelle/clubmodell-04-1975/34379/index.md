---
layout: "image"
title: "Übersicht"
date: "2012-02-23T21:35:27"
picture: "kopiereri01.jpg"
weight: "1"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/34379
imported:
- "2019"
_4images_image_id: "34379"
_4images_cat_id: "2002"
_4images_user_id: "453"
_4images_image_date: "2012-02-23T21:35:27"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34379 -->
Impressionen vom Kopierer