---
layout: "image"
title: "Clubmodell"
date: "2010-07-27T22:47:54"
picture: "clubmodell2.jpg"
weight: "14"
konstrukteure: 
- "Fischertechnik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/27779
imported:
- "2019"
_4images_image_id: "27779"
_4images_cat_id: "2002"
_4images_user_id: "453"
_4images_image_date: "2010-07-27T22:47:54"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27779 -->
