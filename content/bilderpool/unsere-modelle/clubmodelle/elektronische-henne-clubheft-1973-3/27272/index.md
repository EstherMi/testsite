---
layout: "image"
title: "Der Nachbau"
date: "2010-05-16T21:24:00"
picture: "elektronischehenne03.jpg"
weight: "3"
konstrukteure: 
- "qincym und steffalk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/27272
imported:
- "2019"
_4images_image_id: "27272"
_4images_cat_id: "1958"
_4images_user_id: "104"
_4images_image_date: "2010-05-16T21:24:00"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27272 -->
Im Forum hatten wir in diesem Thread http://www.fischertechnik.de/de/fanclub/forum/default.aspx?g=posts&t=5001 besprochen, dieses originelle Modell nachzubauen. Wir sind nämlich einigermaßen sicher, dass 1973 kaum jemand so viel fischergeometric und so viele Elektronikbausteine besaß. qincym hatte fischergeometric, und zwar alle 4 Kästen, die es mittlerweile gab, doppelt - und selbst das hat nicht gereicht. Deshalb hat er extra noch fischergeometric nachgekauft. Außerdem stellte er den rechts sichtbaren Anschluss für Aktivboxen her. steffalk steuerte die Elektronik bei.

Ziel war ein möglichst genauer Nachbau. Unterschiede sind:

- Leider hatten wir keines der kleineren Netzteile ohne Drehwiderstand, also kam ein mot. 4 Netzgerät zum Einsatz.

- Die Elektronik laut Schaltplan verwendet den h4-Mikrofon-Lautsprecher-Baustein, während auf dem Original-Foto eine Eigenkonstruktion zu sehen ist. Hier ist beides kombiniert: Sowohl der h4-ML als auch ein Anschluss für Aktivboxen steht zur Verfügung.

- Möglicherweise ist der Bau der Henne nicht in jedem Detail exakt identisch mit der Vorlage, weil man manche Feinheiten auf dem Foto nicht sehen kann. Aber soweit es auf der Vorlage überhaupt erkennbar war, blieb qincym natürlich so nah wie nur möglich an der Vorlage.