---
layout: "image"
title: "Elektronik und Aktivboxenanschluss"
date: "2010-05-16T21:24:02"
picture: "elektronischehenne10.jpg"
weight: "10"
konstrukteure: 
- "qincym und steffalk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/27279
imported:
- "2019"
_4images_image_id: "27279"
_4images_cat_id: "1958"
_4images_user_id: "104"
_4images_image_date: "2010-05-16T21:24:02"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27279 -->
Die Silberlinge waren auch im Original mit Winkelsteinen 60° angebracht. Ob sie, wie hier, von unten abgestützt wurden, erschließt sich aus dem einen einzigen Foto des Clubheftes nicht. Ohne die Stützen war es steffalk aber zu riskant - es könnte ja ein Silberling-Gehäuse ausbrechen, wenn jemand stark von oben drauf drückt.

Gut zu sehen ist hier auch der Anschluss für die Aktivboxen. Der geht einfach direkt von einem Pol der Stromversorgung und einem Ausgang des als Tongenerator geschalteten h4-Grundbausteins weg. So macht das Gackern natürlich noch mehr her. Im Original wurde hier ein "Phono-Stecker" (Dioden-Stecker, Pegel eines Kristalltonabnehmers) zum Anschluss an einen Verstärker verwendet. Diese Art Stecker ist allerdings heute nicht mehr üblich ist und wurde also durch eine modernere Variante ersetzt.