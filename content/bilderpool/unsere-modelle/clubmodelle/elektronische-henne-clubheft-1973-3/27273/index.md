---
layout: "image"
title: "Die Henne"
date: "2010-05-16T21:24:01"
picture: "elektronischehenne04.jpg"
weight: "4"
konstrukteure: 
- "qincym und steffalk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/27273
imported:
- "2019"
_4images_image_id: "27273"
_4images_cat_id: "1958"
_4images_user_id: "104"
_4images_image_date: "2010-05-16T21:24:01"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27273 -->
Hier ist das brave Tier in seiner ganzen Pracht. Der Schnabel ist ein mit einem Messer bearbeiteter fischergeometric-Verbinder, während der Kamm ein Statikaufnehmer ist, wie er z. B. im hobby-S für die Aufnahme von 4 Statikträgern 60 enthalten war, der mit Hilfe von Heißluft gekrümmt wurde.