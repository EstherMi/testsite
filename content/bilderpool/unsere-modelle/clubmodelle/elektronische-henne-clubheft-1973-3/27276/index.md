---
layout: "image"
title: "Schwanzgefieder"
date: "2010-05-16T21:24:02"
picture: "elektronischehenne07.jpg"
weight: "7"
konstrukteure: 
- "qincym und steffalk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/27276
imported:
- "2019"
_4images_image_id: "27276"
_4images_cat_id: "1958"
_4images_user_id: "104"
_4images_image_date: "2010-05-16T21:24:02"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27276 -->
Hier ein Blick auf das prächtige Gefieder.