---
layout: "image"
title: "Reedkontakte am Keksdrucker"
date: "2017-09-30T11:52:18"
picture: "09_Reedkontakt.jpg"
weight: "9"
konstrukteure: 
- "Fabian, Max, Christian & Stefan"
fotografen:
- "Christian und Stefan"
keywords: ["Reedkontakt"]
uploadBy: "ftpi"
license: "unknown"
legacy_id:
- details/46516
imported:
- "2019"
_4images_image_id: "46516"
_4images_cat_id: "10"
_4images_user_id: "2611"
_4images_image_date: "2017-09-30T11:52:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46516 -->
Die Positionen im Trockenlager (Reihe und Position) werden mit Wartungsarmen Reedkontakten erfasst. Preiswerte Neodymmagneten in einem 4mm Schlauch sind preiswert und platzsparend. In den Endpunkten der Strecken werden Referenztaster eingesetzt.