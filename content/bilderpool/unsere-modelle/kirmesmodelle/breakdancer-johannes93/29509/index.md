---
layout: "image"
title: "Rückansicht"
date: "2010-12-23T15:37:40"
picture: "breakdancer03.jpg"
weight: "3"
konstrukteure: 
- "Johannes W."
fotografen:
- "Johannes W."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/29509
imported:
- "2019"
_4images_image_id: "29509"
_4images_cat_id: "2149"
_4images_user_id: "636"
_4images_image_date: "2010-12-23T15:37:40"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29509 -->
