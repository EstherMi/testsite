---
layout: "image"
title: "Rechts-Vorne"
date: "2007-09-22T12:03:17"
picture: "Zhler_005.jpg"
weight: "1"
konstrukteure: 
- "Constantin N."
fotografen:
- "Constantin N."
keywords: ["Schießbude"]
uploadBy: "Coni"
license: "unknown"
legacy_id:
- details/11902
imported:
- "2019"
_4images_image_id: "11902"
_4images_cat_id: "1063"
_4images_user_id: "653"
_4images_image_date: "2007-09-22T12:03:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11902 -->
