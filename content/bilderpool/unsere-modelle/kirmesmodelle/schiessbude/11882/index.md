---
layout: "image"
title: "Aufstellung"
date: "2007-09-21T15:44:36"
picture: "aufstellung.jpg"
weight: "3"
konstrukteure: 
- "Constantin N."
fotografen:
- "Constantin N."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Coni"
license: "unknown"
legacy_id:
- details/11882
imported:
- "2019"
_4images_image_id: "11882"
_4images_cat_id: "1063"
_4images_user_id: "653"
_4images_image_date: "2007-09-21T15:44:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11882 -->
Die umgeschossenen Figuren werden aufgestellt