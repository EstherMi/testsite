---
layout: "image"
title: "Karussell"
date: "2008-10-22T18:56:33"
picture: "schleuder1.jpg"
weight: "3"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/16028
imported:
- "2019"
_4images_image_id: "16028"
_4images_cat_id: "124"
_4images_user_id: "845"
_4images_image_date: "2008-10-22T18:56:33"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16028 -->
