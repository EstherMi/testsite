---
layout: "image"
title: "Dachrahmen mit Stütze"
date: "2014-06-12T13:24:35"
picture: "musikexpressii04.jpg"
weight: "9"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/38939
imported:
- "2019"
_4images_image_id: "38939"
_4images_cat_id: "2913"
_4images_user_id: "130"
_4images_image_date: "2014-06-12T13:24:35"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38939 -->
Im Original ist das ein Teil. Muss mal schauen ob ich das noch ändere.
Auf diesem Bild sieht man die Verdrehung ganz gut. Muss mal sehen wie die weg bekomme.