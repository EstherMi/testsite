---
layout: "image"
title: "Der vordere Dachrahmen"
date: "2014-06-12T13:24:35"
picture: "musikexpressii02.jpg"
weight: "7"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/38937
imported:
- "2019"
_4images_image_id: "38937"
_4images_cat_id: "2913"
_4images_user_id: "130"
_4images_image_date: "2014-06-12T13:24:35"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38937 -->
Nach hinten geht der noch genauso weiter. Ist im Moment wegen Platzmangels noch nicht angebaut.