---
layout: "image"
title: "Mittelbau"
date: "2014-06-12T13:24:35"
picture: "musikexpressii05.jpg"
weight: "10"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/38940
imported:
- "2019"
_4images_image_id: "38940"
_4images_cat_id: "2913"
_4images_user_id: "130"
_4images_image_date: "2014-06-12T13:24:35"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38940 -->
