---
layout: "image"
title: "Gondel Draufsicht"
date: "2014-06-12T13:24:35"
picture: "musikexpressii08.jpg"
weight: "13"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/38943
imported:
- "2019"
_4images_image_id: "38943"
_4images_cat_id: "2913"
_4images_user_id: "130"
_4images_image_date: "2014-06-12T13:24:35"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38943 -->
