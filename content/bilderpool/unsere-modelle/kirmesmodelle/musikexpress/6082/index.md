---
layout: "image"
title: "Gesamtansicht"
date: "2006-04-12T22:26:52"
picture: "Gesamtansicht_ME.jpg"
weight: "8"
konstrukteure: 
- "stephan"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/6082
imported:
- "2019"
_4images_image_id: "6082"
_4images_cat_id: "521"
_4images_user_id: "130"
_4images_image_date: "2006-04-12T22:26:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6082 -->
Die neusten Bilder von meinem ME Prototypen. Ich bau gerade an der Unterkonstruktion für die Schiene und dem Aufgang.