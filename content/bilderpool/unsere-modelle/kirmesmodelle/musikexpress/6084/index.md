---
layout: "image"
title: "Die Schiene"
date: "2006-04-12T22:26:53"
picture: "Stephan_am_Schiene_konstruieren.jpg"
weight: "10"
konstrukteure: 
- "stephan"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/6084
imported:
- "2019"
_4images_image_id: "6084"
_4images_cat_id: "521"
_4images_user_id: "130"
_4images_image_date: "2006-04-12T22:26:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6084 -->
Die Schiene vom ME. Der normale Kreis aus den Bogensegmenten war mir etwas zu klein darum habe ich ihn etwas vergrössert.