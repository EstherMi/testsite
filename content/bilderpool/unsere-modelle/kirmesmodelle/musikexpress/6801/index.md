---
layout: "image"
title: "Eine andere Draufsicht"
date: "2006-09-14T23:22:17"
picture: "ME_10.jpg"
weight: "4"
konstrukteure: 
- "stephan"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/6801
imported:
- "2019"
_4images_image_id: "6801"
_4images_cat_id: "521"
_4images_user_id: "130"
_4images_image_date: "2006-09-14T23:22:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6801 -->
