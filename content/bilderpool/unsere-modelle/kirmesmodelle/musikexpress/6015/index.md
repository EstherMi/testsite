---
layout: "image"
title: "Mittelbau von meinem neuen Musikexpress"
date: "2006-04-03T20:05:09"
picture: "Gesamtansicht_Mittelbau_ME.jpg"
weight: "12"
konstrukteure: 
- "stephan"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/6015
imported:
- "2019"
_4images_image_id: "6015"
_4images_cat_id: "521"
_4images_user_id: "130"
_4images_image_date: "2006-04-03T20:05:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6015 -->
Hier die ersten Aufnahmen von meinem neuen Modell, einem Musikexpress wie er auf den Kirmessen zu sehen ist. 
Im Moment ist es erst der erste Rohbau von Mittelbau aber im großen und ganzen wird er später so aussehen. Die Säulen sind ausziehbar und da kommt noch ein Motor dran.Habe gestern gemerkt das er doch noch einiger Verbesserungen bedarf. So ist die Säulenbefestigung unten noch viel zu mickrig um den späteren Dachstuhl überhaupt zu tragen. Das bekomme ich aber auch noch hin. Ist nur ein kleines Problem.
Im Moment baue ich den ganzen Unterbau neu, da der der alte zu wackelig war und man die Säulen nicht gescheit befestigen konnte.