---
layout: "image"
title: "Schiene 2"
date: "2006-04-12T22:35:21"
picture: "Stephan_am_Schiene_konstruieren_2.jpg"
weight: "11"
konstrukteure: 
- "stephan"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/6085
imported:
- "2019"
_4images_image_id: "6085"
_4images_cat_id: "521"
_4images_user_id: "130"
_4images_image_date: "2006-04-12T22:35:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6085 -->
Im Vordergrund ist das Oberteil (Dach) vom ME zu sehen. Ist aber noch der erste Aufbau. Will ich noch zusammenklappbar bauen. Ist dann platzsparender und einfacher zum transportieren.