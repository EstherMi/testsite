---
layout: "image"
title: "Wellenflug010"
date: "2011-10-21T15:49:26"
picture: "Wellenflug010.JPG"
weight: "4"
konstrukteure: 
- "Fa. Zierer"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/33277
imported:
- "2019"
_4images_image_id: "33277"
_4images_cat_id: "2462"
_4images_user_id: "4"
_4images_image_date: "2011-10-21T15:49:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33277 -->
Der Gondelträger wird in den Schienen geführt, aber per Stahlseil gehoben. Hier sieht man den gebogenen Teil der Schiene mit den Umlenkrollen.


München, Oktoberfest 2004