---
layout: "image"
title: "Wellenflug043"
date: "2011-10-21T15:46:52"
picture: "Wellenflug043.JPG"
weight: "3"
konstrukteure: 
- "Fa. Zierer"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/33276
imported:
- "2019"
_4images_image_id: "33276"
_4images_cat_id: "2462"
_4images_user_id: "4"
_4images_image_date: "2011-10-21T15:46:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33276 -->
Aus mehr als dem Drahtgerippe, dem Träger in der Mitte und dem Mast mit den Laufschienen drin besteht ein Wellenflieger nicht. Der Rest sind die Ketten nebst Sitzen und dann noch Licht und Pappe zur Dekoration. Die Tücken im Detail haben mich aber beim Nachbau länger beschäftigt als ein ungleich komplexeres Modell wie der 'Jumping'.


München, Oktoberfest 2004