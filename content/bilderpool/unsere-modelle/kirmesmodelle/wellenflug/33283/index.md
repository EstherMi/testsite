---
layout: "image"
title: "WF6843.JPG"
date: "2011-10-21T16:31:09"
picture: "WF6843.JPG"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/33283
imported:
- "2019"
_4images_image_id: "33283"
_4images_cat_id: "2462"
_4images_user_id: "4"
_4images_image_date: "2011-10-21T16:31:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33283 -->
Die unteren Schleifringe sind nötig, weil sich der ganze Sockel mitsamt Seilwinde dreht.