---
layout: "comment"
hidden: true
title: "15594"
date: "2011-11-01T11:33:35"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Harald steht halt konsequent auf Originalteile und würde niemals auch nur einen ft-fremden Faden verwenden. :-)

Gruß,
Stefan