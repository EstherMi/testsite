---
layout: "comment"
hidden: true
title: "15522"
date: "2011-10-23T23:09:06"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Hallo Dirk,
für das Thema "Retten von ft-Teilen vor dem Messer" habe ich zu wenig Substanz zu bieten, fürchte ich ;-). Aber unter der Überschrift "Fremdteile adaptieren" könnte da etwas draus werden. Auf dem Gebiet sind aber noch etliche andere Bastler unterwegs; da wäre wohl eine Serie oder Kollaboration angesagt.

Hallo Thomas,
... jaaah, so möchte man meinen. Da ist aber noch so ne Tücke des Objekts im Spiel: Die ft-Nabenaufnahmen haben allesamt eine Vierer-Symmetrie: sie zeigen nach innen vier "Zahnleisten", die sich mit vier Lücken dazwischen abwechseln. Ich musste die Drehscheiben alle so orientieren, dass die Zahnlücken mit den Kanten der Flachträger am Mast zusammen fallen. Die Schleifringe haben das gleiche System, aber... um ein paar Grad versetzt. Das passt nicht zusammen, wenn man die Schleifring-Kontakte durch die Drehscheibe steckt. Das war noch so ein Punkt, warum dieses Modell beinahe in der Kategorie "Fehlversuche" gelandet wäre.

Gruß,
Harald