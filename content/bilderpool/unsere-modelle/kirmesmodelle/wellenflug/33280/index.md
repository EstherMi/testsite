---
layout: "image"
title: "WF6866.JPG"
date: "2011-10-21T16:03:35"
picture: "WF6866.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/33280
imported:
- "2019"
_4images_image_id: "33280"
_4images_cat_id: "2462"
_4images_user_id: "4"
_4images_image_date: "2011-10-21T16:03:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33280 -->
Die Halterung des schwarzen Motors war doch etwas zu liederlich und musste vor Ort mit Klebeband stabilisiert werden. Der PowerMot zieht mittels Seil den Gondelträger nach oben.