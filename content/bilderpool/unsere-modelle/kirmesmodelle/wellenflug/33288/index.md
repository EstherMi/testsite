---
layout: "image"
title: "WF6848.JPG"
date: "2011-10-21T17:12:19"
picture: "WF6848.JPG"
weight: "15"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/33288
imported:
- "2019"
_4images_image_id: "33288"
_4images_cat_id: "2462"
_4images_user_id: "4"
_4images_image_date: "2011-10-21T17:12:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33288 -->
Damit endlich Ruhe herrscht: Drehbänklein angeschmissen und ein rundes Messingblech mit ein paar Bohrungen angefertigt. Im zweiten Anlauf gelungen. Bohrer mit 2,6 mm Durchmesser für die Stecker "hat man" natürlich vorrätig.
Der zweite Schleifring besteht aus einem Kupferdraht, der in der Nut der Drehscheibe liegt, und einem zweiten, der außen drum herum geschlungen ist und von der schwarzen ft-Feder gespannt wird. Simpler geht es nicht, und funktionieren tut es außerdem. 

Das war am Vortag der ft-Convention, und für schmückende Ausgestaltung des Modells blieb keine Zeit mehr.