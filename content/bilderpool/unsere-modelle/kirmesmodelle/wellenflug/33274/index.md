---
layout: "image"
title: "Wellenflug006.jpg"
date: "2011-10-21T15:40:39"
picture: "Wellenflug006.JPG"
weight: "1"
konstrukteure: 
- "Franz Schwarzkopf (Fa. Zierer)"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/33274
imported:
- "2019"
_4images_image_id: "33274"
_4images_cat_id: "2462"
_4images_user_id: "4"
_4images_image_date: "2011-10-21T15:40:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33274 -->
Der "Wellenflug" hat seinen Namen durch die "eiernde" Bewegung, die der Gondelträger (das runde Teil unten mitten drin) ausführt, wenn er so weit angehoben ist, dass er den schrägen Teil des Mastes erreicht hat. Dort kippt die Bahn in die Schräge, und weil der ganze Mast auch noch (gegenläufig zum Gondelträger) dreht, wandert die Drehachse im Raum wie ein präzessierender Kreisel. "Präzessierender Kreisel" klingt etwas sehr technisch für ein Karussell, also ist man eben auf "Wellenflug" gekommen.

München, Oktoberfest 2004