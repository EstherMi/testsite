---
layout: "image"
title: "WF6109.JPG"
date: "2011-10-21T16:39:24"
picture: "WF6109.JPG"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/33284
imported:
- "2019"
_4images_image_id: "33284"
_4images_cat_id: "2462"
_4images_user_id: "4"
_4images_image_date: "2011-10-21T16:39:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33284 -->
Der Antrieb ist jetzt oben drauf und führt über ein Z40, das nach oben und unten mit drei Achsen 30 mit den Nachbarn verbunden ist. Die Schleifringe sind zusammen gesetzt aus ft-Stahlfedern, die in der Nut je einer Drehscheibe liegen. Das taugt aber nichts. 
Erstens sind die Stahlfedern eher Vorwiderstände als dass sie Strom übertragen würden. Da wurde im hobby 3 sogar eine Heizung mit gebaut (sowas fällt einem aber nur hinterher wieder ein). Zweitens haben die Stromabnehmer der BSB keinen seitlichen Halt und hüpfen ständig herum, bis hin zum Kurzschluss zwischen den Bahnen, worauf der Antrieb sofort steht und ein mächtiger Ruck durch den Apparat geht. Ohne das Verstiften hätte jeder "Kurze" einen Stirnzapfen im Mast gekostet.
Der Rettungsversuch mit dem gebogenen Messingblech (ein Zipfel davon rechts oben, an der Zugfeder) hat auch nicht viel gebracht.