---
layout: "image"
title: "17 Kirmesmodell"
date: "2013-02-03T19:24:42"
picture: "kirmesmodell17.jpg"
weight: "17"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/36575
imported:
- "2019"
_4images_image_id: "36575"
_4images_cat_id: "2712"
_4images_user_id: "860"
_4images_image_date: "2013-02-03T19:24:42"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36575 -->
Im Dunkeln liefern die LEDs ausreichend Licht.

Video: http://www.youtube.com/watch?v=y6XDw-Xxx7A