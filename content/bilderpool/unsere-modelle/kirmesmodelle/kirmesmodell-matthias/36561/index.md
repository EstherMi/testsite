---
layout: "image"
title: "03 Kirmesmodell"
date: "2013-02-03T19:24:42"
picture: "kirmesmodell03.jpg"
weight: "3"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/36561
imported:
- "2019"
_4images_image_id: "36561"
_4images_cat_id: "2712"
_4images_user_id: "860"
_4images_image_date: "2013-02-03T19:24:42"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36561 -->
Die Gondel + Zugangstreppe