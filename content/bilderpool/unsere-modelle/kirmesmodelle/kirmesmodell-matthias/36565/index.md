---
layout: "image"
title: "07 Kirmesmodell"
date: "2013-02-03T19:24:42"
picture: "kirmesmodell07.jpg"
weight: "7"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/36565
imported:
- "2019"
_4images_image_id: "36565"
_4images_cat_id: "2712"
_4images_user_id: "860"
_4images_image_date: "2013-02-03T19:24:42"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36565 -->
Die zweite Achse wird durch einen 20:1 Motor angetrieben