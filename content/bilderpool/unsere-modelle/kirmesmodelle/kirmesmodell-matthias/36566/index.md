---
layout: "image"
title: "08 Kirmesmodell"
date: "2013-02-03T19:24:42"
picture: "kirmesmodell08.jpg"
weight: "8"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/36566
imported:
- "2019"
_4images_image_id: "36566"
_4images_cat_id: "2712"
_4images_user_id: "860"
_4images_image_date: "2013-02-03T19:24:42"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36566 -->
Die dritte Achse wird mit einem 50:1 Motor angetrieben. 
Ziemlich kompakt, wie ich finde ;-)