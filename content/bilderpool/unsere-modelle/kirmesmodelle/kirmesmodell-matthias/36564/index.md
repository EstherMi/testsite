---
layout: "image"
title: "06 Kirmesmodell"
date: "2013-02-03T19:24:42"
picture: "kirmesmodell06.jpg"
weight: "6"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/36564
imported:
- "2019"
_4images_image_id: "36564"
_4images_cat_id: "2712"
_4images_user_id: "860"
_4images_image_date: "2013-02-03T19:24:42"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36564 -->
Mithilfe dieses Tasters kommt die erste Achse zurück in die Ausgangsposition.

Video: http://www.youtube.com/watch?v=y6XDw-Xxx7A