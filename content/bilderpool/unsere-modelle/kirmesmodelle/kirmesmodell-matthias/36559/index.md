---
layout: "image"
title: "01 Kirmesmodell"
date: "2013-02-03T19:24:41"
picture: "kirmesmodell01.jpg"
weight: "1"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/36559
imported:
- "2019"
_4images_image_id: "36559"
_4images_cat_id: "2712"
_4images_user_id: "860"
_4images_image_date: "2013-02-03T19:24:41"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36559 -->
Ursprünglich wollte ich ein anderes Modell, den Airwolf (http://www.airwolf-eberhard.de/?page_id=9 bzw. http://www.ftcommunity.de/categories.php?cat_id=346) bauen, musste dann aber recht bald akzeptieren, dass ich keinen ausreichend stabilen Arm bauen kann, der das Gewicht der Gondel tragen kann, ohne sich zu biegen. Lange Aluprofile wären sicher eine Lösung gewesen, dann hätte aber wohl der Drehkranz Probleme gemacht. 
So habe ich mich entschieden, zur Gondel ein anderes Modell zu bauen. 

Verbaut sind 3 Motoren für die drei Achsen, eine Kompressoreinheit mit Ventilen und Zylindern für die Zugangstreppen, über 20 LEDs zur Beleuchtung und ein Interface zur Steuerung. Die Steuerung erfolgt ausschließlich über ein Bedienfeld im Programm.

Video: http://www.youtube.com/watch?v=y6XDw-Xxx7A