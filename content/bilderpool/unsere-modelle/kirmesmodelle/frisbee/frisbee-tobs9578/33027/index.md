---
layout: "image"
title: "Antrieb des Arms"
date: "2011-10-01T13:47:25"
picture: "frissbevontobs3.jpg"
weight: "3"
konstrukteure: 
- "tobs9578"
fotografen:
- "tobs9578"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- details/33027
imported:
- "2019"
_4images_image_id: "33027"
_4images_cat_id: "2434"
_4images_user_id: "1007"
_4images_image_date: "2011-10-01T13:47:25"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33027 -->
Der Hauptarm wird von diesem Reibrad angetrieben.Natürlich vollgefedert, um die Kraft die nach unten wirkt abzufangen.