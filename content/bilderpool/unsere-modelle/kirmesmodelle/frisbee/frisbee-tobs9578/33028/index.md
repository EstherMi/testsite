---
layout: "image"
title: "Schulterbügel"
date: "2011-10-01T13:47:25"
picture: "frissbevontobs4.jpg"
weight: "4"
konstrukteure: 
- "tobs9578"
fotografen:
- "tobs9578"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- details/33028
imported:
- "2019"
_4images_image_id: "33028"
_4images_cat_id: "2434"
_4images_user_id: "1007"
_4images_image_date: "2011-10-01T13:47:25"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33028 -->
Da sitzt auch schon der erste Probefahrer. ;)