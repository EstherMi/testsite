---
layout: "comment"
hidden: true
title: "15469"
date: "2011-10-20T08:13:18"
uploadBy:
- "Jorobo"
license: "unknown"
imported:
- "2019"
---
Hallo tobs9578,

sieht schon sehr gut aus!
Aber zwei Fragen:
1.Kannst du bestimmte Abschnitte der LED-Kette getrennt ansteuern?
2.Wie hast du die LEDs an den Statikträgern befestigt?

Viel Spaß noch beim Bauen ;-)
Jonas