---
layout: "image"
title: "Ledleiste eingebaut"
date: "2011-10-29T19:13:33"
picture: "fri1.jpg"
weight: "7"
konstrukteure: 
- "tobs9578"
fotografen:
- "tobs9578"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- details/33362
imported:
- "2019"
_4images_image_id: "33362"
_4images_cat_id: "2453"
_4images_user_id: "1007"
_4images_image_date: "2011-10-29T19:13:33"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33362 -->
Hier die Led Leiste im eingebauten Zustand.