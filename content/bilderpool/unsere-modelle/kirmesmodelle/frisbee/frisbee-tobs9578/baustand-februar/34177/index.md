---
layout: "image"
title: "Gesamtes Modell"
date: "2012-02-13T16:50:35"
picture: "frisbee4.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- details/34177
imported:
- "2019"
_4images_image_id: "34177"
_4images_cat_id: "2532"
_4images_user_id: "1007"
_4images_image_date: "2012-02-13T16:50:35"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34177 -->
