---
layout: "image"
title: "Fahrgastgondel von oben"
date: "2011-10-01T13:47:25"
picture: "frissbevontobs5.jpg"
weight: "5"
konstrukteure: 
- "tobs9578"
fotografen:
- "tobs9578"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- details/33029
imported:
- "2019"
_4images_image_id: "33029"
_4images_cat_id: "2434"
_4images_user_id: "1007"
_4images_image_date: "2011-10-01T13:47:25"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33029 -->
Hier sieht man noch einmal die Gondel von oben.