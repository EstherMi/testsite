---
layout: "image"
title: "Die Gondel"
date: "2009-04-13T14:50:49"
picture: "frisbee02.jpg"
weight: "2"
konstrukteure: 
- "Robo Fan"
fotografen:
- "Robo Fan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Robo Fan"
license: "unknown"
legacy_id:
- details/23693
imported:
- "2019"
_4images_image_id: "23693"
_4images_cat_id: "1618"
_4images_user_id: "771"
_4images_image_date: "2009-04-13T14:50:49"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23693 -->
