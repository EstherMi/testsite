---
layout: "image"
title: "Die Aufhängung des Armes"
date: "2009-04-13T14:50:56"
picture: "frisbee14.jpg"
weight: "14"
konstrukteure: 
- "Robo Fan"
fotografen:
- "Robo Fan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Robo Fan"
license: "unknown"
legacy_id:
- details/23705
imported:
- "2019"
_4images_image_id: "23705"
_4images_cat_id: "1618"
_4images_user_id: "771"
_4images_image_date: "2009-04-13T14:50:56"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23705 -->
Wurde mit vier Streben stabiler gemacht.