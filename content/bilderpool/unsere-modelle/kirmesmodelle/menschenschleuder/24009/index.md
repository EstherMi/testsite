---
layout: "image"
title: "Stromzufuhr Gondel-Motor"
date: "2009-05-13T17:01:38"
picture: "menschenschleuder09.jpg"
weight: "9"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/24009
imported:
- "2019"
_4images_image_id: "24009"
_4images_cat_id: "1646"
_4images_user_id: "845"
_4images_image_date: "2009-05-13T17:01:38"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24009 -->
