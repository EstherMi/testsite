---
layout: "image"
title: "Einsteigesteg-Karussel in Betrieb"
date: "2009-05-13T17:01:38"
picture: "menschenschleuder06.jpg"
weight: "6"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/24006
imported:
- "2019"
_4images_image_id: "24006"
_4images_cat_id: "1646"
_4images_user_id: "845"
_4images_image_date: "2009-05-13T17:01:38"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24006 -->
