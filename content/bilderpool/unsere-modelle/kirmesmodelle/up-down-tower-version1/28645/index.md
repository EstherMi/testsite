---
layout: "image"
title: "Gesamtansicht2"
date: "2010-09-27T23:33:13"
picture: "updowntowerversion02.jpg"
weight: "2"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- details/28645
imported:
- "2019"
_4images_image_id: "28645"
_4images_cat_id: "2071"
_4images_user_id: "1030"
_4images_image_date: "2010-09-27T23:33:13"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28645 -->
Hier seht ihr eine Gesamtansicht meines neuen Modells. In diesem Zustand exestiert noch keine Elektronik, doch es ist stabiel gebaut. Die Türme sind leider etwas dünn und deswegen besteht Umsturzgefahr!