---
layout: "overview"
title: "UP & DOWN Tower Version1"
date: 2019-12-17T18:55:18+01:00
legacy_id:
- categories/2071
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2071 --> 
Der kleine Zug fällt von oben runter und schwingt dann unten hin und her.
Es sind zwar nicht die schönsten Bilder, doch Ich hoffe sie gefallen euch trotzdem.

WICHTIG: Das ist die alte Version nicht die von der Convention! Bilder der aktuellen Version folgen in kürze!