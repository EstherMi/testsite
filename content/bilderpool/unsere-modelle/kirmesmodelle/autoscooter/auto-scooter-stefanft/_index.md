---
layout: "overview"
title: "Auto-Scooter (stefanft)"
date: 2019-12-17T18:55:29+01:00
legacy_id:
- categories/1189
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1189 --> 
Dieses Kirmes-Modell ist ein Nachbau des ft Club-Modells Auto-Scooter aus dem Jahr 1978 (die Anleitung dazu ist hier in der ft-community zum download verfügbar - Danke!). Nur mit neueren ft-Teilen und auf doppelter Grundfläche.