---
layout: "image"
title: "Fahrzeug einzeln"
date: "2007-12-26T18:26:58"
picture: "autoscooter4.jpg"
weight: "7"
konstrukteure: 
- "stefanft"
fotografen:
- "stefanft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stefanft"
license: "unknown"
legacy_id:
- details/13170
imported:
- "2019"
_4images_image_id: "13170"
_4images_cat_id: "1189"
_4images_user_id: "672"
_4images_image_date: "2007-12-26T18:26:58"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13170 -->
Auch ganz simpel... aber der Teufel steckt - wie immer - im Detail.

Das Problem bei diesem Modell ist, dass der "Alufolien-Fahrboden" extrem glatt ist. Und die Antriebsräder des Fahrzeugs darauf schnell durchdrehen. Ich habe die Antriebsachse zwar schon mit 2 Gewindestangen (wie sie als Gewichte bei dem Super Cranes Baukasten mitgeliefert werden) beschwert, aber trozdem rutschen die Antriebsräder noch allzu oft durch. Vielleicht sollte ich mich mal auf die Suche nach den paar kg Angler-Blei machen, die hier noch irgendwo rumliegen müssen...