---
layout: "image"
title: "Scooter vorn"
date: "2014-12-28T22:12:40"
picture: "autoscooter30.jpg"
weight: "30"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/40041
imported:
- "2019"
_4images_image_id: "40041"
_4images_cat_id: "3010"
_4images_user_id: "2303"
_4images_image_date: "2014-12-28T22:12:40"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40041 -->
Die Front ist demontiert, darunter liegt die Beleuchtung.