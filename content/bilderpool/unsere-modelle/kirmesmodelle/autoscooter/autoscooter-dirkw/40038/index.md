---
layout: "image"
title: "Antrieb"
date: "2014-12-28T22:12:40"
picture: "autoscooter27.jpg"
weight: "27"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/40038
imported:
- "2019"
_4images_image_id: "40038"
_4images_cat_id: "3010"
_4images_user_id: "2303"
_4images_image_date: "2014-12-28T22:12:40"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40038 -->
Hinten seht ihr den Antrieb über die Räder. 

Beim Anfahren ist das Problem aufgetreten, das die Antriebsräder ständig durchdrehten.
Daher habe ich die Räder mit doppelseitigen Klebeband beklebt. Danach hat es funktioniert.

In der Mitte seht ihr den Federkontakt für die Stromversorgung über die Alu-Bodenplatte.
