---
layout: "image"
title: "Alu-Platte"
date: "2014-12-28T22:12:40"
picture: "autoscooter16.jpg"
weight: "16"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/40027
imported:
- "2019"
_4images_image_id: "40027"
_4images_cat_id: "3010"
_4images_user_id: "2303"
_4images_image_date: "2014-12-28T22:12:40"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40027 -->
Die Kratzer auf der Alu-Platte kommen von den Federkontakten der Scooter.