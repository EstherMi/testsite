---
layout: "image"
title: "Heck hinten"
date: "2014-12-28T22:12:40"
picture: "autoscooter37.jpg"
weight: "37"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/40048
imported:
- "2019"
_4images_image_id: "40048"
_4images_cat_id: "3010"
_4images_user_id: "2303"
_4images_image_date: "2014-12-28T22:12:40"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40048 -->
Ich habe das Heck so gebaut, das in die Aussparungen die Rückleuchten passen.