---
layout: "image"
title: "Scooter"
date: "2014-12-28T22:12:40"
picture: "autoscooter19.jpg"
weight: "19"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/40030
imported:
- "2019"
_4images_image_id: "40030"
_4images_cat_id: "3010"
_4images_user_id: "2303"
_4images_image_date: "2014-12-28T22:12:40"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40030 -->
Bei den Proportionen habe ich versucht, möglichst nahe an dem Original zu bleiben