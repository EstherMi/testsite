---
layout: "overview"
title: "Autoscooter (DirkW)"
date: 2019-12-17T18:55:34+01:00
legacy_id:
- categories/3010
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3010 --> 
Hallo zusammen,

es wurde mal Zeit, etwas ohne viel Elektronik zu bauen. Ein Autoscooter sollte es werden.

Es gab im Jahr 3/4 1978 bereits eine Bauanleitung von Fischertechnik. Siehe
http://ftcommunity.de/data/downloads/bauanleitungen/autoscooter3478.pdf

Leider sehen  in der Anleitung die Scooter nicht so unbedingt den Originalen ähnlich.
Daher war der Anspruch den Autoscooter näher am Original zu bauen.

Herausgekommen ist ein Autoscooter mit 4 Fahrzeugen und Beleuchtung.

Den Autoscooter in Aktion findet ihr unter "youtube".
https://www.youtube.com/watch?v=xy3GFT1186Y



