---
layout: "image"
title: "2 Scooter Front"
date: "2014-12-28T22:12:40"
picture: "autoscooter04.jpg"
weight: "4"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/40015
imported:
- "2019"
_4images_image_id: "40015"
_4images_cat_id: "3010"
_4images_user_id: "2303"
_4images_image_date: "2014-12-28T22:12:40"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40015 -->
