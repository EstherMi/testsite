---
layout: "image"
title: "Scooter auf Alu-Platte"
date: "2014-12-28T22:12:40"
picture: "autoscooter12.jpg"
weight: "12"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/40023
imported:
- "2019"
_4images_image_id: "40023"
_4images_cat_id: "3010"
_4images_user_id: "2303"
_4images_image_date: "2014-12-28T22:12:40"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40023 -->
Hier seht ihr einige Scooter auf der unbearbeiteten Alu-Platte für den Boden.