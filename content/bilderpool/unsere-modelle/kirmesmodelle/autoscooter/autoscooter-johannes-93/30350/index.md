---
layout: "image"
title: "Gitter"
date: "2011-03-31T18:43:01"
picture: "autoscooterjohannes07.jpg"
weight: "7"
konstrukteure: 
- "Johannes93"
fotografen:
- "Johannes93"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/30350
imported:
- "2019"
_4images_image_id: "30350"
_4images_cat_id: "2258"
_4images_user_id: "636"
_4images_image_date: "2011-03-31T18:43:01"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30350 -->
Das Gitter ist ein Lüftungsgitter aus dem Baumarkt. Es ist sehr stabil und ist daher nur mit einigen Drähten an der Dachkonstruktion befestigt.