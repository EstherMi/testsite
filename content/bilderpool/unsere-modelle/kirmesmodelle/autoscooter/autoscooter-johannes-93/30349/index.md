---
layout: "image"
title: "Steuerpult"
date: "2011-03-31T18:43:01"
picture: "autoscooterjohannes06.jpg"
weight: "6"
konstrukteure: 
- "Johannes93"
fotografen:
- "Johannes93"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/30349
imported:
- "2019"
_4images_image_id: "30349"
_4images_cat_id: "2258"
_4images_user_id: "636"
_4images_image_date: "2011-03-31T18:43:01"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30349 -->
Hiermit können die Fahrzeuge gestartet und gestoppt werden. Außerdem kann die Beleuchtung eingeschaltet werden.