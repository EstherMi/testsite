---
layout: "image"
title: "Einer von 4 Lagerböcken"
date: "2012-12-17T08:28:08"
picture: "artistico1.jpg"
weight: "1"
konstrukteure: 
- "Stephan"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/36317
imported:
- "2019"
_4images_image_id: "36317"
_4images_cat_id: "2575"
_4images_user_id: "130"
_4images_image_date: "2012-12-17T08:28:08"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36317 -->
Diese Version ist jetzt richtig stabil und mit den 2 Streben auf der bauplatte wackelt da nun nichts mehr. Die sind jetzt auch den schmalen Seiten miteinander verbunden und hinten auch an den Breitseiten.
Das graue Scharnier wurde gegen einen Winkelstein 7,5° ersetzt und durch den roten Gelenkbaustein hab ich ne kurze Metallachse geschoben als Verstärkung.