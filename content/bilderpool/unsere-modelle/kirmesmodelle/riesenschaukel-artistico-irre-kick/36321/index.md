---
layout: "image"
title: "Die Sitze IV"
date: "2012-12-17T08:28:09"
picture: "artistico5.jpg"
weight: "5"
konstrukteure: 
- "Stephan"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/36321
imported:
- "2019"
_4images_image_id: "36321"
_4images_cat_id: "2575"
_4images_user_id: "130"
_4images_image_date: "2012-12-17T08:28:09"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36321 -->
War ne schöne Tüftelei bis ich diese Version fertig hatte. Finde sie aber ganz niedlich.