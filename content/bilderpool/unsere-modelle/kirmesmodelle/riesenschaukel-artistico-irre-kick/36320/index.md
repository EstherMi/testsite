---
layout: "image"
title: "Die Sitze III"
date: "2012-12-17T08:28:08"
picture: "artistico4.jpg"
weight: "4"
konstrukteure: 
- "Stephan"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/36320
imported:
- "2019"
_4images_image_id: "36320"
_4images_cat_id: "2575"
_4images_user_id: "130"
_4images_image_date: "2012-12-17T08:28:08"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36320 -->
Die Version mit Kopfstützen ist für VIP Gäste.