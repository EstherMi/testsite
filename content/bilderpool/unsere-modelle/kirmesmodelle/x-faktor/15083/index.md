---
layout: "image"
title: "Ring groß"
date: "2008-08-24T16:00:36"
picture: "xfaktor05.jpg"
weight: "5"
konstrukteure: 
- "Daniel"
fotografen:
- "Daniel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dragon"
license: "unknown"
legacy_id:
- details/15083
imported:
- "2019"
_4images_image_id: "15083"
_4images_cat_id: "1375"
_4images_user_id: "637"
_4images_image_date: "2008-08-24T16:00:36"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15083 -->
