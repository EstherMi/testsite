---
layout: "image"
title: "Eingabepulthälfte rechts"
date: "2008-08-24T16:00:36"
picture: "xfaktor01.jpg"
weight: "1"
konstrukteure: 
- "Daniel"
fotografen:
- "Daniel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dragon"
license: "unknown"
legacy_id:
- details/15079
imported:
- "2019"
_4images_image_id: "15079"
_4images_cat_id: "1375"
_4images_user_id: "637"
_4images_image_date: "2008-08-24T16:00:36"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15079 -->
Wenn man den X-Faktor aktivieren will muss man einen 8 Stelligen Code eingeben wenn dieser 3 mal Falsch eingegeben wird beendet sich das Programm.

Bei der ersten Falscheingabe piept der summer einmal kurz bei den nächsten malen auch und beim 3ten malpiept er lang und eine rote Lampe leuchtet auf.

Wenn der Code richtig eingegeben wird leuchtet die grüne Lampe kurz auf.