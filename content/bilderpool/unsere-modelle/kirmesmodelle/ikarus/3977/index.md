---
layout: "image"
title: "Der Drehkranz vom Ikarusmodell , Seitenansicht 2"
date: "2005-04-16T19:14:55"
picture: "modell_ikarus18a.jpg"
weight: "24"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/3977
imported:
- "2019"
_4images_image_id: "3977"
_4images_cat_id: "344"
_4images_user_id: "130"
_4images_image_date: "2005-04-16T19:14:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3977 -->
Auf diesem Bild kann man schon zwei von vier Aufnahmen für die Ausleger sehen.