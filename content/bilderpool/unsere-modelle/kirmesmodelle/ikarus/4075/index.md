---
layout: "image"
title: "Modell Ikarus"
date: "2005-04-25T10:58:12"
picture: "Modell_Ikarus_32.jpg"
weight: "15"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/4075
imported:
- "2019"
_4images_image_id: "4075"
_4images_cat_id: "344"
_4images_user_id: "130"
_4images_image_date: "2005-04-25T10:58:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4075 -->
Eine weitere Seitenansicht.