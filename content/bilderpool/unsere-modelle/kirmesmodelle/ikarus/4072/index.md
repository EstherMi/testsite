---
layout: "image"
title: "Die Stützen für den oberen Drehkranz"
date: "2005-04-24T00:01:47"
picture: "Modell_Ikarus_28.jpg"
weight: "20"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/4072
imported:
- "2019"
_4images_image_id: "4072"
_4images_cat_id: "344"
_4images_user_id: "130"
_4images_image_date: "2005-04-24T00:01:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4072 -->
Das sind die Stützen an denen der obere Ring für die Räder festgemacht wird. Auf kommenden Fotos besser zu erkennen. 
Der ist dafür gedacht damit sich der Kranz nicht verzieht wenn die Ausleger dran kommen und somit auch eine Abstützung nach oben gegeben ist. Hab ich schon fertig muss nur noch Bilder davon machen. Dauert aber nicht mehr lange. ;-)