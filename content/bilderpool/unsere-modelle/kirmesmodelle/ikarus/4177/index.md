---
layout: "image"
title: "Die andere Stütze auf einer Pyramide"
date: "2005-05-21T18:42:07"
picture: "Modell_Ikarus_49.jpg"
weight: "5"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/4177
imported:
- "2019"
_4images_image_id: "4177"
_4images_cat_id: "344"
_4images_user_id: "130"
_4images_image_date: "2005-05-21T18:42:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4177 -->
