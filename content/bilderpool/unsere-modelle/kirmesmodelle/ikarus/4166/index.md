---
layout: "image"
title: "Unterbau (Mittelbau)"
date: "2005-05-20T20:10:09"
picture: "Modell_Ikarus_40.jpg"
weight: "6"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/4166
imported:
- "2019"
_4images_image_id: "4166"
_4images_cat_id: "344"
_4images_user_id: "130"
_4images_image_date: "2005-05-20T20:10:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4166 -->
Hier ein Bild vom Mittelbau meines modells. Deutlich sind die Stützen an den Seiten zu sehen. Die roten Drehplatten werden noch gegen eckige Pyramiden gewechselt, die gerade im Bau sind.