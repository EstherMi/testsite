---
layout: "image"
title: "Innenansicht vom Drehkranz"
date: "2005-04-16T19:14:55"
picture: "modell_ikarus19a.jpg"
weight: "25"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/3978
imported:
- "2019"
_4images_image_id: "3978"
_4images_cat_id: "344"
_4images_user_id: "130"
_4images_image_date: "2005-04-16T19:14:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3978 -->
Der Drehkranz von oben ins Innere gesehen. Deutlich sieht man hier den eigentlichen drehbaren Teil.