---
layout: "image"
title: "Drehkranz Draufsicht"
date: "2005-04-16T20:48:27"
picture: "modell_ikarus20a.jpg"
weight: "26"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/3979
imported:
- "2019"
_4images_image_id: "3979"
_4images_cat_id: "344"
_4images_user_id: "130"
_4images_image_date: "2005-04-16T20:48:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3979 -->
Eine Draufsicht auf die kleinen seitlichen Stützrollen und die Bausteine 30 mit denen es innen weiter nach oben geht wo dann der Abschlusskranz drauf zu sitzen kommt.