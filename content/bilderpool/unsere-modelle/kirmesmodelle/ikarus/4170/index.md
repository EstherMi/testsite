---
layout: "image"
title: "Gesamtansicht"
date: "2005-05-20T20:10:10"
picture: "Modell_Ikarus_41.jpg"
weight: "10"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/4170
imported:
- "2019"
_4images_image_id: "4170"
_4images_cat_id: "344"
_4images_user_id: "130"
_4images_image_date: "2005-05-20T20:10:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4170 -->
Hier mal eine Gesamtansicht. Die Höhe inclusive Unterbau beträgt ca. 135cm, die Breite über die Stützen gemessen ca. 70cm.