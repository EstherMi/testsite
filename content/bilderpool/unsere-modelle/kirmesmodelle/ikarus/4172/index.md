---
layout: "image"
title: "Drehkranz am Turm(Draufsicht)"
date: "2005-05-20T20:27:57"
picture: "Modell_Ikarus_42.jpg"
weight: "12"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/4172
imported:
- "2019"
_4images_image_id: "4172"
_4images_cat_id: "344"
_4images_user_id: "130"
_4images_image_date: "2005-05-20T20:27:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4172 -->
