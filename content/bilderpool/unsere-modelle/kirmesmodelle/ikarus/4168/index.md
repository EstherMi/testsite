---
layout: "image"
title: "Seitenansicht"
date: "2005-05-20T20:10:10"
picture: "Modell_Ikarus_39.jpg"
weight: "8"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/4168
imported:
- "2019"
_4images_image_id: "4168"
_4images_cat_id: "344"
_4images_user_id: "130"
_4images_image_date: "2005-05-20T20:10:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4168 -->
Die Seitenansicht zeigt den Bereich wo demnächst der Motor mit der Seilwinde sitzt. Unten drunter kommt aber noch an jede Ecke eine Stütze(Pyramide) die sich gerade im Bau befindet.