---
layout: "image"
title: "Mittelbau vom Ikarus"
date: "2005-05-20T20:10:10"
picture: "Modell_Ikarus_37.jpg"
weight: "7"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/4167
imported:
- "2019"
_4images_image_id: "4167"
_4images_cat_id: "344"
_4images_user_id: "130"
_4images_image_date: "2005-05-20T20:10:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4167 -->
Hier eine andere Ansicht. Unten fehlt noch ein Aluprofil was inzwischen aber schon eingebaut ist. Die Stützen muss ich wohl doch noch etwas verstärken, wie ich schon bemerkt habe.

Eine Anmerkung von mir nachträglich, die untere Stange der Stütze sitzt an dem hier noch fehlenden Alu Profil und nicht an den BS30 die von oben nach unten sitzen.