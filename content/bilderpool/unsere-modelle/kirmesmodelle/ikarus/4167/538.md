---
layout: "comment"
hidden: true
title: "538"
date: "2005-05-21T00:53:22"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Heavy!Ist ja mächtig imposant, das Teil!

Ich glaube aber, dass ich da ein kleines "aber" loswerden muss (sorry to be raining on your parade there): Es mag ja sein, dass "viel" auch viel hilft, aber dieser Mittelbau treibt Aufwand, wo er nicht erforderlich ist, und bräuchte Material, wo jetzt keins ist:

So wie ich die Sache sehe, tragen die beiden Stangen aus BS-30 (in halber Höhe) üüüberhaupt nichts mehr, wenn unten längs noch der fehlende Alu-Stab drin ist. Was sie halten müssen, ist die Zugkraft aus dem unteren Teil der Stütze und die Druckkraft aus dem oberen Teil. Diese Kräfte beanspruchen die BS-30-Stangen auf Biegung, und das ist in der Statik immer ungünstig. Den Druck im oberen Teil kannst du stattdessen prima ins lange Alu-Teil einleiten, wenn du die obere Strebe einfach noch 30mm höher setzt. Danach kann das ganze BS-30-Zeugs aus der Längswand verschwinden, denn die Zugkraft im unteren Teil kompensiert man am besten... mit der Zugraft aus dem unteren Teil von der Stütze gegenüber. Gegen diese Zugkraft wirkt derzeit nur die Biegesteifigkeit der langen BS-30-Stange, was ziemlich unvorteilhaft ist. Anstelle der vielen BS-30 längs würde ein Stück Seil quer (hier im Bild von rechts unten nach links oben, zur anderen Stütze hin) völlig ausreichen. 

Die U-Träger oben drin tragen genauso wenig (die würden erst dann Belastung erfahren, wenn die kurzen quer liegenen Alus schon kaputt wären, und wenn das passiert, halten sie auch nichts mehr).

An den Seitenstützen ist sicher nichts mehr zu verstärken dran, aber gegen seitliches Wegknicken würden sie etwas Hilfe brauchen. Dazu würde ich die jetzt parallel liegenden oberen Alus der Stütze trennen und so als "V" anordnen, dass sie sich außen beim Stempel treffen.


Gruß,
Harald