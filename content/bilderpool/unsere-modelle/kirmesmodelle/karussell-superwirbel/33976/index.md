---
layout: "image"
title: "Karussell SuperWirbel"
date: "2012-01-17T19:05:36"
picture: "karussellsuperwirbel6.jpg"
weight: "6"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- details/33976
imported:
- "2019"
_4images_image_id: "33976"
_4images_cat_id: "2514"
_4images_user_id: "1361"
_4images_image_date: "2012-01-17T19:05:36"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33976 -->
Steuerungseinheit mit Interface