---
layout: "image"
title: "Karussell SuperWirbel"
date: "2012-01-17T19:05:36"
picture: "karussellsuperwirbel1.jpg"
weight: "1"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- details/33971
imported:
- "2019"
_4images_image_id: "33971"
_4images_cat_id: "2514"
_4images_user_id: "1361"
_4images_image_date: "2012-01-17T19:05:36"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33971 -->
Kasse, Eingangsbereich, Ein- und Ausstiegsplattform