---
layout: "image"
title: "Hully Gully 2"
date: "2015-12-27T10:20:56"
picture: "000_0012.jpg"
weight: "3"
konstrukteure: 
- "Bernd Langer"
fotografen:
- "Bernd Langer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- details/42572
imported:
- "2019"
_4images_image_id: "42572"
_4images_cat_id: "3165"
_4images_user_id: "2496"
_4images_image_date: "2015-12-27T10:20:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42572 -->
