---
layout: "image"
title: "Move it - Entwurf"
date: "2003-12-23T20:27:52"
picture: "Move_it.jpg"
weight: "6"
konstrukteure: 
- " "
fotografen:
- " "
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guest"
license: "unknown"
legacy_id:
- details/2040
imported:
- "2019"
_4images_image_id: "2040"
_4images_cat_id: "124"
_4images_user_id: "-1"
_4images_image_date: "2003-12-23T20:27:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2040 -->
Ein erster Entwurf für das Modell des Karussells "Move it".