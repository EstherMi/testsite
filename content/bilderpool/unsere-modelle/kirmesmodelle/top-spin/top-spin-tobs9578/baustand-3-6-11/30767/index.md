---
layout: "image"
title: "Das gesamte Modell"
date: "2011-06-03T10:16:44"
picture: "topspin1_2.jpg"
weight: "2"
konstrukteure: 
- "Tobias Horst"
fotografen:
- "Tobias Horst"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- details/30767
imported:
- "2019"
_4images_image_id: "30767"
_4images_cat_id: "2295"
_4images_user_id: "1007"
_4images_image_date: "2011-06-03T10:16:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30767 -->
Recht das Kassenhäuschen, vor der Gondel das Wasserbecken, an den Stützen links und rechts je 2 Leds für das Stroboskop, hinten rechts die Stroboskopsteuerung und hiten das Top Spin Schild