---
layout: "image"
title: "Der Wagen mit Sitzen"
date: "2010-11-14T17:58:25"
picture: "topspin1_2.jpg"
weight: "4"
konstrukteure: 
- "Tobias Horst"
fotografen:
- "Tobias Horst (tobs9578)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- details/29236
imported:
- "2019"
_4images_image_id: "29236"
_4images_cat_id: "2125"
_4images_user_id: "1007"
_4images_image_date: "2010-11-14T17:58:25"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29236 -->
Im Fordergrund zu sehen sind die Einsteigebrücken :). Dazu später mehr.