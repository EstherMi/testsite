---
layout: "image"
title: "Gondel"
date: "2010-11-13T21:55:48"
picture: "topspin2.jpg"
weight: "11"
konstrukteure: 
- "Tobias Horst"
fotografen:
- "Tobias Horst (tobs9578)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- details/29232
imported:
- "2019"
_4images_image_id: "29232"
_4images_cat_id: "2125"
_4images_user_id: "1007"
_4images_image_date: "2010-11-13T21:55:48"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29232 -->
Der Fahrgastträger in der vorläufigen Version ohne Sitze