---
layout: "image"
title: "Die Technik"
date: "2010-11-13T21:55:48"
picture: "topspin4.jpg"
weight: "13"
konstrukteure: 
- "Tobias Horst"
fotografen:
- "Tobias Horst (tobs9578)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- details/29234
imported:
- "2019"
_4images_image_id: "29234"
_4images_cat_id: "2125"
_4images_user_id: "1007"
_4images_image_date: "2010-11-13T21:55:48"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29234 -->
Auf dem Bild ist oben in der Mitte ein kleiner weißer Kasten zu erkennen.Das ist der Kompressor der 1,2 Bar druck liefert.
Rechts im Bild ist der Drucktank.
Links befinden sich die beiden Magnetventile für die beiden Zylinder an den Tragarmen.