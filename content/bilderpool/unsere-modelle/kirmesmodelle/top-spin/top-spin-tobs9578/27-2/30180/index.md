---
layout: "image"
title: "Das Wasserbecken von der Seite"
date: "2011-02-28T19:23:02"
picture: "top3_2.jpg"
weight: "4"
konstrukteure: 
- "tobs9578"
fotografen:
- "tobs9578"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- details/30180
imported:
- "2019"
_4images_image_id: "30180"
_4images_cat_id: "2230"
_4images_user_id: "1007"
_4images_image_date: "2011-02-28T19:23:02"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30180 -->
