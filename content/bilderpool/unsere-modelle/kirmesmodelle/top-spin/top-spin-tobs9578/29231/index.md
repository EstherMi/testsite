---
layout: "image"
title: "Top Spin"
date: "2010-11-13T21:55:47"
picture: "topspin1.jpg"
weight: "10"
konstrukteure: 
- "Tobias Horst"
fotografen:
- "Tobias Horst (tobs9578)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- details/29231
imported:
- "2019"
_4images_image_id: "29231"
_4images_cat_id: "2125"
_4images_user_id: "1007"
_4images_image_date: "2010-11-13T21:55:47"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29231 -->
Erste Versuche eines Top Spin