---
layout: "image"
title: "Untere Ansicht - Seite"
date: "2007-11-11T08:33:13"
picture: "Bild_14.jpg"
weight: "17"
konstrukteure: 
- "speedy68"
fotografen:
- "speedy68"
keywords: ["speedy68", "Kirmesmodell"]
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/12631
imported:
- "2019"
_4images_image_id: "12631"
_4images_cat_id: "2124"
_4images_user_id: "409"
_4images_image_date: "2007-11-11T08:33:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12631 -->
