---
layout: "image"
title: "Antrieb Version 12"
date: "2008-03-02T01:49:47"
picture: "Bild_30.jpg"
weight: "3"
konstrukteure: 
- "speedy68"
fotografen:
- "speedy68"
keywords: ["Drehkranz", "speedy", "68", "Kirmesmodell"]
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/13814
imported:
- "2019"
_4images_image_id: "13814"
_4images_cat_id: "2124"
_4images_user_id: "409"
_4images_image_date: "2008-03-02T01:49:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13814 -->
