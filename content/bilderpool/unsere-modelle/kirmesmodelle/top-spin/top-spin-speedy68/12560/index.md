---
layout: "image"
title: "Drehkranz"
date: "2007-11-08T23:07:49"
picture: "Bild_6.jpg"
weight: "32"
konstrukteure: 
- "speedy68"
fotografen:
- "speedy68"
keywords: ["Drehkranz", "speedy", "68", "Kirmesmodell"]
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/12560
imported:
- "2019"
_4images_image_id: "12560"
_4images_cat_id: "2124"
_4images_user_id: "409"
_4images_image_date: "2007-11-08T23:07:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12560 -->
habe Kunststoffachsen genommen - ist stabil. Aber um es noch stabiler zu machen könnte man es noch mit durchgehenden Metallachsen ausführen.