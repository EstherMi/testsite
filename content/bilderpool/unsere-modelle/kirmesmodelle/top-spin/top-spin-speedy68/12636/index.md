---
layout: "image"
title: "Gondel offen"
date: "2007-11-11T08:33:13"
picture: "Bild_19.jpg"
weight: "22"
konstrukteure: 
- "speedy68"
fotografen:
- "speedy68"
keywords: ["speedy", "68", "Kirmesmodell", "Gondel"]
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/12636
imported:
- "2019"
_4images_image_id: "12636"
_4images_cat_id: "2124"
_4images_user_id: "409"
_4images_image_date: "2007-11-11T08:33:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12636 -->
Ist während der Fahrt verriegelt - geht nur auf, wenn Brücke zum Ein/Aussteigen unten ist