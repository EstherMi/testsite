---
layout: "comment"
hidden: true
title: "4559"
date: "2007-11-12T14:01:43"
uploadBy:
- "ThanksForTheFish"
license: "unknown"
imported:
- "2019"
---
Das geht wahrscheinlich nicht, da beim Wiedereinhaken in die Kette Differenzen auftreten und dann hakelt's.
Ich hatte das irgendwann mal bei einem Riesenrad ausprobiert und kam auf die selbe Idee wie du, aber genau an dem Hakeln ist dieser Versuch gescheitert. :(