---
layout: "image"
title: "Antrieb Version 12"
date: "2008-03-02T01:49:48"
picture: "Bild_32.jpg"
weight: "5"
konstrukteure: 
- "speedy68"
fotografen:
- "speedy68"
keywords: ["Antrieb", "Power", "speedy", "68", "Kirmesmodell"]
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/13816
imported:
- "2019"
_4images_image_id: "13816"
_4images_cat_id: "2124"
_4images_user_id: "409"
_4images_image_date: "2008-03-02T01:49:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13816 -->
Der Antrieb wurde jetzt noch um 2 weitere Motoren verstärkt, da die unteren Zahnräder unbedingt auch angetrieben werden mussten.