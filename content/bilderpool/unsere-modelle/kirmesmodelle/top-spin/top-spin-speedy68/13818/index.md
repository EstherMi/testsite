---
layout: "image"
title: "Antrieb mit Antriebskette"
date: "2008-03-02T08:55:36"
picture: "Bild_34.jpg"
weight: "7"
konstrukteure: 
- "speedy68"
fotografen:
- "speedy68"
keywords: ["Antrieb", "Power", "speedy", "68", "Kirmesmodell"]
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/13818
imported:
- "2019"
_4images_image_id: "13818"
_4images_cat_id: "2124"
_4images_user_id: "409"
_4images_image_date: "2008-03-02T08:55:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13818 -->
Eine Antriebskette besteht aus 470 Teilen und ist knapp 1,2 Meter lang