---
layout: "image"
title: "Detail-Mittelstation"
date: "2011-02-12T19:36:34"
picture: "updowntowerversion11.jpg"
weight: "11"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- details/29928
imported:
- "2019"
_4images_image_id: "29928"
_4images_cat_id: "2209"
_4images_user_id: "1030"
_4images_image_date: "2011-02-12T19:36:34"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29928 -->
Die Mittelstation: Die Zahnräder drehen die Stangen zum Abkoppeln heraus.