---
layout: "image"
title: "Zug in Station"
date: "2011-02-12T19:36:34"
picture: "updowntowerversion06.jpg"
weight: "6"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: ["modding"]
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- details/29923
imported:
- "2019"
_4images_image_id: "29923"
_4images_cat_id: "2209"
_4images_user_id: "1030"
_4images_image_date: "2011-02-12T19:36:34"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29923 -->
Der Zug mit Besatzung steht zur Abfahrt bereit! Mitte-Oben sind die etwas bearbeiteten Bausteine 15*30*5, in die der Hochzieher einhakt.