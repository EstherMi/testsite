---
layout: "image"
title: "Detail-Mittelstation2"
date: "2011-02-12T19:36:34"
picture: "updowntowerversion12.jpg"
weight: "12"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- details/29929
imported:
- "2019"
_4images_image_id: "29929"
_4images_cat_id: "2209"
_4images_user_id: "1030"
_4images_image_date: "2011-02-12T19:36:34"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29929 -->
Die Mittelstation: Die Zahnräder drehen die Stangen zum Abkoppeln heraus.