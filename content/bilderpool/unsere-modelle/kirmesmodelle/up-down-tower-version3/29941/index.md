---
layout: "image"
title: "Zug"
date: "2011-02-12T19:36:34"
picture: "updowntowerversion24.jpg"
weight: "24"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- details/29941
imported:
- "2019"
_4images_image_id: "29941"
_4images_cat_id: "2209"
_4images_user_id: "1030"
_4images_image_date: "2011-02-12T19:36:34"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29941 -->
Der Zug besteht aus drei Wagen, die über Gelenke verbunden sind. Die Räder sind mit 7,5° + 7,5° (Oberen) und mit 30° (Unteren) befestigt.