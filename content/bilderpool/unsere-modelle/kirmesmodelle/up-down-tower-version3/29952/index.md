---
layout: "image"
title: "Grundverkabelung"
date: "2011-02-12T19:36:34"
picture: "updowntowerversion35.jpg"
weight: "35"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- details/29952
imported:
- "2019"
_4images_image_id: "29952"
_4images_cat_id: "2209"
_4images_user_id: "1030"
_4images_image_date: "2011-02-12T19:36:34"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29952 -->
Hier seht ihr die dicksten Kabelstränge, die an die entsprechenden Positionen verlegt sind. Rechts ist das mechanische Relais zur Steuerung der Power Motoren in der Spitze, die für hoch/runter zuständig sind.