---
layout: "image"
title: "Detail-Seilwinde"
date: "2011-02-12T19:36:34"
picture: "updowntowerversion19.jpg"
weight: "19"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- details/29936
imported:
- "2019"
_4images_image_id: "29936"
_4images_cat_id: "2209"
_4images_user_id: "1030"
_4images_image_date: "2011-02-12T19:36:34"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29936 -->
Die Verkleidungsplatten können leicht für Reperaturen abgenommen werden.