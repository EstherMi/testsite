---
layout: "image"
title: "Detail-Abdockstation-Mitte"
date: "2011-02-12T19:36:34"
picture: "updowntowerversion16.jpg"
weight: "16"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- details/29933
imported:
- "2019"
_4images_image_id: "29933"
_4images_cat_id: "2209"
_4images_user_id: "1030"
_4images_image_date: "2011-02-12T19:36:34"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29933 -->
Der Motor (50:1) dreht die Zahnräder, diese drehen die Schnecken und diese fahren die Stangen zum abkoppeln herraus.