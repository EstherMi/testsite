---
layout: "image"
title: "Detail-Hochzieher2"
date: "2011-02-12T19:36:34"
picture: "updowntowerversion18.jpg"
weight: "18"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- details/29935
imported:
- "2019"
_4images_image_id: "29935"
_4images_cat_id: "2209"
_4images_user_id: "1030"
_4images_image_date: "2011-02-12T19:36:34"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29935 -->
Die Unterseite: Die Räder sind mit 15° (Unteren) und mit 30° (Oberen) befestigt.