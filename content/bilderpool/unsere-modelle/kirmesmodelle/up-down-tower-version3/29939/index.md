---
layout: "image"
title: "Detail-Seilwinde4"
date: "2011-02-12T19:36:34"
picture: "updowntowerversion22.jpg"
weight: "22"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- details/29939
imported:
- "2019"
_4images_image_id: "29939"
_4images_cat_id: "2209"
_4images_user_id: "1030"
_4images_image_date: "2011-02-12T19:36:34"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29939 -->
Hier seht ihr die Untersetzung (30:1) auf die Seilwinde. Aufgewickelt werden die Seile auf stabile Klopapierrollen, da diese einen Umfang von 13cm haben und sich die Seile sehr gleichmäßig aufwickeln lassen.