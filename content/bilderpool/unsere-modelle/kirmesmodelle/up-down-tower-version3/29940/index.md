---
layout: "image"
title: "Antrieb"
date: "2011-02-12T19:36:34"
picture: "updowntowerversion23.jpg"
weight: "23"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- details/29940
imported:
- "2019"
_4images_image_id: "29940"
_4images_cat_id: "2209"
_4images_user_id: "1030"
_4images_image_date: "2011-02-12T19:36:34"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29940 -->
Die Motoren sind über Z10's verbunden und werden über das PC-Netzteil betrieben, da sie über 1A Strom brauchen.