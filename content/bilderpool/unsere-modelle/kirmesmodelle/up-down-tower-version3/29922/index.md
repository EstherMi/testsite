---
layout: "image"
title: "Station mit Ein- und Ausgang"
date: "2011-02-12T19:36:34"
picture: "updowntowerversion05.jpg"
weight: "5"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- details/29922
imported:
- "2019"
_4images_image_id: "29922"
_4images_cat_id: "2209"
_4images_user_id: "1030"
_4images_image_date: "2011-02-12T19:36:34"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29922 -->
Wird durch die schwarzen Adapter mit Haupt- und Nebenturm verbunden. Die Konstruktion ist EXTREM stabil und schluckt alleine über 4500 Teile!!