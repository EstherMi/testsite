---
layout: "image"
title: "Zug5"
date: "2011-02-12T19:36:34"
picture: "updowntowerversion28.jpg"
weight: "28"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- details/29945
imported:
- "2019"
_4images_image_id: "29945"
_4images_cat_id: "2209"
_4images_user_id: "1030"
_4images_image_date: "2011-02-12T19:36:34"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29945 -->
Noch einmal die Fischermans.