---
layout: "image"
title: "Gegengewicht"
date: "2008-11-28T23:04:42"
picture: "BILD0429.jpg"
weight: "9"
konstrukteure: 
- "Nico Klingel"
fotografen:
- "Nico Klingel"
keywords: ["Speed", "-", "Star"]
uploadBy: "geforce1994"
license: "unknown"
legacy_id:
- details/16527
imported:
- "2019"
_4images_image_id: "16527"
_4images_cat_id: "1493"
_4images_user_id: "871"
_4images_image_date: "2008-11-28T23:04:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16527 -->
Das gegengewicht ist ein motor vom Robo Mobile set