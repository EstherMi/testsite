---
layout: "image"
title: "Frontansicht des Giebels"
date: "2014-08-10T21:11:11"
picture: "IMG_0022.jpg"
weight: "2"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/39232
imported:
- "2019"
_4images_image_id: "39232"
_4images_cat_id: "2749"
_4images_user_id: "1359"
_4images_image_date: "2014-08-10T21:11:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39232 -->
statisch eigentlich ein Klassiker . die beiden Streben, die nach Oben laufen, üben Druck aus und "ziehen" den Unterzug nach oben