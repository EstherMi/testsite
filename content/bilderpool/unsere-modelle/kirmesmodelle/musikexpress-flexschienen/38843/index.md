---
layout: "image"
title: "Detail Flexschienenhalterung"
date: "2014-05-25T17:49:46"
picture: "IMG_0072.jpg"
weight: "17"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/38843
imported:
- "2019"
_4images_image_id: "38843"
_4images_cat_id: "2749"
_4images_user_id: "1359"
_4images_image_date: "2014-05-25T17:49:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38843 -->
hier wird die Flexschiene eingeklemmt, zwischen zwei 15er Platten. je nach erforderlicher Höhe, und ob es gerade Bergauf oder Bergab geht, finden sich dann in der jeweiligen Stütze mehrere 15er oder 30er Winkel, die Bahn wurde i.d.R. mit jew. einem 15er Winkel nach innen geneigt