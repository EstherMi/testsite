---
layout: "image"
title: "Seitenansicht Mittelkranz"
date: "2014-02-23T12:10:16"
picture: "IMG_0013.jpg"
weight: "31"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/38339
imported:
- "2019"
_4images_image_id: "38339"
_4images_cat_id: "2749"
_4images_user_id: "1359"
_4images_image_date: "2014-02-23T12:10:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38339 -->
