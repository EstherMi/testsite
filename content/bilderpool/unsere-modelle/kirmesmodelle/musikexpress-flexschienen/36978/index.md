---
layout: "image"
title: "Detail2"
date: "2013-05-26T12:21:41"
picture: "IMG_9835.jpg"
weight: "39"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/36978
imported:
- "2019"
_4images_image_id: "36978"
_4images_cat_id: "2749"
_4images_user_id: "1359"
_4images_image_date: "2013-05-26T12:21:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36978 -->
