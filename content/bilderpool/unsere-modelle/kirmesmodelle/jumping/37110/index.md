---
layout: "image"
title: "Jumping8689"
date: "2013-06-14T07:21:50"
picture: "IMG_8689.JPG"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/37110
imported:
- "2019"
_4images_image_id: "37110"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-06-14T07:21:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37110 -->
Der Schlitten von unten. Die vier Bauplatten 30x30 gleiten in den längs liegenden Alus, die den ganzen Wagen tragen. Darunter gibt es reichlich Gefrickel, um diese Bauplatten richtig zu positionieren und gegen Verrutschen zu sichern.