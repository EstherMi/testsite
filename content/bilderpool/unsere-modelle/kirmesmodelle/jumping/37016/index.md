---
layout: "image"
title: "Jumping8626"
date: "2013-06-02T10:19:14"
picture: "IMG_8626.JPG"
weight: "24"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/37016
imported:
- "2019"
_4images_image_id: "37016"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-06-02T10:19:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37016 -->
Das Aufrichten kommt in die letzte Phase: der vordere Hilfsmast drückt den Mast per Teleskopschub nach oben, während der Schlitten immer noch weiter nach hinten fährt.

Die Bodenplatten sind nur fürs Foto abgenommen worden. Die mechanischen Teile passen überall aneinander vorbei.

Der Schneckenantrieb verwendet zwei Winkel 60° als Ersatz für Schneckenmuttern. Bei Schneckenmuttern hätte man ja einen Zapfen entfernen müssen (das wäre ja Modding !?!). Wesentlicher Vorteil der Winkel 60° ist, dass man sie einfach aushängen und dann abziehen kann (siehe folgendes Bild).