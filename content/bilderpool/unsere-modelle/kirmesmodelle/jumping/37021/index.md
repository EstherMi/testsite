---
layout: "image"
title: "Jumping8664"
date: "2013-06-04T21:11:58"
picture: "IMG_8664.JPG"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/37021
imported:
- "2019"
_4images_image_id: "37021"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-06-04T21:11:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37021 -->
Genau genommen besteht das Fahrzeug aus ziemlich viel umbauter Luft und wenig Materie. Das ist eben den vielen Schwenk- und Verschiebewegen geschuldet. Als Ausgleich geht es im Mast umso haariger zu...