---
layout: "image"
title: "Jumping8668"
date: "2013-06-04T21:50:49"
picture: "IMG_8668.JPG"
weight: "18"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/37026
imported:
- "2019"
_4images_image_id: "37026"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-06-04T21:50:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37026 -->
So soll es mal aussehen, wenn die Gondeln fertig sind: mit mitdrehender Beleuchtung, die von oben über einen Schleifring mit Strom versorgt wird. Den Schleifring gibt es noch nicht, weswegen die Stelle gnädigerweise von der ft-Figur verdeckt wird.