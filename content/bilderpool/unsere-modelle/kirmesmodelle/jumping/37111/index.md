---
layout: "image"
title: "Jumping8698..jpg"
date: "2013-06-14T07:25:05"
picture: "IMG_8698.JPG"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/37111
imported:
- "2019"
_4images_image_id: "37111"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-06-14T07:25:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37111 -->
von unten. Die hintere Seilwinde (die Trommel gehört auf den BS30, der ganz rechts angeschnitten ist) ist federnd gelagert und wird bei Überlast abgeschaltet.