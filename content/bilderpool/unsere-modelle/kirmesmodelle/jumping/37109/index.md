---
layout: "image"
title: "Jumping8688.jpg"
date: "2013-06-14T07:19:07"
picture: "IMG_8688.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/37109
imported:
- "2019"
_4images_image_id: "37109"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-06-14T07:19:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37109 -->
Das Gelenk am Mastfuß. Die beiden Streben fixieren den Mast, wenn er aufgerichtet ist. Die Seilrolle stellt das untere Ende des Teleskopschubs dar, der nach links mit einem ft-Alu weiter geht. Der zugehörige Motor liegt unter dem Schlitten, auf dem der Mast verfahren wird.