---
layout: "image"
title: "Jumping8662"
date: "2013-06-04T21:03:22"
picture: "IMG_8662.JPG"
weight: "12"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/37020
imported:
- "2019"
_4images_image_id: "37020"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-06-04T21:03:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37020 -->
So, jetzt nochmal mit besseren Kameraeinstellungen geknipst. 

Unten liegt der Teleskop-Hilfsmast, hinten steht der kleine Hilfsmast. Davor und dahinter gibt es noch zwei verschiebbare Platten, die nachher als Abstützung für den Boden des Podiums gebraucht werden.