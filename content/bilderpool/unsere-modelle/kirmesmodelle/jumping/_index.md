---
layout: "overview"
title: "Jumping"
date: 2019-12-17T18:56:09+01:00
legacy_id:
- categories/2752
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2752 --> 
Ein Fahrgeschäft mit fünf rotierenden Fahrgastgondeln, die auf und ab geschwenkt werden und sich um einen zentralen Mast drehen.