---
layout: "image"
title: "Jumping8641"
date: "2013-06-02T10:38:07"
picture: "IMG_8641.JPG"
weight: "26"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/37018
imported:
- "2019"
_4images_image_id: "37018"
_4images_cat_id: "2752"
_4images_user_id: "4"
_4images_image_date: "2013-06-02T10:38:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37018 -->
Der Mast steht; auch die seitlichen Stützen sind an Ort und Stelle. Die Gondeln und restliche Teile des Podiums kommen vom zweiten Wagen hinzu.