---
layout: "image"
title: "Stecker im Interface"
date: "2008-08-05T13:42:50"
picture: "evolution25.jpg"
weight: "25"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/15019
imported:
- "2019"
_4images_image_id: "15019"
_4images_cat_id: "1367"
_4images_user_id: "636"
_4images_image_date: "2008-08-05T13:42:50"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15019 -->
Die Stecker sind beschriftet.