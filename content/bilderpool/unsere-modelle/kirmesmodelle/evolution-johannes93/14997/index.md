---
layout: "image"
title: "Kran"
date: "2008-08-05T13:42:50"
picture: "evolution03.jpg"
weight: "3"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/14997
imported:
- "2019"
_4images_image_id: "14997"
_4images_cat_id: "1367"
_4images_user_id: "636"
_4images_image_date: "2008-08-05T13:42:50"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14997 -->
