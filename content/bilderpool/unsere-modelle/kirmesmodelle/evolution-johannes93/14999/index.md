---
layout: "image"
title: "Transportwagen von vorne"
date: "2008-08-05T13:42:50"
picture: "evolution05.jpg"
weight: "5"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/14999
imported:
- "2019"
_4images_image_id: "14999"
_4images_cat_id: "1367"
_4images_user_id: "636"
_4images_image_date: "2008-08-05T13:42:50"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14999 -->
