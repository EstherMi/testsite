---
layout: "image"
title: "Verbundene Stützen"
date: "2008-08-05T13:42:50"
picture: "evolution17.jpg"
weight: "17"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/15011
imported:
- "2019"
_4images_image_id: "15011"
_4images_cat_id: "1367"
_4images_user_id: "636"
_4images_image_date: "2008-08-05T13:42:50"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15011 -->
