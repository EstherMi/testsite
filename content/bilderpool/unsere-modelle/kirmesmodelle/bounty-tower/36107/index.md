---
layout: "image"
title: "21 Bounty Tower"
date: "2012-11-17T20:18:58"
picture: "bountytower21.jpg"
weight: "21"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/36107
imported:
- "2019"
_4images_image_id: "36107"
_4images_cat_id: "2686"
_4images_user_id: "860"
_4images_image_date: "2012-11-17T20:18:58"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36107 -->
Das ist die Gondel. 

Das Original hat vier Stück mit 7*2 Sitzplätzen, ich musste es auf zwei Gondeln mit 4*1 Sitzplatz reduzieren. Aber trotzdem schön :)