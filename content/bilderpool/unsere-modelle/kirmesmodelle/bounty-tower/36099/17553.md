---
layout: "comment"
hidden: true
title: "17553"
date: "2012-11-18T23:34:34"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Wow, da trifft der Begriff "Seilmanagement" ja wirklich zu. Die Gewichtsverteilung kann man vielleicht noch was optimieren, aber ansonsten sieht das super aus.

Gruß,
Stefan