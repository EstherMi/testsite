---
layout: "image"
title: "23 Bounty Tower"
date: "2012-11-17T20:18:58"
picture: "bountytower23.jpg"
weight: "23"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/36109
imported:
- "2019"
_4images_image_id: "36109"
_4images_cat_id: "2686"
_4images_user_id: "860"
_4images_image_date: "2012-11-17T20:18:58"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36109 -->
Die Beleuchtung, wenn es mal dunkel wird.

Video: http://www.youtube.com/watch?v=vbnUDZFNeU4