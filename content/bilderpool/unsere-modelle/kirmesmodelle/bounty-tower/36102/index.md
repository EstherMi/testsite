---
layout: "image"
title: "16 Bounty Tower"
date: "2012-11-17T20:18:58"
picture: "bountytower16.jpg"
weight: "16"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/36102
imported:
- "2019"
_4images_image_id: "36102"
_4images_cat_id: "2686"
_4images_user_id: "860"
_4images_image_date: "2012-11-17T20:18:58"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36102 -->
Das ist der ganze mittlere Teil. Der Drehkranz mit den zwei Gondeln liegt tätsächsich nur auf 4 Rädern, an jeder Seite eins.