---
layout: "comment"
hidden: true
title: "17560"
date: "2012-11-19T18:23:34"
uploadBy:
- "Marten70"
license: "unknown"
imported:
- "2019"
---
Hallo Matthias,

Ich bin auch begeistert! 
Wass die Massereduzierung angeht schliesse ich mir bei den Anderen an. 

Könntest du anstatt der (bei mir schwer drehende) Drehkranzen nicht Zahnräder Z40/32 und Drehscheibe 60 benützen? Antrieb via (Rast-) Ritzel Z10 am Z32 und eine Minimot näher an der Drehkranz.

Aber: Tolles Modell!!

MfrGr Marten