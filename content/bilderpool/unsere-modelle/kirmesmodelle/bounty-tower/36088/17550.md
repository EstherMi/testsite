---
layout: "comment"
hidden: true
title: "17550"
date: "2012-11-18T16:47:10"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hallo Matthias,

ein sehr gelungenes Modell!

Tobs Vorschlägen möchte ich noch einen hinzufügen: Zumindest die (ver-)Biegung der beiden "Arme" nach unten kannst Du wahrscheinlich vermeiden, wenn Du die beiden Antriebsmotoren weiter nach innen setzt und den Antrieb der Gondeln über eine Kette überträgst.

Gruß, Dirk