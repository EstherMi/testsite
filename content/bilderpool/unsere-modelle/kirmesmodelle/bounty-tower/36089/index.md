---
layout: "image"
title: "03 Bounty Tower"
date: "2012-11-17T20:18:57"
picture: "bountytower03.jpg"
weight: "3"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/36089
imported:
- "2019"
_4images_image_id: "36089"
_4images_cat_id: "2686"
_4images_user_id: "860"
_4images_image_date: "2012-11-17T20:18:57"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36089 -->
Das ist ca. der Hälfte der maximalen Höhe.