---
layout: "image"
title: "Gondel während der Fahrt"
date: "2014-08-07T10:27:25"
picture: "DSC00049.jpg"
weight: "1"
konstrukteure: 
- "Pascal Jan"
fotografen:
- "Pascal Jan"
keywords: ["Looping", "Karussell", "Robo", "Pro", "TX", "Kirmes", "Volksfest", "Gondel", "Schleifkontakt", "bauFischertechnik"]
uploadBy: "bauFischertechnik"
license: "unknown"
legacy_id:
- details/39149
imported:
- "2019"
_4images_image_id: "39149"
_4images_cat_id: "2927"
_4images_user_id: "2086"
_4images_image_date: "2014-08-07T10:27:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39149 -->
Hier sieht man die Gondel während der Fahrt.