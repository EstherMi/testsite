---
layout: "comment"
hidden: true
title: "20697"
date: "2015-06-01T11:00:24"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Die 4mm Messing-Rohre habe ich mit einer kleinere Pfanne gebogen, weil es ein wenig  zuruck biegt.


Die Anschlusse sieht man hier :
http://www.ftcommunity.de/details.php?image_id=41082#col3

Sehr wichtig beim Aluminium-Profilen ist die Entfernung der Anodisierung an die Ecken zum Electrischen Kontakten ! 
Anodiseerlagen zijn elektrisch isolerend. Middels schuurpapier is deze anodiseer-laag effectief te verwijderen met blijvende goede geleiding voor de sleepcontacten.

Gruss,

Peter
Poederoyen NL