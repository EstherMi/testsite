---
layout: "image"
title: "Einbau der 6st Polyamid 3D-Druck  Innenzahnkranz    -oben"
date: "2015-05-27T10:38:12"
picture: "daroundtheworld37.jpg"
weight: "37"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/41080
imported:
- "2019"
_4images_image_id: "41080"
_4images_cat_id: "3080"
_4images_user_id: "22"
_4images_image_date: "2015-05-27T10:38:12"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41080 -->
-Detail   2st  4mm Messing Schleifringe