---
layout: "image"
title: "3D-Around-the-World   -Detail"
date: "2015-05-27T10:38:12"
picture: "daroundtheworld12.jpg"
weight: "12"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/41055
imported:
- "2019"
_4images_image_id: "41055"
_4images_cat_id: "3080"
_4images_user_id: "22"
_4images_image_date: "2015-05-27T10:38:12"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41055 -->
