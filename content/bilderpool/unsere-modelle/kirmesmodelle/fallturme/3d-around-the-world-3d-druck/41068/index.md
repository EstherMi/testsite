---
layout: "image"
title: "3D-Around-the-World mit einer 3D-Druck Innenzahnkranz   -Detail   2st  4mm Messing Schleifringe"
date: "2015-05-27T10:38:12"
picture: "daroundtheworld25.jpg"
weight: "25"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/41068
imported:
- "2019"
_4images_image_id: "41068"
_4images_cat_id: "3080"
_4images_user_id: "22"
_4images_image_date: "2015-05-27T10:38:12"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41068 -->
