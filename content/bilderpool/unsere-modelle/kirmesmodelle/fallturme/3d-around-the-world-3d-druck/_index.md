---
layout: "overview"
title: "3D-Around-the-World + 3D-Druck Innenzahnkranz"
date: 2019-12-17T18:53:32+01:00
legacy_id:
- categories/3080
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3080 --> 
Johannes Visser hat im ft:pedia-2015-1 ein 3D-Druck  Innenzahnkranz  Entwurfen.
Ich habe bei  Trinckle 3D GmbH  12st in grau Polyamid bestellt.   
6 Stuck für die "3D-Around-the-World" + 6 Stuck für andere Modellen.

