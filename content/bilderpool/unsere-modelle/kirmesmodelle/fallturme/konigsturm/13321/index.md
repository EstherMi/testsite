---
layout: "image"
title: "Königsturm Kronenansicht (1)"
date: "2008-01-13T22:29:28"
picture: "free_3.jpg"
weight: "4"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: ["FreeFallTower", "Großmodell", "Leds", "Robointerface", "Kirmesmodell", "Fallturm"]
uploadBy: "Sebo"
license: "unknown"
legacy_id:
- details/13321
imported:
- "2019"
_4images_image_id: "13321"
_4images_cat_id: "1210"
_4images_user_id: "650"
_4images_image_date: "2008-01-13T22:29:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13321 -->
Hier sieht man den Ursprung des Namens für diesen Turm im Detail.