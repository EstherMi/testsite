---
layout: "image"
title: "Königsturm Gesamtansicht (2)"
date: "2008-01-13T22:29:28"
picture: "free_5.jpg"
weight: "6"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: ["FreeFallTower", "Großmodell", "Leds", "Robointerface", "Kirmesmodell", "Fallturm"]
uploadBy: "Sebo"
license: "unknown"
legacy_id:
- details/13323
imported:
- "2019"
_4images_image_id: "13323"
_4images_cat_id: "1210"
_4images_user_id: "650"
_4images_image_date: "2008-01-13T22:29:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13323 -->
Der Königsturm strahlt im Dunkeln bei eingeschalteter Beleuchtung einen ganz anderen Reiz aus. 
Der Turm selbst ist mit weißen Leds im "Erdgeschoss" ausgestattet. Im "ersten, zweiten Geschoss" schmücken jeweils vier rote und grüne Leds das  symmetrisch, verstrebte Grundgerüst. Ganz oben sind ebenfalls zwei rote und grüne Miniglühlampen angebracht.
(Zugegeben, auf dem Bild man kann nicht besonders viel erkennen - dafür fallen die zahlreichen Lichter umso mehr ins Auge.)