---
layout: "image"
title: "Königturm Kronenansicht (2)"
date: "2008-01-13T22:29:28"
picture: "free_6.jpg"
weight: "7"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: ["FreeFallTower", "Großmodell", "Leds", "Robointerface", "Kirmesmodell", "Fallturm"]
uploadBy: "Sebo"
license: "unknown"
legacy_id:
- details/13324
imported:
- "2019"
_4images_image_id: "13324"
_4images_cat_id: "1210"
_4images_user_id: "650"
_4images_image_date: "2008-01-13T22:29:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13324 -->
Ein weiteres Higlight ist die sechszackige Krone, die Spitze des Turmes, bei Einbruch der Dunkelheit. Erst jetzt sieht man die zwölf gelben Leuchtdioden, welche in die Zacken eingebaut sind. Sie bringen die Krone zum Leuchten.
Den höchsten Punkt des Turmes verkörpert eine klare Glühlampe, die zugleich als Signalleuchte für Flugzeuge bei Nacht dient.