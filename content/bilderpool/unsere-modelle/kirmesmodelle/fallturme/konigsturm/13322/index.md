---
layout: "image"
title: "Königsturm Stationsansicht (1)"
date: "2008-01-13T22:29:28"
picture: "free_4.jpg"
weight: "5"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: ["FreeFallTower", "Großmodell", "Leds", "Robointerface", "Kirmesmodell", "Fallturm"]
uploadBy: "Sebo"
license: "unknown"
legacy_id:
- details/13322
imported:
- "2019"
_4images_image_id: "13322"
_4images_cat_id: "1210"
_4images_user_id: "650"
_4images_image_date: "2008-01-13T22:29:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13322 -->
Dies ist die Dockingstation bzw. die Plattform des Königsturmes. Gesichert durch ein Geländer können die Fahrgäste gefahrenlos ein-und aussteigen. 
Vorher muss jedoch jeder an dem freundlichem Kassenwärter vorbei und eine Fahrkarte lösen.