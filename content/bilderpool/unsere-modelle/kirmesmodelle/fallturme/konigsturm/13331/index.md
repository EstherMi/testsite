---
layout: "image"
title: "Königsturm Gondelansicht (1)"
date: "2008-01-14T22:48:29"
picture: "free_8.jpg"
weight: "1"
konstrukteure: 
- "Sebatian"
fotografen:
- "Sebastian"
keywords: ["FreeFallTower", "Großmodell", "Leds", "Robointerface", "Kirmesmodell", "Fallturm"]
uploadBy: "Sebo"
license: "unknown"
legacy_id:
- details/13331
imported:
- "2019"
_4images_image_id: "13331"
_4images_cat_id: "1210"
_4images_user_id: "650"
_4images_image_date: "2008-01-14T22:48:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13331 -->
Betrachtet man die Gondel in der Detailansicht, so fällt der Bügel, der die Fahrgäste sichert, auf. (Bei diesem Bild ist er gehoben.) Dieser wird automatisch beim Ankommmen und Verlassen der Station geöffnet und geschlossen. Die Motoren dafür sind unterhalb der Station befestigt. 
Die nach unten abstehenden Zapfen an der Gondel nehmen über die Kupplungen in der Plattform die Kraft auf auf geben diese über Zahnräder an die Bügel weiter.