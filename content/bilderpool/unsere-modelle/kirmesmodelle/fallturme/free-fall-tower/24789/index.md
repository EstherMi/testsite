---
layout: "image"
title: "Schaltpult"
date: "2009-08-14T21:37:26"
picture: "freefalltower4.jpg"
weight: "4"
konstrukteure: 
- "Armin Jacob"
fotografen:
- "Armin Jacob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "armin.jacob"
license: "unknown"
legacy_id:
- details/24789
imported:
- "2019"
_4images_image_id: "24789"
_4images_cat_id: "1705"
_4images_user_id: "988"
_4images_image_date: "2009-08-14T21:37:26"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24789 -->
Hier sieht man das Schaltpult. Unter den Lampen befinden sich Schalter zum Starten und dem Notaus mit akustischem Alarm.