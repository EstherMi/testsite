---
layout: "image"
title: "Gondel"
date: "2009-08-14T21:37:26"
picture: "freefalltower2.jpg"
weight: "2"
konstrukteure: 
- "Armin Jacob"
fotografen:
- "Armin Jacob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "armin.jacob"
license: "unknown"
legacy_id:
- details/24787
imported:
- "2019"
_4images_image_id: "24787"
_4images_cat_id: "1705"
_4images_user_id: "988"
_4images_image_date: "2009-08-14T21:37:26"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24787 -->
Hier noch mal die Gondel