---
layout: "image"
title: "Free Fall Tower, die Bremse"
date: "2003-06-01T17:18:29"
picture: "die_Bremse.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MaLie"
license: "unknown"
legacy_id:
- details/1168
imported:
- "2019"
_4images_image_id: "1168"
_4images_cat_id: "135"
_4images_user_id: "26"
_4images_image_date: "2003-06-01T17:18:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1168 -->
Näheres zum Prachtstück dieses Towers, die Bremse:
Die Gondel wird mit 3 Pneumatikzylindern gebremst.
Dazu sind 2 Kompressoren im Einsatz um die nötige Luft zu liefern.