---
layout: "image"
title: "Free Fall Tower"
date: "2003-08-11T17:18:54"
picture: "RIMG0073.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MaLie"
license: "unknown"
legacy_id:
- details/1323
imported:
- "2019"
_4images_image_id: "1323"
_4images_cat_id: "135"
_4images_user_id: "26"
_4images_image_date: "2003-08-11T17:18:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1323 -->
Beschreibung:  Ein paar Daten: 

- 1 Power Motor(50:1, rote Kappe) um alles hochzuziehen 
- Insgesamt 8 Pneumatikzylinder verbaut, die mit 3 Magnetventilen gesteuert werden 
- 1 Interface und Extension Module verbaut 
- 26(!!!) Lampen für die Beleuchtung verbaut, sowie 1 Highlights und 1 Lights 
- 16 Sitze mit Männchen verbaut 
- jede Menge Kabel verbaut 
- 2 Kompressoren verbaut (für die luft natürlich) 


Näheres zum Prachtstück dieses Towers, die Bremse: 
Die Gondel wird mit 3 Pneumatikzylindern gebremst. 
Dazu sind 2 Kompressoren im Einsatz um die nötige Luft zu liefern. 

Ablauf: 
Gondel wird hochgezogen --> Wartet oben 3 sec.(wegen der Aussicht) --> Dann Blitzt es kurz(soll den Blitz des Fotoapparat darstellen, ist beim echten nämlich auch so) --> Gondel saust runter(mit einem ECHTEN freien Fall) --> und wird kurz vor Ende gebremst --> fährt dann langsam in die Station zurück --> wartet dann 10 sec. In der Station(zum Ein- und Aussteigen) --> dann fängt wieder alles von vorne an.