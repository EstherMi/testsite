---
layout: "image"
title: "FreeFallTurm_Endansicht"
date: "2010-07-06T20:03:36"
picture: "freefallturm13.jpg"
weight: "13"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- details/27721
imported:
- "2019"
_4images_image_id: "27721"
_4images_cat_id: "1996"
_4images_user_id: "1030"
_4images_image_date: "2010-07-06T20:03:36"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27721 -->
Hier seht ihr eine Endansicht des Eingangbereiches. Die Ein- und Ausgangstüren sind Federn zurückschnappend gebaut. Auf der rechten Seite des Eingangs ist ein Kassenhäuschen intigriert.