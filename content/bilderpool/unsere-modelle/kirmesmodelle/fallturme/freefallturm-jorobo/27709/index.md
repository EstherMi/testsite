---
layout: "image"
title: "FreeFallTurm_Gesamtansicht"
date: "2010-07-06T20:03:35"
picture: "freefallturm01.jpg"
weight: "1"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- details/27709
imported:
- "2019"
_4images_image_id: "27709"
_4images_cat_id: "1996"
_4images_user_id: "1030"
_4images_image_date: "2010-07-06T20:03:35"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27709 -->
Hier ist eine Gesamtansicht meines neuen FreeFallTurms.
Größe: ca. 1,70m
Kapazität: 16 ft-Männchen
Steuerung: ROBO Interface, 2x Powermot (1:50), 2x Magnetventil, 3x Reedkontakt 
Als Beleuchtung: ca. 52 LEDs
Funktionsweise: Zuerst wird die Gondel hoch gezogen. Oben hakt sich die Gondel ab und fällt im "freien Fall" hinunter. Unten (15 cm vor dem Aufprall) wird die Gondel pneumatisch gebremst. Nun kommt der Wagen so schnell es geht wieder runter und hakt sich ein.
Es gibt auch einen Film:     http://www.youtube.com/watch?v=0csm0LIK-84