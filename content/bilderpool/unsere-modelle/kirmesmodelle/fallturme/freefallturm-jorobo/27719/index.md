---
layout: "image"
title: "FreeFallTurm_Bremse"
date: "2010-07-06T20:03:36"
picture: "freefallturm11.jpg"
weight: "11"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- details/27719
imported:
- "2019"
_4images_image_id: "27719"
_4images_cat_id: "1996"
_4images_user_id: "1030"
_4images_image_date: "2010-07-06T20:03:36"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27719 -->
Hier seht ihr einen Teil der Bremskonstruktion. Hierbei drücken 4 Pneumatikzylinder die beiden Bremsklappen auseinander.