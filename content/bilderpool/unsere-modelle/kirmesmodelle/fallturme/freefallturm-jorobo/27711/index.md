---
layout: "image"
title: "FreeFallTurm_LED-Befestigung"
date: "2010-07-06T20:03:35"
picture: "freefallturm03.jpg"
weight: "3"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- details/27711
imported:
- "2019"
_4images_image_id: "27711"
_4images_cat_id: "1996"
_4images_user_id: "1030"
_4images_image_date: "2010-07-06T20:03:35"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27711 -->
Hier seht ihr die Befestigung der LEDs. Hier für ist ein Draht so gebogen, dass er die LED fest hält und die Kante, worüber die Röllchen laufen, nicht überquert.