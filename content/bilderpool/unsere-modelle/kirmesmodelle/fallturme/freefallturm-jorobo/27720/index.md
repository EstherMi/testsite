---
layout: "image"
title: "FreeFallTurm_Bremse(2)"
date: "2010-07-06T20:03:36"
picture: "freefallturm12.jpg"
weight: "12"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- details/27720
imported:
- "2019"
_4images_image_id: "27720"
_4images_cat_id: "1996"
_4images_user_id: "1030"
_4images_image_date: "2010-07-06T20:03:36"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27720 -->
Hier ist der gebremste Passagierwagen zu sehen. Die grün, schwarzen Streifen sind Bremsbeläge aus Isoliierband.