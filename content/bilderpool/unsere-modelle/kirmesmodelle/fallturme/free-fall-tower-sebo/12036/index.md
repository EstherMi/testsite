---
layout: "image"
title: "Free Fall Tower Hauptansicht 1"
date: "2007-09-29T11:01:46"
picture: "DSCF1747_Free_1.jpg"
weight: "1"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: ["Freefalltower", "Free", "Fall", "Tower", "Led", "Gondel", "RoboInterface", "Extension"]
uploadBy: "Sebo"
license: "unknown"
legacy_id:
- details/12036
imported:
- "2019"
_4images_image_id: "12036"
_4images_cat_id: "1076"
_4images_user_id: "650"
_4images_image_date: "2007-09-29T11:01:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12036 -->
Auf diesem Bild sieht man einen kleinen Vorgeschmack auf das, was noch kommt, da er nocht nicht ganz fertig ist.
Trotzdem hier schon mal ein paar Eckdaten:
Höhe: über 1,5 m
Antrieb: ein P-Motor 1:8 ohne extra      
         Untersetzung
Bremse: pneumatische Scheibenbremse
 Anzahl Leuchten: bis jetzt 12 Leds
Steuerung: Robe Interface für Motoren, 
           Ventile und ein Summer;
           Extension für die Lampen
Ablauf: Man den Tower manuell steuern oder
        den automatischen Ablauf abspielen
        lassen.
        Bei dem auto. Ablauf soll die
        Gondel immer ganz hoch und zuerst 
        1/3 dann 2/3 und zuletzt ganz run-
        ter fallen. Zusätzlich leuchten 
        die Leds in bestimmten Reihen-
        folgen.