---
layout: "image"
title: "Free Fall Tower"
date: "2004-04-20T13:46:30"
picture: "Free_Fall_Tower_001F.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/2347
imported:
- "2019"
_4images_image_id: "2347"
_4images_cat_id: "219"
_4images_user_id: "104"
_4images_image_date: "2004-04-20T13:46:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2347 -->
Dieser hier geht knapp bis unter die Decke, läuft ohne Rechner nur mit ft-Elektronik gesteuert, wird angetrieben von 2 nicht-Power-Motoren und gebremst von zwei elektromagnetisch betätigten symmetrischen Doppelbackenbremsen.