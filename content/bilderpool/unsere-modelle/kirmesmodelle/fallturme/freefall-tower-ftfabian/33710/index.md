---
layout: "image"
title: "Bremse / the brake"
date: "2011-12-18T21:02:57"
picture: "ftfabian09.jpg"
weight: "10"
konstrukteure: 
- "Fabian Legl"
fotografen:
- "Fabian Legl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ftFabian"
license: "unknown"
legacy_id:
- details/33710
imported:
- "2019"
_4images_image_id: "33710"
_4images_cat_id: "2493"
_4images_user_id: "1393"
_4images_image_date: "2011-12-18T21:02:57"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33710 -->
Gesamtansicht der Bremse mit dem Powermotor, der die Seilrolle antreibt.

General view of the brake with the power motor which powers the rope roll.