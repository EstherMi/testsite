---
layout: "image"
title: "Kupplung eingekuppelt / the coupler coupled"
date: "2011-12-18T21:02:57"
picture: "ftfabian04.jpg"
weight: "5"
konstrukteure: 
- "Fabian Legl"
fotografen:
- "Fabian Legl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ftFabian"
license: "unknown"
legacy_id:
- details/33705
imported:
- "2019"
_4images_image_id: "33705"
_4images_cat_id: "2493"
_4images_user_id: "1393"
_4images_image_date: "2011-12-18T21:02:57"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33705 -->
Die Kupplung ist eingekuppelt. Ein kleiner Schwachpunkt ist, dass die Kupplungsscheiben nur aus einer Scheibe bestehen, was teilweise zu Durchrutschen der Stange führt.

The coupler is coupled. There is one weak point: the coupling discs are build by one disc. So the rod sometimes slips.