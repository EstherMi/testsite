---
layout: "image"
title: "Die Spitze / the top"
date: "2011-12-18T21:03:16"
picture: "ftfabian13.jpg"
weight: "14"
konstrukteure: 
- "Fabian Legl"
fotografen:
- "Fabian Legl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ftFabian"
license: "unknown"
legacy_id:
- details/33714
imported:
- "2019"
_4images_image_id: "33714"
_4images_cat_id: "2493"
_4images_user_id: "1393"
_4images_image_date: "2011-12-18T21:03:16"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33714 -->
Die Spitze mit Umlenkrollen für die Seile.

The top with the deflexion sheaves for the rope.