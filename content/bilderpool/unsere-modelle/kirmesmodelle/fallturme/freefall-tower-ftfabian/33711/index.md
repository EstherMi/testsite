---
layout: "image"
title: "Interfaces / interfaces"
date: "2011-12-18T21:02:57"
picture: "ftfabian10.jpg"
weight: "11"
konstrukteure: 
- "Fabian Legl"
fotografen:
- "Fabian Legl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ftFabian"
license: "unknown"
legacy_id:
- details/33711
imported:
- "2019"
_4images_image_id: "33711"
_4images_cat_id: "2493"
_4images_user_id: "1393"
_4images_image_date: "2011-12-18T21:02:57"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33711 -->
Der Kabelsalat bei dem Interface. Dahinter ist eine I/O Extension und ein Relais Baustein(steuert die 4 Lichter ganz oben am Turm)

The cables to the interface. Behind of that are a I/O Extension and a relay which controls the four lights on the top of the tower.