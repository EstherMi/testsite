---
layout: "image"
title: "Die Grundplatten / the base"
date: "2011-12-18T21:03:16"
picture: "ftfabian15.jpg"
weight: "16"
konstrukteure: 
- "Fabian Legl"
fotografen:
- "Fabian Legl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ftFabian"
license: "unknown"
legacy_id:
- details/33716
imported:
- "2019"
_4images_image_id: "33716"
_4images_cat_id: "2493"
_4images_user_id: "1393"
_4images_image_date: "2011-12-18T21:03:16"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33716 -->
Man sieht das Kassenhäuschen, der Anfang des Turmes und die Kupplung.

You can see the ticket office, the lower part of the tower, and the coupler.