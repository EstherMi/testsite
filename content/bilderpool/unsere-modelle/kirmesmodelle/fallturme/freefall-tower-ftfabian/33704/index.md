---
layout: "image"
title: "Kraftübertragung des Motors auf Kupplung / powering the coupler"
date: "2011-12-18T21:02:57"
picture: "ftfabian03.jpg"
weight: "4"
konstrukteure: 
- "Fabian Legl"
fotografen:
- "Fabian Legl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ftFabian"
license: "unknown"
legacy_id:
- details/33704
imported:
- "2019"
_4images_image_id: "33704"
_4images_cat_id: "2493"
_4images_user_id: "1393"
_4images_image_date: "2011-12-18T21:02:57"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33704 -->
Der Motor wurde nochmals untersetzt. Evtl. wäre eine noch stärkere Untersetzung von Vorteil.

A gear reduction is used after the motor. It could be even bigger.

