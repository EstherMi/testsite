---
layout: "image"
title: "Haltemechanismus / holding mechanism"
date: "2011-12-18T21:02:57"
picture: "ftfabian07.jpg"
weight: "8"
konstrukteure: 
- "Fabian Legl"
fotografen:
- "Fabian Legl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ftFabian"
license: "unknown"
legacy_id:
- details/33708
imported:
- "2019"
_4images_image_id: "33708"
_4images_cat_id: "2493"
_4images_user_id: "1393"
_4images_image_date: "2011-12-18T21:02:57"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33708 -->
Die Schnecke greift in das darunterliegende Zahnrad (leider nicht zu sehen) und blockiert somit die Seilrolle.

The screw stops the underlying cog wheel and thus blocks the rope roll.