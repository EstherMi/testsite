---
layout: "image"
title: "Gesamtansicht der Kupplung / general view of the coupler"
date: "2011-12-18T21:02:57"
picture: "ftfabian02.jpg"
weight: "3"
konstrukteure: 
- "Fabian Legl"
fotografen:
- "Fabian Legl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ftFabian"
license: "unknown"
legacy_id:
- details/33703
imported:
- "2019"
_4images_image_id: "33703"
_4images_cat_id: "2493"
_4images_user_id: "1393"
_4images_image_date: "2011-12-18T21:02:57"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33703 -->
Die Kupplung hat links einem 50:1 Powermotor (rote Kappe). Daneben sind zwei Minimotoren, welche die Schnecken der Kupplung bewegen. In der Mitte ist das Herzstück, die Kupplungsscheiben und die Seilrolle. Auf der rechten Seite befinden sich der Haltemechanismus und die Bremse mit einem 8:1 Powermotor (schwarze Kappe). Teile der Kupplung stammen nicht von mir, sondern wurden von anderen ftUsern übernommen.

The coupler has got a 50:1 power motor (red marking) on the left side. Then there are two mini motors, which move the screws of the coupler. In the middle there are the coupling discs and the rope roll. On the right side you can see the holding mechanism and the brake. Some parts of the coupler are taken from other ftUsers.