---
layout: "image"
title: "35 Bühne"
date: "2010-06-07T21:41:44"
picture: "freefalltower09.jpg"
weight: "24"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27414
imported:
- "2019"
_4images_image_id: "27414"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-07T21:41:44"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27414 -->
Wenn sie nach der Fahrt ausgefahren ist, können die Ft-Menschen über sie aussteigen.