---
layout: "image"
title: "46 Interface"
date: "2010-06-07T21:41:45"
picture: "freefalltower20.jpg"
weight: "35"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27425
imported:
- "2019"
_4images_image_id: "27425"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-07T21:41:45"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27425 -->
Unter dem Wartebereich ist die ganze Steuerung und natürlich auch die ganzen Kabel. 
Es sieht vielleicht ein bisschen kaotisch aus, wenn man aber das dicke Kabel wegnimmt dann verschwindet auch der Kabelsalat.