---
layout: "image"
title: "73 Wagen"
date: "2010-06-13T11:39:50"
picture: "freefallachterbahn05_2.jpg"
weight: "5"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27475
imported:
- "2019"
_4images_image_id: "27475"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-13T11:39:50"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27475 -->
von unten
gut zu sehen sind die 12 Räder