---
layout: "image"
title: "03 Unterer Teil"
date: "2010-06-04T10:54:32"
picture: "freefallachterbahn3.jpg"
weight: "72"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27366
imported:
- "2019"
_4images_image_id: "27366"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-04T10:54:32"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27366 -->

Der gesamte untere Teil. 
Vorne ist der Wartebereich, unten drunter das Interface. 
Der Ausgang ist auf der anderen Seite, also da, wo der Ft-Mensch steht.