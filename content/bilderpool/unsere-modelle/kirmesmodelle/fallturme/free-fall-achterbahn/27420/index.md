---
layout: "image"
title: "41 Tor"
date: "2010-06-07T21:41:45"
picture: "freefalltower15.jpg"
weight: "30"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27420
imported:
- "2019"
_4images_image_id: "27420"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-07T21:41:45"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27420 -->
Detailaufnahme