---
layout: "image"
title: "40 Tore"
date: "2010-06-07T21:41:45"
picture: "freefalltower14.jpg"
weight: "29"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27419
imported:
- "2019"
_4images_image_id: "27419"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-07T21:41:45"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27419 -->
von der anderen Seite. 
Ich habe hier zwei Taster hingebaut, damit sichergestellt ist, das beide Tore offen sind.