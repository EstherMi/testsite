---
layout: "image"
title: "12 Förderband"
date: "2010-06-05T13:59:45"
picture: "freefallachterbahn09.jpg"
weight: "54"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27376
imported:
- "2019"
_4images_image_id: "27376"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-05T13:59:45"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27376 -->
Rechts nochmal der Zylinder und die Federn, in der Mitte sieht man den Antrieb für das Förderband, das den Wagen in der Station hin und her schieben kann. Der Moter befindet sich auf der anderen Seite.