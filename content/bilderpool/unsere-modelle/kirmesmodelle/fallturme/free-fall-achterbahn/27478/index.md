---
layout: "image"
title: "77 das Wägelchen"
date: "2010-06-13T11:39:51"
picture: "freefallachterbahn08_2.jpg"
weight: "8"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27478
imported:
- "2019"
_4images_image_id: "27478"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-13T11:39:51"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27478 -->
Das Wägelchen zieht den eigentlichen Wagen nach oben