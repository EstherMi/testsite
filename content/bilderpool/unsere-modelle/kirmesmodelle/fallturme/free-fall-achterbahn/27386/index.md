---
layout: "image"
title: "22 Sperre"
date: "2010-06-05T13:59:46"
picture: "freefallachterbahn19.jpg"
weight: "64"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27386
imported:
- "2019"
_4images_image_id: "27386"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-05T13:59:46"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27386 -->
Das ist die Sperre aus einer anderen Perspektive. Der Minimotor treibt sie an. 
Auch zu sehen ist das dicke Kabel.