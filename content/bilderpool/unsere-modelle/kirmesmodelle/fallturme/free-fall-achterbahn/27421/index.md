---
layout: "image"
title: "42 Tore"
date: "2010-06-07T21:41:45"
picture: "freefalltower16.jpg"
weight: "31"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27421
imported:
- "2019"
_4images_image_id: "27421"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-07T21:41:45"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27421 -->
so sieht es untendrunter aus. Die zwei Ritzel an den Schnecken sind mit den Toren verbunden.
Die zwei Kabel kommen von den Tastern.