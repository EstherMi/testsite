---
layout: "image"
title: "11 Links/Federn"
date: "2010-06-05T13:59:45"
picture: "freefallachterbahn08.jpg"
weight: "53"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27375
imported:
- "2019"
_4images_image_id: "27375"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-05T13:59:45"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27375 -->
Nochmal von der anderen Seite (hinten). Unter den Federn ist übrigens noch ein Powermoter, der den hier zu sehenden Kompressor antreibt, der hat aber keine Funktion. Ursprünglich wollte ich das Loslassen des Wagens oben pneumatisch bauen, da ich aber kein Elektroventil habe und das eh nicht so recht geklappt hat, habe ich es dann gelassen.