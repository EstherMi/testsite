---
layout: "image"
title: "29 Schiene"
date: "2010-06-07T21:41:42"
picture: "freefalltower02.jpg"
weight: "17"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27407
imported:
- "2019"
_4images_image_id: "27407"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-07T21:41:42"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27407 -->
So ist die Schiene am Turm befestigt. Links sieht man auch das dicke Kabel.