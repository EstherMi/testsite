---
layout: "image"
title: "86 Einzelteile"
date: "2010-06-13T11:39:51"
picture: "freefallachterbahn15_2.jpg"
weight: "15"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27485
imported:
- "2019"
_4images_image_id: "27485"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-13T11:39:51"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27485 -->
Nochmal von der anderen Seite, wobei man das andere Bild auch einfach hätte drehen können ;-)
Die Schienen sind zum Teil gebogen, da sie so lange in der Kurve verbaut waren.