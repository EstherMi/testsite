---
layout: "image"
title: "75 Wagen"
date: "2010-06-13T11:39:50"
picture: "freefallachterbahn06_2.jpg"
weight: "6"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27476
imported:
- "2019"
_4images_image_id: "27476"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-13T11:39:50"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27476 -->
Der hintere Teil vom Wagen.
In die Stange (unten) hakt sich der Haken ein.
Der Magnet ist dafür da, den Wagen in der Station auf die richtige Position zu schieben.