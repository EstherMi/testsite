---
layout: "image"
title: "18 Station/Wagen"
date: "2010-06-05T13:59:45"
picture: "freefallachterbahn15.jpg"
weight: "60"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27382
imported:
- "2019"
_4images_image_id: "27382"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-05T13:59:45"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27382 -->
Der Wagen wird erst ein Stück nach vorne gefahren, dann kommt das Wägelchen runter und fährt bis zur Lichtschranke (weiter kommt es auch nicht, da es kein Schwung hat). Der Wagen wird dann noch weiter nach vorne gefahren, bis das Förderband ihn nicht mehr greift. Dann kommt der Wagen aufgrund seines Schwunges ein Stück zurück gefahren, drückt den Arm des Wägelchen nach oben und hackt sich dann ein. Dann sieht es so wie im Bild aus.