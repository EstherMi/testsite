---
layout: "image"
title: "45  Tore"
date: "2010-06-07T21:41:45"
picture: "freefalltower19.jpg"
weight: "34"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27424
imported:
- "2019"
_4images_image_id: "27424"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-07T21:41:45"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27424 -->
Hinten sieht man den Motor.