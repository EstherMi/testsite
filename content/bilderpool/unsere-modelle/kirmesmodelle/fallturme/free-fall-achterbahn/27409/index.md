---
layout: "image"
title: "30 kleiner Turm"
date: "2010-06-07T21:41:43"
picture: "freefalltower04.jpg"
weight: "19"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27409
imported:
- "2019"
_4images_image_id: "27409"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-07T21:41:43"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27409 -->
Der obere Teil. Die Kabel der Lampen sehen furchtbar verwurchtelt aus, das fällt in Echt aber kaum auf. ;-)