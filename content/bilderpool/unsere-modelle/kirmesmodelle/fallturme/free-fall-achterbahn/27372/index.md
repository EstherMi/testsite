---
layout: "image"
title: "09 links"
date: "2010-06-05T13:59:44"
picture: "freefallachterbahn05.jpg"
weight: "50"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27372
imported:
- "2019"
_4images_image_id: "27372"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-05T13:59:44"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27372 -->
der linke Teil und das Unterteil des großen Turms. Der Powermoter treibt den Wagen an, der nach oben fährt. 
Unter dem roten Gebilde ist übrigens eine Verteilerbox für die Lampen und Motoren, die nur in eine Richtung laufen.