---
layout: "image"
title: "16 Lichtschranke"
date: "2010-06-05T13:59:45"
picture: "freefallachterbahn13.jpg"
weight: "58"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27380
imported:
- "2019"
_4images_image_id: "27380"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-05T13:59:45"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27380 -->
Von der anderen Seite.