---
layout: "image"
title: "04 Unterer Teil"
date: "2010-06-04T10:54:32"
picture: "freefallachterbahn4.jpg"
weight: "73"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27367
imported:
- "2019"
_4images_image_id: "27367"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-04T10:54:32"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27367 -->
Das Ganze von oben. 

Die beiden Tore sind gerade offen, der Wagen steht allerdings zus weit hinten.

Die Treppe kommt übrigens von diesem Modell: http://www.ftcommunity.de/details.php?image_id=15406
Sie ist also nicht von mir,ich hoffe mal das geht in Ordnung ;-)