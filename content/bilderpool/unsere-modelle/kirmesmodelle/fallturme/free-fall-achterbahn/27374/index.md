---
layout: "image"
title: "11 Federn"
date: "2010-06-05T13:59:45"
picture: "freefallachterbahn07.jpg"
weight: "52"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27374
imported:
- "2019"
_4images_image_id: "27374"
_4images_cat_id: "1966"
_4images_user_id: "860"
_4images_image_date: "2010-06-05T13:59:45"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27374 -->
Die Belastung in der Kurve ist für die Menschen und für das Material eine hohe Belastung, deshalb habe ich hier Federn hingebaut, um die Kräfte etwas zu dämpfen. Ohne die Federn würde der Wagen die Kurve stark nach unten ziehen, wenn ervon oben runtergefallen kommt.