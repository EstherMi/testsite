---
layout: "image"
title: "Blick von oben"
date: "2010-02-14T14:01:02"
picture: "s07.jpg"
weight: "7"
konstrukteure: 
- "sebastian..."
fotografen:
- "Sebastian..."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sebastian..."
license: "unknown"
legacy_id:
- details/26407
imported:
- "2019"
_4images_image_id: "26407"
_4images_cat_id: "1862"
_4images_user_id: "999"
_4images_image_date: "2010-02-14T14:01:02"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26407 -->
Höhe 1.70M