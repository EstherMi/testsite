---
layout: "image"
title: "25K magnet"
date: "2010-03-29T19:03:26"
picture: "gyrodroptowerbauphasestandderdinge9.jpg"
weight: "9"
konstrukteure: 
- "Sebastian..."
fotografen:
- "Sebastian..."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sebastian..."
license: "unknown"
legacy_id:
- details/26848
imported:
- "2019"
_4images_image_id: "26848"
_4images_cat_id: "1920"
_4images_user_id: "999"
_4images_image_date: "2010-03-29T19:03:26"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26848 -->
Hier einer der 25 Kilo Magnete