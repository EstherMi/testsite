---
layout: "image"
title: "Umlenkrollen"
date: "2008-04-19T08:42:56"
picture: "powertower11.jpg"
weight: "11"
konstrukteure: 
- "Toto"
fotografen:
- "Toto"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Toto"
license: "unknown"
legacy_id:
- details/14300
imported:
- "2019"
_4images_image_id: "14300"
_4images_cat_id: "1323"
_4images_user_id: "762"
_4images_image_date: "2008-04-19T08:42:56"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14300 -->
Rolle von oben