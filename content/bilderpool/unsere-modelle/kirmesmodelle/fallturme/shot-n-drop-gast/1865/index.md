---
layout: "image"
title: "Shot´n Drop"
date: "2003-11-05T01:53:51"
picture: "9.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- details/1865
imported:
- "2019"
_4images_image_id: "1865"
_4images_cat_id: "199"
_4images_user_id: "64"
_4images_image_date: "2003-11-05T01:53:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1865 -->
Turmspitze mit Gondel.