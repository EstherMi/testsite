---
layout: "image"
title: "Turmspitze"
date: "2003-10-25T13:20:24"
picture: "Turmspitze.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- details/1843
imported:
- "2019"
_4images_image_id: "1843"
_4images_cat_id: "199"
_4images_user_id: "64"
_4images_image_date: "2003-10-25T13:20:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1843 -->
Turmspitze mit den Laufrollen für den Seilantrieb.