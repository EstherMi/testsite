---
layout: "image"
title: "Seil-Technik"
date: "2008-08-30T13:50:44"
picture: "Free-Fall-Tower_1010.jpg"
weight: "6"
konstrukteure: 
- "Marius Moosmann"
fotografen:
- "Marius Moosmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mari"
license: "unknown"
legacy_id:
- details/15131
imported:
- "2019"
_4images_image_id: "15131"
_4images_cat_id: "1385"
_4images_user_id: "189"
_4images_image_date: "2008-08-30T13:50:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15131 -->
Man sieht zwei Zahnräder. Das eine ist fest und wird von einem Powermotor betrieben. Das andere hingegen wird von einem Pneumatischen Zylinder vor und zurück bewegt. Beim Hochfahren drückt der Zylinder das bewegliche  Zahnrad, an dem auch die Seilwinde angebracht ist, an das fest angebrachte Zahnrad. bei freien Fall wird das bewegliche Zahnrad zurückgefahren
und die Seilwinde kann sich frei bewegen und das Seil abrollen lassen, damit bewegt sich die Gondel frei herab.