---
layout: "image"
title: "Freefalltower Gesamansicht"
date: "2009-10-25T14:30:19"
picture: "freefalltower2.jpg"
weight: "2"
konstrukteure: 
- "Tobias Horst"
fotografen:
- "Tobias Horst"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- details/25570
imported:
- "2019"
_4images_image_id: "25570"
_4images_cat_id: "1348"
_4images_user_id: "1007"
_4images_image_date: "2009-10-25T14:30:19"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25570 -->
Gesamthöhe des Freefalltower:1,60