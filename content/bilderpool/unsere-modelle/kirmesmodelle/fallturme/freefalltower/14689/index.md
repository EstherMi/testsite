---
layout: "image"
title: "Antrieb, Bremse, Notsromakku"
date: "2008-06-14T13:25:33"
picture: "freefalltower12.jpg"
weight: "16"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/14689
imported:
- "2019"
_4images_image_id: "14689"
_4images_cat_id: "1348"
_4images_user_id: "636"
_4images_image_date: "2008-06-14T13:25:33"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14689 -->
Der Antrieb erfolgt über einen Power Motor mit roter Kappe.