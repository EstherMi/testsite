---
layout: "image"
title: "Lichtsschranke unten"
date: "2008-06-14T13:25:33"
picture: "freefalltower17.jpg"
weight: "21"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/14694
imported:
- "2019"
_4images_image_id: "14694"
_4images_cat_id: "1348"
_4images_user_id: "636"
_4images_image_date: "2008-06-14T13:25:33"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14694 -->
Die Lichtschranke dient zum Stoppen der Gondel.