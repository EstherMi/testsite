---
layout: "image"
title: "Bedienfeld hinten"
date: "2008-06-14T13:25:32"
picture: "freefalltower10.jpg"
weight: "14"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/14687
imported:
- "2019"
_4images_image_id: "14687"
_4images_cat_id: "1348"
_4images_user_id: "636"
_4images_image_date: "2008-06-14T13:25:32"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14687 -->
Auf dem Bedienfeld befindet sich die Kransteuereung, der Aktivierungsschalter des Notstroms und das Schlüsselloch. Ohne Schlüssel kann das System weder aktiviert, noch deaktiviert werden.