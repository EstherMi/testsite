---
layout: "image"
title: "Technik meines Freefalltowers"
date: "2009-10-25T14:30:19"
picture: "freefalltower3.jpg"
weight: "3"
konstrukteure: 
- "Tobias Horst"
fotografen:
- "Tobias Horst"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- details/25571
imported:
- "2019"
_4images_image_id: "25571"
_4images_cat_id: "1348"
_4images_user_id: "1007"
_4images_image_date: "2009-10-25T14:30:19"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25571 -->
Nötig sind: 3 Pneumatikzylinder,1 Power Moter, 3 2/3Wege Magnetventile, 1 Robo Interface, 1 Kompressor mit einem 2. Luftspeicher