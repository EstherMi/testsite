---
layout: "image"
title: "Grundgerüst"
date: "2010-03-28T11:37:22"
picture: "freefalltower1.jpg"
weight: "5"
konstrukteure: 
- "tobs9578"
fotografen:
- "tobs9578"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- details/26822
imported:
- "2019"
_4images_image_id: "26822"
_4images_cat_id: "1917"
_4images_user_id: "1007"
_4images_image_date: "2010-03-28T11:37:22"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26822 -->
Das ist das Grundgerüst des Towers.Wollte mal nicht auf Bauplatten bauen.