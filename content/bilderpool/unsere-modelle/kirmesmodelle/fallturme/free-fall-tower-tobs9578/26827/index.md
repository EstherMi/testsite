---
layout: "image"
title: "Getriebe"
date: "2010-03-28T11:37:22"
picture: "freefalltower6.jpg"
weight: "11"
konstrukteure: 
- "tobs9578"
fotografen:
- "tobs9578"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- details/26827
imported:
- "2019"
_4images_image_id: "26827"
_4images_cat_id: "1917"
_4images_user_id: "1007"
_4images_image_date: "2010-03-28T11:37:22"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26827 -->
Hier sieht man das Getriebe mit der Kupplung die ich von Masked übernommen habe.