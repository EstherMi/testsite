---
layout: "image"
title: "Mechanik 2"
date: "2007-07-13T17:46:54"
picture: "freefalltower4.jpg"
weight: "11"
konstrukteure: 
- "Masked (Martin)"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/11051
imported:
- "2019"
_4images_image_id: "11051"
_4images_cat_id: "999"
_4images_user_id: "373"
_4images_image_date: "2007-07-13T17:46:54"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11051 -->
Wenn die Kupplung geöffnet werden soll, zieht der MiniMotor das Seil ein wenig an. Dadurch klinken sich die Zahnräder aus und die Gondel fällt im (fast) freien Fall. Wenn dann wieder gebremst werden soll schaltet sich erst der Power Motor wieder an und dann werden die Zahnräder wieder zusammengebracht. Das ist nicht ganz so ruppig und hoffentlich auch für die Fahrgäste mit etwas empfindlicherem Magen aushaltbar ;-) Ich hab auch noch eine alternative Kupplung gebaut, die funktioniert allerdings nicht so ganz...Bilder davon kommen hoffentlich auch noch