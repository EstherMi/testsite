---
layout: "image"
title: "Unterteil 2"
date: "2007-07-13T17:46:54"
picture: "freefalltower5.jpg"
weight: "12"
konstrukteure: 
- "Masked (Martin)"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/11052
imported:
- "2019"
_4images_image_id: "11052"
_4images_cat_id: "999"
_4images_user_id: "373"
_4images_image_date: "2007-07-13T17:46:54"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11052 -->
Nochmal die Plattform, man erkennt die Steuerungstaster. Mit dem rechten Schiebeschalter schaltet man die Turmbeleuchtung durch die 4 LEDs an (Siehe Bild weiter hinten).