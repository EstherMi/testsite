---
layout: "image"
title: "Oben"
date: "2007-07-13T17:46:54"
picture: "freefalltower6.jpg"
weight: "13"
konstrukteure: 
- "Masked (Martin)"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/11053
imported:
- "2019"
_4images_image_id: "11053"
_4images_cat_id: "999"
_4images_user_id: "373"
_4images_image_date: "2007-07-13T17:46:54"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11053 -->
Die Gondel ist mit 2 Seilen fixiert, da sie mit einem zu sehr durch- bzw. schiefhängt.
Die Lampen sind nur Atrappen, ich hatte keine Lust mehr die Kabel zu verlegen. Falls ich den Turm auf die Convention mitbringe mach ich das natürlich noch.