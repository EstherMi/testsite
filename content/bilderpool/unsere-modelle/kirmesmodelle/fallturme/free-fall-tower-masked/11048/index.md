---
layout: "image"
title: "Übersicht"
date: "2007-07-13T17:46:54"
picture: "freefalltower1.jpg"
weight: "8"
konstrukteure: 
- "Masked (Martin)"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/11048
imported:
- "2019"
_4images_image_id: "11048"
_4images_cat_id: "999"
_4images_user_id: "373"
_4images_image_date: "2007-07-13T17:46:54"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11048 -->
Nach langer, langer Zeit, viel Fluchen und noch viel mehr Bastelei bin ich endlich soweit fertig.
Es ist ein FreeFallTower geworden.
1,55 m hoch
Steuerung über Intelligent Interface (nein, ich habe kein RoboInterface)
5 Taster, 6 Lampen,  2 Motoren, ein Reedkontakt
1 MiniMotor hat das ganze leider nicht überlebt

Folgender Programmablauf:
Prüfen, ob die Gondel ganz unten ist -> wenn nein, runterfahren -> auf Start-Taster warten ->Gondel hochziehen bis oben angekommen -> 4 Sekunden Wartezeit -> Kupplung öffnen -> sobald Gondel an Reedkontakt (ungefähr auf halber Höhe) vorbei, Kupplung wieder schließen -> Rest der Strecke mit Motorkraft runterlassen -> Ende