---
layout: "image"
title: "FT-Shot 'n Drop"
date: "2007-06-18T21:56:52"
picture: "FT-_Shot_n_Drop_004.jpg"
weight: "38"
konstrukteure: 
- "Peter Damen (Poederoyen Holland)"
fotografen:
- "Peter Damen (Poederoyen Holland)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/10886
imported:
- "2019"
_4images_image_id: "10886"
_4images_cat_id: "1214"
_4images_user_id: "22"
_4images_image_date: "2007-06-18T21:56:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10886 -->
Pneumatik Shot 'n Drop mit CNY-70 & Laufrolle zum Zylinder-Positionierung.