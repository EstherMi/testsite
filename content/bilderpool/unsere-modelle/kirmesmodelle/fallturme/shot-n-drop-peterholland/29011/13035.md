---
layout: "comment"
hidden: true
title: "13035"
date: "2010-12-24T18:43:39"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Hallo Sebastian, 

Mit an 2 Seiten 10 st Magneten : 
http://www.supermagnete.de/Q-15-15-08-N 
Q-15-15-08-N 
15 x 15 x 8 mm 
Gewicht 14 g 
vernickelt (Ni-Cu-Ni) 
Magnetisierung: N42 
Haftkraft: ca. 7,6 kg/st 

habe ich die Länge beider standard Alu-Baumark-Profilen beim experimentieren minimalisiert.

Grüss,

Peter
Poederoyen NL