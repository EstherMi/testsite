---
layout: "image"
title: "05 Detail"
date: "2010-07-14T22:09:10"
picture: "achterbahnwagen5.jpg"
weight: "7"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27755
imported:
- "2019"
_4images_image_id: "27755"
_4images_cat_id: "1999"
_4images_user_id: "860"
_4images_image_date: "2010-07-14T22:09:10"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27755 -->
Der vordere Wagen hat weiße Räder, um ihn leichter als diesen zu erkennen ;-)
Die zwei Zapfen in der Mitte können in eine Kette greifen, damit der Wagen in der Station bewegt werden kann.