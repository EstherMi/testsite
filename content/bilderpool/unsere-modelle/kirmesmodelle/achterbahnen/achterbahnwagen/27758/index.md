---
layout: "image"
title: "08 Bügel"
date: "2010-07-14T22:09:10"
picture: "achterbahnwagen8.jpg"
weight: "10"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27758
imported:
- "2019"
_4images_image_id: "27758"
_4images_cat_id: "1999"
_4images_user_id: "860"
_4images_image_date: "2010-07-14T22:09:10"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27758 -->
Die Sicherheitsbügel der ft-Menschen. Das Bild beschreibt alles :-)