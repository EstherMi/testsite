---
layout: "image"
title: "06 Seite"
date: "2010-07-14T22:09:10"
picture: "achterbahnwagen6.jpg"
weight: "8"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27756
imported:
- "2019"
_4images_image_id: "27756"
_4images_cat_id: "1999"
_4images_user_id: "860"
_4images_image_date: "2010-07-14T22:09:10"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27756 -->
Eine schöne Befestigung der Räder, aber hier liegt auch das Problem. Aufgrund der vielen Teile können sich die Räder bei hoher Geschwindigkeit leicht verschieben.