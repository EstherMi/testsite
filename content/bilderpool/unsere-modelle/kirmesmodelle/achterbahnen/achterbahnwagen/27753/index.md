---
layout: "image"
title: "03 hinten"
date: "2010-07-14T22:09:10"
picture: "achterbahnwagen3.jpg"
weight: "5"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27753
imported:
- "2019"
_4images_image_id: "27753"
_4images_cat_id: "1999"
_4images_user_id: "860"
_4images_image_date: "2010-07-14T22:09:10"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27753 -->
Der hintere Wagen hat hier eine Vorrichtung, mit der sich das ganze Gespann auch senkrecht nach oben ziehen lässt.