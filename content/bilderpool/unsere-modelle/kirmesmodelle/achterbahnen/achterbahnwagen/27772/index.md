---
layout: "image"
title: "09 neuer Wagen"
date: "2010-07-20T15:10:41"
picture: "achterbahnwagen1_2.jpg"
weight: "1"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27772
imported:
- "2019"
_4images_image_id: "27772"
_4images_cat_id: "1999"
_4images_user_id: "860"
_4images_image_date: "2010-07-20T15:10:41"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27772 -->
Der hintere ist der neue Wagen. Er ist deutlich stabiler als der erste und schafft auch einen Looping. Am oberen Teil hat sich nichts geändert, lediglich die Befestigung der Räder.