---
layout: "image"
title: "Neues Schaltpult"
date: "2006-12-18T08:17:32"
picture: "schaltpult1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- details/7956
imported:
- "2019"
_4images_image_id: "7956"
_4images_cat_id: "569"
_4images_user_id: "430"
_4images_image_date: "2006-12-18T08:17:32"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7956 -->
Das ist eine Skizze vom neuem Schaltpult was ich demnächst bauen werde. Die Lampen und die Drucktaster sind fast plan mit der Oberfläche. Während hingegen die Leucht-Druck-Taster etwas vertieft sind.