---
layout: "comment"
hidden: true
title: "13642"
date: "2011-02-19T19:31:03"
uploadBy:
- "Lurchi"
license: "unknown"
imported:
- "2019"
---
Hallo,

ich finde das Wägelchen mit den schwarzen Rädern am schönsten.
Eine sehr gute Idee finde ich die Verwendung der seitlich eingebauten Aufnahmestreifen 60 zur Versteifung. Bisher kannte ich diese Teile ausschließlich zum Einsortieren von Statikteilen.

Gruß

Lurchi