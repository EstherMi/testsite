---
layout: "image"
title: "Achterbahn-Wagen Weiß 1"
date: "2011-02-19T19:14:04"
picture: "grossehoellenachterbahn1.jpg"
weight: "1"
konstrukteure: 
- "Jonas Rupp"
fotografen:
- "Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- details/30085
imported:
- "2019"
_4images_image_id: "30085"
_4images_cat_id: "2217"
_4images_user_id: "1030"
_4images_image_date: "2011-02-19T19:14:04"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30085 -->
Hier seht ihr drei verschiedene Achterbahn-Wagen meiner neuen "Höllen-Achterbahn" ;-)
Sie unterscheiden sich in den verschieden Farben der Räder (Rot, Schwarz, Weiß). Details folgen später.

Welche Version gefällt euch am besten??
(Bitte posten, Danke)

Die Grundidee stammt von Stefan Falk, doch diese Wagen sind noch etwas verbessert!