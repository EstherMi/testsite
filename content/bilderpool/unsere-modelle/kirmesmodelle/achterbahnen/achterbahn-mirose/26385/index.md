---
layout: "image"
title: "Die Fahrt beginnt…"
date: "2010-02-13T22:21:37"
picture: "Hochschaubahn_17.jpg"
weight: "25"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/26385
imported:
- "2019"
_4images_image_id: "26385"
_4images_cat_id: "1879"
_4images_user_id: "765"
_4images_image_date: "2010-02-13T22:21:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26385 -->
Auf dem Foto bewegt sich der Wagen NICHT (= technisches Stilleben)
Hier sieht man die beiden S-Riegel 6 seitlich an den Schienen, die den Wagen für das Foto anhalten.