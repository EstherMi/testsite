---
layout: "image"
title: "Wagen Detail obere Rollen"
date: "2010-02-14T20:52:56"
picture: "Hochschaubahn_42.jpg"
weight: "2"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/26438
imported:
- "2019"
_4images_image_id: "26438"
_4images_cat_id: "1879"
_4images_user_id: "765"
_4images_image_date: "2010-02-14T20:52:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26438 -->
Wenn man genau schaut, sieht man, daß der linke obere Winkelstein 15° sich 2 mm nach oben verschoben hat, trotz Sicherung!
Was man alles bei seinen eigenen Fotos entdeckt, wenn sie veröffentlicht sind...