---
layout: "image"
title: "Wagen Fahrgestell"
date: "2010-02-14T20:52:56"
picture: "Hochschaubahn_41.jpg"
weight: "1"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/26437
imported:
- "2019"
_4images_image_id: "26437"
_4images_cat_id: "1879"
_4images_user_id: "765"
_4images_image_date: "2010-02-14T20:52:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26437 -->
Detailansicht von vorne auf einem Extragleisstück. Die Spurweite beträgt 65 mm.
Schön sieht man, wie das Gleisstück von den Rädern umschlossen wird.