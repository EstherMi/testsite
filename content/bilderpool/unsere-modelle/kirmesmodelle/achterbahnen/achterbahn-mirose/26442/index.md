---
layout: "image"
title: "Wagen von unten"
date: "2010-02-14T20:52:56"
picture: "Hochschaubahn_46.jpg"
weight: "6"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/26442
imported:
- "2019"
_4images_image_id: "26442"
_4images_cat_id: "1879"
_4images_user_id: "765"
_4images_image_date: "2010-02-14T20:52:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26442 -->
