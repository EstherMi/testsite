---
layout: "image"
title: "Am Ende des Aufzugs"
date: "2010-02-13T22:21:37"
picture: "Hochschaubahn_16.jpg"
weight: "24"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/26384
imported:
- "2019"
_4images_image_id: "26384"
_4images_cat_id: "1879"
_4images_user_id: "765"
_4images_image_date: "2010-02-13T22:21:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26384 -->
noch wird der Wagen von Aufzug bewegt, es geht aber schon etwas bergab