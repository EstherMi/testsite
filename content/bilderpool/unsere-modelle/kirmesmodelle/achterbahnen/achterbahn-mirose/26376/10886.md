---
layout: "comment"
hidden: true
title: "10886"
date: "2010-02-14T17:46:06"
uploadBy:
- "mirose"
license: "unknown"
imported:
- "2019"
---
Hallo Peter,

schön, daß Dir mein Modell gefällt.
Die Laufrollen sind selbstgemacht (Kunststoffstab, 20mm Durchmesser von Conrad)
Ich habe leider keine bessere Lösung mit ft-Bauteilen gefunden, die entsprechend kompakt ist. Wichtig war mir, daß die Rollen das Gleis so umschließen, daß der Wagen nicht abheben kann. Noch bin ich mit dem Wagen nicht 100%ig zufrieden, da sich die Winkelsteine immer wieder verschieben.

Ich freue mich schon auf die heurige ft-Ausstellung in E.-B. 

Viele Grüße

Mirose