---
layout: "image"
title: "Supercat Aufzug 2"
date: "2008-01-19T17:53:54"
picture: "sc4.jpg"
weight: "27"
konstrukteure: 
- "Limit (Marius)"
fotografen:
- "Limit (Marius)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- details/13349
imported:
- "2019"
_4images_image_id: "13349"
_4images_cat_id: "1215"
_4images_user_id: "430"
_4images_image_date: "2008-01-19T17:53:54"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13349 -->
So sieht die die Kabine inkl. Wagen von unten aus.
Die zwei Zahnräder sind für den Reibrad antrieb zuständig.
