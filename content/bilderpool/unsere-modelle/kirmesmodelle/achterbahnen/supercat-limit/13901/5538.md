---
layout: "comment"
hidden: true
title: "5538"
date: "2008-03-14T07:15:53"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Hallo Marius,

Du hattest 4 Bilder hochgeladen, davon aber 2+2 mit gleichem Inhalt und gleichem Dateinamen (supercat1 und supercat2). Das musste schief gehen, und jetzt sind nur zwei Bilder angekommen (und zwar diejenigen, bei denen Bild und Text vertauscht sind?).


Gruß,
Harald