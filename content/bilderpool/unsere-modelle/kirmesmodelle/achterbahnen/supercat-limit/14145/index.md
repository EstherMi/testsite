---
layout: "image"
title: "Supercat Gesamtansicht"
date: "2008-03-30T20:29:25"
picture: "sc1_2.jpg"
weight: "7"
konstrukteure: 
- "Limit (Marius)"
fotografen:
- "Limit (Marius)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- details/14145
imported:
- "2019"
_4images_image_id: "14145"
_4images_cat_id: "1215"
_4images_user_id: "430"
_4images_image_date: "2008-03-30T20:29:25"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14145 -->
Hier die aktuelle Gesamtansicht von der Achterbahn. ich habe den oberen Teil auf eine neue Stützkonstruktion gebaut. Das war auch nötig, denn seitdem ich eine Platte mehr verwendet habe hat das gar nicht mehr geklappt.