---
layout: "image"
title: "Supercat neue Wippe (Draufsicht)"
date: "2008-02-09T22:49:05"
picture: "sc01.jpg"
weight: "12"
konstrukteure: 
- "Limit (Marius)"
fotografen:
- "Limit (Marius)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- details/13623
imported:
- "2019"
_4images_image_id: "13623"
_4images_cat_id: "1215"
_4images_user_id: "430"
_4images_image_date: "2008-02-09T22:49:05"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13623 -->
Hier sieht man die neue Version von der Wippe von oben.
Zwar wackelt es noch an einigen Stellen aber es funktioniert größtenteils.