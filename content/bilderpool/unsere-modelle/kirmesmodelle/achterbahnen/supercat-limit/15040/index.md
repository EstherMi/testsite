---
layout: "image"
title: "Supercat - Steuerpult 2"
date: "2008-08-10T13:58:44"
picture: "sc1_5.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- details/15040
imported:
- "2019"
_4images_image_id: "15040"
_4images_cat_id: "1215"
_4images_user_id: "430"
_4images_image_date: "2008-08-10T13:58:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15040 -->
Hier eine doch recht verschwommene Aufnahme vom Steuerpult. Ich hab hier mal alle Lichter eingeschalten.