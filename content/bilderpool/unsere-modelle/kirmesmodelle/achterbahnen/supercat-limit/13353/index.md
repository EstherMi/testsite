---
layout: "image"
title: "Supercat Wippe"
date: "2008-01-19T17:53:54"
picture: "sc8.jpg"
weight: "31"
konstrukteure: 
- "Limit (Marius)"
fotografen:
- "Limit (Marius)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- details/13353
imported:
- "2019"
_4images_image_id: "13353"
_4images_cat_id: "1215"
_4images_user_id: "430"
_4images_image_date: "2008-01-19T17:53:54"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13353 -->
Und so sieht aktuell die Wippe aus. Sie wird aber nochmal überarbeitet :)
Auch nett an der Wippe ist, das der Wagen während des Kippvorgangs gehalten wird.