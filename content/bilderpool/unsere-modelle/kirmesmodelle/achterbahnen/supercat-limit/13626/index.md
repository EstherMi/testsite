---
layout: "image"
title: "Supercat neue Wippe"
date: "2008-02-09T22:49:06"
picture: "sc04.jpg"
weight: "15"
konstrukteure: 
- "Limit (Marius)"
fotografen:
- "Limit (Marius)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- details/13626
imported:
- "2019"
_4images_image_id: "13626"
_4images_cat_id: "1215"
_4images_user_id: "430"
_4images_image_date: "2008-02-09T22:49:06"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13626 -->
Die Wippe ist sogar so steil, dass der Wagen nach vorne kippt.
Zum Glück hab ich die Anti-Entgleisungsräder eingebaut :)