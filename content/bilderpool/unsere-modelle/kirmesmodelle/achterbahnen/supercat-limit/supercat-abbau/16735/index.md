---
layout: "image"
title: "Supercat Abbau - Überblick"
date: "2008-12-24T12:16:41"
picture: "supercatabbau25.jpg"
weight: "25"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- details/16735
imported:
- "2019"
_4images_image_id: "16735"
_4images_cat_id: "1513"
_4images_user_id: "430"
_4images_image_date: "2008-12-24T12:16:41"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16735 -->
Der komplett obere Teil ist nun zerlegt. Unten stehen noch ein paar Stützen und die Reste der Schiene in der Station.