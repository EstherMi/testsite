---
layout: "image"
title: "Supercat Abbau - Reste des Aufzugs"
date: "2008-12-24T12:16:41"
picture: "supercatabbau33.jpg"
weight: "33"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- details/16743
imported:
- "2019"
_4images_image_id: "16743"
_4images_cat_id: "1513"
_4images_user_id: "430"
_4images_image_date: "2008-12-24T12:16:41"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16743 -->
Nun wurde auch die letzte Stütze demontiert.