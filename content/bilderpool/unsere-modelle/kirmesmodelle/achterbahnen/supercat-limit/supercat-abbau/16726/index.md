---
layout: "image"
title: "Supercat Abbau - Technik der Gates"
date: "2008-12-24T12:16:40"
picture: "supercatabbau16.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- details/16726
imported:
- "2019"
_4images_image_id: "16726"
_4images_cat_id: "1513"
_4images_user_id: "430"
_4images_image_date: "2008-12-24T12:16:40"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16726 -->
Hier sieht man noch mal sehr schön die Zahnräder der Gates. Hinten, etwas schlechter zu sehen, das Impulsrad, mit dem die Endlagen "erzählt" wurden.