---
layout: "image"
title: "Supercat Abbau -  Überblick 2"
date: "2008-12-24T12:16:41"
picture: "supercatabbau26.jpg"
weight: "26"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- details/16736
imported:
- "2019"
_4images_image_id: "16736"
_4images_cat_id: "1513"
_4images_user_id: "430"
_4images_image_date: "2008-12-24T12:16:41"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16736 -->
Hinten stehen nur noch ein paar Stützen.