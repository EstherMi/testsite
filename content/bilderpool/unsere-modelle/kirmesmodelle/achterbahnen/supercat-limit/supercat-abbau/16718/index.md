---
layout: "image"
title: "Supercat Abbau - Station mit Dach"
date: "2008-12-24T12:16:40"
picture: "supercatabbau08.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- details/16718
imported:
- "2019"
_4images_image_id: "16718"
_4images_cat_id: "1513"
_4images_user_id: "430"
_4images_image_date: "2008-12-24T12:16:40"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16718 -->
Das war die Station zum Be- und Entladen des Zuges.