---
layout: "image"
title: "Supercat Abbau - Zwischen durch mal eine Übersicht"
date: "2008-12-24T12:16:41"
picture: "supercatabbau18.jpg"
weight: "18"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- details/16728
imported:
- "2019"
_4images_image_id: "16728"
_4images_cat_id: "1513"
_4images_user_id: "430"
_4images_image_date: "2008-12-24T12:16:41"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16728 -->
Die obere Welle sowie die dazugehörigen Stützen sind bereits verschwunden. Auch die Wippe existiert nicht mehr in der Form.