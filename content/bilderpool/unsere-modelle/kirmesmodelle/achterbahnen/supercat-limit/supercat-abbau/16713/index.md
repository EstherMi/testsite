---
layout: "image"
title: "Supercat Abbau - Wippe im nichts"
date: "2008-12-24T12:16:40"
picture: "supercatabbau03.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- details/16713
imported:
- "2019"
_4images_image_id: "16713"
_4images_cat_id: "1513"
_4images_user_id: "430"
_4images_image_date: "2008-12-24T12:16:40"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16713 -->
Ein letztes Foto von der Wippe