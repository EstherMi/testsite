---
layout: "image"
title: "Supercat Abbau - Leere Ecke"
date: "2008-12-24T12:16:40"
picture: "supercatabbau07.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- details/16717
imported:
- "2019"
_4images_image_id: "16717"
_4images_cat_id: "1513"
_4images_user_id: "430"
_4images_image_date: "2008-12-24T12:16:40"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16717 -->
Unten wird langsam der Blick auf die Schiene frei.