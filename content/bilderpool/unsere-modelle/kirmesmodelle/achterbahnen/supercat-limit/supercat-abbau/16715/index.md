---
layout: "image"
title: "Supercat Abbau - Entfernen erster Stützen"
date: "2008-12-24T12:16:40"
picture: "supercatabbau05.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- details/16715
imported:
- "2019"
_4images_image_id: "16715"
_4images_cat_id: "1513"
_4images_user_id: "430"
_4images_image_date: "2008-12-24T12:16:40"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16715 -->
Das ist / war die Stützkonstruktion, um die oben das Seil der Wippe geführt wurde.