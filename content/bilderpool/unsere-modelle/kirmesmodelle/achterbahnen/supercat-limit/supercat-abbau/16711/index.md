---
layout: "image"
title: "Supercat Abbau - Abnehmen der Welle"
date: "2008-12-24T12:16:39"
picture: "supercatabbau01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- details/16711
imported:
- "2019"
_4images_image_id: "16711"
_4images_cat_id: "1513"
_4images_user_id: "430"
_4images_image_date: "2008-12-24T12:16:39"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16711 -->
Als erstes wurde die Welle, durch die der Wagen rückwärts gefahren ist, abgenommen und in ihre Einzelteile zerlegt