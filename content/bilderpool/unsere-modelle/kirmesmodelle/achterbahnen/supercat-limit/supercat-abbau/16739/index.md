---
layout: "image"
title: "Supercat Abbau - Flaschenzug Gegenstück oben"
date: "2008-12-24T12:16:41"
picture: "supercatabbau29.jpg"
weight: "29"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- details/16739
imported:
- "2019"
_4images_image_id: "16739"
_4images_cat_id: "1513"
_4images_user_id: "430"
_4images_image_date: "2008-12-24T12:16:41"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16739 -->
Das Segment war das obere Gegenstück im Aufzugschacht.