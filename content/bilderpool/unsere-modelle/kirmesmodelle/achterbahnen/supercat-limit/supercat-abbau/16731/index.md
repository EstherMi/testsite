---
layout: "image"
title: "Supercat Abbau - Drei Arbeiter"
date: "2008-12-24T12:16:41"
picture: "supercatabbau21.jpg"
weight: "21"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- details/16731
imported:
- "2019"
_4images_image_id: "16731"
_4images_cat_id: "1513"
_4images_user_id: "430"
_4images_image_date: "2008-12-24T12:16:41"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16731 -->
Machen Pause auf den noch stehenden Aufzugturm.