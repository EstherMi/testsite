---
layout: "image"
title: "Supercat Abbau - Flaschenzug"
date: "2008-12-24T12:16:41"
picture: "supercatabbau20.jpg"
weight: "20"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- details/16730
imported:
- "2019"
_4images_image_id: "16730"
_4images_cat_id: "1513"
_4images_user_id: "430"
_4images_image_date: "2008-12-24T12:16:41"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16730 -->
Der groß angelegte Flaschenzug, konnte die Kabine mit dem Wagen (Immerhin über 400 Gramm) mühelos heben.