---
layout: "image"
title: "Supercat Abbau - Einreisen der Station"
date: "2008-12-24T12:16:40"
picture: "supercatabbau12.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- details/16722
imported:
- "2019"
_4images_image_id: "16722"
_4images_cat_id: "1513"
_4images_user_id: "430"
_4images_image_date: "2008-12-24T12:16:40"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16722 -->
Auch die Station wurde in ihre Einzelteile zerlegt.