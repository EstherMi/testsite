---
layout: "image"
title: "Supercat Aufzug Kabine"
date: "2008-01-19T17:53:54"
picture: "sc3.jpg"
weight: "26"
konstrukteure: 
- "Limit (Marius)"
fotografen:
- "Limit (Marius)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- details/13348
imported:
- "2019"
_4images_image_id: "13348"
_4images_cat_id: "1215"
_4images_user_id: "430"
_4images_image_date: "2008-01-19T17:53:54"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13348 -->
Hier sieht man auch den neuen Wagen, der gerade in der Kabine eingefahren ist.
