---
layout: "image"
title: "Wagen auf der Wippe"
date: "2008-01-24T21:53:21"
picture: "supercat2.jpg"
weight: "23"
konstrukteure: 
- "Limit (Marius)"
fotografen:
- "Limit (Marius)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- details/13381
imported:
- "2019"
_4images_image_id: "13381"
_4images_cat_id: "1215"
_4images_user_id: "430"
_4images_image_date: "2008-01-24T21:53:21"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13381 -->
Der Wagen wurde gesperrt und die Wippe neigt sich