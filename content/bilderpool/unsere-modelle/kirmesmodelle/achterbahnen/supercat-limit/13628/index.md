---
layout: "image"
title: "Supercat Wagen auf der Wippe"
date: "2008-02-09T22:49:06"
picture: "sc06.jpg"
weight: "17"
konstrukteure: 
- "Limit (Marius)"
fotografen:
- "Limit (Marius)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- details/13628
imported:
- "2019"
_4images_image_id: "13628"
_4images_cat_id: "1215"
_4images_user_id: "430"
_4images_image_date: "2008-02-09T22:49:06"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13628 -->
Dank den Bügeln, kann man auf dem starken Gefälle ruhig mal den Bügel loslassen :)