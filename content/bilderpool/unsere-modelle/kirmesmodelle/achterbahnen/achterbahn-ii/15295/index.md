---
layout: "image"
title: "Gesamtansicht"
date: "2008-09-20T19:20:46"
picture: "achterbahn01.jpg"
weight: "1"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/15295
imported:
- "2019"
_4images_image_id: "15295"
_4images_cat_id: "1397"
_4images_user_id: "636"
_4images_image_date: "2008-09-20T19:20:46"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15295 -->
