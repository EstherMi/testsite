---
layout: "image"
title: "Wagen unter der Schiene"
date: "2008-09-20T19:20:48"
picture: "achterbahn48.jpg"
weight: "48"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/15342
imported:
- "2019"
_4images_image_id: "15342"
_4images_cat_id: "1397"
_4images_user_id: "636"
_4images_image_date: "2008-09-20T19:20:48"
_4images_image_order: "48"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15342 -->
Auch unter der Schiene ist der Wage problemlos einsetzbar.