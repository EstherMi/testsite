---
layout: "overview"
title: "Achterbahn II"
date: 2019-12-17T18:54:09+01:00
legacy_id:
- categories/1397
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1397 --> 
von Johannes Westphal[br][br]Die Achterbahn II ist meine Zweite Achterbahn aus Fischertechnik. Sie hat ein komplett neues Schienen- und Stützensystem, die Wagen können nun nicht mehr entgleisen. Außerdem hat sie viele weitere Verbesserungen...