---
layout: "image"
title: "Kurve II"
date: "2008-09-20T19:20:47"
picture: "achterbahn23.jpg"
weight: "23"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/15317
imported:
- "2019"
_4images_image_id: "15317"
_4images_cat_id: "1397"
_4images_user_id: "636"
_4images_image_date: "2008-09-20T19:20:47"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15317 -->
