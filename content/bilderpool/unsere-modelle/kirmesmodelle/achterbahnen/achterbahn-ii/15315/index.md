---
layout: "image"
title: "Kurve I"
date: "2008-09-20T19:20:47"
picture: "achterbahn21.jpg"
weight: "21"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/15315
imported:
- "2019"
_4images_image_id: "15315"
_4images_cat_id: "1397"
_4images_user_id: "636"
_4images_image_date: "2008-09-20T19:20:47"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15315 -->
In Kurve I, dem Firstdrop, erreicht der Wagen seine Höchstgeschwindigkeit.