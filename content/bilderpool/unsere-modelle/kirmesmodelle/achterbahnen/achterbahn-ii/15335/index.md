---
layout: "image"
title: "Wagen von unten"
date: "2008-09-20T19:20:48"
picture: "achterbahn41.jpg"
weight: "41"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/15335
imported:
- "2019"
_4images_image_id: "15335"
_4images_cat_id: "1397"
_4images_user_id: "636"
_4images_image_date: "2008-09-20T19:20:48"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15335 -->
