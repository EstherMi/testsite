---
layout: "image"
title: "Steuerpult"
date: "2008-09-20T19:20:47"
picture: "achterbahn08.jpg"
weight: "8"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/15302
imported:
- "2019"
_4images_image_id: "15302"
_4images_cat_id: "1397"
_4images_user_id: "636"
_4images_image_date: "2008-09-20T19:20:47"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15302 -->
Am Steuerpult kann der Wagen gestartet, bzw. gestoppt werden. Außerdem kann man zwischen dem automatischen und manuellen Betriebsmodus wechseln. Die Zwei Lampen zeigen den Status der Bahn an.