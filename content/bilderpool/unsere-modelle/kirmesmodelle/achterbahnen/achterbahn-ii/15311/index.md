---
layout: "image"
title: "Höchster Punkt"
date: "2008-09-20T19:20:47"
picture: "achterbahn17.jpg"
weight: "17"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/15311
imported:
- "2019"
_4images_image_id: "15311"
_4images_cat_id: "1397"
_4images_user_id: "636"
_4images_image_date: "2008-09-20T19:20:47"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15311 -->
