---
layout: "image"
title: "Gesamtansicht"
date: "2008-09-20T19:20:46"
picture: "achterbahn05.jpg"
weight: "5"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/15299
imported:
- "2019"
_4images_image_id: "15299"
_4images_cat_id: "1397"
_4images_user_id: "636"
_4images_image_date: "2008-09-20T19:20:46"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15299 -->
