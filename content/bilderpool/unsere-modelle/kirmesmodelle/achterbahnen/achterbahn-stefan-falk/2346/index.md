---
layout: "image"
title: "Achterbahn"
date: "2004-04-20T13:46:30"
picture: "Achterbahn_007F.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/2346
imported:
- "2019"
_4images_image_id: "2346"
_4images_cat_id: "218"
_4images_user_id: "104"
_4images_image_date: "2004-04-20T13:46:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2346 -->
