---
layout: "image"
title: "Rechter Turm - Bahnhof"
date: "2009-01-17T13:23:40"
picture: "freefallachterbahnbeimbau09.jpg"
weight: "9"
konstrukteure: 
- "speedy68"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/17049
imported:
- "2019"
_4images_image_id: "17049"
_4images_cat_id: "1531"
_4images_user_id: "409"
_4images_image_date: "2009-01-17T13:23:40"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17049 -->
Abgesehen von dem Kabelsalat, wird es hier noch ganz schön Eng. Es sollen links noch 3 ROBO I/O Extensionen hin. Habe einfacherweise die Kabel nicht beschriftet und muss dann halt später alles "durchpiepsen".