---
layout: "image"
title: "Mittelteil"
date: "2009-01-17T14:45:22"
picture: "freefallachterbahnbeimbau12.jpg"
weight: "12"
konstrukteure: 
- "speedy68"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/17052
imported:
- "2019"
_4images_image_id: "17052"
_4images_cat_id: "1531"
_4images_user_id: "409"
_4images_image_date: "2009-01-17T14:45:22"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17052 -->
Rechts kommt der linke Turm mit dem Looping hin und links der rechte Turm mit dem Bahnhof (hätte es besser von der anderen Seite Fotografieren sollen. Auch hier wollte ich zuerst keine Alus verwenden, aber es ist immer ein Kompromiss zwischen Schönheit, Stabilität und freier Sicht. Soll heißen man könnte alles aus Statik bauen, aber dann sieht man "nichts" mehr von der Bahn.