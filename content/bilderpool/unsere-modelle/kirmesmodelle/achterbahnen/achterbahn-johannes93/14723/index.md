---
layout: "image"
title: "Systemschalter"
date: "2008-06-21T18:28:23"
picture: "achterbahn05.jpg"
weight: "5"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/14723
imported:
- "2019"
_4images_image_id: "14723"
_4images_cat_id: "1349"
_4images_user_id: "636"
_4images_image_date: "2008-06-21T18:28:23"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14723 -->
Mit dem Systemschalter wird das System ein-, und ausgeschaltet.