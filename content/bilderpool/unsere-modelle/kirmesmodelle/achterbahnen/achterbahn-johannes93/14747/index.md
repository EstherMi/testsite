---
layout: "image"
title: "Kurve"
date: "2008-06-21T18:28:24"
picture: "achterbahn29.jpg"
weight: "29"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/14747
imported:
- "2019"
_4images_image_id: "14747"
_4images_cat_id: "1349"
_4images_user_id: "636"
_4images_image_date: "2008-06-21T18:28:24"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14747 -->
