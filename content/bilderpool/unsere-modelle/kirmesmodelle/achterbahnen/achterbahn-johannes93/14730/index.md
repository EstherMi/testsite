---
layout: "image"
title: "Trafo für Beleuchtung"
date: "2008-06-21T18:28:23"
picture: "achterbahn12.jpg"
weight: "12"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/14730
imported:
- "2019"
_4images_image_id: "14730"
_4images_cat_id: "1349"
_4images_user_id: "636"
_4images_image_date: "2008-06-21T18:28:23"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14730 -->
