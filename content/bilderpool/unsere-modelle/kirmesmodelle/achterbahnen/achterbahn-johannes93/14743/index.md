---
layout: "image"
title: "Wagen in Kurve 5"
date: "2008-06-21T18:28:24"
picture: "achterbahn25.jpg"
weight: "25"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/14743
imported:
- "2019"
_4images_image_id: "14743"
_4images_cat_id: "1349"
_4images_user_id: "636"
_4images_image_date: "2008-06-21T18:28:24"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14743 -->
