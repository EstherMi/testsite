---
layout: "image"
title: "Seitenansicht"
date: "2006-09-05T11:56:12"
picture: "ftpics_002.jpg"
weight: "24"
konstrukteure: 
- "Marius"
fotografen:
- "Marius"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- details/6783
imported:
- "2019"
_4images_image_id: "6783"
_4images_cat_id: "652"
_4images_user_id: "430"
_4images_image_date: "2006-09-05T11:56:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6783 -->
Zwischen den beiden Hauptschienen ist eine Schien die aus 30ger Bausteinen besteht, in Abstand von zwei 30ger ist sind 15ner draufgesteckt. auf der Unterseite vom Wagen befindet sich ein Gelenkwürfel an den ein 30ger montiert ist. dieser schwingende 30ger greift dann in die 15ger.