---
layout: "image"
title: "Wagen in der Steilkurve"
date: "2006-09-10T20:45:20"
picture: "ftpics_007.jpg"
weight: "22"
konstrukteure: 
- "Marius"
fotografen:
- "Marius"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- details/6789
imported:
- "2019"
_4images_image_id: "6789"
_4images_cat_id: "652"
_4images_user_id: "430"
_4images_image_date: "2006-09-10T20:45:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6789 -->
Um die G Kräfte erträglich zu machen ist die kurve geneigt. (Danke Harald*g*)