---
layout: "image"
title: "Der Aufzug"
date: "2007-01-20T19:32:30"
picture: "wagenundlift1.jpg"
weight: "2"
konstrukteure: 
- "Marius"
fotografen:
- "Marius"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- details/8578
imported:
- "2019"
_4images_image_id: "8578"
_4images_cat_id: "652"
_4images_user_id: "430"
_4images_image_date: "2007-01-20T19:32:30"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8578 -->
Mit dieser Vorrichtung wird der Wagen nach oben gebracht