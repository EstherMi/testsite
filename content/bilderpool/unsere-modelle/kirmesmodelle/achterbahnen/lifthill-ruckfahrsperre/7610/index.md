---
layout: "image"
title: "Die Station von der Seite"
date: "2006-11-25T19:00:05"
picture: "bremseundstation2.jpg"
weight: "9"
konstrukteure: 
- "Marius"
fotografen:
- "Marius"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- details/7610
imported:
- "2019"
_4images_image_id: "7610"
_4images_cat_id: "652"
_4images_user_id: "430"
_4images_image_date: "2006-11-25T19:00:05"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7610 -->
Hier soll der Wagen punktgenau stehenbleiben, damit ein komfortables Einsteigen gewährleistet ist.