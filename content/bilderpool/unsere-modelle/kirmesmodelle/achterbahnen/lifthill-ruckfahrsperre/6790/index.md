---
layout: "image"
title: "First Drop"
date: "2006-09-10T20:45:20"
picture: "ftpics_005.jpg"
weight: "23"
konstrukteure: 
- "Marius"
fotografen:
- "Marius"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- details/6790
imported:
- "2019"
_4images_image_id: "6790"
_4images_cat_id: "652"
_4images_user_id: "430"
_4images_image_date: "2006-09-10T20:45:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6790 -->
1# Drop (1. abfahrt) hat zwar noch ein paar Schwachstellen. Aber ich versuch die zu beheben.