---
layout: "image"
title: "Kabelführung"
date: "2011-08-05T16:35:16"
picture: "shuttleachterbahnkieseleck19.jpg"
weight: "19"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- details/31539
imported:
- "2019"
_4images_image_id: "31539"
_4images_cat_id: "2346"
_4images_user_id: "1322"
_4images_image_date: "2011-08-05T16:35:16"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31539 -->
Ich habe versucht die Kabel möglichst ordentlich zu führen. Das im Bild hängende Kabel ist das Kabel für den Elektromagneten.