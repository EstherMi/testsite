---
layout: "overview"
title: "Shuttleachterbahn (Kieseleck)"
date: 2019-12-17T18:54:46+01:00
legacy_id:
- categories/2346
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2346 --> 
Diese Achterbahn hat eine gewisse Ähnlichkeit mit einer Free Fall Achterbahn: Ein Wagen wird eine Steigung hochgezogen, schließlich losgelassen und saust dann den Berg hinab. Ein Shuttlecoaster hat ein offenes Gleissystem, das zweimal durchfahren wird (vorwärts und rückwärts). Meine Achterbahn ist möglichst einfach gehalten so verzichte ich z.B. auf Lichtschranken.