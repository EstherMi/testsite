---
layout: "image"
title: "Interface"
date: "2011-08-05T16:35:16"
picture: "shuttleachterbahnkieseleck20.jpg"
weight: "20"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- details/31540
imported:
- "2019"
_4images_image_id: "31540"
_4images_cat_id: "2346"
_4images_user_id: "1322"
_4images_image_date: "2011-08-05T16:35:16"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31540 -->
Das "Köpfchen" der Achterbahn