---
layout: "image"
title: "Abdeckung"
date: "2011-08-05T16:35:16"
picture: "shuttleachterbahnkieseleck21.jpg"
weight: "21"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- details/31541
imported:
- "2019"
_4images_image_id: "31541"
_4images_cat_id: "2346"
_4images_user_id: "1322"
_4images_image_date: "2011-08-05T16:35:16"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31541 -->
Interfaceverkleidung mit Schaltpult