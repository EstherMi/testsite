---
layout: "image"
title: "Schaltpult"
date: "2011-08-05T16:35:16"
picture: "shuttleachterbahnkieseleck18.jpg"
weight: "18"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- details/31538
imported:
- "2019"
_4images_image_id: "31538"
_4images_cat_id: "2346"
_4images_user_id: "1322"
_4images_image_date: "2011-08-05T16:35:16"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31538 -->
Es besteht aus einem kombinerten Schalter/Taster für Dauerbetrieb/Start (links) und  einem Notaus-Taster (logischerweise rechts).