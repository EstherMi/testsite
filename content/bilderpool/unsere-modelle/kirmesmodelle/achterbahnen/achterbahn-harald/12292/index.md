---
layout: "image"
title: "Achterbahn01.JPG"
date: "2007-10-23T19:46:38"
picture: "Achterbahn01.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/12292
imported:
- "2019"
_4images_image_id: "12292"
_4images_cat_id: "1098"
_4images_user_id: "4"
_4images_image_date: "2007-10-23T19:46:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12292 -->
Das ist mal ein zarter Anfang für eine Achterbahn. Der Aufzug (Lifthill) fehlt, ein Looping fehlt und die Bahn ist nicht einmal geschlossen. Aber die Bahn kann gekrümmt, verschränkt und verbogen werden, braucht wenig Material für das eigentliche Gleis und das wichtigste: sie geht ordentlich "ab".