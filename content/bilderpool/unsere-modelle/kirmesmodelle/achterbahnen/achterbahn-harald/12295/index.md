---
layout: "image"
title: "Achterbahn04.JPG"
date: "2007-10-23T19:56:20"
picture: "Achterbahn04.JPG"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/12295
imported:
- "2019"
_4images_image_id: "12295"
_4images_cat_id: "1098"
_4images_user_id: "4"
_4images_image_date: "2007-10-23T19:56:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12295 -->
Gleisbefestigung in der Variante mit Klemmhülse 7,5 (35980). Davon sitzt eine längs und eine quer, damit sich das Ganze nicht auseinanderwackeln kann.