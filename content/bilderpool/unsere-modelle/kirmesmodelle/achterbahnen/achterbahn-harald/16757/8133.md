---
layout: "comment"
hidden: true
title: "8133"
date: "2009-01-03T10:20:23"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Tja, die Strecke... einen Rundkurs hinbiegen ist das kleinere Problem. Aber ich will so ca. 1,5 m hoch hinauf, damit hinter dem Looping noch etwas Tempo übrig bleibt. Da stellt sich dann gleich die Frage nach dem Transport im PKW.
 
Der Transportwagen kann meinetwegen sehr lange brauchen. Es reicht mir (zumindest für den Anfang), wenn ein Fahrgastwagen in der Bahn ist, und dann ist da noch der Fahrgastwechsel dazwischen ;-)

Mit den Figuren und zwei Metallachsen als Ballast wiegt der Wagen 150 g. Die Sicht genau nach vorn ist schon noch frei (aber nur gerade eben). Weiter nach außen oder nach unten kann ich die Figuren schlecht setzen, sonst passt das Raster nicht mehr. Das Gerüst soll ein Gitter aus Statikträgern 120 mit BS15 in den Ecken werden. Durch so eine Lücke soll der Wagen sauber durchpassen.

Gruß,
Harald