---
layout: "image"
title: "Achterbahn11.jpg"
date: "2008-12-29T16:37:54"
picture: "Achterbahn11.jpg"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/16757
imported:
- "2019"
_4images_image_id: "16757"
_4images_cat_id: "1098"
_4images_user_id: "4"
_4images_image_date: "2008-12-29T16:37:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16757 -->
Der Anfang des Lifthill. Zum Hochziehen des Wagens wird ein zweiter Wagen darüber gestülpt, der im selben Gleis läuft und an zwei Fäden gezogen wird.

Alle anderen Ideen mit Haken und Magneten usw. habe ich wieder verworfen, weil das Ein- und Ausklinken zu hakelig wurde.

Die Stütze in Bildmitte verschwindet, wenn erst mal das obere Ende des Aluträgers fertig ist. Die Bahn muss man sich am unteren Ende des Aluträgers flach nach links fortgesetzt denken. Von dort kommt der Wagen angerollt und wird mit einem Stempel (fehlt noch) in die hier gezeigte Position gedrückt. Dann fährt das Hubgetriebe den oberen (roten) Wagen darüber und die Fahrt bergauf kann losgehen.