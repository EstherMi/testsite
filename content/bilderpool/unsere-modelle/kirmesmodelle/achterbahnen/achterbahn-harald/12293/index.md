---
layout: "image"
title: "Achterbahn02.JPG"
date: "2007-10-23T19:50:27"
picture: "Achterbahn02.JPG"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/12293
imported:
- "2019"
_4images_image_id: "12293"
_4images_cat_id: "1098"
_4images_user_id: "4"
_4images_image_date: "2007-10-23T19:50:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12293 -->
Wesentliches Element im Gleis sind die Flachträger 120. Die lassen sich prima biegen und verwinden. Für Kurven kann man Winkel 7,5° oder an langsamen Stellen auch 15° einsetzen. In Geraden und zum Befestigen kann man alles zwischensetzen, was 15 mm breit und oben flach ist.