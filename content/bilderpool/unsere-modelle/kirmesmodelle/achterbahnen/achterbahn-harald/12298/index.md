---
layout: "image"
title: "Achterbahn09.JPG"
date: "2007-10-23T20:03:18"
picture: "Achterbahn09.JPG"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/12298
imported:
- "2019"
_4images_image_id: "12298"
_4images_cat_id: "1098"
_4images_user_id: "4"
_4images_image_date: "2007-10-23T20:03:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12298 -->
Der Wagen besteht aus zwei Radgestellen und etwas Ballast an der Gelenkverbindung dazwischen. Der Ballast wird durch zwei Gewichte aus einer ft-BSB-Lokomotive gebildet.