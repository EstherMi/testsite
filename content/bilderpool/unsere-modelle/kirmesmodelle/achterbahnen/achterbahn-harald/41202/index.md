---
layout: "image"
title: "Achterbahn1735.jpg"
date: "2015-06-20T09:48:42"
picture: "IMG_1735.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/41202
imported:
- "2019"
_4images_image_id: "41202"
_4images_cat_id: "1098"
_4images_user_id: "4"
_4images_image_date: "2015-06-20T09:48:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41202 -->
Das ist ein Fehlversuch: der Looping hat mich ziemlich zurückgeworfen: Bastelzimmer umräumen, Ikea-Regale aufbauen, um auf zwei Meter Höhe für den Anlauf zu kommen, und dann reicht es doch nicht für den kompletten Looping.

Die Bahn ist zu weich. Je mehr Anlauf man nimmt, desto schlimmer biegt sich alles, und diese ganze Walkerei schluckt die Energie, die eigentlich in das Wiedergewinnen der Höhe fließen sollte.

Für den nächsten Versuch fasse ich mal eine durchgebremste Fahrradfelge als Träger ins Auge. Die Rundprofile sind an einer Stelle zusammengesteckt; da lässt sich was machen...