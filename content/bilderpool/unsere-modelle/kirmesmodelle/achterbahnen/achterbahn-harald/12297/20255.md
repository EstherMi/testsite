---
layout: "comment"
hidden: true
title: "20255"
date: "2015-02-26T06:39:15"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Wenn der Wagen in Kurven entgleist - so wie Du es beschrieben hast - dann stimmt die Überhöhung der Steilkurve nicht. Die muß zum Tempo des Wagens passen, dann ist die Querbeschleunigung am Wagen Null und er kippt nicht aus den Schienen.

Grüße
H.A.R.R.Y.