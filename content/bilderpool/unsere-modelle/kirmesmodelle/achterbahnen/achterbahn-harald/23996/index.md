---
layout: "image"
title: "Lifthill55.JPG"
date: "2009-05-10T16:20:03"
picture: "Lifthill55.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/23996
imported:
- "2019"
_4images_image_id: "23996"
_4images_cat_id: "1098"
_4images_user_id: "4"
_4images_image_date: "2009-05-10T16:20:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23996 -->
Die neue Version des Lifthill.
Der Plan ist jetzt, dass der Fahrgastwagen von hinten durch die Station hindurchfährt (auf dem Gleisstück, das gerade nach oben herausgeschwenkt ist). Anschließend wird ein Schubwagen dahinter gesetzt, der dann das Gespann mittels Motor und Seilzug nach oben befördert. Der Schubwagen ist gerade mitsamt Gleisstück nach rechts herausgeschwenkt.