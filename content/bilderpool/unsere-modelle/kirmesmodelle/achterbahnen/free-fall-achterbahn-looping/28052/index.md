---
layout: "image"
title: "21 Motor"
date: "2010-09-07T18:06:07"
picture: "achterbahn21.jpg"
weight: "21"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/28052
imported:
- "2019"
_4images_image_id: "28052"
_4images_cat_id: "2037"
_4images_user_id: "860"
_4images_image_date: "2010-09-07T18:06:07"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28052 -->
Der 50:1 Motor, der den Drehkranz antreibt. Sein Z10 Ritzel greift in das Z20 Zahnrad. Auf der gleichen Stange befinden sich überhalb zwei Z10 Zahnräder, die in den Drehkranz greifen. 
Leider ist das etwas von den Kabeln verdeckt.