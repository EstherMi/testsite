---
layout: "image"
title: "02 Turm"
date: "2010-09-07T18:06:05"
picture: "achterbahn02.jpg"
weight: "2"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/28033
imported:
- "2019"
_4images_image_id: "28033"
_4images_cat_id: "2037"
_4images_user_id: "860"
_4images_image_date: "2010-09-07T18:06:05"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28033 -->
Der Turm hat eine Gesamthöhe von genau 2 Metern, der Wagen wird allerdings nur ca. 1,5 Meter hochgezogen und fällt demnach auch nur 1,5 Meter in die Tiefe.
Die Fahrgäste können übrigens immer nach unten sehen, da der Wagen rückwärts hochgezogen wird, oben um 90° zur Seite gedreht wird, um dann vorwärts runterfallen zu können.