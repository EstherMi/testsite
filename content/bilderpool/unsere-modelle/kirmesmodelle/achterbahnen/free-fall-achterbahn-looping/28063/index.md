---
layout: "image"
title: "32 Lichtschranke"
date: "2010-09-07T18:06:07"
picture: "achterbahn32.jpg"
weight: "32"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/28063
imported:
- "2019"
_4images_image_id: "28063"
_4images_cat_id: "2037"
_4images_user_id: "860"
_4images_image_date: "2010-09-07T18:06:07"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28063 -->
So sieht eine Lichtschranke aus.