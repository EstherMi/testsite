---
layout: "image"
title: "30 Kompressor"
date: "2010-09-07T18:06:07"
picture: "achterbahn30.jpg"
weight: "30"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/28061
imported:
- "2019"
_4images_image_id: "28061"
_4images_cat_id: "2037"
_4images_user_id: "860"
_4images_image_date: "2010-09-07T18:06:07"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28061 -->
Nochmal der Kompressor. 
Das Ventil ist eigentlich unnötig, da der Wagen ja nur so weit abgebremst werden soll, damit er noch von der Kette erfasst wird und auf die bewegliche Schiene gezogen werden kann. Deshalb muss man die Bremse gar nicht öffnen können.