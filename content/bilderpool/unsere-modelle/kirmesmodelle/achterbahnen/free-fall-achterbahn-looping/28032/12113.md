---
layout: "comment"
hidden: true
title: "12113"
date: "2010-09-08T16:29:17"
uploadBy:
- "-Matthias-"
license: "unknown"
imported:
- "2019"
---
Da hast Du wohl Recht, aber ich kann das wie schon gesagt nicht wirklich umsetzten. Mir fehlen einfach die Teile, um mehr Platz dafür zu schaffen und um die Kurve sanfter zu bauen. Vielleicht komme ich irgendwann mal dazu, das zu verwirklichen, aber an dem Modell gehts wohl nicht mehr weiter.