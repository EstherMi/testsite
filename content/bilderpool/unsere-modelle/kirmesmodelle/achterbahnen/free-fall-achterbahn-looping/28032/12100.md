---
layout: "comment"
hidden: true
title: "12100"
date: "2010-09-07T18:13:32"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Ich weiß was, ich weiß was!

Der Übergang von der geraden Linie im Fall zur Kreisbahn ist es. Die Bahn ist noch stetig (ugs: "kontinuierlich") - klaro, sonst hätte sie ja Sprünge. Aber die Krümmung der Bahn ist es nicht: sie springt von 0 (Gerade) auf einen Wert x, der vom Radius der Bahn abhängt.
Abhilfe: sanfter in die Kurve einlaufen lassen. Das Stichwort lautet "Klothoide", und wenn du das zusammen mit dem Erfinder SchwarzAbhilfe: sanfter in die Kurve einlaufen lassen. Das Stichwort lautet "Klothoide", und wenn du das zusammen mit den Erfindernamen Anton Schwarzkopf und Werner Stengel als Suchbegriff verwendest, ist ein langes Wochenende an Recherche und Hintergrundarbeit "gebongt".

Gruß,
Harald