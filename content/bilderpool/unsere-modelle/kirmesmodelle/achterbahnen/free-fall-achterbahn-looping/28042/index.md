---
layout: "image"
title: "11 oben"
date: "2010-09-07T18:06:07"
picture: "achterbahn11.jpg"
weight: "11"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/28042
imported:
- "2019"
_4images_image_id: "28042"
_4images_cat_id: "2037"
_4images_user_id: "860"
_4images_image_date: "2010-09-07T18:06:07"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28042 -->
Oben hängt das kleine Wägelchen, dass den Personenwagen rückwärts nach oben zieht. Wenn er oben angekommen ist, fährt die Sperre raus und der ganze mitlere Schienenteil macht eine 90° Drehung zur anderen Schiene.