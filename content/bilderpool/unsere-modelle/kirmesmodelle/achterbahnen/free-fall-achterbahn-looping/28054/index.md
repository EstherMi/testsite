---
layout: "image"
title: "23 Wagen"
date: "2010-09-07T18:06:07"
picture: "achterbahn23.jpg"
weight: "23"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/28054
imported:
- "2019"
_4images_image_id: "28054"
_4images_cat_id: "2037"
_4images_user_id: "860"
_4images_image_date: "2010-09-07T18:06:07"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28054 -->
Dieses Wägelschen zieht den großen Wagen nach oben. In der Mitte ist der Baustein mit dem Magnet verbaut.