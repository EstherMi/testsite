---
layout: "image"
title: "17 oben"
date: "2010-09-07T18:06:07"
picture: "achterbahn17.jpg"
weight: "17"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/28048
imported:
- "2019"
_4images_image_id: "28048"
_4images_cat_id: "2037"
_4images_user_id: "860"
_4images_image_date: "2010-09-07T18:06:07"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28048 -->
So sieht der obere Teil vom Turm aus. Der Motor 20:1 rechts im Bild zieht den Wagen hoch, der Motor 50:1 unten (verdeckt von Winkelträger) dreht die Schiene.