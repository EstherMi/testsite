---
layout: "image"
title: "07 Kabel"
date: "2010-09-07T18:06:06"
picture: "achterbahn07.jpg"
weight: "7"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/28038
imported:
- "2019"
_4images_image_id: "28038"
_4images_cat_id: "2037"
_4images_user_id: "860"
_4images_image_date: "2010-09-07T18:06:06"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28038 -->
Nochmal die Steuerung, das Braune in der Mitte ist eine Verteilerbox. Daran sind die Lichtschranken und und alle weiteren Motoren anfgeschlossen, die nur einen Lichtausgang am Interface benötigen.