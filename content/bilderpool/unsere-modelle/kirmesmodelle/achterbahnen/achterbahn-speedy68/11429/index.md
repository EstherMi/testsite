---
layout: "image"
title: "Achterbahn Bild 2"
date: "2007-09-01T15:33:19"
picture: "Achterbahn_Bild_2.jpg"
weight: "2"
konstrukteure: 
- "speedy68"
fotografen:
- "speedy68"
keywords: ["ft", "Fischertechnik", "Achterbahn"]
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/11429
imported:
- "2019"
_4images_image_id: "11429"
_4images_cat_id: "1023"
_4images_user_id: "409"
_4images_image_date: "2007-09-01T15:33:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11429 -->
