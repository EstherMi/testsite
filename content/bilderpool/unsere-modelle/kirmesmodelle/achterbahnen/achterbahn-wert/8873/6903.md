---
layout: "comment"
hidden: true
title: "6903"
date: "2008-08-22T16:34:13"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
@Patrick: Die Schiene gibt es nur in dieser Form, nicht in Kurven wie hier gewünscht. Es ist ja eh die Frage, wie man den Wagen denn von der waagerecht verlegten Geraden auf diese Kurve bekommen soll.

Gruß,
Stefan