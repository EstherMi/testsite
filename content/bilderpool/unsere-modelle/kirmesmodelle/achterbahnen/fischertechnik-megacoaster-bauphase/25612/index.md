---
layout: "image"
title: "Bügel offen"
date: "2009-10-31T14:15:31"
picture: "fischertechnikmegacoasterbauphase11.jpg"
weight: "11"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "Christian Knobloch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "C-Knobloch"
license: "unknown"
legacy_id:
- details/25612
imported:
- "2019"
_4images_image_id: "25612"
_4images_cat_id: "1799"
_4images_user_id: "997"
_4images_image_date: "2009-10-31T14:15:31"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25612 -->
Damit die Männchen auch aussteigen können, muss man in der Station die Bügel öffnen können. Leider kann dies nur manuell geschehen, da der Platz für ein automatisches System nicht ausreicht.

[url=http://www.youtube.com/user/ftmegacoaster]Mein Kanal auf YouTube mit dem Video und dem Trailer der aktuellen Version[/url]