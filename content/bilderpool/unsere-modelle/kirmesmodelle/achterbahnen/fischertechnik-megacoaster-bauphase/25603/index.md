---
layout: "image"
title: "Turm und Looping"
date: "2009-10-31T14:15:30"
picture: "fischertechnikmegacoasterbauphase02.jpg"
weight: "2"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "Christian Knobloch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "C-Knobloch"
license: "unknown"
legacy_id:
- details/25603
imported:
- "2019"
_4images_image_id: "25603"
_4images_cat_id: "1799"
_4images_user_id: "997"
_4images_image_date: "2009-10-31T14:15:30"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25603 -->
man sieht, das der Radius der Schiene sich immer mehr an den Boden anschmiegt, um die Belastungen für die Passagiere gering zu halten ;)

[url=http://www.youtube.com/user/ftmegacoaster]Mein Kanal auf YouTube mit dem Video und dem Trailer der aktuellen Version[/url]