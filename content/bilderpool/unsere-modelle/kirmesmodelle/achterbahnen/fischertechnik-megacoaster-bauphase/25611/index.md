---
layout: "image"
title: "Looping Stütze"
date: "2009-10-31T14:15:31"
picture: "fischertechnikmegacoasterbauphase10.jpg"
weight: "10"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "Christian Knobloch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "C-Knobloch"
license: "unknown"
legacy_id:
- details/25611
imported:
- "2019"
_4images_image_id: "25611"
_4images_cat_id: "1799"
_4images_user_id: "997"
_4images_image_date: "2009-10-31T14:15:31"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25611 -->
[url=http://www.youtube.com/user/ftmegacoaster]Mein Kanal auf YouTube mit dem Video und dem Trailer der aktuellen Version[/url]