---
layout: "image"
title: "*TESTSTRECKE* Überblick"
date: "2009-10-31T14:15:29"
picture: "fischertechnikmegacoasterbauphase01.jpg"
weight: "1"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "Christian Knobloch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "C-Knobloch"
license: "unknown"
legacy_id:
- details/25602
imported:
- "2019"
_4images_image_id: "25602"
_4images_cat_id: "1799"
_4images_user_id: "997"
_4images_image_date: "2009-10-31T14:15:29"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25602 -->
*Das war nur die Teststrecke zum experimentieren und erkenntnisse sammeln.*

Die Strecke ist zwar geschlossen, wird aber evtl. noch verlängert. Der Turm ist ca. 2,20m hoch, der Looping ca. 70cm. Wenn man den Wagen so bei 1.30-1.40 loslässt, schafft er den Looping bereits.

[url=http://www.youtube.com/user/ftmegacoaster]Mein Kanal auf YouTube mit dem Video und dem Trailer der aktuellen Version[/url]