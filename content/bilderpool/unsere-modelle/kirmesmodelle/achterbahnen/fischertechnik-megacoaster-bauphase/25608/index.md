---
layout: "image"
title: "Helix (Kurve)"
date: "2009-10-31T14:15:30"
picture: "fischertechnikmegacoasterbauphase07.jpg"
weight: "7"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "Christian Knobloch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "C-Knobloch"
license: "unknown"
legacy_id:
- details/25608
imported:
- "2019"
_4images_image_id: "25608"
_4images_cat_id: "1799"
_4images_user_id: "997"
_4images_image_date: "2009-10-31T14:15:30"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25608 -->
zurück zur Station

[url=http://www.youtube.com/user/ftmegacoaster]Mein Kanal auf YouTube mit dem Video und dem Trailer der aktuellen Version[/url]