---
layout: "image"
title: "Liftaufgang"
date: "2009-10-31T14:15:30"
picture: "fischertechnikmegacoasterbauphase05.jpg"
weight: "5"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "Christian Knobloch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "C-Knobloch"
license: "unknown"
legacy_id:
- details/25606
imported:
- "2019"
_4images_image_id: "25606"
_4images_cat_id: "1799"
_4images_user_id: "997"
_4images_image_date: "2009-10-31T14:15:30"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25606 -->
eine Kette zieht den Wagen aus der Station zum Lift, wo der Wagen sich in die Liftkette einhakt. Dann gehts Richtung Decke...

[url=http://www.youtube.com/user/ftmegacoaster]Mein Kanal auf YouTube mit dem Video und dem Trailer der aktuellen Version[/url]