---
layout: "image"
title: "Mini-Achterbahn.JPG"
date: "2013-11-01T21:46:41"
picture: "Mini-Achterbahn0096.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/37787
imported:
- "2019"
_4images_image_id: "37787"
_4images_cat_id: "841"
_4images_user_id: "4"
_4images_image_date: "2013-11-01T21:46:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37787 -->
Mit den Flex-Schienen geht noch mehr.

Der Wagen mit den Seilrollen 15 (unten mitte) ist deutlich breiter als der mit Seilrollen 12 (links), braucht aber weniger Abstand zwischen Bahnteil und Aufhängung: da reichen einzelne Klemmhülsen 35980. Die sind immer quer montiert, damit die Bahnteile zusammen bleiben. Mit den Radhaltern 35668 sollte auch noch etwas machbar sein.

Die Aufhängung mit hochkant stehenden BS7,5 hat den Nachteil, dass die Bahnteile auseinander rutschen können. Das lässt sich aber mit etwas Klebeband oder einem Stück Faden leicht verhindern. Mit etwas Angelschnur, durch eine Strebe 15 gefädelt, wird das blaue Bahnteil in der Mitte gegen Durchhängen abgestützt.