---
layout: "image"
title: "Aufzug für die Achterbahn(3)"
date: "2008-09-23T17:19:12"
picture: "aufzugfuerachterbahn03.jpg"
weight: "3"
konstrukteure: 
- "Toto"
fotografen:
- "Toto"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Toto"
license: "unknown"
legacy_id:
- details/15563
imported:
- "2019"
_4images_image_id: "15563"
_4images_cat_id: "1427"
_4images_user_id: "762"
_4images_image_date: "2008-09-23T17:19:12"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15563 -->
Hier ist der Antrieb zum drehen.