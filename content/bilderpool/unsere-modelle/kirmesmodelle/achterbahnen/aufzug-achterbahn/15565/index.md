---
layout: "image"
title: "Aufzug für die Achterbahn(5)"
date: "2008-09-23T17:19:12"
picture: "aufzugfuerachterbahn05.jpg"
weight: "5"
konstrukteure: 
- "Toto"
fotografen:
- "Toto"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Toto"
license: "unknown"
legacy_id:
- details/15565
imported:
- "2019"
_4images_image_id: "15565"
_4images_cat_id: "1427"
_4images_user_id: "762"
_4images_image_date: "2008-09-23T17:19:12"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15565 -->
Von Oben kann man alles sehen.