---
layout: "image"
title: "Abbau"
date: "2008-01-04T16:45:49"
picture: "mobileskarussel12.jpg"
weight: "12"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/13234
imported:
- "2019"
_4images_image_id: "13234"
_4images_cat_id: "1197"
_4images_user_id: "636"
_4images_image_date: "2008-01-04T16:45:49"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13234 -->
