---
layout: "image"
title: "Inferno-Detail16.JPG"
date: "2005-04-22T11:17:24"
picture: "Inferno-Detail16.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4066
imported:
- "2019"
_4images_image_id: "4066"
_4images_cat_id: "346"
_4images_user_id: "4"
_4images_image_date: "2005-04-22T11:17:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4066 -->
Ein Blick ins Gegengewicht.

Der obere E-Tec ist noch frei, der untere steuert zwei Lampen an der Gondelhaltung. Der Motor treibt den Gondelträger an; die Welle dazu verläuft durch den ganzen Arm hindurch.