---
layout: "image"
title: "Inferno-Detail08.JPG"
date: "2005-04-22T11:17:23"
picture: "Inferno-Detail08.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4065
imported:
- "2019"
_4images_image_id: "4065"
_4images_cat_id: "346"
_4images_user_id: "4"
_4images_image_date: "2005-04-22T11:17:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4065 -->
Ein Detail von der Gondelaufnahme. Der blaue Kabelstrang führt zum Schleifring (Modell A, siehe http://www.ftcommunity.de/categories.php?cat_id=347 ), der im Drehkranz montiert ist und links oben teilweise sichtbar ist.
Die beiden Lampen werden von einem E-Tec-Modul (als Wechselblinker geschaltet) angesteuert, das im Gegengewicht eingebaut ist.