---
layout: "image"
title: "Inferno-Gondel16.JPG"
date: "2005-04-19T11:25:11"
picture: "Inferno-Gondel16.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4008
imported:
- "2019"
_4images_image_id: "4008"
_4images_cat_id: "346"
_4images_user_id: "4"
_4images_image_date: "2005-04-19T11:25:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4008 -->
NATÜRLICH wurde auch an die Sicherheit gedacht: das Personal sammelt nicht nur die Fahrchips ein, sondern bringt auch die Rückhaltebügel in Position.