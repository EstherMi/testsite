---
layout: "image"
title: "Inferno-Podium10.JPG"
date: "2005-04-19T20:23:04"
picture: "Inferno-Podium10.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4010
imported:
- "2019"
_4images_image_id: "4010"
_4images_cat_id: "346"
_4images_user_id: "4"
_4images_image_date: "2005-04-19T20:23:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4010 -->
Die Gondel steckt mittendrin, und wenn sie einmal draußen ist, kann das Podium mittels einer Mechanik à la "Nürnberger Schere" aufgeklappt werden.