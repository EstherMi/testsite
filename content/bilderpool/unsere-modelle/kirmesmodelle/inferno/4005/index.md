---
layout: "image"
title: "Inferno58.JPG"
date: "2005-04-19T11:25:11"
picture: "Inferno58.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4005
imported:
- "2019"
_4images_image_id: "4005"
_4images_cat_id: "346"
_4images_user_id: "4"
_4images_image_date: "2005-04-19T11:25:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4005 -->
Jetzt ist der Arm frei (die Streben liegen vor den mittleren Stützen auf dem Boden), das Gegengewicht ist hochgestellt und verriegelt, der Mast für die Seilwinde ist aufgeklappt.

Die Fotos leiden alle ein wenig an Lichtmangel, deswegen kommt leider überall das Rauschen des Sensors durch.