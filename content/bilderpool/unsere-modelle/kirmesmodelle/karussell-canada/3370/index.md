---
layout: "image"
title: "Karussell 003"
date: "2004-12-05T10:21:16"
picture: "Karussell_003.JPG"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/3370
imported:
- "2019"
_4images_image_id: "3370"
_4images_cat_id: "293"
_4images_user_id: "5"
_4images_image_date: "2004-12-05T10:21:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3370 -->
