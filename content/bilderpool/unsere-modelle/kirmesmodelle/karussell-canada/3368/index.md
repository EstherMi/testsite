---
layout: "image"
title: "Karussell 001"
date: "2004-12-05T10:21:16"
picture: "Karussell_001.JPG"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/3368
imported:
- "2019"
_4images_image_id: "3368"
_4images_cat_id: "293"
_4images_user_id: "5"
_4images_image_date: "2004-12-05T10:21:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3368 -->
