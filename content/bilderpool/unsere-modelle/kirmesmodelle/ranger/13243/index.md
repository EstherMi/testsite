---
layout: "image"
title: "Draufsicht"
date: "2008-01-04T16:45:49"
picture: "ranger02.jpg"
weight: "2"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/13243
imported:
- "2019"
_4images_image_id: "13243"
_4images_cat_id: "1198"
_4images_user_id: "636"
_4images_image_date: "2008-01-04T16:45:49"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13243 -->
