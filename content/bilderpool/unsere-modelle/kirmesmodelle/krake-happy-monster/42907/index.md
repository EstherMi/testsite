---
layout: "image"
title: "Die KRAKE - Aufzugsmotor an der Banane"
date: "2016-02-20T20:43:14"
picture: "diekrakeoderauchhappymonster11.jpg"
weight: "11"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/42907
imported:
- "2019"
_4images_image_id: "42907"
_4images_cat_id: "3190"
_4images_user_id: "1359"
_4images_image_date: "2016-02-20T20:43:14"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42907 -->
