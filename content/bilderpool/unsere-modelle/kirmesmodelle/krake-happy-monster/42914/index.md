---
layout: "image"
title: "Die KRAKE - Krak(g)-Arm"
date: "2016-02-20T20:43:15"
picture: "diekrakeoderauchhappymonster18.jpg"
weight: "18"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/42914
imported:
- "2019"
_4images_image_id: "42914"
_4images_cat_id: "3190"
_4images_user_id: "1359"
_4images_image_date: "2016-02-20T20:43:15"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42914 -->
hier das Ende einer der Arme mit Drehkreuz und Antrieb + Gondeln