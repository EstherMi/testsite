---
layout: "image"
title: "die KRAKE - Unterbau des Kugellagers"
date: "2016-02-20T20:43:14"
picture: "diekrakeoderauchhappymonster08.jpg"
weight: "8"
konstrukteure: 
- "Jens Lemkamp (lemkajen)"
fotografen:
- "Jens Lemkamp (lemkajen)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/42904
imported:
- "2019"
_4images_image_id: "42904"
_4images_cat_id: "3190"
_4images_user_id: "1359"
_4images_image_date: "2016-02-20T20:43:14"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42904 -->
