---
layout: "overview"
title: "Die KRAKE - oder auch HAPPY MONSTER"
date: 2019-12-17T18:56:28+01:00
legacy_id:
- categories/3190
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3190 --> 
ich sag mal : "IMMER WIEDER HAPPY MONSTER"   ;-)       "Auf geht's!! - nächste Fahrt!!" 


hier schonmal die ersten Bilder meines aktuellen Bauprojektes - voll funktionsfähiges Modell der bekannten "DIE KRAKE", auch unterwegs als "Polyp" oder "Happy Monster".
ein vielleicht etwas "betagtes" Fahrgeschäft, welches immer noch oft zu sehen ist und auch gut besucht wird. Mechanisch ist das Ganze schon teilweise kniffelig in ft umzusetzen - definitiv sind noch nicht alle Probleme gelöst, aber ich denke, bis zur convention sollte es im großen und Ganzen "rund" laufen. Vieles wurde schon gebaut aber nicht fotografiert - man darf gespannt sein ;-)