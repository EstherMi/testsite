---
layout: "image"
title: "Neue Schleifringe im Einsatz"
date: "2017-01-15T20:05:25"
picture: "IMG_1066.jpg"
weight: "3"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/45044
imported:
- "2019"
_4images_image_id: "45044"
_4images_cat_id: "2983"
_4images_user_id: "1359"
_4images_image_date: "2017-01-15T20:05:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45044 -->
