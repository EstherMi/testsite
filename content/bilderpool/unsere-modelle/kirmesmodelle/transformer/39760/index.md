---
layout: "image"
title: "Seite schräg"
date: "2014-11-02T15:23:34"
picture: "IMG_0204.jpg"
weight: "77"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: ["Karrussell", "Transformer"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/39760
imported:
- "2019"
_4images_image_id: "39760"
_4images_cat_id: "2983"
_4images_user_id: "1359"
_4images_image_date: "2014-11-02T15:23:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39760 -->
Die "Brücke" lässt sich heben und senken zum Ein&Aus-Stieg. Die Gondeln rotieren. Die Brücke rotiert. Der gesamtaufbau soll später auch noch komplett rotieren. Das drehen des Gesamtgewichtes und Integration von entsprechend vielen Schleifkontakten wird das Hauptproblem werden.