---
layout: "image"
title: "Aufsicht Abgriff Schleifringe Brücke"
date: "2014-11-09T17:21:24"
picture: "IMG_0012.jpg"
weight: "73"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/39785
imported:
- "2019"
_4images_image_id: "39785"
_4images_cat_id: "2983"
_4images_user_id: "1359"
_4images_image_date: "2014-11-09T17:21:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39785 -->
beide Seiten sind identisch aufgebaut, es sind pro Ring je Schleifring immer 2 Abgriffe gebaut, um immer die Energieversorgung unterbrechungsfrei bereit zu stellen, ausserdem ist die Belastung auf die Achse dann gleichmäßig von beiden Seiten