---
layout: "image"
title: "Antrieb & Kugellager"
date: "2015-10-18T18:20:08"
picture: "IMG_0742.jpg"
weight: "5"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: ["Transformer", "Antrieb", "Kugellager", "Drehteller", "Drehtisch"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/42095
imported:
- "2019"
_4images_image_id: "42095"
_4images_cat_id: "2983"
_4images_user_id: "1359"
_4images_image_date: "2015-10-18T18:20:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42095 -->
hier sieht man nochmal den Antrieb (inzwischen doppelt vorhanden) und viele Kugeln des Kugellagers (von unten)