---
layout: "image"
title: "Schleifring im Zentraldrehkranz"
date: "2017-01-15T20:05:25"
picture: "IMG_1930.jpg"
weight: "4"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/45045
imported:
- "2019"
_4images_image_id: "45045"
_4images_cat_id: "2983"
_4images_user_id: "1359"
_4images_image_date: "2017-01-15T20:05:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45045 -->
