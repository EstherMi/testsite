---
layout: "image"
title: "Gesamtansicht"
date: "2015-03-14T12:36:48"
picture: "IMG_0073_copy.jpg"
weight: "38"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/40664
imported:
- "2019"
_4images_image_id: "40664"
_4images_cat_id: "2983"
_4images_user_id: "1359"
_4images_image_date: "2015-03-14T12:36:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40664 -->
der derzeitige Bauzustand mit angebauten Stufen(!) und gelber Seitenverblendung