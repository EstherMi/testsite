---
layout: "comment"
hidden: true
title: "19887"
date: "2014-12-31T18:37:00"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Wozu dient denn das äußere Z20? Muss das ein Verbiegen oder Versetzen der Schneckenachse verhindern?
Gruß, Stefan