---
layout: "image"
title: "Riesenrad"
date: "2012-03-10T23:15:59"
picture: "IMG_2468.jpg"
weight: "9"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- details/34622
imported:
- "2019"
_4images_image_id: "34622"
_4images_cat_id: "2555"
_4images_user_id: "740"
_4images_image_date: "2012-03-10T23:15:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34622 -->
Einblick in die Mittelnabe die beiderseits aus jeweils 25 grauen Winkelträger 7,5° (Art.-Nr. 32070) besteht.
Es bleibt allerdings die Frage offen, warum genau 25 Winkelträger 7,5° einen 360° Vollkreis ergeben?!?