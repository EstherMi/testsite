---
layout: "image"
title: "Riesenrad-Vorderansicht"
date: "2012-03-10T23:15:59"
picture: "IMG_6462.jpg"
weight: "1"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- details/34614
imported:
- "2019"
_4images_image_id: "34614"
_4images_cat_id: "2555"
_4images_user_id: "740"
_4images_image_date: "2012-03-10T23:15:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34614 -->
Vorderansicht des Riesenrades mit insgesamt 40 Gondeln.