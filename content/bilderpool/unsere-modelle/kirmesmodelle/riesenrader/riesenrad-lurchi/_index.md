---
layout: "overview"
title: "Riesenrad von Lurchi"
date: 2019-12-17T18:52:27+01:00
legacy_id:
- categories/2555
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2555 --> 
Im Mittelpunkt zum Bau des Riesenradmodells stand die Konstruktion einer geeigneten Nabe.
Ein Wunsch meinerseits war es zudem diese hochbelastbare Nabe ausschließlich aus unveränderten Serienbauteilen zu erstellen, was bei ähnlich großen Modellen dieser Art bisher nicht der Fall war.