---
layout: "image"
title: "Riesenrad"
date: "2012-03-10T23:15:59"
picture: "IMG_2444.jpg"
weight: "16"
konstrukteure: 
- "Lurchi"
fotografen:
- "Lurchi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Lurchi"
license: "unknown"
legacy_id:
- details/34629
imported:
- "2019"
_4images_image_id: "34629"
_4images_cat_id: "2555"
_4images_user_id: "740"
_4images_image_date: "2012-03-10T23:15:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34629 -->
So könnte das Rad in freier Natur unter blauem Himmel aussehen.