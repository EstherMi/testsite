---
layout: "image"
title: "Energieübertragung per Schleifringe"
date: "2015-02-16T17:29:12"
picture: "Schleifring.jpg"
weight: "3"
konstrukteure: 
- "rito"
fotografen:
- "rito"
keywords: ["Riesenrad", "Kirmes", "txt", "Beleuchtung"]
uploadBy: "rito"
license: "unknown"
legacy_id:
- details/40552
imported:
- "2019"
_4images_image_id: "40552"
_4images_cat_id: "3040"
_4images_user_id: "2179"
_4images_image_date: "2015-02-16T17:29:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40552 -->
Auf jeder Seite des Rades befindet sich ein Schleifring von dem jeweils Strom für zwei unterschiedliche Lampen abgenommen wird.