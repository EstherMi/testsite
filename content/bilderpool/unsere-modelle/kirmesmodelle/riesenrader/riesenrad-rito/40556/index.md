---
layout: "image"
title: "Ausstieg"
date: "2015-02-16T17:29:12"
picture: "Ausstieg.jpg"
weight: "7"
konstrukteure: 
- "rito"
fotografen:
- "rito"
keywords: ["Riesenrad", "Kirmes"]
uploadBy: "rito"
license: "unknown"
legacy_id:
- details/40556
imported:
- "2019"
_4images_image_id: "40556"
_4images_cat_id: "3040"
_4images_user_id: "2179"
_4images_image_date: "2015-02-16T17:29:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40556 -->
Der Ausstieg findet über die hintere Seite statt.