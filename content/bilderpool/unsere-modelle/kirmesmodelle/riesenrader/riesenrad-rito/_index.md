---
layout: "overview"
title: "Riesenrad von rito"
date: 2019-12-17T18:52:33+01:00
legacy_id:
- categories/3040
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3040 --> 
Hallo Leute,
soeben habe ich mein erstes "Wiedereinstiegsprojekt" beendet. Mein Riesenrad 1.0 ist nun soweit, dass alle Grundfunktionen implementiert wurden und so arbeiten wie ich es mir wünsche. Nun wird es wohl noch an die Feinheiten gehen. Ich möchte es euch nun nicht vorenthalten und bin gespannt, was ihr davon haltet.