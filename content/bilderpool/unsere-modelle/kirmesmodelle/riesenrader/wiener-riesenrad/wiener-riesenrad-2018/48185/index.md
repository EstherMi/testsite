---
layout: "image"
title: "Riesenradillumination"
date: "2018-09-23T20:30:35"
picture: "riesenradillumination1.jpg"
weight: "1"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/48185
imported:
- "2019"
_4images_image_id: "48185"
_4images_cat_id: "3534"
_4images_user_id: "968"
_4images_image_date: "2018-09-23T20:30:35"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48185 -->
Hier die Einzelteile : Led mit 4mm Durchmesser. Ballonstab ,Aussenduchmesser 7mm, Innen 4mm.
Aluprofil 8 mal8 mm.