---
layout: "image"
title: "Riesenradillumination"
date: "2018-09-23T20:30:35"
picture: "riesenradillumination8.jpg"
weight: "8"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/48192
imported:
- "2019"
_4images_image_id: "48192"
_4images_cat_id: "3534"
_4images_user_id: "968"
_4images_image_date: "2018-09-23T20:30:35"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48192 -->
Für eine flackerfreie Stromübertragung pro Pol einen Schleifring mit 4 Kontakten.


