---
layout: "image"
title: "Wiener Riesenrad"
date: "2012-09-22T17:57:10"
picture: "wienerriesenrad1.jpg"
weight: "2"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/35536
imported:
- "2019"
_4images_image_id: "35536"
_4images_cat_id: "2638"
_4images_user_id: "968"
_4images_image_date: "2012-09-22T17:57:10"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35536 -->
Große Erleichterung.Ladeprobe im T4  bestanden! Jetzt steht einer Reise nach Erbes -Büdesheim hoffentlich nichts mehr im Wege und Platz für Sven und Bernhard ist auch noch genug :)