---
layout: "image"
title: "Wiener Riesenrad"
date: "2012-03-25T16:44:26"
picture: "wienerriesenad9.jpg"
weight: "9"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/34696
imported:
- "2019"
_4images_image_id: "34696"
_4images_cat_id: "2561"
_4images_user_id: "968"
_4images_image_date: "2012-03-25T16:44:26"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34696 -->
Hier die beiden Spanner,die das umlaufende Antriebsseil aus Spannung halten
Diese Mechanik entspricht im wesentkichen dem original.Das konnte ich bei meinem letzten Besuch im Sommer 2011
fotografieren.Der Antrieb ist frei nach Schnauze gebaut,weil man den vor Ort nich zu sehen bekommt und auch nach stundenlangem Suchen im Netz
nichts zu finden war.