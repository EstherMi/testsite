---
layout: "image"
title: "Details vom Riesenrad 3"
date: "2003-06-03T22:56:59"
picture: "CNXT0034_1.jpg"
weight: "7"
konstrukteure: 
- "Stephan Wenkers"
fotografen:
- "Holger Howey"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/1173
imported:
- "2019"
_4images_image_id: "1173"
_4images_cat_id: "119"
_4images_user_id: "130"
_4images_image_date: "2003-06-03T22:56:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1173 -->
Die Nabe von der Seite gesehen. Es fehlt natürlich noch die zweite Stütze.