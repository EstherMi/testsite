---
layout: "image"
title: "Details vom Riesenrad 8"
date: "2003-06-03T23:45:44"
picture: "CNXT0042_1.jpg"
weight: "12"
konstrukteure: 
- "Stephan Wenkers"
fotografen:
- "Stephan Wenkers"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/1178
imported:
- "2019"
_4images_image_id: "1178"
_4images_cat_id: "119"
_4images_user_id: "130"
_4images_image_date: "2003-06-03T23:45:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1178 -->
Noch eine Seitenansicht,ich halte die rechte Stütze mal etwas an,dann sieht man die ungefähre Breite.