---
layout: "image"
title: "Eine Gondel von der Seite aus gesehen"
date: "2003-07-27T11:06:46"
picture: "Frontansicht_von_der_Gondel.jpg"
weight: "1"
konstrukteure: 
- "Stephan Wenkers"
fotografen:
- "Stephan Wenkers"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/1270
imported:
- "2019"
_4images_image_id: "1270"
_4images_cat_id: "119"
_4images_user_id: "130"
_4images_image_date: "2003-07-27T11:06:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1270 -->
Das ist jetzt eine der Gondeln für das RR. Teilweise hab ich auch anstatt des Scharniers einen Gelenkwürfel eingebaut und auch andere Winkelsteine(15°) verbaut. Dann ist sie etwas schmaler geworden.