---
layout: "image"
title: "Details vom Riesenrad 7"
date: "2003-06-03T22:56:59"
picture: "CNXT0040_1.jpg"
weight: "11"
konstrukteure: 
- "Stephan Wenkers"
fotografen:
- "Stephan Wenkers"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/1177
imported:
- "2019"
_4images_image_id: "1177"
_4images_cat_id: "119"
_4images_user_id: "130"
_4images_image_date: "2003-06-03T22:56:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1177 -->
Frontansicht mit Blick auf die Stützen.Die 4. ist in Arbeit.