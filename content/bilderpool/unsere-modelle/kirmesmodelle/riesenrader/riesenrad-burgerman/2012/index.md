---
layout: "image"
title: "Riesenrad rückseite"
date: "2003-11-26T15:00:56"
picture: "Riesenrad_hinten.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "burgerman"
license: "unknown"
legacy_id:
- details/2012
imported:
- "2019"
_4images_image_id: "2012"
_4images_cat_id: "215"
_4images_user_id: "62"
_4images_image_date: "2003-11-26T15:00:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2012 -->
