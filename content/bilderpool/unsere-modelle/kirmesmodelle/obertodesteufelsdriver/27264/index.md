---
layout: "image"
title: "Der Wagen - Zerlegt"
date: "2010-05-16T15:52:21"
picture: "obertodesteufelsdriver13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/27264
imported:
- "2019"
_4images_image_id: "27264"
_4images_cat_id: "1957"
_4images_user_id: "104"
_4images_image_date: "2010-05-16T15:52:21"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27264 -->
Ein Problem dieses Wagens ist, das recht viel längsverschieblich ist. Nach ein paar Läufen verschieben sich die Laufräder gerne, oder der Sitz rutscht nach vorne. Das führt dann zum Schleifen und schließlich zum Blockieren von Rädern, verbunden der Gefahr eines Unfalls: Der Wagen schafft den Looping vielleicht nicht ganz, oder er kommt nach der Sprungschanze nicht sauber auf.

Nach einiger Tüftelei ging es so am besten: Seitlich steckt nur ein Verbinder 15 in den BS7,5, Federnocken dienen der zusätzlichen Befestigung, aber vor allem als Abstandshalter zu den Rädern.

Dennoch muss der Wagen alle paar Läufe etwas korrigiert werden.