---
layout: "image"
title: "Einfahrt in den Looping"
date: "2010-05-16T15:52:20"
picture: "obertodesteufelsdriver04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/27255
imported:
- "2019"
_4images_image_id: "27255"
_4images_cat_id: "1957"
_4images_user_id: "104"
_4images_image_date: "2010-05-16T15:52:20"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27255 -->
Damit der Looping nicht alles durch die Biegekräfte verdreht, ist er stabil befestigt. Die waagerecht liegenden Bausteine 30 sind auch mit Federnocken auf der Grundplatte angebracht.