---
layout: "image"
title: "Looping"
date: "2010-05-16T15:52:20"
picture: "obertodesteufelsdriver03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/27254
imported:
- "2019"
_4images_image_id: "27254"
_4images_cat_id: "1957"
_4images_user_id: "104"
_4images_image_date: "2010-05-16T15:52:20"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27254 -->
Hier muss er durch, der Ärmste. Ein- und Auslauf beginnen mit Bogenstücken 30°, der eigentliche Looping besteht aus Bogenstücken 60°.