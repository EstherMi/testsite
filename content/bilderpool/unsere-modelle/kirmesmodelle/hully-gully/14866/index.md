---
layout: "image"
title: "Blick aufs fahrende Karussell"
date: "2008-07-15T22:17:21"
picture: "hullygully28.jpg"
weight: "28"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/14866
imported:
- "2019"
_4images_image_id: "14866"
_4images_cat_id: "1356"
_4images_user_id: "636"
_4images_image_date: "2008-07-15T22:17:21"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14866 -->
