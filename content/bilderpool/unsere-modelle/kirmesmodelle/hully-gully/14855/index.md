---
layout: "image"
title: "Blick aus der obersten Kabine"
date: "2008-07-15T22:17:21"
picture: "hullygully17.jpg"
weight: "17"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/14855
imported:
- "2019"
_4images_image_id: "14855"
_4images_cat_id: "1356"
_4images_user_id: "636"
_4images_image_date: "2008-07-15T22:17:21"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14855 -->
