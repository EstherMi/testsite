---
layout: "image"
title: "Kabine von oben"
date: "2008-07-15T22:17:06"
picture: "hullygully10.jpg"
weight: "10"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/14848
imported:
- "2019"
_4images_image_id: "14848"
_4images_cat_id: "1356"
_4images_user_id: "636"
_4images_image_date: "2008-07-15T22:17:06"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14848 -->
