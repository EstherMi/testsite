---
layout: "comment"
hidden: true
title: "6756"
date: "2008-07-21T15:03:20"
uploadBy:
- "stephan"
license: "unknown"
imported:
- "2019"
---
Den Antrieb würde ich nie direkt am drehenden Rad anbringen. Die Schwungmasse kann dir dein Getriebe zerfetzen. Bau ne Rutschkupplung oder ähnliches dazwischen dann bist du auf der sicheren Seite.