---
layout: "image"
title: "Gesamtansicht"
date: "2008-07-15T22:12:01"
picture: "hullygully01.jpg"
weight: "1"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/14839
imported:
- "2019"
_4images_image_id: "14839"
_4images_cat_id: "1356"
_4images_user_id: "636"
_4images_image_date: "2008-07-15T22:12:01"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14839 -->
Dies ist mein Modell eines Hully-Gully. Das Karussell ist mit drei Motoren Ausgestattet, die Das Unterteil und das Oberteil des Karussells getrennt voneinander drehen, und das Oberteil des Karussells um 45° kippen können.