---
layout: "image"
title: "Schiffsschaukel"
date: "2006-01-25T21:14:12"
picture: "Bild_0164.jpg"
weight: "5"
konstrukteure: 
- "limit"
fotografen:
- "limit"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/5678
imported:
- "2019"
_4images_image_id: "5678"
_4images_cat_id: "124"
_4images_user_id: "127"
_4images_image_date: "2006-01-25T21:14:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5678 -->
