---
layout: "overview"
title: "Schwebebahn von Ralf"
date: 2019-12-17T19:44:24+01:00
legacy_id:
- categories/529
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=529 --> 
Ein Schwebebahnsystem das sich sehr leicht erweitern lässt. Eine Schiene ist sehr einfach aufgebaut. Die Bahn kann die Kurven in beiden Richtungen (S-Kurve) durchlaufen und auch kleine Steigungen überwinden.