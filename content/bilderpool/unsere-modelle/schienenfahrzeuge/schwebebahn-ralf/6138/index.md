---
layout: "image"
title: "Die komplette Bahn"
date: "2006-04-26T16:02:08"
picture: "Schwebebahn01.jpg"
weight: "1"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: ["Schwebebahn"]
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- details/6138
imported:
- "2019"
_4images_image_id: "6138"
_4images_cat_id: "529"
_4images_user_id: "381"
_4images_image_date: "2006-04-26T16:02:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6138 -->
Erstmal ein Oval und nur ein Wagen