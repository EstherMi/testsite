---
layout: "image"
title: "Rastaufnahmeachse 22,"
date: "2015-04-20T17:22:02"
picture: "rennachterbahn05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/40848
imported:
- "2019"
_4images_image_id: "40848"
_4images_cat_id: "3069"
_4images_user_id: "104"
_4images_image_date: "2015-04-20T17:22:02"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40848 -->
Die Lenkung geschieht passiv als Zwangslenkung durch das grüne Vorstuferad. Das sitzt nämlich genau zwischen den beiden Fahrbahn-Statikträgerspuren.