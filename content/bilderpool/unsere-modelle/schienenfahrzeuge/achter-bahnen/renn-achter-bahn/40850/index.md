---
layout: "image"
title: "Weichen (1)"
date: "2015-04-20T17:22:02"
picture: "rennachterbahn07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/40850
imported:
- "2019"
_4images_image_id: "40850"
_4images_cat_id: "3069"
_4images_user_id: "104"
_4images_image_date: "2015-04-20T17:22:02"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40850 -->
Eine einfache, vom Fahrzeug selbst aus betätigte Weichenmechanik sorgt für die abwechselnde Richtung, in der das Auto durch die Wendeschleifen rast. Wenn es auf diesem Bild von rechts her ankommt, wird es den linken Weichenstrang befahren, weil die bewegliche Zunge das Lenk-Rad dorthin führt.