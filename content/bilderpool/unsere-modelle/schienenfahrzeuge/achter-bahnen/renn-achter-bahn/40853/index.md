---
layout: "image"
title: "Detail der Kurve (2)"
date: "2015-04-20T17:22:02"
picture: "rennachterbahn10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/40853
imported:
- "2019"
_4images_image_id: "40853"
_4images_cat_id: "3069"
_4images_user_id: "104"
_4images_image_date: "2015-04-20T17:22:02"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40853 -->
Die Wendeschleifen sind ansonsten symmetrisch aufgebaut.