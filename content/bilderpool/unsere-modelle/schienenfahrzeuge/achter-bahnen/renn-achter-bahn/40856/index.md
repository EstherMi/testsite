---
layout: "image"
title: "Stromzufuhr (2)"
date: "2015-04-20T17:22:02"
picture: "rennachterbahn13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/40856
imported:
- "2019"
_4images_image_id: "40856"
_4images_cat_id: "3069"
_4images_user_id: "104"
_4images_image_date: "2015-04-20T17:22:02"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40856 -->
... und gelangt über eine lange Leitung am Ende des Auslegers einfach hinunter zum Fahrzeug.