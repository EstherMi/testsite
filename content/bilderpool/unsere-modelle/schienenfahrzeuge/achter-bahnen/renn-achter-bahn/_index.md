---
layout: "overview"
title: "Renn-Achter-Bahn"
date: 2019-12-17T19:45:02+01:00
legacy_id:
- categories/3069
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3069 --> 
Eine Zwischenstation auf dem langen Weg zu sowas wie einer Carrera-Bahn: Eine Achter-Bahn, auf der ein Wagen tatsächlich mal "rennt", also einigermaßen flott unterwegs ist.