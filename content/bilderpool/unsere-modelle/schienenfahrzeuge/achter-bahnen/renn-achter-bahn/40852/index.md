---
layout: "image"
title: "Detail der Kurve (1)"
date: "2015-04-20T17:22:02"
picture: "rennachterbahn09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/40852
imported:
- "2019"
_4images_image_id: "40852"
_4images_cat_id: "3069"
_4images_user_id: "104"
_4images_image_date: "2015-04-20T17:22:02"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40852 -->
Da nur die Vorderräder wegen ihres Lenk-Vorstuferades in der Kurve auf den Schienen laufen, werden die Hinterräder in der Kurve näher am Schleifenmittelpunkt entlang laufen. Deshalb ist die innere Schiene hier doppelt ausgeführt, und das ist auch (neben der Rennwagen-Optik) der Grund dafür, dass die Hinterräder Zwillingsreifen bekommen mussten.