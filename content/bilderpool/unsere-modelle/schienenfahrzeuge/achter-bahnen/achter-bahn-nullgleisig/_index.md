---
layout: "overview"
title: "Achter-Bahn nullgleisig"
date: 2019-12-17T19:44:58+01:00
legacy_id:
- categories/1894
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1894 --> 
Hier fährt ein selbstlenkendes Fahrzeug relativ frei eine "8" und wir so von einem sich nicht verdrillenden Kabel von außen mit Strom versorgt.