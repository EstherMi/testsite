---
layout: "image"
title: "Grundriss"
date: "2010-02-28T22:06:56"
picture: "achterbahnnullgleisig2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/26566
imported:
- "2019"
_4images_image_id: "26566"
_4images_cat_id: "1894"
_4images_user_id: "104"
_4images_image_date: "2010-02-28T22:06:56"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26566 -->
Zwischendrin, sozusagen über die "Kreuzung" hinweg, fährt das Fahrzeug einfach frei.

Dieses Bahnkonzept ist wohl noch ausbaubar auf wild durcheinander laufende (nicht vorhandene) "Bahnen", solange nur letztlich das Fahrzeug so gedreht wird wie bei einer "8", damit sich das einfach von oben zugeführte Kabel nicht verdrillt.