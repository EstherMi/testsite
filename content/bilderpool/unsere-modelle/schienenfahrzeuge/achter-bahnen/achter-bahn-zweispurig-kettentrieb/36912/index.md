---
layout: "image"
title: "PKW (3)"
date: "2013-05-12T21:59:19"
picture: "achterbahnzweispurigmitkettentrieb12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/36912
imported:
- "2019"
_4images_image_id: "36912"
_4images_cat_id: "2742"
_4images_user_id: "104"
_4images_image_date: "2013-05-12T21:59:19"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36912 -->
Im Heck sitzt ein Winkelstein gleichseitig mit 3 Nuten.