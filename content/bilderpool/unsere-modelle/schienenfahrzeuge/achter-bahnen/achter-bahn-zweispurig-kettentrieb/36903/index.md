---
layout: "image"
title: "Antrieb (1)"
date: "2013-05-12T21:59:09"
picture: "achterbahnzweispurigmitkettentrieb03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/36903
imported:
- "2019"
_4images_image_id: "36903"
_4images_cat_id: "2742"
_4images_user_id: "104"
_4images_image_date: "2013-05-12T21:59:09"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36903 -->
Ein einziger Motor treibt über sein Z10 einen Getriebebock mit Schnecke der Ur-ft-Motoren an, die wiederum auf ein Z40 geht. Auf derselben Achse sitzt das die Kette antreibende zweite Z40.