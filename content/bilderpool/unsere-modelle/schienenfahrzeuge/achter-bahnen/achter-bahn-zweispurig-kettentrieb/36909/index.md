---
layout: "image"
title: "Rennwagen (2)"
date: "2013-05-12T21:59:09"
picture: "achterbahnzweispurigmitkettentrieb09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/36909
imported:
- "2019"
_4images_image_id: "36909"
_4images_cat_id: "2742"
_4images_user_id: "104"
_4images_image_date: "2013-05-12T21:59:09"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36909 -->
Eine 3/4-Ansicht des Rennwagens.