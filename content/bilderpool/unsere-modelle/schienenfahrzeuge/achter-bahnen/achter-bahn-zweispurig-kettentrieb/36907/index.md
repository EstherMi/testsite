---
layout: "image"
title: "LKR (2)"
date: "2013-05-12T21:59:09"
picture: "achterbahnzweispurigmitkettentrieb07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/36907
imported:
- "2019"
_4images_image_id: "36907"
_4images_cat_id: "2742"
_4images_user_id: "104"
_4images_image_date: "2013-05-12T21:59:09"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36907 -->
Eine 3/4-Ansicht des LKW. Alles ist recht leicht aufgebaut, damit ich mir viele Stützen unter der Fahrbahn sparen kann.