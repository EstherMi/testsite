---
layout: "overview"
title: "Achter-Bahn zweispurig mit Kettentrieb"
date: 2019-12-17T19:44:59+01:00
legacy_id:
- categories/2742
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2742 --> 
Eine Bahn, auf der mehrere Fahrzeuge gleichzeitig vom selben Motor zentral angetrieben eine "8" fahren.