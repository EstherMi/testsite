---
layout: "image"
title: "Die Schwingen der Kreuzung"
date: "2010-02-21T13:26:38"
picture: "achterbahnzweispurig06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/26495
imported:
- "2019"
_4images_image_id: "26495"
_4images_cat_id: "1885"
_4images_user_id: "104"
_4images_image_date: "2010-02-21T13:26:38"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26495 -->
Die vier Schwingen sind alle gleich aufgebaut: Jeweils nur an einer Seite aufgehängt (der nächste senkrechte Träger muss ja schon das nächste Segment tragen).