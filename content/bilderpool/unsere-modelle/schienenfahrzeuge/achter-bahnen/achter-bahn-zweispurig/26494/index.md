---
layout: "image"
title: "Die Kreuzung"
date: "2010-02-21T13:26:38"
picture: "achterbahnzweispurig05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/26494
imported:
- "2019"
_4images_image_id: "26494"
_4images_cat_id: "1885"
_4images_user_id: "104"
_4images_image_date: "2010-02-21T13:26:38"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26494 -->
Das Problem bei dieser Art Bahn ist die Kreuzung: Wie kommt der Wagen kreuz und quer durch die Kreuzung und bleibt trotzdem zuverlässig auf Spur?

Diese vier Elemente dienen jeweils paarweise als Schienen, die aber, wenn der Wagen in der anderen Richtung durchfährt, einfach vom Wagen weggeklappt werden. Sie sind nämlich an waagerecht liegenden Achsen drehbar aufgehängt und unten schwerer als oben, so dass sie immer wieder so rum wie hier gezeigt zurückschwingen.

Wenn man genau hinguckt, hängen sie leicht nach außen geneigt, damit sie sich beim Schwingen garantiert nicht ins Gehege kommen - dazu später mehr.