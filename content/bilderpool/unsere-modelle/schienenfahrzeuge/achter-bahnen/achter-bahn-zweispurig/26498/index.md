---
layout: "image"
title: "Ausrichtung der Kreuzung gegenüber dem Kabelturm"
date: "2010-02-21T13:26:38"
picture: "achterbahnzweispurig09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/26498
imported:
- "2019"
_4images_image_id: "26498"
_4images_cat_id: "1885"
_4images_user_id: "104"
_4images_image_date: "2010-02-21T13:26:38"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26498 -->
Die ganze Bahn "arbeitet" während der Kurvenfahrten. Sie verschiebt sich mit der Zeit etwas auf glattem Untergrund. Damit der Wagen nicht am Kabelturm hängen bleibt, ist der wie hier gezeigt fest gegenüber der Kreuzung ausgerichtet.