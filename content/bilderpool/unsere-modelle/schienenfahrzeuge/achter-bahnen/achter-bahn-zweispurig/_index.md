---
layout: "overview"
title: "Achter-Bahn zweispurig"
date: 2019-12-17T19:44:55+01:00
legacy_id:
- categories/1885
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1885 --> 
Diese zweigleisige Bahn fährt endlos eine \"8\", ohne dass Akkus oder Stromschienen benötigt würden.