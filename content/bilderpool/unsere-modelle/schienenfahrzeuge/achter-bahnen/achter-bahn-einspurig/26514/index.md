---
layout: "image"
title: "Gesamtansicht"
date: "2010-02-23T21:27:16"
picture: "achterbahneinspurig1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/26514
imported:
- "2019"
_4images_image_id: "26514"
_4images_cat_id: "1888"
_4images_user_id: "104"
_4images_image_date: "2010-02-23T21:27:16"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26514 -->
Auch hier fährt die Bahn eine "8" und man kann Strom bequem von oben mit einem Kabel zuführen, weil es sich nicht verdrillt. Die Bahn benötigt 28 Sekunden für einen kompletten Durchgang.