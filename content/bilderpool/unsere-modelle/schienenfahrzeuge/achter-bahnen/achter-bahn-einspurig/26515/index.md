---
layout: "image"
title: "Die Kreuzung"
date: "2010-02-23T21:27:17"
picture: "achterbahneinspurig2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/26515
imported:
- "2019"
_4images_image_id: "26515"
_4images_cat_id: "1888"
_4images_user_id: "104"
_4images_image_date: "2010-02-23T21:27:17"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26515 -->
Der Balken ist ganz frei drehbar. Der Wagen muss ihn sich vor jeder Durchfahrt selber in die richtige Richtung stellen und kann dann drüberfahren.