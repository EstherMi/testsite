---
layout: "overview"
title: "Achter-Bahnen"
date: 2019-12-17T19:44:54+01:00
legacy_id:
- categories/1884
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1884 --> 
Bahnen, die eine "8" fahren und so ohne Akku und Stromschienen von außen versorgt werden können, ohne dass sich Kabel verdrillen.