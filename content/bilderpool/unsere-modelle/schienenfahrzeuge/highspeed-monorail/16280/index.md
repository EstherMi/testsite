---
layout: "image"
title: "Das Biest in Aktion"
date: "2008-11-16T16:09:14"
picture: "highspeedmonorail1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- details/16280
imported:
- "2019"
_4images_image_id: "16280"
_4images_cat_id: "1468"
_4images_user_id: "853"
_4images_image_date: "2008-11-16T16:09:14"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16280 -->
Es ist schnell... die Katze hat sich dann, nach einem vorsichtigen Blick um die Ecke, auch sichereren Gefilden zugewendet und denkt jetzt ein Zimmer weiter darüber nach, ob ihr Herrchen den Verstand verloren hat.

Video (vom Monorail, nicht von der Katze): www.flying-cat.de/ft/Monorail.wmv (3,6 MB)