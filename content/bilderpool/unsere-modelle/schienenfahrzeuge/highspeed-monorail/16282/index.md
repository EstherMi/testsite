---
layout: "image"
title: "Ladestellung"
date: "2008-11-16T16:09:14"
picture: "highspeedmonorail3.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- details/16282
imported:
- "2019"
_4images_image_id: "16282"
_4images_cat_id: "1468"
_4images_user_id: "853"
_4images_image_date: "2008-11-16T16:09:14"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16282 -->
Hier das angedockte Fahrzeug. Das Video sagt mehr als ein statisches Bild...