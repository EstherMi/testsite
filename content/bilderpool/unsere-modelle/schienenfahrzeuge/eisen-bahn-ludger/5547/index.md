---
layout: "image"
title: "Drehgestell Loren Waggon (Ansicht von unten)"
date: "2005-12-27T15:32:28"
picture: "DSCN0511.jpg"
weight: "49"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/5547
imported:
- "2019"
_4images_image_id: "5547"
_4images_cat_id: "482"
_4images_user_id: "184"
_4images_image_date: "2005-12-27T15:32:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5547 -->
