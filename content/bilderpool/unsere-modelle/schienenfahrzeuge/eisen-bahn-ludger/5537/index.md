---
layout: "image"
title: "vorderes Drehgestell (Ansicht von vorne)"
date: "2005-12-27T15:28:59"
picture: "DSCN0512.jpg"
weight: "39"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/5537
imported:
- "2019"
_4images_image_id: "5537"
_4images_cat_id: "482"
_4images_user_id: "184"
_4images_image_date: "2005-12-27T15:28:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5537 -->
