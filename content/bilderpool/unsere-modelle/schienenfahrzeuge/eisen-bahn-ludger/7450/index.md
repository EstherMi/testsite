---
layout: "image"
title: "elektrische Weiche mit Signal"
date: "2006-11-12T18:26:46"
picture: "DSCN1116.jpg"
weight: "1"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/7450
imported:
- "2019"
_4images_image_id: "7450"
_4images_cat_id: "482"
_4images_user_id: "184"
_4images_image_date: "2006-11-12T18:26:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7450 -->
Das Signal zeigt nicht die Weichenstellung an sondern ist "nur" eine Hilfe für mich damit ich die richtige Weiche umstellen kann.