---
layout: "image"
title: "Akku-Tender (4)"
date: "2006-01-30T17:02:49"
picture: "DSCN0677.jpg"
weight: "19"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/5699
imported:
- "2019"
_4images_image_id: "5699"
_4images_cat_id: "482"
_4images_user_id: "184"
_4images_image_date: "2006-01-30T17:02:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5699 -->
So sieht er etwas mehr nach "Tender" aus.