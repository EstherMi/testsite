---
layout: "image"
title: "noch ein Zug  ;-))"
date: "2006-10-30T18:55:46"
picture: "DSCN1078.jpg"
weight: "18"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/7292
imported:
- "2019"
_4images_image_id: "7292"
_4images_cat_id: "482"
_4images_user_id: "184"
_4images_image_date: "2006-10-30T18:55:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7292 -->
Natürlich gibt es auch ein Abstellgleis mit einem zweiten Zug