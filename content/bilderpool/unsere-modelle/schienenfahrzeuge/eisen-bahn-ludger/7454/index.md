---
layout: "image"
title: "etwas dunkel"
date: "2006-11-12T18:26:47"
picture: "DSCN1115.jpg"
weight: "5"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/7454
imported:
- "2019"
_4images_image_id: "7454"
_4images_cat_id: "482"
_4images_user_id: "184"
_4images_image_date: "2006-11-12T18:26:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7454 -->
damit man die Beleuchtung auch mal sieht