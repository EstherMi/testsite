---
layout: "image"
title: "Lokomotive 006"
date: "2004-12-05T10:22:02"
picture: "Lokomotive_006.JPG"
weight: "6"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/3390
imported:
- "2019"
_4images_image_id: "3390"
_4images_cat_id: "294"
_4images_user_id: "5"
_4images_image_date: "2004-12-05T10:22:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3390 -->
