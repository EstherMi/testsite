---
layout: "image"
title: "Antrieb"
date: "2007-12-03T23:30:28"
picture: "eisenbahn1_4.jpg"
weight: "9"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/12991
imported:
- "2019"
_4images_image_id: "12991"
_4images_cat_id: "1173"
_4images_user_id: "424"
_4images_image_date: "2007-12-03T23:30:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12991 -->
Für die bessere Haftung habe ich Gummibänder um die Reifen geklebt.
Vielleicht finde ich noch passende Gummibänder damit das Kleben wegfällt.

Die Reifen (Art.Nr. 34995) haben sich nicht bewährt, weil zu breit.