---
layout: "image"
title: "Achsen"
date: "2008-01-06T20:09:44"
picture: "eisenbahn4_3.jpg"
weight: "4"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/13287
imported:
- "2019"
_4images_image_id: "13287"
_4images_cat_id: "1173"
_4images_user_id: "424"
_4images_image_date: "2008-01-06T20:09:44"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13287 -->
Die Räder  aus Seilrolle 21 mit einem O-Ring umgeben