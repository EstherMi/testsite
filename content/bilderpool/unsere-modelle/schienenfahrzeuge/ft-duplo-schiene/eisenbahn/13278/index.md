---
layout: "image"
title: "Antrieb"
date: "2008-01-05T06:40:38"
picture: "eisenbahn3_2.jpg"
weight: "7"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/13278
imported:
- "2019"
_4images_image_id: "13278"
_4images_cat_id: "1173"
_4images_user_id: "424"
_4images_image_date: "2008-01-05T06:40:38"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13278 -->
Antrieb über Kegelzahnräder.