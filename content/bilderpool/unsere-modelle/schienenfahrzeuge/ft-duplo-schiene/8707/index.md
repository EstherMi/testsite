---
layout: "image"
title: "Güterlok von vorne"
date: "2007-01-28T08:55:14"
picture: "PICT1237.jpg"
weight: "4"
konstrukteure: 
- "Walter-Mario Graf"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/8707
imported:
- "2019"
_4images_image_id: "8707"
_4images_cat_id: "780"
_4images_user_id: "424"
_4images_image_date: "2007-01-28T08:55:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8707 -->
