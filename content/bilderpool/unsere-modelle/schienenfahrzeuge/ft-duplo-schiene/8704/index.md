---
layout: "image"
title: "Güterlok mit Wagen"
date: "2007-01-28T08:55:13"
picture: "PICT1230.jpg"
weight: "1"
konstrukteure: 
- "Walter-Mario Graf"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/8704
imported:
- "2019"
_4images_image_id: "8704"
_4images_cat_id: "780"
_4images_user_id: "424"
_4images_image_date: "2007-01-28T08:55:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8704 -->
