---
layout: "image"
title: "Versuchsschaltung für Loks"
date: "2007-01-28T08:55:14"
picture: "PICT1250.jpg"
weight: "9"
konstrukteure: 
- "Walter-Mario Graf"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/8712
imported:
- "2019"
_4images_image_id: "8712"
_4images_cat_id: "780"
_4images_user_id: "424"
_4images_image_date: "2007-01-28T08:55:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8712 -->
Hier sieht man die Schaltung die ich mit meinen "Graulingen" einem Doppel Flip-Flop und einem Relais aufgebaut habe.
Werde sie jetzt so verkleinern dass sie in einem Batteriegehäuse Platz hat.