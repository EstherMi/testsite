---
layout: "image"
title: "ft Lok auf Duplo-Schienen"
date: "2007-01-17T20:06:30"
picture: "PICT1201.jpg"
weight: "14"
konstrukteure: 
- "Walter-Mario Graf"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/8493
imported:
- "2019"
_4images_image_id: "8493"
_4images_cat_id: "780"
_4images_user_id: "424"
_4images_image_date: "2007-01-17T20:06:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8493 -->
