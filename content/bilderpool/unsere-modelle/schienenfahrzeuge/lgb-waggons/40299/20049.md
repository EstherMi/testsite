---
layout: "comment"
hidden: true
title: "20049"
date: "2015-01-16T00:34:08"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hallo Thomas,
bisher ist noch nichts gerutscht, aber wir haben auch noch nie 10 beladene Anhänger an eine Lok gehängt... Du hast recht, den B15 sollten wir drehen.
Danke für den Tipp!
Gruß, Dirk