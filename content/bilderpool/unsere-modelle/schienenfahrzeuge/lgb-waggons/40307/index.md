---
layout: "image"
title: "Viehwaggon - im Einsatz"
date: "2015-01-08T17:51:45"
picture: "lgbwaggons12.jpg"
weight: "12"
konstrukteure: 
- "Dirk Fox, Magnus Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/40307
imported:
- "2019"
_4images_image_id: "40307"
_4images_cat_id: "3022"
_4images_user_id: "1126"
_4images_image_date: "2015-01-08T17:51:45"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40307 -->
Viehwaggon beim Beladen (mit Personal)