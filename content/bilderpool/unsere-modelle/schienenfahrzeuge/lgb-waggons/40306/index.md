---
layout: "image"
title: "Viehwaggon - Detailansicht Fahrgestell"
date: "2015-01-08T17:51:45"
picture: "lgbwaggons11.jpg"
weight: "11"
konstrukteure: 
- "Dirk Fox, Magnus Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/40306
imported:
- "2019"
_4images_image_id: "40306"
_4images_cat_id: "3022"
_4images_user_id: "1126"
_4images_image_date: "2015-01-08T17:51:45"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40306 -->
Fahrgestell des Viehwaggons (Drehschemel auf Grundplatte 90 x 45) mit Kupplung