---
layout: "comment"
hidden: true
title: "20037"
date: "2015-01-15T15:44:11"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
An den Spurkränzen ist die besagte Kante noch dran, wenngleich es aus dem Winkel nicht wirklich gut zu sehen ist. Gibt das eigentlich keine Probleme an Weichen und so ohne den ausgleichenden Gummibelag?

Klasse Konstruktion. So möchte ich auch mal bauen können...