---
layout: "image"
title: "Langholzwagen - Gesamtansicht"
date: "2015-01-08T17:51:45"
picture: "lgbwaggons03.jpg"
weight: "3"
konstrukteure: 
- "Dirk Fox, Magnus Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/40298
imported:
- "2019"
_4images_image_id: "40298"
_4images_cat_id: "3022"
_4images_user_id: "1126"
_4images_image_date: "2015-01-08T17:51:45"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40298 -->
Gesamtansicht des Langholzwagens