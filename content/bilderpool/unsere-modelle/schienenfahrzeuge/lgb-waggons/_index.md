---
layout: "overview"
title: "LGB-Waggons"
date: 2019-12-17T19:45:24+01:00
legacy_id:
- categories/3022
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3022 --> 
Die LGB-Schienen passen genau ins fischertechnik- und Playmobil-Maß - ideale Voraussetzungen für die Entwicklung von Modellen, die beide Welten verbinden. Hier einige LGB-Waggons aus unserer jüngsten Fertigung.