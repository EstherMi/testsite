---
layout: "image"
title: "for Gijs...."
date: "2007-01-02T14:58:38"
picture: "fischertechnik_019.jpg"
weight: "2"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/8250
imported:
- "2019"
_4images_image_id: "8250"
_4images_cat_id: "761"
_4images_user_id: "371"
_4images_image_date: "2007-01-02T14:58:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8250 -->
