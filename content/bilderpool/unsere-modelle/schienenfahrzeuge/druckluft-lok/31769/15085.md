---
layout: "comment"
hidden: true
title: "15085"
date: "2011-09-11T09:24:56"
uploadBy:
- "bumpf"
license: "unknown"
imported:
- "2019"
---
Hallo Andreas

Geniales Modell,

Natürlich werde ich ein Gleis für dich freihalten.
Kannst du mir noch angeben mit wieviel Volt dein Kompressor läuft, damit ich den passenden Transformer mitnehmen kann.

Gruss bumpf