---
layout: "overview"
title: "Druckluft-Lok"
date: 2019-12-17T19:45:15+01:00
legacy_id:
- categories/2370
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2370 --> 
2 Zylinder  4 Takt Druckluft-Lok

Achsfolge: 2'B
Spurweite: 45mm
Kesseldruck: ?
Zugkraft: 1,5l Wasserflasche auf einem 4 Achser
Nur eine Fahrtrichtung
Weichen können noch nicht befahren werden
(nächste Ausbaustufe: el.Ventile, zwei Fahrtrichtungen)
