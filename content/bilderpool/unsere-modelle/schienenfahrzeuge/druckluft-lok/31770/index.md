---
layout: "image"
title: "Druckluft-Lok 4/9"
date: "2011-09-10T07:45:06"
picture: "druckluftlok4.jpg"
weight: "4"
konstrukteure: 
- "Ironhorse406"
fotografen:
- "Ironhorse406"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ironhorse406"
license: "unknown"
legacy_id:
- details/31770
imported:
- "2019"
_4images_image_id: "31770"
_4images_cat_id: "2370"
_4images_user_id: "497"
_4images_image_date: "2011-09-10T07:45:06"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31770 -->
