---
layout: "image"
title: "Das Triebdrehgestell"
date: "2011-06-26T19:48:17"
picture: "schwebebahnbauphaseeigentlich19.jpg"
weight: "19"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- details/30964
imported:
- "2019"
_4images_image_id: "30964"
_4images_cat_id: "2313"
_4images_user_id: "1322"
_4images_image_date: "2011-06-26T19:48:17"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30964 -->
Die Achsen, Räder und Motoren sind in einem 60 Grad Winkel zur Schiene aufgehängt.