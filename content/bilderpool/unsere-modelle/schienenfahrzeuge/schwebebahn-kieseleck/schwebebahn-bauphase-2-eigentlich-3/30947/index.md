---
layout: "image"
title: "Die 'Central Station'"
date: "2011-06-26T19:48:17"
picture: "schwebebahnbauphaseeigentlich02.jpg"
weight: "2"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- details/30947
imported:
- "2019"
_4images_image_id: "30947"
_4images_cat_id: "2313"
_4images_user_id: "1322"
_4images_image_date: "2011-06-26T19:48:17"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30947 -->
Die "Central Station" ist der ganz große Bahnhof mit Fahrkartenschalter und allem drum und dran.