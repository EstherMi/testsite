---
layout: "image"
title: "Laufachsendrehgestell"
date: "2011-06-26T19:48:17"
picture: "schwebebahnbauphaseeigentlich21.jpg"
weight: "21"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- details/30966
imported:
- "2019"
_4images_image_id: "30966"
_4images_cat_id: "2313"
_4images_user_id: "1322"
_4images_image_date: "2011-06-26T19:48:17"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30966 -->
Die Räder und Achsen sind in einem 60 Grad Winkel zur Schiene aufgehängt. Dieses Ende des Triebwagens ist das einzige an dem ich eine Kupplung anbringen konnte :-(