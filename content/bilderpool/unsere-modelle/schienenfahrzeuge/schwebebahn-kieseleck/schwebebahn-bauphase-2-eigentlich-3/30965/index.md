---
layout: "image"
title: "Das gesamte Gestell"
date: "2011-06-26T19:48:17"
picture: "schwebebahnbauphaseeigentlich20.jpg"
weight: "20"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- details/30965
imported:
- "2019"
_4images_image_id: "30965"
_4images_cat_id: "2313"
_4images_user_id: "1322"
_4images_image_date: "2011-06-26T19:48:17"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30965 -->
Hier sieht man zusätzlich die Aufhängung (ich konnte nicht näher ran, weil das Bild sonst unscharf gewesen wäre und ich glaube, dass die Makrofunktion an war).