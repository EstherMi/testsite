---
layout: "image"
title: "Mit Aufgang(1)"
date: "2011-06-10T09:06:27"
picture: "schwebebahnkieseleck23.jpg"
weight: "23"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- details/30829
imported:
- "2019"
_4images_image_id: "30829"
_4images_cat_id: "2300"
_4images_user_id: "1322"
_4images_image_date: "2011-06-10T09:06:27"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30829 -->
Der Aufgang an Station2.