---
layout: "image"
title: "Die Anlage"
date: "2011-06-10T09:06:27"
picture: "schwebebahnkieseleck13.jpg"
weight: "13"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- details/30819
imported:
- "2019"
_4images_image_id: "30819"
_4images_cat_id: "2300"
_4images_user_id: "1322"
_4images_image_date: "2011-06-10T09:06:27"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30819 -->
Die Anlage hat:
1.eine 15 Grad Kurve (aus zwei 7,5 Grad Winkelsteinen)
2.eine leichten Anstieg (in der Kurve)
3.2 Stationen