---
layout: "image"
title: "Gerade"
date: "2011-06-10T09:06:27"
picture: "schwebebahnkieseleck02.jpg"
weight: "2"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- details/30808
imported:
- "2019"
_4images_image_id: "30808"
_4images_cat_id: "2300"
_4images_user_id: "1322"
_4images_image_date: "2011-06-10T09:06:27"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30808 -->
Ein gerades Gleis in Station1. Kurven kann mit 7,5 Grad Winkelsteinen oder mit gebogenen Statikelementen realisieren (allerdings schafft der Wagen derartige Kurven nicht).