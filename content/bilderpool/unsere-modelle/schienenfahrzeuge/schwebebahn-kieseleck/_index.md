---
layout: "overview"
title: "Schwebebahn (Kieseleck)"
date: 2019-12-17T19:45:08+01:00
legacy_id:
- categories/2300
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2300 --> 
Ursprünglich wollte ich eine Achterbahn bauen, doch als es mit dem Lifthill nicht ganz klappte, habe ich eine Schwebebahn gebaut. Bis jetzt fährt die Bahn von Station 1 (Unterm Schreibtisch) bis zu Station 2 (Beim Sessel).Ich plane Station 2 zu einem Durchgangsbahnhof zu machen und die Strecke zu verlängern(ein Bw mit Wendevorrichtung?).
Inspiration: Achterbahn von Harald Steinhaus (Harald)