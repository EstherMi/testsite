---
layout: "image"
title: "Die Treibachsenaufhängung"
date: "2011-06-10T09:06:27"
picture: "schwebebahnkieseleck08.jpg"
weight: "8"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- details/30814
imported:
- "2019"
_4images_image_id: "30814"
_4images_cat_id: "2300"
_4images_user_id: "1322"
_4images_image_date: "2011-06-10T09:06:27"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30814 -->
Die Treibachsen sind in einem 60 Grad Winkel zur Schiene aufgehängt.