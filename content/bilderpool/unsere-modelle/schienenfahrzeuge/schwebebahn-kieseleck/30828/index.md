---
layout: "image"
title: "Impressionen(5)"
date: "2011-06-10T09:06:27"
picture: "schwebebahnkieseleck22.jpg"
weight: "22"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- details/30828
imported:
- "2019"
_4images_image_id: "30828"
_4images_cat_id: "2300"
_4images_user_id: "1322"
_4images_image_date: "2011-06-10T09:06:27"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30828 -->
Im Bahnhof.