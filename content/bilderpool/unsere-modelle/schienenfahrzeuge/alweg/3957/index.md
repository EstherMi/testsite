---
layout: "image"
title: "Stadtbahnwagen - Rohbau - 1.Teil(schraege Seitenansicht)"
date: "2005-04-03T16:53:17"
picture: "Stadtbahnwagen - Rohbau - 1.Teil(schraege Seitenansicht).jpg"
weight: "23"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/3957
imported:
- "2019"
_4images_image_id: "3957"
_4images_cat_id: "340"
_4images_user_id: "5"
_4images_image_date: "2005-04-03T16:53:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3957 -->
