---
layout: "image"
title: "Fahrbahnabschnitt - gerade"
date: "2005-04-03T16:53:16"
picture: "Fahrbahnabschnitt - gerade.JPG"
weight: "14"
konstrukteure: 
- "Daniel Nowicki"
fotografen:
- "Daniel Nowicki"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/3948
imported:
- "2019"
_4images_image_id: "3948"
_4images_cat_id: "340"
_4images_user_id: "5"
_4images_image_date: "2005-04-03T16:53:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3948 -->
Hallo !
 
Ich hatte vor "einigen" Tagen im FT-Forum den "ALWEG"-Thread eröffnet. Hier habe ich nun die Fotos meines Prototypen. Allerdings fehlen dem Antriebsgestell die Motoren (der eine Minimot ist mir "verreckt", hatte aber auch schon ein paar Jährchen auf dem Buckel) und nur einen Motor dranzulassen, wäre der Massesymmetrie des "Bogies" abträglich gewesen.
 
Immerhin habe ich allein an dem zweiachsigen Fahrgestell mehrere Tage rumgegrübelt, wie man diesen dicken Trumm dazu bringt, auf diesem zugegebenermaßen sehr schmalen Profil (auch in den sehr engen Kurven!) sicher aber auch leicht zu rollen (siehe Bilder - der große Bogie lenkt mit!) . - Bis jetzt habe ich keine negativen Erfahrungen (Entgleisungen, ganzer Zug kippt vom Beam, Bogie "frißt sich in der Kurve fest") gemacht. In der entgültigen Fassung soll mein "Steckenpferd" ein vollautomatischer (mit Türöffnemechanismus, IBIS-Anzeige, sonstiger Beleuchtung und ZUB (da muß ich noch einiges an Roboterharware "nachkaufen")) dreiteiliger Zweirichtungsstadtbahnwagen (also mit "Führerständen" an beiden Enden der Zuges) sein. Die Länge eines einzelnen Segments beträgt dann (siehe Fotos) zwei "große" Grundplatten (das mal drei - wird 'n' ziemlicher Tatzelwurm!).
 
Da ich aber bei Knobloch nun einiges nachbestellt habe (u.A. auch Pneumatik-Teile für den Türen und Wagenkupplungen, Lichtelemente ...), warte ich nun voller Vorfreude auf die "Brocken", um endlich mit dem Teil fertig zu werden. Außerdem wollte ich noch Fotos von einer vollmotorisierten (leider z.Zt. noch nicht möglich - Teilemangel) "Klappmesserweiche" bringen.
 
Die Fahrbahn habe ich für die Bilder noch aus Fischertechnik gefertigt (um zu zeigen, daß es möglich ist eine ALWEG komplett in FT zu bauen), im Garten meiner Eltern ist aber schon eine Großanlage im Bau (allerdings hier: Holzstützen und die Fahrbahn aus Plastikprofilen - weil billiger ... Geiz ist geil!).
  
Bilder vom Bau der Gartenbahn werden auch noch kommen. Muß noch mit meinem Kumpel einen Weg finden, den Plastikprofilen gleichmäßige Radien zu verpassen (Erhitzen, Biegen und Erstarren lassen).
 
Ich denke schon, das meine "spinnerte" Idee einen Eintrag/Thread in Eurem Bilderpool wert ist, zumal sich die Monorailgeschichte nicht nur auf den Bau von Fahrzeugen beschränkt. Ich denke da an ganze Bahnhöfe (mit Aufzügen und Rolltreppen?!? Allein so ein vollautomatisierter Haltepunkt wär ein Technikleckerbissen...), ein schönes großes Depot/Remise mit Schiebebühne/Gleisharfe, verschiedene Weichenformen (Flexi-, Klappmesser-, Schiebe- und Revolverweichen). Aber auch an Fahrzeugen denke ich, ist noch viel möglich (Niederflur ?!?, Doppelstock ?!?, diverse Servicefahrzeuge). Außerdem muß sich das nicht nur auf die eine Spurweite beschränken (größer, höher, breiter).
 
MfG
 
Daniel Nowicki