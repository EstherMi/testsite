---
layout: "image"
title: "Seitenansicht ohne Fenderskirt"
date: "2005-05-19T21:52:53"
picture: "PICT1267.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "supersongoku"
license: "unknown"
legacy_id:
- details/4160
imported:
- "2019"
_4images_image_id: "4160"
_4images_cat_id: "340"
_4images_user_id: "198"
_4images_image_date: "2005-05-19T21:52:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4160 -->
... und das verbirgt sich unter der Verkleidung ...