---
layout: "image"
title: "2-achsiger Bogie mit Durchgangsportal"
date: "2005-05-19T21:52:53"
picture: "PICT1258.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "supersongoku"
license: "unknown"
legacy_id:
- details/4158
imported:
- "2019"
_4images_image_id: "4158"
_4images_cat_id: "340"
_4images_user_id: "198"
_4images_image_date: "2005-05-19T21:52:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4158 -->
Auch am Bogie habe ich noch einige kleine Änderungen vorgenommen. Inzwischen hab ich es auch hingekriegt, die großen "Old School"- Motoren anzubringen.

Aus Montagegründen habe ich die Motoren allerdings "ausgehängt".