---
layout: "image"
title: "Der Stadtbahnwagen (Seitenansicht)"
date: "2005-11-23T17:29:51"
picture: "PICT2226.jpg"
weight: "3"
konstrukteure: 
- "Daniel Nowicki"
fotografen:
- "Daniel Nowicki"
keywords: ["ALWEG", "monorail", "Seattle", "Turin", "Hitachi", "Disney", "Bombardier"]
uploadBy: "supersongoku"
license: "unknown"
legacy_id:
- details/5353
imported:
- "2019"
_4images_image_id: "5353"
_4images_cat_id: "340"
_4images_user_id: "198"
_4images_image_date: "2005-11-23T17:29:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5353 -->
... und hier nochmal der "Stadtbahnwagen" in der Seitenansicht.