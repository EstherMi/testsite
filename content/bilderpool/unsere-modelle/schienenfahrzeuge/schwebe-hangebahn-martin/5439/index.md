---
layout: "image"
title: "Aufhängung Gondel"
date: "2005-11-30T22:50:56"
picture: "aufhaengung_gondel.jpg"
weight: "2"
konstrukteure: 
- "Martin W."
fotografen:
- "Martin W."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/5439
imported:
- "2019"
_4images_image_id: "5439"
_4images_cat_id: "470"
_4images_user_id: "373"
_4images_image_date: "2005-11-30T22:50:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5439 -->
Die Aufhängung der Gondel. Diese ist mit je einem Baustein 15 mit rotem Zapfen drehbar gemacht, um besser um die Kurve zu kommen.