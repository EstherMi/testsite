---
layout: "image"
title: "LGB- Cable-Car Poederoyen NL"
date: "2018-01-10T19:17:09"
picture: "lgbcablecar17.jpg"
weight: "17"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/47072
imported:
- "2019"
_4images_image_id: "47072"
_4images_cat_id: "3482"
_4images_user_id: "22"
_4images_image_date: "2018-01-10T19:17:09"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47072 -->
Youtube-link : 
https://www.youtube.com/watch?v=vPeT26sIquk&t=


Link zum 20MM - STELRING MET STELSCHROEF A2 : 
https://www.rvspaleis.nl/ringen/stelring-din-705/705-2-20g_1 


Link zum LGB Spur G Speichenradsatz Metall 67319 LGB:: 
https://www.haertle.de/Modelleisenbahn/Spur+G/Ersatzteile+Spur+G/LGB+67319+Metall+Speichenradsatz+2St+Spur+G.html