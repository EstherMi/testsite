---
layout: "comment"
hidden: true
title: "23893"
date: "2018-01-12T13:49:00"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Link zum 20MM - STELRING MET STELSCHROEF A2 :
https://www.rvspaleis.nl/ringen/stelring-din-705/705-2-20g_1


Link zum LGB  Spur G Speichenradsatz Metall 67319 LGB::
https://www.haertle.de/Modelleisenbahn/Spur+G/Ersatzteile+Spur+G/LGB+67319+Metall+Speichenradsatz+2St+Spur+G.html