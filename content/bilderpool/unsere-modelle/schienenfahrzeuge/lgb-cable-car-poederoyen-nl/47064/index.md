---
layout: "image"
title: "LGB- Cable-Car Poederoyen NL"
date: "2018-01-10T19:17:03"
picture: "lgbcablecar09.jpg"
weight: "9"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/47064
imported:
- "2019"
_4images_image_id: "47064"
_4images_cat_id: "3482"
_4images_user_id: "22"
_4images_image_date: "2018-01-10T19:17:03"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47064 -->
Youtube-link : 
https://www.youtube.com/watch?v=vPeT26sIquk&t=