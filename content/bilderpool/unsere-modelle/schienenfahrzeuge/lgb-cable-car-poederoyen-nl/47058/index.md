---
layout: "image"
title: "Wellington's Cable Car - New Zealand"
date: "2018-01-10T19:17:03"
picture: "lgbcablecar03.jpg"
weight: "3"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/47058
imported:
- "2019"
_4images_image_id: "47058"
_4images_cat_id: "3482"
_4images_user_id: "22"
_4images_image_date: "2018-01-10T19:17:03"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47058 -->
Link to a Ride on Wellington's Cable Car - New Zealand

https://www.youtube.com/watch?v=1SV7WUQtvko

https://www.wellingtoncablecar.co.nz/DEUTSCHE/STARTSEITE
