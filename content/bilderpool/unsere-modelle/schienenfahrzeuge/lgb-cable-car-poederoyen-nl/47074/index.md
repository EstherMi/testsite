---
layout: "image"
title: "LGB- Cable-Car Poederoyen NL"
date: "2018-01-10T19:17:09"
picture: "lgbcablecar19.jpg"
weight: "19"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/47074
imported:
- "2019"
_4images_image_id: "47074"
_4images_cat_id: "3482"
_4images_user_id: "22"
_4images_image_date: "2018-01-10T19:17:09"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47074 -->
Youtube-link : 
https://www.youtube.com/watch?v=vPeT26sIquk&t=