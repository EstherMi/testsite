---
layout: "image"
title: "Vorderseite"
date: "2010-06-14T14:39:14"
picture: "Motordraisine_19.jpg"
weight: "9"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/27521
imported:
- "2019"
_4images_image_id: "27521"
_4images_cat_id: "1970"
_4images_user_id: "765"
_4images_image_date: "2010-06-14T14:39:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27521 -->
