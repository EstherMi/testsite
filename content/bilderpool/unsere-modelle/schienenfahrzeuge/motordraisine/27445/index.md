---
layout: "image"
title: "von unten"
date: "2010-06-10T14:27:34"
picture: "Motordraisine_06.jpg"
weight: "17"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/27445
imported:
- "2019"
_4images_image_id: "27445"
_4images_cat_id: "1970"
_4images_user_id: "765"
_4images_image_date: "2010-06-10T14:27:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27445 -->
Blick auf die Unterseite. Gut zu erkennen ist der Antrieb. 4 Power Motoren 8:1 treiben die zwei Achsen über die Ketten an.
In der Mitte des Fahrzeugs der Fahrakku.