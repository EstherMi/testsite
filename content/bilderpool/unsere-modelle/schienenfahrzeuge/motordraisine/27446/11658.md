---
layout: "comment"
hidden: true
title: "11658"
date: "2010-06-11T18:58:50"
uploadBy:
- "mirose"
license: "unknown"
imported:
- "2019"
---
Hallo schnaggels,

ja, ist leider zu klein. Die Spurkranzhöhe sollte mindestens 5 mm betragen (bei Stahlrädern), die Stärke des Spurkranzes 3,8 mm.
Die Drehscheibe 60 hat ca. 62 mm Durchmesser, das ergäbe eine Spurkranzhöhe von nur mehr 3,5 mm, und das war mir zu gering.
Das Innenzahnrad ist aber 5 mm dick.
Die Abmessungen des Spurkranzes sollten aber eingehalten werden, um ein problemloses Befahren der Weichen zu ermöglichen.

Viele Grüße

Mirose
_