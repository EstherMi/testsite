---
layout: "image"
title: "Inneneinrichtung"
date: "2010-06-14T14:39:14"
picture: "Motordraisine_20.jpg"
weight: "10"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/27522
imported:
- "2019"
_4images_image_id: "27522"
_4images_cat_id: "1970"
_4images_user_id: "765"
_4images_image_date: "2010-06-14T14:39:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27522 -->
Der ft-Servo vor dem Empfänger betätigt den Schalter für die Hupe