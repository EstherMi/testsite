---
layout: "image"
title: "Blick aufs Rad"
date: "2010-06-10T14:27:34"
picture: "Motordraisine_04.jpg"
weight: "15"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/27443
imported:
- "2019"
_4images_image_id: "27443"
_4images_cat_id: "1970"
_4images_user_id: "765"
_4images_image_date: "2010-06-10T14:27:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27443 -->
vorne der Fahrregler (hinter,  den Verteilern), rechts davon der Empfänger der Fernsteuerung