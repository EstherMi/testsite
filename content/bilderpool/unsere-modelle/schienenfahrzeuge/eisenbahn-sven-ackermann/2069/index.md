---
layout: "image"
title: "Detail Kranarm"
date: "2004-01-15T21:31:52"
picture: "Skl8.jpg"
weight: "3"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sven Ackermann"
license: "unknown"
legacy_id:
- details/2069
imported:
- "2019"
_4images_image_id: "2069"
_4images_cat_id: "1097"
_4images_user_id: "93"
_4images_image_date: "2004-01-15T21:31:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2069 -->
Hier ein Bild des Kranarms im Detail.