---
layout: "image"
title: "Schienenkran"
date: "2004-01-15T21:31:52"
picture: "Kran5.jpg"
weight: "6"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sven Ackermann"
license: "unknown"
legacy_id:
- details/2072
imported:
- "2019"
_4images_image_id: "2072"
_4images_cat_id: "1097"
_4images_user_id: "93"
_4images_image_date: "2004-01-15T21:31:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2072 -->
Der ft-Schienenkran im fahrbereiten Zustand. Das Fahrzeug benötigt insgesamt drei Playmobil-Wagen als "Schutzwagen" für Ausleger und Gegengewichtsplattform.