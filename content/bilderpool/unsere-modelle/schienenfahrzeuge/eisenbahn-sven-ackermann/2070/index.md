---
layout: "image"
title: "Ansicht von vorne"
date: "2004-01-15T21:31:52"
picture: "Skl9.jpg"
weight: "4"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sven Ackermann"
license: "unknown"
legacy_id:
- details/2070
imported:
- "2019"
_4images_image_id: "2070"
_4images_cat_id: "1097"
_4images_user_id: "93"
_4images_image_date: "2004-01-15T21:31:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2070 -->
Der Skl mit ausgefahrenem Kranarm von vorne.