---
layout: "image"
title: "Türmechanik"
date: "2018-10-15T16:10:18"
picture: "standseilbahn19.jpg"
weight: "19"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48237
imported:
- "2019"
_4images_image_id: "48237"
_4images_cat_id: "3539"
_4images_user_id: "104"
_4images_image_date: "2018-10-15T16:10:18"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48237 -->
Die Minimotoren oben unterm Dach schwenken alle je vier Türen parallel auf oder zu. Endlagentaster gibt's keine, es wird einfach eine Sekunde lang Strom auf die Motoren gegeben.