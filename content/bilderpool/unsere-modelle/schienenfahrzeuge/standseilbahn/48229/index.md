---
layout: "image"
title: "Die Wagen von oben"
date: "2018-10-15T16:10:18"
picture: "standseilbahn11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48229
imported:
- "2019"
_4images_image_id: "48229"
_4images_cat_id: "3539"
_4images_user_id: "104"
_4images_image_date: "2018-10-15T16:10:18"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48229 -->
Man sieht die Befestigung des Seils unten.