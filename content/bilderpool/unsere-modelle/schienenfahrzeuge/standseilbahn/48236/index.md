---
layout: "image"
title: "Kurvenfahrt (4)"
date: "2018-10-15T16:10:18"
picture: "standseilbahn18.jpg"
weight: "18"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48236
imported:
- "2019"
_4images_image_id: "48236"
_4images_cat_id: "3539"
_4images_user_id: "104"
_4images_image_date: "2018-10-15T16:10:18"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48236 -->
Der rechte Wagen in Bergfahrt während der Überfahrt über die obere Weiche.