---
layout: "image"
title: "Türen geschlossen"
date: "2018-10-15T16:10:18"
picture: "standseilbahn21.jpg"
weight: "21"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48239
imported:
- "2019"
_4images_image_id: "48239"
_4images_cat_id: "3539"
_4images_user_id: "104"
_4images_image_date: "2018-10-15T16:10:18"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48239 -->
So sind die Türen zu. Die roten Lampen leuchten, genau wie während der Fahrt.