---
layout: "image"
title: "Wagen und Talstation"
date: "2018-10-15T16:10:18"
picture: "standseilbahn02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48220
imported:
- "2019"
_4images_image_id: "48220"
_4images_cat_id: "3539"
_4images_user_id: "104"
_4images_image_date: "2018-10-15T16:10:18"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48220 -->
Meine Schienenführung ist deutlich primitiver als Ralf Geerkens ausgefuchste Fassung, dafür gibt's hier automatisch öffnende und schließende Türen.

Der Strom für die Tür-Motoren wird in den Endstationen per Kontakt zugeführt; es verlaufen keine Kabel zu den Wagen.