---
layout: "overview"
title: "Standseilbahn"
date: 2019-12-17T19:45:30+01:00
legacy_id:
- categories/3539
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3539 --> 
Die Vorlagen von Peter Damen (https://www.ftcommunity.de/suche.php?suchbegriff=cable-car&R1=search_all&action=suche) und insbesondere die wunderbare Turmbergbahn von Ralf Geerken (https://www.ftcommunity.de/suche.php?suchbegriff=turmberg&R1=search_all&action=suche) weckten den Wunsch in mir, auch so eine Bahn zu bauen.