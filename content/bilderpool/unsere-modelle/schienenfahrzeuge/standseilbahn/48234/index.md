---
layout: "image"
title: "Kurvenfahrt (2)"
date: "2018-10-15T16:10:18"
picture: "standseilbahn16.jpg"
weight: "16"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48234
imported:
- "2019"
_4images_image_id: "48234"
_4images_cat_id: "3539"
_4images_user_id: "104"
_4images_image_date: "2018-10-15T16:10:18"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48234 -->
Der rechte Wagen fährt in Talfahrt gerade über die untere Weiche.