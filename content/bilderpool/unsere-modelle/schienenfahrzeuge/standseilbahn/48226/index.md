---
layout: "image"
title: "Steuerung"
date: "2018-10-15T16:10:18"
picture: "standseilbahn08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48226
imported:
- "2019"
_4images_image_id: "48226"
_4images_cat_id: "3539"
_4images_user_id: "104"
_4images_image_date: "2018-10-15T16:10:18"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48226 -->
Es hätte eine rein elektromechanische Steuerung mit einer Orgie von Schleifringen und Relais werden sollen, aber aus Zeitgründen vor der Convention 2018 wurde es dann doch die "billige" Lösung mit einem Robo Interface, das im Fuß untergebracht ist.