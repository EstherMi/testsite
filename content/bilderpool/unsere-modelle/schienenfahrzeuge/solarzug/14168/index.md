---
layout: "image"
title: "Getriebe"
date: "2008-04-04T17:10:20"
picture: "solarzug3.jpg"
weight: "3"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/14168
imported:
- "2019"
_4images_image_id: "14168"
_4images_cat_id: "1310"
_4images_user_id: "747"
_4images_image_date: "2008-04-04T17:10:20"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14168 -->
Auf diesem Bild sieht man das Getriebe.