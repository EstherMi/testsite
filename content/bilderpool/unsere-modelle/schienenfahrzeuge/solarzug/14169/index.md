---
layout: "image"
title: "Hintere Seite des Solarzugs."
date: "2008-04-04T17:10:20"
picture: "solarzug4.jpg"
weight: "4"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/14169
imported:
- "2019"
_4images_image_id: "14169"
_4images_cat_id: "1310"
_4images_user_id: "747"
_4images_image_date: "2008-04-04T17:10:20"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14169 -->
Hier sieht man den Solarzug von hinten.