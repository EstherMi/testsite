---
layout: "image"
title: "Seitenbild des Solarzuges"
date: "2008-04-04T17:10:20"
picture: "solarzug2.jpg"
weight: "2"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/14167
imported:
- "2019"
_4images_image_id: "14167"
_4images_cat_id: "1310"
_4images_user_id: "747"
_4images_image_date: "2008-04-04T17:10:20"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14167 -->
Hier sieht man die rechte Seite meines Solarzugs.