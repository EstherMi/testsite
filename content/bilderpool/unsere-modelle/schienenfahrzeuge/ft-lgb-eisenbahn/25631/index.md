---
layout: "image"
title: "Lukas der Lokführer"
date: "2009-11-02T21:41:41"
picture: "bumpf7.jpg"
weight: "7"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/25631
imported:
- "2019"
_4images_image_id: "25631"
_4images_cat_id: "1800"
_4images_user_id: "424"
_4images_image_date: "2009-11-02T21:41:41"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25631 -->
Das erste Projekt bei dem ich die ft-männchen verwenden kann. Passt gut zum LGB Massstab.