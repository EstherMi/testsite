---
layout: "image"
title: "Motor"
date: "2010-01-25T22:35:43"
picture: "mitmotorvonvorne.jpg"
weight: "5"
konstrukteure: 
- "kilo70"
fotografen:
- "kilo70"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kilo70"
license: "unknown"
legacy_id:
- details/26156
imported:
- "2019"
_4images_image_id: "26156"
_4images_cat_id: "1854"
_4images_user_id: "1071"
_4images_image_date: "2010-01-25T22:35:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26156 -->
die Metalschnecke treibt direkt das Plastikzahnrad am Ende der Schnecke an. Bis jetzt hat es sich noch nicht zerrupft.
Sieht jedenfalls ganz cool aus, wenn sich die Kardanwelle superschnell dreht, das Fahrzeug durch die Übersetzung aber eher langsam fährt :)