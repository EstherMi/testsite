---
layout: "image"
title: "Entkuppeln 1"
date: "2011-12-04T14:45:52"
picture: "waltermariograf1_3.jpg"
weight: "4"
konstrukteure: 
- "Bumpff"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/33609
imported:
- "2019"
_4images_image_id: "33609"
_4images_cat_id: "2339"
_4images_user_id: "424"
_4images_image_date: "2011-12-04T14:45:52"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33609 -->
Als an der Convention 2011 Dirks Sohn Magnus immer wieder auf den Tisch steigen musste  um die Wagen von der Lok zu trennen,
fasste ich den Entschluss eine automatische Kupplung zu entwickeln. 

