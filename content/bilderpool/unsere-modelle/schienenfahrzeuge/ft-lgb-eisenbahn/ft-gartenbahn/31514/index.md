---
layout: "image"
title: "ft-Gartenbahn"
date: "2011-08-02T21:43:02"
picture: "ftgartenbahn1.jpg"
weight: "18"
konstrukteure: 
- "Bumpf"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/31514
imported:
- "2019"
_4images_image_id: "31514"
_4images_cat_id: "2339"
_4images_user_id: "424"
_4images_image_date: "2011-08-02T21:43:02"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31514 -->
Innenleben bei beiden Loks gleich