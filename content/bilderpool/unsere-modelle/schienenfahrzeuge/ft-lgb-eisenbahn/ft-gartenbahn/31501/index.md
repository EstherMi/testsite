---
layout: "image"
title: "ft-Gartenbahn"
date: "2011-08-01T23:32:12"
picture: "bumpf6.jpg"
weight: "27"
konstrukteure: 
- "Bumpf"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/31501
imported:
- "2019"
_4images_image_id: "31501"
_4images_cat_id: "2339"
_4images_user_id: "424"
_4images_image_date: "2011-08-01T23:32:12"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31501 -->
Rangierlok