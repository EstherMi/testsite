---
layout: "image"
title: "Antrieb"
date: "2011-11-30T22:24:35"
picture: "waltermariograf5.jpg"
weight: "12"
konstrukteure: 
- "Bumpff"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/33593
imported:
- "2019"
_4images_image_id: "33593"
_4images_cat_id: "2339"
_4images_user_id: "424"
_4images_image_date: "2011-11-30T22:24:35"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33593 -->
Hier sieht man den Ritzel  besser.