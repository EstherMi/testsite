---
layout: "image"
title: "Antrieb"
date: "2011-11-30T22:24:35"
picture: "waltermariograf4_2.jpg"
weight: "11"
konstrukteure: 
- "Bumpf"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/33592
imported:
- "2019"
_4images_image_id: "33592"
_4images_cat_id: "2339"
_4images_user_id: "424"
_4images_image_date: "2011-11-30T22:24:35"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33592 -->
Kraftübertragung auf Antriebsachsen. Der Ritzel Z10 ist bearbeitet