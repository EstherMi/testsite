---
layout: "comment"
hidden: true
title: "15722"
date: "2011-11-21T00:34:41"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Das ist wirklich schön gebaut!

Beim Betrachten dieses Bildes habe ich ein ganz seltsames Gefühl, was mich an meine Kindheit erinnert. Ich habe irgend ein ähnliches Bild im Kopf (könnte Lego gewesen sein) und weiß noch, dass ich damals (wie hier) grübelte, wie das wohl gebaut ist (hier nämlich der Kessel mit der schwarzen Verkleidung). Genauer kann ich es leider nicht beschreiben, aber es ist sowas wie ein Déjà-vu, ganz komisches Gefühl, aber gut.

Gruß,
Stefan