---
layout: "image"
title: "Stellpult"
date: "2011-12-18T21:03:16"
picture: "waltermariograf3_4.jpg"
weight: "3"
konstrukteure: 
- "Bumpff"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/33719
imported:
- "2019"
_4images_image_id: "33719"
_4images_cat_id: "2339"
_4images_user_id: "424"
_4images_image_date: "2011-12-18T21:03:16"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33719 -->
Grün freie Fahrt