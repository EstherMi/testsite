---
layout: "image"
title: "Zugkompensation mit beladenen Wagen"
date: "2009-11-02T21:41:41"
picture: "bumpf4.jpg"
weight: "4"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/25628
imported:
- "2019"
_4images_image_id: "25628"
_4images_cat_id: "1800"
_4images_user_id: "424"
_4images_image_date: "2009-11-02T21:41:41"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25628 -->
Der Zug fährt nur eine Runde dann drehen die Räder wieder durch. Der Gummiring fällt vom Spurkranz.