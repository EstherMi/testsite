---
layout: "image"
title: "Zugkomposition"
date: "2009-11-02T21:41:40"
picture: "bumpf3.jpg"
weight: "3"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/25627
imported:
- "2019"
_4images_image_id: "25627"
_4images_cat_id: "1800"
_4images_user_id: "424"
_4images_image_date: "2009-11-02T21:41:40"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25627 -->
Sofort einige Wagen angekoppelt. Es fährt immer noch. Doch dann durchdrehende Räder. Schienen und Gummiring müssen gereinigt werden.