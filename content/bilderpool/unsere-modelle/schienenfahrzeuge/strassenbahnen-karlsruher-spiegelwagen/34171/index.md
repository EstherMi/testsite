---
layout: "image"
title: "Fahrgestell Spiegelwagen"
date: "2012-02-12T18:37:20"
picture: "karlsruherspiegelwagen6.jpg"
weight: "12"
konstrukteure: 
- "Magnus Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/34171
imported:
- "2019"
_4images_image_id: "34171"
_4images_cat_id: "2531"
_4images_user_id: "1126"
_4images_image_date: "2012-02-12T18:37:20"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34171 -->
Wie im Original erfolgt der Antrieb über zwei Achsen mit je einem eigenen Motor (Power-Motor 1:20). Die Nähmaschinenfüßchen dienen als Schleifkontakte.