---
layout: "image"
title: "Zielfilmkasten 2"
date: "2012-02-18T19:58:49"
picture: "Zielfilmkasten_2.jpg"
weight: "2"
konstrukteure: 
- "Magnus Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/34240
imported:
- "2019"
_4images_image_id: "34240"
_4images_cat_id: "2531"
_4images_user_id: "1126"
_4images_image_date: "2012-02-18T19:58:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34240 -->
Hinter dem Transparentpapier sind zwei (LED-) Leuchtwürfel mit den alten weißen Kappen montiert, die eine schöne helle, aber diffuse Hindergrundbeleuchtung erzeugen.