---
layout: "image"
title: "Heckansicht Spiegelwagen"
date: "2012-02-12T18:37:20"
picture: "karlsruherspiegelwagen7.jpg"
weight: "13"
konstrukteure: 
- "Magnus Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/34172
imported:
- "2019"
_4images_image_id: "34172"
_4images_cat_id: "2531"
_4images_user_id: "1126"
_4images_image_date: "2012-02-12T18:37:20"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34172 -->
An den Triebwagen können bis zu drei Beiwagen (nahezu baugleich, ohne Stromabnehmer und Antrieb) angehängt werden.