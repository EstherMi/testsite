---
layout: "image"
title: "Frontansicht Spiegelwagen"
date: "2012-02-12T18:37:20"
picture: "karlsruherspiegelwagen8.jpg"
weight: "14"
konstrukteure: 
- "Magnus Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/34173
imported:
- "2019"
_4images_image_id: "34173"
_4images_cat_id: "2531"
_4images_user_id: "1126"
_4images_image_date: "2012-02-12T18:37:20"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34173 -->
Die Anzeige des Fahrziels im sog. "Zielfilmkasten" ist rückwärtig beleuchtet und enthält sechs Ziele (drei Strecken), die vom Fahrer manuell per Kurbel eingestellt werden. Der Frontstrahler (Leuchtdiode) wird durch eine Linse fokussiert.