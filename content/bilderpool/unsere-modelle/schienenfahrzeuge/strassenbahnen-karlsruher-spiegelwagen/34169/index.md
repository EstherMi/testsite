---
layout: "image"
title: "Seitenansicht Spiegelwagen (rechts)"
date: "2012-02-12T18:37:20"
picture: "karlsruherspiegelwagen4.jpg"
weight: "10"
konstrukteure: 
- "Magnus Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/34169
imported:
- "2019"
_4images_image_id: "34169"
_4images_cat_id: "2531"
_4images_user_id: "1126"
_4images_image_date: "2012-02-12T18:37:20"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34169 -->
Die Schiebetüre für den Ein- und Ausstieg wurde vom Schaffner bedient, der auch das Abfahrtsignal gab und die Fahrscheine kontrollierte.