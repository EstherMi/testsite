---
layout: "comment"
hidden: true
title: "22754"
date: "2016-11-20T19:43:53"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Aber hier sieht man endlich mal diese seltsame Statiklasche nützlich verbaut! Man kann sie auch als Sperrklinke für Kranseilkurbelzahnräder verwenden. Aber hat noch jemand die irgendwann mal wirklich gebraucht? Gibt's da coole Anwendungen?

Gruß,
Stefan