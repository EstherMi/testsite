---
layout: "image"
title: "Zielfilmkasten 1"
date: "2012-02-18T19:58:49"
picture: "Zielfilmkasten_1.jpg"
weight: "1"
konstrukteure: 
- "Magnus Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/34239
imported:
- "2019"
_4images_image_id: "34239"
_4images_cat_id: "2531"
_4images_user_id: "1126"
_4images_image_date: "2012-02-18T19:58:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34239 -->
Hier die versprochenen Details: Der Zielfilm mit den sechs Zielen besteht aus einem sechs Zentimeter breiten Streifen Transparantpapier. Das ist oben und unten mit einem Tesafilmstreifen an den von der Kurbel angetriebenen Rastachse befestigt.