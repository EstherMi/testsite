---
layout: "image"
title: "Seitenansicht Spiegelwagen (links)"
date: "2012-02-12T18:37:20"
picture: "karlsruherspiegelwagen3.jpg"
weight: "9"
konstrukteure: 
- "Magnus Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/34168
imported:
- "2019"
_4images_image_id: "34168"
_4images_cat_id: "2531"
_4images_user_id: "1126"
_4images_image_date: "2012-02-12T18:37:20"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34168 -->
Magnus' Modell entspricht der späteren Version des Spiegelwagens ohne Spiegel an den Fensterholmen. Gut erkennbar das Schleppdach, das im Original-Wagen aus in fette Bleiweißfarbe gedrücktem Segeltuch bestand. Links und rechts waren kippbare Lüftungsfenster angebracht; die Seitenflächen wurden für Werbetafeln genutzt.