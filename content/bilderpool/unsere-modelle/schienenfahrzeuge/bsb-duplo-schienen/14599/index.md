---
layout: "image"
title: "Einsatz in Industrieanlage"
date: "2008-05-30T22:39:35"
picture: "bsb8.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/14599
imported:
- "2019"
_4images_image_id: "14599"
_4images_cat_id: "1343"
_4images_user_id: "424"
_4images_image_date: "2008-05-30T22:39:35"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14599 -->
