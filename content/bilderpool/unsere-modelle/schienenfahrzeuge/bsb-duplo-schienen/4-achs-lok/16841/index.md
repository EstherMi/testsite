---
layout: "image"
title: "Ansicht 4-Achs Lok"
date: "2009-01-02T21:08:42"
picture: "bumpf1.jpg"
weight: "1"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/16841
imported:
- "2019"
_4images_image_id: "16841"
_4images_cat_id: "1519"
_4images_user_id: "424"
_4images_image_date: "2009-01-02T21:08:42"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16841 -->
Endlich fährt sie nach Wunsch