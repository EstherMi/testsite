---
layout: "image"
title: "Kupplung"
date: "2008-05-30T22:39:35"
picture: "bsb6.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/14597
imported:
- "2019"
_4images_image_id: "14597"
_4images_cat_id: "1343"
_4images_user_id: "424"
_4images_image_date: "2008-05-30T22:39:35"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14597 -->
Der Gummi verhindert ein abknicken der Kupplung