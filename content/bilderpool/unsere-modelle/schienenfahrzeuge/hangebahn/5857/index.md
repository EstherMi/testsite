---
layout: "image"
title: "Bahn"
date: "2006-03-12T12:15:55"
picture: "Fischertechnik_018.jpg"
weight: "5"
konstrukteure: 
- "Jakob Wolf"
fotografen:
- "Jakob Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FFBBII"
license: "unknown"
legacy_id:
- details/5857
imported:
- "2019"
_4images_image_id: "5857"
_4images_cat_id: "506"
_4images_user_id: "420"
_4images_image_date: "2006-03-12T12:15:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5857 -->
