---
layout: "image"
title: "GTW 3"
date: "2012-05-29T02:50:20"
picture: "bumpf3.jpg"
weight: "3"
konstrukteure: 
- "Bumpf"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/35010
imported:
- "2019"
_4images_image_id: "35010"
_4images_cat_id: "2593"
_4images_user_id: "424"
_4images_image_date: "2012-05-29T02:50:20"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35010 -->
Nachdem alle Tests erfolgreich verliefen,  geht es jetzt an dieFertigstellungsarbeiten (zB. Licht, Bestuhlung, Verkleidung etc.)