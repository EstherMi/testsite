---
layout: "image"
title: "GTW 2"
date: "2012-05-29T02:50:20"
picture: "bumpf2.jpg"
weight: "2"
konstrukteure: 
- "Bumpf"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/35009
imported:
- "2019"
_4images_image_id: "35009"
_4images_cat_id: "2593"
_4images_user_id: "424"
_4images_image_date: "2012-05-29T02:50:20"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35009 -->
Testfahrten im Innenhof