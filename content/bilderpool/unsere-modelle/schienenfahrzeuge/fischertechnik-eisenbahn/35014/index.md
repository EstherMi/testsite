---
layout: "image"
title: "Fahrzeugdaten"
date: "2012-05-29T02:50:20"
picture: "bumpf7.jpg"
weight: "7"
konstrukteure: 
- "Stadlerrail-Bumpf"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/35014
imported:
- "2019"
_4images_image_id: "35014"
_4images_cat_id: "2593"
_4images_user_id: "424"
_4images_image_date: "2012-05-29T02:50:20"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35014 -->
Vergleich Orginal - Nachbau