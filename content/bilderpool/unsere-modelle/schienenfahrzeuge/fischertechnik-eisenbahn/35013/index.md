---
layout: "image"
title: "GTW Orginal"
date: "2012-05-29T02:50:20"
picture: "bumpf6.jpg"
weight: "6"
konstrukteure: 
- "Stadlerrail"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/35013
imported:
- "2019"
_4images_image_id: "35013"
_4images_cat_id: "2593"
_4images_user_id: "424"
_4images_image_date: "2012-05-29T02:50:20"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35013 -->
Biel-Täuffelen-Ins-Bahn (BTI