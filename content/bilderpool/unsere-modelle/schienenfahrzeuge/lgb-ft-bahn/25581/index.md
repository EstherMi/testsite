---
layout: "image"
title: "Ansicht von Unten"
date: "2009-10-29T11:59:32"
picture: "2009_10_291.jpg"
weight: "1"
konstrukteure: 
- "Martin W."
fotografen:
- "Martin W."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/25581
imported:
- "2019"
_4images_image_id: "25581"
_4images_cat_id: "1797"
_4images_user_id: "373"
_4images_image_date: "2009-10-29T11:59:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25581 -->
Falls die vorhandenen Bilder nicht zum Nachbau reichen, mache ich auch gerne noch ein Paar im halbzerlegten Zustand.
Achja, natürlich sind beim linken Drehgestell die Achsen zu lang, fragt mich nicht, wieso ich da nicht die Kürzeren genommen habe...