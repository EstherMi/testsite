---
layout: "image"
title: "Personenwagen"
date: "2009-10-29T11:55:13"
picture: "lgb1.jpg"
weight: "2"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/25578
imported:
- "2019"
_4images_image_id: "25578"
_4images_cat_id: "1797"
_4images_user_id: "373"
_4images_image_date: "2009-10-29T11:55:13"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25578 -->
Auf Drehgestellen mit Stromabnahme. Es fehlen noch eine gescheite Kupplung, die Beleuchtung und die Inneneinrichtung. Natürlich, wie auch im echten Leben oft zu finden, mit Werbeaufdruck ;-)