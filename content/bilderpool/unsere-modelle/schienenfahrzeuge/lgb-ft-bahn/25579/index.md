---
layout: "image"
title: "Drehgestell mit Stromabnahme"
date: "2009-10-29T11:55:14"
picture: "lgb2.jpg"
weight: "4"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/25579
imported:
- "2019"
_4images_image_id: "25579"
_4images_cat_id: "1797"
_4images_user_id: "373"
_4images_image_date: "2009-10-29T11:55:14"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25579 -->
Sehr kompakt, aber trotzdem stabil. Funktioniert erstaunlich gut, sobald Gewicht drauf kommt und die Räder zur Stromabnahme an die Schienen gedrückt werden. Das einzige nicht-original-Teil sind die Achsen zur Stromabnahme, da sind 30er zu lang. Also musste ich hier Messingachsen mit 22mm Länge nehmen. Außerdem sind die Federn halbierte ft-Federn, Mit Ganzen ging es einfach nicht gescheit *AndieDeckeschielundunschuldigpfeif*