---
layout: "image"
title: "Drehgestell ohne Stromabnahme"
date: "2009-10-29T11:55:14"
picture: "lgb3.jpg"
weight: "5"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/25580
imported:
- "2019"
_4images_image_id: "25580"
_4images_cat_id: "1797"
_4images_user_id: "373"
_4images_image_date: "2009-10-29T11:55:14"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25580 -->
Zum Vergleich das Ganze jetzt nochmal ohne Schleifer.