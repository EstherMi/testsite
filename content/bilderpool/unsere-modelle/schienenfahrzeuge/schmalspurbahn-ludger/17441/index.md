---
layout: "image"
title: "große Dampflok"
date: "2009-02-19T11:53:06"
picture: "DSCN2614.jpg"
weight: "34"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/17441
imported:
- "2019"
_4images_image_id: "17441"
_4images_cat_id: "1568"
_4images_user_id: "184"
_4images_image_date: "2009-02-19T11:53:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17441 -->
Hier habe ich eine Aufnahme gemacht auf der man die Drehpunkte gut erkennen kann.