---
layout: "image"
title: "Waggon für 45mm Gleis von unten"
date: "2009-02-20T09:47:04"
picture: "DSCN2621.jpg"
weight: "26"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/17463
imported:
- "2019"
_4images_image_id: "17463"
_4images_cat_id: "1568"
_4images_user_id: "184"
_4images_image_date: "2009-02-20T09:47:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17463 -->
Die Führung liegt zwischen den Gleisen. Die kleinen Räder werden super von den Statikträgern geführt.