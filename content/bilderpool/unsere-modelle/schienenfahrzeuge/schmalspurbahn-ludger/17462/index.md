---
layout: "image"
title: "Test mit einer anderen Spurweite"
date: "2009-02-20T09:47:03"
picture: "DSCN2620.jpg"
weight: "25"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/17462
imported:
- "2019"
_4images_image_id: "17462"
_4images_cat_id: "1568"
_4images_user_id: "184"
_4images_image_date: "2009-02-20T09:47:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17462 -->
Hier habe ich eine Weite von 45mm gewählt. Die Führung des Waggons liegt in der Mitte.
Die Konstruktion ist fast die gleiche wie beim ersten Modell.
Auf der Geraden und in den ersten Kurvenabschnitten überhaupt kein Problem.
Nur, ich bekomme keinen Vollkreis hin. Ca. 8 cm fehlen...