---
layout: "image"
title: "Prototyp einer 30mm Weiche"
date: "2009-02-20T09:47:04"
picture: "DSCN2623.jpg"
weight: "27"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/17464
imported:
- "2019"
_4images_image_id: "17464"
_4images_cat_id: "1568"
_4images_user_id: "184"
_4images_image_date: "2009-02-20T09:47:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17464 -->
Hier noch eine Handweiche.
Die soll aber später wieder elektrisch gestellt werden.
Als Vorbild habe ich mir die Weiche des Transrapid ausgewählt.