---
layout: "image"
title: "kleine Dampflok"
date: "2009-02-19T11:53:05"
picture: "DSCN2609.jpg"
weight: "30"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/17437
imported:
- "2019"
_4images_image_id: "17437"
_4images_cat_id: "1568"
_4images_user_id: "184"
_4images_image_date: "2009-02-19T11:53:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17437 -->
Erster Versuch.
Leider sieht sie nur etwas nach Dampflok aus.
Es ist ganz schön kompliziert auf so engem Raum ein gut aussehendes Modell zu bauen.