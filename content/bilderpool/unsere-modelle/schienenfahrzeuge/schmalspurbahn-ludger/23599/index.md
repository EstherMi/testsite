---
layout: "image"
title: "sehr große Dampflok"
date: "2009-04-05T15:40:20"
picture: "DSCN2673.jpg"
weight: "8"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/23599
imported:
- "2019"
_4images_image_id: "23599"
_4images_cat_id: "1568"
_4images_user_id: "184"
_4images_image_date: "2009-04-05T15:40:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23599 -->
Seitenansicht