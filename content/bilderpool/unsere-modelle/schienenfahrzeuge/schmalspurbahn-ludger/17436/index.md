---
layout: "image"
title: "Gleis"
date: "2009-02-19T11:53:05"
picture: "DSCN2610.jpg"
weight: "29"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/17436
imported:
- "2019"
_4images_image_id: "17436"
_4images_cat_id: "1568"
_4images_user_id: "184"
_4images_image_date: "2009-02-19T11:53:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17436 -->
3 cm Breit. Größer wollte ich nicht bauen.