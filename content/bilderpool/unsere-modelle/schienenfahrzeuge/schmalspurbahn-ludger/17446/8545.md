---
layout: "comment"
hidden: true
title: "8545"
date: "2009-02-19T12:36:06"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Gratulation zu so viel Mut und Phantasie!!! Eine tolle Idee, und sehr elegant umgesetzt. Ich freue mich auf weitere Modelle!

Schöner fände ich, wenn die nach unten ragenden Teile zum Spurführen immer gleich und quasi standardisiert wären. So sieht es noch sehr nach Bastellösung aus, aber das wird sicher noch... ;o)

Unschön finde ich das Zerstören von FT-Teilen. Es gibt immer Lösungen ohne Teilemodifikationen! Ich mag es auch einfach nicht, weil es zuuu naheliegend ist. Wenn was nicht passt, einfach ein Teil zerstören? Nein, das gefällt mir nicht...

Gruß, Thomas