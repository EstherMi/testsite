---
layout: "image"
title: "Drehgestell"
date: "2009-02-19T11:53:06"
picture: "DSCN2616.jpg"
weight: "36"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: ["modding"]
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/17443
imported:
- "2019"
_4images_image_id: "17443"
_4images_cat_id: "1568"
_4images_user_id: "184"
_4images_image_date: "2009-02-19T11:53:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17443 -->
