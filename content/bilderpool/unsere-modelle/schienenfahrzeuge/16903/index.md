---
layout: "image"
title: "LIM 0.01"
date: "2009-01-04T20:05:25"
picture: "lim1.jpg"
weight: "5"
konstrukteure: 
- "Rob van Oostenbrugge"
fotografen:
- "Rob van Oostenbrugge"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Tovenaar"
license: "unknown"
legacy_id:
- details/16903
imported:
- "2019"
_4images_image_id: "16903"
_4images_cat_id: "481"
_4images_user_id: "814"
_4images_image_date: "2009-01-04T20:05:25"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16903 -->
Erste versuch lineär motor
12 E-magnete symetrische spannungsversorgung und 3 taster unter 120' um eine phase verschobene spannung an die e-magnete zu liefern.
