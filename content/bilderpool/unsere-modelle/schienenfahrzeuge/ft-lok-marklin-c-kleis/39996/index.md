---
layout: "image"
title: "FT-Lok auf Märklin (C-Kleis)"
date: "2014-12-27T18:51:12"
picture: "ftlokaufmaerklinckleis04.jpg"
weight: "4"
konstrukteure: 
- "Wolfgang"
fotografen:
- "Wolfgang"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "webstar"
license: "unknown"
legacy_id:
- details/39996
imported:
- "2019"
_4images_image_id: "39996"
_4images_cat_id: "3008"
_4images_user_id: "1355"
_4images_image_date: "2014-12-27T18:51:12"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39996 -->
An den Märklin-Schleifer wird ein Draht angelötet und an diesen Draht wird einen FT-Stecker befestigt
Dann wird mittels der Klebepistol Klebstoff zwischen den beiden Kunsstoffstreben vom Zug angebracht.
Danach wird der Märklin-Schleifer an den Zug angeklebt. Mit einer kleinen Schraube wird noch dee Schleifer in den warmen/flüssigen Kleber angeschraubt.