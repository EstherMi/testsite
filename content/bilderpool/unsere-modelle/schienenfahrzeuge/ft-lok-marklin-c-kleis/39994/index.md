---
layout: "image"
title: "FT-Lok auf Märklin (C-Kleis)"
date: "2014-12-27T18:51:12"
picture: "ftlokaufmaerklinckleis02.jpg"
weight: "2"
konstrukteure: 
- "Wolfgang"
fotografen:
- "Wolfgang"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "webstar"
license: "unknown"
legacy_id:
- details/39994
imported:
- "2019"
_4images_image_id: "39994"
_4images_cat_id: "3008"
_4images_user_id: "1355"
_4images_image_date: "2014-12-27T18:51:12"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39994 -->
Zerlegte Lok