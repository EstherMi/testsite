---
layout: "overview"
title: "FT-Lok auf Märklin (C-Kleis)"
date: 2019-12-17T19:45:22+01:00
legacy_id:
- categories/3008
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3008 --> 
Auf den folgenden Seiten seht ihr wie ihr eine FT-Lok so umbaut, dass diese auf Märklin-Schienen fährt. 
Dabei wird auch eine Schienen so umgebaut, dass der FT-Trafo (505283 Power Set) verwendet werden kann.

Ihr braucht dazu

.) Klebepistol
.) Kabeln
.) FT-Stecker (3,5mm Durchmesser)
.) Märklin-Schleifer (50,2 mm Nr: 7164)
.) Kleine Schraube