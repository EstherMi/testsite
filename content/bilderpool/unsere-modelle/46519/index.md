---
layout: "image"
title: "Auslagern der trockenen Kekse"
date: "2017-09-30T11:52:18"
picture: "12_Auslagern.jpg"
weight: "12"
konstrukteure: 
- "Fabian, Max, Christian & Stefan"
fotografen:
- "Christian und Stefan"
keywords: ["Keksdrucker"]
uploadBy: "ftpi"
license: "unknown"
legacy_id:
- details/46519
imported:
- "2019"
_4images_image_id: "46519"
_4images_cat_id: "10"
_4images_user_id: "2611"
_4images_image_date: "2017-09-30T11:52:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46519 -->
Keks im Trockenlager abholen, in die zweite Reihe oberhalb des Auswurfs fahren und nach unten fahren bis der Keks vom Greifer rutscht.