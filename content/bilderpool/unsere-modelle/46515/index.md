---
layout: "image"
title: "Vertikalantrieb"
date: "2017-09-30T11:52:18"
picture: "08_Vertikalantrieb.jpg"
weight: "8"
konstrukteure: 
- "Fabian, Max, Christian & Stefan"
fotografen:
- "Christian und Stefan"
keywords: ["Hubgetriebe", "Vertikalantrieb"]
uploadBy: "ftpi"
license: "unknown"
legacy_id:
- details/46515
imported:
- "2019"
_4images_image_id: "46515"
_4images_cat_id: "10"
_4images_user_id: "2611"
_4images_image_date: "2017-09-30T11:52:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46515 -->
Mit zwei verbundenen Hubgetrieben ist der Vertikalantrieb auf Zahnstangen präzise genug. Das zweite Getriebe ist nicht angetrieben - es kann ein defektes Getriebe verwendet werden.