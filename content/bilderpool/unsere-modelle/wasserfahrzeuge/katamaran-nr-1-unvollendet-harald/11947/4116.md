---
layout: "comment"
hidden: true
title: "4116"
date: "2007-09-24T20:28:45"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
fitec, danke sehr :-)

ffcoe, ja du hast vollkommen Recht. Ich hab die Zeitung durch den Reißwolf (ein Billig-Teil, erzeugt lange Streifen) gejagt und dann noch quer in Stücke geschnitten. So hatten wir das im Werkunterricht mal gemacht, aber da sollte ja auch eine Skulptur draus werden, keine eher flächenhafte Schale.
Mit ganzen Blättern oder Streifen wäre es doch noch einen Versuch wert.

Gruß,
Harald