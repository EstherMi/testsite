---
layout: "image"
title: "Katamaran03.JPG"
date: "2007-09-24T18:49:41"
picture: "Katamaran03.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/11944
imported:
- "2019"
_4images_image_id: "11944"
_4images_cat_id: "1070"
_4images_user_id: "4"
_4images_image_date: "2007-09-24T18:49:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11944 -->
Die beiden Ruder werden über Schnecken m1 gesteuert. Der PowerMot hat damit nichts zu tun, er treibt den linken Propeller über die senkrecht verlaufende Welle an.