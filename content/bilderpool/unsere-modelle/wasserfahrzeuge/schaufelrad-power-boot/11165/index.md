---
layout: "image"
title: "nächster Versuch - Bild 1"
date: "2007-07-20T21:56:05"
picture: "schaufelradpowerboot1_3.jpg"
weight: "3"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- details/11165
imported:
- "2019"
_4images_image_id: "11165"
_4images_cat_id: "1001"
_4images_user_id: "120"
_4images_image_date: "2007-07-20T21:56:05"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11165 -->
alles um einen Baustein nach vorn, hintere Verstrebung weg, Spritzwasserschutz