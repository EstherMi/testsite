---
layout: "image"
title: "nächster Versuch - Bild 2"
date: "2007-07-20T22:01:02"
picture: "schaufelradpowerboot1_4.jpg"
weight: "4"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- details/11166
imported:
- "2019"
_4images_image_id: "11166"
_4images_cat_id: "1001"
_4images_user_id: "120"
_4images_image_date: "2007-07-20T22:01:02"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11166 -->
ein Zusatzschwimmer aus 2 der alten Bootskörper ist trotzdem nötig, bei Rückwärtsfahrt schwappt sonst immer noch Wasser rein