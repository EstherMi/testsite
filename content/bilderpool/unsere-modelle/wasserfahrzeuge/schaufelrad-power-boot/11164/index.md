---
layout: "image"
title: "erster Versuch - Bild 4"
date: "2007-07-20T21:47:37"
picture: "schaufelradpowerboot1_2.jpg"
weight: "2"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- details/11164
imported:
- "2019"
_4images_image_id: "11164"
_4images_cat_id: "1001"
_4images_user_id: "120"
_4images_image_date: "2007-07-20T21:47:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11164 -->
Schaufel Detail, ohne Freilaufnaben wäre die Reibung auf den Achshaltern zu groß