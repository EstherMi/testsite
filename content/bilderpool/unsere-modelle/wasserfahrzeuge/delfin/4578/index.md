---
layout: "image"
title: "Delfin2-07.JPG"
date: "2005-08-12T14:07:29"
picture: "Delfin2-07.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4578
imported:
- "2019"
_4images_image_id: "4578"
_4images_cat_id: "313"
_4images_user_id: "4"
_4images_image_date: "2005-08-12T14:07:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4578 -->
Der Antrieb für das Ruder endet noch in der Luft.