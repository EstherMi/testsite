---
layout: "image"
title: "Delfin2-01.JPG"
date: "2005-08-12T14:07:29"
picture: "Delfin2-01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4577
imported:
- "2019"
_4images_image_id: "4577"
_4images_cat_id: "313"
_4images_user_id: "4"
_4images_image_date: "2005-08-12T14:07:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4577 -->
Ein bisschen "Modellpflege": Der Rumpf ist etwas gestreckt worden, jetzt sind Ruder und Schraube dran und außerdem ein Kiel, der zur Seite geklappt werden kann.