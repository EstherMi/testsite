---
layout: "image"
title: "Dübelbox2.JPG"
date: "2007-08-15T17:58:12"
picture: "Dbelbox2.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/11387
imported:
- "2019"
_4images_image_id: "11387"
_4images_cat_id: "643"
_4images_user_id: "4"
_4images_image_date: "2007-08-15T17:58:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11387 -->
Irgendwo im alten ft-Forum hatte ich diese Dübelbox schon mal erwähnt. Ich finde es nur nicht mehr :-(