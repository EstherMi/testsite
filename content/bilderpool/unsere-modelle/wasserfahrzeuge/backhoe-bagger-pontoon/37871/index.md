---
layout: "image"
title: "Backhoe Ponton 'Transport' kondition"
date: "2013-12-02T12:57:36"
picture: "backhoe01.jpg"
weight: "7"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/37871
imported:
- "2019"
_4images_image_id: "37871"
_4images_cat_id: "2815"
_4images_user_id: "162"
_4images_image_date: "2013-12-02T12:57:36"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37871 -->
Link zum Video wie ein Backhoe funktioniert: http://vimeo.com/51531308 
Hier ist das Bagger Ponton in seine transport Kondition zu sehen. Die "Spuds" (Beine) liegen Wagerecht und der Kran ist auf das Deck gedreht