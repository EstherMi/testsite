---
layout: "image"
title: "Spud Aufrechten 4"
date: "2013-12-02T12:57:36"
picture: "backhoe05.jpg"
weight: "11"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/37875
imported:
- "2019"
_4images_image_id: "37875"
_4images_cat_id: "2815"
_4images_user_id: "162"
_4images_image_date: "2013-12-02T12:57:36"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37875 -->
Der erste Spud steht Senkrecht, jetzt kann der zweite Aufgerichtet werden.

Spuds sind hier 30x30mm. Im echte sind die 1,2x1,2 meter bei die "Kleinen" pontons bis zum 1,8x1,8 meter bei die "Grosse".