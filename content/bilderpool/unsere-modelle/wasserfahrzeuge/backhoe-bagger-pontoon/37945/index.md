---
layout: "image"
title: "Innenleben Backhoe"
date: "2013-12-29T19:23:04"
picture: "backhoe2.jpg"
weight: "2"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/37945
imported:
- "2019"
_4images_image_id: "37945"
_4images_cat_id: "2815"
_4images_user_id: "162"
_4images_image_date: "2013-12-29T19:23:04"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37945 -->
XM motor fur den Antrieb einer Spud