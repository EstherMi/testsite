---
layout: "image"
title: "Kran gestartet"
date: "2013-12-02T12:57:36"
picture: "backhoe16.jpg"
weight: "22"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/37886
imported:
- "2019"
_4images_image_id: "37886"
_4images_cat_id: "2815"
_4images_user_id: "162"
_4images_image_date: "2013-12-02T12:57:36"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37886 -->
Dann wird der Kran gestartet (hier mittels ein Kompressor) und der Kran wird nach "außen" gedreht, damit das Baggern anfangen kann