---
layout: "comment"
hidden: true
title: "18495"
date: "2013-12-02T16:30:10"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Prachtig model, waar het nationaal Baggermuseum in Sliedrecht jaloers op zou kunnen zijn !

Heb je de draaischijven in de sputpalen zelf gedraaid of gekocht ?
Voor vele modellen zijn deze interessant.

Gruss,

Peter
Poederoyen NL