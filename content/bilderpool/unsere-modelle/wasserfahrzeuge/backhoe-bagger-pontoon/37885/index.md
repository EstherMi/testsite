---
layout: "image"
title: "Ubersicht"
date: "2013-12-02T12:57:36"
picture: "backhoe15.jpg"
weight: "21"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/37885
imported:
- "2019"
_4images_image_id: "37885"
_4images_cat_id: "2815"
_4images_user_id: "162"
_4images_image_date: "2013-12-02T12:57:36"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37885 -->
Alle spud sind aufgerichtet, Jetzt kann angefangen werden zu baggern.