---
layout: "image"
title: "Innenleben Backhoe 5"
date: "2013-12-29T19:23:04"
picture: "backhoe6.jpg"
weight: "6"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/37949
imported:
- "2019"
_4images_image_id: "37949"
_4images_cat_id: "2815"
_4images_user_id: "162"
_4images_image_date: "2013-12-29T19:23:04"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37949 -->
Mechnismus von den Spud Neigung (fixierte Spuds).
Hohe vons Ponton ist 60mm, sogar alle Steureungen passen innerhalb diese 60mm. Nur dieses Mechanismus kommt einige mm unten das Ponton aus.
Die Schnecke wird angetrieben durch ein Polin motor. Mit eine Steckachse geht es durch denn U-trager. Das wird mittels 2 Zahnrader Z10 die drehung weitergegeben and ein Z10 Zahnrad der auf die Metalachse mit Schnecke verschraubt ist. Mittels Dreieckplatte 32317 wird dan den Wagerechte bewegung konvertiert nach ein Senkrechte bewegung. Mit dieser Senkrechte bewegung wird den Spud gekippt.