---
layout: "image"
title: "Spud Aufrechten 5"
date: "2013-12-02T12:57:36"
picture: "backhoe06.jpg"
weight: "12"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/37876
imported:
- "2019"
_4images_image_id: "37876"
_4images_cat_id: "2815"
_4images_user_id: "162"
_4images_image_date: "2013-12-02T12:57:36"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37876 -->
Wann der Spud senkrecht steht, bewegt der Winde der Spud nach unten damit es sie auf die Boden vons Meer festdruckt.