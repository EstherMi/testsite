---
layout: "image"
title: "Innenleben Backhoe 4"
date: "2013-12-29T19:23:04"
picture: "backhoe5.jpg"
weight: "5"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/37948
imported:
- "2019"
_4images_image_id: "37948"
_4images_cat_id: "2815"
_4images_user_id: "162"
_4images_image_date: "2013-12-29T19:23:04"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37948 -->
Ventile fur den Boom und Stick. (Nach die Idee von Harald: http://www.ftcommunity.de/details.php?image_id=37200) zusammen mit die 2 Kompresoren