---
layout: "image"
title: "Laufen 2"
date: "2013-12-02T12:57:36"
picture: "backhoe14.jpg"
weight: "20"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/37884
imported:
- "2019"
_4images_image_id: "37884"
_4images_cat_id: "2815"
_4images_user_id: "162"
_4images_image_date: "2013-12-02T12:57:36"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37884 -->
Hier ist der Spud am Ende die Laufschiene gekommen. Wann am Ende wird der Spud in die Grund gedruckt.