---
layout: "image"
title: "Spud Aufrechten 7"
date: "2013-12-02T12:57:36"
picture: "backhoe08.jpg"
weight: "14"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/37878
imported:
- "2019"
_4images_image_id: "37878"
_4images_cat_id: "2815"
_4images_user_id: "162"
_4images_image_date: "2013-12-02T12:57:36"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37878 -->
Auf die Winde steckt ein oberes Draht und ein Unter draht. Von der Winde lauft es schräg ubers Ponton durch die Seilrollen. Ein draht geht nach oben, die ander nach unten. Am ende laufen die uber eine Seilrolle nach die Rückseite. In den Spudkasten sind dan 2 Kehr-seilrollen eingebaut (eins oben, eins unten) wodurch das Draht zuruck nach oben/unten gefuhrt werd. Am ende wieder durch die Seilrolle nach vorne und das Ende festgemacht an eine einstellbare Halter