---
layout: "image"
title: "Laufen 1"
date: "2013-12-02T12:57:36"
picture: "backhoe13.jpg"
weight: "19"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/37883
imported:
- "2019"
_4images_image_id: "37883"
_4images_cat_id: "2815"
_4images_user_id: "162"
_4images_image_date: "2013-12-02T12:57:36"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37883 -->
Wann der Spud senkrecht steht, wird mittels ein hydraulik Zylinder (hier ein Messing rohr und Gewindestange) den Spudwagen nach hinten bewogen (normalerweise ist das circa 6 meter) uber die Laufschiene (Alu Profil).
Wann am Ende wird mittels die Winde der Spud ins Bodem gedruckt, damit das Ponton stabil uaf die Grund steht.