---
layout: "image"
title: "Boot neu"
date: "2007-04-04T10:29:45"
picture: "bootneu2.jpg"
weight: "2"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- details/9950
imported:
- "2019"
_4images_image_id: "9950"
_4images_cat_id: "899"
_4images_user_id: "557"
_4images_image_date: "2007-04-04T10:29:45"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9950 -->
Antrieb