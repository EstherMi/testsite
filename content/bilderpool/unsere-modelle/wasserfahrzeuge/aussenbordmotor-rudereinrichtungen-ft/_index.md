---
layout: "overview"
title: "Außenbordmotor mit Rudereinrichtungen für Schiffsrumpf ft# 126927"
date: 2019-12-17T19:34:14+01:00
legacy_id:
- categories/2290
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2290 --> 
Die Beschreibung eines Außenbordantriebes in ftpedia # 1 war der Anlass, nach einer baulichen Lösung mit gleichzeitiger Möglichkeit der Richtungssteuerung für den Schiffsrumpf ft# 126927 zu suchen.