---
layout: "image"
title: "Außenbord03"
date: "2011-05-29T20:45:13"
picture: "aussenbord03.jpg"
weight: "3"
konstrukteure: 
- "qincym"
fotografen:
- "qincym"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- details/30709
imported:
- "2019"
_4images_image_id: "30709"
_4images_cat_id: "2290"
_4images_user_id: "895"
_4images_image_date: "2011-05-29T20:45:13"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30709 -->
Das Bild zeigt den Aufbau des Trägers (befestigt mit 4 Federnocken rot ft# 31982 an der hinteren Wange, wie auch vorn sichtbar) von links nach rechts:

1 Baustein 15 grau ft# 31005 liegend mit dem Zapfen nach rechts als Schiffsschraubenträger

1 Federnocken rot ft# 31982 (verdeckt den Zapfen) als Abstandshalter zum 2:1-Getriebe

1 Baustein 15 ohne Zapfen grau ft# 35003 stehend mit dem "ohne Zapfen" nach oben als 2:1-Getriebeträger

1 Baustein 15 grau ft# 31005 stehend mit dem Zapfen nach oben als Gelenkträger