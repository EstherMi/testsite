---
layout: "image"
title: "Außenbord12"
date: "2011-05-29T20:45:13"
picture: "aussenbord12.jpg"
weight: "12"
konstrukteure: 
- "qincym"
fotografen:
- "qincym"
keywords: ["modding"]
uploadBy: "qincym"
license: "unknown"
legacy_id:
- details/30718
imported:
- "2019"
_4images_image_id: "30718"
_4images_cat_id: "2290"
_4images_user_id: "895"
_4images_image_date: "2011-05-29T20:45:13"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30718 -->
Das Nachfolgende ist wiederum nicht für fischertechnik-Puristen geadacht!

Eine kardanische Verbindung zwischen S-Motor und dem Ritzel mit Getriebeachse kann auch mit Modifikation von ft-Teilen hergestellt werden

Die produktionsbedingte Öffnung im Ritzel wird auf 4 mm Durchmesser aufgebohrt. Dort wird ein abgeschnittenes Stück einer Rastachse mit etwas Sekundenkleber eingeklebt. Ein Ende eines Rastkardangelenkes wird auf 5 mm Durchmesser aufgebohrt. Dort hinein wird dann die Schnecke des S-Motors, nachdem auf die Schnecke ein kurzes Stück Schrumpfschlauch geschrumpft wurde, eingepresst. Fertig ist eine echte kardanische Verbindung, die nur ganz wenig "Walkarbeit" verbraucht.