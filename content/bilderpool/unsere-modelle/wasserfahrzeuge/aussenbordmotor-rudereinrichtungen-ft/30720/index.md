---
layout: "image"
title: "Außenbord14"
date: "2011-05-29T20:45:13"
picture: "aussenbord14.jpg"
weight: "14"
konstrukteure: 
- "qincym"
fotografen:
- "qincym"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- details/30720
imported:
- "2019"
_4images_image_id: "30720"
_4images_cat_id: "2290"
_4images_user_id: "895"
_4images_image_date: "2011-05-29T20:45:13"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30720 -->
Jetzt ist der Basis-Außenbordmotor mit der "Ruderscheibe" komplett aufgebaut.

Die "Ruderscheibe" dreht, genau wie der Außenbordmotor, um die gleiche Drehachse. Deshalb kann über einen aufgelegten Seilzug oder ein aufgestecktes Zahnrad Z40/32, m 1,5 (z.B. schwarz ft# 31022) der Außenbordmotor zur Richtungsänderung um die beiden Gelenke gedreht werden. 

Nun kann mit dem eigentlichen Anbau des Außenbordmotors mit den beiden Rudervarianten an den Schiffsrumpf ft# 126927 begonnen werden.