---
layout: "image"
title: "Außenbord24"
date: "2011-05-29T20:45:13"
picture: "aussenbord24.jpg"
weight: "24"
konstrukteure: 
- "qincym"
fotografen:
- "qincym"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- details/30730
imported:
- "2019"
_4images_image_id: "30730"
_4images_cat_id: "2290"
_4images_user_id: "895"
_4images_image_date: "2011-05-29T20:45:13"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30730 -->
Der Stand der Aufrüstung etwas weiter fortgeschritten.