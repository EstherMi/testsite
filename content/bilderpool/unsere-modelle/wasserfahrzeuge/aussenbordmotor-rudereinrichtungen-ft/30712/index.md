---
layout: "image"
title: "Außenbord06"
date: "2011-05-29T20:45:13"
picture: "aussenbord06.jpg"
weight: "6"
konstrukteure: 
- "qincym"
fotografen:
- "qincym"
keywords: ["modding"]
uploadBy: "qincym"
license: "unknown"
legacy_id:
- details/30712
imported:
- "2019"
_4images_image_id: "30712"
_4images_cat_id: "2290"
_4images_user_id: "895"
_4images_image_date: "2011-05-29T20:45:13"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30712 -->
In der ftpedia #1 wurde empfohlen, das M4-Gewinde der Schiffsschraube "einfach so" über die ft-Rastachse des Kegelzahnrades mit Rastachse m 1 schwarz ft# 35061 zu drehen. Eine elegantere Lösung ist natürlich, auf die ft-Rastachse vor dem Aufdrehen der Schiffsschraube ein M4-Gewinde aufzuschneiden.

Die Abbildung zeigt, wie mit Hilfe eines M4-Schneideisens ein M4-Gewinde auf die ft-Rastachse geschnitten werden kann. Ein Baustein 15 mit Bohrung (in diesem Fall rot ft# 32064) dient zur Führung der ft-Rastachse beim Schneidvorgang. Mit einem breiten Schraubendreher wird dabei das Kegelzahnrad gedreht bis es am Baustein 15 anschlägt.

Am rechten Bildrand liegt das Kegelzahnrad mit Rastachse m 1 schwarz ft# 35061 mit dem aufgebrachten M4-Gewinde (auf dem Bild leider schlecht zu erkennen).