---
layout: "image"
title: "Außenbord15"
date: "2011-05-29T20:45:13"
picture: "aussenbord15.jpg"
weight: "15"
konstrukteure: 
- "qincym"
fotografen:
- "qincym"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- details/30721
imported:
- "2019"
_4images_image_id: "30721"
_4images_cat_id: "2290"
_4images_user_id: "895"
_4images_image_date: "2011-05-29T20:45:13"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30721 -->
Da aus früheren Zeiten noch genügend Bausteine/Bauplatten vorhanden sind, wurde erst einmal eine vernünftige Bootswerft für den Schiffsrumpf ft# 126927 auf einer Bauplatte 500 ft# 32985 aufgebaut. Dazu wurden neben der Bauplatte 500 verwendet: 20 Bausteine 30 grau ft# 31003, 2 Bausteine 15 ft# 3105, 2 Bausteine 15 mit 2 Zapfen grau ft# 31006, 4 Bauplatten 15x15 zum Clipsen rot ft# 31506, 4 Winkelsteine 30 Grad gleichschenklig rot ft# 31011, 6 Verbindungsstücke 30 rot ft# 31061 und 2 Bauplatten 30x30 zum Clipsen rot ft# 31501.

Deutlich sichtbar ist eine 19 mm Platte aus Pappelholz unter der Bauplatte 500, was die Stabilität der Bauplatte 500 merklich verstärkt und daunter, wie an der linken Ecke zu erkennen, befinden sich 5 Filzgleiter.