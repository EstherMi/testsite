---
layout: "image"
title: "Außenbord16"
date: "2011-05-29T20:45:13"
picture: "aussenbord16.jpg"
weight: "16"
konstrukteure: 
- "qincym"
fotografen:
- "qincym"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- details/30722
imported:
- "2019"
_4images_image_id: "30722"
_4images_cat_id: "2290"
_4images_user_id: "895"
_4images_image_date: "2011-05-29T20:45:13"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30722 -->
Im Schiffsrumpf ft# 126927 wird der obere Aufhängungspunkt für den oberen Gelenkwürfel des Außenbordmotors eingebaut. Die Teile 3 Bausteine 30 grau ft# 31003, 1 Baustein 30 mit Bohrung grau ft# 31004, 2 Winkelsteine 10*15*15 rot ft# 38423, 4 Federnocken rot ft# 31982, 1 Verbindungsstück 45 rot ft# 31330, 1 Verbindungsstück 15 rot ft# 31060 und 1 Bauplatte 15*30*5 mit Teilnuten rot ft# 38428 werden dazu verwendet. Der fertige obere Aufhängungspunkt ist im Hintergrund des linken Bildrandes zu sehen.

Die Ruderscheibe (Drehscheibe 60 rot ft# 31019) des Außenbordmotors wird zur Aufnahme des Seilzuges mit 2 Klemmstiften D4,1 rot ft# 107356, 2 Riegelscheiben rot ft# 36334 und 2 Zugfedern für Elektromagnet 4,5*12,5*0,4 ft# 32354 vorbereitet. Am rechten Bildrand ist die fertig montierte Einheit zu sehen.