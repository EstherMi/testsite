---
layout: "image"
title: "Außenbord20"
date: "2011-05-29T20:45:13"
picture: "aussenbord20.jpg"
weight: "20"
konstrukteure: 
- "qincym"
fotografen:
- "qincym"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- details/30726
imported:
- "2019"
_4images_image_id: "30726"
_4images_cat_id: "2290"
_4images_user_id: "895"
_4images_image_date: "2011-05-29T20:45:13"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30726 -->
Dieses Bild zeigt die Rudersteuerung mit dem aufgelegten Seilzug.

Der Seilzug besteht aus kurzen Stücken des Nylonseils 2m blau ft# 31027. Die Stücke werden jeweils an den Zugfedern verknotet und gegenläufig ca. 1,25 Umrundungen um das Ruderrad gewickelt. Zu Schluss werden die Seilstücke an den Seiltrommeln befestigt und gegenläufig aufgespult, so daß, wenn eine Spule abwickelt, die andere Spule jeweils aufwickelt.

Die beiden Zugfedern wirken als Längenausgleich.