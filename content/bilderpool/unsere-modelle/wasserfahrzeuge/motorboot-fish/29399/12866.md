---
layout: "comment"
hidden: true
title: "12866"
date: "2010-12-03T22:07:34"
uploadBy:
- "fish"
license: "unknown"
imported:
- "2019"
---
Wellengang ist allerdings nicht zu empfehlen. :)
Aber du hast recht, hinten liegt das Boot verdammt tief im Wasser - trotz Batteriegewichten. Für den Betrieb auf Teichen und Bächen ist es aber gut genug.