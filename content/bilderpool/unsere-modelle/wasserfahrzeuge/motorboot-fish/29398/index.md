---
layout: "image"
title: "Schiff hinten"
date: "2010-12-01T22:17:03"
picture: "motorbootfish4.jpg"
weight: "4"
konstrukteure: 
- "fish"
fotografen:
- "fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- details/29398
imported:
- "2019"
_4images_image_id: "29398"
_4images_cat_id: "2135"
_4images_user_id: "1113"
_4images_image_date: "2010-12-01T22:17:03"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29398 -->
Das IR-Control-Set steuert die beiden Motoren im Raupenmodus wahlweise mit halber oder voller Geschwindigkeit an. Die Bauplatte ist auf dem Styropoor-Rumpf mit Schrauben befestigt.
