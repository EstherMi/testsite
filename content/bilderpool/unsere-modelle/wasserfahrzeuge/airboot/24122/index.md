---
layout: "image"
title: "Testmodell"
date: "2009-05-29T15:30:25"
picture: "airboot2.jpg"
weight: "5"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/24122
imported:
- "2019"
_4images_image_id: "24122"
_4images_cat_id: "1654"
_4images_user_id: "845"
_4images_image_date: "2009-05-29T15:30:25"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24122 -->
