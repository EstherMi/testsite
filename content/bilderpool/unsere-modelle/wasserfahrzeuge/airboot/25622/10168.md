---
layout: "comment"
hidden: true
title: "10168"
date: "2009-11-01T22:18:32"
uploadBy:
- "mirose"
license: "unknown"
imported:
- "2019"
---
Hallo Marius,

nein, ich habe gemeint, die Motoren so lange verschwenken, bis das Boot, wenn es geradeaus fahren soll, auch geradeaus fährt.
Wenn das richtig eingestellt ist, was nur durch Ausprobieren geht, kannst Du das Boot wieder mit dem Raupenmodus betreiben.
Meine Vermutung ist, da sich beide Propeller in die gleiche Richtung drehen, das Boot durch die Propeller versetzt wird, daher die ungewollte Bogenfahrt.

Viele Grüße

Mirose