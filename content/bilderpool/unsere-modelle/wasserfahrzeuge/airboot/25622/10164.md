---
layout: "comment"
hidden: true
title: "10164"
date: "2009-11-01T17:49:19"
uploadBy:
- "mirose"
license: "unknown"
imported:
- "2019"
---
Hallo Marius,

mir ist aufgefallen, daß Dein Boot etwas hecklastig ist. Eventuell das Akkupaket etwas nach vorne verlagern.
Wegen der Geradeausfahrt: ein Steuerruder anbringen oder die Motoren schwenkbar machen.

Viele Grüße

Mirose