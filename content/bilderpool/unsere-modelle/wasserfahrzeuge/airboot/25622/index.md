---
layout: "image"
title: "Testlauf der 2."
date: "2009-11-01T11:27:03"
picture: "testlaufder1.jpg"
weight: "1"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/25622
imported:
- "2019"
_4images_image_id: "25622"
_4images_cat_id: "1654"
_4images_user_id: "845"
_4images_image_date: "2009-11-01T11:27:03"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25622 -->
Hier die Bilder auf dem Teich. Ein Video kommt auch noch. Leider fährt das Boot nicht ganz exakt geradeaus. Hat jemand eine Idee wie man das beheben kann?