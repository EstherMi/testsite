---
layout: "image"
title: "Ausenborder"
date: "2007-02-12T17:47:07"
picture: "Ausenborder2.jpg"
weight: "2"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/8995
imported:
- "2019"
_4images_image_id: "8995"
_4images_cat_id: "809"
_4images_user_id: "456"
_4images_image_date: "2007-02-12T17:47:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8995 -->
Eigentlich ist er ziemlich ähnlich wie der Ausenborder von Franz, aber ich hab ihn etwas verbessert. Weil ich kein passende Schiffsschraube hab, hab ich eine Schnecke genommen, die warscheinlich nix bringt.