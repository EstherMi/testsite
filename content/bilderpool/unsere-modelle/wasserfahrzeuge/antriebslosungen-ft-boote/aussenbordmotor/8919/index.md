---
layout: "image"
title: "Gesamtansicht 2"
date: "2007-02-10T15:13:34"
picture: "Auenborder02b.jpg"
weight: "6"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- details/8919
imported:
- "2019"
_4images_image_id: "8919"
_4images_cat_id: "809"
_4images_user_id: "488"
_4images_image_date: "2007-02-10T15:13:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8919 -->
Das ganze nochmal von oben betrachtet. Der Motor bleibt bei dieser Konstruktion deutlich oberhalb der Wasserlinie und ist somit geschützt.