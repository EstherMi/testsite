---
layout: "comment"
hidden: true
title: "3118"
date: "2007-05-05T21:20:53"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
... und ganz nebenbei werden doch hier  Bilder nicht zuletzt deshalb eingestellt, DAMIT die dargestellten Ideen für gut befunden und nachgemacht werden. Ich sehe das eher als eine Anerkennung an!

Gruß,
Harald