---
layout: "image"
title: "Detail"
date: "2007-02-10T15:13:34"
picture: "Auenborder03b.jpg"
weight: "7"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- details/8920
imported:
- "2019"
_4images_image_id: "8920"
_4images_cat_id: "809"
_4images_user_id: "488"
_4images_image_date: "2007-02-10T15:13:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8920 -->
Unter der Strömungsgünstigen Verkleidung sitzt ein einfaches Winkelgetriebe. Die Höhe des Antriebes von Befetigung bis Schraubenachse ist variabel, die Höhe des Winkelgetriebes muß dabei ein wenig angepaßt werden. Anstelle des Rast-Z10 denkt man sich nun eine schöne Schiffschraube.