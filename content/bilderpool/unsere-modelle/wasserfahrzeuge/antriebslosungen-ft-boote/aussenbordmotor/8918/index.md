---
layout: "image"
title: "Gesamtansicht 1"
date: "2007-02-10T15:13:34"
picture: "Auenborder01b.jpg"
weight: "5"
konstrukteure: 
- "Franz Osten (Jettaheizer)"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- details/8918
imported:
- "2019"
_4images_image_id: "8918"
_4images_cat_id: "809"
_4images_user_id: "488"
_4images_image_date: "2007-02-10T15:13:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8918 -->
Hier von unten betrachtet. Am BS15, der am Gelenk befestigt ist, soll der Außenborder montiert werden, die gelbe Strebe dient der Steuerung.