---
layout: "overview"
title: "Außenborder"
date: 2019-12-17T19:33:30+01:00
legacy_id:
- categories/1807
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1807 --> 
Schneller, elektrischer Außenbordmotor, kann als Antriebsmotor genutzt werden, wenn die Universalteil Kiste seetauglich gemacht werden soll