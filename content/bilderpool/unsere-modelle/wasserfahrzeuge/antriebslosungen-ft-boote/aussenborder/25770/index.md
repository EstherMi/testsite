---
layout: "image"
title: "Elektromotor"
date: "2009-11-12T11:50:29"
picture: "ausssenborder7.jpg"
weight: "6"
konstrukteure: 
- "Minneralwasser"
fotografen:
- "Minneralwasser"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Minneralwasser"
license: "unknown"
legacy_id:
- details/25770
imported:
- "2019"
_4images_image_id: "25770"
_4images_cat_id: "1807"
_4images_user_id: "1019"
_4images_image_date: "2009-11-12T11:50:29"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25770 -->
Als Antriebsmotor dient ein Bürstenmotror Marke Graupner, der Baugröße 360. Die Antriebswelle des Motors habe ich ganz einfach mit Hilfe von Panzertape an einer Fischertechnik Achse befestigt.