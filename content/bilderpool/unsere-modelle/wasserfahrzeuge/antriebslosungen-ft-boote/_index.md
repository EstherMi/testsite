---
layout: "overview"
title: "Antriebslösungen für ft-Boote"
date: 2019-12-17T19:33:23+01:00
legacy_id:
- categories/808
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=808 --> 
Sammlung von Lösungs- / Bauvorschlägen zum Antrieb der neuen ft-Bootsrümpfe