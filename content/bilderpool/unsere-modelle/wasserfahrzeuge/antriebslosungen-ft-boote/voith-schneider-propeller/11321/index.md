---
layout: "image"
title: "VSP-E02.JPG"
date: "2007-08-09T19:38:11"
picture: "VSP-E02.JPG"
weight: "21"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/11321
imported:
- "2019"
_4images_image_id: "11321"
_4images_cat_id: "1018"
_4images_user_id: "4"
_4images_image_date: "2007-08-09T19:38:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11321 -->
Modell "E" ist etwas Gediegenes und gefällt mir schon recht gut.

Die 8 mm breiten Statikstreben sind in einem BS7,5 geführt, bei dem auf der einen Seite etwas rundes (Achse bzw. Verbindungsstück 30 mit der runden Seite) und auf der anderen Seite etwas flaches (Federnocken) drin steckt. Damit die Führung "ordentlich" wird, gibt es das Ganze zweimal hintereinander.

So weit erst einmal, mehr wird vom Projekt "Voith-Schneider-Propeller" noch nicht preis gegeben :-)