---
layout: "image"
title: "VSP-F04.JPG"
date: "2007-08-18T10:25:22"
picture: "VSP-F04.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/11394
imported:
- "2019"
_4images_image_id: "11394"
_4images_cat_id: "1018"
_4images_user_id: "4"
_4images_image_date: "2007-08-18T10:25:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11394 -->
Modell "F" im Versuchsträger. Man muss sich ein Boot drumherum denken, mit Bug links oben, Heck rechts unten, mit den U-Trägern als seitliche Bordwände. Die Wasserlinie verläuft in Höhe der Unterkante der U-Träger.

Die beiden Propeller werden gegenläufig angetrieben. Die Steuerung von Vorschub-Richtung und -Stärke geschieht für beide gemeinsam: über die Kette unten gleichsinnig in der X-Achse, über die Kette in Bildmitte gegensinnig in der Y-Achse.

Das Ganze wird zum Testen in eine Wanne mit Wasser gestellt. Probleme gibt es noch reichlich: die Streben sind zu kurz, um den Arbeitsbereich der Propeller auszunutzen, aber gleichzeitig schon zu lang und stoßen überall an. 

Eigentlich müssten die beiden Propeller individuell verstellt werden. Der Versuchsträger nutzt eben nur einen kleinen Teil der Möglichkeiten, die man mit zwei Voith-Schneider-Propellern hat. Aber erstmal muss die Mechanik ordentlich mitspielen, bevor zwei unabhängige XY-Stellantriebe dazukommen.