---
layout: "image"
title: "VSP-0x01.JPG"
date: "2007-08-09T18:43:21"
picture: "VSP-0x01.JPG"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/11309
imported:
- "2019"
_4images_image_id: "11309"
_4images_cat_id: "1018"
_4images_user_id: "4"
_4images_image_date: "2007-08-09T18:43:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11309 -->
Das erste Problem beim Nachbau mit ft ist es, Gleitstäbe zu finden und möglichst genau in einem Punkt drehbar zu befestigen. Das hier ist eine Variante mit einem ft-fremden Kabelbinder.