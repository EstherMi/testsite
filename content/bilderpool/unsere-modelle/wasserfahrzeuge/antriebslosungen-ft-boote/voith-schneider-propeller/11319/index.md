---
layout: "image"
title: "VSP-D01.JPG"
date: "2007-08-09T19:30:15"
picture: "VSP-D01.JPG"
weight: "19"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/11319
imported:
- "2019"
_4images_image_id: "11319"
_4images_cat_id: "1018"
_4images_user_id: "4"
_4images_image_date: "2007-08-09T19:30:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11319 -->
Modell "D"

Weil das mit den Gleitführungen bisher nie etwas überzeugendes geworden ist, hier der Versuch über ein anderes Bauprinzip. Dieses beruht auf Hebeln, die sich mit einem Ende in der Mitte (Punkt "G") treffen und am anderen Ende je ein Blatt ansteuern. Die Blattsteuerung geschieht durch geschicktes Verschieben der Lagerpunkte der Hebel. Hört sich kompliziert an, und ist es auch. 

Weiter als bis hierher ist Modell "D" nicht gediehen. Was man aber sieht, ist der vorgesehene Antrieb über eine ft-Achse, die einfach zwischen Speichenrad und Gummiriemen steckt.