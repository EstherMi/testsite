---
layout: "image"
title: "VSP-0x04.JPG"
date: "2007-08-09T18:48:33"
picture: "VSP-0x04.JPG"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/11311
imported:
- "2019"
_4images_image_id: "11311"
_4images_cat_id: "1018"
_4images_user_id: "4"
_4images_image_date: "2007-08-09T18:48:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11311 -->
Diese Anordnung für die Gleitschienen gefällt mir am besten, aber hier wurde gebohrt und Fremdmaterial (Kupferdraht) verwendet.

Das schwarze ist ein Stirnzapfen 31942 für die ft-Aluprofile. Zu denen werde ich auch noch mal ein paar Takte sagen müssen :-)