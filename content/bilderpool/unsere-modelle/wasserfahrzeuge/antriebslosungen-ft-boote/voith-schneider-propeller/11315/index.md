---
layout: "image"
title: "VSP-B04.JPG"
date: "2007-08-09T19:17:41"
picture: "VSP-B04.JPG"
weight: "15"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/11315
imported:
- "2019"
_4images_image_id: "11315"
_4images_cat_id: "1018"
_4images_user_id: "4"
_4images_image_date: "2007-08-09T19:17:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11315 -->
Modell "B".

Die Führung der Gleitstücke (Streben) braucht ein paar Fremdteile in Form von Kabelbindern.  

Der Gummiriemen dient zum Antrieb des ganzen: an einer Stelle wird er ein wenig vom Speichenrad abgehoben, eine nackte ft-Achse dazwischen gesteckt, diese wird angetrieben, und fertig. 

Tja, das würde man meinen. Wenn da nicht die notwendige Lagerung eben dieser Antriebsachse wäre, die entweder unten mit den Blättern oder oben mit den Streben kollidiert. Modell B hat damit ein ziemliches Problem. Unter Verzicht auf zwei der vier Blätter gibt es einen Ausweg -- aber dazu später.

Noch ein Problem: die Gleitführungen sind entweder zu stramm und damit schwergängig, oder leichtgängig und spielbehaftet.

Aber hübsch anzuschauen ist Modell B allemale.