---
layout: "image"
title: "VSP-F02.JPG"
date: "2007-08-18T10:14:50"
picture: "VSP-F02.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/11392
imported:
- "2019"
_4images_image_id: "11392"
_4images_cat_id: "1018"
_4images_user_id: "4"
_4images_image_date: "2007-08-18T10:14:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11392 -->
Modell "F", Ansicht von unterhalb der Wasserlinie.