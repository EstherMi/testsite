---
layout: "image"
title: "VSP-E27.JPG"
date: "2007-08-11T07:45:00"
picture: "VSP-E27.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/11347
imported:
- "2019"
_4images_image_id: "11347"
_4images_cat_id: "1018"
_4images_user_id: "4"
_4images_image_date: "2007-08-11T07:45:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11347 -->
Modell "E" von unten und mit montiertem zweiten Speichenrad.