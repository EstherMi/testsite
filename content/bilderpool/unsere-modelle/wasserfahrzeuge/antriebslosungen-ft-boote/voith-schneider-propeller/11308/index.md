---
layout: "image"
title: "VSP-001.JPG"
date: "2007-08-09T18:40:31"
picture: "VSP-001.JPG"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/11308
imported:
- "2019"
_4images_image_id: "11308"
_4images_cat_id: "1018"
_4images_user_id: "4"
_4images_image_date: "2007-08-09T18:40:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11308 -->
Aus den Skizzen im Wikipedia-Artikel:
http://de.wikipedia.org/wiki/Voith-Schneider
werde ich nicht recht schlau, deshalb hier meine eigene.

Beim Voith-Schneider-Propeller (VSP) stehen die Blattschaufeln senkrecht im Wasser und fahren auf einer Kreisbahn herum.

Wenn die Blattflächen tangential zu dieser Bahn angestellt sind (Skizze links oben), erzeugen sie keinen Vorschub und das Schiff ist antriebslos. 

Wenn die Schaufeln an einer Stelle der Umlaufbahn senkrecht zu ihr angestellt werden (Skizze rechts oben), und sich ansonsten möglichst schnell wieder tangential zu ihr stellen, dann wird Vorschub erzeugt.

Eine (von mehreren) Möglichkeiten, die Blattstellung in geeigneter Weise zu bewerkstelligen, besteht in Gleitstangen, die senkrecht zu den Blättern befestigt sind und in einem gemeinsamen Punkt "G" drehbar gelagert sind (Skizze unten). Die Lage dieses Punktes G entscheidet darüber, was der Propeller tut:
-- je weiter außen (vom Mittelpunkt der Umlaufbahn gesehen) der G-Punkt [ja, ich weiß :-) ] ist, desto ausgeprägter sind die Schwenkbewegungen der Blätter.
-- fällt der G-Punkt mit dem Mittelpunkt der Umlaufbahn zusammen, wird kein Vorschub erzeugt.
-- ein Blatt erzeugt dort Vorschub, wo es quer zur eigenen Umlaufbewegung angestellt ist. Das ist dort der Fall, wo es sich auf den G-Punkt zu bewegt (also im oberen rechten Bahnsektor)