---
layout: "image"
title: "VSP-A01.JPG"
date: "2007-08-09T19:09:00"
picture: "VSP-A01.JPG"
weight: "14"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/11314
imported:
- "2019"
_4images_image_id: "11314"
_4images_cat_id: "1018"
_4images_user_id: "4"
_4images_image_date: "2007-08-09T19:09:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11314 -->
Modell "A".

Das ist mal ein erster Entwurf, bei dem bis auf den Antrieb alles notwendige dran ist. Das Speichenrad wird mittels Führungsplatte 32455 gehalten (das gab es hier schon mal: http://www.ftcommunity.de/details.php?image_id=6761 ).

Die Gleitschienen sind Kurbelstangen aus der ft-BSB, die laut ft-Museum die Nummer 31567 haben. Aber weder "Kurbelstange" noch diese Nummer finde ich bei Knobloch.

Das Modell A funktioniert im Prinzip, aber die Geometrie könnte besser sein. Die Drehpunkte der Blätter liegen zu weit außen, so dass die Pendelbewegung eher nur andeutungsweise stattfindet. Außerdem ist die Gleitführung (zwischen Vierkant-Achse im BS30-Loch und dem BS7,5) nicht präzise genug, d.h. hat zuviel Spiel.