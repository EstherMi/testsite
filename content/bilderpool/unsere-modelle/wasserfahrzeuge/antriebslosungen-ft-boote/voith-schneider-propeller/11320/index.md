---
layout: "image"
title: "VSP-D02.JPG"
date: "2007-08-09T19:32:14"
picture: "VSP-D02.JPG"
weight: "20"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/11320
imported:
- "2019"
_4images_image_id: "11320"
_4images_cat_id: "1018"
_4images_user_id: "4"
_4images_image_date: "2007-08-09T19:32:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11320 -->
Modell "D" von unten.

Das Speichenrad wird mittels Führungsplatte 32455 gehalten und von der Metallachse angetrieben.

Modell "D" ist "die Unvollendete". Weiter als bis hierher ist die Hebelmechanik nicht gediehen.