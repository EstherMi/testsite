---
layout: "image"
title: "VSP-C05.JPG"
date: "2007-08-09T19:25:32"
picture: "VSP-C05.JPG"
weight: "18"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/11318
imported:
- "2019"
_4images_image_id: "11318"
_4images_cat_id: "1018"
_4images_user_id: "4"
_4images_image_date: "2007-08-09T19:25:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11318 -->
Modell "C" von der anderen Seite.

Dieser Aufbau ist insgesamt zu schwabbelig und löst sich nach einigen Umdrehungen in seine Bestandteile auf.