---
layout: "image"
title: "VSP-E33.JPG"
date: "2007-08-11T07:43:12"
picture: "VSP-E33.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/11346
imported:
- "2019"
_4images_image_id: "11346"
_4images_cat_id: "1018"
_4images_user_id: "4"
_4images_image_date: "2007-08-11T07:43:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11346 -->
Modell "E" im Detail.

Die Päckchen aus je 2x BS7,5 sind mit je zwei Federnocken verbunden: ein Federnocken wird hinter dem Zapfen gekürzt und wird spiegelverkehrt zum anderen in die Nut gesteckt.