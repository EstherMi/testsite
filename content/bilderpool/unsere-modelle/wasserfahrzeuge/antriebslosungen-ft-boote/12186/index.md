---
layout: "image"
title: "Schaufelrad"
date: "2007-10-12T21:09:12"
picture: "Dt-Museum76-Schaufelrad.JPG"
weight: "1"
konstrukteure: 
- "NN"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/12186
imported:
- "2019"
_4images_image_id: "12186"
_4images_cat_id: "808"
_4images_user_id: "4"
_4images_image_date: "2007-10-12T21:09:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12186 -->
Fotografiert im Deutschen Museum.

Ein Schaufelrad kann anstelle starr stehender Schaufeln auch einen Mechanismus aufweisen, der diese während des Bades im Wasser immer schön senkrecht zur Wasseroberfläche hält.

Die Anordnung hat ein wenig Ähnlichkeit mit einem Voith-Schneider-Propeller und auch mit der Vorrichtung, die bei einem Mähdrescher die Richtung der Zinken an der Haspel steuert.