---
layout: "image"
title: "Schiffsmotor"
date: "2014-02-02T17:28:10"
picture: "Scheepsmotor_1van2_feb14.jpg"
weight: "1"
konstrukteure: 
- "Jack Steeghs"
fotografen:
- "Jack Steeghs"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "JaSpiel"
license: "unknown"
legacy_id:
- details/38154
imported:
- "2019"
_4images_image_id: "38154"
_4images_cat_id: "643"
_4images_user_id: "1295"
_4images_image_date: "2014-02-02T17:28:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38154 -->
Ein Schiffsmotor. Angetrieb: pullback motor aus Cars & Drives.