---
layout: "image"
title: "baggerschiff"
date: "2007-05-07T18:38:28"
picture: "baggerschiff3.jpg"
weight: "3"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- details/10349
imported:
- "2019"
_4images_image_id: "10349"
_4images_cat_id: "942"
_4images_user_id: "557"
_4images_image_date: "2007-05-07T18:38:28"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10349 -->
modul von oben
