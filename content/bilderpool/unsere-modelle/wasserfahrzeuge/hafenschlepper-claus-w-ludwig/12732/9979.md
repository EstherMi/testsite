---
layout: "comment"
hidden: true
title: "9979"
date: "2009-09-29T22:25:13"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Claus,
wird das Schiffsschraubenruder deines Hafenschleppers vom manuellen Steuerrad bewegt? Sicher hast du auf dem Schiffsrumpf irgendwo einen Servo versteckt?
Gruß Ingo