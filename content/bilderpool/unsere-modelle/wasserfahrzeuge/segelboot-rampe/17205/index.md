---
layout: "image"
title: "Gesamtansicht ohne Rampe-andere Seite"
date: "2009-01-30T19:38:47"
picture: "segelbootmirrampe22.jpg"
weight: "22"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/17205
imported:
- "2019"
_4images_image_id: "17205"
_4images_cat_id: "1542"
_4images_user_id: "845"
_4images_image_date: "2009-01-30T19:38:47"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17205 -->
