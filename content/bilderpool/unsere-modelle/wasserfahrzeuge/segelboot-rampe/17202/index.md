---
layout: "image"
title: "Rampe_2"
date: "2009-01-30T19:38:37"
picture: "segelbootmirrampe19.jpg"
weight: "19"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/17202
imported:
- "2019"
_4images_image_id: "17202"
_4images_cat_id: "1542"
_4images_user_id: "845"
_4images_image_date: "2009-01-30T19:38:37"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17202 -->
