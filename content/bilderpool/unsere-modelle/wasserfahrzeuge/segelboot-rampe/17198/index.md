---
layout: "image"
title: "Rampe mit Boot_2"
date: "2009-01-30T19:38:37"
picture: "segelbootmirrampe15.jpg"
weight: "15"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/17198
imported:
- "2019"
_4images_image_id: "17198"
_4images_cat_id: "1542"
_4images_user_id: "845"
_4images_image_date: "2009-01-30T19:38:37"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17198 -->
