---
layout: "image"
title: "Das Schiff"
date: "2007-10-27T14:49:58"
picture: "bootmitladestaionundcar01.jpg"
weight: "1"
konstrukteure: 
- "water sebi"
fotografen:
- "water sebi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "watersebi"
license: "unknown"
legacy_id:
- details/12319
imported:
- "2019"
_4images_image_id: "12319"
_4images_cat_id: "1102"
_4images_user_id: "642"
_4images_image_date: "2007-10-27T14:49:58"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12319 -->
Schiff mit Auto und Rampe