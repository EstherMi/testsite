---
layout: "image"
title: "Propeller von oben"
date: "2008-04-07T07:56:04"
picture: "motorboot03.jpg"
weight: "3"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/14196
imported:
- "2019"
_4images_image_id: "14196"
_4images_cat_id: "1314"
_4images_user_id: "747"
_4images_image_date: "2008-04-07T07:56:04"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14196 -->
Auf diesem Bild sieht man die Propeller von oben.