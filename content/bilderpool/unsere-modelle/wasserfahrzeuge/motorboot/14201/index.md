---
layout: "image"
title: "Gegengewicht"
date: "2008-04-07T07:56:04"
picture: "motorboot08.jpg"
weight: "8"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/14201
imported:
- "2019"
_4images_image_id: "14201"
_4images_cat_id: "1314"
_4images_user_id: "747"
_4images_image_date: "2008-04-07T07:56:04"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14201 -->
Auf diesem Bild sieht man das Gegengewicht.