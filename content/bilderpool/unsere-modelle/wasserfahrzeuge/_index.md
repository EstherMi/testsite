---
layout: "overview"
title: "Wasserfahrzeuge"
date: 2019-12-17T19:33:14+01:00
legacy_id:
- categories/643
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=643 --> 
fischertechnik zur See (auf oder unter dem Wasserspiegel).
Amphibienfahrzeuge siehe unter "[url="http://www.ftcommunity.de/categories.php?cat_id=612"]SUV+Geländefahrzeuge[/url]".