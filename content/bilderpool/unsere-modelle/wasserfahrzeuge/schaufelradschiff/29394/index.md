---
layout: "image"
title: "Schaufelrad oben"
date: "2010-12-01T22:17:02"
picture: "schaufelradschifffish4.jpg"
weight: "4"
konstrukteure: 
- "fish"
fotografen:
- "fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- details/29394
imported:
- "2019"
_4images_image_id: "29394"
_4images_cat_id: "1937"
_4images_user_id: "1113"
_4images_image_date: "2010-12-01T22:17:02"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29394 -->
Ein Schaufelrad mit Schaufelblättern aus Hartfaserplatte.