---
layout: "image"
title: "Schiff vorne"
date: "2010-12-01T22:17:01"
picture: "schaufelradschifffish2.jpg"
weight: "2"
konstrukteure: 
- "fish"
fotografen:
- "fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- details/29392
imported:
- "2019"
_4images_image_id: "29392"
_4images_cat_id: "1937"
_4images_user_id: "1113"
_4images_image_date: "2010-12-01T22:17:01"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29392 -->
Das Schiff von vorne mit IR-Control-Set, Motoren 50:1, und sechsflügligen Schaufelrädern.