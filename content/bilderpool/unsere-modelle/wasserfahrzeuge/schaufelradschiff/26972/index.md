---
layout: "image"
title: "Schaufelrad"
date: "2010-04-20T21:29:28"
picture: "schaufelradschiff3.jpg"
weight: "7"
konstrukteure: 
- "fish"
fotografen:
- "fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- details/26972
imported:
- "2019"
_4images_image_id: "26972"
_4images_cat_id: "1937"
_4images_user_id: "1113"
_4images_image_date: "2010-04-20T21:29:28"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26972 -->
Schaufelrad mit Spritzschutz. Rechts sind die Motoren zu sehen.