---
layout: "overview"
title: "Schaufelradschiff"
date: 2019-12-17T19:34:07+01:00
legacy_id:
- categories/1937
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1937 --> 
Ferngesteuertes Schiff mit zwei motorenisierten Schaufelrädern. Steuerung über Interface oder IR Control Set.