---
layout: "image"
title: "Schiff oben"
date: "2010-12-01T22:17:01"
picture: "schaufelradschifffish1.jpg"
weight: "1"
konstrukteure: 
- "fish"
fotografen:
- "fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- details/29391
imported:
- "2019"
_4images_image_id: "29391"
_4images_cat_id: "1937"
_4images_user_id: "1113"
_4images_image_date: "2010-12-01T22:17:01"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29391 -->
Von oben mit den beiden Seitenteilen zur Kentersicherheit.