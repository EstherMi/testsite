---
layout: "image"
title: "Schiffchenteich"
date: "2007-06-25T00:32:31"
picture: "GrennderungDSC03061.jpg"
weight: "3"
konstrukteure: 
- "charly"
fotografen:
- "charly"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "charly"
license: "unknown"
legacy_id:
- details/10915
imported:
- "2019"
_4images_image_id: "10915"
_4images_cat_id: "948"
_4images_user_id: "115"
_4images_image_date: "2007-06-25T00:32:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10915 -->
