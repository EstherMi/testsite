---
layout: "image"
title: "Dübelbox1"
date: "2007-08-15T17:57:47"
picture: "Dbelbox1.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/11386
imported:
- "2019"
_4images_image_id: "11386"
_4images_cat_id: "643"
_4images_user_id: "4"
_4images_image_date: "2007-08-15T17:57:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11386 -->
Das ist Original-fischer (Wenn auch nicht fischertechnik)!
Die Nut- und Federverbindung ist 100% kompatibel zu ft; die Breite der Box liegt nur ein paar zehntel mm daneben.

Wenn es diese Dübelbox noch zu kaufen gibt, wäre das schon mal etwas für Auftriebskörper für Boote und Pontons.