---
layout: "comment"
hidden: true
title: "16856"
date: "2012-05-19T23:29:04"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hallo Harald,
meine Befürchtung betrifft auch eher die Länge des Beckens als die Wasserfläche. Mit dem Antrieb prallst Du ja schon vor Erreichung der Höchstgeschwindigkeit auf der gegenüberliegenden Seite an den Beckenrand...
Bei uns steht übrigens auch noch ein Bötchen auf dem Trockendock - mit zwei Außenbordern.
Gruß, Dirk