---
layout: "image"
title: "Rennboot53"
date: "2012-05-17T14:10:10"
picture: "Rennboot53.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/34962
imported:
- "2019"
_4images_image_id: "34962"
_4images_cat_id: "2587"
_4images_user_id: "4"
_4images_image_date: "2012-05-17T14:10:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34962 -->
So schnell habe ich die Technik für ein neues Modell noch niemals beisammen gehabt. Ist aber auch kein Wunder: die Technik ist 1:1 copy&paste vom Außenborder-Boot nebenan.