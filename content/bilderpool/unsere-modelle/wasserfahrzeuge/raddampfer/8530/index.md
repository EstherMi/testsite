---
layout: "image"
title: "Antrieb der Schaufelräder"
date: "2007-01-20T16:45:44"
picture: "schiff03.jpg"
weight: "3"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/8530
imported:
- "2019"
_4images_image_id: "8530"
_4images_cat_id: "787"
_4images_user_id: "453"
_4images_image_date: "2007-01-20T16:45:44"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8530 -->
