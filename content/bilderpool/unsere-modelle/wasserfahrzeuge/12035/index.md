---
layout: "image"
title: "Boot mit einem kleinen Kran"
date: "2007-09-29T06:54:44"
picture: "DSCF0397_Boot_Tennerifa.jpg"
weight: "3"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: ["Schiff", "Boot", "Kran", "Boje", "wasser", "ferngesteuert", "Box1000", "pneumatik", "zylinder"]
uploadBy: "Sebo"
license: "unknown"
legacy_id:
- details/12035
imported:
- "2019"
_4images_image_id: "12035"
_4images_cat_id: "643"
_4images_user_id: "650"
_4images_image_date: "2007-09-29T06:54:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12035 -->
Auf diesem Foto ist ein Boot zu sehen, dass dafür gedacht ist, Bojen auszusetzen. Gebaut habe ich dieses auf Teneriffa im Hotel, wo es auch Pool einen gab. Da ich zu diesem Zeitpunkt noch keine IR-Empfänger 2 hatte, kann man bei diesem Boot nur die Fahrfunktionen und die Seilwinde steuern. Übriges über Handventile und Polwendschalter.