---
layout: "image"
title: "Bootsrumpf-5"
date: "2011-03-23T17:48:48"
picture: "boot_4821.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/30312
imported:
- "2019"
_4images_image_id: "30312"
_4images_cat_id: "2253"
_4images_user_id: "4"
_4images_image_date: "2011-03-23T17:48:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30312 -->
Das fertige Boot, mit Lenk-Servo und Empfänger. Video siehe http://www.youtube.com/watch?v=7YTA257HZJA