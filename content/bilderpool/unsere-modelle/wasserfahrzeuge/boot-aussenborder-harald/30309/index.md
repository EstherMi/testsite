---
layout: "image"
title: "Bootsrumpf-2"
date: "2011-03-23T17:37:03"
picture: "boot_4739.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/30309
imported:
- "2019"
_4images_image_id: "30309"
_4images_cat_id: "2253"
_4images_user_id: "4"
_4images_image_date: "2011-03-23T17:37:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30309 -->
Ganz vorn sitzt ein BS15 mit rundem (rotem) Zapfen (31059 *nml*; gibt es noch in schwarz als 103448) - da ist nichts verbogen oder gequält worden. Im Kiel sitzen zwei Statiksteine (35076 gelb; hier die rote Variante 36973), einer hinten, einer mitten, die aber nur als Führung dienen.