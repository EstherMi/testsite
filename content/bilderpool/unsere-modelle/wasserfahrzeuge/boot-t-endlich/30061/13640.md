---
layout: "comment"
hidden: true
title: "13640"
date: "2011-02-19T11:27:58"
uploadBy:
- "Endlich"
license: "unknown"
imported:
- "2019"
---
@Udo2

ja, da hast du recht, die Schrauben bzw. die Propeller drehen sich nur sehr langsam, aber das Boot kommt trotzdem gut forwärts.
Bis jetzt haben wir aber noch keine Probleme mit dem Getriebe, Motor, oder den Rastachsen. 

MfG
Endlich