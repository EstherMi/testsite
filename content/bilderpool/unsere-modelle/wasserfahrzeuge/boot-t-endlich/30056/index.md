---
layout: "image"
title: "Boot- von vorne"
date: "2011-02-18T14:12:43"
picture: "boot02.jpg"
weight: "3"
konstrukteure: 
- "T.Endlich"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/30056
imported:
- "2019"
_4images_image_id: "30056"
_4images_cat_id: "2215"
_4images_user_id: "1162"
_4images_image_date: "2011-02-18T14:12:43"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30056 -->
Von vorne. Hier kann man die Lichter sehen, die den Weg nach vorne beleuchten. Man sieht noch den Polwendeschalter, mit dem man das Schiff ein- bzw. ausschallten kann.