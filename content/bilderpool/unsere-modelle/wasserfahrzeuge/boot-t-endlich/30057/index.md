---
layout: "image"
title: "Boot- von schräg oben"
date: "2011-02-18T14:12:43"
picture: "boot03.jpg"
weight: "4"
konstrukteure: 
- "T.Endlich"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/30057
imported:
- "2019"
_4images_image_id: "30057"
_4images_cat_id: "2215"
_4images_user_id: "1162"
_4images_image_date: "2011-02-18T14:12:43"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30057 -->
Von Seite. Hier kann man sehr schon das oberste Deck sehen. Auf ihm befindet sich das Steuerrad. Es ist eingerahmt durch eine Rehling.