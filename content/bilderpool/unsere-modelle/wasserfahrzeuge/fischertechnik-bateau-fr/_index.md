---
layout: "overview"
title: "Fischertechnik-bateau-(FR)"
date: 2019-12-17T19:34:05+01:00
legacy_id:
- categories/1697
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1697 --> 
Un bateau fait avec le fischertechnik.[br]Créé en 2009 à Guéret (Greuze, France) et à Vic sur Cère (Cantal France) pendant mes vacances.[br]Pour les fans de fischertechnik en France et le monde Francophonie.[br][br][br]
[i]Edit von Masked: Übersetzung ins Deutsche[br]
Ein Schiff aus fischertechnik. Gebaut 2009 in Guéret und Vic sur Cére (beides Frankreich) während meiner Ferien/meines Urlaubs. Für die Fischertechnik-fans in Frankreich und in der frankophonen (=französischen, französisch-anhänigigen) Welt.[/i]