---
layout: "image"
title: "FT-Bateau En détail 03"
date: "2009-08-04T18:13:30"
picture: "ftbateau07.jpg"
weight: "7"
konstrukteure: 
- "Carel van Leeuwen"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/24707
imported:
- "2019"
_4images_image_id: "24707"
_4images_cat_id: "1697"
_4images_user_id: "136"
_4images_image_date: "2009-08-04T18:13:30"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24707 -->
