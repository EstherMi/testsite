---
layout: "image"
title: "Kata4825.JPG"
date: "2011-11-01T17:43:39"
picture: "IMG_4825_mit.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Leiter", "130925"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/33379
imported:
- "2019"
_4images_image_id: "33379"
_4images_cat_id: "2471"
_4images_user_id: "4"
_4images_image_date: "2011-11-01T17:43:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33379 -->
Im Rumpf werden etliche Statikadapter 35975 eingesetzt, um die Kontur heraus zu arbeiten und den Übergang von Statik auf BS7,5 dar zu stellen. Die beiden Rümpfe sind mit den Teilen der Feuerwehrleiter 130925 als tragenden Elementen verbunden.