---
layout: "image"
title: "Kata4834.JPG"
date: "2011-11-01T17:48:24"
picture: "IMG_4834_mit.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/33381
imported:
- "2019"
_4images_image_id: "33381"
_4images_cat_id: "2471"
_4images_user_id: "4"
_4images_image_date: "2011-11-01T17:48:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33381 -->
Der Rumpf ist jetzt mit Verkleidungsplatten 15x90 beplankt, und der Bug zeigt einen eleganten Schwung. Den hat er aber nicht lange behalten, weil der Auftrieb nicht gereicht hat.