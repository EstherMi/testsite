---
layout: "image"
title: "Kette"
date: "2008-04-13T16:49:30"
picture: "motorbootmitschiffsschraube4.jpg"
weight: "4"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/14261
imported:
- "2019"
_4images_image_id: "14261"
_4images_cat_id: "1319"
_4images_user_id: "747"
_4images_image_date: "2008-04-13T16:49:30"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14261 -->
Hier sieht man die Kettenübertragung vom Motor zu der Schiffsschraube.