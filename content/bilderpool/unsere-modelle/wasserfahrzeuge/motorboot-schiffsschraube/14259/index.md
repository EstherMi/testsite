---
layout: "image"
title: "Motorboot von Oben"
date: "2008-04-13T16:49:29"
picture: "motorbootmitschiffsschraube2.jpg"
weight: "2"
konstrukteure: 
- "Johannee"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/14259
imported:
- "2019"
_4images_image_id: "14259"
_4images_cat_id: "1319"
_4images_user_id: "747"
_4images_image_date: "2008-04-13T16:49:29"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14259 -->
Hier sieht man das Boot von oben. Der Motor der die Schiffsschraube antreibt ist der Power Motor mit der Übersetzung 8:1.