---
layout: "image"
title: "Antriebsstrang"
date: "2014-02-23T18:04:06"
picture: "bootsantrieb3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/38376
imported:
- "2019"
_4images_image_id: "38376"
_4images_cat_id: "2853"
_4images_user_id: "104"
_4images_image_date: "2014-02-23T18:04:06"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38376 -->
Die Aufhängung der Schaufelräder ist stabil und passt gerade so über den Bootskörper-Rumpf.