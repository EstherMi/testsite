---
layout: "overview"
title: "Bootsantrieb"
date: 2019-12-17T19:34:22+01:00
legacy_id:
- categories/2853
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2853 --> 
Dies ist ein einfacher Bootsantrieb mit möglichst tiefliegenden Motoren. Die damit erreichten Geschwindigkeiten passen gut zu den Platzverhältnissen in einer Badewanne.