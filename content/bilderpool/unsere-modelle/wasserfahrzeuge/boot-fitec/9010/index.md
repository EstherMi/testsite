---
layout: "image"
title: "Boot"
date: "2007-02-12T18:45:40"
picture: "Boot13.jpg"
weight: "23"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/9010
imported:
- "2019"
_4images_image_id: "9010"
_4images_cat_id: "819"
_4images_user_id: "456"
_4images_image_date: "2007-02-12T18:45:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9010 -->
Schiffsschraube und Steuerruder.