---
layout: "image"
title: "Gesamtansicht"
date: "2007-06-14T20:34:31"
picture: "Boot22.jpg"
weight: "7"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/10866
imported:
- "2019"
_4images_image_id: "10866"
_4images_cat_id: "819"
_4images_user_id: "456"
_4images_image_date: "2007-06-14T20:34:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10866 -->
Gesamtansicht. Links sind ein paar Gewichte damit es nich umkippt weil es rechts etwas schwerer ist.