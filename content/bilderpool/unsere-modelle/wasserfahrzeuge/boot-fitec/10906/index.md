---
layout: "image"
title: "Aquadynamik"
date: "2007-06-20T17:16:08"
picture: "Boot26.jpg"
weight: "1"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/10906
imported:
- "2019"
_4images_image_id: "10906"
_4images_cat_id: "819"
_4images_user_id: "456"
_4images_image_date: "2007-06-20T17:16:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10906 -->
Hier ist es mehr aquadynamischer. Danke an Jettaheizer für die Idee :-)