---
layout: "image"
title: "Neu"
date: "2007-06-14T20:34:31"
picture: "Boot17.jpg"
weight: "2"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/10861
imported:
- "2019"
_4images_image_id: "10861"
_4images_cat_id: "819"
_4images_user_id: "456"
_4images_image_date: "2007-06-14T20:34:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10861 -->
Das ist mein neues Boot. Es ist super leicht weil ich für den Rumpf nur 2x4 Flachträger + Streben genommen habe.