---
layout: "image"
title: "Antrieb"
date: "2007-06-14T20:34:31"
picture: "Boot18.jpg"
weight: "3"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/10862
imported:
- "2019"
_4images_image_id: "10862"
_4images_cat_id: "819"
_4images_user_id: "456"
_4images_image_date: "2007-06-14T20:34:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10862 -->
In dieser Version ist kein Ruder vorhanden. Es wird der ganze Antrieb gelenkt.