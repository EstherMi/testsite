---
layout: "image"
title: "Boot"
date: "2007-02-12T17:47:07"
picture: "Boot4.jpg"
weight: "14"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/9001
imported:
- "2019"
_4images_image_id: "9001"
_4images_cat_id: "819"
_4images_user_id: "456"
_4images_image_date: "2007-02-12T17:47:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9001 -->
Antrieb des linken Schaufelrads.