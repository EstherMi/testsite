---
layout: "overview"
title: "Boot mit Luftstrahlantrieb"
date: 2019-12-17T19:33:55+01:00
legacy_id:
- categories/1321
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1321 --> 
Das ist ein Boot, das von einem pneumatisch gesteuerten Luftstrahl angetrieben wird.