---
layout: "image"
title: "Schlauch"
date: "2008-04-17T17:56:49"
picture: "bootmitluftstrahlantrieb5.jpg"
weight: "5"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/14275
imported:
- "2019"
_4images_image_id: "14275"
_4images_cat_id: "1321"
_4images_user_id: "747"
_4images_image_date: "2008-04-17T17:56:49"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14275 -->
Hier sieht man den Schlauch, aus dem der Luftstrahl kommt.