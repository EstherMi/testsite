---
layout: "image"
title: "Pneumatik-Schalter"
date: "2008-04-17T17:56:49"
picture: "bootmitluftstrahlantrieb4.jpg"
weight: "4"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/14274
imported:
- "2019"
_4images_image_id: "14274"
_4images_cat_id: "1321"
_4images_user_id: "747"
_4images_image_date: "2008-04-17T17:56:49"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14274 -->
Hier sieht man den Pneumatik-Schalter, mit dem man den Luftstrahl an und ausschalten kann.