---
layout: "image"
title: "Akku"
date: "2008-04-17T17:56:49"
picture: "bootmitluftstrahlantrieb3.jpg"
weight: "3"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/14273
imported:
- "2019"
_4images_image_id: "14273"
_4images_cat_id: "1321"
_4images_user_id: "747"
_4images_image_date: "2008-04-17T17:56:49"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14273 -->
Auf diesem Bild sieht man den Akku.