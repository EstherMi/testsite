---
layout: "comment"
hidden: true
title: "6234"
date: "2008-04-17T20:44:28"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Tolle Idee! Aber ein kleiner Tipp: Auf diesem Bild sieht man ganz gut, dass die Steine, die die Kurbel für den Zylinder führen, ganz schön auf Verdrehung beansprucht werden. Ich glaube es wäre materialschonender, wenn Du die Steine seitlich (also hier nach unten im Bild) weiterbaust und auch auf der gegenüberliegenden Seite der schwarzen Platte verankerst.

Gruß,
Stefan