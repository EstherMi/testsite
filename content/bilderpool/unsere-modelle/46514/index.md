---
layout: "image"
title: "Greifer"
date: "2017-09-30T11:52:18"
picture: "07_Greifer.jpg"
weight: "7"
konstrukteure: 
- "Fabian, Max, Christian & Stefan"
fotografen:
- "Christian und Stefan"
keywords: ["Greifer"]
uploadBy: "ftpi"
license: "unknown"
legacy_id:
- details/46514
imported:
- "2019"
_4images_image_id: "46514"
_4images_cat_id: "10"
_4images_user_id: "2611"
_4images_image_date: "2017-09-30T11:52:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46514 -->
Der Greifer ist möglichst leicht gebaut, um den horizontalantrieb mit einem Hubgetriebe auf einer Zahnstange laufen zu lassen. Der Antrieb hat nur in eine Richtung einen Referenztaster, die andere Richtung wird nur über die Fahrzeit geregelt.