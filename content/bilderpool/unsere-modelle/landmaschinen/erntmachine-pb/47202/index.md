---
layout: "image"
title: "Ernter-gesamt01"
date: "2018-01-30T16:23:29"
picture: "ernter01.jpg"
weight: "1"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/47202
imported:
- "2019"
_4images_image_id: "47202"
_4images_cat_id: "3486"
_4images_user_id: "2449"
_4images_image_date: "2018-01-30T16:23:29"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47202 -->
Hier Bilder aus der 'Homestudio' von den Ernter und seine Bauphasen.