---
layout: "image"
title: "Combi mit Anhänger 2"
date: "2018-01-21T20:14:24"
picture: "combimitanhaenger2.jpg"
weight: "13"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/47163
imported:
- "2019"
_4images_image_id: "47163"
_4images_cat_id: "3486"
_4images_user_id: "2449"
_4images_image_date: "2018-01-21T20:14:24"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47163 -->
Den Anhänger wedre ich hochladen unter Unsere Modelle/Landmaschinen/Anhänger mit Tractor (PB)