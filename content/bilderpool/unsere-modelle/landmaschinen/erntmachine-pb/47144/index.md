---
layout: "image"
title: "erntmachine4.jpg"
date: "2018-01-17T18:01:36"
picture: "erntmachine4.jpg"
weight: "17"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/47144
imported:
- "2019"
_4images_image_id: "47144"
_4images_cat_id: "3486"
_4images_user_id: "2449"
_4images_image_date: "2018-01-17T18:01:36"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47144 -->
