---
layout: "image"
title: "erntmachine1.jpg"
date: "2018-01-17T18:01:36"
picture: "erntmachine1.jpg"
weight: "14"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/47141
imported:
- "2019"
_4images_image_id: "47141"
_4images_cat_id: "3486"
_4images_user_id: "2449"
_4images_image_date: "2018-01-17T18:01:36"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47141 -->
Alles dreht und marchiert und den grauen Tractormotor schafft das ziemlich einfach. Den Tractor ist denen aus den Tractor IR-set. Ich hab nur den Empfänger besser eingebaut, finde ich selber mindenstens...

Den gelben Cilinder vorne met den Achsen ist nur Gegengewicht für den Ausleger. Vielleicht wäre er weniger nötig gewesen wenn ich nicht so leicht gebaut hätte, mit Bausteine 15 und 30 statt Ecksteine und Statikteile.
Den rotierenden Ernt-Cilinder kann um 90 Grad hochgestellt werden während den Transport auf den Weg. Nur soll dan auch den Antrieb unterbrochen werden. Dafür ist nichts eingebaut worden.