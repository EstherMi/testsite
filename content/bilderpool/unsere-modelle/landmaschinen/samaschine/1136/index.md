---
layout: "image"
title: "Zuckerrubben-Saemachine-1"
date: "2003-05-30T16:30:45"
picture: "FT-traktor-Saemachine.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/1136
imported:
- "2019"
_4images_image_id: "1136"
_4images_cat_id: "131"
_4images_user_id: "22"
_4images_image_date: "2003-05-30T16:30:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1136 -->
