---
layout: "image"
title: "MB Trac - Prototyp auf Rampe 3"
date: "2016-02-29T21:09:00"
picture: "mbtrac22.jpg"
weight: "22"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/42951
imported:
- "2019"
_4images_image_id: "42951"
_4images_cat_id: "3192"
_4images_user_id: "2321"
_4images_image_date: "2016-02-29T21:09:00"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42951 -->
Das war ein Testfahrzeug, das hat die 45°-Rampe zwar schon bergauf gemeistert, ....