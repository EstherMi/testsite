---
layout: "image"
title: "MB Trac - Gesamtansicht 5"
date: "2016-02-29T21:09:00"
picture: "mbtrac05.jpg"
weight: "5"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/42934
imported:
- "2019"
_4images_image_id: "42934"
_4images_cat_id: "3192"
_4images_user_id: "2321"
_4images_image_date: "2016-02-29T21:09:00"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42934 -->
Ach ja die Akkus. Weil zwei 9V-Blöcke parallelgeschaltet sind, muss man in die beiden Plus- oder die beiden Minusleitungen Dioden setzen, damit kein Ausgleichsstrom von einen zum anderen Akku fließen kann. Die Leerlaufspannungen sind ja immer etas unterschiedlich, so dass durch diesen Ausgleichsstrom ein Akku leiden würde. Die Dioden sind im Statik-BS 30 versteckt.