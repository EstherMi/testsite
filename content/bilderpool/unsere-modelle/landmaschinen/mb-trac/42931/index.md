---
layout: "image"
title: "MB Trac - Gesamtansicht 2"
date: "2016-02-29T21:09:00"
picture: "mbtrac02.jpg"
weight: "2"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/42931
imported:
- "2019"
_4images_image_id: "42931"
_4images_cat_id: "3192"
_4images_user_id: "2321"
_4images_image_date: "2016-02-29T21:09:00"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42931 -->
Es ist ein MB Trac geworden, allerdings ist die Optik ziemlich mau. Wichtiger war mir ein leichtes Modell nur aus original-ft-Teilen zu bauen, was extrem geländegängig ist und auch die 45°-Rampe schafft.
Ein Video gibt's auch unter https://youtu.be/lpau8PAM0Nw .