---
layout: "image"
title: "MB Trac - Aufhängung Hinterradschwinge 1"
date: "2016-02-29T21:09:00"
picture: "mbtrac16.jpg"
weight: "16"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/42945
imported:
- "2019"
_4images_image_id: "42945"
_4images_cat_id: "3192"
_4images_user_id: "2321"
_4images_image_date: "2016-02-29T21:09:00"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42945 -->
Ein Blick von hinten auf das Lager der Hinterradschwinge. Sie soll sich fürs Gelände sehr leicht in der Längsachse drehen können.
Das ist eine bessere Konstruktion als an der Vorderradschwinge.