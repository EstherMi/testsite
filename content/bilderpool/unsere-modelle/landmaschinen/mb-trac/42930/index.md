---
layout: "image"
title: "MB Trac - Gesamtansicht 1"
date: "2016-02-29T21:09:00"
picture: "mbtrac01.jpg"
weight: "1"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/42930
imported:
- "2019"
_4images_image_id: "42930"
_4images_cat_id: "3192"
_4images_user_id: "2321"
_4images_image_date: "2016-02-29T21:09:00"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42930 -->
Allradfahrzeuge sind mein ft-Lieblingsthema, seitdem ich den Wettbewerb http://forum.ftcommunity.de/viewtopic.php?f=19&t=441 entdeckt habe. Es gibt schon sehr schöne und gute MB Trcs von thomas004 und Peter Poederoyen, und auch das Allradfahrzeug von Stefan Lehnerer und der Unimog von Stefan Rm haben ja schon die Testrampe gemeistert, Jetzt muss ich auch noch meinen Senf dazugeben.
