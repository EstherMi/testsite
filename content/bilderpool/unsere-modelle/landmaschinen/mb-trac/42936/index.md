---
layout: "image"
title: "MB Trac - Gesamtansicht 7"
date: "2016-02-29T21:09:00"
picture: "mbtrac07.jpg"
weight: "7"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/42936
imported:
- "2019"
_4images_image_id: "42936"
_4images_cat_id: "3192"
_4images_user_id: "2321"
_4images_image_date: "2016-02-29T21:09:00"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42936 -->
Schade dass im Cockpit kein Pltaz für einen Sitz mit ft-Männchen ist. Naja, Traktoren fahren ja eh bald vollautomatisch.