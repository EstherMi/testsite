---
layout: "image"
title: "MB Trac - Gesamtansicht 6"
date: "2016-02-29T21:09:00"
picture: "mbtrac06.jpg"
weight: "6"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/42935
imported:
- "2019"
_4images_image_id: "42935"
_4images_cat_id: "3192"
_4images_user_id: "2321"
_4images_image_date: "2016-02-29T21:09:00"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42935 -->
Hinten ist nur eine Feder verbaut, damit die Hinterachse ganz einfach pendeln kann - gut fürs Gelände, schlecht für Anbaugeräte und andere Lasten. Aber letztere gibt es ja noch nicht, vielleicht kommt ja später noch eine zweite Feder rein.