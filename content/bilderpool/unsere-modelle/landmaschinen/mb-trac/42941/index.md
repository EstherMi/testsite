---
layout: "image"
title: "MB Trac - Hinterrad 2"
date: "2016-02-29T21:09:00"
picture: "mbtrac12.jpg"
weight: "12"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/42941
imported:
- "2019"
_4images_image_id: "42941"
_4images_cat_id: "3192"
_4images_user_id: "2321"
_4images_image_date: "2016-02-29T21:09:00"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42941 -->
Noch mal die Innereien des Hinterrads. Wie schon gesagt, ist das noch nicht der Weisheit letzter Schluss, das Langloch kann die Kräfte nicht dauerhaft auf das Lagerstück übertragen.