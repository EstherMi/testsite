---
layout: "image"
title: "claas super 024"
date: "2005-05-26T21:11:25"
picture: "claas_super_024.JPG"
weight: "24"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Claus"
license: "unknown"
legacy_id:
- details/4205
imported:
- "2019"
_4images_image_id: "4205"
_4images_cat_id: "356"
_4images_user_id: "119"
_4images_image_date: "2005-05-26T21:11:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4205 -->
