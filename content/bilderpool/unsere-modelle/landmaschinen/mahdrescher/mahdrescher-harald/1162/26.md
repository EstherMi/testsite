---
layout: "comment"
hidden: true
title: "26"
date: "2003-06-01T11:13:51"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Eigenbau-Zylinderrichtig erkannt.  Zwei ft-Zylinder heben das Schneidwerk nicht, für drei davon reicht der Platz nicht. Der Zylinder oben kostet gut zwei Stunden Arbeit und hat die Kraft von 2,6 ft-Zylindern. 

Die Diskussion gibts hier: [URL=http://www.fischertechnik.de/fanclub/forum/topic.asp?TOPIC_ID=802&FORUM_ID=9&CAT_ID=4&Topic_Title=Teleskop%2DAntriebsachse+%2D+selbstgebaute+Pneumatik&Forum_Title=fischertechnik+Tipps+%26+Tricks]Zylinder-Eigenbau[/URL]