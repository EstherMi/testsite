---
layout: "image"
title: "Maehdr01.JPG"
date: "2003-11-11T19:15:05"
picture: "Maehdr01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/1970
imported:
- "2019"
_4images_image_id: "1970"
_4images_cat_id: "192"
_4images_user_id: "4"
_4images_image_date: "2003-11-11T19:15:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1970 -->
Blick ins Chassis. Der Power-Mot treibt das Fahrzeug an, die Gewindestange hebt und senkt das Schneidwerk.