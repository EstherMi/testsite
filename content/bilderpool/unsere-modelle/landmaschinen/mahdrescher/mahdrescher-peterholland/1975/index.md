---
layout: "image"
title: "Maehdr21.JPG"
date: "2003-11-11T19:15:06"
picture: "Maehdr21.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/1975
imported:
- "2019"
_4images_image_id: "1975"
_4images_cat_id: "192"
_4images_user_id: "4"
_4images_image_date: "2003-11-11T19:15:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1975 -->
Blick von oben auf den oberen Schüttler und die Wendetrommel. Fahrtrichtung ist nach links.