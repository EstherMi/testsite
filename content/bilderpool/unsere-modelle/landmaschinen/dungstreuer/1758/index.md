---
layout: "image"
title: "Dungstreuer"
date: "2003-10-03T14:04:29"
picture: "MSt01.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Dungstreuer"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/1758
imported:
- "2019"
_4images_image_id: "1758"
_4images_cat_id: "191"
_4images_user_id: "4"
_4images_image_date: "2003-10-03T14:04:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1758 -->
