---
layout: "image"
title: "Der Anschluss zwischen Traktor und Presse"
date: "2009-06-29T23:27:03"
picture: "niederdruckpresse10.jpg"
weight: "10"
konstrukteure: 
- "Robo Fan"
fotografen:
- "Robo Fan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Robo Fan"
license: "unknown"
legacy_id:
- details/24485
imported:
- "2019"
_4images_image_id: "24485"
_4images_cat_id: "1681"
_4images_user_id: "771"
_4images_image_date: "2009-06-29T23:27:03"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24485 -->
