---
layout: "image"
title: "Hinten rechts"
date: "2009-06-29T23:27:03"
picture: "niederdruckpresse07.jpg"
weight: "7"
konstrukteure: 
- "Robo Fan"
fotografen:
- "Robo Fan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Robo Fan"
license: "unknown"
legacy_id:
- details/24482
imported:
- "2019"
_4images_image_id: "24482"
_4images_cat_id: "1681"
_4images_user_id: "771"
_4images_image_date: "2009-06-29T23:27:03"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24482 -->
