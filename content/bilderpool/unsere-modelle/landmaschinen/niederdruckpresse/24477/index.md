---
layout: "image"
title: "Vorne"
date: "2009-06-29T23:27:02"
picture: "niederdruckpresse02.jpg"
weight: "2"
konstrukteure: 
- "Robo Fan"
fotografen:
- "Robo Fan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Robo Fan"
license: "unknown"
legacy_id:
- details/24477
imported:
- "2019"
_4images_image_id: "24477"
_4images_cat_id: "1681"
_4images_user_id: "771"
_4images_image_date: "2009-06-29T23:27:02"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24477 -->
Hier sieht man die Kabel für Beleuchtung und herunterlassen des Heuaufnehmers.