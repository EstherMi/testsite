---
layout: "image"
title: "Anh-f19-20"
date: "2018-02-01T15:19:24"
picture: "anhaenger13.jpg"
weight: "13"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/47240
imported:
- "2019"
_4images_image_id: "47240"
_4images_cat_id: "3489"
_4images_user_id: "2449"
_4images_image_date: "2018-02-01T15:19:24"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47240 -->
