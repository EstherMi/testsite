---
layout: "image"
title: "anhaengertractorpb4.jpg"
date: "2018-01-21T20:14:24"
picture: "anhaengertractorpb4.jpg"
weight: "18"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/47167
imported:
- "2019"
_4images_image_id: "47167"
_4images_cat_id: "3489"
_4images_user_id: "2449"
_4images_image_date: "2018-01-21T20:14:24"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47167 -->
