---
layout: "image"
title: "Anh-gesamt-01"
date: "2018-02-01T15:19:18"
picture: "anhaenger01.jpg"
weight: "1"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/47228
imported:
- "2019"
_4images_image_id: "47228"
_4images_cat_id: "3489"
_4images_user_id: "2449"
_4images_image_date: "2018-02-01T15:19:18"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47228 -->
Hier die Bauphasen von der Anhänger. Technisch nichts Besonderes, aber vielleicht find einem es jedoch ein Bißchen interessant.