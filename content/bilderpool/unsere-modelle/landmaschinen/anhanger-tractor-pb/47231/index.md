---
layout: "image"
title: "Anh-f03-04"
date: "2018-02-01T15:19:18"
picture: "anhaenger04.jpg"
weight: "4"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/47231
imported:
- "2019"
_4images_image_id: "47231"
_4images_cat_id: "3489"
_4images_user_id: "2449"
_4images_image_date: "2018-02-01T15:19:18"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47231 -->
