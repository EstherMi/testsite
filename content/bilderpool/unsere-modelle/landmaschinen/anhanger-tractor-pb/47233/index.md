---
layout: "image"
title: "Anh-f06-07"
date: "2018-02-01T15:19:19"
picture: "anhaenger06.jpg"
weight: "6"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/47233
imported:
- "2019"
_4images_image_id: "47233"
_4images_cat_id: "3489"
_4images_user_id: "2449"
_4images_image_date: "2018-02-01T15:19:19"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47233 -->
