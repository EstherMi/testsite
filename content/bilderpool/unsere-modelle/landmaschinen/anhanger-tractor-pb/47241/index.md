---
layout: "image"
title: "Anh-f21"
date: "2018-02-01T15:19:24"
picture: "anhaenger14.jpg"
weight: "14"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/47241
imported:
- "2019"
_4images_image_id: "47241"
_4images_cat_id: "3489"
_4images_user_id: "2449"
_4images_image_date: "2018-02-01T15:19:24"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47241 -->
