---
layout: "image"
title: "anhaengertractorpb9.jpg"
date: "2018-01-21T20:14:24"
picture: "anhaengertractorpb9.jpg"
weight: "23"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/47172
imported:
- "2019"
_4images_image_id: "47172"
_4images_cat_id: "3489"
_4images_user_id: "2449"
_4images_image_date: "2018-01-21T20:14:24"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47172 -->
