---
layout: "image"
title: "anhaengertractorpb8.jpg"
date: "2018-01-21T20:14:24"
picture: "anhaengertractorpb8.jpg"
weight: "22"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/47171
imported:
- "2019"
_4images_image_id: "47171"
_4images_cat_id: "3489"
_4images_user_id: "2449"
_4images_image_date: "2018-01-21T20:14:24"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47171 -->
Und in der Combi met der Ernter. Den gelben Tractor ist jedenfalls ausgestattet mit ein Powermotor 50:1. Das war ziemlich einfach zu realisieren. Später werde ich vielleicht mal ein paar Baufase-bilder hochladen um zu zeigen wie das geht, und auch von einen Version der Tractor mit nur Handbediening (also ohne Motoren und Elektronik) der schon mal in Niederländischen ft-clubblad publiziert worden ist.