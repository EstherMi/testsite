---
layout: "overview"
title: "Quadtrac"
date: 2019-12-17T19:33:02+01:00
legacy_id:
- categories/2947
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2947 --> 
Dies ist mein Nachbau des leistungsfähigen Traktors Quadtrac Case IH 620, der in den USA gebaut wird. Besonders an diesem Traktor ist, dass er an Stelle von Rädern Ketten besitzt und über eine Knicklenkung gesteuert werden kann. Ich habe in mein Modell zahlreiche Funktionen wie "Allradantrieb", eine Knicklenkung und diverse Gelenke zum Geländeausgleich eingebaut...