---
layout: "image"
title: "Lenkung und Geländeausgleich"
date: "2014-09-12T11:45:54"
picture: "qtrac11.jpg"
weight: "11"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/39345
imported:
- "2019"
_4images_image_id: "39345"
_4images_cat_id: "2947"
_4images_user_id: "2228"
_4images_image_date: "2014-09-12T11:45:54"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39345 -->
Hier sind Knicklenkung und das Gelenk zu sehen. Zusammen bilden sie so das Herzstück dieses Traktors. Die Konstruktion ist stabil und platzsparend zugleich.
Das Lenksystem ist das selbe wie in meinem Modell Dumper (http://ftcommunity.de/categories.php?cat_id=2929)