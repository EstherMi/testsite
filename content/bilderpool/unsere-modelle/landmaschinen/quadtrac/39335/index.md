---
layout: "image"
title: "Quadtrac"
date: "2014-09-12T11:45:42"
picture: "qtrac01.jpg"
weight: "1"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/39335
imported:
- "2019"
_4images_image_id: "39335"
_4images_cat_id: "2947"
_4images_user_id: "2228"
_4images_image_date: "2014-09-12T11:45:42"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39335 -->
Informationen:
- "Allradantrieb", es wird je ein Kettenpaar mit einem Encoder Motor angetrieben
- pneumatische Knicklenkung, als Kompressor dient der Fischertechnik Kompressor
- sehr geländegängig dank Gelenken, die Differenzen im Untergrund ausgleichen können
- Beleuchtung vorne und hinten
- optisch am Quadtrac Case IH 620 orientiert