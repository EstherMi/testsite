---
layout: "comment"
hidden: true
title: "19509"
date: "2014-09-14T11:03:17"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Nein, das Unterteil pendelt problemlos. Die schwarzen Z20 sind einzeln an das Fahrwerk montiert und können sich so unabhängig von einander drehen, d.h. die Außenlinie ändert sich nicht.
Die Idee mit den Metallteilen finde ich gut. Ich bräuchte allerdings noch ein paar Spezialteile, um Metallachsen mit dem ft-Differentialgetriebe zu verbinden. Mal sehen, in knapp zwei Wochen ist Convention... Vielleicht hat TST die passende Lösung.

Gruß, David