---
layout: "image"
title: "Drehgelenkt 1"
date: "2014-09-12T11:45:42"
picture: "qtrac07.jpg"
weight: "7"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/39341
imported:
- "2019"
_4images_image_id: "39341"
_4images_cat_id: "2947"
_4images_user_id: "2228"
_4images_image_date: "2014-09-12T11:45:42"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39341 -->
An die Bausteine links im Bild ist die Knicklenkung angebracht, an die Bausteine Rechts im Bild der Hinterwagen. Das schwarze Zahnrad ist fest mit dem rechten roten Rad und dem Hinterwagen verbunden. Das linke rote Rad ist nur auf das schwarze Rad aufgesteckt und kann sich um einen bestimmten Winkel drehen.