---
layout: "image"
title: "ausgeklappt+fertig verspannt"
date: "2007-03-11T12:22:29"
picture: "seilkranmitfahrzeug04.jpg"
weight: "4"
konstrukteure: 
- "Masked (Martin)"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/9398
imported:
- "2019"
_4images_image_id: "9398"
_4images_cat_id: "867"
_4images_user_id: "373"
_4images_image_date: "2007-03-11T12:22:29"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9398 -->
In der Realität werden die Seile an Bäumen befestigt, das geht nur hier nicht mangels maßstabsgetreuer Bäume ;-) Also habe ich Zeltheringe genommen.