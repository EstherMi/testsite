---
layout: "image"
title: "Mastspitze"
date: "2007-03-11T12:22:29"
picture: "seilkranmitfahrzeug07.jpg"
weight: "7"
konstrukteure: 
- "Masked (Martin)"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/9401
imported:
- "2019"
_4images_image_id: "9401"
_4images_cat_id: "867"
_4images_user_id: "373"
_4images_image_date: "2007-03-11T12:22:29"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9401 -->
Der Mast ist vom Boden aus etwa 40cm hoch. An der Seilführung muss ich nochmal arbeiten, das gefällt mir noch nicht wirklich.