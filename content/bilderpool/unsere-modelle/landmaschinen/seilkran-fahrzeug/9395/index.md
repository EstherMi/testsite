---
layout: "image"
title: "Gesamtansicht"
date: "2007-03-11T12:22:29"
picture: "seilkranmitfahrzeug01.jpg"
weight: "1"
konstrukteure: 
- "Masked (Martin)"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/9395
imported:
- "2019"
_4images_image_id: "9395"
_4images_cat_id: "867"
_4images_user_id: "373"
_4images_image_date: "2007-03-11T12:22:29"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9395 -->
Nachdem ich ja schonmal einen Seilkran gebaut hatte (http://www.ftcommunity.de/categories.php?cat_id=508), hier jetzt einer mit dem in Wäldern Holz transportiert wird. Nämlich überall da, wo man mit Traktoren, Harvester, etc. nicht mehr hinkommt, weils zu steil oder zu eng ist. Für Vorbildaufnahmen einfach mal bei Google "Seilkran" eingeben.

Hier ist der Mast eingeklappt. Aufgebaut ist das Ganze auf einem Cars+Trucks-Grundmodell, bei dem ich die vordere Achse um 2 Nuten nach vorne verschoben habe. Wie man sieht habe ich Traktorreifen statt den Normalen Reifen genommen, es passt einfach besser zu einem Geländefahrzeug. Selbstverständlich ist das Fahrzeug komplett motorisiert und mit IR-Control ausgerüstet.