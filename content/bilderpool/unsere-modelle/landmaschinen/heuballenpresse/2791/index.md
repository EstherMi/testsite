---
layout: "image"
title: "Heuballenpresse 010"
date: "2004-11-03T12:23:14"
picture: "Heuballenpresse_010.JPG"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/2791
imported:
- "2019"
_4images_image_id: "2791"
_4images_cat_id: "273"
_4images_user_id: "5"
_4images_image_date: "2004-11-03T12:23:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2791 -->
von Claus-W. Ludwig