---
layout: "image"
title: "Fendt-Holland"
date: "2004-10-24T22:43:10"
picture: "Fischertechnik-modellen-Fendt_001.jpg"
weight: "30"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/2747
imported:
- "2019"
_4images_image_id: "2747"
_4images_cat_id: "268"
_4images_user_id: "22"
_4images_image_date: "2004-10-24T22:43:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2747 -->
