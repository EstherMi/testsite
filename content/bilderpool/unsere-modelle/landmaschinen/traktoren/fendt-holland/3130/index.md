---
layout: "image"
title: "Case-International"
date: "2004-11-15T11:18:21"
picture: "Fischertechnik-modellen-Fendt_038.jpg"
weight: "7"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/3130
imported:
- "2019"
_4images_image_id: "3130"
_4images_cat_id: "268"
_4images_user_id: "22"
_4images_image_date: "2004-11-15T11:18:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3130 -->
