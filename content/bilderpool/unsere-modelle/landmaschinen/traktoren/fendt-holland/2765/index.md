---
layout: "image"
title: "Fendt-Holland"
date: "2004-10-31T21:19:47"
picture: "Fischertechnik-modellen-Fendt_021.jpg"
weight: "22"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/2765
imported:
- "2019"
_4images_image_id: "2765"
_4images_cat_id: "268"
_4images_user_id: "22"
_4images_image_date: "2004-10-31T21:19:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2765 -->
Fendt-Holland weiter in Anbau....