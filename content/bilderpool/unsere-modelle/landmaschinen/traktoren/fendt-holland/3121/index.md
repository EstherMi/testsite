---
layout: "image"
title: "Case-International"
date: "2004-11-14T21:52:45"
picture: "Fischertechnik-modellen-Fendt_029.jpg"
weight: "15"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/3121
imported:
- "2019"
_4images_image_id: "3121"
_4images_cat_id: "268"
_4images_user_id: "22"
_4images_image_date: "2004-11-14T21:52:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3121 -->
