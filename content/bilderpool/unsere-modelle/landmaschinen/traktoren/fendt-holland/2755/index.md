---
layout: "image"
title: "Fendt-Holland"
date: "2004-10-25T13:44:57"
picture: "Fischertechnik-modellen-Fendt_009.jpg"
weight: "24"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/2755
imported:
- "2019"
_4images_image_id: "2755"
_4images_cat_id: "268"
_4images_user_id: "22"
_4images_image_date: "2004-10-25T13:44:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2755 -->
