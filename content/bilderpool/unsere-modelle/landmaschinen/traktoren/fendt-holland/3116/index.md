---
layout: "image"
title: "Case-International"
date: "2004-11-14T21:52:44"
picture: "Fischertechnik-modellen-Fendt_024.jpg"
weight: "10"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/3116
imported:
- "2019"
_4images_image_id: "3116"
_4images_cat_id: "268"
_4images_user_id: "22"
_4images_image_date: "2004-11-14T21:52:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3116 -->
