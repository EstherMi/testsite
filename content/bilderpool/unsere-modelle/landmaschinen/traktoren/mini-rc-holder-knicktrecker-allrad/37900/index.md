---
layout: "image"
title: "Sicht von unten."
date: "2013-12-08T11:56:44"
picture: "minircholderknicktreckermitallrad4.jpg"
weight: "6"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- details/37900
imported:
- "2019"
_4images_image_id: "37900"
_4images_cat_id: "2818"
_4images_user_id: "1582"
_4images_image_date: "2013-12-08T11:56:44"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37900 -->
Einbau des Servos braucht gut klemmende 7.5mm Teile, sonst wird das etwas labil. Ist nicht so einfach, das auf 30mm Breite wirklich hinzubekommen, evtl. geht das auch noch stabiler. Reicht aber für den normalen Spielbetrieb gut aus.