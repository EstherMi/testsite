---
layout: "image"
title: "Mini-Traktor 3"
date: "2007-05-22T21:04:55"
picture: "Traktor_3.jpg"
weight: "7"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/10486
imported:
- "2019"
_4images_image_id: "10486"
_4images_cat_id: "956"
_4images_user_id: "328"
_4images_image_date: "2007-05-22T21:04:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10486 -->
Der Lenk-Motor hat leider nicht mehr unter die vordere Verkleidung gepasst, aber es ist trotzdem ganz schick geworden.

Die Pendelachse benötigt aus Stabilitätsgründen zwingend zwei Gelenksteine, obwohl es mit nur einem eleganter aussieht.