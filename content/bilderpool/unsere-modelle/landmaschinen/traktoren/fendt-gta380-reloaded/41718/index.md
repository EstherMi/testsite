---
layout: "image"
title: "Rückseite"
date: "2015-08-05T21:06:56"
picture: "gtamod04.jpg"
weight: "10"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/41718
imported:
- "2019"
_4images_image_id: "41718"
_4images_cat_id: "3313"
_4images_user_id: "4"
_4images_image_date: "2015-08-05T21:06:56"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41718 -->
Die zwei Rastadapter an der Rückseite des Kabinendachs betätigen die zwei Minitaster für Licht und Kompressor. Zum Einschalten zieht man sie heraus, und ein zweiter Rastadapter am anderen Ende der Achse 30 betätigt den Taster.

Der Antrieb der Hinterachse geschieht mit Untersetzung 1:4 über die Rast-Z10, die auf der hinteren Seite mit den Z40 an den Rädern kämmen. Das musste sein, damit das Mitteldifferenzial weit genug nach hinten kommt.