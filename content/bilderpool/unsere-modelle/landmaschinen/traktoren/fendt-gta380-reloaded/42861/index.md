---
layout: "image"
title: "Mk3: Hinterachse von unten"
date: "2016-02-08T00:03:38"
picture: "gtamk3.jpg"
weight: "3"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/42861
imported:
- "2019"
_4images_image_id: "42861"
_4images_cat_id: "3313"
_4images_user_id: "4"
_4images_image_date: "2016-02-08T00:03:38"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42861 -->
Die Hinterräder haben jetzt ein Planetengetriebe mit Untersetzung 1:3  in die Reifen integriert: Die beiden Stahlachsen für die beiden Räder sind quer durch den Block hindurch am Glanz zu erkennen. Außen sitzen Z10 mit Messingbund (von Andreas Tacke) lose auf den Achsen, denn sie sollen nur verhindern, dass die antreibenden Z10 auf der Rückseite (hier oben) nicht nach innen ausweichen können.