---
layout: "image"
title: "Vorderachse"
date: "2015-08-05T21:06:56"
picture: "gtamod05.jpg"
weight: "11"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/41719
imported:
- "2019"
_4images_image_id: "41719"
_4images_cat_id: "3313"
_4images_user_id: "4"
_4images_image_date: "2015-08-05T21:06:56"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41719 -->
Mit dem Rastsystem ist in der gewünschten Spurbreite nichts zu machen. Deshalb wird eine zweite Reihe von Achse dazwischen gesetzt. So ähnlich habe ich das bei der Vorderachse vom Land Rover auch schon gemacht ( http://ftcommunity.de/details.php?image_id=37692 ), nur hier ist es ein bisschen kompakter geworden