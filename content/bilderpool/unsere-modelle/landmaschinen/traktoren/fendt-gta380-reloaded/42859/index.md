---
layout: "image"
title: "Blockbauweise (09-2015)"
date: "2016-02-08T00:03:38"
picture: "gtamk1.jpg"
weight: "1"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/42859
imported:
- "2019"
_4images_image_id: "42859"
_4images_cat_id: "3313"
_4images_user_id: "4"
_4images_image_date: "2016-02-08T00:03:38"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42859 -->
Das ist der Motorblock vom Herbst 2015: der Block enthält 
- den Fahrmotor (schwarz), 
- den Motor für die durchgehende Zapfwelle (rot,oben) 
- das Längsdifferenzial (hier in Bildmitte unter dem Z10 zu erahnen)
- das Hinterachsdifferenzial (hier verdeckt unter der Platte 90x30).

Die Vorderachse kommt auf die rechte Seite, Durch den etwas heraus stehenden BS15-Loch kommt die tragende Achse fürs Hinterrad, an dem innen ein Z40 montiert ist. Dieses wird von einem Z10 angetrieben, das hier im Bild ganz links unten mittels BS15-Lock an den Verbinder 15 dran montiert zu denken ist und im Bild http://ftcommunity.de/details.php?image_id=41720 auch zu sehen ist.

Dieses Z10 auf K-Achse geht natürlich den Weg des geringsten Widerstands und biegt sich unter deutlich vernehmbaren Knattern nach hinten weg, wenn zu viel Arbeit von ihm verlangt wird. Das war nicht viel, und deshalb hat der Traktor in Dreieich auch mehr gestanden als dass er gefahren wäre.