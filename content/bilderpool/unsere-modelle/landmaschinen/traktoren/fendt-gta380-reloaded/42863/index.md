---
layout: "image"
title: "Mk3: Hinterachse"
date: "2016-02-08T00:03:38"
picture: "gtamk5.jpg"
weight: "5"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/42863
imported:
- "2019"
_4images_image_id: "42863"
_4images_cat_id: "3313"
_4images_user_id: "4"
_4images_image_date: "2016-02-08T00:03:38"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42863 -->
Das Differenzial sitzt hinter der Schneckenmutter mit dem Kugellager, und zwischen zwei Grundplatten 45x45. Unten herum bilden die beiden Zapfwellenmotoren die tragende Konstruktion.

Die Baugruppe links unten gehört mit den nach hinten zeigenden Zapfen von unten an die Bauplatte 45x45 montiert. Die mittlere K-Achse wird dabei durch die Metallachse vom Hinterrad ersetzt, die jetzt links liegende K-Achse kommt durchs Kugellager hindurch ins Differenzial. Das dritteZ10 läuft einfach so mit und ist eigentlich unnütz (sie sollte mal das Antriebszahnrad gegen Ausweichen abstützen, aber das tut ja schon das frei laufende Z10 auf der Stahlachse).

Das Zahnarztwerkzeug mit dem Haken hat sich sehr bewährt. Insbesondere kann man damit "voll versenkte" Verbinder 15 oder 30 wieder heraus-pulen, und Klemmbuchsen aus den Tiefen der Konstruktion fischen, die beim Zusammenbau der Hinterachse regelmäßig vor der Achse ausweichen.