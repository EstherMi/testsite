---
layout: "comment"
hidden: true
title: "21672"
date: "2016-02-11T21:49:19"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Au, wie peinlich - erwischt :-) Da steckt also ein gutes halbes Kardan drin, wenn ich richtig lese. Dann ist's klar. Er ist halt einfach genial, der Harald, und der geometer vergisst wohl überhaupt niemals nichts, was er mal gelesen hatte :-) Danke!

Gruß,
Stefan