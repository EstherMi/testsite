---
layout: "image"
title: "Schnitt durch die Seite"
date: "2015-08-05T21:06:56"
picture: "gtamod07.jpg"
weight: "13"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/41721
imported:
- "2019"
_4images_image_id: "41721"
_4images_cat_id: "3313"
_4images_user_id: "4"
_4images_image_date: "2015-08-05T21:06:56"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41721 -->
Der etwas heraus stehende BS15-Loch nimmt die Achse auf, auf der das Hinterrad montiert ist. Das Z10 dahinter (links) ist der Antrieb; es sitzt auf der Achse des Hinterachsdifferenzials.

Unter dem Kotflügel findet man einen Exoten: "BSB Spur N Grundplatte" 36093, für den es aber beim FanClub-Tag 2015 Nachschub direkt aus der Spritzgussmaschine gab.