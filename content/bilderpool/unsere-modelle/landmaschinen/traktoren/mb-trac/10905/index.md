---
layout: "image"
title: "MB-Truck 20"
date: "2007-06-20T17:16:08"
picture: "mbtruck7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/10905
imported:
- "2019"
_4images_image_id: "10905"
_4images_cat_id: "979"
_4images_user_id: "502"
_4images_image_date: "2007-06-20T17:16:08"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10905 -->
