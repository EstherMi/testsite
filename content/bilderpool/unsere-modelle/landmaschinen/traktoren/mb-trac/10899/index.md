---
layout: "image"
title: "MB-Truck 14"
date: "2007-06-20T17:16:07"
picture: "mbtruck1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/10899
imported:
- "2019"
_4images_image_id: "10899"
_4images_cat_id: "979"
_4images_user_id: "502"
_4images_image_date: "2007-06-20T17:16:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10899 -->
