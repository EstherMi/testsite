---
layout: "image"
title: "MB-Truck"
date: "2007-06-10T20:09:31"
picture: "mbtruck01.jpg"
weight: "8"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/10794
imported:
- "2019"
_4images_image_id: "10794"
_4images_cat_id: "979"
_4images_user_id: "502"
_4images_image_date: "2007-06-10T20:09:31"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10794 -->
Nach diesem Bild (und noch ein paar mehr) hab ich mein Modell von einem MB-Truck gebaut, ich denke das ist mir ganz gut gelungen. Insgesamt sind 5 Motoren verbaut.