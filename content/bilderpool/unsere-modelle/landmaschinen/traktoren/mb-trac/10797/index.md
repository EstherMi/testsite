---
layout: "image"
title: "MB-Truck 3"
date: "2007-06-10T20:09:31"
picture: "mbtruck04.jpg"
weight: "11"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/10797
imported:
- "2019"
_4images_image_id: "10797"
_4images_cat_id: "979"
_4images_user_id: "502"
_4images_image_date: "2007-06-10T20:09:31"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10797 -->
