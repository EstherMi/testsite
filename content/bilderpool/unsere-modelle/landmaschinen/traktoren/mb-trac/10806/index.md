---
layout: "image"
title: "MB-Truck 12"
date: "2007-06-10T20:09:31"
picture: "mbtruck13.jpg"
weight: "20"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/10806
imported:
- "2019"
_4images_image_id: "10806"
_4images_cat_id: "979"
_4images_user_id: "502"
_4images_image_date: "2007-06-10T20:09:31"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10806 -->
