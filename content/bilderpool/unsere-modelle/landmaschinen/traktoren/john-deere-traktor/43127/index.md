---
layout: "image"
title: "Traktor von vorn 1"
date: "2016-03-14T17:01:51"
picture: "johndeeretraktor03.jpg"
weight: "4"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- details/43127
imported:
- "2019"
_4images_image_id: "43127"
_4images_cat_id: "3204"
_4images_user_id: "946"
_4images_image_date: "2016-03-14T17:01:51"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43127 -->
Ich habe bei diesem Modell nur die wichtigsten Elemente umgesetzt.: Pendelachse mit integriertem Microservo 132292k, gut verstecktem IR-Empfänger als Armaturenbrett, und mit einem akzeptablen Platz für den Accu 35537.