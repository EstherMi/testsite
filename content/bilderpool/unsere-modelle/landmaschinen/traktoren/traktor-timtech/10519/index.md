---
layout: "image"
title: "Trecker"
date: "2007-05-27T18:23:24"
picture: "PICT0002.jpg"
weight: "15"
konstrukteure: 
- "timtech"
fotografen:
- "timtech"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "timtech"
license: "unknown"
legacy_id:
- details/10519
imported:
- "2019"
_4images_image_id: "10519"
_4images_cat_id: "958"
_4images_user_id: "590"
_4images_image_date: "2007-05-27T18:23:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10519 -->
Hier sieht man meinen Trecker.Er hat Pendelachse,Allrad,Achsschenkellenkung und eine Hydraulische Hebemechanik.