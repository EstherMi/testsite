---
layout: "image"
title: "Messer"
date: "2007-06-05T17:18:26"
picture: "PICT0016.jpg"
weight: "13"
konstrukteure: 
- "timtech"
fotografen:
- "timtech"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "timtech"
license: "unknown"
legacy_id:
- details/10725
imported:
- "2019"
_4images_image_id: "10725"
_4images_cat_id: "958"
_4images_user_id: "590"
_4images_image_date: "2007-06-05T17:18:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10725 -->
Hier sieht man die scharfen und dünnen Kattermesser.