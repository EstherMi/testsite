---
layout: "image"
title: "trecker neu"
date: "2007-09-29T12:37:09"
picture: "fischertechnik_001.jpg"
weight: "6"
konstrukteure: 
- "timtech"
fotografen:
- "timtech"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "timtech"
license: "unknown"
legacy_id:
- details/12037
imported:
- "2019"
_4images_image_id: "12037"
_4images_cat_id: "958"
_4images_user_id: "590"
_4images_image_date: "2007-09-29T12:37:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12037 -->
Hier sieht man meinen neuen Trecker,er hat eine Pendelachse , er hat Allrad, einen ausklinck baren Kompressor, aber noch keine Hydraulik.