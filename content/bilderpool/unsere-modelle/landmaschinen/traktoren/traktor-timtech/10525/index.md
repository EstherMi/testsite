---
layout: "image"
title: "Zapfwelle"
date: "2007-05-27T18:23:24"
picture: "PICT0009.jpg"
weight: "21"
konstrukteure: 
- "timtech"
fotografen:
- "timtech"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "timtech"
license: "unknown"
legacy_id:
- details/10525
imported:
- "2019"
_4images_image_id: "10525"
_4images_cat_id: "958"
_4images_user_id: "590"
_4images_image_date: "2007-05-27T18:23:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10525 -->
Hier sieht man die Zapfwelle für das Anbaugerät.