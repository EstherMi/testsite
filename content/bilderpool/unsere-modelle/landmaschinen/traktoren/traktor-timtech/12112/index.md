---
layout: "image"
title: "kompressor ausgebaut"
date: "2007-10-03T11:19:38"
picture: "fischertechnik_010.jpg"
weight: "2"
konstrukteure: 
- "timtech"
fotografen:
- "timtech"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "timtech"
license: "unknown"
legacy_id:
- details/12112
imported:
- "2019"
_4images_image_id: "12112"
_4images_cat_id: "958"
_4images_user_id: "590"
_4images_image_date: "2007-10-03T11:19:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12112 -->
Hier sieht man meinen Kompressor ausgebaut.