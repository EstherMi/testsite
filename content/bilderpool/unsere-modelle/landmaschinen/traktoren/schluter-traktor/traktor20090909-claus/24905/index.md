---
layout: "image"
title: "[2/5] Diagonalansicht rechts"
date: "2009-09-10T21:27:15"
picture: "traktorclaus2.jpg"
weight: "2"
konstrukteure: 
- "Claus-Werner Ludwig"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/24905
imported:
- "2019"
_4images_image_id: "24905"
_4images_cat_id: "1716"
_4images_user_id: "723"
_4images_image_date: "2009-09-10T21:27:15"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24905 -->
