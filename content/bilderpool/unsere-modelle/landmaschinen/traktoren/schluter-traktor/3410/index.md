---
layout: "image"
title: "Schlueter Traktor 018"
date: "2004-12-05T10:28:42"
picture: "Schlueter_Traktor_018.JPG"
weight: "18"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/3410
imported:
- "2019"
_4images_image_id: "3410"
_4images_cat_id: "295"
_4images_user_id: "5"
_4images_image_date: "2004-12-05T10:28:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3410 -->
