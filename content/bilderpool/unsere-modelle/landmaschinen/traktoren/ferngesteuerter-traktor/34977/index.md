---
layout: "image"
title: "ferngesteuerter Traktor"
date: "2012-05-19T20:22:22"
picture: "ferngesteuertertraktor7.jpg"
weight: "7"
konstrukteure: 
- "Michael (coini)"
fotografen:
- "Michael (coini)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "coini"
license: "unknown"
legacy_id:
- details/34977
imported:
- "2019"
_4images_image_id: "34977"
_4images_cat_id: "2589"
_4images_user_id: "1476"
_4images_image_date: "2012-05-19T20:22:22"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34977 -->
