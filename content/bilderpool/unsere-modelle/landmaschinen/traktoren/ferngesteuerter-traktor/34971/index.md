---
layout: "image"
title: "ferngesteuerter Traktor"
date: "2012-05-19T20:22:22"
picture: "ferngesteuertertraktor1.jpg"
weight: "1"
konstrukteure: 
- "Michael (coini)"
fotografen:
- "Michael (coini)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "coini"
license: "unknown"
legacy_id:
- details/34971
imported:
- "2019"
_4images_image_id: "34971"
_4images_cat_id: "2589"
_4images_user_id: "1476"
_4images_image_date: "2012-05-19T20:22:22"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34971 -->
