---
layout: "image"
title: "Noch nen´ Trecker"
date: "2009-07-03T09:11:45"
picture: "Traktor_2003.jpg"
weight: "6"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/24488
imported:
- "2019"
_4images_image_id: "24488"
_4images_cat_id: "605"
_4images_user_id: "968"
_4images_image_date: "2009-07-03T09:11:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24488 -->
