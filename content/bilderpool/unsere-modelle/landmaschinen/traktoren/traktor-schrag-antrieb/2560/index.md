---
layout: "image"
title: "Traktor mit schrag-Antrieb"
date: "2004-09-07T18:05:15"
picture: "DSC00192.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/2560
imported:
- "2019"
_4images_image_id: "2560"
_4images_cat_id: "243"
_4images_user_id: "22"
_4images_image_date: "2004-09-07T18:05:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2560 -->
Traktor mit schrag-Antrieb