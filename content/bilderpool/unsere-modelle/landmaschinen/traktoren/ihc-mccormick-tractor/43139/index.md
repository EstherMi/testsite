---
layout: "image"
title: "IHC Vongtion Pendelachse"
date: "2016-03-20T18:05:42"
picture: "ihcmccormicktractor05.jpg"
weight: "5"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- details/43139
imported:
- "2019"
_4images_image_id: "43139"
_4images_cat_id: "3205"
_4images_user_id: "946"
_4images_image_date: "2016-03-20T18:05:42"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43139 -->
ohne Beschreibung