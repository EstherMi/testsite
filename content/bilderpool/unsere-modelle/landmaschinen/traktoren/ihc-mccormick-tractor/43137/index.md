---
layout: "image"
title: "IHC Hinterachse Geräteträger"
date: "2016-03-20T18:05:42"
picture: "ihcmccormicktractor03.jpg"
weight: "3"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- details/43137
imported:
- "2019"
_4images_image_id: "43137"
_4images_cat_id: "3205"
_4images_user_id: "946"
_4images_image_date: "2016-03-20T18:05:42"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43137 -->
Ich glaube das Bild erklärt sich in seiner Konstruktion von selbst.