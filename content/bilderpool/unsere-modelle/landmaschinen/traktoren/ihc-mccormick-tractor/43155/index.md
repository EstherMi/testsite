---
layout: "image"
title: "IHC mit Dreischaren Pflug"
date: "2016-03-20T18:05:55"
picture: "ihcmccormicktractor21.jpg"
weight: "21"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- details/43155
imported:
- "2019"
_4images_image_id: "43155"
_4images_cat_id: "3205"
_4images_user_id: "946"
_4images_image_date: "2016-03-20T18:05:55"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43155 -->
ohne Beschreibung