---
layout: "image"
title: "IHC von hinten oben"
date: "2016-03-20T18:05:55"
picture: "ihcmccormicktractor30.jpg"
weight: "30"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- details/43164
imported:
- "2019"
_4images_image_id: "43164"
_4images_cat_id: "3205"
_4images_user_id: "946"
_4images_image_date: "2016-03-20T18:05:55"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43164 -->
ohne Beschreibung