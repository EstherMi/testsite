---
layout: "image"
title: "IHC von hinten mit Heuwender"
date: "2016-03-20T18:05:55"
picture: "ihcmccormicktractor20.jpg"
weight: "20"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- details/43154
imported:
- "2019"
_4images_image_id: "43154"
_4images_cat_id: "3205"
_4images_user_id: "946"
_4images_image_date: "2016-03-20T18:05:55"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43154 -->
ohne Beschreibung