---
layout: "image"
title: "IHC Hinterachse 2"
date: "2016-03-20T18:05:55"
picture: "ihcmccormicktractor11.jpg"
weight: "11"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- details/43145
imported:
- "2019"
_4images_image_id: "43145"
_4images_cat_id: "3205"
_4images_user_id: "946"
_4images_image_date: "2016-03-20T18:05:55"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43145 -->
Die Bausteine grau 31007 verhindern das Wegrutschen der Powermoter-Anbauplatte 35090 im unteren Bereich.