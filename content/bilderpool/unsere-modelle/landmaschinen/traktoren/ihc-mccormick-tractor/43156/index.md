---
layout: "image"
title: "IHC mit Dreischaren Pflug von hinten"
date: "2016-03-20T18:05:55"
picture: "ihcmccormicktractor22.jpg"
weight: "22"
konstrukteure: 
- "Detlef Ottmann"
fotografen:
- "Detlef Ottmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Detlef Ottmann"
license: "unknown"
legacy_id:
- details/43156
imported:
- "2019"
_4images_image_id: "43156"
_4images_cat_id: "3205"
_4images_user_id: "946"
_4images_image_date: "2016-03-20T18:05:55"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43156 -->
ohne Beschreibung