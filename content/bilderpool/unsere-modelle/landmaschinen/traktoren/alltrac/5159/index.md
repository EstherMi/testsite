---
layout: "image"
title: "AllTrac 9"
date: "2005-10-30T16:57:12"
picture: "AllTrac_09.jpg"
weight: "9"
konstrukteure: 
- "Lothar Vogt      Pilami"
fotografen:
- "Lothar Vogt      Pilami"
keywords: ["Frontantrieb"]
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- details/5159
imported:
- "2019"
_4images_image_id: "5159"
_4images_cat_id: "408"
_4images_user_id: "10"
_4images_image_date: "2005-10-30T16:57:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5159 -->
Blick auf den Vorderradantrieb mit Lenkung. Die beiden gelben Spurstangen sind für die Stabilität zwingend nötig. Sie ermöglichen ein sauberes Rückwärtsfahren.