---
layout: "image"
title: "AllTrac 11"
date: "2005-10-30T17:09:48"
picture: "AllTrac_11.jpg"
weight: "11"
konstrukteure: 
- "Lothar Vogt      Pilami"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- details/5161
imported:
- "2019"
_4images_image_id: "5161"
_4images_cat_id: "408"
_4images_user_id: "10"
_4images_image_date: "2005-10-30T17:09:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5161 -->
Blick auf den mittleren Teil mit drei Antriebssträngen:
oben: Antrieb für den hinteren Geräteanbau.
von links mitte nach rechts unten: Zapfwelle
von links unten nach rechts oben: Radantrieb.
Insgesammt sind 10 Kardangelenke verbaut.