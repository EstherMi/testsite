---
layout: "image"
title: "AllTrac 7"
date: "2005-10-30T16:57:12"
picture: "AllTrac_07.jpg"
weight: "7"
konstrukteure: 
- "Lothar Vogt      Pilami"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- details/5157
imported:
- "2019"
_4images_image_id: "5157"
_4images_cat_id: "408"
_4images_user_id: "10"
_4images_image_date: "2005-10-30T16:57:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5157 -->
Blick in die Lenkung
Im Vordergrund ist das vordere Ende des Zapfwellenanschlusses zu sehen. Die hintere und vordere Zapfwelle laufen parallel und werden über einen Powermotor angetrieben.