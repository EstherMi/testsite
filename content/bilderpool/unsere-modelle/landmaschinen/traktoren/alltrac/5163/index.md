---
layout: "image"
title: "AllTrac 13"
date: "2005-10-30T17:09:48"
picture: "AllTrac_13.jpg"
weight: "13"
konstrukteure: 
- "Lothar Vogt      Pilami"
fotografen:
- "Lothar Vogt      Pilami"
keywords: ["Großreifen", "Monsterreifen", "Allrad"]
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- details/5163
imported:
- "2019"
_4images_image_id: "5163"
_4images_cat_id: "408"
_4images_user_id: "10"
_4images_image_date: "2005-10-30T17:09:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5163 -->
Gesammtansicht.
Die Zapfwelle nach hinten wird durch ein nur locker sitzendes 30er Zahnrad geführt, das wiederrum die gleichmässige Kraftverteilung auf die linke und rechte Seite des hinteren Heckanbauvorrichtung gewährleistet.
Das Prinzip stammt von Harald.