---
layout: "comment"
hidden: true
title: "1180"
date: "2006-07-08T20:15:57"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ich würde sagen, das ist deshalb kein Problem, weil zwischen Vorder- und Hinterachse auch ein Differential sitzt, welches diese Unterschiede automatisch ausgleicht. (Aber ich möchte Lother nicht vorgreifen ;-)

Gruß,
Stefan