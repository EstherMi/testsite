---
layout: "image"
title: "AllTrac 12"
date: "2005-10-30T17:09:48"
picture: "AllTrac_12.jpg"
weight: "12"
konstrukteure: 
- "Lothar Vogt      Pilami"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- details/5162
imported:
- "2019"
_4images_image_id: "5162"
_4images_cat_id: "408"
_4images_user_id: "10"
_4images_image_date: "2005-10-30T17:09:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5162 -->
Blick auf den Antriebsstrang mit Mitteldifferential. Unter dem Differential kann man die beiden Powermotoren erkennen. Der untere für den Antrieb, der obere für die Zapfwelle.