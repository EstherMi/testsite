---
layout: "image"
title: "AllTrac 6"
date: "2005-10-30T16:57:12"
picture: "AllTrac_06.jpg"
weight: "6"
konstrukteure: 
- "Lothar Vogt      Pilami"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- details/5156
imported:
- "2019"
_4images_image_id: "5156"
_4images_cat_id: "408"
_4images_user_id: "10"
_4images_image_date: "2005-10-30T16:57:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5156 -->
Hier kann man den doch grossen Lenkeinschlag sehen