---
layout: "image"
title: "Fendt380GTA05.jpg"
date: "2015-07-19T20:39:07"
picture: "P1110123mit.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/41455
imported:
- "2019"
_4images_image_id: "41455"
_4images_cat_id: "3099"
_4images_user_id: "4"
_4images_image_date: "2015-07-19T20:39:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41455 -->
Der Zapfwellenantrieb geht ein wenig über Eck, aber er geht. Die Vorderachse schwenkt um die Achse, die quer im BS30-Loch drin steckt und nach vorn bis in die gelben Winkelträger reicht. Deren Stirnflächen wurden gemoddet, damit die Achse da durch geht.