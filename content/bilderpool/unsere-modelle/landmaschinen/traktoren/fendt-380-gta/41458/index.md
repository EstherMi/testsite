---
layout: "image"
title: "Fendt380GTA08.jpg"
date: "2015-07-19T20:46:02"
picture: "P1110126mit.JPG"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/41458
imported:
- "2019"
_4images_image_id: "41458"
_4images_cat_id: "3099"
_4images_user_id: "4"
_4images_image_date: "2015-07-19T20:46:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41458 -->
Ein Erinnerungsfoto an die beiden Kurzhub-Zylinder. Die hab ich nicht mehr (sie wurden verkauft), und bis ein Ersatz aus dem 3D-Drucker kommt, vergeht noch Zeit.