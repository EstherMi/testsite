---
layout: "image"
title: "Monster03"
date: "2004-11-01T10:20:22"
picture: "Monster03.jpg"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Traktor", "Monsterreifen", "Großreifen"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2746
imported:
- "2019"
_4images_image_id: "2746"
_4images_cat_id: "267"
_4images_user_id: "4"
_4images_image_date: "2004-11-01T10:20:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2746 -->
Die Innereien des Monsterreifens.