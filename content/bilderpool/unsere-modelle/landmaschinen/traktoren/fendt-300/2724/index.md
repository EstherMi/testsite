---
layout: "image"
title: "Fendt300-06"
date: "2004-11-01T10:20:22"
picture: "Fendt300-06.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Traktor"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2724
imported:
- "2019"
_4images_image_id: "2724"
_4images_cat_id: "267"
_4images_user_id: "4"
_4images_image_date: "2004-11-01T10:20:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2724 -->
Die Lenkung erfolgt über den grauen Mini-Mot (rechts oben), der mit der Vorderachse mitpendelt.

An den Schnecken ist nichts gebohrt oder geklebt, es ist nur das linke Vorderrad (im Bild unten), das den Antrieb am Auseinanderfallen hindert. Ein Knubbel an der Schneckenmutter mußte allerdings weg.