---
layout: "image"
title: "Allradlenkung03.JPG"
date: "2007-09-25T19:04:30"
picture: "Allradlenkung03.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/12011
imported:
- "2019"
_4images_image_id: "12011"
_4images_cat_id: "608"
_4images_user_id: "4"
_4images_image_date: "2007-09-25T19:04:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12011 -->
Das "Alternativ-Hubgetriebe" wird von den beiden K-Achsen 50 und den Winkelsteinen gehalten.