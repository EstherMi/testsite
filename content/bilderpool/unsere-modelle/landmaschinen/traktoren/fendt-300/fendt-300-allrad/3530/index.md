---
layout: "image"
title: "Allrad05.JPG"
date: "2005-01-15T17:27:38"
picture: "Allrad05.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Allrad"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/3530
imported:
- "2019"
_4images_image_id: "3530"
_4images_cat_id: "608"
_4images_user_id: "4"
_4images_image_date: "2005-01-15T17:27:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3530 -->
Das weiße Kegelzahnrad stammt aus einem schwarzen Differenzial.

Damit sollte es wohl werden!