---
layout: "image"
title: "Fendt300-60"
date: "2004-11-01T10:20:22"
picture: "Fendt300-60.jpg"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Traktor", "Großreifen", "Monsterreifen"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2742
imported:
- "2019"
_4images_image_id: "2742"
_4images_cat_id: "267"
_4images_user_id: "4"
_4images_image_date: "2004-11-01T10:20:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2742 -->
