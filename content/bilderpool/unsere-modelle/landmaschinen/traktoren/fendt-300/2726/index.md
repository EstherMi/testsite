---
layout: "image"
title: "Fendt300-08"
date: "2004-11-01T10:20:22"
picture: "Fendt300-08.jpg"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Traktor", "Schnecke", "35977", "modding"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2726
imported:
- "2019"
_4images_image_id: "2726"
_4images_cat_id: "267"
_4images_user_id: "4"
_4images_image_date: "2004-11-01T10:20:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2726 -->
Die schwarzen Mini-Taster (Rücken an Rücken, hier ist nur einer sichtbar) fahren mit rauf und runter und schalten in Endlage den Motor für den Kraftheber ab. Die Messing-Achse in Bildmitte sorgt für die Stabilität in Quer-Richtung, sonst würden die Hinterräder x-beinig dastehen.