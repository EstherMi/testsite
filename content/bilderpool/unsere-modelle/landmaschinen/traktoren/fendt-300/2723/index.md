---
layout: "image"
title: "Fendt300-04"
date: "2004-11-01T10:20:22"
picture: "Fendt300-04.jpg"
weight: "4"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: ["Traktor"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2723
imported:
- "2019"
_4images_image_id: "2723"
_4images_cat_id: "267"
_4images_user_id: "4"
_4images_image_date: "2004-11-01T10:20:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2723 -->
Die Zapfwelle (grauer Mini-Mot in Bildmitte) verläuft durch das Z30 [Korrektur: Z20] des Krafthebers hindurch, welches nur lose auf der Achse sitzt.