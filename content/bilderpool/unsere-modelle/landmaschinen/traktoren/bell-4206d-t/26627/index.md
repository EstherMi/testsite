---
layout: "image"
title: "achterste diff"
date: "2010-03-07T10:12:47"
picture: "P3060367.jpg"
weight: "8"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/26627
imported:
- "2019"
_4images_image_id: "26627"
_4images_cat_id: "1899"
_4images_user_id: "838"
_4images_image_date: "2010-03-07T10:12:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26627 -->
tandwiel overbrenging is nodig tijdens het knikken van de tractor. Er is dan lengte verandering in de aandrijfas. Deze tandwielen schuiven dan ongeveer 3 mm heen en weer.