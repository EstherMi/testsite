---
layout: "image"
title: "Bell 4206D-T"
date: "2010-03-07T10:12:47"
picture: "P3060365.jpg"
weight: "6"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/26625
imported:
- "2019"
_4images_image_id: "26625"
_4images_cat_id: "1899"
_4images_user_id: "838"
_4images_image_date: "2010-03-07T10:12:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26625 -->
flexibele achterkant kan knikken en roteren