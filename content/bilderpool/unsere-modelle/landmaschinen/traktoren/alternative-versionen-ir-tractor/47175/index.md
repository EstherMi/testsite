---
layout: "image"
title: "Tractor-non-IR-fase-03"
date: "2018-01-23T16:33:17"
picture: "tractornonir03.jpg"
weight: "10"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/47175
imported:
- "2019"
_4images_image_id: "47175"
_4images_cat_id: "3490"
_4images_user_id: "2449"
_4images_image_date: "2018-01-23T16:33:17"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47175 -->
