---
layout: "image"
title: "Tractor-PM"
date: "2018-01-24T17:32:34"
picture: "tractorpm1.jpg"
weight: "1"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/47183
imported:
- "2019"
_4images_image_id: "47183"
_4images_cat_id: "3490"
_4images_user_id: "2449"
_4images_image_date: "2018-01-24T17:32:34"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47183 -->
Hier ein Totalbild von der Version des Tractor-IR mit den Powermotor 50:1. Außerdem ist er Gelb, am Ausenseite fast so viel wie möglich. Man kann natürlich auch dafür wählen, den untere Schicht der Chassis in Swarch aus zu führen. Ist auch schön.
Zunächst zeige ich auch wie ich den Empfänger eingebaut habe. Das sieht etwas besser aus als im Original. Für's übrige ist er genau sowie der Original, also ich hab nur wenige Baufasen fotografiert.