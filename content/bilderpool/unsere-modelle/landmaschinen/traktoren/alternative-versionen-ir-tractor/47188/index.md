---
layout: "image"
title: "Tractor-PM-fase05"
date: "2018-01-24T17:32:34"
picture: "tractorpm6.jpg"
weight: "6"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/47188
imported:
- "2019"
_4images_image_id: "47188"
_4images_cat_id: "3490"
_4images_user_id: "2449"
_4images_image_date: "2018-01-24T17:32:34"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47188 -->
