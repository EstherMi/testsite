---
layout: "image"
title: "Bell twin engine"
date: "2010-05-02T22:08:21"
picture: "P5020155.jpg"
weight: "10"
konstrukteure: 
- "ruurd"
fotografen:
- "chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/27046
imported:
- "2019"
_4images_image_id: "27046"
_4images_cat_id: "1948"
_4images_user_id: "838"
_4images_image_date: "2010-05-02T22:08:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27046 -->
2 de Bell compleet nieuw afgezien van de cabine nu voorzien van 2 aandrijfmotoren