---
layout: "image"
title: "diff"
date: "2010-05-02T22:08:21"
picture: "P5020142.jpg"
weight: "11"
konstrukteure: 
- "ruurd"
fotografen:
- "chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/27047
imported:
- "2019"
_4images_image_id: "27047"
_4images_cat_id: "1948"
_4images_user_id: "838"
_4images_image_date: "2010-05-02T22:08:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27047 -->
Onderste diff is voor het koppelen van de 2 motoren en de bovenste is het centraal diff tussen voor en achter as