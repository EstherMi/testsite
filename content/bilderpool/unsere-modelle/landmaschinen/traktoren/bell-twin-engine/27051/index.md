---
layout: "image"
title: "voorkant"
date: "2010-05-03T11:26:29"
picture: "P5020148.jpg"
weight: "3"
konstrukteure: 
- "ruurd"
fotografen:
- "chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/27051
imported:
- "2019"
_4images_image_id: "27051"
_4images_cat_id: "1948"
_4images_user_id: "838"
_4images_image_date: "2010-05-03T11:26:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27051 -->
De aandrijfas van de xm motor gaat onder het diff door en gaat naar het onderste diff voor het koppelen van de 2 motoren