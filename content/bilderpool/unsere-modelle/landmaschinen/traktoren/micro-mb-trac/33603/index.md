---
layout: "image"
title: "Gesamtansicht"
date: "2011-12-03T19:52:32"
picture: "micrombtrac1.jpg"
weight: "1"
konstrukteure: 
- "Stainless"
fotografen:
- "Stainless"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stainless"
license: "unknown"
legacy_id:
- details/33603
imported:
- "2019"
_4images_image_id: "33603"
_4images_cat_id: "2489"
_4images_user_id: "1376"
_4images_image_date: "2011-12-03T19:52:32"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33603 -->
Hier meine Version eines Ferngesteuerten Micro-MB-Trac's. Angetrieben wird über einen S-Motor unten im Chassis, lenken kann er leider nicht. Die Motorhaube wird aus einer Batteriebox, 8 BS7,5 und ein paar anderen Kleinteilen gebildet und sitzt auf dem Chassis. Es wurden ausschließlich Metallachsen verbaut, die hintere ist Platzbedingt gesperrt, die Vordere auch, um den geradeauslauf zu verbessern.