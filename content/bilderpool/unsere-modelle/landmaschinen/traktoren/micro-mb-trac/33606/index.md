---
layout: "image"
title: "Seite"
date: "2011-12-03T19:52:32"
picture: "micrombtrac4.jpg"
weight: "4"
konstrukteure: 
- "Stainless"
fotografen:
- "Stainless"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stainless"
license: "unknown"
legacy_id:
- details/33606
imported:
- "2019"
_4images_image_id: "33606"
_4images_cat_id: "2489"
_4images_user_id: "1376"
_4images_image_date: "2011-12-03T19:52:32"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33606 -->
Die Lampen hinter den Vorderrädern sollten eigentlich als Beleuchtung dienen (wer hätte das gedacht ;o) ), ich hab sie jedoch nicht verkabelt. Das Gewirr in der Kabine reicht jetzt schon.