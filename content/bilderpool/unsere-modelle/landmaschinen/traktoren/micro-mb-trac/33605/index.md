---
layout: "image"
title: "Hinten oben"
date: "2011-12-03T19:52:32"
picture: "micrombtrac3.jpg"
weight: "3"
konstrukteure: 
- "Stainless"
fotografen:
- "Stainless"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stainless"
license: "unknown"
legacy_id:
- details/33605
imported:
- "2019"
_4images_image_id: "33605"
_4images_cat_id: "2489"
_4images_user_id: "1376"
_4images_image_date: "2011-12-03T19:52:32"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33605 -->
