---
layout: "image"
title: "MicroRC-Trac 4"
date: "2013-12-08T16:31:55"
picture: "rcmicrotrac4.jpg"
weight: "4"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- details/37909
imported:
- "2019"
_4images_image_id: "37909"
_4images_cat_id: "2819"
_4images_user_id: "1582"
_4images_image_date: "2013-12-08T16:31:55"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37909 -->
Hat eine gute Bodenfreiheit. Schutzblech leider etwas zu weit hinten. Aber ohne sieht es nicht so gut aus.