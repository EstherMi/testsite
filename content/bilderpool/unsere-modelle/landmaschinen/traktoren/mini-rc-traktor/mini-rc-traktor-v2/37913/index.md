---
layout: "image"
title: "Mini-RC-Traktor V2 1"
date: "2013-12-08T19:22:11"
picture: "minirctraktorv01.jpg"
weight: "1"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- details/37913
imported:
- "2019"
_4images_image_id: "37913"
_4images_cat_id: "2820"
_4images_user_id: "1582"
_4images_image_date: "2013-12-08T19:22:11"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37913 -->
Kaum online, schon gibts ein update: jetzt ist nur ein Rad angetrieben. Damit ist der Trecker langsamer (40km/h => old times 20km/h), aber er geht sehr willig um die Kurven. Auch sind jetzt die Schutzbleche besser montiert. Sonst ist vieles beim Alten - ist vom Lenkeinschlag jetzt auch in der Praxis sehr wendig. Schaumermal, was noch geht.