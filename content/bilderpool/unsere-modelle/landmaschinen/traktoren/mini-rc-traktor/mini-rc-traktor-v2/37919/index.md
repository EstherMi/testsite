---
layout: "image"
title: "Mini-RC-Traktor V2 7"
date: "2013-12-08T19:22:11"
picture: "minirctraktorv07.jpg"
weight: "7"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- details/37919
imported:
- "2019"
_4images_image_id: "37919"
_4images_cat_id: "2820"
_4images_user_id: "1582"
_4images_image_date: "2013-12-08T19:22:11"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37919 -->
Ackublock schaut so eigentlich gar nicht schlecht aus.