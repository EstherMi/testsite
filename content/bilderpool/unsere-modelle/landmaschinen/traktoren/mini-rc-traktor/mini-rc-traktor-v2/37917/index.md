---
layout: "image"
title: "Mini-RC-Traktor V2 5"
date: "2013-12-08T19:22:11"
picture: "minirctraktorv05.jpg"
weight: "5"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- details/37917
imported:
- "2019"
_4images_image_id: "37917"
_4images_cat_id: "2820"
_4images_user_id: "1582"
_4images_image_date: "2013-12-08T19:22:11"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37917 -->
Ist jetzt dank Einzelradantrieb recht wendig und arbeitet sich gut über Teppichfalten.