---
layout: "image"
title: "MicroRC-Trac 1"
date: "2013-12-08T16:31:55"
picture: "rcmicrotrac1.jpg"
weight: "1"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- details/37906
imported:
- "2019"
_4images_image_id: "37906"
_4images_cat_id: "2819"
_4images_user_id: "1582"
_4images_image_date: "2013-12-08T16:31:55"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37906 -->
Mit den neuen grösseren Reifen mal probiert, einen kleinen, Fern-Steuerbaren Traktor zu bauen. Schutzbleche sind nicht optimal, aber man kann soger die Vorderachse verkippen, wenn es ins Gelände geht. Das Teil geht mit dem Direktgetriebe sehr flott, ganz wie diese modernen 40km/h Traktoren... Großer Nachteil ist das fehlende Differential: vorwärts schiebt es doch deutlich aus der Kurve raus. D.h. man muß wahrscheinlich einen Reifen gut mit Öl einschmieren? Oder nur rückwärts fahren? Vielleicht fällt euch ja noch was ein. Finde die Proprtionen nicht so schlecht gelungen. Mag jemand weiterbasteln?