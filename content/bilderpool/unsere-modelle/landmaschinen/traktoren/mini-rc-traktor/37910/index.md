---
layout: "image"
title: "MicroRC-Trac 5"
date: "2013-12-08T16:31:55"
picture: "rcmicrotrac5.jpg"
weight: "5"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- details/37910
imported:
- "2019"
_4images_image_id: "37910"
_4images_cat_id: "2819"
_4images_user_id: "1582"
_4images_image_date: "2013-12-08T16:31:55"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37910 -->
Rückansicht - eine Anhängerkupplung in der Minimalversion musste sein - der Nachwuchs hat sie unbedingt eingefordert...