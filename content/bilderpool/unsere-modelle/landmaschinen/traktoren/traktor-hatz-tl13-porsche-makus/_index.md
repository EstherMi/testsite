---
layout: "overview"
title: "Traktor HATZ TL13 (Porsche-Makus)"
date: 2019-12-17T19:32:01+01:00
legacy_id:
- categories/1289
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1289 --> 
Hallo,

hier ein erster Versuch, meinen HATZ TL13-Ackerschlepper möglichst originalgetreu nachzubauen. Besonders stolz bin ich auf die Lenkung samt Pendelvorderachse, denn das ist wirklich fast genau so wie beim Original.

Später hab ich dann noch das Achsvorgelegegetriebe mit Sperrdifferential dazugebaut, ganz so, wie es das Original vorgab.