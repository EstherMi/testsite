---
layout: "image"
title: "Hinterrad Montageblock (Prototyp)"
date: "2007-09-26T15:07:38"
picture: "DSCN1509.jpg"
weight: "3"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/12014
imported:
- "2019"
_4images_image_id: "12014"
_4images_cat_id: "998"
_4images_user_id: "184"
_4images_image_date: "2007-09-26T15:07:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12014 -->
Hier sieht man wie das mit dem Rastzahnrad (Bild vorher) gemeint ist. Die linke Schneckenmutter trägt zwei Kugellager für die Hinterachse. Läuft "superleicht".