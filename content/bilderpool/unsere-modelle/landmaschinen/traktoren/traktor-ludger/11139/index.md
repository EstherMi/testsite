---
layout: "image"
title: "Radabdeckung"
date: "2007-07-19T14:52:01"
picture: "DSCN1452.jpg"
weight: "10"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/11139
imported:
- "2019"
_4images_image_id: "11139"
_4images_cat_id: "998"
_4images_user_id: "184"
_4images_image_date: "2007-07-19T14:52:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11139 -->
Auch an der Vorderradabdeckung habe ich noch ein wenig gebastelt. Das Vorgängermodell war zu groß, sprich, der Lenkeinschlag wurde dadurch behindert.