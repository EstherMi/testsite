---
layout: "image"
title: "Ansicht von unten"
date: "2007-07-19T14:52:01"
picture: "DSCN1455.jpg"
weight: "9"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/11138
imported:
- "2019"
_4images_image_id: "11138"
_4images_cat_id: "998"
_4images_user_id: "184"
_4images_image_date: "2007-07-19T14:52:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11138 -->
Hier habe ich ihn mal aufs Kreuz gelegt ;-)).

Gut zu erkennen wie die Pendelachse hängt. Insgesamt ein Weg von 4-5 cm.