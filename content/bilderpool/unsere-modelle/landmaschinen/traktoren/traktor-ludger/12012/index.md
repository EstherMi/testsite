---
layout: "image"
title: "schmalere Allrad Vorderachse mit M-Achsen"
date: "2007-09-26T15:07:37"
picture: "DSCN1597.jpg"
weight: "1"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/12012
imported:
- "2019"
_4images_image_id: "12012"
_4images_cat_id: "998"
_4images_user_id: "184"
_4images_image_date: "2007-09-26T15:07:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12012 -->
Hier habe ich die äußeren K-Achsen gegen M-Achsen getauscht. Diese Achsen werden von Andreas (TST) eigentlich für die Differentiale  gebaut. Aber so geht´s auch....