---
layout: "image"
title: "Ansicht"
date: "2007-07-19T14:52:00"
picture: "DSCN1447.jpg"
weight: "5"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/11134
imported:
- "2019"
_4images_image_id: "11134"
_4images_cat_id: "998"
_4images_user_id: "184"
_4images_image_date: "2007-07-19T14:52:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11134 -->
Ein paar Schritte weiter und das Ganze sieht schon etwas mehr nach Traktor aus.
Ich habe jetzt eine Pendelachse eingebaut, den Vorderradantrieb (der ist im Augenblick noch nach oben herausgeführt) und die Lenkung ist motorisiert.
Die schräge der Motorhaube ist auch zu erkennen.