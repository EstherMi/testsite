---
layout: "image"
title: "Ansicht"
date: "2007-07-13T12:03:15"
picture: "DSCN1430.jpg"
weight: "20"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/11045
imported:
- "2019"
_4images_image_id: "11045"
_4images_cat_id: "998"
_4images_user_id: "184"
_4images_image_date: "2007-07-13T12:03:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11045 -->
Blick auf den hinteren Teil. Über die Schnecken (so hatte ich es geplant) soll der Antrieb erfolgen.