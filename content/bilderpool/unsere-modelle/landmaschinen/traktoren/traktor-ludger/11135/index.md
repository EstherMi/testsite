---
layout: "image"
title: "Ansicht von hinten"
date: "2007-07-19T14:52:01"
picture: "DSCN1453.jpg"
weight: "6"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/11135
imported:
- "2019"
_4images_image_id: "11135"
_4images_cat_id: "998"
_4images_user_id: "184"
_4images_image_date: "2007-07-19T14:52:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11135 -->
Hier habe ich erst einmal die Power Motoren unter die Hinterachse eingebaut. Ob das so bleibt steht noch in den Sternen. Über die Welle die sich unterhalb der schrägen Motorhaube befindet könnte der Antrieb erfolgen.