---
layout: "image"
title: "Traktor 2008"
date: "2008-04-29T18:13:32"
picture: "Traktor52.jpg"
weight: "15"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/14411
imported:
- "2019"
_4images_image_id: "14411"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2008-04-29T18:13:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14411 -->
Das wird mein diesjähriger Trakor. Er ist wesentlich größer als letztes Jahr, was ich wohl auch nicht ändern werde. Es ist natürlich eine Kunst und zugleich der Anreiz kompakt zu bauen und möglichst viele Funktionen auf möglichst wenig Raum aufzubauen, aber diesmal sind mehrere Funtionen geplant.
Der Traktor verfügt über einen Allradantrieb. 
Die Vorderachse ist als Pendelachse ausgelegt, der Drehpunkt ist zugleich die Antriebsachse, wordurch man die, dauernd springenden, Kardangelenke einsparen kann.
Die Hebemechanik wird hydraulisch (!) geschehen, nicht wie gewohnt pneumatisch. Trotzdem werde ich nicht auf einen, vom Fahrmotor ausklinkbaren, Kompressor nicht verzichten. 
Zapfwelle ist vorhanden, Anhängerkupplung geplant.
Die Beleuchtung ist durch energiesparende LEDs umgesetzt. Die Gesamtbeleuchtung zieht 100mAh, wäre dies durch ft-Lampen ersetzt betrüge es das 4-fache (400mAh).
Die LEDs sind nicht weniger hell, als die ft-Lampen.