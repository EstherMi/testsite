---
layout: "image"
title: "Neu"
date: "2007-08-12T15:24:19"
picture: "Traktor43.jpg"
weight: "26"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/11359
imported:
- "2019"
_4images_image_id: "11359"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2007-08-12T15:24:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11359 -->
Das Design ist neu, weil die Führerhauskabine anders ist. Und es ist eine Beleuchtung da.