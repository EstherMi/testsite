---
layout: "image"
title: "Gesamtansicht"
date: "2009-06-16T17:17:04"
picture: "traktor1.jpg"
weight: "1"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/24397
imported:
- "2019"
_4images_image_id: "24397"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2009-06-16T17:17:04"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24397 -->
Gesamtansicht