---
layout: "image"
title: "Verbesserung"
date: "2007-07-15T17:49:00"
picture: "Traktor30.jpg"
weight: "54"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/11083
imported:
- "2019"
_4images_image_id: "11083"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2007-07-15T17:49:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11083 -->
Ich habe nach etwas testen gemerkt, dass die Lenkung nicht funktionierte, weil der eine Zylinder erst ausgefahren ist, wenn der erste schon draußen war. Ich habe es auch mit anderen Zylinder ausprobiert, aber es hat nie funktioniert. Deswegen habe ich jetzt nur einen Zylinder genommen und die Räder mit einer Statikstrebe verbunden. Jetzt funktioniert es gut. Ich hoffe, dass es im Gelände auch genug Kraft hat. Naja werde ich sehen. Es wird bestimmt auch noch verbessert. Jetzt könnte man sich aber denken:
Wenn man mit Zylindern lenkt, dann fährt der durch die Druckluft ja ganz schnell ein und aus. Ich mache es aber anders. ich habe 2 Zylinder genommen und an den Lenkzylinder angeschlossen. Wenn man mit einem Hubgetriebe und Minimotor die Zylinder einfährt geht die Luft in den Lenkzylinder und er lenkt nach links. Wenn man die Zylinder ausfährt, ensteht ein Unterdruck (Vakuum) und zieht den Kolben des Lenkzylinders weg und er lenkt nach rechts. da das Hubgetriebe relativ langsam ist kann man auch gut gerade aus fahern oder nur ein bischen lenken.