---
layout: "image"
title: "Seitenansicht"
date: "2008-05-14T17:29:16"
picture: "Traktor53_2.jpg"
weight: "11"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/14510
imported:
- "2019"
_4images_image_id: "14510"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2008-05-14T17:29:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14510 -->
Es gibt einen Schalter für den Empfänger, einen für das Licht und einer kommt noch dran für den Wechselblinker.