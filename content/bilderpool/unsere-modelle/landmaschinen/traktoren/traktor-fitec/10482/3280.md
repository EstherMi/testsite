---
layout: "comment"
hidden: true
title: "3280"
date: "2007-05-29T21:31:01"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Richtig, das Zentraldifferenzial gleicht die Drehzahlunterschiede aus. Aber Thomas hat auch Recht damit, dass das Drehmoment ungleich verteilt wird. Die Vorderachse kriegt mehr Power ab.

Hier:
http://www.fischertechnik.de/de/fanclub/forum/default.aspx?g=posts&t=1433
wurde schon drüber diskutiert, und thkais hat's erfasst.

Gruß,
Harald