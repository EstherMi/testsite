---
layout: "image"
title: "Unterseite"
date: "2007-05-20T18:53:06"
picture: "Traktor10.jpg"
weight: "74"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/10482
imported:
- "2019"
_4images_image_id: "10482"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2007-05-20T18:53:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10482 -->
Man sieht die Unterseite. Man erkennt gut das Zentraldiff und die Kardangelenke für die Pendelachse. Ich habe alles so gebaut, dass die Kardangelenke für die Pendelachse nur einen ganz kleinen Winkel haben. Sonst gehen sie unter hoher Belastung (z.B. Rasen) kaputt.