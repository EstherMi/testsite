---
layout: "image"
title: "Motorraum"
date: "2008-05-14T17:29:16"
picture: "Traktor54_2.jpg"
weight: "12"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/14511
imported:
- "2019"
_4images_image_id: "14511"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2008-05-14T17:29:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14511 -->
Man guckt in den Motorraum.