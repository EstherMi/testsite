---
layout: "image"
title: "Unterseite"
date: "2008-04-29T18:13:32"
picture: "Traktor59.jpg"
weight: "22"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/14418
imported:
- "2019"
_4images_image_id: "14418"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2008-04-29T18:13:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14418 -->
Die Unterseite ist zu sehen. So weit es geht wurde diese verkleidet um vor Schmutz und Dreck zu schützen. Wenn es nicht zu viel an Bodenabstand einbüßt wird auch noch das Mitteldifferenzial geschützt(Diff. sind immer besonders wichtig).