---
layout: "image"
title: "Hydraulik"
date: "2009-06-16T17:17:05"
picture: "traktor6.jpg"
weight: "6"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/24402
imported:
- "2019"
_4images_image_id: "24402"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2009-06-16T17:17:05"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24402 -->
So wird die Hydraulik angetrieben. Der Minimot ist schön in die Karosserie integriert.