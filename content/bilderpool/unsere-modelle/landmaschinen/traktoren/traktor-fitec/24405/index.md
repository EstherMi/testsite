---
layout: "image"
title: "Heck"
date: "2009-06-16T17:17:10"
picture: "traktor9.jpg"
weight: "9"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/24405
imported:
- "2019"
_4images_image_id: "24405"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2009-06-16T17:17:10"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24405 -->
Hier ist die Achse an die Zapfwelle angebaut. Die Zapfwelle kommt direkt vom Power Motor mit einer zusätzlich Übersetzung von 1:2, sie dreht also ziemlich schnell.