---
layout: "image"
title: "Lenkung"
date: "2007-05-20T18:53:06"
picture: "Traktor3.jpg"
weight: "67"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/10475
imported:
- "2019"
_4images_image_id: "10475"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2007-05-20T18:53:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10475 -->
Das ist die Lenkung (Achsschenkellenkung). Sie wird auch angetrieben sonst wäre es ja kein Allrad. Die Kraft wird mit Kardangelenken übertragen. Die Vorderachse ist auch als Pendelachse ausgelegt.