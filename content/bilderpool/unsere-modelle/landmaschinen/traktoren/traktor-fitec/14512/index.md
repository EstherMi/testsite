---
layout: "image"
title: "Hydraulik"
date: "2008-05-14T17:29:16"
picture: "Traktor55_2.jpg"
weight: "13"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/14512
imported:
- "2019"
_4images_image_id: "14512"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2008-05-14T17:29:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14512 -->
Die Zylinder im Motorraum sollen später via Schneckenantrieb bewegt werden.