---
layout: "image"
title: "Lenkeinschlag rechts"
date: "2007-07-15T17:49:00"
picture: "Traktor28.jpg"
weight: "52"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/11081
imported:
- "2019"
_4images_image_id: "11081"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2007-07-15T17:49:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11081 -->
So ist der Lenkeinschlag nach rechts.