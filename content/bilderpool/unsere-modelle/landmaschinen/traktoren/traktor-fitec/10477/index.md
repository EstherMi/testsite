---
layout: "image"
title: "Führerhaus"
date: "2007-05-20T18:53:06"
picture: "Traktor5.jpg"
weight: "69"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/10477
imported:
- "2019"
_4images_image_id: "10477"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2007-05-20T18:53:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10477 -->
Der Platz im Führerhaus wurde gut ausgenutzt. Darin befinden sich: Antrieb des Mitteldiffs durch einen 50:1 P-mot, Magnetventile, Kompressor und Kompressorabschaltung.