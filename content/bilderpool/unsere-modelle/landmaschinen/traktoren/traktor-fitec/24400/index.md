---
layout: "image"
title: "Antrieb"
date: "2009-06-16T17:17:05"
picture: "traktor4.jpg"
weight: "4"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/24400
imported:
- "2019"
_4images_image_id: "24400"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2009-06-16T17:17:05"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24400 -->
Das front Differenzial ist gut verkleidet, aber Pendelachse usw sind wie letztes Jahr.