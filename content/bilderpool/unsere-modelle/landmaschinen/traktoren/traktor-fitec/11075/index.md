---
layout: "image"
title: "Lenkung"
date: "2007-07-15T17:49:00"
picture: "Traktor22.jpg"
weight: "46"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/11075
imported:
- "2019"
_4images_image_id: "11075"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2007-07-15T17:49:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11075 -->
Das ist eine Lenkung für einen Traktor. Sie hat viel mehr Bodenabstand, als vorher, weil ich die Gelenke unter den Kardangelnken weg gemacht habe. Damit es noch stabil genug ist und sich unter hoher Belastung nicht durchbiegt wurde oben 4Grundbausteine drüber gemacht. Mit einer Metallachse drinnen ist es stabil und fast wie ein Aluprofil. Die Lenkung will ich wie bei richtigen Traktoren hydraulisch machen, also pneumatisch, weil Hydraulik gibt es ja nicht mehr von ft.