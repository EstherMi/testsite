---
layout: "image"
title: "Innenleben"
date: "2008-04-29T18:13:32"
picture: "Traktor55.jpg"
weight: "18"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/14414
imported:
- "2019"
_4images_image_id: "14414"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2008-04-29T18:13:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14414 -->
Hier ist das, noch lange nicht vollendete, Innenleben zu sehen. DBis jetzt befindet sich hier nur der Akku(1000mAh) und der Antriebsmotor, welcher 1:1 auf das Mitteldifferenzial geht.