---
layout: "image"
title: "Pendelachse"
date: "2007-07-21T14:05:01"
picture: "Traktor42.jpg"
weight: "45"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/11187
imported:
- "2019"
_4images_image_id: "11187"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2007-07-21T14:05:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11187 -->
Hier sieht man wie er gerade pendelt.