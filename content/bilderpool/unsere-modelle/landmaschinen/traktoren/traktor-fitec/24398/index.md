---
layout: "image"
title: "Vorne"
date: "2009-06-16T17:17:05"
picture: "traktor2.jpg"
weight: "2"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/24398
imported:
- "2019"
_4images_image_id: "24398"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2009-06-16T17:17:05"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24398 -->
Man sieht die ultrahellen Leds mit Reflektor. Der Akku (9,6V/2300mAh) sitzt ganz vorne unter der roten Verkleidung. Dadurch ist genug Gewicht auf der Vorderachse, dass diese auch ordentlich Kraft auf den Boden überträgt.