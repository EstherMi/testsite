---
layout: "image"
title: "Traktor"
date: "2007-08-25T12:44:10"
picture: "Traktor51.jpg"
weight: "25"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/11404
imported:
- "2019"
_4images_image_id: "11404"
_4images_cat_id: "955"
_4images_user_id: "456"
_4images_image_date: "2007-08-25T12:44:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11404 -->
Der Abstand zwischen Vorder- und Hinterräder ist jetzt um ein BS30 kleiner.