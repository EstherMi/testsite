---
layout: "image"
title: "Nano-Traktor 02"
date: "2008-04-04T21:55:04"
picture: "minimini02.jpg"
weight: "2"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- details/14176
imported:
- "2019"
_4images_image_id: "14176"
_4images_cat_id: "1312"
_4images_user_id: "327"
_4images_image_date: "2008-04-04T21:55:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14176 -->
