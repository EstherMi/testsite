---
layout: "overview"
title: "Nano-Traktor"
date: 2019-12-17T19:32:03+01:00
legacy_id:
- categories/1312
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1312 --> 
Nachdem der Bau des motorisierten Mini-Traktors mit Anhänger so viel Spass gemacht hat, wollte ich natürlich wissen, ob es noch kleiner geht (allerdings ohne Motor).