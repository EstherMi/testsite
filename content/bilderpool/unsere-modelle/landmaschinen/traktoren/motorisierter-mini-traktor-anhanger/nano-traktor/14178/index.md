---
layout: "image"
title: "Größenvergleich Mini-Traktor und Nano-Traktor"
date: "2008-04-04T21:55:04"
picture: "mini_und_minimini_01.jpg"
weight: "4"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- details/14178
imported:
- "2019"
_4images_image_id: "14178"
_4images_cat_id: "1312"
_4images_user_id: "327"
_4images_image_date: "2008-04-04T21:55:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14178 -->
