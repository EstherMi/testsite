---
layout: "image"
title: "Motorisierter Mini-Traktor 03"
date: "2008-04-04T21:55:04"
picture: "20080404_Fischertechnik_Mini-Traktoren_05.jpg"
weight: "3"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- details/14173
imported:
- "2019"
_4images_image_id: "14173"
_4images_cat_id: "1311"
_4images_user_id: "327"
_4images_image_date: "2008-04-04T21:55:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14173 -->
Der Ein-/Ausschalter soll so eine Art Mähwerk darfstellen.

Klar kann man den auch weglassen...