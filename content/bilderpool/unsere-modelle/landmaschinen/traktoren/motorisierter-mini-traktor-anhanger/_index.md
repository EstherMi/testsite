---
layout: "overview"
title: "Motorisierter Mini-Traktor mit Anhänger"
date: 2019-12-17T19:32:03+01:00
legacy_id:
- categories/1311
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1311 --> 
Hallo,

nachdem ich ja normalerweise lieber Fahrwerksprototypenkomponenten baue, habe ich mich mal dazu entschlossen, ein komplettes Modell bis zu Ende zu bringen ;-)

Wieder einmal kommt meine Vorliebe zum kompakten Bau mit FT zur Geltung.