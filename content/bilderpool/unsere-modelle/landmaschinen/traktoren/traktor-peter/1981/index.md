---
layout: "image"
title: "Traktor03.JPG"
date: "2003-11-11T20:15:27"
picture: "Traktor03_2.jpg"
weight: "4"
konstrukteure: 
- "Peter Damen (peterholland)"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/1981
imported:
- "2019"
_4images_image_id: "1981"
_4images_cat_id: "118"
_4images_user_id: "4"
_4images_image_date: "2003-11-11T20:15:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1981 -->
