---
layout: "image"
title: "Traktor 21"
date: "2007-01-25T17:58:40"
picture: "traktor21.jpg"
weight: "21"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/8686
imported:
- "2019"
_4images_image_id: "8686"
_4images_cat_id: "796"
_4images_user_id: "502"
_4images_image_date: "2007-01-25T17:58:40"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8686 -->
Der power Motor ist tatsächlich leicht schräg eingebaut weil er sonst das Differential streifen würde,