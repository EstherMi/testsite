---
layout: "image"
title: "Traktor 1"
date: "2007-01-25T17:58:39"
picture: "traktor01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/8666
imported:
- "2019"
_4images_image_id: "8666"
_4images_cat_id: "796"
_4images_user_id: "502"
_4images_image_date: "2007-01-25T17:58:39"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8666 -->
