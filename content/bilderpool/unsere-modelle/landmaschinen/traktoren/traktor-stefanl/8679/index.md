---
layout: "image"
title: "Traktor 14"
date: "2007-01-25T17:58:40"
picture: "traktor14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/8679
imported:
- "2019"
_4images_image_id: "8679"
_4images_cat_id: "796"
_4images_user_id: "502"
_4images_image_date: "2007-01-25T17:58:40"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8679 -->
Mal grob zusammengebaut.
Man sieht den Übergang zur Lenkung mit Kardangelenken.