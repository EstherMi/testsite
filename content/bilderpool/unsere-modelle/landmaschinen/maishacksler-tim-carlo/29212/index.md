---
layout: "image"
title: "tim carlo maishexler"
date: "2010-11-07T21:56:31"
picture: "maishexler__49.jpg"
weight: "11"
konstrukteure: 
- "tim carlo"
fotografen:
- "tim carlo"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "carlo"
license: "unknown"
legacy_id:
- details/29212
imported:
- "2019"
_4images_image_id: "29212"
_4images_cat_id: "2119"
_4images_user_id: "893"
_4images_image_date: "2010-11-07T21:56:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29212 -->
Maishexler nach Vorbild von John Deere und Siku-Modell