---
layout: "comment"
hidden: true
title: "17683"
date: "2013-01-03T11:43:47"
uploadBy:
- "chef8"
license: "unknown"
imported:
- "2019"
---
Als eerste, netjes gemaakt mooie oplossingen voor diverse dingen.
Maar waarom op twee sokkels? Toch te zwaar om alles in de aandrijving sterk genoeg te krijgen of moet ik het meer zien als een functionele demo machine om de werking te kunnen uitleggen?

Verder vind ik hem echt goed gelukt.

vr gr Ruurd