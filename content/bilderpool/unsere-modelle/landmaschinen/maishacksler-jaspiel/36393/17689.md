---
layout: "comment"
hidden: true
title: "17689"
date: "2013-01-03T19:59:56"
uploadBy:
- "JaSpiel"
license: "unknown"
imported:
- "2019"
---
Hallo Harald en Peter,

Allereerst, dank Peter voor de snelle aanvullende info. 

Peter:
- 1) de instelbaarheid van de wrijvingsas is niet gemonteerd (bewust - Met deze as is er de mogelijkheid om de trekkracht toe te laten nemen). In plaats daarvan heb ik op de wrijvingsas de achterwielaandrijving gemonteerd. Dit heeft als effect dat als de grip van de achterwielen afneemt de voorwielen krachtiger grip krijgen.
- 2) de Z20's zitten vast op de as. Als de middelste loszit draaien de achterwielen niet. Wat interessant is: als de middelste Z20 flexibel los/vast gemonteerd wordt is er een (onder last) instelbare mogelijkheid voor 2WD/4WD beschikbaar.

Harald:
- 1) Nein. Der linken Diffenrenzial (auch der rechten) ist normal montiert. Die beiden D. sind oben im Bild 10:30 verbunden, und unter 20:20.
- 2) Das warum der Automatiek ist die flexibele Nm. Der Grip aufs bodem ist wezentlich Besser. Leichten Modellen kriegen Kletterkraft.
- 3) Die 'freizwebende kegelrader' sind OK. Doch, Sie haben recht dass dieses Fahrzeug nicht gut fahrt. Aleine prima, aber komplet montiert mit der 'frontaufzug' ist das ganse zu schwer. Motor zu schwach, Ubersetzung zu Gross.

Ich bin kein Techniker, mehr Liebhaber und Bastler. Ich hoffe dass es soweit klar ist.

Gruss,
Jack