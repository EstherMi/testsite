---
layout: "overview"
title: "Forwarder (Holzernter)"
date: 2019-12-17T19:31:09+01:00
legacy_id:
- categories/537
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=537 --> 
Forwarder sind geländegänige, robuste Maschinen um abgelängte Baumstämme aus dem Wald zu bergen