---
layout: "image"
title: "Forwader 05"
date: "2006-05-07T15:16:33"
picture: "Forwader_05.jpg"
weight: "5"
konstrukteure: 
- "Lothar Vogt      Pilami"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- details/6227
imported:
- "2019"
_4images_image_id: "6227"
_4images_cat_id: "537"
_4images_user_id: "10"
_4images_image_date: "2006-05-07T15:16:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6227 -->
Blick von unten. Die Achse ist nicht durchgängig, sie sind einzeln angetrieben. Gelenkt wird analog zu Raupenlaufwerken, durch einseitiges Antrieben der Räder.