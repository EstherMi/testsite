---
layout: "image"
title: "Lanz Bulldog"
date: "2012-08-26T20:28:54"
picture: "lanzbulldog09.jpg"
weight: "9"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/35377
imported:
- "2019"
_4images_image_id: "35377"
_4images_cat_id: "2624"
_4images_user_id: "22"
_4images_image_date: "2012-08-26T20:28:54"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35377 -->
Geschiedenis
De tractoren met gloeikopmotor zijn geproduceerd van ongeveer 1921 tot en met 1955 in diverse modellen en series. De tractoren met gloeikopmotor waren zeer geliefd vanwege de grote kracht, eenvoud en betrouwbaarheid, maar ook vanwege hun robuustheid. De tractoren liepen behalve op diesel ook op petroleum, afgewerkte olie en nog meer (goedkopere en tevens onzuivere) brandbare stoffen. Toch konden de tractors wel vastlopen, door nalatige smering of te weinig koeling.
Begin jaren vijftig was de gloeikopmotor ten opzichte van de concurrent sterk verouderd en was de vraag naar deze tractor dan ook veel minder. Daardoor besloten ze om de gloeikopmotor te vervangen door een modernere versie namelijk: de ééncilinder halfdiesel, die gewoon met een startmotor kon worden gestart. Het einde was echter niet te houden en in 1955 werd Heinrich LANZ Mannheim AG overgenomen door John Deere.
In totaal hebben ongeveer 250.000 Bulldogs de fabriek in Mannheim verlaten.
