---
layout: "overview"
title: "Lanz-EilBulldog Poederoyen"
date: 2019-12-17T19:32:56+01:00
legacy_id:
- categories/2624
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2624 --> 
De Lanz Bulldog is een type tractor ontwikkeld door de Duitse fabrikant Lanz.
Ontworpen door de Duitse ingenieur Fritz Huber, had de tractor een liggende één-cilinder tweetakt gloeikopdieselmotor. Door gebruik van deze ééncilinder had die een karakteristiek geluid, een staccato "boem-boem" geluid.
