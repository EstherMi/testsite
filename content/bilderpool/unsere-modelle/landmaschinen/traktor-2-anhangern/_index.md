---
layout: "overview"
title: "Traktor mit 2 Anhängern"
date: 2019-12-17T19:33:05+01:00
legacy_id:
- categories/3163
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3163 --> 
'Rollen' heißen die Zweiachsanhänger mit Deichsel. So fuhren sie in meiner Kindheit durchs Ort hoch beladen mit Heu- und Strohballen.