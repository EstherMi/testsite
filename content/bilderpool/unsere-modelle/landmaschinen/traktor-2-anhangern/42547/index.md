---
layout: "image"
title: "Pause"
date: "2015-12-20T18:02:10"
picture: "traktormitanhaengern02.jpg"
weight: "2"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/42547
imported:
- "2019"
_4images_image_id: "42547"
_4images_cat_id: "3163"
_4images_user_id: "1557"
_4images_image_date: "2015-12-20T18:02:10"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42547 -->
Günther entspannt sich gerade ein bißchen.
Der Traktor ist der aus dem Kasten Modellbaukasten Power Tractors von 2003, Seite 5:
http://ft-datenbank.de/details.php?ArticleVariantId=4e3176ea-904c-4994-a8d5-8a447ba2bb29
Allerdings in grau anstelle schwarz und leicht modifiziert.