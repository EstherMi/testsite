---
layout: "image"
title: "2. Anhänger von unten"
date: "2015-12-20T18:02:10"
picture: "traktormitanhaengern09.jpg"
weight: "9"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/42554
imported:
- "2019"
_4images_image_id: "42554"
_4images_cat_id: "3163"
_4images_user_id: "1557"
_4images_image_date: "2015-12-20T18:02:10"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42554 -->
Weitgehend wie der andere auch. Die Clipsachse hält nur nicht, so daß die Vorderachse immer rausfällt, wenn man ihn hochhebt.