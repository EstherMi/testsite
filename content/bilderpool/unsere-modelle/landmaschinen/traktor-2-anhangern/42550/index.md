---
layout: "image"
title: "Traktor von unten"
date: "2015-12-20T18:02:10"
picture: "traktormitanhaengern05.jpg"
weight: "5"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/42550
imported:
- "2019"
_4images_image_id: "42550"
_4images_cat_id: "3163"
_4images_user_id: "1557"
_4images_image_date: "2015-12-20T18:02:10"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42550 -->
Da ist einiges stabiler gebaut als beim Original.