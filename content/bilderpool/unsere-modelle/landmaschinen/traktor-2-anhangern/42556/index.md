---
layout: "image"
title: "Vergleich zweier Traktoren"
date: "2015-12-20T18:02:10"
picture: "traktormitanhaengern11.jpg"
weight: "11"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/42556
imported:
- "2019"
_4images_image_id: "42556"
_4images_cat_id: "3163"
_4images_user_id: "1557"
_4images_image_date: "2015-12-20T18:02:10"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42556 -->
Der Traktor ähnlich dem 91079 im Hintergrund und der aktuelle aus dem 520397 vorne. Design ist Geschmackssache. Der neuere Traktor im Vordergrund ist rauhem Spieltrieb allerdings nicht gewachsen. Da verschieben sich dauernd die Längsträger und er fällt mindestens zweimal die Woche auseinander. Na gut, ist ja auch erst ab 7 und der Junior ist gerade 4.