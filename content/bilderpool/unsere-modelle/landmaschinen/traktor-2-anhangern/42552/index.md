---
layout: "image"
title: "Rückansicht"
date: "2015-12-20T18:02:10"
picture: "traktormitanhaengern07.jpg"
weight: "7"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/42552
imported:
- "2019"
_4images_image_id: "42552"
_4images_cat_id: "3163"
_4images_user_id: "1557"
_4images_image_date: "2015-12-20T18:02:10"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42552 -->
Stoßstange und Kupplungsmaul. Man beachte die durchgehende Strebe. Klemmt ein bißchen stramm, aber geht noch ohne Schäden. Die S-Riegel erinnern an die funzeligen kleinen alten Rücklichter.