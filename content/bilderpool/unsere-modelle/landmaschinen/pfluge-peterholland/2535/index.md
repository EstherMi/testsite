---
layout: "image"
title: "Lokomobil + Pflug"
date: "2004-07-02T12:55:57"
picture: "LokomobilPflug1.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/2535
imported:
- "2019"
_4images_image_id: "2535"
_4images_cat_id: "239"
_4images_user_id: "22"
_4images_image_date: "2004-07-02T12:55:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2535 -->
