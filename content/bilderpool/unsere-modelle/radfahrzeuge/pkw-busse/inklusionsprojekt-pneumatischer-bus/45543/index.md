---
layout: "image"
title: "Fahrgastkabine von oben: Gleitfalttür offen"
date: "2017-03-15T21:16:58"
picture: "IMG_8532.jpg"
weight: "20"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Gleitfalttür"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/45543
imported:
- "2019"
_4images_image_id: "45543"
_4images_cat_id: "3383"
_4images_user_id: "579"
_4images_image_date: "2017-03-15T21:16:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45543 -->
