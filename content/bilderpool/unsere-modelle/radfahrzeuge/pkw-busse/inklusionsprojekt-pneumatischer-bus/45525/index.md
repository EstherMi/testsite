---
layout: "image"
title: "eRolli bereit zum Einsteigen"
date: "2017-03-15T21:16:58"
picture: "IMG_8464.jpg"
weight: "2"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Video"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/45525
imported:
- "2019"
_4images_image_id: "45525"
_4images_cat_id: "3383"
_4images_user_id: "579"
_4images_image_date: "2017-03-15T21:16:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45525 -->
siehe auch das Video auf youtube:

https://www.youtube.com/watch?v=w05f6VYnwNc

oder

https://youtu.be/w05f6VYnwNc