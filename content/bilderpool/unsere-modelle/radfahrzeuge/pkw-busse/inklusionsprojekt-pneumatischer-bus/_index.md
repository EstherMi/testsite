---
layout: "overview"
title: "Inklusionsprojekt: pneumatischer Bus mit eRolli"
date: 2019-12-17T18:46:27+01:00
legacy_id:
- categories/3383
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3383 --> 
Behindertengerechter Bus mit pneumatischen Gleitfalttüren und Luftfederung mit Niveauregulierung. Der Bus kann sich absenken. Ein e-Rolli fährt an der Bushaltestelle in den Bus und steigt später wieder aus. Die Lage der Pneumatik-Zylinder für die Luftfederung wird für die Niveauregulierung über Potentiometer analog erfasst. Die Gleit-Falt-Türen wurden aus CD-Hüllen angefertigt.