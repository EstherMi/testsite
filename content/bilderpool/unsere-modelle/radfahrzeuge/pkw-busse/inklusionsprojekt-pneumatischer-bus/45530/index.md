---
layout: "image"
title: "eRolli von hinten"
date: "2017-03-15T21:16:58"
picture: "IMG_8479.jpg"
weight: "7"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["VNH2SP30", "Motortreiber"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/45530
imported:
- "2019"
_4images_image_id: "45530"
_4images_cat_id: "3383"
_4images_user_id: "579"
_4images_image_date: "2017-03-15T21:16:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45530 -->
