---
layout: "image"
title: "Lageregelung vorne"
date: "2017-03-15T21:16:58"
picture: "IMG_8523.jpg"
weight: "14"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Potentiometer", "Lagebestimmung"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/45537
imported:
- "2019"
_4images_image_id: "45537"
_4images_cat_id: "3383"
_4images_user_id: "579"
_4images_image_date: "2017-03-15T21:16:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45537 -->
