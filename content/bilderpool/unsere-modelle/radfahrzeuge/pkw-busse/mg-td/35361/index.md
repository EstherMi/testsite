---
layout: "image"
title: "Control Set"
date: "2012-08-26T15:33:25"
picture: "mgtd5.jpg"
weight: "5"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- details/35361
imported:
- "2019"
_4images_image_id: "35361"
_4images_cat_id: "2621"
_4images_user_id: "791"
_4images_image_date: "2012-08-26T15:33:25"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35361 -->
Das Control Set kann hochgeklappt werden um die Stecker und die Batterie anzuschließen.
Leider schliesst es nicht ganz Bündi zum Rest der Motorhaube ab.