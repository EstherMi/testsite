---
layout: "image"
title: "Von Unten"
date: "2012-08-26T15:33:25"
picture: "mgtd6.jpg"
weight: "6"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- details/35362
imported:
- "2019"
_4images_image_id: "35362"
_4images_cat_id: "2621"
_4images_user_id: "791"
_4images_image_date: "2012-08-26T15:33:25"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35362 -->
Hier kann man den XM Motor, das Servo und das Differential erkennen.