---
layout: "image"
title: "Kompaktwagen 1"
date: "2010-02-14T14:01:02"
picture: "Kompaktwagen_01.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/26418
imported:
- "2019"
_4images_image_id: "26418"
_4images_cat_id: "1880"
_4images_user_id: "328"
_4images_image_date: "2010-02-14T14:01:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26418 -->
In meinem Lastenheft stand der Bau eines klassischen Kompaktwagens mit Frontantrieb und Platz für 4 Personen, wie z.B. VW Golf oder Opel Astra. Er sollte ferngesteuert sein, vorn und hinten Licht haben und eine halbwegs moderne und schnittige Optik bekommen.

Das Ergebnis erfüllt alle technischen Vorgaben: Der Antrieb erfolgt über einen Power-Motor an die Vorderachse. Gelenkt wird per Servo über den klassischen "thomas004-Drehschemel". Damit im Innenraum Platz für 4 FT-Männchen bleibt, hat kein Akku-Pack mehr reingepasst. Stattdessen kommt der Strom aus einem 9V-Blockakku hinter der Hinterachse. Das Licht (vorn und hinten) lässt sich per Fernbedienung an- und ausschalten.

Durch Motor und Akku im Fahrzeugboden ist der Innenraum dort arg weit nach oben gerutscht, so dass das Fahrzeug eher wenig nach einem klassischem Kompaktwagen aussieht. Für mich hat es eher was von einem militärischen Transportfahrzeug, vor allem durch die großen Räder...