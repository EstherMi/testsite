---
layout: "comment"
hidden: true
title: "10902"
date: "2010-02-15T16:02:10"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Danke, Harald, aber ich habe noch genug Modellideen für viele Jahre im Kopf. Da brauche ich solche (zugegeben sehr putzigen) Anregungen noch nicht.

Ja, ich sehe mein Modell mittlerweile als so eine Art modernen "Soft-Roader", wie sie je gerade in Mode sind. Also quasi Autos mit normaler PKW-Technik (Frontantrieb), aber Geländewagen-Design.

Gruß, Thomas