---
layout: "image"
title: "Kompaktwagen 2"
date: "2010-02-14T14:01:03"
picture: "Kompaktwagen_02.jpg"
weight: "2"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/26419
imported:
- "2019"
_4images_image_id: "26419"
_4images_cat_id: "1880"
_4images_user_id: "328"
_4images_image_date: "2010-02-14T14:01:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26419 -->
