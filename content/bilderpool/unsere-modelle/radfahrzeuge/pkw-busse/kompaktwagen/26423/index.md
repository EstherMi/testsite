---
layout: "image"
title: "Kompaktwagen 6"
date: "2010-02-14T14:01:03"
picture: "Kompaktwagen_06.jpg"
weight: "6"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/26423
imported:
- "2019"
_4images_image_id: "26423"
_4images_cat_id: "1880"
_4images_user_id: "328"
_4images_image_date: "2010-02-14T14:01:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26423 -->
