---
layout: "image"
title: "Lenkung und Befestigung Powermot"
date: "2014-05-09T17:28:26"
picture: "wdfahrgestell06.jpg"
weight: "6"
konstrukteure: 
- "Jeroen"
fotografen:
- "Jeroen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "yoeroen"
license: "unknown"
legacy_id:
- details/38757
imported:
- "2019"
_4images_image_id: "38757"
_4images_cat_id: "2897"
_4images_user_id: "2174"
_4images_image_date: "2014-05-09T17:28:26"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38757 -->
Vom Lenkrad geht's vom Z10 via Z20 nach Z10
Der Zahnstang 60 schiebt.
Die Befestigung vom Zahnstang auf dem Baustein mit Bohrung ist nicht sehr Fest. Entwurfsfehler oder überlastungs-schutz?