---
layout: "comment"
hidden: true
title: "19060"
date: "2014-05-18T22:01:02"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Die quer eingebauten BS15 (hinter der Vorderachse) sehe ich als bruchgefährdet an. Die würde ich zumindest hochkant stellen, wenn nicht noch weiter verstärken.

Gruß,
Harald