---
layout: "image"
title: "Gesammtansicht mit Fernbedienung"
date: "2012-09-12T21:32:11"
picture: "kleinesauto1.jpg"
weight: "1"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- details/35508
imported:
- "2019"
_4images_image_id: "35508"
_4images_cat_id: "2633"
_4images_user_id: "1122"
_4images_image_date: "2012-09-12T21:32:11"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35508 -->
Das Auto hat eine Länge von 13cm. Ansteuerbar sind Motor zum fahren und Servo zum lenken.