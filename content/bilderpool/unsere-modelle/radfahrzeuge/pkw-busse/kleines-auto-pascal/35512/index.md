---
layout: "image"
title: "Von hinten"
date: "2012-09-12T21:32:11"
picture: "kleinesauto5.jpg"
weight: "5"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- details/35512
imported:
- "2019"
_4images_image_id: "35512"
_4images_cat_id: "2633"
_4images_user_id: "1122"
_4images_image_date: "2012-09-12T21:32:11"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35512 -->
Das Grau was im Bild zusehen ist, ist der Motor zum Antrieb der Hinterachse.