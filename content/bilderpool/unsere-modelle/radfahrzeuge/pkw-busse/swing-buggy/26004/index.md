---
layout: "image"
title: "vorderansicht"
date: "2009-12-31T13:11:52"
picture: "swingbuggy13.jpg"
weight: "13"
konstrukteure: 
- "bernd scheurer"
fotografen:
- "bernd scheurer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "be-sign,net"
license: "unknown"
legacy_id:
- details/26004
imported:
- "2019"
_4images_image_id: "26004"
_4images_cat_id: "1833"
_4images_user_id: "808"
_4images_image_date: "2009-12-31T13:11:52"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26004 -->
