---
layout: "image"
title: "lenkeinschlag rechts"
date: "2009-12-31T13:11:36"
picture: "swingbuggy03.jpg"
weight: "3"
konstrukteure: 
- "bernd scheurer"
fotografen:
- "bernd scheurer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "be-sign,net"
license: "unknown"
legacy_id:
- details/25994
imported:
- "2019"
_4images_image_id: "25994"
_4images_cat_id: "1833"
_4images_user_id: "808"
_4images_image_date: "2009-12-31T13:11:36"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25994 -->
wie in der projektbeschreibung erläutert, ist die "schwingende" bewegung beim lenken des fahrzeuges auf den statischen fotos nicht wirklich gut zu erkennen.