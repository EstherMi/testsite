---
layout: "image"
title: "arbeitsplatz"
date: "2009-12-31T13:11:52"
picture: "swingbuggy15.jpg"
weight: "15"
konstrukteure: 
- "bernd scheurer"
fotografen:
- "bernd scheurer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "be-sign,net"
license: "unknown"
legacy_id:
- details/26006
imported:
- "2019"
_4images_image_id: "26006"
_4images_cat_id: "1833"
_4images_user_id: "808"
_4images_image_date: "2009-12-31T13:11:52"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26006 -->
