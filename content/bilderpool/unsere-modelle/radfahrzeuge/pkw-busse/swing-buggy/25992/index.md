---
layout: "image"
title: "totale"
date: "2009-12-31T13:11:36"
picture: "swingbuggy01.jpg"
weight: "1"
konstrukteure: 
- "bernd scheurer"
fotografen:
- "bernd scheurer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "be-sign,net"
license: "unknown"
legacy_id:
- details/25992
imported:
- "2019"
_4images_image_id: "25992"
_4images_cat_id: "1833"
_4images_user_id: "808"
_4images_image_date: "2009-12-31T13:11:36"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25992 -->
