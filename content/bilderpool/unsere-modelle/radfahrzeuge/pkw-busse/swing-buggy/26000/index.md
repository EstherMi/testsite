---
layout: "image"
title: "detail: befestigung der federung"
date: "2009-12-31T13:11:36"
picture: "swingbuggy09.jpg"
weight: "9"
konstrukteure: 
- "bernd scheurer"
fotografen:
- "bernd scheurer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "be-sign,net"
license: "unknown"
legacy_id:
- details/26000
imported:
- "2019"
_4images_image_id: "26000"
_4images_cat_id: "1833"
_4images_user_id: "808"
_4images_image_date: "2009-12-31T13:11:36"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26000 -->
der block, in dem die aufhängungen der hinterräder befestigt sind, ist mit zwei gelenken und zwei federn am vorderen rahmen befestigt.