---
layout: "image"
title: "Auto"
date: "2008-02-17T14:08:52"
picture: "DSC03876.jpg"
weight: "2"
konstrukteure: 
- "Raphael"
fotografen:
- "Raphael"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "baustein"
license: "unknown"
legacy_id:
- details/13663
imported:
- "2019"
_4images_image_id: "13663"
_4images_cat_id: "611"
_4images_user_id: "727"
_4images_image_date: "2008-02-17T14:08:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13663 -->
ein möglichst kleines fahrbares Auto