---
layout: "overview"
title: "Tonga Buggy"
date: 2019-12-17T18:46:10+01:00
legacy_id:
- categories/3011
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3011 --> 
Nach mein erster Beitrag ( http://www.ftcommunity.de/categories.php?cat_id=2897) gab es folgende Kommentare:

- Es war zu Labil.
- Vorderachse kann einfacher bei Anwendung von Lager mit Schneckenmutter
- Die Achsen brauchen zusätsiche Stabilisierung

Ich habe versucht alle Kommentare zu verarbeiten in meinem Tonga Buggy.

Es hat:
- ein stabiles Untergestell aus 4 Alus
- Heckantireb
- Powermot 20:1
- Vorne ein Panhard-Stab
- Vorne BIC Federung, :)
- Hinten ein Watt-Mechanismus
- Lenkung mit Fischertechnik Servo
- Seilwinde
- Fernsteuerung.


Dieses Fahrzeug fährt im Wohnzimmer ganz gut. Eine steile Hang auffahren ist aber nich möglich.
