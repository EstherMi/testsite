---
layout: "image"
title: "Lenkrad und Ein-Ausschalter"
date: "2014-12-30T07:14:11"
picture: "tongabuggy07.jpg"
weight: "7"
konstrukteure: 
- "Jeroen"
fotografen:
- "Jeroen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "yoeroen"
license: "unknown"
legacy_id:
- details/40061
imported:
- "2019"
_4images_image_id: "40061"
_4images_cat_id: "3011"
_4images_user_id: "2174"
_4images_image_date: "2014-12-30T07:14:11"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40061 -->
Lenkrad und Ein-Ausschalter.
Lenkrad ist Rechts, wie auf Tonga üblich.