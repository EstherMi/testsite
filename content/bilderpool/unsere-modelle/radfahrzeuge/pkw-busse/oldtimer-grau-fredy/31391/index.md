---
layout: "image"
title: "Oldtimer"
date: "2011-07-28T11:42:56"
picture: "graueroldtimer1.jpg"
weight: "1"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/31391
imported:
- "2019"
_4images_image_id: "31391"
_4images_cat_id: "2335"
_4images_user_id: "453"
_4images_image_date: "2011-07-28T11:42:56"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31391 -->
http://youtu.be/5GtInetWhkg