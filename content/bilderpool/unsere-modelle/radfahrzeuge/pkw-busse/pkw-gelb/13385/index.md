---
layout: "image"
title: "und einen Kofferraum hat er auch"
date: "2008-01-25T15:36:04"
picture: "DSCN2047.jpg"
weight: "5"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/13385
imported:
- "2019"
_4images_image_id: "13385"
_4images_cat_id: "1219"
_4images_user_id: "184"
_4images_image_date: "2008-01-25T15:36:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13385 -->
