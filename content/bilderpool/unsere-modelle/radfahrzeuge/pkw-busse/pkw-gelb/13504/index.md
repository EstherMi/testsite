---
layout: "image"
title: "Gelbes Geländefahrzeug"
date: "2008-02-01T19:25:34"
picture: "IMG_0584.jpg"
weight: "1"
konstrukteure: 
- "Burkhard Eins"
fotografen:
- "Burkhard Eins"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Burkhard Eins"
license: "unknown"
legacy_id:
- details/13504
imported:
- "2019"
_4images_image_id: "13504"
_4images_cat_id: "1219"
_4images_user_id: "611"
_4images_image_date: "2008-02-01T19:25:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13504 -->
Gelbes Geländefahrzeug

Um den Kontrast etwas abzurunden wurden rote statt gelben Felgen bzw. Winkelsteinen verbaut.