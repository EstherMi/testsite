---
layout: "image"
title: "3/4-Ansicht von hinten"
date: "2012-02-26T22:38:24"
picture: "kleinesautofuerdasaeltereircontrolset2.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/34453
imported:
- "2019"
_4images_image_id: "34453"
_4images_cat_id: "2546"
_4images_user_id: "104"
_4images_image_date: "2012-02-26T22:38:24"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34453 -->
Der Fahrer hat ein schwarzes Leder-Sportlenkrad (Sonderausstattung). Die große Frontscheibe bietet einen super Überblick, allerdings heizt sich der Innenraum bei starker Fahrt auch stark auf - wie gut, dass die Fenster luftdurchlässig sind. ;-)

Der IR-Empfänger ist voll belegt.

Man sieht es auf diesen Bildern nicht so gut, aber die schwarzen I-Streben 120 verlaufen leicht nach außen geschwungen, weil sie vorne von je einem Winkelstein 7,5° gehalten werden.