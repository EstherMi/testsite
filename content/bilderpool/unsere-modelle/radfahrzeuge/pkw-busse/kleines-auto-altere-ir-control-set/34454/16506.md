---
layout: "comment"
hidden: true
title: "16506"
date: "2012-02-27T10:44:00"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Tach auch!

Die Spurweiten ergaben sich. Ich behaupte jetzt einfach rückwirkend, sie seien durch die Citroen DS und CX inspiriert worden, bei denen sie hinten auch deutlich schmaler war als vorne. ;-)

Die querlaufende Nut des von vorne gesehen zweiten BS30 wird benötigt, um die K-Achse für den Lenkantrieb zu lagern. Ohne ein Redesign der Lenkung wird es schwer fallen, da nochmal was rauszuholen. Und der MiniMot für die Lenkung passt nach vorne auch nicht so gut hin, weil dann der Bug so lang werden müsste.

Aber los - suchen wir nach dem kleinstmöglichen gelenkten angetriebenen ferngesteuerten Fahrzeug! "Geht nicht kleiner" wird als Aussage nie akzeptiert, denn schon morgen könnte ja einer eine Idee haben, wie es doch geht! Wer macht alles mit?

Gruß,
Stefan