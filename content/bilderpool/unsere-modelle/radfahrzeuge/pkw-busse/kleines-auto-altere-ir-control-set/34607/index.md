---
layout: "image"
title: "Nur noch ein Arm ab"
date: "2012-03-07T22:31:34"
picture: "servicepack1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/34607
imported:
- "2019"
_4images_image_id: "34607"
_4images_cat_id: "2546"
_4images_user_id: "104"
_4images_image_date: "2012-03-07T22:31:34"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34607 -->
Service Pack 1: Thomas004s Vorschlag geht: Die keilförmige Seitenlinie endet hinten nun etwas tiefer, sodass die Insassen nur noch einen Arm abgeben müssen. Das Dach ist dem auch angepasst.