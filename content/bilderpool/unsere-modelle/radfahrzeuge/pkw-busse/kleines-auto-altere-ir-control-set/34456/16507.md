---
layout: "comment"
hidden: true
title: "16507"
date: "2012-02-27T10:46:56"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
@thomas004: Vor allem deshalb, weil ich ein Auto wollte, bei dem per Fernsteuerung das Licht an/aus geschaltet werden kann, und das geht hier eben gut per M3-Ausgang. Zudem ist dieses Fahrzeug, wenn ich richtig abschätze, noch kürzer als Deines, und ganz abgesehen davon finde ich das alte IR-Set durchaus Klasse. Der Spielspaß ist durch die fehlende feinere Lenkung ja nun nicht sooo eingeschränkt. Und schließlich reizte einfach die Herausforderung, die nicht-Servo-Lenkung klein hinzubekommen.

Gruß,
Stefan