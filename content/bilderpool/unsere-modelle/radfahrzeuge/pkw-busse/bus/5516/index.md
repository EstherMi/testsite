---
layout: "image"
title: "Tür_1"
date: "2005-12-23T15:19:26"
picture: "Neuer_Ordner_001.jpg"
weight: "1"
konstrukteure: 
- "Christopher Wecht\ffcoe"
fotografen:
- "Christopher Wecht\ffcoe"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ffcoe"
license: "unknown"
legacy_id:
- details/5516
imported:
- "2019"
_4images_image_id: "5516"
_4images_cat_id: "406"
_4images_user_id: "332"
_4images_image_date: "2005-12-23T15:19:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5516 -->
Die Tür ist geschlossen.