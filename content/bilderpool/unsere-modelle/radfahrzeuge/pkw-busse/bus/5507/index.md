---
layout: "image"
title: "Heck_3"
date: "2005-12-20T18:26:09"
picture: "Neuer_Ordner_012.jpg"
weight: "9"
konstrukteure: 
- "Christopher Wecht"
fotografen:
- "Christopher Wecht"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ffcoe"
license: "unknown"
legacy_id:
- details/5507
imported:
- "2019"
_4images_image_id: "5507"
_4images_cat_id: "406"
_4images_user_id: "332"
_4images_image_date: "2005-12-20T18:26:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5507 -->
Ja, es stimmt, insgesamt 10 Federn hinten sind absolut notwenig. Mit acht Federn liegt der Bus so auf der Straße wie in den Bildern zuvor.