---
layout: "image"
title: "Frontfederung_2"
date: "2005-12-20T18:26:09"
picture: "Neuer_Ordner_016.jpg"
weight: "12"
konstrukteure: 
- "Christopher Wecht"
fotografen:
- "Christopher Wecht"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ffcoe"
license: "unknown"
legacy_id:
- details/5510
imported:
- "2019"
_4images_image_id: "5510"
_4images_cat_id: "406"
_4images_user_id: "332"
_4images_image_date: "2005-12-20T18:26:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5510 -->
