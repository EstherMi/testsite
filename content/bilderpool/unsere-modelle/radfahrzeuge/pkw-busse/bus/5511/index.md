---
layout: "image"
title: "Frontfederung_3"
date: "2005-12-20T18:26:09"
picture: "Neuer_Ordner_017.jpg"
weight: "13"
konstrukteure: 
- "Christopher Wecht"
fotografen:
- "Christopher Wecht"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ffcoe"
license: "unknown"
legacy_id:
- details/5511
imported:
- "2019"
_4images_image_id: "5511"
_4images_cat_id: "406"
_4images_user_id: "332"
_4images_image_date: "2005-12-20T18:26:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5511 -->
Unter den 120er Statikstreben sieht man insgesamt 4 Bauplten 15x30, denen jeweils ein Zapfen entfernt und ein Stück amputiert wurde. Sie halten jetzt die Bausteine 7,5 und die Lenkklaue an ihrem Platz.