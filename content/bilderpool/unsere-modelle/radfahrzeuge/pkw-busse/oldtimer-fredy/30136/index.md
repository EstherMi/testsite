---
layout: "image"
title: "Oldtimer"
date: "2011-02-26T19:34:18"
picture: "auto1.jpg"
weight: "2"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/30136
imported:
- "2019"
_4images_image_id: "30136"
_4images_cat_id: "2228"
_4images_user_id: "453"
_4images_image_date: "2011-02-26T19:34:18"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30136 -->
