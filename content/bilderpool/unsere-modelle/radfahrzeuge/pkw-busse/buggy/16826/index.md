---
layout: "image"
title: "Lenkung"
date: "2008-12-31T19:10:26"
picture: "buggy09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/16826
imported:
- "2019"
_4images_image_id: "16826"
_4images_cat_id: "1517"
_4images_user_id: "104"
_4images_image_date: "2008-12-31T19:10:26"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16826 -->
Hier ein paar Details der Lenkung. Für diese Aufnahme wurde der Akku herausgenommen. Der ist tatsächlich nur mit den roten Zapfen zweier BS15 in den Löchern der WS60 eingehängt.