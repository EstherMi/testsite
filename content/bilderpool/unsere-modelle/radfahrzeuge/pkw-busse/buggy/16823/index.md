---
layout: "image"
title: "Hinterachse"
date: "2008-12-31T19:10:24"
picture: "buggy06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/16823
imported:
- "2019"
_4images_image_id: "16823"
_4images_cat_id: "1517"
_4images_user_id: "104"
_4images_image_date: "2008-12-31T19:10:24"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16823 -->
Das Kardangelenk knickt genau im richtigen Punkt.