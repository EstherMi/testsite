---
layout: "image"
title: "Vorderachse"
date: "2008-12-31T19:10:25"
picture: "buggy08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/16825
imported:
- "2019"
_4images_image_id: "16825"
_4images_cat_id: "1517"
_4images_user_id: "104"
_4images_image_date: "2008-12-31T19:10:25"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16825 -->
Ein Mini-Motor lenkt. Er ist Teil der gefederten Vorderachse, die dadurch allerdings eine recht hohe ungefederte Masse hat. Der Taster in der Mitte dient für die übliche Verschaltung mit dem IR-Empfänger.