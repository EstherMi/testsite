---
layout: "overview"
title: "Buggy"
date: 2019-12-17T18:45:34+01:00
legacy_id:
- categories/1517
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1517 --> 
Da ich mein ft wieder für längere Zeit wegräumen musste, wollte ich wenigstens *irgendwas* zum Rumspielen machen. Daraus ist in aller Eile an einem kurzen Abend ein etwas verbogen aussehendes Fahrzeug entstanden. ;-)