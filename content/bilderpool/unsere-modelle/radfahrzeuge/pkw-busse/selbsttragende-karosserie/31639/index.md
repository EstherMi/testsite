---
layout: "image"
title: "Von vorne"
date: "2011-08-22T15:46:51"
picture: "selbsttragendekarosserieauto4.jpg"
weight: "7"
konstrukteure: 
- "Barth Thomas"
fotografen:
- "Barth Thomas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "choco"
license: "unknown"
legacy_id:
- details/31639
imported:
- "2019"
_4images_image_id: "31639"
_4images_cat_id: "2359"
_4images_user_id: "1347"
_4images_image_date: "2011-08-22T15:46:51"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31639 -->
