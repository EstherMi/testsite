---
layout: "image"
title: "PKW-station-01"
date: "2015-06-29T23:51:40"
picture: "pkwstation01.jpg"
weight: "2"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/41318
imported:
- "2019"
_4images_image_id: "41318"
_4images_cat_id: "3089"
_4images_user_id: "2449"
_4images_image_date: "2015-06-29T23:51:40"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41318 -->
Ein variant auf ein altes Modell. Ich habe das in der letzte Woche erneut gebaut, weil Ich mal wissen wollte wie es aussehen würde mit den neuen Roten Bausteine die Ich vorher nicht hatte. Auch hab Ich den neuen IR-control set Eingebaut. Das gab nog ein Bißchen Ärger, weil den Kabel vom Servo ziemlich kurz ist, und Ich den Empfanger also nicht Hinten am Auto bauen konnte. Aber in diesen Station-Ausführung gab es genug Raum.