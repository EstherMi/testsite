---
layout: "image"
title: "pkwstation1.jpg"
date: "2017-03-12T13:48:38"
picture: "pkwstation1.jpg"
weight: "1"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/45493
imported:
- "2019"
_4images_image_id: "45493"
_4images_cat_id: "3089"
_4images_user_id: "2449"
_4images_image_date: "2017-03-12T13:48:38"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45493 -->
Das geht auch mit 'ne runde Haube