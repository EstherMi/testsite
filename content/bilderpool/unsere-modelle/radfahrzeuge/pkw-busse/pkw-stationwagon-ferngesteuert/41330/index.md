---
layout: "image"
title: "PKW-station-16"
date: "2015-06-29T23:51:57"
picture: "pkwstation13.jpg"
weight: "14"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/41330
imported:
- "2019"
_4images_image_id: "41330"
_4images_cat_id: "3089"
_4images_user_id: "2449"
_4images_image_date: "2015-06-29T23:51:57"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41330 -->
