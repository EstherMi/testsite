---
layout: "image"
title: "PKW-station-11"
date: "2015-06-29T23:51:40"
picture: "pkwstation08.jpg"
weight: "9"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/41325
imported:
- "2019"
_4images_image_id: "41325"
_4images_cat_id: "3089"
_4images_user_id: "2449"
_4images_image_date: "2015-06-29T23:51:40"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41325 -->
Den Servo. Die 4 kleine Abdeckplatten 15x15 sind nur dazu, das die Lenksteinen nicht schieben.