---
layout: "image"
title: "PKW-station-19"
date: "2015-06-29T23:51:57"
picture: "pkwstation16.jpg"
weight: "17"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/41333
imported:
- "2019"
_4images_image_id: "41333"
_4images_cat_id: "3089"
_4images_user_id: "2449"
_4images_image_date: "2015-06-29T23:51:57"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41333 -->
