---
layout: "image"
title: "Ansicht von vorne"
date: "2015-01-07T22:44:05"
picture: "sportwagen05.jpg"
weight: "5"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/40201
imported:
- "2019"
_4images_image_id: "40201"
_4images_cat_id: "3020"
_4images_user_id: "2321"
_4images_image_date: "2015-01-07T22:44:05"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40201 -->
Vielleicht kann man so die Krümmungen der Fronthaube etwas besser sehen. Ich bin ja eher ein Freund der alten Teile, aber hin und wieder ein kleiner Farbakzent ist auch nicht schlecht.