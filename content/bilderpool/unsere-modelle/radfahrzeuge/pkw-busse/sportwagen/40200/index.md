---
layout: "image"
title: "Ansicht von schräg hinten"
date: "2015-01-07T22:44:05"
picture: "sportwagen04.jpg"
weight: "4"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/40200
imported:
- "2019"
_4images_image_id: "40200"
_4images_cat_id: "3020"
_4images_user_id: "2321"
_4images_image_date: "2015-01-07T22:44:05"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40200 -->
Markante Endrohre müssen sein.