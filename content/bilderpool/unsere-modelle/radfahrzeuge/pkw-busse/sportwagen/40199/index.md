---
layout: "image"
title: "Seitenansicht"
date: "2015-01-07T22:44:05"
picture: "sportwagen03.jpg"
weight: "3"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/40199
imported:
- "2019"
_4images_image_id: "40199"
_4images_cat_id: "3020"
_4images_user_id: "2321"
_4images_image_date: "2015-01-07T22:44:05"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40199 -->
Die Räder stammen von http://www.ftcommunity.de/categories.php?cat_id=2998. Man kann vielleicht schon erahnen, das es typgerecht einen Mittelmotor hat.