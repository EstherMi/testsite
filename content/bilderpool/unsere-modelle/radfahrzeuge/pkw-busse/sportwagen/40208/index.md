---
layout: "image"
title: "Heckklappe demontiert"
date: "2015-01-07T22:44:05"
picture: "sportwagen12.jpg"
weight: "12"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/40208
imported:
- "2019"
_4images_image_id: "40208"
_4images_cat_id: "3020"
_4images_user_id: "2321"
_4images_image_date: "2015-01-07T22:44:05"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40208 -->
Heckklappe einmal ausgebaut, falls es jemanden interessiert. Für den Akkuanschluss musste ich Kabel mit kleinen Steckern nehmen, die normalen hätten nicht unter den Heckspoiler gepasst.