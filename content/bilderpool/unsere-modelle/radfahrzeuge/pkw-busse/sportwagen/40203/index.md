---
layout: "image"
title: "Ansicht von oben"
date: "2015-01-07T22:44:05"
picture: "sportwagen07.jpg"
weight: "7"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/40203
imported:
- "2019"
_4images_image_id: "40203"
_4images_cat_id: "3020"
_4images_user_id: "2321"
_4images_image_date: "2015-01-07T22:44:05"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40203 -->
Die Heckklappe hat Lücken, weniger zur Kühlung als zur Präsentation der Technik (also wie beim Vorbild :-) ).