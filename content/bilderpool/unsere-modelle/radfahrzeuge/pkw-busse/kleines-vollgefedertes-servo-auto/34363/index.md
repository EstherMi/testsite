---
layout: "image"
title: "Hinterachse"
date: "2012-02-22T22:48:30"
picture: "kleinesvollgefedertesservoauto8.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/34363
imported:
- "2019"
_4images_image_id: "34363"
_4images_cat_id: "2541"
_4images_user_id: "104"
_4images_image_date: "2012-02-22T22:48:30"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34363 -->
Außer dem Ein-/Ausschalter sieht man hier die Hinterachse. Per Statikadapter ist da eine I-Strebe 45 quer aufgelegt. Zwischen der und den Bausteinen der Achskonstruktion enden die I-Streben 120, die längs durchs Fahrzeug führen. Dadurch halten die die Hinterachse hinreichen fest nach oben, wenn man das Fahrzeug einfach anhebt. Die Statikadapter selbst haben auch eine Funktion: Sie sichern die Hinterachse zusammen mit den I-Streben 120 gegen seitliches Versetzen.