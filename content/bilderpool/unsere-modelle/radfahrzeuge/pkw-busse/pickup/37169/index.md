---
layout: "image"
title: "pickup01.jpg"
date: "2013-07-20T17:34:05"
picture: "IMG_8987.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/37169
imported:
- "2019"
_4images_image_id: "37169"
_4images_cat_id: "2761"
_4images_user_id: "4"
_4images_image_date: "2013-07-20T17:34:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37169 -->
Der Frontantrieb von hier:
http://www.ftcommunity.de/details.php?image_id=36027
hat ja nun lange genug gewartet, bis er zu einem ganzen Auto ergänzt wurde.