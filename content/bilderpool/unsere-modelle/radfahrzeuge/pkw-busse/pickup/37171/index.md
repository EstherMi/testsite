---
layout: "image"
title: "pickup03.jpg"
date: "2013-07-20T17:37:20"
picture: "IMG_9073mit.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Frontantrieb"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/37171
imported:
- "2019"
_4images_image_id: "37171"
_4images_cat_id: "2761"
_4images_user_id: "4"
_4images_image_date: "2013-07-20T17:37:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37171 -->
Unter der Haube ist alles unverändert geblieben bis auf zwei zusätzliche ft-Scheiben, die für besseren Andruck der Rastkegelräder an die Antriebsräder sorgen.