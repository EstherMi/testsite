---
layout: "image"
title: "pickup05.jpg"
date: "2013-07-20T17:42:49"
picture: "IMG_9077mit.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/37173
imported:
- "2019"
_4images_image_id: "37173"
_4images_cat_id: "2761"
_4images_user_id: "4"
_4images_image_date: "2013-07-20T17:42:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37173 -->
Die "Hinterachse" ist gar keine: das ist Einzelradaufhängung, gefedert. Das Rad sitzt auf einer gelben Freilaufnabe (*) und ist mit einem Rast-Clip gesichert, den man erhält, wenn man ein Rast-Z10 mit dem Messer bearbeitet.

(*) Die gibt es (wieder): der Kasten "Bulldozer" 520395 enthält vier schwarze Freilaufnaben.