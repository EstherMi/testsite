---
layout: "image"
title: "Mini-Frontantrieb 7"
date: "2011-01-23T20:20:34"
picture: "Mini-Frontantrieb_7.jpg"
weight: "7"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/29788
imported:
- "2019"
_4images_image_id: "29788"
_4images_cat_id: "2189"
_4images_user_id: "328"
_4images_image_date: "2011-01-23T20:20:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29788 -->
Der Innenraum ist geprägt von Batteriekasten und Empfänger. Hinten ist eine Art kleiner Kofferraum, wo man etwas verstauen kann.