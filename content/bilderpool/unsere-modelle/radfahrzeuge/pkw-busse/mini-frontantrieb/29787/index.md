---
layout: "image"
title: "Mini-Frontantrieb 6"
date: "2011-01-23T20:20:34"
picture: "Mini-Frontantrieb_6.jpg"
weight: "6"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/29787
imported:
- "2019"
_4images_image_id: "29787"
_4images_cat_id: "2189"
_4images_user_id: "328"
_4images_image_date: "2011-01-23T20:20:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29787 -->
Hier wird klar, warum das Auto ein Rechtslenker wurde: Das "Auge" vom Empfänger geht nur rechts, und daher auch das Lenkrad.