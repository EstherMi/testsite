---
layout: "image"
title: "Ente29.JPG"
date: "2006-07-10T17:57:34"
picture: "Ente29.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/6619
imported:
- "2019"
_4images_image_id: "6619"
_4images_cat_id: "570"
_4images_user_id: "4"
_4images_image_date: "2006-07-10T17:57:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6619 -->
Die ausgebaute Rückenlehne der hinteren Bank.