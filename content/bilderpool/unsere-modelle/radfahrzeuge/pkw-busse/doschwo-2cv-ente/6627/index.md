---
layout: "image"
title: "Ente63.JPG"
date: "2006-07-10T18:16:21"
picture: "Ente63.JPG"
weight: "15"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/6627
imported:
- "2019"
_4images_image_id: "6627"
_4images_cat_id: "570"
_4images_user_id: "4"
_4images_image_date: "2006-07-10T18:16:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6627 -->
Die vordere Sitzbank ist nur an den B-Säulen befestigt, daher haben die hinteren Passagiere sehr viel Beinfreiheit.