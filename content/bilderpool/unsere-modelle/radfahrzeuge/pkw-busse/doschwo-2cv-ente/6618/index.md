---
layout: "image"
title: "Ente25.JPG"
date: "2006-07-10T17:56:10"
picture: "Ente25.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/6618
imported:
- "2019"
_4images_image_id: "6618"
_4images_cat_id: "570"
_4images_user_id: "4"
_4images_image_date: "2006-07-10T17:56:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6618 -->
Blick von unten auf die Lenkung. Der Akku ist herausgenommen, der füllt das ganze freie Loch aus.


Ja richtig, bei der echten Ente sitzt der Motor vorn, und sie hat Frontantrieb. Technisch ist das hier also eher ein Käfer (Motor und Antrieb hinten) als eine Ente. Ich lasse es aber trotzdem bei "Ente"; schließlich sieht das Modell von außen eher nach Ente aus.