---
layout: "image"
title: "Ente01.JPG"
date: "2006-07-10T17:44:41"
picture: "Ente01.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/6613
imported:
- "2019"
_4images_image_id: "6613"
_4images_cat_id: "570"
_4images_user_id: "4"
_4images_image_date: "2006-07-10T17:44:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6613 -->
Es fehlen noch die Kotflügel, die Türen rechts und die Dachplane. Aber bis zur Convention wird das noch.