---
layout: "image"
title: "Ente66.JPG"
date: "2006-07-10T18:18:37"
picture: "Ente66.JPG"
weight: "17"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/6629
imported:
- "2019"
_4images_image_id: "6629"
_4images_cat_id: "570"
_4images_user_id: "4"
_4images_image_date: "2006-07-10T18:18:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6629 -->
Mit Müh und Not geht die Haube zu.