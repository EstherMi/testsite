---
layout: "image"
title: "Ente30.JPG"
date: "2006-07-10T18:00:36"
picture: "Ente30.JPG"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/6620
imported:
- "2019"
_4images_image_id: "6620"
_4images_cat_id: "570"
_4images_user_id: "4"
_4images_image_date: "2006-07-10T18:00:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6620 -->
Der Motor sitzt bei der echten Ente vorn und treibt die Vorderräder an, deshalb haben wir hier eine "falsche Ente". Es ging halt nicht anders. Aber egal: nach Lösen zweier S-Riegel und der Kabel zur Heckbeleuchtung kann der Motor ausgeschwenkt werden.