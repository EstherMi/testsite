---
layout: "image"
title: "Ente04.JPG"
date: "2006-07-10T17:45:10"
picture: "Ente04.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/6614
imported:
- "2019"
_4images_image_id: "6614"
_4images_cat_id: "570"
_4images_user_id: "4"
_4images_image_date: "2006-07-10T17:45:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6614 -->
