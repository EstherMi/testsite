---
layout: "image"
title: "Ente09.JPG"
date: "2006-07-10T17:52:15"
picture: "Ente09.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/6617
imported:
- "2019"
_4images_image_id: "6617"
_4images_cat_id: "570"
_4images_user_id: "4"
_4images_image_date: "2006-07-10T17:52:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6617 -->
Die vordere Tür öffnet man nach Anheben des Rollenbock 32085. Die hintere Tür öffnet man durch Hochziehen der K-Achse 50.

Die hinteren Türscharniere sind mit der Lenkklaue 35998 aufgebaut, Details siehe http://www.ftcommunity.de/details.php?image_id=5594