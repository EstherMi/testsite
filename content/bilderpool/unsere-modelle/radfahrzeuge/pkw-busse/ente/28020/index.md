---
layout: "image"
title: "Pick-up Ente"
date: "2010-08-29T20:56:12"
picture: "Pick-up_Ente_unterbodenleuchte_2.jpg"
weight: "1"
konstrukteure: 
- "lars b."
fotografen:
- "lars b."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lars"
license: "unknown"
legacy_id:
- details/28020
imported:
- "2019"
_4images_image_id: "28020"
_4images_cat_id: "2235"
_4images_user_id: "1177"
_4images_image_date: "2010-08-29T20:56:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28020 -->
mein modell einer ente die mit einen handgriff zu einem pick-up umgebaut werden kann 
im dunklen (+unterbodenbeleuchtung)