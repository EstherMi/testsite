---
layout: "image"
title: "Routemaster"
date: "2018-05-13T08:59:58"
picture: "Routemaster_-_12_of_14.jpg"
weight: "12"
konstrukteure: 
- "Volker"
fotografen:
- "Volker"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "voxbremen"
license: "unknown"
legacy_id:
- details/47657
imported:
- "2019"
_4images_image_id: "47657"
_4images_cat_id: "3513"
_4images_user_id: "2739"
_4images_image_date: "2018-05-13T08:59:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47657 -->
Bluetooth Fernsteuerung für Lenkung, Fahren und Hupe.

Soundmodul für Motorgeräusch, Musik und Hupe. Leider wird der aktuelle Clip immer bis zum Ende gespielt, bevor der nächste starten kann, was bei der Hupe etwas störend ist.