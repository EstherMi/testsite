---
layout: "image"
title: "Dragster"
date: "2011-01-22T19:45:38"
picture: "neueautomodelle1.jpg"
weight: "1"
konstrukteure: 
- "Lars B."
fotografen:
- "Lars B."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lars"
license: "unknown"
legacy_id:
- details/29750
imported:
- "2019"
_4images_image_id: "29750"
_4images_cat_id: "2187"
_4images_user_id: "1177"
_4images_image_date: "2011-01-22T19:45:38"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29750 -->
ein sehr schnelles auto mit einem 8:1 powermotor