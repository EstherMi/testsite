---
layout: "image"
title: "Universal II-Fahrzeug von unten"
date: "2009-01-20T16:50:23"
picture: "IMG_5758.jpg"
weight: "5"
konstrukteure: 
- "Scapegrace"
fotografen:
- "Scapegrace"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "scapegrace"
license: "unknown"
legacy_id:
- details/17115
imported:
- "2019"
_4images_image_id: "17115"
_4images_cat_id: "1536"
_4images_user_id: "903"
_4images_image_date: "2009-01-20T16:50:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17115 -->
nochmal das Fahrzeug von unten. Zu sehen ist die Achsschenkellenkung aus Standard-Teilen sowie der quer eingebaute Motor. Der Antrieb geht nur auf ein Hinterrad - das MiniMot-Differential hat für die Standard-Räder einen zu großen durchbesser (größer als die Räder). Die Achsen kann man einfach in das Rad stecken. Sie sitzt fest genug für den Kraftschluss. 

Besonderen Wert habe ich auch auf die Verlegung der Kabel gelegt (Batteriefach hinten, MiniMot vorne und Schalter im Cockpit. Die Kabel sind in Führungen oder im doppelten Fahrzeugboden verlegt.