---
layout: "image"
title: "Universal II-Fahrzeug mit nicht Universal II-Rädern"
date: "2009-01-20T16:50:23"
picture: "IMG_5760.jpg"
weight: "7"
konstrukteure: 
- "Scapegrace"
fotografen:
- "Scapegrace"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "scapegrace"
license: "unknown"
legacy_id:
- details/17117
imported:
- "2019"
_4images_image_id: "17117"
_4images_cat_id: "1536"
_4images_user_id: "903"
_4images_image_date: "2009-01-20T16:50:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17117 -->
mit anderen Rädern (nicht im Universal II enthalten) sieht das häßliche Entlein schon besser aus.