---
layout: "image"
title: "Fahrzeug aus Universal II Teilen"
date: "2009-01-20T16:50:22"
picture: "IMG_5752.jpg"
weight: "3"
konstrukteure: 
- "Scapegrace"
fotografen:
- "Scapegrace"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "scapegrace"
license: "unknown"
legacy_id:
- details/17113
imported:
- "2019"
_4images_image_id: "17113"
_4images_cat_id: "1536"
_4images_user_id: "903"
_4images_image_date: "2009-01-20T16:50:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17113 -->
Das Fahrzeug von hinten. Zu sehen ist hinter der Hinterachse der verbaute Batterienkasten. Das Fahrzeug ist Heckgetrieben, die Batterie bringt notwendiges Gewicht auf die Achse. Das ist nötig, denn der Motor ist vorne quer eingebaut. Im Cockpit ist neben der Lenkung der Schalter für den Minimot verbaut.