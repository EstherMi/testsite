---
layout: "image"
title: "Kleinwagen 1"
date: "2010-03-20T18:00:05"
picture: "Kleinwagen_01.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/26752
imported:
- "2019"
_4images_image_id: "26752"
_4images_cat_id: "1910"
_4images_user_id: "328"
_4images_image_date: "2010-03-20T18:00:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26752 -->
Dieses Modell eines Kleinwagens ist - ganz unbescheiden - eine im wahrsten Sinne des Wortes kleine Revolution im Fischertechnik-Fahrzeugbau. Verbaut ist eine bahnbrechende Erfindung: Meine "servo-gelenkte Vorderachse 80"!

Erstmalig wurde die kleine Go-Cart-Vorderachse mit einem Servo versehen, so dass sie ferngesteuert lenkbar ist. Dadurch sind endlich kleine realistische ferngesteuerte Fischertechnik-Fahrzeug-Modelle auf Basis des Rades 23 (34994) bzw. der Speichenfelge 23 (31987) möglich! Zusammen mit dem Fischertechnik-Männchen, dem Sitz und dem Lenkrad ergeben sich erstmalig realistische PKW-Proportionen, die bislang aufgrund der großen Reifen bei ferngesteuerten Fahrzeugen nicht möglich waren. Ich wage zu behaupten, dass sich damit neue Modellwelten eröffnen...

Mein Kleinwagen bietet komfortablen Platz für 2 Passagiere. Angetrieben wird er von einem Mini-Motor auf die Hinterachse, gelenkt wird per Servo. Im Unterboden ist Platz für einen 9V-Blockakku; der Empfänger sitzt im Heck. Im Vorderwagen ist der Schalter integriert, der den Empfänger an- und ausschaltet. Auf ein Differenzial an der Hinterachse wurde aus Platzgründen verzichtet.

Antrieb und Spannungsversorgung sitzen platzsparend im Unterboden, so dass das Fahrzeug extrem kurz und kompakt ist. Der moderne schräge Vorderwagen in Verbindung mit dem knackigen Heck ergibt aus nahezu allen Blickrichtungen sehr realistische und teilweise wirklich hinreißende Proportionen. :o)

Das Fahrverhalten des Kleinwagens ist trotz fehlendem Differenzial ganz ordentlich. Zumindest auf unebenem Untergrund kommt er durch die "gesperrte" Hinterachse sehr gut voran. Er fährt nicht allzu schnell, ist aber durch den großen Lenkeinschlag der "servo-gelenkten Vorderachse 80"“sehr wendig. Die neuartige Vorderachse erkläre ich später beim entsprechenden Foto.