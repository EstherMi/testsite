---
layout: "image"
title: "Kleinwagen 12"
date: "2010-03-20T18:00:07"
picture: "Kleinwagen_22.jpg"
weight: "12"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/26763
imported:
- "2019"
_4images_image_id: "26763"
_4images_cat_id: "1910"
_4images_user_id: "328"
_4images_image_date: "2010-03-20T18:00:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26763 -->
Nun zur Beschreibung der neuartigen "servo-gelenkten Vorderachse 80":

Die eigentliche Spurstange R30 (32229) zwischen den Achsschenkeln wurde nach unten verlegt. Oben gibt es eine zweite Spurstange, die aber nur auf einer Seite mit dem Achsschenkel verbunden ist (hier auf der rechten Fahrzeugseite).

Die Basis der Erfindung ist im Grunde die Entdeckung, dass das Rast-Ende einer normalen Rastachse absolut perfekt und spielfrei in das Ende der Spurstange R30 (32229) passt! Als ob es dafür gemacht wäre!

Der Servohebel ist über eine Statikstrebe 30 mit dieser Rastachse verbunden. Es reicht das mittlere Loch im Servohebel; der gesamte Hebel ist zum Lenken zu lang. Da das Ende der oberen Spurstange nun noch beweglich im Raum hängt, wird dieses mittels einer Statikstrebe 15 mit der Drehachse des Vorderrades drehbar verbunden (hier auf der linken Fahrzeugseite). Das Ende der Spurstange liegt nun leicht neben seiner ursprünglichen Lage, klemmt aber keineswegs und lässt sich leicht bewegen.

Alle Elemente der "servo-gelenkten Vorderachse 80" sind perfekt im Raster, sehr stabil und natürlich nach dem Fischertechnik-Reinheitsgebot ohne Modifikationen!

Der durch den halben Servohebel erreichbare Lenkwinkel passt überraschend gut zum maximal möglichen Lenkwinkel der Achsschenkel. Der Ausschlag des Servos wird voll ausgenutzt, aber es klemmt gerade noch nichts. Das Lenkverhalten ist sehr direkt und schnell und wirkt sehr stabil. Besser könnte es kaum sein!