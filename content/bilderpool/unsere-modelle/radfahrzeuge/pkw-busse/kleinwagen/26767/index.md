---
layout: "image"
title: "Kleinwagen 16"
date: "2010-03-20T18:00:09"
picture: "Kleinwagen_20.jpg"
weight: "16"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/26767
imported:
- "2019"
_4images_image_id: "26767"
_4images_cat_id: "1910"
_4images_user_id: "328"
_4images_image_date: "2010-03-20T18:00:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26767 -->
Die Vorderachse bei abgebauter Fronthaube.