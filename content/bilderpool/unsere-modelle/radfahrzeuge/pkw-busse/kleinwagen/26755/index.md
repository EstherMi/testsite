---
layout: "image"
title: "Kleinwagen 4"
date: "2010-03-20T18:00:07"
picture: "Kleinwagen_07.jpg"
weight: "4"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/26755
imported:
- "2019"
_4images_image_id: "26755"
_4images_cat_id: "1910"
_4images_user_id: "328"
_4images_image_date: "2010-03-20T18:00:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26755 -->
Die Kabelverlegung war etwas schwierig, da das Kabel vom Empfänger zum Servo zu kurz ist...