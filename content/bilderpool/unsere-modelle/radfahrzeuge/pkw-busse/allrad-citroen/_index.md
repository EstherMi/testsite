---
layout: "overview"
title: "Allrad-Citroën"
date: 2019-12-17T18:45:31+01:00
legacy_id:
- categories/686
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=686 --> 
Ein Auto mit Allradantrieb, Lenkung, Federung, Einzelradaufhängung, vollautomatischem Niveauausgleich und einstellbarer Bodenfreiheit.