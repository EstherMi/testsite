---
layout: "image"
title: "Von hinten (1)"
date: "2006-10-02T16:16:38"
picture: "allradcitroen04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/7086
imported:
- "2019"
_4images_image_id: "7086"
_4images_cat_id: "686"
_4images_user_id: "104"
_4images_image_date: "2006-10-02T16:16:38"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7086 -->
Mit geschlossener Heckklappe. Man beachte das unglaublich naturgetreue ;-) Citroën-Logo, den Doppelwinkel.