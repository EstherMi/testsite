---
layout: "image"
title: "Vorderachse von rechts"
date: "2006-10-02T16:16:38"
picture: "allradcitroen06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/7088
imported:
- "2019"
_4images_image_id: "7088"
_4images_cat_id: "686"
_4images_user_id: "104"
_4images_image_date: "2006-10-02T16:16:38"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7088 -->
Man sieht gut das Reibrad 14, welches das Vorderrad antreibt. Rechts unten sieht man (quer zur Fahrtrichtung eingebaut) einen MiniMot bzw. S-Motor, der über Winkelzahnräder eine Achse dreht, auf der dadurch ein Gummi mehr oder weniger stark aufgewickelt wird. Damit können die Federspannung und damit die Bodenfreiheit reguliert werden.

Links vom Vorderrad sieht man ein Bisschen eines grauen MiniMot, der die Lenkung übernimmt.

Etwas dahinter sieht man links eine gelenkige Aufhängung, an der letztlich die Metallachse hängt, die schräg nach rechts unten läuft (rechts über dem Vorderrad sichtbar). Dasselbe gibt es auf der linken Seite noch mal. Beide Achsen sind ganz vorne durch Streben miteinander verbunden. Mitten an dieser Strebe sitzt die Sensorik für die Bodenfreiheit sowie das vom MiniMot gezogene Gummi. An den Metallachsen schließlich stützen sich die Vorderradaufhängung ab - dadurch wird das Rad nach unten bzw. das Fahrzeug hoch gedrückt.