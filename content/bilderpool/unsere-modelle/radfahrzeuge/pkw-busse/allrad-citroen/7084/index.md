---
layout: "image"
title: "Von vorne"
date: "2006-10-02T16:16:38"
picture: "allradcitroen02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/7084
imported:
- "2019"
_4images_image_id: "7084"
_4images_cat_id: "686"
_4images_user_id: "104"
_4images_image_date: "2006-10-02T16:16:38"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7084 -->
Die Vorderachse entspricht im Wesentlichen des Prototypen, der hier schon mal veröffentlicht war. Diese Fassung ist aber um 30 mm schmaler, weil die mittleren Gelenke für beide Seiten nur noch an gemeinsamen Achsen aufgehängt sind.