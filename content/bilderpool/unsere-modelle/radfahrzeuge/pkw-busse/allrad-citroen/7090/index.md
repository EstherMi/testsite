---
layout: "image"
title: "Vorderachse von unten (1)"
date: "2006-10-02T16:16:38"
picture: "allradcitroen08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/7090
imported:
- "2019"
_4images_image_id: "7090"
_4images_cat_id: "686"
_4images_user_id: "104"
_4images_image_date: "2006-10-02T16:16:38"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7090 -->
Von den Metallstangen, die längs zur Fahrtrichtung verlaufen und an denen die Gelenke für die Querlenker hängen, gibt es insgesamt 3 Stück, die übereinander angeordnet sind. An der oberen und unteren hängen Gelenke (2 oben, 1 unten). Die mittlere dient nur der zusätzlichen Versteifung, denn außer dem bisschen Motorhabe gibt es außer diesen drei Achsen keine feste Verbindung zwischen der Frontpartie und dem Rest des Autos.