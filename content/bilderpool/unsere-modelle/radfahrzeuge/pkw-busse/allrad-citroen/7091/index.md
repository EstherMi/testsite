---
layout: "image"
title: "Niveauregulierung vorne von unten"
date: "2006-10-02T16:16:38"
picture: "allradcitroen09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/7091
imported:
- "2019"
_4images_image_id: "7091"
_4images_cat_id: "686"
_4images_user_id: "104"
_4images_image_date: "2006-10-02T16:16:38"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7091 -->
Hier sieht man in der Mitte das Gummi, welches vom MiniMot mehr oder weniger aufgewickelt wird. Hinten funktionierte das wunderbar, leider ist aber mein alter MiniMot in Baustein-30-Größe kaputt gegangen. Der passte längs zur Fahrtrichtung hinein und brauchte also keine Winkelzahnräder. Diese Lösung hier funktionierte leider nicht so gut, weil die Kräfte, die über die Winkelzahnräder geleitet werden müssen, doch etwas zu groß sind.

An der ganz vorne sichtbaren, senkrecht stehenden Metallachse gleitet ein BS15 synchron mit der Einfederung der Vorderachse entlang. Der BS15 trägt einen Kontakt (aus dem alten Elektromechanik-Programm), der auf  die Kontaktplättchen einer ebenfalls alten roten Kontaktplatte mit drei Feldern greift. Damit wird das Niveau festgestellt:

Niveau 1: Nur die untere Platte hat Kontakt.
Niveau 2: Untere und mittlere Platten haben Kontakt.
Niveau 3: Nur die mittlere Platte hat Kontakt.
Niveau 4: Die mittleren und oberen Platten haben Kontakt.
Niveau 5: Nur die obere Platte hat Kontakt.

Dieser Wert kann mit einem Sollwert verglichen werden (der einstellbar ist), und dadurch kann der MiniMot entsprechend gesteuert werden.

Die Software fängt an, den MiniMot einzuschalten, wenn eine Achse länger als eine Sekunde über oder unter Sollniveau liegt.

Durch die Niveauregulierung kann das Auto, ganz wie das Original, eine sehr weiche Federung dennoch konstante Bodenfreiheit haben.