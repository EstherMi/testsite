---
layout: "image"
title: "Von links"
date: "2006-10-02T16:16:38"
picture: "allradcitroen03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/7085
imported:
- "2019"
_4images_image_id: "7085"
_4images_cat_id: "686"
_4images_user_id: "104"
_4images_image_date: "2006-10-02T16:16:38"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7085 -->
Außerdem kamen ein automatischer Niveauausgleich und eine verstellbare Bodenfreiheit hinzu. Deshalb wird das ganze via IR-Fernbedienung von einem Robo-Interface gesteuert.