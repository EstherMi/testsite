---
layout: "image"
title: "Hinterachse von unten"
date: "2006-10-02T16:28:11"
picture: "allradcitroen11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/7093
imported:
- "2019"
_4images_image_id: "7093"
_4images_cat_id: "686"
_4images_user_id: "104"
_4images_image_date: "2006-10-02T16:28:11"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7093 -->
Gegenüber der Vorderachse kann man die Hinterachse sicher als primitiv bezeichnen. Allerdings funktioniert sie hervorragend, und die Maße für den Reibradantrieb ergeben sich auch auf ganz natürliche Weise. Der Motor für den Antrieb der Hinterachse treibt das Differential an, und auf den davon getriebenen Achsen sind sowohl die Längslenker als auch die Reibräder selbst aufgehängt.

Der MiniMot links ist für den Niveauausgleich der Hinterachse zuständig. Er zieht hier sogar an zwei Gummis (im Gegensatz zur Vorderachse), wodurch eine evtl. Seitenneigung ein bisschen ausgeglichen werden kann, weil links und rechts anstatt nur in der Mitte gezogen wird.