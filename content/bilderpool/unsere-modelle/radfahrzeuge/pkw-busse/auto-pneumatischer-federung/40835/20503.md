---
layout: "comment"
hidden: true
title: "20503"
date: "2015-04-20T12:53:27"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Hier sieht man's ;-)

http://www.ftcommunity.de/details.php?image_id=39577
http://www.ftcommunity.de/details.php?image_id=39578

Ich hab aber noch ein paar Prototypen-Fotos und -Videos; die lade ich nachher noch hoch. Insbesondere sieht man auf dem Video auch, wie weich man die Federung einstellen kann.

Gruß,
Stefan