---
layout: "image"
title: "Magnetventile und Drosseln"
date: "2015-04-19T16:55:02"
picture: "pneumatikfederung18.jpg"
weight: "18"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/40827
imported:
- "2019"
_4images_image_id: "40827"
_4images_cat_id: "3067"
_4images_user_id: "104"
_4images_image_date: "2015-04-19T16:55:02"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40827 -->
Hier sieht man von unten fotografiert ein Magnetventil und eine Drossel für ein Vorderrad, sowie rechts davon einen Hinterrad-Zylinder.