---
layout: "image"
title: "Technik im Innenraum (1)"
date: "2015-04-19T16:55:02"
picture: "pneumatikfederung14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/40823
imported:
- "2019"
_4images_image_id: "40823"
_4images_cat_id: "3067"
_4images_user_id: "104"
_4images_image_date: "2015-04-19T16:55:02"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40823 -->
Im Fahrzeuginnenraum sitzen Akku, Ein-/Aus-Schalter, Kompressor, drei Magnetventile und drei Drosseln (je eines pro Vorderrad und eines gemeinsam für die Hinterräder).