---
layout: "image"
title: "Vorderachse von unten (2)"
date: "2015-04-19T16:55:02"
picture: "pneumatikfederung22.jpg"
weight: "22"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/40831
imported:
- "2019"
_4images_image_id: "40831"
_4images_cat_id: "3067"
_4images_user_id: "104"
_4images_image_date: "2015-04-19T16:55:02"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40831 -->
Dies ist das rechte Vorderrad von vorne unten fotografiert. Der Zylinder hier ist wiederum der für die andere Seite, nämlich für das rechte Rad.