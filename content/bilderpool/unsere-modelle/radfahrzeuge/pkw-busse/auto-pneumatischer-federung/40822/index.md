---
layout: "image"
title: "Hinterrad in Sollhöhe"
date: "2015-04-19T16:55:02"
picture: "pneumatikfederung13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/40822
imported:
- "2019"
_4images_image_id: "40822"
_4images_cat_id: "3067"
_4images_user_id: "104"
_4images_image_date: "2015-04-19T16:55:02"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40822 -->
Und hier das Hinterrad mit Soll-Bodenfreiheit bei eingeschaltetem, stehendem Fahrzeug.