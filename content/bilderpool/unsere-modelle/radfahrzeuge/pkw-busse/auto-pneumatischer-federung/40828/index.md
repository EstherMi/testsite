---
layout: "image"
title: "Vorderachse von oben"
date: "2015-04-19T16:55:02"
picture: "pneumatikfederung19.jpg"
weight: "19"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/40828
imported:
- "2019"
_4images_image_id: "40828"
_4images_cat_id: "3067"
_4images_user_id: "104"
_4images_image_date: "2015-04-19T16:55:02"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40828 -->
Dies ist die Vorderachse von oben fotografiert. Die beiden Querlenkergruppen bestehen aus je zwei Trägern, bestehend aus einem Gelenkstein 15, einem BS15 und einem Kardan-Gelenk. Letzteres kann also sowohl einfedern als auch das Rad lenken.

Oben sieht man eine Mimik aus via Klemmkontakt elektrisch angeschlossener Metallachse 30 und Federkontakt (beides aus dem älteren ft-Programm) als Sensorik für den Niveauausgleich.