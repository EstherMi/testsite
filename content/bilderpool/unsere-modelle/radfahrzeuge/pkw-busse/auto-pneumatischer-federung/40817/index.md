---
layout: "image"
title: "Blick ins Innere"
date: "2015-04-19T16:55:02"
picture: "pneumatikfederung08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/40817
imported:
- "2019"
_4images_image_id: "40817"
_4images_cat_id: "3067"
_4images_user_id: "104"
_4images_image_date: "2015-04-19T16:55:02"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40817 -->
Das Dach ist mit wenigen Handgriffen komplett nach hinten ablegbar, und die Motorhaube kann leicht nach vorne geöffnet werden. Das gibt den Blick auf den Technik-Haufen darin frei.

Die Vorderachse verfügt über Einzelradaufhängung an Querlenkern, Servo-Lenkung und an beiden Rädern unabhängige pneumatische Federung. Sie baut trotzdem niedriger als der Durchmesser der Reifen.

Die Hinterräder sind ebenfalls einzeln aufgehängt, und zwar wie bei vielen hydropneumatisch gefederten Citroen an Längslenkern. Sie werden einzeln gefedert, die Federung hier teilt sich aber eine zentrale Druckleitung. Damit wird gewährleistet, dass das Fahrzeug auch bei Stand auf unebenem Grund immer mit allen Rädern vollen Bodenkontakt hat: Die Seitenneigung wird an der Vorderachse verhindert (beide Räder federn dort unabhängig voneinander auf die eingestellte Bodenfreiheit ein bzw. aus). Die Hinterachse pumpt sich immer so weit hoch, dass beide Räder mindestens den eingestellten Abstand zum Grund haben, eines davon aber weiter ausgefedert sein könnte.