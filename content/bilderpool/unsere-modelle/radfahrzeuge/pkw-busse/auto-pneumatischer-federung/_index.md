---
layout: "overview"
title: "Auto mit pneumatischer Federung und automatischem Niveauausgleich"
date: 2019-12-17T18:46:15+01:00
legacy_id:
- categories/3067
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3067 --> 
Dieses Fahrzeug verfügt über eine recht weiche Federung, die die Bodenfreiheit unabhängig vom Beladungszustand auf Bruchteile von Millimetern genau einhält. Es war auf der Convention 2014 dabei.