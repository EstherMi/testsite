---
layout: "image"
title: "Aufbau der Vorderachse (2)"
date: "2015-04-19T16:55:02"
picture: "pneumatikfederung24.jpg"
weight: "24"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/40833
imported:
- "2019"
_4images_image_id: "40833"
_4images_cat_id: "3067"
_4images_user_id: "104"
_4images_image_date: "2015-04-19T16:55:02"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40833 -->
Noch etwas weiter zerlegt, sieht man die Radaufhängung und die Kabeldurchführung zum IR-Empfänger.