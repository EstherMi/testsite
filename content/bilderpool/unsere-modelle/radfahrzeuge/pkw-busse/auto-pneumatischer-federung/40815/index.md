---
layout: "image"
title: "Wirkungsweise der Federung (1)"
date: "2015-04-19T16:55:02"
picture: "pneumatikfederung06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/40815
imported:
- "2019"
_4images_image_id: "40815"
_4images_cat_id: "3067"
_4images_user_id: "104"
_4images_image_date: "2015-04-19T16:55:02"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40815 -->
Hier sieht man das Fahrzeug ausgeschaltet von der Seite. Die Federung ist drucklos. Alle Räder sind bis zum Anschlag eingefedert. Das Fahrzeug setzt fast auf dem Boden auf.