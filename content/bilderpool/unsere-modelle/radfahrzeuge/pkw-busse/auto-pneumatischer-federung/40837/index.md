---
layout: "image"
title: "Antrieb"
date: "2015-04-19T16:55:02"
picture: "pneumatikfederung28.jpg"
weight: "28"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/40837
imported:
- "2019"
_4images_image_id: "40837"
_4images_cat_id: "3067"
_4images_user_id: "104"
_4images_image_date: "2015-04-19T16:55:02"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40837 -->
Der PowerMotor treibt also über Z30 und Schnecke das Differential an. Auf seinen Achsen sitzen Z20, die über je zwei weitere Rast-Z10 letztlich die Räder antreiben. Die Differentialachsen werden also zweifach genutzt: a) Als Lagerung für die Längslenker und b) als Antrieb.

Die Streben unten sind wichtig, damit sich das Heck beim Fahren nicht auseinanderdrückt und die Schnecke also immer sauber mit dem Differential in Eingriff steht. Zwischen PowerMotor und PowerMotor-Ritzel wurde eine ältere rote ft-Leuchtkappe mit Bohrung als Abstandshalter eingebaut, damit alles zur Position des Z30 passt. Die Bohrung der Leuchtkappe ist gerade groß genug fürs PowerMot-Ritzel.