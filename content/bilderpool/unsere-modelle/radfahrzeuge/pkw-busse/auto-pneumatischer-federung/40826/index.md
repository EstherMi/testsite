---
layout: "image"
title: "Einbau der Magnetventile und Drosseln"
date: "2015-04-19T16:55:02"
picture: "pneumatikfederung17.jpg"
weight: "17"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/40826
imported:
- "2019"
_4images_image_id: "40826"
_4images_cat_id: "3067"
_4images_user_id: "104"
_4images_image_date: "2015-04-19T16:55:02"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40826 -->
Von der Seite kommt man leicht durch Abnehmen einer Verkleidungsplatte an die Drossel des jeweiligen Vorderrades. Die muss man dann mit einem Schraubendreher und sehr viel Fingerspitzengefühl justieren. Etwas weiter vorne (links im Bild) ist das Magnetventil eingeklemmt. Dasselbe gibt's auf der anderen Fahrzeugseite sowie für die Hinterachse von oben erreichbar.

Da man rechts den Zylinder für das linke Hinterrad sehen kann, ist dies also die linke Fahrzeugseite.