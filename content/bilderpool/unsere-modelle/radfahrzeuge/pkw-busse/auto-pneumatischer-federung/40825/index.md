---
layout: "image"
title: "Fahrzeugboden"
date: "2015-04-19T16:55:02"
picture: "pneumatikfederung16.jpg"
weight: "16"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/40825
imported:
- "2019"
_4images_image_id: "40825"
_4images_cat_id: "3067"
_4images_user_id: "104"
_4images_image_date: "2015-04-19T16:55:02"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40825 -->
Man erkennt - von links nach rechts - IR-Empfänger und auf der anderen Fahrzeugseite den Lenkservo, Spurstange (gelbe Strebe), Vorderachse, Akku, Antriebsmotor unterhalb Kompressor und sonstiger Technik, Differential und Antrieb der Hinterräder (damit ist's also doch kein Citroen).

Die Vorderachsaufhängung befindet sich an einem durch Verbinder 15 zusammengehaltenen Strang von querliegenden BS15 mit zwei Zapfen. Das ergibt eine größtmögliche Verwindungssteifheit.