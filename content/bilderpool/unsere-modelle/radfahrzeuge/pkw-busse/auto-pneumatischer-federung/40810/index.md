---
layout: "image"
title: "Gesamtansicht (1)"
date: "2015-04-19T16:55:02"
picture: "pneumatikfederung01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/40810
imported:
- "2019"
_4images_image_id: "40810"
_4images_cat_id: "3067"
_4images_user_id: "104"
_4images_image_date: "2015-04-19T16:55:02"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40810 -->
Dieses Fahrzeug verfügt über eine recht weiche Federung, die die Bodenfreiheit unabhängig vom Beladungszustand auf Bruchteile von Millimetern genau einhält. Die Form ergab nach künstlerischer Freiheit ;-) sowas ähnliches wie einen Citroen C6. Naja.