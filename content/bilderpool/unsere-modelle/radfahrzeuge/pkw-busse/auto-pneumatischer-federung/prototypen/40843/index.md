---
layout: "image"
title: "Fahrzeug mit Pneumatik-Tanks"
date: "2015-04-20T15:15:14"
picture: "prototypen6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/40843
imported:
- "2019"
_4images_image_id: "40843"
_4images_cat_id: "3068"
_4images_user_id: "104"
_4images_image_date: "2015-04-20T15:15:14"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40843 -->
Hier ist das Fahrzeug praktisch fertig aufgebaut. Der ganze Innenraum wird von drei Pneumatik-Tanks ausgefüllt, die eine Federung so weich wie Sahne (echt) ergeben. Das war an sich total grandios, aber leider genügt dann ein einzelner ft-Kompressor nicht mehr, weil durch Undichtigkeiten und die superfeine Regelung mit häufigem Ventil-Schalten einfach zu viel Druckluft verloren geht. Vielleicht baue ich das Auto nochmal etwas größer und mit zwei Kompressoren.