---
layout: "image"
title: "Kontakte für die Niveauregulierung"
date: "2015-04-20T15:15:14"
picture: "prototypen5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/40842
imported:
- "2019"
_4images_image_id: "40842"
_4images_cat_id: "3068"
_4images_user_id: "104"
_4images_image_date: "2015-04-20T15:15:14"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40842 -->
Die erste Version der Anbringung der Kontakte für die Niveauregulierung war auch noch zu klobig.