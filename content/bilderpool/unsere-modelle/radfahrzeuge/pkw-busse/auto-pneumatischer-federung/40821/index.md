---
layout: "image"
title: "Hinterrad ganz eingefedert"
date: "2015-04-19T16:55:02"
picture: "pneumatikfederung12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/40821
imported:
- "2019"
_4images_image_id: "40821"
_4images_cat_id: "3067"
_4images_user_id: "104"
_4images_image_date: "2015-04-19T16:55:02"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40821 -->
Das linke Hinterrad bei ausgeschaltetem Auto - bis zum Anschlag eingefedert.