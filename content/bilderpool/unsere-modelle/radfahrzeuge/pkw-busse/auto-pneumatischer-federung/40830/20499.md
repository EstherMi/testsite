---
layout: "comment"
hidden: true
title: "20499"
date: "2015-04-20T07:25:41"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Absolut klasse, wie die Vorderräder aufgehängt sind. Die Achsschwinge (heißt hoffentlich so) so zu lagern und gleichzeitig noch die Lenkbewegung da rauszuholen - Mann oh Mann, meisterlich...

Chapeau
H.A.R.R.Y.