---
layout: "image"
title: "Vorderachse von unten (1)"
date: "2015-04-19T16:55:02"
picture: "pneumatikfederung21.jpg"
weight: "21"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/40830
imported:
- "2019"
_4images_image_id: "40830"
_4images_cat_id: "3067"
_4images_user_id: "104"
_4images_image_date: "2015-04-19T16:55:02"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40830 -->
Wir blicken hier von vorne unten in die Aufhängung des linken Vorderrades. Man beachte, dass der dahinter sichtbare Zylinder derjenige für das rechte Rad ist, also auf die andere Fahrzeugseite wirkt.