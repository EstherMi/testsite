---
layout: "image"
title: "Schräg vorn"
date: "2017-06-18T18:00:36"
picture: "ateamreloaded01.jpg"
weight: "1"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/45952
imported:
- "2019"
_4images_image_id: "45952"
_4images_cat_id: "3415"
_4images_user_id: "4"
_4images_image_date: "2017-06-18T18:00:36"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45952 -->
Das A-Team ist zeitgemäß auf einen Fronttriebler umgestiegen.