---
layout: "image"
title: "Innereien"
date: "2017-06-18T18:00:37"
picture: "ateamreloaded09.jpg"
weight: "9"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/45960
imported:
- "2019"
_4images_image_id: "45960"
_4images_cat_id: "3415"
_4images_user_id: "4"
_4images_image_date: "2017-06-18T18:00:37"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45960 -->
Die Akku-Halter sind von unten mittels S-Riegel auf dem Längsträger befestigt und drehen sich leicht weg. Das macht den Einbau zu einem ziemlichen Gefummel. Der Motor sitzt unter dem Differenzial (Details im nächsten Bild)