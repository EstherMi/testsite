---
layout: "image"
title: "Hinterachse"
date: "2017-06-18T18:00:36"
picture: "ateamreloaded04.jpg"
weight: "4"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/45955
imported:
- "2019"
_4images_image_id: "45955"
_4images_cat_id: "3415"
_4images_user_id: "4"
_4images_image_date: "2017-06-18T18:00:36"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45955 -->
Die ft-Lenkklauen sind hier hochkant gestellt und dienen als Schwinge. Die Streben I-45 ergänzen sie zu einem Trapez. Die vier ft-Federn waren noch zu schwach, deswegen gibt es einen Gummizug als zusätzliche Feder.