---
layout: "image"
title: "oldtimer05.jpg"
date: "2017-03-12T13:48:38"
picture: "oldtimer05.jpg"
weight: "5"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/45498
imported:
- "2019"
_4images_image_id: "45498"
_4images_cat_id: "3382"
_4images_user_id: "2449"
_4images_image_date: "2017-03-12T13:48:38"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45498 -->
