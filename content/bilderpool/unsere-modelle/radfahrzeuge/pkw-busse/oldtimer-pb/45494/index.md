---
layout: "image"
title: "oldtimer01.jpg"
date: "2017-03-12T13:48:38"
picture: "oldtimer01.jpg"
weight: "1"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/45494
imported:
- "2019"
_4images_image_id: "45494"
_4images_cat_id: "3382"
_4images_user_id: "2449"
_4images_image_date: "2017-03-12T13:48:38"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45494 -->
