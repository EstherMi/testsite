---
layout: "overview"
title: "Oldtimer (PB)"
date: 2019-12-17T18:46:24+01:00
legacy_id:
- categories/3382
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3382 --> 
Ein alteres Model, das damals auch in Niederländischen 'FT-clubblad' (FT-clubmagazin) erschienen ist. Es ist ferngesteuert (mit den älteren IR-set) und gefedert. Die großen Räder der Dampfmaschine gaben mir den Idee hierfür. Die alten Raupen-gummi's (??) passten gut da herum, damit den Auto 'Grip', Traktion, hat.