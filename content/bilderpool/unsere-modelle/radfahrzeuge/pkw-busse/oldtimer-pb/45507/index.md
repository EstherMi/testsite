---
layout: "image"
title: "oldtimer14.jpg"
date: "2017-03-12T13:48:50"
picture: "oldtimer14.jpg"
weight: "14"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/45507
imported:
- "2019"
_4images_image_id: "45507"
_4images_cat_id: "3382"
_4images_user_id: "2449"
_4images_image_date: "2017-03-12T13:48:50"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45507 -->
