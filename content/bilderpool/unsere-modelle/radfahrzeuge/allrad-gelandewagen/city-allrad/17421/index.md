---
layout: "image"
title: "City-Allrad 6"
date: "2009-02-15T18:22:38"
picture: "City-Allrad_03.jpg"
weight: "6"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/17421
imported:
- "2019"
_4images_image_id: "17421"
_4images_cat_id: "1567"
_4images_user_id: "328"
_4images_image_date: "2009-02-15T18:22:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17421 -->
Hier ist die Allradlenkung gut zu sehen. Die Hinterräder schlagen entgegengesetzt zu den Vorderrädern mit halbem Lenkwinkel ein.