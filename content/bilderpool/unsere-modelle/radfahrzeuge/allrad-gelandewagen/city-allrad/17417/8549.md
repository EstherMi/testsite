---
layout: "comment"
hidden: true
title: "8549"
date: "2009-02-19T16:27:47"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Charmeur! Der Herr belieben zu scherzen -- gerade als ob DU eine Muse zur Anregung bräuchtest, hahaha!

Die Fotos sind schon gewaltig besser! (Wobei mir persönlich der direkte Sonnenstrahl auf dem Modell am besten gefällt. Das ist aber ein Wackelspiel und braucht viele Versuche, weil man im Gegenlicht den Blitz zum Aufhellen braucht, und ruckzuck dann das Ganze überbelichtet ist).

Gruß,
Harald