---
layout: "image"
title: "City-Allrad 9"
date: "2009-02-15T18:22:38"
picture: "City-Allrad_12.jpg"
weight: "9"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/17424
imported:
- "2019"
_4images_image_id: "17424"
_4images_cat_id: "1567"
_4images_user_id: "328"
_4images_image_date: "2009-02-15T18:22:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17424 -->
Der Hebel in der Mitte sorgt für die Untersetzung des Lenkwinkels. Sehr einfach, aber effektiv. Passend zum Leichtbau-Konzept des ganzen Fahrzeugs.