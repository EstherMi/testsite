---
layout: "comment"
hidden: true
title: "13522"
date: "2011-02-11T07:43:18"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Hallo Dirk!

Das ist richtig, dass die Räder nicht auf dem selben Spurkreis liegen. Ganz normal bei unterschiedlichen Lenkwinkeln vorn und hinten. THEORETISCH hast Du Recht, dass es zu Schlupf kommen sollte, wenn alle 4 Räder gleichartig angetrieben werden. Aber wer diesen Antreib mal nachbaut, wird feststellen, dass sich das Fahrzeug trotz 4 Motoren nur sehr langsam in Bewegung setzt und auf Tempo kommt. Auf die Motoren wirkt eine hohe Last, die die Bewegung bremst. Bei Kurvenfahrt ist diese Last je Motor leicht unteschiedlich, und die Drehzahlunterschiede werden ausgeglichen.

Wir hatten im alten Forum mal eine großartige Diskussion zu diesem Thema (RIP). Mein Lieblingssatz (nicht von mir) war: "Jeder Motor gibt, was er kann." Das heißt, die Last, die auf jeden Motor wirkt, bremst diesen individuell. Daher ist dieser Antrieb mit mehreren Motoren sogar technisch absolut "sauber" und realistsich. Bei echten Fahrzeugen macht man es nicht anders.

Gruß, Thomas