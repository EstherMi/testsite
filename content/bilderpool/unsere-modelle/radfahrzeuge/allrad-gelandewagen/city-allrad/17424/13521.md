---
layout: "comment"
hidden: true
title: "13521"
date: "2011-02-10T23:29:05"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hallo Thomas,
Deine Allradlenkung mit "mitlenkenden Motoren" ist wirklich eine originelle Idee! Auf dem Foto sieht es so aus, als ob die Räder nicht exakt auf demselben Spurkreis liegen, da Vorder- und Hinterrad unterschiedlich stark einschlagen. Stimmt das? Falls ja: Führt das zu Drift in der Kurve? Und kennst Du die Ursache für den unterschiedlichen Einschlagwinkel?
Gruß, Dirk