---
layout: "comment"
hidden: true
title: "13141"
date: "2011-01-10T07:50:26"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Der erfahrene FT-Bauer weiß, dass keine Nabenmutter mehr auf die Nabe passt, wenn sie auf die Schnecke eines Mini-Motors geklemmt ist. Die Schnecke ist eigentlich zu dick für eine "normale" FT-Klemmbefestigung. Aber ohne Nabenmutter hält es auch bombenfest.

Gruß, Thomas