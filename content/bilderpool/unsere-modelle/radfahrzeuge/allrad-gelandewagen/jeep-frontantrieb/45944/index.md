---
layout: "image"
title: "Totale von oben"
date: "2017-06-18T11:55:32"
picture: "jeepfa2.jpg"
weight: "3"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/45944
imported:
- "2019"
_4images_image_id: "45944"
_4images_cat_id: "3414"
_4images_user_id: "4"
_4images_image_date: "2017-06-18T11:55:32"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45944 -->
Der U-Träger in der Mitte trägt die Hauptlast, verwindet sich aber leicht. Deshalb sind außen noch Schweller hinzu gekommen.