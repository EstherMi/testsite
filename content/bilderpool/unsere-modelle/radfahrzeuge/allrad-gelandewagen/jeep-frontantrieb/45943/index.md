---
layout: "image"
title: "Totale 1"
date: "2017-06-18T11:55:32"
picture: "jeepfa1.jpg"
weight: "2"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/45943
imported:
- "2019"
_4images_image_id: "45943"
_4images_cat_id: "3414"
_4images_user_id: "4"
_4images_image_date: "2017-06-18T11:55:32"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45943 -->
An dem Auto ist wirklich nicht viel dran - dank Frontantrieb kann die ganze Technik vorne bleiben und hinten bleiben alle Möglichkeiten offen.