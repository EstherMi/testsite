---
layout: "image"
title: "von vorn, ohne Anbauteile"
date: "2017-06-18T11:55:33"
picture: "jeepfa8.jpg"
weight: "9"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/45950
imported:
- "2019"
_4images_image_id: "45950"
_4images_cat_id: "3414"
_4images_user_id: "4"
_4images_image_date: "2017-06-18T11:55:33"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45950 -->
Unterm Motor ist so viel Platz, dass er gerade nochmal da hin könnte, wenn nur nicht der Längsträger da wäre. 

So schief stehen die Teile jetzt nur, weil das Auto etwas verspannt hingestellt wurde.