---
layout: "image"
title: "Jeep2-07.JPG"
date: "2005-08-12T13:29:07"
picture: "Jeep2-07.jpg"
weight: "7"
konstrukteure: 
- "harald steinhaus"
fotografen:
- "harald steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4553
imported:
- "2019"
_4images_image_id: "4553"
_4images_cat_id: "370"
_4images_user_id: "4"
_4images_image_date: "2005-08-12T13:29:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4553 -->
Die Hinterachse ist unverändert, aber die Längsträger sind jetzt weiter auseinander, um Platz für den Akku zu machen. Mein ft-Akku ist leider nicht auffindbar, deshalb ist hier ein Dummy eingebaut.