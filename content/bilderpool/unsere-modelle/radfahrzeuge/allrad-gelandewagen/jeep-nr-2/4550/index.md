---
layout: "image"
title: "Jeep2-04.JPG"
date: "2005-08-12T13:29:07"
picture: "Jeep2-04.jpg"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4550
imported:
- "2019"
_4images_image_id: "4550"
_4images_cat_id: "370"
_4images_user_id: "4"
_4images_image_date: "2005-08-12T13:29:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4550 -->
Der Lichtschalter ist eingebaut, aber das Verkabeln habe ich mir gespart.