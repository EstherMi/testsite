---
layout: "image"
title: "Jeep2-02.JPG"
date: "2005-08-12T13:29:07"
picture: "Jeep2-02.jpg"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4548
imported:
- "2019"
_4images_image_id: "4548"
_4images_cat_id: "370"
_4images_user_id: "4"
_4images_image_date: "2005-08-12T13:29:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4548 -->
Das bullige Aussehen ist aber geblieben :-)