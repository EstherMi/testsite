---
layout: "comment"
hidden: true
title: "6259"
date: "2008-04-19T15:36:58"
uploadBy:
- "Sebo"
license: "unknown"
imported:
- "2019"
---
Hi
Ich bin zwar nicht der Konstruckteur, stelle aber trotzdem eine Vermutung auf:
Da es sich um ein Amphibienfahrzeug handelt und der Propeller bestimmt nicht als Antrieb zum Fahren auf Land gedacht ist, muss sich der Motor oberhalb der Wasseroberfläche befinden, damit es zu keinem Kurzschluss kommt...simpel, oder?

Außerdem schätze ich, dass:
1. Die seitlich angebrachten Propreller nicht vollständig in das Wasser ragen.

2. Die Propeller viel zu groß sind, um unter Wasser ohne Untersetzung an einem Minimotor betrieben werden zu können. Für meine bis jetzt gebauten Boote mit der Box 1000 habe ich immer wie du hinten einen Propeller mit vier Blättern verwendet und diesen ohne Untesetzung an einen 8:1 Moter angeschlossen.

3. Aufgrund der zu großen Propeller und der geringen Kraft des Minimotors wird der Haushaltsgummi vermutlich nicht durchrutschen.

Das wars soweit von mir...
MfG Sebastian