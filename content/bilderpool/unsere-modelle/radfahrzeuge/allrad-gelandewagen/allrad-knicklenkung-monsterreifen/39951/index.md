---
layout: "image"
title: "CAD-Modell"
date: "2014-12-22T22:09:08"
picture: "CAD_Modell_3_DxO.jpg"
weight: "1"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/39951
imported:
- "2019"
_4images_image_id: "39951"
_4images_cat_id: "2999"
_4images_user_id: "502"
_4images_image_date: "2014-12-22T22:09:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39951 -->
