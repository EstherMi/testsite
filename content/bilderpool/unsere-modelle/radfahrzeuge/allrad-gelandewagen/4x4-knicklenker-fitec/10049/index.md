---
layout: "image"
title: "4x4"
date: "2007-04-11T09:59:11"
picture: "4x43.jpg"
weight: "3"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/10049
imported:
- "2019"
_4images_image_id: "10049"
_4images_cat_id: "908"
_4images_user_id: "456"
_4images_image_date: "2007-04-11T09:59:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10049 -->
Hier habe ich mal den IR-Empfänger abgenommen. So sieht man gut den Antrieb. Das Mitteldiff und somit der Antriebsmotor sitzen vorne wie bei jedem Auto. Ich hätte ja noch eine Motorhaube bauen können hihi. Die Übersetzung 2:3 ist sehr gut. Das Auto fährt recht schnell, ist aber durch den 50:1 Motor trotzdem stark.