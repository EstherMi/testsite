---
layout: "image"
title: "4x4"
date: "2007-04-11T09:59:11"
picture: "4x45.jpg"
weight: "5"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/10051
imported:
- "2019"
_4images_image_id: "10051"
_4images_cat_id: "908"
_4images_user_id: "456"
_4images_image_date: "2007-04-11T09:59:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10051 -->
Hier sieht man wie gut die Pendelachse funktioniert.