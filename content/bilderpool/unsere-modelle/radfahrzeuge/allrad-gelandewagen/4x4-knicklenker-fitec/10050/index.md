---
layout: "image"
title: "4x4"
date: "2007-04-11T09:59:11"
picture: "4x44.jpg"
weight: "4"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/10050
imported:
- "2019"
_4images_image_id: "10050"
_4images_cat_id: "908"
_4images_user_id: "456"
_4images_image_date: "2007-04-11T09:59:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10050 -->
Hier sieht man gut die Lenkung. Das die Lenkung direkt auf dem Akku ist täuscht. sonst könnte es ja nicht pendeln.