---
layout: "image"
title: "Ein schöner Rücken..."
date: "2017-07-12T23:40:59"
picture: "suvx03.jpg"
weight: "3"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/46086
imported:
- "2019"
_4images_image_id: "46086"
_4images_cat_id: "3422"
_4images_user_id: "4"
_4images_image_date: "2017-07-12T23:40:59"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46086 -->
Die Heckklappe reicht bis zu den schwarzen Streben herunter.