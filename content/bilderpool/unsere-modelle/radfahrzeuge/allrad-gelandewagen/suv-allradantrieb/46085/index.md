---
layout: "image"
title: "Draufsicht"
date: "2017-07-12T23:40:59"
picture: "suvx02.jpg"
weight: "2"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/46085
imported:
- "2019"
_4images_image_id: "46085"
_4images_cat_id: "3422"
_4images_user_id: "4"
_4images_image_date: "2017-07-12T23:40:59"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46085 -->
Motor vorn und Akku hinten sorgen dafür, dass beide Achsen hinreichend Last bekommen und die Räder nicht durchdrehen.