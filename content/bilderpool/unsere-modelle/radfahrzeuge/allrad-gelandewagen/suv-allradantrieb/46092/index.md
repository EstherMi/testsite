---
layout: "image"
title: "Lenkung"
date: "2017-07-12T23:40:59"
picture: "suvx09.jpg"
weight: "9"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/46092
imported:
- "2019"
_4images_image_id: "46092"
_4images_cat_id: "3422"
_4images_user_id: "4"
_4images_image_date: "2017-07-12T23:40:59"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46092 -->
Die Vorderräder sind hier schon umgerüstet auf die Modding-freie Variante.