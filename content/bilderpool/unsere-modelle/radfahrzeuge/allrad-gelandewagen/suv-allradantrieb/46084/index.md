---
layout: "image"
title: "SUV, Schokoladenseite"
date: "2017-07-12T23:40:58"
picture: "suvx01.jpg"
weight: "1"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/46084
imported:
- "2019"
_4images_image_id: "46084"
_4images_cat_id: "3422"
_4images_user_id: "4"
_4images_image_date: "2017-07-12T23:40:58"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46084 -->
Die Leuchtsteine sind noch Dummies, und die Front könnte etwas aufgepeppt werden. Die überlange Motorhaube sieht man von hier nicht so.

Die Technik der Vorderachse ist schon bekannt von den Fronttrieblern A-Team realoaded https://ftcommunity.de/categories.php?cat_id=3415 und Jeep mit Frontantrieb https://ftcommunity.de/categories.php?cat_id=3414 . Die Hinterachse muss vor allem das gleiche Übersetzungsverhältnis wie die Vorderachse erreichen, damit beide Achsen gleichermaßen zum Antrieb beitragen.
Für den Antriebsblock im Detail verweise ich auch auf den Jeep: https://ftcommunity.de/details.php?image_id=45961