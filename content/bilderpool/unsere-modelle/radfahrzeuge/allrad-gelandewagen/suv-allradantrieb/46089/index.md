---
layout: "image"
title: "Unterseite"
date: "2017-07-12T23:40:59"
picture: "suvx06.jpg"
weight: "6"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/46089
imported:
- "2019"
_4images_image_id: "46089"
_4images_cat_id: "3422"
_4images_user_id: "4"
_4images_image_date: "2017-07-12T23:40:59"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46089 -->
(hier noch mit Modding-Teilen in der vorderen Radlagerung)