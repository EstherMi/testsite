---
layout: "image"
title: "Motorraum"
date: "2017-07-12T23:40:59"
picture: "suvx05.jpg"
weight: "5"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/46088
imported:
- "2019"
_4images_image_id: "46088"
_4images_cat_id: "3422"
_4images_user_id: "4"
_4images_image_date: "2017-07-12T23:40:59"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46088 -->
Da ist reichlich Platz drin (mit einem guten Grund). Dumm ist nur, dass der Antrieb genau mitten drin sitzt und alle "schönen Sachen" wie Einbau eines Robo-Tx verhindert. Der Antriebsblock ist schon bekannt: https://ftcommunity.de/details.php?image_id=45961