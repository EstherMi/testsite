---
layout: "image"
title: "Von Oben"
date: "2007-09-26T15:51:58"
picture: "Amphi3.jpg"
weight: "13"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/12020
imported:
- "2019"
_4images_image_id: "12020"
_4images_cat_id: "1062"
_4images_user_id: "456"
_4images_image_date: "2007-09-26T15:51:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12020 -->
Hier von oben. Man sieht schon das 2-Gang-Getriebe. Der Akku ist vorne, wegen dem Gewicht und man sieht auch einen Empfänger. Die Lenkung fehlt noch.