---
layout: "image"
title: "Wasserantrieb"
date: "2007-11-02T08:49:08"
picture: "Amphi15.jpg"
weight: "3"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/12374
imported:
- "2019"
_4images_image_id: "12374"
_4images_cat_id: "1062"
_4images_user_id: "456"
_4images_image_date: "2007-11-02T08:49:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12374 -->
Der Wasserantrieb ist jetzt sehr kompakt. Die Folie an den Motoren ist als Spritzwasserschutz verwendet.