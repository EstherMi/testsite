---
layout: "image"
title: "Amphi Explorer"
date: "2007-10-15T19:57:56"
picture: "Amphi9.jpg"
weight: "10"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/12233
imported:
- "2019"
_4images_image_id: "12233"
_4images_cat_id: "1062"
_4images_user_id: "456"
_4images_image_date: "2007-10-15T19:57:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12233 -->
Unterseite. Man sieht, dass ich Leichtbau gemacht habe. Es ist aber stabil genug.