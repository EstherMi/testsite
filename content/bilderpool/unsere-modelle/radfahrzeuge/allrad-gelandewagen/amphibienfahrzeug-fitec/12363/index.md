---
layout: "image"
title: "Federung"
date: "2007-10-28T12:45:57"
picture: "Amphi11.jpg"
weight: "5"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/12363
imported:
- "2019"
_4images_image_id: "12363"
_4images_cat_id: "1062"
_4images_user_id: "456"
_4images_image_date: "2007-10-28T12:45:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12363 -->
Die Federung. Sie ist noch nicht perfekt, weil das vorderste Zanhrad nicht gut federt. Die Stabilität ist nämlich vorne noch nicht gut genug, aber das lässt sich machen. :)