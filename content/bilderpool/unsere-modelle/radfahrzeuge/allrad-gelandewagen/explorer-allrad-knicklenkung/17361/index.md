---
layout: "image"
title: "ExplorerMk2-03.JPG"
date: "2009-02-11T18:24:16"
picture: "ExplorerMk2-03.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/17361
imported:
- "2019"
_4images_image_id: "17361"
_4images_cat_id: "1555"
_4images_user_id: "4"
_4images_image_date: "2009-02-11T18:24:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17361 -->
"Mk 2" ist auf dem Weg.

Ich frage mich, warum ich die Lenkung nicht gleich mittels Schnecke gebaut habe. Gut, ich hatte sie immer längs hingehalten und dann gesehen, dass der Lenkeinschlag nicht mehr geht. Aber auf den Quereinbau hätte ich früher kommen können.

Das Chassis ist etwas länger geworden, damit die Motoren hineinpassen. Beim Einsatz von Gelenkwürfeln anstelle der BS15-Loch könnten die Achsen um die Differenziale herum schwingen. Damit kann man das Fahrzeug federn und/oder die Bodenfreiheit variabel machen. Mal sehen.