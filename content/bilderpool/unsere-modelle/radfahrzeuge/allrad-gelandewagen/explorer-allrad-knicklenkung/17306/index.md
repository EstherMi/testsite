---
layout: "image"
title: "Explorer20.JPG"
date: "2009-02-04T16:00:49"
picture: "Explorer20.JPG"
weight: "12"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/17306
imported:
- "2019"
_4images_image_id: "17306"
_4images_cat_id: "1555"
_4images_user_id: "4"
_4images_image_date: "2009-02-04T16:00:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17306 -->
Im Heck gibt es noch genügend Stauraum für Expeditionsausrüstung.
Die Lenkung wird mit Schneckengetriebe auf das Z40 untersetzt. Die Ketten werden gleichzeitig angetrieben und teilen sich die Aufgabe, das Fahrzeug einzuknicken. Eine allein wäre überfordert und könnte zu Bruch gehen.