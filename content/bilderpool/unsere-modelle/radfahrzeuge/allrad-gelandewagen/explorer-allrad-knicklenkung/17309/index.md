---
layout: "image"
title: "Explorer26.JPG"
date: "2009-02-04T16:15:01"
picture: "Explorer26.JPG"
weight: "15"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/17309
imported:
- "2019"
_4images_image_id: "17309"
_4images_cat_id: "1555"
_4images_user_id: "4"
_4images_image_date: "2009-02-04T16:15:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17309 -->
Die Ketten der Lenkung sollen nicht über die Z10 drüber hüpfen, deswegen die Umschlingung um gut 270°. Sie verlaufen ober- und unterhalb der Antriebswelle quer durchs Fahrzeug und dann nach vorn um die liegenden Drehkränze herum. Linker und rechter Lenkantrieb sind über die Kette unter dem Fahrzeugboden verbunden.

In den Lenkklauen 35998 steckt je 1 Klemmbuchse 10, so wie das hier: http://www.ftcommunity.de/details.php?image_id=5593 schon mal beschrieben wurde.