---
layout: "image"
title: "Explorer21.JPG"
date: "2009-02-04T16:04:09"
picture: "Explorer21.JPG"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/17307
imported:
- "2019"
_4images_image_id: "17307"
_4images_cat_id: "1555"
_4images_user_id: "4"
_4images_image_date: "2009-02-04T16:04:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17307 -->
Der Fahrmotor sitzt über dem Mitteldifferenzial. Dazwischen kommt noch ein Z10, das hier nicht zu sehen ist.