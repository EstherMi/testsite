---
layout: "image"
title: "Explorer40.jpg"
date: "2009-02-05T20:17:55"
picture: "Explorer40.jpg"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/17313
imported:
- "2019"
_4images_image_id: "17313"
_4images_cat_id: "1555"
_4images_user_id: "4"
_4images_image_date: "2009-02-05T20:17:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17313 -->
