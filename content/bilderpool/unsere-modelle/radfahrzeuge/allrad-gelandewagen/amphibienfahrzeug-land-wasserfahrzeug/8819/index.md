---
layout: "image"
title: "Amphiebienfahrzeug Oben"
date: "2007-02-03T16:32:11"
picture: "Oben002.jpg"
weight: "10"
konstrukteure: 
- "Paul und Tobias"
fotografen:
- "Paul"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ROBO Paul"
license: "unknown"
legacy_id:
- details/8819
imported:
- "2019"
_4images_image_id: "8819"
_4images_cat_id: "803"
_4images_user_id: "459"
_4images_image_date: "2007-02-03T16:32:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8819 -->
Das Amphiebienfahrzeug von Oben:
In der mitte die Signallampen eines Schiffes, mit einem Tageslichtsensor. Weiter oben im Bild ein Ende des Robo Int. und noch weiter oben der antrieb (Powermotor).
Außen die Schaufelräder, und Colaflaschen als Schwimmer.