---
layout: "comment"
hidden: true
title: "2461"
date: "2007-02-22T13:16:27"
uploadBy:
- "remadus"
license: "unknown"
imported:
- "2019"
---
Sehr interessante Schiffsschraube.

Taucht die ganz oder nur teilweise ins Wasser? Vielleicht steigt ihre Wirksamkeit, wenn du die schrägen Platten weiter nach außen schiebst. Allerdings spritzt sie dann auch mehr.