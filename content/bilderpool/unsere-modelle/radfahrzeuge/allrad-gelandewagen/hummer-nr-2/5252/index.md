---
layout: "image"
title: "Hummer02.JPG"
date: "2005-11-07T19:01:29"
picture: "Hummer02.JPG"
weight: "2"
konstrukteure: 
- "Peter Damen"
fotografen:
- "Harald Steinhaus"
keywords: ["Hummer", "Allrad", "HMMWV", "Humvee", "Großreifen", "Monsterreifen"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5252
imported:
- "2019"
_4images_image_id: "5252"
_4images_cat_id: "450"
_4images_user_id: "4"
_4images_image_date: "2005-11-07T19:01:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5252 -->
Die Besatzung (typisch wären wohl vier Hillbillies mit Schrotflinte) ist gerade ein Bierchen trinken...