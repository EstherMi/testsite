---
layout: "image"
title: "Verseilung zum Ausfahren (1)"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran43.jpg"
weight: "43"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/33074
imported:
- "2019"
_4images_image_id: "33074"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "43"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33074 -->
Das Zugseil zum Ausfahren des Arms läuft außen ans obere Ende des äußeren (stationären) U-Träger-Segments über Umlenkrollen (rote große Seilrollen) und kommt dann hier an den von außen gezählt dritten U-Trägern an. Das ist das Seil, was nicht in den Seilrollen liegt, sondern direkt auf den Metallachsen und nur von den Klemmringen gehalten.

Anfangs hatte ich an den zweiten U-Trägern, also dem ersten ausfahrbaren Segment ziehen lassen. Dann muss nur für ein gleichmäßiges Ausfahren das nächste Segment doppelt und das innerste viermal so schnell ausgetrieben werden. Das ergibt so große Kräfte, dass der Zwirn riss. Der Trick ist nun, nicht am ersten, sondern am zweiten (mittleren ausfahrbaren) Segment zu ziehen. Damit der Arm auch waagerecht noch tragfähig ist, dürfen die Segmente ja nicht beliebig weit ausfahren. Glücklicherweise fährt das zweite Segment nur so weit aus, dass seine hier sichtbaren Seilrollen (die innersten beiden) gerade noch unterhalb dem Ende des äußeren Segments landen.

So genügt es also, an diesem Segment zu ziehen. Nur das innerste Segment muss jetzt noch 1:2 übersetzt schneller gezogen werden, während das von außen gezählt zweite 2:1 untersetzt langsamer einfach nur mitgezogen wird.

Die restlichen hier sichtbaren Schnüre verbinden immer Dreiergruppen von Segmenten so, dass das mittlere genau halb so schnell ausfährt wie das innere (bzw. das innere eben doppelt so schnell wie das mittlere). 

Die schwarzen Streben führen die inneren Segmente an den jeweils äußeren, sodass hier die Segmente nicht einfach herunterfallen, wenn der Arm z. B. waagerecht liegt.