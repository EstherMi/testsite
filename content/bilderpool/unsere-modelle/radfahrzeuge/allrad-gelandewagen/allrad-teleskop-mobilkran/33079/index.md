---
layout: "image"
title: "Verseilung zum Einfahren (3)"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran48.jpg"
weight: "48"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/33079
imported:
- "2019"
_4images_image_id: "33079"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "48"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33079 -->
Am Ende wieder die Verbindung der zweitinnersten Segmente mit den flexiblen Prüfriegeln. Bis auf die auf der Oberseite nicht existenten Umlenkrollen für das Zugseil ist das ansonsten wie auf der Unterseite aufgebaut.