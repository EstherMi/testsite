---
layout: "image"
title: "Hinterradfederung (2)"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran21.jpg"
weight: "21"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/33052
imported:
- "2019"
_4images_image_id: "33052"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33052 -->
Je Rad sind zwei ft-Federfüße mit einem festen und einem beweglichen BS30 verbunden. Letzterer drückt auf den Gelenkstein des jeweiligen Längslenkers. Ein weiterer Federfuß drückt von oben auf den beweglichen BS30, sonst würde das Fahrwerk das Gesamtgewicht mit dem Kranarm zusammen nicht tragen (bzw. immer am Anschlag eingefedert sein).