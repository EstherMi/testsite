---
layout: "image"
title: "Gesamtansicht ausgefahren"
date: "2011-10-03T22:46:37"
picture: "allradteleskopmobilkran04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/33035
imported:
- "2019"
_4images_image_id: "33035"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:37"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33035 -->
Auf diesem Bild sind...

- die Blinklampen eingeschaltet (sieht man aber wegen der Aufnahme im Sonnenlicht kaum),
- die vier Stützen ausgefahren,
- der Kranarm komplett aufgestellt und
- komplett ausgefahren sowie
- die Plattform ca. 70° nach hinten gedreht.