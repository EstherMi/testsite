---
layout: "image"
title: "Verseilung zum Ausfahren (3)"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran45.jpg"
weight: "45"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/33076
imported:
- "2019"
_4images_image_id: "33076"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "45"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33076 -->
Schließlich kommen wir am oberen Ende der Armunterseite an. Hier sieht man die vorher erwähnten großen roten Seilrollen, die das von außen kommende eigentliche Zugseil zum Ausfahren zum unteren Ende des zweitinnersten Segments führen. An dem wird dann zum Ausfahren aller Segmente einfach gezogen.

Die I-Streben 30, die das zweitinnerste Segment zusammenhalten, sind mit Statik-Prüfriegeln befestigt. Die stammen vom hobby-S aus den 1970er Jahren und bestehen aus einem halbtransparenten relativ weichen Material. Das ist hier auch schwer notwendig, denn mit normalen S-Riegeln würde das innerste Segment so stramm eingespannt sein, dass die auftretenden Kräfte wieder zu groß werden.