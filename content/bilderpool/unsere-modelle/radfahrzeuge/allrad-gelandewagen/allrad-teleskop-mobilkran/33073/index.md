---
layout: "image"
title: "Lagerung und Endlagentaster"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran42.jpg"
weight: "42"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/33073
imported:
- "2019"
_4images_image_id: "33073"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33073 -->
Der Arm wird an nur drei Gelenkbausteinen gehalten. Der Graue Taster ist der Endlagentaster für die waagerechte Lage des Arms, der schwarze rechts über der Bildmitte der vom vorherigen Bild, der direkt von einer der Seilrollen des Arms betätigt wird, wenn dieser ganz eingefahren wird.