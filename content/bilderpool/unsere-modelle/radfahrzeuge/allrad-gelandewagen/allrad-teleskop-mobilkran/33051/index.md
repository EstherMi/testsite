---
layout: "image"
title: "Hinterradaufhängung"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran20.jpg"
weight: "20"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/33051
imported:
- "2019"
_4images_image_id: "33051"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33051 -->
Die Achsen der Differentiale und Z10 sind gleichzeitig die, um die die nach vorne zeigenden und mit den alten ft-Gelenkbausteinen (45*15*15) gebauten Längslenker pendelnd ein- und ausfedern können. Die Kombination der 15*30-Teile unter den Längslenkern dient dazu, das Ausfedern zu begrenzen. Ansonsten könnten sich die Längslenker beim Fahren über Hindernisse "aufstellen" wie Stelzen.