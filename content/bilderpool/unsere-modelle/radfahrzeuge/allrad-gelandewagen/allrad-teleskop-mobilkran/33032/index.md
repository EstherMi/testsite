---
layout: "image"
title: "Gesamtansicht fahrbereit"
date: "2011-10-03T22:46:37"
picture: "allradteleskopmobilkran01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/33032
imported:
- "2019"
_4images_image_id: "33032"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33032 -->
Um diesen Kranwagen geht es, und Folgendes ist alles "drin":

Fahrwerk:
- Allradantrieb, auch vorne kräftig
- Einzelradaufhängung aller Räder
- Federung sämtlicher Räder
- Lenkung incl. Endlagenabschaltung
- Vier ausfahrbare Stützen, die das Fahrzeug tatsächlich anheben können, incl. Endlagenabschaltung

Kranaufbau:
- Aufstellbarer Kranarm, von weniger als waagerecht (also sogar etwas nach unten zeigend) bis fast senkrecht incl. Endlagenabschaltung
- Drehbare Plattform, mehrfach 360° drehbar
- Dreifach ausfahrbarer Teleskop-Kranarm incl. Endlagenabschaltung - das Ausfahren geschieht auf allen drei Segmenten gleichzeitig und gleichmäßig
- Man muss fast kein Seil für den Kranhaken nachgeben, wenn der Arm ausfährt
- Doppelter Flaschenzug für das Kranseil
- Endlagenabschaltung und Überlastschutz fürs Kranseil - hängt zu große Last daran, kann man das Seil nur noch ablassen, nicht aber mehr hochziehen

Sonstiges:
- Schön ungleichmäßig blinkende vier Lampen (vorne zwei blaue, mittig zwei gelbe)

Bedienung:
- Intuitives Bedienpult für alle Funktionen