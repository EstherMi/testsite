---
layout: "image"
title: "Radaufhängung und Antrieb"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/33044
imported:
- "2019"
_4images_image_id: "33044"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33044 -->
Auf der Achse des per Kette angetriebenen Z30 sitzt ein Kegelzahnrad, welches das Z30 am Rad antreibt. Der BS7,5 in Bildmitte sitzt deshalb nicht voll in den BS15, weil die von oben kommende Antriebsachse noch vorbei muss. Um diese Achse kann sich das Rad beim Lenken grundsätzlich drehen. Die Geometrie der Radaufhängung sorgt aber dafür, dass sich das Rad ziemlich genau auf seinem Aufstandsflächen-Mittelpunkt dreht. Die Lenkung wird aber von der hier links oben sichtbaren längs verlaufenden Kette bewirkt, die Streben halten und führen nur passiv. Die Verbinder 15, die dort stecken, verhindern beim Einlenken ein ohne äußere Hilfe nicht korrigierbares Einknicken (Überstrecken).

Und so ein bisschen hat das auch was von einer Portalachse. :-)