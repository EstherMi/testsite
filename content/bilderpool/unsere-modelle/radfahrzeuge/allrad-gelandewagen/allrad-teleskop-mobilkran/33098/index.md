---
layout: "image"
title: "Fahrzeug in der Transporthilfe"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran67.jpg"
weight: "67"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/33098
imported:
- "2019"
_4images_image_id: "33098"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "67"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33098 -->
Auch ein Trafo ist fest unterbringbar.