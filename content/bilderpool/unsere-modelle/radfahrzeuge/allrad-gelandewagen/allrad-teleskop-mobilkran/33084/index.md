---
layout: "image"
title: "Der Arm ganz ausgefahren (2)"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran53.jpg"
weight: "53"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/33084
imported:
- "2019"
_4images_image_id: "33084"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "53"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33084 -->
Dieser Endschalter ist es, der dem Ausfahren eine Grenze setzt.