---
layout: "image"
title: "Der Arm ganz ausgefahren (3)"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran54.jpg"
weight: "54"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/33085
imported:
- "2019"
_4images_image_id: "33085"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "54"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33085 -->
Es sind nur noch die schwarzen Streben an den unteren Enden der Segmente und die BS7,5-BS5-Kombinationen an den oberen Enden, die den ganzen Arm zusammenhalten.