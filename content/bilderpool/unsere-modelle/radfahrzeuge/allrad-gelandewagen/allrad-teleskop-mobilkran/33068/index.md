---
layout: "image"
title: "Aufstell-Antrieb (3)"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran37.jpg"
weight: "37"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/33068
imported:
- "2019"
_4images_image_id: "33068"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33068 -->
Das Weiße an der Flachnabe des mittleren Z20 ist Abrieb nach dem Convention-Dauerbetrieb.