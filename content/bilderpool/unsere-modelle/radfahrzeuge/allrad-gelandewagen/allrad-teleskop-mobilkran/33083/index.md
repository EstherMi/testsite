---
layout: "image"
title: "Der Arm ganz ausgefahren (1)"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran52.jpg"
weight: "52"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/33083
imported:
- "2019"
_4images_image_id: "33083"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "52"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33083 -->
Ganz ausgefahren ist der Kranarm praktisch ein hohles Gerippe, aber eben in sich selbst tragend.