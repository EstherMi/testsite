---
layout: "image"
title: "Vorderes Stützenpaar (eingefahren)"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran23.jpg"
weight: "23"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/33054
imported:
- "2019"
_4images_image_id: "33054"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33054 -->
Die Stützenpaare (vorne und hinten) werden von je einem S-Motor recht kräftig über eine Kniegelenkmechanik heruntergezogen. Die Grundidee hab ich von thkais geklaut, Danke auch an ihn. Das geschieht über einen Seilzug von unten. Wieder hoch werden sie von je einer alten ft-Antriebsfeder pro Stützenpaar gezogen.