---
layout: "image"
title: "Antrieb des vorderen Stützenpaares"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran25.jpg"
weight: "25"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/33056
imported:
- "2019"
_4images_image_id: "33056"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33056 -->
Dieser Blick geht auf der Unterseite in Richtung Vordersachse. Zu sehen sind der Fahrantriebs-Powermotor und der schräg eingebaute S-Motor für das vordere Stützenpaar. Über den kleinen Minimotor-Schneckenaufsatz geht es auf ein Z44 mit feinen Zähnen (vom alten mot-1). Direkt darauf ist der Seilzug per Einklemmen in die Hülse mit Scheibe angebracht. Durch den geringen Radius der Achse braucht es keine weitere Untersetzung.