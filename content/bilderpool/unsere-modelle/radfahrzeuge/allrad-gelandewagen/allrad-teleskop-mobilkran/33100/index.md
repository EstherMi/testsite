---
layout: "image"
title: "Ein letztes Stimmungsbild bei der Arbeit"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran69.jpg"
weight: "69"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/33100
imported:
- "2019"
_4images_image_id: "33100"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "69"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33100 -->
Fini! Entschuldigung nochmal für die lange Dokumentation und Danke, falls sich jemand die ganzen Bilder angesehen hat.