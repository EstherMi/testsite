---
layout: "image"
title: "Vorderes Stützenpaar (ausgefahren)"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran24.jpg"
weight: "24"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/33055
imported:
- "2019"
_4images_image_id: "33055"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33055 -->
Der von unten kommende Seilzug zieht am Knie (Danke, Heiko), geht dann aber nochmal durch den Fuß zurück zum festen Endpunkt bei der Seilrolle. Das ergibt nochmal einen Flaschenzug, und damit ist die Kraft genügend, das ganze Fahrzeug wirklich etwas anzuheben.

Ansatzweise zu sehen ist auch der Endlagentaster für die untere Position, der direkt vom oberen Stützenarm betätigt wird.