---
layout: "image"
title: "Bedienpult"
date: "2011-10-03T22:46:37"
picture: "allradteleskopmobilkran05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/33036
imported:
- "2019"
_4images_image_id: "33036"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:37"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33036 -->
Das ist die "intuitive grafische Benutzeroberfläche". Sämtliche Kranfunktionen können von hier aus gesteuert werden. Die Steuerelemente sind dem Kran selbst nachempfunden, damit man gleich sieht, welche Funktion was bewirkt. Überall, wo ein Verbinder 15 offen sichtbar sitzt, ist das ein Griff, an dem man schieben kann.

Grundlage sind zwei miteinander verbundene Bauplatten 500 und unten nochmal zwei (mit Federnocken in den Löchern mit den oberen fest verbunden, und mit Bauplatten 15*45 verbunden). So ist das hinreichend stabil, um herumgetragen werden zu können.

Linke Bauplatte: Fahrwerk und Drehplattform:
- Links unten die vier Räder: Nach rechts/links schieben => Fahrzeug fährt vorwärts/rückwärts
- Rechts daneben die zwei Räder: Nach oben/unten drücken => Fahrzeug lenkt nach links/rechts.
- Links oben: Nach unten ziehen/oben drücken => Stützen fahren nach unten aus/nach oben ein.
- Rechts oben der angedeutete schräg aufgestellte Kranarm: Nach oben drücken/unten ziehen => Plattform dreht sich nach links/rechts

Rechte Bauplatte: Blinkelektronik und Kranarm
- Links oben der graue ft-Schalter: Schaltet das Blinklicht ein bzw. aus.
- Etwas links von der Bildmitte, im linken Teil des angedeuteten schräg aufgestellten Kranarms: Nach oben drücken/unten ziehen => Arm wird aufgestellt/herunter geklappt
- Etwas weiter rechts oben: Nach rechts oben/links unten schieben => Teleskoparm fährt aus/ein
- Rechts unten der Kranhaken: Nach unten/oben schieben => Kranhaken wird abgesenkt/angehoben.

HINWEIS: Ab jetzt kommen noch viele Detailbilder zur ausführlichen Dokumentation. Die wesentlichen Funktionen wurden in den ersten Bildern schon sichtbar. Wer also nur mal grob gucken wollte, kann den Rest getrost überspringen. Ich entschuldige mich bei all jenen, denen das zu viele Bilder sind - ggf. bitte einfach nicht angucken. :-)