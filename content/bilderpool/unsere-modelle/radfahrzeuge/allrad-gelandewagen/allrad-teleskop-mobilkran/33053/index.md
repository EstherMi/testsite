---
layout: "image"
title: "Hinterradfederung (3)"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran22.jpg"
weight: "22"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/33053
imported:
- "2019"
_4images_image_id: "33053"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33053 -->
Hier ein Blick auf die Aufhängung und Federung des rechten vorderen Hinterrades. Die Welle, die da von hinten ankommt, trägt eine Schnecke zum Antrieb des Drehkranzes unter der drehbaren Plattform.