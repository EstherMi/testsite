---
layout: "image"
title: "Überlastschutz (2)"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran63.jpg"
weight: "63"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/33094
imported:
- "2019"
_4images_image_id: "33094"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "63"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33094 -->
Das federnde Element der Überlastsicherung ist einfach eine Bauplatte 15*45. So schaltet der Motor für den Kranhaken sofort ab, wenn das Seil ganz oben an der Kranspitze angekommen ist oder durch ein zu schweres Gewicht belastet wird.