---
layout: "image"
title: "Federung der Vorderachse (2)"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran17.jpg"
weight: "17"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/33048
imported:
- "2019"
_4images_image_id: "33048"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33048 -->
Durch Einstecken von bis zu zwei weiteren Federfüßen kann die Federhärte der Vorderachse nochmal gesteigert werden. Der S-Motor dient der Lenkung: Er dreht mittels "Getriebebock + Schnecke m0,5" (#31069) an einer "Rastachse + Zahnrad Z28 m0,5" (#31082), auf der unter dem BS30 mit Loch direkt eines der beiden Rast-Z10 steckt, die die Lenk-Kette antreiben.