---
layout: "image"
title: "Aufstell-Antrieb (1)"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran35.jpg"
weight: "35"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/33066
imported:
- "2019"
_4images_image_id: "33066"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33066 -->
Diese Mimik ist dafür zuständig, den (hinten gelenkig angeschlagenen) Kranarm zuverlässig anzugeben, egal ob er gerade ein- oder ausgefahren ist. Da muss also richtig "Schmackes" bei, denn die Hebelkräfte des Arms können sehr heftig werden.