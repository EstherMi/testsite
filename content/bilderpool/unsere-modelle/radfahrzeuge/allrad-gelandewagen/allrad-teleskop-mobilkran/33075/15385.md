---
layout: "comment"
hidden: true
title: "15385"
date: "2011-10-10T10:18:39"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Tach auch, Ludger,

das war von meiner Frau "geborgter" Zwirn aus dem Nähkasten, nichts besonderes. Ich meine, sie sprach von "Sternfaden". Er ist bei diversen Vorversionen noch gerissen, aber seit ich am zweiten anstatt am ersten ausfahrbaren Segment ziehe, ist nichts mehr passiert. Der Arm läuft erfreulicherweise total zuverlässig.

Gruß,
Stefan