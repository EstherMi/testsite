---
layout: "comment"
hidden: true
title: "19247"
date: "2014-07-14T19:49:36"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Jetzt bin ich selber am Grübeln, was genau da die Rastkegel führt. Ich hätte auf den ersten Blick wegen des leichten Laufs auf diese roten 15-mm-Hülsen getippt, aber das sieht mir schmaler als ein BS15 aus. Evtl. sind es auch nur diese ganz kleinen roten Abstandshalter, aber ich glaube, das müssten man einfach nochmal austesten.

Und ich dachte ehrlich, ich hätte zu viele Fotos gemacht ;-)

Gruß,
Stefan