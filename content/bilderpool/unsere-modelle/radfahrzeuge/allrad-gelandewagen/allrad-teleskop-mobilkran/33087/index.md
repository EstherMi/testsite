---
layout: "image"
title: "Der Arm ganz ausgefahren (5)"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran56.jpg"
weight: "56"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/33087
imported:
- "2019"
_4images_image_id: "33087"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "56"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33087 -->
Alles an einem bestimmten Segment muss sowohl was die Breite als auch die Dicke des Arms angeht durch alle Führungselement des nächstäußeren passen.