---
layout: "image"
title: "Belastbarkeitstest (2)"
date: "2011-10-03T22:46:38"
picture: "allradteleskopmobilkran59.jpg"
weight: "59"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/33090
imported:
- "2019"
_4images_image_id: "33090"
_4images_cat_id: "2435"
_4images_user_id: "104"
_4images_image_date: "2011-10-03T22:46:38"
_4images_image_order: "59"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33090 -->
Derselbe Zustand (flacher Arm ganz ausgefahren) am Übergang von den ersten zu den zweiten U-Trägern des äußersten Segments gesehen offenbart aber schon, dass wir hier die Grenze der Belastbarkeit des Materials erreicht haben. Die gelben Platten sind mit je 8 Federnocken befestigt und verstärken diesen Übergang.

Lochstreben auf der Oberseite wären natürlich total angenehm, können aber nicht verwendet werden: Die schwarze Querstrebe oben im Bild muss ja beim Einfahren hier wieder ungehindert durch können.