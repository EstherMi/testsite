---
layout: "image"
title: "Moster-Truck Chassis 17"
date: "2007-09-21T20:19:31"
picture: "mostertruckchassis09.jpg"
weight: "25"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/11894
imported:
- "2019"
_4images_image_id: "11894"
_4images_cat_id: "1061"
_4images_user_id: "502"
_4images_image_date: "2007-09-21T20:19:31"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11894 -->
