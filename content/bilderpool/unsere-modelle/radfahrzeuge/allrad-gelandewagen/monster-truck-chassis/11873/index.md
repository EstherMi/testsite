---
layout: "image"
title: "Moster-Truck Chassis 4"
date: "2007-09-19T19:23:50"
picture: "mostertruckchassis4.jpg"
weight: "30"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/11873
imported:
- "2019"
_4images_image_id: "11873"
_4images_cat_id: "1061"
_4images_user_id: "502"
_4images_image_date: "2007-09-19T19:23:50"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11873 -->
Der Antrieb bietet ein gutes Verhältnis aus Kraft und Geschwindigkeit. Die gelbe Strebe verhindert dass sich die Achse waagerecht verschiebt.