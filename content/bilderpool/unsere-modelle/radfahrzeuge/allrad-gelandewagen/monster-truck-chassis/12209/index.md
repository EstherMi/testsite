---
layout: "image"
title: "Moster-Truck Chassis 22"
date: "2007-10-14T15:54:18"
picture: "mostertruckchassis4_2.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/12209
imported:
- "2019"
_4images_image_id: "12209"
_4images_cat_id: "1061"
_4images_user_id: "502"
_4images_image_date: "2007-10-14T15:54:18"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12209 -->
