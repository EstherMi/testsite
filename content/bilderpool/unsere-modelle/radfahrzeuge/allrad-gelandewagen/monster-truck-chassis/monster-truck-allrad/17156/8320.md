---
layout: "comment"
hidden: true
title: "8320"
date: "2009-01-24T21:08:18"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
@StefanL
Hallo Stefan,
tolles Modell, Glückwunsch.
Die Verwindungsmechanik ist u.a. meisterhaft gelöst, z.B. auf Bild 3 erkennt man die Funktion der beiden Drehkränze.
Gruss, Udo2