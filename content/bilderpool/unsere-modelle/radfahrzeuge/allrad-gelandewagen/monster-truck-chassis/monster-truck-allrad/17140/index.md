---
layout: "image"
title: "Monster-Truck Allrad 1"
date: "2009-01-24T16:31:27"
picture: "monstertruckallrad01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/17140
imported:
- "2019"
_4images_image_id: "17140"
_4images_cat_id: "1538"
_4images_user_id: "502"
_4images_image_date: "2009-01-24T16:31:27"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17140 -->
Hier nun endlich mit richtigem Allradantrieb. Durch die 50:1 Power-Motoren und die alten großen Kardangelenke hat er eine unglaubliche Kraft. Die Achsen an den Kardangelenken wurden so befestigt dass sie nicht durchdrehen können.