---
layout: "image"
title: "Monster-Truck Allrad 5"
date: "2009-01-24T16:31:28"
picture: "monstertruckallrad05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/17144
imported:
- "2019"
_4images_image_id: "17144"
_4images_cat_id: "1538"
_4images_user_id: "502"
_4images_image_date: "2009-01-24T16:31:28"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17144 -->
