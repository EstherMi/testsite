---
layout: "image"
title: "Monster-Truck Allrad 3"
date: "2009-01-24T16:31:28"
picture: "monstertruckallrad03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/17142
imported:
- "2019"
_4images_image_id: "17142"
_4images_cat_id: "1538"
_4images_user_id: "502"
_4images_image_date: "2009-01-24T16:31:28"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17142 -->
