---
layout: "image"
title: "Moster-Truck Chassis 2"
date: "2007-09-19T19:23:50"
picture: "mostertruckchassis2.jpg"
weight: "28"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/11871
imported:
- "2019"
_4images_image_id: "11871"
_4images_cat_id: "1061"
_4images_user_id: "502"
_4images_image_date: "2007-09-19T19:23:50"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11871 -->
