---
layout: "image"
title: "Moster-Truck Chassis 11"
date: "2007-09-21T20:19:31"
picture: "mostertruckchassis03.jpg"
weight: "19"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/11888
imported:
- "2019"
_4images_image_id: "11888"
_4images_cat_id: "1061"
_4images_user_id: "502"
_4images_image_date: "2007-09-21T20:19:31"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11888 -->
Mit allen Rädern kommt er gerade noch hoch.