---
layout: "image"
title: "Moster-Truck Chassis 21"
date: "2007-10-14T15:54:18"
picture: "mostertruckchassis3_2.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/12208
imported:
- "2019"
_4images_image_id: "12208"
_4images_cat_id: "1061"
_4images_user_id: "502"
_4images_image_date: "2007-10-14T15:54:18"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12208 -->
