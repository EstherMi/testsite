---
layout: "image"
title: "Moster-Truck Chassis 29"
date: "2007-11-03T22:08:57"
picture: "mostertruckchassis3_3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/12386
imported:
- "2019"
_4images_image_id: "12386"
_4images_cat_id: "1061"
_4images_user_id: "502"
_4images_image_date: "2007-11-03T22:08:57"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12386 -->
