---
layout: "image"
title: "Moster-Truck Chassis 24"
date: "2007-10-14T15:54:18"
picture: "mostertruckchassis6_2.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/12211
imported:
- "2019"
_4images_image_id: "12211"
_4images_cat_id: "1061"
_4images_user_id: "502"
_4images_image_date: "2007-10-14T15:54:18"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12211 -->
