---
layout: "image"
title: "Rock Crawler 5"
date: "2009-02-09T22:00:15"
picture: "Rock_Crawler_05_klein.jpg"
weight: "5"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/17345
imported:
- "2019"
_4images_image_id: "17345"
_4images_cat_id: "1561"
_4images_user_id: "328"
_4images_image_date: "2009-02-09T22:00:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17345 -->
