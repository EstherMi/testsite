---
layout: "image"
title: "Rock Crawler 2"
date: "2009-02-09T22:00:15"
picture: "Rock_Crawler_02_klein.jpg"
weight: "2"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/17342
imported:
- "2019"
_4images_image_id: "17342"
_4images_cat_id: "1561"
_4images_user_id: "328"
_4images_image_date: "2009-02-09T22:00:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17342 -->
Auf Spielereien wie Licht, ein realistisches Fahrerhaus oder einen Aufbau habe ich komplett verzichtet. Mir ging es nur um die bestmögliche Darstellung der Technik, extreme Anstiege zu bewältigen.

Das Fahrzeug kann NICHT federn, auch wenn so aussieht. Vorder- und Hinterachse sind drehbar gelagert und können extrem verschränken. Die beiden gelben vertikalen Statikstreben an den Achsen halten die Fahrzeugmasse; die Federn dienen nur dazu, das Fahrzeug gerade zu halten.