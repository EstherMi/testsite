---
layout: "image"
title: "Rock Crawler 15"
date: "2009-02-09T22:00:25"
picture: "Rock_Crawler_15_klein.jpg"
weight: "15"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/17355
imported:
- "2019"
_4images_image_id: "17355"
_4images_cat_id: "1561"
_4images_user_id: "328"
_4images_image_date: "2009-02-09T22:00:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17355 -->
Blick von oben auf die ausgebaute Vorderachse. Der Mini-Motor sitzt mit auf der Achse und verschränkt also mit.

Die Endschalter sitzen direkt auf der Zahnstange und begrenzen deren Weg, wenn sie an das Gehäuse vom Getriebe stoßen. Die Taster können auf der Zahnstange leicht verschoben werden; so lässt sich der Lenkhub sehr genau justieren.

Die im Gelände auftretenden Kräfte wirken auch stark auf die Zahnstange und das Hubgetriebe. Meines habe ich mir mit diesem Modell jedenfalls ruiniert... ;o) Ein Zahnrad im Hubgetriebe rutscht leicht auf seiner Welle.