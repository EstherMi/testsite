---
layout: "image"
title: "Rock Crawler 14"
date: "2009-02-09T22:00:25"
picture: "Rock_Crawler_14_klein.jpg"
weight: "14"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/17354
imported:
- "2019"
_4images_image_id: "17354"
_4images_cat_id: "1561"
_4images_user_id: "328"
_4images_image_date: "2009-02-09T22:00:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17354 -->
Ohne Fahrerhaus.