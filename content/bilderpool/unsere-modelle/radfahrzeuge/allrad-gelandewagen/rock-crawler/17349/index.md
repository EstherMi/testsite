---
layout: "image"
title: "Rock Crawler 9"
date: "2009-02-09T22:00:15"
picture: "Rock_Crawler_09_klein.jpg"
weight: "9"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/17349
imported:
- "2019"
_4images_image_id: "17349"
_4images_cat_id: "1561"
_4images_user_id: "328"
_4images_image_date: "2009-02-09T22:00:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17349 -->
