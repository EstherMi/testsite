---
layout: "image"
title: "VW T3 Syncro 4"
date: "2008-01-03T18:19:58"
picture: "VW_T3_Syncro_04.jpg"
weight: "4"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/13215
imported:
- "2019"
_4images_image_id: "13215"
_4images_cat_id: "1194"
_4images_user_id: "328"
_4images_image_date: "2008-01-03T18:19:58"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13215 -->
Der mögliche Lenkeinschlag des Drehschemels ist enorm, trotzdem schleift kein Rad.