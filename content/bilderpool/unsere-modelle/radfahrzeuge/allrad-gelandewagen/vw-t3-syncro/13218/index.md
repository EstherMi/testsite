---
layout: "image"
title: "VW T3 Syncro 7"
date: "2008-01-03T18:19:58"
picture: "VW_T3_Syncro_12.jpg"
weight: "7"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/13218
imported:
- "2019"
_4images_image_id: "13218"
_4images_cat_id: "1194"
_4images_user_id: "328"
_4images_image_date: "2008-01-03T18:19:58"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13218 -->
Ohne Dach und Akku sieht’s so aus.