---
layout: "comment"
hidden: true
title: "16940"
date: "2012-06-29T15:42:12"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Kurzes Update:

Beim Zerlegen ist mir aufgefallen, dass die Konstruktion aus 2 nebeneinander liegenden Bausteinen 7,5 mit den beiden Strebenadaptern (hier links im Bild) nicht notwendig ist und keine zusätzliche Stabilität bringt. Ich hatte das erst anders konzipiert.

Für einen evtl. Nachbau kann die Statikstrebe also 15 mm kürzer ausfallen, und die Bodenfreiheit unter dem Drehgelenk wird ein wenig größer.

Gruß, Thomas