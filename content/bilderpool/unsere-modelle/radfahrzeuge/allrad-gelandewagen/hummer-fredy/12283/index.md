---
layout: "image"
title: "Lenkung"
date: "2007-10-22T18:32:47"
picture: "ersteversuchefuereinenhummer4.jpg"
weight: "11"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/12283
imported:
- "2019"
_4images_image_id: "12283"
_4images_cat_id: "1096"
_4images_user_id: "453"
_4images_image_date: "2007-10-22T18:32:47"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12283 -->
Verbindung von Zahnstange und Lenkgestänge.