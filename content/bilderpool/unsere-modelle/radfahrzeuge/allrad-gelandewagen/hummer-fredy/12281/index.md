---
layout: "image"
title: "Lenkung"
date: "2007-10-22T18:32:47"
picture: "ersteversuchefuereinenhummer2.jpg"
weight: "9"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/12281
imported:
- "2019"
_4images_image_id: "12281"
_4images_cat_id: "1096"
_4images_user_id: "453"
_4images_image_date: "2007-10-22T18:32:47"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12281 -->
Der Minimotor bewegt die Zahnstange für die Lenkung nach Links und Rechts.
In der Mitte wieder das Differenzial zu erkennen.