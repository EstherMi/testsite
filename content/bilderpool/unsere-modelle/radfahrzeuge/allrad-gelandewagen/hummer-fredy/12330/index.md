---
layout: "image"
title: "In grau"
date: "2007-10-27T15:04:27"
picture: "hummer1_2.jpg"
weight: "1"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/12330
imported:
- "2019"
_4images_image_id: "12330"
_4images_cat_id: "1096"
_4images_user_id: "453"
_4images_image_date: "2007-10-27T15:04:27"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12330 -->
Harald hatte vorgeschlagen den Hummer in einer Farbe aufzubauen, das habe ich jetzt gemacht und es sieht Prima aus.

Die Gelben Teile in der Hinderachse werde ich noch austauschen. Rote Felgen habe ich leider nicht.