---
layout: "image"
title: "Gesamtansicht"
date: "2007-10-22T20:38:42"
picture: "hummer_1_001.jpg"
weight: "7"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/12286
imported:
- "2019"
_4images_image_id: "12286"
_4images_cat_id: "1096"
_4images_user_id: "453"
_4images_image_date: "2007-10-22T20:38:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12286 -->
Hier mal eine Gesamtansicht.

Wie schon ein paar Bilder vorher erwähnt, die Karosserie hat ihre Form und im groben wird sie so bleiben.

Die Lenkung passt nicht, sie wird noch angepasst.