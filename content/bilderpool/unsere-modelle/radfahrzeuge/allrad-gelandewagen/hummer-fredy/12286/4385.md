---
layout: "comment"
hidden: true
title: "4385"
date: "2007-10-23T20:24:49"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Ich kann mich ja täuschen, aber für mein Auge liegt der Wagenkasten etwa um einen BS15 zu tief und das Ganze wirkt damit ein wenig "brav". Die Hinterachse sollte kein Hindernis für ein bisschen "Lifting" darstellen, bei der Vorderachse kann ich es schlecht sagen. 

Wenn du das Farbenspiel zwischen grau/gelb/schwarz/rot ein bisschen entmischst, gewinnt die Optik sicherlich.


Gruß,
Harald