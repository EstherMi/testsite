---
layout: "overview"
title: "Gelände-Buggy mit Beleuchtung"
date: 2019-12-17T18:46:56+01:00
legacy_id:
- categories/682
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=682 --> 
8 Leuchtbausteine wurden dem Fahrzeug hinzugefügt. Da es keine Befestigungspunkte gab, habe ich vorne eine Art \"Motorraum\" und hinten eine Art \"Kofferraum\" angebracht. Leuchtbausteine sind parallel geschaltet, daher der \"Kabelwust\".