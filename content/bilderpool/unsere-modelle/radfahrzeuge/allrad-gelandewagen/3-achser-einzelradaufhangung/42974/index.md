---
layout: "image"
title: "Einzelradaufhängung am Heck"
date: "2016-03-02T12:54:17"
picture: "achseinzel7.jpg"
weight: "7"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/42974
imported:
- "2019"
_4images_image_id: "42974"
_4images_cat_id: "3195"
_4images_user_id: "558"
_4images_image_date: "2016-03-02T12:54:17"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42974 -->
Ohne leichtes Verkleben ist das ganze natürlich nicht sehr langlebig, glücklicherweise lässt sich Heißkleber wunderbar von ft-Teilen abziehen :P