---
layout: "image"
title: "Mechanik um das Mittendifferenzial"
date: "2016-03-02T12:54:17"
picture: "achseinzel3.jpg"
weight: "3"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/42970
imported:
- "2019"
_4images_image_id: "42970"
_4images_cat_id: "3195"
_4images_user_id: "558"
_4images_image_date: "2016-03-02T12:54:17"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42970 -->
Der S-Motor verschiebt über die beiden Schnecken den Powermotor mit Antriebsachse. Im ersten Fall greift eines der beiden Z10 in das Z20 des Differenzial und ermöglicht so einen Standard Allradantrieb. Im zweiten Fall greifen die beiden Z10 je in ein Z20 (siehe nächstes Bild) und sperren so das Differenzial. Im dritten Fall greift das rechte Z10 in das linke Z20 und treibt so nur das Heck an. Der Vorderradantrieb ist so relativ zum Heck frei beweglich und erfährt keine Kraftübertragung.