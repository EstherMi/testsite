---
layout: "image"
title: "Hummer-01.JPG"
date: "2005-06-12T14:19:15"
picture: "Hummer-01.jpg"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Hummer", "HMMWV", "Humvee", "Allrad", "Großreifen", "Monsterreifen"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4412
imported:
- "2019"
_4images_image_id: "4412"
_4images_cat_id: "363"
_4images_user_id: "4"
_4images_image_date: "2005-06-12T14:19:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4412 -->
Dieses Fahrzeug hat nur das, was an einem Fahrzeug wirklich dran sein muss. 

Plus Extra-Ausstattung: Allrad-Antrieb :-)