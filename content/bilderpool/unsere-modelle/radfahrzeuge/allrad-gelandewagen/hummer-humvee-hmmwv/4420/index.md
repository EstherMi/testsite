---
layout: "image"
title: "Hummer-10.JPG"
date: "2005-06-12T14:19:15"
picture: "Hummer-10.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Hummer", "HMMWV", "Humvee", "Allrad"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4420
imported:
- "2019"
_4images_image_id: "4420"
_4images_cat_id: "363"
_4images_user_id: "4"
_4images_image_date: "2005-06-12T14:19:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4420 -->
Die beiden BS15-Loch, in denen das Differenzial gelagert ist, wurden auf 10,5 mm Breite abgedreht. Dabei bleibt die Längsnut gerade eben erhalten und kann Sachen längs führen, aber richtig klemmen tut darin nichts mehr. Bei 11 mm hätte es wohl auch geklappt, aber das probiere ich später.

Was tut man nicht alles, um das dumme schwarze Differenzial dazu zu bringen, im ft-Raster mitzuspielen!