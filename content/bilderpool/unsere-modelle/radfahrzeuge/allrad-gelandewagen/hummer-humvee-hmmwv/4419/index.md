---
layout: "image"
title: "Hummer-09.JPG"
date: "2005-06-12T14:19:15"
picture: "Hummer-09.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Hummer", "HMMWV", "Humvee", "Allrad"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4419
imported:
- "2019"
_4images_image_id: "4419"
_4images_cat_id: "363"
_4images_user_id: "4"
_4images_image_date: "2005-06-12T14:19:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4419 -->
Die Vorderachse hängt nur an den Federn und den beiden Streben X-63,6. Ähnlich ist es bei der Hinterachse, die nur an den vier grauen Streben hängt und locker in den Federn abgestützt ist.

An der Zahnstange für die Lenkung wurde die Federleiste fast zur Hälfte abgeschnippelt und dann ein Strebenadapter 31848 aufgeklebt.