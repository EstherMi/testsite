---
layout: "image"
title: "Hummer-06.JPG"
date: "2005-06-12T14:19:15"
picture: "Hummer-06.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Hummer", "HMMWV", "Humvee", "Allrad"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4416
imported:
- "2019"
_4images_image_id: "4416"
_4images_cat_id: "363"
_4images_user_id: "4"
_4images_image_date: "2005-06-12T14:19:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4416 -->
Blick von oben auf die (starre, aber gefederte) Hinterachse.

(Wer sich mit den Alu-135-mm-Stäben zum Sonderpreis noch nicht eingedeckt hat, ist selber schuld!)