---
layout: "image"
title: "Hummer-05.JPG"
date: "2005-06-12T14:19:15"
picture: "Hummer-05.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Hummer", "HMMWV", "Humvee", "Allrad"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4415
imported:
- "2019"
_4images_image_id: "4415"
_4images_cat_id: "363"
_4images_user_id: "4"
_4images_image_date: "2005-06-12T14:19:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4415 -->
Die Zahnstange der Lenkung wurde etwas bearbeitet, wie auch die BS15+Loch, durch die die Antriebswellen laufen. Details dazu kommen noch.