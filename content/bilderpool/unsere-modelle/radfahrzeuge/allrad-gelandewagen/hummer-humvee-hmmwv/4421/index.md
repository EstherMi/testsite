---
layout: "image"
title: "Hummer-11.JPG"
date: "2005-06-12T14:19:15"
picture: "Hummer-11.jpg"
weight: "16"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Hummer", "HMMWV", "Humvee", "Allrad"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4421
imported:
- "2019"
_4images_image_id: "4421"
_4images_cat_id: "363"
_4images_user_id: "4"
_4images_image_date: "2005-06-12T14:19:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4421 -->
Ein Stück der Federleiste auf der Zahnstange wurde abgetrennt und dann ein Strebenadapter aufgeklebt.

Der Lenkmotor ist mit drei dieser Knubbel 7,5 am Achskörper befestigt, von denen hier nur der rechte voll zu sehen ist.