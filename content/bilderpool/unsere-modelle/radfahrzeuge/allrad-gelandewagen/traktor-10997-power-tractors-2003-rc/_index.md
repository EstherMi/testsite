---
layout: "overview"
title: "Traktor 10997 Power Tractors 2003 mit RC Fernsteuerung"
date: 2019-12-17T18:49:23+01:00
legacy_id:
- categories/2891
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2891 --> 
das Modell MB-Trac als Grundlage eines RC-Ferngesteuerten Fahrzeuges mit aktuellem Original-Servo, Power-Motor-Antrieb nach Bauanleitung und einer Graupner-RC-Fernsteuerung mit XXL-Motorregler (1,5x1,8cm)(!)