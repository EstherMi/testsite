---
layout: "image"
title: "Classic-Unimog 7"
date: "2011-01-15T20:46:34"
picture: "Classic-Unimog_09.jpg"
weight: "7"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/29694
imported:
- "2019"
_4images_image_id: "29694"
_4images_cat_id: "2178"
_4images_user_id: "328"
_4images_image_date: "2011-01-15T20:46:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29694 -->
Der Unterboden mit dem Kettenantrieb von der Hinterachse nach vorn. Die beiden kleinen schwarzen Räder heben die Kette an und ermöglichen so wenigstens ein wenig Bodenfreiheit zwischen den Achsen.