---
layout: "image"
title: "Classic-Unimog 12"
date: "2011-01-15T20:46:47"
picture: "Classic-Unimog_15.jpg"
weight: "12"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/29699
imported:
- "2019"
_4images_image_id: "29699"
_4images_cat_id: "2178"
_4images_user_id: "328"
_4images_image_date: "2011-01-15T20:46:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29699 -->
