---
layout: "image"
title: "Classic-Unimog 8"
date: "2011-01-15T20:46:34"
picture: "Classic-Unimog_10.jpg"
weight: "8"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/29695
imported:
- "2019"
_4images_image_id: "29695"
_4images_cat_id: "2178"
_4images_user_id: "328"
_4images_image_date: "2011-01-15T20:46:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29695 -->
Deteilansicht der Vorderachse mit vollem Lenkeinschlag. Kleiner dürfte eine angetriebene Vorderachse mit Achsschenkel-Lenkung nicht möglich sein, oder?

Problematisch ist, dass die Räder unter Last manchmal aus den Kardangelenken heraus rasten. Aber im normalen Fahrbetrieb hält es. Wichtig ist eine gute Bauteilpaarung von Rastachse und Kardangelenk, die fest zusammen halten (am besten nagelneue Teile verwenden).

Das Zahnrad Z20 in der Mitte ist natürlich nicht so toll. Aber hat jemand eine bessere Idee, wie man ein brauchbares Drehmoment auf die freien 15 mm Rastachse zwischen den Kardangelenken übertragen kann? Die Achse muss natürlich auch mindestens noch einmal am Fahrgestell gelagert sein! Danke für Eure Ideen!!!