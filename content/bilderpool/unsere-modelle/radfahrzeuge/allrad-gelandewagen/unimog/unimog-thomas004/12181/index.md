---
layout: "image"
title: "Unimog 8"
date: "2007-10-10T19:44:37"
picture: "Unimog_17.jpg"
weight: "8"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/12181
imported:
- "2019"
_4images_image_id: "12181"
_4images_cat_id: "1090"
_4images_user_id: "328"
_4images_image_date: "2007-10-10T19:44:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12181 -->
Die Vorderachse von unten von vorn.

Das Lenktrapez ist ganz ordentlich und sorgt prima für den unterschiedlichen Lenkeinschlag beider Räder.

Die Zahnstange wird nur durch das Zahnrad selbst am Rausfallen gehindert. Sie ist daher sehr wackelig gelagert, hält aber bombenfest, läuft extrem leicht und klemmt nicht!