---
layout: "image"
title: "Unimog 6"
date: "2007-10-10T19:44:37"
picture: "Unimog_14.jpg"
weight: "6"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/12179
imported:
- "2019"
_4images_image_id: "12179"
_4images_cat_id: "1090"
_4images_user_id: "328"
_4images_image_date: "2007-10-10T19:44:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12179 -->
Die Hinterachse von unten. Sehr stabil; da gehen ordentlich Kräfte drüber. In der Welle zum Differenzial ist bewusst kein Rast-Kardengelenk eingebaut, da dies die erwünschten hohen Kräfte nicht übertragen kann. Es federt aber auch so wunderbar.