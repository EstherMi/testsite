---
layout: "image"
title: "Unimog 10"
date: "2007-10-10T19:44:56"
picture: "Unimog_18.jpg"
weight: "10"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/12183
imported:
- "2019"
_4images_image_id: "12183"
_4images_cat_id: "1090"
_4images_user_id: "328"
_4images_image_date: "2007-10-10T19:44:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12183 -->
Hier noch ein Blick seitlich auf die Vorderachse. Man sieht gut, dass ich versucht habe, so stabil wie möglich zu bauen.

Die Bodenfreiheit vorn ist durch die untere Querträgerebene nicht so die Wucht, aber es geht trotzdem ganz schön voran!