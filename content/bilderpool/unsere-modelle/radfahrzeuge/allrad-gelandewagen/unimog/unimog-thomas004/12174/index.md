---
layout: "image"
title: "Unimog 1"
date: "2007-10-10T19:44:36"
picture: "Unimog_10.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/12174
imported:
- "2019"
_4images_image_id: "12174"
_4images_cat_id: "1090"
_4images_user_id: "328"
_4images_image_date: "2007-10-10T19:44:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12174 -->
Hier von mir ein Beitrag zum beliebten Thema "Gelenkter und gefederter Allrad", quasi dem "heiligen Gral" von Fischertechnik. ;o)

Das Fahrzeug ist hinsichtlich Proportionen, Design und Funktionalität vom Unimog inspiriert und sollte so kräftig wie möglich aufgebaut werden.

Der Antrieb erfolgt per Power-Motor unter dem Fahrerhaus über ein Getriebe und drei Differenziale auf alle vier Räder.

Die Lenkung erfolgt per klassischem Zahnstangenlenkgetriebe über das Lenkrad im Fahrerhaus, "unterstützt" durch einen Mini-Motor. Das Lenkrad dreht sich also mit, so wie es sein soll. :o)

Beide Achsen sind gefedert. Ich habe bewusst vollständig auf die Rast-Kardangelenke verzichtet, um möglichst große Kräfte übertragen zu können. Das Mittel-Differenzial wurde axial leicht verschiebbar aufgehängt, so dass in Verbindung mit den Elastizitäten in den Kunststoffachsen der beiden Abgänge nach vorn und hinten die Federbewegungen der Achsen ermöglicht werden. Da die Federwege recht gering sind, funktioniert dies prächtig.

Der Akku-Pack ist im Fahrerhaus "versteckt"; die Steuerung des Fahrzeugs erfolgt per Kabelfernbedienung.

Trotz des stabilen Aufbaus aller Antriebskomponenten kommt es bei höheren Hindernissen bzw. starken Steigungen zu springenden Rast-Kegelrädern in der Vorderachse. Schade...