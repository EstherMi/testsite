---
layout: "image"
title: "Kompaktkran und Stützsystem"
date: "2014-08-07T12:53:04"
picture: "u8.jpg"
weight: "10"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/39166
imported:
- "2019"
_4images_image_id: "39166"
_4images_cat_id: "2928"
_4images_user_id: "2228"
_4images_image_date: "2014-08-07T12:53:04"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39166 -->
Hier ist ebenfalls das Anbausystem für weiter Arbeitsgeräte erkennbar. Es lässt sich beispielsweise ein Salzstreugerät für den "Wintereinsatz" des Unimogs anbauen. siehe auch "Ansicht vorne"