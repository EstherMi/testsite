---
layout: "image"
title: "Kompaktkran eingefahren (2)"
date: "2014-08-07T12:53:04"
picture: "u7.jpg"
weight: "9"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/39165
imported:
- "2019"
_4images_image_id: "39165"
_4images_cat_id: "2928"
_4images_user_id: "2228"
_4images_image_date: "2014-08-07T12:53:04"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39165 -->
Der Klappkran kann um 360° per XS Motor gedreht werden. Hierführ habe ich die Motorsteuerung über zwei Taster verwendet. Siehe "Motosteuerung Teil 3", ftpedia 3/2011