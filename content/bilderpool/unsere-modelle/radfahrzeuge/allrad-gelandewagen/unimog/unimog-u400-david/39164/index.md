---
layout: "image"
title: "Kompaktkran eingefahren (1)"
date: "2014-08-07T12:53:04"
picture: "u6.jpg"
weight: "8"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/39164
imported:
- "2019"
_4images_image_id: "39164"
_4images_cat_id: "2928"
_4images_user_id: "2228"
_4images_image_date: "2014-08-07T12:53:04"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39164 -->
Hier sieht man den Kompaktkran in eingefahrenem Zustand. Über zwei Zylinder lässt sich dieser ausklappen.