---
layout: "image"
title: "Unimog mit Kompaktran (1)"
date: "2014-08-07T12:53:04"
picture: "u1.jpg"
weight: "3"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/39159
imported:
- "2019"
_4images_image_id: "39159"
_4images_cat_id: "2928"
_4images_user_id: "2228"
_4images_image_date: "2014-08-07T12:53:04"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39159 -->
Dies ist mein Fischertechnik Unimog mit Kompaktkran! Der Unimog lässt sich mit dem Fischertechnik Control Set steuern. Das Fahrgestell ist gefedert, als Motor für die Fortbewegung dient ein Encoder Motor. Desweitern gibt es zwei Motoren (Powermotor 50:1) für das Anbausystem (siehe Bild "Ansicht vorne"), mit dem sich der Unimog erweitern lässt.
Ein weiters Highlight des Unimogs ist der Kompaktkran, der sich pneumatisch ausklappen kann und mit einem XS Motor gedreht werden kann.