---
layout: "image"
title: "Seitenansicht"
date: "2014-08-07T12:53:04"
picture: "u5.jpg"
weight: "7"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/39163
imported:
- "2019"
_4images_image_id: "39163"
_4images_cat_id: "2928"
_4images_user_id: "2228"
_4images_image_date: "2014-08-07T12:53:04"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39163 -->
Dank dem neuen ft Kompressor ist es möglich, den Unimog (etwa 2,5kg) mit den Stützen anzuheben. Dies entlastet die Federung.