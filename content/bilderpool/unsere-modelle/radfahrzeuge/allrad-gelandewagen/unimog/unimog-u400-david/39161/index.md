---
layout: "image"
title: "Ansicht vorne"
date: "2014-08-07T12:53:04"
picture: "u3.jpg"
weight: "5"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/39161
imported:
- "2019"
_4images_image_id: "39161"
_4images_cat_id: "2928"
_4images_user_id: "2228"
_4images_image_date: "2014-08-07T12:53:04"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39161 -->
Der Unimog lässt sich mit Anbaumaschinen (Schneefräse, Mähmaschine, Seilwinde...) erweitern. Diese kann man an den schwarzen Statikträgern 30 am Unimog befestigen. Sowohl vorne, als auch hinten stehen je eine Abtriebswelle (schwarze Stange), ein Stromanschluss und ein Pneumatikanschluss (dieser ist auf diesem Bild schlecht zu erkennen, er befindet sich direkt unter der roten Bauplatte 30*90)  zur Verfügung.