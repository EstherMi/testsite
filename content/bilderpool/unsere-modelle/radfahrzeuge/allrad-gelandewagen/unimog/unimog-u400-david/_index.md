---
layout: "overview"
title: "Unimog U400 (David)"
date: 2019-12-17T18:48:18+01:00
legacy_id:
- categories/2928
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2928 --> 
Dies ist mein Nachbau des Unimog U400. Der ein oder andere kennt dieses Modell vielleicht aus der Gallerie auf der Fischertechnik Website. Der Unimog hat zahlreiche realistische Funktionen wie Federung, Beleuchtung und Ereweiterungsmöglichkeiten. Für einen Allradantrieb hat es aus Platzgründen leider nicht gereicht...