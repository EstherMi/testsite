---
layout: "image"
title: "gesamt"
date: "2010-12-22T15:27:27"
picture: "DSCF4568.jpg"
weight: "6"
konstrukteure: 
- "luchs"
fotografen:
- "luchs"
keywords: ["Unimog", "Kran"]
uploadBy: "luchs"
license: "unknown"
legacy_id:
- details/29502
imported:
- "2019"
_4images_image_id: "29502"
_4images_cat_id: "2108"
_4images_user_id: "1201"
_4images_image_date: "2010-12-22T15:27:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29502 -->
Hier sieht man nochmal den Kran mit einem Contaiener. Man sieht, dass er ziemlich einsinkt, das kommt aber daher, dass der ganze Unimog auf einer Matratze steht. Auf hartem Boden ist er viel stabieler.