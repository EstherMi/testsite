---
layout: "image"
title: "Unimog-Kran2"
date: "2010-12-22T15:27:26"
picture: "DSCF4565.jpg"
weight: "4"
konstrukteure: 
- "luchs"
fotografen:
- "luchs"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "luchs"
license: "unknown"
legacy_id:
- details/29500
imported:
- "2019"
_4images_image_id: "29500"
_4images_cat_id: "2108"
_4images_user_id: "1201"
_4images_image_date: "2010-12-22T15:27:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29500 -->
