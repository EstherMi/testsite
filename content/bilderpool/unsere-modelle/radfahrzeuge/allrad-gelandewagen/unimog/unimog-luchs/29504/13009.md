---
layout: "comment"
hidden: true
title: "13009"
date: "2010-12-22T18:57:47"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Ein Tipp: Du kannst die Kabelenden auch alle in einem Stecker zusammenführen, anstatt die Stecker miteinander zu verbinden. Das wird übersichtlicher, und es kann auch so leicht kein Stecker mehr herausrutschen.