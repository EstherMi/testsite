---
layout: "image"
title: "Unimog von rechts"
date: "2010-10-16T21:50:50"
picture: "DSCF3619.jpg"
weight: "11"
konstrukteure: 
- "luchs"
fotografen:
- "luchs"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "luchs"
license: "unknown"
legacy_id:
- details/29013
imported:
- "2019"
_4images_image_id: "29013"
_4images_cat_id: "2108"
_4images_user_id: "1201"
_4images_image_date: "2010-10-16T21:50:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29013 -->
Dieser Unimog besitzt eine motorisierte Seilwinde, eine Pendelachse und ist mit LED Lichtern ausgestattet. Gesteuert wird er durch das neue Control Set.