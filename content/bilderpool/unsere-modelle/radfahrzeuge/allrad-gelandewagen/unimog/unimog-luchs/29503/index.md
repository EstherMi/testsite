---
layout: "image"
title: "Feder"
date: "2010-12-22T15:27:27"
picture: "DSCF4574.jpg"
weight: "7"
konstrukteure: 
- "luchs"
fotografen:
- "luchs"
keywords: ["Unimog", "Kran"]
uploadBy: "luchs"
license: "unknown"
legacy_id:
- details/29503
imported:
- "2019"
_4images_image_id: "29503"
_4images_cat_id: "2108"
_4images_user_id: "1201"
_4images_image_date: "2010-12-22T15:27:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29503 -->
Hier ist die Feder mit der Pendelachse zusehen.