---
layout: "comment"
hidden: true
title: "12555"
date: "2010-10-19T09:13:05"
uploadBy:
- "sparifankerl"
license: "unknown"
imported:
- "2019"
---
Ich habe die original Birne aus der weißen Fassung herausgenommen,
dann habe ich die LED "hineinimplantiert". Zwei rote Flachsteckern mit
einem Widerstand (270 Ohm) verbunden. So habe ich mir das lästige Löten 
erspart.  Ich hätte es zwar gern so gemacht, wie sebo, hab's  auch probiert,
aber habe es nicht so schön hinbekommen. Die Lösung von franky gefällt
mir auch gut, weil der Widerstand innerhalb der Abdeckkappe liegt.
Leider habe ich noch keine Digicam. Zu Weihnachten kaufe ich mir aber eine
und wenn das mit der beweglichen Pendel-Tandemhinterachse funktioniert,
stelle ich Bilder ein.