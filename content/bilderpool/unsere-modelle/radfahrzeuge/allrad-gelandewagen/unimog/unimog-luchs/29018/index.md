---
layout: "image"
title: "Unimog Pendelachse"
date: "2010-10-16T21:50:52"
picture: "DSCF3625.jpg"
weight: "16"
konstrukteure: 
- "luchs"
fotografen:
- "luchs"
keywords: ["Unimog", "Geländewagen"]
uploadBy: "luchs"
license: "unknown"
legacy_id:
- details/29018
imported:
- "2019"
_4images_image_id: "29018"
_4images_cat_id: "2108"
_4images_user_id: "1201"
_4images_image_date: "2010-10-16T21:50:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29018 -->
Bei der Pendelachse habe ich eine Weile gebraucht, bis ich eine geignete Methode gefunden habe. Denn es sollte kompakt und stabil sein. Außerdem musste noch der Servo mit hineinpassen. Nun habe ich aber eine bewärte Methode gefunden, die ich auch schon in zahlreichen Modellen verbaut habe z.B. in einem MB Truck.