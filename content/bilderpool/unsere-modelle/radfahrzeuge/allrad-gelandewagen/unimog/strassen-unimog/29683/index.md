---
layout: "image"
title: "Kabine"
date: "2011-01-14T13:47:47"
picture: "strassenunimog5.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "T-Bones"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "T-Bones"
license: "unknown"
legacy_id:
- details/29683
imported:
- "2019"
_4images_image_id: "29683"
_4images_cat_id: "2176"
_4images_user_id: "1232"
_4images_image_date: "2011-01-14T13:47:47"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29683 -->
