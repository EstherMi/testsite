---
layout: "image"
title: "Gesamtansicht"
date: "2011-01-14T13:47:47"
picture: "strassenunimog1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "T-Bones"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "T-Bones"
license: "unknown"
legacy_id:
- details/29679
imported:
- "2019"
_4images_image_id: "29679"
_4images_cat_id: "2176"
_4images_user_id: "1232"
_4images_image_date: "2011-01-14T13:47:47"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29679 -->
Der Unimog ist wegen der der fehlenden Federung und der Bereifung nur für die Straße und leichtes Gelände geeignet. Sobald Gas gegeben wird, leuchtet das rote Licht. Mit dem Schalter am Dach werden die Front-Lichter angeschaltet.