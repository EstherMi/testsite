---
layout: "comment"
hidden: true
title: "22256"
date: "2016-07-24T20:23:08"
uploadBy:
- "uffi"
license: "unknown"
imported:
- "2019"
---
Er schafft auf rutschfester Unterlage 100% Steigung. Wegen der Gewichtsverlagerung nach hinten rutschen die Vorderräder dabei allerdings etwas durch.

Siehe hier im Video: 
https://youtu.be/oZbU2iciPYw 

Gruß, Dirk