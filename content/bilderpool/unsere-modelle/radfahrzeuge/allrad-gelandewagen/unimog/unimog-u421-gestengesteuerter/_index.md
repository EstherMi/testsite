---
layout: "overview"
title: "Unimog U421 mit gestengesteuerter Fernbedienung"
date: 2019-12-17T18:48:24+01:00
legacy_id:
- categories/3254
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3254 --> 
Hier mein später Beitrag zum Thema Unimog. Er hat ein paar bekannte Klassiker: Allradantrieb, Federung, Servolenkung.

Und er hat ein paar Neuigkeiten:
- Funkfernsteuerung mit Gestenkontrolle: die Fahrgeschwindigkeit und die Lenkung werden über den Kipp- bzw Drehwinkel um zwei Achsen gesteuert: Kippen nach vorne = Vorwärtsfahrt, Kippen nach hinten = Rückwärtsfahrt, Kippen nach links = Lenkeinschlag nach links, Kippen nach rechts = Lenkeinschlag nach rechts. Alle diese Bewegungen werden mit feiner Auflösung von 8 bit übertragen, dadurch ist das Fahrzeug sehr fein steuerbar.
- Datenübertragung über NRF24 (Sender und Empfänger) jeweils an Arduino Nano Board
- Erfassung des Kipp- bzw. Drehwinkels über MPU6050 am Arduino Nano (Sender)
- Eigenbau-Servoshield am Arduino-Nano Empfänger
- Monster-Motoshield mit VNH2SP30 Motortreiber als Fahrtregler
- Zweiton-Hupe, die über Taster an der Fernbedienung betätigt wird
- Beleuchtung, Blinker, Bremslichter