---
layout: "image"
title: "Eine neue Achse"
date: "2015-04-03T10:00:06"
picture: "Foto_17.jpg"
weight: "1"
konstrukteure: 
- "Julian Bußmeier"
fotografen:
- "Julian Bußmeier"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "JulianBussmeier"
license: "unknown"
legacy_id:
- details/40696
imported:
- "2019"
_4images_image_id: "40696"
_4images_cat_id: "2908"
_4images_user_id: "1474"
_4images_image_date: "2015-04-03T10:00:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40696 -->
Mein Unimog hatte das Problem des Radschiefstandes. Daher habe ich diese neue Vorderachse entwickelt. Das Besondere: Sie ist kleiner, leichter und kommt ohne Teilmodifikationen aus.