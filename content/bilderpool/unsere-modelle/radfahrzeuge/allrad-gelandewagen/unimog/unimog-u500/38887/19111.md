---
layout: "comment"
hidden: true
title: "19111"
date: "2014-06-04T16:00:34"
uploadBy:
- "DasKasperle"
license: "unknown"
imported:
- "2019"
---
Hallo Julian,
ein wirklich klasse, schönes und bestimmt spielstabiles  Modell, mein Sohn und ich würden uns am liebsten die Anleitung dazu kaufen. Deine Bilder haben eine gute Qualität, viele Perspektiven und danke auch für das Bild vom Outdoor Einsatz. :)

*Hast du evtl. Bilder vom Aufbau?
*Sind die Spezialteile von TST und NBGer ?

Viele Grüße aus Düsseldorf
Kai