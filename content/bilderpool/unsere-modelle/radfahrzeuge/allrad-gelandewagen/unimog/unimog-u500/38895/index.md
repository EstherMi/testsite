---
layout: "image"
title: "unimog U500 von Unten"
date: "2014-06-01T15:26:33"
picture: "Foto_7.jpg"
weight: "14"
konstrukteure: 
- "Julian Bußmeier"
fotografen:
- "Julian Bußmeier"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "JulianBussmeier"
license: "unknown"
legacy_id:
- details/38895
imported:
- "2019"
_4images_image_id: "38895"
_4images_cat_id: "2908"
_4images_user_id: "1474"
_4images_image_date: "2014-06-01T15:26:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38895 -->
Man erkennt deutlich den Aufbau der Achsen und des Antriebes.