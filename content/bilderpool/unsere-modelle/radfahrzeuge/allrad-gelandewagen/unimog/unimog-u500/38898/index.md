---
layout: "image"
title: "Unimog U500, Hinterachse Detail"
date: "2014-06-01T18:57:44"
picture: "Foto_15.jpg"
weight: "17"
konstrukteure: 
- "Julian Bußmeier"
fotografen:
- "Julian Bußmeier"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "JulianBussmeier"
license: "unknown"
legacy_id:
- details/38898
imported:
- "2019"
_4images_image_id: "38898"
_4images_cat_id: "2908"
_4images_user_id: "1474"
_4images_image_date: "2014-06-01T18:57:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38898 -->
Durch die hohe Belastung waren 2 Federn pro Rad nötig.