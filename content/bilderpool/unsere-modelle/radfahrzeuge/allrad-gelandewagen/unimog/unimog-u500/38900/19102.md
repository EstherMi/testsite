---
layout: "comment"
hidden: true
title: "19102"
date: "2014-06-02T07:27:12"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Die Lenkung gefällt mir. Ist zwar etwas zu schmächtig für ein so schweres Fahrzeug, aber die Technik ist "da". Zu den gekappten Differenzialen werden sich vielleicht noch andere äußern (ich sach nix!! :-)) ).
Die Kombination aus Parallelogrammführung fürs Radlager und einem einzelnen Gelenk für die Achsführung (Pendelachse, http://de.wikipedia.org/wiki/Pendelachse ) geht nur auf, wenn die Achse im Lager pendeln kann, d.h. ohne den Winkelstein 15 mit seiner dünnen Wand geht das nicht zusammen. Man sieht es auch beim Bild http://ftcommunity.de/details.php?image_id=38890 : beim Einfedern kippt das Rad um den Mittelpunkt des Kardangelenks, d.h. es schwenkt auf einer Kreisbahn und steht deshalb nur in einer Lage senkrecht auf dem Boden. Beim Einfedern gibt es X-Beine und O-Beine beim Ausfedern -- der alte VW-Käfer lässt grüßen.

Gruß,
Harald