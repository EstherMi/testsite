---
layout: "image"
title: "Unimog Abtriebswellen"
date: "2007-09-18T11:29:50"
picture: "PICT5728.jpg"
weight: "36"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/11829
imported:
- "2019"
_4images_image_id: "11829"
_4images_cat_id: "1065"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:29:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11829 -->
Druckluft, Antrieb, und eine dritte Welle in zwei Geschwindigkeiten, alle ferngesteuert.