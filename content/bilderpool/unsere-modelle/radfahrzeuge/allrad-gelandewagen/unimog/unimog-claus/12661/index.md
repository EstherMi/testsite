---
layout: "image"
title: "modellevonclauswludwig01.jpg"
date: "2007-11-13T17:28:58"
picture: "modellevonclauswludwig01.jpg"
weight: "1"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/12661
imported:
- "2019"
_4images_image_id: "12661"
_4images_cat_id: "1065"
_4images_user_id: "127"
_4images_image_date: "2007-11-13T17:28:58"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12661 -->
