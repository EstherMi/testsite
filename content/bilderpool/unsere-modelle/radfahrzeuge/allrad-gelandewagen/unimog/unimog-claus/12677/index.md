---
layout: "image"
title: "modellevonclauswludwig17.jpg"
date: "2007-11-13T17:28:59"
picture: "modellevonclauswludwig17.jpg"
weight: "17"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/12677
imported:
- "2019"
_4images_image_id: "12677"
_4images_cat_id: "1065"
_4images_user_id: "127"
_4images_image_date: "2007-11-13T17:28:59"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12677 -->
