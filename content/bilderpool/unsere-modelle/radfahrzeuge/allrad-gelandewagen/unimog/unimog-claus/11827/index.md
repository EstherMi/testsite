---
layout: "image"
title: "Unimog Frontscheibe"
date: "2007-09-18T11:28:06"
picture: "PICT5726.jpg"
weight: "34"
konstrukteure: 
- "Claus-Werner Ludwig"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/11827
imported:
- "2019"
_4images_image_id: "11827"
_4images_cat_id: "1065"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:28:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11827 -->
