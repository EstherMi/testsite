---
layout: "image"
title: "cu062.JPG"
date: "2007-09-21T20:34:17"
picture: "cu062.JPG"
weight: "28"
konstrukteure: 
- "Claus-Werner Ludwig"
fotografen:
- "Harald Steinhaus"
keywords: ["modding"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/11899
imported:
- "2019"
_4images_image_id: "11899"
_4images_cat_id: "1065"
_4images_user_id: "4"
_4images_image_date: "2007-09-21T20:34:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11899 -->
An der Vorderachse wurde das eine oder andere Teil mit dem Teppichmesser bearbeitet. Oben im Bild sieht man den eingeklappten Motor mit Hubgetriebe (schwarz/schwarz) für Frontanbaugeräte. Dahinter sieht man den quer eingebauten Motor für die Lenkung (von Hand rot lackiert).