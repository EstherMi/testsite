---
layout: "image"
title: "cu058.JPG"
date: "2007-09-21T20:27:52"
picture: "cu058.JPG"
weight: "27"
konstrukteure: 
- "Claus-Werner Ludwig"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/11898
imported:
- "2019"
_4images_image_id: "11898"
_4images_cat_id: "1065"
_4images_user_id: "4"
_4images_image_date: "2007-09-21T20:27:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11898 -->
Die Hinterachse sieht sehr nach "heavy-duty" aus. Die Vorderachse auch, aber hier musste mit etwas Kleber nachgeholfen werden.

Was man nicht sehen kann: Claus verbaut ft-Traktorreifen INNERHALB der dicken Conrad-Reifen. Dadurch drücken sich diese unter dem Fahrzeuggewicht nicht mehr platt.