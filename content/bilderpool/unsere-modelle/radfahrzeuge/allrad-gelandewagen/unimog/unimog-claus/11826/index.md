---
layout: "image"
title: "Unimog Batteriefach"
date: "2007-09-18T11:27:47"
picture: "PICT5725.jpg"
weight: "33"
konstrukteure: 
- "Claus-Werner Ludwig"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/11826
imported:
- "2019"
_4images_image_id: "11826"
_4images_cat_id: "1065"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:27:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11826 -->
