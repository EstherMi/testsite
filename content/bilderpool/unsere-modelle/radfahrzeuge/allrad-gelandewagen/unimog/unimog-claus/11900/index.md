---
layout: "image"
title: "cu064.JPG"
date: "2007-09-21T20:36:32"
picture: "cu064.JPG"
weight: "29"
konstrukteure: 
- "Claus-Werner Ludwig"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/11900
imported:
- "2019"
_4images_image_id: "11900"
_4images_cat_id: "1065"
_4images_user_id: "4"
_4images_image_date: "2007-09-21T20:36:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11900 -->
Blick auf den Kompressor und die Akkus. Der Kompressor versorgt die Anschlüsse direkt, ohne Speicher oder Ventile.