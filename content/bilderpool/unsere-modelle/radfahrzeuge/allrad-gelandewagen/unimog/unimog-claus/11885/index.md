---
layout: "image"
title: "cu049.JPG"
date: "2007-09-21T20:19:04"
picture: "cu049.JPG"
weight: "24"
konstrukteure: 
- "Claus-Werner Ludwig"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/11885
imported:
- "2019"
_4images_image_id: "11885"
_4images_cat_id: "1065"
_4images_user_id: "4"
_4images_image_date: "2007-09-21T20:19:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11885 -->
Unter der vorderen Stoßstange gibt es 
- zwei Steckdosen für Anbaugeräte, 
- einen Motor mit Hubgetriebe (im Moment flach eingeklappt), 
- einen Druckluftanschluss (der Kompressor sitzt zwischen den linken Rädern in der roten Box mit Klarsichtdeckel).
Über der Stoßstange:
- zwei Zapfwellen