---
layout: "image"
title: "cu051.JPG"
date: "2007-09-21T20:21:32"
picture: "cu051.JPG"
weight: "25"
konstrukteure: 
- "Claus-Werner Ludwig"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/11896
imported:
- "2019"
_4images_image_id: "11896"
_4images_cat_id: "1065"
_4images_user_id: "4"
_4images_image_date: "2007-09-21T20:21:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11896 -->
Mittendrin die Box mit dem Kompressor (von Lemo-Solar). Durch die Gelenke am Führerhaus kann die Frontscheibe nach vorn und das Dach nach hinten heruntergeklappt werden.

Alle drei Bordwände können heruntergeklappt werden. Die Riegel waren einmal L-Laschen und haben jetzt eine kleine Ausnehmung.