---
layout: "image"
title: "Mini-Unimog 7"
date: "2007-04-28T15:22:26"
picture: "miniunimog7.jpg"
weight: "13"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/10180
imported:
- "2019"
_4images_image_id: "10180"
_4images_cat_id: "921"
_4images_user_id: "502"
_4images_image_date: "2007-04-28T15:22:26"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10180 -->
Da die Ritzel der Motoren ein wenig abstand zum Differential haben fährt er sehr leichtgängig und schnell.