---
layout: "image"
title: "Mini-Unimog 10"
date: "2007-05-07T18:38:28"
picture: "miniunimog1_2.jpg"
weight: "1"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/10350
imported:
- "2019"
_4images_image_id: "10350"
_4images_cat_id: "921"
_4images_user_id: "502"
_4images_image_date: "2007-05-07T18:38:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10350 -->
Der Unimog wird jetzt mit dem rechten Rad gelenkt, wie bei einer richtigen Lenkung. Sie wird mit Poits gesteuert sowie die Geschwindigkeit des Fehrzeugs mit dem linken Zahrad.