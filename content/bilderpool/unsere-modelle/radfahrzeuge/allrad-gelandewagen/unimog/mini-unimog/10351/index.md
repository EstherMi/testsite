---
layout: "image"
title: "Mini-Unimog 11"
date: "2007-05-07T18:38:28"
picture: "miniunimog2_2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/10351
imported:
- "2019"
_4images_image_id: "10351"
_4images_cat_id: "921"
_4images_user_id: "502"
_4images_image_date: "2007-05-07T18:38:28"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10351 -->
