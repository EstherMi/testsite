---
layout: "image"
title: "Mini-Unimog 15"
date: "2007-05-07T18:38:28"
picture: "miniunimog6_2.jpg"
weight: "6"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/10355
imported:
- "2019"
_4images_image_id: "10355"
_4images_cat_id: "921"
_4images_user_id: "502"
_4images_image_date: "2007-05-07T18:38:28"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10355 -->
