---
layout: "image"
title: "Mini-Unimog 3"
date: "2007-04-28T15:22:26"
picture: "miniunimog3.jpg"
weight: "9"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/10176
imported:
- "2019"
_4images_image_id: "10176"
_4images_cat_id: "921"
_4images_user_id: "502"
_4images_image_date: "2007-04-28T15:22:26"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10176 -->
