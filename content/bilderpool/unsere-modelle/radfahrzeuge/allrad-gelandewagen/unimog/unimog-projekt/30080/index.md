---
layout: "image"
title: "Unimog 14"
date: "2011-02-18T23:20:35"
picture: "Unimog_14.jpg"
weight: "48"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/30080
imported:
- "2019"
_4images_image_id: "30080"
_4images_cat_id: "2216"
_4images_user_id: "328"
_4images_image_date: "2011-02-18T23:20:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30080 -->
Die Vorderachse im Detail von unten.

3 Federn pro Rad halten den extrem schweren Vorbau. Das Servo liegt knapp über dem Differenzial und hat überraschend wenig Probleme, das schwere Fahrzeug auch unter widrigen Umständen zu lenken.

Die Bodenfreiheit des gesamten Fahrzeugs ist vergleichsweise enorm. Mein Ziel war, dass die Unterkante des Differenzials der tiefste Punkt des ganzen Fahrzeugs ist - erreicht!

Gut zu sehen ist der bekannte Kegelradsatz, allerdings hier mit den Kegelzahnrädern mit Rastachse (35061). Die damit verbundenen Probleme hatte ich bereits unter Bild 1 beschrieben. Thema Gewicht ...

Die ganze Vorderachse ist extrem stabil gebaut - es kann sich unter Last nichts ungewollt verschieben!