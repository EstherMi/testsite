---
layout: "comment"
hidden: true
title: "13696"
date: "2011-02-25T21:17:19"
uploadBy:
- "Marten70"
license: "unknown"
imported:
- "2019"
---
Super Modell!  Sofort als Unimog erkennbar, schöne Lösungen!

Das Problem mit durchdrehende Metallachsen kenne ich leider auch, ist FT konstruktionsbedingt leider, gleich wie das verschieben der Bausteine bei Belastung. Man kann nicht alles haben :(

Mit deiner Lösung für Vorderradantrieb habe ich auch experimentiert. Ich habe radseitig aber die Rastkegelräder benutzt, weil ich die Räder gleich auf die Achse montieren wollte aus Steifigkeitsgründen. Nachteil: extra 3mm Baustein notwendig -> mehr Probleme mit Ausrichtung und Fixation. Na ja :(
Vielleicht könntest du mit einige Statikstreben und Statikadapter (mittig im Bild) die Unterseite beim Kegelradsatz versteifen: längere Metallaschse und ein extra weißes Kegelrad. Baut kaum tiefer, aber leichter. Ich bin gespannt auf das Resultat!