---
layout: "image"
title: "Unimog 13"
date: "2011-02-18T23:20:35"
picture: "Unimog_13.jpg"
weight: "47"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/30079
imported:
- "2019"
_4images_image_id: "30079"
_4images_cat_id: "2216"
_4images_user_id: "328"
_4images_image_date: "2011-02-18T23:20:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30079 -->
Die Hinterachse ist extrem stabil (und schwer) gebaut.

Unten im Bild der Akku, oben die leere Kassette.