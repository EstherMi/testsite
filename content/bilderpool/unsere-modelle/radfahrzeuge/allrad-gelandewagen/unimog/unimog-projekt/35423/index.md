---
layout: "image"
title: "Unimog V2 01"
date: "2012-09-03T10:24:20"
picture: "Unimog_01_2.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/35423
imported:
- "2019"
_4images_image_id: "35423"
_4images_cat_id: "2216"
_4images_user_id: "328"
_4images_image_date: "2012-09-03T10:24:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35423 -->
Beim Sichten meiner FT-Fotos ist mir aufgefallen, dass ich die finale Evolutionsstufe meines großen Unimogs noch gar nicht auf der ftc hochgeladen hatte ... Daher hier die (mittlerweile 1 Jahr alten) Fotos des optimierten Modells.

Ich hatte ja angedeutet, dass der ursprüngliche Stand deutliche Schwächen aufwies: Das Gewicht und die Reibung im gesamten Antriebsstrang waren viel zu hoch und die aufwendige Differenzialsperre nahezu wirkungslos! Daher habe ich folgende Änderungen umgesetzt:

- neues und wesentlich leichteres Führerhaus
- wo es möglich war, leichte Statiksteine statt Grundbausteine
- vereinfachte Elektrik ohne separaten Schalter fürs Licht
- leichtere Hinterachse
- reibungsarme Statiksteine als Lager der Antriebswellen
- Differenzialsperre raus

Das Ergebnis sind ein ca. 0,5 kg geringeres Gewicht (1,7 statt 2,2 kg) und eine deutlich bessere Performance im Gelände! Somit konnte ich auch die Anzahl der Federn pro Achse von 6 auf 4 reduzieren.

Trotzdem war ich letztendlich nicht hundertprozentig zufrieden: Leichte Hindernisse und auch große Steigungen ließen sich mit Schwung jederzeit problemlos meistern. Stand das Fahrzeug aber direkt vor einem Hindernis, war es aus dem Stand nicht zu bewältigen. Die großen Räder stellen für die Motoren offensichtlich ein großes Problem dar, und der Empfänger schaltet einfach ab ...