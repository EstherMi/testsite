---
layout: "image"
title: "Unimog 02"
date: "2011-02-18T23:20:35"
picture: "Unimog_02.jpg"
weight: "36"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/30068
imported:
- "2019"
_4images_image_id: "30068"
_4images_cat_id: "2216"
_4images_user_id: "328"
_4images_image_date: "2011-02-18T23:20:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30068 -->
Der Akku ist auf der rechten Seite (hier nicht im Bild). Für eine symmetrische Optik sorgt eine leere rote Kassette.