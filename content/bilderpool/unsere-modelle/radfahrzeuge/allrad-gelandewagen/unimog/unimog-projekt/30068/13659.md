---
layout: "comment"
hidden: true
title: "13659"
date: "2011-02-21T07:59:01"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Danke, Harald, für Deine Tipps!

Ich habe ein nettes Abspeck-Wochenende hinter mir, und zum Leidwesen meiner Freundin nicht an MIR, sondern nur am Fahrzeug, hihi.

Das Führerhaus war in der ersten Version wirklich schwerer Maschinenbau und ist mittlerweile einer wesentlich leichteren Variante gewichen! Auch in der Motorenaufhängung, dem Rahmen und den Achsen steckte tatsächlich extrem viel Gewichtspotenzial.

Aber einen richtigen großen Sprung habe ich bzgl. Reibungsoptimierung gemacht. Die Bausteine 30 mit Loch, die ich hauptsächlich zur Lagerung des Antriebs genommen hatte, hatten alle deutlichen Abrieb an den Kunststoffachsen erzeugt. Das hatte ich schon bei anderen Modellen - so richtig gute Lager scheinen das nicht zu sein. Ich nutze nun Statikstreben als Lager und - siehe da - auf einmal sieht die Welt viel besser aus! Erste Versuche auf der Norm-Rampe waren jedenfalls sehr vielversprechend! :o)

Wirkte das Modell in der ersten Version wie ein träger Büffel, ist es nun eine leichte Gazelle. Ihr werdet sehen ...

Gruß, Thomas