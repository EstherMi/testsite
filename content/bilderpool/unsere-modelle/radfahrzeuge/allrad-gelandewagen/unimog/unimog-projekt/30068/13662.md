---
layout: "comment"
hidden: true
title: "13662"
date: "2011-02-21T19:15:02"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Nur zu, dann ist's bis zum Rehlein auch nicht mehr weit :-)

Bei den BS30-Loch, insbesondere den neueren mit dem eckigen Loch, habe ich auch fast mehr Schatten als Licht. Bei einigen war es nicht mal möglich, Metallachse nur durch ihr eigenes Gewicht hinein zu bringen. Da musste dann eine Reibahle ran.

Gruß,
Harald