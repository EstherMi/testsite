---
layout: "image"
title: "Unimog V2 24"
date: "2012-09-03T10:24:21"
picture: "Unimog_26.jpg"
weight: "24"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/35446
imported:
- "2019"
_4images_image_id: "35446"
_4images_cat_id: "2216"
_4images_user_id: "328"
_4images_image_date: "2012-09-03T10:24:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35446 -->
Das Führerhaus abgenommen. Man sieht nun gut die Kraftübertagung der beiden Power-Motoren auf das schwarze Z20, das wiederrum das rote Z20 darunter antreibt, das auf der Welle des Mittel-Differenzials sitzt. Als Lagerung dienen wieder Statiksteine, um die Reibung zu verringern.