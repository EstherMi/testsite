---
layout: "image"
title: "Unimog 15"
date: "2011-02-18T23:20:35"
picture: "Unimog_15.jpg"
weight: "49"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/30081
imported:
- "2019"
_4images_image_id: "30081"
_4images_cat_id: "2216"
_4images_user_id: "328"
_4images_image_date: "2011-02-18T23:20:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30081 -->
Die Frontansicht. Ich denke, man sieht, was mein Arbeitgeber ist. ;o)

Die inneren Federn sitzen direkt auf den Drehachsen, sind daher ohne Federboden. Die Klemmbuchsen auf den Achsen sorgen dafür, dass sich die Federn nicht verschieben können. Um die fehlenden 5 mm für den fehlenden Federboden auzugleichen, habe ich die kleinen Hülsen mit Scheibe (35981) verbaut. Vorteil ist, dass ich so pro Seite eine Gelenkwürfel-Zunge (31426) leicht drunter schieben kann, um so ein Verschieben der gesamten Radaufhängung außen unter Last zu vermeiden. Ein Rattern des äußeren Kegelrades wird so unmöglich.