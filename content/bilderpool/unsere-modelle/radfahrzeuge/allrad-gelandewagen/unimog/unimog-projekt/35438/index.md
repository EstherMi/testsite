---
layout: "image"
title: "Unimog V2 16"
date: "2012-09-03T10:24:20"
picture: "Unimog_18.jpg"
weight: "16"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/35438
imported:
- "2019"
_4images_image_id: "35438"
_4images_cat_id: "2216"
_4images_user_id: "328"
_4images_image_date: "2012-09-03T10:24:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35438 -->
Die Hinterachse habe ich stark vereinfacht, ohne die Funktion einzuschränken. Sie ist nach wie vor extrem stabil.