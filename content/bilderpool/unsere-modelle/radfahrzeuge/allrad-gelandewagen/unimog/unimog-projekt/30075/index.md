---
layout: "image"
title: "Unimog 09"
date: "2011-02-18T23:20:35"
picture: "Unimog_10.jpg"
weight: "43"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/30075
imported:
- "2019"
_4images_image_id: "30075"
_4images_cat_id: "2216"
_4images_user_id: "328"
_4images_image_date: "2011-02-18T23:20:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30075 -->
Blick auf die Differenzialsperre.

Der XS-Motor kann die Grundbausteine an der Feder über ein Hubgetriebe um einige Millimeter verschieben. Endschalter begrenzen den Weg. Die Feder dient dazu, ein ungewünschtes Verspannen zu vermeiden, wenn die Zahnräder, die ineinander rasten sollen, gerade mal nicht passend zueinander stehen. Das Hubgetriebe ist dann bereits in seiner Endlage, die Sperre aber noch nicht. Passen die Zähne zusammen, dann rastet die Feder die Sperre ein.

Oben rechts im Bild noch mal eine der vier Aufhängungen der Ladefläche.