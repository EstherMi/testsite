---
layout: "image"
title: "max. Einfederung"
date: "2014-08-24T10:52:09"
picture: "unimog21.jpg"
weight: "21"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- details/39283
imported:
- "2019"
_4images_image_id: "39283"
_4images_cat_id: "2938"
_4images_user_id: "1924"
_4images_image_date: "2014-08-24T10:52:09"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39283 -->
Es würde zwar mehr gehen, dann würde sich aber das Chassis verbiegen.