---
layout: "image"
title: "Portalachse-Detail"
date: "2014-08-24T10:52:09"
picture: "unimog18.jpg"
weight: "18"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- details/39280
imported:
- "2019"
_4images_image_id: "39280"
_4images_cat_id: "2938"
_4images_user_id: "1924"
_4images_image_date: "2014-08-24T10:52:09"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39280 -->
