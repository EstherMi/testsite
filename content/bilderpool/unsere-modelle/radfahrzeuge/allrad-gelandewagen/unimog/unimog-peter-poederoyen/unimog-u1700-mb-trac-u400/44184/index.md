---
layout: "image"
title: "U1300"
date: "2016-08-12T16:24:18"
picture: "unimogumbtracu10.jpg"
weight: "29"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/44184
imported:
- "2019"
_4images_image_id: "44184"
_4images_cat_id: "3266"
_4images_user_id: "22"
_4images_image_date: "2016-08-12T16:24:18"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44184 -->
Ohne Akku-block

Link zum Details :

https://www.ftcommunity.de/categories.php?cat_id=1955