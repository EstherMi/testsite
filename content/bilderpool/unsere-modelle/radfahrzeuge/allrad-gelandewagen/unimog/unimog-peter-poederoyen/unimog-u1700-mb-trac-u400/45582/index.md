---
layout: "image"
title: "Eisenbahn-Draisine"
date: "2017-03-19T16:36:53"
picture: "unimogfuerrangierarbeiteneisenbahndraisine18.jpg"
weight: "19"
konstrukteure: 
- "Peter Peoderoyen"
fotografen:
- "Peter Peoderoyen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/45582
imported:
- "2019"
_4images_image_id: "45582"
_4images_cat_id: "3266"
_4images_user_id: "104"
_4images_image_date: "2017-03-19T16:36:53"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45582 -->
Unten