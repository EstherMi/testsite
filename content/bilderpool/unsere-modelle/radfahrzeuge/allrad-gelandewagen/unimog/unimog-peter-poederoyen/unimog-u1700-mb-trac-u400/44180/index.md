---
layout: "image"
title: "MB-trac"
date: "2016-08-12T16:24:18"
picture: "unimogumbtracu06.jpg"
weight: "25"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/44180
imported:
- "2019"
_4images_image_id: "44180"
_4images_cat_id: "3266"
_4images_user_id: "22"
_4images_image_date: "2016-08-12T16:24:18"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44180 -->
