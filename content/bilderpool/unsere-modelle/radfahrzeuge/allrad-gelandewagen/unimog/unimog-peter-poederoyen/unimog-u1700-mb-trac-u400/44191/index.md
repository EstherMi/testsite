---
layout: "image"
title: "U400"
date: "2016-08-12T16:24:18"
picture: "unimogumbtracu17.jpg"
weight: "36"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/44191
imported:
- "2019"
_4images_image_id: "44191"
_4images_cat_id: "3266"
_4images_user_id: "22"
_4images_image_date: "2016-08-12T16:24:18"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44191 -->
Allradantrieb auf "Portalachsen" vorne und hinten zum hohe Bodenfreiheit.