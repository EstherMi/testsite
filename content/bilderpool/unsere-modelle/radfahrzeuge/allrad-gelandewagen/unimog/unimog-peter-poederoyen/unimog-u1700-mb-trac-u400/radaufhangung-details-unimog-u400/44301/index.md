---
layout: "image"
title: "Radaufhängung -Details"
date: "2016-08-13T18:13:24"
picture: "radaufhaengungdetailsunimogu3.jpg"
weight: "3"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/44301
imported:
- "2019"
_4images_image_id: "44301"
_4images_cat_id: "3268"
_4images_user_id: "22"
_4images_image_date: "2016-08-13T18:13:24"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44301 -->
Rechts oben ein grauer Baustein 15 mit Senkung + M4 Gewinde + feste Achse + Z10 ohne Antrieb-Funktion.
Untere Z10 dient zum Antrieb Innen-Z30 .
Gelenkwürfel-Zunge 31426 durch bohrt + M4 Befestigung