---
layout: "image"
title: "U400"
date: "2016-08-12T16:24:18"
picture: "unimogumbtracu19.jpg"
weight: "38"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/44193
imported:
- "2019"
_4images_image_id: "44193"
_4images_cat_id: "3266"
_4images_user_id: "22"
_4images_image_date: "2016-08-12T16:24:18"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44193 -->
Detail -unten

Allradantrieb auf "Portalachsen" vorne und hinten zum hohe Bodenfreiheit.