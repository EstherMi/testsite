---
layout: "image"
title: "Unimog als Zweiwegefahrzeug für Rangierarbeiten"
date: "2017-03-19T14:19:17"
picture: "unimogfuerrangierarbeiteneisenbahndraisine03.jpg"
weight: "4"
konstrukteure: 
- "Peter Peoderoyen"
fotografen:
- "Peter Peoderoyen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/45567
imported:
- "2019"
_4images_image_id: "45567"
_4images_cat_id: "3266"
_4images_user_id: "104"
_4images_image_date: "2017-03-19T14:19:17"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45567 -->
Auch bei den Eisenbahnen werden sie häufig als Zweiwegefahrzeuge beispielsweise für Rangierarbeiten eingesetzt.       
https://de.wikipedia.org/wiki/Unimog
http://www.unimog-museum.com/
