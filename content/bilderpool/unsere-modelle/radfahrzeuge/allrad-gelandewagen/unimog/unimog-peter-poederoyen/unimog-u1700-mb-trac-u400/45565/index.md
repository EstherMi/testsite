---
layout: "image"
title: "Unimog als Zweiwegefahrzeug für Rangierarbeiten"
date: "2017-03-19T14:19:17"
picture: "unimogfuerrangierarbeiteneisenbahndraisine01.jpg"
weight: "1"
konstrukteure: 
- "Peter Peoderoyen"
fotografen:
- "Peter Peoderoyen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/45565
imported:
- "2019"
_4images_image_id: "45565"
_4images_cat_id: "3266"
_4images_user_id: "104"
_4images_image_date: "2017-03-19T14:19:17"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45565 -->
Unimog ist ein Akronym für Universal-Motor-Gerät und eine eingetragene Marke der Daimler AG. Deren Geschäftsfeld Daimler Trucks stellt unter diesem Begriff allradgetriebene Geräteträger und Klein-Lkw vor allem für die Land- und Forstwirtschaft, das Militär und für kommunale Aufgaben her. Einige Unimogmodelle sind klassische Dual-Use-Güter und werden auch für andere Aufgaben in unwegsamem Gelände, z. B. als Bohrfahrzeug oder bei der Katastrophenhilfe verwendet. Die Fahrzeuge sind als "Frontsitztraktor mit Allradantrieb" klassifiziert. Die seit 1945 entwickelten Fahrzeuge wurden ab 1949 zunächst in Göppingen bei Boehringer Werkzeugmaschinen gebaut. Nach der Übernahme der Produktion durch den bisherigen Motorenlieferanten Daimler-Benz im Jahr 1951 war Unimog eine Modellbezeichnung von Mercedes-Benz. Mehr als 50 Jahre lang wurde der Unimog im Werk Gaggenau gefertigt, bevor die Produktion 2002 ins Mercedes-Benz-Werk Wörth wechselte. Neben dem traditionellen Einsatz in der Landwirtschaft und der Forstwirtschaft werden Unimogs auch beim Militär (Typ Unimog S mit Sechszylinder-Benzinmotor) und bei Feuerwehren, THW und anderen Hilfsorganisationen eingesetzt. 
Weitere typische Einsatzgebiete des Unimogs sind die Versorgung von Berghütten und in Straßenmeistereien und Gemeinden, gerade auch in den Alpenländern. Auch bei den Eisenbahnen werden sie häufig als Zweiwegefahrzeuge beispielsweise für Rangierarbeiten eingesetzt.