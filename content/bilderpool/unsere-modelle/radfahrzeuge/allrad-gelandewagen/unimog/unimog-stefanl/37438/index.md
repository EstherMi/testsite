---
layout: "image"
title: "Unimog 05"
date: "2013-09-29T13:29:22"
picture: "DSCN0912.jpg"
weight: "8"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/37438
imported:
- "2019"
_4images_image_id: "37438"
_4images_cat_id: "2783"
_4images_user_id: "502"
_4images_image_date: "2013-09-29T13:29:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37438 -->
