---
layout: "image"
title: "Bedienelemente"
date: "2014-04-27T16:09:00"
picture: "unimog04.jpg"
weight: "4"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- details/38671
imported:
- "2019"
_4images_image_id: "38671"
_4images_cat_id: "2889"
_4images_user_id: "1635"
_4images_image_date: "2014-04-27T16:09:00"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38671 -->
Die Ventile steuern den Kran hoch/runter, die Taster links daneben drehen ihn. Der Taster oben bedient den Elektromagnet zum Festhalten der Gegensrände.