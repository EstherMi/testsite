---
layout: "image"
title: "Von der Seite"
date: "2014-04-27T16:09:00"
picture: "unimog07.jpg"
weight: "7"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- details/38674
imported:
- "2019"
_4images_image_id: "38674"
_4images_cat_id: "2889"
_4images_user_id: "1635"
_4images_image_date: "2014-04-27T16:09:00"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38674 -->
Der Batteriekasten ist nur zur Zierde.