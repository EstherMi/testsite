---
layout: "image"
title: "Unimog 8"
date: "2006-12-29T22:04:52"
picture: "unimogfastfertig8.jpg"
weight: "8"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/8195
imported:
- "2019"
_4images_image_id: "8195"
_4images_cat_id: "756"
_4images_user_id: "502"
_4images_image_date: "2006-12-29T22:04:52"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8195 -->
