---
layout: "image"
title: "Unimog 1"
date: "2006-12-29T22:04:51"
picture: "unimogfastfertig1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/8188
imported:
- "2019"
_4images_image_id: "8188"
_4images_cat_id: "756"
_4images_user_id: "502"
_4images_image_date: "2006-12-29T22:04:51"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8188 -->
Das Teil wiegt 3 kilo. Der Minimot tut sich noch ein bisschen schwer mit der Lenkung. Sonst bin ich ganz zufrieden damit.