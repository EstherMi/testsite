---
layout: "image"
title: "Unimog 7"
date: "2007-01-02T14:58:38"
picture: "unimog07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/8267
imported:
- "2019"
_4images_image_id: "8267"
_4images_cat_id: "763"
_4images_user_id: "502"
_4images_image_date: "2007-01-02T14:58:38"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8267 -->
Zu sehen sind die zwei Powermotoren 50:1 für den Radantrieb und der Minimotor für den Kipper.