---
layout: "image"
title: "Unimog 4"
date: "2007-01-02T14:58:38"
picture: "unimog04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/8264
imported:
- "2019"
_4images_image_id: "8264"
_4images_cat_id: "763"
_4images_user_id: "502"
_4images_image_date: "2007-01-02T14:58:38"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8264 -->
Halb oben.