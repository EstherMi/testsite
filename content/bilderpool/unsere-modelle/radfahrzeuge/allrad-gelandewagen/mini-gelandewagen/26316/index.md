---
layout: "image"
title: "Mini-Geländewagen 9"
date: "2010-02-10T18:54:41"
picture: "Gelndewagen_12.jpg"
weight: "9"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/26316
imported:
- "2019"
_4images_image_id: "26316"
_4images_cat_id: "1871"
_4images_user_id: "328"
_4images_image_date: "2010-02-10T18:54:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26316 -->
Ansicht von oben bei abgenommener Fronthaube und Dach.