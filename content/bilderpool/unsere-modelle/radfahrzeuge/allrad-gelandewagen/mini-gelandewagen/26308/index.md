---
layout: "image"
title: "Mini-Geländewagen 1"
date: "2010-02-10T18:46:22"
picture: "Gelndewagen_01.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/26308
imported:
- "2019"
_4images_image_id: "26308"
_4images_cat_id: "1871"
_4images_user_id: "328"
_4images_image_date: "2010-02-10T18:46:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26308 -->
Hier seht Ihr eine weitere (und sicher nicht die letzte ;o) Evolutionstufe meiner kleinen Drehschemel-Allradler.

Die Vorstufen sind diese beiden Modelle:

http://www.ftcommunity.de/categories.php?cat_id=730

http://www.ftcommunity.de/categories.php?cat_id=1194

Nur war ich mit der Technik und dem Design noch nicht ganz zufrieden. Mein neuer Mini-Geländewagen bietet daher mehr Technik (Licht vorn und hinten, ferngesteuert mit aktuellem Control Set, Starthebel im Cockpit), besseres PKW-Design und kompaktere Dimensionen. Kürzer düfte diese Art von Antrieb kaum realisierbar sein...

Natürlich sind Drehschemel-Lenkung, Allrad mit 3 Differenzialen, Antrieb per Power-Motor und Akkupack wieder mit an Bord.

Das Fahrverhalten ist in "normalem" maßstabsgetreuen Gelände sehr gut. Die Kraft vom Power-Motor wird gut auf die 4 Räder übertragen, und es geht gut voran.

Das Problem sind stärkere Steigungen, wo dem Fahzeug sein hoher und weit hinten liegender Schwerpunkt durch den Akkupack unterm Dach zum Verhängnis wird. Es kippt dann einfach nach hinten um. Variante 2 (der VW T3 Syncro, siehe oben) war da wesentlich besser, sah aber schlechter aus. Aber irgendwas ist ja immer... ;o)