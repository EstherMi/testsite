---
layout: "image"
title: "Motorblock"
date: "2007-07-01T12:32:57"
picture: "PICT0018.jpg"
weight: "4"
konstrukteure: 
- "timtech"
fotografen:
- "timtech"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "timtech"
license: "unknown"
legacy_id:
- details/11009
imported:
- "2019"
_4images_image_id: "11009"
_4images_cat_id: "996"
_4images_user_id: "590"
_4images_image_date: "2007-07-01T12:32:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11009 -->
Hier sieht man den kompackten Motorblock.