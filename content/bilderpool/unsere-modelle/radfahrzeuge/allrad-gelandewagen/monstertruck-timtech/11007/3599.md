---
layout: "comment"
hidden: true
title: "3599"
date: "2007-07-06T15:54:56"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Beide Power-Motoren sind DIREKT mit einer STARREN Welle verkoppelt und treiben gemeinsam das Mitteldifferenzial an?!? Das ist aber keine saubere Lösung! Die haben doch immer geringfügig unterschiedliche Geschwindigkeiten, so dass sie sich hier gegenseitig verspannen. Nicht schön...

Da gehört ein weiteres Differenzial dazwischen, das eigentlich wunderbar längs unter das Mitteldifferenzial passen würde! Vorn und hinten die beiden Antriebe der Motoren rein, und der Käfig treibt den Käfig des Mitteldifferenzials an!

Gruß, Thomas