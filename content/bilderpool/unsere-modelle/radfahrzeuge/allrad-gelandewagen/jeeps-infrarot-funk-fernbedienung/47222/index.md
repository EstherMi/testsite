---
layout: "image"
title: "Andere Seite des Empfängers mit dem zweiten MX1508"
date: "2018-01-30T16:23:43"
picture: "J10.jpg"
weight: "10"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["MX1508", "Motortreiber"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/47222
imported:
- "2019"
_4images_image_id: "47222"
_4images_cat_id: "3494"
_4images_user_id: "579"
_4images_image_date: "2018-01-30T16:23:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47222 -->
