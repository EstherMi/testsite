---
layout: "image"
title: "Empfänger im Vollausbau mit zweimal MX1508 für 4 Motoren"
date: "2018-01-30T16:23:37"
picture: "J9.jpg"
weight: "9"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Stapelaufbau", "PCB", "Boards"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/47221
imported:
- "2019"
_4images_image_id: "47221"
_4images_cat_id: "3494"
_4images_user_id: "579"
_4images_image_date: "2018-01-30T16:23:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47221 -->
Im Vollausbau sind es vier Board-Ebenen im Stapelaufbau, damit können vier Motroen und zwei Servos angesteuert werden (mit Umschaltung über die DIP Schalter am Sender).