---
layout: "image"
title: "Golden Eagle Details 3"
date: "2015-06-16T21:06:00"
picture: "goldeneagle13.jpg"
weight: "13"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/41196
imported:
- "2019"
_4images_image_id: "41196"
_4images_cat_id: "3084"
_4images_user_id: "2321"
_4images_image_date: "2015-06-16T21:06:00"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41196 -->
... was für den Akkutausch wichtig ist.