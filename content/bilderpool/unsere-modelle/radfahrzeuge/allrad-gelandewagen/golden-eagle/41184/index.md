---
layout: "image"
title: "Golden Eagle Gesamtansicht 1"
date: "2015-06-16T21:06:00"
picture: "goldeneagle01.jpg"
weight: "1"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/41184
imported:
- "2019"
_4images_image_id: "41184"
_4images_cat_id: "3084"
_4images_user_id: "2321"
_4images_image_date: "2015-06-16T21:06:00"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41184 -->
Endlich fertig. Es sollte ursprünglich ein US-Army-Jeep werden, dann ein Jeep Golden Eagle. Es ist aber, wie so häufig, doch eine eigene Komposition. Ein Humvee mischt auch ein wenig mit.
