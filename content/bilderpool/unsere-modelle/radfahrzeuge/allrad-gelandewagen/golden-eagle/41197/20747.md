---
layout: "comment"
hidden: true
title: "20747"
date: "2015-06-17T22:33:20"
uploadBy:
- "Martin Wanke"
license: "unknown"
imported:
- "2019"
---
Vielen Dank, Stefan. Man lernt aber auch immer noch Neues. Dass die Clipplattenverbinder (rechts im Bild) sich auf den Noppen so verschieben lassen, dass genau eine flache Bauplatte drunter passt, war mir zum Beispiel neu.
Viele Grüße,
Martni