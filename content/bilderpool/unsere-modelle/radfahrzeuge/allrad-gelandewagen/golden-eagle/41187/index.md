---
layout: "image"
title: "Golden Eagle Gelände 1"
date: "2015-06-16T21:06:00"
picture: "goldeneagle04.jpg"
weight: "4"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/41187
imported:
- "2019"
_4images_image_id: "41187"
_4images_cat_id: "3084"
_4images_user_id: "2321"
_4images_image_date: "2015-06-16T21:06:00"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41187 -->
Die Vorderachse ist starr, dafür ist die Hinterachse sehr flexibel.