---
layout: "image"
title: "Hebebühne + Gelände-Buggy"
date: "2015-10-18T15:42:43"
picture: "universalfahrzeug5.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/42089
imported:
- "2019"
_4images_image_id: "42089"
_4images_cat_id: "3132"
_4images_user_id: "1359"
_4images_image_date: "2015-10-18T15:42:43"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42089 -->
Aufbau Modellbaukasten der Hebebühne (30456, 1982)  mit Gelände-Buggy (30463, 1982)

hier sieht man mal wieder schön, wie gut alles zusammenpasst . . :-)
