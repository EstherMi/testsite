---
layout: "image"
title: "Rampensau1781.jpg"
date: "2015-06-22T18:53:09"
picture: "Rampe1781_352.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/41203
imported:
- "2019"
_4images_image_id: "41203"
_4images_cat_id: "2213"
_4images_user_id: "4"
_4images_image_date: "2015-06-22T18:53:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41203 -->
Mit ordentlich tief liegendem Schwerpunkt, starken Motoren und reichlich Untersetzung komme ich hier auf arctan(12 / 17) = 35,2° Neigungswinkel. 

Allerdings ist das 'außer Konkurrenz', weil die weißen Felgen mit 6-er Teilung aus dem 3D-Drucker stammen, und die Z10 darin aus Rastkurbeln zurecht-gemoddet worden sind (Details dazu unter "Großreifen und Eigenbau-Felgen", http://ftcommunity.de/categories.php?cat_id=366&page=4 )