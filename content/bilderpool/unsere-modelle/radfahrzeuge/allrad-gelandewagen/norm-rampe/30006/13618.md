---
layout: "comment"
hidden: true
title: "13618"
date: "2011-02-18T20:00:31"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Papperlapapp! Nur weil *ein* Forumsnutzer meint, er müsse da noch seinen Senf dazu geben, musst du nicht auf "die" oder "alle" Nutzer schließen. 

Gruß,
Harald

PS: Außerdem solltest du vielleicht deine Sensoren etwas anders justieren. Das WAR doch schon positive Kritik! "Nicht geschimpft ist schon Lob genug"