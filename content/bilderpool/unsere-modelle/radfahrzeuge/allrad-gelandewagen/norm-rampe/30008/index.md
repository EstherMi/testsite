---
layout: "image"
title: "Norm-Rampe 3"
date: "2011-02-17T21:29:19"
picture: "Norm-Rampe_3.jpg"
weight: "6"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/30008
imported:
- "2019"
_4images_image_id: "30008"
_4images_cat_id: "2213"
_4images_user_id: "328"
_4images_image_date: "2011-02-17T21:29:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30008 -->
Hier die Verstärkungen, die natürlich je nach Modell unterschiedlich gestaltet werden können und müssen. Auch die 2. Rampe kann zur Not entfallen, wenn unter der mittleren Platte senkrechte Abstützungen angebracht werden, die den Steigungswinkel nicht verfälschen.