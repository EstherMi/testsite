---
layout: "image"
title: "kleiner Racker"
date: "2011-02-25T16:19:23"
picture: "IMG_4706.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/30123
imported:
- "2019"
_4images_image_id: "30123"
_4images_cat_id: "2213"
_4images_user_id: "4"
_4images_image_date: "2011-02-25T16:19:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30123 -->
Na, dann will ich mal den Anfang machen :-)

Der kleine Racker da hat drei Motoren (was dem steffalk recht ist, soll mir billig sein), Allradantrieb und ist butterweich gefedert. Die Rampe hat ~35° Neigung, und mit reichlich Schlupf unter den Rädern kommt er da gerade noch hinauf. Rückwärts (wie hier gezeigt) noch besser als vorwärts. Die Metallachsen und Magnete sind Ballast.

Mir ist aber erst hinterher aufgefallen ("wie hat der nur die Winkelsteine da fest gekriegt, die sind doch ... *kopfkratz* "), dass hier die glatte "Nutenseite" der Grundplatte oben ist, während die echte Normrampe die "Kästchenseite" oben hat. Daher kann dieser Versuch nicht in die Wertung der hochgeschätzten Jury eingehen.


So ein Teil hier: http://www.ftcommunity.de/details.php?image_id=26449
sollte sich auch ganz gut auf der Rampe machen. Vielleicht mit kleineren Rädern und mehr Ballast, damit der Schwerpunkt tiefer kommt.


Gruß,
Harald