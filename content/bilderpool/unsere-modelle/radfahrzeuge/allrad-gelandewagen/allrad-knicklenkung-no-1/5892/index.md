---
layout: "image"
title: "Allrad 2"
date: "2006-03-15T21:07:36"
picture: "Allrad_2.jpg"
weight: "2"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/5892
imported:
- "2019"
_4images_image_id: "5892"
_4images_cat_id: "509"
_4images_user_id: "328"
_4images_image_date: "2006-03-15T21:07:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5892 -->
Die Stromversorgung ist bewusst hinten angebracht, damit die Ladefläche über dem Antrieb mit Nutzlast versehen oder z.B. einem Kran ausgestattet werden kann. Ist vom Schwerpunkt her nicht optimal... Aber wenn's mal vorwärts nicht voran geht, dann definitiv rückwärts mit der ganzen Masse das Akkus auf der Achse! ;o)

Die Kette wirkt übrigens auf eine Rastschnecke 35977, die ins mittlere Differenzial greift (Danke an Harald für die tolle Idee!).