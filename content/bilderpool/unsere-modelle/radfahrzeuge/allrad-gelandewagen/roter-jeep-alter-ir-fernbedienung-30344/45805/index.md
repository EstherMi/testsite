---
layout: "image"
title: "Roter Jeep: Ansicht von unten"
date: "2017-05-09T11:00:36"
picture: "Jeep2.jpg"
weight: "2"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["roter", "Jeep"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/45805
imported:
- "2019"
_4images_image_id: "45805"
_4images_cat_id: "3405"
_4images_user_id: "579"
_4images_image_date: "2017-05-09T11:00:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45805 -->
Hab zwei Stück davon in einer Sammlung auf ebay erworben.
Ausstattung: 

- mit Bausteinen 15 und 30 in roter Farbe
- Fahrmotor = Power-Motor mit Differential
- Lenkmotor = S-Motor mit Taster an Mittelstellung
- Stromversorgung = roter NiMH Akku mit 1500 mAh
- alte IR Fernbedienung 30344

Kennt jemand dieses Modell aus irgendeinem Baukasten?
Oder war das mal ein Promotion Modell von Fischertechnik?