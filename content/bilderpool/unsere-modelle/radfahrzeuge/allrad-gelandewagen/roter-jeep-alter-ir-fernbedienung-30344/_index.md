---
layout: "overview"
title: "roter Jeep mit alter IR Fernbedienung 30344"
date: 2019-12-17T18:49:41+01:00
legacy_id:
- categories/3405
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3405 --> 
Hab zwei Stück davon in einer Sammlung auf ebay erworben.
Ausstattung: 

- mit Bausteinen 15 und 30 in roter Farbe
- Fahrmotor = Power-Motor mit Differential
- Lenkmotor = S-Motor mit Taster an Mittelstellung
- Stromversorgung = roter NiMH Akku mit 1500 mAh
- alte IR Fernbedienung 30344

Kennt jemand dieses Modell aus irgendeinem Baukasten?
Oder war das mal ein Promotion Modell von Fischertechnik?