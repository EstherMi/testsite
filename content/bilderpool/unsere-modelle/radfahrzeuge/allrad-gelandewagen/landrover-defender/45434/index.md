---
layout: "image"
title: "landrover45.jpg"
date: "2017-03-03T21:38:24"
picture: "landrover45.jpg"
weight: "55"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/45434
imported:
- "2019"
_4images_image_id: "45434"
_4images_cat_id: "3378"
_4images_user_id: "2449"
_4images_image_date: "2017-03-03T21:38:24"
_4images_image_order: "45"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45434 -->
Jetzt mussen auch die Kabel geleitet werden.