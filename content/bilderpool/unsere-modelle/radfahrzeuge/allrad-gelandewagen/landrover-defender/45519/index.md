---
layout: "image"
title: "landrover07.jpg"
date: "2017-03-13T19:57:05"
picture: "landrover07_2.jpg"
weight: "7"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/45519
imported:
- "2019"
_4images_image_id: "45519"
_4images_cat_id: "3378"
_4images_user_id: "2449"
_4images_image_date: "2017-03-13T19:57:05"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45519 -->
Bei Photo 27: Der Lagerung der Hinten-/Antriebsachse ist etwas verstärkt worden mit eine Zuzatsliche Winkelstein