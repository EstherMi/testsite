---
layout: "image"
title: "landrover06.jpg"
date: "2017-03-13T19:57:05"
picture: "landrover06_2.jpg"
weight: "6"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/45518
imported:
- "2019"
_4images_image_id: "45518"
_4images_cat_id: "3378"
_4images_user_id: "2449"
_4images_image_date: "2017-03-13T19:57:05"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45518 -->
Hier ist er Verbaut und Befestigt worden.