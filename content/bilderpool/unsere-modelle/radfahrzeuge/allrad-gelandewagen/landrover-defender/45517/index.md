---
layout: "image"
title: "landrover05.jpg"
date: "2017-03-13T19:57:05"
picture: "landrover05_2.jpg"
weight: "5"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/45517
imported:
- "2019"
_4images_image_id: "45517"
_4images_cat_id: "3378"
_4images_user_id: "2449"
_4images_image_date: "2017-03-13T19:57:05"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45517 -->
Bei Photo 27: Den Neuen Boden im Mitte der Auto. Flacher und deshalb Schöner.