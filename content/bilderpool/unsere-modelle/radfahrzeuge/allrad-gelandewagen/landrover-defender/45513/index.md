---
layout: "image"
title: "landrover01.jpg"
date: "2017-03-13T19:57:05"
picture: "landrover01_2.jpg"
weight: "1"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/45513
imported:
- "2019"
_4images_image_id: "45513"
_4images_cat_id: "3378"
_4images_user_id: "2449"
_4images_image_date: "2017-03-13T19:57:05"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45513 -->
Die Nächtste Photo's zeigen ein Paar Änderungen / Verbesserungen.

Bei Photo 6: Damit die Lagerung der Vorderachse nicht Schiebt, sind 2 Verkleidungplatten 15x15 (hier nur Einen zu sehen) und eine von 15x30 verwendet.