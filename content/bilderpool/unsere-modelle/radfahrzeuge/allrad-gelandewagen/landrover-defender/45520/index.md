---
layout: "image"
title: "landrover08.jpg"
date: "2017-03-13T19:57:05"
picture: "landrover08_2.jpg"
weight: "8"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/45520
imported:
- "2019"
_4images_image_id: "45520"
_4images_cat_id: "3378"
_4images_user_id: "2449"
_4images_image_date: "2017-03-13T19:57:05"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45520 -->
Bei Photo 32: Man kann verzichten aud die 2 Federnocken in die 2 BS15. Statt dessen kan man später an beide Seiten eine Federnocke in die BS15 direkt hinten den Hintensitz schieben, und dann mit 2 Steine 15x15x7,5 den Accu ein festen Platz geben, wie hier gezeigt.