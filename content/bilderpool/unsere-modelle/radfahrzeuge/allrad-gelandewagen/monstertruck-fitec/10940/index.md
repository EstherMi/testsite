---
layout: "image"
title: "Monstertruck"
date: "2007-06-27T18:34:58"
picture: "Monstertruck1.jpg"
weight: "1"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/10940
imported:
- "2019"
_4images_image_id: "10940"
_4images_cat_id: "988"
_4images_user_id: "456"
_4images_image_date: "2007-06-27T18:34:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10940 -->
Das ist mein Monstertruck. Er hat Allrad, Vollfederung und conrad-Reifen.