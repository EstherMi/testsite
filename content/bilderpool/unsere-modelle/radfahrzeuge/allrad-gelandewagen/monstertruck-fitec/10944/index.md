---
layout: "image"
title: "Frontfederung"
date: "2007-06-27T18:35:09"
picture: "Monstertruck5.jpg"
weight: "5"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/10944
imported:
- "2019"
_4images_image_id: "10944"
_4images_cat_id: "988"
_4images_user_id: "456"
_4images_image_date: "2007-06-27T18:35:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10944 -->
Hier sieht man die Frontfederung. Sie ist genauso gebaut wie die Heckfederung.