---
layout: "image"
title: "Jeep01"
date: "2003-04-29T19:07:22"
picture: "Jeep01.jpg"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/985
imported:
- "2019"
_4images_image_id: "985"
_4images_cat_id: "33"
_4images_user_id: "4"
_4images_image_date: "2003-04-29T19:07:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=985 -->
auf dem gleichen Chassis, Lenkung geändert.