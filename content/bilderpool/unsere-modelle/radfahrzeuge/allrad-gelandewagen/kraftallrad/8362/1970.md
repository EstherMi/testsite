---
layout: "comment"
hidden: true
title: "1970"
date: "2007-01-13T09:35:44"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
In Sachen Stabilität: ist das Gefährt auch geländetauglich? Zumindest würde ich den zentralen Gelenkstein rundherum "verbauen", damit auf schiefem Grund kein Zapfen abgedreht werden kann.

Gruß,
Harald