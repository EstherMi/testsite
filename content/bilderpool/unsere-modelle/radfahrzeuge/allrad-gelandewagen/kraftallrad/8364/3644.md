---
layout: "comment"
hidden: true
title: "3644"
date: "2007-07-16T16:49:05"
uploadBy:
- "fitec"
license: "unknown"
imported:
- "2019"
---
Hi,
das kommt auf die Belastung an. Wenn man das Auto ganz stark auf den Boden drückt, wird (wegen des Mitteldifferenzials) alle Kraft nach vorne übertrage. Wenn man es nur ganz leicht anhebt fährt es weiter.

Gruß fitec