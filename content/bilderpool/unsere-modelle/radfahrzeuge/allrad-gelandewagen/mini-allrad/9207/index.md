---
layout: "image"
title: "Mini-Allrad 8"
date: "2007-03-02T08:54:40"
picture: "Mini-Allrad_11.jpg"
weight: "5"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/9207
imported:
- "2019"
_4images_image_id: "9207"
_4images_cat_id: "553"
_4images_user_id: "328"
_4images_image_date: "2007-03-02T08:54:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9207 -->
Der platzraubende Akku ist sehr geschickt mittig unter dem Antrieb positioniert, was einen schön tiefen Schwerpunkt zur Folge hat. Somit wird die Kippneigung im Gelände verringert.