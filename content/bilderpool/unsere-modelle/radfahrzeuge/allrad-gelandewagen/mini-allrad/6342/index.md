---
layout: "image"
title: "Mini-Allrad 2"
date: "2006-06-01T21:29:46"
picture: "Mini-Allrad_06.jpg"
weight: "7"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/6342
imported:
- "2019"
_4images_image_id: "6342"
_4images_cat_id: "553"
_4images_user_id: "328"
_4images_image_date: "2006-06-01T21:29:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6342 -->
Hier der Vorderradantrieb. Die Lenk- und Antriebsachse sind gleich. Vorn wie hinten drückt eine Achse fest von oben auf das Rast-Kegelrad, um dessen Position zu fixieren und ein Herausspringen bei hohen Antriebskräften zu vermeiden. Das gelingt, allerdings kommt es bei starker Belastung häufig zu lautem haarsträubenden Springen der Zähne...