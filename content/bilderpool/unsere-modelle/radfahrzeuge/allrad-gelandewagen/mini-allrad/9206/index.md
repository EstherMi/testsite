---
layout: "image"
title: "Mini-Allrad 7"
date: "2007-03-02T08:54:40"
picture: "Mini-Allrad_04.jpg"
weight: "4"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/9206
imported:
- "2019"
_4images_image_id: "9206"
_4images_cat_id: "553"
_4images_user_id: "328"
_4images_image_date: "2007-03-02T08:54:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9206 -->
Hier in der Seitenansicht sieht man besonders gut, dass mir damals - ich war jung und unerfahren - die Proportionen völlig aus dem Ruder gelaufen sind...