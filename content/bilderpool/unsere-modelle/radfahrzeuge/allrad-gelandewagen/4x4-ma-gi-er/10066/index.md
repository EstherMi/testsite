---
layout: "image"
title: "Vorne"
date: "2007-04-12T10:05:11"
picture: "x01.jpg"
weight: "1"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/10066
imported:
- "2019"
_4images_image_id: "10066"
_4images_cat_id: "911"
_4images_user_id: "445"
_4images_image_date: "2007-04-12T10:05:11"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10066 -->
