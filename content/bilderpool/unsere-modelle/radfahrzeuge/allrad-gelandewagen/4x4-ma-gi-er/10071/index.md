---
layout: "image"
title: "Lenkung unten1"
date: "2007-04-12T10:05:11"
picture: "x06.jpg"
weight: "6"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/10071
imported:
- "2019"
_4images_image_id: "10071"
_4images_cat_id: "911"
_4images_user_id: "445"
_4images_image_date: "2007-04-12T10:05:11"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10071 -->
Optimal ist es nicht, aber es geht in der Geradelage (Normallage geht es nicht).