---
layout: "image"
title: "Knick-4x4-04.JPG"
date: "2006-03-26T12:32:29"
picture: "Knick-4x4-04.JPG"
weight: "15"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5914
imported:
- "2019"
_4images_image_id: "5914"
_4images_cat_id: "513"
_4images_user_id: "4"
_4images_image_date: "2006-03-26T12:32:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5914 -->
