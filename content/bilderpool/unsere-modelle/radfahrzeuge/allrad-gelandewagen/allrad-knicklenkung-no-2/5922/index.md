---
layout: "image"
title: "Knick-4x4-16.JPG"
date: "2006-03-26T12:36:52"
picture: "Knick-4x4-16.JPG"
weight: "23"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5922
imported:
- "2019"
_4images_image_id: "5922"
_4images_cat_id: "513"
_4images_user_id: "4"
_4images_image_date: "2006-03-26T12:36:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5922 -->
