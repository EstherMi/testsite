---
layout: "image"
title: "Knick-4x4b-04.JPG"
date: "2006-04-02T14:01:49"
picture: "Knick-4x4b-04.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/6005
imported:
- "2019"
_4images_image_id: "6005"
_4images_cat_id: "513"
_4images_user_id: "4"
_4images_image_date: "2006-04-02T14:01:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6005 -->
