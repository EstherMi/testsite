---
layout: "image"
title: "Knick-4x4c-01.JPG"
date: "2006-04-02T14:07:39"
picture: "Knick-4x4c-01.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/6007
imported:
- "2019"
_4images_image_id: "6007"
_4images_cat_id: "513"
_4images_user_id: "4"
_4images_image_date: "2006-04-02T14:07:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6007 -->
Das ist Variante C mit verkürztem Vorderwagen.