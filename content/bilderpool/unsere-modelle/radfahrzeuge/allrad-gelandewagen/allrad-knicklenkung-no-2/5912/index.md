---
layout: "image"
title: "Knick-4x4-01.JPG"
date: "2006-03-26T12:29:59"
picture: "Knick-4x4-01.JPG"
weight: "13"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Allrad", "Knicklenkung", "Monsterreifen", "Großreifen"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5912
imported:
- "2019"
_4images_image_id: "5912"
_4images_cat_id: "513"
_4images_user_id: "4"
_4images_image_date: "2006-03-26T12:29:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5912 -->
Thomas hat nebenan die Vorlage zum Allrad mit Knicklenkung geliefert. Das Fahrzeug hier hat zusätzlich noch ein Drehgelenk in der Mitte und wird damit voll geländegängig.