---
layout: "image"
title: "Knick-4x4b-05.JPG"
date: "2006-04-02T14:05:13"
picture: "Knick-4x4b-05.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/6006
imported:
- "2019"
_4images_image_id: "6006"
_4images_cat_id: "513"
_4images_user_id: "4"
_4images_image_date: "2006-04-02T14:05:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6006 -->
Die schräg liegende Strebe I-30 Loch sorgt dafür, dass beim Einschlagen der Lenkung nach rechts auch wirklich das Knickgelenk bewegt wird, und nicht das Vorderteil auseinander gedrückt. Um zwischen Z30 und der Schnecke hindurch zu kommen, musste der BS7,5 hochkant hinein.