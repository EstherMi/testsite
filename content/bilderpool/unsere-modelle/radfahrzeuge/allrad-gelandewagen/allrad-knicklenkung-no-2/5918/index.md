---
layout: "image"
title: "Knick-4x4-10.JPG"
date: "2006-03-26T12:34:43"
picture: "Knick-4x4-10.JPG"
weight: "19"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5918
imported:
- "2019"
_4images_image_id: "5918"
_4images_cat_id: "513"
_4images_user_id: "4"
_4images_image_date: "2006-03-26T12:34:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5918 -->
