---
layout: "image"
title: "Knick-4x4-03.JPG"
date: "2006-03-26T12:32:08"
picture: "Knick-4x4-03.JPG"
weight: "14"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5913
imported:
- "2019"
_4images_image_id: "5913"
_4images_cat_id: "513"
_4images_user_id: "4"
_4images_image_date: "2006-03-26T12:32:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5913 -->
Das Drehgelenk hat keinen Anschlag, deshalb kann der Vorderwagen problemlos eine volle Rolle seitwärts drehen (aber nur, weil noch keine Kabel oder Schläuche verlegt sind)