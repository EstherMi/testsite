---
layout: "image"
title: "Knick-4x4b-02.JPG"
date: "2006-04-02T14:00:57"
picture: "Knick-4x4b-02.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/6004
imported:
- "2019"
_4images_image_id: "6004"
_4images_cat_id: "513"
_4images_user_id: "4"
_4images_image_date: "2006-04-02T14:00:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6004 -->
Der hintere Teil, mit der Unterseite nach oben. Die Antriebsachse verschwindet jetzt gleich hinter dem Drehgelenk auf die Unterseite.