---
layout: "image"
title: "Übersicht von Hinten"
date: "2014-02-01T16:00:15"
picture: "Hinterachse-V2-AnsichtvHinten.jpg"
weight: "1"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38149
imported:
- "2019"
_4images_image_id: "38149"
_4images_cat_id: "2839"
_4images_user_id: "1729"
_4images_image_date: "2014-02-01T16:00:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38149 -->
Die Achse habe ich komplett neu konstruiert, da ich in der ersten Version Probleme mit den zu übertragenden Kräften hatte. Die neue Version hat jetzt Planetengetriebe direkt an den Rädern.
Links ist der fertige Stand zu sehen. Rechts habe ich zum Fotografieren die Federn weggelassen. Außerdem sind dort nur FT Originalteile und baut dadurch etwas breiter. Leider dreht bei meinem letzten Z10 mit Messingnabe die Madenschraube durch, so daß ich vorerst diese unsymmetrische Lösung einsetze.

Die Achse ist sehr stabil. Leider ist sie etwas breiter als die erste Version, so daß ich sie entweder nochmal ändern muß oder das Modell insgesamt etwas vergrößere (sollte die selbe Spurbreite haben wie die Vorderachse)