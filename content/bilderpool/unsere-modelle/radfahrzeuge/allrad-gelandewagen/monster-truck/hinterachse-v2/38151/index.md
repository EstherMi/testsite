---
layout: "image"
title: "Anischt von unten"
date: "2014-02-01T16:00:15"
picture: "Hinterachse-V2-AnsichtvUnten.jpg"
weight: "3"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38151
imported:
- "2019"
_4images_image_id: "38151"
_4images_cat_id: "2839"
_4images_user_id: "1729"
_4images_image_date: "2014-02-01T16:00:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38151 -->
Ursprünglich hatte ich starre Achsverbinder anstatt den Kardangelenken. Die gesamte Achskonstruktion ist so ausgelegt, daß alle Wellenlager in einer Flucht sind, um einen leichtgängen Lauf zu ermöglichen. Damit habe ich es mir unnötig schwer gemacht. Es kommt dann echt auf jeden1/10 Millimeter an! Außerdem...wenn sich die Achse unter Last etwas durchbiegt, laufen auch die Stahlwellen schwerer. Die Kardangelenke sind also eine sinnvolle Verbesserung.