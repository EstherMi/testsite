---
layout: "image"
title: "Ansicht von oben"
date: "2014-02-01T16:00:15"
picture: "Hinterachse-V2-AnsichtvOben.jpg"
weight: "2"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38150
imported:
- "2019"
_4images_image_id: "38150"
_4images_cat_id: "2839"
_4images_user_id: "1729"
_4images_image_date: "2014-02-01T16:00:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38150 -->
Hier sieht man gut den Käfig für die Kegelzahnräder und das Differential. Die Anordnung der Kegelzahnräder verschlingt den meisten Platz und ist der Hauptgrund der Überbreite. Ich überlege, ob ich es mit einem Differential aus dem RC Car Modellbau ersetzen soll