---
layout: "image"
title: "Detail Radlager"
date: "2014-02-01T16:00:15"
picture: "Hinterachse-V2-Detail-Radlager.jpg"
weight: "4"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38152
imported:
- "2019"
_4images_image_id: "38152"
_4images_cat_id: "2839"
_4images_user_id: "1729"
_4images_image_date: "2014-02-01T16:00:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38152 -->
Man sieht nochmal schön meine Lösung mit dem Planetengetriebe, welches so gut wie keinen zusätzlichen Platz benötigt. Die Details dazu habe ich hier gepostet:
http://ftcommunity.de/categories.php?cat_id=2832
Weiterhin stelle ich dort noch das Detail-Bild von der rechten Seite ein mit Original fischertechnik Z10