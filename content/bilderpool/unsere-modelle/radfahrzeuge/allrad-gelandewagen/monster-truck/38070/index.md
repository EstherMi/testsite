---
layout: "image"
title: "Vorderansicht"
date: "2014-01-16T15:59:47"
picture: "FrontView.jpg"
weight: "3"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38070
imported:
- "2019"
_4images_image_id: "38070"
_4images_cat_id: "2831"
_4images_user_id: "1729"
_4images_image_date: "2014-01-16T15:59:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38070 -->
Die Vorderansicht des Monster Trucks.
Hier ist die vordere Starrachse und die zugehörige Federung und Aufhängung schön zu sehen.
Die Motorhaube wirkt optisch ein bisschen zu groß. Für ein echtes Semiscale Modell müsste die Karosserie ein bisschen kleiner im Vergleich zum Chassis (Fahrwerk) sein. 
Beachtet auch den "Bigblock-Motor", der oben aus der Motorhaube rausschaut ;-)