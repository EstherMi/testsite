---
layout: "image"
title: "Seitenansicht"
date: "2014-01-16T15:59:47"
picture: "SideView.jpg"
weight: "4"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38071
imported:
- "2019"
_4images_image_id: "38071"
_4images_cat_id: "2831"
_4images_user_id: "1729"
_4images_image_date: "2014-01-16T15:59:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38071 -->
Von der Seite ist der Eindruck schon nahe an einem Monster Truck. Die grauen Statikstreben sind reine Zierde. Echte Monster Trucks sind meistens im Chassis mit unzähligen Metallrohren verstrebt, um die notwendige Stabilität zu erreichen.
Meiner hält eigentlich weitgehend durch die schwarzen waagrechten Streben.
Die Karosserie ist nur mit 4 Punkten auf dem Chassis fixiert und kann leicht abgenommen werden.