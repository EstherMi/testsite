---
layout: "image"
title: "Monster Truck in Aktion"
date: "2014-02-05T12:24:17"
picture: "MonsterTruckInAktion.jpg"
weight: "1"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38166
imported:
- "2019"
_4images_image_id: "38166"
_4images_cat_id: "2840"
_4images_user_id: "1729"
_4images_image_date: "2014-02-05T12:24:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38166 -->
Quizz-Frage:
Warum shreddert ein MonsterTruck andere Autos? .....
Antwort: WEIL ER ES KANN! ;-)

Hier sind man den Truck in Aktion. Die Viper ist zwar nicht ganz maßstabsgerecht, ist aber bereits 7 cm hoch. Obwohl die ft-Federn wenig Federweg bieten, arbeitet das Fahrwerk astrein. Alle Räder stehen noch fest auf dem Boden. Vordere und hintere Federn zusammen gleichen den Höhenunterschied aus.
Um ein wirklich authentisches Bild zu machen, müsste ich einen Hammer nehmen und die Viper bearbeiten. Dach eindrücken, Glas-Splitter.....aber da war mir das Auto dann doch zu schade.

Wie geht es weiter?
Der Truck hat einen ganz guten Stand erreicht (zumindest optisch). Er fährt nicht besonders gut. Trotz Planetengetriebe an den Rädern habe ich weiterhin Probleme mit dem Schlupf, zum Anfahren muß man manchmal etwas schubsen, außerdem fährt er sehr langsam.

Ich dachte immer, im Original sind Monster Trucks auch eher was zum anschauen, das ganze Fahrwerk eher empfindlich und kaum zum Fahren geeignet. Ab und zu in ner Show mal über ein anderes Auto drüberfahren, das wäre schon das Maximum.
Aber ganz im Gegenteil! Schaut mal z.B. dieses Video: http://www.youtube.com/watch?v=5_3ZeN6Zmzc
Die Dinger springen wie Grashüpfer. Wenn sie sich nicht gerade überschlagen, stecken sie enorm viel weg.
Außerdem, was ich bisher nicht wusste...die haben nicht nur 4WD, sondern auch noch 4 Wheel Steering.
Also, technisch echt der Hammer!
Ich denke, daß ich mein Modell nach und nach weiterentwickle. Ideen habe ich Einige:
- große Stoßdämpfer aus einem RC Modell; 10 cm Federweg wäre angemessen
- den Antrieb verbessern, Schlupf minimieren (Fremdteile)
- Motorleistung verbessern...ein Brushless Motor wäre genial
- Vierrad-Antrieb, wobei dadurch bestimmt der Wendekreis leidet
- 4 Wheel Steering, das wird technisch aber extrem aufwändig

Zum Glück ist der Truck recht groß, so daß sich einiges in dem Maßstab einbauen lässt.