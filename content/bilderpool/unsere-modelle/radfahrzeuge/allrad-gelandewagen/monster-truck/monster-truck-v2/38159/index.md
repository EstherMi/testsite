---
layout: "image"
title: "Rückseite nochmal"
date: "2014-02-04T20:39:24"
picture: "Rueckseite.jpg"
weight: "5"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38159
imported:
- "2019"
_4images_image_id: "38159"
_4images_cat_id: "2840"
_4images_user_id: "1729"
_4images_image_date: "2014-02-04T20:39:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38159 -->
