---
layout: "image"
title: "Rückseite"
date: "2014-02-04T20:39:24"
picture: "MonsterTruck_Rckseite.jpg"
weight: "4"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38158
imported:
- "2019"
_4images_image_id: "38158"
_4images_cat_id: "2840"
_4images_user_id: "1729"
_4images_image_date: "2014-02-04T20:39:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38158 -->
naja, die neue Hinterachse ist zu breit geworden