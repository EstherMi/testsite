---
layout: "image"
title: "Verteiler in der Motorhaube"
date: "2014-02-04T20:39:24"
picture: "LichtVerteiler.jpg"
weight: "10"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38164
imported:
- "2019"
_4images_image_id: "38164"
_4images_cat_id: "2840"
_4images_user_id: "1729"
_4images_image_date: "2014-02-04T20:39:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38164 -->
...bitte überbrücken....
hier könnte man eigentlich Fremdspannung einspeisen.

die 4 Lichter benötigen ja bereits acht Drähte, da sind die beiden fischertechnik Verteilerplatten gerade ausreichend. Obwohl mein Modell ziemlich groß ist, ist manchmal der Platz trotzdem ganz schön eng.
Die Verteilerplatten selbst sind ja schon ganz schön groß, aber die ganzen Stecker brauchen noch viel mehr Platz. Direkt unter dem "Blech" bzw. dem "Lüftungsgitter" habe ich noch einen Platz dafür gefunden. Auf der einen Seite die Platte für +9V, auf der anderen für Minus.
Damit die Stecker sich nicht in die Quere kommen, sind die Platten gegeneinander etwas verschoben.
Mein Ziel insgesamt ist ja, das man von der Elektrik nichts sehen sollte, um den Gesamteindruck des Modells nicht zu stören. Die Frontscheinwerfer sind komplett verdeckt verdrahtet, nur bei den Rücklichtern ist mir das nicht ganz gelungen.