---
layout: "image"
title: "MonsterTruck Cockpit"
date: "2014-02-04T20:39:24"
picture: "Cockpit.jpg"
weight: "11"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38165
imported:
- "2019"
_4images_image_id: "38165"
_4images_cat_id: "2840"
_4images_user_id: "1729"
_4images_image_date: "2014-02-04T20:39:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38165 -->
Der Bursche sitzt eigentlich ganz bequem. Wenn man genau hinsieht, sieht man auch ein Fremdteil. Das Männchen ist mit einem Klebe-Strip befestigt, sonst rutscht er ständig von der Sitzbank. Ich wollte hier ne Bank anstatt den ft-Schalensitzen, da es besser zum Thema passt.

Außerdem sieht man den Schaltknüppel:
- Nach Vorne: Strom aus
- Nach Hinten: Strom an