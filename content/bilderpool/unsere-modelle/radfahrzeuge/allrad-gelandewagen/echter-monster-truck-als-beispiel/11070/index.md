---
layout: "image"
title: "03-Heckansicht"
date: "2007-07-15T17:49:00"
picture: "03-Heckansicht.jpg"
weight: "3"
konstrukteure: 
- "unbek."
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/11070
imported:
- "2019"
_4images_image_id: "11070"
_4images_cat_id: "1004"
_4images_user_id: "46"
_4images_image_date: "2007-07-15T17:49:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11070 -->
Der Vollständigkeit halber, um die Proportionen zu zeigen. Leider ungünstiges Sonnenwetter und die interessanten Teile im Schatten. Ein Königreich für ein Monster-Blitzgerät.