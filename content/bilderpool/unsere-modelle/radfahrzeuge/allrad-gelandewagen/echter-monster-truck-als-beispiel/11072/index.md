---
layout: "image"
title: "05-Radaufhängung"
date: "2007-07-15T17:49:00"
picture: "05-Radaufhngung.jpg"
weight: "5"
konstrukteure: 
- "unbek."
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/11072
imported:
- "2019"
_4images_image_id: "11072"
_4images_cat_id: "1004"
_4images_user_id: "46"
_4images_image_date: "2007-07-15T17:49:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11072 -->
Oje! Ich nenne das Bild mal Radaufhängung, aber diese Konstruktion würde man jedem Maschinenbaustudenten im 2. Semester um die Ohren hauen. Ja, gut, für diesen Zweck mag es gehen.

Von rechts nach links: Aufhängung der Federung, ok. Dann Drehpunkt der Lenkung außerhalb des Rades. Aua aua aua, die arme Spurstange. Egal, in welche Richtung das Rad Kräfte abbekommt, immer muß die Spurstange das halten. Normalerweise zielt die Drehachse der Lenkung auf den Aufstandspunkt des Rades auf dem Boden. Guckt mal unter einen Unimog, dann wirds deutlich.

Dann kommt noch die Radlagerung, die auch nicht mittig ist. Neben dem Eigengewicht muß die Radlagerung daher noch ein daraus resultierendes Drehmoment aufnehmen, sowie die Radkräfte durch den Antrieb, die ebenfalls ein Drehmoment in Radlager geben, weil die Lager weit von der Radmitte weg sind.

Man beachte auch die Kürze des Lenkhebels für das Rad. Das ist ganz filigraner Leichtbau für ganz kleine Beanspruchung.