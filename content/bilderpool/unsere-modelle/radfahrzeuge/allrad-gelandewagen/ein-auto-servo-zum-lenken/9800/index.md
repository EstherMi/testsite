---
layout: "image"
title: "Ferngesteuertes Auto"
date: "2007-03-27T14:05:50"
picture: "auto2.jpg"
weight: "3"
konstrukteure: 
- "1958230dermitdemhut"
fotografen:
- "1958230dermitdemhut"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/9800
imported:
- "2019"
_4images_image_id: "9800"
_4images_cat_id: "884"
_4images_user_id: "453"
_4images_image_date: "2007-03-27T14:05:50"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9800 -->
Hier sieht man den Antrieb.