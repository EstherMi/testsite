---
layout: "image"
title: "Ferngesteuertes Auto"
date: "2007-03-27T14:05:50"
picture: "auto4.jpg"
weight: "5"
konstrukteure: 
- "1958230dermitdemhut"
fotografen:
- "1958230dermitdemhut"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/9802
imported:
- "2019"
_4images_image_id: "9802"
_4images_cat_id: "884"
_4images_user_id: "453"
_4images_image_date: "2007-03-27T14:05:50"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9802 -->
Hier ist das Servo noch ein mal von nah.