---
layout: "image"
title: "hauptansicht"
date: "2006-12-30T09:56:29"
picture: "gelaendewagen01.jpg"
weight: "1"
konstrukteure: 
- "Michael K."
fotografen:
- "Michael K."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Wert"
license: "unknown"
legacy_id:
- details/8208
imported:
- "2019"
_4images_image_id: "8208"
_4images_cat_id: "757"
_4images_user_id: "366"
_4images_image_date: "2006-12-30T09:56:29"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8208 -->
das modell von schräg-oben