---
layout: "comment"
hidden: true
title: "1825"
date: "2006-12-30T17:09:50"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Respekt! Eine sehr interessante Lenkung, und der Antriebsstrang ist auch nicht ohne.

Zum Einparken müsste aber eine Achse von der Lenkung gelöst werden, sonst pendelt das Auto nur um die Parklücke herum ;-)

Gruß,
Harald