---
layout: "overview"
title: "Geländewagen Nr. 2 (Michael K.)"
date: 2019-12-17T18:47:00+01:00
legacy_id:
- categories/757
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=757 --> 
Dies ist ein Fahrzeug mit Allradantrieb und Allradlenkung. Es ist komplett gefedert.