---
layout: "image"
title: "Heck2"
date: "2014-01-12T17:58:03"
picture: "pickup06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Reinmüller, Sebastian Erler"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- details/38042
imported:
- "2019"
_4images_image_id: "38042"
_4images_cat_id: "2829"
_4images_user_id: "1924"
_4images_image_date: "2014-01-12T17:58:03"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38042 -->
