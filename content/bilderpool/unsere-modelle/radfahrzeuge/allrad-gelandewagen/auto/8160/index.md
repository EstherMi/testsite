---
layout: "image"
title: "Kraftübertragung"
date: "2006-12-26T21:04:39"
picture: "Allradauto4.jpg"
weight: "4"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/8160
imported:
- "2019"
_4images_image_id: "8160"
_4images_cat_id: "748"
_4images_user_id: "456"
_4images_image_date: "2006-12-26T21:04:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8160 -->
Hier sieht man die Kraftübertragung. Wenn das Auto in starkem Gelände fährt kommt es manchmal vor, dass sich zwar das Z20 dreht, aber nicht die Welle.