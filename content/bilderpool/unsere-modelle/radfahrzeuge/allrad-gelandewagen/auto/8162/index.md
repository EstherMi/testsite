---
layout: "image"
title: "Federung"
date: "2006-12-26T21:04:39"
picture: "Allradauto6.jpg"
weight: "6"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/8162
imported:
- "2019"
_4images_image_id: "8162"
_4images_cat_id: "748"
_4images_user_id: "456"
_4images_image_date: "2006-12-26T21:04:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8162 -->
Hier sieht man die Federung. Die ist von Cars&trucks.