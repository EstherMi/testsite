---
layout: "image"
title: "Motorhaube"
date: "2006-03-11T11:09:10"
picture: "P3100011.jpg"
weight: "5"
konstrukteure: 
- "Michael K."
fotografen:
- "Michael K."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Wert"
license: "unknown"
legacy_id:
- details/5853
imported:
- "2019"
_4images_image_id: "5853"
_4images_cat_id: "504"
_4images_user_id: "366"
_4images_image_date: "2006-03-11T11:09:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5853 -->
Wenn man die Motorhaube öffnet, sieht man das IR-Control Set, Accupack und die Elektronik für eine Hupe(9V-Block,Taster,Summer)