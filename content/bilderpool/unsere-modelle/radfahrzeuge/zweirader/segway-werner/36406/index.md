---
layout: "image"
title: "Segway beim Balancieren"
date: "2013-01-04T14:44:53"
picture: "segwaymitabstandssensor5.jpg"
weight: "5"
konstrukteure: 
- "Jan Werner"
fotografen:
- "Jan Werner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "werner"
license: "unknown"
legacy_id:
- details/36406
imported:
- "2019"
_4images_image_id: "36406"
_4images_cat_id: "2702"
_4images_user_id: "1196"
_4images_image_date: "2013-01-04T14:44:53"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36406 -->
Der Rekord beträgt 28 Sekunden, leider habe ich das nicht per Video festgehalten.
Trotzdem ein Video: https://www.dropbox.com/s/oo4wwefcec8p7d5/Segway.mp4