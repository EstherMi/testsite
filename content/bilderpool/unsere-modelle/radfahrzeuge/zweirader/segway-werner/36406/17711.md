---
layout: "comment"
hidden: true
title: "17711"
date: "2013-01-06T19:30:22"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hallo Triceratops,
ein Segway ist ein elektromotorgetriebenes Zweirad mit Gleichgewichtssteuerung - siehe http://www.segway.de/. Lenken und Beschleunigen/Verzögern erfolgt durch Gewicchtsverlagerung, und die Sensorik verhindert, dass das Gefährt umkippt...
Gruß, Dirk