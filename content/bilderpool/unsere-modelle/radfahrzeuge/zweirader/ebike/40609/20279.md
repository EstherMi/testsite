---
layout: "comment"
hidden: true
title: "20279"
date: "2015-03-02T11:47:43"
uploadBy:
- "uffi"
license: "unknown"
imported:
- "2019"
---
Hallo Roland,

danke für den Link zu Deinem Motorrad. Tolles Modell!!! Das hatte ich noch nicht gesehen! Ist anscheinend ein Promotion-Modell von Fischertechnik. Haben die Reifen richtig Grip? Oder ist das harter Kunststoff?

Und ja, mein Motorrad fährt zur Zeit vorwiegend geradeaus, das Kurvenfahren oder im Kreis Fahren muss ich ihm noch beibringen. Im Moment klappt das nur, wenn es in seiner Gleichgewichtslage gestört wird. Hast Du das Video dazu gesehen?

https://www.youtube.com/watch?v=EGWPJl7Khiw

Nette Idee mit einem Rennen der beiden Motorräder. Vielleicht sollten wir Dein Modell mit meiner Elektronik ausstatten???

Gruß, Dirk