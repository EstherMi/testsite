---
layout: "comment"
hidden: true
title: "20264"
date: "2015-02-28T23:37:54"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hallo Dirk,
das Video (genauer: Dein Motorrad natürlich...) ist ja der Hammer. Fehlt nur noch der Fahrer, der mit dem Gegengewicht gegensteuert... Ein tolles Teil.
Reifen: Traktorreifen? Oder die "fetten" alten Hartkunststoffreifen mit Gummiband auf der Lauffläche?
Gruß, Dirk