---
layout: "image"
title: "Nochmal umgebaut"
date: "2015-03-03T17:27:28"
picture: "IMG_0425.jpg"
weight: "1"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Vibrationen"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/40611
imported:
- "2019"
_4images_image_id: "40611"
_4images_cat_id: "3042"
_4images_user_id: "579"
_4images_image_date: "2015-03-03T17:27:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40611 -->
Mit dem weit hinten angebrachten Gepäckträger gab es zu viele Vibrationen am Gyroskop / GY-86 / MPU6050. Nun ist es besser bzw. vibriert weniger und die Regelung läuft stabiler damit.