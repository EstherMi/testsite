---
layout: "image"
title: "Elektronik neu plaziert"
date: "2015-03-01T10:40:27"
picture: "IMG_0417.jpg"
weight: "4"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/40610
imported:
- "2019"
_4images_image_id: "40610"
_4images_cat_id: "3042"
_4images_user_id: "579"
_4images_image_date: "2015-03-01T10:40:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40610 -->
