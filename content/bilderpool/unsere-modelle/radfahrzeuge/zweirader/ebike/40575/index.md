---
layout: "image"
title: "Am Startblock: fertig zum Losfahren"
date: "2015-02-20T18:29:18"
picture: "IMG_0375.jpg"
weight: "5"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Startblock"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/40575
imported:
- "2019"
_4images_image_id: "40575"
_4images_cat_id: "3042"
_4images_user_id: "579"
_4images_image_date: "2015-02-20T18:29:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40575 -->
Die ersten Fahr-Tests zeigen: da sind noch Bugs in der Software... jetzt gehen die Debug-Arbeiten los