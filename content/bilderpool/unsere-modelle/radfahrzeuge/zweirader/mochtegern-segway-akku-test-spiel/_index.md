---
layout: "overview"
title: "Möchtegern-Segway für Akku-Test und Spiel"
date: 2019-12-17T18:50:56+01:00
legacy_id:
- categories/3476
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3476 --> 
Ein Gerät für Geschicklichkeitsspiele, gebaut für den Test eines Fan-Selbstbau-Akkus.