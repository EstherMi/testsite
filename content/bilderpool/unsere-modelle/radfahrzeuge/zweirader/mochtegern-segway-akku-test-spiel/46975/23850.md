---
layout: "comment"
hidden: true
title: "23850"
date: "2017-12-11T14:39:22"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Die Gummis waren notwendig, damit auch der herunterhängende Akku nicht am Boden schleift.

Ich grüble noch, mit nur-ft-Mitteln irgend eine Regelung gegen "Überschlag bei zuviel Vollgas" zu realisieren, hab aber noch keine gute Idee.

Gruß,
Stefan