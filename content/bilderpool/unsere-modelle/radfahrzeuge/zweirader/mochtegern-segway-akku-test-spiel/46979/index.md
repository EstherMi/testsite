---
layout: "image"
title: "Befestigung des Akkus"
date: "2017-12-10T22:55:39"
picture: "moechtegernsegwayfuerakkutestundspiel5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46979
imported:
- "2019"
_4images_image_id: "46979"
_4images_cat_id: "3476"
_4images_user_id: "104"
_4images_image_date: "2017-12-10T22:55:39"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46979 -->
Der Akku hängt an drei BS7,5, die am oberen Querbalken angebracht sind.

Das Teil hat bei jedem Besucher gut Laune gemacht. Alle wollten sie mal damit rumfahren, und sofort resultierte das in den Versuchen, das Gerät zielsicher zu steuern, ohne dass es sich überschlägt.