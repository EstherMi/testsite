---
layout: "overview"
title: "Motorrad (vollgefedert)"
date: 2019-12-17T18:50:52+01:00
legacy_id:
- categories/2756
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2756 --> 
Hier sieht man ein vollgefdertes Motorrad, das man fernbedienen kann. Ich wollte alle Bauteile, die man zur Fernsteuerung benötigt, platzsparend unterbringen.
Ein Video kann man hier sehen: http://home.versanet.de/~udohenkel/motorrad.mp4  (Ich habe gefilmt und gesteuert, deshalb die schlechte Bildführung)