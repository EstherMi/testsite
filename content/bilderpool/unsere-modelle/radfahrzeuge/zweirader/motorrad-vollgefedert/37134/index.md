---
layout: "image"
title: "Motorrad (rechte Seite)"
date: "2013-07-02T08:30:08"
picture: "motorradvollgefedert1.jpg"
weight: "1"
konstrukteure: 
- "uhen"
fotografen:
- "uhen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- details/37134
imported:
- "2019"
_4images_image_id: "37134"
_4images_cat_id: "2756"
_4images_user_id: "1112"
_4images_image_date: "2013-07-02T08:30:08"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37134 -->
Hier sieht man die Elektronik für die Fernsteuerung. Trotz des zugebauten Empfängers, kann man das Motorrad gut fernsteuern.