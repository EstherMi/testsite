---
layout: "image"
title: "ConferenceBike"
date: "2006-01-09T16:10:19"
picture: "Ft-CoBi.jpg"
weight: "5"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: ["CoBi"]
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- details/5582
imported:
- "2019"
_4images_image_id: "5582"
_4images_cat_id: "484"
_4images_user_id: "381"
_4images_image_date: "2006-01-09T16:10:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5582 -->
