---
layout: "image"
title: "Programm von Segway"
date: "2011-05-19T15:36:26"
picture: "Programm_Segway.jpg"
weight: "3"
konstrukteure: 
- "Simon"
fotografen:
- "Simon"
keywords: ["Segway", "Programm"]
uploadBy: "Guest"
license: "unknown"
legacy_id:
- details/30587
imported:
- "2019"
_4images_image_id: "30587"
_4images_cat_id: "2281"
_4images_user_id: "-1"
_4images_image_date: "2011-05-19T15:36:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30587 -->
