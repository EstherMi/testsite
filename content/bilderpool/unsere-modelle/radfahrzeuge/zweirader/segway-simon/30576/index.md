---
layout: "image"
title: "Segway"
date: "2011-05-18T17:01:31"
picture: "Segway.jpg"
weight: "4"
konstrukteure: 
- "Simon"
fotografen:
- "Simon"
keywords: ["Segway"]
uploadBy: "Guest"
license: "unknown"
legacy_id:
- details/30576
imported:
- "2019"
_4images_image_id: "30576"
_4images_cat_id: "2281"
_4images_user_id: "-1"
_4images_image_date: "2011-05-18T17:01:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30576 -->
