---
layout: "image"
title: "Motorrad 1"
date: "2005-08-22T20:06:59"
picture: "Motorrad_1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/4638
imported:
- "2019"
_4images_image_id: "4638"
_4images_cat_id: "373"
_4images_user_id: "328"
_4images_image_date: "2005-08-22T20:06:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4638 -->
Ziel war es, auf möglichst kleinem Raum eine detaillierte Nachbildung eines Motorrades mit folgenden realistischen Eigenschaften zu bauen:

- Federgabel vorn
- Hinterachse gefedert
- Antrieb (klar... ;o)
- eigene autarke Energieversorgung (=Batteriekasten)