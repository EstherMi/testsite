---
layout: "image"
title: "Motorrad 6"
date: "2005-08-22T20:07:00"
picture: "Motorrad_6.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/4643
imported:
- "2019"
_4images_image_id: "4643"
_4images_cat_id: "373"
_4images_user_id: "328"
_4images_image_date: "2005-08-22T20:07:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4643 -->
Hier die demontierte Federgabel. Die beiden Gelenke sind über je einen "Baustein 15 x 30 x 5 rot mit Nut und Zapfen" je an einem der beiden Bausteine 30 angebracht, um ein Klemmen der Federgabel zu vermeiden. Die beiden 5er-Bausteine haben keine Funktion und dienen der Zierde/Symmetrie.