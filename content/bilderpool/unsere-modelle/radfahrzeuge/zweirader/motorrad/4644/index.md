---
layout: "image"
title: "Motorrad 7"
date: "2005-08-22T20:07:00"
picture: "Motorrad_7.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/4644
imported:
- "2019"
_4images_image_id: "4644"
_4images_cat_id: "373"
_4images_user_id: "328"
_4images_image_date: "2005-08-22T20:07:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4644 -->
Hier nochmal die Federgabel mit demontiertem Scheinwerfer in ihrer ganzen Pracht. Durch die lose Lagerung gleitet sie sehr leicht.