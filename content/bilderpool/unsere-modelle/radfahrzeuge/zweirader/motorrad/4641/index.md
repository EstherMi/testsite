---
layout: "image"
title: "Motorrad 4"
date: "2005-08-22T20:07:00"
picture: "Motorrad_4.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/4641
imported:
- "2019"
_4images_image_id: "4641"
_4images_cat_id: "373"
_4images_user_id: "328"
_4images_image_date: "2005-08-22T20:07:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4641 -->
Das Hinterrad ist wie beim echten Motorrad gefedert. Der Antrieb des Hinterrads erfolgt über einen Original-FT-Gummi.