---
layout: "image"
title: "LKW Rampe unten"
date: "2006-12-20T00:22:35"
picture: "lkwtransporter05.jpg"
weight: "5"
konstrukteure: 
- "JMN"
fotografen:
- "JMN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/7967
imported:
- "2019"
_4images_image_id: "7967"
_4images_cat_id: "743"
_4images_user_id: "162"
_4images_image_date: "2006-12-20T00:22:35"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7967 -->
LKW Rampe unten