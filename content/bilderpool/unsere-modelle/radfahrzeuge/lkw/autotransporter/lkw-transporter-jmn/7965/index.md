---
layout: "image"
title: "LKW ohne Ladung"
date: "2006-12-20T00:22:35"
picture: "lkwtransporter03.jpg"
weight: "3"
konstrukteure: 
- "JMN"
fotografen:
- "JMN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/7965
imported:
- "2019"
_4images_image_id: "7965"
_4images_cat_id: "743"
_4images_user_id: "162"
_4images_image_date: "2006-12-20T00:22:35"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7965 -->
LKW ohne Ladung