---
layout: "image"
title: "LKW mit Ladung"
date: "2006-12-20T00:22:35"
picture: "lkwtransporter02.jpg"
weight: "2"
konstrukteure: 
- "JMN"
fotografen:
- "JMN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/7964
imported:
- "2019"
_4images_image_id: "7964"
_4images_cat_id: "743"
_4images_user_id: "162"
_4images_image_date: "2006-12-20T00:22:35"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7964 -->
LKW mit Ladung