---
layout: "image"
title: "PKW-Transporter"
date: "2007-07-20T16:55:57"
picture: "pkwtrans2.jpg"
weight: "2"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- details/11156
imported:
- "2019"
_4images_image_id: "11156"
_4images_cat_id: "1010"
_4images_user_id: "557"
_4images_image_date: "2007-07-20T16:55:57"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11156 -->
hier dir untere klappe, die auf den 1sten stock führt