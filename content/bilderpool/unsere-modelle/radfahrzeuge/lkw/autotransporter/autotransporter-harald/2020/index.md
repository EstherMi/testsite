---
layout: "image"
title: "0x6Tr05.JPG"
date: "2003-12-22T10:57:21"
picture: "0x6Tr05.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2020
imported:
- "2019"
_4images_image_id: "2020"
_4images_cat_id: "1025"
_4images_user_id: "4"
_4images_image_date: "2003-12-22T10:57:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2020 -->
Der Seilzug betätigt die obere Laderampe und die Heckrampe. Die Winde sitzt vorn auf dem Auflieger, das Seil führt schräg nach oben, um die schwarzen Seilrollen herum,  durch Ösen am hinteren Ende der Laderampe hindurch und dann zu den Spitzen der Heckrampe.

Wenn die (obere) Laderampe verriegelt ist, klappt also nur die Heckrampe herunter. Ist die Laderampe frei, senkt sie sich erst ein Stückchen, dann geht die Heckrampe etwas runter, dann wieder die Laderampe usw, je nachdem wie das Kräfte-Vieleck aus den Befestigungs/Führungsstellen des Seils gerade aussieht. Beim Hochfahren gibt es dasselbe Spielchen.