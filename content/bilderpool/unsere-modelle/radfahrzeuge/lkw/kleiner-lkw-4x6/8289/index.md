---
layout: "image"
title: "LKW 4x6 7"
date: "2007-01-02T19:55:07"
picture: "LKW_4x6_07.jpg"
weight: "7"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/8289
imported:
- "2019"
_4images_image_id: "8289"
_4images_cat_id: "764"
_4images_user_id: "328"
_4images_image_date: "2007-01-02T19:55:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8289 -->
Hier der Antrieb der beiden Hinterachsen. Nix Besonderes.

Ja, nach der reinen Lehre gehört in die Kardanwelle normalerweise ein Kardangelenk, um die Federbewegung zu entkoppeln, aber das hätte das Fahrzeug zu lang gemacht... Es funktioniert auch so, da alles elastisch genug ist.