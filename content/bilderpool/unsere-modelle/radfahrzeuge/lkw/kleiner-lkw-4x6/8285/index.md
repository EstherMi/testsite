---
layout: "image"
title: "LKW 4x6 3"
date: "2007-01-02T19:55:07"
picture: "LKW_4x6_03.jpg"
weight: "3"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/8285
imported:
- "2019"
_4images_image_id: "8285"
_4images_cat_id: "764"
_4images_user_id: "328"
_4images_image_date: "2007-01-02T19:55:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8285 -->
Hier sieht man gut, dass auf der Länge von nur drei Grundbausteinen alle Antriebskomponenten im Führerhaus untergebracht sind. Der Heckaufbau lässt sich - ausgehend vom Abtriebsritzel des Power-Motors - relativ modular und frei weiter aufbauen und erweitern. In diesem Fall ist es ein 4x6 geworden, damit die Proportionen stimmen.