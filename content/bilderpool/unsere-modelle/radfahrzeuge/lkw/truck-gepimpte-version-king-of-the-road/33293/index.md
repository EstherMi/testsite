---
layout: "image"
title: "Zugmaschine - Seitenansicht"
date: "2011-10-23T12:24:11"
picture: "kingoftheroadgepimpteversion2.jpg"
weight: "2"
konstrukteure: 
- "Magnus Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/33293
imported:
- "2019"
_4images_image_id: "33293"
_4images_cat_id: "2463"
_4images_user_id: "1126"
_4images_image_date: "2011-10-23T12:24:11"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33293 -->
Die Zugmaschine erhielt Metallachsen, eine größere Bereifung, Vorder- und Rücklichter (sehr helle LED), vier Blink-LED (gesteuert über das E-Tec-Modul), eine Fernsteuerung mit Servo-Lenkung, ein Signalhorn (steuerbar über die Fernsteuerung) und einen 9V-Block-Akku (unter der Haube).