---
layout: "image"
title: "Zugmaschine - Seitenspiegel"
date: "2011-10-23T12:24:11"
picture: "kingoftheroadgepimpteversion4.jpg"
weight: "4"
konstrukteure: 
- "Magnus Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/33295
imported:
- "2019"
_4images_image_id: "33295"
_4images_cat_id: "2463"
_4images_user_id: "1126"
_4images_image_date: "2011-10-23T12:24:11"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33295 -->
... erhielt die Zugmaschine korrekt eingestellte Seitenspiegel.