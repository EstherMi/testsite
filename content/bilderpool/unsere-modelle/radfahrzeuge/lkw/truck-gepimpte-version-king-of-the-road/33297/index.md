---
layout: "image"
title: "Sattelauflieger - Seitenansicht"
date: "2011-10-23T12:24:11"
picture: "kingoftheroadgepimpteversion6.jpg"
weight: "6"
konstrukteure: 
- "Magnus Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/33297
imported:
- "2019"
_4images_image_id: "33297"
_4images_cat_id: "2463"
_4images_user_id: "1126"
_4images_image_date: "2011-10-23T12:24:11"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33297 -->
Der Auflieger entspricht im Wesentlichen dem Modell aus der Anleitung "Advanced Super Trucks" - allerdings mit roten LED-Rücklichtern und einer motorisiert (und ferngesteuert) ausfahrbaren Auflieger-Stütze.