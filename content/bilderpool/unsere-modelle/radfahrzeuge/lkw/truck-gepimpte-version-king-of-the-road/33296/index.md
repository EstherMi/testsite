---
layout: "image"
title: "Zugmaschine - Ansicht von unten"
date: "2011-10-23T12:24:11"
picture: "kingoftheroadgepimpteversion5.jpg"
weight: "5"
konstrukteure: 
- "Magnus Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/33296
imported:
- "2019"
_4images_image_id: "33296"
_4images_cat_id: "2463"
_4images_user_id: "1126"
_4images_image_date: "2011-10-23T12:24:11"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33296 -->
"Verteilerkästen" und Ein-/Ausschalter sitzen unter dem Führerhaus.