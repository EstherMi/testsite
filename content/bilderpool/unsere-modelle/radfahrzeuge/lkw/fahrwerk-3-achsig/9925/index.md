---
layout: "image"
title: "LKW-Fahrwerk"
date: "2007-04-03T17:33:56"
picture: "fahrwerk2.jpg"
weight: "2"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- details/9925
imported:
- "2019"
_4images_image_id: "9925"
_4images_cat_id: "897"
_4images_user_id: "557"
_4images_image_date: "2007-04-03T17:33:56"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9925 -->
Antrieb durch Powermotor