---
layout: "image"
title: "WRECKER_1"
date: "2004-06-06T17:55:29"
picture: "WRECKER_1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/2494
imported:
- "2019"
_4images_image_id: "2494"
_4images_cat_id: "232"
_4images_user_id: "144"
_4images_image_date: "2004-06-06T17:55:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2494 -->
Peter Holland hat auf basis der von mir gebauten LKW chassis, einen Kenworth Wrecker gebaut.
Hier sind die bedienungsschalter zu sehen und die beide mini-getriebenmotoren von Lemo für die seilwinden.