---
layout: "image"
title: "Erweiterung für klappbare Fahrerkabine - Befestigungsdetail"
date: "2016-03-06T19:14:38"
picture: "einfacherabschleppwagen13.jpg"
weight: "13"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/42992
imported:
- "2019"
_4images_image_id: "42992"
_4images_cat_id: "3197"
_4images_user_id: "1557"
_4images_image_date: "2016-03-06T19:14:38"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42992 -->
So werden die BS30 mit den Quernuten in die Seitenteile des Fahrerhauses eingeschoben.