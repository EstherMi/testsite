---
layout: "image"
title: "Erweiterung zum Abschleppwagen - Zugstrebenlager"
date: "2016-03-06T18:40:13"
picture: "einfacherabschleppwagen04.jpg"
weight: "4"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/42983
imported:
- "2019"
_4images_image_id: "42983"
_4images_cat_id: "3197"
_4images_user_id: "1557"
_4images_image_date: "2016-03-06T18:40:13"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42983 -->
Montagedetail. 3 S-Riegel halten alles zusammen. Es paßt ohne Probleme, auch wenn es etwas eng zugeht.