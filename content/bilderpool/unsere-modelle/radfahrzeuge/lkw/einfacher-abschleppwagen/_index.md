---
layout: "overview"
title: "Einfacher Abschleppwagen"
date: 2019-12-17T18:42:17+01:00
legacy_id:
- categories/3197
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3197 --> 
Ein einfacher Abschleppwagen, aufbauend - im wahrsten Wortsinne - auf dem einfachen Lastwagen aus "Einfacher Lastwagen mit Anhänger" (http://www.ftcommunity.de/categories.php?cat_id=3162). Optional ist auch noch die Fahrerkabine klappbar. Beide Erweiterungen sind voneinander unabhängig. Und natürlich sind das Anregungen, die nach eigenem Ermessen abgewandelt werden können.

