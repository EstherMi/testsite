---
layout: "image"
title: "Erweiterung für klappbare Fahrerkabine - Umbau des Unterbodens"
date: "2016-03-06T18:40:13"
picture: "einfacherabschleppwagen10.jpg"
weight: "10"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/42989
imported:
- "2019"
_4images_image_id: "42989"
_4images_cat_id: "3197"
_4images_user_id: "1557"
_4images_image_date: "2016-03-06T18:40:13"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42989 -->
Zuerst ist mal der Leiterrahmem im vorderen Bereich zu ändern. Anstelle zweier WT30 mit BS15 kommen dort zwei (Feder-)Gelenksteine hin. Das ist allerdings etwas fummelig, da die Statikplatte mittels Verschlußriegel in die Nut des Gelenksteins einzuschieben ist. Deswegen sind die WT120 und die hinteren WT30 vom Vorbild vertauscht und die WT30 sind verdreht gegenüber den WT120. Behält man die ursprüngliche Orientierung bei, ist es unmöglich auf einer Seite den Verschlußriegel einzuschieben. Ohne Verschlußriegel hält allerdings die Bauplatte nicht richtig unter der Fahrerkabine.

Wer sich für Federgelenksteine entscheidet, sollte bitte berücksichtigen: Zwei Stück davon haben genug Drehmoment um die Kabine von alleine wieder hochzuklappen. Entweder nimmt man nur einen oder gar keinen Federgelenkstein. Dann bleibt die Kabine runtergeklappt.