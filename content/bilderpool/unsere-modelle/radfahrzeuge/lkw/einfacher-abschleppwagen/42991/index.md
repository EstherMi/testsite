---
layout: "image"
title: "Erweiterung für klappbare Fahrerkabine - Frontverkleidung"
date: "2016-03-06T19:14:38"
picture: "einfacherabschleppwagen12.jpg"
weight: "12"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/42991
imported:
- "2019"
_4images_image_id: "42991"
_4images_cat_id: "3197"
_4images_user_id: "1557"
_4images_image_date: "2016-03-06T19:14:38"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42991 -->
Für eine akzeptable Kühlerpartie sind weitere Änderungen fällig. Dank der Gelenksteine sind nun vorne keine Quernuten mehr sondern Zapfen! Die Bauplatte 90x30 paßt nicht mehr. Stattdessen werden in freien V-Federn an den Fahrerhausteilen zwei BS30 eingehängt (hier gelb, rot oder schwarz macht sich aber noch besser). Je zwei Federnocken zeigen nach vorne, ein Flachstein 30 stabilisiert die Konstruktion. Der Achshalter macht sich als Kühlergrill nützlich und wird seitlich auf die Federnocken aufgeschoben. Die zwei Bauplatten 5 mit Zapfen sichern dann den "Kühlergrill" gegen seitliches Wegrutschen.