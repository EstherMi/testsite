---
layout: "comment"
hidden: true
title: "21756"
date: "2016-03-06T22:53:23"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Wie schön, dieses Scharnier mal wieder verbaut zu werden. Noch dazu neben so einer modernen Seiltrommel. Passt ja immer noch alles :-)

Zudem ist das einfach ein schönes Modell und außerdem richtig gut fotografiert.

Gruß,
Stefan