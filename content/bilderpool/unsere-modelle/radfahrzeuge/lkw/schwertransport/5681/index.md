---
layout: "image"
title: "So war er auf der Steinfurter Modellschau zu sehen"
date: "2006-01-27T13:55:56"
picture: "DSCN0041.jpg"
weight: "1"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/5681
imported:
- "2019"
_4images_image_id: "5681"
_4images_cat_id: "489"
_4images_user_id: "184"
_4images_image_date: "2006-01-27T13:55:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5681 -->
Der Anhänger ist leider nicht mehr da.