---
layout: "image"
title: "ft-Riesen-LKW"
date: "2008-07-06T19:28:52"
picture: "pdft_MOTruck.jpg"
weight: "1"
konstrukteure: 
- "fischerwerke"
fotografen:
- "fischerwerke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/14803
imported:
- "2019"
_4images_image_id: "14803"
_4images_cat_id: "205"
_4images_user_id: "4"
_4images_image_date: "2008-07-06T19:28:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14803 -->
