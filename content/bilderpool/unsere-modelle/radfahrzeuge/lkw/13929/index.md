---
layout: "image"
title: "Adapter"
date: "2008-03-16T19:36:08"
picture: "road-train3.jpg"
weight: "3"
konstrukteure: 
- "thkais"
fotografen:
- "thkais"
keywords: ["Road", "Train"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- details/13929
imported:
- "2019"
_4images_image_id: "13929"
_4images_cat_id: "205"
_4images_user_id: "41"
_4images_image_date: "2008-03-16T19:36:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13929 -->
Dies ist das Bindeglied, um einen Auflieger an einen normalen LKW oder hinter einen vorhergehenden Anhänger anzuhängen. Hat irgendwer eine Ahnung, wie das Teil tatsächlich heißt?