---
layout: "image"
title: "Abrollkipper Mai 2004"
date: "2012-01-15T20:33:35"
picture: "3.jpg"
weight: "3"
konstrukteure: 
- "Johannes Weber"
fotografen:
- "Johannes Weber"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rheingauer01"
license: "unknown"
legacy_id:
- details/33943
imported:
- "2019"
_4images_image_id: "33943"
_4images_cat_id: "2512"
_4images_user_id: "1428"
_4images_image_date: "2012-01-15T20:33:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33943 -->
Wie beim Original verfügt der Arm über 2 Gelenke.
Beim Aufnehmen und Absetzen ist der kurze Teil des Armes gesperrt. Dies erfolgt hier durch die beiden modifizierten Seilhaken die an beiden Seiten das Fahrgestellrahmens angebracht sind.