---
layout: "image"
title: "Abrollkipper Mai 2004"
date: "2012-01-15T20:58:24"
picture: "12.jpg"
weight: "12"
konstrukteure: 
- "Johannes Weber"
fotografen:
- "Johannes Weber"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rheingauer01"
license: "unknown"
legacy_id:
- details/33952
imported:
- "2019"
_4images_image_id: "33952"
_4images_cat_id: "2512"
_4images_user_id: "1428"
_4images_image_date: "2012-01-15T20:58:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33952 -->
Abkippen der Ladung.