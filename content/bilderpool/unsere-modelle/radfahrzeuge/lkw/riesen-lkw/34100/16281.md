---
layout: "comment"
hidden: true
title: "16281"
date: "2012-02-07T08:33:03"
uploadBy:
- "ludger-ftc"
license: "unknown"
imported:
- "2019"
---
Hallo Thomas,

in einer älteren Version hatte ich sie senkrecht angeordnet. Da brauchte ich wesentlich weniger Federn. Aber dadurch wurde der Aufbau so "unschön" das ich das geändert habe. Durch den relativ steilen Winkel treten jetzt allerdings große Kräfte am oberen Befestigungspunkt auf.

Gruß ludger