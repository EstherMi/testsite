---
layout: "image"
title: "Lenkung"
date: "2012-02-05T20:01:28"
picture: "10_Lenkung_Sicht_von_hinten.jpg"
weight: "33"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/34091
imported:
- "2019"
_4images_image_id: "34091"
_4images_cat_id: "2522"
_4images_user_id: "184"
_4images_image_date: "2012-02-05T20:01:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34091 -->
Die Lenkung aus der Sicht von hinten. Zu erkennen das Zahnstangengetriebe und die linke Spurstange.
Die Federung besteht aus zwei weichen und zwei harten Federn 26. Zum Glück hatte ich gerade 4 harte.
Dieser obige Blinker ist noch nicht aktiv, er wird vielleicht später noch angeschlossen.
Ihr glaubt ja gar nicht wie viel Kabel man in einem solchem Modell einbauen kann......