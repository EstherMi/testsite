---
layout: "image"
title: "Lenkung von vorn"
date: "2012-02-05T20:01:28"
picture: "09_Lenkung_Detail_Sicht_von_vorn.jpg"
weight: "32"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/34090
imported:
- "2019"
_4images_image_id: "34090"
_4images_cat_id: "2522"
_4images_user_id: "184"
_4images_image_date: "2012-02-05T20:01:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34090 -->
Hier eine ältere Version. Das Z20 war zum Antrieb der Seilwinde gedacht.
Leider gibt es beim Lenken ein großes Problem. Das Fahrzeug ist vorn zu schwer.
Auf der Vorderachse lasten 2.5 bis 3 Kg.
Deswegen ist sie auch so komplex aufgebaut. Zuerst sollte sie, ähnlich wie die Hinterachse aussehen.
Das funktionierte aber nicht. Deswegen jetzt so. So frei nach dem Motto "Darf `s ein bisschen mehr sein".