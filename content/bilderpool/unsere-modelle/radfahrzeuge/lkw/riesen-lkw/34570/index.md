---
layout: "image"
title: "Ansicht von oben"
date: "2012-03-05T12:56:13"
picture: "03_Ansicht_von_oben.jpg"
weight: "3"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/34570
imported:
- "2019"
_4images_image_id: "34570"
_4images_cat_id: "2522"
_4images_user_id: "184"
_4images_image_date: "2012-03-05T12:56:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34570 -->
