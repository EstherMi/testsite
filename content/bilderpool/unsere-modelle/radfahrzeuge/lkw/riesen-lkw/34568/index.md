---
layout: "image"
title: "Gesamtansicht"
date: "2012-03-05T12:56:13"
picture: "Gesamt.jpg"
weight: "1"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/34568
imported:
- "2019"
_4images_image_id: "34568"
_4images_cat_id: "2522"
_4images_user_id: "184"
_4images_image_date: "2012-03-05T12:56:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34568 -->
Damit der LKW nicht so ganz allein dasteht habe ich noch einen Auflieger dazu gebaut.
H = 42, B = 33, L ohne Rampe = 110, L mit Rampe = 160 cm.
Die größten Probleme hatte ich bei der Hebemechanik für die Rampe.
Aber ich konnte sie lösen.