---
layout: "image"
title: "Klauenkupplung geschlossen"
date: "2012-02-05T20:01:27"
picture: "05_Klauenkupplung_geschlossen.jpg"
weight: "28"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/34086
imported:
- "2019"
_4images_image_id: "34086"
_4images_cat_id: "2522"
_4images_user_id: "184"
_4images_image_date: "2012-02-05T20:01:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34086 -->
Mit der Klauenkupplung wird die Vorderachse zum permanenten Heckantrieb dazugeschaltet.
Die Drehscheibe ist mit ihrer Achse mittels zweier Kugellager (in der Schneckenmutter) frei beweglich. 
Der Antrieb erfolgt über die rechte Kette.
Das Problem an der Sache:
Im Normalbetrieb (ohne Allrad) dreht sich die Drehscheibe immer mit weil sie fest mit dem Differential der Vorderachse verbunden ist.
Dieses muss hier beachtet werden. Sie wird, während sie sich dreht
(natürlich nur wenn der LKW auch in Bewegung ist) verschoben.
Der Antrieb des LKW erfolgt über die linke Kette.