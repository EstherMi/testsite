---
layout: "image"
title: "Radträger"
date: "2012-02-05T20:01:41"
picture: "16_Radtrger_3.jpg"
weight: "39"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/34097
imported:
- "2019"
_4images_image_id: "34097"
_4images_cat_id: "2522"
_4images_user_id: "184"
_4images_image_date: "2012-02-05T20:01:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34097 -->
eingebauter Radträger. Die großen Reifen werden zwischen den beiden Drehscheiben eingebaut.