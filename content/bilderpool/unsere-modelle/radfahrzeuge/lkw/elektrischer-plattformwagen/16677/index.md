---
layout: "image"
title: "Platformwagen 8"
date: "2008-12-20T19:31:36"
picture: "Platformwagen_10_klein.jpg"
weight: "8"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/16677
imported:
- "2019"
_4images_image_id: "16677"
_4images_cat_id: "1510"
_4images_user_id: "328"
_4images_image_date: "2008-12-20T19:31:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16677 -->
Hier die besondere Verlängerung des Servohebels. Montiert man das Servo mittig im Raster (z.B. wie hier über eine Bauplatte 15x45 mit Zapfen 38277), passt das Loch im Servohebel glücklicherweise perfekt ins Raster! Da hat bei FT wirklich mal jemand nachgedacht, danke!

Die Verlängerungsstrebe ist mit einem Strebenadapter 31848 exakt unter der Drehachse des Servos gelagert, und der schwarze Verbindungsstopfen 32316 verbindet Servohebel und Statikstrebe. Klappt wunderbar, und die Möglichkeiten des Servos werden erheblich erweitert, ohne aus dem Raster zu geraten!