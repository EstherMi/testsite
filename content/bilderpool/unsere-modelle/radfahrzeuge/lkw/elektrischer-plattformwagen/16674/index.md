---
layout: "image"
title: "Platformwagen 5"
date: "2008-12-20T19:31:36"
picture: "Platformwagen_06_klein.jpg"
weight: "5"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/16674
imported:
- "2019"
_4images_image_id: "16674"
_4images_cat_id: "1510"
_4images_user_id: "328"
_4images_image_date: "2008-12-20T19:31:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16674 -->
Extremer Lenkeinschlag + kurzer Radstand = erstaunliche Wendigkeit!