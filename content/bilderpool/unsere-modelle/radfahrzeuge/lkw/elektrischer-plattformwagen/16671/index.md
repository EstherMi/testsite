---
layout: "image"
title: "Platformwagen 2"
date: "2008-12-20T19:31:36"
picture: "Platformwagen_02_klein.jpg"
weight: "2"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/16671
imported:
- "2019"
_4images_image_id: "16671"
_4images_cat_id: "1510"
_4images_user_id: "328"
_4images_image_date: "2008-12-20T19:31:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16671 -->
