---
layout: "image"
title: "Platformwagen 7"
date: "2008-12-20T19:31:36"
picture: "Platformwagen_09_klein.jpg"
weight: "7"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/16676
imported:
- "2019"
_4images_image_id: "16676"
_4images_cat_id: "1510"
_4images_user_id: "328"
_4images_image_date: "2008-12-20T19:31:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16676 -->
Nochmal die Vorderachse von unten mit ihrer filigranen Aufhängung und Lenkung. Reicht aber, und funktioniert prächtig.