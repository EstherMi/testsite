---
layout: "image"
title: "Platformwagen 6"
date: "2008-12-20T19:31:36"
picture: "Platformwagen_08_klein.jpg"
weight: "6"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/16675
imported:
- "2019"
_4images_image_id: "16675"
_4images_cat_id: "1510"
_4images_user_id: "328"
_4images_image_date: "2008-12-20T19:31:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16675 -->
Die Vorderachse von unten. Die Aufhängung der Radnabenmotoren und der Vorderachse am Fahrzeug erfolgt nur über Strebenadapter 31848.