---
layout: "image"
title: "Auflieger"
date: "2008-03-16T19:36:09"
picture: "road-train2.jpg"
weight: "4"
konstrukteure: 
- "thkais"
fotografen:
- "thkais"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thkais"
license: "unknown"
legacy_id:
- details/13930
imported:
- "2019"
_4images_image_id: "13930"
_4images_cat_id: "205"
_4images_user_id: "41"
_4images_image_date: "2008-03-16T19:36:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13930 -->
So sieht dann der Auflieger des Super-Trucks Kastens aus, fertig zum Anhängen.
Der Auflieger ist etwas modifiziert, das Original liegt am Drehpunkt etwas zu hoch. Detailbilder werde ich bei Gelegenheit noch machen.