---
layout: "image"
title: "muellwagen16.jpg"
date: "2006-12-17T20:22:59"
picture: "muellwagen16.jpg"
weight: "16"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/7936
imported:
- "2019"
_4images_image_id: "7936"
_4images_cat_id: "742"
_4images_user_id: "5"
_4images_image_date: "2006-12-17T20:22:59"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7936 -->
