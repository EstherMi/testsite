---
layout: "image"
title: "muellwagen03.jpg"
date: "2006-12-17T20:22:59"
picture: "muellwagen03.jpg"
weight: "3"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/7923
imported:
- "2019"
_4images_image_id: "7923"
_4images_cat_id: "742"
_4images_user_id: "5"
_4images_image_date: "2006-12-17T20:22:59"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7923 -->
