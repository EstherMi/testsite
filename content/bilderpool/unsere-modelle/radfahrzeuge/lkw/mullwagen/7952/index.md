---
layout: "image"
title: "muellwagen32.jpg"
date: "2006-12-17T20:22:59"
picture: "muellwagen32.jpg"
weight: "32"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/7952
imported:
- "2019"
_4images_image_id: "7952"
_4images_cat_id: "742"
_4images_user_id: "5"
_4images_image_date: "2006-12-17T20:22:59"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7952 -->
