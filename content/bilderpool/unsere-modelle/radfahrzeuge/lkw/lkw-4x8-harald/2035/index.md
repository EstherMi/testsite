---
layout: "image"
title: "4x8Kl06.JPG"
date: "2003-12-23T20:27:34"
picture: "4x8Kl06.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2035
imported:
- "2019"
_4images_image_id: "2035"
_4images_cat_id: "432"
_4images_user_id: "4"
_4images_image_date: "2003-12-23T20:27:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2035 -->
Die Lenkung ist deutlich erkennbar vom LKW 0x6 abgeleitet worden.