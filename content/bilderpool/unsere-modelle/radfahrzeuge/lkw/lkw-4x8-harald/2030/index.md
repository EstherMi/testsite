---
layout: "image"
title: "4x8Kl01.JPG"
date: "2003-12-23T20:27:34"
picture: "4x8Kl01.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2030
imported:
- "2019"
_4images_image_id: "2030"
_4images_cat_id: "432"
_4images_user_id: "4"
_4images_image_date: "2003-12-23T20:27:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2030 -->
Stationen einer Entwicklung, oder: Auf dem Weg zum Allradantrieb.

Der Urgroßvater ist der LKW mit 3 Achsen ohne Antrieb (aber geländegängig), der unter 'PKW-Transporter' zu finden ist.

Das hier ist Großvater, der mit den kleinen Reifen 45 etwas unproportioniert daher kommt und wegen des langen Federwegs ohne Last ziemlich nach vorn hängt. Das 'Kl' im Dateinamen steht für 'kleine Reifen', die bei 'Vater' dann zu 'Gr'=große Reifen werden. 

Geländegängig sind alle. 'Großvater' und 'Vater' haben angetriebene Hinterachsen; erst die Generation 'Sohn' (siehe Kategorie 'Radar') hat Antrieb auf allen Achsen (8x8).