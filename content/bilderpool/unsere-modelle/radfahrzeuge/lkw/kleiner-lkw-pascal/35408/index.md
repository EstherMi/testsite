---
layout: "image"
title: "Die Lenkung"
date: "2012-08-29T20:08:53"
picture: "kleinerlkw06.jpg"
weight: "6"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- details/35408
imported:
- "2019"
_4images_image_id: "35408"
_4images_cat_id: "2627"
_4images_user_id: "1122"
_4images_image_date: "2012-08-29T20:08:53"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35408 -->
-