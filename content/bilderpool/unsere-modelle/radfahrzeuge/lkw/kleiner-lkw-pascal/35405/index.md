---
layout: "image"
title: "Von hinten"
date: "2012-08-29T20:08:53"
picture: "kleinerlkw03.jpg"
weight: "3"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- details/35405
imported:
- "2019"
_4images_image_id: "35405"
_4images_cat_id: "2627"
_4images_user_id: "1122"
_4images_image_date: "2012-08-29T20:08:53"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35405 -->
Gut zu sehen ist hier der AKKU.