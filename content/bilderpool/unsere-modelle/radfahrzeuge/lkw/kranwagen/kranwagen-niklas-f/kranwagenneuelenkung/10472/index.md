---
layout: "image"
title: "Kranwagen"
date: "2007-05-19T09:12:25"
picture: "kranwagen7.jpg"
weight: "7"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- details/10472
imported:
- "2019"
_4images_image_id: "10472"
_4images_cat_id: "954"
_4images_user_id: "557"
_4images_image_date: "2007-05-19T09:12:25"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10472 -->
lenkung