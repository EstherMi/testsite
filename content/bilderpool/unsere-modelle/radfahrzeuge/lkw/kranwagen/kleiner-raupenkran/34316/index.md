---
layout: "image"
title: "Verpackt in Koffer"
date: "2012-02-20T21:15:25"
picture: "kleinerraupenkran01.jpg"
weight: "1"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- details/34316
imported:
- "2019"
_4images_image_id: "34316"
_4images_cat_id: "2538"
_4images_user_id: "1122"
_4images_image_date: "2012-02-20T21:15:25"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34316 -->
Den kleinen Kran hat ich eigentlich gebaut, um ihn überall hin mit zunehmen, z.B.: Urlaub, besuch bei Oma und Opa... .