---
layout: "image"
title: "Von Vorne"
date: "2012-02-20T21:16:19"
picture: "kleinerraupenkran11.jpg"
weight: "11"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- details/34326
imported:
- "2019"
_4images_image_id: "34326"
_4images_cat_id: "2538"
_4images_user_id: "1122"
_4images_image_date: "2012-02-20T21:16:19"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34326 -->
-