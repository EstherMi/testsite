---
layout: "image"
title: "Von unten"
date: "2012-02-20T21:15:26"
picture: "kleinerraupenkran07.jpg"
weight: "7"
konstrukteure: 
- "pascal"
fotografen:
- "pascal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- details/34322
imported:
- "2019"
_4images_image_id: "34322"
_4images_cat_id: "2538"
_4images_user_id: "1122"
_4images_image_date: "2012-02-20T21:15:26"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34322 -->
Motor zum drehen des Oberteils.