---
layout: "image"
title: "Blick in die Fernbedinung"
date: "2012-02-23T21:07:06"
picture: "kleinerraupenkran14.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- details/34377
imported:
- "2019"
_4images_image_id: "34377"
_4images_cat_id: "2542"
_4images_user_id: "1122"
_4images_image_date: "2012-02-23T21:07:06"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34377 -->
-