---
layout: "image"
title: "Schwebeballast, ..."
date: "2012-02-23T21:06:55"
picture: "kleinerraupenkran04.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- details/34367
imported:
- "2019"
_4images_image_id: "34367"
_4images_cat_id: "2542"
_4images_user_id: "1122"
_4images_image_date: "2012-02-23T21:06:55"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34367 -->
... der 150g auf die Küchenwaage bringt.