---
layout: "image"
title: "Fernbedinung"
date: "2012-02-23T21:07:06"
picture: "kleinerraupenkran11.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pascal"
license: "unknown"
legacy_id:
- details/34374
imported:
- "2019"
_4images_image_id: "34374"
_4images_cat_id: "2542"
_4images_user_id: "1122"
_4images_image_date: "2012-02-23T21:07:06"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34374 -->
Der Akku ist ebenfalls in der Fernbedinung enthalten und einfach ein-und ausnehmbar.