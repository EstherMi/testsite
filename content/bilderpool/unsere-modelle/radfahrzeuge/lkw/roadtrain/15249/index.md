---
layout: "image"
title: "Das Dach lässt sich auch öffnen"
date: "2008-09-16T18:20:59"
picture: "009.jpg"
weight: "14"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/15249
imported:
- "2019"
_4images_image_id: "15249"
_4images_cat_id: "1395"
_4images_user_id: "184"
_4images_image_date: "2008-09-16T18:20:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15249 -->
Hier kann ein zweiter Akkublock eingebaut werden.
Im Augenblick ist hier die "Technik" für die Pneumatik untergebracht.
Der Kompressor läuft nur wenn auch Luft benötigt wird.