---
layout: "image"
title: "Trailer"
date: "2008-09-16T18:21:00"
picture: "017.jpg"
weight: "22"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/15257
imported:
- "2019"
_4images_image_id: "15257"
_4images_cat_id: "1395"
_4images_user_id: "184"
_4images_image_date: "2008-09-16T18:21:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15257 -->
von dieser Sorte gibt es 4 Stück im "Train".