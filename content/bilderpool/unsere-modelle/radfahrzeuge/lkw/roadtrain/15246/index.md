---
layout: "image"
title: "Blick von vorn"
date: "2008-09-16T18:20:59"
picture: "006.jpg"
weight: "11"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/15246
imported:
- "2019"
_4images_image_id: "15246"
_4images_cat_id: "1395"
_4images_user_id: "184"
_4images_image_date: "2008-09-16T18:20:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15246 -->
