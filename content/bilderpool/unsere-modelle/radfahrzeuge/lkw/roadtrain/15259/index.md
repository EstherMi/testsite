---
layout: "image"
title: "Ansicht"
date: "2008-09-16T18:21:00"
picture: "019.jpg"
weight: "24"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/15259
imported:
- "2019"
_4images_image_id: "15259"
_4images_cat_id: "1395"
_4images_user_id: "184"
_4images_image_date: "2008-09-16T18:21:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15259 -->
