---
layout: "image"
title: "Front"
date: "2008-09-16T18:20:59"
picture: "005.jpg"
weight: "10"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/15245
imported:
- "2019"
_4images_image_id: "15245"
_4images_cat_id: "1395"
_4images_user_id: "184"
_4images_image_date: "2008-09-16T18:20:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15245 -->
Überarbeitet.
Jetzt mit "Zierleisten" und Kuhfänger.