---
layout: "comment"
hidden: true
title: "19734"
date: "2014-11-24T12:59:27"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Gibt es die schwarze Feder in unterschiedlichen Längen oder Härtegraden?

Ich vermute, die kürzere Feder wurde nur zu lange in zusammengedrückter Lage gelagert/verbaut, so dass sie nun etwas kürzer geworden ist ...

Gruß, Thomas