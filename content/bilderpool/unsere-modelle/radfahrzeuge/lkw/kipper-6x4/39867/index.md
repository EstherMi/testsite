---
layout: "image"
title: "Kipper montiert"
date: "2014-11-23T19:12:24"
picture: "kipperx15.jpg"
weight: "15"
konstrukteure: 
- "Jeroen"
fotografen:
- "Jeroen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "yoeroen"
license: "unknown"
legacy_id:
- details/39867
imported:
- "2019"
_4images_image_id: "39867"
_4images_cat_id: "2993"
_4images_user_id: "2174"
_4images_image_date: "2014-11-23T19:12:24"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39867 -->
Im Hintergrund ist der Radsatz mit alten Differentialen (Prototyp) zu sehen. Das Ganze ist eigentlich zu Schwer für die Kunststoff Rastachsen.