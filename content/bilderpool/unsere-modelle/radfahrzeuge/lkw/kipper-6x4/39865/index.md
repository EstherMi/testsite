---
layout: "image"
title: "Korbe mit Ersatzrad"
date: "2014-11-23T19:12:24"
picture: "kipperx13.jpg"
weight: "13"
konstrukteure: 
- "Jeroen"
fotografen:
- "Jeroen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "yoeroen"
license: "unknown"
legacy_id:
- details/39865
imported:
- "2019"
_4images_image_id: "39865"
_4images_cat_id: "2993"
_4images_user_id: "2174"
_4images_image_date: "2014-11-23T19:12:24"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39865 -->
Korbe mit Ersatzrad