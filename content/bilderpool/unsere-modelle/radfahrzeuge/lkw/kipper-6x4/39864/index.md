---
layout: "image"
title: "Ein/Aus Schalter für LKW"
date: "2014-11-23T19:12:24"
picture: "kipperx12.jpg"
weight: "12"
konstrukteure: 
- "Jeroen"
fotografen:
- "Jeroen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "yoeroen"
license: "unknown"
legacy_id:
- details/39864
imported:
- "2019"
_4images_image_id: "39864"
_4images_cat_id: "2993"
_4images_user_id: "2174"
_4images_image_date: "2014-11-23T19:12:24"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39864 -->
Anfänglich wollte ich Schalter 36708 (Polwendeschalter) benutzen. Da aber dann die Möglichkeit besteht die Polen zu wenden und danach der Empfänger ausfällt habe ich diesen Schalter gewählt.
