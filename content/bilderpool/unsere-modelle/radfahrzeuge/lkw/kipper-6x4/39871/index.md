---
layout: "image"
title: "Antriebsmotor montiert"
date: "2014-11-23T19:12:24"
picture: "kipperx19.jpg"
weight: "19"
konstrukteure: 
- "Jeroen"
fotografen:
- "Jeroen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "yoeroen"
license: "unknown"
legacy_id:
- details/39871
imported:
- "2019"
_4images_image_id: "39871"
_4images_cat_id: "2993"
_4images_user_id: "2174"
_4images_image_date: "2014-11-23T19:12:24"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39871 -->
Diese Befestigung des Motors reicht aus; unter der Motor ist noch eine Strebe 60.
