---
layout: "image"
title: "Doppelte Hinterachse (Unteransicht)"
date: "2014-11-23T19:12:24"
picture: "kipperx09.jpg"
weight: "9"
konstrukteure: 
- "Jeroen"
fotografen:
- "Jeroen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "yoeroen"
license: "unknown"
legacy_id:
- details/39861
imported:
- "2019"
_4images_image_id: "39861"
_4images_cat_id: "2993"
_4images_user_id: "2174"
_4images_image_date: "2014-11-23T19:12:24"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39861 -->
Die zwei Platten 75*15 schränken die Bodenfreiheit etwas ein, aber es reicht noch immer aus.
