---
layout: "image"
title: "Hinterachse unter Untergestell"
date: "2014-11-23T19:12:24"
picture: "kipperx18.jpg"
weight: "18"
konstrukteure: 
- "Jeroen"
fotografen:
- "Jeroen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "yoeroen"
license: "unknown"
legacy_id:
- details/39870
imported:
- "2019"
_4images_image_id: "39870"
_4images_cat_id: "2993"
_4images_user_id: "2174"
_4images_image_date: "2014-11-23T19:12:24"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39870 -->
Ich habe die Bilder vom LKW von PK (2004) mal gut angeschaut. Hoffentlich liegt kein Urheberrecht darauf.
