---
layout: "image"
title: "Vorderachse montiert (1)"
date: "2014-11-23T19:12:24"
picture: "kipperx20.jpg"
weight: "20"
konstrukteure: 
- "Jeroen"
fotografen:
- "Jeroen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "yoeroen"
license: "unknown"
legacy_id:
- details/39872
imported:
- "2019"
_4images_image_id: "39872"
_4images_cat_id: "2993"
_4images_user_id: "2174"
_4images_image_date: "2014-11-23T19:12:24"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39872 -->
Zur Seite ist ein Achse 30 damit die Zapfen nicht zerbrechen.