---
layout: "image"
title: "Fahrerhaus und Kraftstofftank"
date: "2014-11-23T19:12:24"
picture: "kipperx24.jpg"
weight: "24"
konstrukteure: 
- "Jeroen"
fotografen:
- "Jeroen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "yoeroen"
license: "unknown"
legacy_id:
- details/39876
imported:
- "2019"
_4images_image_id: "39876"
_4images_cat_id: "2993"
_4images_user_id: "2174"
_4images_image_date: "2014-11-23T19:12:24"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39876 -->
Für die Befestigung der Kraftstofftank hab ich zwei Kettenglieder genommen. Das geht, ist aber nicht zu gut zu montieren; die gleiten immer vom Platz.
Motorhaube

