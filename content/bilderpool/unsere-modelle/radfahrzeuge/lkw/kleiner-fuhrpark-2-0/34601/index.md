---
layout: "image"
title: "Gruppenbild"
date: "2012-03-06T15:40:49"
picture: "gruppenbild.jpg"
weight: "1"
konstrukteure: 
- "Johannes Richter"
fotografen:
- "Johannes Richter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Beluthius"
license: "unknown"
legacy_id:
- details/34601
imported:
- "2019"
_4images_image_id: "34601"
_4images_cat_id: "2552"
_4images_user_id: "1443"
_4images_image_date: "2012-03-06T15:40:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34601 -->
So, mein Furhpark ist recht übersichtlich gworden, aber das hat einen recht einfachen Grund: mehr Teile habe ich nicht! Ich habe nicht einen Baustein mehr über, nur noch rote Kleinteile...
Deshalb ist der Anhänger des Sattelzuges auch so starkes Flickwerk.