---
layout: "image"
title: "Sattelzug mit geschlossenen Türen"
date: "2012-03-06T15:40:49"
picture: "sattelzug_detail_tuer_geschlossen.jpg"
weight: "2"
konstrukteure: 
- "Johannes Richter"
fotografen:
- "Johannes Richter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Beluthius"
license: "unknown"
legacy_id:
- details/34602
imported:
- "2019"
_4images_image_id: "34602"
_4images_cat_id: "2552"
_4images_user_id: "1443"
_4images_image_date: "2012-03-06T15:40:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34602 -->
