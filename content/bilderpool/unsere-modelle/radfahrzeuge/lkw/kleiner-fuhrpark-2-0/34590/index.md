---
layout: "image"
title: "monster_renner 1"
date: "2012-03-05T22:09:42"
picture: "rennwagen_01.jpg"
weight: "12"
konstrukteure: 
- "Johannes Richter"
fotografen:
- "Johannes Richter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Beluthius"
license: "unknown"
legacy_id:
- details/34590
imported:
- "2019"
_4images_image_id: "34590"
_4images_cat_id: "2552"
_4images_user_id: "1443"
_4images_image_date: "2012-03-05T22:09:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34590 -->
Ein wahrhaft großer "Rennwagen" Quasi ein tiefergelegter LKW-Renner. Hat sich so ergeben, als ich ein wenig drauf los gebastelt habe...