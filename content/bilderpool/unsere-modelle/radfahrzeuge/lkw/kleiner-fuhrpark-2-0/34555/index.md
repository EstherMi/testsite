---
layout: "image"
title: "zugmaschiene 6"
date: "2012-03-04T19:50:12"
picture: "zugmaschiene_06_oben.jpg"
weight: "31"
konstrukteure: 
- "Johannes Richter"
fotografen:
- "Johannes Richter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Beluthius"
license: "unknown"
legacy_id:
- details/34555
imported:
- "2019"
_4images_image_id: "34555"
_4images_cat_id: "2552"
_4images_user_id: "1443"
_4images_image_date: "2012-03-04T19:50:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34555 -->
