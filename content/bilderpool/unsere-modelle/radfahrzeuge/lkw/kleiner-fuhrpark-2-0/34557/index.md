---
layout: "image"
title: "zugmaschiene 8"
date: "2012-03-04T19:50:12"
picture: "zugmaschiene_unten.jpg"
weight: "33"
konstrukteure: 
- "Johannes Richter"
fotografen:
- "Johannes Richter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Beluthius"
license: "unknown"
legacy_id:
- details/34557
imported:
- "2019"
_4images_image_id: "34557"
_4images_cat_id: "2552"
_4images_user_id: "1443"
_4images_image_date: "2012-03-04T19:50:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34557 -->
Die ganze "Pracht" des Aufbaus. Habe ich innerhalb einer Woche zigmal geändert. Denke aber es bleibt erst einmal dabei und ich vervollständige die Zugmaschiene und den Anhänger erst einmal.

Meine Überschrift lautet ja: Kleiner Fuhrpark. Es gibt natürlich noch mehr Fahrzeuge; eine Art "Renn-LKW" sowie einen recht geländegängigen "LKW/Buggy"
Lade ich hoch, sobald ich Zeit habe...