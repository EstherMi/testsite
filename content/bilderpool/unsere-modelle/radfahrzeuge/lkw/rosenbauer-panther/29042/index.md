---
layout: "image"
title: "21 Kompressor"
date: "2010-10-19T18:24:56"
picture: "rosenbauerpanther10_2.jpg"
weight: "29"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/29042
imported:
- "2019"
_4images_image_id: "29042"
_4images_cat_id: "2107"
_4images_user_id: "860"
_4images_image_date: "2010-10-19T18:24:56"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29042 -->
Die Welle vom Kompressor ist natürlich an der Decke befestigt.