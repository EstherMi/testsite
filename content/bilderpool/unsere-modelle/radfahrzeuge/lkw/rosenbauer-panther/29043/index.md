---
layout: "image"
title: "22 Tank"
date: "2010-10-19T18:24:56"
picture: "rosenbauerpanther11_2.jpg"
weight: "30"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/29043
imported:
- "2019"
_4images_image_id: "29043"
_4images_cat_id: "2107"
_4images_user_id: "860"
_4images_image_date: "2010-10-19T18:24:56"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29043 -->
Der Tank, der Akku
untendrunter ist der Motor, der den Wasserwerfer dreht.