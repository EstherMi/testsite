---
layout: "image"
title: "12 Reichweite"
date: "2010-10-19T18:24:54"
picture: "rosenbauerpanther01_2.jpg"
weight: "20"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/29033
imported:
- "2019"
_4images_image_id: "29033"
_4images_cat_id: "2107"
_4images_user_id: "860"
_4images_image_date: "2010-10-19T18:24:54"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29033 -->
Um so weniger Wasser im Tank ist, desto mehr Druck kann aufgebaut werden und umso weiter spritzt er auch. Tatsächlich kommt er bis zu dem Tischbein, das etwa 1 Meter entfernt ist.