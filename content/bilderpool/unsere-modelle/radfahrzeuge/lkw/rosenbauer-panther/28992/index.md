---
layout: "image"
title: "05 Wasserwerfer"
date: "2010-10-14T17:59:02"
picture: "rosenbauerpanther05.jpg"
weight: "37"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/28992
imported:
- "2019"
_4images_image_id: "28992"
_4images_cat_id: "2107"
_4images_user_id: "860"
_4images_image_date: "2010-10-14T17:59:02"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28992 -->
Der kraftvolle Dachwasserwerfer ist wie beim Original drehbar. Nur für hoch/runter war kein Platz mehr für einen Motor.