---
layout: "image"
title: "35 Interface"
date: "2011-01-23T20:20:32"
picture: "rosenbauerpanther11_3.jpg"
weight: "11"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/29773
imported:
- "2019"
_4images_image_id: "29773"
_4images_cat_id: "2107"
_4images_user_id: "860"
_4images_image_date: "2011-01-23T20:20:32"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29773 -->
Hier sieht man die Schnittstelle von hinten, die zum Steuerpult geführt hat.