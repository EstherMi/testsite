---
layout: "image"
title: "06 Tankauffüllung"
date: "2010-10-14T17:59:02"
picture: "rosenbauerpanther06.jpg"
weight: "38"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/28993
imported:
- "2019"
_4images_image_id: "28993"
_4images_cat_id: "2107"
_4images_user_id: "860"
_4images_image_date: "2010-10-14T17:59:02"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28993 -->
An der Seite befindet sich diese Klappe, hinter der sich die Tankauffüllung befindet. Man kann auch eine manuelle Pumpe anschließen, um mehr Druck zu bekommen.
Die Lampe ist normalerweise an, wenn der Taster nicht gedrückt ist.