---
layout: "image"
title: "28 Vorne"
date: "2011-01-23T20:20:32"
picture: "rosenbauerpanther04_3.jpg"
weight: "4"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/29766
imported:
- "2019"
_4images_image_id: "29766"
_4images_cat_id: "2107"
_4images_user_id: "860"
_4images_image_date: "2011-01-23T20:20:32"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29766 -->
