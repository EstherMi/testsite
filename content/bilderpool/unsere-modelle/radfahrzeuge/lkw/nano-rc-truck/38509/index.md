---
layout: "image"
title: "Nano-RC-Truck 11"
date: "2014-03-26T20:48:57"
picture: "Nano-RC-Truck_11.jpg"
weight: "8"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/38509
imported:
- "2019"
_4images_image_id: "38509"
_4images_cat_id: "2872"
_4images_user_id: "328"
_4images_image_date: "2014-03-26T20:48:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38509 -->
Der Antrieb hat mir viele schlaflose Nächte bereitet ...

Grundidee ist, dass die kleinen Räder 14 (36573) auf Klemmstifte D4,1 (107356) gepresst werden. Nur so kann man das Antriebsmoment zuverlässig auf die Straße bringen.

Dass die Riegelscheibe über einen Baustein 7,5 perfekt an das Getriebezahnrad passt, war ein Zufallstreffer! Aber erst das war die Basis für den minimalistischen Antrieb.