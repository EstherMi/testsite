---
layout: "image"
title: "Nano-RC-Truck 2"
date: "2014-03-24T20:07:36"
picture: "Nano-RC-Truck_2.jpg"
weight: "17"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/38487
imported:
- "2019"
_4images_image_id: "38487"
_4images_cat_id: "2872"
_4images_user_id: "328"
_4images_image_date: "2014-03-24T20:07:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38487 -->
