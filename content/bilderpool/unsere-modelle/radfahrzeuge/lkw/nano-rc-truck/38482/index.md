---
layout: "image"
title: "Nano-RC-Truck 1"
date: "2014-03-23T13:22:44"
picture: "Nano-RC-Truck_1.jpg"
weight: "18"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/38482
imported:
- "2019"
_4images_image_id: "38482"
_4images_cat_id: "2872"
_4images_user_id: "328"
_4images_image_date: "2014-03-23T13:22:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38482 -->
Vor ziemlich genau 2 Jahren zeigte ich hier in der ftc mein Modell "Micro-RC-LKW II", ein ferngesteuertes Fahrzeug auf einer Breite von nur 3 Grundbausteinen. Damals schrieb ich dazu:

"Ich denke, damit sollte das Ende der Fahnenstange hinsichtlich miniaturisierter ferngesteuerter FT-Fahrzeuge erreicht sein! Kleiner kann ich mir ein FT-Fahrzeug mit Control Set und ohne Modifikationen nicht mehr vorstellen. Aber man weiß ja nie ... ;o)"

Nun ja, viele Monate hatte ich die Idee im Kopf, dass es doch noch kleiner gehen müsste, quasi IR-Empfänger-Breite (30 mm) + Verkleidung. Und es ist gelungen!

Fast 1 Jahr habe ich an diesem Modell getüftelt - inkl. Pausen und herben Rückschlägen. Aber nun kann ich es präsentieren: Das wohl kleinste ferngesteuerte FT-Modell der Welt!

Die Details:

- Antrieb per XS-Motor auf die beiden Achsen des Aufliegers 
- Knicklenkung per Servo über die Drehachse des Aufliegers
- Ein/Aus-Schalter im Heck
- Stromversorgung per 9V-Blockakku 
- Fernsteuerung per Control Set 
- realistische Optik und Proportionen
- Gesamtbreite nur 2 Grundbausteine + Verkleidung

Alles natürlich nach dem FT-Reinheitsgebot ohne Fremdteile, Modifikationen oder Kleber!

Jetzt sollte aber wirklich die Grenze der Miniaturisierung von ferngesteuerten FT-Modellen erreicht sein ... ;o)

Aber nicht alles ist toll: Die kleinen Räder 14 (36573) haben leider überhaupt keinen Grip auf glatten Untergründen, so dass der Truck z.B. auf einem Glastisch kaum vom Fleck kommt. Die Lenkung ist dann auch nicht wirkungsvoll, und er schiebt einfach stur geradeaus und ignoriert den Lenkvorgang völlig. Gut funktioniert es aber auf rauhem Holz oder Papier.

Ein Video erstelle ich noch!