---
layout: "image"
title: "Nano-RC-Truck 18"
date: "2014-03-26T20:48:57"
picture: "Nano-RC-Truck_18.jpg"
weight: "15"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/38516
imported:
- "2019"
_4images_image_id: "38516"
_4images_cat_id: "2872"
_4images_user_id: "328"
_4images_image_date: "2014-03-26T20:48:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38516 -->
Das Servo hat oben innen einen definierten Anschlag, so dass es beim Zusammenbau nicht zu tief in den Auflieger rein rutscht.

Rechts im Bild angedeutete Stützen.