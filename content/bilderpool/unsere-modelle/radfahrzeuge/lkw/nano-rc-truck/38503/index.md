---
layout: "image"
title: "Nano-RC-Truck 5"
date: "2014-03-26T20:48:57"
picture: "Nano-RC-Truck_5.jpg"
weight: "2"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/38503
imported:
- "2019"
_4images_image_id: "38503"
_4images_cat_id: "2872"
_4images_user_id: "328"
_4images_image_date: "2014-03-26T20:48:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38503 -->
Die Batterie (9V-Blockakku) liegt hinter dem Empfänger im Auflieger.