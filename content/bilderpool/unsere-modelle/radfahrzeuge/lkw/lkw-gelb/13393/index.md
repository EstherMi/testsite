---
layout: "image"
title: "Ansicht von unten"
date: "2008-01-25T15:36:14"
picture: "DSCN2051.jpg"
weight: "15"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/13393
imported:
- "2019"
_4images_image_id: "13393"
_4images_cat_id: "1220"
_4images_user_id: "184"
_4images_image_date: "2008-01-25T15:36:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13393 -->
Ganz simple Konstruktion dieser Drehschemel.