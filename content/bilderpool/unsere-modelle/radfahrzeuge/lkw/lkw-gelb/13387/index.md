---
layout: "image"
title: "PKW mit Kipp-Ladefläche"
date: "2008-01-25T15:36:04"
picture: "DSCN2041.jpg"
weight: "9"
konstrukteure: 
- "Ludger Mäsing und ft"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/13387
imported:
- "2019"
_4images_image_id: "13387"
_4images_cat_id: "1220"
_4images_user_id: "184"
_4images_image_date: "2008-01-25T15:36:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13387 -->
Nachbau