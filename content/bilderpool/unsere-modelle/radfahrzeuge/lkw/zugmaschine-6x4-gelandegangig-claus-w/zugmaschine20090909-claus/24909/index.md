---
layout: "image"
title: "[1/6] Diagonalansicht links"
date: "2009-09-11T21:40:45"
picture: "zugmaschineclaus1.jpg"
weight: "1"
konstrukteure: 
- "Claus-Werner Ludwig"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/24909
imported:
- "2019"
_4images_image_id: "24909"
_4images_cat_id: "1717"
_4images_user_id: "723"
_4images_image_date: "2009-09-11T21:40:45"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24909 -->
