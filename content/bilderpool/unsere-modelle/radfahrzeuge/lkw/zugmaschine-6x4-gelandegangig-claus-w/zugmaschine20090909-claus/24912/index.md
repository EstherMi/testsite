---
layout: "image"
title: "[4/6] Technik im Heck"
date: "2009-09-11T21:40:46"
picture: "zugmaschineclaus4.jpg"
weight: "4"
konstrukteure: 
- "Claus-Werner Ludwig"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/24912
imported:
- "2019"
_4images_image_id: "24912"
_4images_cat_id: "1717"
_4images_user_id: "723"
_4images_image_date: "2009-09-11T21:40:46"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24912 -->
