---
layout: "image"
title: "[2/6] Frontansicht"
date: "2009-09-11T21:40:46"
picture: "zugmaschineclaus2.jpg"
weight: "2"
konstrukteure: 
- "Claus-Werner Ludwig"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/24910
imported:
- "2019"
_4images_image_id: "24910"
_4images_cat_id: "1717"
_4images_user_id: "723"
_4images_image_date: "2009-09-11T21:40:46"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24910 -->
