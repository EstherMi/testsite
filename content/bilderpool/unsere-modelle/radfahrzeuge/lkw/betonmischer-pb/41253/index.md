---
layout: "image"
title: "Mixer-lr-14"
date: "2015-06-24T14:11:15"
picture: "betonmischer07.jpg"
weight: "7"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/41253
imported:
- "2019"
_4images_image_id: "41253"
_4images_cat_id: "3085"
_4images_user_id: "2449"
_4images_image_date: "2015-06-24T14:11:15"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41253 -->
