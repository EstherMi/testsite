---
layout: "image"
title: "Ausgangsposition"
date: "2011-11-21T22:08:23"
picture: "wlfzetros02.jpg"
weight: "2"
konstrukteure: 
- "Masked (Martin)"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/33536
imported:
- "2019"
_4images_image_id: "33536"
_4images_cat_id: "2482"
_4images_user_id: "373"
_4images_image_date: "2011-11-21T22:08:23"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33536 -->
Man erkennt unten schon die Schraubenelemente der Teleskopmechanik