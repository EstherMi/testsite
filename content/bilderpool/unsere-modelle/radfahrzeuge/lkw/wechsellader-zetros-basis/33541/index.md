---
layout: "image"
title: "Antrieb Klappmechanismus"
date: "2011-11-21T22:08:24"
picture: "wlfzetros07.jpg"
weight: "7"
konstrukteure: 
- "Masked (Martin)"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/33541
imported:
- "2019"
_4images_image_id: "33541"
_4images_cat_id: "2482"
_4images_user_id: "373"
_4images_image_date: "2011-11-21T22:08:24"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33541 -->
Zwei Minimotoren waren die optimale Kombination. Ein 50:1er-PM war immer noch zu schnell, für ein Getriebe war kein Platz mehr. Also war das die Beste Lösung. Klar, theoretisch bestehen Drehzahlunterschiede. Aber wirklich relevant waren sie jetzt nicht, insbesondere da die Motoren parallel geschaltet sind.