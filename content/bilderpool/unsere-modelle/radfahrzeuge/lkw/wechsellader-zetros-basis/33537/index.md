---
layout: "image"
title: "ausgefahren"
date: "2011-11-21T22:08:23"
picture: "wlfzetros03.jpg"
weight: "3"
konstrukteure: 
- "Masked (Martin)"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/33537
imported:
- "2019"
_4images_image_id: "33537"
_4images_cat_id: "2482"
_4images_user_id: "373"
_4images_image_date: "2011-11-21T22:08:23"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33537 -->
Zuerst wird der komplette, schwenkende Arm verkürzt, um den Hebel kürzer zu machen und somit einfacher klappbar zu halten.
Auf weiteren Bildern sieht man die Mechanik dafür.