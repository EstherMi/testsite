---
layout: "image"
title: "vorne"
date: "2008-08-31T08:58:52"
picture: "DSC00820.jpg"
weight: "7"
konstrukteure: 
- "lil mike"
fotografen:
- "lil mike"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lil-mike"
license: "unknown"
legacy_id:
- details/15156
imported:
- "2019"
_4images_image_id: "15156"
_4images_cat_id: "1172"
_4images_user_id: "822"
_4images_image_date: "2008-08-31T08:58:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15156 -->
