---
layout: "image"
title: "Zugmaschine"
date: "2007-11-30T19:45:05"
picture: "achszugmaschine2.jpg"
weight: "11"
konstrukteure: 
- "stefanft"
fotografen:
- "stefanft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stefanft"
license: "unknown"
legacy_id:
- details/12942
imported:
- "2019"
_4images_image_id: "12942"
_4images_cat_id: "1172"
_4images_user_id: "672"
_4images_image_date: "2007-11-30T19:45:05"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12942 -->
Als Fernsteuerung wird hier eine aus dem RC-Bereich verwendet. Mit dem IR Control Set von ft ginge das natürlich auch.