---
layout: "image"
title: "Auflieger"
date: "2007-11-30T19:45:05"
picture: "achszugmaschine6.jpg"
weight: "15"
konstrukteure: 
- "stefanft"
fotografen:
- "stefanft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stefanft"
license: "unknown"
legacy_id:
- details/12946
imported:
- "2019"
_4images_image_id: "12946"
_4images_cat_id: "1172"
_4images_user_id: "672"
_4images_image_date: "2007-11-30T19:45:05"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12946 -->
Der Auflieger ist "geklaut": es handelt sich dabei um das ft fanclub Modell "Nr. 10". War damals als Auflieger für den "King of the Road"-Baukasten gedacht.