---
layout: "image"
title: "Zugmaschine"
date: "2007-11-30T19:45:04"
picture: "achszugmaschine1.jpg"
weight: "10"
konstrukteure: 
- "stefanft"
fotografen:
- "stefanft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stefanft"
license: "unknown"
legacy_id:
- details/12941
imported:
- "2019"
_4images_image_id: "12941"
_4images_cat_id: "1172"
_4images_user_id: "672"
_4images_image_date: "2007-11-30T19:45:04"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12941 -->
Wird angetrieben von 4 S-Motoren. Power-Motoren wären mir lieber gewesen, allerdings wollte ich die Proportionen so halbwegs am Original orientieren - und da waren die Power-Motoren vom Platz her nicht unter zu bringen.