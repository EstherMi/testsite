---
layout: "image"
title: "lkwmehr2.jpg"
date: "2017-03-23T14:27:57"
picture: "lkwmehr2.jpg"
weight: "2"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/45639
imported:
- "2019"
_4images_image_id: "45639"
_4images_cat_id: "3389"
_4images_user_id: "2449"
_4images_image_date: "2017-03-23T14:27:57"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45639 -->
