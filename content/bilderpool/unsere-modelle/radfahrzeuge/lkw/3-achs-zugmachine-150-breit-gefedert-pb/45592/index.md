---
layout: "image"
title: "lkw04.jpg"
date: "2017-03-22T15:50:27"
picture: "lkw04.jpg"
weight: "9"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/45592
imported:
- "2019"
_4images_image_id: "45592"
_4images_cat_id: "3389"
_4images_user_id: "2449"
_4images_image_date: "2017-03-22T15:50:27"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45592 -->
Von die Zwei Federn in Mitte, ist der schwarzen Boden entfernt. Die offene Seite fällt später über ein Statik-Riegel
Die kleinen Motor mit Hubgetriebe versorgt den Lenkung.