---
layout: "image"
title: "lkwmehr4.jpg"
date: "2017-03-23T14:27:57"
picture: "lkwmehr4.jpg"
weight: "4"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/45641
imported:
- "2019"
_4images_image_id: "45641"
_4images_cat_id: "3389"
_4images_user_id: "2449"
_4images_image_date: "2017-03-23T14:27:57"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45641 -->
Eine Weg um die Stütze des Aufliegers motorisiert aus zu schieben. Strom kan man abnimmen vom Zugmaschine (wenn da ausser Fahren und Lenken keine andere Funktionen wie Licht oder Hupe verwendet sind)