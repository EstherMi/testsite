---
layout: "image"
title: "lkw18.jpg"
date: "2017-03-22T15:50:27"
picture: "lkw18.jpg"
weight: "23"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/45606
imported:
- "2019"
_4images_image_id: "45606"
_4images_cat_id: "3389"
_4images_user_id: "2449"
_4images_image_date: "2017-03-22T15:50:27"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45606 -->
