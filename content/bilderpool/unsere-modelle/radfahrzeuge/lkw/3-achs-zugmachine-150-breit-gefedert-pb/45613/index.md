---
layout: "image"
title: "lkw25.jpg"
date: "2017-03-22T15:50:27"
picture: "lkw25.jpg"
weight: "30"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/45613
imported:
- "2019"
_4images_image_id: "45613"
_4images_cat_id: "3389"
_4images_user_id: "2449"
_4images_image_date: "2017-03-22T15:50:27"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45613 -->
