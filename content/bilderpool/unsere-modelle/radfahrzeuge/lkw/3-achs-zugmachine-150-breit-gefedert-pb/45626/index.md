---
layout: "image"
title: "lkw38.jpg"
date: "2017-03-22T15:50:27"
picture: "lkw38.jpg"
weight: "43"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/45626
imported:
- "2019"
_4images_image_id: "45626"
_4images_cat_id: "3389"
_4images_user_id: "2449"
_4images_image_date: "2017-03-22T15:50:27"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45626 -->
