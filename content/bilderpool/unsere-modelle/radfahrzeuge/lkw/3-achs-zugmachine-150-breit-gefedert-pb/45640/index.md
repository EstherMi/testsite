---
layout: "image"
title: "lkwmehr3.jpg"
date: "2017-03-23T14:27:57"
picture: "lkwmehr3.jpg"
weight: "3"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/45640
imported:
- "2019"
_4images_image_id: "45640"
_4images_cat_id: "3389"
_4images_user_id: "2449"
_4images_image_date: "2017-03-23T14:27:57"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45640 -->
