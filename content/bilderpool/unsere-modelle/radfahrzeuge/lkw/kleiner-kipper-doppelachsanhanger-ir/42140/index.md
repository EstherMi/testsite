---
layout: "image"
title: "Mit Hydraulik-Anhänger"
date: "2015-10-26T11:27:24"
picture: "DSC08329_1.jpg"
weight: "1"
konstrukteure: 
- "Peter Habermehl"
fotografen:
- "Peter Habermehl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- details/42140
imported:
- "2019"
_4images_image_id: "42140"
_4images_cat_id: "3137"
_4images_user_id: "2488"
_4images_image_date: "2015-10-26T11:27:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42140 -->
Dafür war der LKW gedacht - als Zugmaschine für den Hydraulik-Anhänger.

Der rote Doppelachsanhänger entstand dann, weil ich enorme Probleme beim Rückwärtsrangieren hatte...