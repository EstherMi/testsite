---
layout: "overview"
title: "Kleiner Kipper mit Doppelachsanhänger, Pneumatik und IR Fernsteuerung"
date: 2019-12-17T18:42:14+01:00
legacy_id:
- categories/3137
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3137 --> 
Kleiner  Kipper-LKW mit Doppelachs-Kippanhänger und IR Fernsteuerung im Stil der 80er Modellbaukästen.