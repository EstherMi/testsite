---
layout: "comment"
hidden: true
title: "21193"
date: "2015-11-03T11:03:50"
uploadBy:
- "PHabermehl"
license: "unknown"
imported:
- "2019"
---
Hallo Stefan,

Danke für das Lob - der "Kleine" kommt bei meinen Kindern super an, er dient jetzt hauptsächlich dazu, MAOAMs zum Automaten zu liefern :-)

Die Ventile sind nicht original FT, sondern Ersatz, den ich im Internet auftun konnte. Sie sind kompakter, haben eine etwas geringere Stromaufnahme, schalten bei ca. 0,5v niedrigerer Spannung als die Originale und kosten so um 12¤ pro Stück...

Gruß
Peter
Ich habe sogar noch ein paar abzugeben (mußte en gros bestellen :-()