---
layout: "image"
title: "Gesamtansicht von oben ohne Abdeckungen"
date: "2009-11-11T20:20:46"
picture: "ferngesteuerteselektromehrzweckfahrzeug6.jpg"
weight: "6"
konstrukteure: 
- "Minneralwasser"
fotografen:
- "Minneralwasser"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Minneralwasser"
license: "unknown"
legacy_id:
- details/25760
imported:
- "2019"
_4images_image_id: "25760"
_4images_cat_id: "1806"
_4images_user_id: "1019"
_4images_image_date: "2009-11-11T20:20:46"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25760 -->
Auch der Motor wurde mit Klebeband befestigt. Dieses dient aber nur zur Fixierung, damit der Motor nicht aus der Mulde im Chassis rutscht.