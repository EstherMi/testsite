---
layout: "overview"
title: "Ferngesteuertes Mehrzweckfahrzeug"
date: 2019-12-17T18:40:47+01:00
legacy_id:
- categories/1806
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1806 --> 
Die Antriebs- und Fernsteuerkomponenten, sowie der Akku sind nicht-Fischertechnikteile. Diese stammen aus dem RC-Car-Bereich. Das Fahrzeug ist recht flott unterwegs.[br]Allerdings ist bei den Geschwindigkeiten, die es erreicht schon ein Verschleiß an den Ft-Teilen. Besonders die Achsen und deren Lagerung sind betroffen.[br][br]Technische Daten[br]Motor: Industriebürstenmotor der Baugröße 540 mit 18 Turns[br]Regler: Robbe Rookie[br]Servo: Carson[br]Gewicht: 900 Gramm mit Akku[br]Geschwindigkeit: geschätzt ca. 15 km/h (mache demnächst einen genauen Test mit dem Navi) [br]Fahrzeit: mit dem 3000er Akku etwas über 20 Minuten