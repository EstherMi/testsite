---
layout: "image"
title: "Anlenkung"
date: "2009-11-11T20:20:46"
picture: "ferngesteuerteselektromehrzweckfahrzeug7.jpg"
weight: "7"
konstrukteure: 
- "Minneralwasser"
fotografen:
- "Minneralwasser"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Minneralwasser"
license: "unknown"
legacy_id:
- details/25761
imported:
- "2019"
_4images_image_id: "25761"
_4images_cat_id: "1806"
_4images_user_id: "1019"
_4images_image_date: "2009-11-11T20:20:46"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25761 -->
Die Anlenkung übernimmt ein herkömmliches RC-Car Servo. Auch hier hat der Zufall mitgespielt, sodass die schwarzen "Nieten" zur Befestigung des Anlenkarmes verwendet werden konnten. 
Übrigens wurde auch das Servo mit Klebeband befestigt.
Der hier im Bild zu sehende Regler hatte anfangs seinen Platz unter der gelben Lenkstange, da er aber dem Ende des Antriebsritzels auf der Motorachse zu Nahe kam (siehe Bild 9), musste ich ihn versetzen.
Zudem war dort unten auch nicht ausreichend Fahrtwind um ihn gut zu kühlen.