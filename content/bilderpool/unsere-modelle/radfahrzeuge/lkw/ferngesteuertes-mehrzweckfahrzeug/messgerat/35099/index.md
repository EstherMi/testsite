---
layout: "image"
title: "Steckenmessung"
date: "2012-07-06T10:46:19"
picture: "ftMessung1.jpg"
weight: "1"
konstrukteure: 
- "Gerhard Birkenstock"
fotografen:
- "Gerhard Birkenstock"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "gggb"
license: "unknown"
legacy_id:
- details/35099
imported:
- "2019"
_4images_image_id: "35099"
_4images_cat_id: "2604"
_4images_user_id: "1524"
_4images_image_date: "2012-07-06T10:46:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35099 -->
Automatisches fahren eines Schlittens.
Gesteuert über ROBO Interface und Ansteuern eines Schrittmotors wird der Schlitten mit einer Geschwindigkeit von 1mm pro Minute gefahren. Das große Zahnrad mit 80 Zähnen wurde ursprünglich für ein anderes Projekt auf der CNC hergestellt. Hier wurde es der Bequemlichkeit halber wieder verwendet.