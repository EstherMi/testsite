---
layout: "image"
title: "Akku Schacht"
date: "2009-11-11T20:20:46"
picture: "ferngesteuerteselektromehrzweckfahrzeug3.jpg"
weight: "3"
konstrukteure: 
- "Minneralwasser"
fotografen:
- "Minneralwasser"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Minneralwasser"
license: "unknown"
legacy_id:
- details/25757
imported:
- "2019"
_4images_image_id: "25757"
_4images_cat_id: "1806"
_4images_user_id: "1019"
_4images_image_date: "2009-11-11T20:20:46"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25757 -->
Die Platten, die den Akku verdecken, sind an Scharnieren befestigt. So kann man diese ganz bequem hochklappen, um den Akku zu wechseln.