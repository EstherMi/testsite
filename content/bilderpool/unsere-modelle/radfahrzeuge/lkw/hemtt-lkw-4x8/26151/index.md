---
layout: "image"
title: "Antrieb"
date: "2010-01-25T19:29:25"
picture: "DSCN0251.jpg"
weight: "10"
konstrukteure: 
- "Bastian"
fotografen:
- "Bastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Basti13.02"
license: "unknown"
legacy_id:
- details/26151
imported:
- "2019"
_4images_image_id: "26151"
_4images_cat_id: "1853"
_4images_user_id: "1046"
_4images_image_date: "2010-01-25T19:29:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26151 -->
Ich werde den Antrieb noch etwas verändern. Anstatt den zwei Differenzialgetrieben werde ich zwei durchgehende Metallachsen verwenden zur besseren Kraftübertragung.