---
layout: "image"
title: "Kabine"
date: "2010-01-25T19:29:24"
picture: "DSCN0239.jpg"
weight: "2"
konstrukteure: 
- "Bastian"
fotografen:
- "Bastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Basti13.02"
license: "unknown"
legacy_id:
- details/26143
imported:
- "2019"
_4images_image_id: "26143"
_4images_cat_id: "1853"
_4images_user_id: "1046"
_4images_image_date: "2010-01-25T19:29:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26143 -->
Rechts hinter dem Fahrer sieht man die Membranpume, links davon den Drucklufttank.