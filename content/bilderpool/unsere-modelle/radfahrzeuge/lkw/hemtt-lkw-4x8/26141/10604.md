---
layout: "comment"
hidden: true
title: "10604"
date: "2010-01-25T09:15:37"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Da schließe ich mich an. Schönes Modell! Ein paar Fotos von den Innereien des Führerhauses und von der Unterseite wären noch interessant.

Gruß,
Stefan