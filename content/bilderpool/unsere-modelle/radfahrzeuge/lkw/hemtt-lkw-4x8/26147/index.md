---
layout: "image"
title: "'Maschinenraum' offen"
date: "2010-01-25T19:29:25"
picture: "DSCN0245.jpg"
weight: "6"
konstrukteure: 
- "Bastian"
fotografen:
- "Bastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Basti13.02"
license: "unknown"
legacy_id:
- details/26147
imported:
- "2019"
_4images_image_id: "26147"
_4images_cat_id: "1853"
_4images_user_id: "1046"
_4images_image_date: "2010-01-25T19:29:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26147 -->
Die zwei Empfänger, Verkabelung, Blink-Modul und die zwei Tasteer für Blinklicht und Arbeitsscheinwerfer