---
layout: "image"
title: "Pneumatik"
date: "2010-01-25T19:29:25"
picture: "DSCN0241.jpg"
weight: "3"
konstrukteure: 
- "Bastian"
fotografen:
- "Bastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Basti13.02"
license: "unknown"
legacy_id:
- details/26144
imported:
- "2019"
_4images_image_id: "26144"
_4images_cat_id: "1853"
_4images_user_id: "1046"
_4images_image_date: "2010-01-25T19:29:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26144 -->
Linkes Ventil ist zum Befüllen des Tanke (eigentlich nicht nötig), rechtes Ventil ist zum Steuern des Frontgerätes.