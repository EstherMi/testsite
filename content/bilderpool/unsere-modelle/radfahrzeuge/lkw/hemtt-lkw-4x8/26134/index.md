---
layout: "image"
title: "HEMTT 4x8 Seitenansicht"
date: "2010-01-24T20:29:49"
picture: "DSCN0237.jpg"
weight: "11"
konstrukteure: 
- "Bastian"
fotografen:
- "Bastian"
keywords: ["LKW", "HEMTT", "4x8"]
uploadBy: "Basti13.02"
license: "unknown"
legacy_id:
- details/26134
imported:
- "2019"
_4images_image_id: "26134"
_4images_cat_id: "1853"
_4images_user_id: "1046"
_4images_image_date: "2010-01-24T20:29:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26134 -->
Seitenansicht von meinem HEMTT 4x8 mit Bergekran, Membranpumpe, universal Hebevorrichtung und Beleutung.