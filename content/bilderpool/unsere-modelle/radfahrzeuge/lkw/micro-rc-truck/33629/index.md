---
layout: "image"
title: "Nano-Truck_08"
date: "2011-12-11T21:51:12"
picture: "N_Truck_bfl_w.jpg"
weight: "1"
konstrukteure: 
- "rumpelwilly"
fotografen:
- "rumpelwilly"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rumpelwilly"
license: "unknown"
legacy_id:
- details/33629
imported:
- "2019"
_4images_image_id: "33629"
_4images_cat_id: "2356"
_4images_user_id: "1404"
_4images_image_date: "2011-12-11T21:51:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33629 -->
...in dieser variante ist er 10 mm kürzer geworden..., die stabilität ist besser zum vorgänger...