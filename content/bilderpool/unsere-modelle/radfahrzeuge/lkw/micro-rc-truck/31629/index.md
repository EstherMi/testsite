---
layout: "image"
title: "Micro-RC-Truck 16"
date: "2011-08-19T19:11:20"
picture: "Micro-RC-Truck_16.jpg"
weight: "26"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/31629
imported:
- "2019"
_4images_image_id: "31629"
_4images_cat_id: "2356"
_4images_user_id: "328"
_4images_image_date: "2011-08-19T19:11:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31629 -->
Im Inneren des Aufliegers ist es sehr eng. Der 9V-Blockakku liegt nur lose drin und wird durch die seitliche Verkleidung (hier im Bild abgenommen) fest geklemmt.

Im Bild hinter dem Akku der Taster zum An- und Ausschalten.