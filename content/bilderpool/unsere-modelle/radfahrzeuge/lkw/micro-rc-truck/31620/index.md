---
layout: "image"
title: "Micro-RC-Truck 07"
date: "2011-08-19T19:11:20"
picture: "Micro-RC-Truck_07.jpg"
weight: "17"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/31620
imported:
- "2019"
_4images_image_id: "31620"
_4images_cat_id: "2356"
_4images_user_id: "328"
_4images_image_date: "2011-08-19T19:11:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31620 -->
Die Zugmaschine mit angedeutetem Grill und Lampen. Das Führerhaus sitzt minimal weiter vorn aus dem Raster, da ich eine kleine Bauplatte 15x45 davor anordnen wollte, um eine glatte und hübsche Front zu erreichen.