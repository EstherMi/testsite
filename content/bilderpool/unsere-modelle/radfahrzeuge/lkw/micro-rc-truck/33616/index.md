---
layout: "image"
title: "Nano RC Truck 03"
date: "2011-12-05T23:19:12"
picture: "03_M_Truck_bfv_w.jpg"
weight: "6"
konstrukteure: 
- "rumpelwilly"
fotografen:
- "rumpelwilly"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rumpelwilly"
license: "unknown"
legacy_id:
- details/33616
imported:
- "2019"
_4images_image_id: "33616"
_4images_cat_id: "2356"
_4images_user_id: "1404"
_4images_image_date: "2011-12-05T23:19:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33616 -->
....leichte Vogelperspektive von der "Beifahrerseite"......