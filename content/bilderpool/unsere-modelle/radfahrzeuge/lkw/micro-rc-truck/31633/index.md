---
layout: "image"
title: "Micro-RC-Truck 20"
date: "2011-08-19T19:11:20"
picture: "Micro-RC-Truck_20.jpg"
weight: "30"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/31633
imported:
- "2019"
_4images_image_id: "31633"
_4images_cat_id: "2356"
_4images_user_id: "328"
_4images_image_date: "2011-08-19T19:11:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31633 -->
Wird die Stange hinten nach vorn gedrückt, betätigt die Rastkupplung den Taster, und der Empfänger bekommt Strom. Der Reedkontakt-Halter darunter stellt sicher, dass der Taster auch vollständig betätigt wird und sich die Rastachse nicht nach unten weg biegt.