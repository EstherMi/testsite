---
layout: "comment"
hidden: true
title: "15024"
date: "2011-09-02T21:46:31"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Es ist tatsächlich so! Ich baue gerade ein ähnliches Modell (stelle ich noch vor), und da ist der IR-Empfänger VOLL verkleidet. Und die Fernbedienung funktioniert trotzdem noch perfekt!

Entweder, das Licht sucht sich seinen Weg durch diverse Löcher und Ritzen, oder die roten Verkleidungsplattten sind tatsächlich durchlässig für IR-Licht. Egal - jedenfalls eine tolle Entdeckung vom Namesvetter! ;o)

Gruß, Thomas