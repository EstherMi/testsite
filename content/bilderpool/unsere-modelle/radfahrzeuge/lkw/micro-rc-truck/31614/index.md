---
layout: "image"
title: "Micro-RC-Truck 01"
date: "2011-08-19T19:11:19"
picture: "Micro-RC-Truck_01.jpg"
weight: "11"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/31614
imported:
- "2019"
_4images_image_id: "31614"
_4images_cat_id: "2356"
_4images_user_id: "328"
_4images_image_date: "2011-08-19T19:11:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31614 -->
Dieses Modell ist ein ferngesteuerter Micro-Truck auf einer Breite von nur 3 Grundbausteinen (45 mm).

Ziel des Projektes war, einen realistisch aussehenden und fahrenden Sattelschlepper so klein wie möglich darzustellen. Schnell war klar, dass ich es auf der Basis der kleinen roten Grundplatten probiere. Hat geklappt! ;o)

Der Antrieb erfolgt auf die beiden Achsen des Aufliegers per XS-Motor. Gelenkt wird per Servo über die Drehachse des Aufliegers an der Zugmaschine. Kleiner dürfte die Lenkung eines derart kleinen Fahrzeugs kaum darstellbar sein.

Die Stromversorgung übernimmt ein 9V-Blockakku, der zusammen mit dem Empfänger im Auflieger angeordnet ist. Der Auflieger ist hinten offen, damit das Auge des Empfängers angesteuert werden kann.

Im Auflieger integriert ist ein Schalter, der die Stromversorgung zum Empfänger an- und ausschaltet. Der vorn im Auflieger befindliche Schalter wird über eine Zug-/Druckstange bedient, die von hinten zugänglich ist.

Der Auflieger lässt sich leicht abnehmen und stützt sich dann auf nur grob angedeutete starre Räder hinter dem seitlichen Unterfahrschutz.

Der Vortrieb und das Lenkverhalten sind ganz ordentlich, obwohl die kleinen Räder 23 ohne Gummi natürlich wenig Reibung haben und auf glatten Oberflächen leicht durchrutschen.

Ich war bemüht, ein Video aufzunehmen, um das Fahrverhalten zu zeigen:

http://www.youtube.com/watch?v=Z4K_tCjxaNQ

Bei den Aufnahmen zum Video ist auch was schiefgegangen, dass ich Euch nicht vorenthalten möchte:

http://www.youtube.com/watch?v=Osd7O-dkZbQ

Glücklicherweise durch eine Blitzreaktion kurz über dem Parkettboden mit der Hand gefangen, puh ...