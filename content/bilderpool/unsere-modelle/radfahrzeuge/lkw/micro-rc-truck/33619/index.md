---
layout: "image"
title: "Nano RC Truck 06"
date: "2011-12-05T23:19:12"
picture: "06_M_Truck_h_w.jpg"
weight: "9"
konstrukteure: 
- "rumpelwilly"
fotografen:
- "rumpelwilly"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rumpelwilly"
license: "unknown"
legacy_id:
- details/33619
imported:
- "2019"
_4images_image_id: "33619"
_4images_cat_id: "2356"
_4images_user_id: "1404"
_4images_image_date: "2011-12-05T23:19:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33619 -->
...die Heckansicht, alle notwendige Technik auf einen Blick...