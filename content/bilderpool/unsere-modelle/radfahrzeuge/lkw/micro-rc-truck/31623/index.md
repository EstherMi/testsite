---
layout: "image"
title: "Micro-RC-Truck 10"
date: "2011-08-19T19:11:20"
picture: "Micro-RC-Truck_10.jpg"
weight: "20"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/31623
imported:
- "2019"
_4images_image_id: "31623"
_4images_cat_id: "2356"
_4images_user_id: "328"
_4images_image_date: "2011-08-19T19:11:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31623 -->
Die Zugmaschine von unten. Alle Räder sind starr angebunden.