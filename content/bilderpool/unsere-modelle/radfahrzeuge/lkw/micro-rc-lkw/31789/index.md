---
layout: "image"
title: "Micro-RC-LKW 07"
date: "2011-09-13T18:20:13"
picture: "Micro-RC-LKW_07.jpg"
weight: "7"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/31789
imported:
- "2019"
_4images_image_id: "31789"
_4images_cat_id: "2372"
_4images_user_id: "328"
_4images_image_date: "2011-09-13T18:20:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31789 -->
Der Antrieb ist unkonventionell, funktioniert aber gut. In diesem Maßstab braucht man manche seltsame Lösung, um zum Ziel zu kommen ...

Sicher gibt es hier mit alter grauer FT eine bessere Lösung, aber ich baue nur mit aktueller FT, also nicht mit grauen Teilen.