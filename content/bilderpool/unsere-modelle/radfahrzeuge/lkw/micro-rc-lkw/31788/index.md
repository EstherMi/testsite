---
layout: "image"
title: "Micro-RC-LKW 06"
date: "2011-09-13T18:20:13"
picture: "Micro-RC-LKW_06.jpg"
weight: "6"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/31788
imported:
- "2019"
_4images_image_id: "31788"
_4images_cat_id: "2372"
_4images_user_id: "328"
_4images_image_date: "2011-09-13T18:20:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31788 -->
Das Zahnrad über der Vorderachse ist nur Show und hat keine Funktion. Die Drehschemellenkung benötigt naturgemäß viel Bauraum, und das Zahnrad füllt diesen ein wenig.