---
layout: "image"
title: "Micro-RC-LKW 01"
date: "2011-09-13T18:20:13"
picture: "Micro-RC-LKW_01.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/31783
imported:
- "2019"
_4images_image_id: "31783"
_4images_cat_id: "2372"
_4images_user_id: "328"
_4images_image_date: "2011-09-13T18:20:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31783 -->
Mein letztes Modell "Micro-RC-Truck" hat mir großen Spielspaß beschert und gezeigt, dass mit FT auch winzige ferngesteuerte Modelle möglich sind. Schon kurz danach dachte ich, dass müsste doch alles noch kleiner gehen, und vor allem sollte doch auch eine "normale" Lenkung auf 3 Grundbausteinen Breite möglich sein ...

Es hat geklappt! Dieses Modell ist nur 3 Grundbausteine breit, 6 hoch und etwas mehr als 9 lang, ist aber voll ferngesteuert, kann also über das Control Set fahren und lenken.

Die Stromversorgung übernimmt ein 9V-Blockakku. Gestartet wird über einen Schalter im Führerhaus. Den Antrieb übernimmt ein XS-Motor.

Der Empfänger ist komplett mit Bauplatten verkleidet, empfängt das IR-Licht aber trotzdem ganz brauchbar. Man muss nur etwas näher ans Modell gehen, damit der Empfang keine Probleme bereitet. Ich hatte zweimal das Problem, dass das Servo nach dem Lenken in seiner ausgelenkten Lage verharrte und sich nicht mehr zurückstellen lies, was auf unvollständigen Empfang schließen lässt (ich musste das Modell jeweils wieder komplett zerlegen, um den Fehler zu beheben). Ohne Vollverkleidung ist der Empfang aber wie immer.

Das Modell ist - wie immer bei mir - nach dem FT-Reinheitsgebot gebaut, also keinerlei Modifikationen, Fremdteile oder Kleber!

Ein kleines Video gibt es auch:

http://www.youtube.com/watch?v=GDC5jKvx2Hk

Ich denke, kleiner dürfte ein ferngesteuertes Fahrzeug mit Fischertechnik kaum möglich sein, oder?