---
layout: "image"
title: "Hauptmastteil"
date: "2007-08-13T17:04:05"
picture: "transporterkranteile09.jpg"
weight: "9"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/11375
imported:
- "2019"
_4images_image_id: "11375"
_4images_cat_id: "1020"
_4images_user_id: "504"
_4images_image_date: "2007-08-13T17:04:05"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11375 -->
Hier die Version mit einem großen Teil.