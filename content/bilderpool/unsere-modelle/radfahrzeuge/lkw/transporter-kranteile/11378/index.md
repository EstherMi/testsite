---
layout: "image"
title: "Ohne Last"
date: "2007-08-13T17:04:25"
picture: "transporterkranteile12.jpg"
weight: "12"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/11378
imported:
- "2019"
_4images_image_id: "11378"
_4images_cat_id: "1020"
_4images_user_id: "504"
_4images_image_date: "2007-08-13T17:04:25"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11378 -->
So kann der Transporter rumfahren ohne ein Kranteil geladen zu haben.