---
layout: "image"
title: "IR-Empfänger"
date: "2007-08-13T17:04:04"
picture: "transporterkranteile05.jpg"
weight: "5"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/11371
imported:
- "2019"
_4images_image_id: "11371"
_4images_cat_id: "1020"
_4images_user_id: "504"
_4images_image_date: "2007-08-13T17:04:04"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11371 -->
Hier ist die BElegung des Ir-Empfängers zu sehen.