---
layout: "image"
title: "Zugfahrzeug (von unten)"
date: "2007-08-13T17:04:04"
picture: "transporterkranteile04.jpg"
weight: "4"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/11370
imported:
- "2019"
_4images_image_id: "11370"
_4images_cat_id: "1020"
_4images_user_id: "504"
_4images_image_date: "2007-08-13T17:04:04"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11370 -->
Hier mal von unten zu sehen.