---
layout: "image"
title: "Zugfahrzeug"
date: "2007-08-13T17:04:04"
picture: "transporterkranteile03.jpg"
weight: "3"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/11369
imported:
- "2019"
_4images_image_id: "11369"
_4images_cat_id: "1020"
_4images_user_id: "504"
_4images_image_date: "2007-08-13T17:04:04"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11369 -->
Hier das Zugfahrzeug. Antrieb Powermotor 1:8, Lenkung Minimotor.