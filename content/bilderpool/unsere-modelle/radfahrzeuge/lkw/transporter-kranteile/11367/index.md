---
layout: "image"
title: "Transporter (insgesamt)"
date: "2007-08-13T17:04:04"
picture: "transporterkranteile01.jpg"
weight: "1"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/11367
imported:
- "2019"
_4images_image_id: "11367"
_4images_cat_id: "1020"
_4images_user_id: "504"
_4images_image_date: "2007-08-13T17:04:04"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11367 -->
Das ist der Transporter. Bei dieser Version hat er 2 von den Wippspitzstücken geladen. Er ist ferngesteuert (IR).