---
layout: "image"
title: "modellevonclauswludwig59.jpg"
date: "2007-11-13T17:28:59"
picture: "modellevonclauswludwig59.jpg"
weight: "14"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/12719
imported:
- "2019"
_4images_image_id: "12719"
_4images_cat_id: "1138"
_4images_user_id: "127"
_4images_image_date: "2007-11-13T17:28:59"
_4images_image_order: "59"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12719 -->
