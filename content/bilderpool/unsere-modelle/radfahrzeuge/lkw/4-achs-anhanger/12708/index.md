---
layout: "image"
title: "modellevonclauswludwig48.jpg"
date: "2007-11-13T17:28:59"
picture: "modellevonclauswludwig48.jpg"
weight: "3"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Claus-W. Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/12708
imported:
- "2019"
_4images_image_id: "12708"
_4images_cat_id: "1138"
_4images_user_id: "127"
_4images_image_date: "2007-11-13T17:28:59"
_4images_image_order: "48"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12708 -->
