---
layout: "overview"
title: "Fahrgestell 8x4/4, Zugmaschine, unvollendet"
date: 2019-12-17T18:41:06+01:00
legacy_id:
- categories/2299
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2299 --> 
Unvollendetes Projekt aus 2008 (?). Motorisierung mit Gleichlaufgetriebe, innovative Achsschenkellenkung mit geringem Lenkrollradius, Doppelachsantrieb mit Mittendifferential, gefederte Radaufhaengung, ansprechende Verkleidungsteile. 