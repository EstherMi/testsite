---
layout: "image"
title: "Achsschenkel, zum Nachbauen"
date: "2011-06-07T17:30:21"
picture: "fahrgestellxzugmaschine13.jpg"
weight: "13"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/30797
imported:
- "2019"
_4images_image_id: "30797"
_4images_cat_id: "2299"
_4images_user_id: "9"
_4images_image_date: "2011-06-07T17:30:21"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30797 -->
Hier sind alle Einzelteile erkennbar. Unten rechts am Winkelstein 15 Grad ist ein Baustein 7,5.