---
layout: "image"
title: "Heckansicht"
date: "2011-06-07T17:30:21"
picture: "fahrgestellxzugmaschine05.jpg"
weight: "5"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/30789
imported:
- "2019"
_4images_image_id: "30789"
_4images_cat_id: "2299"
_4images_user_id: "9"
_4images_image_date: "2011-06-07T17:30:21"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30789 -->
Rechts fehlt der Radkasten ganz, links halb. Dadurch wird die Pendelachsaufhaengung sichtbar. Die Registerkupplung fuer zwei unterschiedliche Deichsel-Hoehen ist leider nur eine Attrappe, die Kraft wird nicht vernuenftig in den Rahmen eingefuehrt.