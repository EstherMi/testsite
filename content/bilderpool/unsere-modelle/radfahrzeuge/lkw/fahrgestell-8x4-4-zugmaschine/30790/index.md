---
layout: "image"
title: "Radkasten Achse 3"
date: "2011-06-07T17:30:21"
picture: "fahrgestellxzugmaschine06.jpg"
weight: "6"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/30790
imported:
- "2019"
_4images_image_id: "30790"
_4images_cat_id: "2299"
_4images_user_id: "9"
_4images_image_date: "2011-06-07T17:30:21"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30790 -->
Die Vorderkante des Radkastens der hinteren Achsgruppe. Die Verkleidungsteile erlauben einen Winkel von 60 Grad, leider wird der Radkasten dadurch etwas unfoermig und ist zu weit vom Rad entfernt. Im Hintergrund Kardanwelle und Lenkservo.