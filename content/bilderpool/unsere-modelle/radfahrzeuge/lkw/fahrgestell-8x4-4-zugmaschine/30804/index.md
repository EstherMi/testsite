---
layout: "image"
title: "Motorblock"
date: "2011-06-07T17:30:22"
picture: "fahrgestellxzugmaschine20.jpg"
weight: "20"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/30804
imported:
- "2019"
_4images_image_id: "30804"
_4images_cat_id: "2299"
_4images_user_id: "9"
_4images_image_date: "2011-06-07T17:30:22"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30804 -->
Gleichlaufgetriebe, um einen weiten Geschwindigkeitsbereich abzudecken. Mit ein bisschen Geschick kriegt man so allerhand Geraeusche zustande, und einfacher zu bauen als ein Schaltgetriebe ist es allemal. Beim Anfahren addieren sich wirklich die Drehmomente beider Motoren, so dass das Getriebe nichtmal unsinnig ist.

Die Bausteine 7,5 werden an der Kroepfung des Rahmens genau ans Ende der kurzen Aluprofile gehaengt. Das muss reichen ... ist aber nicht optimal.