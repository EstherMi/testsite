---
layout: "image"
title: "Mittendifferential"
date: "2011-06-07T17:30:22"
picture: "fahrgestellxzugmaschine18.jpg"
weight: "18"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/30802
imported:
- "2019"
_4images_image_id: "30802"
_4images_cat_id: "2299"
_4images_user_id: "9"
_4images_image_date: "2011-06-07T17:30:22"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30802 -->
Man sieht das Differential zwischen den beiden Hinterachsen und das Z20, das es von oben antreibt. Nach rechts geht die Antriebswelle weg Richtung Motor.

Man beachte die Verbindung zwischen den Bausteinen 15 mit Loch an den Radachsen, und Bausteinen 30 auf dem Weg zu den Spiralfedern. Die Spiralfedern koennten freilich auch weiter innen stehen. Weil sie das aber nicht tun, ist die Bauplatte 15*30*5 mit Nuten notwendig. Und so sehen die Spiralfedern gleich viel toller aus. Oder?

Den Streben 30 mit Loch ging es uebrigens auch schonmal besser.