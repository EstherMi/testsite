---
layout: "image"
title: "Achsschenkel, Detail"
date: "2011-06-07T17:30:21"
picture: "fahrgestellxzugmaschine10.jpg"
weight: "10"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/30794
imported:
- "2019"
_4images_image_id: "30794"
_4images_cat_id: "2299"
_4images_user_id: "9"
_4images_image_date: "2011-06-07T17:30:21"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30794 -->
Das Rad wurde nun entfernt, die Achse (Clipsachse 34 schwarz) wird von innen gehalten. Das Rad muss von aussen festgeschraubt werden, weil die Achse zu kurz ist um zu clipsen. 

Der Achsschenkel ist oben ausgehaengt. Der Baustein 15 mit Loch ragt in die Felge hinein, so dass nur fuer die Bausteine 5 und Strebenadapter Platz ist. Rechst am Baustein 15 mit Loch haengt eine Bauplatte 15x30x3,75 rot, die die einzige Verbindung zur Spurstange ist. Nicht die stabilste Loesung, aber auch nicht der Schwachpunkt -- die Anlenkung der Spurstange ueber zwei Klemmhuelsen D 7.5 rot und die Clipsachse 20 rot ist viel schlimmer.