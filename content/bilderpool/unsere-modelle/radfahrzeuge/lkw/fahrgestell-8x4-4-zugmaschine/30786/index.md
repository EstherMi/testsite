---
layout: "image"
title: "Radkasten und Aufstieg"
date: "2011-06-07T17:30:21"
picture: "fahrgestellxzugmaschine02.jpg"
weight: "2"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/30786
imported:
- "2019"
_4images_image_id: "30786"
_4images_cat_id: "2299"
_4images_user_id: "9"
_4images_image_date: "2011-06-07T17:30:21"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30786 -->
Das Vorderrad kann am Alu-Rahmen schleifen. Das ist nicht schoen. 

Die Einzelteile sollten gut zu erkennen sein, viel Spass beim Nachbau :-)