---
layout: "image"
title: "Achsschenkel, Seitenansciht"
date: "2011-06-07T17:30:21"
picture: "fahrgestellxzugmaschine11.jpg"
weight: "11"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/30795
imported:
- "2019"
_4images_image_id: "30795"
_4images_cat_id: "2299"
_4images_user_id: "9"
_4images_image_date: "2011-06-07T17:30:21"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30795 -->
Die verschiedenen Eben der Streben sind gut erkennbar. Die Anlenkung der Achsen (d.h. die Verbindung von Achse 1 nach Achse 2) muss zwischen der oberen und unteren Achsschenkel-Aufhaengung verlaufen, was ziemliches Gedraenge erzeugt.