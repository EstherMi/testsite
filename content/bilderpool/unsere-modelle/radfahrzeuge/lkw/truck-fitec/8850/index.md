---
layout: "image"
title: "Truck"
date: "2007-02-04T12:35:01"
picture: "Truck8.jpg"
weight: "8"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/8850
imported:
- "2019"
_4images_image_id: "8850"
_4images_cat_id: "804"
_4images_user_id: "456"
_4images_image_date: "2007-02-04T12:35:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8850 -->
Um den Akku anzustöppseln, muss man eine Platte wegmachen.