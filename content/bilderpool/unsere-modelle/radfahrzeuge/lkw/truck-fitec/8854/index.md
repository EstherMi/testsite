---
layout: "image"
title: "Truck"
date: "2007-02-04T12:35:11"
picture: "Truck12.jpg"
weight: "12"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/8854
imported:
- "2019"
_4images_image_id: "8854"
_4images_cat_id: "804"
_4images_user_id: "456"
_4images_image_date: "2007-02-04T12:35:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8854 -->
Akku und Fernsteuerung sind auch an der Unterseite angebracht.