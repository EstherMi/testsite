---
layout: "image"
title: "In dem Krankenwagen"
date: "2011-03-21T18:35:36"
picture: "krankenwagen08.jpg"
weight: "7"
konstrukteure: 
- "Janft"
fotografen:
- "Janft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- details/30302
imported:
- "2019"
_4images_image_id: "30302"
_4images_cat_id: "2252"
_4images_user_id: "1164"
_4images_image_date: "2011-03-21T18:35:36"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30302 -->
hat sich wohl einer verletzt