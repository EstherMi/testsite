---
layout: "image"
title: "Geamtansicht"
date: "2011-03-21T18:35:36"
picture: "krankenwagen01.jpg"
weight: "1"
konstrukteure: 
- "Janft"
fotografen:
- "Janft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- details/30296
imported:
- "2019"
_4images_image_id: "30296"
_4images_cat_id: "2252"
_4images_user_id: "1164"
_4images_image_date: "2011-03-21T18:35:36"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30296 -->
Die vier Kabel sind die Stromversorgung