---
layout: "image"
title: "E-Tec Modul"
date: "2011-03-21T18:35:36"
picture: "krankenwagen06.jpg"
weight: "5"
konstrukteure: 
- "Janft"
fotografen:
- "Janft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- details/30300
imported:
- "2019"
_4images_image_id: "30300"
_4images_cat_id: "2252"
_4images_user_id: "1164"
_4images_image_date: "2011-03-21T18:35:36"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30300 -->
steuert das Blaulicht