---
layout: "image"
title: "Salz-und Granulatstreuer von oben"
date: "2008-12-01T17:55:45"
picture: "winterdienstfahrzeug04.jpg"
weight: "4"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/16535
imported:
- "2019"
_4images_image_id: "16535"
_4images_cat_id: "1262"
_4images_user_id: "845"
_4images_image_date: "2008-12-01T17:55:45"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16535 -->
