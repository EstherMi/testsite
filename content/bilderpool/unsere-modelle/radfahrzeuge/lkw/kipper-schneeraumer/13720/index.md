---
layout: "image"
title: "Kipper01"
date: "2008-02-23T17:21:52"
picture: "kipperschneeraeumer1.jpg"
weight: "13"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/13720
imported:
- "2019"
_4images_image_id: "13720"
_4images_cat_id: "1262"
_4images_user_id: "729"
_4images_image_date: "2008-02-23T17:21:52"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13720 -->
von vorne