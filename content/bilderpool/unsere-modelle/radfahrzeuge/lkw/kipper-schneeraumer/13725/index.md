---
layout: "image"
title: "Kipper06"
date: "2008-02-23T17:21:53"
picture: "kipperschneeraeumer6.jpg"
weight: "18"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/13725
imported:
- "2019"
_4images_image_id: "13725"
_4images_cat_id: "1262"
_4images_user_id: "729"
_4images_image_date: "2008-02-23T17:21:53"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13725 -->
von hinten