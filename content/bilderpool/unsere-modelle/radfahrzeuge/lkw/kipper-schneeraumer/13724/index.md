---
layout: "image"
title: "Kipper05"
date: "2008-02-23T17:21:53"
picture: "kipperschneeraeumer5.jpg"
weight: "17"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/13724
imported:
- "2019"
_4images_image_id: "13724"
_4images_cat_id: "1262"
_4images_user_id: "729"
_4images_image_date: "2008-02-23T17:21:53"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13724 -->
Die Kipper-Mechanik