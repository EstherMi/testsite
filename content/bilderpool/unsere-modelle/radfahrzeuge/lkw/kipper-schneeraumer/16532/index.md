---
layout: "image"
title: "Gesamtansicht"
date: "2008-12-01T17:55:45"
picture: "winterdienstfahrzeug01.jpg"
weight: "1"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/16532
imported:
- "2019"
_4images_image_id: "16532"
_4images_cat_id: "1262"
_4images_user_id: "845"
_4images_image_date: "2008-12-01T17:55:45"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16532 -->
