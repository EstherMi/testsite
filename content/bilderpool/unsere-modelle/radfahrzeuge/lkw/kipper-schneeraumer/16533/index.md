---
layout: "image"
title: "Gesamtansicht_Heck"
date: "2008-12-01T17:55:45"
picture: "winterdienstfahrzeug02.jpg"
weight: "2"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/16533
imported:
- "2019"
_4images_image_id: "16533"
_4images_cat_id: "1262"
_4images_user_id: "845"
_4images_image_date: "2008-12-01T17:55:45"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16533 -->
