---
layout: "image"
title: "01 King of the Road"
date: "2012-07-17T16:34:37"
picture: "zugmaschine1.jpg"
weight: "1"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/35182
imported:
- "2019"
_4images_image_id: "35182"
_4images_cat_id: "2607"
_4images_user_id: "860"
_4images_image_date: "2012-07-17T16:34:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35182 -->
Dieses Modell steht schon seit Jahren bei mir herum und jetzt dachte ich, es ist mal an der Zeit, ein paar Fotos zu machen. Das eigentliche Modell besitze ich nicht, ich habe es vor langer Zeit nur auf der Grundlage eines Bildes gebaut. Jetzt habe ich ihn "restauriert", um ein paar Bilder machen zu können ;)

Zwei Motoren habe ich verbaut & es wäre auch sicher noch Platz für einen Akku und eine IR-Modul, ich habe aber eine serielle Schnittstelle eingebaut.