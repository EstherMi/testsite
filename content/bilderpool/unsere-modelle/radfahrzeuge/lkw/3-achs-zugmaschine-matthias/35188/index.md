---
layout: "image"
title: "07 Lenkung"
date: "2012-07-17T16:34:37"
picture: "zugmaschine7.jpg"
weight: "7"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/35188
imported:
- "2019"
_4images_image_id: "35188"
_4images_cat_id: "2607"
_4images_user_id: "860"
_4images_image_date: "2012-07-17T16:34:37"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35188 -->
Keine Servolenkung, sondern ganz normal mit einem S-Motor