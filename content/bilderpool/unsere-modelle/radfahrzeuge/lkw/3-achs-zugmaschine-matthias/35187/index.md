---
layout: "image"
title: "06 Motor"
date: "2012-07-17T16:34:37"
picture: "zugmaschine6.jpg"
weight: "6"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/35187
imported:
- "2019"
_4images_image_id: "35187"
_4images_cat_id: "2607"
_4images_user_id: "860"
_4images_image_date: "2012-07-17T16:34:37"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35187 -->
Der Motor treibt nur eine Achse an, obwohl man in auch auf die andere Seite bauen könnte, damit Platz für Zahnräder wäre, die dann beide Achsen antreiben.

So in etwa: http://www.ftcommunity.de/details.php?image_id=29037