---
layout: "image"
title: "Alle Fahrzeuge der 'Spedition'"
date: "2007-11-05T15:54:02"
picture: "DSCN1987.jpg"
weight: "1"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/12493
imported:
- "2019"
_4images_image_id: "12493"
_4images_cat_id: "1120"
_4images_user_id: "184"
_4images_image_date: "2007-11-05T15:54:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12493 -->
von vorn:
Ein kleiner LKW mit Kippmulde,
dahinter ein Servicefahrzeug für den Caterpillar mit Ersatzkette,
dahinter ein Sattelschlepper mit zwei Kippmulden,
und ganz hinten ein Zug bestehend aus Zugmaschine mit Anhänger (beide mit Kippmulde).