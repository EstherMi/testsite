---
layout: "image"
title: "gelenkte Vorderachsen..."
date: "2008-09-25T08:44:42"
picture: "lkwachsenlenkbar5.jpg"
weight: "5"
konstrukteure: 
- "Bernd Scheurer"
fotografen:
- "Bernd Scheurer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "be-sign,net"
license: "unknown"
legacy_id:
- details/15592
imported:
- "2019"
_4images_image_id: "15592"
_4images_cat_id: "1429"
_4images_user_id: "808"
_4images_image_date: "2008-09-25T08:44:42"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15592 -->
...über eine Kette im Aufbau hinter dem Führerhaus