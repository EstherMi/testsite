---
layout: "image"
title: "Fahrgestell hinten"
date: "2008-09-25T08:44:42"
picture: "lkwachsenlenkbar6.jpg"
weight: "6"
konstrukteure: 
- "Bernd Scheurer"
fotografen:
- "Bernd Scheurer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "be-sign,net"
license: "unknown"
legacy_id:
- details/15593
imported:
- "2019"
_4images_image_id: "15593"
_4images_cat_id: "1429"
_4images_user_id: "808"
_4images_image_date: "2008-09-25T08:44:42"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15593 -->
extrem belastbar