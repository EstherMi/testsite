---
layout: "image"
title: "Schneeräumfahrzeug"
date: "2016-10-24T21:20:51"
picture: "schneeraeumfahrzeug03.jpg"
weight: "3"
konstrukteure: 
- "Claus Ludwig"
fotografen:
- "Claus Ludwig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Claus"
license: "unknown"
legacy_id:
- details/44673
imported:
- "2019"
_4images_image_id: "44673"
_4images_cat_id: "3325"
_4images_user_id: "119"
_4images_image_date: "2016-10-24T21:20:51"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44673 -->
