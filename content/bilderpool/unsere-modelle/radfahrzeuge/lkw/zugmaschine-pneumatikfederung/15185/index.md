---
layout: "image"
title: "Zugmaschine - v.1 - Federung von unten"
date: "2008-09-05T19:36:55"
picture: "IMG_1472_800.jpg"
weight: "13"
konstrukteure: 
- "Norbert E. Wagner"
fotografen:
- "Norbert E. Wagner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "zeuz"
license: "unknown"
legacy_id:
- details/15185
imported:
- "2019"
_4images_image_id: "15185"
_4images_cat_id: "1390"
_4images_user_id: "634"
_4images_image_date: "2008-09-05T19:36:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15185 -->
Ein blick unter die Federung verrät, daß in der ersten Version noch alles etwas schief und krumm ist ...