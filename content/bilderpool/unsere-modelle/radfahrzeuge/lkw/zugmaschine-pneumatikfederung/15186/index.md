---
layout: "image"
title: "Zugmaschine - v.1 - Unten"
date: "2008-09-06T09:04:28"
picture: "IMG_1473_800.jpg"
weight: "1"
konstrukteure: 
- "Norbert E. Wagner"
fotografen:
- "Norbert E. Wagner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "zeuz"
license: "unknown"
legacy_id:
- details/15186
imported:
- "2019"
_4images_image_id: "15186"
_4images_cat_id: "1390"
_4images_user_id: "634"
_4images_image_date: "2008-09-06T09:04:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15186 -->
