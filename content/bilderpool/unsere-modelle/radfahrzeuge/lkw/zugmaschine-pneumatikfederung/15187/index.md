---
layout: "image"
title: "Zugmaschine - v.2 - Seitenansicht"
date: "2008-09-06T09:04:28"
picture: "DSCN0062_800.jpg"
weight: "2"
konstrukteure: 
- "Norbert E. Wagner"
fotografen:
- "Norbert E. Wagner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "zeuz"
license: "unknown"
legacy_id:
- details/15187
imported:
- "2019"
_4images_image_id: "15187"
_4images_cat_id: "1390"
_4images_user_id: "634"
_4images_image_date: "2008-09-06T09:04:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15187 -->
Die überarbeitete Version, der hintere teil wurde komplett neu aufgebaut. Leider hat sich an meinem Zylindermangel noch nichts geändert.

Außerdem habe ich bedenken ob die Zylinder in der aktuellen Konstruktion nicht zu viel seitliche Kräfte abbekommen.
Was sagen die Experten?