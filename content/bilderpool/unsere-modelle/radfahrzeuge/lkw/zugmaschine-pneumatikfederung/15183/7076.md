---
layout: "comment"
hidden: true
title: "7076"
date: "2008-09-07T16:01:42"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Ganz großartige Proportionen, tolle technische Details, kaum unnützer Firlefanz, SO SOLL ES SEIN!!! Ganz klasse! Ich kann mich kaum satt sehen...

Und der Farbenmix mit den blauen Teilen ist - in Kombination mit der Pneumatik-Technik - auch absolut sinnvoll und stimmig! Für mich eines der schönsten Modelle der letzten Zeit.

Gruß, Thomas