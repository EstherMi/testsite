---
layout: "image"
title: "Zugmaschine - v.1 - Vorne unter die Achse"
date: "2008-09-05T19:36:55"
picture: "IMG_1470_800.jpg"
weight: "12"
konstrukteure: 
- "Norbert E. Wagner"
fotografen:
- "Norbert E. Wagner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "zeuz"
license: "unknown"
legacy_id:
- details/15184
imported:
- "2019"
_4images_image_id: "15184"
_4images_cat_id: "1390"
_4images_user_id: "634"
_4images_image_date: "2008-09-05T19:36:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15184 -->
Die Lenkung und Federung vorne ist in etwa die der 80ger Trucks.