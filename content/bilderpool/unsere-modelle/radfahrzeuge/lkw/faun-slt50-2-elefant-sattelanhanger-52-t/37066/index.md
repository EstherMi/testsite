---
layout: "image"
title: "FAUN SLT50-2 'ELEFANT' _ 32"
date: "2013-06-10T08:26:52"
picture: "faunsltelefantmitsattelanhaengerkaessbohrert32.jpg"
weight: "42"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/37066
imported:
- "2019"
_4images_image_id: "37066"
_4images_cat_id: "2753"
_4images_user_id: "144"
_4images_image_date: "2013-06-10T08:26:52"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37066 -->
Federung und bevestigung