---
layout: "image"
title: "FAUN SLT50-2 'ELEFANT' _ 24"
date: "2013-06-10T08:26:52"
picture: "faunsltelefantmitsattelanhaengerkaessbohrert24.jpg"
weight: "34"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/37058
imported:
- "2019"
_4images_image_id: "37058"
_4images_cat_id: "2753"
_4images_user_id: "144"
_4images_image_date: "2013-06-10T08:26:52"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37058 -->
Unterseite: Vorderachsen