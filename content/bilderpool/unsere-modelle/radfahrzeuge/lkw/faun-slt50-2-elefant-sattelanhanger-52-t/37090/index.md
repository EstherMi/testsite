---
layout: "image"
title: "Sattelanhänger Kässbohrer 52 t _ 10"
date: "2013-06-11T22:27:47"
picture: "sattelanhaengerkaessbohrert05.jpg"
weight: "5"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/37090
imported:
- "2019"
_4images_image_id: "37090"
_4images_cat_id: "2753"
_4images_user_id: "144"
_4images_image_date: "2013-06-11T22:27:47"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37090 -->
Ein imposantes Modell, auch wen er nie verstich gestelt worden ist.