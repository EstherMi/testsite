---
layout: "overview"
title: "FAUN SLT50-2 'ELEFANT' mit Sattelanhänger Kässbohrer 52 t"
date: 2019-12-17T18:41:51+01:00
legacy_id:
- categories/2753
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2753 --> 
Obwol mir Harald Steinhaus mal schrieb das dieses projekt ein Fehlverzuch ist, möchte ich das nicht zo beschreiben.
Ein "überschreiten der Grenzen des machtbare" wehre ein besseren beschreibung.

Angefangen hat dieses Project als studie zur bau einen 8x8 LKW Chassis.
Ich wolte einfach ein grosses modell von diesen TankTransporter, wovon ich schon van klein an einen Maßstabmodell in der Vitrine stehen hab: Maßstab 1:87 von ROCO.
Erst nachdem ich die Robbe Reiffen gesehen habbe und mir ein ausgabe von Tankograd-Publising, "Die Pantzertransporter de Bundeswehr 1956 - Heute", gekauft habbe, habbe ich angefangen dieses Modell weiter als SLT50-2 auf zu bauen.
Ein weitere hilfe wahr das downloaden von der baubeschreibung von das 1:72 modell #03145 von Revell.

Die Reiffen sind für ein model in Maßstab 1:14 gedacht und passen hervoragend auf die 43er Felgen von fischertechnik.
Das die Räder wegen der Breite, 3150mm, weit aus ein ander stehen, wahr für mein modell von vorteil: genügend raum zur einbau der 8x8 antrieb und richtig funktionierende Federpackete.
Da die Rast-differentialen mehr raum brauchten, wurden die Achsen doch sehr gros und grob.
Bei den gelengte voderachsen kam noch das problem das ich die drehpunkten so weit wie möglich innerhalb der Räder haben wolte: Lagerung, drehpunkt und antriebachse auf einen punkt.

Alle Achsen sind mittels Kogellager 9x4x3 gelagerd. Für die gelenkte Räder auch Nadellager.
Der antrieb auf alle 8 Räder funktioniert. Für der lenkung hatte ich ein Digital Servo vorgesehen.
Der verbindung von beide Achsen wahr aber, wegen zu weinig raum, nicht möglich.
Die Achsen konten auch das ganse gewicht nicht tragen. Deswegen habbe ich nicht einmal der Kabine aufgebaut.