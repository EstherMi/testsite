---
layout: "image"
title: "Sattelanhänger Kässbohrer 52 t _ 3"
date: "2013-06-10T08:26:52"
picture: "faunsltelefantmitsattelanhaengerkaessbohrert39.jpg"
weight: "49"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/37073
imported:
- "2019"
_4images_image_id: "37073"
_4images_cat_id: "2753"
_4images_user_id: "144"
_4images_image_date: "2013-06-10T08:26:52"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37073 -->
