---
layout: "image"
title: "FAUN SLT50-2 'ELEFANT' _ 10"
date: "2013-06-10T08:26:52"
picture: "faunsltelefantmitsattelanhaengerkaessbohrert10.jpg"
weight: "20"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/37044
imported:
- "2019"
_4images_image_id: "37044"
_4images_cat_id: "2753"
_4images_user_id: "144"
_4images_image_date: "2013-06-10T08:26:52"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37044 -->
