---
layout: "image"
title: "FAUN SLT50-2 'ELEFANT' _ 34"
date: "2013-06-10T08:26:52"
picture: "faunsltelefantmitsattelanhaengerkaessbohrert34.jpg"
weight: "44"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/37068
imported:
- "2019"
_4images_image_id: "37068"
_4images_cat_id: "2753"
_4images_user_id: "144"
_4images_image_date: "2013-06-10T08:26:52"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37068 -->
Da diese art von feder aufbau nicht stark is: Metalachse damit sich nichts zoviel verbeugt