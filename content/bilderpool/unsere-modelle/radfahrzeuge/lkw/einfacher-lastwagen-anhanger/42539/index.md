---
layout: "image"
title: "Ladefläche der Zugmaschine"
date: "2015-12-20T13:47:03"
picture: "einfacherlastwagenmitanhaenger06.jpg"
weight: "6"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/42539
imported:
- "2019"
_4images_image_id: "42539"
_4images_cat_id: "3162"
_4images_user_id: "1557"
_4images_image_date: "2015-12-20T13:47:03"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42539 -->
Eine Platte 90x45 bildet die Rückseite.