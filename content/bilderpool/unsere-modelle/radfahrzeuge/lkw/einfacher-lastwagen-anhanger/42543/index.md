---
layout: "image"
title: "Befestigung des Führerhauses II"
date: "2015-12-20T13:47:03"
picture: "einfacherlastwagenmitanhaenger10.jpg"
weight: "10"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/42543
imported:
- "2019"
_4images_image_id: "42543"
_4images_cat_id: "3162"
_4images_user_id: "1557"
_4images_image_date: "2015-12-20T13:47:03"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42543 -->
Naja, nicht wirklich befestigt. Es liegt hinten nur locker auf.