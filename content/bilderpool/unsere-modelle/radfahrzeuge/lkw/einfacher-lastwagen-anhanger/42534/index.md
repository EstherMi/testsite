---
layout: "image"
title: "Lastwagen mit Anhänger"
date: "2015-12-20T13:46:57"
picture: "einfacherlastwagenmitanhaenger01.jpg"
weight: "1"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["einfacher", "Lastwagen", "Spielmodell", "kindgerecht", "universell"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/42534
imported:
- "2019"
_4images_image_id: "42534"
_4images_cat_id: "3162"
_4images_user_id: "1557"
_4images_image_date: "2015-12-20T13:46:57"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42534 -->
Es fing so harmlos an: "Papa baust Du mit mir einen Lastwagen?"
Was macht man denn da als fischertechniker? Lenkung, Federung, Hydraulik, Beleuchtung, Antrieb, ...
Der Bub ist gerade 4 geworden und mit dem ft klappt es da nur bedingt. Also Arbeitsteilung. Sohnemann sucht schon mal die Räder aus, Papa's Haare werden noch grauer als er die gähnende Leere in der Kiste der BS30 sieht, und die 180x90x5,5-Platten sind auch alle unterwegs. Also lassen wir einfach alles weg, was nicht unbedingt an einen Laster gehört. Federung? Braucht kein Mensch. Hydraulik? Iiiih, die 'pinkelt' ja auf den Teppich. Lenkung und Antrieb? Kinderhand schiebt den schon in die passende Richtung. Beleuchtung? Nachts wird geschlafen! Liebevolle filigrane Details? Stören eh nur beim Spielen.

Und das ist also nun das was dabei rausgekommen ist. H.A.R.R.Y. Nichtsnutzfahrzeugbau und Clonix&Klauknowlogies Pfuschware Inc. proudly presents: Spielzeuglaster für die jungen ft-Fans.
Der Betatester gibt ihn jedenfalls nicht mehr her. 3 Tüten Gummibären waren der Preis für den Fototermin - die großen Tüten!