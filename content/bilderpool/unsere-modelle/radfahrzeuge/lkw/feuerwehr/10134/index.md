---
layout: "image"
title: "Feuerwehr"
date: "2007-04-21T14:21:45"
picture: "feuer2.jpg"
weight: "2"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- details/10134
imported:
- "2019"
_4images_image_id: "10134"
_4images_cat_id: "915"
_4images_user_id: "557"
_4images_image_date: "2007-04-21T14:21:45"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10134 -->
von oben. eine Pumpe spritzt wasser, eine andere kann den Tank auffüllen( muss noch per Hand bedient werden)