---
layout: "image"
title: "6x6"
date: "2008-11-30T00:35:42"
picture: "6x6Truck1.jpg"
weight: "1"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/16530
imported:
- "2019"
_4images_image_id: "16530"
_4images_cat_id: "1494"
_4images_user_id: "456"
_4images_image_date: "2008-11-30T00:35:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16530 -->
Das soll mal ein Truck mit 6 Rädern, Allradantrieb, vorne Achsschenkellenkung, Federung, Hydraulik u.v.m werden.
Momentan arbeite ich nur daran die beiden Hinterachsen zu federn, vorne komt vieleicht noch.