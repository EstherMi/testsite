---
layout: "overview"
title: "RC-Kleinstlaster mit Lenkung"
date: 2019-12-17T18:41:59+01:00
legacy_id:
- categories/2814
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2814 --> 
Ferngesteuerter Oldtimer-Laster mit minimalen Außenmaßen. Trotzem sehr flott, vorderradgefedert und mit sehr kleinem Wendekreis. Gute Basis für allerlei ferngesteuerte Kleinstmodelle.