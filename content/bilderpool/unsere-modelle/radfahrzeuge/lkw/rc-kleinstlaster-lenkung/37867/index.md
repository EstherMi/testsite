---
layout: "image"
title: "RC-Kleinstlaster mit Lenkung: flott und stabil 01"
date: "2013-12-02T12:57:35"
picture: "rckleinstlaster1.jpg"
weight: "1"
konstrukteure: 
- "Dieter und Uli Braun"
fotografen:
- "Dieter Braun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- details/37867
imported:
- "2019"
_4images_image_id: "37867"
_4images_cat_id: "2814"
_4images_user_id: "1582"
_4images_image_date: "2013-12-02T12:57:35"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37867 -->
Die Miniaturisierung der ferngesteuerten Modelle ist ja sehr spannend. Da unsere Kids (6+8) auch flotte Modelle wollen, haben wir mal ein bißchen geknobelt. 

Die Lenkung wird vorne direkt an das Servo angekoppelt und mit einer kleinen roten Achse senkrecht auf den Servoarm fixiert. Das ist stabil und gibt gleichzeitig eine kleine Vorderrad-Federung. 
Antrieb hinten mit dem größeren Mini-Motor ist recht flott und mit der Vorderrad-Federung kommt der Laster fast überall durch. 
Vor allem ist er durch den kleinen Radstand und engen Lenkradius geht das Teil sehr gut in die Kurven und ist durch den Motor sehr flink.
Ist glaube ich eine gute Basis für allerlei Modelle.

Viel Spaß beim Nachbauen!