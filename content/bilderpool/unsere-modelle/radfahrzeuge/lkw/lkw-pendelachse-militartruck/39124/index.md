---
layout: "image"
title: "Vorderachse mit Federung 2"
date: "2014-08-01T17:52:03"
picture: "lkjwmitpendelachse07.jpg"
weight: "7"
konstrukteure: 
- "Kryn1998"
fotografen:
- "Kryn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kryn1998"
license: "unknown"
legacy_id:
- details/39124
imported:
- "2019"
_4images_image_id: "39124"
_4images_cat_id: "2925"
_4images_user_id: "2221"
_4images_image_date: "2014-08-01T17:52:03"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39124 -->
Die Streben stabilisieren die Vorderachse und sorgen dafüf, dass sie sich nicht wegdreht.
Ausserdem sorgt sie dafür, dass der truck immer aufrecht herumfahren kann.