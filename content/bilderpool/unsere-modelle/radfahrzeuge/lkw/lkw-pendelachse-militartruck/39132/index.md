---
layout: "image"
title: "Aufbau Doppelachse Teil 2: Aufhängung"
date: "2014-08-01T17:52:19"
picture: "lkjwmitpendelachse15.jpg"
weight: "15"
konstrukteure: 
- "Kryn1998"
fotografen:
- "Kryn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kryn1998"
license: "unknown"
legacy_id:
- details/39132
imported:
- "2019"
_4images_image_id: "39132"
_4images_cat_id: "2925"
_4images_user_id: "2221"
_4images_image_date: "2014-08-01T17:52:19"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39132 -->
Hier sieht man die Aufhängung des zweiten Teils.