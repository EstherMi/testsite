---
layout: "overview"
title: "LKW mit Pendelachse (Militärtruck)"
date: 2019-12-17T18:42:02+01:00
legacy_id:
- categories/2925
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2925 --> 
Ich habe einen Militärtruck gebaut, den manoft in alten Kriegsfilmen sieht.
Als Vorbild dafür dienen zum Teil der "King of the Road" und ältere Modelle von mir.