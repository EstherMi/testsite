---
layout: "image"
title: "Doppelpendelachse Aufhängung Ausgebaut 1"
date: "2014-08-01T17:52:19"
picture: "lkjwmitpendelachse11.jpg"
weight: "11"
konstrukteure: 
- "Kryn1998"
fotografen:
- "Kryn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kryn1998"
license: "unknown"
legacy_id:
- details/39128
imported:
- "2019"
_4images_image_id: "39128"
_4images_cat_id: "2925"
_4images_user_id: "2221"
_4images_image_date: "2014-08-01T17:52:19"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39128 -->
Hier der Aufbau der  Pendelachse:
Oben sieht man die Aufhängung, die an der antriebsachse fixiert ist, und unten die Antriebseinheit.