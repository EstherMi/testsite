---
layout: "image"
title: "büssing04.jpg"
date: "2013-07-20T19:21:12"
picture: "IMG_9060mit.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/37179
imported:
- "2019"
_4images_image_id: "37179"
_4images_cat_id: "2762"
_4images_user_id: "4"
_4images_image_date: "2013-07-20T19:21:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37179 -->
Das Differenzial 31500 sitzt mittig in einer Wippe, auf der auch die beiden Achsen gelagert sind. Die Wippe schaukelt auf den Zapfen der BS15/Loch, in denen die Z20 gelagert sind.