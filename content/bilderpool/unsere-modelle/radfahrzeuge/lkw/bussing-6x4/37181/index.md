---
layout: "image"
title: "büssing06.jpg"
date: "2013-07-20T19:26:00"
picture: "IMG_9064mit.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/37181
imported:
- "2019"
_4images_image_id: "37181"
_4images_cat_id: "2762"
_4images_user_id: "4"
_4images_image_date: "2013-07-20T19:26:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37181 -->
Die Lenkung war nicht so einfach hin zu kriegen. Die Hebel überstreichen eine große Fläche, und irgendwo müssen sich die Federn abstützen.