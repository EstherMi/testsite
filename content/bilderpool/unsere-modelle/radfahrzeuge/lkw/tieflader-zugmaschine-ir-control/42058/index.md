---
layout: "image"
title: "Detail Hinterachse"
date: "2015-10-06T17:22:16"
picture: "DSC08205-sc.jpg"
weight: "5"
konstrukteure: 
- "Fischertechnik Modellbaukasten 30475 / Änderungen PHabermehl"
fotografen:
- "PHabermehl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- details/42058
imported:
- "2019"
_4images_image_id: "42058"
_4images_cat_id: "3128"
_4images_user_id: "2488"
_4images_image_date: "2015-10-06T17:22:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42058 -->
Hier nochmal die Radaufhängung hinten (Diskret aufgebaut ohne Anbauwinkel 31258) und der Antrieb