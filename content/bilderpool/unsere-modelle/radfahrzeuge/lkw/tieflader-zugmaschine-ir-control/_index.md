---
layout: "overview"
title: "Tieflader-Zugmaschine mit IR Control"
date: 2019-12-17T18:42:13+01:00
legacy_id:
- categories/3128
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3128 --> 
Replik der Zugmaschine aus dem Modellbaukasten Tieflader (Art. 30475) mit aktueller IR-Fernbedienung.