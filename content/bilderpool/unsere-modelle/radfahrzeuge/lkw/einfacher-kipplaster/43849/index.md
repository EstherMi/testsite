---
layout: "image"
title: "Detail - unter der Mulde"
date: "2016-07-07T15:20:37"
picture: "einfacherkipplaster05.jpg"
weight: "10"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/43849
imported:
- "2019"
_4images_image_id: "43849"
_4images_cat_id: "3249"
_4images_user_id: "1557"
_4images_image_date: "2016-07-07T15:20:37"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43849 -->
Unter der Kippmulde wird die Statikplatte 180 x 90 mit Verschlußriegeln an den Winkelträger des Leiterrahmens befestigt. S-Riegel wären zu hoch. Naja, die Mulde könnte schon höher sitzen, wer mag kann es ja für sich verschönern. Zu dem Zweck müßten dann die BS5 an den Gelenkwürfeln durch BS15 ersetzt werden oder wie auch immer.

Die WS30° am Muldenboden neben den Gelenkwürfeln verhindern ein Verrutschen in den langen Nuten der 90 x 90 Platte. Da kann auch etwas anderes hin - WS60°, BS15, ...