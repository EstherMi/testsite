---
layout: "image"
title: "Montagedetail - Kragen"
date: "2016-07-07T15:20:37"
picture: "einfacherkipplaster11.jpg"
weight: "16"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/43855
imported:
- "2019"
_4images_image_id: "43855"
_4images_cat_id: "3249"
_4images_user_id: "1557"
_4images_image_date: "2016-07-07T15:20:37"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43855 -->
Ein Verbinder 30 steckt je Seite nicht ganz im Rahmen - Überstand 7,5mm. Nebenbei arretiert er noch die obere Traverse gegen Abrutschen.