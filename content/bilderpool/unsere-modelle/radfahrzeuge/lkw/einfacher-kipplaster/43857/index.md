---
layout: "image"
title: "'Halbe' Mulde"
date: "2016-07-07T15:20:37"
picture: "einfacherkipplaster13.jpg"
weight: "18"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/43857
imported:
- "2019"
_4images_image_id: "43857"
_4images_cat_id: "3249"
_4images_user_id: "1557"
_4images_image_date: "2016-07-07T15:20:37"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43857 -->
Ein paar Teile fehlen hier, man sieht das Innenleben und was wo wie befestigt ist - hoffentlich. Zur Frontplatte kommen wir gleich noch.