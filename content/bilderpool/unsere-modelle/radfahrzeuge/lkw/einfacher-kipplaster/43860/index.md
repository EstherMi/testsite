---
layout: "image"
title: "Detail - Gelenkwürfel"
date: "2016-07-07T15:20:37"
picture: "einfacherkipplaster16.jpg"
weight: "21"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/43860
imported:
- "2019"
_4images_image_id: "43860"
_4images_cat_id: "3249"
_4images_user_id: "1557"
_4images_image_date: "2016-07-07T15:20:37"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43860 -->
So sieht das hinten aus, wenn die Mulde waagerecht liegt. Der BS5 auf dem Gelenkwürfel sorgt für den nötigen Freiraum für Verschlußriegel und Statikplatte.