---
layout: "image"
title: "Verbesserung - Geänderte Heckpartie"
date: "2016-07-09T08:24:57"
picture: "einfacherkipplasterii1.jpg"
weight: "1"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/43863
imported:
- "2019"
_4images_image_id: "43863"
_4images_cat_id: "3249"
_4images_user_id: "1557"
_4images_image_date: "2016-07-09T08:24:57"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43863 -->
Die Kippmulde verschob sich doch noch zu leicht seitlich. Jetzt ist eine Quertraverse aus 3 Bausteinen 30 eingebaut. Zusätzlichen Halt bieten die Bauplatten; aussen je 1 Bauplatte 15 x 15 mit 1 Zapfen (38246) und in der Mitte 1 Bauplatte 15 x 30 mit 2 Zapfen (38241).

Die Gelenkwürfel sitzen jetzt eine Position weiter hinten an der Kippmulde und ganz innen in den Nuten. Als Nebeneffekt bleibt die Muldenöffnung beim Abkippen etwas weiter über dem Boden.