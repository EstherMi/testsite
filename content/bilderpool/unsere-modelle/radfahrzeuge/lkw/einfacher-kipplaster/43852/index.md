---
layout: "image"
title: "Detail - Muldenseite hinten"
date: "2016-07-07T15:20:37"
picture: "einfacherkipplaster08.jpg"
weight: "13"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/43852
imported:
- "2019"
_4images_image_id: "43852"
_4images_cat_id: "3249"
_4images_user_id: "1557"
_4images_image_date: "2016-07-07T15:20:37"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43852 -->
Wie schon angemerkt, so ganz paßt das mit der Ecke nicht, ist aber kein Problem hier.