---
layout: "image"
title: "Blick in die Mulde"
date: "2016-07-07T15:20:37"
picture: "einfacherkipplaster07.jpg"
weight: "12"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/43851
imported:
- "2019"
_4images_image_id: "43851"
_4images_cat_id: "3249"
_4images_user_id: "1557"
_4images_image_date: "2016-07-07T15:20:37"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43851 -->
So sieht es in der Mulde aus. So frei als möglich, damit das Ladegut auch ungehemmt herausrutscht.

Die gelbe Grundplatte 30 x 90 ist mit zwei Bauplatten 15 x 30 an der roten Grundplatte 90 x 90 angesteckt. Die schwarzen Grundplatten 120 x 60 sind hinten mit je einem WS90° und einer Federnocke an der Grundplatte 90 x 90 befestigt. Die jeweils zweite Federnocke, die da "kieloben" in der Nut der Grundplatte mit drinsteckt, sorgt dafür dass die WS90° nicht nach innen rutschen können. 100%-ig paßt das nicht (nächstes Bild), stört hier aber niemanden.