---
layout: "overview"
title: "Einfacher Kipplaster"
date: 2019-12-17T18:42:18+01:00
legacy_id:
- categories/3249
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3249 --> 
Erst "Einfacher Lastwagen mit Anhänger" (http://www.ftcommunity.de/categories.php?cat_id=3162) - dort ist die Grundkonstruktion des Fahrzeugrahmens zu finden. Und das feste Führerhaus.
Dann "Einfacher Abschleppwagen" (https://www.ftcommunity.de/categories.php?cat_id=3197) - von dem stammt das verwendete klappbare Führerhaus. Mit dem festen Führerhaus geht das aber auch.
Und nun "Einfacher Kipplaster" - der Abschleppkran ist durch eine Kippmulde ersetzt.

Wie man sieht ist das einfache Lasterchen durchaus wandelbar und das ist gut so. Junior's neuester Wunsch ließ sich auf die Art einigermaßen schnell erfüllen.