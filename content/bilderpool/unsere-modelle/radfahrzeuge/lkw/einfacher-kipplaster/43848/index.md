---
layout: "image"
title: "Kippstellung"
date: "2016-07-07T15:20:37"
picture: "einfacherkipplaster04.jpg"
weight: "9"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/43848
imported:
- "2019"
_4images_image_id: "43848"
_4images_cat_id: "3249"
_4images_user_id: "1557"
_4images_image_date: "2016-07-07T15:20:37"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43848 -->
Die Frontwand wird durch eine Grundplatte 90 x 90 gebildet. Auch sie ist am Rahmen befestigt. Der Boden besteht vorne aus einer Bauplatte 30 x 90 (hier gelb, weil rot gerade nicht zur Hand war) und einer weiteren Grundplatte 90 x 90. Die unterschiedlichen Dicken sind  sogar mal nützlich - doch dazu später.