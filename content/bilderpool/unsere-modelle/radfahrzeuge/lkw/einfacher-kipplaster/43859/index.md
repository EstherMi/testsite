---
layout: "image"
title: "Montagedetail - Frontwand II"
date: "2016-07-07T15:20:37"
picture: "einfacherkipplaster15.jpg"
weight: "20"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/43859
imported:
- "2019"
_4images_image_id: "43859"
_4images_cat_id: "3249"
_4images_user_id: "1557"
_4images_image_date: "2016-07-07T15:20:37"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43859 -->
Einfach von oben in den Rahmen einschieben. Die Federnocken sind beim ersten Versuch noch etwas fummelig, aber das sollte klappen. Am besten liegt die gelbe Bodenplatte auf einer festen Fläche auf, die Bauplatte 90 x 90 wird dann bis Anschlag runtergeschoben. Wenn die Seitenwände drin sind, kann man sie seitlich noch fein ausrichten. Vorne paßt das Maß exakt. Nur hinten ist die Mulde einen Hauch zu breit.