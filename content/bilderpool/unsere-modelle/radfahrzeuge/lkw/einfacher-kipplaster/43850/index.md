---
layout: "image"
title: "Befestigung der Kippgelenke"
date: "2016-07-07T15:20:37"
picture: "einfacherkipplaster06.jpg"
weight: "11"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/43850
imported:
- "2019"
_4images_image_id: "43850"
_4images_cat_id: "3249"
_4images_user_id: "1557"
_4images_image_date: "2016-07-07T15:20:37"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43850 -->
Das Fahrzeugheck ist schmucklos und hinten offen. Auf den Zapfen der WT sitzen die Gelenkwürfel. Eine Quertraverse gibt es hier nicht, eine Anhängerkupplung schon deswegen nicht. Das hat aber auch noch einen tieferen Sinn: Die Anhängerkupplung ist nicht im Weg wenn abgekippt wird.