---
layout: "image"
title: "Rechte Seite"
date: "2012-02-26T15:46:21"
picture: "kleinerservogelenktertransporter1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/34445
imported:
- "2019"
_4images_image_id: "34445"
_4images_cat_id: "2545"
_4images_user_id: "104"
_4images_image_date: "2012-02-26T15:46:21"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34445 -->
Lenkung und Antrieb liegen unten, oben sitzen IR-Empfänger, Batteriehalter und eine große nutzbare Ladefläche.