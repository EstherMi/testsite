---
layout: "comment"
hidden: true
title: "16583"
date: "2012-03-07T17:33:43"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Das klingt ja so, als würde das Teil bei Dir gar nicht fahren oder überhaupt nicht lenken? Also herumfahren, einschließlich Kurven und Wenden, konnte ich damit schon. Wird denn der mögliche Ausschlag bei Dir vom Servo gar nicht erreicht oder sowas? Klemmt da was?

Gruß,
Stefan