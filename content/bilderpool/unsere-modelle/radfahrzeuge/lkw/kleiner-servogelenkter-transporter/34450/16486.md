---
layout: "comment"
hidden: true
title: "16486"
date: "2012-02-26T15:55:06"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Nicht schlecht, der Kleine! Trotz der geringen Größe eine ordentliche Ladefläche - prima!

Und natürlich vielen Dank auch an thomas004 für die obergeniale Idee mit Klemmkupplung auf der Servoachse ... ;o)

Gruß, Thomas