---
layout: "comment"
hidden: true
title: "16580"
date: "2012-03-07T12:52:00"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Stimmt, berauschend war der Wendekreis nicht. Ab wann man das unbrauchbar nennt, ist vielleicht Geschmackssache, aber ich gestehe gerne zu, dass ich das Teil schon wieder zerlegt habe. ;-)

Gruß,
Stefan