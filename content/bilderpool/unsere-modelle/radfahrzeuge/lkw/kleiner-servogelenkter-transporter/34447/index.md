---
layout: "image"
title: "Antrieb"
date: "2012-02-26T15:46:21"
picture: "kleinerservogelenktertransporter3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/34447
imported:
- "2019"
_4images_image_id: "34447"
_4images_cat_id: "2545"
_4images_user_id: "104"
_4images_image_date: "2012-02-26T15:46:21"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34447 -->
Das U-Getriebe hätte das Fahrzeug so unschön lang gemacht, deshalb kommt mal wieder die gute alte Minischnecke zu Ehren. Die Fahrleistungen sind nicht berauschend, aber es fährt.