---
layout: "image"
title: "Presse Wand vorne"
date: "2006-04-08T12:26:19"
picture: "DSCN4449_2.jpg"
weight: "22"
konstrukteure: 
- "Arjen Neijsen"
fotografen:
- "Arjen Neijsen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/6047
imported:
- "2019"
_4images_image_id: "6047"
_4images_cat_id: "524"
_4images_user_id: "162"
_4images_image_date: "2006-04-08T12:26:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6047 -->
