---
layout: "image"
title: "Gesamtansicht 2"
date: "2009-09-22T18:47:58"
picture: "lkwmitladeflaeche02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/25057
imported:
- "2019"
_4images_image_id: "25057"
_4images_cat_id: "1770"
_4images_user_id: "502"
_4images_image_date: "2009-09-22T18:47:58"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25057 -->
