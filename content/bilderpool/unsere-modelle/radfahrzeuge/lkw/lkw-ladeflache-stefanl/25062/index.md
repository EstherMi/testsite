---
layout: "image"
title: "Unterseite 4"
date: "2009-09-22T18:47:58"
picture: "lkwmitladeflaeche07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/25062
imported:
- "2019"
_4images_image_id: "25062"
_4images_cat_id: "1770"
_4images_user_id: "502"
_4images_image_date: "2009-09-22T18:47:58"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25062 -->
