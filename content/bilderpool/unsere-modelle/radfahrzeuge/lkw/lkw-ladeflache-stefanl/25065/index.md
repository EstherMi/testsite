---
layout: "image"
title: "Kipper 1"
date: "2009-09-22T18:47:59"
picture: "lkwmitladeflaeche10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/25065
imported:
- "2019"
_4images_image_id: "25065"
_4images_cat_id: "1770"
_4images_user_id: "502"
_4images_image_date: "2009-09-22T18:47:59"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25065 -->
