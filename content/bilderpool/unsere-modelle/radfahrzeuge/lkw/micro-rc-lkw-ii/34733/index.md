---
layout: "image"
title: "Micro-RC-LKW II 12"
date: "2012-03-31T23:32:00"
picture: "Micro-RC-LKW_II_12.jpg"
weight: "24"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/34733
imported:
- "2019"
_4images_image_id: "34733"
_4images_cat_id: "2565"
_4images_user_id: "328"
_4images_image_date: "2012-03-31T23:32:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34733 -->
Durch das hintere Fenster im Führerhaus kommt eigentlich genug Licht zum IR Empfänger im Heck, damit sich das Fahrzeug sicher steuern lässt. Das Loch im Dach habe ich trotzdem vorgesehen, da die Reichweite zum Sender damit deutlich größer ist!