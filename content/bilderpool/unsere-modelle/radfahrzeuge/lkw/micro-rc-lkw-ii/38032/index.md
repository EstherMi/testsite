---
layout: "image"
title: "Micro-RC-LKW II 23"
date: "2014-01-08T23:15:05"
picture: "DSC09213.jpg"
weight: "10"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/38032
imported:
- "2019"
_4images_image_id: "38032"
_4images_cat_id: "2565"
_4images_user_id: "328"
_4images_image_date: "2014-01-08T23:15:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38032 -->
Das Führerhaus noch weiter zerlegt, Ansicht von hinten.

Man sieht gut, wie die Servo-Achse über eine Klemmkupplung 31024 durch die Bauplatte hindurch die Vorderachse lenkt. Wie bekannt ist, funktionieren nicht alle Klemmkupplungen, denn es gibt wohl starke Toleranzen in der Größe der Öffnungen. Mit ein wenig Probieren findet man aber leicht eine Kupplung, die sich gut auf der Servo-Achse befestigen lässt und die das Drehmoment gut auf die Lenkung überträgt.