---
layout: "image"
title: "Micro-RC-LKW II 11"
date: "2012-03-31T23:32:00"
picture: "Micro-RC-LKW_II_11.jpg"
weight: "23"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/34732
imported:
- "2019"
_4images_image_id: "34732"
_4images_cat_id: "2565"
_4images_user_id: "328"
_4images_image_date: "2012-03-31T23:32:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34732 -->
Hier sieht man nochmal deutlich die beiden Mitnehmer 31712.