---
layout: "image"
title: "Micro-RC-LKW II 20"
date: "2014-01-08T23:15:05"
picture: "DSC09205.jpg"
weight: "7"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/38029
imported:
- "2019"
_4images_image_id: "38029"
_4images_cat_id: "2565"
_4images_user_id: "328"
_4images_image_date: "2014-01-08T23:15:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38029 -->
Das Führerhaus im Detail.