---
layout: "comment"
hidden: true
title: "17674"
date: "2013-01-01T21:49:47"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Klassische fischertechnik vom Feinsten... für die damaligen Möglichkeiten von ft ein gigantisches Teil. Ich wäre vor Neid erblasst, hätte ich das Modell damals gesehen!
Gruß, Dirk