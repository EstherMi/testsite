---
layout: "image"
title: "Radlader"
date: "2012-02-07T19:33:36"
picture: "radlader_solo.jpg"
weight: "15"
konstrukteure: 
- "Johannes Richter"
fotografen:
- "Johannes Richter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Beluthius"
license: "unknown"
legacy_id:
- details/34117
imported:
- "2019"
_4images_image_id: "34117"
_4images_cat_id: "2523"
_4images_user_id: "1443"
_4images_image_date: "2012-02-07T19:33:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34117 -->
