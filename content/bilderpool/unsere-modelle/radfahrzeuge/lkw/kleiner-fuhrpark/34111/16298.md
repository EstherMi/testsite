---
layout: "comment"
hidden: true
title: "16298"
date: "2012-02-08T17:01:16"
uploadBy:
- "Beluthius"
license: "unknown"
imported:
- "2019"
---
Danke für die Blumen. Ganz ehrlich, in den letzen Jahren hatte ich immer wieder vor, was zu bauen, allein schon weil ich gerne mit den Händen was mache und einfach drauf los baue.
Richtig los gelegt habe ich erst vor ein paar Wochen wieder, quasi als Therapie; denn beim Bauen vergesse ich alles um mich herum, ich benutze Kopf und Hände. 
Kurz: Es macht einfach sehr viel Spass. Und nicht zuletzt haben micht die tollen Modelle und Detaillösungen hier angespornt.

Gruß, Johannes