---
layout: "image"
title: "4achser 1"
date: "2012-02-07T19:33:25"
picture: "4achser_01.jpg"
weight: "1"
konstrukteure: 
- "Johannes Richter"
fotografen:
- "Johannes Richter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Beluthius"
license: "unknown"
legacy_id:
- details/34103
imported:
- "2019"
_4images_image_id: "34103"
_4images_cat_id: "2523"
_4images_user_id: "1443"
_4images_image_date: "2012-02-07T19:33:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34103 -->
Nach fast 30 Jahren hab auch ich mein ft wieder heraus gekramt und konnte einen Kindheitstraum: Ein LKW mit 2 gelenkten Vorderachsen! Dank dieser tollen Seite hatte ich die nötige Inspiration und habe mich an mein "altes" Lieblingsspielzeug gesetzt...