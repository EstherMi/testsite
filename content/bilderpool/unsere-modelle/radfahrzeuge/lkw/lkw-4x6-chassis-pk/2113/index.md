---
layout: "image"
title: "LKW4x6-10.JPG"
date: "2004-02-15T20:20:31"
picture: "LKW4x6-10.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2113
imported:
- "2019"
_4images_image_id: "2113"
_4images_cat_id: "430"
_4images_user_id: "4"
_4images_image_date: "2004-02-15T20:20:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2113 -->
Hier die Hinterachskonstruktion. Das besondere daran: alles Originalteile, nichts geschnippelt, nichts fremdes genommen.