---
layout: "image"
title: "Antrieb"
date: "2007-04-04T10:29:45"
picture: "lkw2.jpg"
weight: "9"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- details/9948
imported:
- "2019"
_4images_image_id: "9948"
_4images_cat_id: "898"
_4images_user_id: "557"
_4images_image_date: "2007-04-04T10:29:45"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9948 -->
über Schnecke