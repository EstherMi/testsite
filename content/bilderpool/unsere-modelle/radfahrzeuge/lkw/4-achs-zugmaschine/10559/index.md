---
layout: "image"
title: "4-Achs-Zugmaschine 6"
date: "2007-05-30T15:13:24"
picture: "LKW_-_6.jpg"
weight: "6"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/10559
imported:
- "2019"
_4images_image_id: "10559"
_4images_cat_id: "960"
_4images_user_id: "9"
_4images_image_date: "2007-05-30T15:13:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10559 -->
