---
layout: "image"
title: "4-Achs-Zugmaschine 1"
date: "2007-05-30T15:10:36"
picture: "LKW_-_1.jpg"
weight: "1"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: ["LKW", "Zugmaschine"]
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/10554
imported:
- "2019"
_4images_image_id: "10554"
_4images_cat_id: "960"
_4images_user_id: "9"
_4images_image_date: "2007-05-30T15:10:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10554 -->
Versuch eines LKW-Modells, das die Proportionen des Originals erhält. Gleichzeitig ist die Doppelachslenkung recht interessant. 

Ein Antrieb fehlt, der Rahmen ist nicht unbedingt stark belastbar.