---
layout: "image"
title: "4-Achs-Zugmaschine Rückansicht"
date: "2007-05-30T15:12:25"
picture: "LKW_-_4.jpg"
weight: "4"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/10557
imported:
- "2019"
_4images_image_id: "10557"
_4images_cat_id: "960"
_4images_user_id: "9"
_4images_image_date: "2007-05-30T15:12:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10557 -->
