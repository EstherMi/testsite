---
layout: "image"
title: "4-Achs-Zugmaschine Seitenansicht"
date: "2007-05-30T15:11:53"
picture: "LKW_-_3.jpg"
weight: "3"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/10556
imported:
- "2019"
_4images_image_id: "10556"
_4images_cat_id: "960"
_4images_user_id: "9"
_4images_image_date: "2007-05-30T15:11:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10556 -->
