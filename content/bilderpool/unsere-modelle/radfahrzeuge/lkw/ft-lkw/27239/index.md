---
layout: "image"
title: "LKW"
date: "2010-05-16T12:29:39"
picture: "ftlkw2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "michelino"
license: "unknown"
legacy_id:
- details/27239
imported:
- "2019"
_4images_image_id: "27239"
_4images_cat_id: "1956"
_4images_user_id: "876"
_4images_image_date: "2010-05-16T12:29:39"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27239 -->
Von unten gesehen, Antrieb und Lenkung.