---
layout: "overview"
title: "Ft LKW"
date: 2019-12-17T18:40:52+01:00
legacy_id:
- categories/1956
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1956 --> 
LKW mit Sound und Light, Front  und Rückscheinwerfer selbstgebaut mit LEDs. Unter der Ladefläche ist Platz für Accu, Kabel und weitere Module.