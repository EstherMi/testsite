---
layout: "image"
title: "LKW"
date: "2010-05-16T12:29:39"
picture: "ftlkw1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "michelino"
license: "unknown"
legacy_id:
- details/27238
imported:
- "2019"
_4images_image_id: "27238"
_4images_cat_id: "1956"
_4images_user_id: "876"
_4images_image_date: "2010-05-16T12:29:39"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27238 -->
LKW von der Rückseite fotografiert, Empfänger zu sehen.
Mit Ladefläche