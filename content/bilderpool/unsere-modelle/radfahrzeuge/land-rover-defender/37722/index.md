---
layout: "image"
title: "IMG_9945.JPG"
date: "2013-10-19T16:00:21"
picture: "IMG_9945.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/37722
imported:
- "2019"
_4images_image_id: "37722"
_4images_cat_id: "2801"
_4images_user_id: "4"
_4images_image_date: "2013-10-19T16:00:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37722 -->
Mit tiefer gesetzter Rückpartie, damit die Hecktür hinein passt. Die Tür dreht in BS15 mit runden (roten) Zapfen.