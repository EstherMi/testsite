---
layout: "image"
title: "IMG_9195.JPG"
date: "2013-10-07T21:12:34"
picture: "IMG_9195mit.JPG"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/37693
imported:
- "2019"
_4images_image_id: "37693"
_4images_cat_id: "2801"
_4images_user_id: "4"
_4images_image_date: "2013-10-07T21:12:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37693 -->
Das Lenkgestänge passt schön flach noch unter den Antriebsstrang. 
Die Lenkhebel sitzen ziemlich knapp, und sind auch die Schwachstelle des Antriebs: nach einem Weilchen Fahrbetrieb wackeln sie sich frei.

Die graue Scheibe 15 oben ist ein Originalteil, und die Achse da hindurch möge man bitte nicht als gemoddet anrechnen: da tut es jede andere Achse auch; nur diese war zuerst zur Hand.