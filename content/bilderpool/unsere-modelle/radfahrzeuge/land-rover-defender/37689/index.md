---
layout: "image"
title: "IMG_9191.JPG"
date: "2013-10-07T20:53:46"
picture: "IMG_9191mit.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/37689
imported:
- "2019"
_4images_image_id: "37689"
_4images_cat_id: "2801"
_4images_user_id: "4"
_4images_image_date: "2013-10-07T20:53:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37689 -->
Die Vorderachse. Das Differenzial sitzt tiefer als die seitlichen schwarzen Rastkegel, und macht damit oben drüber den Platz für Motor und Empfänger frei. Die Lenkung hat ihren Ursprung bei der ft-Kettenlenkung von Stefan Falk ( http://www.ftcommunity.de/details.php?image_id=33043 ). "Was der mit der Kette macht, sollte auch mit nem Hebel gehen", und siehe da, es ging.