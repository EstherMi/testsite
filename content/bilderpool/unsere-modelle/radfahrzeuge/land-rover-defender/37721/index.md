---
layout: "image"
title: "IMG_9948.JPG"
date: "2013-10-19T15:59:18"
picture: "IMG_9948.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/37721
imported:
- "2019"
_4images_image_id: "37721"
_4images_cat_id: "2801"
_4images_user_id: "4"
_4images_image_date: "2013-10-19T15:59:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37721 -->
Das ist die verlängerte Version von der Convention 2013.