---
layout: "image"
title: "IMG_9189.JPG"
date: "2013-10-07T20:46:02"
picture: "IMG_9189mit.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/37688
imported:
- "2019"
_4images_image_id: "37688"
_4images_cat_id: "2801"
_4images_user_id: "4"
_4images_image_date: "2013-10-07T20:46:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37688 -->
Die Unterseite im Ganzen. Das Differenzial der Hinterachse musste etwas tiefer gelegt werden, weil sich sonst das Fahrzeug beim Losfahren zu sehr aufbäumt. Alle vier Räder sitzen auf Freilaufnaben und werden über Rastkegel am äußeren Umfang angetrieben. 

Die Anordnung in der Mitte nennt sich "frei fliegendes Mitteldifferenzial" -- sie tut aber, was sie soll!