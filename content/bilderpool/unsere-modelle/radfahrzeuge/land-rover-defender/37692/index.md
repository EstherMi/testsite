---
layout: "image"
title: "IMG_9199.JPG"
date: "2013-10-07T21:08:16"
picture: "IMG_9199mit.JPG"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/37692
imported:
- "2019"
_4images_image_id: "37692"
_4images_cat_id: "2801"
_4images_user_id: "4"
_4images_image_date: "2013-10-07T21:08:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37692 -->
Die Schrägansicht zeigt besser, wie das alles zusammen hängt. Die schwarzen Hülsen 15 wie auch die Radaufnahmen ("Rastachse mit Platte" 130593) sind soweit in die Gelenkwürfel eingesteckt, wie es möglich ist, und dann stimmt der Abstand zwischen dem weißen Kegelzahnrad und dem Zahnkranz des Reifen 60. Was von den 'Rastachse mit Platte' nach unten übersteht, wird gebraucht, um da die Lenkhebel aufzuschieben.