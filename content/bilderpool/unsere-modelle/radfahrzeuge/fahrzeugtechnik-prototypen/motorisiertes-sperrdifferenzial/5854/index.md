---
layout: "image"
title: "Sperrdifferential (1)"
date: "2006-03-12T10:51:05"
picture: "P3110004.jpg"
weight: "1"
konstrukteure: 
- "Michael K."
fotografen:
- "Michael K."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Wert"
license: "unknown"
legacy_id:
- details/5854
imported:
- "2019"
_4images_image_id: "5854"
_4images_cat_id: "505"
_4images_user_id: "366"
_4images_image_date: "2006-03-12T10:51:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5854 -->
Ich habe hier auf die schnelle mal ein motorisiertes Sperrdifferential gebaut. Das hat einfach den Vorteil, das man das D. z.B.über den Computer steuern kann. Leider ist es aber nur eine 0%auf100%-sperre.