---
layout: "image"
title: "Differentialsperre geschlossen"
date: "2005-04-27T16:06:21"
picture: "Sperre_geschlossen_variabel.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/4091
imported:
- "2019"
_4images_image_id: "4091"
_4images_cat_id: "641"
_4images_user_id: "328"
_4images_image_date: "2005-04-27T16:06:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4091 -->
Schiebt man die Welle seitlich, so dass beide Z10 in die jeweiligen Z20 ragen, greift die Sperre. Der Trick ist, dass das kleine Z10 rechts im Bild nur halbfest auf der Welle verschraubt ist und - je nach Anziehkraft der roten Schraube - entsprechend rutscht. Somit wird ermöglicht, die Sperrwirkung von 0 Prozent (Z10 ganz locker) bis 100 Prozent (Z10 ganz fest verschraubt) stufenlos zu variieren! Das Drehmoment wird über das Rutschen immer auch mit auf das durchdrehende Rad verteilt, so dass die Geländegängigkeit erhöht wird.