---
layout: "image"
title: "Differentialsperre offen"
date: "2005-04-27T16:06:21"
picture: "Sperre_offen.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/4090
imported:
- "2019"
_4images_image_id: "4090"
_4images_cat_id: "641"
_4images_user_id: "328"
_4images_image_date: "2005-04-27T16:06:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4090 -->
Schaltbare Differentialsperre in offenen Zustand: Auf der Antriebswelle rechts im Bild ist ein Z20 fest verschraubt. Die parallele Welle trägt zwei Z10 und ist seitlich verschiebbar. Das kann z.B. über einen Hebel oder einen Pneumatikzylinder erfolgen (hier noch nicht dargestellt). Hier ist die Sperre offen (0 Prozent Sperrwirkung)