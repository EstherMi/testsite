---
layout: "image"
title: "endu62-106.JPG"
date: "2007-10-03T20:56:47"
picture: "endu62-106.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["modding"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/12128
imported:
- "2019"
_4images_image_id: "12128"
_4images_cat_id: "1086"
_4images_user_id: "4"
_4images_image_date: "2007-10-03T20:56:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12128 -->
Mit geeigneter Bearbeitung (spanabhebend) kann man die "alten" Reifen 60 reibschlüssig an den Traktorreifen montieren. Sie werden auf die Nabe 45 aufgesetzt und mittels Nabenmutter festgezogen.

Und nein, die Metallachsen haben KEINE Rändelung. Da spiegeln sich nur die Riffel der Reifen 60 drin :-)