---
layout: "image"
title: "endu62-098.JPG"
date: "2007-10-03T21:02:52"
picture: "endu62-098.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["modding"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/12129
imported:
- "2019"
_4images_image_id: "12129"
_4images_cat_id: "1086"
_4images_user_id: "4"
_4images_image_date: "2007-10-03T21:02:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12129 -->
Nichts für empfindsame Gemüter!

Der Reifen 60 (37236) wird auf der Innenseite soweit abgedreht, dass er plan mit der Nabe abschließt. Außen wird in der Laufbahn des Gummirings durchgestochen und dann noch soviel weggenommen, bis der Steg zwischen Riffelung und Gummiring-Aufnahme nur noch zur Hälfte da ist.

Der Fachmann wird die Rattermarken erkennen. Ich sollte nochmal den Grundkurs "Drehen" besuchen.