---
layout: "image"
title: "endu62-109.JPG"
date: "2007-10-03T20:50:43"
picture: "endu62-109.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/12127
imported:
- "2019"
_4images_image_id: "12127"
_4images_cat_id: "1086"
_4images_user_id: "4"
_4images_image_date: "2007-10-03T20:50:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12127 -->
Im Forum erhob Thomas004 die Frage nach einem Allradantrieb, der den Kräften eines Powermot wirklich gewachsen ist. 

Das hier könnte einer werden. Wenn man auf den Traktorreifen verzichtet und den "alten" Reifen 60 solo einsetzt, geht es auch ohne Teile-Modding.

Der Antrieb ist allerdings darauf angewiesen, dass Gewicht von oben drauf liegt. Nur dann wird das Kegelrad ausreichend in die Riffelung der Radflanke gedrückt.