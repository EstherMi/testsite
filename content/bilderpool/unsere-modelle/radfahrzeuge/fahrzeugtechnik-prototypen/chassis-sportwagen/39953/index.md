---
layout: "image"
title: "Gesamtansicht von oben"
date: "2014-12-22T22:09:08"
picture: "chassisfuersportwagen02.jpg"
weight: "10"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/39953
imported:
- "2019"
_4images_image_id: "39953"
_4images_cat_id: "3003"
_4images_user_id: "2321"
_4images_image_date: "2014-12-22T22:09:08"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39953 -->
Der Rahmen ist einfach gehalten