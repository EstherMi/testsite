---
layout: "image"
title: "Gesamtansich von unten"
date: "2014-12-22T22:09:08"
picture: "chassisfuersportwagen03.jpg"
weight: "11"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/39954
imported:
- "2019"
_4images_image_id: "39954"
_4images_cat_id: "3003"
_4images_user_id: "2321"
_4images_image_date: "2014-12-22T22:09:08"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39954 -->
Manche Teile sind nicht gegen Verschieben gesichert. Ich denke, das kann später noch zusammen mit dem Aufbau der Karosserie erfolgen.