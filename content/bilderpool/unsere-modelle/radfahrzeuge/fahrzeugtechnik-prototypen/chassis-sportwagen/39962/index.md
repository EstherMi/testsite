---
layout: "image"
title: "Antrieb Vorbereitung"
date: "2014-12-22T22:09:08"
picture: "chassisfuersportwagen11.jpg"
weight: "19"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/39962
imported:
- "2019"
_4images_image_id: "39962"
_4images_cat_id: "3003"
_4images_user_id: "2321"
_4images_image_date: "2014-12-22T22:09:08"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39962 -->
Der Powermotor liegt hoffentlich übermorgen unter dem Weihnachstbaum, hier ist erst mal die Vorbereitung zu sehen.