---
layout: "image"
title: "Gesamtansicht"
date: "2014-12-22T22:09:08"
picture: "chassisfuersportwagen01.jpg"
weight: "9"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/39952
imported:
- "2019"
_4images_image_id: "39952"
_4images_cat_id: "3003"
_4images_user_id: "2321"
_4images_image_date: "2014-12-22T22:09:08"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39952 -->
Mein Ziel war es, ein sehr flaches Chassis zu bauen. Besondere Gimmicks: Das Lenkrad dreht sich mit, wenn der Lenkservo arbeitet, und zwar auch realistisch um mehrere Umdrehungen. Außerdem habe ich die Alufelgen aus http://www.ftcommunity.de/categories.php?cat_id=2998 verarbeitet und die Aufhängung der Achsschenkel befindet sich innerhalb der Radnabe. Derzeit fehlt noch der Powermotor, den gibt's erst zu Weihnachten. Aber die Befestigung ist schon vorbereitet.