---
layout: "image"
title: "Ansatz Karosserie, Motorhaube unt Tür"
date: "2014-12-31T07:49:08"
picture: "9_Karosserie_5_klein.jpg"
weight: "8"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/40087
imported:
- "2019"
_4images_image_id: "40087"
_4images_cat_id: "3003"
_4images_user_id: "2321"
_4images_image_date: "2014-12-31T07:49:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40087 -->
So, ich denke als Prototyp für ein Chassis ist hier erst mal Schluss und eine Pause tut auch ganz gut. Die weiteren Schritte werde ich dann wohl in der Kategorie "PKW und Busse" unterbringen.