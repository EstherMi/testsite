---
layout: "image"
title: "Ansatz Karosserie"
date: "2014-12-31T07:49:08"
picture: "9_Karosserie_1_klein.jpg"
weight: "7"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/40086
imported:
- "2019"
_4images_image_id: "40086"
_4images_cat_id: "3003"
_4images_user_id: "2321"
_4images_image_date: "2014-12-31T07:49:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40086 -->
Ein erster Ansatz für die Befestigung einer Motorhaube.