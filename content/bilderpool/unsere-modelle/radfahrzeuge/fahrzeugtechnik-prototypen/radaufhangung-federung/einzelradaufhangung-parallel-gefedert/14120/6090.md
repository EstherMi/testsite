---
layout: "comment"
hidden: true
title: "6090"
date: "2008-03-27T00:00:29"
uploadBy:
- "Porsche-Makus"
license: "unknown"
imported:
- "2019"
---
Alternativ kann auf der rechten Seite natürlich auch noch eine zusätzliche zweite schwarze Feder eingebaut werden. Das würde die Stabilität auch noch zusätzlich erhöhen, da das Ganze dann "gespiegelt" einfedern würde.

Allerdings "baut" es dann etwas größer und breiter und die Federrate ist natürlich härter, was nicht immer gewünscht ist.

Muß jeder von Fall zu Fall für sich selbst entscheiden, wie er es macht.