---
layout: "image"
title: "Einzelradaufhängung parallel gefedert 05"
date: "2008-03-26T21:50:31"
picture: "05.jpg"
weight: "5"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- details/14121
imported:
- "2019"
_4images_image_id: "14121"
_4images_cat_id: "1297"
_4images_user_id: "327"
_4images_image_date: "2008-03-26T21:50:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14121 -->
Hier sieht man das ganze eingefedert.

Die schwarze Feder wirkt hierbei einfach "andersherum" als bei einer Normalen Federung, nämlich als Zug- anstallt als Druckfeder.

Funktioniert genau so gut!