---
layout: "overview"
title: "Einzelradaufhängung parallel gefedert (Porsche-Makus)"
date: 2019-12-17T18:44:12+01:00
legacy_id:
- categories/1297
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1297 --> 
Hallo,
die ursprüngliche Idee habe ich vom "Lowrider"-Modell übernommen.

Bei meiner Konstruktion waren mir folgende Punkte wichtig:

&#8226; Nur häufig vorhandene und günstige Standard-FT-Bausteine
&#8226; Kompakte und filigrane Bauweise
&#8226; Trotzdem genug Stabilität
&#8226; Hohe Wiederverwendbarkeit
&#8226; Federrate über Druckluft variabel
&#8226; Einfache Integration in vorhandene Modelle
&#8226; Möglichst realistisch

Was mir hier nicht gelungen ist, ist der Antrieb des Rades. Dadurch, daß sich das Rad parallel bewegt, ist dies ein enorm schwieriges Unterfangen.