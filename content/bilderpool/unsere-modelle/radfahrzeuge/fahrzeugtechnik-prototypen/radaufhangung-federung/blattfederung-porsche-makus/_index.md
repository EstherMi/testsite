---
layout: "overview"
title: "Blattfederung (Porsche-Makus)"
date: 2019-12-17T18:44:10+01:00
legacy_id:
- categories/1288
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1288 --> 
Hallo,

weiß nicht, ob das schon mal da war. Ich habe einfach aus einigen Statikstreben eine Blattfederung nachgebaut. Zusätzlich brauchts aber noch die schwarze Feder, damit das ganze wieder zurückfedert. Aber nett anzusehen, wie ich finde und dem Original schon sehr nah!