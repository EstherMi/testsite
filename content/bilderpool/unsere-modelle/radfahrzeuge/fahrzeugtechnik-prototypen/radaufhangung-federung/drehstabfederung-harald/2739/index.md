---
layout: "image"
title: "Drehstab10"
date: "2004-10-21T21:41:57"
picture: "Drehstab10.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Federung", "Torsion", "Drehstab"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2739
imported:
- "2019"
_4images_image_id: "2739"
_4images_cat_id: "279"
_4images_user_id: "4"
_4images_image_date: "2004-10-21T21:41:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2739 -->
Noch eine Variante von Torsionsfederung. Der schwere Akkublock (hier durch die leere Box vertreten) ist in Achsmitte und knapp über der Fahrbahn am besten aufgehoben. 
Die Vorspannung der Federung kann über die Auswahl der S-Streben und deren Befestigungspunkte in gewissen Grenzen verändert werden.