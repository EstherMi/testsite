---
layout: "image"
title: "Drehstab12"
date: "2004-10-21T21:41:57"
picture: "Drehstab12.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Federung", "Torsion", "Drehstab"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2741
imported:
- "2019"
_4images_image_id: "2741"
_4images_cat_id: "279"
_4images_user_id: "4"
_4images_image_date: "2004-10-21T21:41:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2741 -->
