---
layout: "image"
title: "Drehstab2.JPG"
date: "2004-09-09T23:17:23"
picture: "Drehstab2.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Federung", "Achse"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2576
imported:
- "2019"
_4images_image_id: "2576"
_4images_cat_id: "279"
_4images_user_id: "4"
_4images_image_date: "2004-09-09T23:17:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2576 -->
Die Achse mit dem dünneren Ende dran gehört 'eigentlich' zum Hubgetriebe. Die Vorspannung kann stufenweise geändert werden (je nachdem, in welcher Lage man die Kegelräder zusammenschiebt).