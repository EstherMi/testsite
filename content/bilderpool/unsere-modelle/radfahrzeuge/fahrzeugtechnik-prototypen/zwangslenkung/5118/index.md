---
layout: "image"
title: "Der Zug"
date: "2005-10-26T15:25:23"
picture: "Zwangslenkung_-_1.jpg"
weight: "1"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/5118
imported:
- "2019"
_4images_image_id: "5118"
_4images_cat_id: "185"
_4images_user_id: "9"
_4images_image_date: "2005-10-26T15:25:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5118 -->
Der Anhänger bleibt nicht hundertprozentig in der Spur - aber mit ein bisschen Justieren sollte es ausreichen.