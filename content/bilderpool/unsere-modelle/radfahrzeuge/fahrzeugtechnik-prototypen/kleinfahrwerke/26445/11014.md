---
layout: "comment"
hidden: true
title: "11014"
date: "2010-02-24T12:58:27"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Tach Thomas,

so ungefähr, da federt aber nichts. Die Strebe ist ja starr, und was vor allem vor Verdrehung geschützt werden muss, ist das das Differential treibende Zahnrad. Wäre das nur vorne gelagert, wäre das nach oben/unten drehbar und würde kaum antreiben, sondern sich herausdrehen. Der Rest stimmt: Die BS5 hängen per Statikadapter in der Strebe und sind über den Verbinder 45 verbunden, damit sich letztlich der Antrieb nicht aus dem Differential winden kann - das ist ihr einziger Zweck. Die Hinterachse ist nicht mit den Streben oder BS5 verbunden.

Gruß,
Stefan