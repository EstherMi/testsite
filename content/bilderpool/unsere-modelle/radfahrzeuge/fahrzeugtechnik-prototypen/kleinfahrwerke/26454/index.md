---
layout: "image"
title: "Allrad, ungefedert, kurzer Radstand"
date: "2010-02-15T23:12:33"
picture: "kleinfahrwerke1_2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/26454
imported:
- "2019"
_4images_image_id: "26454"
_4images_cat_id: "1881"
_4images_user_id: "104"
_4images_image_date: "2010-02-15T23:12:33"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26454 -->
Noch ein kleines Allradfahrwerk mit besonders kurzem Radstand. Es fährt ziemlich flott.