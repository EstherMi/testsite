---
layout: "image"
title: "Allrad, ungefedert, kurzer Radstand"
date: "2010-02-15T23:12:33"
picture: "kleinfahrwerke2_2.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/26455
imported:
- "2019"
_4images_image_id: "26455"
_4images_cat_id: "1881"
_4images_user_id: "104"
_4images_image_date: "2010-02-15T23:12:33"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26455 -->
Die Differentiale werden von außen angetrieben.