---
layout: "image"
title: "Allradantrieb, gefedert"
date: "2010-02-15T21:05:56"
picture: "kleinfahrwerke8.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/26452
imported:
- "2019"
_4images_image_id: "26452"
_4images_cat_id: "1881"
_4images_user_id: "104"
_4images_image_date: "2010-02-15T21:05:56"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26452 -->
Hier ein Detailblick auf die Radaufhängung. Die "Hülsen mit Scheibe" sind mit einem Statikstopfen mit der I-Strebe 30 verbunden.