---
layout: "image"
title: "Allradantrieb, gefedert"
date: "2010-02-15T21:05:56"
picture: "kleinfahrwerke7.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/26451
imported:
- "2019"
_4images_image_id: "26451"
_4images_cat_id: "1881"
_4images_user_id: "104"
_4images_image_date: "2010-02-15T21:05:56"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26451 -->
Die Räder sind an Längslenkern aufgehängt und können so federn und sich verschränken. Die roten "Hülsen mit Scheibe" sind für die Federung wichtig: Das sind die Teile, die sich an der Federung abstützen.

Wenn man will, kann man die vier BS7,5 noch mit zwei Verbindern 45 koppeln und das Fahrgestell dadurch versteifen, aber es fährt auch so prima.