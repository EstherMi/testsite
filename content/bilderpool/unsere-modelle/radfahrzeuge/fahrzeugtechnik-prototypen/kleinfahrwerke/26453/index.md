---
layout: "image"
title: "Allradantrieb, gefedert"
date: "2010-02-15T21:05:56"
picture: "kleinfahrwerke9.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/26453
imported:
- "2019"
_4images_image_id: "26453"
_4images_cat_id: "1881"
_4images_user_id: "104"
_4images_image_date: "2010-02-15T21:05:56"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26453 -->
Hier wurden die Räder auf einer Seite abgenommen. Die waagerecht liegenden schwarzen I-Streben 60 sind mit je zwei Statikadaptern an den BS7,5 befestigt, ähnlich wie die Längslenker auch. Man sieht hier links die maximale Ausfederung. Die wird durch das Anschlagen der Hülse mit Scheibe am BS7,5 begrenzt. Das Differential ist dabei immer noch sauber angetrieben. Rechts sieht man die normale Fahrstellung, und das könnte noch einfedern.