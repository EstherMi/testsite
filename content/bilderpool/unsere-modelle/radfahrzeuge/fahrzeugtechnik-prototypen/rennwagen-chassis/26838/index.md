---
layout: "image"
title: "Rennwagen-Chassis 4"
date: "2010-03-29T19:03:25"
picture: "Rennwagen-Chassis_4.jpg"
weight: "4"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/26838
imported:
- "2019"
_4images_image_id: "26838"
_4images_cat_id: "1919"
_4images_user_id: "328"
_4images_image_date: "2010-03-29T19:03:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26838 -->
Die Draufsicht.