---
layout: "image"
title: "Rennwagen-Chassis 1"
date: "2010-03-29T19:03:24"
picture: "Rennwagen-Chassis_1.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/26835
imported:
- "2019"
_4images_image_id: "26835"
_4images_cat_id: "1919"
_4images_user_id: "328"
_4images_image_date: "2010-03-29T19:03:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26835 -->
Meine neue "servo-gelenkte Vorderachse 80" aus diesem Modell

http://www.ftcommunity.de/categories.php?cat_id=1910

ist durch ihre Kompaktheit und ihr direktes Lenkgefühl natürlich prädestiniert dafür, in einem schnelleren Fahrzeug mit größerem Motor eingebaut zu werden.

Als Ergebnis entstand dieses sehr kompakte und vor allem flache Rennwagen-Chassis mit Power-Motor, Hinterachsdifferenzial und 4 Speichenfelgen 23. Zusätzlich müssen nur noch der Empfänger und der Akku-Pack oben auf dem Chassis integriert werden.

Das Fahrzeug ist sehr schnell und durch den großen möglichen Lenkwinkel an der Vorderachse extrem wendig. Durch das sehr schnelle Ansprechen des Servos auf die Lenkbefehle ist es überraschend direkt und schnell steuerbar. Baut es nach, Ihr werdet begeistert sein!