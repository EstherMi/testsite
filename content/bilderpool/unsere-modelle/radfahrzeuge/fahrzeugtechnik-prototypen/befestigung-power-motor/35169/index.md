---
layout: "image"
title: "Extra Befestigung Power-Motor 3"
date: "2012-07-15T19:48:00"
picture: "bevestigungpowermotor3.jpg"
weight: "3"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/35169
imported:
- "2019"
_4images_image_id: "35169"
_4images_cat_id: "2606"
_4images_user_id: "144"
_4images_image_date: "2012-07-15T19:48:00"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35169 -->
Problem ist nur, das der gesamt lenge der Power-Motor 65,5m ist und die beide Bundhülsen zur anschlus, eben nicht 15mm aus ein ander sind, sonder <14mm. Der höhe ist mit 20mm aber kein problem.

Der gesamt lenge aber, werd dan eben 80,5mm.