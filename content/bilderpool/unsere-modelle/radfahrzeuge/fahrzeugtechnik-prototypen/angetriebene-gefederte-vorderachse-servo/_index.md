---
layout: "overview"
title: "Angetriebene, gefederte Vorderachse mit Differential und Standard-Servo"
date: 2019-12-17T18:44:56+01:00
legacy_id:
- categories/3117
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3117 --> 
Eine 45er-Statikstrebe wurde gemodded, um das Standard-Servo einfach anzubauen