---
layout: "image"
title: "Kupplung mit pneumatischem Ausrücklager von Porsche-Makus 03"
date: "2009-02-12T20:51:04"
picture: "porsche-makus_kupplung_03.jpg"
weight: "3"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- details/17366
imported:
- "2019"
_4images_image_id: "17366"
_4images_cat_id: "1562"
_4images_user_id: "327"
_4images_image_date: "2009-02-12T20:51:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17366 -->
