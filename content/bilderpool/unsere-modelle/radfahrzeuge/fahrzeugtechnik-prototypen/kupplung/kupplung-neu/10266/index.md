---
layout: "image"
title: "Übertragung auf Räder"
date: "2007-05-01T19:08:42"
picture: "kupplungneu3.jpg"
weight: "3"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/10266
imported:
- "2019"
_4images_image_id: "10266"
_4images_cat_id: "930"
_4images_user_id: "445"
_4images_image_date: "2007-05-01T19:08:42"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10266 -->
Wenn man das alte Differential nicht hat, kann man auch ein neues nehmen und etwas höher montieren und per Zahnrad übertragen (auf das "schräge" Zahnrad) .