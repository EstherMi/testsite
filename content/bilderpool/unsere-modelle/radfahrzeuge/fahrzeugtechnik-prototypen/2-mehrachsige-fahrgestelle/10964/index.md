---
layout: "image"
title: "Mulde gekippt 2"
date: "2007-06-29T17:59:36"
picture: "DSCN1376.jpg"
weight: "10"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/10964
imported:
- "2019"
_4images_image_id: "10964"
_4images_cat_id: "989"
_4images_user_id: "184"
_4images_image_date: "2007-06-29T17:59:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10964 -->
So ein Ding nennt sich "Motorjapaner"