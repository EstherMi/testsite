---
layout: "image"
title: "Gesamtansicht"
date: "2007-06-30T15:51:01"
picture: "DSCN1406.jpg"
weight: "14"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/10979
imported:
- "2019"
_4images_image_id: "10979"
_4images_cat_id: "992"
_4images_user_id: "184"
_4images_image_date: "2007-06-30T15:51:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10979 -->
