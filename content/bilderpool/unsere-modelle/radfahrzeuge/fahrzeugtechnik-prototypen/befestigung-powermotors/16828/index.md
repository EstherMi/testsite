---
layout: "image"
title: "Powermotor - Standard-Befestigung"
date: "2009-01-01T13:18:29"
picture: "bild1.jpg"
weight: "1"
konstrukteure: 
- "thkais"
fotografen:
- "thkais"
keywords: ["Powermotor", "Befestigung", "befestigen"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- details/16828
imported:
- "2019"
_4images_image_id: "16828"
_4images_cat_id: "1518"
_4images_user_id: "41"
_4images_image_date: "2009-01-01T13:18:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16828 -->
Die standardmäßige (in den Anleitungen gezeigte) Befestigung des Powermotors ist leider nicht so fest, wie man es sich wünschen würde. Bei intensivem Spielbetrieb kommt es immer wieder vor, dass der Powermotor sich aus der Befestigung herausschiebt und dadurch das Kegelzahnrad hörbar durchrutscht. Auf den folgenden Bildern möchte ich meine Lösung vorstellen, die mit originalen ft-Teilen auskommt und absolut bombenfest ist.
Auf diesem Bild zunächst die Standard-Befestigung via 2x 38240.