---
layout: "image"
title: "Baustufe 1"
date: "2009-01-01T13:18:31"
picture: "bild3.jpg"
weight: "3"
konstrukteure: 
- "thkais"
fotografen:
- "thkais"
keywords: ["Powermotor", "Befestigung", "befestigen"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- details/16830
imported:
- "2019"
_4images_image_id: "16830"
_4images_cat_id: "1518"
_4images_user_id: "41"
_4images_image_date: "2009-01-01T13:18:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16830 -->
Zunächst werden die beiden in der Bauanleitung Winkelsteine (38423) entfernt.