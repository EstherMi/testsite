---
layout: "image"
title: "Baustufe 3"
date: "2009-01-01T13:24:33"
picture: "bild5.jpg"
weight: "5"
konstrukteure: 
- "thkais"
fotografen:
- "thkais"
keywords: ["Powermotor", "Befestigung", "befestigen"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- details/16832
imported:
- "2019"
_4images_image_id: "16832"
_4images_cat_id: "1518"
_4images_user_id: "41"
_4images_image_date: "2009-01-01T13:24:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16832 -->
Nun kommt der wichtigste Teil: In die noch freie Nut der Motorplatte und die anliegende Nut des BS 15 wird ein Verbindungsstück 15 (31060) eingeschoben. Wenn man es bis zum Anschlag der Motorplatte einschiebt, überlappt es ungefähr zur Hälfte. Diese Verbindung verhindert, dass der Powermotor vom Differential weggedrückt wird und damit das Durchrutschen der Zahnräder.