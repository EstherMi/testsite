---
layout: "image"
title: "Getriebe 01"
date: "2015-05-25T11:43:53"
picture: "chassisfuergelaendewagen21.jpg"
weight: "24"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/41018
imported:
- "2019"
_4images_image_id: "41018"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:53"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41018 -->
Ja das Getriebe. Das hat mir die meisten Kopfzerbrechen bereitet. Große Übersetzung auf kleinem Raum war gefragt. Etliche Versuche mit Zahnrädern, Ketten und Gummiriemen später bin ich bei diesem Ergebnis gelandet.