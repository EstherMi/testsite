---
layout: "image"
title: "Getriebe 02"
date: "2015-05-25T11:43:53"
picture: "chassisfuergelaendewagen22.jpg"
weight: "25"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/41019
imported:
- "2019"
_4images_image_id: "41019"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:53"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41019 -->
Die Klemmkupplung verbindet die Motorwelle mit dem grauen Seilklemmstift. Auf diesem wird die Riegelscheibe von zwei Klemmbuchsen über einen Mitnehmer "mitgenommen". Leider ist der Mitnehmer auf dem Bild nicht gut zu sehen, er ist grade nach hinten gedreht. Die Riegelscheibe treibt das Zahnrad auf einer Achse 50 aus dem Winkelgetriebe an, dann erfolgt noch eine Übersetzung vom Z10 auf das Z20 des Differentials.