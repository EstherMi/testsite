---
layout: "image"
title: "Getriebe 10"
date: "2015-05-25T11:43:53"
picture: "chassisfuergelaendewagen30.jpg"
weight: "33"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/41027
imported:
- "2019"
_4images_image_id: "41027"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:53"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41027 -->
Die Statikstrebe mit der L-Lasche halten das Lager für das Mitteldifferentiel im Raster, es könnte sich sonst nach hinten wegschieben.