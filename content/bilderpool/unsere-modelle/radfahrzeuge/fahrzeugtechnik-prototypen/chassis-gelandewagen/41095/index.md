---
layout: "image"
title: "30 Grad Steigung 01"
date: "2015-05-30T15:35:02"
picture: "30_Grad_Steigung_01_klein.jpg"
weight: "1"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/41095
imported:
- "2019"
_4images_image_id: "41095"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-30T15:35:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41095 -->
Test mit 30°-Steigung