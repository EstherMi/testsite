---
layout: "image"
title: "Lenkrad 02"
date: "2015-05-25T11:43:53"
picture: "chassisfuergelaendewagen12.jpg"
weight: "15"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/41009
imported:
- "2019"
_4images_image_id: "41009"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:53"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41009 -->
Das Lenkrad steckt nur locker auf der Welle, weil es sonst nicht unter die Motorhaube passt. Die ist schon in Arbeit.