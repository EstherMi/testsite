---
layout: "image"
title: "Bodenfreiheit 03"
date: "2015-05-25T11:43:37"
picture: "chassisfuergelaendewagen07.jpg"
weight: "10"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/41004
imported:
- "2019"
_4images_image_id: "41004"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:37"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41004 -->
Ansicht von hinten. Man kann die Federung erahnen - und dass ordentlich PS drinstecken.