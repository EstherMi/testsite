---
layout: "image"
title: "Lenkrad 01"
date: "2015-05-25T11:43:53"
picture: "chassisfuergelaendewagen11.jpg"
weight: "14"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/41008
imported:
- "2019"
_4images_image_id: "41008"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:53"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41008 -->
Ein Faible von mir: Das Lenkrad dreht sich mit. Die Formschlüssigkeit zwischen der Riegelscheibe und dem Winkelzahnrad ist zwar nicht so toll, aber meistens gehts.