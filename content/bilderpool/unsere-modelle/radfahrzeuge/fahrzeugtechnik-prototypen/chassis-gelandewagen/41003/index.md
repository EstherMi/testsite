---
layout: "image"
title: "Bodenfreiheit 02"
date: "2015-05-25T11:43:37"
picture: "chassisfuergelaendewagen06.jpg"
weight: "9"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/41003
imported:
- "2019"
_4images_image_id: "41003"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:37"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41003 -->
Ansicht von vorne. Für mehr Bodenfreiheit wären noch Portalachsen super. Vielleicht beim nächsten Modell.