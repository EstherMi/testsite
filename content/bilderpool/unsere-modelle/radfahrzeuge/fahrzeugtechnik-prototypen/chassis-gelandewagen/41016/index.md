---
layout: "image"
title: "Hinterradschwinge 02"
date: "2015-05-25T11:43:53"
picture: "chassisfuergelaendewagen19.jpg"
weight: "22"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/41016
imported:
- "2019"
_4images_image_id: "41016"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:53"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41016 -->
Das Kettenglied stützt das Kegelzahnrad ein wenig, damit es nicht rattert.