---
layout: "image"
title: "Getriebe 16"
date: "2015-05-25T11:43:53"
picture: "chassisfuergelaendewagen36.jpg"
weight: "39"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/41033
imported:
- "2019"
_4images_image_id: "41033"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:53"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41033 -->
Der letzte Schritt, so bleibt fast nur noch das Antriebsmodul übrig.