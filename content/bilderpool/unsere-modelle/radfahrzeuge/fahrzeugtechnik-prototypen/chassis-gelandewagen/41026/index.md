---
layout: "image"
title: "Getriebe 09"
date: "2015-05-25T11:43:53"
picture: "chassisfuergelaendewagen29.jpg"
weight: "32"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/41026
imported:
- "2019"
_4images_image_id: "41026"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:53"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41026 -->
Die Getriebestufen ergeben eine Gesamtübersetzung von schätzungsweise 1:7 oder so. Dadurch fährt das Auto recht langsam, ist aber kräftig.