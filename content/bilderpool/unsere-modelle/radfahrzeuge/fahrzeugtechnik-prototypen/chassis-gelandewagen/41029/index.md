---
layout: "image"
title: "Getriebe 12"
date: "2015-05-25T11:43:53"
picture: "chassisfuergelaendewagen32.jpg"
weight: "35"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/41029
imported:
- "2019"
_4images_image_id: "41029"
_4images_cat_id: "3079"
_4images_user_id: "2321"
_4images_image_date: "2015-05-25T11:43:53"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41029 -->
Der Abdeckstein 3,75x30 gibt dem Ganzen mehr Stabilität und verhindert, dass sich der hier ausgebeute Winkelstein 60° nach vorne verschiebt.