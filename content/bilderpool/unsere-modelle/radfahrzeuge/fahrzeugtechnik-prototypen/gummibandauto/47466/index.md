---
layout: "image"
title: "Das Tetra-Pak Auto"
date: "2018-04-19T20:19:22"
picture: "gummibandauto2.jpg"
weight: "2"
konstrukteure: 
- "H.A.R.R.Y.s Nachwuchs"
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/47466
imported:
- "2019"
_4images_image_id: "47466"
_4images_cat_id: "3506"
_4images_user_id: "1557"
_4images_image_date: "2018-04-19T20:19:22"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47466 -->
Angespornt aus den Erfahrungen mit dem Prototyp hat der Nachwuchs mit Tatendrang das Modell hier aus vorhandenem Material zusammengestellt. Im Bild ist die noch schmucklose Rohkarosse zu sehen. Es rollt, es fährt aus eigenem Gummiantrieb etwa 4 bis 5 Meter weit und erfüllt die Vorgaben - auch wenn zu meinem Bedauern noch nicht mal ein einziges fischertechnik-Teil dran ist. Jammerschade. Einige Buben haben auf dem Pausenhof schon angekündigt etwas aus den sattsam bekannten dänischen Klötzchen bauen zu wollen um dann den Getränkekarton drüber zu stülpen, oder so.

Anekdote am Rande:
Papa: "Soll da nicht besser ein Elektromotor rein? ft-Getriebemotor, -räder und fertig?"
Nachwuchs: "Ach nee, Papa. Mit Motor will das die halbe Klasse bauen. Ich will was anderes machen. Und aus fischertechnik auch nicht. Die Anderen bauen schon Lego. Außerdem denken dann alle du hättest das gebaut."
Papa: ":(  Seufz" [Wenn der Nachwuchs sich etwas in den Kopf gesetzt hat, ...]

Nachtrag:
Es gab vom Lehrkörper mittendrin noch eine Regeländerung. Egal aus was gebaut, den Tetra-Pak nur überstülpen zählt nicht. Auch Lehrer sollten mal lernen erst nachzudenken und dann von Anfang an *klare* Spielregeln zu machen. Hat zwar das dänische Rennteam disqualifiziert, war aber trotzdem unfair. Mittlerweile ist der "Wettbewerb" vorbei und die Rennpappe hat ganz gut abgeschnitten.