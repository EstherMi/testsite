---
layout: "comment"
hidden: true
title: "2852"
date: "2007-03-30T22:53:24"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Doch, kann man (und hat man sicher schon): es ist ein normales Rast-Z10, dem ein Rastanschluss entfernt wurde. Das abgeschnittene Ende kann man weiterverwenden (etwa wie eine Klemmbuchse, aber nur am Ende einer Rastachse). Die Idee dazu stammt von Claus-Werner Ludwig.

Gruß,
Harald