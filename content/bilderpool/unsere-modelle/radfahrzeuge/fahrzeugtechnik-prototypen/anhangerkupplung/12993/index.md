---
layout: "image"
title: "Anhängerkuplung"
date: "2007-12-04T16:58:08"
picture: "anhaengerkuplung2.jpg"
weight: "2"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/12993
imported:
- "2019"
_4images_image_id: "12993"
_4images_cat_id: "1176"
_4images_user_id: "558"
_4images_image_date: "2007-12-04T16:58:08"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12993 -->
Man kann auch den plastikhaken durch eine zusätliche feder ersetzen