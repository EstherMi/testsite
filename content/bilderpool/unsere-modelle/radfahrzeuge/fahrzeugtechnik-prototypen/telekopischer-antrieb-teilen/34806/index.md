---
layout: "image"
title: "Ganz lang"
date: "2012-04-21T16:46:54"
picture: "laengsverschieblicherantrieb1.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/34806
imported:
- "2019"
_4images_image_id: "34806"
_4images_cat_id: "2511"
_4images_user_id: "104"
_4images_image_date: "2012-04-21T16:46:54"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34806 -->
Eine Abwandlung von Ludgers Vorschlag ist dies hier: Verwendet werden die Bauplatten 15 * 75 mit nur zwei Zapfen (an jedem Ende eines). Es geht auch mit den Bauplatten 15 * 45 mit zwei Zapfen, dann ist's halt kürzer. Hier ist der Verschiebeweg schön groß.