---
layout: "image"
title: "Doppelten Kardan mit längenausgleig"
date: "2012-01-15T19:08:53"
picture: "telekopischerantriebteilenmitlaengenausgleig2.jpg"
weight: "8"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/33934
imported:
- "2019"
_4images_image_id: "33934"
_4images_cat_id: "2511"
_4images_user_id: "144"
_4images_image_date: "2012-01-15T19:08:53"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33934 -->
Wurde man die 4 kleine Noken inerhalb der V- stein entfernen, taucht der Getriebeklaue, weiter ein.
Braucht man keine Gardangelenken kan man auch ein 31036: Achskupplung 40, benutsen. Oder ein gekurzten Getriebklaue 35467