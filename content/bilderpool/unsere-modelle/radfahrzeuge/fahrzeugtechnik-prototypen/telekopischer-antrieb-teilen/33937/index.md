---
layout: "image"
title: "Axialer längenausgleig"
date: "2012-01-15T19:08:53"
picture: "telekopischerantriebteilenmitlaengenausgleig5.jpg"
weight: "11"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/33937
imported:
- "2019"
_4images_image_id: "33937"
_4images_cat_id: "2511"
_4images_user_id: "144"
_4images_image_date: "2012-01-15T19:08:53"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33937 -->
32850: Riegelstein, gekurzt bis 7,5mm
36227: Rastadapter
38246: Bauplatte 15x15