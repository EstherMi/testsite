---
layout: "image"
title: "Noch ein Vorschlag"
date: "2012-04-21T14:16:21"
picture: "laengenverschieblicherantrieb1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/34805
imported:
- "2019"
_4images_image_id: "34805"
_4images_cat_id: "2511"
_4images_user_id: "104"
_4images_image_date: "2012-04-21T14:16:21"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34805 -->
So geht's auch, mit Licht und Schatten:
Plus: Langer Verschiebeweg, Achsen fluchten von alleine und können an den Enden also einfach mit Kardangelenken angebunden werden.
Minus: Bei den Übergängen der Klemmbuchsen hakt es mitunter ein wenig. Abhilfe könnte sein, bei je einem Zweig pro BS15 anstatt 3 lange (die älteren) Klemmringe je 2 kurze (an den Enden) und 2 lange (mittig) zu verwenden.