---
layout: "image"
title: "Axialer längenausgleig"
date: "2012-01-15T19:08:53"
picture: "telekopischerantriebteilenmitlaengenausgleig4.jpg"
weight: "10"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/33936
imported:
- "2019"
_4images_image_id: "33936"
_4images_cat_id: "2511"
_4images_user_id: "144"
_4images_image_date: "2012-01-15T19:08:53"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33936 -->
Der Bauplatte kan man erzetsen durch ein kombination aus volgenden teilen:
36227: Rastadapter
38226: Seilklemmstift 
oder
107356: Seilklemmstift 

Der Seilklemmstift wird an beide enden über eine länge von 4mm abgedreht bis auf 2mm dicke, und wird dan in der Rastadapter gesteckt