---
layout: "image"
title: "Nachbau RC-Geländewagen"
date: "2007-04-21T14:21:45"
picture: "rcgelaende1.jpg"
weight: "1"
konstrukteure: 
- "Niklas Frühauf"
fotografen:
- "Niklas Frühauf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fruehauf Jan Niklas"
license: "unknown"
legacy_id:
- details/10138
imported:
- "2019"
_4images_image_id: "10138"
_4images_cat_id: "916"
_4images_user_id: "557"
_4images_image_date: "2007-04-21T14:21:45"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10138 -->
Federung vorne mit Lenkung