---
layout: "image"
title: "Hinterachse von Oben"
date: "2014-06-07T15:21:00"
picture: "Foto_6.jpg"
weight: "13"
konstrukteure: 
- "Julian Bußmeier"
fotografen:
- "Julian Bußmeier"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "JulianBussmeier"
license: "unknown"
legacy_id:
- details/38914
imported:
- "2019"
_4images_image_id: "38914"
_4images_cat_id: "2910"
_4images_user_id: "1474"
_4images_image_date: "2014-06-07T15:21:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38914 -->
Der Antrieb erfolgt über 6 Z10 pro Seite.