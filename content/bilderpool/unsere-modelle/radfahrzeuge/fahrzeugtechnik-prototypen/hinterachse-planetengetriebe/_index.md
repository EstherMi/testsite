---
layout: "overview"
title: "Hinterachse mit Planetengetriebe"
date: 2019-12-17T18:44:42+01:00
legacy_id:
- categories/2910
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2910 --> 
Hallo, 
seit längerem experimentiere ich mit den großen Traktorreifen von Conrad Elektronik. Immer wieder tauchten folgende Probleme auf:
 
&#8226;	Dauerstandplatten 
&#8226;	Hohes Anfahrdrehmoment

Mein Lösungsvorschlag: