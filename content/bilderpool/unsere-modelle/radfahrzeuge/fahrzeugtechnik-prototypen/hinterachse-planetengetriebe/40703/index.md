---
layout: "image"
title: "Federung"
date: "2015-04-03T10:00:06"
picture: "Foto_10.jpg"
weight: "2"
konstrukteure: 
- "Julian Bußmeier"
fotografen:
- "Julian Bußmeier"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "JulianBussmeier"
license: "unknown"
legacy_id:
- details/40703
imported:
- "2019"
_4images_image_id: "40703"
_4images_cat_id: "2910"
_4images_user_id: "1474"
_4images_image_date: "2015-04-03T10:00:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40703 -->
Die Blattfeder zwischen Achse und Achsaufhängung hat keine ausreichende Federwirkung.