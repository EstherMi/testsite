---
layout: "image"
title: "Hinterachse mit Planetengetriebe"
date: "2014-06-07T15:21:00"
picture: "Foto_8.jpg"
weight: "11"
konstrukteure: 
- "Julian Bußmeier"
fotografen:
- "Julian Bußmeier"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "JulianBussmeier"
license: "unknown"
legacy_id:
- details/38912
imported:
- "2019"
_4images_image_id: "38912"
_4images_cat_id: "2910"
_4images_user_id: "1474"
_4images_image_date: "2014-06-07T15:21:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38912 -->
Durch das Verteilen des Drehmoments auf mehre Zahnräder entsteht eine gleichmäßige Kraftübertragung. Dafür habe ich einen sehr massiven &#8222;Käfig&#8220; konstruiert und modifizierte Teile verwendet.