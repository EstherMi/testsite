---
layout: "image"
title: "Hinterachse von Unten"
date: "2014-06-07T15:21:00"
picture: "Foto_7.jpg"
weight: "14"
konstrukteure: 
- "Julian Bußmeier"
fotografen:
- "Julian Bußmeier"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "JulianBussmeier"
license: "unknown"
legacy_id:
- details/38915
imported:
- "2019"
_4images_image_id: "38915"
_4images_cat_id: "2910"
_4images_user_id: "1474"
_4images_image_date: "2014-06-07T15:21:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38915 -->
Das rote Z10 ist freilaufend auf der Achse montiert. 
Um ein Herauslösen zu verhindern, wurde die Achse zweifach gelagert und mit 6 Muttern gesichert.