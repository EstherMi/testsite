---
layout: "image"
title: "Rückansicht"
date: "2015-04-03T10:00:06"
picture: "Foto_16.jpg"
weight: "8"
konstrukteure: 
- "Julian Bußmeier"
fotografen:
- "Julian Bußmeier"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "JulianBussmeier"
license: "unknown"
legacy_id:
- details/40709
imported:
- "2019"
_4images_image_id: "40709"
_4images_cat_id: "2910"
_4images_user_id: "1474"
_4images_image_date: "2015-04-03T10:00:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40709 -->
Die schlanke und kompakte Konstruktion der Achse, vereinfacht den Einbau in ein Fahrzeug.