---
layout: "image"
title: "22"
date: "2008-03-22T20:56:13"
picture: "22.jpg"
weight: "21"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- details/14029
imported:
- "2019"
_4images_image_id: "14029"
_4images_cat_id: "1139"
_4images_user_id: "327"
_4images_image_date: "2008-03-22T20:56:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14029 -->
Hier habe ich zusätzlich versucht, einen Antrieb der einzeln aufgehängten Räder zu realisieren. Aber ich komme da momentan nicht weiter:
1. Wie soll ich da noch das Differential unterbringen? Klar wäre es am einfachsten, zwei Motoren direkt dahinter zu platzieren, aber das ist mir irgendwie zu simpel.
2. Die Rastachsen an den Aussenseiten schauen ja nur noch ein paar mm raus. Wie soll ich da noch ein Rad befestigen?