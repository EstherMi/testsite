---
layout: "image"
title: "Sperrbares Zentraldifferenzial"
date: "2007-02-25T18:59:45"
picture: "Sperrbares_Zentraldifferenzial5.jpg"
weight: "5"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/9152
imported:
- "2019"
_4images_image_id: "9152"
_4images_cat_id: "844"
_4images_user_id: "456"
_4images_image_date: "2007-02-25T18:59:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9152 -->
Hier ist der Mini-Motor für die Sperrung zu sehen.