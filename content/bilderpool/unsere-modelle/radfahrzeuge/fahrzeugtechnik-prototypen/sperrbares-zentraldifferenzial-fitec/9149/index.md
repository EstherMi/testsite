---
layout: "image"
title: "Sperrbares Zentraldifferenzial"
date: "2007-02-25T18:59:44"
picture: "Sperrbares_Zentraldifferenzial2.jpg"
weight: "2"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/9149
imported:
- "2019"
_4images_image_id: "9149"
_4images_cat_id: "844"
_4images_user_id: "456"
_4images_image_date: "2007-02-25T18:59:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9149 -->
Von unten. Man sieht das Differenzial und das Z20.