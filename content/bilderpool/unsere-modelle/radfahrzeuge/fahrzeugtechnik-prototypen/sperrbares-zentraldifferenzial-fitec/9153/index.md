---
layout: "image"
title: "Sperrbares Zentraldifferenzial"
date: "2007-02-25T18:59:45"
picture: "Sperrbares_Zentraldifferenzial6.jpg"
weight: "6"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/9153
imported:
- "2019"
_4images_image_id: "9153"
_4images_cat_id: "844"
_4images_user_id: "456"
_4images_image_date: "2007-02-25T18:59:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9153 -->
Hier von unten. Man sieht die Aufhängung des Power-Motor, der sich ja auch mitbewegt.