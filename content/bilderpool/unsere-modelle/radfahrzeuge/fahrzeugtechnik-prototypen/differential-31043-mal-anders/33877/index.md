---
layout: "image"
title: "Mitnehmer"
date: "2012-01-09T16:34:50"
picture: "DSCN4230.jpg"
weight: "4"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/33877
imported:
- "2019"
_4images_image_id: "33877"
_4images_cat_id: "2506"
_4images_user_id: "184"
_4images_image_date: "2012-01-09T16:34:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33877 -->
Der S-Riegel hakt sich zwischen die Zähne. Er wird durch die M-Achse gehalten.