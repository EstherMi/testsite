---
layout: "image"
title: "Differential mit Z40"
date: "2012-01-09T16:34:49"
picture: "DSCN4225.jpg"
weight: "2"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/33875
imported:
- "2019"
_4images_image_id: "33875"
_4images_cat_id: "2506"
_4images_user_id: "184"
_4images_image_date: "2012-01-09T16:34:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33875 -->
Ich habe hier versucht das alte Diff so einzusetzen das auch ein Kettenantrieb möglich ist.