---
layout: "image"
title: "Schwimmsattelbremse 03"
date: "2009-02-19T21:15:15"
picture: "Porsche-Makus_Schwimmsattelbremse_04.jpg"
weight: "3"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- details/17449
imported:
- "2019"
_4images_image_id: "17449"
_4images_cat_id: "1569"
_4images_user_id: "327"
_4images_image_date: "2009-02-19T21:15:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17449 -->
