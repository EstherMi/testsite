---
layout: "comment"
hidden: true
title: "8578"
date: "2009-02-22T20:29:41"
uploadBy:
- "Porsche-Makus"
license: "unknown"
imported:
- "2019"
---
hi fitec,

solche betätiger sind fast schon "heilig" und deshalb wäre es das letzte, was ich zulassen würde, dass sich der betätiger in irgendeiner art und weise abnutzt!

die einzige stelle, an der reibung und damit abnutzung entsteht, ist die rückseite. will man da absolut sicher gehen, schiebt man einfach eine 1er bauplatte drauf und gut ist.

dann wird der betätiger selbst nirgends mehr beansprucht und es nutzt sich auch nichts ab!