---
layout: "comment"
hidden: true
title: "8568"
date: "2009-02-20T22:46:16"
uploadBy:
- "Porsche-Makus"
license: "unknown"
imported:
- "2019"
---
Bisher bin ich doch immer mit unmodifizierten Bauteilen ausgekommen, wobei ich im Grunde kein Problem damit habe, die Säge anzusetzen. Aber wenn's nicht unbedingt sein muss, dann lieber ohne Customizing.

Ich mag am liebsten Lösungen, die Kombinationen von FT-Teilen verwenden, die so nie vorgesehen waren.