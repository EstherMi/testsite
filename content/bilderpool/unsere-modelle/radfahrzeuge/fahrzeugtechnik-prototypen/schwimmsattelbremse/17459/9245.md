---
layout: "comment"
hidden: true
title: "9245"
date: "2009-05-11T16:25:01"
uploadBy:
- "laserman"
license: "unknown"
imported:
- "2019"
---
Hallo Marius,
da ist eine schwarze Gummimembran drin, die sich aufbläht, wenn man Druck drauf gibt. Dann kann entweder die Bremse bremsen. Oder früher (eigentlich auch heute noch) kann man damit Pneumatik-Ventile betätigen.
Wenn DU mehr zu diesem Bauteil erfahren willst, schau mal hier:
http://ftcommunity.de/data/downloads/bauanleitungen/pneumatik/festo_pneumatik.pdf
Das ist der alte Pneumatik Kasten.
Viele Grüße, Andreas.