---
layout: "comment"
hidden: true
title: "9246"
date: "2009-05-11T16:26:05"
uploadBy:
- "zeuz"
license: "unknown"
imported:
- "2019"
---
Hallo Marius,
das ist ein Betaetiger aus dem alten Pneumatik Programm: 36075. Damit kann man zB Taster ueber Pneumatik Betaetigen. Hier ist er dazu eingesetzt die Bremse zu aktivieren.

Gruss, Norbert