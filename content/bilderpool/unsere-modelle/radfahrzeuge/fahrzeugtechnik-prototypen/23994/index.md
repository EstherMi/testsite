---
layout: "image"
title: "Zwillingsachse63.JPG"
date: "2009-05-10T16:00:55"
picture: "Zwillingsachse63.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/23994
imported:
- "2019"
_4images_image_id: "23994"
_4images_cat_id: "297"
_4images_user_id: "4"
_4images_image_date: "2009-05-10T16:00:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23994 -->
Eine begrenzt geländegängige Zwillings-Hinterachse für LKW und dergleichen. Die Räder sitzen auf starren ft-Achsen und werden über das Rastkegelzahnrad angetrieben.