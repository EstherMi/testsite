---
layout: "image"
title: "Lenkeinschlag"
date: "2008-02-03T20:45:25"
picture: "lenkeinschlag.jpg"
weight: "3"
konstrukteure: 
- "Reus"
fotografen:
- "Reus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Reus"
license: "unknown"
legacy_id:
- details/13513
imported:
- "2019"
_4images_image_id: "13513"
_4images_cat_id: "1238"
_4images_user_id: "708"
_4images_image_date: "2008-02-03T20:45:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13513 -->
Hier hab ich das Lenkrad nach links gedreht. Dadurch wird der rechte Minitaster gedrückt und der Motor dreht aktiv die Lenkung so lange nach links, bis der Taster wieder freigegeben ist. Fühlt sich ganz witzig an.