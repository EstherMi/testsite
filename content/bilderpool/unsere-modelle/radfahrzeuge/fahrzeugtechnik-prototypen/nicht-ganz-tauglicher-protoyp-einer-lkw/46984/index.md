---
layout: "image"
title: "Blick von unten"
date: "2017-12-12T12:59:49"
picture: "nichtganztauglicherprotoypeinerlkwvorderachse5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46984
imported:
- "2019"
_4images_image_id: "46984"
_4images_cat_id: "3477"
_4images_user_id: "104"
_4images_image_date: "2017-12-12T12:59:49"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46984 -->
Die Unterseite wäre brauchbar glatt und flach.