---
layout: "image"
title: "Radaufhängung"
date: "2017-12-12T12:59:49"
picture: "nichtganztauglicherprotoypeinerlkwvorderachse6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46985
imported:
- "2019"
_4images_image_id: "46985"
_4images_cat_id: "3477"
_4images_user_id: "104"
_4images_image_date: "2017-12-12T12:59:49"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46985 -->
In den unteren BS15 mit Bohrung wird das Rad an einer Metallachse aufgehängt und auf der innenseite mit mehreren Klemmringen gesichert.