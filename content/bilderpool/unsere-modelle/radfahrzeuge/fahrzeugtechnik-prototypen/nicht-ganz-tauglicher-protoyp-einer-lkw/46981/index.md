---
layout: "image"
title: "Federung (1)"
date: "2017-12-12T12:59:49"
picture: "nichtganztauglicherprotoypeinerlkwvorderachse2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46981
imported:
- "2019"
_4images_image_id: "46981"
_4images_cat_id: "3477"
_4images_user_id: "104"
_4images_image_date: "2017-12-12T12:59:49"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46981 -->
Ein Detailblick, wie die Federung funktionieren sollte, es aber eben nicht tut. Der untere Ansatzpunkt (die schwarzen Rollen) müsste weit draußen liegen - da soll ja aber das Rad auf und ab federn.