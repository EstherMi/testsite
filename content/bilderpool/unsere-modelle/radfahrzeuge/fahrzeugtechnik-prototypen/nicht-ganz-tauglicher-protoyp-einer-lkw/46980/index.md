---
layout: "image"
title: "Gesamtansicht"
date: "2017-12-12T12:59:49"
picture: "nichtganztauglicherprotoypeinerlkwvorderachse1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46980
imported:
- "2019"
_4images_image_id: "46980"
_4images_cat_id: "3477"
_4images_user_id: "104"
_4images_image_date: "2017-12-12T12:59:49"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46980 -->
Die Radaufhängung ist eine vergrößerte Fassung von https://www.ftcommunity.de/details.php?image_id=40830

Der Antrieb erfolgt über die zwei (außer über das Differential nirgends verbundenen) Ketten auf Rastkegelzahnräder, die am oberen Innenrand der großen Reifen reiben. Damit das klappt, muss einer der Reifen vom Profil gesehen her verkehrherum montiert werden, weil die offenbar nicht ganz symmetrisch sind. Wenn das Fahrzeug sein Eigengewicht auf den Reifen trägt, drücken die sehr brauchbar gegen die Rastkegelräder. Der Antrieb ist dann recht kräftig.

Durch Absenken von zwei Zahnrädern auf einer gemeinsamen Achse auf die Kette könnte man eine DIfferenzialsperre realisieren.

Das Lenkgestänge selbst fehlt noch.

Die Federung sollte über aufwickelbare Gummis a) butterweich sein und b) eine Niverauregulierung bekommen. Oben zwischen die BS7,5 passen zwei S-Motoren, die per Schnecke an den vorderen Z20 drehen, um die Gummis auf den längs liegenden Metallachsen aufzuwickeln.

Das Problem ist, dass die Geometrie der Gummizüge überhaupt nichts taugt. Wenn man anzieht, macht es irgendwann "flupp",  und das Fahrwerk steht sozusagen auf Stelzen. Lässt man wieder Gummi nach, macht es irgendwann "plopp" und das Fahrwerk fällg ganz ab. Die Federung muss ganz anders gebaut werden, deshalb ist das in dieser Form hier erstmal zu den Akten gelegt.