---
layout: "image"
title: "Frontalansicht"
date: "2009-09-19T21:24:11"
picture: "lenkungdurchgewichtsverlagerung9.jpg"
weight: "9"
konstrukteure: 
- "Bernd Scheurer"
fotografen:
- "Bernd Scheurer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "be-sign,net"
license: "unknown"
legacy_id:
- details/24938
imported:
- "2019"
_4images_image_id: "24938"
_4images_cat_id: "1719"
_4images_user_id: "808"
_4images_image_date: "2009-09-19T21:24:11"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24938 -->
Starke Perspektive!