---
layout: "image"
title: "Links"
date: "2009-09-19T21:24:10"
picture: "lenkungdurchgewichtsverlagerung2.jpg"
weight: "2"
konstrukteure: 
- "Bernd Scheurer"
fotografen:
- "Bernd Scheurer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "be-sign,net"
license: "unknown"
legacy_id:
- details/24931
imported:
- "2019"
_4images_image_id: "24931"
_4images_cat_id: "1719"
_4images_user_id: "808"
_4images_image_date: "2009-09-19T21:24:10"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24931 -->
Durch eine Gewichtsverlagerung nach rechtswird das Fahrzeug nach links glenkt.