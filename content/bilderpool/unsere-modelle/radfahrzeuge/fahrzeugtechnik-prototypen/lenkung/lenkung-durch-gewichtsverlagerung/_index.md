---
layout: "overview"
title: "Lenkung durch Gewichtsverlagerung"
date: 2019-12-17T18:42:42+01:00
legacy_id:
- categories/1719
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1719 --> 
Die Lenkung dieses Fahrzeuges funktioniert durch bloßes Verlagern des Gewichtes nach links oder rechts. Um trotz der Fahrzeugneigung den Bodenkontakt der Räder sicherzustellen, sind alle Teile beweglich miteinander verbunden.