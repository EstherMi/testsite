---
layout: "comment"
hidden: true
title: "9367"
date: "2009-06-01T11:46:56"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Ich habe ein Fischertechnik-ABS-Antiblockiersystem-Modell gebaut und ein Robopro-Programm ABS-2 geuploaded im FT-Community-downloads, aber es ist noch nicht freigeschaltet.
 
Mit eine Auslauf-Schleife (lokale Variable) über Geschwindigkeit M2 is das Modell etwas realistischer.

In Praxis ist alles doch nicht so einfach, da auch der Brems(Luft)druck dosiert wird. 

Gruss, 
 
Peter Damen
Poederoyen NL