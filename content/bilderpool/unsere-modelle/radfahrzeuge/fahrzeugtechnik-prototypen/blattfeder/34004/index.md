---
layout: "image"
title: "Vorlage"
date: "2012-01-24T11:51:12"
picture: "315_Feder_2.jpg"
weight: "6"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/34004
imported:
- "2019"
_4images_image_id: "34004"
_4images_cat_id: "2518"
_4images_user_id: "184"
_4images_image_date: "2012-01-24T11:51:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34004 -->
Hier kann man sehen das die Blattfeder an zwei Punkten (durch die Bügel) mit der Achse verbunden ist.
Lediglich geht bei dem Modell noch die Rastachse hindurch.
Am hinteren Ende der Feder sieht man das sie hier auch untereinander verbunden sind.

Gruß ludger