---
layout: "image"
title: "Detail"
date: "2012-01-23T14:53:33"
picture: "Blattfeder_2.jpg"
weight: "10"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/33994
imported:
- "2019"
_4images_image_id: "33994"
_4images_cat_id: "2518"
_4images_user_id: "184"
_4images_image_date: "2012-01-23T14:53:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33994 -->
