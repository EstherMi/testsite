---
layout: "comment"
hidden: true
title: "16322"
date: "2012-02-10T08:30:44"
uploadBy:
- "ludger-ftc"
license: "unknown"
imported:
- "2019"
---
Hallo,

wenn ich die seitliche Befestigung entferne wackelt die Achse. Nur die mittlere Achse (mit Bauplatte 15*15) ist nicht in der Lage alles stabil zu halten. Oder ich muß, wie bei der ersten Version, wieder eine Strebe zum stützen verwenden. Ich sehe das einfach so das diese Befestigung den Ringen entspricht die im Original am Federende sitzen.
Ob es ein Modell wird ..... Im Augenblick sind es nur Studien.....

Gruß ludger