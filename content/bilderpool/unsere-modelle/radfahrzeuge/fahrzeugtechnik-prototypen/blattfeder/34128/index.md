---
layout: "image"
title: "überarbeitet"
date: "2012-02-09T13:12:13"
picture: "Blattfeder_01.jpg"
weight: "1"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/34128
imported:
- "2019"
_4images_image_id: "34128"
_4images_cat_id: "2518"
_4images_user_id: "184"
_4images_image_date: "2012-02-09T13:12:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34128 -->
eine neue Version.
Der Aufbau ist wesentlich dezenter als vorher. Die roten BS15 musste ich gegen schwarze tauschen. Werden zwei rote BS15 nebeneinander nur auf einer Seite zusammengehalten, läuft die Achse nicht richtig. Bei den schwarzen muss die größere Bohrung außen sein, dann läuft es gut.