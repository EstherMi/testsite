---
layout: "image"
title: "Blick von unten"
date: "2012-01-23T14:53:33"
picture: "Blattfeder_3.jpg"
weight: "11"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/33995
imported:
- "2019"
_4images_image_id: "33995"
_4images_cat_id: "2518"
_4images_user_id: "184"
_4images_image_date: "2012-01-23T14:53:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33995 -->
aufgebaut ist es so:
2 BS 15 mit Loch, auf den äußeren eine Rastachse mit Bauplatte. Diese durch 5 Streben gesteckt die lose miteinander verbunden sind. fertig....
Die schräg liegenden Streben sind nötig damit das Ganze etwas stabiler wird.