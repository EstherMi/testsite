---
layout: "image"
title: "Gesamtansicht"
date: "2014-01-17T15:27:08"
picture: "Gesamtansicht_Reifen_Planetengetriebe_Aufhngung.jpg"
weight: "2"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38078
imported:
- "2019"
_4images_image_id: "38078"
_4images_cat_id: "2832"
_4images_user_id: "1729"
_4images_image_date: "2014-01-17T15:27:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38078 -->
Hier sieht man, wie wenig das Getriebe auffällt. Der Planetenträger ist eigentlich ja schon die Radaufhängung, so daß kein zusätzlicher Platz benötigt wird.
(die grauen Bausteine sind nur als Stütze zum fotographieren und haben ansonsten keine Bedeutung)