---
layout: "image"
title: "Detailansicht"
date: "2014-01-17T15:27:08"
picture: "Detailansicht_Planetengetriebe.jpg"
weight: "3"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38079
imported:
- "2019"
_4images_image_id: "38079"
_4images_cat_id: "2832"
_4images_user_id: "1729"
_4images_image_date: "2014-01-17T15:27:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38079 -->
Hier sieht man die Planetenzahnräder Z10. Die roten stammen aus alten ft Getrieben. 
Das mittlere schwarze Z10 geht leider im Schatten unter