---
layout: "image"
title: "5"
date: "2010-07-05T16:39:58"
picture: "solarflitzer5.jpg"
weight: "5"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27682
imported:
- "2019"
_4images_image_id: "27682"
_4images_cat_id: "1992"
_4images_user_id: "1082"
_4images_image_date: "2010-07-05T16:39:58"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27682 -->
Hier einmal von unten.