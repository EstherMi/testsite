---
layout: "image"
title: "7"
date: "2010-07-05T16:39:58"
picture: "solarflitzer7.jpg"
weight: "7"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27684
imported:
- "2019"
_4images_image_id: "27684"
_4images_cat_id: "1992"
_4images_user_id: "1082"
_4images_image_date: "2010-07-05T16:39:58"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27684 -->
Noch einmal die Übertragung der Kraft auf die Achse. Der Motor dreht sich sehr schnell, deshalb die starke untersetzung.