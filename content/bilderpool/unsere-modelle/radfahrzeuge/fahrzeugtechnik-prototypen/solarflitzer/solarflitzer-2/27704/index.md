---
layout: "image"
title: "14"
date: "2010-07-06T17:38:34"
picture: "solarflitzer5.jpg"
weight: "5"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27704
imported:
- "2019"
_4images_image_id: "27704"
_4images_cat_id: "1994"
_4images_user_id: "1082"
_4images_image_date: "2010-07-06T17:38:34"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27704 -->
Nochmal das Schneckengetriebe.