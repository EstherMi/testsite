---
layout: "image"
title: "11"
date: "2010-07-06T17:38:34"
picture: "solarflitzer2.jpg"
weight: "2"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27701
imported:
- "2019"
_4images_image_id: "27701"
_4images_cat_id: "1994"
_4images_user_id: "1082"
_4images_image_date: "2010-07-06T17:38:34"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27701 -->
Da die Solarzellen in Reihe geschaltet sind, reicht es eine abzudecken, dann bleibt der Solarflitzer stehen.