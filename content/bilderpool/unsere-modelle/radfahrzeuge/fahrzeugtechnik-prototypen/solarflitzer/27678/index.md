---
layout: "image"
title: "1"
date: "2010-07-05T16:39:58"
picture: "solarflitzer1.jpg"
weight: "1"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27678
imported:
- "2019"
_4images_image_id: "27678"
_4images_cat_id: "1992"
_4images_user_id: "1082"
_4images_image_date: "2010-07-05T16:39:58"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27678 -->
So fährt das Auto nicht. Da alle Solarzellen in Reihe geschaltet sind, hält das Auto an, sobald man eine zuhält. Bei voller Sonne kriegt es ungefähr 8 km/h drauf. 
Video gibt es hier: http://www.youtube.com/watch?v=DmT5zi2ezqQ