---
layout: "image"
title: "Konstruktionshilfe"
date: "2014-04-27T20:22:40"
picture: "lenkungconceptcar10.jpg"
weight: "11"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38724
imported:
- "2019"
_4images_image_id: "38724"
_4images_cat_id: "2890"
_4images_user_id: "1729"
_4images_image_date: "2014-04-27T20:22:40"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38724 -->
Hier seht Ihr, wie ich eine Lenkung konstruiere bzw. überprüfe.
Zuerst baue ich meistens die Vorderachse mit der Lenkkinematik.Dann getrennt davon eine Hinterachse.
Die Lenkung muß natürlich zuerst auf guten Geradeauslauf überprüft werden. Die Räder müssen in "0"-Stellung exakt parallel sein 
Dann stelle ich den maximalen Lenkeinschlag ein und verlängere die Achsen mit Stangen oder ähnlichem (wird alles nur auf den Boden gelegt).
Diese Linien müssen sich in einem Punkt schneiden und zwar genau in der Flucht der Hinterachse.
Nur dann gibt es ein sauberes Kurvenverhalten. Ein 4-Rad Fahrzeug lenkt ja eigentlich über die Hinterachse. Passt die Geometrie nicht, dann wird es Probleme beim Lenken geben (Rubbeln der Reifen, Schlechtes Lenkverhalten, hohe Lenkkräfte....)

Meistens muß man an den Lenkhebeln ein bisschen ändern, bis alles passt und man einen sauberen Schnittpunkt hat. Schliesslich habe ich über dieses Verfahren den optimalen Achsabstand ermittelt und kann dann das Fahrzeug (die beiden Achsen) zusammenbauen.