---
layout: "image"
title: "Teile für die Lenkung"
date: "2014-04-27T20:22:40"
picture: "lenkungconceptcar01.jpg"
weight: "2"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38715
imported:
- "2019"
_4images_image_id: "38715"
_4images_cat_id: "2890"
_4images_user_id: "1729"
_4images_image_date: "2014-04-27T20:22:40"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38715 -->
Die Idee basiert auf der Verwendung von Schneckenmuttern mit Kugellagern.
Dieser einfache Aufbau ist nur möglich, weil die Teile eine Presspassung untereinander haben und die Schneckenmutter einen runden Zapfen hat.
Also im Detail:
- Die dunkelrote Schneckenmutter stellt den Lenkkopf dar. Ich habe 2 Kugellager rechts und links eingepresst, damit die Achse eine gute Führung hat. Das Rad wird mit einem Hexadapter befestigt, auf der inneren Seite wird die Achse mit einem Stellring fixiert.
- Ein weiteres Kugellager wird auf den runden Zapfen der dunkelroten Schneckenmutter gepresst und dann in eine weitere Schneckenmutter eingepresst.

Übrigens presse ich die Kugellager immer mit Hilfe des Schraubstockes ein, dann ist immer alles gerade zueinander und die Lager verkanten nicht.

Falls es jemand nachbauen will: Alle Metallteile (Kugellager und Alu-Teile) könnt Ihr bei fischerfriendsman bestellen!