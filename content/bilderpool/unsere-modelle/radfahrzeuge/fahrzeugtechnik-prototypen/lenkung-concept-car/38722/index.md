---
layout: "image"
title: "Von Unten"
date: "2014-04-27T20:22:40"
picture: "lenkungconceptcar08.jpg"
weight: "9"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38722
imported:
- "2019"
_4images_image_id: "38722"
_4images_cat_id: "2890"
_4images_user_id: "1729"
_4images_image_date: "2014-04-27T20:22:40"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38722 -->
Alles schön aufgeräumt. Alle Kabel liegen unter dem Batteriefach. Je nach Schalterstellung kann man vorwärts und rückwärts fahren.