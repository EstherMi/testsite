---
layout: "image"
title: "Heckansicht"
date: "2014-04-27T20:22:40"
picture: "lenkungconceptcar06.jpg"
weight: "7"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38720
imported:
- "2019"
_4images_image_id: "38720"
_4images_cat_id: "2890"
_4images_user_id: "1729"
_4images_image_date: "2014-04-27T20:22:40"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38720 -->
Hier sieht man das Antriebskonzept am Besten!
Das ist eigentlich auch extrem simpel aufgebaut!
Das Differential wird von 2 S-Motoren angetrieben. Das ganze Modell ist ja nicht besonders schwer und fährt daher recht zügig!
Die Motoren liegen schräg und sind genau so justiert, daß die Z10 Rastritzel in das Differential greifen.
Durch die Lage der Motoren sind diese schön in das Modell integriert und haben sogar einen gestalterischen Effekt. Die schräge Heckpartie erinnert mich an einen Humvee.
Den Kraftschluß zwischen Z10 und Differential müsste man allerdings noch etwas verbessern. Unter starker Belastung springen die Z10 über die Zähne des Differential. Dafür ist mir aber noch keine optisch schöne Lösung gelungen.