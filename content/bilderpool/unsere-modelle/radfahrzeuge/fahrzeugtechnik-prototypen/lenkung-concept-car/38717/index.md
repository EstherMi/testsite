---
layout: "image"
title: "Fahrzeug komplett"
date: "2014-04-27T20:22:40"
picture: "lenkungconceptcar03.jpg"
weight: "4"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38717
imported:
- "2019"
_4images_image_id: "38717"
_4images_cat_id: "2890"
_4images_user_id: "1729"
_4images_image_date: "2014-04-27T20:22:40"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38717 -->
wenn man mal anfängt, macht man meistens gleich weiter...
Ein paar Teile zur Lenkung dazu, und fertig ist ein komplettes Fahrzeug-Chassis.
Karosserie hab ich keine gemacht, ich bin mir noch nicht sicher, was für ein Fahrzeug ich daraus machen könnte.
Das Chassis ist minimalistisch aufgebaut, passend zum Maßstab der Reifen.
Es ist fahrbereit! Es hat eine 9V-Batterie, 2 Motoren, Differential, Ein/Aus Schalter.
Nur Empfänger und Servo hab ich nicht eingebaut (sonst müsste ich ein anderes Modell auseinander reißen).
Aber vorne auf dem Träger müsste für beides eigentlich mehr als genug Platz sein!
