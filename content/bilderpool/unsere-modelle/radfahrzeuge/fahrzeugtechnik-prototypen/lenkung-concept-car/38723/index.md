---
layout: "image"
title: "Räder abmontiert"
date: "2014-04-27T20:22:40"
picture: "lenkungconceptcar09.jpg"
weight: "10"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38723
imported:
- "2019"
_4images_image_id: "38723"
_4images_cat_id: "2890"
_4images_user_id: "1729"
_4images_image_date: "2014-04-27T20:22:40"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38723 -->
ohne Räder hat man einen guten Blick auf die eigentlich simple Technik
