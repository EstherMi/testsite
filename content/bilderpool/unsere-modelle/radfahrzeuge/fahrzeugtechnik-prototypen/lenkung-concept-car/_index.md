---
layout: "overview"
title: "Lenkung Concept Car"
date: 2019-12-17T18:44:39+01:00
legacy_id:
- categories/2890
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2890 --> 
Mir kam über das Wochenende eine neue Idee für den Aufbau einer Lenkung. 
Die Lenkung kann mit ganz wenigen Teilen aufgebaut werden, ist aber trotzdem ziemlich stabil und erlaubt extreme Lenkeinschläge.
Ich wollte eigentlich nur diese Art der Lenkung ausprobieren, am Ende kam dann ein kleiner Chassis-Prototyp raus.
Ich bin noch nicht sicher, ob ich daran weiterbaue und was ich daraus machen könnte.
Vielleicht einen Fun-Car, einen Strand-Buggy oder ein kleines Renn-Auto.
Auf jeden Fall ist der Flitzer extrem wendig.
Ich werde auch ein kleines Video bereitstellen, um den extremen Wendekreis zu demonstrieren.