---
layout: "image"
title: "Schwenkachse unten"
date: "2018-10-31T19:15:46"
picture: "chassisfuergelaendewagenii15.jpg"
weight: "15"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/48336
imported:
- "2019"
_4images_image_id: "48336"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:46"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48336 -->
Hier noch mal die Schwekachse in Aktion.