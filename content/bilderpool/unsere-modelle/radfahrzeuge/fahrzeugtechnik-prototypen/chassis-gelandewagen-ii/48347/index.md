---
layout: "image"
title: "Vorderachse 3"
date: "2018-10-31T19:15:47"
picture: "chassisfuergelaendewagenii26.jpg"
weight: "26"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/48347
imported:
- "2019"
_4images_image_id: "48347"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:47"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48347 -->
Hier sieht man den Platz zum Einschlagen der Räder.