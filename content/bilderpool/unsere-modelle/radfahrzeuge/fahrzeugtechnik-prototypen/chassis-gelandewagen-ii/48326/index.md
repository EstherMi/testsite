---
layout: "image"
title: "Bettengebirge 3"
date: "2018-10-31T19:15:46"
picture: "chassisfuergelaendewagenii05.jpg"
weight: "5"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/48326
imported:
- "2019"
_4images_image_id: "48326"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:46"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48326 -->
Dabei ist die Schwenkachse hinten sehr nützlich.