---
layout: "image"
title: "Ansicht unten links"
date: "2018-10-31T19:15:46"
picture: "chassisfuergelaendewagenii14.jpg"
weight: "14"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/48335
imported:
- "2019"
_4images_image_id: "48335"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:46"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48335 -->
Das Anbauteil (Adapter/Querlager) für die alten Motoren eignet sich hier gut, um die Batteriehalter hinten zusammen zu halten.