---
layout: "image"
title: "Ansicht hinten links"
date: "2018-10-31T19:15:46"
picture: "chassisfuergelaendewagenii09.jpg"
weight: "9"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/48330
imported:
- "2019"
_4images_image_id: "48330"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:46"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48330 -->
Die Stromversorgung übernehmen zwei parallel geschaltete 9V-Akkus.
So beim verarbeiten der Bilder fällt mir auf, dass ja doch Fremdteile verbaut sind: Die beiden Dioden, die den Strom-Rückfluss von einem zum anderen Akku verhindern, sind zugekauft. Ich werde sie noch ersetzen durch die 1N4001 aus dem Elektronik-Praktikum.