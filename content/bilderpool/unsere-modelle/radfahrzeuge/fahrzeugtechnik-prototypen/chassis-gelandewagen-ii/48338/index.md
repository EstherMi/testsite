---
layout: "image"
title: "Bodenfreiheit hinten"
date: "2018-10-31T19:15:46"
picture: "chassisfuergelaendewagenii17.jpg"
weight: "17"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/48338
imported:
- "2019"
_4images_image_id: "48338"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:46"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48338 -->
Durch die Portalachsen sitzen die Differentiale weit oben, was im Gelände sehr nützlich ist.