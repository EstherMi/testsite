---
layout: "image"
title: "Vorderradaufhängung 4"
date: "2018-10-31T19:15:47"
picture: "chassisfuergelaendewagenii34.jpg"
weight: "34"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/48355
imported:
- "2019"
_4images_image_id: "48355"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:47"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48355 -->
Die Radaufhängung in Einzelteilen. Die Kette hält alles zusammen.