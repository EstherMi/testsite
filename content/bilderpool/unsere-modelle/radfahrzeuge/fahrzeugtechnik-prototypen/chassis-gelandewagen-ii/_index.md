---
layout: "overview"
title: "Chassis für Geländewagen II"
date: 2019-12-17T18:45:00+01:00
legacy_id:
- categories/3542
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3542 --> 
Allradantrieb mit Portalachsen und den Reifen 65, alles ft-pur.
Erklimmt die 45°-Rampe.
Video unter https://youtu.be/Q2r4f7Y7fNI