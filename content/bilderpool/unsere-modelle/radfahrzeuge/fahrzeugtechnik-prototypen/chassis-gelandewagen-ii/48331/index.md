---
layout: "image"
title: "Ansicht rechts"
date: "2018-10-31T19:15:46"
picture: "chassisfuergelaendewagenii10.jpg"
weight: "10"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/48331
imported:
- "2019"
_4images_image_id: "48331"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:46"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48331 -->
Das was man sieht, ist eigentlich eine recht einfache Konstruktion. Es verbirgt sich allerdings noch einiges, hier noch unsichtbar, in den Rädern.