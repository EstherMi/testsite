---
layout: "image"
title: "Vorderradaufhängung 3"
date: "2018-10-31T19:15:47"
picture: "chassisfuergelaendewagenii33.jpg"
weight: "33"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/48354
imported:
- "2019"
_4images_image_id: "48354"
_4images_cat_id: "3542"
_4images_user_id: "2321"
_4images_image_date: "2018-10-31T19:15:47"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48354 -->
Das Rad ist abgenommen. Das obere Z10 dient dem Antrieb und der Führung, das untere nur der Führung des Rades.
Über die beiden Distanzhülsen (eine auf der Pleuelstange vor den beiden Z10, die andere auf der im Bild senkrecht stehenden schwarzen Clipachse) rollen die Ketten-Förderglieder, Sie verhindern, dass sich das Rad nach vorne oder hinten biegt.