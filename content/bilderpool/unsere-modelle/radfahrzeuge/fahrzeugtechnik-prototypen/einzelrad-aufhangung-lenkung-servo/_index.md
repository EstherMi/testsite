---
layout: "overview"
title: "Einzelrad-Aufhängung, Lenkung mit Servo-Unterstützung"
date: 2019-12-17T18:44:34+01:00
legacy_id:
- categories/2806
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2806 --> 
gefedertes Lastwagen-Chassis, Vorderachse mit Einzelrad Aufhängung und Lenkung mit Servo Unterstützung