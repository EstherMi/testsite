---
layout: "image"
title: "Vorderachse mit Einzelradaufhängung und Lenkung mit Servounterstützung 7"
date: "2013-10-25T00:40:03"
picture: "7_IMG_0337.jpg"
weight: "7"
konstrukteure: 
- "steve"
fotografen:
- "steve"
keywords: ["Lenkung", "Servounterstützung", "Einzelradaufhängung", "Radaufhängung", "Dreieckslenker"]
uploadBy: "steme"
license: "unknown"
legacy_id:
- details/37773
imported:
- "2019"
_4images_image_id: "37773"
_4images_cat_id: "2806"
_4images_user_id: "1752"
_4images_image_date: "2013-10-25T00:40:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37773 -->
Unterseite: die Lenkung mit Servo Unterstützung besteht im Prinzip aus 2 Komponenten. 
Einerseits die motorgetriebene Zahnstangenlenkung, wobei die graue 106er Strebe als Lenkstange die Kraft überträgt. Als zweite Komponente kommt dazu: 
Lenkrad mit Getriebe (3:1), und einer weiteren 'Lenkstange' (die gelbe 90er Strebe, die über die schwarze Lochstrebe mit dem 30er Zahnrad verbunden ist) , die dazu dient, 
den Motor über 2 Taster (links oder rechts) einzuschalten. 
Dies ergibt den Servoeffekt. Dreht man am Lenkrad nach links, 
wird der Taster links gedrückt, und der Motor setzt sich nach links in Bewegung
(Lenkeinschlag nach links), zwar solange, bis man das Lenkrad wieder loslässt, 
worauf der Motor noch einen 'Milimeter' weiterdreht, bis der Taster nicht mehr 
gedrückt ist.