---
layout: "image"
title: "Vorderachse mit Einzelradaufhängung und Lenkung mit Servounterstützung 9"
date: "2013-10-25T00:40:03"
picture: "9_IMG_0335.jpg"
weight: "9"
konstrukteure: 
- "steve"
fotografen:
- "steve"
keywords: ["Lenkung", "Servounterstützung", "Einzelradaufhängung", "Radaufhängung", "Dreieckslenker"]
uploadBy: "steme"
license: "unknown"
legacy_id:
- details/37775
imported:
- "2019"
_4images_image_id: "37775"
_4images_cat_id: "2806"
_4images_user_id: "1752"
_4images_image_date: "2013-10-25T00:40:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37775 -->
Die maximale Lenkeinschlag kann übrigens nicht 'überdehnt' werden: Zuerst schlägt die 
vom Lenkrad bewegte Stange mechanisch an den 'Anschlag', worauf der Motor ebenfalls stoppt, sobald der Taster frei wird. 

Das ganze funktioniert prinzipiell auch ohne Strom. Man  muss nur vorher nur nicht vergessen, den Motor etwas vom HubGetriebe zu lösen  , das Lenken wird dann aber zur (Finger) Knochenarbeit, denn das Hubgetrieb sowie alle anderen beteiligten Teile drehen ja wacker mit..