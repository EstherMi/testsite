---
layout: "image"
title: "Vorderachse mit Einzelradaufhängung und Lenkung mit Servounterstützung 3"
date: "2013-10-25T00:40:03"
picture: "3_IMG_0329.jpg"
weight: "3"
konstrukteure: 
- "steve"
fotografen:
- "steve"
keywords: ["Lenkung", "Servounterstützung", "Einzelradaufhängung", "Radaufhängung", "Dreieckslenker"]
uploadBy: "steme"
license: "unknown"
legacy_id:
- details/37769
imported:
- "2019"
_4images_image_id: "37769"
_4images_cat_id: "2806"
_4images_user_id: "1752"
_4images_image_date: "2013-10-25T00:40:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37769 -->
Die Vorderräder sind an einer Art Dreiecks Querlenker augehängt, dh an 4 Streben pro Rad. Damit lässt sich noch ordentlich Gewicht auflegen, und vorallem ein guter Lenkauschlag realisieren