---
layout: "image"
title: "Mausefallenauto"
date: "2018-06-02T18:40:19"
picture: "P1050041klein.jpg"
weight: "3"
konstrukteure: 
- "EstherM"
fotografen:
- "EstherM"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "EstherM"
license: "unknown"
legacy_id:
- details/47684
imported:
- "2019"
_4images_image_id: "47684"
_4images_cat_id: "297"
_4images_user_id: "2781"
_4images_image_date: "2018-06-02T18:40:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47684 -->
Nach Anregung durch H.A.R.R.Ys Papa-Mobil (https://ftcommunity.de/details.php?image_id=47467) und durch einen Bericht über Mausefallenautos ist jetzt diese einfache Konstruktion entstanden. 
Der Vorteil eines Mausefallenantriebs gegenüber dem Gummiband ist die bessere Standardisierung von Mausefallen, so dass Wettbewerbe erleichtert werden. Der Nachteil ist eine gewisse Verletzungsgefahr.
Dringend zu verbessern ist die Befestigung des Antriebs auf dem Fahrgestell, damit da keine Energie verloren geht. Trotzdem fährt das Ding jetzt schon ungefähr 5 m weit.