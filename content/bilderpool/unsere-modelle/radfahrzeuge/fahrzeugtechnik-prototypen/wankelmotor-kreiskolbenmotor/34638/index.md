---
layout: "image"
title: "Der Wankelmotor"
date: "2012-03-11T21:42:31"
picture: "wankemotorkreiskolbenmotor6.jpg"
weight: "6"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/34638
imported:
- "2019"
_4images_image_id: "34638"
_4images_cat_id: "2556"
_4images_user_id: "833"
_4images_image_date: "2012-03-11T21:42:31"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34638 -->
Diese beiden Taster lassen ja ein Lämpchen aufleuchten, dass die Position der Hot- und Coldspots zeigt. Da die AT immer an der gleichen Stelle stattfinden, entwickelt sich um die Zündkerze herum ein Hotspot und um den Einlasskanal herum ein Coldspot. An diesen Stellen leuchtet dann die Lampe. Blau = Coldspot; Rot = Hotspot.