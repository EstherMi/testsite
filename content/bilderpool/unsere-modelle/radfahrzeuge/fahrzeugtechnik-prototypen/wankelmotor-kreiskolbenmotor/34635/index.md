---
layout: "image"
title: "Der Wankelmotor"
date: "2012-03-11T21:42:31"
picture: "wankemotorkreiskolbenmotor3.jpg"
weight: "3"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/34635
imported:
- "2019"
_4images_image_id: "34635"
_4images_cat_id: "2556"
_4images_user_id: "833"
_4images_image_date: "2012-03-11T21:42:31"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34635 -->
Mit Blick von oben. Hinter der Vorderplatte steckt die Schaltung und Elektronik für die Zündkerze und die Arbeitstakt anzeige. Ausserdem ein Motor damit sich der Kolben auch bewegt und ein entsprechendes Getriebe.