---
layout: "overview"
title: "Wankelmotor (Kreiskolbenmotor)"
date: 2019-12-17T18:44:32+01:00
legacy_id:
- categories/2556
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2556 --> 
Modell eines Wankelmotors (Kreiskolbenmotors) wie er im NSU ro80 eingetzt wurde. Ich habe das Modell für meine Abitur Präsentationsprüfung gebaut um den Bewegungsablauf zu erläutern.