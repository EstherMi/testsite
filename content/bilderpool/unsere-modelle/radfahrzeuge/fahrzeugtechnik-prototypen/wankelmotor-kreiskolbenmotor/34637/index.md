---
layout: "image"
title: "Der Wankelmotor"
date: "2012-03-11T21:42:31"
picture: "wankemotorkreiskolbenmotor5.jpg"
weight: "5"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/34637
imported:
- "2019"
_4images_image_id: "34637"
_4images_cat_id: "2556"
_4images_user_id: "833"
_4images_image_date: "2012-03-11T21:42:31"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34637 -->
Blick "in" den Motor.