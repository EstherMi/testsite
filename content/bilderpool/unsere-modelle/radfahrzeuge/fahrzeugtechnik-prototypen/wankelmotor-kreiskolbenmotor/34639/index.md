---
layout: "image"
title: "Der Wankelmotor"
date: "2012-03-11T21:42:31"
picture: "wankemotorkreiskolbenmotor7.jpg"
weight: "7"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "Thilo Bleumer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/34639
imported:
- "2019"
_4images_image_id: "34639"
_4images_cat_id: "2556"
_4images_user_id: "833"
_4images_image_date: "2012-03-11T21:42:31"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34639 -->
Hier sind die zwei Scheiben zu sehen, die durch die Nockendie Lampen steuern. Die linke Scheibe hat einen Nocken und steuert die Zündkerze. Die rechte Schiebe hat vier Nocken und steuert die Anzeige Lämpchen die vier ATs.