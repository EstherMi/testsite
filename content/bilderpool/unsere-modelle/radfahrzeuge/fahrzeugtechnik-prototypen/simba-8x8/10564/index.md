---
layout: "image"
title: "Simba01.JPG"
date: "2007-05-30T19:01:40"
picture: "Simba01.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Allrad", "8x8"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/10564
imported:
- "2019"
_4images_image_id: "10564"
_4images_cat_id: "961"
_4images_user_id: "4"
_4images_image_date: "2007-05-30T19:01:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10564 -->
Das ist mal der Anfang für einen LKW mit 8x8-Antriebsformel und Einhaltung des ft-Reinheitsgebots ;-) 

(soll heißen: ohne Schnippeln und Bohren -- sowas soll ja schon mal vorgekommen sein)