---
layout: "image"
title: "Simba03.JPG"
date: "2007-05-30T19:07:27"
picture: "Simba03.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/10566
imported:
- "2019"
_4images_image_id: "10566"
_4images_cat_id: "961"
_4images_user_id: "4"
_4images_image_date: "2007-05-30T19:07:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10566 -->
Der Antrieb geht ein wenig ums Eck, aber das ist nötig, damit das Kardangelenk genau in den Drehpunkt des Radschemels kommt und gleichzeitig möglichst nah über den Bodenaufstandspunkt des Rades heranrückt. D.h. das Rad sollte idealerweise "auf dem Punkt" drehen. Daraus wurde aber nichts; es befährt beim Einschlagen der Lenkung einen Kreis. Nun galt es, diese Bahn möglichst eng zu halten.