---
layout: "image"
title: "Simba05.JPG"
date: "2007-05-30T19:12:24"
picture: "Simba05.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/10568
imported:
- "2019"
_4images_image_id: "10568"
_4images_cat_id: "961"
_4images_user_id: "4"
_4images_image_date: "2007-05-30T19:12:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10568 -->
Eine Hälfte des Fahrzeugs im Überblick. Die Spurstangen müssen noch an die Geometrie angepasst werden; sie sind derzeit für alle Achsen gleich. Zwischen den Achsen fehlt noch das Lenkgetriebe oder Gestänge oder was auch immer das werden wird.