---
layout: "image"
title: "Simba11.jpg"
date: "2008-08-03T11:40:28"
picture: "Simba11.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/14992
imported:
- "2019"
_4images_image_id: "14992"
_4images_cat_id: "961"
_4images_user_id: "4"
_4images_image_date: "2008-08-03T11:40:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14992 -->
Das Lenkgetriebe, frei schwebend aufgehängt.