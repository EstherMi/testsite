---
layout: "overview"
title: "Antriebsstrang mit altem Differential und M-Motoren für Antrieb und Lenkung"
date: 2019-12-17T18:44:37+01:00
legacy_id:
- categories/2868
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2868 --> 
Motorisierung für Antrieb und Lenkung mit alten grauen Motoren und Potentiometer als Lenkabgriff