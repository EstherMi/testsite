---
layout: "image"
title: "Verriegelung des M-Motors mit dem alten Differential"
date: "2014-03-16T17:58:36"
picture: "S1060005.jpg"
weight: "5"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/38466
imported:
- "2019"
_4images_image_id: "38466"
_4images_cat_id: "2868"
_4images_user_id: "579"
_4images_image_date: "2014-03-16T17:58:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38466 -->
Hab mich immer geärgert über die schlechten Möglichkeiten, den grauen Motor an das alte Differential anzubauen. Bis ich endlich diese Lösung fand. Hier verrutscht nichts mehr.