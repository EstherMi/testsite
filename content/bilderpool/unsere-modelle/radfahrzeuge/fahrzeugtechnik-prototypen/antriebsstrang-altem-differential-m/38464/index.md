---
layout: "image"
title: "Lenkantrieb"
date: "2014-03-16T17:58:36"
picture: "S1060003.jpg"
weight: "3"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/38464
imported:
- "2019"
_4images_image_id: "38464"
_4images_cat_id: "2868"
_4images_user_id: "579"
_4images_image_date: "2014-03-16T17:58:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38464 -->
Hier habe ich eine schwarze Plastik-Hülse aus einer Kabelisolierung aufgeschoben, das gegen einen Baustein drückt, um das Lenkspiel zu minimieren. Dadurch kann der M-Motor nicht zurückrutschen.