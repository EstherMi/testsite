---
layout: "image"
title: "Ansicht"
date: "2006-04-01T12:38:21"
picture: "DSCN0685.jpg"
weight: "25"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/6002
imported:
- "2019"
_4images_image_id: "6002"
_4images_cat_id: "367"
_4images_user_id: "184"
_4images_image_date: "2006-04-01T12:38:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6002 -->
hier eine Gesamtansicht einer Kette