---
layout: "image"
title: "Prototyp I Ketten Detail"
date: "2006-05-01T19:12:31"
picture: "DSCN0724.jpg"
weight: "7"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/6197
imported:
- "2019"
_4images_image_id: "6197"
_4images_cat_id: "367"
_4images_user_id: "184"
_4images_image_date: "2006-05-01T19:12:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6197 -->
Hier kann man den Unterschied so richtig gut erkennen.
Bei dem Modell ist nicht nur ein einzelnes Zahnrad sondern ein doppeltes auf jeder Seite. Dadurch wird das Ganze stabiler.