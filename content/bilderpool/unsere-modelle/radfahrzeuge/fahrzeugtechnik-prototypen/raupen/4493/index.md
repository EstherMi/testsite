---
layout: "image"
title: "raupen_18"
date: "2005-06-19T16:07:01"
picture: "raupen_18.jpg"
weight: "44"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/4493
imported:
- "2019"
_4images_image_id: "4493"
_4images_cat_id: "367"
_4images_user_id: "144"
_4images_image_date: "2005-06-19T16:07:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4493 -->
Der Drehkranz werd wieder aus einen stück Alu rohr und zwei Alu platten 150x150mm aufgebaut sein. Innen Durchmesser: 110mm.