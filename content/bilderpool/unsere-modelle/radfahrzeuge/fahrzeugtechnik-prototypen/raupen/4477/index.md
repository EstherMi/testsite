---
layout: "image"
title: "raupen_2"
date: "2005-06-19T16:06:51"
picture: "raupen_2.jpg"
weight: "28"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/4477
imported:
- "2019"
_4images_image_id: "4477"
_4images_cat_id: "367"
_4images_user_id: "144"
_4images_image_date: "2005-06-19T16:06:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4477 -->
Um sich ein bild zu machen: ein ft mänchen zwischen die beide Raupen.

ft mänchen: 9cm hoch
die Raupen: 11cm hoch