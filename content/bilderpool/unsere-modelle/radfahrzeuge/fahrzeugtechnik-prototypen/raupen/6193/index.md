---
layout: "image"
title: "Prototyp I Ansicht von vorn"
date: "2006-05-01T18:57:20"
picture: "DSCN0719.jpg"
weight: "3"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/6193
imported:
- "2019"
_4images_image_id: "6193"
_4images_cat_id: "367"
_4images_user_id: "184"
_4images_image_date: "2006-05-01T18:57:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6193 -->
