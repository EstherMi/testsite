---
layout: "image"
title: "Prototyp I Ansicht von hinten"
date: "2006-05-01T18:57:19"
picture: "DSCN0717.jpg"
weight: "2"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/6192
imported:
- "2019"
_4images_image_id: "6192"
_4images_cat_id: "367"
_4images_user_id: "184"
_4images_image_date: "2006-05-01T18:57:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6192 -->
Das sollte eines von vier Raupenfahrwerken werden welche unter meinem aktuell geplanten Modell sitzen sollten. Leider habe ich mich im Massstab verrechnet und das Ding zu kurz gebaut.