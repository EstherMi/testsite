---
layout: "image"
title: "Raupenkette aus 32085 (Detail)"
date: "2006-04-01T12:38:20"
picture: "DSCN0684.jpg"
weight: "23"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/6000
imported:
- "2019"
_4images_image_id: "6000"
_4images_cat_id: "367"
_4images_user_id: "184"
_4images_image_date: "2006-04-01T12:38:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6000 -->
erste Versuche mit dem 32085