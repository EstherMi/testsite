---
layout: "image"
title: "raupen_14"
date: "2005-06-19T16:07:01"
picture: "raupen_14.jpg"
weight: "40"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/4489
imported:
- "2019"
_4images_image_id: "4489"
_4images_cat_id: "367"
_4images_user_id: "144"
_4images_image_date: "2005-06-19T16:07:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4489 -->
Dreht man diesen Rast-Ritzel, dan werd der Raupe abgespant oder entspant.