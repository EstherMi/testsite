---
layout: "image"
title: "Variante mit Z20"
date: "2006-04-12T19:52:48"
picture: "DSCN0693.jpg"
weight: "20"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/6074
imported:
- "2019"
_4images_image_id: "6074"
_4images_cat_id: "367"
_4images_user_id: "184"
_4images_image_date: "2006-04-12T19:52:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6074 -->
So sieht´s von innen aus