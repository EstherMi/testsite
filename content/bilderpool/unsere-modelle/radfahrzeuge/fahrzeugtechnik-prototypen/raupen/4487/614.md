---
layout: "comment"
hidden: true
title: "614"
date: "2005-06-20T20:46:44"
uploadBy:
- "pk"
license: "unknown"
imported:
- "2019"
---
einselteillenTotal circa 70700 einselteillen, wovon:

1389 x 31982 Federnocken 
1175 x 36248 Rastkettenglied 
5984 x 36263 Kettenglied
1370 x 37237 Baustein 5 
1140 x 37468 Baustein 7,5 
 247 x 38245 Bauplatte 15x90

Gruß Peter