---
layout: "image"
title: "Seitenansicht"
date: "2016-03-21T13:33:37"
picture: "lenkung04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- details/43174
imported:
- "2019"
_4images_image_id: "43174"
_4images_cat_id: "3207"
_4images_user_id: "1924"
_4images_image_date: "2016-03-21T13:33:37"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43174 -->
Innsgesammt baut die Lenkeinheit relativ kompakt. Oben dauf fehlt nur noch die Federung.