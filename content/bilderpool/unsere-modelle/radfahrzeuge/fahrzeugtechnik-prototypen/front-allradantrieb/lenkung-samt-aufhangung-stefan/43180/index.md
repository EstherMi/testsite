---
layout: "image"
title: "Achsschenkel Gesamtansicht"
date: "2016-03-21T13:33:37"
picture: "lenkung10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Reinmüller"
fotografen:
- "Stefan Reinmüller"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Rm"
license: "unknown"
legacy_id:
- details/43180
imported:
- "2019"
_4images_image_id: "43180"
_4images_cat_id: "3207"
_4images_user_id: "1924"
_4images_image_date: "2016-03-21T13:33:37"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43180 -->
Zur besseren Sicht ohne Reifen.
Die Kunststoffachse dient zur Lagerung des Reifen, angetrieben wird er über das darüberliegene Z10.