---
layout: "image"
title: "Detailblick 2 auf das Getriebe"
date: "2006-05-21T17:52:59"
picture: "Allrad6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/6283
imported:
- "2019"
_4images_image_id: "6283"
_4images_cat_id: "548"
_4images_user_id: "104"
_4images_image_date: "2006-05-21T17:52:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6283 -->
Der MiniMot zieht am Gummiring (Original ft, für einen älteren Reifen 45 gedacht) und drückt damit die verschiebliche Getriebeachse gegen den Federdruck (die Feder stammt aus dem alten ft Elektromechanikprogramm).