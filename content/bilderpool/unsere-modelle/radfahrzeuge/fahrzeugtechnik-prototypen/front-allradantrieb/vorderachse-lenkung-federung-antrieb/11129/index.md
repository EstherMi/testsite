---
layout: "image"
title: "Gesamtansicht vorne"
date: "2007-07-18T18:34:53"
picture: "vorderachsemitlenkungfederungundantrieb2.jpg"
weight: "5"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/11129
imported:
- "2019"
_4images_image_id: "11129"
_4images_cat_id: "1008"
_4images_user_id: "453"
_4images_image_date: "2007-07-18T18:34:53"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11129 -->
Das Differenzial und der Antreib ist noch nicht gebastelt, mir ging es bei diesem versuch erstmal nur um die Federung(und Kardanwelle).