---
layout: "image"
title: "Allrad-R45-51.JPG"
date: "2009-03-08T11:47:13"
picture: "Allrad-R45-51.JPG"
weight: "15"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/23421
imported:
- "2019"
_4images_image_id: "23421"
_4images_cat_id: "1588"
_4images_user_id: "4"
_4images_image_date: "2009-03-08T11:47:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23421 -->
Hier ist die Pendelachse zur anderen Seite hin gekippt und gibt den Blick auf die Kegelräder frei.