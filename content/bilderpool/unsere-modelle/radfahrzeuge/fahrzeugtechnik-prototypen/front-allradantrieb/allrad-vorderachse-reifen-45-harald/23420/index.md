---
layout: "image"
title: "Allrad-R45-54.JPG"
date: "2009-03-08T11:45:53"
picture: "Allrad-R45-54.JPG"
weight: "14"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/23420
imported:
- "2019"
_4images_image_id: "23420"
_4images_cat_id: "1588"
_4images_user_id: "4"
_4images_image_date: "2009-03-08T11:45:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23420 -->
Das Ganze ist jetzt die vordere Pendelachse eines Traktors. Die Antriebswelle verläuft durchs Drehgelenk, das von zwei Gelenksteinen gebildet wird. Die Spurstange wird nach oben oder vielleicht auch nach vorn (vor die Achse) ausweichen müssen, und die zwei fehlenden unteren Querlenker müssen auch wieder dran.

Die Alustange braucht etwas Abstand zur Antriebswelle, damit das Mitteldifferenzial auch noch Platz findet.