---
layout: "image"
title: "Allrad_4699"
date: "2011-02-25T16:30:39"
picture: "IMG_4699.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/30124
imported:
- "2019"
_4images_image_id: "30124"
_4images_cat_id: "1588"
_4images_user_id: "4"
_4images_image_date: "2011-02-25T16:30:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30124 -->
Hab ich bei Stefan (Falk) abgeguckt: man spendiert jedem Vorderrad einen eigenen Motor, und schon ist man den ganzen Ärger mit Kardan und Differential los geworden.