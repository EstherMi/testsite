---
layout: "image"
title: "Allrad_4701.JPG"
date: "2011-02-25T16:35:28"
picture: "Allrad_4701.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Frontantrieb"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/30125
imported:
- "2019"
_4images_image_id: "30125"
_4images_cat_id: "1588"
_4images_user_id: "4"
_4images_image_date: "2011-02-25T16:35:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30125 -->
Die Winkelsteine sind nötig, damit das Differentialrad 137196 in die richtige Position über dem Drehpunkt und im Eingriff mit der Verzahnung am Rad 45 kommt. In der oberen Schräge folgen 15° - BS5 - BS5 - 15° (halb verdeckt) aufeinander.