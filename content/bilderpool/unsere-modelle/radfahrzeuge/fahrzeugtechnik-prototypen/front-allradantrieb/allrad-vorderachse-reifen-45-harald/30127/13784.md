---
layout: "comment"
hidden: true
title: "13784"
date: "2011-03-03T19:11:20"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Wozu der Akku in diesem Bild gut ist, hab ich schon verstanden, aber ich wollte wissen, wie die Stromversorgung des Autos gedacht war. "Gilt" es, die für den Rampentest von außen zuzuführen?

Gruß,
Stefan