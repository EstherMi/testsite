---
layout: "image"
title: "Mini-Allrad-R45-03.jpg"
date: "2009-03-11T16:37:49"
picture: "Mini-Allrad-R45-03.jpg"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Frontantrieb"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/23445
imported:
- "2019"
_4images_image_id: "23445"
_4images_cat_id: "1588"
_4images_user_id: "4"
_4images_image_date: "2009-03-11T16:37:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23445 -->
Die Bodenfreiheit ist nicht berauschend, und man muss die Reifen richtig nach innen hin festziehen, damit es nicht gleich losrattert. Aber immerhin, einen T5 Syncro oder ähnliches müsste man da drum herum stricken können.
Oberhalb der BS15-Loch liegt lose eine Scheibe 15 als Abstandshalter. Auf der rechten Seite hat sich die Scheibe schon verabschiedet.