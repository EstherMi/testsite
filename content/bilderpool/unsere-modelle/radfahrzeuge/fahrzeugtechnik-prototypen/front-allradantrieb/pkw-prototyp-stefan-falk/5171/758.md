---
layout: "comment"
hidden: true
title: "758"
date: "2005-11-01T21:22:55"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
BreiteNetter Kommentar, danke! In der Breite ist sicher noch Luft drin. Federnocken sind aber glaube ich gar nicht nötig. Die einzige verkürzbare, weil nicht für die Achsführung benötigte Länge sind ja die frei schwebenden Achsen nahe der Fahrzeugmitte. Und die sind gar nicht so kritisch. Ich will die ja eh noch parallel zur Antriebsachse versetzt werden (siehe die weiteren Beschreibungen; das ist es übrigens auch, was ich bei unserer damaligen Unterhaltung bei dem anderen Prototypen mit den "zwei Kardangelenken" meinte). Die gelben Statik-BS15 lassen sich vielleicht auch noch durch einen BS 7,5 oder so ähnlich ersetzen, ohne dass die Räder beim Federn zu sehr nach innen/außen wandern. Dann würde das ganze sogar um insgesamt 45 anstatt 30 mm schrumpfen (auch siehe weitere Beschreibungen).

Gruß,
Stefan