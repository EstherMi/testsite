---
layout: "comment"
hidden: true
title: "398"
date: "2005-01-12T20:37:57"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ja, hab ich mal angedachtIch hab mal experimentiert mit eine Flachnabe im Reifen (Felge 45 hab ich leider nicht), direkt (!) dadran ein Rastachsen-Kardan, und Führung auf der anderen Seite. Ist aber zu wackelig. Ich bin noch nicht im Reinen mit meinen Überlegungen und greife das Thema wohl nach einer Denkpause erst wieder auf.

Vielleicht bau ich das ganze auch dermaßen groß, dass selbst das große Kardangelenk nicht mehr weh tut. Dann muss ich halt nur auch die Räder selbst bauen...

Gruß,
Stefan