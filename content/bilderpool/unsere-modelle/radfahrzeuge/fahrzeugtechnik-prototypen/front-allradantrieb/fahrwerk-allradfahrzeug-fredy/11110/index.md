---
layout: "image"
title: "Allrad Fahrzeug"
date: "2007-07-16T16:20:58"
picture: "fahrzeug05.jpg"
weight: "5"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/11110
imported:
- "2019"
_4images_image_id: "11110"
_4images_cat_id: "1007"
_4images_user_id: "453"
_4images_image_date: "2007-07-16T16:20:58"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11110 -->
In der Mitte das Diff, Links und Rechts die beiden Kardan Gelenke für die Lenkung.