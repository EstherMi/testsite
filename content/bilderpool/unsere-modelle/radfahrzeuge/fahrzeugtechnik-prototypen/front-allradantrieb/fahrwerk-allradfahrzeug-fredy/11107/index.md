---
layout: "image"
title: "Allrad Fahrzeug"
date: "2007-07-16T16:20:58"
picture: "fahrzeug02.jpg"
weight: "2"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/11107
imported:
- "2019"
_4images_image_id: "11107"
_4images_cat_id: "1007"
_4images_user_id: "453"
_4images_image_date: "2007-07-16T16:20:58"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11107 -->
In der Mitte, sprich in dem freien Raum soll später mal der Akku und die Steuerung eingebaut werden.