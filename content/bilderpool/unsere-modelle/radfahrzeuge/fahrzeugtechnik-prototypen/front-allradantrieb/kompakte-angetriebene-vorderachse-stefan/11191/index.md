---
layout: "image"
title: "Ansicht von oben"
date: "2007-07-22T18:50:00"
picture: "kompakteangetriebenevorderachse2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/11191
imported:
- "2019"
_4images_image_id: "11191"
_4images_cat_id: "1012"
_4images_user_id: "104"
_4images_image_date: "2007-07-22T18:50:00"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11191 -->
Für die folgenden Aufnahmen habe ich die MiniMots abgenommen, damit man die Details besser erkennen kann.

Das interessante ist die Lenkgeometrie. Die beiden Metallachsen in der Mitte, an denen der Prototyp aufgehängt ist, liegen nämlich nicht in der Mitte zwischen den in der Nähe befindlichen Kunststoffachsen, an denen die Streben befestigt sind. Das hat beim Lenken durch Verdrehen der roten Bausteine einen bemerkenswerten Effekt.