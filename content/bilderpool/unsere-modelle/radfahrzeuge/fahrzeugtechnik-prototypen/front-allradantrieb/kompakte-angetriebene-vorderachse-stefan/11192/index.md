---
layout: "image"
title: "Lenkeinschlag (1)"
date: "2007-07-22T18:50:01"
picture: "kompakteangetriebenevorderachse3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/11192
imported:
- "2019"
_4images_image_id: "11192"
_4images_cat_id: "1012"
_4images_user_id: "104"
_4images_image_date: "2007-07-22T18:50:01"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11192 -->
Die Wirkung ist nämlich, dass die Streben aus ihrer quer zum Fahrzeug liegenden Richtung heraus bewegt werden. Das wiederum führt dazu, dass die Räder 60 sich praktisch genau um ihren Aufstandsmittelpunkt drehen!

Auch bei den Lenkbewegungen würden die MiniMots nie aus dem Fahrzeug herausragen.