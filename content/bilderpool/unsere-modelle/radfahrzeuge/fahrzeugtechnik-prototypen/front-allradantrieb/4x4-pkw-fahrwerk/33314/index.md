---
layout: "image"
title: "Unten"
date: "2011-10-23T18:54:23"
picture: "xpkwfahrwerk8.jpg"
weight: "8"
konstrukteure: 
- "Stainless"
fotografen:
- "Stainless"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stainless"
license: "unknown"
legacy_id:
- details/33314
imported:
- "2019"
_4images_image_id: "33314"
_4images_cat_id: "2466"
_4images_user_id: "1376"
_4images_image_date: "2011-10-23T18:54:23"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33314 -->
Das Namenlose Teil gegen das Wegbiegen des Differenzialrades hinten lässt sich auf einen Noppen aufschieben. Der Reedkontakthalter macht seinen Dienst etwas besser, kann jedoch nur in Nuten eingeschoben werden. Gebraucht wird das eigentlich nur, wenn man zB 1:50 PMs hat, aber bei meinen Modellen ist das mittlerweile Standart. Wegen der großen übertragbaren Kräfte auch das zugeklebte Differenzial :/
*SchlechtesGewissenHabWegenTeilemodding*
Jedoch platzen die Differenziale bei solchen Kräften auf, wenn sie nicht zugeklebt sind. Na Toll :/
Hier sinds ja nur 2 XMs, da ist noch alles im grünen Bereich, aber ich hab halt nur diese 3 Differenziale :)