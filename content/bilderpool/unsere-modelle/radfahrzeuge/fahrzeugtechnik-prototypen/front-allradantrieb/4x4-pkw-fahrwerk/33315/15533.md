---
layout: "comment"
hidden: true
title: "15533"
date: "2011-10-24T10:40:19"
uploadBy:
- "Stainless"
license: "unknown"
imported:
- "2019"
---
Yep, das Fahrwerk steht zu 100% gerade zum Boden - auf der´m Foto mit der Seitenansicht schaut das nicht ganz so aus, liegt aber daran, dass mir ein Winkelträger etwas verrutscht ist. 
Die Verbindungsträger sind 1-2 mm zu kurz, was aber auch beabsichtigt ist. Nur so wird absolute Stabilität erreicht. Allerdings sind die 1-2mm nur so unwesentlich, dass sich absolut NICHTS verzieht.