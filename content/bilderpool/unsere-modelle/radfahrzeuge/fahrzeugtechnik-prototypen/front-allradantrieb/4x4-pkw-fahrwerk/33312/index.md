---
layout: "image"
title: "Oben"
date: "2011-10-23T18:54:23"
picture: "xpkwfahrwerk6.jpg"
weight: "6"
konstrukteure: 
- "Stainless"
fotografen:
- "Stainless"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stainless"
license: "unknown"
legacy_id:
- details/33312
imported:
- "2019"
_4images_image_id: "33312"
_4images_cat_id: "2466"
_4images_user_id: "1376"
_4images_image_date: "2011-10-23T18:54:23"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33312 -->
Hier zu sehen: Hinten wurde gegn das Wegbiegen des "Differenzial-Zahnrades" mal wieder ein Teil verbaut, von dem ich absolut keine Ahnung hab, wie es heißt, und vorne ein Reedkontakthalter. Den sieht man am nächsten Bild nochmal wesentlich besser.