---
layout: "image"
title: "Gesamtansicht"
date: "2007-05-28T19:32:43"
picture: "allradprototyp01.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/10542
imported:
- "2019"
_4images_image_id: "10542"
_4images_cat_id: "959"
_4images_user_id: "104"
_4images_image_date: "2007-05-28T19:32:43"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10542 -->
Vom Allrad-Citroen hab ich noch ein paar Prototypen-Bilder gefunden, auf denen ein paar Details der Vorderachskonstruktion besser sichtbar sind. Dieser Prototyp wird noch von einem Zentralmotor für Vorder- und Hinterachse angetrieben (das fertige Auto hatte für beide Achsen je einen Motor).

Dies ist der Prototyp eines Fahrwerks mit folgenden Features:

- Lenkung
- Allradantrieb
- Einzelradaufhängung
- Federung aller Räder mit vollautomatischen Niveauausgleich und verstellbarer Bodenfreiheit (fehlt hinten noch)
- elektrisch geschaltetes 2-Gang-Getriebe (im Moment gibt es noch ein bisschen viel innere Reibungsverluste)

Die einzigen nicht von fischertechnik stammenden Teile sind zwei handelsübliche Gummiringe. Das Fahrwerk ist ansonsten ausschließlich aus unveränderten ft-Teilen aufgebaut. Ob's wirklich mal ein Citroën wird oder halt nur ein Allrad-Auto, wird sich noch zeigen.