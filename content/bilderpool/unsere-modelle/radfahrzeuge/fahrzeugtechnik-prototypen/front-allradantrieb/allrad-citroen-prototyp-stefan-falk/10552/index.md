---
layout: "image"
title: "Gesamtblick auf den Vorbau"
date: "2007-05-28T19:33:55"
picture: "allradprototyp11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/10552
imported:
- "2019"
_4images_image_id: "10552"
_4images_cat_id: "959"
_4images_user_id: "104"
_4images_image_date: "2007-05-28T19:33:55"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10552 -->
Sorry, auch nicht gerade scharf.