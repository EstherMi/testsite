---
layout: "overview"
title: "Allrad-Citroën Prototyp  (Stefan Falk)"
date: 2019-12-17T18:42:57+01:00
legacy_id:
- categories/959
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=959 --> 
Alte Prototypen-Bilder des Fahrzeugs mit Lenkung, Federung, Niveauausgleich (deshalb Citroën) und Allradantrieb.