---
layout: "image"
title: "Motor für Niveauausgleich vorne"
date: "2007-05-28T19:32:44"
picture: "allradprototyp03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/10544
imported:
- "2019"
_4images_image_id: "10544"
_4images_cat_id: "959"
_4images_user_id: "104"
_4images_image_date: "2007-05-28T19:32:44"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10544 -->
Dieser alte MiniMot in Bausteingröße hatte leider während der Prototypenphase das Ende seiner Lebensdauer erreicht.