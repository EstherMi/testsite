---
layout: "comment"
hidden: true
title: "20285"
date: "2015-03-02T19:36:57"
uploadBy:
- "Martin Wanke"
license: "unknown"
imported:
- "2019"
---
Hallo Thomas,
bin grade bei meinen Recherchen nach Frontantrieben vorbeigeschneit. Für eine Rein-ft-Lösung auf jeden Fall ein guter Ansatz. Aber ich bin mittlerweile so weit, dass ich beim Thema Frontantrieb nicht an eine elegante Lösung ohne Fremdmaterial glaube. Wobei "elegant" natürlich Meinungssache ist.
Gruß,
Martin