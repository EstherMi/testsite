---
layout: "image"
title: "Angetriebene gefederte Vorderachse 1"
date: "2012-09-08T15:23:52"
picture: "Angetriebene_gefederte_Vorderachse_1.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/35467
imported:
- "2019"
_4images_image_id: "35467"
_4images_cat_id: "2630"
_4images_user_id: "328"
_4images_image_date: "2012-09-08T15:23:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35467 -->
Das ist der Prototyp einer kompakten angetriebenen und gefederten Vorderachse mit Einzelradaufhängung - das allseits beliebte Thema. Die Grundidee ist, Lenkung und Federung nicht zu entkoppeln, sondern beides über nur 1 Kardangelenk zu führen. Das spart vor allem an der Breite, denn man braucht pro Seite nur 1 statt 2 Kardangelenke.

Die Lenkachse sitzt genau über den Kardangelenk und bildet mit der Feder auf der Achse eine Art Federbein. Die Radführung übernehmen die beiden Statikstreben recht erfolgreich.

Oben am Federbein habe ich die Anbindung der Spurstange nur grob angedeutet. Baut man die Achshälfte zweimal auf, ergibt sich eine recht brauchbare schmale Vorderachse, die gerade noch mit den Reifen 50 harmoniert.

Aber Halt! Bevor Ihr voller Euphorie auf die Knie fallt: Die Achse sieht nur in der Theorie toll aus - sie funktioniert leider nicht! Egal wie leicht das Fahrzeug ist, was man darüber baut: Die Räder haben beim Fahren durch die Federung die Tendenz, nach außen zu wandern, so dass sich die Rastverbindungen der Antriebswelle lösen und die Achse quasi auseinander fällt. :o( Das ist schon bei Geradeausfahrt nach spätestens 1 m Fahrt der Fall, bei Kurvenfahrt schon viel früher. Ich habe es mit leichter Vorspur versucht (die Räder "schielen" also nach innen), was etwas hilft. Aber rückwärts darf man da nicht mehr fahren ...

Eigentlich gehört diese Achse in die Kategorie "Fehlversuche", aber ich möchte nicht, dass sie dort versauert, denn das Konzept ist meiner Meinung nach wert, optimiert und weiterentwickelt zu werden.