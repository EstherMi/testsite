---
layout: "overview"
title: "Kompakter Frontantrieb ohne Differential"
date: 2019-12-17T18:43:25+01:00
legacy_id:
- categories/2227
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2227 --> 
Kleiner, stabiler Frontantrieb für die großen Traktorräder. Weil diese Vorderachse später für ein geländegängiges Fahrzeug dienen soll, wurde auf ein Differential verzichtet.