---
layout: "image"
title: "Rückseite"
date: "2011-02-25T22:43:34"
picture: "A4_.jpg"
weight: "4"
konstrukteure: 
- "thkais"
fotografen:
- "thkais"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thkais"
license: "unknown"
legacy_id:
- details/30131
imported:
- "2019"
_4images_image_id: "30131"
_4images_cat_id: "2227"
_4images_user_id: "41"
_4images_image_date: "2011-02-25T22:43:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30131 -->
Erstaunlicherweise ist die Antriebswelle mit dem Z20 relativ stabil. Durch die beiden Bausteine 7,5 zwischen den als Rahmen gedachten Baustein 30 wird die Achse zusätzlich stabilisiert, Lager sind an dieser Stelle offenbar garnicht notwendig. Später plane ich einen Antrieb per Kette.