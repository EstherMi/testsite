---
layout: "image"
title: "Vorderachse (1)"
date: "2007-07-22T23:35:54"
picture: "fahrwerkmitfrontantrieb2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/11197
imported:
- "2019"
_4images_image_id: "11197"
_4images_cat_id: "1013"
_4images_user_id: "104"
_4images_image_date: "2007-07-22T23:35:54"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11197 -->
Die beiden längeren Metallachsen sorgen für den wichtigen definierten Stand der Mittelpunkte der BS15 mit Loch. Die Vorderachse wird ansonsten nur von den vier grauen Streben 60 in Position gehalten, die gleichzeitig für deren Federung zuständig sind. Damit der Antrieb reibungslos läuft, muss allerdings erst mein Akku geladen werden. Wenn man genau hinschaut, sind die beiden MiniMots hier nämlich noch parallel anstatt in Serie geschaltet. Dann werde ich auch sehen, ob ich die Antriebsschnecken auch noch per Gummi o.ä. besser anpressen muss. Gefahren ist das Fahrwerk so aber schon erfolgreich.