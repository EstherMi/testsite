---
layout: "image"
title: "Gesamtansicht"
date: "2011-11-13T18:14:36"
picture: "vorderachse1.jpg"
weight: "1"
konstrukteure: 
- "Stainless"
fotografen:
- "Stainless"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stainless"
license: "unknown"
legacy_id:
- details/33467
imported:
- "2019"
_4images_image_id: "33467"
_4images_cat_id: "2481"
_4images_user_id: "1376"
_4images_image_date: "2011-11-13T18:14:36"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33467 -->
Hier meine Version einer extrem stabilen, angetriebenen Vorderachse ohne Differential. 
Für ein Differential müsste man die Achse etwas verbreitern, jedoch soll da später mal ein Unimog drum rum, wofür die Vorderachse aufgrund halbwegs realistischer Proportionen nicht breiter werden darf. 
Ich wollte wie in einem Original-Unimog Portalchsen für mehr Bodenfreiheit - dadraus is zwar nix geworden, aber dafür eine extrem stabile Vorderachse. Von hinten kommen 2 Kardanwellen, die sich die Kraft teilen; in Verbindung mit der 1:4 Untersetzung im Endantrieb ist dies fast schon ein Paradies für die ganzen Kegelrastzahnräder.
Von den Kardanwellen wird die Kraft nach unten auf eine Reihe Z10 geleitet, die dann wieder über diverse Rastkegelzahnräder ein weiteres Z1 antreiben, welches schließlich das Z40 antreibt.
 Von den 4 Statikstreben ist eigentlich nur eine nötig, so ists aber wesentlich stabiler.
