---
layout: "image"
title: "Allrad-R60-04.JPG"
date: "2009-04-08T11:31:35"
picture: "Allrad-R60-04.JPG"
weight: "15"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/23641
imported:
- "2019"
_4images_image_id: "23641"
_4images_cat_id: "1613"
_4images_user_id: "4"
_4images_image_date: "2009-04-08T11:31:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23641 -->
Die Lenkung ist vollständig nach ft-Reinheitsgebot gebaut. Die einzigen Fremd/Mod-Teile sind die beiden Kugellager in den Schneckenmuttern.