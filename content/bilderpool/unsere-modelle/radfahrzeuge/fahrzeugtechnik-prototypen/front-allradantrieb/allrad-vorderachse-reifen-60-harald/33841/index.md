---
layout: "image"
title: "Allrad-R60-74.JPG"
date: "2012-01-06T10:21:15"
picture: "allrad3.jpg"
weight: "7"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/33841
imported:
- "2019"
_4images_image_id: "33841"
_4images_cat_id: "1613"
_4images_user_id: "4"
_4images_image_date: "2012-01-06T10:21:15"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33841 -->
Die Vorderansicht. Die Achse sollte unter die Silhouette eines heute üblichen PKW passen. Die haben ja jetzt alle so "aufgeblasene" Motorhauben (nicht nur wegen Fußgänger-Aufprallschutz, sondern sicher auch, damit es wuchtiger wirkt). Vorerst denke ich aber an einen Citroen DS (Déesse = Göttin).