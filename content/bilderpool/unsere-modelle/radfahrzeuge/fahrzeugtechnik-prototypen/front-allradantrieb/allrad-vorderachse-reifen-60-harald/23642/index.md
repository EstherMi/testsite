---
layout: "image"
title: "Allrad-R60-07.JPG"
date: "2009-04-08T11:39:10"
picture: "Allrad-R60-07.JPG"
weight: "16"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/23642
imported:
- "2019"
_4images_image_id: "23642"
_4images_cat_id: "1613"
_4images_user_id: "4"
_4images_image_date: "2009-04-08T11:39:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23642 -->
Das gelbe ist die Freilaufnabe, die es offiziell nur noch in schwarz (68535) gibt.

Das schwarze Teil ist eine Rastachse mit Platte 130593, derzeit nur 1x im Baukasten "Feuerwehr" enthalten und noch nicht einzeln erhältlich. Das zweite hab ich mit Bohrmaschine und Klebstoff hergestellt.

Die Riegelscheibe liegt hier nur auf der Innenseite, damit die Kamera etwas zum Fokussieren hat. Eigentlich gehört sie außen drauf.

Der BS7,5 sitzt etwas versetzt und stützt sich damit auf dem Rand des Differenzials ab. Unter Last würde er sowieso dahin rutschen.