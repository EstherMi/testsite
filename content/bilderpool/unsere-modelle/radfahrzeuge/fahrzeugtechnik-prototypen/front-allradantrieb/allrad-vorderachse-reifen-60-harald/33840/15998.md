---
layout: "comment"
hidden: true
title: "15998"
date: "2012-01-06T11:13:19"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Wunderbar kreative Anwendung der Kardangelenke! Schwärm! Und im Raster! Und ungefrickelt! Das mit dem nicht zentrierten Antriebsstrang ist bestimmt das kleinste Problem für einen Allrad.

Gruß,
Stefan