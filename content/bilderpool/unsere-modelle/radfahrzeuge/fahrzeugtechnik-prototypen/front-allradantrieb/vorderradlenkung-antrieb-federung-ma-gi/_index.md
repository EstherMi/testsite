---
layout: "overview"
title: "Vorderradlenkung mit Antrieb und Federung (Ma-gi-er)"
date: 2019-12-17T18:42:59+01:00
legacy_id:
- categories/964
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=964 --> 
Ich habe im Chat mal gesagt, dass Vorderradlenkung mit Antrieb und Federung einfach sei. Die Funktionalität kann man fast nicht mehr überbieten. Aber dass sie etwas kompakter ist, schon.