---
layout: "image"
title: "Von der Seite"
date: "2007-06-01T20:08:58"
picture: "vorderradlenkungmitantriebundfederung5.jpg"
weight: "5"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/10650
imported:
- "2019"
_4images_image_id: "10650"
_4images_cat_id: "964"
_4images_user_id: "445"
_4images_image_date: "2007-06-01T20:08:58"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10650 -->
Zwischen den Rädern ist ein Gummi gespannt , um die Drehung zu übertragen.