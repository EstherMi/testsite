---
layout: "image"
title: "Gesamtansicht"
date: "2007-06-01T20:08:57"
picture: "vorderradlenkungmitantriebundfederung1.jpg"
weight: "1"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/10646
imported:
- "2019"
_4images_image_id: "10646"
_4images_cat_id: "964"
_4images_user_id: "445"
_4images_image_date: "2007-06-01T20:08:57"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10646 -->
Es ist eigentlich nicht eine ganze Lenkung, aber das reicht, denn steuern kann man sie ganz normal.