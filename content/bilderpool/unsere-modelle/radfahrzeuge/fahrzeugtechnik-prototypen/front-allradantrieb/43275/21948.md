---
layout: "comment"
hidden: true
title: "21948"
date: "2016-04-24T11:57:57"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Das sieht mehrfach interessant aus - kommen da noch mehr Bilder?

Sind die Radaufhängungen selbstgemachte Teile?

Soll da wirklich ein Laster drauf, und die Federn plus Pneumatikzylinder sollen die Federung übernehmen? Sind die dafür denn kräftig genug?

Gruß,
Stefan