---
layout: "image"
title: "Bodenfreiheit"
date: "2009-03-24T00:03:24"
picture: "angetriebenevorderachse6.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/23499
imported:
- "2019"
_4images_image_id: "23499"
_4images_cat_id: "1603"
_4images_user_id: "104"
_4images_image_date: "2009-03-24T00:03:24"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23499 -->
Solange der Antrieb des Differentials nicht nach unten geleitet werden kann, bleibt viel Bodenfreiheit.