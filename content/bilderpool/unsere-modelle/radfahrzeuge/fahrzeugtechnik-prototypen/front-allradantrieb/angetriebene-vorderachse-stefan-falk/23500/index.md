---
layout: "image"
title: "Radaufhängung"
date: "2009-03-24T00:03:24"
picture: "angetriebenevorderachse7.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/23500
imported:
- "2019"
_4images_image_id: "23500"
_4images_cat_id: "1603"
_4images_user_id: "104"
_4images_image_date: "2009-03-24T00:03:24"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23500 -->
Die Aufhängung selbst ist recht simpel, da das Rad ja von außen angetrieben und innen also einfach nur gelagert wird. Die beiden roten Teile unten am BS15 mit Loch sind ganz nach außen geschoben und verhindern, dass der BS15 bei starker Gewichtsbelastung in den BS7,5 nach oben geschoben wird.