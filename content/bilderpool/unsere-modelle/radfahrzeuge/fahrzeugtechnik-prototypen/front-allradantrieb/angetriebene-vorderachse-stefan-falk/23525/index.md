---
layout: "image"
title: "Noch eine Gesamtansicht"
date: "2009-03-26T21:25:13"
picture: "vorderachse6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/23525
imported:
- "2019"
_4images_image_id: "23525"
_4images_cat_id: "1603"
_4images_user_id: "104"
_4images_image_date: "2009-03-26T21:25:13"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23525 -->
Evtl. gingen auch I-Streben 90 anstatt 120, aber ob das dann ohne die Rastkupplung und den Federnocken auf beiden Seiten auch gerade so gut aufgeht, müsste man noch probieren.