---
layout: "image"
title: "Detailansicht von unten"
date: "2009-03-24T00:03:24"
picture: "angetriebenevorderachse8.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/23501
imported:
- "2019"
_4images_image_id: "23501"
_4images_cat_id: "1603"
_4images_user_id: "104"
_4images_image_date: "2009-03-24T00:03:24"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23501 -->
Hier sieht man im Detail, wie die Lenkgeometrie aufgebaut ist. In die Löcher der beiden BS15 mit Loch müssen Achsen angebracht werden, auf denen das Fahrwerk aufgehängt ist. Alles andere, insbesondere die Statikstreben, verdrehen sich beim Lenken und scheiden damit als Aufhängungspunkte aus.

Die mittleren roten Knubbel sind übrigens Seilrollen-Achsen.