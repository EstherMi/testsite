---
layout: "image"
title: "Gesamtansicht"
date: "2009-03-24T00:03:23"
picture: "angetriebenevorderachse1.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/23494
imported:
- "2019"
_4images_image_id: "23494"
_4images_cat_id: "1603"
_4images_user_id: "104"
_4images_image_date: "2009-03-24T00:03:23"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23494 -->
Das ist die nachträgliche Dokumentation zu der Vorderachse, die ich auf der Convention 2008 dabei hatte. Die Lenkung funktioniert nach dem Prinzip des Prototypen unter http://www.ftcommunity.de/categories.php?cat_id=1012, das heißt, die angetriebenen Räder drehen sich beim Lenken praktisch genau über ihrem Aufstandsmittelpunkt.

Die Achse ist hier auf einer Trägergruppe aufgesteckt, damit man sie überhaupt ausprobieren kann.