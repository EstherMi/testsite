---
layout: "comment"
hidden: true
title: "8830"
date: "2009-03-24T14:01:53"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ich hab gestern Abend noch etwas getüftelt wegen des Antriebs. Man kann zwar den Differentialantrieb von unten anführen, aber wie gesagt geht das auf die Bodenfreiheit. Ich habe aber eine viel einfachere Möglichkeit gefunden: Der PowerMotor wird direkt, ganz wie "üblich" an das Differential angesetzt. Er macht beim Lenken dann die leichte Drehbewegung des Differentials mit den schwarzen Streben mit, aber das stört nicht. Wenig Reibungsverluste resultieren aber, weil nicht über viele Ecken hinweg Drehmoment geleitet werden muss. Mal schauen, ob ich damit nicht ein Fahrzeug hinkriege.

Gruß,
Stefan