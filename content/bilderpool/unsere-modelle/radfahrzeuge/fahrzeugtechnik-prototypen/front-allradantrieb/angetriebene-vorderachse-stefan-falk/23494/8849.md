---
layout: "comment"
hidden: true
title: "8849"
date: "2009-03-27T16:56:34"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Jau, stimmt natürlich alles, Udo!

Zu 1.: Allerdings ist die Breitenschrumpfung beim Lenken für die Modellpraxis vernachlässigbar klein, das dürfte die Funktion überhaupt nicht beeinträchtigen.

Zu 2.: Stimmt, der Antrieb wie hier von oben ist ein Platzfresser und sowieso nicht kräftig, weil alles so filigran gebaut ist. Die neueren Fotos sollten da schon etwas weiter sein.

Zu 3.: Auf den neueren Fotos habe ich das in Statik-Flachträger aufgehängt. Das federt *etwas*, mit Druck nämlich etwa 1 cm. Da federn sowohl die Flachträger als auch die schwarzen Streben selbst.

Wie gesagt, mal sehen, was draus wird. Im Moment hab ich das Problem, dass alle anderen Federnocken und BS 7,5 in der Digitaluhr verbaut sind und ich also nicht weiterbauen kann. ;-)

Gruß,
Stefan