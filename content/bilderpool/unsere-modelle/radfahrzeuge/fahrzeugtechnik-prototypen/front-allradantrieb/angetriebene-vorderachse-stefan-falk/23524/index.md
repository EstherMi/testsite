---
layout: "image"
title: "Lenkung (2)"
date: "2009-03-26T21:25:13"
picture: "vorderachse5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/23524
imported:
- "2019"
_4images_image_id: "23524"
_4images_cat_id: "1603"
_4images_user_id: "104"
_4images_image_date: "2009-03-26T21:25:13"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23524 -->
In die andere Richtung gelenkt sieht man, wie die Räder sich tatsächlich im Wesentlichen nur drehen, aber kaum ihre Position verlassen.