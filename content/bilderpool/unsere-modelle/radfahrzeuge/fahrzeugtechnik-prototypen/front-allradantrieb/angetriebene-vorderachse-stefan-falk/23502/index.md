---
layout: "image"
title: "Antrieb"
date: "2009-03-24T00:03:24"
picture: "angetriebenevorderachse9.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/23502
imported:
- "2019"
_4images_image_id: "23502"
_4images_cat_id: "1603"
_4images_user_id: "104"
_4images_image_date: "2009-03-24T00:03:24"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23502 -->
Der Antrieb funktioniert, ist aber noch lange nicht ausgereift. Das Differential kommt mit seinen Zähnen schon mal an die Klemmringe, es baut noch viel zu hoch und ist noch zu filigran. Auch der schwarze BS15 im Vordergrund kann nicht für die Befestigung der Achse verwendet werden, sondern höchstens für die Lenkung: Der wird beim Lenken nach links bzw. rechts gedreht. Das ist das Problem bei diesem Antrieb: Außer den beiden Löchern der BS15 mit Loch (siehe vorherige Bilder) bleibt überhaupt nichts auch nur annähernd in fester Position.