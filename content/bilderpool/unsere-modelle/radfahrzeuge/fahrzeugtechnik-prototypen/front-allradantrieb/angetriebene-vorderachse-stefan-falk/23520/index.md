---
layout: "image"
title: "Bodenfreiheit"
date: "2009-03-26T21:25:12"
picture: "vorderachse1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/23520
imported:
- "2019"
_4images_image_id: "23520"
_4images_cat_id: "1603"
_4images_user_id: "104"
_4images_image_date: "2009-03-26T21:25:12"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23520 -->
Der Antrieb kommt so, dass weder die Bodenfreiheit leidet noch die Höhe die des Differentials selbst überschreitet.