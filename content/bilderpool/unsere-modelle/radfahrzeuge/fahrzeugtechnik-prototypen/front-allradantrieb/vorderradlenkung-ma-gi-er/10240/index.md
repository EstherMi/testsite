---
layout: "image"
title: "Kurbel für Antrieb"
date: "2007-04-30T18:47:23"
picture: "vorderradlenkung2.jpg"
weight: "2"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/10240
imported:
- "2019"
_4images_image_id: "10240"
_4images_cat_id: "929"
_4images_user_id: "445"
_4images_image_date: "2007-04-30T18:47:23"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10240 -->
Man kann auch einen Motor nehmen, ich empfele es sogar...