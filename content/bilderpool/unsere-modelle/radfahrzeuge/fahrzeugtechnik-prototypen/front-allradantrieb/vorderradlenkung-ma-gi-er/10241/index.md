---
layout: "image"
title: "Lenkstück oben"
date: "2007-04-30T18:47:24"
picture: "vorderradlenkung3.jpg"
weight: "3"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/10241
imported:
- "2019"
_4images_image_id: "10241"
_4images_cat_id: "929"
_4images_user_id: "445"
_4images_image_date: "2007-04-30T18:47:24"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10241 -->
Rechts die aufnahme der Lenkstange und in der mitte das Lenkjarnier.