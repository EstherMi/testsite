---
layout: "image"
title: "Bodenfreiheit"
date: "2015-03-05T23:55:35"
picture: "angetriebenevorderachsemitdifferentialundfederung2.jpg"
weight: "2"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/40613
imported:
- "2019"
_4images_image_id: "40613"
_4images_cat_id: "3047"
_4images_user_id: "2321"
_4images_image_date: "2015-03-05T23:55:35"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40613 -->
Die Bodenfreiheit ist nicht übel. Aber man sieht, das Ganze ist sehr Kleinteilaufwendig. Mit den Statik-Gelenklaschen bin ich an meiner Bevorratungsgrenze angestoßen. Man braucht pro Rad 8 Gelenklaschen. Die Kardangelenke sind nicht genau in der Ebene der Laschengelenke, aber das Rasachsensystem ist flexibel genug, das gut wegzustecken.