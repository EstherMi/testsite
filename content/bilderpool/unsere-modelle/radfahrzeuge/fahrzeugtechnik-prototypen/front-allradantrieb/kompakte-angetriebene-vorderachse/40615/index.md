---
layout: "image"
title: "Unterseite"
date: "2015-03-05T23:55:35"
picture: "angetriebenevorderachsemitdifferentialundfederung4.jpg"
weight: "4"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/40615
imported:
- "2019"
_4images_image_id: "40615"
_4images_cat_id: "3047"
_4images_user_id: "2321"
_4images_image_date: "2015-03-05T23:55:35"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40615 -->
Durch den Statikbaustein kommt das äußere Kardangelenk in die Radnabe hinein.