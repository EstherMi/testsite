---
layout: "image"
title: "Einzelrad Aufhängung"
date: "2008-10-17T16:31:51"
picture: "allrad2.jpg"
weight: "6"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/15999
imported:
- "2019"
_4images_image_id: "15999"
_4images_cat_id: "1453"
_4images_user_id: "558"
_4images_image_date: "2008-10-17T16:31:51"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15999 -->
Komplettansicht, wobei nur links eine Lenkung eingebaut ist.