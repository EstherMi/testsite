---
layout: "comment"
hidden: true
title: "7608"
date: "2008-10-17T17:04:35"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Schick. Wenn das Lenkteil da der Würfel eines alten Kardangelenkes ist, verstehe ich aber noch nicht, wie Du das auf der Achse (in Fahrtrichtung) festbekommen hast. Wie geht der Trick?

Gruß,
Stefan