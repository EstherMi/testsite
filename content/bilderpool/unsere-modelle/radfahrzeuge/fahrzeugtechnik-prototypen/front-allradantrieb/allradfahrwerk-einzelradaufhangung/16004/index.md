---
layout: "image"
title: "Spurbreite"
date: "2008-10-18T12:58:13"
picture: "verkuertzt1.jpg"
weight: "2"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/16004
imported:
- "2019"
_4images_image_id: "16004"
_4images_cat_id: "1453"
_4images_user_id: "558"
_4images_image_date: "2008-10-18T12:58:13"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16004 -->
Mit einfachen mitteln die Spurbreite verringert