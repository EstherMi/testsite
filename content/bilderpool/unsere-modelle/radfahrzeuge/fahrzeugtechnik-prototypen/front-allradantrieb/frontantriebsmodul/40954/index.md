---
layout: "image"
title: "Frontatrieb von hinten unten"
date: "2015-05-08T17:37:28"
picture: "frontantriebsmodul04.jpg"
weight: "4"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/40954
imported:
- "2019"
_4images_image_id: "40954"
_4images_cat_id: "3076"
_4images_user_id: "2321"
_4images_image_date: "2015-05-08T17:37:28"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40954 -->
Das ist die Ansicht von hinten unten. Die Antriebs-Ritzel werden noch durch ein stärker untersetztes Getribe ersetzt, das hier ist erst mal prorotypisch.