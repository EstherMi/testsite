---
layout: "image"
title: "Frontantriebn Lenkantrieb"
date: "2015-05-08T17:37:28"
picture: "frontantriebsmodul10.jpg"
weight: "10"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/40960
imported:
- "2019"
_4images_image_id: "40960"
_4images_cat_id: "3076"
_4images_user_id: "2321"
_4images_image_date: "2015-05-08T17:37:28"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40960 -->
Das Ritzel aus dem Winkelgetriebe und das Schneckenende ist, wie es so schön in den ft-Bauanleitungen steht, mit einem "Faden" in die schwarze Lagerbuchse geklemmt, damit die Verbindung gut hält - nein tatsächlich ist es ein Stück Plastikfolie. Die hat mehr Reibung. Die Schneckenmutter ist durch eine BS7,5 mit dem Winkelstein 15 verbunden. Der Motor ist etwas aus dem Raster verschoben für den späteren Stromanschluss.