---
layout: "image"
title: "Frontantrieb aufgeklappt von vorne"
date: "2015-05-08T17:37:28"
picture: "frontantriebsmodul08.jpg"
weight: "8"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/40958
imported:
- "2019"
_4images_image_id: "40958"
_4images_cat_id: "3076"
_4images_user_id: "2321"
_4images_image_date: "2015-05-08T17:37:28"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40958 -->
So, noch mal Ansicht von vorne.