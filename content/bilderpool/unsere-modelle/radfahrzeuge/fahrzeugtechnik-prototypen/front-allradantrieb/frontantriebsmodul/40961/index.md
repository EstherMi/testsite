---
layout: "image"
title: "Frontantrieb Differentialkäfig, Achsschenkel und Spurstange"
date: "2015-05-08T17:37:28"
picture: "frontantriebsmodul11.jpg"
weight: "11"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/40961
imported:
- "2019"
_4images_image_id: "40961"
_4images_cat_id: "3076"
_4images_user_id: "2321"
_4images_image_date: "2015-05-08T17:37:28"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40961 -->
Hier ist noch mal der Differentialkäfig und die Achsschenkel mit der Spurstange separiert. Der quer eingebaute BS30 ist nur zur Stabilisierung, sonst sitzt da der Antriebsmotor.