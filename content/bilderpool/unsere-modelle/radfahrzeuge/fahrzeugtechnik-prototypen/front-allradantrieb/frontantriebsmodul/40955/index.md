---
layout: "image"
title: "Frontantrieb von hinten oben"
date: "2015-05-08T17:37:28"
picture: "frontantriebsmodul05.jpg"
weight: "5"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/40955
imported:
- "2019"
_4images_image_id: "40955"
_4images_cat_id: "3076"
_4images_user_id: "2321"
_4images_image_date: "2015-05-08T17:37:28"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40955 -->
Ansicht von hinten oben. Einen (anderen) S-Riegel konnte ich wirklcih nicht von oben reinstecken und habe ihn von unten durch einen Verschlussriegel ersetzt.