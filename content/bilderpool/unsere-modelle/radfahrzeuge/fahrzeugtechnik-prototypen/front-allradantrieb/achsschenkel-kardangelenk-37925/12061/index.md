---
layout: "image"
title: "Achsschenkel 4"
date: "2007-10-02T08:32:54"
picture: "Achsschenkel_4.jpg"
weight: "4"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/12061
imported:
- "2019"
_4images_image_id: "12061"
_4images_cat_id: "1078"
_4images_user_id: "328"
_4images_image_date: "2007-10-02T08:32:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12061 -->
Die Schneckenmutter von innen. Hätte mir die Finger waschen sollen...