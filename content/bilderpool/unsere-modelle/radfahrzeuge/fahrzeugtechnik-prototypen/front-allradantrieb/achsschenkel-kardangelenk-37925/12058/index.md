---
layout: "image"
title: "Achsschenkel 1"
date: "2007-10-02T08:32:53"
picture: "Achsschenkel_1.jpg"
weight: "1"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/12058
imported:
- "2019"
_4images_image_id: "12058"
_4images_cat_id: "1078"
_4images_user_id: "328"
_4images_image_date: "2007-10-02T08:32:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12058 -->
Angeregt durch Ludgers grandiose Idee, die Schneckenmutter 37925 zur Lagerung der Rast-Kardangelenke zu nutzen, um den gesamten Achsschenkel kürzer bauen zu können,

http://www.ftcommunity.de/categories.php?cat_id=1074&page=2

habe ich mal versucht, wie sich das Ganze ohne Fremdteile (Kugellager) nur mit Original-FT-Teilen darstellen lässt. Man benötigt nur eine Riegelscheibe 36334 und zwei Abstandsringe 31597, und schon kann man wunderbar das krumme Maß der Differenzial-Wellen ausgleichen und dieses trotzdem stabil und im Raster lagern. Außen am Rad benötigt man dann aber doch eine konventionelle Lagerung...