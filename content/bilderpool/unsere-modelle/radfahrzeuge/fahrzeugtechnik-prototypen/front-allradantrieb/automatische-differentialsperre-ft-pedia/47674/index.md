---
layout: "image"
title: "Sperre für Mitteldifferential, eingebaut in MB-Trac 4"
date: "2018-05-18T18:41:09"
picture: "sperrefuermitteldifferential15.jpg"
weight: "15"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/47674
imported:
- "2019"
_4images_image_id: "47674"
_4images_cat_id: "3514"
_4images_user_id: "2321"
_4images_image_date: "2018-05-18T18:41:09"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47674 -->
