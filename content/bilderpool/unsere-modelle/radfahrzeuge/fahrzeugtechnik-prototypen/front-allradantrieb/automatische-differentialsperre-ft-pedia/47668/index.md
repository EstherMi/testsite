---
layout: "image"
title: "Sperre für Mitteldifferential 7"
date: "2018-05-18T18:40:13"
picture: "sperrefuermitteldifferential09.jpg"
weight: "9"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/47668
imported:
- "2019"
_4images_image_id: "47668"
_4images_cat_id: "3514"
_4images_user_id: "2321"
_4images_image_date: "2018-05-18T18:40:13"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47668 -->
Her sieht man was passiert, wenn der Verbinder bei Rückwärtsfahrt nicht das Überschlagen des Hebels verhindert.