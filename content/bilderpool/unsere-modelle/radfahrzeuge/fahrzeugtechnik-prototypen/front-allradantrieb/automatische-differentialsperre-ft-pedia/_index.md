---
layout: "overview"
title: "Automatische Differentialsperre aus der ft:pedia - es geht weiter"
date: 2019-12-17T18:43:48+01:00
legacy_id:
- categories/3514
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3514 --> 
In der ft:pedia   ( https://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2018-1.pdf )
und auf Youtube   ( https://www.youtube.com/watch?v=OPzr2LrQjrI )
hatte ich eine automatische Differentialsperre vorgestellt, zu der hier noch ein paar weitere Ideen hinzukommen.