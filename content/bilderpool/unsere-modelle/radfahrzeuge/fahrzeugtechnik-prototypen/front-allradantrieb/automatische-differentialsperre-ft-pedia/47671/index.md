---
layout: "image"
title: "Sperre für Mitteldifferential, eingebaut in MB-Trac 1"
date: "2018-05-18T18:41:08"
picture: "sperrefuermitteldifferential12.jpg"
weight: "12"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/47671
imported:
- "2019"
_4images_image_id: "47671"
_4images_cat_id: "3514"
_4images_user_id: "2321"
_4images_image_date: "2018-05-18T18:41:08"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47671 -->
Hier ist das Ganze mal eingebaut in meinen MB-Trac.
Siehe auch 
https://www.ftcommunity.de/categories.php?cat_id=3192&page=2
https://www.ftcommunity.de/details.php?image_id=43029