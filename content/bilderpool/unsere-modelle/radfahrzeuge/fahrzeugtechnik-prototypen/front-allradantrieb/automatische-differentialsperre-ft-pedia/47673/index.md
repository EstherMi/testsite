---
layout: "image"
title: "Sperre für Mitteldifferential, eingebaut in MB-Trac 3"
date: "2018-05-18T18:41:08"
picture: "sperrefuermitteldifferential14.jpg"
weight: "14"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/47673
imported:
- "2019"
_4images_image_id: "47673"
_4images_cat_id: "3514"
_4images_user_id: "2321"
_4images_image_date: "2018-05-18T18:41:08"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47673 -->
Die Kabel sind nicht sorgfältig verlegt, dazu hatte ich keine Lust mehr.