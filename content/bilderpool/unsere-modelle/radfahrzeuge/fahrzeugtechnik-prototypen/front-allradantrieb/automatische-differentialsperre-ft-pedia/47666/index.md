---
layout: "image"
title: "Sperre für Mitteldifferential 5"
date: "2018-05-18T18:40:12"
picture: "sperrefuermitteldifferential07.jpg"
weight: "7"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/47666
imported:
- "2019"
_4images_image_id: "47666"
_4images_cat_id: "3514"
_4images_user_id: "2321"
_4images_image_date: "2018-05-18T18:40:12"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47666 -->
Diese Situation ist hier mal "aufgeschnitten" dargestellt. Der Rollenblock mit seiner Riegelscheibe und die eine Hälfte des Hebels sind entfernt. Die Motorwelle dreht rechtsrum, das Rechendifferential mit seinen Wellen dreht linksrum. Der Hebel wir runtergedrückt und wirkt durch die Geometrie und die fest sitzende rechte Riegelscheibe als Sperrklinke für die Riegelscheibe auf der Welle. Linksrum kann sie sich drehen und rattert dabei unter der Sperrklinke durch.
Diese Situation bleibt erhalten, solange sich das hintere Z20 weniger als doppelt so schnell dreht wie das Rechendifferential, sich also die Vorderachse weniger als dreimal so schnell dreht wie die Hinterachse. Das ist genügend Reserve für Kurvenfahrten (da muss ich die Vorderachse wegen des größeren Kurvenradius schneller drehen können).

Erst wenn sich die Vorderachse mehr als dreimal so schnell dreht wie die Hinterachse (=Vorderachse dreht durch) ändert die Riegelscheibe auf der Welle die Drehrichtung, dreht sich also rechtsrum und verhakt sich aufgrund der Geometrie mit der festsitzenden Riegelscheibe. Die Sperrklinke sperrt, und damit wirkt die Differentialsperre. Genaugenommen ist es nur eine Differential"bremse", denn wenn das Drehmoment größer wird, rutscht dreht die Welle in der Rigelscheibe. Aber auf jeden Fall entsteht ein Bremsmoment, so dass mehr Leisutng an die Hinterachse geht.
