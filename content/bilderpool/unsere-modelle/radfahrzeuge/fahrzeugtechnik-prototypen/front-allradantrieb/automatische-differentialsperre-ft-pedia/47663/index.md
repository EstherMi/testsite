---
layout: "image"
title: "Sperre für Mitteldifferential 2"
date: "2018-05-18T18:40:12"
picture: "sperrefuermitteldifferential04.jpg"
weight: "4"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/47663
imported:
- "2019"
_4images_image_id: "47663"
_4images_cat_id: "3514"
_4images_user_id: "2321"
_4images_image_date: "2018-05-18T18:40:12"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47663 -->
Der Motor wirkt über das rote Z20 auf das Käfigzahnrad des (halb verdeckten) Mitteldifferentials unter dem Z20. Die Welle links unten (halb verdeckt) soll vom Mitteldifferential zur Hinterachse gehen.