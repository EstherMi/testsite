---
layout: "image"
title: "kleinere Fliehkraftbremse"
date: "2018-05-18T18:40:12"
picture: "sperrefuermitteldifferential02.jpg"
weight: "2"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/47661
imported:
- "2019"
_4images_image_id: "47661"
_4images_cat_id: "3514"
_4images_user_id: "2321"
_4images_image_date: "2018-05-18T18:40:12"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47661 -->
Mir war die Fliehkraftbremse zu sperrig - so ist sie kleiner. Das lose Ende des Kardangelenks soll sich abwinkeln und verhaken, wenn es sich schnell dreht.
Das funktioniert zwar prinzipiell, aber nicht zuverlässig genug. Ist also eher ein Flop :-(