---
layout: "image"
title: "Frontantrieb II, Lenkeinschlag 3"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul12.jpg"
weight: "42"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/42586
imported:
- "2019"
_4images_image_id: "42586"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42586 -->
Bei maximalem Lenkeinschlag biegt sich die innere BSB-Pleuelstange, wird dadurch kürzer und macht aus dem Lenktrapez leider wieder ein Parallelogramm. Das Biegen verkraften sie aber ganz gut. Der Lenkrollradius ist sehr gering, weil sich die Lenkachsen innerhalb der Räder befinden.
