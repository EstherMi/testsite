---
layout: "image"
title: "Frontantrieb II, Achsaufhängung"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul36.jpg"
weight: "66"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/42610
imported:
- "2019"
_4images_image_id: "42610"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42610 -->
Das Gelenk zur Aufhängung der Vorderachs hat mir echte Kopfzerbrechen bereitet, Im Drehpunkt muss das Kardangelenk sitzen, es muss sich um die Fahrzeugquer- und -längsachse drehen können, gleichzeitig aber um die Fahrzeughochachse und gegen längsverschiebung möglichst steif sein und zu guter Letzt muss es auch noch schmal sein. Tja, so sind langer Zeit mal wieder die Hobby-Adapter zum Einsatz gekommen, und - welch Wunder - siehe da, es gibt sogar Teile wie die Klemmfkontakte, die man damit kombinieren kann.