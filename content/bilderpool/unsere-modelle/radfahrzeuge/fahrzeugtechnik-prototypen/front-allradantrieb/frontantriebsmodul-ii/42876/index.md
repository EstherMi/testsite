---
layout: "image"
title: "Frontantrieb II verbessert, Lenkung 5"
date: "2016-02-15T17:21:21"
picture: "96_verbesserte_Lenkung_5.jpg"
weight: "16"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/42876
imported:
- "2019"
_4images_image_id: "42876"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2016-02-15T17:21:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42876 -->
Hier muss man ganz genau hinsehen: Die Metallstange sitz etwas schräg zum Differential. Von rechts nach links wird der Abstand etwas größer. Das ist notwendig, damit der Differential-Zahnkranz nicht die Stange berührt. Erreicht habe ich das durch die Verwendung einer Schubstange, deren dünnes Ende etwas exzentrisch im Loch des Achsadapters mit dem Federnocken befestigt ist. Hier kommt's echt auf Millimeter-Bruchteile an.