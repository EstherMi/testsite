---
layout: "image"
title: "Frontantrieb II, Radaufhängungs-Variante 2"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul40.jpg"
weight: "70"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/42614
imported:
- "2019"
_4images_image_id: "42614"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42614 -->
So, das war's erst mal. Ein Geländewagen ist in Vorbereitung, mir schwebt da ein MB-Trac vor. Mal schauen ob das alles im Praxiseinsatz funktioniert.