---
layout: "image"
title: "Frontantrieb II, Lenkeinschlag 4"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul13.jpg"
weight: "43"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/42587
imported:
- "2019"
_4images_image_id: "42587"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42587 -->
Ich habe auf der im Bild linken Seite Winkelsteine 60° mit drei Nuten verbaut. Dadurch fällt außen ein Zapfen weg und der Lenkeinschlag ist noch ein wenig größer. Ich werde die rechte Seite auch noch umbauen.