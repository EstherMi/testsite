---
layout: "image"
title: "Frontantrieb II, Radaufhängung 3"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul16.jpg"
weight: "46"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/42590
imported:
- "2019"
_4images_image_id: "42590"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42590 -->
Makrofotografie ist nicht so einfach - vor allem für einen Foto-Laien wie mich. Immerhin hat die Sonne schön geschienen :-)