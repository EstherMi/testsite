---
layout: "image"
title: "Frontantrieb II, Versatzgetriebe 4"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul22.jpg"
weight: "52"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/42596
imported:
- "2019"
_4images_image_id: "42596"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42596 -->
Wie gesagt, die roten V-Achsen muss man sich wegdenken, im Reifen sitzen die Klemmen weiter außen und halten nur durch die Anpresskraft der beiden Innenzahnräder.