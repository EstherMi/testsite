---
layout: "image"
title: "Frontantrieb II, Servo-Montage 1"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul37.jpg"
weight: "67"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/42611
imported:
- "2019"
_4images_image_id: "42611"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42611 -->
Der Servo wird nur von den Kunststoffkurbeln gehalten. hoffentlich reicht das im Alltagsbetrieb.