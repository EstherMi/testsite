---
layout: "image"
title: "Frontantrieb II, Versatzgetriebe 2"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul20.jpg"
weight: "50"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/42594
imported:
- "2019"
_4images_image_id: "42594"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42594 -->
Das schwarze Z10 stammt aus dem neuen Winkelgetriebe mit K-Achsen. Es ist schrägverzahnt, was nicht besonders gut mit den graden Zähnen des Innenzahnrads zusammen passt. Aber es geht, nur auch hier ist kein Dauerbetrieb angesagt... obwohl - bis die Zähne so http://www.ftcommunity.de/details.php?image_id=11940 aussehen, gehen schon ein paar Kilometer im Gelände ;-)