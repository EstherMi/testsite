---
layout: "image"
title: "Frontantrieb II verbessert, Radaufhängung 3"
date: "2016-01-27T21:37:09"
picture: "70_verbesserte_Radaufhngung_3.jpg"
weight: "19"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/42823
imported:
- "2019"
_4images_image_id: "42823"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2016-01-27T21:37:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42823 -->
Im Reifen wird die Sternlasche durch K-Achsen zentriert.