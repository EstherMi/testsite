---
layout: "image"
title: "Frontantrieb II, Unterseite 2"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul09.jpg"
weight: "39"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/42583
imported:
- "2019"
_4images_image_id: "42583"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42583 -->
Das Kardangelenk wiederum ist in einem U-Stein gelagert, der zusammen mit den Klemmkontakten und den Hobby-Verbindern das Gelenk für die Achsaufhängung bildet (dazu später mehr). Das Kardangelenk ist an der offenen Seite des U-Steins mit einem Verbinder 15 und einer Bauplatte 15x15 gesichert. Die Bauplatte ist in diesem Bild entfernt und der Verbinder hochgeklappt.