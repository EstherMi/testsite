---
layout: "image"
title: "Frontantrieb II, Zahnrad 4"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul35.jpg"
weight: "65"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/42609
imported:
- "2019"
_4images_image_id: "42609"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42609 -->
Noch mal von vorne. Links kann man den Pneumatikschlauch und den Tesafilm erahnen.