---
layout: "image"
title: "Frontantrieb II, Zahnrad 3"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul34.jpg"
weight: "64"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/42608
imported:
- "2019"
_4images_image_id: "42608"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42608 -->
Ich habe mich aber für die obere Variante entschieden, weil ft-pur und ohne Tesafilm.