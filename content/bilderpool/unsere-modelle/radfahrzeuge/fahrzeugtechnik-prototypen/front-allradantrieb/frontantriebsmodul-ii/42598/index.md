---
layout: "image"
title: "Frontantrieb II, Versatzgetriebe 6"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul24.jpg"
weight: "54"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/42598
imported:
- "2019"
_4images_image_id: "42598"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42598 -->
Als Spurstande dienen zwei Pleulstanden der BSB. Sie sind am Achsschenkel mit einem Förderglied befestigt.