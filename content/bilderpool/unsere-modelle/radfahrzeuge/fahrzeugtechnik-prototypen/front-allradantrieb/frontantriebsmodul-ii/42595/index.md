---
layout: "image"
title: "Frontantrieb II, Versatzgetriebe 3"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul21.jpg"
weight: "51"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/42595
imported:
- "2019"
_4images_image_id: "42595"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42595 -->
Hier kann man sehen, wie das Lagerstück1 in der Höhe verstellt ist, damit das Radlager wenig Spiel hat. Je weiter unten das Lagerstück sitzt, desto strammer ist das Radlager und das Z10 und das Innenzahrad werden stärker zusammengepresst.