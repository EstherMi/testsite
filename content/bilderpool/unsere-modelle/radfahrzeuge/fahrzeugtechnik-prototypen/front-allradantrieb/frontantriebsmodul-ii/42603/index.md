---
layout: "image"
title: "Frontantrieb II, Radaufhängungs-Details 5"
date: "2015-12-27T10:20:57"
picture: "frontantriebsmodul29.jpg"
weight: "59"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/42603
imported:
- "2019"
_4images_image_id: "42603"
_4images_cat_id: "3166"
_4images_user_id: "2321"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42603 -->
Das Förder-kettenglied, welches auf dieser Seite die Kette an der Gelenkwürfel-Klaue fixiert, dient zusätzlich als Aufhängung der Spurstange.