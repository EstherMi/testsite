---
layout: "image"
title: "Prototyp einer LKW-Vorderachse"
date: "2017-06-19T21:31:26"
picture: "prototypeinerlkrvorderachse1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/45994
imported:
- "2019"
_4images_image_id: "45994"
_4images_cat_id: "922"
_4images_user_id: "104"
_4images_image_date: "2017-06-19T21:31:26"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45994 -->
Das hier liegt schon seit dermaßen vielen Monaten hier rum, dass ich es mal veröffentliche. Es soll noch eine weiche Gummi-Federung mit automatischem Niveauausgleich ähnlich wie in https://www.ftcommunity.de/details.php?image_id=7091 dran, aber die Versuche dazu sind noch gar nicht nicht zufriedenstellend.

Ans Differential kann ein PowerMotor dran, und durch Absenken zweier Zahnräder auf derselben Achse auf die Kette würde man eine Differentialsperre erhalten.

Die Radaufhängung ist ein Zwitter zwischen https://www.ftcommunity.de/details.php?image_id=40830 und https://www.ftcommunity.de/details.php?image_id=33043. Den Federweg würde ich mir größer wünschen, vor allem bei eingeschlagener Lenkung. Mal sehen, ob aus diesem Teil mal noch was Fertiges wird.