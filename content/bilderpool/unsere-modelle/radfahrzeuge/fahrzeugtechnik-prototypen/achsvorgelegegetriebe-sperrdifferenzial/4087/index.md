---
layout: "image"
title: "20050422 Fischertechnik Ackerschlepper mit Antrieb 13"
date: "2005-04-25T20:35:23"
picture: "20050422_Fischertechnik_Ackerschlepper_mit_Antrieb_13.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/4087
imported:
- "2019"
_4images_image_id: "4087"
_4images_cat_id: "349"
_4images_user_id: "5"
_4images_image_date: "2005-04-25T20:35:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4087 -->
