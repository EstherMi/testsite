---
layout: "image"
title: "Unterseite des Raketenautos"
date: "2018-06-02T21:00:26"
picture: "Unterseite.jpg"
weight: "2"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Thomas Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- details/47686
imported:
- "2019"
_4images_image_id: "47686"
_4images_cat_id: "3516"
_4images_user_id: "1088"
_4images_image_date: "2018-06-02T21:00:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47686 -->
Die Unterseite des Fahrzeugs. Natürlich darf man die Achsen mit den Klemmbuchsen nicht festklemmen, sondern muss den Achsen etwas Spiel nach links und rechts erlauben. Die Reifen 45 haben sehr gute Laufeigenschaften. Die neueren, breiteren Reifen haben mehr Grip, aber das ist hier nicht gewünscht!