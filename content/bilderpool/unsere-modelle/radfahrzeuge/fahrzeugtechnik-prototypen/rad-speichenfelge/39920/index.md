---
layout: "image"
title: "Rad mit Achsen"
date: "2014-12-13T19:42:17"
picture: "radimalufelgenlook2.jpg"
weight: "2"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/39920
imported:
- "2019"
_4images_image_id: "39920"
_4images_cat_id: "2998"
_4images_user_id: "2321"
_4images_image_date: "2014-12-13T19:42:17"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39920 -->
Auf den Lenk-Gelenkwürfel passt das Rad super. Es läuft sehr leicht, hat wenig Spiel und hält ganz gut. Schwierig ist aber eine Befestigung auf einer angetriebenen Achse. Die Seiltrommel klemmt zwar, aber große Momente kann man so nicht übertragen. Wahrscheinlich könnte man mit einem stramm aufgewickelten Seil den Halt deutlich verbessern.