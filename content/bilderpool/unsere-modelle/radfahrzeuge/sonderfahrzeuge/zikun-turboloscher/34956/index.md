---
layout: "image"
title: "14 Steuerpult"
date: "2012-05-15T22:00:23"
picture: "turboloescher11.jpg"
weight: "11"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/34956
imported:
- "2019"
_4images_image_id: "34956"
_4images_cat_id: "2585"
_4images_user_id: "860"
_4images_image_date: "2012-05-15T22:00:23"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34956 -->
Das ist das Steuerpult