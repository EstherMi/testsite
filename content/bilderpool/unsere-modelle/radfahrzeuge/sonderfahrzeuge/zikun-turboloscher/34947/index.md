---
layout: "image"
title: "05 Tankbefüllung"
date: "2012-05-15T22:00:23"
picture: "turboloescher02.jpg"
weight: "2"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/34947
imported:
- "2019"
_4images_image_id: "34947"
_4images_cat_id: "2585"
_4images_user_id: "860"
_4images_image_date: "2012-05-15T22:00:23"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34947 -->
Die zwei Ventile lassen sich aus der Seite rausziehen. 
Das obere Ventil gibt eine Verbindung zum Wassertank, hier kann ein externer Tank angeschlossen werden. Es ist aber geschlossen, damit nichts entweicht.
Das untere Ventil öffnet oder schließt die Verbindung vom Drucklufttank zum Wassertank. Das ist wichtig, damit die Kompressoren die Luft in den externen Tank pumpen können und somit das Wasser in den Wassertank gepumpt wird.