---
layout: "image"
title: "MultifunktionsFahrzeug von vorne"
date: "2010-08-20T09:27:26"
picture: "MF_von_vorne_2.jpg"
weight: "2"
konstrukteure: 
- "lars"
fotografen:
- "lars"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lars"
license: "unknown"
legacy_id:
- details/27830
imported:
- "2019"
_4images_image_id: "27830"
_4images_cat_id: "1704"
_4images_user_id: "1177"
_4images_image_date: "2010-08-20T09:27:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27830 -->
diesmal die Stoß-stange
mit lampen