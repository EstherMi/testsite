---
layout: "image"
title: "Gesamtansicht"
date: "2009-08-13T20:03:29"
picture: "erkundungsauto02.jpg"
weight: "7"
konstrukteure: 
- "Laurens Wagner"
fotografen:
- "Laurens Wagner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Laurens"
license: "unknown"
legacy_id:
- details/24771
imported:
- "2019"
_4images_image_id: "24771"
_4images_cat_id: "1704"
_4images_user_id: "987"
_4images_image_date: "2009-08-13T20:03:29"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24771 -->
Gesamtansicht