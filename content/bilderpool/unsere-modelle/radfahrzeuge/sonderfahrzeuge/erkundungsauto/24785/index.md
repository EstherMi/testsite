---
layout: "image"
title: "Von oben"
date: "2009-08-13T20:03:44"
picture: "erkundungsauto16.jpg"
weight: "21"
konstrukteure: 
- "Laurens Wagner"
fotografen:
- "Laurens Wagner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Laurens"
license: "unknown"
legacy_id:
- details/24785
imported:
- "2019"
_4images_image_id: "24785"
_4images_cat_id: "1704"
_4images_user_id: "987"
_4images_image_date: "2009-08-13T20:03:44"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24785 -->
Alles nochmal von oben