---
layout: "image"
title: "Rad"
date: "2010-05-27T12:28:04"
picture: "dreiraedigerseitlichfahrer3.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kilo70"
license: "unknown"
legacy_id:
- details/27303
imported:
- "2019"
_4images_image_id: "27303"
_4images_cat_id: "1960"
_4images_user_id: "1071"
_4images_image_date: "2010-05-27T12:28:04"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27303 -->
Antrieb:
Die Kette dreht das obere waagerechte Zahnrad. 
Die Welle läuft durch die beiden roten Bausteine mit Bohrung dann durch den roten Drehteller mit dem aufgesetzten Zahnrad.
Über Rastkegelräder wird das aussen sichtbare Z10 Zahnradpaar angetrieben, wobei auf der unteren Achse das Rad sitzt.
Das ist nötig damit das Rad auf der Stelle dreht wenn gelenkt wird.

Lenkung:
ein Z10 (hier nicht sichtbar) greift in die seitliche Verzahnung des Drehtellers.