---
layout: "image"
title: "von oben"
date: "2010-05-27T12:28:05"
picture: "dreiraedigerseitlichfahrer6.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kilo70"
license: "unknown"
legacy_id:
- details/27306
imported:
- "2019"
_4images_image_id: "27306"
_4images_cat_id: "1960"
_4images_user_id: "1071"
_4images_image_date: "2010-05-27T12:28:05"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27306 -->
auf der Metalachsebe neben dem Fahrersitz sitzt die Schnecke.
eeeeeigentlich sollte auf diese Achse noch ein senkrecht stehendes Differential und der Motor sollte dieses dann antreiben.

Die Idee war:
Eine Seite des Differentials: der Lenkantrieb.
Andere Seite: eine Art "drehender Käfig" um das gesamte Fahrzeug.
Wenn es nun an eine Wand stösst, wird der drehende Käfig blockier -> die Kraft wird in die Lenkung geleitet -> die Räder verändern ihre Stellung -> das Fahrzeug entfernt sich vom Hindernis.

Problem:
Stabiliät, der Käfig hat sich zu schnell gedreht, durch die Dreiecksform konnte man nicht richtig "im Raster bauen", es hätte alles grösser gemusst...