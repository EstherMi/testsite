---
layout: "image"
title: "jauchzender Fahrer"
date: "2010-05-27T12:28:04"
picture: "dreiraedigerseitlichfahrer2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kilo70"
license: "unknown"
legacy_id:
- details/27302
imported:
- "2019"
_4images_image_id: "27302"
_4images_cat_id: "1960"
_4images_user_id: "1071"
_4images_image_date: "2010-05-27T12:28:04"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27302 -->
Die umlaufende Kette treibt die Räder an und ist TÜV Kritikpunkt Nr 1, da sie das einsteigen etwas gefährlich macht ;)
Die Drehteller dienen als Hohlwelle/Freilaubnabe.