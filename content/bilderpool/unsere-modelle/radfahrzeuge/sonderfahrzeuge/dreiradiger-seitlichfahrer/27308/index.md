---
layout: "image"
title: "ein Rad von oben"
date: "2010-05-27T12:28:06"
picture: "dreiraedigerseitlichfahrer8.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kilo70"
license: "unknown"
legacy_id:
- details/27308
imported:
- "2019"
_4images_image_id: "27308"
_4images_cat_id: "1960"
_4images_user_id: "1071"
_4images_image_date: "2010-05-27T12:28:06"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27308 -->
dieser S-Motor treibt die Kette an.
An dieser Ecke sitzt im Drehteller der obere Teil einer Nabe mit einer dieser roten Hülsen auf der Achse. An den anderen Ecken ist nur das "Unterteil" auf der Achse. Funktioniert beides, unten hält ja auch noch ein Bausteil mit Loch die Achse fest. Am besten wären natürlich richtige Freilaufnaben.

Naja, das wars eigentlich auch schon. Auch wenn die Idee sicher nicht neu ist, war so ein Fahrzeug glaube ich hier noch nicht hochgeladen.
Gerade als 4-Rad ist da sicher noch Potential. Man könnte auch die Räder "gegeneinander" lenken lassen, so dass sich das Fahrzeug mit eingeschlagenen Rädern auf der Stelle dreht.