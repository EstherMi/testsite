---
layout: "comment"
hidden: true
title: "11597"
date: "2010-05-27T17:09:23"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Nur falls Du das nicht kennen solltest: Eine richtige Freilaufnabe gibt es tatsächlich von ft:

http://www.knobloch-gmbh.de/shop/query.php?cp_tpl=5504&cp_pid=21127

http://www.ft-datenbank.de/show?show=bauteil_details&id=2085

Gruß,
Stefan