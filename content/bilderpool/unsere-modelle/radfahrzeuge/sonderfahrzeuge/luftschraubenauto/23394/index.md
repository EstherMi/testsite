---
layout: "image"
title: "Luftschraubenauto (3)"
date: "2009-03-06T19:34:35"
picture: "luftschraubenauto3.jpg"
weight: "4"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- details/23394
imported:
- "2019"
_4images_image_id: "23394"
_4images_cat_id: "1595"
_4images_user_id: "592"
_4images_image_date: "2009-03-06T19:34:35"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23394 -->
Von oben sieht man, dass die Luftschrauben leicht nach innen gedreht sind. Damit lassen sich engere Kurven fahren.