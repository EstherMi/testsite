---
layout: "image"
title: "Luftschraubenauto (2)"
date: "2009-03-06T19:34:35"
picture: "luftschraubenauto2.jpg"
weight: "3"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- details/23393
imported:
- "2019"
_4images_image_id: "23393"
_4images_cat_id: "1595"
_4images_user_id: "592"
_4images_image_date: "2009-03-06T19:34:35"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23393 -->
Von der Seite.

Die Kamera vorne am Auto ist eine kleine Funkkamera. Es macht einen Riesenspaß damit am Fernseher durchs Haus zu fahren. Leider ist die Reichweite durch die IR-Fernbedienung doch sehr begrenzt. Toll wäre doch mal eine Bluetooth-Fernbedienung (auch für den neuen TX).