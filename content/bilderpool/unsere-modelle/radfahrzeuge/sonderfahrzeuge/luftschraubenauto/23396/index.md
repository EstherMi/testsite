---
layout: "image"
title: "Luftschraubenauto (5)"
date: "2009-03-06T19:34:35"
picture: "luftschraubenauto5.jpg"
weight: "6"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- details/23396
imported:
- "2019"
_4images_image_id: "23396"
_4images_cat_id: "1595"
_4images_user_id: "592"
_4images_image_date: "2009-03-06T19:34:35"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23396 -->
Unten sieht man das möglichst leichtgängig gebaute Fahrwerk.