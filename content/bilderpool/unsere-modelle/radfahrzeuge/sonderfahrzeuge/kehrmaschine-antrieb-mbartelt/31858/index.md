---
layout: "image"
title: "Lenkung der Kehrmaschine"
date: "2011-09-18T17:54:55"
picture: "kehrmaschine06.jpg"
weight: "12"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- details/31858
imported:
- "2019"
_4images_image_id: "31858"
_4images_cat_id: "2376"
_4images_user_id: "936"
_4images_image_date: "2011-09-18T17:54:55"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31858 -->
Hier die Ansicht von unten um zu zeigen, wie die Lenkung realisiert wurde.