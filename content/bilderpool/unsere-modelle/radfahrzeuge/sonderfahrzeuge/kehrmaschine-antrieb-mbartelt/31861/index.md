---
layout: "image"
title: "Seitenansicht"
date: "2011-09-18T17:54:55"
picture: "kehrmaschine09.jpg"
weight: "15"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- details/31861
imported:
- "2019"
_4images_image_id: "31861"
_4images_cat_id: "2376"
_4images_user_id: "936"
_4images_image_date: "2011-09-18T17:54:55"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31861 -->
Über den Schalter wird die Stromzuführung ein/ausgeschaltet.