---
layout: "image"
title: "Vordere Reinigungsbürste"
date: "2011-09-18T17:54:55"
picture: "kehrmaschine02.jpg"
weight: "8"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- details/31854
imported:
- "2019"
_4images_image_id: "31854"
_4images_cat_id: "2376"
_4images_user_id: "936"
_4images_image_date: "2011-09-18T17:54:55"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31854 -->
Ansicht und Antrieb der vorderen Reinigungsbürste. Die Haltekonstruktion ist etwas wackelig und der Antreib per Gummiband hinterläßt auch einen nicht so guten Eindruck.