---
layout: "overview"
title: "Kehrmaschine mit Antrieb (mbartelt)"
date: 2019-12-17T18:51:14+01:00
legacy_id:
- categories/2376
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2376 --> 
Hier wurde versucht in das Modell Nr. 5 aus dem Baukasten "Kehrmaschinen", Art.Nr. 500878 einen Antriebsmotor, den Servo für die Lenkung, das Light & Sound Mudul sowie einen Akku einzubauen. Da meine Sammlung überwiegend aus Teilen der 70er Kästen stammt und besteht, sind alle Ergänzugen mit grauen Steinen vorgenommen worden.