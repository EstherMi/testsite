---
layout: "image"
title: "monowheel_1"
date: "2013-09-13T11:07:02"
picture: "monowheel_1_2.jpg"
weight: "2"
konstrukteure: 
- "steve"
fotografen:
- "steve"
keywords: ["Monowheel"]
uploadBy: "steme"
license: "unknown"
legacy_id:
- details/37377
imported:
- "2019"
_4images_image_id: "37377"
_4images_cat_id: "2234"
_4images_user_id: "1752"
_4images_image_date: "2013-09-13T11:07:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37377 -->
Hier der Prototyp eines sogenannten Monowheel. 
Das Wheel wird angetrieben von einer einachsigen 'Laufkatze' (ich nenn das einfach mal so), mit 2 Antriebs (doppel) rädern, die das Wheel beim Beschleunigen nach vorn drückt, und so für den Vortrieb sorgt. Ausserdem ist ein 2-Gang Getriebe eingebaut, inklusive Leerlauf. Damait kann das Gerät, einmal in Fahrt, auch  frei weiterrollen..
Der einzige Kontakt der Laufkatze mit dem Wheel sind die beiden Antriebsräder. Das Teil kann also frei nach vorn und hinten kippen, unten ist ist auch genug Platz dafür da.

Im Prinzip sollten mit so einem Teil - zumindest für FT-Modell 
Verhältnisse - hohe, bis sogar sehr hohe Geschwindigkeiten erreicht werden können, die Laufkatze läuft ja  quasi 'auf Schienen', resp. führt seine Schienen  immer gleich mit sich ..