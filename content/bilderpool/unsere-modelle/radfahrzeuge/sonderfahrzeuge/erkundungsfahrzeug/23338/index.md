---
layout: "image"
title: "Federung 2"
date: "2009-03-02T18:07:36"
picture: "erkundungsfahrzeug09.jpg"
weight: "11"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/23338
imported:
- "2019"
_4images_image_id: "23338"
_4images_cat_id: "1586"
_4images_user_id: "845"
_4images_image_date: "2009-03-02T18:07:36"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23338 -->
