---
layout: "overview"
title: "Erkundungsfahrzeug"
date: 2019-12-17T18:51:01+01:00
legacy_id:
- categories/1586
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1586 --> 
Dieses Erkundungsfahrzeug ist vollgefedert, hat eine Greifzange und eine Kamerahalterung um Bilder zu schießen.