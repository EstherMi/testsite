---
layout: "comment"
hidden: true
title: "8652"
date: "2009-03-03T08:48:10"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Schönes Modell, und tolle Idee! Aber die Radaufhängung ist kinematisch nicht optimal. Beim Einfedern lenken die Räder automatisch nach außen. Das ist selten gewollt. Längslenker vorn (wie an der Hinterachse) wären besser.

Ein Auslöser für die Kamera wäre technisch eigentlich kein Problem, oder? Platz dazu ist genug da. Und an der Fernbedienung ist ja noch 1 Kanal frei. Einfach einen weiteren Mini-Motor so anbringen, dass er die Kamera auslöst.

Dann kannst Du heimlich in das Zimmer Deiner pubertierenden Schwester fahren und unbemerkt... *zensur* ;o)

Gruß, Thomas