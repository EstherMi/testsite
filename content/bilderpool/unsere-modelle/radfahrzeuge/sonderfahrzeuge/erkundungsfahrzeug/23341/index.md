---
layout: "image"
title: "Vorderräder"
date: "2009-03-02T18:07:37"
picture: "erkundungsfahrzeug12.jpg"
weight: "14"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/23341
imported:
- "2019"
_4images_image_id: "23341"
_4images_cat_id: "1586"
_4images_user_id: "845"
_4images_image_date: "2009-03-02T18:07:37"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23341 -->
