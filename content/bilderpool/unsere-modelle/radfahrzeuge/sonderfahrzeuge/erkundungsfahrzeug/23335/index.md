---
layout: "image"
title: "Kamera"
date: "2009-03-02T18:07:36"
picture: "erkundungsfahrzeug06.jpg"
weight: "8"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/23335
imported:
- "2019"
_4images_image_id: "23335"
_4images_cat_id: "1586"
_4images_user_id: "845"
_4images_image_date: "2009-03-02T18:07:36"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23335 -->
