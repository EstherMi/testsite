---
layout: "image"
title: "monowheel_7"
date: "2013-09-13T11:07:03"
picture: "monowheel_7.jpg"
weight: "7"
konstrukteure: 
- "steve"
fotografen:
- "steve"
keywords: ["monowheel", "schaltung", "gangschaltung", "getriebe"]
uploadBy: "steme"
license: "unknown"
legacy_id:
- details/37384
imported:
- "2019"
_4images_image_id: "37384"
_4images_cat_id: "2780"
_4images_user_id: "1752"
_4images_image_date: "2013-09-13T11:07:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37384 -->
Das 2 Gang Getriebe wird durch über das Hubgetriebe geschaltet, indem die Achse vorn im Bild verschoben wird. In Mittelstellung ist der Leerlauf. Weiter hat es 2 Tastern für die Endstellungen.