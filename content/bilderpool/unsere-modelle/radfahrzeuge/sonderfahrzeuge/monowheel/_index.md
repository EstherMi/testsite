---
layout: "overview"
title: "monowheel"
date: 2019-12-17T18:51:24+01:00
legacy_id:
- categories/2780
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2780 --> 
Fahrzeug bestehend aus einem einzigen Rad. Antrieb, Schaltung etc: alles integriert!