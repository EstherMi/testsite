---
layout: "image"
title: "Langstreckenrennwagen"
date: "2016-04-02T17:06:56"
picture: "lsrph1.jpg"
weight: "1"
konstrukteure: 
- "Patrick"
fotografen:
- "Patrick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/43222
imported:
- "2019"
_4images_image_id: "43222"
_4images_cat_id: "3210"
_4images_user_id: "2228"
_4images_image_date: "2016-04-02T17:06:56"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43222 -->
Dieser Rennwagen wird mit einem XM-Motor betrieben. Alles ist über das Fernsteuerungsmodul fernsteuerbar.