---
layout: "overview"
title: "Langstreckenrennwagen"
date: 2019-12-17T18:50:24+01:00
legacy_id:
- categories/3210
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3210 --> 
Modell eines Langstreckenrennwagens beim 24-Stundenrennen von Le Mans. Ich habe das Modell bereits am Fischertechnik Fanclubtag 2015 ausgestellt.