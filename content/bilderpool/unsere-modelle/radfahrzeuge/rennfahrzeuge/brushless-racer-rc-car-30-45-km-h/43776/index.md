---
layout: "image"
title: "Und an seinem eigentlichen Einsatzort"
date: "2016-06-17T13:47:48"
picture: "brushlessracerrccarkmhmitfischertechnik6.jpg"
weight: "7"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- details/43776
imported:
- "2019"
_4images_image_id: "43776"
_4images_cat_id: "3240"
_4images_user_id: "1582"
_4images_image_date: "2016-06-17T13:47:48"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43776 -->
