---
layout: "image"
title: "Bodenfreiheit - wozu Bodenfreiheit?"
date: "2014-03-26T10:34:33"
picture: "rapidracer05.jpg"
weight: "5"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- details/38493
imported:
- "2019"
_4images_image_id: "38493"
_4images_cat_id: "2874"
_4images_user_id: "1582"
_4images_image_date: "2014-03-26T10:34:33"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38493 -->
Die neuen Reifen (super!) geben dem Racer genau den richtigen Bodenabstand zum Rumflitzen. Bei größeren Reifen ist die Abstimmung zwischen Motor/Getriebe und Stromzug der Motoren (der arme Empfänger!) nicht mehr optimal und die Kiste rattert und steht mehr mit blinkender LED, als sie fährt. Ein bißchen ein stärkerer Empfänger, weiter versteiftes Getriebe und die Kiste wäre noch fixer. Naja, so ist er aber auch schon recht flink.