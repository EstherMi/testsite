---
layout: "image"
title: "Racer Aufsicht"
date: "2014-03-26T10:34:33"
picture: "rapidracer06.jpg"
weight: "6"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- details/38494
imported:
- "2019"
_4images_image_id: "38494"
_4images_cat_id: "2874"
_4images_user_id: "1582"
_4images_image_date: "2014-03-26T10:34:33"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38494 -->
Enger Radstand und maximale Lenkwinkel mit tiefem Schwerpunkt machen aus der Kiste den Racer