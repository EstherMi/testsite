---
layout: "image"
title: "Getriebekasten 2"
date: "2014-03-26T10:34:33"
picture: "rapidracer10.jpg"
weight: "10"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- details/38498
imported:
- "2019"
_4images_image_id: "38498"
_4images_cat_id: "2874"
_4images_user_id: "1582"
_4images_image_date: "2014-03-26T10:34:33"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38498 -->
Ich hoffe man kann das in den folgenden Bildern gut erkennen. Die Motoren sind mit allen möglichen Ankopplungen versteift, trotzdem hat man von hinten vollen Zugriff auf das Getriebe, falls mal etwas ausgewechselt werden muss. Die Zahnrädchen machen den Stress erstaunlich gut mit.