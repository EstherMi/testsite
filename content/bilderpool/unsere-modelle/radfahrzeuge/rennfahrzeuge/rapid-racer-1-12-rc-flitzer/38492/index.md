---
layout: "image"
title: "Rapid Racer hinten"
date: "2014-03-26T10:34:33"
picture: "rapidracer04.jpg"
weight: "4"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- details/38492
imported:
- "2019"
_4images_image_id: "38492"
_4images_cat_id: "2874"
_4images_user_id: "1582"
_4images_image_date: "2014-03-26T10:34:33"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38492 -->
Der Empfänger ist vielleicht noch etwas lieblos angepappt - aber Optik ist mein Ding hier nicht. So hat er ganz guten Empfang in allen Lagen.