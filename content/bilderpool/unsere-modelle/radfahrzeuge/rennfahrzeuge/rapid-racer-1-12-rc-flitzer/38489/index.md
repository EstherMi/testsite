---
layout: "image"
title: "Rapid Racer seite"
date: "2014-03-26T10:34:33"
picture: "rapidracer01.jpg"
weight: "1"
konstrukteure: 
- "Dieter Braun"
fotografen:
- "Dieter Braun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- details/38489
imported:
- "2019"
_4images_image_id: "38489"
_4images_cat_id: "2874"
_4images_user_id: "1582"
_4images_image_date: "2014-03-26T10:34:33"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38489 -->
Wendekreis ist dank des neuen Servoteils sehr gut, die Räder streifen knapp nicht an den Motoren. Die schweren Motoren und Akku (da könnte man ja noch Gewicht sparen...) sind Teil des Chassis und machen eine sehr feine Straßenlage, wo der Racer bei engen Kurven auch schonmal das Driften anfängt.