---
layout: "image"
title: "Tractorpuller(Chevy502)"
date: "2004-10-31T21:19:47"
picture: "Tractor1.jpg"
weight: "6"
konstrukteure: 
- "Chevyfahrer"
fotografen:
- "Chevyfahrer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chevyfahrer"
license: "unknown"
legacy_id:
- details/2766
imported:
- "2019"
_4images_image_id: "2766"
_4images_cat_id: "269"
_4images_user_id: "103"
_4images_image_date: "2004-10-31T21:19:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2766 -->
Hab mal einen Tractorpuller gebaut um den diversen Fendt´s u.s.w. im Board den Auspuff zu zeigen.Noch nicht motorisiert,soll mal ein Powermot rein.Werde dann auch noch den nötigen Bremsschlitten bauen um die Performance zu testen.Daten des Originals:Chevrolet V8 502cui(8,2l),8-71er Blower(kompressor),Scoop,3Gangautomatik(manuell geschaltet)