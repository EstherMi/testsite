---
layout: "image"
title: "01"
date: "2011-02-06T16:04:42"
picture: "speedtruck1.jpg"
weight: "1"
konstrukteure: 
- "Lars B."
fotografen:
- "Lars B."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lars"
license: "unknown"
legacy_id:
- details/29876
imported:
- "2019"
_4images_image_id: "29876"
_4images_cat_id: "2204"
_4images_user_id: "1177"
_4images_image_date: "2011-02-06T16:04:42"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29876 -->
ohne elektronik
nur motor
und lampen