---
layout: "overview"
title: "Rennwagen"
date: 2019-12-17T18:50:08+01:00
legacy_id:
- categories/1560
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1560 --> 
Dieser Rennwagen hat einen Überrollbügel und wird vom Power Motor angetrieben. Gelenkt wird es durch den Servo. Gesteuert wird das Auto mit dem IR Control Set.