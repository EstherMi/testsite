---
layout: "image"
title: "Frontansicht"
date: "2009-11-01T11:27:02"
picture: "rennwagen3.jpg"
weight: "3"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/25617
imported:
- "2019"
_4images_image_id: "25617"
_4images_cat_id: "1560"
_4images_user_id: "845"
_4images_image_date: "2009-11-01T11:27:02"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25617 -->
