---
layout: "image"
title: "Empfänger"
date: "2009-02-09T17:44:11"
picture: "rennwagen12.jpg"
weight: "19"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/17340
imported:
- "2019"
_4images_image_id: "17340"
_4images_cat_id: "1560"
_4images_user_id: "845"
_4images_image_date: "2009-02-09T17:44:11"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17340 -->
