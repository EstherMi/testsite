---
layout: "image"
title: "Frontalansicht"
date: "2009-02-09T15:56:40"
picture: "rennwagen04.jpg"
weight: "11"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/17332
imported:
- "2019"
_4images_image_id: "17332"
_4images_cat_id: "1560"
_4images_user_id: "845"
_4images_image_date: "2009-02-09T15:56:40"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17332 -->
