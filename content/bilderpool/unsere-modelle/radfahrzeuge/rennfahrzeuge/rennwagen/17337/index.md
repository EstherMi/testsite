---
layout: "image"
title: "Lenkservo"
date: "2009-02-09T15:56:40"
picture: "rennwagen09.jpg"
weight: "16"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/17337
imported:
- "2019"
_4images_image_id: "17337"
_4images_cat_id: "1560"
_4images_user_id: "845"
_4images_image_date: "2009-02-09T15:56:40"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17337 -->
