---
layout: "image"
title: "Männchen mit Überrollbügel"
date: "2009-02-09T15:56:40"
picture: "rennwagen10.jpg"
weight: "17"
konstrukteure: 
- "Marius Käpernick"
fotografen:
- "Marius Käpernick"
keywords: ["modding"]
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/17338
imported:
- "2019"
_4images_image_id: "17338"
_4images_cat_id: "1560"
_4images_user_id: "845"
_4images_image_date: "2009-02-09T15:56:40"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17338 -->
