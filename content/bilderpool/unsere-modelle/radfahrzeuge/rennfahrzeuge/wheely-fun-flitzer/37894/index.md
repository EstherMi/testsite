---
layout: "image"
title: "Vorderansicht."
date: "2013-12-02T12:57:36"
picture: "funflitzer6.jpg"
weight: "6"
konstrukteure: 
- "Dieter Braun & Kids"
fotografen:
- "Dieter Braun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- details/37894
imported:
- "2019"
_4images_image_id: "37894"
_4images_cat_id: "2816"
_4images_user_id: "1582"
_4images_image_date: "2013-12-02T12:57:36"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37894 -->
Ordentliche Bodenfreiheit. [Evtl. sollte man noch eine Stoßstange machen, dass bei Crashs nicht gleich der Servoarm belastet wird - der fällt dann gerne heraus. Muß ich noch unbedingt machen]