---
layout: "image"
title: "Fun-Flitzer"
date: "2013-12-02T12:57:36"
picture: "funflitzer1.jpg"
weight: "1"
konstrukteure: 
- "Dieter Braun & Kids"
fotografen:
- "Dieter Braun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- details/37889
imported:
- "2019"
_4images_image_id: "37889"
_4images_cat_id: "2816"
_4images_user_id: "1582"
_4images_image_date: "2013-12-02T12:57:36"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37889 -->
Der hohe Schwerpunkt hat einen sehr witzigen Effekt: wenn man zu viel Gas gibt, heben sich die Vorderräder an und das Modell wird unlenkbar. Macht unsere Kids sehr viel Spaß. Andererseits, wenn man aber Rückwärts fährt, wird die Lenkung sehr direkt und dreht sich mitunter auf der Hinterachse. Sehr witziges und oft kaum vorhersehbares Verhalten machen den Spaß des Modells aus.