---
layout: "image"
title: "Seitenansicht 2"
date: "2013-12-02T12:57:36"
picture: "funflitzer5.jpg"
weight: "5"
konstrukteure: 
- "Dieter Braun & Kids"
fotografen:
- "Dieter Braun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dieter Braun"
license: "unknown"
legacy_id:
- details/37893
imported:
- "2019"
_4images_image_id: "37893"
_4images_cat_id: "2816"
_4images_user_id: "1582"
_4images_image_date: "2013-12-02T12:57:36"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37893 -->
Gut sichtbar ist der enge Radstand, der das Modell so wendig macht. Verkabelung etc. ist erstmal sehr pragmatisch gelöst. Der Akku ist mit einem Klettverschluß am einem 30er Baustein angeklebt - macht eine sehr kompakte Stromversorgung, aber ist natürlich nicht die feine FT-Art (sorry).