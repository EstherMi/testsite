---
layout: "overview"
title: "Formel 1 Rennwagen (PB)"
date: 2019-12-17T18:50:28+01:00
legacy_id:
- categories/3391
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3391 --> 
Ein F1 Rennwagen wollte ich seit lange her mal bauen. Mehr oder weniger Maßstabsgetreu, mit IR-steuerung, gute Lenkung, ein Bißchen Geschwindigkeit und gut aussehend. Die Geschwindigkeit ist nicht wirklich gelungen: da war einfach kein Platz übrig für eine Übersetzung mit etwa ein Zahnrad Z30 nach Z10. Das Ganze ist etwa 50cm lang, die Lenkung ist dank den Ackermann-princip okay: vollig eingeschlagen schafft er ein Kreis von etwa 1.20m Diameter.  Was Maßstab betrifft: als Vorbild fand ich Bilder von einen Ferrari, Schwarz und Rot wie FT, also davon bin ich ausgegangen. Ich werde davon auch ein Paar Bilder hoch laden, eine sogar mit den Michael Schumacher daneben! Aber vielleicht das ich nochmal Ein Rennwagen bauen muss mit Gelb und Blau dazu, so dass es für den Max Verstappen geeignet sein wird...