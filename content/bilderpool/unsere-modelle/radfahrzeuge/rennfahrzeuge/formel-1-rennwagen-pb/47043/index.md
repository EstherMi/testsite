---
layout: "image"
title: "fbesser10.jpg"
date: "2018-01-03T19:28:44"
picture: "fbesser10.jpg"
weight: "10"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/47043
imported:
- "2019"
_4images_image_id: "47043"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2018-01-03T19:28:44"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47043 -->
