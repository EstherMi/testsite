---
layout: "image"
title: "f01.jpg"
date: "2017-03-24T06:50:47"
picture: "f01.jpg"
weight: "31"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/45643
imported:
- "2019"
_4images_image_id: "45643"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2017-03-24T06:50:47"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45643 -->
Dies ist den erste Version, den Bau sieht man in die erste 54 Bilder. Später hab ich nog Einiges geändert:
1. Die Maßstabtreue ist verbessert, damit ich den Abstand zwischen Vorderräder und Kabine um etwa 40mm eingekürzt habe.
2. Das Hintenteil ist Konvergent ausgeführt worden.
3. Ich fand später eine gute Methode für den Lenkung den Servo zu verwenden.