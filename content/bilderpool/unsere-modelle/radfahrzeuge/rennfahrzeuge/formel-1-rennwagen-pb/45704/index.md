---
layout: "image"
title: "fgeaendert5.jpg"
date: "2017-03-30T12:04:20"
picture: "fgeaendert5.jpg"
weight: "27"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/45704
imported:
- "2019"
_4images_image_id: "45704"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2017-03-30T12:04:20"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45704 -->
Totalmodell