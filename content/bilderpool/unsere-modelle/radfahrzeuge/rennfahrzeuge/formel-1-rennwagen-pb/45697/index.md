---
layout: "image"
title: "Etwas kürzer 1"
date: "2017-03-25T13:34:58"
picture: "fetwaskuerzer1.jpg"
weight: "28"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/45697
imported:
- "2019"
_4images_image_id: "45697"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2017-03-25T13:34:58"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45697 -->
Um den Maßstabs Treue noch zu verbessern, hab ich vom Vorneteil etwa 30mm eincremen können, so daß weniger Raum ist zwischen Lenkrad und Vorderräder. Das Design ist als Bonus auch ein Bißchen einfacher geworden, bin da ziemlich zufrieden.