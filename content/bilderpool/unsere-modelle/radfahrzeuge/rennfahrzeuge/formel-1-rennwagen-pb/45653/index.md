---
layout: "image"
title: "f11.jpg"
date: "2017-03-24T06:51:52"
picture: "f11.jpg"
weight: "41"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/45653
imported:
- "2019"
_4images_image_id: "45653"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2017-03-24T06:51:52"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45653 -->
Um den Accu wechseln zu können, muss was Verkleiding entfernt werden. Auch die BS5 Rechts Mitte im Bild muss von U-träger geschoben werden. Den Accu sitzt eingeklemmt, und mit an beide Seiten die BS5, fällt er nicht raus wenn man das Auto auffassen oder sogar undrehen muss.