---
layout: "image"
title: "fservo2.jpg"
date: "2017-04-04T16:33:53"
picture: "fservo2.jpg"
weight: "17"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/45716
imported:
- "2019"
_4images_image_id: "45716"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2017-04-04T16:33:53"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45716 -->
