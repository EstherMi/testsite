---
layout: "image"
title: "fbesser11.jpg"
date: "2018-01-03T19:28:44"
picture: "fbesser11.jpg"
weight: "11"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/47044
imported:
- "2019"
_4images_image_id: "47044"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2018-01-03T19:28:44"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47044 -->
