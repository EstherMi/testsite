---
layout: "image"
title: "f46.jpg"
date: "2017-03-24T06:51:52"
picture: "f46.jpg"
weight: "76"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/45688
imported:
- "2019"
_4images_image_id: "45688"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2017-03-24T06:51:52"
_4images_image_order: "46"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45688 -->
