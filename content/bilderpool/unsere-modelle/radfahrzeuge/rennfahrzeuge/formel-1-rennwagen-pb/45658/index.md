---
layout: "image"
title: "f16.jpg"
date: "2017-03-24T06:51:52"
picture: "f16.jpg"
weight: "46"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/45658
imported:
- "2019"
_4images_image_id: "45658"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2017-03-24T06:51:52"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45658 -->
