---
layout: "image"
title: "f04.jpg"
date: "2017-03-24T06:50:47"
picture: "f04.jpg"
weight: "34"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/45646
imported:
- "2019"
_4images_image_id: "45646"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2017-03-24T06:50:47"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45646 -->
