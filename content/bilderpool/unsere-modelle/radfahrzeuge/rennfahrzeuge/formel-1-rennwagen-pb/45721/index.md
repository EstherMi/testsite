---
layout: "image"
title: "fservo7.jpg"
date: "2017-04-04T16:33:53"
picture: "fservo7.jpg"
weight: "22"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/45721
imported:
- "2019"
_4images_image_id: "45721"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2017-04-04T16:33:53"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45721 -->
Verkleidung oben den Servo.