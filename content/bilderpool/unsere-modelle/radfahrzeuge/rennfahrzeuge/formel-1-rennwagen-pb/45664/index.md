---
layout: "image"
title: "f22.jpg"
date: "2017-03-24T06:51:52"
picture: "f22.jpg"
weight: "52"
konstrukteure: 
- "Paul Bataille"
fotografen:
- "Paul Bataille"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PB"
license: "unknown"
legacy_id:
- details/45664
imported:
- "2019"
_4images_image_id: "45664"
_4images_cat_id: "3391"
_4images_user_id: "2449"
_4images_image_date: "2017-03-24T06:51:52"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45664 -->
Jetzt nach Hinten, zum Lenkmotor und Cabine