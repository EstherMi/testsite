---
layout: "image"
title: "FT Carrera Racer Draufsicht"
date: "2005-01-04T15:20:02"
picture: "FT_Carrera_Racer_001.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Carrera", "Rennauto"]
uploadBy: "joker"
license: "unknown"
legacy_id:
- details/3497
imported:
- "2019"
_4images_image_id: "3497"
_4images_cat_id: "321"
_4images_user_id: "89"
_4images_image_date: "2005-01-04T15:20:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3497 -->
Basis: Racing Car

Slot: von Carrera 
Slothalter: Baustein 30 umgebaut
MotorTyp: Fox
Achsen 3mm in Messingbuchsen geführt.
Zahnräder: aus dem Slotraceing bedarf