---
layout: "image"
title: "Porsche 908 Teil 3"
date: "2008-03-24T08:40:09"
picture: "P1010872.jpg"
weight: "3"
konstrukteure: 
- "Adrian Raiber"
fotografen:
- "Adrian Raiber"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "adrian"
license: "unknown"
legacy_id:
- details/14067
imported:
- "2019"
_4images_image_id: "14067"
_4images_cat_id: "1290"
_4images_user_id: "759"
_4images_image_date: "2008-03-24T08:40:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14067 -->
