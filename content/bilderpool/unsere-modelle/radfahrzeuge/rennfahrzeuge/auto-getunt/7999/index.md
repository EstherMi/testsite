---
layout: "image"
title: "Nochmal Seitlicher Lufteinzug"
date: "2006-12-20T19:22:05"
picture: "magi9.jpg"
weight: "34"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/7999
imported:
- "2019"
_4images_image_id: "7999"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2006-12-20T19:22:05"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7999 -->
