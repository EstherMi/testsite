---
layout: "image"
title: "Kabelhalterung (rechts)"
date: "2007-01-15T22:50:01"
picture: "mabi10.jpg"
weight: "10"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/8479
imported:
- "2019"
_4images_image_id: "8479"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2007-01-15T22:50:01"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8479 -->
Ein bisschen Ordnung muss sein.