---
layout: "image"
title: "Vier Auspüffe (schräg gegen aussen)"
date: "2006-12-09T13:38:38"
picture: "gif41.jpg"
weight: "76"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/7792
imported:
- "2019"
_4images_image_id: "7792"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2006-12-09T13:38:38"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7792 -->
