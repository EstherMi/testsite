---
layout: "image"
title: "Neue Befestigung der Fehrnsteuerung"
date: "2007-01-15T22:50:01"
picture: "mabi01.jpg"
weight: "1"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/8470
imported:
- "2019"
_4images_image_id: "8470"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2007-01-15T22:50:01"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8470 -->
Eingebaut wo es noch Platz hatte...