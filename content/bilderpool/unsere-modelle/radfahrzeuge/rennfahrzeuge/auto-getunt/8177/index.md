---
layout: "image"
title: "Lenkung von vorne"
date: "2006-12-29T17:56:18"
picture: "magi4_2.jpg"
weight: "20"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/8177
imported:
- "2019"
_4images_image_id: "8177"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2006-12-29T17:56:18"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8177 -->
