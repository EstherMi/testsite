---
layout: "image"
title: "Altes Dach 2"
date: "2006-12-09T13:38:10"
picture: "gif20.jpg"
weight: "55"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/7771
imported:
- "2019"
_4images_image_id: "7771"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2006-12-09T13:38:10"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7771 -->
