---
layout: "image"
title: "Seitlicher Lufteinzug"
date: "2006-12-20T19:22:05"
picture: "magi8.jpg"
weight: "33"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/7998
imported:
- "2019"
_4images_image_id: "7998"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2006-12-20T19:22:05"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7998 -->
