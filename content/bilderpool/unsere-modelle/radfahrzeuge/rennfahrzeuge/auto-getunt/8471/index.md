---
layout: "image"
title: "Und von weither"
date: "2007-01-15T22:50:01"
picture: "mabi02.jpg"
weight: "2"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/8471
imported:
- "2019"
_4images_image_id: "8471"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2007-01-15T22:50:01"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8471 -->
So siet es jetzt aus, nur dass die Rückspiegel noch grau wurden