---
layout: "image"
title: "Strom für Licht"
date: "2006-12-09T13:38:38"
picture: "gif47.jpg"
weight: "82"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/7798
imported:
- "2019"
_4images_image_id: "7798"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2006-12-09T13:38:38"
_4images_image_order: "47"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7798 -->
