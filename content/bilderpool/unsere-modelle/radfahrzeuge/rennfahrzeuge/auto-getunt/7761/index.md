---
layout: "image"
title: "Front-Lufteinzug"
date: "2006-12-09T13:37:59"
picture: "gif10.jpg"
weight: "45"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/7761
imported:
- "2019"
_4images_image_id: "7761"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2006-12-09T13:37:59"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7761 -->
