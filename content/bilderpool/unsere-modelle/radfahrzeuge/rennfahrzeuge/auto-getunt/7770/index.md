---
layout: "image"
title: "Altes Dach"
date: "2006-12-09T13:38:10"
picture: "gif19.jpg"
weight: "54"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/7770
imported:
- "2019"
_4images_image_id: "7770"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2006-12-09T13:38:10"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7770 -->
Der gelbe Bateriekasten sollte die Fernsteuerung darstellen. Der rote ist der fürs Licht (nicht an fernsteuerung!).