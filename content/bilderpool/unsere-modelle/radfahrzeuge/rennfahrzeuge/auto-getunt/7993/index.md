---
layout: "image"
title: "Setlicher Aufkleber-Spoiler"
date: "2006-12-20T19:22:05"
picture: "magi3.jpg"
weight: "28"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/7993
imported:
- "2019"
_4images_image_id: "7993"
_4images_cat_id: "735"
_4images_user_id: "445"
_4images_image_date: "2006-12-20T19:22:05"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7993 -->
