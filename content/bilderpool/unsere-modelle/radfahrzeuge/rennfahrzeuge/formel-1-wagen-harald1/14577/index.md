---
layout: "image"
title: "F1a-15.JPG"
date: "2008-05-23T18:09:55"
picture: "F1a-15.JPG"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/14577
imported:
- "2019"
_4images_image_id: "14577"
_4images_cat_id: "1341"
_4images_user_id: "4"
_4images_image_date: "2008-05-23T18:09:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14577 -->
Die Lenkung basiert auf zwei Schnecken 35977 in der Version ohne Stummel (*), die sich stirnseitig gegenüber stehen. Beim Zusammenstecken muss man darauf achten, dass der Schneckengang ohne Sprung über beide Schnecken durchgeht. Die zweite Schneckenmutter dient dazu, das ganze zu "entwackeln".


(*) siehe Bild http://www.ftcommunity.de/details.php?image_id=12780 
Es ist die Sorte in der Mitte und unten.