---
layout: "image"
title: "F1a-02.JPG"
date: "2008-05-23T17:56:03"
picture: "F1a-02.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/14571
imported:
- "2019"
_4images_image_id: "14571"
_4images_cat_id: "1341"
_4images_user_id: "4"
_4images_image_date: "2008-05-23T17:56:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14571 -->
Das Chassis (mit gefederter Hinterachse) ist eigentlich für einen PKW gedacht, aber vielleicht merkt's ja keiner :-)