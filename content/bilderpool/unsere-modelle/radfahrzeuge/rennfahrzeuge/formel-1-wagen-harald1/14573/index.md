---
layout: "image"
title: "F1a-10.JPG"
date: "2008-05-23T17:59:14"
picture: "F1a-10.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/14573
imported:
- "2019"
_4images_image_id: "14573"
_4images_cat_id: "1341"
_4images_user_id: "4"
_4images_image_date: "2008-05-23T17:59:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14573 -->
Zwischen Motor und Hinterachse fehlt noch das Getriebe (3-Gang-Schaltung mit den Zahnrädern von Michael). Der Strom kommt aus einer 9V-Batterie in der Box auf der rechten Fahrzeugseite (hier: unten).