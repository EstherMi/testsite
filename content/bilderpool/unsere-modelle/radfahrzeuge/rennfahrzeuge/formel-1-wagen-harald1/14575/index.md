---
layout: "image"
title: "F1a-13.JPG"
date: "2008-05-23T18:03:39"
picture: "F1a-13.JPG"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Vorgelege"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/14575
imported:
- "2019"
_4images_image_id: "14575"
_4images_cat_id: "1341"
_4images_user_id: "4"
_4images_image_date: "2008-05-23T18:03:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14575 -->
Das Z10 ist ein wenig bearbeitet worden (Idee von Claus).