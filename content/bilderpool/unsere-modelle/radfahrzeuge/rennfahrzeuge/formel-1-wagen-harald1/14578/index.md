---
layout: "image"
title: "F1-beide.JPG"
date: "2008-05-23T18:11:17"
picture: "F1-beide.JPG"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/14578
imported:
- "2019"
_4images_image_id: "14578"
_4images_cat_id: "1341"
_4images_user_id: "4"
_4images_image_date: "2008-05-23T18:11:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14578 -->
Zu einem Rennen gehören mindestens zwei.