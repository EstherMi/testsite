---
layout: "image"
title: "Gesamtansicht"
date: "2009-05-14T19:55:30"
picture: "DSC01005.jpg"
weight: "1"
konstrukteure: 
- "ich"
fotografen:
- "ich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- details/24022
imported:
- "2019"
_4images_image_id: "24022"
_4images_cat_id: "1647"
_4images_user_id: "920"
_4images_image_date: "2009-05-14T19:55:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24022 -->
Jedes Rad wird mit einem MiniMotor angetrieben, Fernsteuerung über das IR Control-Set