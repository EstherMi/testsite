---
layout: "image"
title: "Oberseite"
date: "2009-05-14T19:55:31"
picture: "DSC01008.jpg"
weight: "3"
konstrukteure: 
- "ich"
fotografen:
- "ich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- details/24024
imported:
- "2019"
_4images_image_id: "24024"
_4images_cat_id: "1647"
_4images_user_id: "920"
_4images_image_date: "2009-05-14T19:55:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24024 -->
Hinten sieht man das Batteriefach