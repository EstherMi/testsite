---
layout: "image"
title: "F1b-01.JPG"
date: "2008-05-23T18:15:31"
picture: "F1b-01.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/14579
imported:
- "2019"
_4images_image_id: "14579"
_4images_cat_id: "1342"
_4images_user_id: "4"
_4images_image_date: "2008-05-23T18:15:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14579 -->
Der hier ist auch noch nicht ganz fertig. Aber weil gerade mal die Sonne scheint, habe ich die Gelegenheit genutzt.