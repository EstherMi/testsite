---
layout: "image"
title: "F1b-13.JPG"
date: "2008-05-23T18:24:17"
picture: "F1b-13.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/14585
imported:
- "2019"
_4images_image_id: "14585"
_4images_cat_id: "1342"
_4images_user_id: "4"
_4images_image_date: "2008-05-23T18:24:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14585 -->
