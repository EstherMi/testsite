---
layout: "image"
title: "F1b-16.JPG"
date: "2008-05-23T18:28:14"
picture: "F1b-16.JPG"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/14587
imported:
- "2019"
_4images_image_id: "14587"
_4images_cat_id: "1342"
_4images_user_id: "4"
_4images_image_date: "2008-05-23T18:28:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14587 -->
Details der Vorderradaufhängung. Es sieht ziemlich eng aus, aber der Lenkeinschlag reicht aus.