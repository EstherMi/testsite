---
layout: "image"
title: "F1b-02.JPG"
date: "2008-05-23T18:18:00"
picture: "F1b-02.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/14580
imported:
- "2019"
_4images_image_id: "14580"
_4images_cat_id: "1342"
_4images_user_id: "4"
_4images_image_date: "2008-05-23T18:18:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14580 -->
Die gelbe Box muss man sich als ft-Akku vorstellen (der meinige steckt derzeit im Flieger), und das E-Tec als Infrarot-Fernsteuerung (da sind meine beiden auch anderweitig unterwegs). An der Lenkung fehlt noch etwas, was die Schneckenmutter stabilisiert.