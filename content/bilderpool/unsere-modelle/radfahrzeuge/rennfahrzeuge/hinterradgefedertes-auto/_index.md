---
layout: "overview"
title: "Hinterradgefedertes Auto mit Sturzverstellung"
date: 2019-12-17T18:50:12+01:00
legacy_id:
- categories/1929
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1929 --> 
Ich habe ein Auto gebaut, bei dem man hinten durch zwei Minimotoren den Sturz verstellen kann. Natürlich ist er hinten noch gefedert! Vorne hat er eine Pendelachse.