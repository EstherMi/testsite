---
layout: "image"
title: "Von der Seite"
date: "2010-04-07T12:40:33"
picture: "hinterradgefaedertesautomitsturtzverstellung3.jpg"
weight: "3"
konstrukteure: 
- "dasbinich273"
fotografen:
- "dasbinich273"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dasbinich273"
license: "unknown"
legacy_id:
- details/26905
imported:
- "2019"
_4images_image_id: "26905"
_4images_cat_id: "1929"
_4images_user_id: "1057"
_4images_image_date: "2010-04-07T12:40:33"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26905 -->
Zu sehen ist ein Minimotor für den Sturtz