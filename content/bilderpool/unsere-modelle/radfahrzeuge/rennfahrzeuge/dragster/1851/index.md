---
layout: "image"
title: "FT-Dragstar mit LEMO-micro-membranpumpe MK610"
date: "2003-10-29T09:36:15"
picture: "FT-DragstarKompressor0001.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/1851
imported:
- "2019"
_4images_image_id: "1851"
_4images_cat_id: "207"
_4images_user_id: "22"
_4images_image_date: "2003-10-29T09:36:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1851 -->
Dragstar mit LEMO-micro-membranpumpe MK610, (LxBxH ) 32 x16 x 25.5 mm, Druck 1 bar, Fodermenge 1L / Min. 
Die LEMO-micro-membranpumpe gefallt mir sehr gut !......