---
layout: "image"
title: "Achsschenkel"
date: "2014-04-23T18:24:50"
picture: "formelbolideferrari06.jpg"
weight: "6"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38656
imported:
- "2019"
_4images_image_id: "38656"
_4images_cat_id: "2886"
_4images_user_id: "1729"
_4images_image_date: "2014-04-23T18:24:50"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38656 -->
Der Achschenkel selbst ist relativ einfach aufgebaut.
Eine Schneckenmutter M1 mit 2 Kugellagern zur Aufnahme der Radachse, Winkelsteine, um den Spreizungswinkel hinzubekommen und ein U-Stein (Rollenlager) nimmt die eigentliche Drehachse auf.
Dann noch ein paar Aluteile: Stellringe, um sowohl Lenkachse als auch Radachse zu fixieren und ein Hex-Adapter, um das Rad an der 4mm Metallachse zu befestigen