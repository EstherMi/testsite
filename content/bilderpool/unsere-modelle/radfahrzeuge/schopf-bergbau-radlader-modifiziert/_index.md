---
layout: "overview"
title: "Schopf-Bergbau-Radlader  -modifiziert"
date: 2019-12-17T18:51:28+01:00
legacy_id:
- categories/2805
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2805 --> 
Inspiration:  Erbes-Budeheim-2013   -Jörg und Erik Busch

Ferngesteuerter Schopf-Bergbau-Radlader mit pneumatischer, proportionaler Knicklenkung und pneumatischer Schaufelsteuerung

modifiziert mit:

- pneumatik Ventilen 3-way, 2 position  NO + NC & Distributor 

- 15x15 Alu-Profilen OpenBeam 
