---
layout: "image"
title: "ATeamV-01.JPG"
date: "2005-01-04T16:46:14"
picture: "ATeamV-01.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["ATeam", "A-Team", "Vandura", "Van", "Lieferwagen"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/3516
imported:
- "2019"
_4images_image_id: "3516"
_4images_cat_id: "322"
_4images_user_id: "4"
_4images_image_date: "2005-01-04T16:46:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3516 -->
Das ist ein Versuchsträger mit manueller Lenkung und nicht schräg gestellten Lenkachsen. Funktionieren tut es so herum auch, es fehlt nur ein kleiner Gummiring, um dafür zu sorgen, dass die Strebe auf dem Zapfen der Schneckenmutter sitzen bleibt.