---
layout: "image"
title: "ATeam15.JPG"
date: "2005-01-04T16:46:14"
picture: "ATeam15.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["ATeam", "A-Team", "Vandura", "Van", "Lieferwagen"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/3514
imported:
- "2019"
_4images_image_id: "3514"
_4images_cat_id: "322"
_4images_user_id: "4"
_4images_image_date: "2005-01-04T16:46:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3514 -->
Quer unter/hinter dem Lichtschalter liegt eine Strebe I-105 Loch, die aus einer I-120 Loch mittels Taschenmesser entstanden ist. Ihre Endpunkte markieren die oberen Punkte der Achsen, um die die Räder beim Lenken geschwenkt werden.