---
layout: "image"
title: "ATeam11.JPG"
date: "2005-01-04T16:28:30"
picture: "ATeam11.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["ATeam", "A-Team", "Vandura", "Van", "Lieferwagen"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/3510
imported:
- "2019"
_4images_image_id: "3510"
_4images_cat_id: "322"
_4images_user_id: "4"
_4images_image_date: "2005-01-04T16:28:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3510 -->
Die Hinterachse kann zu Wartungszwecken aufgeklappt werden.