---
layout: "image"
title: "ATeam14.JPG"
date: "2005-01-04T16:31:11"
picture: "ATeam14.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["ATeam", "A-Team", "Vandura", "Van", "Lieferwagen"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/3513
imported:
- "2019"
_4images_image_id: "3513"
_4images_cat_id: "322"
_4images_user_id: "4"
_4images_image_date: "2005-01-04T16:31:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3513 -->
Der Unterbau sieht nicht nur 'lüttich' und 'schlapperich' aus, er ist es auch! Erst durch die Bauplatten des Wagenbodens und insbesondere die Verstrebungen des Oberbaus wird die ganze Zelle biegesteif:

Im gezeigten Zustand kann man eine vordere Ecke des Fahrzeugs um gut 60mm anheben und das Heck bleibt trotzdem auf dem Boden. Im fertigen Aufbau verwindet sich der Rahmen um höchstens 5 mm.