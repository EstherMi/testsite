---
layout: "image"
title: "ATeam03.JPG"
date: "2005-01-04T16:28:20"
picture: "ATeam03.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["ATeam", "A-Team", "Vandura", "Van", "Lieferwagen"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/3502
imported:
- "2019"
_4images_image_id: "3502"
_4images_cat_id: "322"
_4images_user_id: "4"
_4images_image_date: "2005-01-04T16:28:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3502 -->
Die Z20 in den Radnaben sind reine Zierde. Hinten wackeln sie lose mit, vorne sind sie ordentlich festgeschraubt.