---
layout: "image"
title: "ATeam10.JPG"
date: "2005-01-04T16:28:30"
picture: "ATeam10.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["ATeam", "A-Team", "Vandura", "Van", "Lieferwagen"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/3509
imported:
- "2019"
_4images_image_id: "3509"
_4images_cat_id: "322"
_4images_user_id: "4"
_4images_image_date: "2005-01-04T16:28:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3509 -->
Die Schiebetür hängt an genau einem Zapfen, muss daher also beim Öffnen und Schließen pfleglich behandelt werden. Wenn sie einmal zu ist, klemmt sie aber recht ordentlich.