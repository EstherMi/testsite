---
layout: "image"
title: "ATeam07.JPG"
date: "2005-01-04T16:28:20"
picture: "ATeam07.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["ATeam", "A-Team", "Vandura", "Van", "Lieferwagen"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/3506
imported:
- "2019"
_4images_image_id: "3506"
_4images_cat_id: "322"
_4images_user_id: "4"
_4images_image_date: "2005-01-04T16:28:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3506 -->
Die Lenkung erfolgt durch einen Mini-Mot mit Hubgetriebe. Vorn und hinten hat er Taster mit Dioden als Endanschlag.
Oben im Bild ist der Hauptschalter, um den Akku vom Rest der Elektrik zu trennen. Damit die Stecker im Akku nicht so weit nach unten herausstehen, wurden zwei ft-Stecker abgesägt, so dass sie nur noch aus Federstift und einem Stummel mit Querbohrung bestehen.
Die Schiebetür läuft auf einem kaputten Hubgetriebe, für einen Motor wäre sowieso kein Platz gewesen.