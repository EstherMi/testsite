---
layout: "image"
title: "IR-Gabelstapler"
date: "2008-10-03T19:46:03"
picture: "IR-Gabelstapler.jpg"
weight: "4"
konstrukteure: 
- "HLGR"
fotografen:
- "HLGR"
keywords: ["Gabelstapler", "Control", "Set"]
uploadBy: "HLGR"
license: "unknown"
legacy_id:
- details/15817
imported:
- "2019"
_4images_image_id: "15817"
_4images_cat_id: "354"
_4images_user_id: "832"
_4images_image_date: "2008-10-03T19:46:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15817 -->
Antrieb durch drei Mini-Motoren.
Durch Raupenschaltung gesteuert auf der Stelle drehbar.