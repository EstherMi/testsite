---
layout: "image"
title: "Gabelstabler_01"
date: "2009-01-11T20:37:18"
picture: "gabelstaplerfranksversion1.jpg"
weight: "1"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/16978
imported:
- "2019"
_4images_image_id: "16978"
_4images_cat_id: "1528"
_4images_user_id: "729"
_4images_image_date: "2009-01-11T20:37:18"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16978 -->
von links vorne