---
layout: "image"
title: "Gabelstabler_04"
date: "2009-01-11T20:37:18"
picture: "gabelstaplerfranksversion4.jpg"
weight: "4"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/16981
imported:
- "2019"
_4images_image_id: "16981"
_4images_cat_id: "1528"
_4images_user_id: "729"
_4images_image_date: "2009-01-11T20:37:18"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16981 -->
von rechts vorne