---
layout: "image"
title: "Gabelstabler_03"
date: "2009-01-11T20:37:18"
picture: "gabelstaplerfranksversion3.jpg"
weight: "3"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/16980
imported:
- "2019"
_4images_image_id: "16980"
_4images_cat_id: "1528"
_4images_user_id: "729"
_4images_image_date: "2009-01-11T20:37:18"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16980 -->
von rechts hinten