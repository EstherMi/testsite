---
layout: "overview"
title: "Gabelstapler Franks Version"
date: 2019-12-17T18:45:13+01:00
legacy_id:
- categories/1528
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1528 --> 
Nachbau von HLGR\'s Stapler mit 2 Mini-Motoren und einem Powermotor. Ferngesteuert mit dem neuen Control Set.