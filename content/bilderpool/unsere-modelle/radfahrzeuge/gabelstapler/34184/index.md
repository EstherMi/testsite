---
layout: "image"
title: "umgedrehtes Servo"
date: "2012-02-14T23:29:34"
picture: "ftstapler31.jpg"
weight: "3"
konstrukteure: 
- "mattnik"
fotografen:
- "mattnik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mattnik"
license: "unknown"
legacy_id:
- details/34184
imported:
- "2019"
_4images_image_id: "34184"
_4images_cat_id: "354"
_4images_user_id: "1447"
_4images_image_date: "2012-02-14T23:29:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34184 -->
Wie man deutlich sieht, man braucht größere Räder...