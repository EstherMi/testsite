---
layout: "image"
title: "umgedrehtes Servo"
date: "2012-02-14T23:29:33"
picture: "ftstapler32.jpg"
weight: "1"
konstrukteure: 
- "mattnik"
fotografen:
- "mattnik"
keywords: ["Gabelstapler", "Fernsteuerung", "Servo", "Lenkung", "Modell"]
uploadBy: "mattnik"
license: "unknown"
legacy_id:
- details/34182
imported:
- "2019"
_4images_image_id: "34182"
_4images_cat_id: "354"
_4images_user_id: "1447"
_4images_image_date: "2012-02-14T23:29:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34182 -->
Die untere rote Platte ist nur Podest, gehört nicht dazu