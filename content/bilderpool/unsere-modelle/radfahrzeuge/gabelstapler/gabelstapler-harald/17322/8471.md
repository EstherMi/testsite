---
layout: "comment"
hidden: true
title: "8471"
date: "2009-02-07T09:00:02"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Nein, da ist sonst nichts mehr. Das schwankt schon ziemlich beim Ausfahren, und da schwankt auch die ganze Vorderachse mit, die ja nur mit zwei BS15-Loch plus BS30 an den Drehkränzen fest ist.

Zum Schwerlast-Stapler wird der erst, wenn man noch eine Nabe in die Mitte des unteren Drehkranz einbaut (sprich: alles zerlegt), eine Achse durchsteckt, und von da aus zu den unteren Aufnahmen des Teleskops versteift. Ist halt so, wenn einem erst hinterher noch Sachen einfallen.

Gruß,
Harald