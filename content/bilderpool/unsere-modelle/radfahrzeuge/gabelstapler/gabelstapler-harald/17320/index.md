---
layout: "image"
title: "Stapler05.jpg"
date: "2009-02-05T21:40:03"
picture: "Stapler05.jpg"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/17320
imported:
- "2019"
_4images_image_id: "17320"
_4images_cat_id: "1558"
_4images_user_id: "4"
_4images_image_date: "2009-02-05T21:40:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17320 -->
Einzelheiten vom Aufbau. Die Führungsplatte 32455 ist wieder mit dabei.