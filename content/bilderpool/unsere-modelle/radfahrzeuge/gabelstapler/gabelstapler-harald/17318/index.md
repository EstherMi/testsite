---
layout: "image"
title: "Stapler03.jpg"
date: "2009-02-05T21:23:42"
picture: "Stapler03.jpg"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/17318
imported:
- "2019"
_4images_image_id: "17318"
_4images_cat_id: "1558"
_4images_user_id: "4"
_4images_image_date: "2009-02-05T21:23:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17318 -->
Die Rückseite des Teleskopschubs. Der Motor mit 1:20 (graue Kappe) ist etwas zu schnell, aber meine 1:50 sind noch anderweitig verbaut.