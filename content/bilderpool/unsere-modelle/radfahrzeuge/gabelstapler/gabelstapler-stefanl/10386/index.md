---
layout: "image"
title: "Gabelstapler 13"
date: "2007-05-12T15:09:01"
picture: "gabelstaplerstefanl13.jpg"
weight: "18"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/10386
imported:
- "2019"
_4images_image_id: "10386"
_4images_cat_id: "946"
_4images_user_id: "502"
_4images_image_date: "2007-05-12T15:09:01"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10386 -->
Ganz unten.