---
layout: "image"
title: "Gabelstapler 3"
date: "2007-05-12T15:08:46"
picture: "gabelstaplerstefanl03.jpg"
weight: "5"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/10376
imported:
- "2019"
_4images_image_id: "10376"
_4images_cat_id: "946"
_4images_user_id: "502"
_4images_image_date: "2007-05-12T15:08:46"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10376 -->
