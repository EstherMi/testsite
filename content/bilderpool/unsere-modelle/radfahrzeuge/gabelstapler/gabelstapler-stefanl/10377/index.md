---
layout: "image"
title: "Gabelstapler 4"
date: "2007-05-12T15:08:46"
picture: "gabelstaplerstefanl04.jpg"
weight: "7"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/10377
imported:
- "2019"
_4images_image_id: "10377"
_4images_cat_id: "946"
_4images_user_id: "502"
_4images_image_date: "2007-05-12T15:08:46"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10377 -->
