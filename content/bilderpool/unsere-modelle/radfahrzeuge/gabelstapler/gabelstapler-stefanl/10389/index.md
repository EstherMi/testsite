---
layout: "image"
title: "Gabelstabler 16"
date: "2007-05-12T21:51:43"
picture: "gabelstabler2.jpg"
weight: "4"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/10389
imported:
- "2019"
_4images_image_id: "10389"
_4images_cat_id: "946"
_4images_user_id: "502"
_4images_image_date: "2007-05-12T21:51:43"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10389 -->
Endlich ein Platz für den Allradantrieb.