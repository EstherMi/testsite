---
layout: "image"
title: "Gabelstapler 5"
date: "2007-05-12T15:08:46"
picture: "gabelstaplerstefanl05.jpg"
weight: "9"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/10378
imported:
- "2019"
_4images_image_id: "10378"
_4images_cat_id: "946"
_4images_user_id: "502"
_4images_image_date: "2007-05-12T15:08:46"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10378 -->
Ganz ausgefahren.