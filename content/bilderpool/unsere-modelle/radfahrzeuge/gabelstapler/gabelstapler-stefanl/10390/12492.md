---
layout: "comment"
hidden: true
title: "12492"
date: "2010-10-11T17:15:39"
uploadBy:
- "sparifankerl"
license: "unknown"
imported:
- "2019"
---
Für solche Fahrzeuge (Traktoren, Radlader, Motorjapaner) wäre einmal ein Drehkranz ohne Zähne rundherum wünschenswert. Nur die Achteck-Platform mit den Einschubnuten. Vielleicht könnte Andreas Tacke so etwas machen.
Er hat ja auch die Bauplatten 30 x 90 abgesägt.
Würden sicher etliche Fans begrüßen, oder?