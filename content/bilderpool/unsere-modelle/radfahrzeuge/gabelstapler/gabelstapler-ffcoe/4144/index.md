---
layout: "image"
title: "Gabelstabler1"
date: "2005-05-15T17:28:52"
picture: "Gabelstabler_001.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ffcoe"
license: "unknown"
legacy_id:
- details/4144
imported:
- "2019"
_4images_image_id: "4144"
_4images_cat_id: "1556"
_4images_user_id: "332"
_4images_image_date: "2005-05-15T17:28:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4144 -->
Das ist die linke Seitenansicht. Dieser gabelstabler ist eigentlich nur die vollkommene Motoriesierung des ft-Baukastens "Gabelastabler". Er wird mit dem IR Controll Set gesteuert. Er kann vorwärts und rüchwärts fahren, sowie sich auf der stelle drehen und die Gabel auf- und abbewegen.