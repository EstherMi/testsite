---
layout: "image"
title: "Gabel unten"
date: "2007-04-06T15:10:45"
picture: "Gabelstapler_020.jpg"
weight: "4"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: ["Gabelstapler"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/9995
imported:
- "2019"
_4images_image_id: "9995"
_4images_cat_id: "903"
_4images_user_id: "504"
_4images_image_date: "2007-04-06T15:10:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9995 -->
Jetzt kann man eine Palette heben.