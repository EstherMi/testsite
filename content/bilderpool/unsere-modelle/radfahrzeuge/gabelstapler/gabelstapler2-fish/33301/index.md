---
layout: "image"
title: "Gabelstapler unten"
date: "2011-10-23T16:00:07"
picture: "gabelstaplerfish3.jpg"
weight: "3"
konstrukteure: 
- "fish"
fotografen:
- "fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- details/33301
imported:
- "2019"
_4images_image_id: "33301"
_4images_cat_id: "2464"
_4images_user_id: "1113"
_4images_image_date: "2011-10-23T16:00:07"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33301 -->
Das Fahrwerk besteht aus zwei 1:50 Powermotoren mit den beiden großen Antriebsrädern. Hinten sind die kleinen, drehbaren Räder ohne Reifen. Die Impulszahnräder sind zwar montiert, aber keine Taster angeschlossen.