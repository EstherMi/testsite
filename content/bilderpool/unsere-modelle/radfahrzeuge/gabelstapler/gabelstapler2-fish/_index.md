---
layout: "overview"
title: "Gabelstapler2 (fish)"
date: 2019-12-17T18:45:19+01:00
legacy_id:
- categories/2464
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2464 --> 
Ein normaler Gabelstapler mit Farbsensor, Endtastern für die Gabel und einem RoboInterface welches über Python vom PC aus gesteuert wird.