---
layout: "image"
title: "Gabelstapler"
date: "2009-01-02T16:28:15"
picture: "IMG_8718.jpg"
weight: "5"
konstrukteure: 
- "tim-carlo"
fotografen:
- "tim"
keywords: ["baumaschine", "Gabelstapler"]
uploadBy: "carlo"
license: "unknown"
legacy_id:
- details/16839
imported:
- "2019"
_4images_image_id: "16839"
_4images_cat_id: "1557"
_4images_user_id: "893"
_4images_image_date: "2009-01-02T16:28:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16839 -->
