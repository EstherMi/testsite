---
layout: "image"
title: "alles neu"
date: "2015-08-03T22:11:02"
picture: "straddlecarrier1.jpg"
weight: "2"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/41709
imported:
- "2019"
_4images_image_id: "41709"
_4images_cat_id: "2955"
_4images_user_id: "4"
_4images_image_date: "2015-08-03T22:11:02"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41709 -->
Der Straddle Carrier hat neue Fahrwerksbeine, einen geänderten Antrieb und eine neue Lenkung bekommen. Jetzt fährt er (fast) so wie es sich gehört. Eine verbleibende Schwachstelle ist die Abstimmung zwischen links und rechts: die Seiten streben auseinander und das Fahrwerk versucht, eine Grätsche zu machen.