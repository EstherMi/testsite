---
layout: "image"
title: "Fahrwerksbein"
date: "2015-08-03T22:11:02"
picture: "straddlecarrier3.jpg"
weight: "4"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/41711
imported:
- "2019"
_4images_image_id: "41711"
_4images_cat_id: "2955"
_4images_user_id: "4"
_4images_image_date: "2015-08-03T22:11:02"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41711 -->
Das ganze Gefriemel mitten drin dient nur dazu, die Abstände zwischen den Kegelzahnrädern passgenau hin zu bekommen.

Die gelbe Strebe meldet den aktuellen Lenkeinschlag über die Messingachse nach oben.

Die äußeren Fahrwerksbeine haben die kurzen Anlenkhebel und lenken deswegen stärker als die inneren.