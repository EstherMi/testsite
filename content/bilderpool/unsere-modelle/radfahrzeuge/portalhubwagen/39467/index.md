---
layout: "image"
title: "Totale"
date: "2014-09-29T22:15:30"
picture: "portalhubwagen01.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/39467
imported:
- "2019"
_4images_image_id: "39467"
_4images_cat_id: "2955"
_4images_user_id: "4"
_4images_image_date: "2014-09-29T22:15:30"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39467 -->
Ansicht von vorn. Der gezeigte Lenkeinschlag ist getürkt: eine Seite steht auf "geradeaus", die andere auf "ganz nach links"