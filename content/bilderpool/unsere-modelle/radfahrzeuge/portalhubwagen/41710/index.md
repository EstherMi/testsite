---
layout: "image"
title: "neue Fahrwerksbeine"
date: "2015-08-03T22:11:02"
picture: "straddlecarrier2.jpg"
weight: "3"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/41710
imported:
- "2019"
_4images_image_id: "41710"
_4images_cat_id: "2955"
_4images_user_id: "4"
_4images_image_date: "2015-08-03T22:11:02"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41710 -->
Die Fahrwerksbeine mussten deutlich stabiler gebaut werden. Das Chassis 31557 der BSB kam dazu gerade recht. Die Achsen in den Reifen 60 sind mit Absicht weich und nicht aus Metall: die Reifen brauchen etwas Druck von oben, damit sie an die Kegelräder angedrückt werden, also muss die Achse nachgeben können.

Der gelbe U-Träger ist Transportsicherung. Er hält den Ladebalken fest und hält die Fahrwerksseiten auf Abstand.