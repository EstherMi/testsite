---
layout: "image"
title: "Anschlag, vorn"
date: "2014-09-29T22:15:30"
picture: "portalhubwagen07.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/39473
imported:
- "2019"
_4images_image_id: "39473"
_4images_cat_id: "2955"
_4images_user_id: "4"
_4images_image_date: "2014-09-29T22:15:30"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39473 -->
Es war ja alles so einfach: ein Seil durch die vier Ecken fädeln, Motor dran, und dann heben... Nein, da kommt die erste Ecke zuerst und die letzte zuletzt, der Ladebalken verkantet und bleibt hängen.

Also gibt es zwei Seilwinden, mit separaten Motoren, und jede hat einen eigenen Endtaster, der den Antrieb in der oberen Endlage still setzt und dann nur noch "Fahrt abwärts" erlaubt.