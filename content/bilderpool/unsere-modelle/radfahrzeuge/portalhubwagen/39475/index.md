---
layout: "image"
title: "Ladebalken links"
date: "2014-09-29T22:15:30"
picture: "portalhubwagen09.jpg"
weight: "18"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/39475
imported:
- "2019"
_4images_image_id: "39475"
_4images_cat_id: "2955"
_4images_user_id: "4"
_4images_image_date: "2014-09-29T22:15:30"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39475 -->
Der schwarze Taster unten rechts überwacht die Zugspannung im linken Seil: die Containerschlösser können nur bewegt werden, wenn beide Seile ohne Zug sind. 
Die Containerschlösser stehen auf "offen", angedeutet durch die Stellung der gelben V-Verbinder auf den zugehörigen Achsen.

Der graue Taster am rechten Rand sitzt auf dem Schlitten, der die Containerschlösser betätigt. Er ist gerade in Endposition und gedrückt.