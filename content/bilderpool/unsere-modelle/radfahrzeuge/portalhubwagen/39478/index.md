---
layout: "image"
title: "Lenkung 1"
date: "2014-09-29T22:15:40"
picture: "portalhubwagen12.jpg"
weight: "21"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/39478
imported:
- "2019"
_4images_image_id: "39478"
_4images_cat_id: "2955"
_4images_user_id: "4"
_4images_image_date: "2014-09-29T22:15:40"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39478 -->
Die Lenkung ist "eigentlich" ganz einfach: das hier ist ein ganz gewöhnliches Lenktrapez ("Ackermann-Lenkung"), mit der grauen Spurstange und einem Antrieb mittels ft-Servo (kurze graue Strebe). 

Allerdings ist die Geschichte hiermit noch nicht beendet, weil diese Konstruktion nicht direkt irgendwelche Räder verstellt, sondern nur die (unterschiedlichen) Soll-Positionen für Lenkgetriebe der linken und rechten Seite vorgibt. Solange "Soll" und "Ist" für irgend eine Seite von einander abweichen, bleiben einer oder zwei der vier schwarzen Taster am oberen Bildrand gedrückt und der zugehöre Lenkmotor hat "Strom".