---
layout: "image"
title: "Getriebe Spritzenpresse"
date: "2017-09-30T11:52:18"
picture: "05_Getriebe_Spritzenpresse.jpg"
weight: "5"
konstrukteure: 
- "Fabian, Max, Christian & Stefan"
fotografen:
- "Christian und Stefan"
keywords: ["Getriebe", "Spindel"]
uploadBy: "ftpi"
license: "unknown"
legacy_id:
- details/46512
imported:
- "2019"
_4images_image_id: "46512"
_4images_cat_id: "10"
_4images_user_id: "2611"
_4images_image_date: "2017-09-30T11:52:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46512 -->
Getriebe um die beiden Spindeln gleichmäßig zu verfahren. Durch die Untersetzung der Spindel reicht ein S-Motor für den Antrieb aus.