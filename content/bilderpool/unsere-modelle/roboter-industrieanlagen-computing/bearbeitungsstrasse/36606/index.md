---
layout: "image"
title: "Komplette Béarbeitungsstraße"
date: "2013-02-14T13:45:39"
picture: "IMG_4579.jpg"
weight: "5"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lukas99h."
license: "unknown"
legacy_id:
- details/36606
imported:
- "2019"
_4images_image_id: "36606"
_4images_cat_id: "2714"
_4images_user_id: "1631"
_4images_image_date: "2013-02-14T13:45:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36606 -->
Folgende Teile wurden verbaut:
1.TX-Controler
1.Kompressor
3.Lampen
3.Fototransistoren
3.Taster
1.Magnetventil 
2.Zylinder
1.S Motor