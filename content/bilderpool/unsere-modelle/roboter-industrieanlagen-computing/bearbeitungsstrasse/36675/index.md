---
layout: "image"
title: "Bearbeitungsstraße 2"
date: "2013-02-24T14:53:34"
picture: "IMG_4622.jpg"
weight: "4"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lukas99h."
license: "unknown"
legacy_id:
- details/36675
imported:
- "2019"
_4images_image_id: "36675"
_4images_cat_id: "2714"
_4images_user_id: "1631"
_4images_image_date: "2013-02-24T14:53:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36675 -->
Hier kann man den Robo-TX-Controller sehen, dahinter das Überdruckventil und daneben den Kompressor!!