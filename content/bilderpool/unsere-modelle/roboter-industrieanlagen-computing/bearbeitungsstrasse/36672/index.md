---
layout: "image"
title: "Bearbeitungsstraße 2"
date: "2013-02-24T14:53:33"
picture: "fischertechnik__2.jpg"
weight: "1"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lukas99h."
license: "unknown"
legacy_id:
- details/36672
imported:
- "2019"
_4images_image_id: "36672"
_4images_cat_id: "2714"
_4images_user_id: "1631"
_4images_image_date: "2013-02-24T14:53:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36672 -->
Hier habe ich nun meine Bearbeitungstation um einiges vergrößert. Komplett ist sie zwar noch nicht ,denn es müssen noch die Kabel sowie manche Pneumatik-Schläuche angeschlossen werden. So solle sie einmal laufen wenn  ich sie fertig programmiert habe!!!!
1:Eingabe der Werkstücke ( 1,2...............20) 
2:Stanzen
3:Aufnahme mit dem Vakuumgreifer 
4:Ablage in die rote Box