---
layout: "image"
title: "Auswurf der Werkstücke"
date: "2013-02-14T13:45:39"
picture: "fischertechnik_.jpg"
weight: "6"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lukas99h."
license: "unknown"
legacy_id:
- details/36607
imported:
- "2019"
_4images_image_id: "36607"
_4images_cat_id: "2714"
_4images_user_id: "1631"
_4images_image_date: "2013-02-14T13:45:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36607 -->
Hir kann man gut den Auswurf mit dem Zylinder sehen. Im Hintergrund befindet sich mein TX-Controler.