---
layout: "image"
title: "Das Bedinfeld"
date: "2009-03-20T16:55:18"
picture: "codeschloss3.jpg"
weight: "3"
konstrukteure: 
- "Kacker303"
fotografen:
- "Kacker303"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kacker303"
license: "unknown"
legacy_id:
- details/23472
imported:
- "2019"
_4images_image_id: "23472"
_4images_cat_id: "1598"
_4images_user_id: "924"
_4images_image_date: "2009-03-20T16:55:18"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23472 -->
Im Grunde ganz einfach wie n' Normales Zahlenschloss zum Verschließen muss man nach Codeeingabe Schließen drücken.