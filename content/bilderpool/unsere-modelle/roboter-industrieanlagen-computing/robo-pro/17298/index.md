---
layout: "image"
title: "Automat3"
date: "2009-02-03T15:20:35"
picture: "prog3.jpg"
weight: "5"
konstrukteure: 
- "Niklas"
fotografen:
- "Niklas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "conradelectric2"
license: "unknown"
legacy_id:
- details/17298
imported:
- "2019"
_4images_image_id: "17298"
_4images_cat_id: "1553"
_4images_user_id: "911"
_4images_image_date: "2009-02-03T15:20:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17298 -->
