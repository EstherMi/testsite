---
layout: "image"
title: "fischertechnik CNC-Fräse"
date: "2017-02-14T19:16:21"
picture: "fraese05.jpg"
weight: "5"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/45204
imported:
- "2019"
_4images_image_id: "45204"
_4images_cat_id: "3367"
_4images_user_id: "2303"
_4images_image_date: "2017-02-14T19:16:21"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45204 -->
