---
layout: "image"
title: "Teilkopf"
date: "2017-02-14T19:27:01"
picture: "fraese28.jpg"
weight: "28"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/45227
imported:
- "2019"
_4images_image_id: "45227"
_4images_cat_id: "3367"
_4images_user_id: "2303"
_4images_image_date: "2017-02-14T19:27:01"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45227 -->
Der Teilkopf ist 360 Grad drehbar und besitzt einen Reedkontakt. Der Endschalter wird über einen 4 mm Magneten 
in der Drehscheibe angesteuert.