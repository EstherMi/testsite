---
layout: "image"
title: "Frästisch"
date: "2017-02-14T19:26:39"
picture: "fraese19.jpg"
weight: "19"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/45218
imported:
- "2019"
_4images_image_id: "45218"
_4images_cat_id: "3367"
_4images_user_id: "2303"
_4images_image_date: "2017-02-14T19:26:39"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45218 -->
Hier ist der Frästisch zu sehen. Vor und zurück = 42 mm. Links und rechts = 75 mm Fahrweg.
Der Teilkopf kann auch demontiert werden.