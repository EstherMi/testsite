---
layout: "overview"
title: "fischertechnik CNC-Fräse"
date: 2019-12-17T19:08:53+01:00
legacy_id:
- categories/3367
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3367 --> 
Hiermit möchte ich euch meine fischertechnik CNC-Fräse vorstellen.
Die Abmaße der Maschine betragen 370 mm Höhe, 320 mm Breite und 300 mm Tiefe.

Die fischertechnik CNC-Fräse besteht aus 6 Encoder-Motoren. 4 alten Encoder Motoren und 2 neuen Encoder Motoren.
Angesteuert werden diese über 2 ROBO TX Controller. 

Vorlage für meine CNC Fräsmaschine waren die Idee von Udo2 der Bohr- und Fräsmaschine BF1-2-RI.
https://ftcommunity.de/details.php?image_id=23560 

Die Wellen sind kugelgelagert incl. des Drehkranzes für den Spindelkopf.

Fahrwege:
X-Achse = 75 mm (links/rechts)
Y-Achse = 42 mm (vor/zurück)
Z-Achse = 42 mm (auf/ab)

A-Achse = 360 Grd
B-Achse = + - 45 Grd
C-Achse = 360 Grd

Power-Motor 8:1
ca. 725 U./min

Stufenlos regelbare Spindelgeschwindigkeit über Rad (Poti)
Beleuchtung ist über Taster ein und ausschaltbar.

Die CNC-Fräse kann über das Bedienpanel mit Menüführung zum Teachen, oder Abrufen der Programme angesteuert werden.
Alternativ Online über ein ROBO Pro Programm. Sie ist in der Lage Steckschaumteile (Simulation) zu bearbeiten.

Hier findet ihr die Videos zur CNC-Fräsmaschine.

fischertechnik CNC-Fräse Teil 1 Simulation
https://www.youtube.com/watch?v=UitXZCra07U

fischertechnik CNC-Fräse Teil 2 Bohren und Fräsen
https://www.youtube.com/watch?v=YZNMv2GfTkE

