---
layout: "image"
title: "Bohr- und Fräsfutter"
date: "2017-02-14T19:27:01"
picture: "fraese21.jpg"
weight: "21"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/45220
imported:
- "2019"
_4images_image_id: "45220"
_4images_cat_id: "3367"
_4images_user_id: "2303"
_4images_image_date: "2017-02-14T19:27:01"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45220 -->
Das Bohr- und Fräsfutter ist von einen Dremel (1 - 3,2 mm )