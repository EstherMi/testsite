---
layout: "image"
title: "Spindelkopf unten"
date: "2017-02-14T19:26:39"
picture: "fraese11.jpg"
weight: "11"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/45210
imported:
- "2019"
_4images_image_id: "45210"
_4images_cat_id: "3367"
_4images_user_id: "2303"
_4images_image_date: "2017-02-14T19:26:39"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45210 -->
Hier sieht man die Beleuchtung mit zwei Leuchtsteinen.