---
layout: "image"
title: "fischertechnik CNC-Fräse hinten"
date: "2017-02-14T19:16:21"
picture: "fraese04.jpg"
weight: "4"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/45203
imported:
- "2019"
_4images_image_id: "45203"
_4images_cat_id: "3367"
_4images_user_id: "2303"
_4images_image_date: "2017-02-14T19:16:21"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45203 -->
