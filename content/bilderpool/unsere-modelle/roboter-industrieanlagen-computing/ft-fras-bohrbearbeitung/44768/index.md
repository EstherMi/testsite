---
layout: "image"
title: "Bearbeitungszentrum"
date: "2016-11-15T17:08:09"
picture: "2016-11-14_10.12.151.jpg"
weight: "3"
konstrukteure: 
- "Fischertechnik"
fotografen:
- "Bernd Langer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- details/44768
imported:
- "2019"
_4images_image_id: "44768"
_4images_cat_id: "3336"
_4images_user_id: "2496"
_4images_image_date: "2016-11-15T17:08:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44768 -->
