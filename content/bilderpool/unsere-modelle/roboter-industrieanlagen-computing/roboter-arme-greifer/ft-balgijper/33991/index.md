---
layout: "image"
title: "steurung"
date: "2012-01-22T17:36:24"
picture: "steuerungk.jpg"
weight: "3"
konstrukteure: 
- "Ton van Beekum"
fotografen:
- "Ton van Beekum"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "beeton"
license: "unknown"
legacy_id:
- details/33991
imported:
- "2019"
_4images_image_id: "33991"
_4images_cat_id: "2517"
_4images_user_id: "1253"
_4images_image_date: "2012-01-22T17:36:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33991 -->
