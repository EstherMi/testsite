---
layout: "image"
title: "Tripod-3"
date: "2009-04-27T21:09:58"
picture: "Tripod-3.jpg"
weight: "3"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/23822
imported:
- "2019"
_4images_image_id: "23822"
_4images_cat_id: "1627"
_4images_user_id: "46"
_4images_image_date: "2009-04-27T21:09:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23822 -->
Mein Arbeitgeber hat es mir dann gestattet, die Idee in Messing, Stahl und Aluminium umzusetzen. Es ist auf der Messe tatsächlich zu einer Attraktion geworden.

Und es funktioniert...