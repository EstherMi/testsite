---
layout: "overview"
title: "Tripod-Roboter"
date: 2019-12-17T18:59:39+01:00
legacy_id:
- categories/1627
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1627 --> 
Drei Achsen frei im Raum beweglich. Dafür keine Rotationen.