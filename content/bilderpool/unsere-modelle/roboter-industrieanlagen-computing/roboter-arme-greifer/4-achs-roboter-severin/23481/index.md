---
layout: "image"
title: "Achse 4 im Detail"
date: "2009-03-20T16:55:25"
picture: "achsroboter9.jpg"
weight: "9"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/23481
imported:
- "2019"
_4images_image_id: "23481"
_4images_cat_id: "1599"
_4images_user_id: "558"
_4images_image_date: "2009-03-20T16:55:25"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23481 -->
Die Achs eist ziemlich genau Gleich wie die Achse 3 aufgebaut.