---
layout: "overview"
title: "4-Achs-Roboter (Severin)"
date: 2019-12-17T18:59:28+01:00
legacy_id:
- categories/1599
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1599 --> 
Bilder von meinem Jugendforschtprojekt 2009. Es ging darum einen Roboter über Potentiometer zu positionieren, Ziel war es die vordere Ablagefläche Parallel zum Boden zu halten.