---
layout: "image"
title: "Von Links"
date: "2009-03-20T16:55:18"
picture: "achsroboter3.jpg"
weight: "3"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/23475
imported:
- "2019"
_4images_image_id: "23475"
_4images_cat_id: "1599"
_4images_user_id: "558"
_4images_image_date: "2009-03-20T16:55:18"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23475 -->
Beim bewegen der 2. Achse hat die platte gut geschaukelt..