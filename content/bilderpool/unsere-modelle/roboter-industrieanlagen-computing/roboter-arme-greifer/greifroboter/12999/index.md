---
layout: "image"
title: "Greifroboter"
date: "2007-12-04T16:58:10"
picture: "greifer2.jpg"
weight: "5"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/12999
imported:
- "2019"
_4images_image_id: "12999"
_4images_cat_id: "1178"
_4images_user_id: "558"
_4images_image_date: "2007-12-04T16:58:10"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12999 -->
