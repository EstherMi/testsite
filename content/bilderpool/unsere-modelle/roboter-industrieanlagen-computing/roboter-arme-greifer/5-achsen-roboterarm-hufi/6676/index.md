---
layout: "image"
title: "6AX  Zusammenbau"
date: "2006-08-13T00:09:36"
picture: "DSC03510.jpg"
weight: "13"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- details/6676
imported:
- "2019"
_4images_image_id: "6676"
_4images_cat_id: "526"
_4images_user_id: "438"
_4images_image_date: "2006-08-13T00:09:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6676 -->
Schön langsam finden alle Einheiten zusammen und die Sensoren werden angebracht