---
layout: "image"
title: "5 Achser -2"
date: "2006-04-11T22:17:07"
picture: "Main2.jpg"
weight: "32"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- details/6062
imported:
- "2019"
_4images_image_id: "6062"
_4images_cat_id: "526"
_4images_user_id: "438"
_4images_image_date: "2006-04-11T22:17:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6062 -->
