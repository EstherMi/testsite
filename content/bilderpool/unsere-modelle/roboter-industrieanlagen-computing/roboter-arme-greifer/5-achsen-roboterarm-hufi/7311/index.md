---
layout: "image"
title: "6AX - Analoge Joysticks für Teach in"
date: "2006-11-03T23:16:07"
picture: "DSC03527.jpg"
weight: "5"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- details/7311
imported:
- "2019"
_4images_image_id: "7311"
_4images_cat_id: "526"
_4images_user_id: "438"
_4images_image_date: "2006-11-03T23:16:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7311 -->
