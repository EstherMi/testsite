---
layout: "image"
title: "6AX Alu Drehkranz verbaut"
date: "2006-05-29T00:33:48"
picture: "0006.jpg"
weight: "31"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- details/6291
imported:
- "2019"
_4images_image_id: "6291"
_4images_cat_id: "526"
_4images_user_id: "438"
_4images_image_date: "2006-05-29T00:33:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6291 -->
Funktioniert perfekt, kann ohne Spiel hohes Kippmoment aufnehmen und bleibt extrem leichtgängig.
Mit dem Powermotor werden mit dieser Übersetzung ca. 70 Grad/Sek. gedreht, mit der geplanten Sensorik eine Auflösung von 0,02 Grad erreicht.