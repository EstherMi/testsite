---
layout: "image"
title: "6AX Motoren Controller"
date: "2006-07-03T22:30:12"
picture: "0502.jpg"
weight: "15"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- details/6595
imported:
- "2019"
_4images_image_id: "6595"
_4images_cat_id: "526"
_4images_user_id: "438"
_4images_image_date: "2006-07-03T22:30:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6595 -->
Erweiterungsboard für die 68HC11F1 CPU vom 5 Achser
Das Board hat 4Stk. 10polige Motorenanschlüsse wobei jeder Anschluß über einen PWM kontrollierbaren Motorausgang bis 1.5A, 2 Digitalausgängen, 1 Digitaleingang und 2 Eingängen für einen Encoder besitzt.
Das PWM wird durch eine IO Adresse geteuert die den Motorstrom linear von 0-100% steuert.
Die Encodereingänge werden jeweils von einen HCTL2022 Chip gezählt (32Bit, max. Zählfrequenz 20MHz) und liegen folglich auf 4 IO Adressen. Der Reset auf 0 erfolgt durch schreiben auf eine der IO´s.
Die Boards können im Adressraum per DIPswitch verschoben werden sodaß max. 4 Board (=16 Motoren) gleichzeitig betrieben werden können.