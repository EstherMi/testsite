---
layout: "comment"
hidden: true
title: "1501"
date: "2006-10-30T23:19:48"
uploadBy:
- "hufi"
license: "unknown"
imported:
- "2019"
---
Habe gerade stundenlang über einer Änderung gebrütet die ich auch soeben gepostet habe.
Die Probleme liegt weniger in der Anzahl der Achsen als in der hohen Auflösungen mit denen das System arbeitet.
Mittlerweile stehen schon etliche Teile der Kontrollsoftware die am Controller laufen sollen, auch ein Teach in System mit analogen Joysticks wir ein Bestandteil des Ganzen sein.
Wie auch immer, man tastet sich Schritt für Schritt voran.
Es wird sicher noch eine Weile dauern bis das Projekt komplett und Videoreif ist, werde aber sobald die 4 Grundachsen laufen ein Filmchen reinstellen, das sollte nicht mehr allzulang dauern.
Die Ausstellungen sind meist doch sehr weit entfernt (Österreich) sollte es sich anders ergeben bin ich sicher dabei.
lg hufi