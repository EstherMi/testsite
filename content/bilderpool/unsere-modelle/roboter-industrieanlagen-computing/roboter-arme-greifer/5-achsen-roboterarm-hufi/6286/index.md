---
layout: "image"
title: "6AX Alu Drehkranz 1"
date: "2006-05-29T00:33:38"
picture: "0001.jpg"
weight: "26"
konstrukteure: 
- "Hufnagl Oliver"
fotografen:
- "Hufnagl Oliver"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hufi"
license: "unknown"
legacy_id:
- details/6286
imported:
- "2019"
_4images_image_id: "6286"
_4images_cat_id: "526"
_4images_user_id: "438"
_4images_image_date: "2006-05-29T00:33:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6286 -->
