---
layout: "image"
title: "Knickarm_mit_leuchte"
date: "2008-07-13T21:17:25"
picture: "p7130061.jpg"
weight: "13"
konstrukteure: 
- "Defiant"
fotografen:
- "Defiant"
keywords: ["knickarm", "roboter", "led", "light", "licht"]
uploadBy: "Defiant"
license: "unknown"
legacy_id:
- details/14833
imported:
- "2019"
_4images_image_id: "14833"
_4images_cat_id: "633"
_4images_user_id: "3"
_4images_image_date: "2008-07-13T21:17:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14833 -->
Blitzidee aus dem Chat:

Wer kennt es nicht? Man hat oft genug Ecken an seinem Modell, an denen man zum Arbeiten nicht genug sieht.

Hier ist die Alternative zur beweglichen Tischlampe oder manuellem halten einer Taschenlampe: (verstaubter) Knickarmroboter mit Lampe :)