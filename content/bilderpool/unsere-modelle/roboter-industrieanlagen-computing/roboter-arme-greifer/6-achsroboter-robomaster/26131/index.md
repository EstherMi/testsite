---
layout: "image"
title: "Potentiometereinheit"
date: "2010-01-24T19:16:54"
picture: "DSCN55372.jpg"
weight: "11"
konstrukteure: 
- "RoboMaster"
fotografen:
- "RoboMaster"
keywords: ["Potentiometereinheit", "Fernsteuerung"]
uploadBy: "RoboMaster"
license: "unknown"
legacy_id:
- details/26131
imported:
- "2019"
_4images_image_id: "26131"
_4images_cat_id: "1852"
_4images_user_id: "1056"
_4images_image_date: "2010-01-24T19:16:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26131 -->
dies ist die Potentiometereinheit, sie ist zurzeit noch nicht im Einsatz, sie entspricht einem kleinemem Nachbau des roboterarm, jedoch mit Potentiometern statt Motoren, später bewegt man die Potentiometereinheit und der Roboter führt die gleichen Bewegungen aus.
So ist eine ideale steuerung möglich.