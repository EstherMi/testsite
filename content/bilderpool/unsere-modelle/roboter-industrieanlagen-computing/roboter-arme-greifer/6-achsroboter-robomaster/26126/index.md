---
layout: "image"
title: "Greifer mit Webcam"
date: "2010-01-24T18:00:46"
picture: "DSCN55322.jpg"
weight: "6"
konstrukteure: 
- "RoboMaster"
fotografen:
- "RoboMaster"
keywords: ["Kamera", "Greifer"]
uploadBy: "RoboMaster"
license: "unknown"
legacy_id:
- details/26126
imported:
- "2019"
_4images_image_id: "26126"
_4images_cat_id: "1852"
_4images_user_id: "1056"
_4images_image_date: "2010-01-24T18:00:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26126 -->
Hier kann man gut den Greifer sehen, der so konstruiert ist dass ,wenn der Moter stärker wäre, er richtig gut zupacken könnte. Die Webcam habe ich noch nicht ausprobiert sie soll mir später am Computer helfen den Roboter exakter zu steuern, da die software dafür zu ungenau wäre.