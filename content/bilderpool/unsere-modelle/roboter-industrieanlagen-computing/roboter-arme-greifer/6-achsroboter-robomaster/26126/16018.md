---
layout: "comment"
hidden: true
title: "16018"
date: "2012-01-07T23:02:30"
uploadBy:
- "lars.blome"
license: "unknown"
imported:
- "2019"
---
Sehr schönes Modell.
Die gleiche Webcam hab ich auch. :)
Benutze aber ein Funkkamera mit eingebautem Akku.
Das ist praktischer, denn man hat keine USB-Kabel, die man verlegen muss. :)

Gruß lars  :)