---
layout: "image"
title: "Roboter ausgestreckt zu größter Höhe"
date: "2010-01-24T17:17:10"
picture: "DSCN55271.jpg"
weight: "1"
konstrukteure: 
- "RoboMaster"
fotografen:
- "RoboMaster"
keywords: ["Roboterarm"]
uploadBy: "RoboMaster"
license: "unknown"
legacy_id:
- details/26121
imported:
- "2019"
_4images_image_id: "26121"
_4images_cat_id: "1852"
_4images_user_id: "1056"
_4images_image_date: "2010-01-24T17:17:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26121 -->
Hier ist der Roboterarm voll ausgestreckt
(Greifer ist über einen Meter vom Erdboden entfernt).