---
layout: "overview"
title: "6-Achsroboter (RoboMaster)"
date: 2019-12-17T18:59:49+01:00
legacy_id:
- categories/1852
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1852 --> 
Roboterarm mit sechs Freiheitsgeraden, der mit einer Poteniometereinheit ferngesteuert werden kann.