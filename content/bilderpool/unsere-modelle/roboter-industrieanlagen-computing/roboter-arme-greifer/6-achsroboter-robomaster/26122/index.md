---
layout: "image"
title: "Roboter mit fernsteuereinheit"
date: "2010-01-24T17:31:26"
picture: "DSCN55282.jpg"
weight: "2"
konstrukteure: 
- "RoboMaster"
fotografen:
- "RoboMaster"
keywords: ["Potentometereinheit", "Fernsteuerung"]
uploadBy: "RoboMaster"
license: "unknown"
legacy_id:
- details/26122
imported:
- "2019"
_4images_image_id: "26122"
_4images_cat_id: "1852"
_4images_user_id: "1056"
_4images_image_date: "2010-01-24T17:31:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26122 -->
Der Roboter mit der Potentometereinheit  zur fernsteuerung aller Achsen ausser des Greifers.