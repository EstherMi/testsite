---
layout: "image"
title: "roboterarm nachforn gebeugt"
date: "2010-01-24T17:31:27"
picture: "DSCN55292.jpg"
weight: "3"
konstrukteure: 
- "RoboMaster"
fotografen:
- "RoboMaster"
keywords: ["Roboterarm"]
uploadBy: "RoboMaster"
license: "unknown"
legacy_id:
- details/26123
imported:
- "2019"
_4images_image_id: "26123"
_4images_cat_id: "1852"
_4images_user_id: "1056"
_4images_image_date: "2010-01-24T17:31:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26123 -->
In dieser Stellung würde man ein Objekt vor dem Roboterarm greifen.
Diese Stellung beansprucht den Roboter am meisten, da der Schwerpunkt des Roboters nicht genau in der Mitte ist.