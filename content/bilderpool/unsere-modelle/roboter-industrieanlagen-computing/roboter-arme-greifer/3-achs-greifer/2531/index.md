---
layout: "image"
title: "Boschzylinder"
date: "2004-06-13T18:31:25"
picture: "Boschzylinder_1.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "charly"
license: "unknown"
legacy_id:
- details/2531
imported:
- "2019"
_4images_image_id: "2531"
_4images_cat_id: "237"
_4images_user_id: "115"
_4images_image_date: "2004-06-13T18:31:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2531 -->
In stationären Anlagen kann man gut auch
passende Industrieteile einbauen.
Hier ein Boschzylinder mit Führung und
50 mm Hub. Einen höheren Druck braucht man für die Teile schon. Ich arbeite mit 
2-2,5 bar.