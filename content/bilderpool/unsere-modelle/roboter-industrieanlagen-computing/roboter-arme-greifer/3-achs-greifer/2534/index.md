---
layout: "image"
title: "schwenkmodul"
date: "2004-06-13T18:31:26"
picture: "Schwenkmodul_3.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "charly"
license: "unknown"
legacy_id:
- details/2534
imported:
- "2019"
_4images_image_id: "2534"
_4images_cat_id: "237"
_4images_user_id: "115"
_4images_image_date: "2004-06-13T18:31:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2534 -->
Schwenkt um 90 Grad.