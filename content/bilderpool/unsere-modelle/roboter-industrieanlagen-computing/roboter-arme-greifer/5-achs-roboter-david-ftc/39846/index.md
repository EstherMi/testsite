---
layout: "image"
title: "Werkzeug"
date: "2014-11-23T19:12:24"
picture: "far2.jpg"
weight: "2"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/39846
imported:
- "2019"
_4images_image_id: "39846"
_4images_cat_id: "2990"
_4images_user_id: "2228"
_4images_image_date: "2014-11-23T19:12:24"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39846 -->
Das Werkzeug des Roboters ist / sind  einer bzw. drei Vakuumsauger. Generell wird nur der mittlere der drei Vakuumsauger zum Ansaugen von Objekten verwendet. Um jedoch größere Bauelemente anzuheben, können die Vakuumsauger links und rechts "zugeschaltet" werden.