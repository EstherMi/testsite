---
layout: "image"
title: "5-Achs Roboter"
date: "2014-11-23T19:12:24"
picture: "far1.jpg"
weight: "1"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/39845
imported:
- "2019"
_4images_image_id: "39845"
_4images_cat_id: "2990"
_4images_user_id: "2228"
_4images_image_date: "2014-11-23T19:12:24"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39845 -->
Achsenübersicht:
1. X-Achse: Achse für horizontale Bewegungen
2. Y-Achse: Achse zum drehen des Aufbaus
3 / 4. A / B-Achse: Achsen zum Ausrichten des Armes
5. C-Achse: Achse zum Ausgleichen, diese wird benötigt, um den Saugnapf zu kippen
Die X Achse wird mit einem Powermotor betrieben, die restlichen vier Achsen werden von je einem Encodermotor angetrieben. Dieser ist erfordlich, um den Roboter relativ genau zu steuern.

Der Roboter ist so programmiert, dass man dem Programm lediglich die Position und den Winkel des Saugnapfes angibt. Das Programm errechnet die Winkel der einzelnen Achsen A bis C und steurert dann die Maschine. Während sich der Roboter bewegt, ist der Saugnapf stets zum Boden gerichtet. (Diese Funktion ist im Programm verankert)