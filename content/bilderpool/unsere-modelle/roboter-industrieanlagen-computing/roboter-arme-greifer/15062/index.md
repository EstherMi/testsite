---
layout: "image"
title: "Schweißroboter"
date: "2008-08-21T07:55:11"
picture: "Schweiroboter.jpg"
weight: "12"
konstrukteure: 
- "-Michael-"
fotografen:
- "-Michael-"
keywords: ["Schweißroboter", "-Michael-", "Dreharm", "Roboter", "Interface"]
uploadBy: "-Michael-"
license: "unknown"
legacy_id:
- details/15062
imported:
- "2019"
_4images_image_id: "15062"
_4images_cat_id: "633"
_4images_user_id: "820"
_4images_image_date: "2008-08-21T07:55:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15062 -->
