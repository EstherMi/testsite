---
layout: "image"
title: "Antrieb Schulter und Ellenbogen"
date: "2012-10-22T19:11:45"
picture: "achsroboterprototypwerner2.jpg"
weight: "2"
konstrukteure: 
- "Jan Werner"
fotografen:
- "Jan Werner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "werner"
license: "unknown"
legacy_id:
- details/36032
imported:
- "2019"
_4images_image_id: "36032"
_4images_cat_id: "2681"
_4images_user_id: "1196"
_4images_image_date: "2012-10-22T19:11:45"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36032 -->
Die schwarzen Statikstreben hinten müsst ihr euch wegdenken, die entlasten die Kette im Ruhezustand