---
layout: "image"
title: "Greifer"
date: "2012-10-22T19:11:45"
picture: "achsroboterprototypwerner6.jpg"
weight: "6"
konstrukteure: 
- "Jan Werner"
fotografen:
- "Jan Werner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "werner"
license: "unknown"
legacy_id:
- details/36036
imported:
- "2019"
_4images_image_id: "36036"
_4images_cat_id: "2681"
_4images_user_id: "1196"
_4images_image_date: "2012-10-22T19:11:45"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36036 -->
Die Spindel und die beiden Spindelmuttern sind von TST
Die Statikstreben halten die Kesselhalter gerade