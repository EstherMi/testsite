---
layout: "comment"
hidden: true
title: "6954"
date: "2008-08-27T17:14:00"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Hallo Michael,

das ist der normale Sortierkasten ft 1000 in der früheren grauen Version. Aktuell ist der Kasten blau und der Deckel schwarz.

Gruß,
Stefan