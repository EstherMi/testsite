---
layout: "image"
title: "Das Gewicht"
date: "2009-06-10T14:57:07"
picture: "knickarmroboter10.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- details/24300
imported:
- "2019"
_4images_image_id: "24300"
_4images_cat_id: "1664"
_4images_user_id: "920"
_4images_image_date: "2009-06-10T14:57:07"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24300 -->
Es wiegt 4-5 kg. Zur Halterung hab ich Statik verwendet, da diese dafür vorgesehen ist, sich verbiegen zu können. Eine Halterung mit normalen Bausteinen würde diese über kurz oder lang zerstören.