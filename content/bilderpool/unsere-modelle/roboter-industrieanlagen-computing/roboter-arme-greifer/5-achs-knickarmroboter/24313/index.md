---
layout: "image"
title: "Greifer"
date: "2009-06-10T14:57:08"
picture: "knickarmroboter23.jpg"
weight: "23"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- details/24313
imported:
- "2019"
_4images_image_id: "24313"
_4images_cat_id: "1664"
_4images_user_id: "920"
_4images_image_date: "2009-06-10T14:57:08"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24313 -->
Der Greifer in nahaufnahme