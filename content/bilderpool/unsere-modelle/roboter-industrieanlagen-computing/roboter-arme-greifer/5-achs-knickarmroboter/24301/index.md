---
layout: "image"
title: "Achse 3 - Antrieb"
date: "2009-06-10T14:57:07"
picture: "knickarmroboter11.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- details/24301
imported:
- "2019"
_4images_image_id: "24301"
_4images_cat_id: "1664"
_4images_user_id: "920"
_4images_image_date: "2009-06-10T14:57:07"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24301 -->
hier sieht man den Antrieb der 3. Achse. Der Motor ist nach hinten gesetzt, dadurch kann die 3. Achse um 340° gedreht werden.