---
layout: "image"
title: "Achse 2 Befestigung"
date: "2009-06-10T14:57:07"
picture: "knickarmroboter07.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- details/24297
imported:
- "2019"
_4images_image_id: "24297"
_4images_cat_id: "1664"
_4images_user_id: "920"
_4images_image_date: "2009-06-10T14:57:07"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24297 -->
Um den Arm aus den Drehkränzen zu bekommen, müssen die beiden Alus voneinander weggezogen werden. Da sie aber mehrmals in der Mitte zusammengehalten werden, ist das nicht möglich.