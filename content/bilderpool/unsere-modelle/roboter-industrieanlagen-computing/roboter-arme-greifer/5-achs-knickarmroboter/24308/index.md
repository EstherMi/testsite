---
layout: "image"
title: "Achse 4 Antrieb"
date: "2009-06-10T14:57:08"
picture: "knickarmroboter18.jpg"
weight: "18"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- details/24308
imported:
- "2019"
_4images_image_id: "24308"
_4images_cat_id: "1664"
_4images_user_id: "920"
_4images_image_date: "2009-06-10T14:57:08"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24308 -->
Da ich keinen Drehkranz mehr hatte war das die platzsparendste Lösung.