---
layout: "image"
title: "Bild 2"
date: "2006-03-28T14:54:55"
picture: "S240.jpg"
weight: "2"
konstrukteure: 
- "erkn"
fotografen:
- "erkn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ekneubuehl"
license: "unknown"
legacy_id:
- details/5978
imported:
- "2019"
_4images_image_id: "5978"
_4images_cat_id: "540"
_4images_user_id: "428"
_4images_image_date: "2006-03-28T14:54:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5978 -->
