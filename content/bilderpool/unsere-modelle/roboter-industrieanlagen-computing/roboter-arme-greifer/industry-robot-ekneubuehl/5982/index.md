---
layout: "image"
title: "Bild6"
date: "2006-03-28T14:54:55"
picture: "S2400002.jpg"
weight: "6"
konstrukteure: 
- "erkn"
fotografen:
- "erkn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ekneubuehl"
license: "unknown"
legacy_id:
- details/5982
imported:
- "2019"
_4images_image_id: "5982"
_4images_cat_id: "540"
_4images_user_id: "428"
_4images_image_date: "2006-03-28T14:54:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5982 -->
