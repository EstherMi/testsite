---
layout: "image"
title: "Joy2Rob 3"
date: "2008-09-30T22:22:56"
picture: "IMGP8025.jpg"
weight: "3"
konstrukteure: 
- "Friedrich Mütschele nach FT Anleitung Modifiziert"
fotografen:
- "Friedrich Mütschele"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fridl"
license: "unknown"
legacy_id:
- details/15759
imported:
- "2019"
_4images_image_id: "15759"
_4images_cat_id: "1441"
_4images_user_id: "753"
_4images_image_date: "2008-09-30T22:22:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15759 -->
