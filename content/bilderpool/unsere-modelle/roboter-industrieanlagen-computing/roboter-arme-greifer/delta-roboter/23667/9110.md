---
layout: "comment"
hidden: true
title: "9110"
date: "2009-04-30T20:30:44"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Heiko,
da kann ich nur bedingt zustimmen, weil Folgendes dennoch zu beachten ist: Die Stabilität gilt für Belastungen in Richtung kleiner 90°. Umgekehrt in Richtung grösser 90° gibt die Verbinderseite wegen der Elatizität oder eines schlechten Sitzes des Verbinders nach. Eine Stabilität in beiden Richtungen ist wie hier gezeigt ist nur möglich mit zwei entgegensetzt angeordneten Anbauwinkeln und eines an beiden Anbauwinkeln auf der Verbinderseite durchgehenden Alu-Profils. Das wollte ich hiermit Irrtümern vorbeugend nochmals deutlich machen. 
Gruss, Udo2