---
layout: "image"
title: "Delta Version 2 Fuß"
date: "2009-04-12T23:44:36"
picture: "Delta_Oberarm-005.jpg"
weight: "5"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: ["Anbauwinkel", "Aluprofil"]
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/23667
imported:
- "2019"
_4images_image_id: "23667"
_4images_cat_id: "1193"
_4images_user_id: "9"
_4images_image_date: "2009-04-12T23:44:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23667 -->
Eine Menge Aluminium zur Abstützung. Sieht elegant aus und ist wahnsinnig stabil, wenn man es erstmal alles zusammengebaut hat. 

Ich muss einfach mal wieder Werbung für Anbauwinkel machen. Diese Teile geben eine wahnsinnige Stabilität, nicht nur dort, wo Aluprofile liegen.