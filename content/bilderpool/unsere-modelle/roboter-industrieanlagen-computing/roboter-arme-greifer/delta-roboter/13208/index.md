---
layout: "image"
title: "Delta-Roboter Version 1 Mittellage"
date: "2008-01-03T02:44:57"
picture: "delta-prototyp-4.jpg"
weight: "9"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/13208
imported:
- "2019"
_4images_image_id: "13208"
_4images_cat_id: "1193"
_4images_user_id: "9"
_4images_image_date: "2008-01-03T02:44:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13208 -->
Ein Ellbogen kann gezielt bewegt werden, wenn die beiden anderen Schultern sich bewegen. Die dritte Schulter ruht. Hier: Mittellage.