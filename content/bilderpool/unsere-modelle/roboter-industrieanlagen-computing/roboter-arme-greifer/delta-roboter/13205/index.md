---
layout: "image"
title: "Delta-Roboter Version 1 Antrieb"
date: "2008-01-03T02:41:39"
picture: "delta-prototyp-1.jpg"
weight: "6"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: ["Delta", "roboter", "kinematik", "roboterarm", "computing"]
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/13205
imported:
- "2019"
_4images_image_id: "13205"
_4images_cat_id: "1193"
_4images_user_id: "9"
_4images_image_date: "2008-01-03T02:41:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13205 -->
Antrieb per Schrittmotor, Lagerung der Achsen verbesserungswürdig.