---
layout: "image"
title: "Delta Version 2 Schulter"
date: "2009-04-12T23:29:52"
picture: "Delta_Oberarm-002.jpg"
weight: "2"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/23664
imported:
- "2019"
_4images_image_id: "23664"
_4images_cat_id: "1193"
_4images_user_id: "9"
_4images_image_date: "2009-04-12T23:29:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23664 -->
Auf diese Art kann man eine Achse so blockieren, dass sie noch frei rotieren kann, sich aber nicht mehr verschiebt. Das Gelenk hat also zwei Freiheitsgrade mit dem Antrieb als eine Zwangsbedingung.

Die Winkelsteine 7.5 sind wichtig: Die Nabe ist länger als 20mm, also klappt es mit Bausteinen 5 und einer Bauplatte 15x60 nicht, die Bausteine 15 mit Loch zu verbinden. Mit ein bisschen Biegen funktioniert es aber.