---
layout: "image"
title: "Roboter Ansicht von oben"
date: "2011-01-06T11:36:24"
picture: "achsrobotermitfoerderband07.jpg"
weight: "7"
konstrukteure: 
- "Janft"
fotografen:
- "Janft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- details/29622
imported:
- "2019"
_4images_image_id: "29622"
_4images_cat_id: "2167"
_4images_user_id: "1164"
_4images_image_date: "2011-01-06T11:36:24"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29622 -->
Es sind:
3 mini Motoren
1 Power Motor
8 Taster
verbaut