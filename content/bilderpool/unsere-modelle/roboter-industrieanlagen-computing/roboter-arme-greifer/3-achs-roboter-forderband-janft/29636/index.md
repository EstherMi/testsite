---
layout: "image"
title: "2. Lichtschranke und Antrieb Förderband"
date: "2011-01-06T11:36:24"
picture: "achsrobotermitfoerderband21.jpg"
weight: "21"
konstrukteure: 
- "Janft"
fotografen:
- "Janft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- details/29636
imported:
- "2019"
_4images_image_id: "29636"
_4images_cat_id: "2167"
_4images_user_id: "1164"
_4images_image_date: "2011-01-06T11:36:24"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29636 -->
Anschluss an Extension:
Lampe M3
Transistor I1