---
layout: "image"
title: "Hebeturm"
date: "2011-01-06T11:36:24"
picture: "achsrobotermitfoerderband10.jpg"
weight: "10"
konstrukteure: 
- "Janft"
fotografen:
- "Janft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- details/29625
imported:
- "2019"
_4images_image_id: "29625"
_4images_cat_id: "2167"
_4images_user_id: "1164"
_4images_image_date: "2011-01-06T11:36:24"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29625 -->
wird von einem Power Motor angetrieben