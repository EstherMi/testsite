---
layout: "image"
title: "Gesamtansicht"
date: "2011-01-06T11:36:23"
picture: "achsrobotermitfoerderband01.jpg"
weight: "1"
konstrukteure: 
- "Janft"
fotografen:
- "Janft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- details/29616
imported:
- "2019"
_4images_image_id: "29616"
_4images_cat_id: "2167"
_4images_user_id: "1164"
_4images_image_date: "2011-01-06T11:36:23"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29616 -->
Der  Roboter ist aus dem Set "Industry Robots" das Förderband ist Marke Eigenbau