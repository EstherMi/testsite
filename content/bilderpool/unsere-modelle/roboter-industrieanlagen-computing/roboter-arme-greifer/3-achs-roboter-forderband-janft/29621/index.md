---
layout: "image"
title: "Verkabelung Förderband"
date: "2011-01-06T11:36:24"
picture: "achsrobotermitfoerderband06.jpg"
weight: "6"
konstrukteure: 
- "Janft"
fotografen:
- "Janft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- details/29621
imported:
- "2019"
_4images_image_id: "29621"
_4images_cat_id: "2167"
_4images_user_id: "1164"
_4images_image_date: "2011-01-06T11:36:24"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29621 -->
Es sind

2Lampen
1 Motor
2Fototransistoren
verbaut