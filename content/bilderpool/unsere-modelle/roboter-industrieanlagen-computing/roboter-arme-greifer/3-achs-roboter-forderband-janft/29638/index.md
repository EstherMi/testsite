---
layout: "image"
title: "Stabilisierung des Förderbandes"
date: "2011-01-06T11:36:24"
picture: "achsrobotermitfoerderband23.jpg"
weight: "23"
konstrukteure: 
- "Janft"
fotografen:
- "Janft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- details/29638
imported:
- "2019"
_4images_image_id: "29638"
_4images_cat_id: "2167"
_4images_user_id: "1164"
_4images_image_date: "2011-01-06T11:36:24"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29638 -->
... damit das Förderband nicht durchhängt