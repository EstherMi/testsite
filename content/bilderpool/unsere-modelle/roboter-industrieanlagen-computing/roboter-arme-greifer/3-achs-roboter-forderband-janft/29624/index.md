---
layout: "image"
title: "Drehkranz"
date: "2011-01-06T11:36:24"
picture: "achsrobotermitfoerderband09.jpg"
weight: "9"
konstrukteure: 
- "Janft"
fotografen:
- "Janft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- details/29624
imported:
- "2019"
_4images_image_id: "29624"
_4images_cat_id: "2167"
_4images_user_id: "1164"
_4images_image_date: "2011-01-06T11:36:24"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29624 -->
wird von einem Mini Mot angetrieben