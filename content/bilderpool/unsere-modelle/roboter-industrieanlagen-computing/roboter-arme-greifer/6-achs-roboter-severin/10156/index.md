---
layout: "image"
title: "Neues Handgelenk"
date: "2007-04-23T21:15:31"
picture: "handgelenk2.jpg"
weight: "9"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/10156
imported:
- "2019"
_4images_image_id: "10156"
_4images_cat_id: "869"
_4images_user_id: "558"
_4images_image_date: "2007-04-23T21:15:31"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10156 -->
Die Rutschkuplung im Detail