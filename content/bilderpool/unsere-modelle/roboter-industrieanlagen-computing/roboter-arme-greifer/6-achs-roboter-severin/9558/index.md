---
layout: "image"
title: "Neues Handgelenk von Unten"
date: "2007-03-18T11:28:25"
picture: "achsroboter2.jpg"
weight: "31"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/9558
imported:
- "2019"
_4images_image_id: "9558"
_4images_cat_id: "869"
_4images_user_id: "558"
_4images_image_date: "2007-03-18T11:28:25"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9558 -->
Hat jemand Verbesserungsvorschläge?