---
layout: "image"
title: "Sensorkopf"
date: "2008-04-21T16:01:00"
picture: "robi2_2.jpg"
weight: "2"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/14347
imported:
- "2019"
_4images_image_id: "14347"
_4images_cat_id: "869"
_4images_user_id: "558"
_4images_image_date: "2008-04-21T16:01:00"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14347 -->
Mit diesem "Kopf" möchte ich den roboter den Roboexplorerparcour abfahren lassen. Bis es soweit ist muss ersteinmal ein 2. ROBO int. her.