---
layout: "comment"
hidden: true
title: "6347"
date: "2008-04-28T17:51:47"
uploadBy:
- "kehrblech"
license: "unknown"
imported:
- "2019"
---
Das wird schwierig werden. Die US-Sensoren können keine Abstände < 3cm messen, die anderen Sensoren dürfen höchstens 3cm vom zu messenden Objekt entfernt sein. Das heißt du musst so nah an den Parcour, dass die US-Sensoren 3cm melden. Dann merkt der Roboter aber nicht mehr, ob er aufsetzt oder nicht, da die US-Sensoren ja keine Entfernungen unter 3cm messen können.
Eventuell hilft es die US-Sensoren etwas weiter nach hinten zu setzen.
Ach ja, hast du dir schon Gedanken über die Programmmierung gemacht?