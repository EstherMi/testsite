---
layout: "image"
title: "Drehpunkt"
date: "2007-03-15T13:57:02"
picture: "IMG_1249.jpg"
weight: "41"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/9526
imported:
- "2019"
_4images_image_id: "9526"
_4images_cat_id: "869"
_4images_user_id: "558"
_4images_image_date: "2007-03-15T13:57:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9526 -->
Das erwünschte scharfe Bild. Was man auf dem unscharfen Bild nicht erkennen konnte ist, dass das Zahnrad vom motor in das Z20 eingreift.