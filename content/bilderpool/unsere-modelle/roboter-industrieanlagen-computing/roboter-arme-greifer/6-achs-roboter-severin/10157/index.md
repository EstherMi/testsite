---
layout: "image"
title: "Neues Handgelenk"
date: "2007-04-23T21:15:31"
picture: "handgelenk3.jpg"
weight: "10"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/10157
imported:
- "2019"
_4images_image_id: "10157"
_4images_cat_id: "869"
_4images_user_id: "558"
_4images_image_date: "2007-04-23T21:15:31"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10157 -->
Ich hab das Problem mit der Zange die sich bei der Drehung langsam öffnet mit einer Rutschkuplung gelöst