---
layout: "image"
title: "6-Achsiger Knickarmroboter"
date: "2007-03-27T20:34:49"
picture: "achsroboter05.jpg"
weight: "19"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/9807
imported:
- "2019"
_4images_image_id: "9807"
_4images_cat_id: "869"
_4images_user_id: "558"
_4images_image_date: "2007-03-27T20:34:49"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9807 -->
Befestigung der Schnecke die die Zange schließt.