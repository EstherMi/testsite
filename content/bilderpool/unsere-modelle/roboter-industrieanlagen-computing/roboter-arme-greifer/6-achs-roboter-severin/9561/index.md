---
layout: "image"
title: "Achse 4 Motorisierung"
date: "2007-03-18T11:28:25"
picture: "achsroboter5.jpg"
weight: "34"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/9561
imported:
- "2019"
_4images_image_id: "9561"
_4images_cat_id: "869"
_4images_user_id: "558"
_4images_image_date: "2007-03-18T11:28:25"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9561 -->
