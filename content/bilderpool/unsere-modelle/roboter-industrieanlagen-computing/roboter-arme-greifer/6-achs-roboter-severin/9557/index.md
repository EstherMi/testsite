---
layout: "image"
title: "Neues Handgelenk von oben"
date: "2007-03-18T11:28:25"
picture: "achsroboter1.jpg"
weight: "30"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/9557
imported:
- "2019"
_4images_image_id: "9557"
_4images_cat_id: "869"
_4images_user_id: "558"
_4images_image_date: "2007-03-18T11:28:25"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9557 -->
Hat jemand Verbesserungsvorschläge?