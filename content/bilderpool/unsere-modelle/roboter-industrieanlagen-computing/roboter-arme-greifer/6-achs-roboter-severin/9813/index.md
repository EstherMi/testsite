---
layout: "image"
title: "6-Achsiger Knickarmroboter"
date: "2007-03-27T20:35:55"
picture: "achsroboter12.jpg"
weight: "25"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/9813
imported:
- "2019"
_4images_image_id: "9813"
_4images_cat_id: "869"
_4images_user_id: "558"
_4images_image_date: "2007-03-27T20:35:55"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9813 -->
Achse 4