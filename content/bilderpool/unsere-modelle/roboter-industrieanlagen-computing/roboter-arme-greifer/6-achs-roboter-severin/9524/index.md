---
layout: "image"
title: "Seitenansicht"
date: "2007-03-15T13:57:02"
picture: "IMG_1251.jpg"
weight: "39"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/9524
imported:
- "2019"
_4images_image_id: "9524"
_4images_cat_id: "869"
_4images_user_id: "558"
_4images_image_date: "2007-03-15T13:57:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9524 -->
Der Roboarm auf der Grundplatte.