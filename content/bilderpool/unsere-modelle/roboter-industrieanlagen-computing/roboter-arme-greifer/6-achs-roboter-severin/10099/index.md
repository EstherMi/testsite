---
layout: "image"
title: "6-Achsrobo"
date: "2007-04-19T20:05:30"
picture: "roboterseverin3.jpg"
weight: "14"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/10099
imported:
- "2019"
_4images_image_id: "10099"
_4images_cat_id: "869"
_4images_user_id: "558"
_4images_image_date: "2007-04-19T20:05:30"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10099 -->
