---
layout: "image"
title: "Fernsteuerung mit Relais"
date: "2007-03-18T11:28:25"
picture: "achsroboter6.jpg"
weight: "35"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/9562
imported:
- "2019"
_4images_image_id: "9562"
_4images_cat_id: "869"
_4images_user_id: "558"
_4images_image_date: "2007-03-18T11:28:25"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9562 -->
Die Fernsteuerung startet das Relais das mit dem Robointerface und dem Intelligent Interface verbunden ist.