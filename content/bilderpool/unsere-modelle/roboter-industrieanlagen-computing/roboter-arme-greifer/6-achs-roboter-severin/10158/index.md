---
layout: "image"
title: "Neues Handgelenk"
date: "2007-04-23T21:15:43"
picture: "handgelenk4.jpg"
weight: "11"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/10158
imported:
- "2019"
_4images_image_id: "10158"
_4images_cat_id: "869"
_4images_user_id: "558"
_4images_image_date: "2007-04-23T21:15:43"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10158 -->
Von oben