---
layout: "image"
title: "Kabelstrang + Übersetzung für ausfahrbares Gegengewicht"
date: "2010-09-18T13:36:54"
picture: "achsroboterarmgreifergesteuertviawebcam13.jpg"
weight: "13"
konstrukteure: 
- "manumffilms"
fotografen:
- "manumffilms"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- details/28175
imported:
- "2019"
_4images_image_id: "28175"
_4images_cat_id: "2044"
_4images_user_id: "934"
_4images_image_date: "2010-09-18T13:36:54"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28175 -->
Leider verschwindet fast die gesamte Übersetzung zwischen den anderen Bauteilen :(