---
layout: "image"
title: "Kopf"
date: "2010-09-18T13:36:54"
picture: "achsroboterarmgreifergesteuertviawebcam15.jpg"
weight: "15"
konstrukteure: 
- "manumffilms"
fotografen:
- "manumffilms"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- details/28177
imported:
- "2019"
_4images_image_id: "28177"
_4images_cat_id: "2044"
_4images_user_id: "934"
_4images_image_date: "2010-09-18T13:36:54"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28177 -->
hier ist der Kopf des Roboters zu erkennen. Man sieht die pneumatische Zange(Greifer und die Achsen 5 + 6.