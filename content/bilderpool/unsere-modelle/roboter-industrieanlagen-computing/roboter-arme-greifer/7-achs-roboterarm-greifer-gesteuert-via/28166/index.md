---
layout: "image"
title: "Motor M1 / Achse 1"
date: "2010-09-18T13:36:53"
picture: "achsroboterarmgreifergesteuertviawebcam04.jpg"
weight: "4"
konstrukteure: 
- "manumffilms"
fotografen:
- "manumffilms"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- details/28166
imported:
- "2019"
_4images_image_id: "28166"
_4images_cat_id: "2044"
_4images_user_id: "934"
_4images_image_date: "2010-09-18T13:36:53"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28166 -->
Hier seht ihr einen von 2 Motoren, die die Achse 1 antreiben(das "Fahren")
Man sieht, dass ich wegen der zum Teil sehr Hohen Belastung auf Teile von TST (Andreas Tacke) gesetzt habe...

