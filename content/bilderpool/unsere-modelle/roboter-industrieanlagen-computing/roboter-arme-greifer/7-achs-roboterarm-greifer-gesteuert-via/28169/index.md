---
layout: "image"
title: "Interfaces"
date: "2010-09-18T13:36:53"
picture: "achsroboterarmgreifergesteuertviawebcam07.jpg"
weight: "7"
konstrukteure: 
- "manumffilms"
fotografen:
- "manumffilms"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- details/28169
imported:
- "2019"
_4images_image_id: "28169"
_4images_cat_id: "2044"
_4images_user_id: "934"
_4images_image_date: "2010-09-18T13:36:53"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28169 -->
Hier sieht man einen Teil der Steuerung, nämlich das Interface + Extension (Intelligent Interface)