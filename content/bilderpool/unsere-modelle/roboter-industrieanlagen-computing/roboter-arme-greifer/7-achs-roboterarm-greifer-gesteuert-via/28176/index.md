---
layout: "image"
title: "Achsen 4 + 5"
date: "2010-09-18T13:36:54"
picture: "achsroboterarmgreifergesteuertviawebcam14.jpg"
weight: "14"
konstrukteure: 
- "manumffilms"
fotografen:
- "manumffilms"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- details/28176
imported:
- "2019"
_4images_image_id: "28176"
_4images_cat_id: "2044"
_4images_user_id: "934"
_4images_image_date: "2010-09-18T13:36:54"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28176 -->
Hier seht ihr die Achsen 4 + 5. Auch hier habe ich teils mit TSTs Bauteilen gearbeitet