---
layout: "image"
title: "sonstige Elektronik"
date: "2010-09-18T13:36:53"
picture: "achsroboterarmgreifergesteuertviawebcam08.jpg"
weight: "8"
konstrukteure: 
- "manumffilms"
fotografen:
- "manumffilms"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- details/28170
imported:
- "2019"
_4images_image_id: "28170"
_4images_cat_id: "2044"
_4images_user_id: "934"
_4images_image_date: "2010-09-18T13:36:53"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28170 -->
Hier noch einiges anderes an Steuerung:
Flip Flop, Stromverteiler, Kompressor, Magnetventile, ...

Auf der anderen Seite des Interfaces befinden sich dann noch E-Tec und IR-Empfänger (hier nicht sichtbar!)