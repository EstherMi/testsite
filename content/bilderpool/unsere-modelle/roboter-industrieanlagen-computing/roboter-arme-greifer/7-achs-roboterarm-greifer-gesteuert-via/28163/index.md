---
layout: "image"
title: "allgemeine Ansicht"
date: "2010-09-18T13:36:52"
picture: "achsroboterarmgreifergesteuertviawebcam01.jpg"
weight: "1"
konstrukteure: 
- "manumffilms"
fotografen:
- "manumffilms"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- details/28163
imported:
- "2019"
_4images_image_id: "28163"
_4images_cat_id: "2044"
_4images_user_id: "934"
_4images_image_date: "2010-09-18T13:36:52"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28163 -->
Hier seht ihr mal das Ganze am Stück. Maße ca. 160 x 100 cm