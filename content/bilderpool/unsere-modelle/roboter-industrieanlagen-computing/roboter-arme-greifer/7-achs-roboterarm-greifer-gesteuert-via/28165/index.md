---
layout: "image"
title: "Ablage"
date: "2010-09-18T13:36:52"
picture: "achsroboterarmgreifergesteuertviawebcam03.jpg"
weight: "3"
konstrukteure: 
- "manumffilms"
fotografen:
- "manumffilms"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- details/28165
imported:
- "2019"
_4images_image_id: "28165"
_4images_cat_id: "2044"
_4images_user_id: "934"
_4images_image_date: "2010-09-18T13:36:52"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28165 -->
Das ist die Ablage, in der der Roboter am Ende eines Durchlaufs den gehobenen Gegenstand(eine ft-Tonne) abliefert. 
Dadurch, dass die Lichtschranke betätigt wird, wird dann ein akustisches Signal ausgegeben (eine Schaltung über Flip-Flop, E-Tec und Summer)

Leider ist das Bild nicht perfekt belichtet :( (mein Fehler)