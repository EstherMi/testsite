---
layout: "image"
title: "[1/7] Modellanlage"
date: "2009-05-04T21:14:31"
picture: "rotopodrp1.jpg"
weight: "1"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/23865
imported:
- "2019"
_4images_image_id: "23865"
_4images_cat_id: "1634"
_4images_user_id: "723"
_4images_image_date: "2009-05-04T21:14:31"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23865 -->
Schnaggels Link http://www.prsco.com/rotopod.html war der Auslöser, dass ich mich mit dem Modellthema "Rotopod in ft" spontan versucht habe.
Kranzringführung, schlanke Encodermotoren, Getriebemechanik und Kugelgelenke der Stützbeine des Originals zeigten sich als die Knackpunkte.
Viel Zeit war nicht - im Mai beginnt die Freiluftsaison meines Ehrenamtes als Wegewart - und aktuell greifbares ft aus meinem Bestand sollte eingesetzt werden.
Die Stützbeinantriebe und -gelenke ließen sich nur über Ersatzlösungen als Modell realisieren. 
Die zu meiner Bohr- und Fräsmaschine BF1-2 hilfsweise mit dem Bedienfeld vorgestellte Steuerung dient etwas modifiziert auch hier zunächst als Steuerung. Eine Steuerung mit der Fähigkeit in Echtzeit die erforderlichen Prozesswerte für die Bewegungen zu berechnen ist hier bei einer vorläufigen Auflösung von rechnerisch 3,93 mm/Impuls noch nicht sinnvoll. Diese "Qualität des Synchronlaufs" der 6 Antriebe des Modells scheidet dazu erst mal aus ...
Der von ft angekündigte XM-Motor getriebeseitig modifiziert und mit Encoder käme hier bei der Übersetzung 1:1 mit 75 Impulsen/Umdr. gerechnet auf eine noch zu grobe Auflösung von 0,42 mm/Impuls.
Auf dem Bedienfeld ist erkennbar ein geladenes Ablaufprogramm erprobt mit 31 Schritten zunächst für Grundbewegungen der Tischplatine wie z.B. Senken, Heben, zentrisch und asymmetrisch gekippt Heben und Senken mit und ohne Drehung.