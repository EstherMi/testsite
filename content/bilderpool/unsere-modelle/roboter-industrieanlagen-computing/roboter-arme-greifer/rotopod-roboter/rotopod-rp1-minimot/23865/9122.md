---
layout: "comment"
hidden: true
title: "9122"
date: "2009-05-04T21:40:16"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Schön komplex! Sehe ich das richtig, dass die Taster zwischen den Ringen nur so eine Art Nullstellung realisieren und genauere Positionierungen über Impulstaster an den MiniMot laufen? Die finde ich aber irgendwie nicht auf dem Bild. Und was sagen da die kumulativen Fehler, denn das ist doch letztlich ein Reibantrieb, oder?

Gruß,
Stefan