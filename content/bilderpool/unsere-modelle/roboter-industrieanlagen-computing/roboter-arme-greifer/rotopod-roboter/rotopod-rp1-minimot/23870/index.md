---
layout: "image"
title: "[6/7] Stützbeinantrieb von links"
date: "2009-05-04T21:14:32"
picture: "rotopodrp6.jpg"
weight: "6"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/23870
imported:
- "2019"
_4images_image_id: "23870"
_4images_cat_id: "1634"
_4images_user_id: "723"
_4images_image_date: "2009-05-04T21:14:32"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23870 -->
Das Rad 14 dient der Einstellung der Laufachsen in die Waagerechte. 
Die Referenztaster müssen dann tangential genau justiert werden, damit die Schaltkufen nicht ihre Ecken berühren.
Der Impulstaster ist am BS 7,5 zum Impulsrad radial einstellbar.