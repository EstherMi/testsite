---
layout: "image"
title: "[5/7] Stützbeingetriebe"
date: "2009-05-04T21:14:32"
picture: "rotopodrp5.jpg"
weight: "5"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/23869
imported:
- "2019"
_4images_image_id: "23869"
_4images_cat_id: "1634"
_4images_user_id: "723"
_4images_image_date: "2009-05-04T21:14:32"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23869 -->
Die Kreisbogenführung der Antriebe erfolgt durch die angeschrägten Stirnseiten der Rastritzel. Die Bogenteile müssen deshalb gratfrei sein. Die Laufrollen Ø10mm auf Rastachsen 30 bestehen aus Lagerhülsen und wegen der erforderlichen Haftreibung aus aufgezogenen Schlauchstücken.

Bei einer Fehlsteuerung hat so ein Antrieb die Kraft seinen stehenden Nachbarn etwas anzuschieben ...