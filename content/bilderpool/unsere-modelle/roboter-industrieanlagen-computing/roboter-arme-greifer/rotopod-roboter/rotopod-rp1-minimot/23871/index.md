---
layout: "image"
title: "[7/7] Stützbeinantrieb von rechts"
date: "2009-05-04T21:14:32"
picture: "rotopodrp7.jpg"
weight: "7"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/23871
imported:
- "2019"
_4images_image_id: "23871"
_4images_cat_id: "1634"
_4images_user_id: "723"
_4images_image_date: "2009-05-04T21:14:32"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23871 -->
Die Kabelführung am Stützbeinantrieb ist vorläufig. Sie ändert sich mit der Verfügbarkeit besser geeigneter Motoren und Impulsmessungen. Das wird auch nach Vorliegen der neuen ft-Komponenten möglicherweise zu senkrechten Laufachsen führen.

Wie schrieb doch Remadus in diesen Tagen: Irgendwie riecht das alles nach Fortsetzung ...