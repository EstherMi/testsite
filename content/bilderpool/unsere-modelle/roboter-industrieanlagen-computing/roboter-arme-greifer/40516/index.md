---
layout: "image"
title: "Umrüstung der zwei Drehachsen auf M-Motoren"
date: "2015-02-09T17:13:11"
picture: "S1160007.jpg"
weight: "5"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["M-Motoren"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/40516
imported:
- "2019"
_4images_image_id: "40516"
_4images_cat_id: "633"
_4images_user_id: "579"
_4images_image_date: "2015-02-09T17:13:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40516 -->
Die M-Motoren haben mehr Kraft als die S-Motoren im originalen Aufbau.