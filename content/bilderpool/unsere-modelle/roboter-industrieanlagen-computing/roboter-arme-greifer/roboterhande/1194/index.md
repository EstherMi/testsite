---
layout: "image"
title: "Die Sehnenwinden"
date: "2003-06-20T06:11:03"
picture: "fthandwinde.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- details/1194
imported:
- "2019"
_4images_image_id: "1194"
_4images_cat_id: "12"
_4images_user_id: "27"
_4images_image_date: "2003-06-20T06:11:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1194 -->
Detailaufnahme der Sehnenwinden. Zwar nicht sehr schnell aber dafür kraftvoll: Mit der Roboterhand ließ sich problemlos eine Pikolo Flasche Sekt eingießen. (Eindruck vor JugendForscht Jury mach...;-) Das Handgelenk ließ sich auch bei unter Zug stehenden Sehnen problemlos um 360 Grad drehen. (s.Detailbild Finger) Die Sehnen (leicht gefettet) wurden u.a. in Schläuchen geführt.