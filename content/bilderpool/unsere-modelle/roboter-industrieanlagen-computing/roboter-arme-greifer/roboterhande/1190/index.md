---
layout: "image"
title: "Detail der Hand"
date: "2003-06-20T06:11:03"
picture: "fthandhinten.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- details/1190
imported:
- "2019"
_4images_image_id: "1190"
_4images_cat_id: "12"
_4images_user_id: "27"
_4images_image_date: "2003-06-20T06:11:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1190 -->
Das ist die Antriebsmechanik für die "Schulter" sowie die Winden für die einzelnen Finger