---
layout: "overview"
title: "Knickarm-Roboter (ohne Alus)"
date: 2019-12-17T19:00:07+01:00
legacy_id:
- categories/2934
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2934 --> 
Ein Roboterarm mit drei Freiheitsgraden - ohne Alus, ohne Modding.