---
layout: "image"
title: "'Kniegelenk'"
date: "2014-08-10T15:00:28"
picture: "knickarmroboterohnealus6.jpg"
weight: "6"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/39227
imported:
- "2019"
_4images_image_id: "39227"
_4images_cat_id: "2934"
_4images_user_id: "1126"
_4images_image_date: "2014-08-10T15:00:28"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39227 -->
Auch das Kniegelenk wird durch zwei Drehkränze bewegt, die von einem Power-Motor angetrieben werden. Die beiden Schneckengetriebe werden über Z10er auf beiden Seiten synchronisiert.