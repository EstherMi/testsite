---
layout: "image"
title: "08 Bertha V2 Baustufe (5448)"
date: "2014-08-26T18:59:41"
picture: "08_Bertha_V2_Baustufe_5448.jpg"
weight: "14"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- details/39306
imported:
- "2019"
_4images_image_id: "39306"
_4images_cat_id: "2942"
_4images_user_id: "2106"
_4images_image_date: "2014-08-26T18:59:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39306 -->
Wie 05, aus anderer Perspektive.