---
layout: "image"
title: "04 Bertha V2 Baustufe (5443)"
date: "2014-08-26T18:59:41"
picture: "04_Bertha_V2_Baustufe_5443.jpg"
weight: "10"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- details/39302
imported:
- "2019"
_4images_image_id: "39302"
_4images_cat_id: "2942"
_4images_user_id: "2106"
_4images_image_date: "2014-08-26T18:59:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39302 -->
Der Fuß vollständig auf die Bauplatte gestellt. (Die kleine Bauplatte wird noch ersetzt werden, mit ihr fällt der Roboter derzeit in vielen Positionen um.)