---
layout: "image"
title: "14 Stand Sept: Arm (5483)"
date: "2014-09-04T22:58:48"
picture: "14_Stand_Sept_Arm_5483.jpg"
weight: "2"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- details/39330
imported:
- "2019"
_4images_image_id: "39330"
_4images_cat_id: "2942"
_4images_user_id: "2106"
_4images_image_date: "2014-09-04T22:58:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39330 -->
Der Roboterarm selbst noch einmal etwas näher. Ich habe ihn gegenüber den vorigen Bildern vom August von der anderen Seite fotografiert, damit man en Encodermotor am Arm besser sieht.