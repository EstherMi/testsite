---
layout: "image"
title: "12 Bertha V2 gesamt (5456)"
date: "2014-08-26T18:59:41"
picture: "12_Bertha_V2_gesamt_5456.jpg"
weight: "18"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- details/39310
imported:
- "2019"
_4images_image_id: "39310"
_4images_cat_id: "2942"
_4images_user_id: "2106"
_4images_image_date: "2014-08-26T18:59:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39310 -->
Wie 10, aber aus anderer Perspektive.