---
layout: "image"
title: "09 Bertha V2 gesamt (5450)"
date: "2014-08-26T18:59:41"
picture: "09_Bertha_V2_gesamt_5450.jpg"
weight: "15"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- details/39307
imported:
- "2019"
_4images_image_id: "39307"
_4images_cat_id: "2942"
_4images_user_id: "2106"
_4images_image_date: "2014-08-26T18:59:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39307 -->
Der ganze Arm auf den Fuß montiert. Abgesehen von der zu kleinen Bauplatte ist das Ding so sehr stabil. Wenn man die Bauplatte festhält, kann der Arm so voll ausfahren, auch seitlich gestreckt.