---
layout: "overview"
title: "Bertha V2 (Aug 2014, mit besserem Unterbau)"
date: 2019-12-17T19:00:04+01:00
legacy_id:
- categories/2942
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2942 --> 
Angeregt durch den Knickarm-Roboter von Dirk Fox http://www.ftcommunity.de/categories.php?cat_id=2934 habe ich Bertha noch ein zweites Mal gebaut. Wesentliche Änderungen: 1) Der Fuß ist massiver, um seitliches Ausbrechen zu verhindern. 2) Die Gelenke 2 und 3 haben doppelte Drehkränze und werden über Z10 vom Powermotor gleichzeitig angetrieben, wie bei Dirk. 3) Die Verkabelung gibt's noch nicht, aber so einen Salat wie bei Bertha V1 mach ich nicht nochmal. Ich hoffe auf mehrere Arduinos, die sich über I2C unterhalten, aber das gibt's noch nicht... Fortsetzung folgt.