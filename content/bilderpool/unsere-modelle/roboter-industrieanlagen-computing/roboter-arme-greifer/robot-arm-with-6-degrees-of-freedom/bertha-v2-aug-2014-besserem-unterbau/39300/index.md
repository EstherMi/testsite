---
layout: "image"
title: "02 Bertha V2 Baustufe (5439)"
date: "2014-08-26T18:59:41"
picture: "02_Bertha_V2_Baustufe_5439.jpg"
weight: "8"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- details/39300
imported:
- "2019"
_4images_image_id: "39300"
_4images_cat_id: "2942"
_4images_user_id: "2106"
_4images_image_date: "2014-08-26T18:59:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39300 -->
Wie 01, aber von unten betrachtet. Wieder ist in der Mitte des Drehkranzes das Ende das Aluprofils zu sehen. Auf der Unterseite sind nur BS 15 statt BS 30 wie auf der Oberseite.