---
layout: "image"
title: "Handgelenk (Gelenke 4 bis 6) Draufsicht (3913)"
date: "2014-04-22T16:15:54"
picture: "Handgelenk_Gelenke_4_bis_6_Draufsicht_3913.jpg"
weight: "31"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- details/38622
imported:
- "2019"
_4images_image_id: "38622"
_4images_cat_id: "2884"
_4images_user_id: "2106"
_4images_image_date: "2014-04-22T16:15:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38622 -->
