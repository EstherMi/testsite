---
layout: "image"
title: "Steuerung (3862)"
date: "2014-04-22T16:15:54"
picture: "Steuerung_3862.jpg"
weight: "37"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- details/38628
imported:
- "2019"
_4images_image_id: "38628"
_4images_cat_id: "2884"
_4images_user_id: "2106"
_4images_image_date: "2014-04-22T16:15:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38628 -->
Die Steuerung ist zur Zeit "analog", bis vielleicht irgendwann eine intelligente Lösung per Smartphone, Nunchuk oder ähnlichem fertig ist. Das heißt, für jedes Gelenk gibt es ein Paar Minitaster, um das Gelenk ein- und auszufahren. Die Taster sind wie die Gelenke angeordnet: in der unteren Reihe die Steuerung der Gelenke in "roll"-Dimension (Gelenke 1, 4, 6), in der oberen Reihe die Gelenke in "pitch"-Dimension (Gelenke 2, 3, 5), jeweils vorwärts und rückwärts. 

Hinzu kommt ein Polwendeschalter oben rechts als Ein-/Ausschalter für den Greifer ("ein" wirft den Kompressor an und schaltet das eine Ventil durch, um den Greifer zu schließen; "aus" schaltet das andere Ventil durch, um den Kolben zurückzufahren, und schaltet den Kompressor wieder aus). Der rote Taster oben rechts ist die Kontrolltaste, über die besondere Funktionen der Software (Reset, Programme, Menü) gesteuert werden können. Das ist aber noch in den Anfängen.

Die Taster sind jeweils an einem Arduino-Eingang angeschlossen und ziehen den Eingang auf Masse, so dass die Software die Motoren entsprechend steuern kann. (Gleiches gilt für die Endtaster im Roboter selbst.)