---
layout: "image"
title: "Gelenk 3 - Geschrottete Rastachse (3864)"
date: "2014-04-22T16:15:53"
picture: "Gelenk_3_-_Geschrottete_Rastachse_3864.jpg"
weight: "11"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- details/38602
imported:
- "2019"
_4images_image_id: "38602"
_4images_cat_id: "2884"
_4images_user_id: "2106"
_4images_image_date: "2014-04-22T16:15:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38602 -->
Bisschen viel Drehmoment für das arme Ding. Die war in Gelenk Nr. 3, als es mal krachte.