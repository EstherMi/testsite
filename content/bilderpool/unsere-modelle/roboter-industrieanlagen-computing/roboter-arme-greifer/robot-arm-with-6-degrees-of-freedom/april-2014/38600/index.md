---
layout: "image"
title: "Gelenk 3 (3891)"
date: "2014-04-22T16:15:53"
picture: "Gelenk_3_3891.jpg"
weight: "9"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- details/38600
imported:
- "2019"
_4images_image_id: "38600"
_4images_cat_id: "2884"
_4images_user_id: "2106"
_4images_image_date: "2014-04-22T16:15:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38600 -->
Gelenk 3 ist hier in Nullposition, der Endtaster unten Mitte im Bild ist gedrückt. Gelenk 3 wird von einem grauen Powermotor (20:1) angetrieben, ist also schneller als Gelenk Nr. 2 mit dem roten Powermotor. (Gelenk 2 wurde für bessere Sicht zuvor etwas ausgefahren, so dass Armteil Nr. 2 aufrecht steht.)