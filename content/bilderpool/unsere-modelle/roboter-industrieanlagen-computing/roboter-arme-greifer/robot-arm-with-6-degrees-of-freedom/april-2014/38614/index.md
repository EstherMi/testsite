---
layout: "image"
title: "Gelenk 6 (3914)"
date: "2014-04-22T16:15:53"
picture: "Gelenk_6_3914.jpg"
weight: "23"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- details/38614
imported:
- "2019"
_4images_image_id: "38614"
_4images_cat_id: "2884"
_4images_user_id: "2106"
_4images_image_date: "2014-04-22T16:15:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38614 -->
Hier sieht man den Greifer und die Drehkränze von den Gelenken 4 bis 6. Die Zahnräder auf der linken Seite gehören zum komplizierten Antrieb von Gelenk Nr. 6, das ja vom Motor vor Gelenk 4 angetrieben wird. Hier sind auch die Endtaster von Gelenk Nr. 6 schön zu sehen.