---
layout: "image"
title: "Gelenk 5 (3901)"
date: "2014-04-22T16:15:53"
picture: "Gelenk_5_3901.jpg"
weight: "17"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- details/38608
imported:
- "2019"
_4images_image_id: "38608"
_4images_cat_id: "2884"
_4images_user_id: "2106"
_4images_image_date: "2014-04-22T16:15:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38608 -->
Wieder zurück in der alten Blickrichtung. Der vordere Encodermotor treibt Gelenk Nr. 5 an, welches den Greifer in "pitch"-Dimension schwenken kann (hier in Nullposition; der rechte der beiden gelben Statiksteine auf dem letzten Drehkranz links unten im Bild drückt den Endtaster). Die Konstruktion der Hohlwelle durch den Drehkranz von Gelenk Nr. 4 basiert auf http://www.ftcommunity.de/details.php?image_id=34697 . Es werden ft-Originalteile benutzt, allerdings habe ich ein bisschen dremeln und heißkleben müssen. Die Hohlwelle kann ich leider ohne Auseinanderbauen jetzt nicht dokumentieren, das bleibt für später vorbehalten.