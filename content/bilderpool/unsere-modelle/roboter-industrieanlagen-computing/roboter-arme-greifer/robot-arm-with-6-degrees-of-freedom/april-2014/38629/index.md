---
layout: "image"
title: "Y - Arduino (3856)"
date: "2014-04-22T16:15:54"
picture: "Y_-_Arduino_3856.jpg"
weight: "38"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- details/38629
imported:
- "2019"
_4images_image_id: "38629"
_4images_cat_id: "2884"
_4images_user_id: "2106"
_4images_image_date: "2014-04-22T16:15:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38629 -->
Hier ist der über die Monate gewachsene Kabelsalat am Arduino. Es ist ein Mega 2560 mit zwei Adafruit Motor Shields V2, die jeweils vier Motorausgänge haben. Von den damit insgesamt acht Ausgängen sind sechs für die Motoren gebraucht, die anderen zwei für die Pneumatik des Greifers (Kompressor und Ventile). Die über 50 Ein-/Ausgänge des Mega sind großenteils belegt, weil so viele Taster und Encoder verbaut sind: pro Motor 1 Encoder und 2 Endtaster sowie 2 Steuerungstaster. Die Encodersignale lösen Interrupts im Arduino aus. Weil Arduino nicht so viele Interrupts unterstützt, sind neben den Hardware-Interrupts auch noch Pin Change Interrupts an der Arduino-Software vorbei programmiert.