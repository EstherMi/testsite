---
layout: "image"
title: "Gelenk 1 (3877)"
date: "2014-04-22T16:15:53"
picture: "Gelenk_1_3877.jpg"
weight: "5"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- details/38596
imported:
- "2019"
_4images_image_id: "38596"
_4images_cat_id: "2884"
_4images_user_id: "2106"
_4images_image_date: "2014-04-22T16:15:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38596 -->
Gelenk 1 wird von einem Encodermotor angetrieben. Obwohl dieser ganz unten sitzt, muss er nicht sehr stark sein, denn er dreht den ganzen Turm, was nicht viel Kraft erfordert. Allerdings besteht das Gelenk 1 aus zwei Drehkränzen (siehe nächstes Bild 3879). Gelenk 1 kann sich um fast 360° drehen; von der mechanischen Konstruktion wäre auch Endlosdrehung möglich, aber dann wickeln sich Kabel und Schläuche auf.