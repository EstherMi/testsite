---
layout: "image"
title: "Z - Anzeige (3854)"
date: "2014-04-22T16:15:54"
picture: "Z_-_Anzeige_3854.jpg"
weight: "42"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- details/38633
imported:
- "2019"
_4images_image_id: "38633"
_4images_cat_id: "2884"
_4images_user_id: "2106"
_4images_image_date: "2014-04-22T16:15:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38633 -->
Mit der Anzeige kann man den Status der sechs Gelenke überwachen. Hier ist Motor 1 ("M1") schon kalibiert und kann also anzeigen, dass das Gelenk auf 52% steht. Die anderen Motoren 2 bis 6 sind noch nicht kalibiert und zeigen deshalb "!c" an. (Ist halt nicht so viel Platz.) Man könnte jetzt das Kalibrierungsprogramm starten, siehe Video.

Das Display ist nicht der Brüller, es arbeitet nur mit 9600 Baud, das würde ich nicht nochmal kaufen.