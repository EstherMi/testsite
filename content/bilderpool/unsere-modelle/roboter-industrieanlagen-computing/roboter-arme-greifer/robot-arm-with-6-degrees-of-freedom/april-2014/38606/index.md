---
layout: "image"
title: "Gelenk 4 (3900)"
date: "2014-04-22T16:15:53"
picture: "Gelenk_4_3900.jpg"
weight: "15"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- details/38606
imported:
- "2019"
_4images_image_id: "38606"
_4images_cat_id: "2884"
_4images_user_id: "2106"
_4images_image_date: "2014-04-22T16:15:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38606 -->
Gelenk 4 in 100%-Stellung.