---
layout: "image"
title: "3Achs Rob_04"
date: "2011-03-04T09:23:07"
picture: "3Achs_Rob_4.jpg"
weight: "5"
konstrukteure: 
- "??"
fotografen:
- "con.barriga"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- details/30191
imported:
- "2019"
_4images_image_id: "30191"
_4images_cat_id: "2242"
_4images_user_id: "1284"
_4images_image_date: "2011-03-04T09:23:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30191 -->
