---
layout: "image"
title: "Riesenrad"
date: "2011-04-20T19:47:21"
picture: "Riesenrad_Anim_2720.jpg"
weight: "1"
konstrukteure: 
- "???"
fotografen:
- "POV-Ray"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "con.barriga"
license: "unknown"
legacy_id:
- details/30468
imported:
- "2019"
_4images_image_id: "30468"
_4images_cat_id: "2242"
_4images_user_id: "1284"
_4images_image_date: "2011-04-20T19:47:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30468 -->
Ganz schön kitschig.
Und ein Regenbogen gegen die Sonne??