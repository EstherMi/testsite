---
layout: "image"
title: "Fin-Ray Greifer - Prinzip 2"
date: "2013-10-02T19:18:30"
picture: "DSC00284_800x800.jpg"
weight: "8"
konstrukteure: 
- "MickyW"
fotografen:
- "MickyW"
keywords: ["Fin-Ray"]
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- details/37493
imported:
- "2019"
_4images_image_id: "37493"
_4images_cat_id: "633"
_4images_user_id: "1806"
_4images_image_date: "2013-10-02T19:18:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37493 -->
