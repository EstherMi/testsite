---
layout: "image"
title: "Greifzange von vorne"
date: "2005-05-26T12:15:43"
picture: "Schwenkbare-pneumaticgreifzange004.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["mari", "mari", "mari", "mari", "mari"]
uploadBy: "mari"
license: "unknown"
legacy_id:
- details/4180
imported:
- "2019"
_4images_image_id: "4180"
_4images_cat_id: "352"
_4images_user_id: "189"
_4images_image_date: "2005-05-26T12:15:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4180 -->
