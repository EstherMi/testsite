---
layout: "image"
title: "Gesammtansicht (0.9)"
date: "2009-04-22T17:56:29"
picture: "achsroboterseverin8.jpg"
weight: "8"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/23755
imported:
- "2019"
_4images_image_id: "23755"
_4images_cat_id: "1624"
_4images_user_id: "558"
_4images_image_date: "2009-04-22T17:56:29"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23755 -->
Hier sieht man Wunderschön die extreme Belastung von Achse 1 und 2. Selbst die aluverstärkte Platte hängt in der Luft. Besonders dort sind die Kugellager extrem effektiv. Um den Roboter aus dieser Position zu bewegen reichen auch 2 Motoren aus, ohne Kugellager geht da selbst mit 4 Motoren nichts. Mit 4 Motoren ist an der Spitze sogar noch 200g anhebbar. In der Position macht eher der Schrittmotor Probleme....