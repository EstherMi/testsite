---
layout: "overview"
title: "6-Achs-Roboter 2 (Severin)"
date: 2019-12-17T18:59:37+01:00
legacy_id:
- categories/1624
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1624 --> 
Ein neuer Versuch von mir einen 6-Achser zu Bauen. Diesmal wird mehr auf Gewicht und vor allem auf Stabilität geachtet. Das Hauptziel ist einen schnellen, leichten und robusten Roboter zu bauen.