---
layout: "image"
title: "Ansicht 2"
date: "2007-01-20T16:46:18"
picture: "DSCI0055.jpg"
weight: "12"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: ["Lagerroboter"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/8566
imported:
- "2019"
_4images_image_id: "8566"
_4images_cat_id: "789"
_4images_user_id: "504"
_4images_image_date: "2007-01-20T16:46:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8566 -->
Hier ist er ganz oben.