---
layout: "overview"
title: "Lagerroboter (Olli)"
date: 2019-12-17T18:59:08+01:00
legacy_id:
- categories/789
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=789 --> 
Roboter zum Ein- und Auslagern von Tonnen.