---
layout: "image"
title: "Regalfächer"
date: "2007-01-20T16:46:18"
picture: "DSCI0073.jpg"
weight: "6"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: ["Lagerroboter"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/8559
imported:
- "2019"
_4images_image_id: "8559"
_4images_cat_id: "789"
_4images_user_id: "504"
_4images_image_date: "2007-01-20T16:46:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8559 -->
Hier ist dann die eingelagerte Tonne.