---
layout: "image"
title: "Handgelenk neu 2"
date: "2005-11-23T21:37:32"
picture: "Handgelenk_neu_-_2.jpg"
weight: "2"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: ["Roboterarm", "Handgelenk", "Getriebe", "Kegelrad", "Kegelräder", "Brickwedde", "Sägen", "Säge", "Aluprofil"]
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/5356
imported:
- "2019"
_4images_image_id: "5356"
_4images_cat_id: "186"
_4images_user_id: "9"
_4images_image_date: "2005-11-23T21:37:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5356 -->
