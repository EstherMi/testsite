---
layout: "image"
title: "6drob- 005"
date: "2005-01-03T21:27:49"
picture: "6drob-_005.JPG"
weight: "13"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/3463
imported:
- "2019"
_4images_image_id: "3463"
_4images_cat_id: "186"
_4images_user_id: "5"
_4images_image_date: "2005-01-03T21:27:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3463 -->
