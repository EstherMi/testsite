---
layout: "image"
title: "Handgelenk neu 3"
date: "2005-11-23T21:38:13"
picture: "Handgelenk_neu_-_3.jpg"
weight: "3"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: ["Roboterarm", "Handgelenk", "Getriebe", "Kegelrad", "Kegelräder", "Brickwedde", "Sägen", "Säge", "Aluprofil"]
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/5357
imported:
- "2019"
_4images_image_id: "5357"
_4images_cat_id: "186"
_4images_user_id: "9"
_4images_image_date: "2005-11-23T21:38:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5357 -->
