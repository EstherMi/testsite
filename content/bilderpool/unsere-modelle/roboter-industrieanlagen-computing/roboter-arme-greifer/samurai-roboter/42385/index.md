---
layout: "image"
title: "Samurai Roboter Kopf"
date: "2015-11-15T19:54:42"
picture: "dirkw5.jpg"
weight: "5"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/42385
imported:
- "2019"
_4images_image_id: "42385"
_4images_cat_id: "3154"
_4images_user_id: "2303"
_4images_image_date: "2015-11-15T19:54:42"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42385 -->
Mit dem Drehkranz habe ich den Kopf gedreht.
Links der Antrieb für die Arme.