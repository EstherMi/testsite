---
layout: "image"
title: "Samurai Roboter von hinten"
date: "2015-11-15T19:54:42"
picture: "dirkw2.jpg"
weight: "2"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/42382
imported:
- "2019"
_4images_image_id: "42382"
_4images_cat_id: "3154"
_4images_user_id: "2303"
_4images_image_date: "2015-11-15T19:54:42"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42382 -->
Hier sehr ihr den Roboter von hinten. Huckepack mit 2 Robo TX Controller.