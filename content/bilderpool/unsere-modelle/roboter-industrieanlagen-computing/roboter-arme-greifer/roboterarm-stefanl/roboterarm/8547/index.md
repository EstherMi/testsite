---
layout: "image"
title: "Roboterarm 19"
date: "2007-01-20T16:46:18"
picture: "roboterarm07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/8547
imported:
- "2019"
_4images_image_id: "8547"
_4images_cat_id: "788"
_4images_user_id: "502"
_4images_image_date: "2007-01-20T16:46:18"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8547 -->
