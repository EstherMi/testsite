---
layout: "image"
title: "Roboterarm 15"
date: "2008-09-28T13:58:10"
picture: "roboterarm5.jpg"
weight: "5"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/15657
imported:
- "2019"
_4images_image_id: "15657"
_4images_cat_id: "1369"
_4images_user_id: "502"
_4images_image_date: "2008-09-28T13:58:10"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15657 -->
