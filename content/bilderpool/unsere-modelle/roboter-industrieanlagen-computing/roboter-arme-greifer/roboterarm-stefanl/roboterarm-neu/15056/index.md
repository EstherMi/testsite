---
layout: "image"
title: "Roboterarm 5"
date: "2008-08-17T18:33:42"
picture: "roboterarmneu05.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/15056
imported:
- "2019"
_4images_image_id: "15056"
_4images_cat_id: "1369"
_4images_user_id: "502"
_4images_image_date: "2008-08-17T18:33:42"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15056 -->
