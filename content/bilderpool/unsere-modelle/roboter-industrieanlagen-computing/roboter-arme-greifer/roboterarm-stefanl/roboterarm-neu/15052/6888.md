---
layout: "comment"
hidden: true
title: "6888"
date: "2008-08-20T09:51:16"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo StefanL,
eines möchte ich aber noch abschließend nicht vergessen. Das ft-System ermöglicht mit überwiegend eingesetzten Nylonteilen dynamisch nur eine begrenzte Schwingungssteife. Zwischen ihr und den Beschleunigungs- sowie Bremkräften des Antriebs besteht ein Zusammenhang, aus dem man nicht ausbrechen kann. Ob da ein schneller Direktantrieb mit Power-Motoren die optimale Lösung ist? Beschleunigungs- und Abbremsrampen sind zwar programmierbar, könnten dann aber in den unteren Geschwingigkeitsstufen u.U. ruckelig werden und das System wiederum aufschaukeln.
Gruß Udo2