---
layout: "image"
title: "Roboterarm 18"
date: "2008-09-28T13:58:10"
picture: "roboterarm8.jpg"
weight: "8"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/15660
imported:
- "2019"
_4images_image_id: "15660"
_4images_cat_id: "1369"
_4images_user_id: "502"
_4images_image_date: "2008-09-28T13:58:10"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15660 -->
...dieses Rad...