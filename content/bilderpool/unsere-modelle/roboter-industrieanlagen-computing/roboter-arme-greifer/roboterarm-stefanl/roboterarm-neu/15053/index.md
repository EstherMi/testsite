---
layout: "image"
title: "Roboterarm 2"
date: "2008-08-17T18:33:41"
picture: "roboterarmneu02.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/15053
imported:
- "2019"
_4images_image_id: "15053"
_4images_cat_id: "1369"
_4images_user_id: "502"
_4images_image_date: "2008-08-17T18:33:41"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15053 -->
