---
layout: "image"
title: "Roboterarm 12"
date: "2007-01-18T22:46:10"
picture: "roboterarm12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/8508
imported:
- "2019"
_4images_image_id: "8508"
_4images_cat_id: "783"
_4images_user_id: "502"
_4images_image_date: "2007-01-18T22:46:10"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8508 -->
