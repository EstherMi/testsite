---
layout: "image"
title: "Roboterarm 2"
date: "2007-01-18T22:04:46"
picture: "roboterarm02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/8498
imported:
- "2019"
_4images_image_id: "8498"
_4images_cat_id: "783"
_4images_user_id: "502"
_4images_image_date: "2007-01-18T22:04:46"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8498 -->
