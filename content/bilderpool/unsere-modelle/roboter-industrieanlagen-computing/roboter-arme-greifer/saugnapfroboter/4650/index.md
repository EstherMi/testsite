---
layout: "image"
title: "Saugnapf"
date: "2005-08-26T16:47:18"
picture: "motorisierte_Roboter_074.jpg"
weight: "7"
konstrukteure: 
- "mari"
fotografen:
- "mari"
keywords: ["Eigenbau", "mari"]
uploadBy: "mari"
license: "unknown"
legacy_id:
- details/4650
imported:
- "2019"
_4images_image_id: "4650"
_4images_cat_id: "374"
_4images_user_id: "189"
_4images_image_date: "2005-08-26T16:47:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4650 -->
Hier ist der Saugnapf des Roboters der flache Gegenstände aufnehmen kann.