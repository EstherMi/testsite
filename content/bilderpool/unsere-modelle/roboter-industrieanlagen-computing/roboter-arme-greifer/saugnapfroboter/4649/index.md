---
layout: "image"
title: "Untere Drehfunktion"
date: "2005-08-26T16:47:18"
picture: "motorisierte_Roboter_073.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["mari", "mari", "mari", "mari", "mari"]
uploadBy: "mari"
license: "unknown"
legacy_id:
- details/4649
imported:
- "2019"
_4images_image_id: "4649"
_4images_cat_id: "374"
_4images_user_id: "189"
_4images_image_date: "2005-08-26T16:47:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4649 -->
Hier ist der Motor der ein großes Zahnrad betreibt und damit den Roboter zum Drehen bringt.