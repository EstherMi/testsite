---
layout: "image"
title: "Gesammtansicht"
date: "2006-03-16T22:18:50"
picture: "neu_robot.jpg"
weight: "1"
konstrukteure: 
- "Marius Moosmann"
fotografen:
- "Marius Moosmann"
keywords: ["mari", "mari", "mari", "mari", "mari"]
uploadBy: "mari"
license: "unknown"
legacy_id:
- details/5900
imported:
- "2019"
_4images_image_id: "5900"
_4images_cat_id: "374"
_4images_user_id: "189"
_4images_image_date: "2006-03-16T22:18:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5900 -->
