---
layout: "image"
title: "Gesamtansicht 4-Achs-Roboter"
date: "2015-02-18T15:55:34"
picture: "IMG_0318.jpg"
weight: "5"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["4-Achs-Roboter", "Gabellichtschranke", "Drehachse", "Pneumatik", "Greifer"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/40567
imported:
- "2019"
_4images_image_id: "40567"
_4images_cat_id: "3041"
_4images_user_id: "579"
_4images_image_date: "2015-02-18T15:55:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40567 -->
Da ich mit dem Modell nicht zufrieden war, habe ich ein paar Modifikationen vorgenommen:
- Pneumatik-Greifer statt Schneckengetriebe
- M-Motoren statt S-Motoren
- Gabellichtschranken statt Impulszahnräder + Taster
- zusätzliche Drehachse, um den Arbeitsbereich nicht auf eine Kreislinie beschränken zu müssen