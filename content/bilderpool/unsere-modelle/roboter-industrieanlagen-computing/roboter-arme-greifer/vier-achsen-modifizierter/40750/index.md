---
layout: "image"
title: "Station mit Kamera und Drehteller zur Ziffernerkennung"
date: "2015-04-10T16:19:04"
picture: "IMG_0658.jpg"
weight: "2"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/40750
imported:
- "2019"
_4images_image_id: "40750"
_4images_cat_id: "3041"
_4images_user_id: "579"
_4images_image_date: "2015-04-10T16:19:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40750 -->
