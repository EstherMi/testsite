---
layout: "image"
title: "Vierte Drehachse aus anderer Ansicht"
date: "2015-02-18T15:55:34"
picture: "IMG_0322.jpg"
weight: "7"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Roboter", "Drehachse", "Excenter"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/40569
imported:
- "2019"
_4images_image_id: "40569"
_4images_cat_id: "3041"
_4images_user_id: "579"
_4images_image_date: "2015-02-18T15:55:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40569 -->
