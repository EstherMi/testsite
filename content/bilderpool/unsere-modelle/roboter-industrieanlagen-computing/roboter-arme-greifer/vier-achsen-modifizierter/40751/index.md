---
layout: "image"
title: "Entfernungsmesser am Roboterarm"
date: "2015-04-10T16:19:04"
picture: "IMG_0659.jpg"
weight: "3"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/40751
imported:
- "2019"
_4images_image_id: "40751"
_4images_cat_id: "3041"
_4images_user_id: "579"
_4images_image_date: "2015-04-10T16:19:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40751 -->
