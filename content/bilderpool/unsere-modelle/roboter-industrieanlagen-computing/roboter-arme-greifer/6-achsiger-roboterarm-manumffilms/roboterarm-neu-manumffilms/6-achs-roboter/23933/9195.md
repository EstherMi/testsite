---
layout: "comment"
hidden: true
title: "9195"
date: "2009-05-09T21:52:54"
uploadBy:
- "heiko"
license: "unknown"
imported:
- "2019"
---
Das sieht aus, als wäre da schon eine Menge Hirnschmalz hineingeflossen – Respekt! Das hält schonmal besser als meine Konstruktion damals.

Was mich aber beunruhigt, ist der Antrieb der sechsten Achse. Ich habe die Erfahrung gemacht, dass die Z10 ein großes Umkehrspiel haben. Wenn Du zweimal die Kraft über Z10 überträgst, wie stabil ist dann noch die sechste Achse? Wird die vom Motor noch festgehalten?