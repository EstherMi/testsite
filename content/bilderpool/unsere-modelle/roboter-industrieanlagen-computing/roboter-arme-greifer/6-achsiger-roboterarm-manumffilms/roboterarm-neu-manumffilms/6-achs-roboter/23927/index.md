---
layout: "image"
title: "nochmal die Grungplatte"
date: "2009-05-08T23:33:19"
picture: "achsroboter02.jpg"
weight: "2"
konstrukteure: 
- "manuMFfilms"
fotografen:
- "manuMFfilms"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- details/23927
imported:
- "2019"
_4images_image_id: "23927"
_4images_cat_id: "1642"
_4images_user_id: "934"
_4images_image_date: "2009-05-08T23:33:19"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23927 -->
Grundplatte ... aber jetzt schon mit Kabeln