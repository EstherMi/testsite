---
layout: "image"
title: "zange"
date: "2009-05-08T23:33:19"
picture: "achsroboter10.jpg"
weight: "10"
konstrukteure: 
- "manuMFfilms"
fotografen:
- "manuMFfilms"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- details/23935
imported:
- "2019"
_4images_image_id: "23935"
_4images_cat_id: "1642"
_4images_user_id: "934"
_4images_image_date: "2009-05-08T23:33:19"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23935 -->
die Zange des Roboters