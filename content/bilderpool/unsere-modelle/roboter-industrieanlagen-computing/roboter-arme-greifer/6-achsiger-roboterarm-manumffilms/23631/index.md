---
layout: "image"
title: "ausleger achse 4 - 6(alte Version)"
date: "2009-04-06T10:07:34"
picture: "DSCF3938.jpg"
weight: "10"
konstrukteure: 
- "manuMFfilms"
fotografen:
- "manuMFfilms"
keywords: ["roboter", "roboterarm", "6", "achsig", "manuMFfilms"]
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- details/23631
imported:
- "2019"
_4images_image_id: "23631"
_4images_cat_id: "1612"
_4images_user_id: "934"
_4images_image_date: "2009-04-06T10:07:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23631 -->
