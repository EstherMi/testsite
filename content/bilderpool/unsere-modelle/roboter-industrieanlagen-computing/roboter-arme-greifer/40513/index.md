---
layout: "image"
title: "Mitnehmer am Roboterarm"
date: "2015-02-09T17:13:11"
picture: "S1160004.jpg"
weight: "2"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Winkelachse", "Drehscheibe", "Mitnehmer"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/40513
imported:
- "2019"
_4images_image_id: "40513"
_4images_cat_id: "633"
_4images_user_id: "579"
_4images_image_date: "2015-02-09T17:13:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40513 -->
Details zum Roboterarm-Mitnehmer montiert an einer Drehscheibe. Endlich kam mal eine von diesen 90°-Winkelachsen zum Einsatz, von denen ich Unzählige herumfliegen habe und bisher nie gebrauchen konnte.