---
layout: "image"
title: "Gesamtansicht 4"
date: "2007-01-21T21:01:29"
picture: "roboterarm11.jpg"
weight: "11"
konstrukteure: 
- "Michael K."
fotografen:
- "Michael K."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Wert"
license: "unknown"
legacy_id:
- details/8603
imported:
- "2019"
_4images_image_id: "8603"
_4images_cat_id: "792"
_4images_user_id: "366"
_4images_image_date: "2007-01-21T21:01:29"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8603 -->
Gesamtansicht