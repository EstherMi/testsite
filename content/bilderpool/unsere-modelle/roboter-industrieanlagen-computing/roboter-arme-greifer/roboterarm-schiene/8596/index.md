---
layout: "image"
title: "Unterbau"
date: "2007-01-21T15:27:21"
picture: "roboterarm04.jpg"
weight: "4"
konstrukteure: 
- "Michael K."
fotografen:
- "Michael K."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Wert"
license: "unknown"
legacy_id:
- details/8596
imported:
- "2019"
_4images_image_id: "8596"
_4images_cat_id: "792"
_4images_user_id: "366"
_4images_image_date: "2007-01-21T15:27:21"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8596 -->
Man sieht  Interface, Akku, Fahrschiene und Antriebsmotor des Drehkranzes