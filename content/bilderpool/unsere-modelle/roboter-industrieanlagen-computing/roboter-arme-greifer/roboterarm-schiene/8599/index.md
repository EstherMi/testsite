---
layout: "image"
title: "3. Achse"
date: "2007-01-21T21:01:29"
picture: "roboterarm07.jpg"
weight: "7"
konstrukteure: 
- "Michael K."
fotografen:
- "Michael K."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Wert"
license: "unknown"
legacy_id:
- details/8599
imported:
- "2019"
_4images_image_id: "8599"
_4images_cat_id: "792"
_4images_user_id: "366"
_4images_image_date: "2007-01-21T21:01:29"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8599 -->
Man sieht den Motor für rauf/runter des Arms