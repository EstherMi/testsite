---
layout: "image"
title: "Gesamtansicht_1"
date: "2007-01-21T15:27:21"
picture: "roboterarm01.jpg"
weight: "1"
konstrukteure: 
- "Michael K."
fotografen:
- "Michael K."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Wert"
license: "unknown"
legacy_id:
- details/8593
imported:
- "2019"
_4images_image_id: "8593"
_4images_cat_id: "792"
_4images_user_id: "366"
_4images_image_date: "2007-01-21T15:27:21"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8593 -->
Das ist ein Roboterarm auf einer  Schiene mit 7 Motoren und 14 Tastern. Der Roboter ist mit einem RF Data Link am Computer angeschlossen. Alle Sensoren werden 'mitgefahren'. Das Modell ist noch nicht verkabelt. Zugegeben, der Roboter hat kleine ähnlichkeiten mit Heikos Roboterarm.