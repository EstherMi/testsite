---
layout: "comment"
hidden: true
title: "2119"
date: "2007-01-21T18:05:37"
uploadBy:
- "wahsager"
license: "unknown"
imported:
- "2019"
---
Tierquälerei! Was sagen denn die Tiere zu dem Geschrei der Motoren? ;-)

Nein, im Ernst: Die Konstruktion sieht ziemlich gut aus. Ich hoff mal, du bekommst den Arm und insbesondere das Handgelenk so zum Laufen, wie du's dir vorstellst.