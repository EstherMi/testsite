---
layout: "image"
title: "Robo TX Controller"
date: "2011-03-06T15:38:51"
picture: "roboterausaltemindustryrobotsset04.jpg"
weight: "4"
konstrukteure: 
- "Philip Lawall"
fotografen:
- "Philip Lawall"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fueller"
license: "unknown"
legacy_id:
- details/30203
imported:
- "2019"
_4images_image_id: "30203"
_4images_cat_id: "2244"
_4images_user_id: "1026"
_4images_image_date: "2011-03-06T15:38:51"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30203 -->
Robo TX Controller