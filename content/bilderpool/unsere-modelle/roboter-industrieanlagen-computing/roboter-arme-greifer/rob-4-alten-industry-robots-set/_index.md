---
layout: "overview"
title: "Rob 4 aus dem alten Industry Robots Set"
date: 2019-12-17T18:59:27+01:00
legacy_id:
- categories/2244
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2244 --> 
Ich habe mal den Rob 4 aus dem alten Industry Robots Set nachgebaut. Ich habe micht soweit es ginge an die original Anleitung gehalten. An den Stellen, wo ich mit meinen Teilen nicht weiter kam habe ich ein wenig Improvisiert (wenn ich das so sagen darf?).