---
layout: "image"
title: "Robo TX Controller II"
date: "2011-03-06T15:38:51"
picture: "roboterausaltemindustryrobotsset10.jpg"
weight: "10"
konstrukteure: 
- "Philip Lawall"
fotografen:
- "Philip Lawall"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fueller"
license: "unknown"
legacy_id:
- details/30209
imported:
- "2019"
_4images_image_id: "30209"
_4images_cat_id: "2244"
_4images_user_id: "1026"
_4images_image_date: "2011-03-06T15:38:51"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30209 -->
Robo TX Controller II