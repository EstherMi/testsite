---
layout: "image"
title: "Gesamtansicht"
date: "2011-03-06T15:38:51"
picture: "roboterausaltemindustryrobotsset01.jpg"
weight: "1"
konstrukteure: 
- "Philip Lawall"
fotografen:
- "Philip Lawall"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fueller"
license: "unknown"
legacy_id:
- details/30200
imported:
- "2019"
_4images_image_id: "30200"
_4images_cat_id: "2244"
_4images_user_id: "1026"
_4images_image_date: "2011-03-06T15:38:51"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30200 -->
Gesamtansicht des Roboters