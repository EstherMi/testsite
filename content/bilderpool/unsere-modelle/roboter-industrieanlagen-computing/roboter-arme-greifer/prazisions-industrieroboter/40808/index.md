---
layout: "image"
title: "10-Präzisionsroboter I"
date: "2015-04-18T21:33:57"
picture: "PICT6573.jpg"
weight: "4"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/40808
imported:
- "2019"
_4images_image_id: "40808"
_4images_cat_id: "2528"
_4images_user_id: "46"
_4images_image_date: "2015-04-18T21:33:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40808 -->
Endlich fertig. Alle Motoren an Bord und angekabelt, alle Sensoren installiert und funktionstüchtig.

Das Achskonzept ist jetzt neu. Die Achsen 2, 3 und 4 sind jetzt parallel. Während die Achsen 2 und 3 die Höhe und Ausladung bringen, kompensiert Achse 4 deren Rotationen und hält so die Werkzeugachse 5 in ihrer Orientierung konstant. Zeigt diese Achse beispielsweise nach unten (so wie im Bild) dann kann Achse 5 die Drehung von Achse 1 kompensierten.

Betreibe ich die Achsen so wie oben beschrieben, dann ergeben sich zwar kleinere Einschränkungen, aber die mathematische Beschreibung der Achsen vereinfacht sich dramatisch: Achsen 2, 3 und 4 bilden ein Dreieck, Achse 5 kompensiert Achse 1. So bleibt nur noch eine zarte Umrechnung von kartesischen Koordinaten in Zylinderkoordinaten und eine Dreiecksberechnung übrig. Das sollte zu schaffen sein.

Die Holzplatte unter dem Roboter stabilisiert die Grundplatte, so dass man den Roboter überhaupt hochheben und transportieren kann. Sein Betriebsgewicht ist jetzt 8,2 kg.