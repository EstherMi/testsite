---
layout: "image"
title: "07-Verkabeln-2"
date: "2015-02-08T16:31:35"
picture: "I-Robot-Kabel2.jpg"
weight: "7"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/40509
imported:
- "2019"
_4images_image_id: "40509"
_4images_cat_id: "2528"
_4images_user_id: "46"
_4images_image_date: "2015-02-08T16:31:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40509 -->
Wenn die Verkabelung der ersten Achse auf der Standplatte des Roboters schon nervig war und einen ganzen Tag gedauert hat, denn lässt sich der weitere Aufwand für die nächsten vier Achse schätzen. Im Vordergrund liegt das Päckchen mit den Steuerelementen, die auf das Chassis draufsollen.

Der Aufwand lohnt sich: würde ich die Motoren fernsteuern und die Sensoren zu Hause auswerten, dann müsste ich zwischen Chassis und Steuereinheit 37 Kabel verlegen. Diesen Kabelbaum muss das Chassis hinter sich herziehen, wenn es schwenkt. Das haut nicht hin.

Komplett aufbereitet sind es nur noch 2 Kabel je Motor, eines je Sensor, zwei Kabel für Schrittauflösung und eine Enable-Leitung, sowie Leistung (12 V und 5 V) und Masse. Macht zusammen 18 Kabel.

Im Bild sieht man den allmählich entstehenden Kabelgalgen. Noch ist die Sache wüst und wirr, aber so nach und nach entwirrt sich das immer mehr und wenn die Kabel mal schön gefixt sind, dann sieht das ganze Layout bestimmt schön aufgeräumt aus. Das wird aber noch einige Tage in Anspruch nehmen.