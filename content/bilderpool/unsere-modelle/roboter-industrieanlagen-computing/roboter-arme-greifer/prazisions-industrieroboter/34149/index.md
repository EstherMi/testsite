---
layout: "image"
title: "05-Alle Achsen im Überblick"
date: "2012-02-12T15:04:51"
picture: "05-Roboter.jpg"
weight: "14"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/34149
imported:
- "2019"
_4images_image_id: "34149"
_4images_cat_id: "2528"
_4images_user_id: "46"
_4images_image_date: "2012-02-12T15:04:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34149 -->
Eigentlich hatte ich den Roboter kompakter vorgesehen, aber jetzt, wo ich ihn zusammenstecke, kann ich über die erreichte Größe nur staunen. Maximale Ausladung ca. 50 cm, maximale Reichhöhe ca. 60 cm.

Wenn die Achsbalance aufgeht, Achse fünf den Greifer nicht zur Seite dreht und Achse vier die Orientierung des Greifers konstant hält, dann kann sich der Roboter in seinem gesamten Arbeitsbereich ohne Gewichtsverlagerung bewegen. Die Bewegungsauflösung wird generell bei Zehntelmillimetern liegen und so recht gut aufgelöste Bewegungen ermöglichen. Wie genau die werden, dass muss dann die Messung zeigen. Bis dahin ist noch sehr viel zu tun.

Der Antrieb von Achse drei fehlt noch, nahezu sämtliche Initiatoren und jegliche Verkabelung. Wahrscheinlich entsteht hinter dem Roboter ein Kabelgalgen, der die Kabel dem Drehpunkt von Achse zwei von oben her zuführt. Nicht unbedingt elegant, aber machbar.

Dann wird viel zu programmieren sein. Ein wenig Matrizenoperationen habe ich schon im Griff, aber das Denavit-Hartenberg-Verfahren doch noch nicht gefressen. Ich hoffe, ich pack' das, weil es sich bestimmt lohnt.

Zur nächsten Convention kann ich leider nicht kommen. Wahrscheinlich ist der Roboter bis dahin noch nicht fertig.

Als dann