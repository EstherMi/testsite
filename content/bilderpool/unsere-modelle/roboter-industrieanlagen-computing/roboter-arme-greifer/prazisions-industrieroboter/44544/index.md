---
layout: "image"
title: "13-Präzisionsroboter II"
date: "2016-10-03T19:55:15"
picture: "13-Przisionsroboter_II.jpg"
weight: "2"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/44544
imported:
- "2019"
_4images_image_id: "44544"
_4images_cat_id: "2528"
_4images_user_id: "46"
_4images_image_date: "2016-10-03T19:55:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44544 -->
Er hat sogar gemacht, was er sollte: Das Aufstapeln der kleinen Pyramide aus vier Spielfiguren (drei unten, einer oben) hat nahezu regelmäßig funktioniert.

Und dann hat er mir auch noch den Gefallen getan und die große Pyramide gestapelt. Allerdings muss ich zugeben, dass ich den automatischen Ablauf beim Abstellen des obersten Pylone angehalten habe, eine winzige Koordinatenkorrektur durchgeführt habe (der Pylone musste noch 0,7 mm nach links und 0,5 mm nach vorne) und den letzten Pylon erst dann habe abstellen lassen.

Von Hand habe ich das bis heute noch nicht geschafft.