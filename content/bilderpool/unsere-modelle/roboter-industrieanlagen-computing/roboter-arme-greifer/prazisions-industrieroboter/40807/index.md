---
layout: "image"
title: "10-Neue Motoren"
date: "2015-04-18T21:33:57"
picture: "PICT6564.jpg"
weight: "3"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/40807
imported:
- "2019"
_4images_image_id: "40807"
_4images_cat_id: "2528"
_4images_user_id: "46"
_4images_image_date: "2015-04-18T21:33:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40807 -->
Auf dem Weg der Fertigstellung ist aufgefallen, dass die Achsfolge der beiden letzten Achsen sehr ungeschickt ist. Also tausche ich die beiden Achsen.

Dazu brauche ich aber einen leichten Motor. Bei meiner Recherche entdecke ich das Format NEMA 8. Links ist so ein Motor, dessen Korpus mal gerade 20x20x30 mm groß ist und gerade mal 80 g wiegt. Seine Achse hat das Fischertechnik-Format 4 mm.

Und: bei einer Auflösung von 200 Schritt/Umdr. ist der Motor auch noch mikroschrittfähig. Bei Stepper-online in den USA kriegt man den Motor für 17 Dollar (zurzeit).