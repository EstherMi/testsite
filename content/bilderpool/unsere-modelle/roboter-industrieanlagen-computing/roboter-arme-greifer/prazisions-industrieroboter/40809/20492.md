---
layout: "comment"
hidden: true
title: "20492"
date: "2015-04-19T12:51:58"
uploadBy:
- "uffi"
license: "unknown"
imported:
- "2019"
---
Eine tolle Konstruktion und beeindruckende technische Daten!!! Hut ab!!! Die Schrittmotoren gefallen mir auch! Zum Thema Präzision: welche Positioniergenauigkeit erreichst Du denn? 1 mm? 
Und welche Zeit wird benötigt, um eine bestimmte Position anzufahren?
In den ganzen Aluprofilen und den ganzen Schrittmotoren steckt doch sicher ein Vermögen... 
Was schätzt Du hast Du da in Summe investiert? 
Kannst Du mal ein Video machen, wie Dein Roboter arbeitet?
Danke und Gruß, DirkU