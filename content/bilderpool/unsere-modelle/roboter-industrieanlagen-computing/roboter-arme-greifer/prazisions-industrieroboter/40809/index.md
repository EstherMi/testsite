---
layout: "image"
title: "11 Präzisionsroboter II"
date: "2015-04-18T21:33:57"
picture: "PICT6574.jpg"
weight: "5"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/40809
imported:
- "2019"
_4images_image_id: "40809"
_4images_cat_id: "2528"
_4images_user_id: "46"
_4images_image_date: "2015-04-18T21:33:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40809 -->
Hier noch mal der Blick von hinten: Die beiden Anschlüsse sind rechts eine dreipolige XLR-Buchse für 5 Volt (Elektrik) und 12 Volt (Motoren) und links ein SubD-25-Stecker für die Steuerung. Der Anschluss ist ein ganz normaler Parallel-Port, d. h. der Roboter passt an jeden Rechner, der noch einen Parallel-Port hat.

Die gesamte Steuerungselektrik ist an Bord.

By the way: durch den Umbau der Achsen ist der vordere Ausleger um 150 g leichter geworden. Dadurch, dass das Messinggegengewicht schon fertig war, habe ich die Ausladung der 2. Achse mal eben um 30 mm verlängert. Jetzt hat der Roboter eine ziemliche Ausladung.

Hier einige technischen Daten:
Achse 1 senkrecht, Schwenkbereich +/-90°
Achse 2 waagerecht, Höhe 280 mm, Ausladung 260 mm, Schwenkbereich -60°&#8230;+90°
Achse 3 waagerecht, Ausladung 240 mm, Schwenkbereich technisch 270° möglich
Achse 4 waagerecht, Ausladung variabel je nach Werkzeug, Schwenkbereich 300°
Achse 5 variabel, Ausladung variabel je nach Werkzeug, Schwenkbereich theor. unbegrenzt