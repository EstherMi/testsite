---
layout: "image"
title: "03-Die zweite Achse"
date: "2012-02-12T14:48:21"
picture: "03-Roboter.jpg"
weight: "12"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/34141
imported:
- "2019"
_4images_image_id: "34141"
_4images_cat_id: "2528"
_4images_user_id: "46"
_4images_image_date: "2012-02-12T14:48:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34141 -->
Ist die erste Achse fürwahr ganz schön groß, so steht ihr die zweite Achse wohl in nichts nach. Da beide Achsen sauber mit der Erdschwerkraft kooperieren, können sie alles an Stabilität und Baugröße aufbieten was so geht. Deshalb kommt es zu diesem Entwurf. Doch der Reihe nach von unten nach oben:

Auf die schwarze Platte sind 15x15 mm Baumarkt-Aluprofile aufgelegt, die das Durchbiegen der Platte verhindern und so eine feste Auflage auf den Drehkranz ermöglichen. Unter den roten Klötzen verbirgt sich die Klemmung, das macht das Erscheinungsbild der sonst recht behäbigen Konstruktion ansehnlich und schick.

Gleich darüber steht die Säule der zweiten Achse, die so (Drehkranz mitgezählt) auf 25 cm über  Grund kommt. Die Ausladung der Achse ist 22 cm. Die 39 cm-Alus stehen nach hinten reichlich aus, um später das Gegengewicht aufzunehmen. Die zweite Achse wird also vollständig ausbalanciert, so dass sich später im Betrieb keine Massenverlagerungen ergeben, wenn der Winkel dieser Achse verändert wird. Das wird das Gewicht der Achse mächtig anheben, endet aber nur in Reibung in den großen Drehkränzen rechts und links.

Damit sich nichts unnötig biegt, habe ich einen zweiseitigen Antrieb gewählt. Das vergleichmäßigt die Kräfte, denn sie werden mit Sicherheit auftreten und macht die Achse steifer gegen Verwindung. Im Bild ist auch zu erkennen, wie die Schubkräfte aus den Antriebsschnecken über die Schrägstrebe direkt ins Maschinenfundament übergeleitet werden.

Die Untersetzungen sind 1:3 und 1:58 bei ebenfalls 200 Schritt/Motorumdrehung. Macht zusammen für einen 360°-Schwenk 348000 Schritte oder an der Achsspitze gezählt 2 Mikrometer Auflösung. Das ist jetzt nicht Absicht, sondern dem Getriebe geschuldet und hat den Nachteil, dass sich der Roboter nicht durch Schnelligkeit auszeichnen wird. Mit etwas Glück kann der Schrittmotor auf vielleicht 1000 Schritt/sec gebracht werden. Das gibt eine Vorstellung von der Bewegung.

Die Achse kann praktisch senkrecht aufgerichtet werden, wobei sie auf keinen Fall über Kopf fahren wird. Das schafft mathematisch einen Haufen Scherereien. In der anderen Richtung kann die Achse soweit abgesenkt werden, dass sich der Roboter an die Füsse fassen kann.

Dieses Aggregat bringt jetzt schon 1,3 kg auf die Waage, mit dem Gegengewicht, das noch bestimmt werden muss, wird das Gewicht auf ca. 2,2 kg ansteigen. Das ist aber kein Problem für den Drehkranz, der selber ebenfalls schon 1,3 kg wiegt.