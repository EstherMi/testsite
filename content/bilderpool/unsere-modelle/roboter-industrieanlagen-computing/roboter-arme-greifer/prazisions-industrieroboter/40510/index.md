---
layout: "image"
title: "08-Verkabeln-3"
date: "2015-02-08T16:31:35"
picture: "I-Robot-Kabel3.jpg"
weight: "8"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/40510
imported:
- "2019"
_4images_image_id: "40510"
_4images_cat_id: "2528"
_4images_user_id: "46"
_4images_image_date: "2015-02-08T16:31:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40510 -->
Hier mal der Blick von vorne rechts. Unter dem Schultergelenk münden die Kabel für die höheren Achsen und deren Sensoren. Die Schulterachse kann senkrecht nach oben zeigen bis schräg nach unten. Die Kabel haben damit kein Problem.

Rechts im Bild sieht man einen kleinen Spielpylon in der "Hand" des Roboters. Vielleicht schafft er eines Tages den Umgang mit den kleinen "Männchen". Das wäre perfekt.