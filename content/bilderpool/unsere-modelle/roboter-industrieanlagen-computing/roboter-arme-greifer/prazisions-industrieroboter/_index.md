---
layout: "overview"
title: "Präzisions-Industrieroboter"
date: 2019-12-17T18:59:55+01:00
legacy_id:
- categories/2528
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2528 --> 
Bei diesem Projekt soll ein Handhabungsroboter herauskommen, der seine Positionen sicher und exakt erreicht. Dafür wird ein eigenes, fischertechnik-taugliches Konzept entwickelt.