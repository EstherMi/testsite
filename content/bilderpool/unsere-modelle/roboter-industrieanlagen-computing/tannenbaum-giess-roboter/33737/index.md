---
layout: "image"
title: "Wassersensor"
date: "2011-12-23T19:30:21"
picture: "IMG_3239.jpg"
weight: "3"
konstrukteure: 
- "Simixus"
fotografen:
- "Simixus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Simixus"
license: "unknown"
legacy_id:
- details/33737
imported:
- "2019"
_4images_image_id: "33737"
_4images_cat_id: "2495"
_4images_user_id: "986"
_4images_image_date: "2011-12-23T19:30:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33737 -->
bestehend aus einer Holzleiste, zwei Schrauben sowie jeweils vier Muttern und Unterlegscheiben