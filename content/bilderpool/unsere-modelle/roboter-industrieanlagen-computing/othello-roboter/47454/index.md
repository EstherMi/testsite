---
layout: "image"
title: "Spielsteinwender, Detail rechts"
date: "2018-04-17T22:07:48"
picture: "othelloroboter07.jpg"
weight: "7"
konstrukteure: 
- "hamlet"
fotografen:
- "hamlet"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hamlet"
license: "unknown"
legacy_id:
- details/47454
imported:
- "2019"
_4images_image_id: "47454"
_4images_cat_id: "3505"
_4images_user_id: "1327"
_4images_image_date: "2018-04-17T22:07:48"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47454 -->
Die andere Seite. Das Z30 ist mit drei Federnocken mit dem Drehscheiben-Planetenträger verbunden.
Die Kabelführung sieht abenteuerlich aus, aber sie muss ja nur eine halbe Umdrehung überstehen.