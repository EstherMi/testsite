---
layout: "image"
title: "Spielsteinspender, Detail"
date: "2018-04-17T22:07:48"
picture: "othelloroboter10.jpg"
weight: "10"
konstrukteure: 
- "hamlet"
fotografen:
- "hamlet"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hamlet"
license: "unknown"
legacy_id:
- details/47457
imported:
- "2019"
_4images_image_id: "47457"
_4images_cat_id: "3505"
_4images_user_id: "1327"
_4images_image_date: "2018-04-17T22:07:48"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47457 -->
Zum Nachlegen kann man den Halteschlitten entlang der Stangen hochschieben.
Trotzdem fummelig, wenn er ganz leer ist.