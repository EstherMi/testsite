---
layout: "overview"
title: "Othello Roboter"
date: 2019-12-17T19:09:06+01:00
legacy_id:
- categories/3505
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3505 --> 
* Dieser Roboter ermöglicht es Othello mit dem TXT als Gegner zu spielen.  
* Der Roboter besteht bis auf ein paar Ausnahmen ausschließlich aus ft-Teilen, beschränkt sich auf ft-Sensoren und Aktoren und ist ganz old-school in RoboPro programmiert. 
* Das Spielbrett ist ein originales Reversi-Brett vermutlich aus den 70'ern. Die originalen Spielsteine habe ich durch gereinigte und bemalte Cent-Stücke ersetzt. 
* Der Roboter wendet die Spielsteine mittels zweier Magnete, die auf einem Planetengetriebe montiert sind. Der Wendemechanismus steht fest über dem Spielbrett, das durch einen linear verschiebbaren Drehtisch unter dem Wender positioniert wird.   
* Zur Steuerung sind verbaut der TXT Controller, drei Encoder-Motoren, ein xs-Motor, zwei Magnete, sechs Taster, ein Farbsensor und die Kamera.
* Weitere Informationen finden sich hier: https://forum.ftcommunity.de/viewtopic.php?f=6&t=4777