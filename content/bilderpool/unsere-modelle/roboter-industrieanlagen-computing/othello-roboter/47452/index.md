---
layout: "image"
title: "Die linke Seite, noch etwas näher"
date: "2018-04-17T22:07:48"
picture: "othelloroboter05.jpg"
weight: "5"
konstrukteure: 
- "hamlet"
fotografen:
- "hamlet"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hamlet"
license: "unknown"
legacy_id:
- details/47452
imported:
- "2019"
_4images_image_id: "47452"
_4images_cat_id: "3505"
_4images_user_id: "1327"
_4images_image_date: "2018-04-17T22:07:48"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47452 -->
Mittig in der linken Bildhälfte ist der Farbsensor zu erkennen. Er kann schnell einen frischen Stein aus dem Spender identifizieren (rot, blau oder leer). Nicht unbedingt notwendig, aber ein Scan des Spielfeldes mit der Kamera dauert mit Positionierung sehr viel länger.