---
layout: "image"
title: "RoboPro ASCII-Art"
date: "2018-04-17T22:07:48"
picture: "othelloroboter17.jpg"
weight: "17"
konstrukteure: 
- "hamlet"
fotografen:
- "hamlet"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hamlet"
license: "unknown"
legacy_id:
- details/47464
imported:
- "2019"
_4images_image_id: "47464"
_4images_cat_id: "3505"
_4images_user_id: "1327"
_4images_image_date: "2018-04-17T22:07:48"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47464 -->
Das Display, minimal aber ausreichend.