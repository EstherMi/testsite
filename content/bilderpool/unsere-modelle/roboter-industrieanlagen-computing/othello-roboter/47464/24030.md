---
layout: "comment"
hidden: true
title: "24030"
date: "2018-04-18T17:00:52"
uploadBy:
- "hamlet"
license: "unknown"
imported:
- "2019"
---
Hallo Dirk,
Der Bildschirm zeigt das Spielfeld in der Start-Position und die Bedienelemente. Die "x" sind  die Spielsteine des TXT die "o" diejenigen des Spielers. Anbei noch ein Auszug der Anleitung aus dem Programm, das man auch solo, d.h. ohne Roboter auf dem TXT-Display spielen kann.
https://1drv.ms/u/s!AjSvbnB4EKxJjFlHCC5P9oBVjL_s 
Anbei noch ein Auszug aus der Anleitung des Programms, leider nur in meinem Schrömmel-Englisch:  
"&#8230;
&#8226; You play the "o" tokens, the TXT plays the "x". 
&#8226; You begin by default. Choose with the "<" and ">" buttons your move and confirm it with the "OK" button. Or pass the first move over to TXT by pressing the "X" button.
&#8226; With the slider on the right side you can adjust the search depth of the TXT's Othello engine. Moving it up increases the search depth, i.e. the TXT's playing strength, but it also slows down the mid game quite a bit. With the default depth of four moves you don't have to wait too long for the TXT's move and at least for my poor Othello skills this difficulty level is more than sufficient.
&#8226; During the game you can undo moves by pressing the "X" button. 
&#8226; With the "->" button you can switch to two debug screens and back to the main screen. An interesting feature can be found on the second debug screen: If the game gets too frustrating, just move the "wR" slider (random contribution to the score function) a bit to the right to force the TXT to be a bit more human. &#8230;"
Beste Grüße,
     Helmut