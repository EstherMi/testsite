---
layout: "image"
title: "Spielsteinspender, Detail 1"
date: "2018-04-17T22:07:48"
picture: "othelloroboter09.jpg"
weight: "9"
konstrukteure: 
- "hamlet"
fotografen:
- "hamlet"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hamlet"
license: "unknown"
legacy_id:
- details/47456
imported:
- "2019"
_4images_image_id: "47456"
_4images_cat_id: "3505"
_4images_user_id: "1327"
_4images_image_date: "2018-04-17T22:07:48"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47456 -->
Das Teil war 'ne schöne Herausforderung. Aber jetzt funktioniert er recht zuverlässig.
Ich hatte auch versucht einen Mangel an Cent-Spielsteinen mit einer Widerstandsmessung über die beiden Stangen zu verwirklichen. ... Das funktioniert allerdings nicht zuverlässig.