---
layout: "image"
title: "Arduino Mega"
date: "2017-04-13T17:42:39"
picture: "tvha3.jpg"
weight: "3"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/45738
imported:
- "2019"
_4images_image_id: "45738"
_4images_cat_id: "3399"
_4images_user_id: "2228"
_4images_image_date: "2017-04-13T17:42:39"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45738 -->
https://ftcommunity.de/details.php?image_id=45630
https://youtu.be/UdzuQfM4sNw