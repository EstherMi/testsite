---
layout: "image"
title: "Überblick"
date: "2013-03-22T10:51:12"
picture: "farbsortierer01.jpg"
weight: "1"
konstrukteure: 
- "Sylvia Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/36786
imported:
- "2019"
_4images_image_id: "36786"
_4images_cat_id: "2728"
_4images_user_id: "104"
_4images_image_date: "2013-03-22T10:51:12"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36786 -->
Nachtragsdokumentation zur Convention 2012:

Links legt man rote und weiße Vorstuferäder aufs Förderband, welches die Räder durch seine unterschiedlich schnell laufenden Stufen schön vereinzelt. In der Messstation wird ihre Farbe bestimmt. Der Greifer nimmt sie danach am Ende des Förderbandes auf und lässt sie in die richtige Ablage fallen. Das Robo-Interface befand sich auf der Platte mit den vielen Kabeln im Vordergrund und ist hier schon abgebaut.