---
layout: "image"
title: "Mehrstufiges Förderband"
date: "2013-03-22T10:51:12"
picture: "farbsortierer02.jpg"
weight: "2"
konstrukteure: 
- "Sylvia Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/36787
imported:
- "2019"
_4images_image_id: "36787"
_4images_cat_id: "2728"
_4images_user_id: "104"
_4images_image_date: "2013-03-22T10:51:12"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36787 -->
Den Trick mit den unterschiedlich schnell laufenden Förderbandabschnitten kennen alte fischertechnik-Hasen noch von den hobby-Kästen und Sortiermodellen aus den 1970er Jahren: Das jeweils nächste Band läuft schneller als das vorherige, sodass die einzeln evtl. dicht an dicht aufgelegten Teile auf wunderbar einfache Art und Weise vereinzelt werden.