---
layout: "image"
title: "Roboter Gesamtansicht"
date: "2013-03-22T10:51:13"
picture: "farbsortierer09.jpg"
weight: "9"
konstrukteure: 
- "Sylvia Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/36794
imported:
- "2019"
_4images_image_id: "36794"
_4images_cat_id: "2728"
_4images_user_id: "104"
_4images_image_date: "2013-03-22T10:51:13"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36794 -->
Der komplette Robbi.