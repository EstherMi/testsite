---
layout: "image"
title: "Förderband 1 mit Messeinheit"
date: "2008-12-22T19:09:56"
picture: "sortieranlagefuerftkasetten06.jpg"
weight: "10"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- details/16686
imported:
- "2019"
_4images_image_id: "16686"
_4images_cat_id: "1511"
_4images_user_id: "731"
_4images_image_date: "2008-12-22T19:09:56"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16686 -->
oben Farbsensor, unten Reedkontakt, rechts davon die beiden unterschiedlich hohen Lichtschranken