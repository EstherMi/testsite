---
layout: "image"
title: "Anlage gesamt von hinten"
date: "2008-12-22T19:09:56"
picture: "sortieranlagefuerftkasetten05.jpg"
weight: "9"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- details/16685
imported:
- "2019"
_4images_image_id: "16685"
_4images_cat_id: "1511"
_4images_user_id: "731"
_4images_image_date: "2008-12-22T19:09:56"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16685 -->
