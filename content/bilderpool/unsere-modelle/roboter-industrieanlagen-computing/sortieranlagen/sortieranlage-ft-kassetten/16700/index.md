---
layout: "image"
title: "graue Kasette in Bearbeitungsstation"
date: "2008-12-22T19:10:09"
picture: "sortieranlagefuerftkasetten20.jpg"
weight: "24"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- details/16700
imported:
- "2019"
_4images_image_id: "16700"
_4images_cat_id: "1511"
_4images_user_id: "731"
_4images_image_date: "2008-12-22T19:10:09"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16700 -->
Eine graue Kasette durchläuft die Anlage komplett, wobei ihr bei der Bearbeitungsstation ein Loch gebohrt wird.