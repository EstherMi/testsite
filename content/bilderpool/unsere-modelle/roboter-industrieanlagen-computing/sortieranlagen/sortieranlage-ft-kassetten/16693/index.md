---
layout: "image"
title: "drehbares Förderband 2 von hinten"
date: "2008-12-22T19:10:09"
picture: "sortieranlagefuerftkasetten13.jpg"
weight: "17"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- details/16693
imported:
- "2019"
_4images_image_id: "16693"
_4images_cat_id: "1511"
_4images_user_id: "731"
_4images_image_date: "2008-12-22T19:10:09"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16693 -->
