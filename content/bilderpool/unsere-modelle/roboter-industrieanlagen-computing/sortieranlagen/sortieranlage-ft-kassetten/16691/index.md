---
layout: "image"
title: "Förderband 1 mit Messeinheit von vorne"
date: "2008-12-22T19:10:09"
picture: "sortieranlagefuerftkasetten11.jpg"
weight: "15"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- details/16691
imported:
- "2019"
_4images_image_id: "16691"
_4images_cat_id: "1511"
_4images_user_id: "731"
_4images_image_date: "2008-12-22T19:10:09"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16691 -->
