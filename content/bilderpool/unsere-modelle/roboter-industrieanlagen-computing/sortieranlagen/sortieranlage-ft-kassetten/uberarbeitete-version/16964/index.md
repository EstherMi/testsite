---
layout: "image"
title: "Kompressor mit veränderter Rutsche"
date: "2009-01-09T22:16:26"
picture: "ueberarbeiteteversion03.jpg"
weight: "4"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- details/16964
imported:
- "2019"
_4images_image_id: "16964"
_4images_cat_id: "1527"
_4images_user_id: "731"
_4images_image_date: "2009-01-09T22:16:26"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16964 -->
