---
layout: "image"
title: "Schienenwagen+Stromschienen+Stromabnehmer"
date: "2009-01-09T22:17:17"
picture: "ueberarbeiteteversion09.jpg"
weight: "10"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- details/16970
imported:
- "2019"
_4images_image_id: "16970"
_4images_cat_id: "1527"
_4images_user_id: "731"
_4images_image_date: "2009-01-09T22:17:17"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16970 -->
