---
layout: "image"
title: "Schienenwagen"
date: "2009-01-09T22:17:17"
picture: "ueberarbeiteteversion06.jpg"
weight: "7"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- details/16967
imported:
- "2019"
_4images_image_id: "16967"
_4images_cat_id: "1527"
_4images_user_id: "731"
_4images_image_date: "2009-01-09T22:17:17"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16967 -->
Der Antrieb wird über die beisen Stromschienen versorgt. Die Lampe und der Fototransistor für die Lichtschranke werden (leider noch) über die grünen Kabel versorgt.