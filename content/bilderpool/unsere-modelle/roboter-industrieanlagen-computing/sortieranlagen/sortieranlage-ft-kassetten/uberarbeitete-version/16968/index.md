---
layout: "image"
title: "Schienenwagen mit grauer Kasette"
date: "2009-01-09T22:17:17"
picture: "ueberarbeiteteversion07.jpg"
weight: "8"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- details/16968
imported:
- "2019"
_4images_image_id: "16968"
_4images_cat_id: "1527"
_4images_user_id: "731"
_4images_image_date: "2009-01-09T22:17:17"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16968 -->
