---
layout: "image"
title: "Bedienfeld in RoboPro"
date: "2008-12-22T20:31:39"
picture: "sortieranlagefuerftkasetten21.jpg"
weight: "25"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- details/16701
imported:
- "2019"
_4images_image_id: "16701"
_4images_cat_id: "1511"
_4images_user_id: "731"
_4images_image_date: "2008-12-22T20:31:39"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16701 -->
Im Bedienfeld des RoboPro-Programmes kann man den Ablauf jedes Durchganges verfolgen. Die Lampen auf den Förderbändern zeigen die aktuelle Position der Kasette an, das Textfeld gibt den Status der Anlage an ("Warte auf neue Kasette", Ergebnis der Messung: "zu groß", "gelb", "magnetisch", grau", Ende eines Durchgangs: "ausgeworfen") und die kleinen Lampenspuren zeigen unmittelbar nach der Messung die Position an, wo die Kasette ausgeworfen wird.