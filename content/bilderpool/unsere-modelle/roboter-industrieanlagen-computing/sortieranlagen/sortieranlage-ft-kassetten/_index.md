---
layout: "overview"
title: "Sortieranlage für ft Kassetten"
date: 2019-12-17T19:03:23+01:00
legacy_id:
- categories/1511
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1511 --> 
Es wird nach vier Kriterien aussortiert: Ist die Kassette zu groß? Wenn nicht, ist die gelb? Wenn nicht (sie ist also grau), ist sie magnetisch? Und die resultierenden grauen Kassetten druchlaufen anschließend noch eine Bearbeitungsstation, bevor sie ebenfalls ausgeworfen werden.