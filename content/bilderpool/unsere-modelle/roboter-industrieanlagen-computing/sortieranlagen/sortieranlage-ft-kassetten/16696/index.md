---
layout: "image"
title: "pneumatische Auswurfanlage"
date: "2008-12-22T19:10:09"
picture: "sortieranlagefuerftkasetten16.jpg"
weight: "20"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- details/16696
imported:
- "2019"
_4images_image_id: "16696"
_4images_cat_id: "1511"
_4images_user_id: "731"
_4images_image_date: "2008-12-22T19:10:09"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16696 -->
