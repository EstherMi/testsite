---
layout: "image"
title: "Auswurf-Förderband für magnetische Kisten (b)"
date: "2009-04-04T17:05:34"
picture: "ueberarbeiteteversion11.jpg"
weight: "11"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- details/23588
imported:
- "2019"
_4images_image_id: "23588"
_4images_cat_id: "1611"
_4images_user_id: "731"
_4images_image_date: "2009-04-04T17:05:34"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23588 -->
