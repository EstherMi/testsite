---
layout: "image"
title: "Auwerfer"
date: "2009-04-04T17:05:30"
picture: "ueberarbeiteteversion04.jpg"
weight: "4"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- details/23581
imported:
- "2019"
_4images_image_id: "23581"
_4images_cat_id: "1611"
_4images_user_id: "731"
_4images_image_date: "2009-04-04T17:05:30"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23581 -->
Das Ausfahren wird über Zeit gesteuert, zur Ausgangslage fährt er bis der Reedkontakt auslöst.