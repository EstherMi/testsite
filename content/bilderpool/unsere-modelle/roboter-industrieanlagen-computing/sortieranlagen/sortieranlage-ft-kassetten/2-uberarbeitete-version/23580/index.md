---
layout: "image"
title: "Schienenstrecke mit Auswerfer"
date: "2009-04-04T17:05:30"
picture: "ueberarbeiteteversion03.jpg"
weight: "3"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- details/23580
imported:
- "2019"
_4images_image_id: "23580"
_4images_cat_id: "1611"
_4images_user_id: "731"
_4images_image_date: "2009-04-04T17:05:30"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23580 -->
Die Schienenstrecke habe ich nun mit Zahnräder und Zahnstangen umgebaut, da ich so das Problem mit dem durchdrehenden Antriebsrad am Wagen nicht habe.