---
layout: "image"
title: "Auswurf-Förderband für magnetische Kisten (e)"
date: "2009-04-04T17:05:34"
picture: "ueberarbeiteteversion14.jpg"
weight: "14"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- details/23591
imported:
- "2019"
_4images_image_id: "23591"
_4images_cat_id: "1611"
_4images_user_id: "731"
_4images_image_date: "2009-04-04T17:05:34"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23591 -->
Hier sieht man gut den Antrieb und den Endtaster, der über die Platten ausgelöst wird.