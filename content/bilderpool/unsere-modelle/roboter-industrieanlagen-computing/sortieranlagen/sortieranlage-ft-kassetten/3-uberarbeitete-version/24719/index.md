---
layout: "image"
title: "Bearbeitungsstation von der Seite"
date: "2009-08-09T23:39:12"
picture: "ueberarbeiteteversion07.jpg"
weight: "7"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- details/24719
imported:
- "2019"
_4images_image_id: "24719"
_4images_cat_id: "1698"
_4images_user_id: "731"
_4images_image_date: "2009-08-09T23:39:12"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24719 -->
