---
layout: "image"
title: "NOT-Aus-, Reset- und Lichtschalter"
date: "2009-08-09T23:39:12"
picture: "ueberarbeiteteversion09.jpg"
weight: "9"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- details/24721
imported:
- "2019"
_4images_image_id: "24721"
_4images_cat_id: "1698"
_4images_user_id: "731"
_4images_image_date: "2009-08-09T23:39:12"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24721 -->
Den NOT-Aus- und Reset-Schalter musste ich aus Platzgründen auf die IR-Fernbedienung ausquartieren. Die beiden Knöpfe werden nun mit den Platten gedrückt.