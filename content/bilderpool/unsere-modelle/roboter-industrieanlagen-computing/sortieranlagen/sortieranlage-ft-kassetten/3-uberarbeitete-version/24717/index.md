---
layout: "image"
title: "Wechselmechanismus Bearbeitungsstation"
date: "2009-08-09T23:39:12"
picture: "ueberarbeiteteversion05.jpg"
weight: "5"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- details/24717
imported:
- "2019"
_4images_image_id: "24717"
_4images_cat_id: "1698"
_4images_user_id: "731"
_4images_image_date: "2009-08-09T23:39:12"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24717 -->
