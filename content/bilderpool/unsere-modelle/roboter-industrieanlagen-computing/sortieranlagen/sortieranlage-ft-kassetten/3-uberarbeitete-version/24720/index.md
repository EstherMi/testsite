---
layout: "image"
title: "Bearbeitungsstation von hinten"
date: "2009-08-09T23:39:12"
picture: "ueberarbeiteteversion08.jpg"
weight: "8"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- details/24720
imported:
- "2019"
_4images_image_id: "24720"
_4images_cat_id: "1698"
_4images_user_id: "731"
_4images_image_date: "2009-08-09T23:39:12"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24720 -->
