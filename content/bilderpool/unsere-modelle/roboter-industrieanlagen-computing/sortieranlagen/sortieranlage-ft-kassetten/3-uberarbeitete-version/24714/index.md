---
layout: "image"
title: "Auswurfanlage + Bearbeitungsstation"
date: "2009-08-09T23:39:11"
picture: "ueberarbeiteteversion02.jpg"
weight: "2"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- details/24714
imported:
- "2019"
_4images_image_id: "24714"
_4images_cat_id: "1698"
_4images_user_id: "731"
_4images_image_date: "2009-08-09T23:39:11"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24714 -->
links die Auswurfanlage für die weißen "Klötzchen" und rechts die Bearbeitungsstation. Die verschiedenen Werkzeuge können durch Drehen eingerastet werden.