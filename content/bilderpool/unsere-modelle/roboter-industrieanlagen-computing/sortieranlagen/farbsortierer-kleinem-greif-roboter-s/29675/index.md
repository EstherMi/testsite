---
layout: "image"
title: "Interface"
date: "2011-01-11T19:13:20"
picture: "farbsortierermitkleinemachsengreifroboter10.jpg"
weight: "10"
konstrukteure: 
- "Martin S"
fotografen:
- "Martin S"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin S"
license: "unknown"
legacy_id:
- details/29675
imported:
- "2019"
_4images_image_id: "29675"
_4images_cat_id: "2173"
_4images_user_id: "1264"
_4images_image_date: "2011-01-11T19:13:20"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29675 -->
