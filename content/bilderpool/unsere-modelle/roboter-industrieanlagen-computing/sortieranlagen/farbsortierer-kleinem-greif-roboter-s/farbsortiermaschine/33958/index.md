---
layout: "image"
title: "Farbsortiermaschine"
date: "2012-01-16T19:37:32"
picture: "farbsortiermaschemitpalettenlader06.jpg"
weight: "6"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- details/33958
imported:
- "2019"
_4images_image_id: "33958"
_4images_cat_id: "2513"
_4images_user_id: "1361"
_4images_image_date: "2012-01-16T19:37:32"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33958 -->
Farbsensoren mit Justierschnecken. Es werden nur zwei Farbsensoren benötigt. Alle schwarzen Steine werden von den Farbsensoren nicht erfasst und werden beim Unterbrechen einer gewöhnlichen Lichtschranke auf die letzte Palette geschoben.