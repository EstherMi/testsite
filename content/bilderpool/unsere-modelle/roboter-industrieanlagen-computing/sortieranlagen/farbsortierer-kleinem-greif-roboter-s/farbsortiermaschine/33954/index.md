---
layout: "image"
title: "Farbsortiermaschine"
date: "2012-01-16T19:37:32"
picture: "farbsortiermaschemitpalettenlader02.jpg"
weight: "2"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- details/33954
imported:
- "2019"
_4images_image_id: "33954"
_4images_cat_id: "2513"
_4images_user_id: "1361"
_4images_image_date: "2012-01-16T19:37:32"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33954 -->
Steile Draufsicht mit Blick auf die Paletten im Vordergrund.