---
layout: "image"
title: "Farbsortiermaschine"
date: "2012-01-16T19:38:03"
picture: "farbsortiermaschemitpalettenlader16.jpg"
weight: "16"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- details/33968
imported:
- "2019"
_4images_image_id: "33968"
_4images_cat_id: "2513"
_4images_user_id: "1361"
_4images_image_date: "2012-01-16T19:38:03"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33968 -->
Ansicht von hinten. Der Kabelstrang verbindet die Sensoren und Aktoren der Maschine mit Interface und Erweiterung, die auf der Bauplatte der Maschine keinen Platz mehr gefunden haben.