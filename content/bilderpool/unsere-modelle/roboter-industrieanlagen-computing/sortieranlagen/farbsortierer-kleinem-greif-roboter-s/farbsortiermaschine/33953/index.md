---
layout: "image"
title: "Farbsortiermaschine mit angeschlossener Palettenladeeinrichtung"
date: "2012-01-16T19:37:32"
picture: "farbsortiermaschemitpalettenlader01.jpg"
weight: "1"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- details/33953
imported:
- "2019"
_4images_image_id: "33953"
_4images_cat_id: "2513"
_4images_user_id: "1361"
_4images_image_date: "2012-01-16T19:37:32"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33953 -->
Links hinten sieht man das Dreischachtmagazin, völlig ungeordnet gefüllt mit jeweils 18 roten, 18 gelben und 18 schwarzen Grundbausteinen. Vor dem Magazin befindet sich der Auslösemechanismus. Dieser bewirkt, dass bei einem Umlauf jeweils ein Baustein zunächst aus dem rechten, dann aus dem mittleren und zum Schluss aus dem linken Schacht auf ein Förderband fällt, das die Steine nach rechts transportiert. Unter der großen roten flächigen Abdeckung gegen Streulicht befinden sich die Farbsensoren, die über Schnecken sehr exakt justiert werden können. Sobald ein Farbsensor einen Baustein mit der Farbe auf die der Farbsensor eingestellt ist, erfasst, schiebt ein Auswerfer (hinter dem Förderband) den entsprechenden Stein nach vorn auf eine Palette. Diese kann 6 Steine nebeneinander aufnehmen. Danach senkt sich die Palette, lichtschrankengesteuert, um eine Bausteinhöhe ab, so dass nun eine weitere Lage Bausteine aufgeschoben werden kann. Ein abermaliges Absenken bereitet die Palette für die Aufnahme einer dritten Reihe Steine vor. Wenn alle 3 Paletten  mit jeweils 18 Steinen einer Farbe gefüllt sind werden die Paletten zur besseren Entnahme wieder nach oben gefahren.
Mit dem weißen Nothaltknopf (vorne links) lassen sich alle Motoren bei  eventuellen Störungen, die leider auch immer wieder vorkommen, stoppen. Nach Beseitigung der Störung läuft die Maschinensteuerung durch das Computerprogramm an genau der Stelle weiter, an der es unterbrochen wurde.
Ein Video, das die Maschine in Betrieb zeigt kann auf Youtube unter "fischertechnik Farbsortiermaschine mit Palettenladeeinrichtung.avi" angesehen werden.