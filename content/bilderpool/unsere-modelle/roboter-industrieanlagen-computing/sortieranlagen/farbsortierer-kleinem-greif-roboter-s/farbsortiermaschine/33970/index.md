---
layout: "image"
title: "Farbsortiermaschine"
date: "2012-01-16T19:38:03"
picture: "farbsortiermaschemitpalettenlader18.jpg"
weight: "18"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- details/33970
imported:
- "2019"
_4images_image_id: "33970"
_4images_cat_id: "2513"
_4images_user_id: "1361"
_4images_image_date: "2012-01-16T19:38:03"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33970 -->
Taster zum Heben der Paletten. Der weiße Knopf löst den Nothalt aus und setzt sowohl Blinkwarnlicht und Warnton in Gang.