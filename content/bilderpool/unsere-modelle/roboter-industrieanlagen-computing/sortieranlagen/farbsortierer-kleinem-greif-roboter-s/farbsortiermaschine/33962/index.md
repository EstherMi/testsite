---
layout: "image"
title: "Farbsortiermaschine"
date: "2012-01-16T19:37:32"
picture: "farbsortiermaschemitpalettenlader10.jpg"
weight: "10"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- details/33962
imported:
- "2019"
_4images_image_id: "33962"
_4images_cat_id: "2513"
_4images_user_id: "1361"
_4images_image_date: "2012-01-16T19:37:32"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33962 -->
Die rotierenden Hebel, die die Schächte des Magazins öffnen und schließen.