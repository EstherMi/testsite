---
layout: "image"
title: "Greif Roboter"
date: "2011-01-11T19:13:19"
picture: "farbsortierermitkleinemachsengreifroboter04.jpg"
weight: "4"
konstrukteure: 
- "Martin S"
fotografen:
- "Martin S"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin S"
license: "unknown"
legacy_id:
- details/29669
imported:
- "2019"
_4images_image_id: "29669"
_4images_cat_id: "2173"
_4images_user_id: "1264"
_4images_image_date: "2011-01-11T19:13:19"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29669 -->
