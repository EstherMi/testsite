---
layout: "image"
title: "4"
date: "2010-08-23T23:25:26"
picture: "strebensortierer04.jpg"
weight: "4"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27901
imported:
- "2019"
_4images_image_id: "27901"
_4images_cat_id: "2018"
_4images_user_id: "1082"
_4images_image_date: "2010-08-23T23:25:26"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27901 -->
Bei dem TX sind alle Anschlusse bis auf C2 belegt.