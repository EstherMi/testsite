---
layout: "image"
title: "8"
date: "2010-08-23T23:25:26"
picture: "strebensortierer08.jpg"
weight: "8"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27905
imported:
- "2019"
_4images_image_id: "27905"
_4images_cat_id: "2018"
_4images_user_id: "1082"
_4images_image_date: "2010-08-23T23:25:26"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27905 -->
Dann fährt hier eine Lichtschranke an der Strebe entlang, und misst die Länge.