---
layout: "image"
title: "6"
date: "2010-08-23T23:25:26"
picture: "strebensortierer06.jpg"
weight: "6"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27903
imported:
- "2019"
_4images_image_id: "27903"
_4images_cat_id: "2018"
_4images_user_id: "1082"
_4images_image_date: "2010-08-23T23:25:26"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27903 -->
Zwischen den Platten sieht man die Kette, die die Streben in die Maschiene befördert.