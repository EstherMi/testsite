---
layout: "image"
title: "10"
date: "2010-08-23T23:25:26"
picture: "strebensortierer10.jpg"
weight: "10"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27907
imported:
- "2019"
_4images_image_id: "27907"
_4images_cat_id: "2018"
_4images_user_id: "1082"
_4images_image_date: "2010-08-23T23:25:26"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27907 -->
So, dass die Streben hier auf das andere Laufband geschoben werden kann.