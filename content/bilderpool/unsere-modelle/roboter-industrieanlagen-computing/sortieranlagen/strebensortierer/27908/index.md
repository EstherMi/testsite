---
layout: "image"
title: "11"
date: "2010-08-23T23:25:34"
picture: "strebensortierer11.jpg"
weight: "11"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27908
imported:
- "2019"
_4images_image_id: "27908"
_4images_cat_id: "2018"
_4images_user_id: "1082"
_4images_image_date: "2010-08-23T23:25:34"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27908 -->
Dieser Schieber fährt dann an das jeweilige Fach, und die Strebe fällt dort vom Laufband.