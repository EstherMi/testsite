---
layout: "overview"
title: "Strebensortierer"
date: 2019-12-17T19:03:37+01:00
legacy_id:
- categories/2018
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2018 --> 
Diese Maschine sortiert FT-Streben nach ihrer Länge. Leider funktioniert es nicht so gut, die Maschine kann nur die vier längsten Streben sortieren, obwohl es eigentlich 8 sein sollten. Außerdem verhakt sich alles oft ein bisschen.