---
layout: "overview"
title: "Ur-Sortieranlage mit l-e1-Stäben"
date: 2019-12-17T19:04:05+01:00
legacy_id:
- categories/3182
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3182 --> 
Ein originalgetreuer Nachbau der wohl ersten dokumentierten Sortiermaschine für Bausteine - aus 1969!