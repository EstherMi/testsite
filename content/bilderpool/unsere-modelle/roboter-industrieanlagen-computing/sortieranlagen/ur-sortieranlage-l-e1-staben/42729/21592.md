---
layout: "comment"
hidden: true
title: "21592"
date: "2016-01-24T22:33:49"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Video: https://www.youtube.com/watch?v=pG1rvI733HY

@H.A.R.R.Y.: Die Elektronik war damals wohl das teuerste, was man von fischertechnik kaufen konnte. Der l-e1 kostete irgendwas jenseits der 100 DM und evtl. nur knapp unter 200 DM.

@Kalti: Stimmt, die Kabel sind von teilweise heute. Das "originalbetreu" bezieht sich auf die Bauweise überhaupt: Die gleichen Teile, grau, die Ur-Flachplatten 30, der Ur-mini-mot und natürlich die Ur-Elektronik ;-)