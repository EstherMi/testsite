---
layout: "image"
title: "Trainingsroboter 2016"
date: "2016-06-10T13:57:10"
picture: "IMG_2989.jpg"
weight: "2"
konstrukteure: 
- "Derk"
fotografen:
- "Derk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- details/43738
imported:
- "2019"
_4images_image_id: "43738"
_4images_cat_id: "2739"
_4images_user_id: "1289"
_4images_image_date: "2016-06-10T13:57:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43738 -->
