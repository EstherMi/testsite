---
layout: "image"
title: "Neue Gripper 2"
date: "2014-12-06T12:36:44"
picture: "DSC_6054.jpg"
weight: "33"
konstrukteure: 
- "Derk"
fotografen:
- "Derk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- details/39890
imported:
- "2019"
_4images_image_id: "39890"
_4images_cat_id: "2739"
_4images_user_id: "1289"
_4images_image_date: "2014-12-06T12:36:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39890 -->
