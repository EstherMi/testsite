---
layout: "image"
title: "Neue Robotarm 5"
date: "2015-02-06T21:35:55"
picture: "DSC_6112.jpg"
weight: "30"
konstrukteure: 
- "Derk"
fotografen:
- "Derk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- details/40452
imported:
- "2019"
_4images_image_id: "40452"
_4images_cat_id: "2739"
_4images_user_id: "1289"
_4images_image_date: "2015-02-06T21:35:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40452 -->
