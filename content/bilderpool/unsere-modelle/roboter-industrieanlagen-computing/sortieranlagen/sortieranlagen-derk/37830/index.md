---
layout: "image"
title: "Portaalrobot3"
date: "2013-11-11T09:44:44"
picture: "IMG_0913.jpg"
weight: "51"
konstrukteure: 
- "Derk"
fotografen:
- "Derk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- details/37830
imported:
- "2019"
_4images_image_id: "37830"
_4images_cat_id: "2739"
_4images_user_id: "1289"
_4images_image_date: "2013-11-11T09:44:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37830 -->
