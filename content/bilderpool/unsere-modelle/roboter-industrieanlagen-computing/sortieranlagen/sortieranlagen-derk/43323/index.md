---
layout: "image"
title: "Trainingsroboter 2016"
date: "2016-04-30T21:27:21"
picture: "IMG_2963.jpg"
weight: "9"
konstrukteure: 
- "Derk"
fotografen:
- "Derk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- details/43323
imported:
- "2019"
_4images_image_id: "43323"
_4images_cat_id: "2739"
_4images_user_id: "1289"
_4images_image_date: "2016-04-30T21:27:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43323 -->
