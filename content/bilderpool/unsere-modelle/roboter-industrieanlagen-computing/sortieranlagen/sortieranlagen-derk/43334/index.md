---
layout: "image"
title: "Trainingsroboter 2016"
date: "2016-05-05T22:51:55"
picture: "IMG_2983.jpg"
weight: "4"
konstrukteure: 
- "Derk"
fotografen:
- "Derk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- details/43334
imported:
- "2019"
_4images_image_id: "43334"
_4images_cat_id: "2739"
_4images_user_id: "1289"
_4images_image_date: "2016-05-05T22:51:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43334 -->
Die Dreiecksplatte 33,6 Computing Trainings-Roboter
in detail