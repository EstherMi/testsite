---
layout: "image"
title: "Details 2"
date: "2015-02-08T12:54:27"
picture: "DSC_6133.jpg"
weight: "17"
konstrukteure: 
- "Derk"
fotografen:
- "Derk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- details/40475
imported:
- "2019"
_4images_image_id: "40475"
_4images_cat_id: "2739"
_4images_user_id: "1289"
_4images_image_date: "2015-02-08T12:54:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40475 -->
