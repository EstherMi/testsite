---
layout: "image"
title: "Sotieranlage (4)"
date: "2008-09-27T21:15:12"
picture: "SNV80008_2.jpg"
weight: "4"
konstrukteure: 
- "-Michael-"
fotografen:
- "-Michael-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Michael-"
license: "unknown"
legacy_id:
- details/15649
imported:
- "2019"
_4images_image_id: "15649"
_4images_cat_id: "1438"
_4images_user_id: "820"
_4images_image_date: "2008-09-27T21:15:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15649 -->
