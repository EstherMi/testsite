---
layout: "image"
title: "Sotieranlage (6)"
date: "2008-09-27T21:15:12"
picture: "SNV80015.jpg"
weight: "6"
konstrukteure: 
- "-Michael-"
fotografen:
- "-Michael-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Michael-"
license: "unknown"
legacy_id:
- details/15651
imported:
- "2019"
_4images_image_id: "15651"
_4images_cat_id: "1438"
_4images_user_id: "820"
_4images_image_date: "2008-09-27T21:15:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15651 -->
