---
layout: "image"
title: "Sortierer"
date: "2006-11-28T21:19:00"
picture: "Sortiermaschine22.jpg"
weight: "10"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/7643
imported:
- "2019"
_4images_image_id: "7643"
_4images_cat_id: "685"
_4images_user_id: "456"
_4images_image_date: "2006-11-28T21:19:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7643 -->
