---
layout: "image"
title: "Sortieranlage Detail"
date: "2006-11-03T18:11:48"
picture: "PICT1001.jpg"
weight: "19"
konstrukteure: 
- "Uwe Timm"
fotografen:
- "Uwe Timm"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Chemikus"
license: "unknown"
legacy_id:
- details/7309
imported:
- "2019"
_4images_image_id: "7309"
_4images_cat_id: "685"
_4images_user_id: "156"
_4images_image_date: "2006-11-03T18:11:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7309 -->
