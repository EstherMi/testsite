---
layout: "comment"
hidden: true
title: "1460"
date: "2006-10-19T19:41:26"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
Sieht schon viel übersichtlicher aus :)

Kleiner Tip: Die Schläuche immer so kurz wie möglich halten, dann bewegst du nicht so viel Luft umsonst hin und her...

Weiter so,
Thomas