---
layout: "image"
title: "Sortieranlage"
date: "2006-10-02T16:28:11"
picture: "Sortiermaschine2.jpg"
weight: "28"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/7097
imported:
- "2019"
_4images_image_id: "7097"
_4images_cat_id: "685"
_4images_user_id: "456"
_4images_image_date: "2006-10-02T16:28:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7097 -->
Hier sieht man wie das Förederband angetrieben wird.