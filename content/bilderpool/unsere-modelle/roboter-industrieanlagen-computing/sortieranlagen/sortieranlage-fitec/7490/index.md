---
layout: "image"
title: "Sortierer"
date: "2006-11-19T12:45:02"
picture: "Sortiermaschine19.jpg"
weight: "14"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/7490
imported:
- "2019"
_4images_image_id: "7490"
_4images_cat_id: "685"
_4images_user_id: "456"
_4images_image_date: "2006-11-19T12:45:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7490 -->
Das ist der neue "Sortierer". Da der alte mit dem Drehkranz und dem Zylinder nicht wirklich gut funktioniert hat hab ich etwas einfacheres gebraucht.