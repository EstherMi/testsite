---
layout: "image"
title: "Farberkennung"
date: "2006-11-19T12:59:04"
picture: "Sortiermaschine20.jpg"
weight: "15"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/7491
imported:
- "2019"
_4images_image_id: "7491"
_4images_cat_id: "685"
_4images_user_id: "456"
_4images_image_date: "2006-11-19T12:59:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7491 -->
Das ist die neue Farberkennung. Die funktioniert so: Die Lampe strahlt auf den unteren Fototransistor. Fährt ein Baustein dort entlang, wird der Fototransistor unterbrochen( wie bei einer Lichtschranke), dann stoppt das Förderband. Und weil jetzt der Baustein im Weg ist  strahlt das Licht an den Baustein. Bei weiß strahlt das Licht auf den oberen Fototransistor zurück, bei schwarz nicht.