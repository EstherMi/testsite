---
layout: "image"
title: "Antrieb"
date: "2006-11-19T12:42:13"
picture: "Sortiermaschine17.jpg"
weight: "12"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/7488
imported:
- "2019"
_4images_image_id: "7488"
_4images_cat_id: "685"
_4images_user_id: "456"
_4images_image_date: "2006-11-19T12:42:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7488 -->
Hier sieht man den Antrieb vom neuen Förderband.
Diesmal mit einem Mini-Mot und einer 2:1 Untersetzung.