---
layout: "image"
title: "...von links..."
date: "2006-10-16T19:01:02"
picture: "Sortiermaschine12.jpg"
weight: "24"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/7189
imported:
- "2019"
_4images_image_id: "7189"
_4images_cat_id: "685"
_4images_user_id: "456"
_4images_image_date: "2006-10-16T19:01:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7189 -->
Hier sieht man gut wie der erste Zylinder gesteuert wird.