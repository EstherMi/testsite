---
layout: "image"
title: "Neuer Luftspeicher"
date: "2006-10-16T19:01:02"
picture: "Sortiermaschine10.jpg"
weight: "22"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/7187
imported:
- "2019"
_4images_image_id: "7187"
_4images_cat_id: "685"
_4images_user_id: "456"
_4images_image_date: "2006-10-16T19:01:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7187 -->
Ich habe mir jetzt einen neuen Drucktank aus einer Aldi-Flasche(0,5L) gebaut. Der fasst viel mehr Luft als der von fischertechnik und der Druck geht nicht so schnell aus.