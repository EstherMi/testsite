---
layout: "image"
title: "Sortieranlage"
date: "2006-10-02T16:28:11"
picture: "Sortiermaschine1.jpg"
weight: "27"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/7096
imported:
- "2019"
_4images_image_id: "7096"
_4images_cat_id: "685"
_4images_user_id: "456"
_4images_image_date: "2006-10-02T16:28:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7096 -->
Hier werden die schwarzen und weißen Bausteine per Zylinder auf das Förderband geschoben.