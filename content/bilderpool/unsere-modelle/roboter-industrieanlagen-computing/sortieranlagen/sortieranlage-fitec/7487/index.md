---
layout: "image"
title: "Erneuerung"
date: "2006-11-19T12:42:12"
picture: "Sortiermaschine16.jpg"
weight: "11"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/7487
imported:
- "2019"
_4images_image_id: "7487"
_4images_cat_id: "685"
_4images_user_id: "456"
_4images_image_date: "2006-11-19T12:42:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7487 -->
Hier habe ich meine Sortiermaschine fast komplett erneuert. Da das alte Förderband oft geklemmt hat hab ich es ohne Raupenbeläge gebaut. Dafür sind es 2 nebeneinander.