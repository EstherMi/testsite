---
layout: "image"
title: "robot arm"
date: "2012-04-30T20:02:36"
picture: "Robot_03.jpg"
weight: "6"
konstrukteure: 
- "Derk"
fotografen:
- "Derk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- details/34846
imported:
- "2019"
_4images_image_id: "34846"
_4images_cat_id: "738"
_4images_user_id: "1289"
_4images_image_date: "2012-04-30T20:02:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34846 -->
