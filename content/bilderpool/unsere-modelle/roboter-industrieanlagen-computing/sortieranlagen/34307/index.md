---
layout: "image"
title: "Sortieranlage 5"
date: "2012-02-19T20:36:27"
picture: "FT_Derk_05-2012.jpg"
weight: "15"
konstrukteure: 
- "Derk"
fotografen:
- "Derk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- details/34307
imported:
- "2019"
_4images_image_id: "34307"
_4images_cat_id: "738"
_4images_user_id: "1289"
_4images_image_date: "2012-02-19T20:36:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34307 -->
