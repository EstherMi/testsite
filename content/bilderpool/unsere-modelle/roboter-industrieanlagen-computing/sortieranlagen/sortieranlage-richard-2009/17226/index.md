---
layout: "image"
title: "detaillierte Bilder der Tabelle Kontrolle"
date: "2009-01-31T12:59:11"
picture: "DSC_2074.jpg"
weight: "8"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/17226
imported:
- "2019"
_4images_image_id: "17226"
_4images_cat_id: "1544"
_4images_user_id: "371"
_4images_image_date: "2009-01-31T12:59:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17226 -->
