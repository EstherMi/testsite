---
layout: "image"
title: "Top ansiegt Tabelle Roboter"
date: "2009-01-31T12:59:10"
picture: "DSC_2071_-_Version_2.jpg"
weight: "2"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/17220
imported:
- "2019"
_4images_image_id: "17220"
_4images_cat_id: "1544"
_4images_user_id: "371"
_4images_image_date: "2009-01-31T12:59:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17220 -->
