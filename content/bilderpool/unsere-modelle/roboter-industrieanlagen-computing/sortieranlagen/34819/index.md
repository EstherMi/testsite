---
layout: "image"
title: "Robotarm nieuwe versie"
date: "2012-04-22T21:30:28"
picture: "FT_Derk_03.jpg"
weight: "10"
konstrukteure: 
- "Derk"
fotografen:
- "Derk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- details/34819
imported:
- "2019"
_4images_image_id: "34819"
_4images_cat_id: "738"
_4images_user_id: "1289"
_4images_image_date: "2012-04-22T21:30:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34819 -->
