---
layout: "image"
title: "Sorteercentrum met portaalrobot 1.2"
date: "2011-11-13T18:14:36"
picture: "FT_Derk_0027.jpg"
weight: "27"
konstrukteure: 
- "Derk"
fotografen:
- "Derk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- details/33475
imported:
- "2019"
_4images_image_id: "33475"
_4images_cat_id: "738"
_4images_user_id: "1289"
_4images_image_date: "2011-11-13T18:14:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33475 -->
