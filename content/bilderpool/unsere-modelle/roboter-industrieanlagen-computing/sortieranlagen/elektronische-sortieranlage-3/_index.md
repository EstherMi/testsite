---
layout: "overview"
title: "Elektronische Sortieranlage für 3 Bausteinlängen"
date: 2019-12-17T19:04:07+01:00
legacy_id:
- categories/3475
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3475 --> 
Diese elektronisch gesteuerte Sortieranlage trennt über zwei Lichtschranken Bausteine von drei verschiedenen Längen.