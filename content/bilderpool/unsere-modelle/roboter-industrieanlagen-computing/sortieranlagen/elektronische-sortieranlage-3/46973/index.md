---
layout: "image"
title: "Verteiler-Antrieb"
date: "2017-12-10T22:08:34"
picture: "elektronischesortieranlagefuerbausteinlaengen7.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46973
imported:
- "2019"
_4images_image_id: "46973"
_4images_cat_id: "3475"
_4images_user_id: "104"
_4images_image_date: "2017-12-10T22:08:34"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46973 -->
Hier der zweite XS-Motor, der den Verteiler dreht.