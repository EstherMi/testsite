---
layout: "image"
title: "Gesamtansicht"
date: "2017-12-10T22:08:34"
picture: "elektronischesortieranlagefuerbausteinlaengen1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46967
imported:
- "2019"
_4images_image_id: "46967"
_4images_cat_id: "3475"
_4images_user_id: "104"
_4images_image_date: "2017-12-10T22:08:34"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46967 -->
Diese Maschine trennt die unten gezeigten Kombinationen aus je zwei BS15 mit keinem, einem oder zwei Verbindern 15 eingesteckt. Die Steine werden links oben eingelegt und gelangen über ein zweistufiges Förderband durch zwei Lichtschranken. Ein drehbarer Verteiler lässt die mittlere Größe gerade durchfallen, dreht sich aber bei zu kurzen bzw. zu langen so, dass die kurzen Bausteine im (hier halbverdeckten) vorderen und die langen im hinteren Fach landen.

Eine solche Anlage hatte ich in den 1990ern schon mal mit Ur-ft gebaut, und hatte einfach mal wieder Lust auf ein paar Silberlinge. Eine Anlage mit ähnlicher Verteiltechnik findet sich auf dem Packungsaufdruck des hobby-4-Kastens und im IC-Digital-Praktikum-Handbuch. Im hobby-4-Begleitband 5 ist ab Seite 66 die grundlegende Idee beschrieben, wie man mit zwei Lichtschranken und Timing-Überwachung drei Längen unterscheiden kann. Anders als dort werden hier aber zu lange und zu kurze Bausteine nicht einfach aussortiert, sondern getrennt abgelegt.