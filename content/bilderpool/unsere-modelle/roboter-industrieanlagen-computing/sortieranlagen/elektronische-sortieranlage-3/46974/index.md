---
layout: "image"
title: "Elektronik"
date: "2017-12-10T22:08:34"
picture: "elektronischesortieranlagefuerbausteinlaengen8.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46974
imported:
- "2019"
_4images_image_id: "46974"
_4images_cat_id: "3475"
_4images_user_id: "104"
_4images_image_date: "2017-12-10T22:08:34"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46974 -->
Links unten der Gleichrichterbaustein für die Stromversorgung (der macht glatte 9V), rechts daneben zwei h4G Grundbausteine als Verstärker für die Lichtschranken. Daneben rechts die beiden angesprochenen Monoflops für die beiden Zeiten.

Links oben drei Flipflops: Eines für "muss der Verteiler überhaupt drehen, wenn das Bauteil durch ist", eines für "die Drehung wird gestartet" (wird durch den Reedkontakt nach einer Umdrehung zurückgesetzt) und eines für die Drehrichtung des Verteilers. Letzteres geht auf das linke der beiden Relais, das als Polwender verschaltet ist.

Der Reedkontakt legt "-" (die logische "1" der Silberlinge) ans Flipflop. Da ein Reedkontakt aber einfach nur ein Schließer ist, wird das per 22-kOhm-Widerstand (unten unterhalb der h4-Grundbausteine zu sehen) nach "+" gezogen, damit das Flipflop auch eine echte Flanke sieht.