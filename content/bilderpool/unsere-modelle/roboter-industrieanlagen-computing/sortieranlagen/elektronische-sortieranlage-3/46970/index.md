---
layout: "image"
title: "Zweistufiges Förderband"
date: "2017-12-10T22:08:34"
picture: "elektronischesortieranlagefuerbausteinlaengen4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46970
imported:
- "2019"
_4images_image_id: "46970"
_4images_cat_id: "3475"
_4images_user_id: "104"
_4images_image_date: "2017-12-10T22:08:34"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46970 -->
Das in diesem Bild untere (erste) Förderband wird direkt von der Metallachse angetrieben. Das zweite Förderband (zu den Lichtschranken) sitzt aber auf Klemmringen. Da sich die Achsen gleich schnell drehen, ergibt das über den größeren Radius den Effekt, dass das zweite Förderband schneller läuft als das erste. Dadurch werden auch direkt eng aneinander liegende Bausteine rechtzeitig vor den Lichtschranken hinreichend getrennt und vereinzelt.