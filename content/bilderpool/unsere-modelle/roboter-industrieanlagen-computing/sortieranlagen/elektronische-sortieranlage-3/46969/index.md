---
layout: "image"
title: "Antriebsdetail"
date: "2017-12-10T22:08:34"
picture: "elektronischesortieranlagefuerbausteinlaengen3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46969
imported:
- "2019"
_4images_image_id: "46969"
_4images_cat_id: "3475"
_4images_user_id: "104"
_4images_image_date: "2017-12-10T22:08:34"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46969 -->
Die rechte untere Achse ist die vom Motor angetriebene. In den BS7,5 oben steckt eine 31064 "U-Achse 40 + Zahnrad Z28 m0,5 mini". In dieser Anordnung passt das genau, um das Ritzel des Z10 links anzutreiben. Somit laufen beide unteren Achsen gleichsinnig und gleich schnell.