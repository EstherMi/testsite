---
layout: "image"
title: "Auswurf"
date: "2013-09-17T19:09:15"
picture: "sortieranlage06.jpg"
weight: "6"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- details/37405
imported:
- "2019"
_4images_image_id: "37405"
_4images_cat_id: "2782"
_4images_user_id: "1635"
_4images_image_date: "2013-09-17T19:09:15"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37405 -->
Der Enc.-Motor macht genau eine Umdrehung.