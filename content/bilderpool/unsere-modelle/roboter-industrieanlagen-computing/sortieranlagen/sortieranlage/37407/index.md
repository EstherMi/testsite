---
layout: "image"
title: "Farbsortierband"
date: "2013-09-17T19:09:15"
picture: "sortieranlage08.jpg"
weight: "8"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- details/37407
imported:
- "2019"
_4images_image_id: "37407"
_4images_cat_id: "2782"
_4images_user_id: "1635"
_4images_image_date: "2013-09-17T19:09:15"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37407 -->
Hier werden die übrigen Bausteine nach Farbe sortiert.