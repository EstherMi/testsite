---
layout: "image"
title: "Farbsensor"
date: "2013-09-17T19:09:15"
picture: "sortieranlage10.jpg"
weight: "10"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- details/37409
imported:
- "2019"
_4images_image_id: "37409"
_4images_cat_id: "2782"
_4images_user_id: "1635"
_4images_image_date: "2013-09-17T19:09:15"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37409 -->
Ich habe einen Sporsensor verwendet, da ich keinen Farbsensor habe.