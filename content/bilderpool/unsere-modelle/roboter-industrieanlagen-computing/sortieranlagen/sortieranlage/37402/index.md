---
layout: "image"
title: "Von hinten"
date: "2013-09-17T19:09:15"
picture: "sortieranlage03.jpg"
weight: "3"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- details/37402
imported:
- "2019"
_4images_image_id: "37402"
_4images_cat_id: "2782"
_4images_user_id: "1635"
_4images_image_date: "2013-09-17T19:09:15"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37402 -->
Man sieht die Ablage für gelbe Teile.