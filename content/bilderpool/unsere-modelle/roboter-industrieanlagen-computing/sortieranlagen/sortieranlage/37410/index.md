---
layout: "image"
title: "Das Bedienfeld"
date: "2013-09-17T19:09:15"
picture: "sortieranlage11.jpg"
weight: "11"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- details/37410
imported:
- "2019"
_4images_image_id: "37410"
_4images_cat_id: "2782"
_4images_user_id: "1635"
_4images_image_date: "2013-09-17T19:09:15"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37410 -->
Mein Robo-Pro Bedienfeld zur Steuerung.