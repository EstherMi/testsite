---
layout: "image"
title: "Drehkranz mit Führung"
date: "2007-01-02T14:58:38"
picture: "Neuer_Ordner_2_001_3.jpg"
weight: "25"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/8260
imported:
- "2019"
_4images_image_id: "8260"
_4images_cat_id: "690"
_4images_user_id: "453"
_4images_image_date: "2007-01-02T14:58:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8260 -->
