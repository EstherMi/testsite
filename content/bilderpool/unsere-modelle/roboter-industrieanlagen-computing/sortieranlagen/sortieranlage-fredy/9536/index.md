---
layout: "image"
title: "Halterung"
date: "2007-03-17T12:00:07"
picture: "sortiermaschine1.jpg"
weight: "9"
konstrukteure: 
- "Lcd: thkias, Rest: Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/9536
imported:
- "2019"
_4images_image_id: "9536"
_4images_cat_id: "690"
_4images_user_id: "453"
_4images_image_date: "2007-03-17T12:00:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9536 -->
Hier sieht man die Halterung von dem Lcd.