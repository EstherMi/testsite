---
layout: "image"
title: "Ketten oben"
date: "2007-08-11T17:21:31"
picture: "industriemodell3.jpg"
weight: "3"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/11355
imported:
- "2019"
_4images_image_id: "11355"
_4images_cat_id: "690"
_4images_user_id: "453"
_4images_image_date: "2007-08-11T17:21:31"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11355 -->
Die ketten hängen dort in der Mitte runter wenn sie hoch gezogen werden.