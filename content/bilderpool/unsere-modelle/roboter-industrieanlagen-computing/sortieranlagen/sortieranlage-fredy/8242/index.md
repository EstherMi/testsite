---
layout: "image"
title: "Drehkranz ohne Führung"
date: "2007-01-01T19:50:00"
picture: "Neuer_Ordner_2_004.jpg"
weight: "26"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/8242
imported:
- "2019"
_4images_image_id: "8242"
_4images_cat_id: "690"
_4images_user_id: "453"
_4images_image_date: "2007-01-01T19:50:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8242 -->
