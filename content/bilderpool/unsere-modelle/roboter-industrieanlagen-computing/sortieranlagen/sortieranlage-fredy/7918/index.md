---
layout: "image"
title: "Steuerpult von oben"
date: "2006-12-17T17:47:45"
picture: "Neuer_Ordner_2_002.jpg"
weight: "31"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/7918
imported:
- "2019"
_4images_image_id: "7918"
_4images_cat_id: "690"
_4images_user_id: "453"
_4images_image_date: "2006-12-17T17:47:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7918 -->
