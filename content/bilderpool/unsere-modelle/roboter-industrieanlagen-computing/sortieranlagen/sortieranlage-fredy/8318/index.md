---
layout: "image"
title: "Die Steuerung"
date: "2007-01-07T13:19:20"
picture: "Neuer_Ordner_2_009.jpg"
weight: "22"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/8318
imported:
- "2019"
_4images_image_id: "8318"
_4images_cat_id: "690"
_4images_user_id: "453"
_4images_image_date: "2007-01-07T13:19:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8318 -->
Links im Bild sieht man den Motor, welcher das Laufband antreibt.