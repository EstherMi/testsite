---
layout: "image"
title: "Halterung für das Kabel"
date: "2006-12-21T15:29:24"
picture: "Neuer_Ordner_2_002_2.jpg"
weight: "29"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/8094
imported:
- "2019"
_4images_image_id: "8094"
_4images_cat_id: "690"
_4images_user_id: "453"
_4images_image_date: "2006-12-21T15:29:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8094 -->
