---
layout: "image"
title: "Antrieb"
date: "2007-08-11T17:21:31"
picture: "industriemodell4.jpg"
weight: "4"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/11356
imported:
- "2019"
_4images_image_id: "11356"
_4images_cat_id: "690"
_4images_user_id: "453"
_4images_image_date: "2007-08-11T17:21:31"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11356 -->
Antrieb mit einem Mini Motor.....