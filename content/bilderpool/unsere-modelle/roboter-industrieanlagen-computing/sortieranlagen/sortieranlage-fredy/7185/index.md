---
layout: "image"
title: "Gesamtansicht"
date: "2006-10-12T11:45:33"
picture: "Fr_ftcommunity_009.jpg"
weight: "58"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/7185
imported:
- "2019"
_4images_image_id: "7185"
_4images_cat_id: "690"
_4images_user_id: "453"
_4images_image_date: "2006-10-12T11:45:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7185 -->
Hier ist noch eine Gesamtansicht von der Sortiermaschine