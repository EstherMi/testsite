---
layout: "image"
title: "Teil zwei"
date: "2006-12-15T15:28:56"
picture: "Neuer_Ordner_3_005.jpg"
weight: "36"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/7907
imported:
- "2019"
_4images_image_id: "7907"
_4images_cat_id: "690"
_4images_user_id: "453"
_4images_image_date: "2006-12-15T15:28:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7907 -->
