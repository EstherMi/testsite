---
layout: "image"
title: "Anschluss"
date: "2007-03-17T12:00:08"
picture: "sortiermaschine3.jpg"
weight: "11"
konstrukteure: 
- "Lcd: thkias, Rest: Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/9538
imported:
- "2019"
_4images_image_id: "9538"
_4images_cat_id: "690"
_4images_user_id: "453"
_4images_image_date: "2007-03-17T12:00:08"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9538 -->
Hier sieht man den Anschluss zum Interface.