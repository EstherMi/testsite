---
layout: "image"
title: "dietuermevonhanoi06.jpg"
date: "2009-11-29T22:08:58"
picture: "dietuermevonhanoi06.jpg"
weight: "6"
konstrukteure: 
- "Ironsmurf"
fotografen:
- "Ironsmurf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ironsmurf"
license: "unknown"
legacy_id:
- details/25871
imported:
- "2019"
_4images_image_id: "25871"
_4images_cat_id: "1818"
_4images_user_id: "461"
_4images_image_date: "2009-11-29T22:08:58"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25871 -->
