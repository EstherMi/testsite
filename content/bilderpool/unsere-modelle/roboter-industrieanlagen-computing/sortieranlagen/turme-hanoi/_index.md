---
layout: "overview"
title: "Türme von Hanoi"
date: 2019-12-17T19:03:35+01:00
legacy_id:
- categories/1818
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1818 --> 
Die Aufgabe besteht darin, die 4 Becher von links nach rechts zu versetzen. Es gibt 3 Regeln:[br][br]1. Es gibt nur 3 Ablagemöglichkeiten[br]2. Es darf immer nur ein Becher bewgt werden.[br]3. Es darf NIE ein größerer Becher auf einem kleineren abgestellt werden.[br][br]Die Bilder zeigen die Lösung mit 4 Bechern.[br][br]Mein Roboter erkennt selbstständig um wieviele Becher es sich handelt, mögliche Kombinationen sind zwischen 2 und 5 Bechern. Sollten weniger wie 2 unter dem Ausleger stehen, wird er das Spiel nicht beginnen.[br][br]PS: Da ich nach vielen Jahren wieder mit ft begonnen habe, suchte ich ein leichtes Objekt, um mich mit der Materie wieder vertraut zu machen und auch den Umgang mit RoboPro zu lernen.[br]       Inspiriert wurde ich von den ersten Computerbaukasten von ft für den legendären C64. Dort war das Beispiel für die Türme, dargestellt wurden sie damals mit Metallscheiben.[br]