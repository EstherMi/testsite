---
layout: "image"
title: "Der erste Vereinzeler"
date: "2011-02-28T17:31:46"
picture: "industrieanlage06.jpg"
weight: "6"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- details/30160
imported:
- "2019"
_4images_image_id: "30160"
_4images_cat_id: "2231"
_4images_user_id: "1275"
_4images_image_date: "2011-02-28T17:31:46"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30160 -->
Hier fährt Blau geradeaus und Weiß und Rot werden mit den Greifer auf das 2. Fließband gebracht.