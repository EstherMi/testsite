---
layout: "image"
title: "Greifer"
date: "2011-02-28T17:31:46"
picture: "industrieanlage07.jpg"
weight: "7"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- details/30161
imported:
- "2019"
_4images_image_id: "30161"
_4images_cat_id: "2231"
_4images_user_id: "1275"
_4images_image_date: "2011-02-28T17:31:46"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30161 -->
Der Greifer nimmt den weißen oder roten Stein.