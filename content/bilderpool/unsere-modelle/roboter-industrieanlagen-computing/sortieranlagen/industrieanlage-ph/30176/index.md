---
layout: "image"
title: "Der einzige Arbeiter"
date: "2011-02-28T17:32:13"
picture: "industrieanlage22.jpg"
weight: "22"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- details/30176
imported:
- "2019"
_4images_image_id: "30176"
_4images_cat_id: "2231"
_4images_user_id: "1275"
_4images_image_date: "2011-02-28T17:32:13"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30176 -->
Er muss gucken ob alles ok ist.