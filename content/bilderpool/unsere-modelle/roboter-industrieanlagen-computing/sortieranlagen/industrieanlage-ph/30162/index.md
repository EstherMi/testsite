---
layout: "image"
title: "Stein auf dem 2. Fließband"
date: "2011-02-28T17:31:46"
picture: "industrieanlage08.jpg"
weight: "8"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- details/30162
imported:
- "2019"
_4images_image_id: "30162"
_4images_cat_id: "2231"
_4images_user_id: "1275"
_4images_image_date: "2011-02-28T17:31:46"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30162 -->
Der Greifer hat den Stein auf der 2. Fließband gelegt und fährt in die Ausgangs Position.