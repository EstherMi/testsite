---
layout: "image"
title: "Trio-Kompressor"
date: "2011-02-28T17:32:01"
picture: "industrieanlage13.jpg"
weight: "13"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- details/30167
imported:
- "2019"
_4images_image_id: "30167"
_4images_cat_id: "2231"
_4images_user_id: "1275"
_4images_image_date: "2011-02-28T17:32:01"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30167 -->
Von mir entwickelt:
Dreimal so viel Luft wie bei einen normalen Kompressor !!!