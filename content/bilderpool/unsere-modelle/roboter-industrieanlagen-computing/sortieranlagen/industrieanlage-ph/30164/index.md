---
layout: "image"
title: "Greifer 2"
date: "2011-02-28T17:32:01"
picture: "industrieanlage10.jpg"
weight: "10"
konstrukteure: 
- "PH"
fotografen:
- "PH"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PH"
license: "unknown"
legacy_id:
- details/30164
imported:
- "2019"
_4images_image_id: "30164"
_4images_cat_id: "2231"
_4images_user_id: "1275"
_4images_image_date: "2011-02-28T17:32:01"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30164 -->
Das ist ein voll automatischer Pneumartischergreifer.

Achsen:
Vor, Auf und Zu, drehen