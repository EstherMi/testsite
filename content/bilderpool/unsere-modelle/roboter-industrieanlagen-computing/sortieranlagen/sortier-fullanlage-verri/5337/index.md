---
layout: "image"
title: "Prozess 1 und 2 von oben"
date: "2005-11-13T21:17:25"
picture: "FT0003.jpg"
weight: "7"
konstrukteure: 
- "verri"
fotografen:
- "verri"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Verri"
license: "unknown"
legacy_id:
- details/5337
imported:
- "2019"
_4images_image_id: "5337"
_4images_cat_id: "458"
_4images_user_id: "384"
_4images_image_date: "2005-11-13T21:17:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5337 -->
Die beiden Prozesse von oben. Links die Lageprüfung und Wendung, rechts die Farberkennung.