---
layout: "image"
title: "Aufzug und Rütteltisch"
date: "2007-04-11T09:59:11"
picture: "sortieranlage1.jpg"
weight: "1"
konstrukteure: 
- "Bumpf"
fotografen:
- "Bumpf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/10043
imported:
- "2019"
_4images_image_id: "10043"
_4images_cat_id: "907"
_4images_user_id: "424"
_4images_image_date: "2007-04-11T09:59:11"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10043 -->
