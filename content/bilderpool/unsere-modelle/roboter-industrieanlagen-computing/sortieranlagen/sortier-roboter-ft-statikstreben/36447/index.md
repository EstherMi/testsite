---
layout: "image"
title: "Gesammt Ansicht von vorne"
date: "2013-01-12T20:19:35"
picture: "sortierroboterfuerftstatikstreben01.jpg"
weight: "1"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/36447
imported:
- "2019"
_4images_image_id: "36447"
_4images_cat_id: "2707"
_4images_user_id: "833"
_4images_image_date: "2013-01-12T20:19:35"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36447 -->
Der Roboter ist auf zwei ft-Bauplatten aufgebaut. Ganz rechts ist der Streben-Abnehmer, der die Streben eine nach der anderen auf den Messtisch legt. In der Mitte ist der Messtisch, auf dem die Streben gemessen werden.Llinks ist das Regal mit den Kästen zu sehen in die die Streben einsortiert werden.