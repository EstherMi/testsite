---
layout: "image"
title: "zweites Förderband"
date: "2013-01-12T20:19:35"
picture: "sortierroboterfuerftstatikstreben11.jpg"
weight: "11"
konstrukteure: 
- "Thilo Bleumer"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Thilo"
license: "unknown"
legacy_id:
- details/36457
imported:
- "2019"
_4images_image_id: "36457"
_4images_cat_id: "2707"
_4images_user_id: "833"
_4images_image_date: "2013-01-12T20:19:35"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36457 -->
vom Mini-Mot M3 wird das Förderband an einer Zahnstannge nach oben verfahren. Der Taster I7 gibt das Signal wenn er auf der Höhe der richtigen Kästchens angekommen ist. Dann dreht sich das Förderband noch einmal bis die Strebe im Kästchen landet. Zum Schluss fährt das Förderband wieder nach unten.