---
layout: "image"
title: "Sortieranlage 2"
date: "2012-02-19T20:33:56"
picture: "FT_Derk_02-2012.jpg"
weight: "12"
konstrukteure: 
- "Derk"
fotografen:
- "Derk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- details/34304
imported:
- "2019"
_4images_image_id: "34304"
_4images_cat_id: "738"
_4images_user_id: "1289"
_4images_image_date: "2012-02-19T20:33:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34304 -->
