---
layout: "image"
title: "Schaltschrank"
date: "2010-08-28T14:01:10"
picture: "Schaltschrank.jpg"
weight: "11"
konstrukteure: 
- "Jan"
fotografen:
- "Jan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- details/27993
imported:
- "2019"
_4images_image_id: "27993"
_4images_cat_id: "2027"
_4images_user_id: "1164"
_4images_image_date: "2010-08-28T14:01:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27993 -->
Von dem Schaltschrank wird alles gesteuert