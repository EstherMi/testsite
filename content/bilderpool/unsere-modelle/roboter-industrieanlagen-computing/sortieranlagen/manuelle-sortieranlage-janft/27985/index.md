---
layout: "image"
title: "Antrieb Förderband 1"
date: "2010-08-28T09:49:15"
picture: "Antrieb_Forderband_I.jpg"
weight: "3"
konstrukteure: 
- "Jan"
fotografen:
- "Jan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- details/27985
imported:
- "2019"
_4images_image_id: "27985"
_4images_cat_id: "2027"
_4images_user_id: "1164"
_4images_image_date: "2010-08-28T09:49:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27985 -->
Per Minimotor wird das Förderband angetrieben