---
layout: "image"
title: "Der mobile Roboter holt die nächste Tonne"
date: "2018-01-30T16:23:43"
picture: "T4.jpg"
weight: "4"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/47227
imported:
- "2019"
_4images_image_id: "47227"
_4images_cat_id: "3495"
_4images_user_id: "579"
_4images_image_date: "2018-01-30T16:23:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47227 -->
