---
layout: "overview"
title: "Tonnen-Sortierer mit einem mobilen und einem stationären Roboter"
date: 2019-12-17T19:04:09+01:00
legacy_id:
- categories/3495
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3495 --> 
Ein mobiler Roboter mit Greifarm sammelt markierte Tonnen ein und übergibt sie einem stationären Knickarm-Roboter zur Einlagerung in ein Hochregallager. Die Tonnen und ihre Entfernung vom Greifarm werden anhand ihrer roten Farbmarkierung vom mobilen Roboter erkannt, der über eine CMOS-Kamera OV7670 mit Bildverarbeitung verfügt. Dann fährt der mobile Roboter die Tonnen zum stationären Roboter, den er an dessen Positionslichtern erkennt und dessen Entfernung er aus der y-Position der Positionslichter abschätzen kann. Der stationäre Roboter lädt die Tonnen in eine Station mit einer weiteren CMOS Kamera OV7670 mit Bildverarbeitung, die die Ziffern auf den Tonnen erkennt. Dann werden die Tonnen entsprechend ihrer Ziffern in ein Hochregallager einsortiert.