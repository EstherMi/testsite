---
layout: "image"
title: "Anlieferung der Tonne durch den mobilen Roboter"
date: "2018-01-30T16:23:43"
picture: "T1.jpg"
weight: "1"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/47224
imported:
- "2019"
_4images_image_id: "47224"
_4images_cat_id: "3495"
_4images_user_id: "579"
_4images_image_date: "2018-01-30T16:23:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47224 -->
Ein mobiler Roboter mit Greifarm sammelt markierte Tonnen ein und übergibt sie einem stationären Knickarm-Roboter zur Einlagerung in ein Hochregallager. Die Tonnen und ihre Entfernung vom Greifarm werden anhand ihrer roten Farbmarkierung vom mobilen Roboter erkannt, der über eine CMOS-Kamera OV7670 mit Bildverarbeitung verfügt. Dann fährt der mobile Roboter die Tonnen zum stationären Roboter, den er an dessen Positionslichtern erkennt und dessen Entfernung er aus der y-Position der Positionslichter abschätzen kann. Der stationäre Roboter lädt die Tonnen in eine Station mit einer weiteren CMOS Kamera OV7670 mit Bildverarbeitung, die die Ziffern auf den Tonnen erkennt. Dann werden die Tonnen entsprechend ihrer Ziffern in ein Hochregallager einsortiert.

Video gibt es hier: 

https://www.youtube.com/watch?v=h7dR_Q8h76U&t=184s