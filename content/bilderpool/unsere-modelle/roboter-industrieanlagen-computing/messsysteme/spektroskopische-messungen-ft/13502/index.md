---
layout: "image"
title: "Messanordnung von hinten mit Bedienfeldanzeige"
date: "2008-02-01T17:44:25"
picture: "Messanordnung_hinten_mit_Laptop_u_Anschlsse.jpg"
weight: "4"
konstrukteure: 
- "equester"
fotografen:
- "equester"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "equester"
license: "unknown"
legacy_id:
- details/13502
imported:
- "2019"
_4images_image_id: "13502"
_4images_cat_id: "1233"
_4images_user_id: "731"
_4images_image_date: "2008-02-01T17:44:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13502 -->
Die Werte der Fotodiode und deren Ort in cm wird während der Messung an den Anzeigen angezeigt.