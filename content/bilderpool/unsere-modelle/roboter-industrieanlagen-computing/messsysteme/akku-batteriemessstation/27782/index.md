---
layout: "image"
title: "Von Oben"
date: "2010-07-29T09:24:39"
picture: "akkuundbatteriemessstation2.jpg"
weight: "2"
konstrukteure: 
- "Fabian M."
fotografen:
- "Fabian M."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fabian M."
license: "unknown"
legacy_id:
- details/27782
imported:
- "2019"
_4images_image_id: "27782"
_4images_cat_id: "2001"
_4images_user_id: "639"
_4images_image_date: "2010-07-29T09:24:39"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27782 -->
Die Halbe Abdeckung ist abgenommen.