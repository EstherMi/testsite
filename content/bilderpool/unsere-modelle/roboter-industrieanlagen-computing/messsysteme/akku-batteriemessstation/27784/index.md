---
layout: "image"
title: "Von Oben III"
date: "2010-07-29T09:24:39"
picture: "akkuundbatteriemessstation4.jpg"
weight: "4"
konstrukteure: 
- "Fabian M."
fotografen:
- "Fabian M."
keywords: ["Batterie", "Akku", "Messen", "Voll", "Leer", "Spannung"]
uploadBy: "Fabian M."
license: "unknown"
legacy_id:
- details/27784
imported:
- "2019"
_4images_image_id: "27784"
_4images_cat_id: "2001"
_4images_user_id: "639"
_4images_image_date: "2010-07-29T09:24:39"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27784 -->
Ansicht von Vorne.Die beiden Wiederstände sind Vorwiederstände der LED's. Zum auseinanderhalten der LED-Beinchen nutze ich die zwei Achsen.