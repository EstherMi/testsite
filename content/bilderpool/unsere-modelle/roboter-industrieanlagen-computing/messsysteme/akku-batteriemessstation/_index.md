---
layout: "overview"
title: "Akku - und Batteriemessstation"
date: 2019-12-17T19:07:24+01:00
legacy_id:
- categories/2001
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2001 --> 
Mit diesem Gerät kann man ganz einfach 9V und 1,2(1,5) Batterien und Akkus
nachmessen.