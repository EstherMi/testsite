---
layout: "image"
title: "Target too close"
date: "2007-01-02T14:58:38"
picture: "fischertechnik_004.jpg"
weight: "3"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/8248
imported:
- "2019"
_4images_image_id: "8248"
_4images_cat_id: "760"
_4images_user_id: "371"
_4images_image_date: "2007-01-02T14:58:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8248 -->
