---
layout: "overview"
title: "Radar 360 (Abstandsensoren)"
date: 2019-12-17T19:07:22+01:00
legacy_id:
- categories/760
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=760 --> 
Bewegungen im Wohnzimmer mit 4 Robo Schnittstellen und acht Abstandsensoren feststellen.