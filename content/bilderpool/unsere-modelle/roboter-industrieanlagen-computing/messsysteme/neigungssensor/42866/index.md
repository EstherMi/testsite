---
layout: "image"
title: "BNO055 Gyroscope with Velocity control"
date: "2016-02-13T21:30:12"
picture: "BNO055_Gyroscope_with_velocity_control.jpg"
weight: "2"
konstrukteure: 
- "chehr"
fotografen:
- "chehr"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chehr"
license: "unknown"
legacy_id:
- details/42866
imported:
- "2019"
_4images_image_id: "42866"
_4images_cat_id: "2887"
_4images_user_id: "2374"
_4images_image_date: "2016-02-13T21:30:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42866 -->
Objective: I2C MPU driver optimization wrt user needs.
Setup: Encodermotor with Turning table and lever arm. The Sensor BNO055 is located at the outer position with a lever arm of approx. 17cm.
Table is rotated firstly by +360° afterwards by -360° with two different speeds. Closed loop control used.
BNO055 Gyroscope Sensor data has been recorded.
Issues:
Workaraund &#8220;Closed loop Speed control &#8220; used in RoboPro -> Syncron distance 
Accurate sampling rate