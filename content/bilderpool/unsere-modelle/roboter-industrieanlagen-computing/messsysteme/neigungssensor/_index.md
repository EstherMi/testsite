---
layout: "overview"
title: "Neigungssensor"
date: 2019-12-17T19:07:27+01:00
legacy_id:
- categories/2887
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2887 --> 
Ein kompakter Sensor zum Messen von Neigungen