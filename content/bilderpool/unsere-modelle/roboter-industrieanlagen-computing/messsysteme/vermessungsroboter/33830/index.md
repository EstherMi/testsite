---
layout: "image"
title: "Messarm während der Kalibrierfahrt"
date: "2012-01-02T19:55:04"
picture: "Bildschirmfoto_2012-01-02_um_17.16.13.jpg"
weight: "2"
konstrukteure: 
- "Gaffalover"
fotografen:
- "Gaffalover"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Gaffalover"
license: "unknown"
legacy_id:
- details/33830
imported:
- "2019"
_4images_image_id: "33830"
_4images_cat_id: "2501"
_4images_user_id: "1427"
_4images_image_date: "2012-01-02T19:55:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33830 -->
Um Spannungsschwankungen des Akkus auszugleichen führt der Roboter nach dem Programmstart eine Kalibrierungsfahrt durch, bei der die benötigte Zeit zum abfahren des Weges in einer Variable gespeichert wird.