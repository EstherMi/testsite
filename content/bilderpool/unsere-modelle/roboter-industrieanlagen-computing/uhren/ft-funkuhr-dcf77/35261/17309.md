---
layout: "comment"
hidden: true
title: "17309"
date: "2012-09-30T18:49:11"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Hallo Dirk,

Es war ein interessantes und erfolgreiches FT-Treffen-2012  in Erbes-Budesheim !
Viele neue Ideeën + schöne Modellen.
 
Deine FT-Funkuhr, die auf der Convention 2012 zu sehen war, hat 3st  I²C-LED-Displays (Conrad, Best.-Nr. 198344)  für die Zeit (Stunde/Minute), Datum (Tag/Monat) und Jahr.

Wäre es möglich auch das RoboPro-Programm mit anwendung dieser 3st  I²C-LED-Displays 
zu uploaden ?

Grüss,
 
Peter
Poederoyen NL