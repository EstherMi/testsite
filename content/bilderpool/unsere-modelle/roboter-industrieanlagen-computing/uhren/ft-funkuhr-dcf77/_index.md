---
layout: "overview"
title: "ft-Funkuhr (DCF77)"
date: 2019-12-17T19:08:02+01:00
legacy_id:
- categories/2613
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2613 --> 
Mit einer DCF-Empfängerplatine von Conrad und einem RoboPro-Programm wird der TX zur vollwertigen Funkuhr.