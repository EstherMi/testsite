---
layout: "image"
title: "DCF-Empfänger"
date: "2012-08-04T23:33:33"
picture: "ftfunkuhrdcf2.jpg"
weight: "2"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/35260
imported:
- "2019"
_4images_image_id: "35260"
_4images_cat_id: "2613"
_4images_user_id: "1126"
_4images_image_date: "2012-08-04T23:33:33"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35260 -->
Die DCF-Empfängerplatine von Conrad Electronic (erhältlich für ca. 10 Euro, Best.-Nr. 641138-62) verträgt eine Betriebsspannung zwischen 2,5 und 15 V - passt also perfekt an den 9V-Ausgang des TX.
Zum Anschluss an den TX benötigt man noch einen 4,7 kOhm-Widerstand (Conrad-Best.-Nr. 403334) und einen 100 nF-Kondensator (Conrad-Best.-Nr. 500812). Mit dem Widerstand werden die Anschlussklemmen 2 und 4 verbunden, der Kondensator wird zwischen die Klemmen 1 und 2 "montiert". Die Klemme 4 liefert das invertierte DCF77-Signal, das an einem TX-Eingang (5 kOhm digital) abgegriffen werden kann.
Die DCF-Empfängerplatine passt inklusive Ferritantenne, Widerstand und Kondensator in ein ft-Batteriekästchen.