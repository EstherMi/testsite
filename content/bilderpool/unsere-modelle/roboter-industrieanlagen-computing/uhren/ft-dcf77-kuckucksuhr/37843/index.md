---
layout: "image"
title: "RTC mit dem DS1307 in Kombination mit dem Kuckuck-Antrieb funktioniert nicht"
date: "2013-11-25T19:57:44"
picture: "RTC-Treiber_-DS1307Kuckuck.jpg"
weight: "1"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/37843
imported:
- "2019"
_4images_image_id: "37843"
_4images_cat_id: "2767"
_4images_user_id: "22"
_4images_image_date: "2013-11-25T19:57:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37843 -->
Stimmt es das es im neuen RTC_Treiber DS1307 v1.0.rpp die variabele TX-Minuten gibt statt CLK-Minuten im (alte) DCF77-Decoder v2.1 
und TX-Stunden statt CLK-Stunden  ?

Die Real Time Clock - RTC mit dem DS1307 funktioniert bei mir prima ! ......

In Kombination mit dem Kuckuck-antrieb gibt es aber Problemen die zusammen hangen mit die variabele TX-Stunden.
Es gleicht als ob die variabele TX-Stunden  nicht stabil ist...als ob es nur ein kleines Moment 0 ist und dann wieder die richtige Stunde....???????......und deswegen hort der Kuckuck nicht auf.
Was habe ich falsch gemacht ?

Beim alten Programm DCF77-Decoder v2.1 funktioniert die Kuckuck Problemlos........(es gab nur problemen beim niedrige Empfangt).