---
layout: "image"
title: "FT-DCF77-Kuckucksuhr"
date: "2013-07-28T19:13:47"
picture: "kuckucksuhr4.jpg"
weight: "15"
konstrukteure: 
- "Peterholland"
fotografen:
- "Peterholland"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/37204
imported:
- "2019"
_4images_image_id: "37204"
_4images_cat_id: "2767"
_4images_user_id: "22"
_4images_image_date: "2013-07-28T19:13:47"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37204 -->
Antrieb dieser Kuckucks-Blasebälge mit eine kleine Motorantrieb.
Für 1 mal "Kuckuck" reicht eine Achse-Umdrehung mit Nocke + Schalter.