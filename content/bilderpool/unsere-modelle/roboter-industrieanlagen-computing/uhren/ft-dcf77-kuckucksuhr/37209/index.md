---
layout: "image"
title: "Kuckucksuhr - Details"
date: "2013-07-30T23:44:19"
picture: "kuckuksuhrdetails02.jpg"
weight: "3"
konstrukteure: 
- "Peterholland"
fotografen:
- "Peterholland"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/37209
imported:
- "2019"
_4images_image_id: "37209"
_4images_cat_id: "2767"
_4images_user_id: "22"
_4images_image_date: "2013-07-30T23:44:19"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37209 -->
