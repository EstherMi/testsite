---
layout: "image"
title: "Kuckucksuhr - Details"
date: "2013-07-30T23:44:19"
picture: "kuckuksuhrdetails05.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/37212
imported:
- "2019"
_4images_image_id: "37212"
_4images_cat_id: "2767"
_4images_user_id: "22"
_4images_image_date: "2013-07-30T23:44:19"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37212 -->
