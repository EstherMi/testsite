---
layout: "image"
title: "FT-DCF77-Kuckucksuhr"
date: "2013-07-28T19:13:46"
picture: "kuckucksuhr1.jpg"
weight: "12"
konstrukteure: 
- "Peterholland"
fotografen:
- "Peterholland"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/37201
imported:
- "2019"
_4images_image_id: "37201"
_4images_cat_id: "2767"
_4images_user_id: "22"
_4images_image_date: "2013-07-28T19:13:46"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37201 -->
Kuckuck mit Goldplatten nach hinten.