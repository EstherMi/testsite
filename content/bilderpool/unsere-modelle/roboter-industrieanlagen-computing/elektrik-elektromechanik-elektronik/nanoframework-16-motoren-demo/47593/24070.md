---
layout: "comment"
hidden: true
title: "24070"
date: "2018-05-14T09:56:19"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ja, Ziel ist, superelegant einfach fischertechnik autonom mit .net steuern zu können. Ich will zunächst mal alles, was ich habe, in AbstractIO gießen: Motoren, Schrittmotoren, Servos, und verschiedene Sensoren. Da es aber eben abstrakt ist, kann man Modellcode schreiben, und es ist egal, ob der Temperatursensor ein NTC am Analogeingang oder ein per I²C oder so angesteuerter Sensor ist - was immer da läuft, liefert halt einfach °C als Zahl in einem IDoubleInput oder sowas.

Außerdem ist das Ausreizen hier ein wunderbarer Test für das nanoFramework, und auf dem Weg bis dahin bin ich schon über eine Reihe von Bugs gestolpert, die dann behoben werden konnten. Aktuell sitze ich an Schrittmotoren, und zwar auch abstrakt: Es gibt ein Interface IIntegerPositioningUnit, und es ist dem Modell-Code egal, ob da ein Schrittmotor oder z.B. ein fischertechnik-Motor mit Impulstaster dran hängt.


Dann noch die Servos, und das Adafruit Motor Shield ist vollständig abgedeckt.


AbstractIO soll letztlich ein simples NuGet-Package werden, was jedermann einfach in ein Visual-Studio-Projekt rein nehmen und benutzen kann. Und ein konkretes Ziel ist die mechanische Simulation eines Computerspiels, bei dem nämlich mein RoboInt schlicht überfordert ist, wie ich leidvoll feststellen musste.

Gruß,
Stefan