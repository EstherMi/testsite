---
layout: "image"
title: "Relais"
date: "2012-10-07T17:05:47"
picture: "internet-02.jpg"
weight: "2"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: ["Internet", "Elektromechanik"]
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- details/35814
imported:
- "2019"
_4images_image_id: "35814"
_4images_cat_id: "2675"
_4images_user_id: "1322"
_4images_image_date: "2012-10-07T17:05:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35814 -->
Die besagten Relais...
Das Linke sortiert zwei Server aus, das Rechte noch eins und dann bleibt der gewünschte Server übrig.