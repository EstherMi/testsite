---
layout: "image"
title: "Nahaufnahme Server"
date: "2012-10-07T17:05:47"
picture: "internet-19.jpg"
weight: "6"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: ["Elektromechanik", "Internet"]
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- details/35818
imported:
- "2019"
_4images_image_id: "35818"
_4images_cat_id: "2675"
_4images_user_id: "1322"
_4images_image_date: "2012-10-07T17:05:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35818 -->
Man kann über 1 und 0 alles übertragen (auh "Hallo": 10010001100001110110011011001101111); bloß ist hier wenig Speicherplatz da :(