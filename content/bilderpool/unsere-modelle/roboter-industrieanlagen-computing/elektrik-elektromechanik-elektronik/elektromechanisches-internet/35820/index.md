---
layout: "image"
title: "Das Terminal"
date: "2012-10-07T17:05:47"
picture: "internet-18.jpg"
weight: "8"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: ["Elektromechanik", "Internet"]
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- details/35820
imported:
- "2019"
_4images_image_id: "35820"
_4images_cat_id: "2675"
_4images_user_id: "1322"
_4images_image_date: "2012-10-07T17:05:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35820 -->
Über die beiden Schalter wird die Adresse eingestellt:
11
00
10
01
Die Leuchte zeigt das was der Server schickt (Leuchten: 1, Nicht Leuchten: 0)