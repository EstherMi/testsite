---
layout: "comment"
hidden: true
title: "22494"
date: "2016-09-08T18:12:53"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Oh ja, da ist noch Raum für gaaanz viele Experimente. Dies hier ist nur ein erstes Antesten, um auszuloten, was man dem Board zumuten kann und wo die Grenze ist.

Der Poti läuft hier als Spannungsteiler (zum Analogeingang geht der Schieber-Anschluss). Da der Gesamtwiderstand nur 20 (oder 40? weiß grad nicht mehr) Ohm waren, hängt der Poti aber in Serie mit dem ft-Lämpchen rechts oben, um die Stromquelle nicht unnötig zu belasten.

Per schöner Input-Konverter-Klasse wird das Signal vom Poti selbstlernend auf einen Wertebereich zwischen -1.0 und +1.0 skaliert, und das geht zum Motorausgang für den Ventilator.

Gruß,
Stefan