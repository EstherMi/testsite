---
layout: "comment"
hidden: true
title: "24015"
date: "2018-04-03T19:55:35"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
The interesting point in all this is the great comfort you have developing the software: Nearly full .net, including object orientation, multithreading, garbage collection, networking, exception handling, Visual Studio with the full live debugging experience, etc. etc.

By using all this, the software can be given a nice architecture, and you can use everthing modern software development has to offer. You buy this at the cost of some more CPU cycles as compared to native C programs in Arduinos or PIs, because .net Micro Framework interprets the compled IL byte code at run time. However, things are quite fast enough for I/O with some kHz frequency, and for me, this gained comfort is well worth it.

Additionally, I use the driver boards of the 1980ies fischertechnik electronics as additional drivers (besides that on the Adafruit board). They are attached simply to the digital I/O pins of the Netduino board.