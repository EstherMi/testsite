---
layout: "overview"
title: "Netduino-Demo"
date: 2019-12-17T19:07:54+01:00
legacy_id:
- categories/3273
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3273 --> 
Ein Demonstrationsmodell mit einem Netduino-Board und einem Adafruit-Motorshield. Demonstriert wird Multithreading mit dem Microsoft .NET Micro Framework, um 4 Modelle mitsamt gemeinsamem Not-Aus unabhängig voneinander parallel laufen zu lassen.