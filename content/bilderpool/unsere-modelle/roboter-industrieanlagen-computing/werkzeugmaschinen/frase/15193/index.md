---
layout: "image"
title: "CNC-Fräser1"
date: "2008-09-07T10:44:39"
picture: "cncfraeser1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "wudo94"
license: "unknown"
legacy_id:
- details/15193
imported:
- "2019"
_4images_image_id: "15193"
_4images_cat_id: "2240"
_4images_user_id: "824"
_4images_image_date: "2008-09-07T10:44:39"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15193 -->
