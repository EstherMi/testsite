---
layout: "image"
title: "Schweißer"
date: "2006-04-17T20:29:35"
picture: "Fischertechnik-Bilder_023.jpg"
weight: "4"
konstrukteure: 
- "Jakob Wolf"
fotografen:
- "Jakob Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FFBBII"
license: "unknown"
legacy_id:
- details/6130
imported:
- "2019"
_4images_image_id: "6130"
_4images_cat_id: "634"
_4images_user_id: "420"
_4images_image_date: "2006-04-17T20:29:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6130 -->
