---
layout: "image"
title: "untere Drehfunktion"
date: "2006-04-17T20:29:35"
picture: "Fischertechnik-Bilder_021.jpg"
weight: "2"
konstrukteure: 
- "Jakob Wolf"
fotografen:
- "Jakob Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FFBBII"
license: "unknown"
legacy_id:
- details/6128
imported:
- "2019"
_4images_image_id: "6128"
_4images_cat_id: "634"
_4images_user_id: "420"
_4images_image_date: "2006-04-17T20:29:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6128 -->
