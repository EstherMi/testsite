---
layout: "image"
title: "Austoßverfahren"
date: "2006-12-08T22:51:26"
picture: "DSCI0010.jpg"
weight: "15"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: ["Bearbeitungszentrum", "Austoßverfahren"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/7743
imported:
- "2019"
_4images_image_id: "7743"
_4images_cat_id: "734"
_4images_user_id: "504"
_4images_image_date: "2006-12-08T22:51:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7743 -->
Das ist der Austoßer der die Werkstücke aus dem Magazin in die Fächer des Drehtisches schiebt.