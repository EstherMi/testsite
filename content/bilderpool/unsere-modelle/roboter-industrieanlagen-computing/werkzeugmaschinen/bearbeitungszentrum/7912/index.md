---
layout: "image"
title: "2. Bohrmaschine"
date: "2006-12-17T11:38:54"
picture: "Bearbeitungszentrum_057.jpg"
weight: "2"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: ["Bearbeitungszentrum", "Kegelzahnräder"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/7912
imported:
- "2019"
_4images_image_id: "7912"
_4images_cat_id: "734"
_4images_user_id: "504"
_4images_image_date: "2006-12-17T11:38:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7912 -->
Noch einmal die Kegelzahnräder