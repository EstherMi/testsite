---
layout: "image"
title: "Ansicht von oben"
date: "2006-12-08T22:51:26"
picture: "DSCI0004.jpg"
weight: "19"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: ["Bearbeitungszentrum", "oben"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/7747
imported:
- "2019"
_4images_image_id: "7747"
_4images_cat_id: "734"
_4images_user_id: "504"
_4images_image_date: "2006-12-08T22:51:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7747 -->
Bearbeitungszentrum von oben.