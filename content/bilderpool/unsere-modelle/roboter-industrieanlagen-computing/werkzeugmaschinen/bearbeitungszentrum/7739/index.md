---
layout: "image"
title: "Personal"
date: "2006-12-08T22:51:19"
picture: "DSCI0028.jpg"
weight: "11"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: ["Bearbeitungszentrum", "Personal"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/7739
imported:
- "2019"
_4images_image_id: "7739"
_4images_cat_id: "734"
_4images_user_id: "504"
_4images_image_date: "2006-12-08T22:51:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7739 -->
...diesen beiden Angestellten geprüft und notfalls aussortiert werden.