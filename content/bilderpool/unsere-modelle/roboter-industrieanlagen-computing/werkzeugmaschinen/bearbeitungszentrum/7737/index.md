---
layout: "image"
title: "Bohrmaschine"
date: "2006-12-08T22:51:19"
picture: "DSCI0018.jpg"
weight: "9"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: ["Bearbeitungszentrum", "Bohrmaschine"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/7737
imported:
- "2019"
_4images_image_id: "7737"
_4images_cat_id: "734"
_4images_user_id: "504"
_4images_image_date: "2006-12-08T22:51:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7737 -->
Hier siht man die zweite Bearbeitungstelle: Eine Bohrmaschine.