---
layout: "image"
title: "Lichtschranke im Magazin"
date: "2006-12-08T22:51:19"
picture: "DSCI0026.jpg"
weight: "7"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: ["Bearbeitungszentrum", "Lichtschranke"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/7735
imported:
- "2019"
_4images_image_id: "7735"
_4images_cat_id: "734"
_4images_user_id: "504"
_4images_image_date: "2006-12-08T22:51:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7735 -->
Hier sieht man die Lichtschranke im Detail.