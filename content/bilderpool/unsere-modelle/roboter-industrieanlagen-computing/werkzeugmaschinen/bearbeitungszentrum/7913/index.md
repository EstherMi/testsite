---
layout: "image"
title: "2. Bohrmaschine"
date: "2006-12-17T11:38:54"
picture: "Bearbeitungszentrum_058.jpg"
weight: "3"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: ["Bearbeitungszentrum"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/7913
imported:
- "2019"
_4images_image_id: "7913"
_4images_cat_id: "734"
_4images_user_id: "504"
_4images_image_date: "2006-12-17T11:38:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7913 -->
Und noch einmal...