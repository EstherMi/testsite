---
layout: "image"
title: "Drehtisch"
date: "2006-12-08T22:51:18"
picture: "DSCI0024.jpg"
weight: "5"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: ["Bearbeitungszentrum", "Drehtisch"]
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/7733
imported:
- "2019"
_4images_image_id: "7733"
_4images_cat_id: "734"
_4images_user_id: "504"
_4images_image_date: "2006-12-08T22:51:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7733 -->
Links kann man jetzt sehr gut das Magazin sehen und den dazugehörigen Austoßer. In der Mitte sieht man den Drehtisch mit den beiden Bearbeitungsstationen Stanze und Bohrmaschine. Etwas daneben den Auswerfer mit dem Abförderband.