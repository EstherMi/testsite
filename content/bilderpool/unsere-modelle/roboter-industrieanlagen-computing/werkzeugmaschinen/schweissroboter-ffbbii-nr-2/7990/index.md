---
layout: "image"
title: "Schweißroboter-Unterprogramm Lampe_1"
date: "2006-12-20T17:23:14"
picture: "Schweiroboter_011.jpg"
weight: "9"
konstrukteure: 
- "Jakob Wolf"
fotografen:
- "Jakob Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FFBBII"
license: "unknown"
legacy_id:
- details/7990
imported:
- "2019"
_4images_image_id: "7990"
_4images_cat_id: "745"
_4images_user_id: "420"
_4images_image_date: "2006-12-20T17:23:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7990 -->
