---
layout: "image"
title: "Schweißroboter-2. Achenfunktion"
date: "2006-12-20T17:23:14"
picture: "Schweiroboter_007.jpg"
weight: "5"
konstrukteure: 
- "Jakob Wolf"
fotografen:
- "Jakob Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FFBBII"
license: "unknown"
legacy_id:
- details/7986
imported:
- "2019"
_4images_image_id: "7986"
_4images_cat_id: "745"
_4images_user_id: "420"
_4images_image_date: "2006-12-20T17:23:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7986 -->
