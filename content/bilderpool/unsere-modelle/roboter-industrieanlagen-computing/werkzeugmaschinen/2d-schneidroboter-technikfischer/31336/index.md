---
layout: "image"
title: "Heißdraht"
date: "2011-07-22T16:23:27"
picture: "g08.jpg"
weight: "8"
konstrukteure: 
- "technikfischer"
fotografen:
- "technikfischer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- details/31336
imported:
- "2019"
_4images_image_id: "31336"
_4images_cat_id: "2329"
_4images_user_id: "1218"
_4images_image_date: "2011-07-22T16:23:27"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31336 -->
Hier mit Blitz, damit der Heißdraht sichtbar ist. Er ist zwischen 2 Federn gespannt