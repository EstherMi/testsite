---
layout: "image"
title: "Y-System"
date: "2011-07-22T16:23:27"
picture: "g05.jpg"
weight: "5"
konstrukteure: 
- "technikfischer"
fotografen:
- "technikfischer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- details/31333
imported:
- "2019"
_4images_image_id: "31333"
_4images_cat_id: "2329"
_4images_user_id: "1218"
_4images_image_date: "2011-07-22T16:23:27"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31333 -->
Hier das System, durch dass die Y-Achsen angetrieben werden