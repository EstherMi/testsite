---
layout: "image"
title: "Test"
date: "2011-07-22T16:23:27"
picture: "g09.jpg"
weight: "9"
konstrukteure: 
- "technikfischer"
fotografen:
- "technikfischer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- details/31337
imported:
- "2019"
_4images_image_id: "31337"
_4images_cat_id: "2329"
_4images_user_id: "1218"
_4images_image_date: "2011-07-22T16:23:27"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31337 -->
Hier ein frisch geschnittenes Styroporstück