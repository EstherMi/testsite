---
layout: "image"
title: "Fräsroboter"
date: "2006-03-18T22:59:04"
picture: "Fischertechnik-Bilder_008.jpg"
weight: "1"
konstrukteure: 
- "Jakob Wolf"
fotografen:
- "Jakob Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FFBBII"
license: "unknown"
legacy_id:
- details/5901
imported:
- "2019"
_4images_image_id: "5901"
_4images_cat_id: "512"
_4images_user_id: "420"
_4images_image_date: "2006-03-18T22:59:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5901 -->
