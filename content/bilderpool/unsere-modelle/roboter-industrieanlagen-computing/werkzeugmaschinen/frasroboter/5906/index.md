---
layout: "image"
title: "Fräsroboter"
date: "2006-03-18T22:59:05"
picture: "Fischertechnik-Bilder_013.jpg"
weight: "6"
konstrukteure: 
- "Jakob Wolf"
fotografen:
- "Jakob Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FFBBII"
license: "unknown"
legacy_id:
- details/5906
imported:
- "2019"
_4images_image_id: "5906"
_4images_cat_id: "512"
_4images_user_id: "420"
_4images_image_date: "2006-03-18T22:59:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5906 -->
