---
layout: "image"
title: "Achsenschnecke"
date: "2006-03-18T22:59:05"
picture: "Fischertechnik-Bilder_010.jpg"
weight: "3"
konstrukteure: 
- "Jakob Wolf"
fotografen:
- "Jakob Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FFBBII"
license: "unknown"
legacy_id:
- details/5903
imported:
- "2019"
_4images_image_id: "5903"
_4images_cat_id: "512"
_4images_user_id: "420"
_4images_image_date: "2006-03-18T22:59:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5903 -->
