---
layout: "image"
title: "Anschaltknopf ( Aus )"
date: "2013-02-15T00:33:19"
picture: "IMG_4615.jpg"
weight: "4"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lukas99h."
license: "unknown"
legacy_id:
- details/36634
imported:
- "2019"
_4images_image_id: "36634"
_4images_cat_id: "2715"
_4images_user_id: "1631"
_4images_image_date: "2013-02-15T00:33:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36634 -->
Hier kann man die Säge ein-  und ausschalten,  dazu muss man die gelbe Strebe nach vorne drücken,  dadurch wird der Taster betätigt.