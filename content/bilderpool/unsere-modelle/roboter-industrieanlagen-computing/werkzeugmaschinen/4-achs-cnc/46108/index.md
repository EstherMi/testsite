---
layout: "image"
title: "Fräskopf"
date: "2017-08-01T09:50:58"
picture: "achscnc5.jpg"
weight: "6"
konstrukteure: 
- "Jan"
fotografen:
- "Jan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- details/46108
imported:
- "2019"
_4images_image_id: "46108"
_4images_cat_id: "3424"
_4images_user_id: "1164"
_4images_image_date: "2017-08-01T09:50:58"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46108 -->
Um das Gewicht der beiden Motoren für den Kopf möglichst nah an der Z-Achse zu halten sind beide Motoren verlängert. Die Spindel wird von einem PowerMotor angetrieben, an welchen direkt die Achse angeflanscht ist. Dafür wurden auf der Drehmaschine 2 Adapter gedreht, um einmal von Motor auf Achse zu gehen und einmal von Achse auf Werkzeug.