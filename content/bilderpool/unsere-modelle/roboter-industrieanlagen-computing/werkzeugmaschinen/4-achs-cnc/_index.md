---
layout: "overview"
title: "4-Achs CNC"
date: 2019-12-17T19:04:28+01:00
legacy_id:
- categories/3424
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3424 --> 
Hier die Fotos meiner 4-Achs CNC Maschine ;-)

Der Hauptteil der Fräse wird mit einem TX angesteuert. Die Spindel wird über eins der alten Interfaces angesteuert.
Weitere Features:

-Automatische Türen
-Druckluftanschluss
-Werkzeughalterung
-Beleuchtung