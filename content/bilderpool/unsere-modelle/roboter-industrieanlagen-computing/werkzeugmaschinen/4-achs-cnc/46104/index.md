---
layout: "image"
title: "Gesamtansicht"
date: "2017-08-01T09:50:58"
picture: "achscnc1.jpg"
weight: "1"
konstrukteure: 
- "Jan"
fotografen:
- "Jan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- details/46104
imported:
- "2019"
_4images_image_id: "46104"
_4images_cat_id: "3424"
_4images_user_id: "1164"
_4images_image_date: "2017-08-01T09:50:58"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46104 -->
Hier eine Ansicht, bei der die Fräse gerade ein imaginäres Werkstück plan gefräst hat