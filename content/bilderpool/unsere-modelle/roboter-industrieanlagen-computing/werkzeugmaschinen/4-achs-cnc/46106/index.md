---
layout: "image"
title: "Antrieb X und Y-Achse"
date: "2017-08-01T09:50:58"
picture: "achscnc3.jpg"
weight: "4"
konstrukteure: 
- "Jan"
fotografen:
- "Jan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- details/46106
imported:
- "2019"
_4images_image_id: "46106"
_4images_cat_id: "3424"
_4images_user_id: "1164"
_4images_image_date: "2017-08-01T09:50:58"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46106 -->
Zu sehen rechts der Antrieb der X-Achse und Hinten links der Antrieb der Y- Achse.

Der Mini-Motor in der oberen Bildmitte ist für die Tür zuständig.