---
layout: "image"
title: "GymBot_03"
date: "2011-07-17T14:47:32"
picture: "gymbot3.jpg"
weight: "3"
konstrukteure: 
- "Lars B."
fotografen:
- "Lars B."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lars"
license: "unknown"
legacy_id:
- details/31317
imported:
- "2019"
_4images_image_id: "31317"
_4images_cat_id: "2327"
_4images_user_id: "1177"
_4images_image_date: "2011-07-17T14:47:32"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31317 -->
(geöffnete Sensorklappe)

In die Röhre legt man die Taler rein.
Die werden dann von der CD nach und nach aufs Fließband gelegt.
Über dem Fließband ist ein Farbsensor der erkennt wann ein Taler vorbeikommt.
Vom Fließband fallen die münzen direkt auf eine Rutsche.
Unter die Rutsche kann man dann einen Karton oder eine Tüte tun zum auffangen der Taler.