---
layout: "image"
title: "IMG_20160604_081249"
date: "2018-01-21T09:22:20"
picture: "roboter10.jpg"
weight: "10"
konstrukteure: 
- "nicolas kurz"
fotografen:
- "nicolas kurz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nicolas kurz"
license: "unknown"
legacy_id:
- details/47155
imported:
- "2019"
_4images_image_id: "47155"
_4images_cat_id: "3487"
_4images_user_id: "2819"
_4images_image_date: "2018-01-21T09:22:20"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47155 -->
eine Idee für einen kopf mit kräftiger beißzange