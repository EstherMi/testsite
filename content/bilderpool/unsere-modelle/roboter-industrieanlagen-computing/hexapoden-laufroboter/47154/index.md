---
layout: "image"
title: "IMG_20160604_081230"
date: "2018-01-21T09:22:20"
picture: "roboter09.jpg"
weight: "9"
konstrukteure: 
- "nicolas kurz"
fotografen:
- "nicolas kurz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nicolas kurz"
license: "unknown"
legacy_id:
- details/47154
imported:
- "2019"
_4images_image_id: "47154"
_4images_cat_id: "3487"
_4images_user_id: "2819"
_4images_image_date: "2018-01-21T09:22:20"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47154 -->
hier zu sehen ist eine alter SD21. Der fliegt auß und wird durch ein SC 32 servo controller board ersetzt.

der roboter hat sogar eine kleine ladefläche