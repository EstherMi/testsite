---
layout: "image"
title: "IMG_20160604_081342"
date: "2018-01-21T09:22:20"
picture: "roboter13.jpg"
weight: "13"
konstrukteure: 
- "nicolas kurz"
fotografen:
- "nicolas kurz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nicolas kurz"
license: "unknown"
legacy_id:
- details/47158
imported:
- "2019"
_4images_image_id: "47158"
_4images_cat_id: "3487"
_4images_user_id: "2819"
_4images_image_date: "2018-01-21T09:22:20"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47158 -->
konstruktrion von beinen für einen Laufroboter. Als Antrieb sind Standard servos (40x40x20 mm) vorgesehen. 
Die hubkraft der servos sollte bei dem Modellgewicht etwa 10-20 kg betragen.