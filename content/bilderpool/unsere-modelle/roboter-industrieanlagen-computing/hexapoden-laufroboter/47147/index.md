---
layout: "image"
title: "IMG_20160604_080909"
date: "2018-01-21T09:22:20"
picture: "roboter02.jpg"
weight: "2"
konstrukteure: 
- "nicolas kurz"
fotografen:
- "nicolas kurz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nicolas kurz"
license: "unknown"
legacy_id:
- details/47147
imported:
- "2019"
_4images_image_id: "47147"
_4images_cat_id: "3487"
_4images_user_id: "2819"
_4images_image_date: "2018-01-21T09:22:20"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47147 -->
ein bisschen was zur elektronik.

ein Arduino und ein SSC 32  werden zusamen den roboter ansteuern.  der Arduino berechnet die servopositionen und der SSC-32 sorgt für die korrekte ansteuerung.
In einem anderen laufroboter (nicht fischertechnik) funktioniert das alles Prima


beide bausteine den Arduino bzw den SSC 32 bekommt man bei :   https://www.robotshop.com/eu/en/lynxmotion-ssc-32u-usb-servo-controller.html
                                                                                                      :   https://www.robotshop.com/eu/en/lynxmotion-botboarduino-robot-controller.html

Wer Interesse an dem Projekt hat der kann ja mal hier schauen:  https://www.robotshop.com/eu/en/lynxmotion-phoenix-3dof-hexapod---black-no-servos---electronics.html    
wenn der Link funktioniert sieht man gleich viel besser was ich spannendes nachbauen möchte mit fischertechnik.