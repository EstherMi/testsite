---
layout: "image"
title: "IMG_20160604_081314"
date: "2018-01-21T09:22:20"
picture: "roboter12.jpg"
weight: "12"
konstrukteure: 
- "nicolas kurz"
fotografen:
- "nicolas kurz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nicolas kurz"
license: "unknown"
legacy_id:
- details/47157
imported:
- "2019"
_4images_image_id: "47157"
_4images_cat_id: "3487"
_4images_user_id: "2819"
_4images_image_date: "2018-01-21T09:22:20"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47157 -->
und geschlossen