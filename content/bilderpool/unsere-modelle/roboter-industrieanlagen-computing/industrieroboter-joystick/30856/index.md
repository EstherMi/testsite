---
layout: "image"
title: "Drehteller Arm1"
date: "2011-06-14T22:35:23"
picture: "rob2.jpg"
weight: "2"
konstrukteure: 
- "scripter1"
fotografen:
- "scripter1"
keywords: ["Industrieroboter", "Zange", "Baustein", "Joystick", "Gamepad", "Java"]
uploadBy: "scripter1"
license: "unknown"
legacy_id:
- details/30856
imported:
- "2019"
_4images_image_id: "30856"
_4images_cat_id: "2303"
_4images_user_id: "1305"
_4images_image_date: "2011-06-14T22:35:23"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30856 -->
Hier sieht man den Drehteller, der den ganzen Roboter dreht. Daneben ist das Interface.

Das Video zum Roboter gibt's hier: http://www.youtube.com/watch?v=43wwCYx08m4