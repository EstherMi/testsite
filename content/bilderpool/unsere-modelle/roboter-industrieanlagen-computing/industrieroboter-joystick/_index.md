---
layout: "overview"
title: "Industrieroboter mit Joystick"
date: 2019-12-17T19:07:43+01:00
legacy_id:
- categories/2303
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2303 --> 
Mein ftIndustrieroboter lässt sich mit einem normalen Joystick oder Gamepad steuern. Das Programm ist in Java programmiert. Der Roboter ist im Normalzustand 58cm hoch, mit ausgefahrenem Arm2, 90cm hoch. Arm2 hat eine Länge von 102cm, mit Zange 119cm. Der Roboter lässt sich drehen und hat dabei im Moment einen Arbeitsbereich von 180°. Arm 2 lässt sich in einem Bereich von 75° auf und abbewegen. 

Ein Video gibts unter: 
http://www.youtube.com/watch?v=43wwCYx08m4