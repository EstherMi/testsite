---
layout: "image"
title: "Wie das Programm ändern zum Positionierung mit Potmeter zum Cosinus-Bewegung wie im Natur ?"
date: "2014-02-05T13:37:57"
picture: "positionierungmitpotmeterzumcosinusbewegung1.jpg"
weight: "1"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/38207
imported:
- "2019"
_4images_image_id: "38207"
_4images_cat_id: "2842"
_4images_user_id: "22"
_4images_image_date: "2014-02-05T13:37:57"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38207 -->
Ich möchte gerne ein Robopro-Programm machen für eine Positionierung mit Potmeter zum Cosinus-Bewegung wie im Natur.
Pneumatisch weil es mit eine -Kolben-Zylinder oder eine -Muskel grossere, schnellere und kompaktere modellen möglich sein. Dann funktioniert es mit eine Zylinder und 2 Pneumatik-Ventilen
 oder
Elektrisch mit Motor (links, stop oder rechts) + eine Potmeter-Positionierung mit 10 Umdrehungen oder mit einer Schiebepotentiometer 

 Wie muss ich eine Timer + cos360 ( oder cos2pi ) im RoboProProgramm einbauen für eine continue Cosinus-Bewegung mit Ventilen bei einer Pneumatik-Antrieb ?