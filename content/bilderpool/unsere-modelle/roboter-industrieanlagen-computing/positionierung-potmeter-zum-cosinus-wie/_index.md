---
layout: "overview"
title: "Positionierung mit Potmeter zum Cosinus-Bewegung wie im Natur"
date: 2019-12-17T19:08:03+01:00
legacy_id:
- categories/2842
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2842 --> 
Ich möchte gerne ein anderes  Robopro-Programm machen für ein Pneumatik-Zylinder-Positionierung mit Potmeter zum Cosinus-Bewegung wie im Natur.

Pneumatisch weil es mit eine -Kolben-Zylinder oder eine -Muskel grossere und schnellere modellen möglich sein.
Elektrisch wäre auch moglich mit eine Potmeter-Positionierung mit 10 Umdrehungen oder mit einer Schiebepotentiometer.

Problem beim RoboPro-Programm fur eine Cosinus-Bewegung wie im Natur :

- Wie muss ich eine Timer für dieses Problem nutzen ?