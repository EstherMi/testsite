---
layout: "image"
title: "Steuerung 2"
date: "2010-01-11T18:20:07"
picture: "kameraroboter12.jpg"
weight: "12"
konstrukteure: 
- "Philip Lawall"
fotografen:
- "Philip Lawall"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fueller"
license: "unknown"
legacy_id:
- details/26066
imported:
- "2019"
_4images_image_id: "26066"
_4images_cat_id: "1841"
_4images_user_id: "1026"
_4images_image_date: "2010-01-11T18:20:07"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26066 -->
