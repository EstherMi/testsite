---
layout: "image"
title: "Detail"
date: "2010-01-11T18:19:56"
picture: "kameraroboter03.jpg"
weight: "3"
konstrukteure: 
- "Philip Lawall"
fotografen:
- "Philip Lawall"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fueller"
license: "unknown"
legacy_id:
- details/26057
imported:
- "2019"
_4images_image_id: "26057"
_4images_cat_id: "1841"
_4images_user_id: "1026"
_4images_image_date: "2010-01-11T18:19:56"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26057 -->
