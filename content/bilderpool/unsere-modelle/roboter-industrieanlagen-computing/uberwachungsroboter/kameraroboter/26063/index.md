---
layout: "image"
title: "Deatail5"
date: "2010-01-11T18:19:57"
picture: "kameraroboter09.jpg"
weight: "9"
konstrukteure: 
- "Philip Lawall"
fotografen:
- "Philip Lawall"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fueller"
license: "unknown"
legacy_id:
- details/26063
imported:
- "2019"
_4images_image_id: "26063"
_4images_cat_id: "1841"
_4images_user_id: "1026"
_4images_image_date: "2010-01-11T18:19:57"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26063 -->
