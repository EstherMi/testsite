---
layout: "image"
title: "Gelenk"
date: "2011-07-09T15:57:38"
picture: "bild6.jpg"
weight: "6"
konstrukteure: 
- "Martin Peché"
fotografen:
- "Martin Peché"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- details/31018
imported:
- "2019"
_4images_image_id: "31018"
_4images_cat_id: "2317"
_4images_user_id: "1218"
_4images_image_date: "2011-07-09T15:57:38"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31018 -->
Hier das Gelenk. Mit einem Strohalm wird das einzwicken der Kabel verhindert