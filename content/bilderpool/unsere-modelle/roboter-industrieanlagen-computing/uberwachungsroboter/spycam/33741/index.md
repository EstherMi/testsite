---
layout: "image"
title: "Gesamtansicht"
date: "2011-12-23T19:30:21"
picture: "spycam01.jpg"
weight: "1"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- details/33741
imported:
- "2019"
_4images_image_id: "33741"
_4images_cat_id: "2496"
_4images_user_id: "1322"
_4images_image_date: "2011-12-23T19:30:21"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33741 -->
Hallo!
Hier seht ihr das ganze Modell, es ist dazu gedacht eine DSLR zu drehen und zu neigen. Das "Blickfeld" der Kamera kann per Live View eingesehen werden (dazu verwende ich Sofortbild ( www.sofortbildapp.com ), es funktioniert bis jetzt aber nur mit Nikon Kameras, aber für Canon usw. gibt es sicher auch ähnliche Programme). Die Neigungssteuerung ist nicht mit Interface etc. gelöst, sondern durch eine Schaltung (siehe ft:pedia 2/2011). Die Drehung wird einfach durch den "Rechts-Aus-Links-Schalter" gelöst.
LG, Lukas

PS: Video kommt noch!