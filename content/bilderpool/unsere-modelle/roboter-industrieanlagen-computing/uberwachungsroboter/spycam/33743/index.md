---
layout: "image"
title: "Live Bild"
date: "2011-12-23T19:30:21"
picture: "spycam03.jpg"
weight: "3"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kieseleck"
license: "unknown"
legacy_id:
- details/33743
imported:
- "2019"
_4images_image_id: "33743"
_4images_cat_id: "2496"
_4images_user_id: "1322"
_4images_image_date: "2011-12-23T19:30:21"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33743 -->
So sieht das Live Bild am Computer aus. Zum Bild sag ich nur: Aug um Aug, Zahn um Zahn...;-)