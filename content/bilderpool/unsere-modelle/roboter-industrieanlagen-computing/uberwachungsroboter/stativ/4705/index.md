---
layout: "image"
title: "Stativ_7"
date: "2005-08-30T21:06:48"
picture: "Stativ_007_2.jpg"
weight: "8"
konstrukteure: 
- "ffcoe"
fotografen:
- "ffcoe"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ffcoe"
license: "unknown"
legacy_id:
- details/4705
imported:
- "2019"
_4images_image_id: "4705"
_4images_cat_id: "380"
_4images_user_id: "332"
_4images_image_date: "2005-08-30T21:06:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4705 -->
Hier kann man ganz schwach das Releis erkennen. Es Schaltet vom einen zum anderen Akku um. Es wird geschaltet, wenn der Akku (vorne Unten) Unter eine bestimmte Spannungsgrenze fällt. Somit läuft das Interface auf "reserve" weiter.