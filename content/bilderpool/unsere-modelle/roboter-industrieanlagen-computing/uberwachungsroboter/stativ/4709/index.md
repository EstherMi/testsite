---
layout: "image"
title: "Ständer_1"
date: "2005-08-31T19:45:00"
picture: "E-Modelle_001.jpg"
weight: "1"
konstrukteure: 
- "ffcoe"
fotografen:
- "ffcoe"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ffcoe"
license: "unknown"
legacy_id:
- details/4709
imported:
- "2019"
_4images_image_id: "4709"
_4images_cat_id: "380"
_4images_user_id: "332"
_4images_image_date: "2005-08-31T19:45:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4709 -->
Der Ständer besteht aus zwei Teilen, die man auseinander schieben kann. So wird der Transport erleichtert.