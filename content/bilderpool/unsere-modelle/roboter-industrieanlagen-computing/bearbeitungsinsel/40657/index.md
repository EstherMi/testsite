---
layout: "image"
title: "Bs 30 und Farbsensor"
date: "2015-03-14T12:36:48"
picture: "bearbeitungsinsel5.jpg"
weight: "5"
konstrukteure: 
- "Gunnar A."
fotografen:
- "G. Andresen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "gunand256"
license: "unknown"
legacy_id:
- details/40657
imported:
- "2019"
_4images_image_id: "40657"
_4images_cat_id: "3050"
_4images_user_id: "2357"
_4images_image_date: "2015-03-14T12:36:48"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40657 -->
Über dem roten Förderband schwebt ein drehbar aufgehängter Bs30. Er hat die Aufgabe das Werkstück bis zum Mitnehmer auf der Kette zurückzuschieben. Das gewährleistet, dass ein Ws sauber und zuverlässig vom Magazin in die Bucht auf dem Drehkreuz gedrückt werden kann. - Die ermittelte Farbe beeinflusst nur, wie oft ein Schweiß- oder Putzvorgang wiederholt wird, bevor das Kreuz sich weiterdreht.