---
layout: "overview"
title: "Bearbeitungsinsel"
date: 2019-12-17T19:08:19+01:00
legacy_id:
- categories/3050
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3050 --> 
Es werden verschiedenfarbige Werkstücke endlos im Kreis herum bewegt und dabei bearbeitet.Die Idee für dieses Modell bekam ich beim Durchforsten des Bilderpools.
