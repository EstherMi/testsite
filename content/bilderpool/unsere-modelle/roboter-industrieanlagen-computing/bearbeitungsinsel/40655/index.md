---
layout: "image"
title: "Förderband"
date: "2015-03-14T12:36:48"
picture: "bearbeitungsinsel3.jpg"
weight: "3"
konstrukteure: 
- "Gunnar A."
fotografen:
- "G. Andresen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "gunand256"
license: "unknown"
legacy_id:
- details/40655
imported:
- "2019"
_4images_image_id: "40655"
_4images_cat_id: "3050"
_4images_user_id: "2357"
_4images_image_date: "2015-03-14T12:36:48"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40655 -->
Vorne rechts sieht man die Schiebemechanik mit Pneumatikzylinder, die ein Werkstück aus der Bucht im Drehkreuz auf das Förderband bewegt. Auf der linken Seite die Pressstation mit Lichtschranke.