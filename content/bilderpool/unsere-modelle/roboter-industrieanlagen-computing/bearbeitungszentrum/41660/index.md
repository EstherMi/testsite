---
layout: "image"
title: "5-Achsroboter"
date: "2015-07-31T11:29:50"
picture: "bzmfr02.jpg"
weight: "2"
konstrukteure: 
- "david-ftc"
fotografen:
- "david-ftc"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/41660
imported:
- "2019"
_4images_image_id: "41660"
_4images_cat_id: "3105"
_4images_user_id: "2228"
_4images_image_date: "2015-07-31T11:29:50"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41660 -->
Steurung des Roboters über X / Y Koordinaten, Programm errechnet daraus die Winkel für die Achsen A, B und C
Der gesamte Aufbau sowie die zwei Vakuumsauger sind drehbar.
Die Steuerung ist identische mit der von meinem ersten 5-Achsroboter: http://ftcommunity.de/categories.php?cat_id=2990
