---
layout: "image"
title: "Förderband (2)"
date: "2015-07-31T11:29:50"
picture: "bzmfr06.jpg"
weight: "6"
konstrukteure: 
- "david-ftc"
fotografen:
- "david-ftc"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/41664
imported:
- "2019"
_4images_image_id: "41664"
_4images_cat_id: "3105"
_4images_user_id: "2228"
_4images_image_date: "2015-07-31T11:29:50"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41664 -->
Förderband liefert "neues Material"
Lichtschranke