---
layout: "overview"
title: "Bearbeitungszentrum"
date: 2019-12-17T19:08:21+01:00
legacy_id:
- categories/3105
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3105 --> 
- Das Bearbeitungszentrum ist eines meiner Modelle, dass ich am Fischertechnik Fan Club Tag 2015 ausgestellt habe.
- Es besteht aus einem 5-Achsroboter und mehreren Bearbeitungsstationen.
- Programmablauf:
  - Roboter nimmt per Vakuumsauger ein Werkstück vom Förderband
  - Der Farbsensor bestimmt die Farbe des Werkstücks (mehr dazu siehe Bild "Scanner")
  - Das Plättchen wird von drei Seiten geschweißt
  - Zuletzt wird es an der Stanzpresse bearbeitet und dann nach Farbe in ein Magazin sortiert