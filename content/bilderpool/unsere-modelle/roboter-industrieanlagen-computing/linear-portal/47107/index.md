---
layout: "image"
title: "Linearlager"
date: "2018-01-15T16:48:06"
picture: "Portal1_kl_2.jpg"
weight: "3"
konstrukteure: 
- "Lothar Vogt      Pilami"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- details/47107
imported:
- "2019"
_4images_image_id: "47107"
_4images_cat_id: "3484"
_4images_user_id: "10"
_4images_image_date: "2018-01-15T16:48:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47107 -->
nochmal die andere Seite