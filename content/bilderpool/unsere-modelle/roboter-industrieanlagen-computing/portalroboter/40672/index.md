---
layout: "image"
title: "Portalroboter-Antrieb Y-Achse"
date: "2015-03-21T18:16:47"
picture: "portalroboter05.jpg"
weight: "5"
konstrukteure: 
- "Jahnn"
fotografen:
- "Jahnn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jahnn"
license: "unknown"
legacy_id:
- details/40672
imported:
- "2019"
_4images_image_id: "40672"
_4images_cat_id: "3054"
_4images_user_id: "2327"
_4images_image_date: "2015-03-21T18:16:47"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40672 -->
Die Kette für die Y-Achse wird auf beiden Seiten angetrieben. Da das Standartzahnrad für die "Umdrehungserfassung" nicht genügend Schritte hat, verwende ich hier ein Zahnrad. Diese Lösung ist aber nicht perfekt, da sie durch ein großes Spiel ungenau wird, wenn man oft hoch/runter fährt.