---
layout: "overview"
title: "Portalroboter"
date: 2019-12-17T19:08:20+01:00
legacy_id:
- categories/3054
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3054 --> 
Ein Portalroboter mit beweglichem Tisch und variablen Einsatzzwecken. Hier ist er mit einem Vakuumgreifer ausgestattet, um Holzklötze zu stapeln. Denkbar wäre aber auch ein Stift um ihn als Plotter zu verwenden oder eine Zange um auch andere Dinge greifen zu können.