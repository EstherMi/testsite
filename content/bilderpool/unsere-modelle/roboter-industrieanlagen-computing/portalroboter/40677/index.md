---
layout: "image"
title: "Portalroboter-Tisch"
date: "2015-03-21T18:16:47"
picture: "portalroboter10.jpg"
weight: "10"
konstrukteure: 
- "Jahnn"
fotografen:
- "Jahnn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jahnn"
license: "unknown"
legacy_id:
- details/40677
imported:
- "2019"
_4images_image_id: "40677"
_4images_cat_id: "3054"
_4images_user_id: "2327"
_4images_image_date: "2015-03-21T18:16:47"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40677 -->
Blick unter den Tisch. Das Papier bietet eine bessere Standfläche