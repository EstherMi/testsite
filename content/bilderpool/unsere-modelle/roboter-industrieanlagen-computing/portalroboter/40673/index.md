---
layout: "image"
title: "Portalroboter-Antrieb Y-Achse"
date: "2015-03-21T18:16:47"
picture: "portalroboter06.jpg"
weight: "6"
konstrukteure: 
- "Jahnn"
fotografen:
- "Jahnn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jahnn"
license: "unknown"
legacy_id:
- details/40673
imported:
- "2019"
_4images_image_id: "40673"
_4images_cat_id: "3054"
_4images_user_id: "2327"
_4images_image_date: "2015-03-21T18:16:47"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40673 -->
Antrieb Y-Achse von Innen