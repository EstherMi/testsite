---
layout: "image"
title: "Portalroboter-Steuerung und Kompressor"
date: "2015-03-21T18:16:47"
picture: "portalroboter03.jpg"
weight: "3"
konstrukteure: 
- "Jahnn"
fotografen:
- "Jahnn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jahnn"
license: "unknown"
legacy_id:
- details/40670
imported:
- "2019"
_4images_image_id: "40670"
_4images_cat_id: "3054"
_4images_user_id: "2327"
_4images_image_date: "2015-03-21T18:16:47"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40670 -->
Hier sieht man den umgebauten Kompressor, um den Unterdruck zu erzeugen. Dazu wurde der Schlauch auf der anderen Seite des Rückschlagsventil mit Hilfe des weißen Anschlussstückes für Magnetventile befestigt.