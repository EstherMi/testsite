---
layout: "image"
title: "Portalroboter-Antrieb Tisch"
date: "2015-03-21T18:16:47"
picture: "portalroboter04.jpg"
weight: "4"
konstrukteure: 
- "Jahnn"
fotografen:
- "Jahnn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jahnn"
license: "unknown"
legacy_id:
- details/40671
imported:
- "2019"
_4images_image_id: "40671"
_4images_cat_id: "3054"
_4images_user_id: "2327"
_4images_image_date: "2015-03-21T18:16:47"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40671 -->
Antrieb des Tisches. Der stehende Taster ist für die Endlagenerkennung