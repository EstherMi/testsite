---
layout: "image"
title: "Ansicht von oben"
date: "2015-08-03T13:01:55"
picture: "992.jpg"
weight: "5"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/41697
imported:
- "2019"
_4images_image_id: "41697"
_4images_cat_id: "3108"
_4images_user_id: "579"
_4images_image_date: "2015-08-03T13:01:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41697 -->
