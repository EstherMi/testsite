---
layout: "image"
title: "Hinten"
date: "2010-05-01T14:59:00"
picture: "superflacherroboter2.jpg"
weight: "2"
konstrukteure: 
- "T-bear"
fotografen:
- "T-bear"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "T-bear"
license: "unknown"
legacy_id:
- details/27024
imported:
- "2019"
_4images_image_id: "27024"
_4images_cat_id: "1944"
_4images_user_id: "1124"
_4images_image_date: "2010-05-01T14:59:00"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27024 -->
Hier seht ihr den roboter von hinten. Man sieht den accu und auch gerade noch den TX.