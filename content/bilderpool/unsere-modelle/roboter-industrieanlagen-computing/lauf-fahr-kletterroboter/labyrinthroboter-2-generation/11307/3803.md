---
layout: "comment"
hidden: true
title: "3803"
date: "2007-08-09T11:05:01"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Wie kommt es denn, dass der Verlauf beim Drehen einige Messpunkte lang stetig verläuft und dann plötzlich eine Stufe erscheint?

Gruß,
Stefan