---
layout: "image"
title: "Lab2-10"
date: "2007-08-08T20:06:34"
picture: "Winkel.jpg"
weight: "5"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/11307
imported:
- "2019"
_4images_image_id: "11307"
_4images_cat_id: "977"
_4images_user_id: "46"
_4images_image_date: "2007-08-08T20:06:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11307 -->
Im Diagramm geht die Vorwärtsrichtung nach rechts. Dort ist die Entfernung mit 25 cm gefunden und nach links mit 13 cm. Die Werte sind zu klein und müssen noch angepaßt werden. Bei über 40 cm Entfernung wird der Wert zu Null gesetzt.

Erkennbar ist jedoch die recht mangelhafte Auflösung des AD-Wandlers, die deutlich geringer ist, als es der Wertebereich erahnen läßt. So benimmt sich ein 8-bit-Wandler.