---
layout: "image"
title: "Lab2-02"
date: "2007-06-09T20:47:33"
picture: "Lab2-02.jpg"
weight: "7"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/10771
imported:
- "2019"
_4images_image_id: "10771"
_4images_cat_id: "977"
_4images_user_id: "46"
_4images_image_date: "2007-06-09T20:47:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10771 -->
Heckansicht. Hier ist jetzt unten der deutlich kleinere Batteriekasten zu sehen. Darin sind 8 Stück Mignon-Akkus. Damit sollte der Roboter mehrere Stunden arbeiten können.