---
layout: "comment"
hidden: true
title: "5337"
date: "2008-02-17T19:03:32"
uploadBy:
- "remadus"
license: "unknown"
imported:
- "2019"
---
Das liegt daran, daß dieser Roboter nur einen Distanzsensor hat, den er wie ein Entfernungsradar zur Erkundung der Umgebung benutzt.

Wenn er dann die Abstände zu den Wänden kennt, kann er entscheiden, in welcher Richtung er das Wendemanöver durchführt. In diesem Fall arbeitet die Odometrie wesentlich präziser und ist programmiertechnisch deutlich einfacher.

Wie gesagt, ich benutze die Odometrie nur für kurze Strecken und richte sie immer wieder nach den Distanzsensoren neu aus.