---
layout: "comment"
hidden: true
title: "5331"
date: "2008-02-17T16:58:57"
uploadBy:
- "remadus"
license: "unknown"
imported:
- "2019"
---
Dafür habe ich ja noch den "großen" Labyrinthroboter. Der odometriert derzeit mit 2000 Impulsen pro Radumdrehung, während dieser hier nur 32 nutzt.

Das macht halt schon einen 486er Prozessor aus.