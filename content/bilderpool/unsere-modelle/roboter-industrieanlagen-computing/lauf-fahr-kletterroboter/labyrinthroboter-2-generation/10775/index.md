---
layout: "image"
title: "Lab2-06"
date: "2007-06-09T20:47:34"
picture: "Lab2-06.jpg"
weight: "11"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/10775
imported:
- "2019"
_4images_image_id: "10775"
_4images_cat_id: "977"
_4images_user_id: "46"
_4images_image_date: "2007-06-09T20:47:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10775 -->
Andere Seite. Das Antriebsrad paßt mal gerade so zwischen die Stecker. 1 mm mehr, und es würde so nicht gehen.