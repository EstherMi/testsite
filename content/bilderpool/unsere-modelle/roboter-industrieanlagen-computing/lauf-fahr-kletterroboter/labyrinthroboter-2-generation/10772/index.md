---
layout: "image"
title: "Lab2-03"
date: "2007-06-09T20:47:33"
picture: "Lab2-03.jpg"
weight: "8"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/10772
imported:
- "2019"
_4images_image_id: "10772"
_4images_cat_id: "977"
_4images_user_id: "46"
_4images_image_date: "2007-06-09T20:47:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10772 -->
Sicht auf den Rundumsensor. Ein Entfernungssensor reicht, wenn dieser sich umschauen kann. Also wird er 360 Grad drehbar gemacht und kriegt eine Referenzmarke. Von da aus zählt der Antrieb seine Positionen mit, so daß der Roboter "weiß", wo er hinschaut. So kann er auch rückwärts navigieren.

Im Vordergrund steckt ein Elko in dem Reedkontakthalter. Diese kleine Elektronik stellt dem Entfernungssensor gesiebte 5 Volt zur Verfügung und macht auch noch einen Tiefpaß für das Interface. So kommt vom Sensor ein einwandfreies Signal.

Die Kabel verschwinden einfach durch das Loch in der Mitte. Die Elektronik muß selber darauf achten, die Kabel nicht aufzuwickeln.