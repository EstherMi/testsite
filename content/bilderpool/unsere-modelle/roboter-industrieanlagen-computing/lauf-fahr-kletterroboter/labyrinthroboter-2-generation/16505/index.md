---
layout: "image"
title: "Lab2-13"
date: "2008-11-24T19:38:44"
picture: "16-Abstandssensor.jpg"
weight: "1"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: ["Sharp", "Entfernungssensor", "Infrarot"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/16505
imported:
- "2019"
_4images_image_id: "16505"
_4images_cat_id: "977"
_4images_user_id: "46"
_4images_image_date: "2008-11-24T19:38:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16505 -->
Im Infrarotlicht kann man einem Sharp-Entfernungssensor bei seiner Arbeit zuschauen. Obwohl die IR-LED immer nur sehr kurz aufblitzt, ist der Punkt für die Kamera sehr hell.

Und er ist erstaunlich scharf gebündelt.