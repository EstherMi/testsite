---
layout: "image"
title: "Lab2-01"
date: "2007-06-09T20:47:33"
picture: "Lab2-01.jpg"
weight: "6"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/10770
imported:
- "2019"
_4images_image_id: "10770"
_4images_cat_id: "977"
_4images_user_id: "46"
_4images_image_date: "2007-06-09T20:47:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10770 -->
Der neue Labyrinthroboter der zweiten Generation. Irgendwie erinnert er doch sehr an den ersten. Das Konzept habe ich 1:1 übernommen. Das bedeutet eine angetriebene Lenkachse, Lenkwinkelsensor, Radsensoren für Wegstrecke und hier neu, der Rundumentfernungssensor.

Im Gegensatz zum ersten Roboter wiegt dieser hier nicht einmal die Hälfte. Und bis auf das Potentiometer auf der Lenkachse und dem Entfernungssensor ist der ganze Rest purer Fischertechnik.