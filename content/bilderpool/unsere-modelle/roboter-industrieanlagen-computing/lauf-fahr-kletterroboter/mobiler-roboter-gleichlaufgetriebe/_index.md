---
layout: "overview"
title: "Mobiler Roboter mit Gleichlaufgetriebe"
date: 2019-12-17T18:56:54+01:00
legacy_id:
- categories/722
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=722 --> 
Ein mobiler Roboter mit Gleichlaufgetriebe.
Eine Verbesserung des Basismodells vom Robo Mobile Set.