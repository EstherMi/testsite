---
layout: "image"
title: "Beide Antriebe"
date: "2006-12-04T16:39:28"
picture: "MobilerRoboter3.jpg"
weight: "3"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/7700
imported:
- "2019"
_4images_image_id: "7700"
_4images_cat_id: "722"
_4images_user_id: "456"
_4images_image_date: "2006-12-04T16:39:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7700 -->
Hinzu kommt der Antrieb des Lenkdifferenzials. Es ist das selbe Prinzip wie beim Fahrdifferenzial.