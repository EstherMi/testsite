---
layout: "image"
title: "Anfang"
date: "2006-12-04T16:39:28"
picture: "MobilerRoboter1.jpg"
weight: "1"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/7698
imported:
- "2019"
_4images_image_id: "7698"
_4images_cat_id: "722"
_4images_user_id: "456"
_4images_image_date: "2006-12-04T16:39:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7698 -->
Dies ist der Anfang eines Mobilen Roboters mit Gleichlaufgetriebe. Ich baue ihn, weil das Basismodell aus dem Robo Mobile Set sehr schräg fährt.