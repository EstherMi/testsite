---
layout: "image"
title: "Befestigung der Power-mots"
date: "2006-12-04T16:39:28"
picture: "MobilerRoboter4.jpg"
weight: "4"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/7701
imported:
- "2019"
_4images_image_id: "7701"
_4images_cat_id: "722"
_4images_user_id: "456"
_4images_image_date: "2006-12-04T16:39:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7701 -->
Mit einer Kette habe ich eine zusätzliche Befestigung für die Power-mots. Den Trick hab ich mir vom Cub Solver von Mar-mack abgeschaut.