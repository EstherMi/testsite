---
layout: "image"
title: "Schwenkrad"
date: "2006-12-04T16:39:29"
picture: "MobilerRoboter6.jpg"
weight: "6"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/7703
imported:
- "2019"
_4images_image_id: "7703"
_4images_cat_id: "722"
_4images_user_id: "456"
_4images_image_date: "2006-12-04T16:39:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7703 -->
Das ist das hintere Schwenkrad. Selbes Prinzip wie beim Robo Mobile Set.