---
layout: "image"
title: "Wagen"
date: "2008-08-30T13:50:57"
picture: "Schienen-Magnet-Roboter_15.jpg"
weight: "6"
konstrukteure: 
- "Marius Moosmann"
fotografen:
- "Marius Moosmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mari"
license: "unknown"
legacy_id:
- details/15147
imported:
- "2019"
_4images_image_id: "15147"
_4images_cat_id: "1387"
_4images_user_id: "189"
_4images_image_date: "2008-08-30T13:50:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15147 -->
