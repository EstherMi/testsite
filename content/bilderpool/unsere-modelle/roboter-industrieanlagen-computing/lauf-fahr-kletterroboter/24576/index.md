---
layout: "image"
title: "Gartenroboter vorne"
date: "2009-07-17T22:04:01"
picture: "Robo_001_2.jpg"
weight: "6"
konstrukteure: 
- "Olaf Panteleit"
fotografen:
- "Olaf Panteleit"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ole"
license: "unknown"
legacy_id:
- details/24576
imported:
- "2019"
_4images_image_id: "24576"
_4images_cat_id: "579"
_4images_user_id: "916"
_4images_image_date: "2009-07-17T22:04:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24576 -->
Die vorderseite des Gartenroboters. Im Bild sieht man die Hindernisserkenung