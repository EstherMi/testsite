---
layout: "image"
title: "Impuls vorne"
date: "2007-02-11T15:17:16"
picture: "classicstileroboter7.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/8938
imported:
- "2019"
_4images_image_id: "8938"
_4images_cat_id: "811"
_4images_user_id: "445"
_4images_image_date: "2007-02-11T15:17:16"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8938 -->
Wieder mit schwarzem (mini-)Taster.