---
layout: "image"
title: "Mobiler Roboter"
date: "2008-03-22T12:31:00"
picture: "mr1.jpg"
weight: "2"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/13996
imported:
- "2019"
_4images_image_id: "13996"
_4images_cat_id: "1284"
_4images_user_id: "456"
_4images_image_date: "2008-03-22T12:31:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13996 -->
Das Ziel ist es, einen geländetüchtigen mobilen Roboter zu bauen, welcher sich alleine bewegen kann, seine Umgebung wahrnimmt und darauf reagiert, welcher u.a. Aufträge ausführen soll, z.B. zum Gartenteich fahren und einen Stein versenken o.Ä. 
Bis jetzt verfügt er über einen mit 50:1Power-Motoren angertiebenen Antrieb, auf jeder Seite einer. Die Kette ist dann zusätzlich untersetzt (3:1). 
Trotzdem hat er ein zugiges Tempo und ordentlich Kraft. Besonders die ASteigfähigkeit ist erstaunlich.
Ich entschuldige, dass er so dreckig ist, ich hatte bis jetzt nur die LKetten geputzt, für den rest war noch keine Zeit.
Außerdem verfügt er über ein Radar, welches mit Schleifringen gelagert ist. 
Der Gleichlauf wird per Software funktionieren.
Bis jetzt wiegt er ziemlich genau 2Kg.