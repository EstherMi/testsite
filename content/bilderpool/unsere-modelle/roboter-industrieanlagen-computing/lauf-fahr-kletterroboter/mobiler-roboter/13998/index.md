---
layout: "image"
title: "Monbiler Roboter von der Seite"
date: "2008-03-22T12:31:00"
picture: "mr3.jpg"
weight: "4"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/13998
imported:
- "2019"
_4images_image_id: "13998"
_4images_cat_id: "1284"
_4images_user_id: "456"
_4images_image_date: "2008-03-22T12:31:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13998 -->
Seitenansicht.