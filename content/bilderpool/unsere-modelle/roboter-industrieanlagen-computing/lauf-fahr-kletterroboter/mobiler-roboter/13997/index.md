---
layout: "image"
title: "Mobiler Roboter von vorne"
date: "2008-03-22T12:31:00"
picture: "mr2.jpg"
weight: "3"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/13997
imported:
- "2019"
_4images_image_id: "13997"
_4images_cat_id: "1284"
_4images_user_id: "456"
_4images_image_date: "2008-03-22T12:31:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13997 -->
Vorne.