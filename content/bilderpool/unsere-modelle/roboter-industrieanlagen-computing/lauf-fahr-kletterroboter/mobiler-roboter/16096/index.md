---
layout: "image"
title: "Winkelzahnräder splint"
date: "2008-11-01T22:33:43"
picture: "Winkelzahnrad_vernietet.jpg"
weight: "1"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: ["modding"]
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/16096
imported:
- "2019"
_4images_image_id: "16096"
_4images_cat_id: "1284"
_4images_user_id: "456"
_4images_image_date: "2008-11-01T22:33:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16096 -->
Der mr wurde schon vor einiger Zeit auseinander genommen. Im Moment entsteht ein neuer, naja im Grunde genommen soll es ein Panzer werden gesteuert von einem Atmel und einigen Sensoren(kamera usw wird wohl irgendwann mal kommen, wenn es Zeit und Geld zulassen).
Zur Mechanik dient Severins Panzer als Vorbild, sorry, aber das ist einfach genial ich werde die Fotos auch eher über Steuerung und eigene Details machen.
Hier habe ich die Winkelzahnräder. Sie rattern leider und ein Antrieb ist gar unmöglich. Das liegt wohl daran, dass die Messingachsen nicht 2mal gelagert sind.
Kommt noch...
Ich habe mit einem 1,5mm Bohrer durch Achse und Zahnrad gebohrt, einen Nagel durch und abgezwickt und dann mit dem Hammer "vernietet". Da dreht die Achse nicht mehr durch, habe das übrigens auch bei den Z20 auf denen die Kette laufen gemacht.