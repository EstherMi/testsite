---
layout: "image"
title: "Kugel-Lager Detail"
date: "2016-06-08T18:14:50"
picture: "Buggy7.jpg"
weight: "7"
konstrukteure: 
- "Roland Enzenhofer"
fotografen:
- "Dirk Uffmann"
keywords: ["BBC", "Buggy", "Economatics", "Labyrinth", "Maze", "3D", "Drucker"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/43704
imported:
- "2019"
_4images_image_id: "43704"
_4images_cat_id: "3237"
_4images_user_id: "579"
_4images_image_date: "2016-06-08T18:14:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43704 -->
Spezialteil aus Rolands 3D Drucker