---
layout: "image"
title: "Liniensensor"
date: "2016-06-08T18:14:50"
picture: "Buggy9a.jpg"
weight: "10"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["BBC", "Buggy", "Economatics", "Labyrinth", "Maze", "Liniensensor"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/43707
imported:
- "2019"
_4images_image_id: "43707"
_4images_cat_id: "3237"
_4images_user_id: "579"
_4images_image_date: "2016-06-08T18:14:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43707 -->
Hier der eigens für diesen Zweck konstruierte Liniensensor mit 4 Sensorelemente ITR8307 angelehnt an das Messprinzip des Polulu QTR-8RC.