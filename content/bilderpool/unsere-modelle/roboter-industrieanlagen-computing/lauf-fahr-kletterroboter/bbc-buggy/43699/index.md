---
layout: "image"
title: "Von schräg oben gesehen"
date: "2016-06-08T15:17:13"
picture: "Buggy2.jpg"
weight: "2"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["BBC", "Buggy", "Economatics", "1983", "Labyrinth", "Maze", "Linien"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/43699
imported:
- "2019"
_4images_image_id: "43699"
_4images_cat_id: "3237"
_4images_user_id: "579"
_4images_image_date: "2016-06-08T15:17:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43699 -->
