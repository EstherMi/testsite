---
layout: "image"
title: "Von schräg hinten"
date: "2016-06-08T18:14:50"
picture: "Buggy5.jpg"
weight: "5"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["BBC", "Buggy", "Economatics", "Labyrinth", "Maze", "Linien"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/43702
imported:
- "2019"
_4images_image_id: "43702"
_4images_cat_id: "3237"
_4images_user_id: "579"
_4images_image_date: "2016-06-08T18:14:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43702 -->
