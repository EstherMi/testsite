---
layout: "image"
title: "Das verwendete Labyrinth"
date: "2016-06-08T18:14:50"
picture: "maze2.jpg"
weight: "13"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Labyrinth", "Maze", "BBC", "Buggy"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/43710
imported:
- "2019"
_4images_image_id: "43710"
_4images_cat_id: "3237"
_4images_user_id: "579"
_4images_image_date: "2016-06-08T18:14:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43710 -->
Das Labyrinth wurde mit inkscape erstellt und in Größe A3 ausgedruckt.