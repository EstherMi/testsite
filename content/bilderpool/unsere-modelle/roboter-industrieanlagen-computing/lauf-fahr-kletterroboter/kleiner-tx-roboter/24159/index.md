---
layout: "image"
title: "TX Roboter"
date: "2009-06-05T16:02:16"
picture: "tx2.jpg"
weight: "2"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/24159
imported:
- "2019"
_4images_image_id: "24159"
_4images_cat_id: "1659"
_4images_user_id: "453"
_4images_image_date: "2009-06-05T16:02:16"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24159 -->
