---
layout: "image"
title: "Explorer 7"
date: "2007-10-06T18:50:07"
picture: "explorerstefanl07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/12150
imported:
- "2019"
_4images_image_id: "12150"
_4images_cat_id: "1087"
_4images_user_id: "502"
_4images_image_date: "2007-10-06T18:50:07"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12150 -->
Der Wärmesensor.