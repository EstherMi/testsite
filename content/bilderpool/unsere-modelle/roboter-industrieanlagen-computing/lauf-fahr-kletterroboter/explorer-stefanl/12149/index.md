---
layout: "image"
title: "Explorer 6"
date: "2007-10-06T18:50:07"
picture: "explorerstefanl06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/12149
imported:
- "2019"
_4images_image_id: "12149"
_4images_cat_id: "1087"
_4images_user_id: "502"
_4images_image_date: "2007-10-06T18:50:07"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12149 -->
Hier zu sehen die 3 Lichter zum anzeigen der Farben.