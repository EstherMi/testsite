---
layout: "image"
title: "Explorer 4"
date: "2007-10-06T18:50:07"
picture: "explorerstefanl04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/12147
imported:
- "2019"
_4images_image_id: "12147"
_4images_cat_id: "1087"
_4images_user_id: "502"
_4images_image_date: "2007-10-06T18:50:07"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12147 -->
Hier der seitliche Taster.