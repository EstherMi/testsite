---
layout: "image"
title: "Explorer 10"
date: "2007-10-06T18:50:07"
picture: "explorerstefanl10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/12153
imported:
- "2019"
_4images_image_id: "12153"
_4images_cat_id: "1087"
_4images_user_id: "502"
_4images_image_date: "2007-10-06T18:50:07"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12153 -->
Mit diesem Schalter kann man zwischen Spurensucher und Lichtsucher umschalten. Der Wärmesensor und der obere Lichtsensor werden durch die 2 vorderen Lichtsensoren ausgetauscht, dies geschieht durch die 2 rechten Taster. Das Programm weiß durch den linken Taster welche Funktion eingestellt ist.