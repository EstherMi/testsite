---
layout: "image"
title: "Explorer 5"
date: "2007-10-06T18:50:07"
picture: "explorerstefanl05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/12148
imported:
- "2019"
_4images_image_id: "12148"
_4images_cat_id: "1087"
_4images_user_id: "502"
_4images_image_date: "2007-10-06T18:50:07"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12148 -->
