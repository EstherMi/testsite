---
layout: "image"
title: "Explorer 4"
date: "2008-05-20T14:18:36"
picture: "grosserexplorer4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/14546
imported:
- "2019"
_4images_image_id: "14546"
_4images_cat_id: "1338"
_4images_user_id: "502"
_4images_image_date: "2008-05-20T14:18:36"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14546 -->
Der Einschalter.