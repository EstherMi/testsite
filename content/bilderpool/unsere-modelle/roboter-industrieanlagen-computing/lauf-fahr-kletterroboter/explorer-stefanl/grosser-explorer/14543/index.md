---
layout: "image"
title: "Explorer 1"
date: "2008-05-20T14:18:36"
picture: "grosserexplorer1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/14543
imported:
- "2019"
_4images_image_id: "14543"
_4images_cat_id: "1338"
_4images_user_id: "502"
_4images_image_date: "2008-05-20T14:18:36"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14543 -->
Hier mal ein etwas größerer Explorer auf der Basis eines Kettenfahrzeugs. Der Abstandssensor ist schwenkbar. Während des Fahrens schenkt er hin und her und weicht aus wenn er ein Hindernis entdeckt.