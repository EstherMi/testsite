---
layout: "image"
title: "Explorer 9"
date: "2008-05-20T14:18:36"
picture: "grosserexplorer9.jpg"
weight: "9"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/14551
imported:
- "2019"
_4images_image_id: "14551"
_4images_cat_id: "1338"
_4images_user_id: "502"
_4images_image_date: "2008-05-20T14:18:36"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14551 -->
