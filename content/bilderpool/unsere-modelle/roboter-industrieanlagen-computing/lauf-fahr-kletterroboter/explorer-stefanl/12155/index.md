---
layout: "image"
title: "Explorer 12"
date: "2007-10-06T18:50:13"
picture: "explorerstefanl12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/12155
imported:
- "2019"
_4images_image_id: "12155"
_4images_cat_id: "1087"
_4images_user_id: "502"
_4images_image_date: "2007-10-06T18:50:13"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12155 -->
Hier der Kabelsalat, ein ziemliches Durcheinander :-)