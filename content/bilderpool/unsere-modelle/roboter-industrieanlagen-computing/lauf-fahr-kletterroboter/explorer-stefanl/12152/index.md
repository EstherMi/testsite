---
layout: "image"
title: "Explorer 9"
date: "2007-10-06T18:50:07"
picture: "explorerstefanl09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/12152
imported:
- "2019"
_4images_image_id: "12152"
_4images_cat_id: "1087"
_4images_user_id: "502"
_4images_image_date: "2007-10-06T18:50:07"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12152 -->
