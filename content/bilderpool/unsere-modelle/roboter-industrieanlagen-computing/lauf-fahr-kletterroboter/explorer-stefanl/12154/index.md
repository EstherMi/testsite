---
layout: "image"
title: "Explorer 11"
date: "2007-10-06T18:50:13"
picture: "explorerstefanl11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/12154
imported:
- "2019"
_4images_image_id: "12154"
_4images_cat_id: "1087"
_4images_user_id: "502"
_4images_image_date: "2007-10-06T18:50:13"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12154 -->
Hier ist der Schalter geschlossen.