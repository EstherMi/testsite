---
layout: "overview"
title: "Star Wars"
date: 2019-12-17T18:57:53+01:00
legacy_id:
- categories/2697
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2697 --> 
Dieses Projekt ist ungefähr 104 cm hoch und 63 cm breit.
Die Bauzeit betrug ca. 4-5 Stunden.
Es stellt den kleinen Roboter R2D2 aus Star-Wars dar.