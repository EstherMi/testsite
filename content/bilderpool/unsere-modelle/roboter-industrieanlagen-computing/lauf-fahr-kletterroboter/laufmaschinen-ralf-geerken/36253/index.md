---
layout: "image"
title: "Herrmann, der kleine Laufroboter Seite"
date: "2012-12-10T22:46:33"
picture: "herrmann1.jpg"
weight: "2"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- details/36253
imported:
- "2019"
_4images_image_id: "36253"
_4images_cat_id: "2690"
_4images_user_id: "381"
_4images_image_date: "2012-12-10T22:46:33"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36253 -->
Mein kaum wippender Gang basiert auf einer Viergelenkkette mit Geradführung