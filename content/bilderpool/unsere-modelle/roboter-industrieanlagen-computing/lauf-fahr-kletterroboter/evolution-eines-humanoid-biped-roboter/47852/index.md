---
layout: "image"
title: "...grimmig?"
date: "2018-09-03T16:06:26"
picture: "Kopf_2.jpg"
weight: "3"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/47852
imported:
- "2019"
_4images_image_id: "47852"
_4images_cat_id: "3529"
_4images_user_id: "1729"
_4images_image_date: "2018-09-03T16:06:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47852 -->
