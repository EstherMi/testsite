---
layout: "image"
title: "Erster Entwurf Bein"
date: "2018-08-27T17:50:38"
picture: "ErstesBein.jpg"
weight: "8"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: ["Roboter", "Humanoid", "Laufmaschine", "Biped"]
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/47846
imported:
- "2019"
_4images_image_id: "47846"
_4images_cat_id: "3529"
_4images_user_id: "1729"
_4images_image_date: "2018-08-27T17:50:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47846 -->
Hallo,
nach längerer Zeit will ich auch mal wieder etwas posten. Ich habe leider viel zu wenig Zeit, und mehrere angefangene Ideen stehen im Schrank und es geht nicht weiter.

Jetzt will ich mal von Anfang an eine Idee vorstellen und dann - wenn die Zeit es zulässt - den Projektfortschritt veröffentlichen.

Also, es geht um einen humanoiden Roboter, der weitgehend mit Fischertechnik aufgebaut werden soll und am Ende möglicherweise richtig laufen kann.

Hier der erste Entwurf eines Beines