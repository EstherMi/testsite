---
layout: "image"
title: "Rückansicht"
date: "2008-06-14T13:25:32"
picture: "invertiertespendel3.jpg"
weight: "3"
konstrukteure: 
- "Jan Käberich"
fotografen:
- "Käberich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- details/14677
imported:
- "2019"
_4images_image_id: "14677"
_4images_cat_id: "1347"
_4images_user_id: "521"
_4images_image_date: "2008-06-14T13:25:32"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14677 -->
Für diesen Roboter musste ich mir ein eigenes kleines "Interface" bauen, weil das Robo Interface dafür zu langsam war.