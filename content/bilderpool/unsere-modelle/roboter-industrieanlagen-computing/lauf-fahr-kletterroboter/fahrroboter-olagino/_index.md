---
layout: "overview"
title: "Fahrroboter von Olagino"
date: 2019-12-17T18:58:04+01:00
legacy_id:
- categories/3236
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3236 --> 
Der gezeigte Roboter wurde für eine Präsentation in der Schule gebaut und programmiert - wobei der Fokus auf der Programmierung lag. Das Fahrwerk basiert dabei teilweise aus dem Fischertechnik TXT Discovery Set. Der Roboter in Aktion und die dazugehörigen Programme sind unter https://youtu.be/-g0o633Fjts zu sehen.