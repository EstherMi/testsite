---
layout: "image"
title: "Robbie - bottom view"
date: "2006-11-14T16:23:03"
picture: "Robbie_bottomview.jpg"
weight: "8"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Paul van Niekerk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "niekerk"
license: "unknown"
legacy_id:
- details/7459
imported:
- "2019"
_4images_image_id: "7459"
_4images_cat_id: "704"
_4images_user_id: "385"
_4images_image_date: "2006-11-14T16:23:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7459 -->
At the bottom you see the three photo diodes meant for line following.