---
layout: "image"
title: "Robbie - display"
date: "2006-11-25T08:51:01"
picture: "Robbie_display.jpg"
weight: "4"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Paul van Niekerk"
keywords: ["display", "rotary", "encoder"]
uploadBy: "niekerk"
license: "unknown"
legacy_id:
- details/7599
imported:
- "2019"
_4images_image_id: "7599"
_4images_cat_id: "704"
_4images_user_id: "385"
_4images_image_date: "2006-11-25T08:51:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7599 -->
The "intelligent alphanumerical display" of Robbie. Actually, it is two displays of four characters each. They are easy to interface, and can be obtained from Conrad.

The display is controlled by an Atmel AtMega168, which also reads the rotary encoder. It also takes care of packing and unpacking of the 7-byte messages used by the Robo Interface on its serial port.