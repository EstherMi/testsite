---
layout: "image"
title: "Robbie - rear view"
date: "2006-11-14T16:23:03"
picture: "Robbie_rearview.jpg"
weight: "10"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Paul van Niekerk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "niekerk"
license: "unknown"
legacy_id:
- details/7461
imported:
- "2019"
_4images_image_id: "7461"
_4images_cat_id: "704"
_4images_user_id: "385"
_4images_image_date: "2006-11-14T16:23:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7461 -->
The back side shows the battery, some switches, and the display. The display is connected to the serial port of the Robo Interface.