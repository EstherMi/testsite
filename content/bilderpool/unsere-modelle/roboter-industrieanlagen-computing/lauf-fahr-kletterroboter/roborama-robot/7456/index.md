---
layout: "image"
title: "Robbie - overview"
date: "2006-11-14T16:23:03"
picture: "Robbie_overview.jpg"
weight: "5"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Paul van Niekerk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "niekerk"
license: "unknown"
legacy_id:
- details/7456
imported:
- "2019"
_4images_image_id: "7456"
_4images_cat_id: "704"
_4images_user_id: "385"
_4images_image_date: "2006-11-14T16:23:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7456 -->
(See below for Babelfish translation)

Robbie was made to participate in the RoboRama contest. This is a contest held by the Dutch HCC Robotica user group
and the Belgian Robot MC, similar to the RoboRama held for years by the Dallas Personal Robotics Group (DPRG). Besides 
RoboRama they also hold a mini Sumo contest.

This RoboRama contest consists of four parts:

- Quick Trip ("Heen en weer")
- T-time
- Line following
- Tin-retrieval ("Blikken")

More information on the contest can be found on http://members.home.nl/wim.deboer/roborama/
and on http://www.robotmc.org. Both sites have pictures of the event.

Robbie has 4 Sharp distance sensors, 3 photo diodes, 4 switches, 3 motors, a buzzer, and a display.

As you can see on the abovementioned pages, Robbie performed quite well. Except for the cans: it could lift them, but it couldn't find them!

-----
Babelfish translation:

Robbie wurde gebildet, um am RoboRama Wettbewerb teilzunehmen. Dieses ist ein Wettbewerb, der durch die holländische HCC Robotica Benutzergruppe und den belgischen Roboter MC gehalten wird, der dem RoboRama ähnlich ist, das für Jahre durch den Dallas Personal Robotics Group (DPRG) gehalten wird. Außer RoboRama halten sie auch einen MiniSumowettbewerb. Dieser 
RoboRama Wettbewerb besteht aus vier Teilen: 

- schnelle Reise ("Heen en weer") 
- T-Zeit 
- Linie Folgen 
- Zinn-Wiederherstellung ("Blikken") 

Mehr Informationen über den Wettbewerb  können auf http://members.home.nl/wim.deboer/roborama/ und auf http://www.robotmc.org gefunden werden. Beide Aufstellungsorte haben Abbildungen des Falls. Robbie hat 4 Sharp 
Abstand Sensoren, 3 Fotodioden, 4 Schalter, 3 Motoren, einen Summer und eine Anzeige. Wie Sie auf die obenerwähnten Seiten sehen können, führte Robbie ziemlich gut durch. Außer den Dosen: es könnte sie anheben, aber es könnte nicht sie finden!