---
layout: "image"
title: "Robbie - the gripper"
date: "2006-11-14T16:23:03"
picture: "Robbie_gripper.jpg"
weight: "9"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Paul van Niekerk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "niekerk"
license: "unknown"
legacy_id:
- details/7460
imported:
- "2019"
_4images_image_id: "7460"
_4images_cat_id: "704"
_4images_user_id: "385"
_4images_image_date: "2006-11-14T16:23:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7460 -->
This picture shows the gripper mechanism for lifting the cola cans. There's also a movie of this.

I cheated a bit by using tie wraps in some places, since I was otherwise not able to complete it in time for the match :-(