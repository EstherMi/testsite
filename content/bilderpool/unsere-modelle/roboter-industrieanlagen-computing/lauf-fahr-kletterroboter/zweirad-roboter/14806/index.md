---
layout: "image"
title: "Radsensor"
date: "2008-07-07T09:33:46"
picture: "zweiradroboter3.jpg"
weight: "3"
konstrukteure: 
- "Jan Käberich"
fotografen:
- "Käberich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- details/14806
imported:
- "2019"
_4images_image_id: "14806"
_4images_cat_id: "1353"
_4images_user_id: "521"
_4images_image_date: "2008-07-07T09:33:46"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14806 -->
Er besteht aus einem CNY37 und wird im Moment nicht benutzt. Später soll der Roboter aber auch messen können wie weit er gefahren ist.