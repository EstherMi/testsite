---
layout: "image"
title: "Elektronik+Display"
date: "2008-07-07T09:33:46"
picture: "zweiradroboter2.jpg"
weight: "2"
konstrukteure: 
- "Jan Käberich"
fotografen:
- "Käberich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- details/14805
imported:
- "2019"
_4images_image_id: "14805"
_4images_cat_id: "1353"
_4images_user_id: "521"
_4images_image_date: "2008-07-07T09:33:46"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14805 -->
Für diesen Roboter habe ich eine neue Elektronik gebaut, die etwas zu viele Funktionen hat, aber sie soll auch noch für andere Projekte benutzt werden.
Sie besteht aus einem Atmega32, Attiny2313(nicht eingebaut) und Motortreiber. Außerdem hat sie einen Infrarotsender und -empfänger. Der Roboter kann mit der ft-Fernbedienung gesteuert werden.
