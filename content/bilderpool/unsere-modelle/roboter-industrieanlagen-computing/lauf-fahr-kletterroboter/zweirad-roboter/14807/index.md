---
layout: "image"
title: "Kugel-Antrieb"
date: "2008-07-07T09:33:46"
picture: "zweiradroboter4.jpg"
weight: "4"
konstrukteure: 
- "Jan Käberich"
fotografen:
- "Käberich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- details/14807
imported:
- "2019"
_4images_image_id: "14807"
_4images_cat_id: "1353"
_4images_user_id: "521"
_4images_image_date: "2008-07-07T09:33:46"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14807 -->
Dies ist der erste Versuch eines neuen Antriebes für den Roboter(liegt hier verkehrt herum). Er soll dann auf einer Kugel balancieren. Allerdings ist die Kugel etwas glatt, sodass sie auf dem Boden immer durchdreht, ich werde mal nach einer Gummi-Kugel suchen.