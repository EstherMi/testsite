---
layout: "image"
title: "Gesamtansicht"
date: "2008-07-07T09:33:46"
picture: "zweiradroboter1.jpg"
weight: "1"
konstrukteure: 
- "Jan Käberich"
fotografen:
- "Käberich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- details/14804
imported:
- "2019"
_4images_image_id: "14804"
_4images_cat_id: "1353"
_4images_user_id: "521"
_4images_image_date: "2008-07-07T09:33:46"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14804 -->
Dieser Roboter ist der Nachfolger des Invertierten Pendels, er balanciert sich selbst auf zwei Rädern.
Bis jetzt kann er auf der Stelle balancieren und sich im Kreis drehen. Wenn er vorwärts oder rückwärts fahren soll kippt er noch um.