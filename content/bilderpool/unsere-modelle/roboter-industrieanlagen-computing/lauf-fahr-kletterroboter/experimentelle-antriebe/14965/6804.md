---
layout: "comment"
hidden: true
title: "6804"
date: "2008-07-27T21:39:00"
uploadBy:
- "Defiant"
license: "unknown"
imported:
- "2019"
---
Kehrblech: Zwischen den beiden anderen hat der dritte Motor dann aber keine Kraft um das Gestell zu bewegen. Außerdem ist das so stabiler.

Der Sinn ist übrigens auch noch das ganze Modell etwas schräger stellen zu können.