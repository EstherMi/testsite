---
layout: "image"
title: "von links"
date: "2012-09-08T15:23:52"
picture: "omnidirektionalesfahrwerk2.jpg"
weight: "2"
konstrukteure: 
- "Jan Werner"
fotografen:
- "Jan Werner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "werner"
license: "unknown"
legacy_id:
- details/35463
imported:
- "2019"
_4images_image_id: "35463"
_4images_cat_id: "2629"
_4images_user_id: "1196"
_4images_image_date: "2012-09-08T15:23:52"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35463 -->
Dieses Fahrwerk kann in der Ebene in alle Richtungen fahren und sich dabei auch drehen.