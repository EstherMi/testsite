---
layout: "image"
title: "von rechts"
date: "2012-09-08T15:23:52"
picture: "omnidirektionalesfahrwerk3.jpg"
weight: "3"
konstrukteure: 
- "Jan Werner"
fotografen:
- "Jan Werner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "werner"
license: "unknown"
legacy_id:
- details/35464
imported:
- "2019"
_4images_image_id: "35464"
_4images_cat_id: "2629"
_4images_user_id: "1196"
_4images_image_date: "2012-09-08T15:23:52"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35464 -->
Die Räder sind gleichmäßig in einem Kreis angeordnet.