---
layout: "image"
title: "von hinten"
date: "2012-10-22T19:11:45"
picture: "omnidirektionalesfahrwerkii1.jpg"
weight: "1"
konstrukteure: 
- "werner"
fotografen:
- "werner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "werner"
license: "unknown"
legacy_id:
- details/36039
imported:
- "2019"
_4images_image_id: "36039"
_4images_cat_id: "2682"
_4images_user_id: "1196"
_4images_image_date: "2012-10-22T19:11:45"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36039 -->
Der Vorteil dieser Version ist die Genauigkeit, die auf den höheren Abstand der Räder zurückzuführen ist.
Die Hartplastikrollen an den Rädern drehen beim Beschleunigen und Bremsen allerdings sehr stark durch,
sodass man eine Gummimatte als Unterlage benötigt...