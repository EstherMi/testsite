---
layout: "image"
title: "Nahaufnahme Hebewerk / Kabelführung"
date: "2017-10-02T17:32:28"
picture: "tarslaufroboter08.jpg"
weight: "8"
konstrukteure: 
- "olagino / Leon"
fotografen:
- "olagino / Leon"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- details/46656
imported:
- "2019"
_4images_image_id: "46656"
_4images_cat_id: "3445"
_4images_user_id: "2042"
_4images_image_date: "2017-10-02T17:32:28"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46656 -->
Gut zu erkennen ist hierbei der Kabel/Schlauchstrang zu einem der beiden Beine und die dazugehörige Schnecke des Hebewerks.