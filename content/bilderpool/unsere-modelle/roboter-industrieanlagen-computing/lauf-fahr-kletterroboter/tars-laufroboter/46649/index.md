---
layout: "image"
title: "Gesamtansicht Vorderseite"
date: "2017-10-02T17:32:28"
picture: "tarslaufroboter01.jpg"
weight: "1"
konstrukteure: 
- "olagino / Leon"
fotografen:
- "olagino / Leon"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- details/46649
imported:
- "2019"
_4images_image_id: "46649"
_4images_cat_id: "3445"
_4images_user_id: "2042"
_4images_image_date: "2017-10-02T17:32:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46649 -->
Hier steht der Roboter in Ruheposition. Gut zu erkennen sind hier die vier Encodermotoren (zwei der neuen Generation für den Hebemechanismus und zwei "alte" für die Kippbewegung der beiden äußeren Beine)