---
layout: "image"
title: "Nahaufnahme TX-Controller und Gelenke"
date: "2017-10-02T17:32:28"
picture: "tarslaufroboter03.jpg"
weight: "3"
konstrukteure: 
- "olagino / Leon"
fotografen:
- "olagino / Leon"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- details/46651
imported:
- "2019"
_4images_image_id: "46651"
_4images_cat_id: "3445"
_4images_user_id: "2042"
_4images_image_date: "2017-10-02T17:32:28"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46651 -->
Links und rechts zu sehen sind die beiden Encodermotoren für die äußeren Beine sowie der TX-Controller der den Roboter kansteuert. Über den Polwendeschalter darüber kann man dem Programm mitteilen ob noch ein weiterer Schritt durchgeführt werden soll oder nicht.