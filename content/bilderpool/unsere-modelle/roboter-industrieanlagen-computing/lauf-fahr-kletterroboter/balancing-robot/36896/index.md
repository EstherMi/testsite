---
layout: "image"
title: "The drive part - 2"
date: "2013-05-12T16:50:50"
picture: "balancingrobot06.jpg"
weight: "6"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- details/36896
imported:
- "2019"
_4images_image_id: "36896"
_4images_cat_id: "2741"
_4images_user_id: "1505"
_4images_image_date: "2013-05-12T16:50:50"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36896 -->
Having the motors off the central axis, means that power needs to be transfered to the central axes. Gear wheels are not good because of the slack they create. In a balancing robot that jitters a lot, slack is bad. Also rast axles and rast connectors are not stiff enough for the fairly significant forces that the 1:50 power motors can create. The solution was a good old chain. With a lot of trial and error a construction was found that has just the right distance between the gears for a chain of certain length to fit without too much slack. The result doesn't get the price for elegant looks, but it proved to be a very sturdy, reliable solution. Once built, it run for hundreds of hours, and I never had to look at it again.