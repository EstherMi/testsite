---
layout: "image"
title: "Next Project: Ball bot"
date: "2013-05-12T16:50:50"
picture: "balancingrobot10.jpg"
weight: "10"
konstrukteure: 
- "Willem Evert Nijenhuis"
fotografen:
- "Willem Evert Nijenhuis"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- details/36900
imported:
- "2019"
_4images_image_id: "36900"
_4images_cat_id: "2741"
_4images_user_id: "1505"
_4images_image_date: "2013-05-12T16:50:50"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36900 -->
What's next after balancing in one dimension? Balancing in two, on a ball! This is work in progress. Right now it can stand upright (sort of) and maintain direction, but not very elegantly yet. More tuning needed. I will publish more details when it's doing better. Preliminary video here: http://www.youtube.com/watch?v=l9xvWE6e5_I