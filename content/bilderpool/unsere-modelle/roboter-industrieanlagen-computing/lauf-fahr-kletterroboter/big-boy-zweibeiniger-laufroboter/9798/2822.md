---
layout: "comment"
hidden: true
title: "2822"
date: "2007-03-27T13:19:29"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Cool! Saustarkes Teil. Mehr Fotos und nähere Details zur Steuerung (auch von Hand) wären willkommen :-)

Gruß,
Stefan