---
layout: "image"
title: "Fuß"
date: "2007-03-28T09:08:54"
picture: "131_3178.jpg"
weight: "1"
konstrukteure: 
- "Holger Howey"
fotografen:
- "Holger Howey"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/9823
imported:
- "2019"
_4images_image_id: "9823"
_4images_cat_id: "883"
_4images_user_id: "34"
_4images_image_date: "2007-03-28T09:08:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9823 -->
Fuß vom Roboter