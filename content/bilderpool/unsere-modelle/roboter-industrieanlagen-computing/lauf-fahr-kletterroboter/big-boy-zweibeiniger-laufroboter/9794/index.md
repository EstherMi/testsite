---
layout: "image"
title: "Gesammtansicht"
date: "2007-03-27T12:30:45"
picture: "131_3193.jpg"
weight: "10"
konstrukteure: 
- "Holger Howey"
fotografen:
- "Holger Howey"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/9794
imported:
- "2019"
_4images_image_id: "9794"
_4images_cat_id: "883"
_4images_user_id: "34"
_4images_image_date: "2007-03-27T12:30:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9794 -->
Hier noch ohne Kopf.
Die Gewichtsverlagerung ist ohne Sensoren sehr schwer.