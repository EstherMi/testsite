---
layout: "image"
title: "frontsensoren.jpg"
date: "2009-09-05T21:49:26"
picture: "frontsensoren.jpg"
weight: "6"
konstrukteure: 
- "Defiant"
fotografen:
- "Defiant"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Defiant"
license: "unknown"
legacy_id:
- details/24874
imported:
- "2019"
_4images_image_id: "24874"
_4images_cat_id: "1710"
_4images_user_id: "3"
_4images_image_date: "2009-09-05T21:49:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24874 -->
Kabelsalat geht los: Frontsensoren fertig montiert.