---
layout: "image"
title: "kabelsalatv1.jpg"
date: "2009-10-04T21:04:13"
picture: "kabelsalatv1.jpg"
weight: "5"
konstrukteure: 
- "Defiant"
fotografen:
- "Defiant"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Defiant"
license: "unknown"
legacy_id:
- details/25492
imported:
- "2019"
_4images_image_id: "25492"
_4images_cat_id: "1710"
_4images_user_id: "3"
_4images_image_date: "2009-10-04T21:04:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25492 -->
Noch nicht alles eingebaut und schon geht der Kabelsalat los...
...für Tips wär ich dankbar.