---
layout: "image"
title: "kopf.jpg"
date: "2009-08-30T18:33:42"
picture: "kopf.jpg"
weight: "10"
konstrukteure: 
- "Defiant"
fotografen:
- "Defiant"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Defiant"
license: "unknown"
legacy_id:
- details/24873
imported:
- "2019"
_4images_image_id: "24873"
_4images_cat_id: "1710"
_4images_user_id: "3"
_4images_image_date: "2009-08-30T18:33:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24873 -->
Der Kopf wird 1:1 einfach vom altem MR ohne Änderungen übernommen.