---
layout: "image"
title: "basis.jpg"
date: "2009-08-30T18:33:41"
picture: "basis.jpg"
weight: "7"
konstrukteure: 
- "Defiant"
fotografen:
- "Defiant"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Defiant"
license: "unknown"
legacy_id:
- details/24870
imported:
- "2019"
_4images_image_id: "24870"
_4images_cat_id: "1710"
_4images_user_id: "3"
_4images_image_date: "2009-08-30T18:33:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24870 -->
Die Basis für die neue Version.
Alu - gibt es dazu noch was zu sagen?