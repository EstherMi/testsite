---
layout: "image"
title: "P2150001.JPG"
date: "2012-02-25T14:04:02"
picture: "P2150001.jpg"
weight: "2"
konstrukteure: 
- "Defiant"
fotografen:
- "Defiant"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Defiant"
license: "unknown"
legacy_id:
- details/34396
imported:
- "2019"
_4images_image_id: "34396"
_4images_cat_id: "1710"
_4images_user_id: "3"
_4images_image_date: "2012-02-25T14:04:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34396 -->
Mal ein aktuelles Bild:
Hier zu sehen das Pandaboard (ARM Dual Cortex A9, Linux) was jetzt den ganzen Roboter über einen I2C-Bus steuert.
Zusätzlich wurde der rechte IR-Distanz-Sensor im Kopf durch eine Kamera ersetzt.