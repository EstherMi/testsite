---
layout: "image"
title: "FT Explorer"
date: "2011-10-20T21:35:01"
picture: "FT_Derk_022.jpg"
weight: "4"
konstrukteure: 
- "FT Derk"
fotografen:
- "Derk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- details/33271
imported:
- "2019"
_4images_image_id: "33271"
_4images_cat_id: "579"
_4images_user_id: "1289"
_4images_image_date: "2011-10-20T21:35:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33271 -->
