---
layout: "image"
title: "05-Endschalter und Zugstange"
date: "2008-11-09T14:32:57"
picture: "05-Endschalter_und_Zugstange.jpg"
weight: "9"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: ["Lenkgetriebe", "Endschalter", "Reflexlichtschranke", "Lenkeinschlag", "Lenkwinkel", "Zahnstange", "Schubgetriebe", "Lenkung", "Steuerung"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/16236
imported:
- "2019"
_4images_image_id: "16236"
_4images_cat_id: "1464"
_4images_user_id: "46"
_4images_image_date: "2008-11-09T14:32:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16236 -->
Hier ist jetzt im Detail das Lenkgetriebe mit dem Endschalter und der Zugstange aus Draht zu sehen. Lediglich die Reflexlichtschranke am Getriebe fehlt noch.

Der hintere Teil der Zahnstange schiebt sich noch unter den Endschalter. Das geht hauteng auf. Um die Lenkung zu benutzen, muß die Steuerung erst die Zahnstange komplett bis in den Endschalter einziehen, erst dann kann der Lenkeinschlag von da an gezählt werden. Einen Endschalter für das Herausschieben der Zahnstange gibt es aus Platzgründen nicht. Wozu auch?