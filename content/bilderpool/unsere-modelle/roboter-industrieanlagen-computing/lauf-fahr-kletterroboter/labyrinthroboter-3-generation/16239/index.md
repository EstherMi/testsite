---
layout: "image"
title: "08-Frontansicht"
date: "2008-11-09T14:32:58"
picture: "08-Frontansicht.jpg"
weight: "12"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: ["Kleinwagen"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/16239
imported:
- "2019"
_4images_image_id: "16239"
_4images_cat_id: "1464"
_4images_user_id: "46"
_4images_image_date: "2008-11-09T14:32:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16239 -->
Das ist der Neue. Er gehört echt in die Kategorie der Kleinwägen.