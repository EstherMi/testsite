---
layout: "image"
title: "02-Chassis"
date: "2008-11-09T14:32:56"
picture: "02-Chassis.jpg"
weight: "6"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: ["Chassis", "Radstand", "Differential", "Wegmessung", "Lenkwinkel"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/16233
imported:
- "2019"
_4images_image_id: "16233"
_4images_cat_id: "1464"
_4images_user_id: "46"
_4images_image_date: "2008-11-09T14:32:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16233 -->
Die Neuerwerbung des Power-Motors mit der dazugehörigen Spezialaufhängung macht dieses winzige Chassis möglich. Der Radstand mißt gerade mal 106 mm.

Auf dem Differentialgetriebe ist schon die Schwarzweiß-Markierung für die Wegmessung drauf. Ebenso die weißen Quadrate auf dem Zahnrad des Lenkgetriebes. Damit wird später der Lenkwinkel eingestellt. Wegen des Lenkspiels ist die Lenkung nicht wirklich präzise, aber sie funktioniert sehr gut.