---
layout: "image"
title: "12-Reflexlichtschranke"
date: "2008-11-23T16:17:33"
picture: "12-Reflexlichtschranke_2.jpg"
weight: "2"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: ["Reflexlichtschranke"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/16501
imported:
- "2019"
_4images_image_id: "16501"
_4images_cat_id: "1464"
_4images_user_id: "46"
_4images_image_date: "2008-11-23T16:17:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16501 -->
Das ist das kleine Teil. Leider ist die Reflexlichtschranke zu klein, um da eine Typenbezeichnung draufzutun. Deshalb weiß ich nicht, was das für eine ist.