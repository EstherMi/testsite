---
layout: "image"
title: "04-Unterboden"
date: "2008-11-09T14:32:57"
picture: "04-Unterboden.jpg"
weight: "8"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: ["Lenkmotor", "Unterboden", "Reflexlichtschranke", "Differentialgetriebe", "Wegmessung"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/16235
imported:
- "2019"
_4images_image_id: "16235"
_4images_cat_id: "1464"
_4images_user_id: "46"
_4images_image_date: "2008-11-09T14:32:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16235 -->
Hier der Blick unter die Kulisse. Der Lenkmotor ist ganze 2 mm vom Zahnkranz des Differentialgetriebes entfernt. Damit ist nur wenig Platz verschenkt.

Jetzt besser sichtbar, die Reflexmarkierung für die Wegmessung auf dem Differentialgetriebe. Die Auflösung der Wegmessung ist auf 1 cm begrenzt. Naja, aber besser als nichts.