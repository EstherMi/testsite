---
layout: "image"
title: "03-Lenkeinschlag"
date: "2008-11-09T14:32:56"
picture: "03-Lenkeinschlag.jpg"
weight: "7"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: ["Lenkgetriebe", "Wendekreis", "Schubgetriebe", "Kugellager", "Differential", "Lenkhebel", "Roboter"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/16234
imported:
- "2019"
_4images_image_id: "16234"
_4images_cat_id: "1464"
_4images_user_id: "46"
_4images_image_date: "2008-11-09T14:32:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16234 -->
Der Lenkeinschlag beträgt beachtlich 45 Grad und ist in beiden Richtungen problemlos erreichbar. Der Wendekreis ist damit minimal 42 cm über die Außenkante gemessen. Der Roboter kann also in einem 30 cm schmalen Gang mit nur einmal zurücksetzen wenden.

Gut sichtbar ist auch die Drahtbiegekonstruktion, die den Lenkhebel zieht und schiebt.

Ein Hinterrad ist ab, damit die hintere Achsaufhängung und die Kugellagerung sichtbar werden.