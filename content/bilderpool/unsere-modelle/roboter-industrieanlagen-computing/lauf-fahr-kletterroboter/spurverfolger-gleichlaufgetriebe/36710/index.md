---
layout: "image"
title: "Gleichlaufgetriebe mit Führungsrädern und Spursensor (von unten)"
date: "2013-03-04T14:09:17"
picture: "spurverfolgermitgleichlaufgetriebe2.jpg"
weight: "6"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/36710
imported:
- "2019"
_4images_image_id: "36710"
_4images_cat_id: "2722"
_4images_user_id: "1126"
_4images_image_date: "2013-03-04T14:09:17"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36710 -->
Mit wenigen Teilen lässt sich das Gleichlaufgetriebe zu einem Spurverfolger-Chassis erweitern: zwei Führungsrädchen und ein Spursensor (in möglichst großem Abstand von den Antriebsrädern montiert).
Ersetzt man die BS30 durch Statikteile, ist das Resultat ein echtes Leichtgewicht.