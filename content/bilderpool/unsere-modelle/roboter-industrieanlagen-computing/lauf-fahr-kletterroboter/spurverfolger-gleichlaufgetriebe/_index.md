---
layout: "overview"
title: "Spurverfolger mit Gleichlaufgetriebe"
date: 2019-12-17T18:57:53+01:00
legacy_id:
- categories/2722
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2722 --> 
Ein Fahrroboter mit Gleichlaufgetriebe benötigt keine Schritt- oder Encodermotoren für eine saubere Geradeausfahrt und kann punktgenau auf der Stelle drehen.