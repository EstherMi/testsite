---
layout: "image"
title: "Lenkmotor"
date: "2009-06-14T19:34:55"
picture: "explorer6.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- details/24366
imported:
- "2019"
_4images_image_id: "24366"
_4images_cat_id: "1669"
_4images_user_id: "920"
_4images_image_date: "2009-06-14T19:34:55"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24366 -->
Der Motor ist für das Lenken zuständig.