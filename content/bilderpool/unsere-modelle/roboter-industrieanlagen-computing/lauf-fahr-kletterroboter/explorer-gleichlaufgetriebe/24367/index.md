---
layout: "image"
title: "Vorne"
date: "2009-06-14T19:34:55"
picture: "explorer7.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Mr Smith"
license: "unknown"
legacy_id:
- details/24367
imported:
- "2019"
_4images_image_id: "24367"
_4images_cat_id: "1669"
_4images_user_id: "920"
_4images_image_date: "2009-06-14T19:34:55"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24367 -->
Abstandssensor und Lichtsucher.