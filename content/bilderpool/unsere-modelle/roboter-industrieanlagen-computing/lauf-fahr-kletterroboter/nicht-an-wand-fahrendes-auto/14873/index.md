---
layout: "image"
title: "Summer"
date: "2008-07-15T22:17:21"
picture: "nichtandiewandfahrendesauto6.jpg"
weight: "6"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/14873
imported:
- "2019"
_4images_image_id: "14873"
_4images_cat_id: "1357"
_4images_user_id: "747"
_4images_image_date: "2008-07-15T22:17:21"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14873 -->
Hier sieht man den Summer.