---
layout: "image"
title: "Gesamtbild"
date: "2008-07-15T22:17:21"
picture: "nichtandiewandfahrendesauto1.jpg"
weight: "1"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/14868
imported:
- "2019"
_4images_image_id: "14868"
_4images_cat_id: "1357"
_4images_user_id: "747"
_4images_image_date: "2008-07-15T22:17:21"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14868 -->
Das ist ein Gesamtbild von meinem Auto. Wenn es sich einer Wand oder einem Hindernis nähert, fährt das Auto ein Stückchen zurück und der Summer geht zwei Sekunden lang an.