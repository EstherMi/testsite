---
layout: "overview"
title: "Nicht an die Wand fahrendes Auto"
date: 2019-12-17T18:57:23+01:00
legacy_id:
- categories/1357
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1357 --> 
Das ist ein Auto, das nicht an die Wand fährt. Wenn es kurz davor ist geht der Summer an und das Auto fährt zwei Sekunden lang Rückwärts.