---
layout: "image"
title: "Onmi wheels"
date: "2014-01-18T17:48:18"
picture: "ballbalancingrobot03.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- details/38084
imported:
- "2019"
_4images_image_id: "38084"
_4images_cat_id: "2833"
_4images_user_id: "1505"
_4images_image_date: "2014-01-18T17:48:18"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38084 -->
Omni wheels allow free movement in the direction of the axis, as the small dark grey wheels rotate freely. I used 48mm omni wheels from robotshop.com (http://www.robotshop.com/en/48mm-omniwheel-lego-nxt-servo.html). They come with a hub for lego axles, which is useless for the 4mm fischertechnik axles, but luckily they have an 11mm hub that fits snugly over the fischertechnik hub nut (31915+35113), which can be tightened very firmly on the metal axles using two Z15 wheels (35695) to provide grip. To make a very stiff connection between the wheel and the hub nut, I forced a small copper nail in the little hub key hole.