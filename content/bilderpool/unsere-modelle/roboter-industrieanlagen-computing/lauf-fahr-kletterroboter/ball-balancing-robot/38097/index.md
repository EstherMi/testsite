---
layout: "image"
title: "The controllers (2)"
date: "2014-01-18T17:48:18"
picture: "ballbalancingrobot16.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- details/38097
imported:
- "2019"
_4images_image_id: "38097"
_4images_cat_id: "2833"
_4images_user_id: "1505"
_4images_image_date: "2014-01-18T17:48:18"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38097 -->
The power lines between the battery and the TX, as well as the connections between the TXs and the 3 motors were done using thicker wire than the regular red ft wires. I measured quite some power loss in the relatively thin ft wires, and replacing them with thicker ones helped significantly.