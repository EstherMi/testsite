---
layout: "image"
title: "Ball Balancing Robot"
date: "2014-01-18T17:48:17"
picture: "ballbalancingrobot01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- details/38082
imported:
- "2019"
_4images_image_id: "38082"
_4images_cat_id: "2833"
_4images_user_id: "1505"
_4images_image_date: "2014-01-18T17:48:17"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38082 -->
The end result. The balancing worked reasonably well. Driving was a lot more difficult, especially to get it smooth. As you can see in the video, I never managed it very well. In the process I learned that the system was very sensitive to friction, slack in the drive system and movement in the frame. I suspect that I achieved close to what is possible with the ft motors and a simple linear controller. Especially the slack in the 1:50 power motor gear box is probably a big factor, as well as the friction between the ball and the omni wheels.

 Anyway, lot's of trial and error to get even to this point. Here's a description of how I ended up there.

Video: http://www.youtube.com/watch?v=QuTUYofiwcE