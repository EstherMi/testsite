---
layout: "image"
title: "Remote Control"
date: "2014-01-18T17:48:18"
picture: "ballbalancingrobot09.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- details/38090
imported:
- "2019"
_4images_image_id: "38090"
_4images_cat_id: "2833"
_4images_user_id: "1505"
_4images_image_date: "2014-01-18T17:48:18"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38090 -->
With the ft remote control until it is possible to drive the robot. Operating the left control stick makes the robot roll in any direction. It won't rotate for that, as it can just roll anywhere without rotating.

The robot uses the compass and the z-axis gyro to maintain 'looking' in the direction it was started, no matter in which direction it drives. It will resist attempts to rotate when pushed. The right stick of the remote control is used to rotate the robot itself.