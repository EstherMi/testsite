---
layout: "image"
title: "The ball (3)"
date: "2014-01-18T17:48:18"
picture: "ballbalancingrobot19.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- details/38100
imported:
- "2019"
_4images_image_id: "38100"
_4images_cat_id: "2833"
_4images_user_id: "1505"
_4images_image_date: "2014-01-18T17:48:18"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38100 -->
To prevent slippage of the ball on the floor, I gave the ball a final coat of spray-on rubber. Of course this also provided additional friction between the omni wheels (which already had a relatively soft rubber coating) and the ball, which had a noticable negative effect on the smoothness of the robot's driving.