---
layout: "image"
title: "The ball (4)"
date: "2014-01-18T17:48:18"
picture: "ballbalancingrobot20.jpg"
weight: "20"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "winijenh"
license: "unknown"
legacy_id:
- details/38101
imported:
- "2019"
_4images_image_id: "38101"
_4images_cat_id: "2833"
_4images_user_id: "1505"
_4images_image_date: "2014-01-18T17:48:18"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38101 -->
End result. After tuning the controller, this was probably the part of the project that took the most time. At least the ball is light and smooth and provided sufficient friction on the floor to prevent significant slippage when the robot was driving or rotating.