---
layout: "image"
title: "Stock hinten"
date: "2007-03-01T16:56:00"
picture: "blinderroboter5.jpg"
weight: "17"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/9191
imported:
- "2019"
_4images_image_id: "9191"
_4images_cat_id: "849"
_4images_user_id: "445"
_4images_image_date: "2007-03-01T16:56:00"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9191 -->
Zwei Taster die angeben wann der Motor drehen muss.