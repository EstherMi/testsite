---
layout: "image"
title: "Neues Design von oben"
date: "2007-04-04T10:29:45"
picture: "blinderroboter11.jpg"
weight: "11"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/9943
imported:
- "2019"
_4images_image_id: "9943"
_4images_cat_id: "849"
_4images_user_id: "445"
_4images_image_date: "2007-04-04T10:29:45"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9943 -->
Hier sieht man warum diese Streben passen. Sie spalten sich genau dort wo die Inselchen für den Akkupack währen, und gehen dort auseinander wo die Kette auseinander geht.