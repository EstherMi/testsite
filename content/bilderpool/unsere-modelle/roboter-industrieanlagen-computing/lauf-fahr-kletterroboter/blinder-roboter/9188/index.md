---
layout: "image"
title: "Blindenstock"
date: "2007-03-01T16:56:00"
picture: "blinderroboter2.jpg"
weight: "14"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/9188
imported:
- "2019"
_4images_image_id: "9188"
_4images_cat_id: "849"
_4images_user_id: "445"
_4images_image_date: "2007-03-01T16:56:00"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9188 -->
Vier Taster habe ich eingebaut- einer der kontrolliert ob was unter ihm ist oder nicht, einer der checkt ob links was ist, einer der dasselbe rechts macht und einer der vorne prüft.