---
layout: "image"
title: "Taster seite"
date: "2007-03-01T16:56:00"
picture: "blinderroboter4.jpg"
weight: "16"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/9190
imported:
- "2019"
_4images_image_id: "9190"
_4images_cat_id: "849"
_4images_user_id: "445"
_4images_image_date: "2007-03-01T16:56:00"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9190 -->
Ein gartentor als "Spürfläche".