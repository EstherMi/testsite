---
layout: "image"
title: "walker_2"
date: "2012-08-21T17:42:04"
picture: "walker_1.jpg"
weight: "6"
konstrukteure: 
- "xbach"
fotografen:
- "xbach"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "xbach"
license: "unknown"
legacy_id:
- details/35348
imported:
- "2019"
_4images_image_id: "35348"
_4images_cat_id: "1184"
_4images_user_id: "427"
_4images_image_date: "2012-08-21T17:42:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35348 -->
