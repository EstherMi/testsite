---
layout: "comment"
hidden: true
title: "7628"
date: "2008-10-19T12:53:37"
uploadBy:
- "heiko"
license: "unknown"
imported:
- "2019"
---
Was Du brauchst, ist ein hohes Trägheitsmoment. Das ist definiert als &#8721; m_i * r_i^2: Du rechnest für jede Masse, die Du hinzufügst, Masse mal Entfernung zur Drehachse zum Quadrat, und addierst das auf.

Kurz gesagt, Dir nützt ein größerer Radius. Wenn Du die Drehscheibe benutzt, um Metallgewichte an ihre Außenseiten zu bringen, oder wenn Du noch die großen schwarzen Räder von der Dampfwalze hast, dann bringen die den Roboter viel leichter zum kippen.