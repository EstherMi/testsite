---
layout: "image"
title: "Von hinten"
date: "2008-10-16T00:46:14"
picture: "einradroboter3.jpg"
weight: "3"
konstrukteure: 
- "Jan Käberich"
fotografen:
- "Käberich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- details/15994
imported:
- "2019"
_4images_image_id: "15994"
_4images_cat_id: "1452"
_4images_user_id: "521"
_4images_image_date: "2008-10-16T00:46:14"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15994 -->
Der Roboter misst seinen Winkel nicht mehr mit Abstandssensoren, sondern mit einem Beschleunigungssensor.