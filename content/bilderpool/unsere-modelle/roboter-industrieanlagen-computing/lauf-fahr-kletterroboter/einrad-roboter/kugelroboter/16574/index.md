---
layout: "image"
title: "Gesamtansicht"
date: "2008-12-10T16:45:52"
picture: "kugelroboter1.jpg"
weight: "15"
konstrukteure: 
- "Jan Käberich"
fotografen:
- "Käberich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- details/16574
imported:
- "2019"
_4images_image_id: "16574"
_4images_cat_id: "1500"
_4images_user_id: "521"
_4images_image_date: "2008-12-10T16:45:52"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16574 -->
Der Roboter soll auf einer Kugel(Durchmesser ca. 70cm) balancieren, indem er auf ihr herumfährt. Dazu muss er sofort in jede Richtung fahren können, ohne sich erst drehen zu müssen. Deshalb fährt der Roboter mit Allseitenrädern.
