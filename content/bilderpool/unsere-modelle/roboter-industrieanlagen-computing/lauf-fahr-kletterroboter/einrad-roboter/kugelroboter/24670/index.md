---
layout: "image"
title: "Kugelroboter - unten"
date: "2009-07-23T18:00:53"
picture: "kugelroboter4_2.jpg"
weight: "6"
konstrukteure: 
- "Jan Käberich"
fotografen:
- "Käberich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- details/24670
imported:
- "2019"
_4images_image_id: "24670"
_4images_cat_id: "1500"
_4images_user_id: "521"
_4images_image_date: "2009-07-23T18:00:53"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24670 -->
Die Motoren sind sehr kräftig und haben keine Probleme, den Roboter, der ca. 2kg wiegt, zu bewegen. In der Mitte erkennt man die Sensorplatine. Die Elektronik besteht aus insgesamt 5 Platinen: Sensorplatine, Hauptplatine, 3*Motortreiber. Auf der Hauptplatine befindet sich ein Mega32, auf den anderen jeweils ein Mega8. Übrigens sind diese gar nicht so einfach zu löten (TQFP, 1mm Lötzinn und eine viel zu große Lötspitze). Die Hauptplatine enthält den Regler und das RFM12-Funkmodul. Die Platinen "reden" über I²C. Die beiden freien Anschlüsse (auf der Sensorplatine unten rechts und am Elektronikkasten links) sind ISP-Schnittstellen. Die Motortreiber kann man nicht mehr programmieren.