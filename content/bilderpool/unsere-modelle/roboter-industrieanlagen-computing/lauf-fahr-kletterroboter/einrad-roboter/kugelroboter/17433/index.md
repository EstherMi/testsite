---
layout: "image"
title: "Winkelmessung"
date: "2009-02-18T21:52:03"
picture: "winkelmessung1.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- details/17433
imported:
- "2019"
_4images_image_id: "17433"
_4images_cat_id: "1500"
_4images_user_id: "521"
_4images_image_date: "2009-02-18T21:52:03"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17433 -->
Das sind die Werte der Sensoren. Rot und Blau sind die beiden Beschleunigungssensoren. Sie liefern den absoluten Winkel(0° entspricht ungefähr der Zahl 520). Grün und Gelb sind die Gyros. Der Gelbe gehört zum blauen Beschleunigungssensor(gleiche Achse), der Grüne zum Roten. Die Gyros messen die Winkelgeschwindigkeit.