---
layout: "image"
title: "Radantrieb"
date: "2008-12-10T16:45:53"
picture: "kugelroboter4.jpg"
weight: "18"
konstrukteure: 
- "Jan Käberich"
fotografen:
- "Käberich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- details/16577
imported:
- "2019"
_4images_image_id: "16577"
_4images_cat_id: "1500"
_4images_user_id: "521"
_4images_image_date: "2008-12-10T16:45:53"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16577 -->
Um möglichst wenig schräge Teile zu haben, sind nur die Räder angewinkelt, die Motoren sind schon wieder gerade.
Die Räder funktionieren genau wie ein normales Rad, nur das sie sich senkrecht zur normalen Drehrichtung frei drehen können.