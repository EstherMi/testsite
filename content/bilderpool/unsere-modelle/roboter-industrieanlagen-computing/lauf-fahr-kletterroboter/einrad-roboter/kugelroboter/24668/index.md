---
layout: "image"
title: "Kugelroboter - vorne rechts"
date: "2009-07-23T18:00:52"
picture: "kugelroboter2_2.jpg"
weight: "4"
konstrukteure: 
- "Jan Käberich"
fotografen:
- "Käberich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- details/24668
imported:
- "2019"
_4images_image_id: "24668"
_4images_cat_id: "1500"
_4images_user_id: "521"
_4images_image_date: "2009-07-23T18:00:52"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24668 -->
Auch der Aufbau ist fast gleich. Wieder ist er dreieckig, mit drei unabhängig voneinander angetriebenen Allseitenrädern. In dem Kasten vorne befindet sich die gesamte Elektronik. Nur die Sensoren sitzen in der Mitte des Roboters. Als Bedienelement hat er nur den Ein-Schalter. Alles andere (Reglereinstellungen, Balancieren starten/stoppen, ...) wird über die Fernsteuerung erledigt. Man kann sich z.B. während dem Balancieren die aktuellen Werte der Sensoren und den Absoluten Winkel auf dem Display anzeigen lassen. Alle Werte werden gleichzeitig auch an den PC weitergeleitet (RS232), sodass man genau sehen kann welche Auswirkungen Änderungen der Reglereinstellungen haben.