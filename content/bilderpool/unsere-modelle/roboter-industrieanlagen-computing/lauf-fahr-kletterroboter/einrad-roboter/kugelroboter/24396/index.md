---
layout: "image"
title: "Zini - links"
date: "2009-06-16T17:17:04"
picture: "IMG_2108_1.jpg"
weight: "9"
konstrukteure: 
- "Fitzcarraldo"
fotografen:
- "Fitzcarraldo"
keywords: ["Kugel", "Zini"]
uploadBy: "Fitzcarraldo"
license: "unknown"
legacy_id:
- details/24396
imported:
- "2019"
_4images_image_id: "24396"
_4images_cat_id: "1500"
_4images_user_id: "971"
_4images_image_date: "2009-06-16T17:17:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24396 -->
Zini's linke Seite