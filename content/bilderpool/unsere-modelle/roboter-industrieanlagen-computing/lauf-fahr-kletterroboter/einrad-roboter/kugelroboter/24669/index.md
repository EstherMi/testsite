---
layout: "image"
title: "Kugelroboter - oben"
date: "2009-07-23T18:00:53"
picture: "kugelroboter3_2.jpg"
weight: "5"
konstrukteure: 
- "Jan Käberich"
fotografen:
- "Käberich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- details/24669
imported:
- "2019"
_4images_image_id: "24669"
_4images_cat_id: "1500"
_4images_user_id: "521"
_4images_image_date: "2009-07-23T18:00:53"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24669 -->
Hier erkennt man sehr gut den dreieckigen Aufbau. Die Gewichtsverteilung ist nicht genau symetrisch, die Akkus sind etwas zu schwer. Deshalb hängt der Roboter immer etwas schief, wenn er balanciert.