---
layout: "comment"
hidden: true
title: "8536"
date: "2009-02-18T22:03:27"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Da müsste sich doch mit gleitenden Durchschnitten was machen lassen, wenn Du's elektronisch nicht dämpfen kannst. Also immer der Mittelwert der letzten 5, 10, oder wieviel auch immer Messungen.

Gruß,
Stefan