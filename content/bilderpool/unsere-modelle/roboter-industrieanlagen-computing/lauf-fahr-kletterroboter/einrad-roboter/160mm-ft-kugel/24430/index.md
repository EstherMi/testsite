---
layout: "image"
title: "160mm-FT-Kugel"
date: "2009-06-21T11:29:38"
picture: "2009-juni-Zaltbommel_012.jpg"
weight: "12"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/24430
imported:
- "2019"
_4images_image_id: "24430"
_4images_cat_id: "1674"
_4images_user_id: "22"
_4images_image_date: "2009-06-21T11:29:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24430 -->
160mm-FT-Kugel

In die Niederlände gibt es Kugel: 

http://www.hobbyhandboek.nl/detail.asp?cat=G1&subcat=G11&id=07604 

http://www.hobbyhandboek.nl/catalogus.asp 

( Es gibt jetzt auch 180mm Kugel ! )
-----------------------------------------------------------------------------------------------------

Seilrolle-18mm gibt es bei Opitec:    (artnr. 801433)

http://www.opitec.nl/cgi/ITMAIN%20%20%20%20%20%20%20317716013778?P_L=N&P_S=N&P_V=509065147-&P_P=ITSUCH&P_M=1000&P_PGM=ITSUCH&P_2=801433&subsearch=Zoeken


Gruss, 

Peter Damen 
Poederoyen NL