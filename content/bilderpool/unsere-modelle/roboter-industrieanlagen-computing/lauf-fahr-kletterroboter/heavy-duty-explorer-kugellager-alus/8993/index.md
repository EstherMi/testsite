---
layout: "image"
title: "Heavy-duty Explorer mit Kugellager und Alu's"
date: "2007-02-12T17:47:07"
picture: "Explorer-Eucalypta_012.jpg"
weight: "7"
konstrukteure: 
- "Peter Damen (Poederoyen Holland)"
fotografen:
- "Peter Damen (Poederoyen Holland)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/8993
imported:
- "2019"
_4images_image_id: "8993"
_4images_cat_id: "818"
_4images_user_id: "22"
_4images_image_date: "2007-02-12T17:47:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8993 -->
Heavy-duty Explorer mit Kugellager und Alu's