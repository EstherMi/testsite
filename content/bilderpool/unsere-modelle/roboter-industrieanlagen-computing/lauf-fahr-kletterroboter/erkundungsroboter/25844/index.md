---
layout: "image"
title: "Ansicht 8"
date: "2009-11-28T14:30:57"
picture: "FT-Bilder08.jpg"
weight: "8"
konstrukteure: 
- "Manu"
fotografen:
- "Manu"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "manude12"
license: "unknown"
legacy_id:
- details/25844
imported:
- "2019"
_4images_image_id: "25844"
_4images_cat_id: "1813"
_4images_user_id: "736"
_4images_image_date: "2009-11-28T14:30:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25844 -->
Kettenfahrzeug.
Über Kommentare, gerne auch Fragen würde ich mich freuen.