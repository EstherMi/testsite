---
layout: "image"
title: "Ansicht 2"
date: "2009-11-28T14:30:57"
picture: "FT-Bilder02.jpg"
weight: "2"
konstrukteure: 
- "Manu"
fotografen:
- "Manu"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "manude12"
license: "unknown"
legacy_id:
- details/25838
imported:
- "2019"
_4images_image_id: "25838"
_4images_cat_id: "1813"
_4images_user_id: "736"
_4images_image_date: "2009-11-28T14:30:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25838 -->
