---
layout: "image"
title: "Pixy I2C Kamera"
date: "2014-11-10T20:16:13"
picture: "erkundungsroboterundickameraobjekterkennung07.jpg"
weight: "7"
konstrukteure: 
- "Dirk W"
fotografen:
- "Dirk W"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/39796
imported:
- "2019"
_4images_image_id: "39796"
_4images_cat_id: "2985"
_4images_user_id: "2303"
_4images_image_date: "2014-11-10T20:16:13"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39796 -->
Hier die Kamera als Bauteil. Sie kann so für andere FT-Projekte gut verbaut werden.
