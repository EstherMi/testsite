---
layout: "image"
title: "PIXY I2C-Kamera hinten"
date: "2014-11-10T20:16:13"
picture: "erkundungsroboterundickameraobjekterkennung20.jpg"
weight: "20"
konstrukteure: 
- "Dirk W"
fotografen:
- "Dirk W"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/39809
imported:
- "2019"
_4images_image_id: "39809"
_4images_cat_id: "2985"
_4images_user_id: "2303"
_4images_image_date: "2014-11-10T20:16:13"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39809 -->
Hier seht ihr den 10-pol Anschluß.

Hierüber könnt ihr das I2C Signal auslesen. Die Stromversorung für die Kamera erfolgt hierüber ebenfalls.
(5 Volt vom I2C des RoboTX-Controller.)

Unten rechts gibt es noch Anschlüsse für 2 Servos (pan/tilt) und einen separaten 5-10 Volt Anschluß.

