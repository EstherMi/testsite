---
layout: "image"
title: "Erkundungsroboter + I2C Kamera von oben"
date: "2014-11-10T20:16:13"
picture: "erkundungsroboterundickameraobjekterkennung03.jpg"
weight: "3"
konstrukteure: 
- "Dirk W"
fotografen:
- "Dirk W"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/39792
imported:
- "2019"
_4images_image_id: "39792"
_4images_cat_id: "2985"
_4images_user_id: "2303"
_4images_image_date: "2014-11-10T20:16:13"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39792 -->
