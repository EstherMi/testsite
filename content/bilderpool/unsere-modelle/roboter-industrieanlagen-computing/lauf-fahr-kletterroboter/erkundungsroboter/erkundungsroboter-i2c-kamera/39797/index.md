---
layout: "image"
title: "Pixy I2C Kamera"
date: "2014-11-10T20:16:13"
picture: "erkundungsroboterundickameraobjekterkennung08.jpg"
weight: "8"
konstrukteure: 
- "Dirk W"
fotografen:
- "Dirk W"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/39797
imported:
- "2019"
_4images_image_id: "39797"
_4images_cat_id: "2985"
_4images_user_id: "2303"
_4images_image_date: "2014-11-10T20:16:13"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39797 -->
Das Kameragehäuse von hinten. 

Man sieht 2 Befestigungsschrauben für die I2C-Kamera.
Ein Loch für die Kabeldurchführung und den Mini-USB Anschluß der Kamera.