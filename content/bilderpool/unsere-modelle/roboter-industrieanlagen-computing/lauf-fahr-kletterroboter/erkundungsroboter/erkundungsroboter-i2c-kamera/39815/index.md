---
layout: "image"
title: "Robo Pro I2C Erkennung"
date: "2014-11-10T20:16:13"
picture: "erkundungsroboterundickameraobjekterkennung26.jpg"
weight: "26"
konstrukteure: 
- "Dirk W"
fotografen:
- "Dirk W"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/39815
imported:
- "2019"
_4images_image_id: "39815"
_4images_cat_id: "2985"
_4images_user_id: "2303"
_4images_image_date: "2014-11-10T20:16:13"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39815 -->
Hier mein Robo-Pro Programm für die Objekterkennung.
Version 1.0