---
layout: "image"
title: "PIXY I2C-Kamera"
date: "2014-11-10T20:16:13"
picture: "erkundungsroboterundickameraobjekterkennung18.jpg"
weight: "18"
konstrukteure: 
- "Dirk W"
fotografen:
- "Dirk W"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/39807
imported:
- "2019"
_4images_image_id: "39807"
_4images_cat_id: "2985"
_4images_user_id: "2303"
_4images_image_date: "2014-11-10T20:16:13"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39807 -->
Oben rechts ist der weisse "Modus-Knopf" zu sehen, zum manuellen Erlernen von Objekten.