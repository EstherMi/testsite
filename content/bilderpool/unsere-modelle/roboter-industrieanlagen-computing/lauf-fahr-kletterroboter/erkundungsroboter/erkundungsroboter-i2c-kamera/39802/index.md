---
layout: "image"
title: "Kassette von hinten"
date: "2014-11-10T20:16:13"
picture: "erkundungsroboterundickameraobjekterkennung13.jpg"
weight: "13"
konstrukteure: 
- "Dirk W"
fotografen:
- "Dirk W"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/39802
imported:
- "2019"
_4images_image_id: "39802"
_4images_cat_id: "2985"
_4images_user_id: "2303"
_4images_image_date: "2014-11-10T20:16:13"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39802 -->
Bohrung 16 mm für die Kabeldurchführung
und 2 Befestigungslöcher a 4 mm.