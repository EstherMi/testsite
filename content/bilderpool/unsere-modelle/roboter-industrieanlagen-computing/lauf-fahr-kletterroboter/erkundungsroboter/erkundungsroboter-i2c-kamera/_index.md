---
layout: "overview"
title: "Erkundungsroboter und I2C-Kamera Objekterkennung"
date: 2019-12-17T18:57:43+01:00
legacy_id:
- categories/2985
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2985 --> 
Ich suche schon seit langem nach einer I2C Kamera für den Robo TX Controller.

Bei "Charmed Labs" bin ich mit der PIXYY CMUcam5 fündig geworden. Sie hat eine 
Objekterkennung über I2C.

Gesagt, getan, ich habe mir eine Kamera bestellt und experimentiert. Hier das Ergebnis.
Ich habe ein Video für euch auf youtube gestellt.

https://www.youtube.com/channel/UC4xME8CsjPQxq7E6JSkrfuw


weitere Quellen:

Pixy Cam Bezug:
http://www.watterott.com/de/Pixy-CMUcam

Charmed Labs Hersteller:
http://charmedlabs.com/default/?cat=4

Pixy Website Software:
http://cmucam.org/projects/cmucam5/wiki

