---
layout: "image"
title: "PIXY Robot Erkundungsroboter Objekterkennung RoboPro"
date: "2014-11-15T19:29:09"
picture: "pixysoftware4.jpg"
weight: "4"
konstrukteure: 
- "Dirk W."
fotografen:
- "Dirk W."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/39821
imported:
- "2019"
_4images_image_id: "39821"
_4images_cat_id: "2987"
_4images_user_id: "2303"
_4images_image_date: "2014-11-15T19:29:09"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39821 -->
Hier dazu das passende RoboPro Programm
(stelle ich in den Download Bereich

Links seht ihr die Objekterkennung. Hier werden die Daten vom Pixy für ein Objekt ausgegeben.

Rechts seht ihr 3 Regler für die Schwellwerte von X und Y
(Y-Wert wird nicht gebraucht)

Die Schwellwerte braucht ihr, damit euere Motoren etwas langsamer reagieren.
So könnt ihr die Empfindlichkeit einstellen.

Ein Regler ist für die Nähe zu einem Objekt. Dazu lese ich Länge und Breite des Objekts aus.
Diese wird multipliziert und ich weiss wie groß das Objekt ist (Werte)
