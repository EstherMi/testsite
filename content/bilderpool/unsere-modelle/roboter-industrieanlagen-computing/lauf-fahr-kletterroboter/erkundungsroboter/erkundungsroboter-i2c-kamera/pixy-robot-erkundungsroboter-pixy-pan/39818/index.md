---
layout: "image"
title: "PIXY Pan/Tilt Objekterkennung"
date: "2014-11-15T19:29:09"
picture: "pixysoftware1.jpg"
weight: "1"
konstrukteure: 
- "Dirk W."
fotografen:
- "Dirk W."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/39818
imported:
- "2019"
_4images_image_id: "39818"
_4images_cat_id: "2987"
_4images_user_id: "2303"
_4images_image_date: "2014-11-15T19:29:09"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39818 -->
Hier mein Pixy Modell Pan/Tilt

M1= Links / Rechts
Endschalter = I1 und I3

M2 = Auf / Ab
Endschalter = I2 und I4

Kamera an Robo TX Ext 2 I2C
Pixy I2C = Hex 0x14 

Das Modell dazu findet ihr auch auf
https://www.youtube.com/watch?v=9Rk8K6oQ-xQ