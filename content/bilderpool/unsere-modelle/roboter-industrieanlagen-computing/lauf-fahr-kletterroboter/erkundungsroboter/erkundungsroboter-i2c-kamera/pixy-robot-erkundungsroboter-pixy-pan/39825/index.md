---
layout: "image"
title: "Pixy Objekt anlernen"
date: "2014-11-15T19:29:09"
picture: "pixysoftware8.jpg"
weight: "8"
konstrukteure: 
- "Dirk W."
fotografen:
- "Dirk W."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/39825
imported:
- "2019"
_4images_image_id: "39825"
_4images_cat_id: "2987"
_4images_user_id: "2303"
_4images_image_date: "2014-11-15T19:29:09"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39825 -->
Jetzt oben im Menü auf die Home Position klicken. Es werden die Daten an I2C übergeben.
Robo Tx Controller anschließen und los gehts.

Viel Spass beim Basteln wünscht
Dirk