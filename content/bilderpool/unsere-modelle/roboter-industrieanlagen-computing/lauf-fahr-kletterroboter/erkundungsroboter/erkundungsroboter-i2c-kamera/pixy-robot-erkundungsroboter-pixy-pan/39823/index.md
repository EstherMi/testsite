---
layout: "image"
title: "Pixy Objekt anlernen"
date: "2014-11-15T19:29:09"
picture: "pixysoftware6.jpg"
weight: "6"
konstrukteure: 
- "Dirk W."
fotografen:
- "Dirk W."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/39823
imported:
- "2019"
_4images_image_id: "39823"
_4images_cat_id: "2987"
_4images_user_id: "2303"
_4images_image_date: "2014-11-15T19:29:09"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39823 -->
Objekt suchen, am besten ein Objekt mit einer kräftigen Farbe.
Linse scharf stellen.