---
layout: "image"
title: "PIXY Pan/Tilt Objekterkennung RoboPro"
date: "2014-11-15T19:29:09"
picture: "pixysoftware2.jpg"
weight: "2"
konstrukteure: 
- "Dirk W."
fotografen:
- "Dirk W."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/39819
imported:
- "2019"
_4images_image_id: "39819"
_4images_cat_id: "2987"
_4images_user_id: "2303"
_4images_image_date: "2014-11-15T19:29:09"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39819 -->
Hier dazu das passende RoboPro Programm
(stelle ich in den Download Bereich)

Links seht ihr die Objekterkennung. Hier werden die Daten vom Pixy für ein Objekt ausgegeben.

Rechts seht ihr 2 Regler für die Schwellwerte von X und Y

Die Schwellwerte braucht ihr, damit euere Motoren etwas langsamer reagieren.
So könnt ihr die Empfindlichkeit einstellen.