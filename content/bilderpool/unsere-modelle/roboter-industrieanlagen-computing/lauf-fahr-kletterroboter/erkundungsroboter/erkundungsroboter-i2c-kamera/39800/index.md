---
layout: "image"
title: "Kamera Klarsichtdeckel"
date: "2014-11-10T20:16:13"
picture: "erkundungsroboterundickameraobjekterkennung11.jpg"
weight: "11"
konstrukteure: 
- "Dirk W"
fotografen:
- "Dirk W"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/39800
imported:
- "2019"
_4images_image_id: "39800"
_4images_cat_id: "2985"
_4images_user_id: "2303"
_4images_image_date: "2014-11-10T20:16:13"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39800 -->
