---
layout: "image"
title: "robomax18.jpg"
date: "2005-07-11T09:52:25"
picture: "img_3910_resize.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- details/4512
imported:
- "2019"
_4images_image_id: "4512"
_4images_cat_id: "281"
_4images_user_id: "120"
_4images_image_date: "2005-07-11T09:52:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4512 -->
Lenkung mit Endabschaltung im Detail. Wer erkennt den Schaltplan?