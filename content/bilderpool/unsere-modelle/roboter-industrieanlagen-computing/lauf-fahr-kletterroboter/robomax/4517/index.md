---
layout: "image"
title: "robomax23.jpg"
date: "2005-07-11T09:52:34"
picture: "img_4494_resize.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- details/4517
imported:
- "2019"
_4images_image_id: "4517"
_4images_cat_id: "281"
_4images_user_id: "120"
_4images_image_date: "2005-07-11T09:52:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4517 -->
Eine von zwei Achsen von oben. Gelenkt wird später gegenläufig um den großen Radstand etwas auszugleichen. Die Kardangelenke sind übrigens auf 4mm aufgebohrt und die Metallachsen straff eingesteckt. Eventuell muß ich die doch noch einkleben...