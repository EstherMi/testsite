---
layout: "image"
title: "robomax07.jpg"
date: "2004-11-23T19:48:21"
picture: "img_3697_resize.jpg"
weight: "23"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- details/3317
imported:
- "2019"
_4images_image_id: "3317"
_4images_cat_id: "281"
_4images_user_id: "120"
_4images_image_date: "2004-11-23T19:48:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3317 -->
Gut erkennbar die Metallachsen der 40'er. Die bisherigen steckbaren 10'er (außen dran) haben sich zu sehr verbogen :( Hebt so locker 1,5kg an extra Gewicht, aber viel zu langsam und die Zahnräder rutschen nach kurzer Zeit!