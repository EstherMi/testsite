---
layout: "comment"
hidden: true
title: "3807"
date: "2007-08-09T20:08:34"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
In der Tat. Der Link führt ins alte Forum, das im Oktober 2005 verschoben wurde. Hier gehts aber weiter:

http://www.fischertechnik.de/de/fanclub/forumalt/topic.asp?TOPIC_ID=2549&FORUM_ID=12&CAT_ID=2&Topic_Title=RoboMax&Forum_Title=Modellideen

Gruß,
Harald