---
layout: "image"
title: "robomax11.jpg"
date: "2005-07-11T09:52:24"
picture: "img_4484_resize.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- details/4505
imported:
- "2019"
_4images_image_id: "4505"
_4images_cat_id: "281"
_4images_user_id: "120"
_4images_image_date: "2005-07-11T09:52:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4505 -->
Der Antriebsstrang für die mittlere Stütze musste mit 2 Z20 versetzt werden, da ich so etwas kompakter in der Höhe bauen konnte.