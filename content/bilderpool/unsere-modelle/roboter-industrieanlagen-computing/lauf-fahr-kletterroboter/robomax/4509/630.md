---
layout: "comment"
hidden: true
title: "630"
date: "2005-07-14T21:14:09"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Na, ob das hält?Ich meine das Rast-Kardangelenk. Bei meinen Versuchen mit Allradantrieb war es immer so, dass sich das Gelenk ziemlich früh aufbiegt und dann auseinander geht. Und das sehr gerne bei eingeschlagener Lenkung; bei Geradeausfahrt geht's noch halbwegs.

Ich habe schon mit dem Gedanken gespielt, noch ein Stück Messingrohr darüber zu stülpen, aber zum Ausprobieren bin ich noch nicht gekommen.


Gruß, Harald