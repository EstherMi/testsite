---
layout: "image"
title: "Abstandssensoren"
date: "2005-10-30T09:56:14"
picture: "18-Abstandssensoren.jpg"
weight: "11"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/5140
imported:
- "2019"
_4images_image_id: "5140"
_4images_cat_id: "407"
_4images_user_id: "46"
_4images_image_date: "2005-10-30T09:56:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5140 -->
Im ersten Versuch soll der Roboter mit drei Abstandssensoren auskommen.

Zur Seite hin arbeiten zwei Sensoren mit Reichweite 4 bis 30 cm. Sie sind so angebracht, daß der Meßbereich genau mit der Außenkontur des Roboters beginnt.

Der obere Abstandssensor hat die Reichweite 10 bis 80 cm und 'blickt' mit 7,5 Grad Abwärtsneigung nach vorne. Auch sein Meßbereich beginnt erst ab der Außenkontur des Roboters.