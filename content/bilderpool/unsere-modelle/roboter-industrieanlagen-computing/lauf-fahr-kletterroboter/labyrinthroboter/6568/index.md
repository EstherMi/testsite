---
layout: "image"
title: "Platine für Radsensor"
date: "2006-06-24T19:38:10"
picture: "Radsensor.jpg"
weight: "5"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/6568
imported:
- "2019"
_4images_image_id: "6568"
_4images_cat_id: "407"
_4images_user_id: "46"
_4images_image_date: "2006-06-24T19:38:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6568 -->
So soll der Radsensor werden. Eine kleine Platine von 15x15 mm, die dann auf die kleine Bauplatte 15x15 aufgeklebt wird.