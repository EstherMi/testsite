---
layout: "image"
title: "Ausfahrt"
date: "2005-10-29T17:25:15"
picture: "04-Ausfahrt.jpg"
weight: "16"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/5126
imported:
- "2019"
_4images_image_id: "5126"
_4images_cat_id: "407"
_4images_user_id: "46"
_4images_image_date: "2005-10-29T17:25:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5126 -->
Kaum gewendet, könnte diese Maschine die Sackgasse vorwärts verlassen, als wäre nichts geschehen.