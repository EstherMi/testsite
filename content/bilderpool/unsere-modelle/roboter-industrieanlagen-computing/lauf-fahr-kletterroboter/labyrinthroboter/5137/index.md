---
layout: "image"
title: "Fast vollständige Ausrüstung"
date: "2005-10-29T17:25:37"
picture: "15-fast_komplett_gerstet.jpg"
weight: "27"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/5137
imported:
- "2019"
_4images_image_id: "5137"
_4images_cat_id: "407"
_4images_user_id: "46"
_4images_image_date: "2005-10-29T17:25:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5137 -->
Der Rechner bringt weitere Schnittstellen mit: Parallelport, RS232, Tastenmatrixeingang, VGA-Ausgang, Tastaturanschluß und LCD-Anschluß.

Was jetzt noch fehlt ist das Tastenfeld mit 6 Tasten, die Sensoren außenrum und die Steuerelektronik für die Motoren.
Sowie die ganze Programmierung.