---
layout: "comment"
hidden: true
title: "1220"
date: "2006-08-16T10:23:02"
uploadBy:
- "remadus"
license: "unknown"
imported:
- "2019"
---
Das Löten ist nicht unbedingt der schwierigste Teil, dann dafür habe ich eine ganz kleine Lötnadel und eine sehr große Lupe.

Der mit neuen Schrittmotoren ertüchtigte Plotter hat mit einem modifizierten Tuschestift das Layout plotten können. Plottauflösung ist 25 µm bei einer Leiterbahnenbreite von 0,4 mm. Damit bin ich zwischen den SMD-Lötpads des LS7183 hindurchgekommen. Hier waren 0,7 mm Platz.

Sowie dieser Prototyp ans funktionieren kommt (die Strichplatten sind noch das Problem), kommt noch ein weiterer Widerstand auf die Platine und eine Leiterbahn mehr.