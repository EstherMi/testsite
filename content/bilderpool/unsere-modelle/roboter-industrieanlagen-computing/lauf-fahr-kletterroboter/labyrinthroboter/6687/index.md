---
layout: "image"
title: "Radsensor ausgeführt"
date: "2006-08-15T21:04:46"
picture: "Sensor-neu.jpg"
weight: "3"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/6687
imported:
- "2019"
_4images_image_id: "6687"
_4images_cat_id: "407"
_4images_user_id: "46"
_4images_image_date: "2006-08-15T21:04:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6687 -->
So sieht er bestückt aus. Im Hintergrund habe ich noch ein Ritzel Z10 dabeigepackt. Daran kann man erkennen, daß die Sensorplatine die Größe eines Fingernagels hat.