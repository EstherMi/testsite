---
layout: "image"
title: "Vorläufige Meßachse"
date: "2006-08-15T21:04:46"
picture: "Meachse.jpg"
weight: "4"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/6688
imported:
- "2019"
_4images_image_id: "6688"
_4images_cat_id: "407"
_4images_user_id: "46"
_4images_image_date: "2006-08-15T21:04:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6688 -->
Da der zuvor gezeigte Radsensor noch nicht so tut, wie er soll, muß diese Meßachse erst einmal die Odometrie bewerkstelligen. Unter den gelben Platten befinden sich zwei Gleichstrommaschinen mit Drehgeber. Diese Motoren hatte ehemals auch die Wurfmaschine angetrieben. Dort ist ein Exemplar besser zu erkennen.