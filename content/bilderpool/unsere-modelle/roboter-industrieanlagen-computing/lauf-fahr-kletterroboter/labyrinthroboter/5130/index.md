---
layout: "image"
title: "Einfahrt in die Sackgasse 2"
date: "2005-10-29T17:25:15"
picture: "08-Einfahrt.jpg"
weight: "20"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/5130
imported:
- "2019"
_4images_image_id: "5130"
_4images_cat_id: "407"
_4images_user_id: "46"
_4images_image_date: "2005-10-29T17:25:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5130 -->
Naja, jetzt sieht man, wie groß das neue Chassis ist. Das wird noch ein Problem.