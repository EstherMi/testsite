---
layout: "image"
title: "Erster Versuch"
date: "2005-10-29T17:25:15"
picture: "01-Erster_Versuch.jpg"
weight: "13"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/5123
imported:
- "2019"
_4images_image_id: "5123"
_4images_cat_id: "407"
_4images_user_id: "46"
_4images_image_date: "2005-10-29T17:25:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5123 -->
Diese Version hatte ich ja schon auf der Convention gezeigt. Netter Versuch, aber das Chassis ist viel zu klein.