---
layout: "image"
title: "Labyrinthroboter"
date: "2006-06-02T11:33:39"
picture: "Labyrinthroboter1.jpg"
weight: "6"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/6403
imported:
- "2019"
_4images_image_id: "6403"
_4images_cat_id: "407"
_4images_user_id: "46"
_4images_image_date: "2006-06-02T11:33:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6403 -->
Hier der fast fertige Aufbau, bei dem nur noch die Radsensoren fehlen.

Oben: Display, dahinter Netzgeräte (+5V, +12V, -12V), daneben die PWM-Steuerung für den Fahrmotor von Thkais (Danke) und die Relaissteuerung für den Lenkmotor.

Mittelebene: vorne das Lenkzahnrad mit Potentiometer zur Lenkwinkelmessung, dahinter Rechner mit Multi-IO-Board, Stromverteiler und Außenanschlüsse für Tastatur, VGA-Monitor und Ladestrom.

Unten: Gelenkte Antriebsachse mit Schleifring für Stromübertragung auf den schwarzen Antriebsmotor, grauer Lenkmotor und dahinter der Akku.

Gesamtgewicht bis jetzt knapp drei Pfund