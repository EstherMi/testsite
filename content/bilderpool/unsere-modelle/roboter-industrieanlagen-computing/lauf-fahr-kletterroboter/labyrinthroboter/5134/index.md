---
layout: "image"
title: "Große Breite"
date: "2005-10-29T17:25:36"
picture: "12-Groe_Breite.jpg"
weight: "24"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/5134
imported:
- "2019"
_4images_image_id: "5134"
_4images_cat_id: "407"
_4images_user_id: "46"
_4images_image_date: "2005-10-29T17:25:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5134 -->
Mit einer Baubreite von jetzt immerhin gut 18 cm paßt diese Maschine noch ganz passabel durch das Labyrinth. Die dreieckige Erscheinungsform macht den Roboter kompakt.