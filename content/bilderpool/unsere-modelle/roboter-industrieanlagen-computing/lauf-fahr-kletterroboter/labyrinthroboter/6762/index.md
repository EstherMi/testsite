---
layout: "image"
title: "Sensor im Testbetrieb"
date: "2006-08-31T17:10:53"
picture: "Sensor_im_Betrieb.jpg"
weight: "2"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: ["Drehgeber", "Liniensensor", "Quadratur-Decoder", "Strichplatte"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/6762
imported:
- "2019"
_4images_image_id: "6762"
_4images_cat_id: "407"
_4images_user_id: "46"
_4images_image_date: "2006-08-31T17:10:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6762 -->
Der Sensor im Testbetrieb über einer 75 lpi Strichplatte.

Das rote Leuchten zeigt, daß der Sensor aktiv ist und einen kleinen Lichtpunkt auf die Strichplatte projiziert. Diese ist stark reflektierend, so daß der Sensor durch Umgebungslicht normalerweise nicht gestört wird.

Zusammen mit dem benachbarten Quadratur-Decoder gibt die Sensorbaugruppe mit dieser Strichplatte 300 Impulse / Zoll ab.