---
layout: "image"
title: "Teilstruktur der Robotersoftware"
date: "2007-04-23T14:12:53"
picture: "Agenten.jpg"
weight: "1"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/10147
imported:
- "2019"
_4images_image_id: "10147"
_4images_cat_id: "407"
_4images_user_id: "46"
_4images_image_date: "2007-04-23T14:12:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10147 -->
Das Buch "Mentopolis" von Marvin Minski, einem renommierten KI-Forscher hat mich auf eine womöglich machbare Fährte gebracht. Sein Modell von hierarchisch geordneten und interagierenden Agenten habe ich mal auf den Roboter umgemünzt.

Das Beispiel hier soll mal darstellen, wie mein Roboter eine Wand erkennen soll. Er soll ja nicht nur nicht mit der Wand zusammenstoßen, sondern sie in Daten speichern, um später noch zu wissen, wo er schon gewesen ist. Das macht der Agent "Sehen".

Dieser Agent bedient sich einer weitern Zahl von Agenten. Auf unterster Ebene befinden sich die Sensoragenten S1 bis S3 für die Entfernungssensoren, sowie LW für den Lenkwinkel und Akku für die Akkuspannung. Messen ist als Agent übergeordnet und enthält die Funktionen zum Auslesen der Sensordaten. Dann gibt es noch den Agenten Odo, das ist die Odometrie, die die Daten von den beiden Radsensoren bezieht. Ort ist eine Unterfunktion von Odo und berechnet aus den Radsensordaten den aktuellen Ort.

Die erste größere, übergeordnete Funktion hat der Agent "Norm". Dieser liest während einer Bewegung die Sensordaten und die Odometriedaten ein und rechnet sofort in ein feststehendes Koordinatensystem außerhalb des Roboters um (Weltkoordinaten). Daraus gewinnt er die Daten X,Y für den Ort, N für den Sensor, der die Basisdaten geliefert hat und ZP für den Zeitpunkt der Messung. (Er braucht die Zeit um Geschwindigkeiten und andere Veränderungen zu berechnen.) Diese Weltkoordinaten kommen in das Kurzzeitgedächtnis.

Nochmals übergeordnet ist "Regress". Er greift sich die Weltkoordinaten aus dem Kurzzeitgedächtnis, nachdem er §Norm" aufgefordert hatte, neue Koordinaten zu machen. Regress rechnet jetzt alle Punkte durch, um zu entscheiden, welche Punkte zu einer Geraden gehören. Diese Geraden kommen auch wieder in das Kurzzeitgedächtnis.

Zuletzt entscheidet "Sehen", ob die im Kurzzeitgedächtnis abgelegten Geraden einer Wand im Labyrinth entsprechen. Wird so eine Übereinstimmung gefunden, dann wird diese Wand in das Langzeitgedächtnis aufgenommen. Da ist tatsächlich eine Wand und die vergißt der Roboter nicht mehr. Wird das Programm unterbrochen, dann kann der Roboter später anhand des Langzeitgedächtnisses weitermachen.

Bleibt noch zu erwähnen, daß die beiden grünen Agenten M und L den Antriebsmotor fürs Fahren und den Lenkmotor bezeichnen.

Damit bin ich jetzt bei 5 Ebenen gelandet, nochmal drüber kommt die Ebene des Navigierens, der Ereigniserkennung und der Alarmbehandlung. Ein Alarm ist bereits eingezeichnet: Wenn beispielsweise irgendetwas einem Abstandssensor sehr nahekommt oder die Akkuspannung weggeht, dann werden Norm, Steuer und Sehen angehalten.

Nochwas: Das Langzeitgedächtnis und das Kurzzeitgedächtnis haben in sich selbst ebenfalls wieder Funktionen, wie z. B. das komplette drehen des Labyrinthfeldes 'im Kopf', wenn sich der Roboter dreht. Deshalb sind die beiden Kästen in dem Struktogramm ähnlich strukturiert zu sehen, wie "Sehen" selbst. Im Langzeitgedächtnis arbeiten jetzt schon 6 Agenten.

Ich weiß nicht, wo das noch hinführt.