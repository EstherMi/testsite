---
layout: "image"
title: "Labyrinthroboter - Lenksensor"
date: "2006-06-02T11:33:39"
picture: "Labyrinthroboter2.jpg"
weight: "7"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/6404
imported:
- "2019"
_4images_image_id: "6404"
_4images_cat_id: "407"
_4images_user_id: "46"
_4images_image_date: "2006-06-02T11:33:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6404 -->
Etwas näher dran: das Potentiometer zur Bestimmung des Lenkwinkels. Einfach, billig und ausreichend genau.