---
layout: "comment"
hidden: true
title: "4760"
date: "2007-12-14T16:56:00"
uploadBy:
- "remadus"
license: "unknown"
imported:
- "2019"
---
Ganz wichtig beim RoboInterface: den nicht gebrauchten Analogeingang auf Masse legen. Dann stört er den anderen Eingang nicht.

Bei dem Roboter oben ist das anders: da hängen die Abstandssensoren auf einem 12-AD-Wandlerboard, werden mehrmals ausgelesen, dann gemittelt. Die Genauigkeit ist voll ausgereizt.