---
layout: "image"
title: "Lenk- und Antriebsachse"
date: "2005-10-29T17:25:15"
picture: "06-Gelenkte_Antriebsachse.jpg"
weight: "18"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/5128
imported:
- "2019"
_4images_image_id: "5128"
_4images_cat_id: "407"
_4images_user_id: "46"
_4images_image_date: "2005-10-29T17:25:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5128 -->
Ein kleines Highlight ist die angetriebene Lenkachse. Das hat den Vorteil, daß es kompakt baut und daß die Achse beliebige Lenkwinkel realisieren kann.

Bleibt als Problem nur die Stromzufuhr zum Antriebsmotor.