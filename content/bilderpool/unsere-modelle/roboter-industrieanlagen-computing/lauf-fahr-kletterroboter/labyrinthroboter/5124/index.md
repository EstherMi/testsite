---
layout: "image"
title: "Einfahrt in die Sackgasse"
date: "2005-10-29T17:25:15"
picture: "02-Einfahrt_in_Sackgasse.jpg"
weight: "14"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/5124
imported:
- "2019"
_4images_image_id: "5124"
_4images_cat_id: "407"
_4images_user_id: "46"
_4images_image_date: "2005-10-29T17:25:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5124 -->
Das angedachte Labyrinth mit der Maschenweite 30 cm ist hier mal als Papier nachgebildet.

Der kleine Roboter hätte hier echte Vorteile, denn hier hat er bergeweise Platz.