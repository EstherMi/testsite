---
layout: "comment"
hidden: true
title: "16728"
date: "2012-04-05T22:21:45"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Oh, Mann, Fredy,
wo hast Du immer diese rattenscharfen Modellideen her?
Sensationell, das Teil. 
Würden die Walzenräder (http://www.ft-datenbank.de/details.php?ArticleVariantId=656bedff-28df-4182-b805-39cb088ffc99) nicht für einen "ruckefreieren" Lauf sorgen als die Seilrollen?
Gruß, Dirk