---
layout: "image"
title: "Rad"
date: "2012-03-30T13:45:04"
picture: "roboter3.jpg"
weight: "3"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/34708
imported:
- "2019"
_4images_image_id: "34708"
_4images_cat_id: "2563"
_4images_user_id: "453"
_4images_image_date: "2012-03-30T13:45:04"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34708 -->
Das Mecanum-Rad