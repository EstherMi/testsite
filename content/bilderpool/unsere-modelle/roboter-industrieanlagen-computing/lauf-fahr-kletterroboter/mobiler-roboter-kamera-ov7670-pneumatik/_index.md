---
layout: "overview"
title: "Mobiler Roboter mit Kamera OV7670 und Pneumatik-Greifer"
date: 2019-12-17T18:58:02+01:00
legacy_id:
- categories/3109
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3109 --> 
Mobiler Roboter mit Kettenfahrwerk und Kamera sucht, erkennt und greift eine mit einem roten Streifen markierte Tonne mit seinem Pneumatik-Greifer.

Video dazu gibt es hier:

https://www.youtube.com/watch?v=vDrwGIaLlIo

Auf dem Farbdisplay wird oben das originale Farbbild der Kamera dargestellt (RGB565-Format) und unten das aus dem YCbCr-Format nach einem Farbfilter auf rote Farbe gefilterte Bild. Die x-y-Bildkoordinaten der gefilterten Objekte werden dann im Originalbild oben an den Objekten angezeigt.

Steuerelektronik:

- AT90USB1287 Controller mit Farb-Display 2,2 Zoll (ILI9341) an der SPI-Schnittstelle 
- Omnivision OV7670 Kamera mit FIFO AL422
- Gabellichtschranken als Motor-Encoder