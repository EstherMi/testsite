---
layout: "overview"
title: "Fishbug - Der große Bruder"
date: 2019-12-17T18:57:59+01:00
legacy_id:
- categories/3063
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3063 --> 
Inspieriert durch lemkajen´s Bauten  habe ich mir gedacht:
Jetzt muss auch ein echter Fischer-Bug her :)

Die jetztige Konstruktion "geht erst richtig ab", wenn sie mit 16V "befeuert" wird.
Damit der Motor aber nicht "abraucht" wird also die Konstruktion noch verändert.
MEhr Nockenscheiben, Leichter, evtl. KAchsen als Beine usw. mal sehen wie weit wir kommen.....
