---
layout: "image"
title: "Fishbug von der Seite - Winkelsteine Nahaufnahme"
date: "2015-04-16T17:40:45"
picture: "fishbugdergrossebruder2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- details/40764
imported:
- "2019"
_4images_image_id: "40764"
_4images_cat_id: "3063"
_4images_user_id: "1677"
_4images_image_date: "2015-04-16T17:40:45"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40764 -->
