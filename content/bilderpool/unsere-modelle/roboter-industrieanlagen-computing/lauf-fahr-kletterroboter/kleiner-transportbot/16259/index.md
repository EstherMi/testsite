---
layout: "image"
title: "Seitenprofil"
date: "2008-11-12T21:53:43"
picture: "autonomerkleinroboter2.jpg"
weight: "2"
konstrukteure: 
- "flyingcat"
fotografen:
- "flyingcat"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- details/16259
imported:
- "2019"
_4images_image_id: "16259"
_4images_cat_id: "1466"
_4images_user_id: "853"
_4images_image_date: "2008-11-12T21:53:43"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16259 -->
Das Interface musste schräg eingebaut werden, damit das Stützrad noch frei drunter weg drehen kann,