---
layout: "image"
title: "Nutzlast"
date: "2008-11-12T21:53:44"
picture: "autonomerkleinroboter6.jpg"
weight: "6"
konstrukteure: 
- "flyingcat"
fotografen:
- "flyingcat"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- details/16263
imported:
- "2019"
_4images_image_id: "16263"
_4images_cat_id: "1466"
_4images_user_id: "853"
_4images_image_date: "2008-11-12T21:53:44"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16263 -->
So sieht das dann mit einem Förderband obendrauf aus.