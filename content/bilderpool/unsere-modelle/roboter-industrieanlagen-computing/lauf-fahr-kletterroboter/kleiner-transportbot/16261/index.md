---
layout: "image"
title: "Stützrad"
date: "2008-11-12T21:53:44"
picture: "autonomerkleinroboter4.jpg"
weight: "4"
konstrukteure: 
- "flyingcat"
fotografen:
- "flyingcat"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- details/16261
imported:
- "2019"
_4images_image_id: "16261"
_4images_cat_id: "1466"
_4images_user_id: "853"
_4images_image_date: "2008-11-12T21:53:44"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16261 -->
Läuft leichtgängig nach. Über die Impulsschalter am Antrieb ist einigermassen zielgerichtetes Navigieren möglich.