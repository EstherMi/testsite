---
layout: "overview"
title: "Kleiner Transportbot"
date: 2019-12-17T18:57:38+01:00
legacy_id:
- categories/1466
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1466 --> 
Dieses kleine Biest ist dafür gedacht, kleine Paletten gezielt durch die Gegend zu fahren. Zwei Räder werden getrennt angetrieben, ein Stützrad läuft nach. Dadurch ist Drehen auf der Stelle möglich. Akkupack und Interface sind mit an Bord.