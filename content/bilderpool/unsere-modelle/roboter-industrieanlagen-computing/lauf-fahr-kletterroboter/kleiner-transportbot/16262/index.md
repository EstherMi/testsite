---
layout: "image"
title: "Unterbau"
date: "2008-11-12T21:53:44"
picture: "autonomerkleinroboter5.jpg"
weight: "5"
konstrukteure: 
- "flyingcat"
fotografen:
- "flyingcat"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- details/16262
imported:
- "2019"
_4images_image_id: "16262"
_4images_cat_id: "1466"
_4images_user_id: "853"
_4images_image_date: "2008-11-12T21:53:44"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16262 -->
Ein Problem ist, dass die Antriebsräder nur sehr wenig Achse zur Verfügung haben. Ich kenne aber keine Möglichkeit, diesen Winkeltrieb mit längeren Achsen zu bauen, ohne das Fahrzeug insgesamt breiter werden zu lassen - Tipps zu diesem Punkt sind herzlich willkommen.