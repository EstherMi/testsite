---
layout: "image"
title: "Nutzlast 2"
date: "2008-11-12T21:53:44"
picture: "autonomerkleinroboter7.jpg"
weight: "7"
konstrukteure: 
- "flyingcat"
fotografen:
- "flyingcat"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- details/16264
imported:
- "2019"
_4images_image_id: "16264"
_4images_cat_id: "1466"
_4images_user_id: "853"
_4images_image_date: "2008-11-12T21:53:44"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16264 -->
Hier ist das Förderband besser zu erkennen. Kriegt das insgesamt jemand kleiner hin?