---
layout: "overview"
title: "Autonomes Fahrzeug"
date: 2019-12-17T19:09:01+01:00
legacy_id:
- categories/3366
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3366 --> 
Bei dem seit 2008 jährlich an der TU Braunschweig veranstalteten Carolo-Cup treten Studierendenteams mit selbst entwickelten autonomen Fahrzeugen im Maßstab 1:10 gegeneinander an. Die Fahrzeugmodelle müssen einem Staßenverlauf mit Kreuzungen und unterbrochenen Fahrbahnmarkierungen folgen und selbstständig einparken können (http://www.carolocup.de).

2017 startete erstmals ein Schülerteam (Team RoBoss: Johann Fox und Robin Pfannendörfer vom Bismarck-Gymnasium Karlsruhe) am Vorabend des Hauptwettbewerbs und bewältigte beide Disziplinen fehlerfrei. Hier zeigte sich mal wieder, wozu fischertechnik eigentlich fähig ist: Das Auto basiert fast vollständig auf fischertechnik - mit Ausnahme eines Lasers, eines Servo-Shields und zweier Spannungswandler, die Servo und Servo-Shield mit Spannung versorgen.

Ein Video des Wertungslaufs vom 06.02.2017 gibt es hier: https://youtu.be/K7pG3Md4btM