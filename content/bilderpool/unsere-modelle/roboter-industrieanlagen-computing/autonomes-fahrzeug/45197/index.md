---
layout: "image"
title: "Draufsicht"
date: "2017-02-12T20:43:14"
picture: "selbststaendigfahrendeseinparkendesautocarolocup3.jpg"
weight: "3"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "johafo"
license: "unknown"
legacy_id:
- details/45197
imported:
- "2019"
_4images_image_id: "45197"
_4images_cat_id: "3366"
_4images_user_id: "1292"
_4images_image_date: "2017-02-12T20:43:14"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45197 -->
Von oben ist der Kameraturm gut zu erkennen (rechts im Bild). Die Spur wird mit der Kamera erkannt, das funktioniert sehr gut und zuverlässig

Die Stromversorgung des TXT ist über einen der ft-Akkus realisiert. Zum Starten der Disziplin "Einparken" wird der blaue Taster mit der Aufschrift "Einparken" gedrückt, für die Disziplin "Rundkurs" wird der silberne Taster mit der Aufschrift "Rundkurs" gedrückt. Silber für den grauen Staßenbelag und blau, weil Parkplatzschilder immer blau sind :)

Die blaue LED direkt neben der Kamera blinkt, sobald sich das Auto im RC-Modus befindet (manueller Steuerungseingriff im Notfall). Fährt das Auto im Wettbewerb von der Straße oder begeht es einen sonstigen Regelverstoß, darf es manuell wieder zurück auf die Fahrbahn gesteuert werden. (Da wir ja fehlerfrei gefahren sind, sieht man das im Video nicht.)
Drei rote LEDs an der Hinterseite des Autos signalisieren einen Bremsvorgang; Spurwechsel werden mit Blinkern (auf jeder Seite zwei) angezeigt.