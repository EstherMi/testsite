---
layout: "image"
title: "Robo Bug"
date: "2009-09-18T20:32:47"
picture: "sm_roach3.jpg"
weight: "23"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["PCS", "BRAIN", "Robo", "Bug"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/24928
imported:
- "2019"
_4images_image_id: "24928"
_4images_cat_id: "125"
_4images_user_id: "585"
_4images_image_date: "2009-09-18T20:32:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24928 -->
This is a light-fleeing robot integrating the PCS BRAIN. This is an early version of the robot.