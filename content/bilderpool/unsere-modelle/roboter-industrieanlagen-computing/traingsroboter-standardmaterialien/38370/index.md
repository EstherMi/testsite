---
layout: "image"
title: "von Oben.."
date: "2014-02-23T18:04:06"
picture: "IMG_0005_2.jpg"
weight: "4"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/38370
imported:
- "2019"
_4images_image_id: "38370"
_4images_cat_id: "2849"
_4images_user_id: "1359"
_4images_image_date: "2014-02-23T18:04:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38370 -->
