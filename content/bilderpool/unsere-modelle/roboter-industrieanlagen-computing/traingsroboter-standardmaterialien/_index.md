---
layout: "overview"
title: "Traingsroboter mit Standardmaterialien"
date: 2019-12-17T19:08:04+01:00
legacy_id:
- categories/2849
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2849 --> 
Ein Versuch, den 80er-Jahre Trainingsroboter mit heute gut verfügbaren Teilen (bzw. ohne Spezialkomponenten - d.h. rote Dreiecksplatte, U-Getriebe mit langer Stange etc.) aufzubauen.