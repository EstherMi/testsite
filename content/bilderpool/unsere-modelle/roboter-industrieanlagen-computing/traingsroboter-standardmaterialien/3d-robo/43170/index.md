---
layout: "image"
title: "3D Robo"
date: "2016-03-21T13:33:37"
picture: "drobo1.jpg"
weight: "1"
konstrukteure: 
- "M. Bäter"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischmike"
license: "unknown"
legacy_id:
- details/43170
imported:
- "2019"
_4images_image_id: "43170"
_4images_cat_id: "3206"
_4images_user_id: "2291"
_4images_image_date: "2016-03-21T13:33:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43170 -->
Modell eines 3D Robo mit großer Greifzange für die Fischertechnik Kisten