---
layout: "image"
title: "Modifikation Dreiecksplatte"
date: "2014-02-16T21:02:14"
picture: "IMG_0002.jpg"
weight: "6"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/38308
imported:
- "2019"
_4images_image_id: "38308"
_4images_cat_id: "2849"
_4images_user_id: "1359"
_4images_image_date: "2014-02-16T21:02:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38308 -->
Idee von Stefan Falk - funktioniert - d.h. der Arm steht (ausser in extremen Positionen) immer ziemlich waagerecht :-)