---
layout: "comment"
hidden: true
title: "18751"
date: "2014-02-19T11:47:25"
uploadBy:
- "Lurchi"
license: "unknown"
imported:
- "2019"
---
Hallo,

zu:  "Eine I-Strebe 105 gibt es leider nicht, das war ft wohl zu nah an der X-Strebe 106 (...)."

So kann man das eigentlich nicht stehen lassen&#8230;:-), 
denn im Jahr 2013 kam der Kasten &#8222;Power-Machines&#8220; auf den Markt.
Im Kasten waren erstmals die geforderten I-Streben 105 mit Loch in Gelb (Art.-Nr. 145 987) enthalten.

Gruß

Lurchi