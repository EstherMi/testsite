---
layout: "overview"
title: "Model 30554: Computing CB 1985"
date: 2019-12-17T19:07:32+01:00
legacy_id:
- categories/1667
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1667 --> 
Dieser Baukasten ermöglicht den Bau von 10 verschiedenen Modellen zum erlernen der Ansteuerung mit einem parallel Interface.
Dem Baukasten liegt ein Coupon bei zum Bestellen der Software bei fischertechnik.