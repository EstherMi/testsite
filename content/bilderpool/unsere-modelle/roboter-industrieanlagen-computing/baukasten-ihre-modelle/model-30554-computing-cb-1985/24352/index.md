---
layout: "image"
title: "ersten Teil"
date: "2009-06-14T09:40:01"
picture: "DSC_2309.jpg"
weight: "10"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/24352
imported:
- "2019"
_4images_image_id: "24352"
_4images_cat_id: "1667"
_4images_user_id: "371"
_4images_image_date: "2009-06-14T09:40:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24352 -->
