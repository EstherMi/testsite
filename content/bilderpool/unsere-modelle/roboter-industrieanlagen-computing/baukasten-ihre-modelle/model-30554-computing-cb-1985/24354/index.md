---
layout: "image"
title: "robot arm."
date: "2009-06-14T09:40:01"
picture: "DSC_2315.jpg"
weight: "12"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/24354
imported:
- "2019"
_4images_image_id: "24354"
_4images_cat_id: "1667"
_4images_user_id: "371"
_4images_image_date: "2009-06-14T09:40:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24354 -->
