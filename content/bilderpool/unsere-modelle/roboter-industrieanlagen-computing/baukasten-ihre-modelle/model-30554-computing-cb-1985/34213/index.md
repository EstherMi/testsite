---
layout: "image"
title: "Teach-In fertig-01"
date: "2012-02-18T13:28:17"
picture: "TeachIn.jpg"
weight: "1"
konstrukteure: 
- "SkobyMobil"
fotografen:
- "SkobyMobil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "SkobyMobil"
license: "unknown"
legacy_id:
- details/34213
imported:
- "2019"
_4images_image_id: "34213"
_4images_cat_id: "1667"
_4images_user_id: "1407"
_4images_image_date: "2012-02-18T13:28:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34213 -->
so hat es wieder angefangen. Etwas wilde Verdrahtung. Gruß, Spaß und Dank, Andreas