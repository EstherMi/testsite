---
layout: "comment"
hidden: true
title: "10566"
date: "2010-01-19T20:50:07"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Hallo FT-Freunden,

Gibt es irgentwo eine 5V-pin zum Spannungsversorgung für Druck- und andere 5V-Sensoren beim Robo-Interface ?

Grüss,

Peter, Poederoyen NL