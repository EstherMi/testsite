---
layout: "image"
title: "Beschreibung der Interface Steckplätze"
date: "2009-04-10T17:32:48"
picture: "robo-interface.jpg"
weight: "1"
konstrukteure: 
- "Fischertechnik"
fotografen:
- "Bilbearbeitung : Lysander Blote"
keywords: ["Belegung", "der", "internen", "Pins"]
uploadBy: "Lysander Blote"
license: "unknown"
legacy_id:
- details/23659
imported:
- "2019"
_4images_image_id: "23659"
_4images_cat_id: "1614"
_4images_user_id: "805"
_4images_image_date: "2009-04-10T17:32:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23659 -->
So weit ich weiss ist die Belegung so :
1 : Schnittstelle für Robo RF Data Link
2 : Schnittstelle für ein (geplantes ) Internet-Modul ( http://www.fischertechnik.de/de/fanclub/forum/default.aspx?g=posts&t=3629 )
genaueres weiss ich leider auch nicht .