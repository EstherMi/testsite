---
layout: "image"
title: "Nahaufnahme der Sensoren!!"
date: "2007-09-16T00:04:24"
picture: "Die_neuen_Sensoren_nahaufnahme.jpg"
weight: "5"
konstrukteure: 
- "Fischertechnik"
fotografen:
- "Reiner Stähler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- details/11466
imported:
- "2019"
_4images_image_id: "11466"
_4images_cat_id: "827"
_4images_user_id: "426"
_4images_image_date: "2007-09-16T00:04:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11466 -->
