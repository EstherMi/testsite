---
layout: "image"
title: "Version 3 Bild 08"
date: "2011-10-15T17:38:05"
picture: "rcjrescuebot11.jpg"
weight: "11"
konstrukteure: 
- "André Mohr (mohr)"
fotografen:
- "André Mohr (mohr)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mohr"
license: "unknown"
legacy_id:
- details/33162
imported:
- "2019"
_4images_image_id: "33162"
_4images_cat_id: "2452"
_4images_user_id: "671"
_4images_image_date: "2011-10-15T17:38:05"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33162 -->
