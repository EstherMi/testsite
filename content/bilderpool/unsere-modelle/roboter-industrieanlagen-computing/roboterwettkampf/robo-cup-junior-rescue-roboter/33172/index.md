---
layout: "image"
title: "Version 3 Bild 18"
date: "2011-10-15T17:38:05"
picture: "rcjrescuebot21.jpg"
weight: "21"
konstrukteure: 
- "André Mohr (mohr)"
fotografen:
- "André Mohr (mohr)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mohr"
license: "unknown"
legacy_id:
- details/33172
imported:
- "2019"
_4images_image_id: "33172"
_4images_cat_id: "2452"
_4images_user_id: "671"
_4images_image_date: "2011-10-15T17:38:05"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33172 -->
Hier sieht man den Antrieb der Zange.