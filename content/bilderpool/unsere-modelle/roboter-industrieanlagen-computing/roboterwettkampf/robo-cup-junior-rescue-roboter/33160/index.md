---
layout: "image"
title: "Version 3 Bild 06"
date: "2011-10-15T17:38:05"
picture: "rcjrescuebot09.jpg"
weight: "9"
konstrukteure: 
- "André Mohr (mohr)"
fotografen:
- "André Mohr (mohr)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mohr"
license: "unknown"
legacy_id:
- details/33160
imported:
- "2019"
_4images_image_id: "33160"
_4images_cat_id: "2452"
_4images_user_id: "671"
_4images_image_date: "2011-10-15T17:38:05"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33160 -->
