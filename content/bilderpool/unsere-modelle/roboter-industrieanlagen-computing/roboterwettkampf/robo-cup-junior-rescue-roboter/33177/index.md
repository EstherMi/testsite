---
layout: "image"
title: "Version 3 Bild 23"
date: "2011-10-15T17:38:05"
picture: "rcjrescuebot26.jpg"
weight: "26"
konstrukteure: 
- "André Mohr (mohr)"
fotografen:
- "André Mohr (mohr)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mohr"
license: "unknown"
legacy_id:
- details/33177
imported:
- "2019"
_4images_image_id: "33177"
_4images_cat_id: "2452"
_4images_user_id: "671"
_4images_image_date: "2011-10-15T17:38:05"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33177 -->
Hier sieht man den Antrieb für den Hub.