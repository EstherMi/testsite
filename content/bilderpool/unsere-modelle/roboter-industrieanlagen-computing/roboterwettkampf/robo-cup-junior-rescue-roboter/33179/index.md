---
layout: "image"
title: "Version 3 Bild 25"
date: "2011-10-15T17:38:05"
picture: "rcjrescuebot28.jpg"
weight: "28"
konstrukteure: 
- "André Mohr (mohr)"
fotografen:
- "André Mohr (mohr)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mohr"
license: "unknown"
legacy_id:
- details/33179
imported:
- "2019"
_4images_image_id: "33179"
_4images_cat_id: "2452"
_4images_user_id: "671"
_4images_image_date: "2011-10-15T17:38:05"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33179 -->
Der "Kopf" ist unten auf der Platine, es handelt sich um einen ATmega2561.