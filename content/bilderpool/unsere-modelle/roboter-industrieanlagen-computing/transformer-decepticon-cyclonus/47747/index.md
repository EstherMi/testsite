---
layout: "image"
title: "Bauch geoffnet"
date: "2018-07-20T18:39:16"
picture: "transformer20.jpg"
weight: "20"
konstrukteure: 
- "jmn"
fotografen:
- "jmn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/47747
imported:
- "2019"
_4images_image_id: "47747"
_4images_cat_id: "3523"
_4images_user_id: "162"
_4images_image_date: "2018-07-20T18:39:16"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47747 -->
