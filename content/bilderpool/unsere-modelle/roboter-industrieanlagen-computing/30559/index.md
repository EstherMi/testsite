---
layout: "image"
title: "Training Roboter II"
date: "2011-05-14T22:07:37"
picture: "DSC00643.jpg"
weight: "6"
konstrukteure: 
- "Marspau"
fotografen:
- "Marspau"
keywords: ["Details", "of", "the", "three", "encoders", "moters.", "I", "have", "omited", "the", "lamps", "on", "the", "the", "new", "version", "because", "they", "draw", "to", "much", "current", "Details", "der", "drei", "Encoder", "moters.", "Ich", "habe", "die", "Lampen", "auf", "dem", "die", "neue", "Version", "weggelassen", "weil", "sie", "ziehen", "zu", "viel", "Strom"]
uploadBy: "marspau"
license: "unknown"
legacy_id:
- details/30559
imported:
- "2019"
_4images_image_id: "30559"
_4images_cat_id: "125"
_4images_user_id: "416"
_4images_image_date: "2011-05-14T22:07:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30559 -->
Details of the three encoders moters.
 I have omited the lamps on the the new version,because
they draw to much current