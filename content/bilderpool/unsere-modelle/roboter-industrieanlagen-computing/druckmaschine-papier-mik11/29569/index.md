---
layout: "image"
title: "Druckmaschine - Schneidmechanismus"
date: "2011-01-01T11:41:32"
picture: "P1000197.jpg"
weight: "6"
konstrukteure: 
- "MiK11"
fotografen:
- "MiK11"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MiK11"
license: "unknown"
legacy_id:
- details/29569
imported:
- "2019"
_4images_image_id: "29569"
_4images_cat_id: "3398"
_4images_user_id: "1258"
_4images_image_date: "2011-01-01T11:41:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29569 -->
