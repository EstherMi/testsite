---
layout: "image"
title: "Druckmaschine - Druckkopf"
date: "2011-01-01T11:41:33"
picture: "P1000200.jpg"
weight: "9"
konstrukteure: 
- "MiK11"
fotografen:
- "MiK11"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MiK11"
license: "unknown"
legacy_id:
- details/29572
imported:
- "2019"
_4images_image_id: "29572"
_4images_cat_id: "3398"
_4images_user_id: "1258"
_4images_image_date: "2011-01-01T11:41:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29572 -->
