---
layout: "image"
title: "Druckmaschine - Rückansicht"
date: "2011-01-01T11:41:33"
picture: "P1000199.jpg"
weight: "8"
konstrukteure: 
- "MiK11"
fotografen:
- "MiK11"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MiK11"
license: "unknown"
legacy_id:
- details/29571
imported:
- "2019"
_4images_image_id: "29571"
_4images_cat_id: "3398"
_4images_user_id: "1258"
_4images_image_date: "2011-01-01T11:41:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29571 -->
