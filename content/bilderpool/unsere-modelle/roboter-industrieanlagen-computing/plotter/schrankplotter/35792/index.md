---
layout: "image"
title: "Draufsicht"
date: "2012-10-06T21:10:39"
picture: "schrankplotter03.jpg"
weight: "4"
konstrukteure: 
- "da-kid"
fotografen:
- "da-kid"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "da-kid"
license: "unknown"
legacy_id:
- details/35792
imported:
- "2019"
_4images_image_id: "35792"
_4images_cat_id: "2660"
_4images_user_id: "1169"
_4images_image_date: "2012-10-06T21:10:39"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35792 -->
Die Zeichenfläche beträgt ca. 19,5 cm x 9cm