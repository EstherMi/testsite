---
layout: "image"
title: "Detail Y-Achse"
date: "2012-10-06T21:10:39"
picture: "schrankplotter07.jpg"
weight: "8"
konstrukteure: 
- "da-kid"
fotografen:
- "da-kid"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "da-kid"
license: "unknown"
legacy_id:
- details/35796
imported:
- "2019"
_4images_image_id: "35796"
_4images_cat_id: "2660"
_4images_user_id: "1169"
_4images_image_date: "2012-10-06T21:10:39"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35796 -->
Der komplette Y-Achsenantrieb ist zu sehen