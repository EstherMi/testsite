---
layout: "image"
title: "Bedienteil"
date: "2012-10-06T21:10:39"
picture: "schrankplotter02.jpg"
weight: "3"
konstrukteure: 
- "da-kid"
fotografen:
- "da-kid"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "da-kid"
license: "unknown"
legacy_id:
- details/35791
imported:
- "2019"
_4images_image_id: "35791"
_4images_cat_id: "2660"
_4images_user_id: "1169"
_4images_image_date: "2012-10-06T21:10:39"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35791 -->
Hier sieht man das Bedienteil.
Von links nach rechts:
Not-Aus-Schalter, vier Eingabetaster, Stromversorgung, zwei Schlüsselschalter