---
layout: "overview"
title: "Schrankplotter"
date: 2019-12-17T19:02:53+01:00
legacy_id:
- categories/2660
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2660 --> 
Das ist ein Plotter, der dafür gedacht ist, etwa postkatengroße Plots zu plotten.
Wie der Name vermuten lässt, ist er für den Einsatz in meinem Schrank gedacht.

Darum waren die Konstruktionsziele die folgenden:
 - Eine möglichst hohe Genauigkeit
 - Direkte Ansteuerung über ein Programm am Computer
 - Und ein möglichst kompaktes Ausmaß

Er ist für Zeichnungen im Quervormat gedacht.
Ausserdem ist er in zwei Module unterteilt:
 - Das Plotmodul
 - Und das Technikmodul