---
layout: "image"
title: "Plotter Schrittmotor"
date: "2010-04-23T19:51:01"
picture: "plotter3.jpg"
weight: "3"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- details/26979
imported:
- "2019"
_4images_image_id: "26979"
_4images_cat_id: "1939"
_4images_user_id: "1112"
_4images_image_date: "2010-04-23T19:51:01"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26979 -->
Das Ritzel von FT paßt genau auf diese Schrittmotoren drauf