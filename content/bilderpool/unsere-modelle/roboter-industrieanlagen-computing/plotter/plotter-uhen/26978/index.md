---
layout: "image"
title: "Plotter Stifthalter"
date: "2010-04-23T19:51:00"
picture: "plotter2.jpg"
weight: "2"
konstrukteure: 
- "uhen"
fotografen:
- "Udo Henkel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uhen"
license: "unknown"
legacy_id:
- details/26978
imported:
- "2019"
_4images_image_id: "26978"
_4images_cat_id: "1939"
_4images_user_id: "1112"
_4images_image_date: "2010-04-23T19:51:00"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26978 -->
Ich habe leider keinen neueren E-Magneten, so dass dieser hier manchmal zu schwach ist, den Stift herunterzudrücken.