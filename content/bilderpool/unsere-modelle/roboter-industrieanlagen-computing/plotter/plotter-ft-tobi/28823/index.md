---
layout: "image"
title: "Befestigung des Schreibkopfes"
date: "2010-10-02T23:55:11"
picture: "plotter4.jpg"
weight: "4"
konstrukteure: 
- "ft-tobi"
fotografen:
- "ft-tobi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-tobi"
license: "unknown"
legacy_id:
- details/28823
imported:
- "2019"
_4images_image_id: "28823"
_4images_cat_id: "2100"
_4images_user_id: "1109"
_4images_image_date: "2010-10-02T23:55:11"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28823 -->
Das Aluprofiel wird hier als eine Art Schiene genutzt.