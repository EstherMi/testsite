---
layout: "image"
title: "Der Antrieb der Y-Achse (2)"
date: "2010-10-02T23:55:11"
picture: "plotter3.jpg"
weight: "3"
konstrukteure: 
- "ft-tobi"
fotografen:
- "ft-tobi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-tobi"
license: "unknown"
legacy_id:
- details/28822
imported:
- "2019"
_4images_image_id: "28822"
_4images_cat_id: "2100"
_4images_user_id: "1109"
_4images_image_date: "2010-10-02T23:55:11"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28822 -->
Durch die Schnecke wird das Zahnrad, welches man im vorherigen Bild sieht, angetrieben.