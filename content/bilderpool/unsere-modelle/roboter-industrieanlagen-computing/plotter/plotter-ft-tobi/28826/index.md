---
layout: "image"
title: "(fast) perfekte Kreise"
date: "2010-10-02T23:55:12"
picture: "plotter7.jpg"
weight: "7"
konstrukteure: 
- "ft-tobi"
fotografen:
- "ft-tobi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-tobi"
license: "unknown"
legacy_id:
- details/28826
imported:
- "2019"
_4images_image_id: "28826"
_4images_cat_id: "2100"
_4images_user_id: "1109"
_4images_image_date: "2010-10-02T23:55:12"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28826 -->
Hier wird softwareseitig nachgeholfen, um Ungenauigkeiten auszugleichen: Immer dann, wenn eine Achse die Bewegungsrichtung ändert, wird die entsprechende Bewegung ein paar Schritte länger ausgeführt. Dabei habe ich jeweils leicht unterschiedliche Parameter verwendet.