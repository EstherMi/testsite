---
layout: "image"
title: "Neuer Plotter (von oben)"
date: "2014-05-02T16:40:08"
picture: "Plotter2.jpg"
weight: "2"
konstrukteure: 
- "majus"
fotografen:
- "majus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "majus"
license: "unknown"
legacy_id:
- details/38735
imported:
- "2019"
_4images_image_id: "38735"
_4images_cat_id: "2336"
_4images_user_id: "1239"
_4images_image_date: "2014-05-02T16:40:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38735 -->
Siehe zur Erklärung Bild vorher.