---
layout: "image"
title: "Plotter"
date: "2011-07-28T19:40:55"
picture: "DSCF7559.jpg"
weight: "4"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "majus"
license: "unknown"
legacy_id:
- details/31398
imported:
- "2019"
_4images_image_id: "31398"
_4images_cat_id: "2336"
_4images_user_id: "1239"
_4images_image_date: "2011-07-28T19:40:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31398 -->
Auf dem Bild sieht man den von mir beim Fischertechnik Fan-Club Tag ausgestellten Plotter. Er kann "I LOVE FT" oder ein rechteckiges Smily zeichnen, was man an der Steuerungseinheit rechts unten auswählen kann.