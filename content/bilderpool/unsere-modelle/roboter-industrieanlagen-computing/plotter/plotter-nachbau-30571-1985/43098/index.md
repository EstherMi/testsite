---
layout: "image"
title: "PLOT"
date: "2016-03-13T12:19:14"
picture: "plotternachbauaus08.jpg"
weight: "15"
konstrukteure: 
- "Lemkajen (JENS)"
fotografen:
- "Lemkajen (JENS)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/43098
imported:
- "2019"
_4images_image_id: "43098"
_4images_cat_id: "3202"
_4images_user_id: "1359"
_4images_image_date: "2016-03-13T12:19:14"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43098 -->
