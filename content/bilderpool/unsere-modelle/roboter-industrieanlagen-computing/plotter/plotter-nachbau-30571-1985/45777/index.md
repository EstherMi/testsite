---
layout: "image"
title: "Stift E Magnet"
date: "2017-04-16T22:07:55"
picture: "IMG_2327.jpg"
weight: "3"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/45777
imported:
- "2019"
_4images_image_id: "45777"
_4images_cat_id: "3202"
_4images_user_id: "1359"
_4images_image_date: "2017-04-16T22:07:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45777 -->
