---
layout: "image"
title: "Geamtansicht einer Achse"
date: "2007-04-30T13:23:02"
picture: "plotter1.jpg"
weight: "14"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/10233
imported:
- "2019"
_4images_image_id: "10233"
_4images_cat_id: "928"
_4images_user_id: "453"
_4images_image_date: "2007-04-30T13:23:02"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10233 -->
Hier sieht man die Achse von meinem Plotter, an den beiden Schwarzen Steinen in der mitte wird der Stift befestigt, der ist noch nicht ganz fertig deshalb habe ich ihn noch nicht geknipst. Unten wird der Schlitten mit einer Zahnstange geführt, oben ist eine Metallstange zu führung. Die Statikteile  um den Plotter sind nur da damit ich ein Halter für die eine Achse habe. Ob ich das Extensions Modul dort lasse und mit einem Flachbandkabell zum Interface gehen, welches die andere Achse steuern soll oder ob ich beide neben dem Plotter anbringe und mit einer Kabelkette oder so die Kabel führe, weiß ich noch nicht genau.