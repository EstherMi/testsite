---
layout: "image"
title: "Schreibfläche"
date: "2007-07-23T12:03:13"
picture: "plotter10.jpg"
weight: "10"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/11213
imported:
- "2019"
_4images_image_id: "11213"
_4images_cat_id: "928"
_4images_user_id: "453"
_4images_image_date: "2007-07-23T12:03:13"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11213 -->
