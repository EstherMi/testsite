---
layout: "image"
title: "Papiereinzugsrollen"
date: "2007-07-23T12:07:00"
picture: "plotter12.jpg"
weight: "12"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/11215
imported:
- "2019"
_4images_image_id: "11215"
_4images_cat_id: "928"
_4images_user_id: "453"
_4images_image_date: "2007-07-23T12:07:00"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11215 -->
Auf diesem Bild kann man erkennen, wie die Rollen angeodnet sind. Die vordere Stange und die beiden Hinteren Stangen dienen nur dazu das Papier ohne knicke durch den Plotter zuleiten.