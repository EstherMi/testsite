---
layout: "image"
title: "Endschalter 2"
date: "2015-08-03T13:01:55"
picture: "cncfraese7.jpg"
weight: "7"
konstrukteure: 
- "Jori"
fotografen:
- "Jori"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jori"
license: "unknown"
legacy_id:
- details/41694
imported:
- "2019"
_4images_image_id: "41694"
_4images_cat_id: "3107"
_4images_user_id: "2465"
_4images_image_date: "2015-08-03T13:01:55"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41694 -->
Der zweite Endschalter wird von einem BS15(hinter der roten Platte rechts) betätigt