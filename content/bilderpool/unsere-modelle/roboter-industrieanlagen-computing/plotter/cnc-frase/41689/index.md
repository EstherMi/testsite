---
layout: "image"
title: "Antrieb x-Achse"
date: "2015-08-03T13:01:55"
picture: "cncfraese2.jpg"
weight: "2"
konstrukteure: 
- "Jori"
fotografen:
- "Jori"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jori"
license: "unknown"
legacy_id:
- details/41689
imported:
- "2019"
_4images_image_id: "41689"
_4images_cat_id: "3107"
_4images_user_id: "2465"
_4images_image_date: "2015-08-03T13:01:55"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41689 -->
Der Antrieb der x-Achse: Ein Powermotor 1:50  treibt über eine Kette beide Schnecken an