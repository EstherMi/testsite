---
layout: "image"
title: "Der Gummizug"
date: "2010-03-31T20:00:31"
picture: "plotterseb9.jpg"
weight: "9"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/26860
imported:
- "2019"
_4images_image_id: "26860"
_4images_cat_id: "1922"
_4images_user_id: "558"
_4images_image_date: "2010-03-31T20:00:31"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26860 -->
