---
layout: "comment"
hidden: true
title: "14533"
date: "2011-06-30T19:21:10"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ich glaube, ein großer Teil der Toleranzen kommt von der zu langen Stiftführung. Da hat der Stift viel zu viel Gelegenheit, zu wackeln oder sich zu biegen. Der muss möglichst kurz über dem Zeichenboden und möglichst fest und starr gepackt werden.

Gruß,
Stefan