---
layout: "image"
title: "09 Portalroboter Endschalter Z-Achse"
date: "2005-05-11T16:15:30"
picture: "09-Endschalter_Z-Achse.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/4133
imported:
- "2019"
_4images_image_id: "4133"
_4images_cat_id: "351"
_4images_user_id: "46"
_4images_image_date: "2005-05-11T16:15:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4133 -->
Mangels Bauraum im Z-Achsenschlitte (innen drin ist der eng gepackt wie ein Hühnerei) mußten die Endschalter der Z-Achse auswandern. Hier hilft der gleiche Trick, wie er sich schon bei dem Hexapod bewährt hat: ein Kabel wird an der Zugschnur befestigt und der elektrische Kontakt mit Metallwinkeln abgeleitet. Diese Winkel lehnen sich leicht an die Schnur. Die Berührung des angebundenen Kupferkabels mit den Metallwinkeln gibt ein ortsgenaues Signal.