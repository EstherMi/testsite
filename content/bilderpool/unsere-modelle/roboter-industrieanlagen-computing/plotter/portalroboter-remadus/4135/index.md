---
layout: "image"
title: "11 Portalroboter Antrieb Y-Achse"
date: "2005-05-11T16:15:30"
picture: "11-Antriebswelle_Y-Achse.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/4135
imported:
- "2019"
_4images_image_id: "4135"
_4images_cat_id: "351"
_4images_user_id: "46"
_4images_image_date: "2005-05-11T16:15:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4135 -->
Hier der Überblick von hinten. Die durchgehende Welle im Antrieb der Y-Achse schafft eine ordentliche Zwangs-Parallelführung. Statisch ist das nicht ganz so korrekt, aber die Steifigkeit des Modells ist sonst nicht groß genug.