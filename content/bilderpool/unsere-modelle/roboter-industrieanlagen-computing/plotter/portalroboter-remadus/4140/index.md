---
layout: "image"
title: "16 Portalroboter Anfahrrampe"
date: "2005-05-11T19:12:34"
picture: "Diagramm.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/4140
imported:
- "2019"
_4images_image_id: "4140"
_4images_cat_id: "351"
_4images_user_id: "46"
_4images_image_date: "2005-05-11T19:12:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4140 -->
Hier die Diagrammdarstellung der Anfahrrampe mit minimalem Ruck. Die Darstellung ist normiert, so daß nur die qualitativen Verläufe zählen.

Die Pausenzeiten von Schritt zu Schritt des Motors rechnet sich beispielsweise für die Y-Achse so: 
Pause = 28000 * (tanhyp(X)+1) + 5000 
X läuft von 0,5 bis -3,5. Der Wert Pause beträgt dann anfänglich ca. 47000 und fällt auf ca. 6000 ab. Der Wert Pause wird in einer for-to-Schleife ohne Inhalt einfach durchgezählt.