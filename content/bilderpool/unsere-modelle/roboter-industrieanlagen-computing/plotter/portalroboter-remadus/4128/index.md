---
layout: "image"
title: "04 Portalroboter Portal vorgefahren"
date: "2005-05-10T23:27:35"
picture: "04-Portal_vorgefahren2.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/4128
imported:
- "2019"
_4images_image_id: "4128"
_4images_cat_id: "351"
_4images_user_id: "46"
_4images_image_date: "2005-05-10T23:27:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4128 -->
Nach 35 cm Strecke steht das Portal vorne am Endschalter. Dadurch, daß die Schrittmotore jetzt richtige Anfahr- und Bremsrampen fahren, lassen sie sich bis einige kHz beaufschlagen, so daß das Portal in gut 4 Sekunden vorne ist. Nach rechts sind auch die mitgeführten Kabel zu sehen.