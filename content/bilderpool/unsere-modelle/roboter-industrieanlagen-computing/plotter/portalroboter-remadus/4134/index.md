---
layout: "image"
title: "10 Portalroboter Antrieb Y-Achse"
date: "2005-05-11T16:15:30"
picture: "10-Antrieb_Y-Achse.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/4134
imported:
- "2019"
_4images_image_id: "4134"
_4images_cat_id: "351"
_4images_user_id: "46"
_4images_image_date: "2005-05-11T16:15:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4134 -->
Einfach und hochwirksam. Ein Schrittmotor mit einer Auflösung von 200 Schritt/Umdrehung (wie alle anderen Motore auch) ist 1:3 auf die Abtriebswelle untersetzt.

Im Hintergrund: der große Kasten mit der Elektronik drin, in den eine Menge Kabel rein und rausmüssen.