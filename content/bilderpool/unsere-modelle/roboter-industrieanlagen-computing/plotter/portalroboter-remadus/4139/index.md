---
layout: "image"
title: "15 Portalroboter Pyramide"
date: "2005-05-11T16:15:37"
picture: "15-Pyramide.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/4139
imported:
- "2019"
_4images_image_id: "4139"
_4images_cat_id: "351"
_4images_user_id: "46"
_4images_image_date: "2005-05-11T16:15:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4139 -->
Geschafft! Die Beschleunigungs- und Bremsrampen machen ein sorgfältiges Manovrieren möglich, ohne unterwegs zu trödeln.