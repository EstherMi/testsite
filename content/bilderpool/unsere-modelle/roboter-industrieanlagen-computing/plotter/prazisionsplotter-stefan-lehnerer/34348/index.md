---
layout: "image"
title: "Präzisionsplotter 3"
date: "2012-02-21T18:03:03"
picture: "praezisionsplotter3.jpg"
weight: "13"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/34348
imported:
- "2019"
_4images_image_id: "34348"
_4images_cat_id: "2540"
_4images_user_id: "502"
_4images_image_date: "2012-02-21T18:03:03"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34348 -->
Führung der X-Achse