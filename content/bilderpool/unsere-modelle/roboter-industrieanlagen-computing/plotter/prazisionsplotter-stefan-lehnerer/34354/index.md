---
layout: "image"
title: "Plot 3"
date: "2012-02-22T16:06:50"
picture: "plot2.jpg"
weight: "8"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/34354
imported:
- "2019"
_4images_image_id: "34354"
_4images_cat_id: "2540"
_4images_user_id: "502"
_4images_image_date: "2012-02-22T16:06:50"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34354 -->
Durchmesse ca. 5cm