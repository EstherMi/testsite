---
layout: "image"
title: "Plot 5"
date: "2012-02-26T14:42:24"
picture: "plot1_2.jpg"
weight: "3"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/34442
imported:
- "2019"
_4images_image_id: "34442"
_4images_cat_id: "2540"
_4images_user_id: "502"
_4images_image_date: "2012-02-26T14:42:24"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34442 -->
