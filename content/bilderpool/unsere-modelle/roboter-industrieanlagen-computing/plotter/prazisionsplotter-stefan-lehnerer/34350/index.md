---
layout: "image"
title: "Präzisionsplotter 5"
date: "2012-02-21T18:03:03"
picture: "praezisionsplotter5.jpg"
weight: "15"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/34350
imported:
- "2019"
_4images_image_id: "34350"
_4images_cat_id: "2540"
_4images_user_id: "502"
_4images_image_date: "2012-02-21T18:03:03"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34350 -->
Der Stift wird durch die zwei Federn hinuntergedrückt und durch das Seil wieder nach oben gezogen.