---
layout: "image"
title: "Plotter 8"
date: "2006-11-20T19:01:26"
picture: "plotterneueversion4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/7515
imported:
- "2019"
_4images_image_id: "7515"
_4images_cat_id: "708"
_4images_user_id: "502"
_4images_image_date: "2006-11-20T19:01:26"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7515 -->
Beide Antriebe.