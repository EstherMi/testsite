---
layout: "image"
title: "Plotter"
date: "2006-11-20T19:01:26"
picture: "plotterueberarbeitet2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/7517
imported:
- "2019"
_4images_image_id: "7517"
_4images_cat_id: "709"
_4images_user_id: "502"
_4images_image_date: "2006-11-20T19:01:26"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7517 -->
Nochmal überarbeitet