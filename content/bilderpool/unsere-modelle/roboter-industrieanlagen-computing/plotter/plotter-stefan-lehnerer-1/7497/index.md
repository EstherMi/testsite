---
layout: "image"
title: "Plotter3"
date: "2006-11-19T16:27:23"
picture: "plotter3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/7497
imported:
- "2019"
_4images_image_id: "7497"
_4images_cat_id: "706"
_4images_user_id: "502"
_4images_image_date: "2006-11-19T16:27:23"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7497 -->
