---
layout: "image"
title: "Plotter1"
date: "2006-11-19T16:27:23"
picture: "plotter1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/7495
imported:
- "2019"
_4images_image_id: "7495"
_4images_cat_id: "706"
_4images_user_id: "502"
_4images_image_date: "2006-11-19T16:27:23"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7495 -->
etwas klein geworden. Hatte nicht genug Teile. Blatt mit ersten Versuchen.