---
layout: "image"
title: "Portalroboter3"
date: "2006-11-04T21:25:07"
picture: "IMG_0173.jpg"
weight: "4"
konstrukteure: 
- "Frank Walter"
fotografen:
- "Frank Walter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "The Specialist"
license: "unknown"
legacy_id:
- details/7316
imported:
- "2019"
_4images_image_id: "7316"
_4images_cat_id: "1600"
_4images_user_id: "496"
_4images_image_date: "2006-11-04T21:25:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7316 -->
