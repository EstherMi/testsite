---
layout: "image"
title: "Stifthalterung von hinten"
date: "2005-03-29T00:25:30"
picture: "Plotter_006.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/3916
imported:
- "2019"
_4images_image_id: "3916"
_4images_cat_id: "338"
_4images_user_id: "104"
_4images_image_date: "2005-03-29T00:25:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3916 -->
Der Elektromagnet wird nicht etwa ein- und ausgeschaltet, sondern umgepolt, um das Auf und Ab des Stiftes zu realisieren. Auf diese Weise gibt es schön viel Hub (damit verlieren Wellen im Papier ihre Schrecken) und der Stift wird aktiv aufs Papier gedrückt. Die Stromversorgung des Magneten erfolgt über die beiden parallel zur Längsachse des Papiers laufenden ft-Stahlachsen. Leider fehlt mir noch ein sechster Scheckenbaustein zwecks Spielausgleich, aber das will ich noch via Software machen.