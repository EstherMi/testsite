---
layout: "image"
title: "Prototyp-Zeichnung 1"
date: "2005-03-29T09:54:46"
picture: "Plotter_009.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/3919
imported:
- "2019"
_4images_image_id: "3919"
_4images_cat_id: "338"
_4images_user_id: "104"
_4images_image_date: "2005-03-29T09:54:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3919 -->
Eine der Funktionen der Software ist das Zeichnen eines regelmäßigen N-Ecks, bei dem jeder Punkt mit jedem verbunden ist. Diese Zeichnung stammt von einer etwas früheren Entwicklungsstufe. Ich bin während der Erstellung mit der Hand an den Stift gekommen (deshalb der Schlenker), und der Stifthub war noch zu klein (deshalb der überflüssige Strich). Auch der Spielausgleich, vor allem in Richtung der langen Papierseite, fehlt noch deutlich.