---
layout: "image"
title: "Prototyp-Zeichnung 2"
date: "2005-03-29T09:54:46"
picture: "Plotter_010.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/3920
imported:
- "2019"
_4images_image_id: "3920"
_4images_cat_id: "338"
_4images_user_id: "104"
_4images_image_date: "2005-03-29T09:54:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3920 -->
Ein zweites Feature der Software ist das zeichen einer (fast) beliebigen mathematischen Funktion. Die sichtbare Zackenbreite bei den sehr steilen Flanken sind die gut 0,2 mm Auflösung.