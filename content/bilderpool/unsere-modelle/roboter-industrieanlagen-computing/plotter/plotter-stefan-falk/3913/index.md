---
layout: "image"
title: "Antrieb kurze Achse"
date: "2005-03-29T00:25:17"
picture: "Plotter_003.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/3913
imported:
- "2019"
_4images_image_id: "3913"
_4images_cat_id: "338"
_4images_user_id: "104"
_4images_image_date: "2005-03-29T00:25:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3913 -->
Minimot mit Impulsrad und normalem Taster, 1:3 untersetzt und ab auf die Schnecke mit 5 mm Schneckenweite. Das ergibt 8 abtastbare Impulse pro Umdrehung, durch 3, somit 24 pro Schneckenumdrehung, also 5 mm / 24 = etwas über 0,2 mm Auflösung. Das ist für die Minimots ein ganz brauchbarer Kompromiss zwischen Genauigkeit und Geschwindigkeit.