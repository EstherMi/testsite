---
layout: "comment"
hidden: true
title: "9683"
date: "2009-07-30T22:28:40"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Tach Ingo,

das lief durchaus sauber, weil es ja softwaremäßig wie Schrittmotoren behandelt wurde. Immer eine Änderung eines Impulstasters (egal ob 0-1 oder 1-0) ist ein Schritt, und da gab es nur dann mal Schrittfehler, wenn der Bildschirmschoner meinte, mal eben etwas die anderen Prozesse anzuhalten.

Die .net-Software war von mir, aber die Anbindung fürs Interface an .net war die von UMueller, ja.

Gruß,
Stefan