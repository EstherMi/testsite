---
layout: "image"
title: "Gesamtansicht 2"
date: "2005-03-29T00:25:16"
picture: "Plotter_002.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/3912
imported:
- "2019"
_4images_image_id: "3912"
_4images_cat_id: "338"
_4images_user_id: "104"
_4images_image_date: "2005-03-29T00:25:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3912 -->
Er hängt an einem alten Parallel-Interface, weil ich mein Robo-Interface noch nicht schön von .NET aus ansteuern kann. Die Mechanik ist recht simpel, funktioniert aber gut.