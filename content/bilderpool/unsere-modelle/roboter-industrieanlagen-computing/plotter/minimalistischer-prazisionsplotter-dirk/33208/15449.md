---
layout: "comment"
hidden: true
title: "15449"
date: "2011-10-16T23:19:51"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Tja... Stefan... Ein bisserl herumjustieren musste ich - ich lasse den Motor einfach eine definierte Zeit (0,15 sec) laufen, dann sitzt er perfekt (bis die Mine heruntergeschrieben ist ;-). Nach oben dienen zwei Steckerbuchsen 21 (35307) aus den em-Kästen als Endlagenschalter: Sobald sie sich berühren, stoppt der Motor (ein Taster war mir an der Stelle zu klobig).

Plots kommen. Ich bastele noch an hübsch herausfordernden Monster-Bildern (viele Kreise, die brauchen ein wenig Zeit), an denen man die Genauigkeit gut erkennen kann.

Gruß, Dirk