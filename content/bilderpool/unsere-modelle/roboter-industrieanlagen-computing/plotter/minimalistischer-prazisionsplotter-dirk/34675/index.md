---
layout: "image"
title: "Verbesserter Schreibkopf (mit Seilwindengestell)"
date: "2012-03-24T18:41:26"
picture: "Schreibkopf_mit_Seilwindengestell_schrg.jpg"
weight: "2"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/34675
imported:
- "2019"
_4images_image_id: "34675"
_4images_cat_id: "2456"
_4images_user_id: "1126"
_4images_image_date: "2012-03-24T18:41:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34675 -->
Am zuverlässigsten arbeitet diese Version des Schreibkopfes. Die Absenkung des Schreibstifts erfolgt durch eine Schaltscheibe (die auch den Endlagentaster bedient), angehoben wird er durch eine Rückstellfeder auf der Metallachse zwischen den Rollenlagern. (Eine ähnliche Lösung findet sich im "Experimentierbuch Profi Computing" von 1991.)