---
layout: "comment"
hidden: true
title: "16667"
date: "2012-03-25T17:15:29"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Hallo Harald,
die Parallelführung ist nach der bewährten "Harald-Methode" entstanden: alle Teile, durch die man eine Stange stecken kann, nebeneinandergelegt und so lange kombiniert, bis die stabilste und kleinstmögliche Kombination vor mir lag...
Gruß, Dirk