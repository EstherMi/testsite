---
layout: "overview"
title: "Minimalistischer Präzisionsplotter (Dirk Fox)"
date: 2019-12-17T19:02:45+01:00
legacy_id:
- categories/2456
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2456 --> 
Plotter mit Standard-ft-Teilen (Ansteuerung über TX mit Encoder-Motoren).
Ansteuerung mit einer Genauigkeit von 0,02 mm