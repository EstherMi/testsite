---
layout: "image"
title: "Einzelteilübersicht, Designer-Datei und HP-GL-Steuerprogramm"
date: "2012-03-31T23:31:59"
picture: "Einzelteilbersicht_HP-GL-Plotter_v1.1.jpg"
weight: "1"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/34721
imported:
- "2019"
_4images_image_id: "34721"
_4images_cat_id: "2456"
_4images_user_id: "1126"
_4images_image_date: "2012-03-31T23:31:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34721 -->
Der Plotter kommt mit insgesamt 170 (unmodifizierten) fischertechnik-Bauteilen aus; Bauzeit ca. 30-40 Minuten. Eine 3D-Konstruktionsdatei für den ft-Designer findet sich im Download-Bereich (http://www.ftcommunity.de/data/downloads/bauanleitungen/konstruktion_hpglplotter_v1.1.zip), das Robo Pro-Steuerprogramm ebenfalls (http://www.ftcommunity.de/data/downloads/robopro/steuerprogramm_hpglplotter_v1.1.zip). Mehr zum HP-GL-Steuerprogramm in ft:pedia 1/2012.