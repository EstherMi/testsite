---
layout: "image"
title: "Antrieb und Endlagentaster II"
date: "2011-10-16T23:09:12"
picture: "minimalistischerpraezisionsplotterdirkfox4.jpg"
weight: "12"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/33206
imported:
- "2019"
_4images_image_id: "33206"
_4images_cat_id: "2456"
_4images_user_id: "1126"
_4images_image_date: "2011-10-16T23:09:12"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33206 -->
Blick auf Antrieb und Endlagentaster von der Rückseite des Plotters.