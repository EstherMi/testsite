---
layout: "image"
title: "Landeswappen Baden-Württemberg"
date: "2012-03-14T15:43:44"
picture: "Wappen_Baden-Wrttemberg.jpg"
weight: "4"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/34647
imported:
- "2019"
_4images_image_id: "34647"
_4images_cat_id: "2456"
_4images_user_id: "1126"
_4images_image_date: "2012-03-14T15:43:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34647 -->
Hier ein weiterer Plot. Die Datei verdanke ich dem Tipp von Stefan Lehnerer, die HPGL-Dateien durch Konvertierung mit BOcnc aus frei verfügbaren .dwg-Dateien zu gewinnen; das Ergebnis verwendet nur die Befehle IN, PU, PD und PA. Das Bild besteht aus knapp 2.200 Plot-Befehlen und insgesamt ca. 116.000 Impulsen in Y- und ca. 137.000 Impulsen in X-Richtung (Plot-Dauer ca. 2,5 h).
Statt des Ballpens (wie in den anderen Plots) habe ich eine Kugelschreibermine verwendet; der Strich ist nicht so fein, dafür gibt es keine Tintenklekse.

Die leichte Verschiebung in der rechten Bildhälfte in Y-Richtung ist keine Ungenauigkeit des Plotters (wie ich zunächst fürchtete), sondern verursacht einer meiner Schrittmotoren: Wenn ich den Motor drehe, verschiebt sich die rechte Bildhälfte nach unten.