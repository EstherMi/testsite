---
layout: "image"
title: "Plotter 8"
date: "2007-12-21T16:24:43"
picture: "plotter1_2.jpg"
weight: "6"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/13132
imported:
- "2019"
_4images_image_id: "13132"
_4images_cat_id: "1181"
_4images_user_id: "502"
_4images_image_date: "2007-12-21T16:24:43"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13132 -->
