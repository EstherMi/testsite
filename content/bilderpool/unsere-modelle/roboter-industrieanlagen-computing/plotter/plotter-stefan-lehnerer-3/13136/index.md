---
layout: "image"
title: "Plotter 12"
date: "2007-12-21T16:24:43"
picture: "plotter5_2.jpg"
weight: "10"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/13136
imported:
- "2019"
_4images_image_id: "13136"
_4images_cat_id: "1181"
_4images_user_id: "502"
_4images_image_date: "2007-12-21T16:24:43"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13136 -->
