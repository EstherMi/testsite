---
layout: "image"
title: "Plotter 11"
date: "2007-12-21T16:24:43"
picture: "plotter4_2.jpg"
weight: "9"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/13135
imported:
- "2019"
_4images_image_id: "13135"
_4images_cat_id: "1181"
_4images_user_id: "502"
_4images_image_date: "2007-12-21T16:24:43"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13135 -->
