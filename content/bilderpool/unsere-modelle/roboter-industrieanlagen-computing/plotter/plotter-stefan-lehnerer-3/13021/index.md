---
layout: "image"
title: "Plotter 2"
date: "2007-12-09T13:33:17"
picture: "plotter2.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/13021
imported:
- "2019"
_4images_image_id: "13021"
_4images_cat_id: "1181"
_4images_user_id: "502"
_4images_image_date: "2007-12-09T13:33:17"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13021 -->
