---
layout: "image"
title: "[2/13] Steuerungs-Modul am ROBO TX Controller"
date: "2010-09-08T14:39:27"
picture: "portalroboterdxyzges02.jpg"
weight: "2"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/28069
imported:
- "2019"
_4images_image_id: "28069"
_4images_cat_id: "2038"
_4images_user_id: "723"
_4images_image_date: "2010-09-08T14:39:27"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28069 -->
Die Modellbasis kommt zunächst mit einem Robo TX Controller aus. Da aber z.B. bis zu weitere 3 Achsen plus Antriebsmotoren bei den Anwendungen und Experimenten hinzukommen können, wird selbst ein zweiter Controller nicht mehr ausreichen. Der Pultaufbau ist daher prophylaktisch anreihbar ...

Meine vorläufige Schnellwechsel-Adaptierung zwischen Robo TX Controller und mehreren Modellen habe ich am 26.02.2010 hier in der ftC schon ausführlich vorgestellt http://www.ftcommunity.de/details.php?image_id=26552
Die gezeigte Adaptierung an den Frontbuchsen des Controllers erlaubt immer noch ohne Veränderungen temporär zusätzliche 24 pol. Direktverkabelungen an Bord eines Modells :o)