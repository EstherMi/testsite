---
layout: "image"
title: "[8/13] Applikations-Modul 2,5D-XY-Plott"
date: "2010-09-08T14:39:28"
picture: "portalroboterdxyzges08.jpg"
weight: "8"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/28075
imported:
- "2019"
_4images_image_id: "28075"
_4images_cat_id: "2038"
_4images_user_id: "723"
_4images_image_date: "2010-09-08T14:39:28"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28075 -->
Das ist mein schon bekanntes? erstes allerdings noch mechanisches Applikations-Modul zum 2D-XY-Test-Plott. Mit den ft-Federn ist der Modul-Schlitten bei einem senkrechten Einbau vertikal im Kräftegleichgewicht, was einen feinfühligen Stiftlauf auf dem Papier ermöglicht. Die Führungsmechanik ist leichtgängig und spielfrei mit den Gelenkwürfel-Klauen an seiner Grundplatte eingestellt. Die M-Achsen sind dünnflüssig geölt. Bei axialer Belastung der Stiftspitze verlängert sich die Führung bei gleichzeitigem Verkürzen der Auskragung.

Als Zeichenstift ist hier eingesetzt ein Roller 97 mit einer Rollkugel 0,3 mm, der wegen seiner Instabilität gegen Verbiegen kurz und 4-fach geklemmt wird. Die Roller 97 gibt es 6-farbig im Set. Alle anderen landläufig eingesetzten Stifttypen sind ungeeignet sofern man nicht breitere Linien benötigt. Das Zeichenpapier darf dazu allerdings keine "Fettfinger" haben. Das kann hier zum Rutschen der Rollkugel auf dem Papier und damit zum zeitweisen Ausfall des Linienstrichs führen.