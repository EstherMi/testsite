---
layout: "image"
title: "[9/13] Applikations-Modul 2,5D-XY-Plott aufgeschoben"
date: "2010-09-08T14:39:29"
picture: "portalroboterdxyzges09.jpg"
weight: "9"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/28076
imported:
- "2019"
_4images_image_id: "28076"
_4images_cat_id: "2038"
_4images_user_id: "723"
_4images_image_date: "2010-09-08T14:39:29"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28076 -->
Der Applikations-Modul wird auf die beiden BS5-2Z hier rechts am Z-Schlitten aufgeschoben und auf der nicht einsehbaren Seite mit Kreuznocken an zwei BS5 in die Plananlage gesichert.