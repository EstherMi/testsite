---
layout: "image"
title: "[1/13] Modellanlage"
date: "2010-09-08T14:39:25"
picture: "portalroboterdxyzges01.jpg"
weight: "1"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/28068
imported:
- "2019"
_4images_image_id: "28068"
_4images_cat_id: "2038"
_4images_user_id: "723"
_4images_image_date: "2010-09-08T14:39:25"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28068 -->
Die Kurzbezeichnung 3D-XYZ-GES des Modells steht für dreidimensional mit Linearachsen XYZ und Gleitführung mit Encodermotorantrieb über Schneckengetriebe.

Die hier angewandte trockene Gleitführung habe ich schon in 2D-Bildern aus der 3D-Konstruktion am 05.03.2008 hier in der ftCommunity vorgestellt http://www.ftcommunity.de/details.php?image_id=13838 Sie erlaubt wie zu sehen schlanke Konstruktionen, die allerdings nur mit Alu-Profilen stabil realisierbar sind. Das YZ-Schlittenkreuz hat in der X-Achse eine Bauhöhe von nur 15mm. Alle drei Achsenführungen sind innerhalb der engen Grenzen der ft-Nylons (Zapfen-Nut) mechanisch für Kraftbeaufschlagungen senkrecht und quer zum Profil in jeweils beide Richtungen ausgelegt. 

Die Konstruktion zunächst als reines ft-Modell ist mit Statik und Schlittenmechanik bereits seit Januar 2008 aufgebaut. Mit der Verfügbarkeit über ft-Encodermotoren konnte es 11/2009 zu Testläufen mit zunächst einfachen RoboPro-Steuerprogrammen (Level 1) in Betrieb genommen werden. Die Betriebszeit des Modells liegt mittlererweile bei über 6 Std.

Die seit 2009 verfügbare erweiterte wegsynchrone Motorsteuerung als eine der Grundfunktionen des Robo Pro erlaubt hier vorerst nur rechtwinklige und 45° schräge Linien in 2D oder 3D. Erst eine zeitsynchrone mit dem Bresenham-Algorithmus vergleichbare Motorsteuerung wird es ermöglichen, die bereits in Robo Pro vorhandenen mathematischen Funktionen in Echtzeit berechnet mehrachsig über Linear- oder Drehachsen zu fahren. RoboProEntwickler hat das auch in seinem Arbeitsprogramm ! Es bleibt allerdings zu befürchten, dass verkaufsabhängige ft-Anliegen ihn davon noch länger? abhalten.

Zur Abstimmung der Stromaufnahme der Motoren vom Robo TX Controller wurden Anordnung,  Auswahl und teils stufige Modifizierung der Führungselemente Verbindungsstücke 15 überarbeitet.

Zum Modellzweck sind zunächst 2 Punkte zu benennen:
1. Langfristige Erprobung der trockenen Gleitführung mit der Werkstoffpaarung Alu anox  - Nylon
2. Modellbasis für mehrachsige Anwendungen und Experimente mittels Wechseladapter-Technologie

Noch ein paar Auszüge aus den technischen Details:
Impulsauflösung rechnerisch:  0,063 mm / Dekoderimpuls
Umkehrspiel: mechanisch korrigiert z.Zt. in X- und Y-Achse
Lichte YZ-Portal: 270 mm in der Y-Achse, 105 mm in der Z-Achse
Verfahrwege: X-Achse 310 mm, Y-Achse 213 mm und Z-Achse 88 mm