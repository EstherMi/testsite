---
layout: "image"
title: "[4/13] Encodermotoren X-Achse"
date: "2010-09-08T14:39:28"
picture: "portalroboterdxyzges04.jpg"
weight: "4"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/28071
imported:
- "2019"
_4images_image_id: "28071"
_4images_cat_id: "2038"
_4images_user_id: "723"
_4images_image_date: "2010-09-08T14:39:28"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28071 -->
Zum Antrieb der beiden Schlitten auf der X-Achse gab es 3 Alternativen:

1. Ein Motor mit Kettenkopplung der beiden Schlitten
2. Zwei unsynchronisierte Motoren kettengekoppelt
3. Zwei synchronisierte Motoren ohne Kettenkopplung

Ich habe mich nach Versuchen für die Alternative 3 entschieden. RoboProEntwickler verfolgt das? :o)
Ich hoffe sehr, dass es ihm gelingt ft von der notwendigen Zweipoligkeit der Encoder zu überzeugen  ...

Die Motoren sind auf je zwei Anbauwinkel aufgesetzt.