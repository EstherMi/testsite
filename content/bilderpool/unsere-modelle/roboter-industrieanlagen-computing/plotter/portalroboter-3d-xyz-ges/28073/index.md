---
layout: "image"
title: "[6/13] Encodermotor Z-Achse"
date: "2010-09-08T14:39:28"
picture: "portalroboterdxyzges06.jpg"
weight: "6"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/28073
imported:
- "2019"
_4images_image_id: "28073"
_4images_cat_id: "2038"
_4images_user_id: "723"
_4images_image_date: "2010-09-08T14:39:28"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28073 -->
Die Z- Achse mit Encodermotor ist hier beim Portalroboter im Gegensatz zu den 2,5D-Plottern eine vollwertige wegprogrammierbare Linearachse, die wie dort logisch nicht nur als Hebeachse 0,5D verwendbar ist ...

Eine 0,5D-Hebeachse kann sich in Z nur auf einem konstruktiv fest vorgegebenen Hubweg bewegen. Auf diesen Weg hat die Steuerung keinen variablen Einfluss. Sie steuert in der Regel nur das Heben und Senken.