---
layout: "image"
title: "[12/13] XY-Testplott, Quadrat 35 von 35"
date: "2010-09-08T14:39:29"
picture: "portalroboterdxyzges12.jpg"
weight: "12"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/28079
imported:
- "2019"
_4images_image_id: "28079"
_4images_cat_id: "2038"
_4images_user_id: "723"
_4images_image_date: "2010-09-08T14:39:29"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28079 -->
Der letzte Strich die zweite Diagonale des 35. Quadrates wird gezeichet.

Zum Test der Achsen-Lauftoleranz unter einem Programm mit vielen Richtungsänderungen für die einpoligen Encodermotoren (zunächst XY-Arbeitsebene) habe ich auch schon mehrere Läufe verschiedenfarbig übereinander zeichnen lassen. Wer hat das mit seinem Plottermodell auch mal gemacht? Jedenfalls sind solche Plotts sehr aufschlussreich ... 

Da die Rastritzel Z10 auf den Motorzapfen sich axial lösen habe ich sie inzwischen durch die Z10 mit Nabe von Andreas Tacke ersetzt.

Ich denke aber auch, dass die Synchronsteuerung mit strafferen Führungen logisch Probleme bekommt, die bei einpoligen Encodern auf der Plotterlinie besonders diagonal stellenweise ihre Spuren in kleinen flachen Wellenlinien hinterlassen. Ich bin sicher, dass RoboProEntwickler mit den einpoligen Encodern zu den dann notwendigen Schätzverfahren in Robo Pro durch ft unnötige qualitative Probleme serviert bekommt.