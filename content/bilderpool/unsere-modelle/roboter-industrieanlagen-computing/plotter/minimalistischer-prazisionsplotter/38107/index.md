---
layout: "image"
title: "Minimalistischer Präzisionsplotter von Dirk Fox"
date: "2014-01-20T20:51:53"
picture: "plotter06.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "supermaxi19840"
license: "unknown"
legacy_id:
- details/38107
imported:
- "2019"
_4images_image_id: "38107"
_4images_cat_id: "2835"
_4images_user_id: "2109"
_4images_image_date: "2014-01-20T20:51:53"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38107 -->
der Controller
Die Motoren sind wie folgt am Controller angeschlossen: M1 (x- Achse), M2 (y- Achse), M3 (Schreibkopf), Impuseingänge (blauer Stecker): Rot an +, Schwarz an C1/2  und grün an GND , Taster von M1 an I1, Taster von M2 an I2 und Taster von M3 (Schreibkopf ) an I3, Taster sind alle an 1 und 3 angeschlossen.