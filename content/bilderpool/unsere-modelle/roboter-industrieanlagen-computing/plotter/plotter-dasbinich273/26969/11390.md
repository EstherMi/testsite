---
layout: "comment"
hidden: true
title: "11390"
date: "2010-04-19T16:06:33"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Tach auch!

Ich kann Dir anbieten, meine Plottersoftware von dem hier http://www.ftcommunity.de/details.php?image_id=3921 zu mailen. Die ist in Visual Basic .net (noch zu .net-1.1-Zeiten) gemacht und steuert das ft-Parallel-Interface an. Du müsstest also etwas programmieren, VB und .net können und das Programm an Deinen Plotter selber anpassen. Wenn Du willst, gib mir Bescheid.

Gruß,
Stefan