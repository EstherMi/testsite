---
layout: "image"
title: "Details des Stifthalters"
date: "2010-04-18T19:35:08"
picture: "plotter10.jpg"
weight: "10"
konstrukteure: 
- "dasbinich273"
fotografen:
- "dasbinich273"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "dasbinich273"
license: "unknown"
legacy_id:
- details/26969
imported:
- "2019"
_4images_image_id: "26969"
_4images_cat_id: "1936"
_4images_user_id: "1057"
_4images_image_date: "2010-04-18T19:35:08"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26969 -->
Die Kabelhalter sind fast an der Belastungsgrenze,um den Stift zu halten.Anders wäre es aber zu groß, bzw.lang geworden