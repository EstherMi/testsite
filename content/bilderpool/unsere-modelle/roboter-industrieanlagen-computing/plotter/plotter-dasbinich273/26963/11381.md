---
layout: "comment"
hidden: true
title: "11381"
date: "2010-04-18T20:56:42"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Raffiniert das mit der Achse! Schön, wieder mal einen Plotter in dieser Bauart zu sehen!

Gruß,
Stefan