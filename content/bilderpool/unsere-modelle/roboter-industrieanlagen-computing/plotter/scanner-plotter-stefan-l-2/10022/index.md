---
layout: "image"
title: "Scanner/Plotter 6"
date: "2007-04-07T11:10:28"
picture: "scannerplotter06.jpg"
weight: "24"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/10022
imported:
- "2019"
_4images_image_id: "10022"
_4images_cat_id: "904"
_4images_user_id: "502"
_4images_image_date: "2007-04-07T11:10:28"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10022 -->
Der Scannkopf