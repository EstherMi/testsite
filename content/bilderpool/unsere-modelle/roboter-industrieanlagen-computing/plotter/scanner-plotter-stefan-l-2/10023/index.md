---
layout: "image"
title: "Scanner/Plotter 7"
date: "2007-04-07T11:10:28"
picture: "scannerplotter07.jpg"
weight: "25"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/10023
imported:
- "2019"
_4images_image_id: "10023"
_4images_cat_id: "904"
_4images_user_id: "502"
_4images_image_date: "2007-04-07T11:10:28"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10023 -->
Hier wird der Strom Übertragen.