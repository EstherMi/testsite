---
layout: "image"
title: "Scanner/Plotter 31"
date: "2007-04-23T21:15:31"
picture: "scannerplotter1_4.jpg"
weight: "1"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/10148
imported:
- "2019"
_4images_image_id: "10148"
_4images_cat_id: "904"
_4images_user_id: "502"
_4images_image_date: "2007-04-23T21:15:31"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10148 -->
Hier mal ein paar aktuelle Bilder von meinem Plotter, er liefert jetzt schon ziemlich gute scann-Ergebnisse.