---
layout: "image"
title: "Scanner/Plotter 35"
date: "2007-04-23T21:15:31"
picture: "scannerplotter5_2.jpg"
weight: "5"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/10152
imported:
- "2019"
_4images_image_id: "10152"
_4images_cat_id: "904"
_4images_user_id: "502"
_4images_image_date: "2007-04-23T21:15:31"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10152 -->
