---
layout: "image"
title: "Scanner/Plotter 1"
date: "2007-04-07T11:10:28"
picture: "scannerplotter01.jpg"
weight: "18"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/10017
imported:
- "2019"
_4images_image_id: "10017"
_4images_cat_id: "904"
_4images_user_id: "502"
_4images_image_date: "2007-04-07T11:10:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10017 -->
Der Aufbau ist der eines Plotter nur das er auch Scannen kann. Es gibt den Scannkopf und den Schreibkopf. Das gescannte kann er drucken.