---
layout: "image"
title: "Scanner/Plotter 10"
date: "2007-04-07T11:10:28"
picture: "scannerplotter10.jpg"
weight: "28"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/10026
imported:
- "2019"
_4images_image_id: "10026"
_4images_cat_id: "904"
_4images_user_id: "502"
_4images_image_date: "2007-04-07T11:10:28"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10026 -->
