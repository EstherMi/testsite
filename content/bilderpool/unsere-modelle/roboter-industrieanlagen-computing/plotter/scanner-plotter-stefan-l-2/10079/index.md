---
layout: "image"
title: "Scanner/Plotter 22"
date: "2007-04-13T16:09:16"
picture: "scannerplotter1_2.jpg"
weight: "10"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/10079
imported:
- "2019"
_4images_image_id: "10079"
_4images_cat_id: "904"
_4images_user_id: "502"
_4images_image_date: "2007-04-13T16:09:16"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10079 -->
Mit den Schrittmotoren hat er eine Genauigkeit von 0,025cm.