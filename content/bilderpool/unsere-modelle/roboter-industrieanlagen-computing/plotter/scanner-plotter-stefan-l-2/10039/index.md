---
layout: "image"
title: "Scanner/Plotter 21"
date: "2007-04-09T13:36:53"
picture: "scannerplotter1.jpg"
weight: "17"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/10039
imported:
- "2019"
_4images_image_id: "10039"
_4images_cat_id: "904"
_4images_user_id: "502"
_4images_image_date: "2007-04-09T13:36:53"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10039 -->
