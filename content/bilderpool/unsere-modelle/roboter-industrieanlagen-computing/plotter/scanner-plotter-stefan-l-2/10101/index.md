---
layout: "image"
title: "Scanner/Plotter 30"
date: "2007-04-20T20:44:11"
picture: "scannerplotter2_2.jpg"
weight: "9"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/10101
imported:
- "2019"
_4images_image_id: "10101"
_4images_cat_id: "904"
_4images_user_id: "502"
_4images_image_date: "2007-04-20T20:44:11"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10101 -->
....am Anfang ziemlich gut aber dann kommt wieder dieser murks. Er erkennt schwarz bis ganz rechts und alle 2 Zeile auch noch im mittleren Teil. Am Programm kann das eigentlich nicht liegen, am Sensor aber auch nicht. Kann mir jemand dafür eine plausible Erklärung geben??? Auf wunsch kann ich natürlich auch das programm hochladen.