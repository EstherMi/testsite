---
layout: "image"
title: "Vakuumpumpe und Spränkler"
date: "2013-05-27T15:45:22"
picture: "Splmaschine.jpg"
weight: "5"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "majus"
license: "unknown"
legacy_id:
- details/36995
imported:
- "2019"
_4images_image_id: "36995"
_4images_cat_id: "2751"
_4images_user_id: "1239"
_4images_image_date: "2013-05-27T15:45:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36995 -->
Hier seht ihr die Vakuumpumpe im unteren Bereich von der Seite. Hinten ist ein Zylinder, der von einem Motor recht langsam vor und zurück gefahren wird. Wenn er lauft herausdrückt, ist durch die Impulsscheibe ein Ventil offen, wenn er sie einsaugt das andere. Für ein stabiles Vakuum sorgt der Verschließung des Ventils, da es im ausgeschalteten Zustand Luft einfließen lässt.