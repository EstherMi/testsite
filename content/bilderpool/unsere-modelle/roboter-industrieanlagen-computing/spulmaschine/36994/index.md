---
layout: "image"
title: "Funktionsprinzip"
date: "2013-05-27T15:45:22"
picture: "Splmaschine4.jpg"
weight: "4"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "majus"
license: "unknown"
legacy_id:
- details/36994
imported:
- "2019"
_4images_image_id: "36994"
_4images_cat_id: "2751"
_4images_user_id: "1239"
_4images_image_date: "2013-05-27T15:45:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36994 -->
So funktioniert die Anlage: 
Rechts seht ihr in meiner Grafik die 3 Ventile (v1-v3) sowie den Druckwassertank, indem ich das Wasser hineindrücken oder herauspumpen kann. Rechts oben ist der Becher, der im Bild daneben ganz links zu sehen ist, abgebildet.
Das Bild an sich zeigt die drei Handventile, die mit Motoren gestellt werden. 
An v1 schließt sich einmal ein Druckschlauch und ein Vakuumschlauch an, da es Vakuumpumpen von FT nicht wirklich gibt, habe ich selbst eine entworfen, die ich im nächsten Bild erkläre.