---
layout: "image"
title: "CT - in Schräglage"
date: "2012-01-28T15:38:06"
picture: "Bild_5.jpg"
weight: "6"
konstrukteure: 
- "Majus"
fotografen:
- "Majus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "majus"
license: "unknown"
legacy_id:
- details/34071
imported:
- "2019"
_4images_image_id: "34071"
_4images_cat_id: "2520"
_4images_user_id: "1239"
_4images_image_date: "2012-01-28T15:38:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34071 -->
Wieder eine Stellung des CT, diesmal in Schräglage. Die Schnecke oben ersetzt übrigens nur den Mangel an Fototransistoren. Es könnten dort genauso auch 4 Fototransistoren sitzen.