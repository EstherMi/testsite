---
layout: "overview"
title: "Magnetrührer"
date: 2019-12-17T19:08:23+01:00
legacy_id:
- categories/3135
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3135 --> 
Ein Magnetrührer, der mit Hilfe eines Rührmagneten Flüssigkeiten in einem Marmeladenglas umrühren kann.
Link zu den Magneten: http://www.reichelt.de/MAGNET-4-2/3/index.html?&ACTION=3&LA=446&ARTICLE=151644&artnr=MAGNET+4.2&SEARCH=magnete