---
layout: "image"
title: "Schema Ampelphasen Kreuzung"
date: "2015-01-14T19:13:30"
picture: "ampelanlage03.jpg"
weight: "3"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/40318
imported:
- "2019"
_4images_image_id: "40318"
_4images_cat_id: "3024"
_4images_user_id: "2303"
_4images_image_date: "2015-01-14T19:13:30"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40318 -->
Programm 1:

Die Ampelanlage durchläuft ein festes Programm, egal wie viele Autos an der Ampel stehen. Das heisst, es 
wird keine Zählung der Autos vorgenommen, um die Ampelanlage zu schalten. Die Fußgänger bekommen 
nur in einem bestimmten Zeitfenster grün. 
Dies war die Grundlage für die spätere Optimierung und wurde in Robopro umgesetzt.