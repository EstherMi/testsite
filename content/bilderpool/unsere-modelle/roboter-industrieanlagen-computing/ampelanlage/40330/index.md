---
layout: "image"
title: "Ampelanlage"
date: "2015-01-14T19:13:30"
picture: "ampelanlage15.jpg"
weight: "15"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/40330
imported:
- "2019"
_4images_image_id: "40330"
_4images_cat_id: "3024"
_4images_user_id: "2303"
_4images_image_date: "2015-01-14T19:13:30"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40330 -->
