---
layout: "image"
title: "Ampelanlage Robopro"
date: "2015-01-14T19:13:30"
picture: "ampelanlage06.jpg"
weight: "6"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/40321
imported:
- "2019"
_4images_image_id: "40321"
_4images_cat_id: "3024"
_4images_user_id: "2303"
_4images_image_date: "2015-01-14T19:13:30"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40321 -->
Die Logik hinter der Anlage hat am längsten gedauert, weil viele Abhägigkeiten programmiert werden mussten.