---
layout: "image"
title: "Ampelanlage Schema"
date: "2015-01-14T19:13:30"
picture: "ampelanlage02.jpg"
weight: "2"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/40317
imported:
- "2019"
_4images_image_id: "40317"
_4images_cat_id: "3024"
_4images_user_id: "2303"
_4images_image_date: "2015-01-14T19:13:30"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40317 -->
Dies ist ein Schema, wie eine dynamische Ampelsteuerung aufgebaut ist.
In der Fahrbahn sind so genannte Induktionschleifen (Zähler) eingelassen.
Die kennt jeder, der einmal Rot überfahren hat. :-)