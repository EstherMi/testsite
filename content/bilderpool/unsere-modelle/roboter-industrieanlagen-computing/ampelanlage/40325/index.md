---
layout: "image"
title: "Ampelanlage schräg"
date: "2015-01-14T19:13:30"
picture: "ampelanlage10.jpg"
weight: "10"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/40325
imported:
- "2019"
_4images_image_id: "40325"
_4images_cat_id: "3024"
_4images_user_id: "2303"
_4images_image_date: "2015-01-14T19:13:30"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40325 -->
