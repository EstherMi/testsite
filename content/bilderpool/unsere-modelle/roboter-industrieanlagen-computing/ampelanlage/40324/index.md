---
layout: "image"
title: "Ampelanlage schräg"
date: "2015-01-14T19:13:30"
picture: "ampelanlage09.jpg"
weight: "9"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/40324
imported:
- "2019"
_4images_image_id: "40324"
_4images_cat_id: "3024"
_4images_user_id: "2303"
_4images_image_date: "2015-01-14T19:13:30"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40324 -->
Ich habe farbige Pappe  für die Fahrbahn verwende und diese mit doppelseitigem Teppichklebeband auf den Grundplatten befestigt.