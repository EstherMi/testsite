---
layout: "overview"
title: "Sylvias Auto-Lackieranlage"
date: 2019-12-17T19:08:23+01:00
legacy_id:
- categories/3138
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3138 --> 
Eine rein elektromechanisch und elektronisch (mit Silberlingen) gesteuerte Maschine, die Fahrzeuge mit einer simulierten Düse von allen Seiten lackiert und sich selbstständig der unterschiedlichen Karosseriehöhe anpasst.