---
layout: "image"
title: "Detail Lager für Extruder-Antrieb"
date: "2009-03-09T18:52:32"
picture: "Detail_Lager_fr_Extruder-Antrieb.jpg"
weight: "1"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/23442
imported:
- "2019"
_4images_image_id: "23442"
_4images_cat_id: "1577"
_4images_user_id: "724"
_4images_image_date: "2009-03-09T18:52:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23442 -->
Kleine Verbesserung:
Durch das Lager hat der Antrieb etwas mehr Power. Es besteht nicht mehr die Gefahr, daß sich die Schnecke vom Zahnrad wegbewegt.