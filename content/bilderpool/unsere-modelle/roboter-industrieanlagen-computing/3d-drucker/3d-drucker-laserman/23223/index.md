---
layout: "image"
title: "3D-Drucker vorne"
date: "2009-02-26T14:40:26"
picture: "3D-Drucker_Vorne.jpg"
weight: "5"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: ["3d-drucker", "3d", "drucker", "printer", "reprap", "rapid", "prototyping"]
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/23223
imported:
- "2019"
_4images_image_id: "23223"
_4images_cat_id: "1577"
_4images_user_id: "724"
_4images_image_date: "2009-02-26T14:40:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23223 -->
Original von Andreas Rozek.
Weitere Einzelheiten gibt es auf der Wiki-Seite:
http://objects.reprap.org/wiki/Builders/FTIStrap
Man beachte die 4 Alustangen, die gegenüber dem Original eine beträchtliche Festigkeitssteigerung bringen.
Das Intelligent Interface mit Erweiterungs-Modul ist für Flachbandkabel-Anschluß modifiziert.

Hier geht es zum Zeitraffer-Film, wie ein Objekt entsteht:
http://www.youtube.com/watch?v=HI0MLHHeOP0