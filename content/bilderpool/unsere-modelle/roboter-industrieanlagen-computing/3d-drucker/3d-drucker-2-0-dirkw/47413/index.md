---
layout: "image"
title: "3D-Drucker 2.0 Front"
date: "2018-04-16T19:24:06"
picture: "ddrucker02.jpg"
weight: "3"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/47413
imported:
- "2019"
_4images_image_id: "47413"
_4images_cat_id: "3504"
_4images_user_id: "2303"
_4images_image_date: "2018-04-16T19:24:06"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47413 -->
PID-Temperaturregung (mittig), der Ein-/Aus-Schalter (rechts) für
das Heizbett und das Licht für die LED-Beleuchtung.