---
layout: "image"
title: "3D-Drucker 2.0 hinten"
date: "2018-04-16T19:24:12"
picture: "ddrucker13.jpg"
weight: "14"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/47424
imported:
- "2019"
_4images_image_id: "47424"
_4images_cat_id: "3504"
_4images_user_id: "2303"
_4images_image_date: "2018-04-16T19:24:12"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47424 -->
