---
layout: "image"
title: "3D-Drucker 2.0 Hotend"
date: "2018-04-16T19:24:12"
picture: "ddrucker18.jpg"
weight: "19"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/47429
imported:
- "2019"
_4images_image_id: "47429"
_4images_cat_id: "3504"
_4images_user_id: "2303"
_4images_image_date: "2018-04-16T19:24:12"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47429 -->
Das Hotend sitzt im Druckkopf. Durch Bausteine 15 mit Bohrung wird dieser über 2 Achsen geführt. 
Vorne ist der Endschalter zu erkennen.