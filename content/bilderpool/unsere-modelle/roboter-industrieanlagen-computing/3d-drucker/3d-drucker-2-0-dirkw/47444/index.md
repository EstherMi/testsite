---
layout: "image"
title: "3D-Drucker 2.0"
date: "2018-04-16T19:24:22"
picture: "ddrucker33.jpg"
weight: "34"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/47444
imported:
- "2019"
_4images_image_id: "47444"
_4images_cat_id: "3504"
_4images_user_id: "2303"
_4images_image_date: "2018-04-16T19:24:22"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47444 -->
Aus Platzgründen habe ich den Original fischertechnik-Controller auf der Rückseite verbaut.
Im Boden war kein Platz mehr vorhanden.