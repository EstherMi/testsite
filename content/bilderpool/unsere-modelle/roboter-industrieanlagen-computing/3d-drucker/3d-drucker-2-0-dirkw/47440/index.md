---
layout: "image"
title: "3D-Drucker 2.0"
date: "2018-04-16T19:24:18"
picture: "ddrucker29.jpg"
weight: "30"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/47440
imported:
- "2019"
_4images_image_id: "47440"
_4images_cat_id: "3504"
_4images_user_id: "2303"
_4images_image_date: "2018-04-16T19:24:18"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47440 -->
Hier seht ihr das 350 W Netzteil 12 Volt für das Heizbett. Mittig, der PID-Regler von Inkbird für die Temperaturregelung. Rechts ein SSR-Relais zum Ansteuern des Heizbetts.