---
layout: "image"
title: "3D-Drucker 2.0 rechts"
date: "2018-04-16T19:24:07"
picture: "ddrucker05.jpg"
weight: "6"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/47416
imported:
- "2019"
_4images_image_id: "47416"
_4images_cat_id: "3504"
_4images_user_id: "2303"
_4images_image_date: "2018-04-16T19:24:07"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47416 -->
