---
layout: "image"
title: "3D-Drucker 2.0 hinten schräg"
date: "2018-04-16T19:24:07"
picture: "ddrucker09.jpg"
weight: "10"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/47420
imported:
- "2019"
_4images_image_id: "47420"
_4images_cat_id: "3504"
_4images_user_id: "2303"
_4images_image_date: "2018-04-16T19:24:07"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47420 -->
