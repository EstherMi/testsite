---
layout: "image"
title: "3D-Drucker 2.0 Hinten"
date: "2018-04-16T19:24:22"
picture: "ddrucker32.jpg"
weight: "33"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/47443
imported:
- "2019"
_4images_image_id: "47443"
_4images_cat_id: "3504"
_4images_user_id: "2303"
_4images_image_date: "2018-04-16T19:24:22"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47443 -->
