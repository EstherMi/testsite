---
layout: "image"
title: "3D-Drucker 2.0 hinten schräg"
date: "2018-04-16T19:24:12"
picture: "ddrucker11.jpg"
weight: "12"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/47422
imported:
- "2019"
_4images_image_id: "47422"
_4images_cat_id: "3504"
_4images_user_id: "2303"
_4images_image_date: "2018-04-16T19:24:12"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47422 -->
