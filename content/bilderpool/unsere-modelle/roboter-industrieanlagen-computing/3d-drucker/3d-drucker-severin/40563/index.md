---
layout: "image"
title: "Kerzenständer im Druck"
date: "2015-02-17T21:23:03"
picture: "drucker6.jpg"
weight: "7"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/40563
imported:
- "2019"
_4images_image_id: "40563"
_4images_cat_id: "3000"
_4images_user_id: "558"
_4images_image_date: "2015-02-17T21:23:03"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40563 -->
Nahaufnahme