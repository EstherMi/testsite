---
layout: "image"
title: "BS30"
date: "2015-02-17T21:23:03"
picture: "drucker1.jpg"
weight: "2"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/40558
imported:
- "2019"
_4images_image_id: "40558"
_4images_cat_id: "3000"
_4images_user_id: "558"
_4images_image_date: "2015-02-17T21:23:03"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40558 -->
Erster Versuch nen Fischertechnik Teil zu klonen.
Es ist zwar noch ne ganze Testreihe erforderlich bis der Stein dann wirklich nutzbar ist, aber für ein "mal eben schnell" Ausdruck, der recht hohe Genauigkeit braucht, ist das Ergebnis sehr akzeptabel.