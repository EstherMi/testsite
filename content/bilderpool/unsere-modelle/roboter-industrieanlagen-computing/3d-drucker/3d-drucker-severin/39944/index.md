---
layout: "image"
title: "3D Drucker"
date: "2014-12-19T11:56:14"
picture: "ddrucker16.jpg"
weight: "25"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/39944
imported:
- "2019"
_4images_image_id: "39944"
_4images_cat_id: "3000"
_4images_user_id: "558"
_4images_image_date: "2014-12-19T11:56:14"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39944 -->
Das Ergebnis: Portal weighted compation cube
Hat ca 50 min gedauert bei niedrigst möglicher Auflösung und 30mm/s
Material: PLA