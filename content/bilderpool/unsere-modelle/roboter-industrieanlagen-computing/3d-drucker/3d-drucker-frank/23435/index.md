---
layout: "image"
title: "3D-Drucker02"
date: "2009-03-09T08:11:10"
picture: "ddruckerfrank2.jpg"
weight: "9"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/23435
imported:
- "2019"
_4images_image_id: "23435"
_4images_cat_id: "1597"
_4images_user_id: "729"
_4images_image_date: "2009-03-09T08:11:10"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23435 -->
Ansicht von links