---
layout: "comment"
hidden: true
title: "8780"
date: "2009-03-15T19:35:11"
uploadBy:
- "Knarf Bokaj"
license: "unknown"
imported:
- "2019"
---
Hallo Peter,

mir fehlt leider noch eine I/O-Extension zum Anschluß der Schrittmotoren für die Z-Achse und dem Extruder. Einige andere Bauteile sind noch in einer Laufmaschine verbaut. Wahrscheinlich dauert es noch ein paar Wochen bis zum ersten "Druck"

Gruß,
Frank