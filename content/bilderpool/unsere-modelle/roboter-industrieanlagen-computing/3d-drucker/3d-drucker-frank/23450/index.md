---
layout: "image"
title: "3D-Drucker12"
date: "2009-03-14T20:33:25"
picture: "PICT4674.jpg"
weight: "4"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/23450
imported:
- "2019"
_4images_image_id: "23450"
_4images_cat_id: "1597"
_4images_user_id: "729"
_4images_image_date: "2009-03-14T20:33:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23450 -->
Ansicht von rechts