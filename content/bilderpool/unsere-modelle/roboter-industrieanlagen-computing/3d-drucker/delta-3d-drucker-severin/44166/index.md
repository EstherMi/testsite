---
layout: "image"
title: "Gedruckter Baustein"
date: "2016-08-03T14:24:22"
picture: "IMG_3311_-_Kopie.JPG"
weight: "1"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/44166
imported:
- "2019"
_4images_image_id: "44166"
_4images_cat_id: "3262"
_4images_user_id: "558"
_4images_image_date: "2016-08-03T14:24:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44166 -->
Gedruckt mit 80mm/s für Infill und 60mm/s für die Aussenkanten,

Nachbearbeitung: Flächen schleifen, Nuten von 3,95 auf 4,1mm aufbohren. => ~2min