---
layout: "image"
title: "RAMPS"
date: "2016-07-31T17:44:14"
picture: "deltaddrucker17.jpg"
weight: "19"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/44062
imported:
- "2019"
_4images_image_id: "44062"
_4images_cat_id: "3262"
_4images_user_id: "558"
_4images_image_date: "2016-07-31T17:44:14"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44062 -->
Die Elektronik ist die aus dem alten Drucker.