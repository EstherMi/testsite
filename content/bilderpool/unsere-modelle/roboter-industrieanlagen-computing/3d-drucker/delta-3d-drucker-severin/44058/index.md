---
layout: "image"
title: "Bedienpanel von unten"
date: "2016-07-31T17:44:14"
picture: "deltaddrucker13.jpg"
weight: "15"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/44058
imported:
- "2019"
_4images_image_id: "44058"
_4images_cat_id: "3262"
_4images_user_id: "558"
_4images_image_date: "2016-07-31T17:44:14"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44058 -->
Zu sehen ist hier das Bedienpanel von unten. Der Raspberry Pi 2 steuert über USB den Arduino im Drucker, stellt die Netzwerkfunktionalität und bietet die Bedienoberfläche