---
layout: "image"
title: "Führung und Umlenkrolle"
date: "2016-07-31T17:44:04"
picture: "deltaddrucker08.jpg"
weight: "10"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/44053
imported:
- "2019"
_4images_image_id: "44053"
_4images_cat_id: "3262"
_4images_user_id: "558"
_4images_image_date: "2016-07-31T17:44:04"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44053 -->
Hier die Führung für die Spinne. Die Rollen werden leicht in die Aluvertiefungen gedrückt und ergeben mit ein wenig Silikonspray erstaunlich leichtgängige (und gute) Lager ab.
