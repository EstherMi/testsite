---
layout: "image"
title: "Standfüße"
date: "2016-07-31T17:44:14"
picture: "deltaddrucker18.jpg"
weight: "20"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/44063
imported:
- "2019"
_4images_image_id: "44063"
_4images_cat_id: "3262"
_4images_user_id: "558"
_4images_image_date: "2016-07-31T17:44:14"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44063 -->
Der Drucker steht auf insgesamt sechs 60°-Winkeln