---
layout: "image"
title: "Oberteil und Druckspinne"
date: "2016-07-31T17:44:04"
picture: "deltaddrucker10.jpg"
weight: "12"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/44055
imported:
- "2019"
_4images_image_id: "44055"
_4images_cat_id: "3262"
_4images_user_id: "558"
_4images_image_date: "2016-07-31T17:44:04"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44055 -->
Mittig am oberen Teil wird der Kabelschlauch für das Hotend herabgelassen.