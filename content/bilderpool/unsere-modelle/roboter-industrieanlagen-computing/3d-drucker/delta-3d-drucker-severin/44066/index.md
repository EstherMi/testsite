---
layout: "image"
title: "Der ganze Drucker"
date: "2016-07-31T17:44:19"
picture: "deltaddrucker21.jpg"
weight: "23"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/44066
imported:
- "2019"
_4images_image_id: "44066"
_4images_cat_id: "3262"
_4images_user_id: "558"
_4images_image_date: "2016-07-31T17:44:19"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44066 -->
1m lange Alus und ganz schön schwer..