---
layout: "image"
title: "Innereien"
date: "2016-07-31T17:44:14"
picture: "deltaddrucker16.jpg"
weight: "18"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/44061
imported:
- "2019"
_4images_image_id: "44061"
_4images_cat_id: "3262"
_4images_user_id: "558"
_4images_image_date: "2016-07-31T17:44:14"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44061 -->
Die ganze Technik unter dem Heizbett:
24V 400W Netzteil
RAMPS 1.4 auf einem Arduino Mega

FSR (Force Sensitive Restistor)  an jeder Kante als Auflage für das Heizbett. Der Drucker merkt so ob sich etwas auf dem Druckbett befindet und erkennt Berührungen des Hotends. Das Hotend kann so kontrolliert die Heizplatte abtasten und somit Unebenheiten und Neigung erkennen und später beim Druck dann berücksichtigen.