---
layout: "image"
title: "Extruder"
date: "2016-07-31T17:44:04"
picture: "deltaddrucker07.jpg"
weight: "9"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/44052
imported:
- "2019"
_4images_image_id: "44052"
_4images_cat_id: "3262"
_4images_user_id: "558"
_4images_image_date: "2016-07-31T17:44:04"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44052 -->
Das Filament wird in die Teflonschläuche gedrückt, welche auf der anderen Seite am Hotend befestigt sind. So können die schweren Motoren vom Hotend nach aussen verlagert werden, wodurch man, dank geringerem Gewicht des Effektors dann schneller Drucken kann, da weniger Masse beschleunigt werden muss.