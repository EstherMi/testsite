---
layout: "image"
title: "Extruder"
date: "2016-07-31T17:44:04"
picture: "deltaddrucker06.jpg"
weight: "8"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/44051
imported:
- "2019"
_4images_image_id: "44051"
_4images_cat_id: "3262"
_4images_user_id: "558"
_4images_image_date: "2016-07-31T17:44:04"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44051 -->
Hier ist ein Teil des Doppelextruders zu sehen. Der Motor wird hinten angehoben und drückt so mit dem Treibrad auf das Filament, welches am Kugellager vorbei muss. Das Filament wird dann in ein Schlauchaufnehmer, welcher in eine Radmutter geschraubt ist hineingedrückt.