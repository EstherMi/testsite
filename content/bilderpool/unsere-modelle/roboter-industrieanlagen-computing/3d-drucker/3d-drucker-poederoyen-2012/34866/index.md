---
layout: "image"
title: "3D-Drucker Poederoyen-2012"
date: "2012-05-05T13:12:25"
picture: "ddruckerpoederoyen10.jpg"
weight: "10"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Peoderoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/34866
imported:
- "2019"
_4images_image_id: "34866"
_4images_cat_id: "2582"
_4images_user_id: "22"
_4images_image_date: "2012-05-05T13:12:25"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34866 -->
3D-Drucken "Schnee"

Anleitung gibt es unter:
http://www.ftcommunity.de/data/downloads/bauanleitungen/anleitungfischertechnik3drucker2008.pdf