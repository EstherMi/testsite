---
layout: "comment"
hidden: true
title: "8645"
date: "2009-03-01T22:24:50"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Sehe gerade du hast schon geantwortet.
Die "BS15 mit Bohrung" - ich korrigiere mich - sind natürlich Schneckenmuttern in X (lange Achse) ... :o)
Da könnten ihre Schwenkzapfen in der Alunut laufen, was dann in Y eine Stabilisierung wäre. Das habe ich schon mal zum Leichtlauf mit 0,5mm geschlitzten Drehzapfen erfolgreich probiert. Ist aber abhängig von der Toleranz der Nutweite bei den Alus. Ein Mitlaufen des Lüfters mit der Z-Achse (Tischhub) wäre wohl dann das Beste.