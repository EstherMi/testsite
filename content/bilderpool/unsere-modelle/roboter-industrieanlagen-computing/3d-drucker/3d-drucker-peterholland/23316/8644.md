---
layout: "comment"
hidden: true
title: "8644"
date: "2009-03-01T22:19:09"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Hallo Udo,

Nur in der X-Achse gleiten die BS15 mit Bohrung auf dem 2 Alu-Profilen.

Zur Stabilisierung der Y-Asche reichen  die 3 Führungsstangen zu meiner Zufriedenheit aus.

Die Anordnung des Lüfters auf dem Bilder ist eigentlich falsch. Ich habe die Fan jetzt hoher befestigt damit das Werkstück besser gekuhlt wird, wie auch beim Modell Andreas Rozek.  Bevor wurde bei meiner Modell die Umgebung unten das Glas gekuhlt. 

Die Anordnung des Lüfters auf dem Bilder Andreas Gürten gleicht auf dem bild auch niedrig montiert. Da wurde vorallem die Umgebung unten das Glas gekuhlt. 

Gruss,

Peter