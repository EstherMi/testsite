---
layout: "image"
title: "1e Product “FT-3D-Drucker-Poederoyen-NL”:  Trechter"
date: "2009-03-04T21:18:57"
picture: "3D-Drucker-Poederoyen-NL-funnel-techter_035.jpg"
weight: "26"
konstrukteure: 
- "Peter Damen (Poederoyen-NL)"
fotografen:
- "Peter Damen (Poederoyen-NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/23374
imported:
- "2019"
_4images_image_id: "23374"
_4images_cat_id: "1585"
_4images_user_id: "22"
_4images_image_date: "2009-03-04T21:18:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23374 -->
