---
layout: "comment"
hidden: true
title: "8677"
date: "2009-03-05T19:20:06"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Hallo Udo,

Meine "Glassplatte" ist eine 3mm Messing-platte. 
Bis zum Topf der Trichter ist ohne Fan-Kuhlung. Gerade beim Topf war Fan-Kuhlung sehr notwendig; zu viel Wärme.

Iche nutze eine Serial-Kabel zum Serial-Anschluss an meinem PC. 

Anwendung funktioniert nur im "Online"-Modus des fischertechnik Interface (zum zeitgenau ansteuern der Schrittmotoren. Man benötigt also entweder eine serielle Schnittstelle oder einen USB-Seriell-Konverter.

Schau auch mal unter: 
http://www.fischertechnik.de/de/fanclub/forum/default.aspx?g=posts&t=3750&p=2

Gruss,

Peter