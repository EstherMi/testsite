---
layout: "image"
title: "“Fischertechnik-3D-Drucker-Poederoyen-NL”"
date: "2009-03-01T20:05:03"
picture: "3D-Drucker-Poederoyen-NL_004.jpg"
weight: "30"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/23318
imported:
- "2019"
_4images_image_id: "23318"
_4images_cat_id: "1585"
_4images_user_id: "22"
_4images_image_date: "2009-03-01T20:05:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23318 -->
