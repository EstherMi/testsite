---
layout: "image"
title: "Roboter-Tiefbett-Schlitten-03"
date: "2012-02-18T13:28:17"
picture: "Schlitten-03.jpg"
weight: "3"
konstrukteure: 
- "SkobyMobil"
fotografen:
- "SkobyMobil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "SkobyMobil"
license: "unknown"
legacy_id:
- details/34210
imported:
- "2019"
_4images_image_id: "34210"
_4images_cat_id: "2534"
_4images_user_id: "1407"
_4images_image_date: "2012-02-18T13:28:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34210 -->
hier ist zu sehen, wie flach der Schlitten tatsächlich ist. Ich weiß nur noch nicht, wie ich den Drehkranz antreibe. Befestige ich den Motor am Schlitten- oder befestige ich den Motor am Drehkranz...?