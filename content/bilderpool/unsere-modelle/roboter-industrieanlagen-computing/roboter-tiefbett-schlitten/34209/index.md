---
layout: "image"
title: "Roboter-Tiefbett-Schlitten-02"
date: "2012-02-18T13:28:17"
picture: "Schlitten-02.jpg"
weight: "2"
konstrukteure: 
- "SkobyMobil"
fotografen:
- "SkobyMobil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "SkobyMobil"
license: "unknown"
legacy_id:
- details/34209
imported:
- "2019"
_4images_image_id: "34209"
_4images_cat_id: "2534"
_4images_user_id: "1407"
_4images_image_date: "2012-02-18T13:28:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34209 -->
hier sind die Laurollen des Schlitten zu sehen. Ich hatte erst Hülsen benutzt. Dann bin ich auf Silikonschlauch gekommen, jeder Millimeter zählt. Flacher geht nicht. Die Achsen laufen in 7,5mm Bausteinen. Auf diesen Bausteinen und den Achsen lagert der Drehkranz. Der Rollwiderstand geht so fast gegen Null. Durch den tiefen Schwerpunkt und den breiten Abstand der Laufrollen ist ein "Umkippen" fast unmöglich.