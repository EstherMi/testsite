---
layout: "image"
title: "Roboter-Tiefbett-Schlitten-05"
date: "2012-02-18T13:28:17"
picture: "Schlitten-05.jpg"
weight: "5"
konstrukteure: 
- "SkobyMobil"
fotografen:
- "SkobyMobil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "SkobyMobil"
license: "unknown"
legacy_id:
- details/34212
imported:
- "2019"
_4images_image_id: "34212"
_4images_cat_id: "2534"
_4images_user_id: "1407"
_4images_image_date: "2012-02-18T13:28:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34212 -->
hier ist der Antrieb durch die Schnecke zu sehen, auch die Führung durch die Bausteine. Ich hoffe es hat gefallen. Gruß und Spaß, Andreas