---
layout: "comment"
hidden: true
title: "20228"
date: "2015-02-22T19:23:41"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Absolut super, fürs die Schneiden a) die Kraft durch die leichte Schräge und b) den Schneideeffekt (anstatt nur Durchdrücken) durch die Querbewegung auszunutzen. Mechanik at it's best!
Gruß,
Stefan