---
layout: "image"
title: "vor dem Schnitt"
date: "2015-02-22T19:09:59"
picture: "DS_06_vor_dem_Schnitt.jpg"
weight: "6"
konstrukteure: 
- "Mi_ch_ae_l   Se_ng_st_sc_hm_id"
fotografen:
- "Mi_ch_ae_l   Se_ng_st_sc_hm_id"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/40592
imported:
- "2019"
_4images_image_id: "40592"
_4images_cat_id: "3037"
_4images_user_id: "765"
_4images_image_date: "2015-02-22T19:09:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40592 -->
