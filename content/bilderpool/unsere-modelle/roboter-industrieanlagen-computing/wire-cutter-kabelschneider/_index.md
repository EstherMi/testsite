---
layout: "overview"
title: "Wire Cutter / Kabelschneider"
date: 2019-12-17T19:08:16+01:00
legacy_id:
- categories/3037
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3037 --> 
Halbautomatischer Kabel- / Litzenschneider
Man stellt Länge und Menge (am TX-Controller) ein
Dann wird die Litze um den eingestellten Wert weiter transportiert
Nun kann man die Litze mit dem Seitenscheider abschneiden