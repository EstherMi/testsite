---
layout: "image"
title: "Einrichtbetrieb"
date: "2015-02-12T22:59:16"
picture: "5_Einrichtbetrieb.jpg"
weight: "12"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/40533
imported:
- "2019"
_4images_image_id: "40533"
_4images_cat_id: "3037"
_4images_user_id: "724"
_4images_image_date: "2015-02-12T22:59:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40533 -->
Man drückt beliebig oft den Taster und kann nun die Litze einfädeln
Der Motor stoppt immer beim Loslassen des Tasters