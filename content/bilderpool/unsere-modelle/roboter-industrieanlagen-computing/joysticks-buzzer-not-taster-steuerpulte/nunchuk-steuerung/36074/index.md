---
layout: "image"
title: "Nunchuk am TX"
date: "2012-10-26T23:10:29"
picture: "nunchuksteuerung1.jpg"
weight: "1"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/36074
imported:
- "2019"
_4images_image_id: "36074"
_4images_cat_id: "2684"
_4images_user_id: "1126"
_4images_image_date: "2012-10-26T23:10:29"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36074 -->
Der Nintendo Nunchuk enthält einen Joystick, zwei Taster und einen 3D-Beschleunigungssensor, die sich via I²C-Protokoll mit 400 kbit/s auslesen lassen - und das für unschlagbare 8 Euro.
Mit ein wenig Bastelei lässt sich der Nunchuk sogar "lötfrei" an den I²C-Ausgang des TX anschließen. Schöner wäre noch ein Spannungswandler, da der Sensor so mit 5 statt der vorgesehenen 3,3 V betrieben wird. Im Download-Mode lässt sich der Nunchuk in Echtzeit auslesen. 

Das Testprogramm zeigt die Joystick-Werte (0-255, 8 bit), die Taster C und Z sowie die Werte des Beschleunigungssensors (0-1023, 10 bit) an.

Dokumentation und RoboPro-Treiber folgen.