---
layout: "comment"
hidden: true
title: "22144"
date: "2016-06-15T20:52:38"
uploadBy:
- "uffi"
license: "unknown"
imported:
- "2019"
---
Diese 3D Joysticks gibt es auf Aliexpress: 

http://de.aliexpress.com/item/3D-Button-Analog-Joystick-Stick-Cap-Control-for-Sony-PSP-2000-Black/32651802091.html?spm=2114.13010208.99999999.264.Yjlvrp 

Sind 3D analoge Joysticks, die auch in der Sony PSP2000 verwendet werden.
Diese Joysticks sind schön flach, können mit einem einzigen Finger oder Daumen bedient werden und Kosten nur 1 ¤ / Stück.