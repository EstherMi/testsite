---
layout: "image"
title: "Koordinatenschalter"
date: "2008-01-23T21:41:00"
picture: "Koordinatenschalter-2_003.jpg"
weight: "44"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/13373
imported:
- "2019"
_4images_image_id: "13373"
_4images_cat_id: "909"
_4images_user_id: "22"
_4images_image_date: "2008-01-23T21:41:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13373 -->
