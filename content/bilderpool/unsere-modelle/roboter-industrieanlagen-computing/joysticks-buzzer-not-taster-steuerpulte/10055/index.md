---
layout: "image"
title: "Joistik 1"
date: "2007-04-11T17:01:07"
picture: "joistiks1.jpg"
weight: "51"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/10055
imported:
- "2019"
_4images_image_id: "10055"
_4images_cat_id: "909"
_4images_user_id: "445"
_4images_image_date: "2007-04-11T17:01:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10055 -->
Das ist der Joistik der sich bei mir am besten bewährt hat, Er ist theoretisch eine Grundform.