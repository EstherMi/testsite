---
layout: "image"
title: "Fischertechnik Koordinaten Schalter mit (alte) FT-Feder"
date: "2008-01-20T19:57:07"
picture: "Koordinatenschalter-1.jpg"
weight: "50"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/13363
imported:
- "2019"
_4images_image_id: "13363"
_4images_cat_id: "909"
_4images_user_id: "22"
_4images_image_date: "2008-01-20T19:57:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13363 -->
Fischertechnik Koordinaten Schalter mit (alte) FT-Feder