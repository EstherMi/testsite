---
layout: "image"
title: "Joystick"
date: "2008-03-23T21:08:22"
picture: "joystick_porsche_makus.jpg"
weight: "42"
konstrukteure: 
- "Porsche-Makus"
fotografen:
- "Porsche-Makus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Porsche-Makus"
license: "unknown"
legacy_id:
- details/14043
imported:
- "2019"
_4images_image_id: "14043"
_4images_cat_id: "909"
_4images_user_id: "327"
_4images_image_date: "2008-03-23T21:08:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14043 -->
Hallo,

hier ein Bild meines Joystick's. Ich habe für den 4-Punkt-Joysticks links im Bild einfach den Kardanwürfel verwendet.