---
layout: "image"
title: "Not aus Schalter das Programm 2"
date: "2006-12-15T17:14:12"
picture: "notausschalter2_2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/7910
imported:
- "2019"
_4images_image_id: "7910"
_4images_cat_id: "740"
_4images_user_id: "502"
_4images_image_date: "2006-12-15T17:14:12"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7910 -->
Hier ist Programm wenn der Motor ein bestimmte Zeit lang laufen soll. Alle 0,01 sekunden wird von der variablen 0,01 abgezogen bis sie den wert 0 hat. Wenn nun Taster 8 (not aus) gedrückt wird stoppt der Motor und von der Variable wird auch nichts mehr abgezogen erst wenn man wieder not aus drückt läuft der Motor nach 0,25 sekunden wieder.