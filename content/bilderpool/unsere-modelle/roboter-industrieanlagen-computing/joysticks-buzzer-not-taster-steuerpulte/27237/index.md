---
layout: "image"
title: "Joystick 10"
date: "2010-05-15T23:49:44"
picture: "joystick10.jpg"
weight: "15"
konstrukteure: 
- "Matthias"
fotografen:
- "Matthias"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/27237
imported:
- "2019"
_4images_image_id: "27237"
_4images_cat_id: "909"
_4images_user_id: "860"
_4images_image_date: "2010-05-15T23:49:44"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27237 -->
und nochmal ohne den Hebel