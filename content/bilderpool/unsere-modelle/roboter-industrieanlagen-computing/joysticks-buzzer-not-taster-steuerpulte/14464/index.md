---
layout: "image"
title: "joystick_9"
date: "2008-05-04T14:56:37"
picture: "joystick_9.jpg"
weight: "36"
konstrukteure: 
- "pk"
fotografen:
- "pk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/14464
imported:
- "2019"
_4images_image_id: "14464"
_4images_cat_id: "909"
_4images_user_id: "144"
_4images_image_date: "2008-05-04T14:56:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14464 -->
