---
layout: "overview"
title: "Autowaschstraße"
date: 2019-12-17T19:07:44+01:00
legacy_id:
- categories/2468
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2468 --> 
Meine FT-Autowaschstraße simuliert eine vollautomatische Wasch- und Trocknungsanlage für PKW, wie sie beispielsweise an Tankstellen und in Werkstätten eingesetzt wird. Die Autowaschstraße besteht aus einem fahrbaren Portal, an dem ein Gebläsetrockner, eine Horizontalbürste, zwei Vertikalbürsten und zwei Felgenbürsten beweglich angebracht sind. Im Simulationsablauf wird ein PKW-Modell "gewaschen" und "getrocknet".