---
layout: "image"
title: "Fischertechnik Autowaschstrasse"
date: "2011-10-26T17:53:53"
picture: "autowaschstrasse11.jpg"
weight: "11"
konstrukteure: 
- "Peter   Poederoyen NL"
fotografen:
- "Peter  Poederoyen  NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/33332
imported:
- "2019"
_4images_image_id: "33332"
_4images_cat_id: "2468"
_4images_user_id: "22"
_4images_image_date: "2011-10-26T17:53:53"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33332 -->
.	Anschließend fahren die beiden Vertikalbürsten gerade zum PKW.   Position der Vertikalbürsten wird von einer Schiebepotentiometer bestimmt : gleich die  Distanz-Sensorwert (D1A).  Es gibt grosse und kleinere PKW !!!
http://www.conrad.de/ce/de/product/442121/SCHIEBEPOTENTIOMETER
