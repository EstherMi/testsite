---
layout: "image"
title: "Fischertechnik Autowaschstrasse"
date: "2011-10-26T17:53:53"
picture: "autowaschstrasse07.jpg"
weight: "7"
konstrukteure: 
- "Peter   Poederoyen NL"
fotografen:
- "Peter  Poederoyen  NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/33328
imported:
- "2019"
_4images_image_id: "33328"
_4images_cat_id: "2468"
_4images_user_id: "22"
_4images_image_date: "2011-10-26T17:53:53"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33328 -->
Das Portal  bewegt sich um die Vertikalbürsten in die Position der Fahrzeug-hintenseite zu bringen. Ich nutze eine Distanz-Sensor (D1A) dafür. Die Vertikalbürsten beginnen sich zu drehen und aufeinander zuzufahren. Nach dem Aufeinandertreffen der beiden Vertikalbürsten, das durch einen Reed-Schalter erfasst wird, führen die beiden Vertikalbürsten zusammen eine Hin- und Herbewegung aus, die durch mechanische Endtaster begrenzt wird.