---
layout: "image"
title: "Seitenansicht / schräg"
date: "2014-04-04T22:26:58"
picture: "IMG_0018.jpg"
weight: "1"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/38527
imported:
- "2019"
_4images_image_id: "38527"
_4images_cat_id: "2876"
_4images_user_id: "1359"
_4images_image_date: "2014-04-04T22:26:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38527 -->
"Spalten" sind in gelb / roter Stecker verkabelt, "Zeilen" in grün/grün