---
layout: "image"
title: "Training Roboter Original"
date: "2011-05-14T22:07:37"
picture: "DSC00646.jpg"
weight: "4"
konstrukteure: 
- "lMarspau"
fotografen:
- "Marspau"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "marspau"
license: "unknown"
legacy_id:
- details/30557
imported:
- "2019"
_4images_image_id: "30557"
_4images_cat_id: "125"
_4images_user_id: "416"
_4images_image_date: "2011-05-14T22:07:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30557 -->
Also showing the lamps