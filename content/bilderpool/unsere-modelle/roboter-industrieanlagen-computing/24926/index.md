---
layout: "image"
title: "Robo Bug"
date: "2009-09-18T20:32:46"
picture: "sm_roach1.jpg"
weight: "21"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["PCS", "BRAIN"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/24926
imported:
- "2019"
_4images_image_id: "24926"
_4images_cat_id: "125"
_4images_user_id: "585"
_4images_image_date: "2009-09-18T20:32:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24926 -->
This is a light-fleeing robot integrating the PCS BRAIN. This was an early version of the model.