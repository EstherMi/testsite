---
layout: "image"
title: "Taster gedrückt"
date: "2014-06-03T18:59:30"
picture: "Oberflche_Taster_gedrckt.jpg"
weight: "4"
konstrukteure: 
- "bauFischertechnik"
fotografen:
- "bauFischertechnik"
keywords: ["ROBOPro", "Taster", "Eigendiagnose", "TX", "Controller", "Wiederstand"]
uploadBy: "bauFischertechnik"
license: "unknown"
legacy_id:
- details/38907
imported:
- "2019"
_4images_image_id: "38907"
_4images_cat_id: "2909"
_4images_user_id: "2086"
_4images_image_date: "2014-06-03T18:59:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38907 -->
Lämpchen bestätigt Signal