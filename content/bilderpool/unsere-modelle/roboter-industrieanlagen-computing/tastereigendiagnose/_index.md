---
layout: "overview"
title: "Tastereigendiagnose"
date: 2019-12-17T19:08:10+01:00
legacy_id:
- categories/2909
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2909 --> 
Programmierung eines Tasters, welcher auf Funktionstüchtigkeit überprüft wird.