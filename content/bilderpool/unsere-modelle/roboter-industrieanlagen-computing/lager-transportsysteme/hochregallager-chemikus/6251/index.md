---
layout: "image"
title: "Hochregallager, Knickarmrobot"
date: "2006-05-09T22:24:22"
picture: "PICT4.jpg"
weight: "4"
konstrukteure: 
- "Uwe Timm / Chemikus"
fotografen:
- "Uwe Timm / Chemikus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Chemikus"
license: "unknown"
legacy_id:
- details/6251
imported:
- "2019"
_4images_image_id: "6251"
_4images_cat_id: "1082"
_4images_user_id: "156"
_4images_image_date: "2006-05-09T22:24:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6251 -->
