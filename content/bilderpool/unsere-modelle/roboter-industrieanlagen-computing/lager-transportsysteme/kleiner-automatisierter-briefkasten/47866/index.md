---
layout: "image"
title: "Fächer-Detail"
date: "2018-09-08T19:16:53"
picture: "kleinerautomatisierterbriefkasten12.jpg"
weight: "13"
konstrukteure: 
- "ba-ft-ler"
fotografen:
- "ba-ft-ler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ba-ft-ler"
license: "unknown"
legacy_id:
- details/47866
imported:
- "2019"
_4images_image_id: "47866"
_4images_cat_id: "3530"
_4images_user_id: "2197"
_4images_image_date: "2018-09-08T19:16:53"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47866 -->
Das sind die Fächer. Oben sieht man den Motor, der den Eunwurf öffnet, oder schließt. Der Schieber hängt leider etwas schief, aber ich hatte keine Lösung parat...Unten sieht man wieder einmal die Lichtübermittlung
.