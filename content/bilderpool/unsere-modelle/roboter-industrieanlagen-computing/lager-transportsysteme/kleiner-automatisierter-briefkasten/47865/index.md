---
layout: "image"
title: "Kartenleser-Detail"
date: "2018-09-08T19:16:53"
picture: "kleinerautomatisierterbriefkasten11.jpg"
weight: "12"
konstrukteure: 
- "ba-ft-ler"
fotografen:
- "ba-ft-ler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ba-ft-ler"
license: "unknown"
legacy_id:
- details/47865
imported:
- "2019"
_4images_image_id: "47865"
_4images_cat_id: "3530"
_4images_user_id: "2197"
_4images_image_date: "2018-09-08T19:16:53"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47865 -->
Der Kartenleser besteht grundsätzlich aus vier Teilen: Das Rad zieht die Karte an, bzw. spuckt sie wieder aus und wird mit einem normlen XS-Motor angetrieben. Die Lichtschranke (Man sieht den gelben Fototransistor und darunter die Lampe) erkennt, wenn eine Karte auf dem "Tisch" liegt. Die Kamera erkennt die Karte schlussendlich. Wenn die Kamera noch nichts erkennt, wird die Karte noch drei mal weiter eingezogen, bis sie wieder ausgeworfen wird. Das vierte Teil ist auf der anderen Seite noch eine kleine Beleuchtungslampe, die übrigens mit der orangen Lampe auf der vorderseite wg. mangelnder Anschlüsse gekoppelt ist.