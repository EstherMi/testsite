---
layout: "image"
title: "TX-Controller"
date: "2018-09-08T19:16:53"
picture: "kleinerautomatisierterbriefkasten16.jpg"
weight: "17"
konstrukteure: 
- "ba-ft-ler"
fotografen:
- "ba-ft-ler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ba-ft-ler"
license: "unknown"
legacy_id:
- details/47870
imported:
- "2019"
_4images_image_id: "47870"
_4images_cat_id: "3530"
_4images_user_id: "2197"
_4images_image_date: "2018-09-08T19:16:53"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47870 -->
Das ist der Tx. Er hat etwas weniger Kabel, muss aber auch nur den Part mit den Fächern übernehmen. Außerdem läuft der über die Stromversorgung vom TxT mit, weil ich nur ein Netzteil verwenden wollte...und es funktioniert gut!