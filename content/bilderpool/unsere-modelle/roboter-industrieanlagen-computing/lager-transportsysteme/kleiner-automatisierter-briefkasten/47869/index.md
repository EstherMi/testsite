---
layout: "image"
title: "TXT-Controller"
date: "2018-09-08T19:16:53"
picture: "kleinerautomatisierterbriefkasten15.jpg"
weight: "16"
konstrukteure: 
- "ba-ft-ler"
fotografen:
- "ba-ft-ler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ba-ft-ler"
license: "unknown"
legacy_id:
- details/47869
imported:
- "2019"
_4images_image_id: "47869"
_4images_cat_id: "3530"
_4images_user_id: "2197"
_4images_image_date: "2018-09-08T19:16:53"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47869 -->
Das ist der TxT. Leider habe ich einen Stecker und ein Kabel, das nicht von Fischertechnik ist verwendet...aber es geht genauso mit Original Teilen.