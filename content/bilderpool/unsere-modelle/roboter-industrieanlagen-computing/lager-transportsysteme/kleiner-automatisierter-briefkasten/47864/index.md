---
layout: "image"
title: "Fächer-Vorne"
date: "2018-09-08T19:16:53"
picture: "kleinerautomatisierterbriefkasten10.jpg"
weight: "11"
konstrukteure: 
- "ba-ft-ler"
fotografen:
- "ba-ft-ler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ba-ft-ler"
license: "unknown"
legacy_id:
- details/47864
imported:
- "2019"
_4images_image_id: "47864"
_4images_cat_id: "3530"
_4images_user_id: "2197"
_4images_image_date: "2018-09-08T19:16:53"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47864 -->
Das sind die - zugegeben kleinen - Fächer von vorne. Ich hätte die roten Abdeckplatten gerne komplett außen herum und auch an den Seiten des ganzen befestigt, aber ich hatte leider nicht genug Teile.