---
layout: "image"
title: "Gesamt-Schräg"
date: "2018-09-08T19:16:53"
picture: "kleinerautomatisierterbriefkasten02.jpg"
weight: "3"
konstrukteure: 
- "ba-ft-ler"
fotografen:
- "ba-ft-ler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ba-ft-ler"
license: "unknown"
legacy_id:
- details/47856
imported:
- "2019"
_4images_image_id: "47856"
_4images_cat_id: "3530"
_4images_user_id: "2197"
_4images_image_date: "2018-09-08T19:16:53"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47856 -->
