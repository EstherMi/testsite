---
layout: "image"
title: "Tasten zur HRL Steuerung"
date: "2006-03-05T11:13:46"
picture: "DSC01455.jpg"
weight: "1"
konstrukteure: 
- "Felix und Julian"
fotografen:
- "Felix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "felix"
license: "unknown"
legacy_id:
- details/5809
imported:
- "2019"
_4images_image_id: "5809"
_4images_cat_id: "1083"
_4images_user_id: "410"
_4images_image_date: "2006-03-05T11:13:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5809 -->
Das sind drei Tasten, daneben noch zwei "nackte Taster.