---
layout: "image"
title: "3D-Roboter"
date: "2006-02-25T18:38:18"
picture: "DSC01469.jpg"
weight: "4"
konstrukteure: 
- "Felix und Julian"
fotografen:
- "Felix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "felix"
license: "unknown"
legacy_id:
- details/5794
imported:
- "2019"
_4images_image_id: "5794"
_4images_cat_id: "1083"
_4images_user_id: "410"
_4images_image_date: "2006-02-25T18:38:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5794 -->
