---
layout: "image"
title: "Industrietransportsystem"
date: "2013-02-27T20:52:49"
picture: "industrie5.jpg"
weight: "5"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/36705
imported:
- "2019"
_4images_image_id: "36705"
_4images_cat_id: "2721"
_4images_user_id: "453"
_4images_image_date: "2013-02-27T20:52:49"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36705 -->
