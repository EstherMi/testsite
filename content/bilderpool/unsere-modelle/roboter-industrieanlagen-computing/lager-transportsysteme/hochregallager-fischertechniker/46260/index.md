---
layout: "image"
title: "Die Schienen"
date: "2017-09-17T18:02:23"
picture: "hochregallager12.jpg"
weight: "12"
konstrukteure: 
- "alwin"
fotografen:
- "Alwin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischertechniker"
license: "unknown"
legacy_id:
- details/46260
imported:
- "2019"
_4images_image_id: "46260"
_4images_cat_id: "3432"
_4images_user_id: "2770"
_4images_image_date: "2017-09-17T18:02:23"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46260 -->
Das RBG läuft auf Zahnschienen.