---
layout: "image"
title: "Noch mehr Kabel"
date: "2017-09-17T18:02:23"
picture: "hochregallager14.jpg"
weight: "14"
konstrukteure: 
- "alwin"
fotografen:
- "Alwin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischertechniker"
license: "unknown"
legacy_id:
- details/46262
imported:
- "2019"
_4images_image_id: "46262"
_4images_cat_id: "3432"
_4images_user_id: "2770"
_4images_image_date: "2017-09-17T18:02:23"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46262 -->
der TXT bekommt seinen Strom über ein Netzteil.