---
layout: "image"
title: "Das Regal von hinten"
date: "2017-09-17T18:02:23"
picture: "hochregallager15.jpg"
weight: "15"
konstrukteure: 
- "alwin"
fotografen:
- "Alwin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischertechniker"
license: "unknown"
legacy_id:
- details/46263
imported:
- "2019"
_4images_image_id: "46263"
_4images_cat_id: "3432"
_4images_user_id: "2770"
_4images_image_date: "2017-09-17T18:02:23"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46263 -->
Von hinten ist das Regal verstrebt.