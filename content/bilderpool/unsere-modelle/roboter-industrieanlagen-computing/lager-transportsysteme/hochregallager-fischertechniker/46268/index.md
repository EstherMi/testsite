---
layout: "image"
title: "Antrieb"
date: "2017-09-17T18:02:23"
picture: "hochregallager20.jpg"
weight: "20"
konstrukteure: 
- "alwin"
fotografen:
- "Alwin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischertechniker"
license: "unknown"
legacy_id:
- details/46268
imported:
- "2019"
_4images_image_id: "46268"
_4images_cat_id: "3432"
_4images_user_id: "2770"
_4images_image_date: "2017-09-17T18:02:23"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46268 -->
Die X-Achse wird über einen kleinen Motor angetrieben.