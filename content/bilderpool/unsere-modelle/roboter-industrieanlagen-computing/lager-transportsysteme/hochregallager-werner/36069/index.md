---
layout: "image"
title: "Regal"
date: "2012-10-22T21:09:23"
picture: "hochregallagerwerner27.jpg"
weight: "27"
konstrukteure: 
- "Jan Werner"
fotografen:
- "Jan Werner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "werner"
license: "unknown"
legacy_id:
- details/36069
imported:
- "2019"
_4images_image_id: "36069"
_4images_cat_id: "2683"
_4images_user_id: "1196"
_4images_image_date: "2012-10-22T21:09:23"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36069 -->
