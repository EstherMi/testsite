---
layout: "comment"
hidden: true
title: "17475"
date: "2012-10-27T17:52:06"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Schöne scharfe Fotos und eine gut nachvollziehbare Dokumentation Deiner Konstruktion.
Das sollte Nachahmer finden...
Beste Grüße,
Dirk