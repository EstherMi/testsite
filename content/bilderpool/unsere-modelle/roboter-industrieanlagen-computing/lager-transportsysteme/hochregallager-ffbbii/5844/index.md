---
layout: "image"
title: "HRL Roboter"
date: "2006-03-07T18:20:04"
picture: "Fischertechnik_013.jpg"
weight: "1"
konstrukteure: 
- "Jakob Wolf"
fotografen:
- "Jakob Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FFBBII"
license: "unknown"
legacy_id:
- details/5844
imported:
- "2019"
_4images_image_id: "5844"
_4images_cat_id: "497"
_4images_user_id: "420"
_4images_image_date: "2006-03-07T18:20:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5844 -->
