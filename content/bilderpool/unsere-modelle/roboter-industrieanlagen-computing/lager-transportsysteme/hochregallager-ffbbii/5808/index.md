---
layout: "image"
title: "HRL Greifer"
date: "2006-03-04T19:32:03"
picture: "Fischertechnik_008.jpg"
weight: "6"
konstrukteure: 
- "Jakob Wolf"
fotografen:
- "Jakob Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FFBBII"
license: "unknown"
legacy_id:
- details/5808
imported:
- "2019"
_4images_image_id: "5808"
_4images_cat_id: "497"
_4images_user_id: "420"
_4images_image_date: "2006-03-04T19:32:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5808 -->
