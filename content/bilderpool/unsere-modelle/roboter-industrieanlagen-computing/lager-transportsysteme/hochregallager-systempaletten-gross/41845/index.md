---
layout: "image"
title: "Unteransicht"
date: "2015-08-19T16:44:51"
picture: "hrlsystempaletten13.jpg"
weight: "13"
konstrukteure: 
- "Matthias10"
fotografen:
- "Matthias10"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Matthias10"
license: "unknown"
legacy_id:
- details/41845
imported:
- "2019"
_4images_image_id: "41845"
_4images_cat_id: "3115"
_4images_user_id: "1200"
_4images_image_date: "2015-08-19T16:44:51"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41845 -->
Man sieht hier die Statikträger. Hätte ich Grundbausteine verwendet wäre es sogar noch schwerer geworden. Statikträger sind zum Glück leicht und recht stabil