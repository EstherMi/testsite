---
layout: "image"
title: "Alu"
date: "2015-08-19T16:44:51"
picture: "hrlsystempaletten14.jpg"
weight: "14"
konstrukteure: 
- "Matthias10"
fotografen:
- "Matthias10"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Matthias10"
license: "unknown"
legacy_id:
- details/41846
imported:
- "2019"
_4images_image_id: "41846"
_4images_cat_id: "3115"
_4images_user_id: "1200"
_4images_image_date: "2015-08-19T16:44:51"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41846 -->
Das Aluprofil muss man nicht unbedingt verwenden aber ich wollte es der Optik wegen und als durchgängige Gleitfläche ohne Übergänge wie bei Grundbausteinen