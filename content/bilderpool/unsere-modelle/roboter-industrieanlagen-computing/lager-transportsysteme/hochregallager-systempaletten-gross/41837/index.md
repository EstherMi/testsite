---
layout: "image"
title: "Gewicht"
date: "2015-08-19T16:44:51"
picture: "hrlsystempaletten05.jpg"
weight: "5"
konstrukteure: 
- "Matthias10"
fotografen:
- "Matthias10"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Matthias10"
license: "unknown"
legacy_id:
- details/41837
imported:
- "2019"
_4images_image_id: "41837"
_4images_cat_id: "3115"
_4images_user_id: "1200"
_4images_image_date: "2015-08-19T16:44:51"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41837 -->
Der Korb wie ich ihn jetzt einfach nenne ist einfach extrem schwer. Ich habe versucht weitere Kassetten mit Gegengewicht auf der anderen Seite wie bei einem Aufzug zu befestigen welche mit hoch- und runterfahren sollten. Das wollte ich wegen dem benötigten Platz nicht machen, weshalb der Motor die komplette Last des Korbes zieht. Der 50:1 Powermotor ist zum Glück stark genug dafür.