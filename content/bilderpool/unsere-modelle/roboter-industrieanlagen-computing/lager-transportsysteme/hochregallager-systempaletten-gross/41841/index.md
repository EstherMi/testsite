---
layout: "image"
title: "Lagerposition"
date: "2015-08-19T16:44:51"
picture: "hrlsystempaletten09.jpg"
weight: "9"
konstrukteure: 
- "Matthias10"
fotografen:
- "Matthias10"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Matthias10"
license: "unknown"
legacy_id:
- details/41841
imported:
- "2019"
_4images_image_id: "41841"
_4images_cat_id: "3115"
_4images_user_id: "1200"
_4images_image_date: "2015-08-19T16:44:51"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41841 -->
So liegt die Palette im Regal. Auch hier sind schräge Bausteine verwendet um die Palette aufzunehmen, dieses mal jedoch ohne eine Positionierung. Lediglich als verstellbare seitliche Aufnahmepunkte