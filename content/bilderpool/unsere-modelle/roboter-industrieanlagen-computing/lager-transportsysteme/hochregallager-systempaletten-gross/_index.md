---
layout: "overview"
title: "Hochregallager für Systempaletten übertrieben groß"
date: 2019-12-17T19:06:53+01:00
legacy_id:
- categories/3115
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3115 --> 
Hier ist das Modell eines Hochregallagers für spezielle Paletten zu sehen. Ich habe das Projekt abgebrochen und zeige hier vor allem den Mechanismus zum Ein- oder Auslagern der Paletten in ein Regalfach. Wichtig war mir, dass ich ein Doppelseitiges Regallager baue. Der Hauptgrund für das Zerlegen ist, dass ich die Teile benötige und es zu viel Platz in diesem Raum nimmt und ich ein nützlicheres Alltagsmodell baue das auch eine Lagerfunktion hat.