---
layout: "image"
title: "Teleskopmechanismus"
date: "2015-08-19T16:44:52"
picture: "hrlsystempaletten28.jpg"
weight: "28"
konstrukteure: 
- "Matthias10"
fotografen:
- "Matthias10"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Matthias10"
license: "unknown"
legacy_id:
- details/41860
imported:
- "2019"
_4images_image_id: "41860"
_4images_cat_id: "3115"
_4images_user_id: "1200"
_4images_image_date: "2015-08-19T16:44:52"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41860 -->
Erneut eine Aufnahme bei ausgefahrenem Teleskop