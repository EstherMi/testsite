---
layout: "image"
title: "Führung ausgefahren"
date: "2015-08-19T16:44:52"
picture: "hrlsystempaletten24.jpg"
weight: "24"
konstrukteure: 
- "Matthias10"
fotografen:
- "Matthias10"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Matthias10"
license: "unknown"
legacy_id:
- details/41856
imported:
- "2019"
_4images_image_id: "41856"
_4images_cat_id: "3115"
_4images_user_id: "1200"
_4images_image_date: "2015-08-19T16:44:52"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41856 -->
Hier seht ihr den Statikträger der die Kräfte aufnimmt und gegen Abknicken hilft