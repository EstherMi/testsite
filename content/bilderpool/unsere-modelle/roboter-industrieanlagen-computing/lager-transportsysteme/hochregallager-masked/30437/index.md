---
layout: "image"
title: "Rückseite"
date: "2011-04-10T18:43:01"
picture: "hrl3.jpg"
weight: "3"
konstrukteure: 
- "Martin W. (Masked)"
fotografen:
- "Martin W. (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/30437
imported:
- "2019"
_4images_image_id: "30437"
_4images_cat_id: "2263"
_4images_user_id: "373"
_4images_image_date: "2011-04-10T18:43:01"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30437 -->
unverändert gegenüber Bauanleitung