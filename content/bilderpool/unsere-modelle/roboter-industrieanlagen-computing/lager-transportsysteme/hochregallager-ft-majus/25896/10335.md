---
layout: "comment"
hidden: true
title: "10335"
date: "2009-12-07T16:44:24"
uploadBy:
- "fitec"
license: "unknown"
imported:
- "2019"
---
Hi,
sehr schönes Modell, schaue ich mir immer wieder gern an. Wie ich sehe findet die Idee der Kaugummidose als Lufttank so langsam Verbreitung ;-)
Ich schätz mal, dass wenn der Saugnapf auf die Kiste aufkommt, solange draufdrückt, bis sich der Saugnapf verbogen hat ?
Bei meinem ehemaligen Modell war das immer das Problem, dass beim andrücken des Saugnapfes auf die Kiste die ganze z-Achse (und indirekt der ganze Einlagerer) verbogen wurde.

Gruß fitec