---
layout: "image"
title: "Förderband 1"
date: "2008-06-29T18:30:37"
picture: "116_1633.jpg"
weight: "69"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- details/14786
imported:
- "2019"
_4images_image_id: "14786"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2008-06-29T18:30:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14786 -->
Auf dieses Förderband werden die Kästchen gestellt. Die Kästchen werden über eine CNY 70 erkennt.