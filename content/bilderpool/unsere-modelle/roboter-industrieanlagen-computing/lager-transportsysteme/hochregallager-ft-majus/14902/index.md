---
layout: "image"
title: "Y-Achse"
date: "2008-07-16T20:44:28"
picture: "117_1716.jpg"
weight: "56"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- details/14902
imported:
- "2019"
_4images_image_id: "14902"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2008-07-16T20:44:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14902 -->
Der Antrieb der Y-Achse erfolgt über einen P-Motor 20:1. Auf dem Bild erkennt man das Getriebe und den Impulszähler.