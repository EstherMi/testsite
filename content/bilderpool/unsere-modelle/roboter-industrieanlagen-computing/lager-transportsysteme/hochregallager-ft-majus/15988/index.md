---
layout: "image"
title: "EEprom"
date: "2008-10-14T21:37:50"
picture: "120_2039.jpg"
weight: "45"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- details/15988
imported:
- "2019"
_4images_image_id: "15988"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2008-10-14T21:37:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15988 -->
Auf der Platine links neben dem vierten Förderban befindet sich mein EEprom. Es ist der gleiche, wie der, den nula hier verbaut hat: http://ftcommunity.de/details.php?image_id=14627