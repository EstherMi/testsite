---
layout: "image"
title: "Z-Achse"
date: "2008-07-16T20:44:28"
picture: "117_1719.jpg"
weight: "57"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- details/14903
imported:
- "2019"
_4images_image_id: "14903"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2008-07-16T20:44:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14903 -->
