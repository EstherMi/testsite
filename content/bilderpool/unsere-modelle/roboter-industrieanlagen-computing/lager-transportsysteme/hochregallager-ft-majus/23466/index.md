---
layout: "image"
title: "Neu Verkabelung"
date: "2009-03-15T15:30:04"
picture: "125_2527.jpg"
weight: "36"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- details/23466
imported:
- "2019"
_4images_image_id: "23466"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2009-03-15T15:30:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23466 -->
Das gleich nur aus der Frontansicht.