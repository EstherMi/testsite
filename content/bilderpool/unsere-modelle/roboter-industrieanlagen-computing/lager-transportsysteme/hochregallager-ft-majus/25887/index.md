---
layout: "image"
title: "Auslagerung"
date: "2009-12-06T13:37:25"
picture: "HRL_5.12.09_005.jpg"
weight: "1"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- details/25887
imported:
- "2019"
_4images_image_id: "25887"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2009-12-06T13:37:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25887 -->
Auf das gelbe Podest kommen die Ausgelagerten Kästchen. Dahinter befindet sich der Kompressor für den Vakuumgreifer.