---
layout: "image"
title: "Steuerpult"
date: "2008-10-14T21:37:50"
picture: "120_2028.jpg"
weight: "46"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- details/15989
imported:
- "2019"
_4images_image_id: "15989"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2008-10-14T21:37:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15989 -->
Mein Steuerpult:
Damit kann ich das Int und das Em, die Lampe zur Farbsortierung, den Kompressor und das EEprom Einschalten.
Am Steuerpult wählt man auch zwischen der Ein -und Auslagerungsfunktion aus.