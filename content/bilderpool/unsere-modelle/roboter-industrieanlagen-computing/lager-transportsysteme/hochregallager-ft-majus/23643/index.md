---
layout: "image"
title: "Neu"
date: "2009-04-09T14:13:23"
picture: "125_2567.jpg"
weight: "28"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- details/23643
imported:
- "2019"
_4images_image_id: "23643"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2009-04-09T14:13:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23643 -->
Die Förderbänder wurden neu angeordnet. Auf das erste Förderband werden weiterhin die Kästchen draufgestellt werden. Im Tunnel wird ihre Farbe ermittelt. Über den Drehtisch und das letzte Förderband werden nun die Kästchen zum Einlagerer gebracht. Ausgelagerte Kästchen werden darüber auch wieder abtransportiert.