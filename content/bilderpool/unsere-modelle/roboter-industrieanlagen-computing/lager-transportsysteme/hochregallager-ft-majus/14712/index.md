---
layout: "image"
title: "HRL"
date: "2008-06-18T18:08:48"
picture: "115_1573.jpg"
weight: "83"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- details/14712
imported:
- "2019"
_4images_image_id: "14712"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2008-06-18T18:08:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14712 -->
Die Kästchen werden auf das erste Förderband gestellt. Anschließend gelangen sie über den Drehtisch zur Farbsortierung. Danach werden sie über den Fördertisch zum Einlagerer gebracht, der sie dann ins Regal bringt.