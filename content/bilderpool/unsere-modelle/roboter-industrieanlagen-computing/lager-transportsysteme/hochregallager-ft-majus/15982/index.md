---
layout: "image"
title: "Gesamtansicht neu"
date: "2008-10-14T21:37:49"
picture: "120_2024.jpg"
weight: "39"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- details/15982
imported:
- "2019"
_4images_image_id: "15982"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2008-10-14T21:37:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15982 -->
Die Gesamtansicht des umgebauten HRL.