---
layout: "image"
title: "Drehtisch"
date: "2009-04-13T11:49:10"
picture: "126_2639.jpg"
weight: "22"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- details/23682
imported:
- "2019"
_4images_image_id: "23682"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2009-04-13T11:49:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23682 -->
Der neue Antrieb des Drehtisches.