---
layout: "image"
title: "Ventile"
date: "2008-07-16T20:44:28"
picture: "117_1722.jpg"
weight: "58"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- details/14906
imported:
- "2019"
_4images_image_id: "14906"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2008-07-16T20:44:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14906 -->
Die Ventile sind zur Steuerung des Vakuumgreifers da. Über das erste Ventil kommt das Vakuum vom Kompressor zum Greifer. Über das zweite Ventil wird die normale Raumluft zum Greifer gepumt. Dadurch wird ein sofortiges Absetzen der Box möglich.