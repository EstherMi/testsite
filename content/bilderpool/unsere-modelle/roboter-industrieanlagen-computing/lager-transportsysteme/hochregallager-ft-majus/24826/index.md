---
layout: "image"
title: "Steuerung"
date: "2009-08-22T13:19:15"
picture: "IMG_3776.jpg"
weight: "21"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- details/24826
imported:
- "2019"
_4images_image_id: "24826"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2009-08-22T13:19:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24826 -->
Die Relais diehnen zur Motersteuerung.
Auf der Bodenplatte sind die Anschlussklemen für die Leitungen, die vom Modell kommen.