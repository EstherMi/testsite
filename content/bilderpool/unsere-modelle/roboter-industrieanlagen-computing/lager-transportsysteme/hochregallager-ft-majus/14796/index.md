---
layout: "image"
title: "Drehtisch"
date: "2008-07-04T22:47:07"
picture: "116_1653.jpg"
weight: "60"
konstrukteure: 
- "Marius Bächle"
fotografen:
- "Marius Bächle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft-majus"
license: "unknown"
legacy_id:
- details/14796
imported:
- "2019"
_4images_image_id: "14796"
_4images_cat_id: "1278"
_4images_user_id: "751"
_4images_image_date: "2008-07-04T22:47:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14796 -->
Die Positionierung des Drehtisches erfolgt über einen Taster und einen Reek-Kontakt.