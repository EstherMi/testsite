---
layout: "image"
title: "Schieber"
date: "2010-06-05T18:08:50"
picture: "streichholzschachteltransportstrasse3.jpg"
weight: "3"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/27394
imported:
- "2019"
_4images_image_id: "27394"
_4images_cat_id: "1967"
_4images_user_id: "453"
_4images_image_date: "2010-06-05T18:08:50"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27394 -->
Hier ist es im Grunde das selbe Prinzip, die beiden Taster oben drauf übernehmen das stoppen und starten.
Der Hebel schiebt die Schachteln dann weiter. Auch er macht immer eine umdrehung.