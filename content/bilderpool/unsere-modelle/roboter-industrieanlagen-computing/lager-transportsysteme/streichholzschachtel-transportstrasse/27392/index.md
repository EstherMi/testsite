---
layout: "image"
title: "Gesamtansicht"
date: "2010-06-05T18:08:49"
picture: "streichholzschachteltransportstrasse1.jpg"
weight: "1"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/27392
imported:
- "2019"
_4images_image_id: "27392"
_4images_cat_id: "1967"
_4images_user_id: "453"
_4images_image_date: "2010-06-05T18:08:49"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27392 -->
Die Streichholzschachteln werden mit Hilfe von zwei unterschiedlichen Schiebern, von denen insgesamt vier existieren, im Kreis befördert.
Das Modell wird komplett, ohne Interface, über Schalter und Schleifringe gesteuert.