---
layout: "image"
title: "Transportsystem"
date: "2011-10-12T18:45:53"
picture: "4.jpg"
weight: "4"
konstrukteure: 
- "Simon"
fotografen:
- "Simon"
keywords: ["Hochregallager", "von", "Simon"]
uploadBy: "Guest"
license: "unknown"
legacy_id:
- details/33121
imported:
- "2019"
_4images_image_id: "33121"
_4images_cat_id: "2448"
_4images_user_id: "-1"
_4images_image_date: "2011-10-12T18:45:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33121 -->
