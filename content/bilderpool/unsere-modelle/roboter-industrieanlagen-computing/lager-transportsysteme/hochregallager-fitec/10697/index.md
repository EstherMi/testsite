---
layout: "image"
title: "Kabel"
date: "2007-06-04T15:48:16"
picture: "HRL31.jpg"
weight: "43"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/10697
imported:
- "2019"
_4images_image_id: "10697"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-06-04T15:48:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10697 -->
Die Kabel sind in Spiralschläuchen gebündelt.