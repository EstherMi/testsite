---
layout: "image"
title: "Kompressorabschaltung"
date: "2007-06-10T20:09:31"
picture: "HRL51.jpg"
weight: "33"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/10789
imported:
- "2019"
_4images_image_id: "10789"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-06-10T20:09:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10789 -->
Jetzt habe ich die Kompressorabschaltung mit 2 Federn gemacht, damit auch ein starkes Vakuum zustande kommt. Abgeschaltet wird wenn der Taster losgelassen wird. Dazu braucht man natürlich kein Int.