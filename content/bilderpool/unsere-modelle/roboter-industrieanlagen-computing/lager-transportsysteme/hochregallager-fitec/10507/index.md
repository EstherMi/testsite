---
layout: "image"
title: "Magazin"
date: "2007-05-25T21:23:34"
picture: "HRL25.jpg"
weight: "65"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/10507
imported:
- "2019"
_4images_image_id: "10507"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-05-25T21:23:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10507 -->
