---
layout: "image"
title: "Vakuumsaugnapf"
date: "2007-06-10T20:09:31"
picture: "HRL53.jpg"
weight: "35"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/10791
imported:
- "2019"
_4images_image_id: "10791"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-06-10T20:09:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10791 -->
Hier sieht man noch mal den Vakuumsaugnapf und den Positionstaster. Fotografiert im Makromodus ;-)