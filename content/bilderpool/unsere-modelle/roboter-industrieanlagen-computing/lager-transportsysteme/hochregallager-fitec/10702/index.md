---
layout: "image"
title: "Laufband mit Drehkranz"
date: "2007-06-04T15:48:16"
picture: "HRL36.jpg"
weight: "48"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/10702
imported:
- "2019"
_4images_image_id: "10702"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-06-04T15:48:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10702 -->
Hier sieht man das Laufband mit Drehkranz.