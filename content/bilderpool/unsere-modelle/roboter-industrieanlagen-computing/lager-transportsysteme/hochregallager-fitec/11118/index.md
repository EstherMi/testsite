---
layout: "image"
title: "Antrieb"
date: "2007-07-17T16:52:55"
picture: "HRL66.jpg"
weight: "20"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/11118
imported:
- "2019"
_4images_image_id: "11118"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-07-17T16:52:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11118 -->
Hier sieht man das Z20, das den Antrieb auf der Schiene macht.