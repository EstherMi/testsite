---
layout: "image"
title: "Neuer Antrieb"
date: "2007-12-09T09:55:47"
picture: "HRL87.jpg"
weight: "1"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/13018
imported:
- "2019"
_4images_image_id: "13018"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-12-09T09:55:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13018 -->
Jetzt habe ich einen Antrieb hingekriegt, der (zumindest bis jetzt) gut funktioniert. Man sieht die Zahnstange, die um 90Grad gedreht ist.