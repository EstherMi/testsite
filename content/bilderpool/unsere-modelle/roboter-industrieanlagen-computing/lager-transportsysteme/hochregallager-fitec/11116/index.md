---
layout: "image"
title: "Einlagerer_neu"
date: "2007-07-17T16:52:55"
picture: "HRL64.jpg"
weight: "18"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/11116
imported:
- "2019"
_4images_image_id: "11116"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-07-17T16:52:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11116 -->
Das ist mein neuer Einlagerer. Er ist wesentlich stabiler. Jetzt sind auch die Schienen anders. Das Problem ist, dass es jetzt Minimotor-Antrieb hat und sogar noch 2:1 untersetzt, aber die Kraft reicht trotzdem nicht. Ich werde wohl noch einen 50:1 Powermotor dranbasteln müssen.