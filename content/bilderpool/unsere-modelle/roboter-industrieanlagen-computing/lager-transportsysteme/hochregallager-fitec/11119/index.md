---
layout: "image"
title: "Gegengewicht"
date: "2007-07-17T16:52:55"
picture: "HRL67.jpg"
weight: "21"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/11119
imported:
- "2019"
_4images_image_id: "11119"
_4images_cat_id: "949"
_4images_user_id: "456"
_4images_image_date: "2007-07-17T16:52:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11119 -->
Hier ist das gegengewicht. Man braucht es dringend, weil der Teil vorne sehr schwer ist und leicht umkippt. Das war auch eine der Schwachstellen, meines letzten Einlagerers. Das mit dem Umkippen passiert jetzt nicht mehr. Das ist auch der Grund, warum das Em an der Seite und nicht mehr hinten ist.