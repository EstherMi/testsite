---
layout: "image"
title: "HRL (1) nula"
date: "2007-10-03T08:48:26"
picture: "hrl01.jpg"
weight: "6"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- details/12070
imported:
- "2019"
_4images_image_id: "12070"
_4images_cat_id: "1080"
_4images_user_id: "592"
_4images_image_date: "2007-10-03T08:48:26"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12070 -->
Frontansicht des Hochregallagers, welches Fischertechnikkisten automatisch ein- und auslagert. Gesteuert wird das ganze mit einem Robo Interface.