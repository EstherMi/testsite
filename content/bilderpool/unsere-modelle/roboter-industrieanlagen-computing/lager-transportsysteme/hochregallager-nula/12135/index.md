---
layout: "image"
title: "HRL (38)"
date: "2007-10-05T22:34:52"
picture: "HRL_20071_2.jpg"
weight: "3"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- details/12135
imported:
- "2019"
_4images_image_id: "12135"
_4images_cat_id: "1080"
_4images_user_id: "592"
_4images_image_date: "2007-10-05T22:34:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12135 -->
Die Impulstastereinheit, die vorher etwas gewackelt und dadurch Zählfehler verursacht hat, habe ich jetzt besser befestigt.