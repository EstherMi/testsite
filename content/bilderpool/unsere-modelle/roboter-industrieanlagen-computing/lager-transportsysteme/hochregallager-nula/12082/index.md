---
layout: "image"
title: "HRL (13)"
date: "2007-10-03T08:48:26"
picture: "hrl13.jpg"
weight: "18"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- details/12082
imported:
- "2019"
_4images_image_id: "12082"
_4images_cat_id: "1080"
_4images_user_id: "592"
_4images_image_date: "2007-10-03T08:48:26"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12082 -->
Hier sieht man, dass ich das Impulsrad 1:2 übersetzt habe. Dadurch bekomme ich doppelt so viele Impulse und kann genauer positionieren.
Die Zahnräder haben etwas gewackelt, ich habe jetzt eine Verbesserung,
siehe HRL (39)!