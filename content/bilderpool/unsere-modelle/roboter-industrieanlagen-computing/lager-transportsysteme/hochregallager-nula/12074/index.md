---
layout: "image"
title: "HRL (5)"
date: "2007-10-03T08:48:26"
picture: "hrl05.jpg"
weight: "10"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- details/12074
imported:
- "2019"
_4images_image_id: "12074"
_4images_cat_id: "1080"
_4images_user_id: "592"
_4images_image_date: "2007-10-03T08:48:26"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12074 -->
Eine andere Sicht.