---
layout: "overview"
title: "Hochregallager (nula)"
date: 2019-12-17T19:04:58+01:00
legacy_id:
- categories/1080
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1080 --> 
Über ein Förderband werden Fischertechnikkisten zugeführt und automatisch in einen freien Platz im Hochregal eingelagert. Per Mausklick kann jede Kiste auch wieder ausgelagert werden.