---
layout: "image"
title: "HRL (9) Abbau"
date: "2009-09-27T23:59:14"
picture: "hochregallagerversionabbau09.jpg"
weight: "9"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- details/25385
imported:
- "2019"
_4images_image_id: "25385"
_4images_cat_id: "1778"
_4images_user_id: "592"
_4images_image_date: "2009-09-27T23:59:14"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25385 -->
Hier folgen ein paar leider teils unscharfe Detailbilder vom Abbau meines Hochregallagers.