---
layout: "image"
title: "HRL (4) Abbau"
date: "2009-09-27T23:59:14"
picture: "hochregallagerversionabbau04.jpg"
weight: "4"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- details/25380
imported:
- "2019"
_4images_image_id: "25380"
_4images_cat_id: "1778"
_4images_user_id: "592"
_4images_image_date: "2009-09-27T23:59:14"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25380 -->
Hier folgen ein paar leider teils unscharfe Detailbilder vom Abbau meines Hochregallagers.