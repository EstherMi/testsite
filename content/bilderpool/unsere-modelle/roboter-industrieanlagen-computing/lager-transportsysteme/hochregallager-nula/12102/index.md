---
layout: "image"
title: "HRL (33)"
date: "2007-10-03T08:48:27"
picture: "hrl33.jpg"
weight: "38"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- details/12102
imported:
- "2019"
_4images_image_id: "12102"
_4images_cat_id: "1080"
_4images_user_id: "592"
_4images_image_date: "2007-10-03T08:48:27"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12102 -->
Danach geht er erst unter die Kiste und hebt sie dann aus dem Fach.