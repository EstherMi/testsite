---
layout: "image"
title: "HRL (20)"
date: "2007-10-03T08:48:26"
picture: "hrl20.jpg"
weight: "25"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- details/12089
imported:
- "2019"
_4images_image_id: "12089"
_4images_cat_id: "1080"
_4images_user_id: "592"
_4images_image_date: "2007-10-03T08:48:26"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12089 -->
So, und nun der "Kistengreifer". Ich transportiere die Kiste , indem ich den Greifer under die Kiste bringe und die Kiste ein bisschen anhebe. Dadurch liegt die Kiste auf dem Greifer.