---
layout: "image"
title: "HRL (2) Einlagerer"
date: "2009-09-27T23:59:13"
picture: "hochregallagerversion2.jpg"
weight: "2"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- details/25375
imported:
- "2019"
_4images_image_id: "25375"
_4images_cat_id: "1777"
_4images_user_id: "592"
_4images_image_date: "2009-09-27T23:59:13"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25375 -->
Ich hatte recht lange eine sehr klobige hässliche drum-herum-Konstruktion für die Z-Achse meines Einlagerers. Das sieht so viel besser aus und hält auch besser.