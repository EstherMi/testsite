---
layout: "image"
title: "HRL (2) von vorne"
date: "2008-06-04T18:44:51"
picture: "hochregallager02.jpg"
weight: "2"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- details/14618
imported:
- "2019"
_4images_image_id: "14618"
_4images_cat_id: "1344"
_4images_user_id: "592"
_4images_image_date: "2008-06-04T18:44:51"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14618 -->
Die Bedienung erfolgt über zwei Taster. Die linke Taste ist die Wahltaste, die rechte die Bestätigungstaste.