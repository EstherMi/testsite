---
layout: "comment"
hidden: true
title: "6567"
date: "2008-06-05T21:47:15"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Stefan,
das wäre allerdings keine modellmäßige Umsetzung der Realität. Bei einem reellen HRL wird vor der Beschickung ein leeres Fach im EDV-Speicher abgefragt bzw. danach mit den konkreten Inhalt als belegt gespeichert. Zur Entnahme erfolgt über den Inhalt in ihm die Adreßabfrage des Faches und anschließende Freigabe desselben.
Gruß,
Udo2