---
layout: "image"
title: "HRL (1) Übersicht"
date: "2008-06-04T18:44:50"
picture: "hochregallager01.jpg"
weight: "1"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- details/14617
imported:
- "2019"
_4images_image_id: "14617"
_4images_cat_id: "1344"
_4images_user_id: "592"
_4images_image_date: "2008-06-04T18:44:50"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14617 -->
Dies ist die aktuelle Version meines Hochregallagers. Vom Prinzp her ist es wie das ältere, hat aber folgende neue Features:

#  Das HRL läuft ohne Computer (Bild 2).
#  Das Förderband verfügt über zwei Lichtschranken, die die Position der Kiste bestimmen (Bild 4).
#  Die Positionserkennung der X-Achse erfolgt nicht mehr über Impulstaster, sondern über Positionstaster   (Bild 9).
#  Es verfügt über ein Extensionmodule (Bild 10).
#  Die Positionen der Kisten werden auf einem EEprom gespeichert und können auch nach Trennung der Stromversorgung ausgelesen werden (Bild 11).

Außerdem ist es viel zuverlässiger und schöner... :-)

Dies ist die Version 3.0, Version 2.0 habe ich nicht fotografiert (Ist das gleiche nur ohne EEprom).

Hier ein Video:

http://www.youtube.com/watch?v=2TGYcYOSWhk&feature=player_embedded

Auf dem Video fällt die Kiste noch runter, mitlerweile bleibt sie stehen...