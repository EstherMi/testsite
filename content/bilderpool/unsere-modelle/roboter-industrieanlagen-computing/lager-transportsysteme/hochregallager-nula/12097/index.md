---
layout: "image"
title: "HRL (28)"
date: "2007-10-03T08:48:26"
picture: "hrl28.jpg"
weight: "33"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- details/12097
imported:
- "2019"
_4images_image_id: "12097"
_4images_cat_id: "1080"
_4images_user_id: "592"
_4images_image_date: "2007-10-03T08:48:26"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12097 -->
Der alte graue Motor hat, um Platz zu sparen, unter dem Förderband Platz gefunden.