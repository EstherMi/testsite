---
layout: "image"
title: "Regalanlage mit Handsteuerung"
date: "2007-06-26T22:17:28"
picture: "Regalanlage_23.jpg"
weight: "21"
konstrukteure: 
- "Jürgen Fischer"
fotografen:
- "Jürgen Fischer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jürgen Fischer"
license: "unknown"
legacy_id:
- details/10916
imported:
- "2019"
_4images_image_id: "10916"
_4images_cat_id: "990"
_4images_user_id: "341"
_4images_image_date: "2007-06-26T22:17:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10916 -->
Meine Anlage, soll auch für kleine "Fischertechnik-Freunde" und Rentner bespielbar sein,so wie meine Wenigkeit.(68.Jahre alt)Viel Spaß mit den Bildern wünscht Euch Jürgen