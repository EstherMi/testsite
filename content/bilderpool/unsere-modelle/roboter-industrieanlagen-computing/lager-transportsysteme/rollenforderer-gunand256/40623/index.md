---
layout: "image"
title: "Rollenförderer von links"
date: "2015-03-10T18:15:07"
picture: "rofoe2.jpg"
weight: "2"
konstrukteure: 
- "gunand256"
fotografen:
- "gunand256"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "gunand256"
license: "unknown"
legacy_id:
- details/40623
imported:
- "2019"
_4images_image_id: "40623"
_4images_cat_id: "3048"
_4images_user_id: "2357"
_4images_image_date: "2015-03-10T18:15:07"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40623 -->
Hier sieht man das Kassettenmagazin und die Rückseite der Füllstation.