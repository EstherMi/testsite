---
layout: "image"
title: "Rollenförderer von rechts"
date: "2015-03-10T18:15:07"
picture: "rofoe3.jpg"
weight: "3"
konstrukteure: 
- "gunand256"
fotografen:
- "gunand256"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "gunand256"
license: "unknown"
legacy_id:
- details/40624
imported:
- "2019"
_4images_image_id: "40624"
_4images_cat_id: "3048"
_4images_user_id: "2357"
_4images_image_date: "2015-03-10T18:15:07"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40624 -->
Ganz links sieht man das Hubgetriebe für das Kassettenmagazin. Die Rollen sind alle über Zahnräder Z20 und Z10 miteinander verbunden.