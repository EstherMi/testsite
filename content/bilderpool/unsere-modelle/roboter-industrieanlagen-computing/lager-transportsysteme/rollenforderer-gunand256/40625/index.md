---
layout: "image"
title: "E-tec Bausteine"
date: "2015-03-10T18:15:07"
picture: "rofoe4.jpg"
weight: "4"
konstrukteure: 
- "gunand256"
fotografen:
- "gunand256"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "gunand256"
license: "unknown"
legacy_id:
- details/40625
imported:
- "2019"
_4images_image_id: "40625"
_4images_cat_id: "3048"
_4images_user_id: "2357"
_4images_image_date: "2015-03-10T18:15:07"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40625 -->
Einige der 6 E-tec Bausteine arbeiten mit Digitalfunktion. z.B Monoflop oder Logicgatter. Der Powermotor ist direkt an den Ausgang eines Und-gatters angeschlossen. Mir wurde gesagt, dass der Treiber das abkann.
Und das Modell läuft ganz prima mit einem Standart 9V Netzteil.