---
layout: "image"
title: "Industriemodell - E-Magnet"
date: "2011-01-21T15:16:08"
picture: "modell06.jpg"
weight: "6"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/29727
imported:
- "2019"
_4images_image_id: "29727"
_4images_cat_id: "2184"
_4images_user_id: "1162"
_4images_image_date: "2011-01-21T15:16:08"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29727 -->
Hier ist der Greifer, der durch einen Zylinder herausgeschoben werden kann.