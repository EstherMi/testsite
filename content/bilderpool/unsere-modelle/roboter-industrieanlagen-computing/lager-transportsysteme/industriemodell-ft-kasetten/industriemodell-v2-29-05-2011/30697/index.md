---
layout: "image"
title: "Serienaufnahme"
date: "2011-05-29T15:10:01"
picture: "modell11.jpg"
weight: "11"
konstrukteure: 
- "Endlich"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/30697
imported:
- "2019"
_4images_image_id: "30697"
_4images_cat_id: "2288"
_4images_user_id: "1162"
_4images_image_date: "2011-05-29T15:10:01"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30697 -->
