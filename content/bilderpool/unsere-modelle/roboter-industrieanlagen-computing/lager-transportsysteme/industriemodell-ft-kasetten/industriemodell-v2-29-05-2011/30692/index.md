---
layout: "image"
title: "Rollenband"
date: "2011-05-29T15:10:01"
picture: "modell06.jpg"
weight: "6"
konstrukteure: 
- "Endlich"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/30692
imported:
- "2019"
_4images_image_id: "30692"
_4images_cat_id: "2288"
_4images_user_id: "1162"
_4images_image_date: "2011-05-29T15:10:01"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30692 -->
Hier der Ablageort auf dem man oben die Kassette stellt und dann sie nach unten schupst.