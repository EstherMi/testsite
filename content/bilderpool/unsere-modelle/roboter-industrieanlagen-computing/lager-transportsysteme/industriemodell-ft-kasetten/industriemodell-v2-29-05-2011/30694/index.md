---
layout: "image"
title: "Tisch2"
date: "2011-05-29T15:10:01"
picture: "modell08.jpg"
weight: "8"
konstrukteure: 
- "Endlich"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/30694
imported:
- "2019"
_4images_image_id: "30694"
_4images_cat_id: "2288"
_4images_user_id: "1162"
_4images_image_date: "2011-05-29T15:10:01"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30694 -->
Der zweite Tisch auf dem die weißen Kassetten abgelegt werden.