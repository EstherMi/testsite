---
layout: "image"
title: "Arm und ein Teil der Rutsche"
date: "2011-05-29T15:10:01"
picture: "modell17.jpg"
weight: "17"
konstrukteure: 
- "Endlich"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/30703
imported:
- "2019"
_4images_image_id: "30703"
_4images_cat_id: "2288"
_4images_user_id: "1162"
_4images_image_date: "2011-05-29T15:10:01"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30703 -->
Der Arm legt dann die Kassette auf der Rutsche ab.