---
layout: "image"
title: "schwarze Kassette"
date: "2011-05-29T15:10:01"
picture: "modell20.jpg"
weight: "20"
konstrukteure: 
- "Endlich"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/30706
imported:
- "2019"
_4images_image_id: "30706"
_4images_cat_id: "2288"
_4images_user_id: "1162"
_4images_image_date: "2011-05-29T15:10:01"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30706 -->
Ein schwarzer Aufkleber wurde aufgeklebt.