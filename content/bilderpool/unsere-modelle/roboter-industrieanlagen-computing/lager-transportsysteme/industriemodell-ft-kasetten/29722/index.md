---
layout: "image"
title: "Industriemodell - Gesamtansicht"
date: "2011-01-21T15:16:07"
picture: "modell01.jpg"
weight: "1"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/29722
imported:
- "2019"
_4images_image_id: "29722"
_4images_cat_id: "2184"
_4images_user_id: "1162"
_4images_image_date: "2011-01-21T15:16:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29722 -->
Endlich hat der Endlich wieder Bilder XD. So, Spaß beiseite. Ich hab mich mal wieder hingesetzt und ein Modell gebaut. Ein Industriemodell. Es schiebt die FT-Kasetten aus dem Magazin heraus auf das Förderband, dies transportiert es dann auf den Tisch und der E-Magnet greift es dann. Weitere Information gibt es dann bei den einzelnen Bildern.

Ich hoffe euch gefallen die Bilder, ein Video kommt, sobald es funktioniert und programmiert ist.

MfG
Endlich