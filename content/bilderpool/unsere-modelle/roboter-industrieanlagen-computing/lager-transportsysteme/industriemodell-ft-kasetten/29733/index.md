---
layout: "image"
title: "Industriemodell - Kompressor und Lufttank"
date: "2011-01-21T15:16:08"
picture: "modell12.jpg"
weight: "12"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/29733
imported:
- "2019"
_4images_image_id: "29733"
_4images_cat_id: "2184"
_4images_user_id: "1162"
_4images_image_date: "2011-01-21T15:16:08"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29733 -->
Hier sieht man den Lufttank und den Kompressor.