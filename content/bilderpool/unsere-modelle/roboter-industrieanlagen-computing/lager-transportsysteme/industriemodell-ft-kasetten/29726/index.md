---
layout: "image"
title: "Industriemodell - Magazin"
date: "2011-01-21T15:16:08"
picture: "modell05.jpg"
weight: "5"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/29726
imported:
- "2019"
_4images_image_id: "29726"
_4images_cat_id: "2184"
_4images_user_id: "1162"
_4images_image_date: "2011-01-21T15:16:08"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29726 -->
Hier sieht man das Magazin in dem problemlos 5 Kasetten gelagert werden können. Die zwei Pneumatikzylinder schieben die Kasetten aus dem Magazin heraus. Allerdings funktioniert es noch nicht, da die Zylinder nicht weit genug herausfahren. Vielleicht habt ihr eine (platzsparende, einfache) Lösung.