---
layout: "image"
title: "Industriemodell - Motor"
date: "2011-01-21T15:16:08"
picture: "modell09.jpg"
weight: "9"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/29730
imported:
- "2019"
_4images_image_id: "29730"
_4images_cat_id: "2184"
_4images_user_id: "1162"
_4images_image_date: "2011-01-21T15:16:08"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29730 -->
Hier kann man den Motor sehen, der den Drehkranz antreibt.