---
layout: "overview"
title: "Turmregallager"
date: 2019-12-17T19:06:06+01:00
legacy_id:
- categories/1949
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1949 --> 
Mein Turmregallager sortiert automatisch die Kisten in das nächste freie Fach ein, sodass dieses dann wieder schnell aussortiert werden kann.