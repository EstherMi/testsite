---
layout: "image"
title: "Förderband mit sensor tunnel (20)"
date: "2013-09-23T21:35:22"
picture: "turmregallager20.jpg"
weight: "20"
konstrukteure: 
- "nevs"
fotografen:
- "nevs"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nevs"
license: "unknown"
legacy_id:
- details/37433
imported:
- "2019"
_4images_image_id: "37433"
_4images_cat_id: "1949"
_4images_user_id: "1463"
_4images_image_date: "2013-09-23T21:35:22"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37433 -->
Dieses förderband hab ich mal als prototüb gebaut.
Im tunnel ist ein farbsensor eingebaut mit dem ich später mal Barcodes lesen lasen will.
Das förderbandsoll später die Packete zum abholplatz transportieren und über den Barcode passend einsortieren