---
layout: "image"
title: "X- Achse"
date: "2010-06-11T18:37:53"
picture: "X-Achse.jpg"
weight: "8"
konstrukteure: 
- "Fischerpapa"
fotografen:
- "Fischerpapa"
keywords: ["Turmregallager", "TRL", "Regallager"]
uploadBy: "FischerPapa"
license: "unknown"
legacy_id:
- details/27457
imported:
- "2019"
_4images_image_id: "27457"
_4images_cat_id: "1950"
_4images_user_id: "1127"
_4images_image_date: "2010-06-11T18:37:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27457 -->
Das ist das Antriebsmodul der X- Achse. Ich habe hier den neuen Encodermotor gewählt, um den Roboter genauer positionieren zu können.