---
layout: "image"
title: "Steuerung"
date: "2010-06-11T18:37:54"
picture: "Schalter.jpg"
weight: "13"
konstrukteure: 
- "Fischerpapa"
fotografen:
- "Fischerpapa"
keywords: ["Turmregallager", "TRL", "Regallager"]
uploadBy: "FischerPapa"
license: "unknown"
legacy_id:
- details/27462
imported:
- "2019"
_4images_image_id: "27462"
_4images_cat_id: "1950"
_4images_user_id: "1127"
_4images_image_date: "2010-06-11T18:37:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27462 -->
Hier seht ihr die zwei Taster, die benötigt werden, um die auszulagernde Kiste auszuwählen. Den Rechten drückt man solange bis auf dem Display das entsprechende Fach ausgewählt ist und danach drückt man zur Bestätigung noch den linken Taster.