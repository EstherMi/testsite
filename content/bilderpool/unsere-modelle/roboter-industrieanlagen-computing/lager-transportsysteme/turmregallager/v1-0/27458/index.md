---
layout: "image"
title: "Y- Achse"
date: "2010-06-11T18:37:54"
picture: "Y-Achse.jpg"
weight: "9"
konstrukteure: 
- "Fischerpapa"
fotografen:
- "Fischerpapa"
keywords: ["Turmregallager", "TRL", "Regallager"]
uploadBy: "FischerPapa"
license: "unknown"
legacy_id:
- details/27458
imported:
- "2019"
_4images_image_id: "27458"
_4images_cat_id: "1950"
_4images_user_id: "1127"
_4images_image_date: "2010-06-11T18:37:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27458 -->
Das ist das Antriebsmodul der Y- Achse. Ich habe hier auch ein Encodermotor gewählt, um den Roboter besser positionieren zu können.