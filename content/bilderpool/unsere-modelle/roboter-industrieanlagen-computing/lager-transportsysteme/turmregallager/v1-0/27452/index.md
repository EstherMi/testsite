---
layout: "image"
title: "Gesamtübersicht"
date: "2010-06-11T18:37:53"
picture: "Gesamtansicht_ohne_Kiste.jpg"
weight: "3"
konstrukteure: 
- "Fischerpapa"
fotografen:
- "Fischerpapa"
keywords: ["Turmregallager", "TRL", "Regallager"]
uploadBy: "FischerPapa"
license: "unknown"
legacy_id:
- details/27452
imported:
- "2019"
_4images_image_id: "27452"
_4images_cat_id: "1950"
_4images_user_id: "1127"
_4images_image_date: "2010-06-11T18:37:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27452 -->
Nochmal eine Gesamtübersicht, diesmal jedoch ohne Kiste.