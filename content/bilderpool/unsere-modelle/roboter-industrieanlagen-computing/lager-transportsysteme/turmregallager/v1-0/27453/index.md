---
layout: "image"
title: "Ein-/ Auslagerung"
date: "2010-06-11T18:37:53"
picture: "Ein-_und_Auslagerung.jpg"
weight: "4"
konstrukteure: 
- "Fischerpapa"
fotografen:
- "Fischerpapa"
keywords: ["Turmregallager", "TRL", "Regallager"]
uploadBy: "FischerPapa"
license: "unknown"
legacy_id:
- details/27453
imported:
- "2019"
_4images_image_id: "27453"
_4images_cat_id: "1950"
_4images_user_id: "1127"
_4images_image_date: "2010-06-11T18:37:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27453 -->
Hier wird die Kiste hingebracht, wenn sie ausgelagert wird. Wenn man eine Kiste hierin stellt wird sie übrigens auch einsortiert. Die Lichtschranke erkennt, ob sich gerade eine Kiste dort befindet oder nicht.