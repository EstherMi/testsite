---
layout: "comment"
hidden: true
title: "12478"
date: "2010-10-09T18:24:30"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo,
das Foto "X/Y-Achse" zeigt doch die senkrechte Parallellagerung 2x31436 Gelenkwürfel-Klaue mit 2x 31893 Hülse Ø4,1xØ7,9x15 auf M-Achse. Das ist mit nur 15mm Führungslänge (Laibung) für den langen waagerechten Arm eine entschieden zu kurze Lagerung. Zwei Lagerungen auf jeder M-Achse mit mindestens 30mm Rasterabstand wären da schon besser. Auf dem obigen Foto ist auch die Durchbiegung des waagerechten Armes gut zu sehen, die seitlich mit ein oder beidseitig zwei eingelegten M-Achsen an den BS30 wirksam kompensiert werden kann.
Gruss, Udo2