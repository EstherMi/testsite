---
layout: "image"
title: "X / Y- Achse"
date: "2010-06-11T18:37:54"
picture: "GesamtansichtRoboter.jpg"
weight: "11"
konstrukteure: 
- "Fischerpapa"
fotografen:
- "Fischerpapa"
keywords: ["Turmregallager", "TRL", "Regallager"]
uploadBy: "FischerPapa"
license: "unknown"
legacy_id:
- details/27460
imported:
- "2019"
_4images_image_id: "27460"
_4images_cat_id: "1950"
_4images_user_id: "1127"
_4images_image_date: "2010-06-11T18:37:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27460 -->
Hier seht ihr nochmal die X-Achse und die Hochachse (Y- Achse).