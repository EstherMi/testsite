---
layout: "image"
title: "Arbeitsarm (16)"
date: "2013-09-23T21:35:22"
picture: "turmregallager16.jpg"
weight: "16"
konstrukteure: 
- "nevs"
fotografen:
- "nevs"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nevs"
license: "unknown"
legacy_id:
- details/37429
imported:
- "2019"
_4images_image_id: "37429"
_4images_cat_id: "1949"
_4images_user_id: "1463"
_4images_image_date: "2013-09-23T21:35:22"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37429 -->
