---
layout: "image"
title: "Band"
date: "2009-10-11T18:13:03"
picture: "Band_2.jpg"
weight: "2"
konstrukteure: 
- "Lothar Vogt      Pilami"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- details/25537
imported:
- "2019"
_4images_image_id: "25537"
_4images_cat_id: "1791"
_4images_user_id: "10"
_4images_image_date: "2009-10-11T18:13:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25537 -->
erkennbar sind Antriebszahnräder Z10