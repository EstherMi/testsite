---
layout: "image"
title: "Schubladenantrieb"
date: "2009-10-11T18:13:03"
picture: "Schublade_2.jpg"
weight: "3"
konstrukteure: 
- "Lothar Vogt      Pilami"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- details/25538
imported:
- "2019"
_4images_image_id: "25538"
_4images_cat_id: "1791"
_4images_user_id: "10"
_4images_image_date: "2009-10-11T18:13:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25538 -->
Das ist der Antrieb der Schublade zum Be- und Entladen des Bandes