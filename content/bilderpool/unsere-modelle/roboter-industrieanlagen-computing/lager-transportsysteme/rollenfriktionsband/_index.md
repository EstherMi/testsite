---
layout: "overview"
title: "Rollenfriktionsband"
date: 2019-12-17T19:06:05+01:00
legacy_id:
- categories/1791
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1791 --> 
Mal eine andere Version eines Transportbandes. Hier erfolgt die Kraftübertragung per Reibung über Rollen auf eine Palette oder ähnliches. 
Vorteil: keine Ketten, niedrige Bauform möglich, Vielseitig.