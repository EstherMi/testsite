---
layout: "image"
title: "Antrieb"
date: "2007-10-13T11:40:15"
picture: "DSCN1699.jpg"
weight: "33"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/12197
imported:
- "2019"
_4images_image_id: "12197"
_4images_cat_id: "1092"
_4images_user_id: "184"
_4images_image_date: "2007-10-13T11:40:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12197 -->
Der Antrieb (durch einen Power Motor 50:1)