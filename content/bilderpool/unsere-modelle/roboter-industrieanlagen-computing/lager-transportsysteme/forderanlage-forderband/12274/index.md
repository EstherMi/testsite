---
layout: "image"
title: "Ansicht"
date: "2007-10-22T15:19:23"
picture: "PICT2214.jpg"
weight: "6"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/12274
imported:
- "2019"
_4images_image_id: "12274"
_4images_cat_id: "1092"
_4images_user_id: "424"
_4images_image_date: "2007-10-22T15:19:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12274 -->
