---
layout: "image"
title: "Anfang"
date: "2007-10-13T11:40:15"
picture: "DSCN1710.jpg"
weight: "35"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/12199
imported:
- "2019"
_4images_image_id: "12199"
_4images_cat_id: "1092"
_4images_user_id: "184"
_4images_image_date: "2007-10-13T11:40:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12199 -->
Das kann man hier von der anderen (Innenseite) Seite sehen.