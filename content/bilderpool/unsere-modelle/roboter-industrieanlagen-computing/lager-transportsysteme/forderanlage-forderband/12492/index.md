---
layout: "image"
title: "Detail"
date: "2007-11-05T15:54:02"
picture: "DSCN1959.jpg"
weight: "5"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/12492
imported:
- "2019"
_4images_image_id: "12492"
_4images_cat_id: "1092"
_4images_user_id: "184"
_4images_image_date: "2007-11-05T15:54:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12492 -->
