---
layout: "image"
title: "Ansicht"
date: "2007-10-17T13:29:22"
picture: "DSCN1762.jpg"
weight: "12"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/12245
imported:
- "2019"
_4images_image_id: "12245"
_4images_cat_id: "1092"
_4images_user_id: "184"
_4images_image_date: "2007-10-17T13:29:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12245 -->
Den Unterbau habe ich ein wenig geändert. Die Rückführung klappte nicht so gut.