---
layout: "image"
title: "Zuführung überarbeitet"
date: "2007-10-22T15:19:24"
picture: "DSCN1818.jpg"
weight: "10"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/12278
imported:
- "2019"
_4images_image_id: "12278"
_4images_cat_id: "1092"
_4images_user_id: "184"
_4images_image_date: "2007-10-22T15:19:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12278 -->
Ein Blick von hinten. Die gelbe Platte wird durch die Mitnehmer hochgedrückt und fällt danach wieder herunter. So kann kein Transportgut nach hinten herausfallen. Die Mitnehmer rutschen über den Kanalboden und werden durch die seitlich angebrachten gelben Bauplatten gehalten.