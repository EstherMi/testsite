---
layout: "image"
title: "Zuführung überarbeitet"
date: "2007-10-22T15:19:24"
picture: "DSCN1799.jpg"
weight: "9"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/12277
imported:
- "2019"
_4images_image_id: "12277"
_4images_cat_id: "1092"
_4images_user_id: "184"
_4images_image_date: "2007-10-22T15:19:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12277 -->
Das ist der neue "Kanalquerschnitt" der Zuführungseinheit.