---
layout: "image"
title: "Ende 3"
date: "2007-10-13T11:40:15"
picture: "DSCN1711.jpg"
weight: "38"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/12202
imported:
- "2019"
_4images_image_id: "12202"
_4images_cat_id: "1092"
_4images_user_id: "184"
_4images_image_date: "2007-10-13T11:40:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12202 -->
Kleineres Transportgut will sehr gerne auf die Kette fallen und so die Zahnräder blockieren. Das passiert nach Einbau eines BS15 mit Winkelstein 15(60 Grad) 38309 nicht mehr.