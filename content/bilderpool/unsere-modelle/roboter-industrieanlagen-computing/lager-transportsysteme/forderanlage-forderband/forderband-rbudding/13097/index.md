---
layout: "image"
title: "Farbsensor II"
date: "2007-12-17T22:22:39"
picture: "DSC_0131.jpg"
weight: "6"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/13097
imported:
- "2019"
_4images_image_id: "13097"
_4images_cat_id: "1187"
_4images_user_id: "371"
_4images_image_date: "2007-12-17T22:22:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13097 -->
Distance between the sensor and the bars are perfect. Do not attach them too close...
The A1 input on the Robo Interface will give a perfect reading on different colors.