---
layout: "image"
title: "Modul HRL"
date: "2008-08-22T23:17:15"
picture: "modul1.jpg"
weight: "1"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/15069
imported:
- "2019"
_4images_image_id: "15069"
_4images_cat_id: "1372"
_4images_user_id: "558"
_4images_image_date: "2008-08-22T23:17:15"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15069 -->
Das wäre ein HRL zur Ausgabe von Werkstücken die in einer Modulkette verwendet werden können. Selbstverständlich kann das Förderband auch durchgezogen werden, wobei das HRL dann als Puffer dienen würde.