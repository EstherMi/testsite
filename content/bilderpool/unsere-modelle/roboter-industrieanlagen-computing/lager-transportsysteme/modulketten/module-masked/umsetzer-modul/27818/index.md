---
layout: "image"
title: "Greifer 1"
date: "2010-08-08T20:53:37"
picture: "umsetzermodul3.jpg"
weight: "3"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: ["35977"]
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/27818
imported:
- "2019"
_4images_image_id: "27818"
_4images_cat_id: "2006"
_4images_user_id: "373"
_4images_image_date: "2010-08-08T20:53:37"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27818 -->
geklaut bei MisterWho: http://www.ftcommunity.de/categories.php?cat_id=1169