---
layout: "image"
title: "Befestigung Motor"
date: "2010-10-01T15:14:19"
picture: "drehkranz5.jpg"
weight: "5"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/28800
imported:
- "2019"
_4images_image_id: "28800"
_4images_cat_id: "2097"
_4images_user_id: "373"
_4images_image_date: "2010-10-01T15:14:19"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28800 -->
Die ist etwas kompliziert. der Motor wird von einem BS5 von unten gehalten, auf der anderen Seite setzt der BS 7,5 am Getriebe an.
Hier sieht man auch die beiden Haken an der Sache: An einem BS 15 rot mit Loch mussten beide Zapfen weg. Und normale Stecker passen hier nicht mehr dazwischen, also habe ich die Kabel nur abisoliert, reingeklemmt und mit Tesa fixiert. Hält einwandfrei, hatte nicht einen Wackelkontakt.