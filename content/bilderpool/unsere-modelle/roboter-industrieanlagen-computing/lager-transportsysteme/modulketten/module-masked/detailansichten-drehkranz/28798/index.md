---
layout: "image"
title: "Drehkranz alleine"
date: "2010-10-01T15:14:18"
picture: "drehkranz3.jpg"
weight: "3"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/28798
imported:
- "2019"
_4images_image_id: "28798"
_4images_cat_id: "2097"
_4images_user_id: "373"
_4images_image_date: "2010-10-01T15:14:18"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28798 -->
Der Antrieb läuft vom Getriebe vorne nur nach rechts, über die Kette nach hinten und über die hintere Achse auf die zweite Kette. Vorne besteht zwischen den Ketten keine Verbindung.
Das ist die kompakteste Lösung, da für eine vorne durchgehende Achse dann wieder Schraub-Z10 nötig wären, und die brauchen mehr Platz.