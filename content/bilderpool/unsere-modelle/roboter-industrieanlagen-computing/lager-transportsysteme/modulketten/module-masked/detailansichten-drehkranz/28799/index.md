---
layout: "image"
title: "Drehkranz alleine 2"
date: "2010-10-01T15:14:18"
picture: "drehkranz4.jpg"
weight: "4"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/28799
imported:
- "2019"
_4images_image_id: "28799"
_4images_cat_id: "2097"
_4images_user_id: "373"
_4images_image_date: "2010-10-01T15:14:18"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28799 -->
Ohne die zusätzliche Führung auf der hier oberen Seite. Die ist nicht unbedingt nötig, hält die Kassetten aber besser in der Mitte. Wenn man sie weglässt, ist der komplette Drehkranz mit Allem nur 45mm hoch. Flacher geht nun wirklich nicht mehr.