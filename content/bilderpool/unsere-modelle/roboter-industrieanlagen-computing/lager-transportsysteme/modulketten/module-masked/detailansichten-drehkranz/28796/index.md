---
layout: "image"
title: "Ultra-Flacher Drehkranz"
date: "2010-10-01T15:14:18"
picture: "drehkranz1.jpg"
weight: "1"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/28796
imported:
- "2019"
_4images_image_id: "28796"
_4images_cat_id: "2097"
_4images_user_id: "373"
_4images_image_date: "2010-10-01T15:14:18"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28796 -->
Nochmal als Detailansicht beim Zerlegen. Auch wenns nicht so aussieht, ein Drehen um 360° ist hier möglich, einzige Begrenzung ist das Kabel.