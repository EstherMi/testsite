---
layout: "comment"
hidden: true
title: "10621"
date: "2010-01-26T22:24:26"
uploadBy:
- "Masked"
license: "unknown"
imported:
- "2019"
---
Inzwischen habe ich das ganze nochmal umgebaut, so dass sich der Drehkranz (bis auf das Kabel) komplett frei drehen kann und nirgendwo anstößt. Die Lösung war einfacher als gedacht: Ich habe einfach den Motor um eine Nut, also 7,5mm, weiter Richtung Mitte des Drehkranzes versetzt. Schon sind sämtliche Probleme beseitigt.

@Harald: Wie soll das gehen? Mir ist klar, dass es wohl gehen wird, wenn du es hinbekommst. Aber alleine der Drehkranz selber hat ja schon 15mm, wenn man ihn denn verwendet...Wir wollen Bilder sehen!

Grüße,
Martin