---
layout: "image"
title: "Zuliefertisch HRL 5"
date: "2007-11-18T00:51:46"
picture: "bumpf5.jpg"
weight: "5"
konstrukteure: 
- "bumpf"
fotografen:
- "walter mario graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/12767
imported:
- "2019"
_4images_image_id: "12767"
_4images_cat_id: "1150"
_4images_user_id: "424"
_4images_image_date: "2007-11-18T00:51:46"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12767 -->
Ansicht von vorne