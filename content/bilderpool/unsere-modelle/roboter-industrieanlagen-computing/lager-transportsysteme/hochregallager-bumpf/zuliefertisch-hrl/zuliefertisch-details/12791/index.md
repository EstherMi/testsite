---
layout: "image"
title: "Zuliefertisch Details"
date: "2007-11-21T17:41:16"
picture: "zuliefertischdetails3.jpg"
weight: "3"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/12791
imported:
- "2019"
_4images_image_id: "12791"
_4images_cat_id: "1152"
_4images_user_id: "424"
_4images_image_date: "2007-11-21T17:41:16"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12791 -->
Tisch von unten. Mit dem Baustein 30, ganz links im Bild, wiird die Kette fixiert.