---
layout: "image"
title: "Wendeplatz"
date: "2006-12-30T09:56:05"
picture: "HRL_Fischertechnik_020.jpg"
weight: "10"
konstrukteure: 
- "Walter-Mario Graf (Bumpf)"
fotografen:
- "Walter-Mario Graf (Bumpf)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/8199
imported:
- "2019"
_4images_image_id: "8199"
_4images_cat_id: "20"
_4images_user_id: "424"
_4images_image_date: "2006-12-30T09:56:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8199 -->
Auf dem Wendeplatz wird die Kassette ein erstesmal geprüft. Falsche Kassetten werden aussortiert.