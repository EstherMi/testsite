---
layout: "image"
title: "Code"
date: "2006-12-30T09:56:05"
picture: "HRL_Fischertechnik_040.jpg"
weight: "17"
konstrukteure: 
- "Walter-Mario Graf (Bumpf)"
fotografen:
- "Walter-Mario Graf (Bumpf)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/8206
imported:
- "2019"
_4images_image_id: "8206"
_4images_cat_id: "20"
_4images_user_id: "424"
_4images_image_date: "2006-12-30T09:56:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8206 -->
Kassette mit Inhaltsangabe, Strichcode und Lagerplatznummer.