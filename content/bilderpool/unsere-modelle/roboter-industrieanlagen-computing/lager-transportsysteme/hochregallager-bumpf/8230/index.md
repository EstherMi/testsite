---
layout: "image"
title: "Lichtschranke"
date: "2006-12-31T01:34:43"
picture: "HRL_Fischertechnik_Detail_019.jpg"
weight: "3"
konstrukteure: 
- "Walter-Mario Graf"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/8230
imported:
- "2019"
_4images_image_id: "8230"
_4images_cat_id: "20"
_4images_user_id: "424"
_4images_image_date: "2006-12-31T01:34:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8230 -->
Im Baustein 15 grau ist ein LED eingebaut