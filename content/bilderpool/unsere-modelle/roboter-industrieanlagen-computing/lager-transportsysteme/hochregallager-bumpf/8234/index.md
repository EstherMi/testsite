---
layout: "image"
title: "Wendeplatz mit Lichtschranke"
date: "2006-12-31T12:49:52"
picture: "HRL_Fischertechnik_Detail_006.jpg"
weight: "7"
konstrukteure: 
- "Walter-Mario Graf"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/8234
imported:
- "2019"
_4images_image_id: "8234"
_4images_cat_id: "20"
_4images_user_id: "424"
_4images_image_date: "2006-12-31T12:49:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8234 -->
Lichtschranke mit einem CNY 70 (Das kleine runde Loch)