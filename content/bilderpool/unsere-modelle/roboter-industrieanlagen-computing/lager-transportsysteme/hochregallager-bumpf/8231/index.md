---
layout: "image"
title: "Kassetten"
date: "2006-12-31T01:34:43"
picture: "HRL_Fischertechnik_Detail_026.jpg"
weight: "4"
konstrukteure: 
- "Walter-Mario Graf"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/8231
imported:
- "2019"
_4images_image_id: "8231"
_4images_cat_id: "20"
_4images_user_id: "424"
_4images_image_date: "2006-12-31T01:34:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8231 -->
Hier sieht man die verschiedenen Strichcode.