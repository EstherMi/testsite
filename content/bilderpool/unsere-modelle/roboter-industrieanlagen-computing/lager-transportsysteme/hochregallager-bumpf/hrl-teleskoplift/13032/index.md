---
layout: "image"
title: "Ansicht Lift"
date: "2007-12-12T07:32:25"
picture: "hrlmitteleskoplift4.jpg"
weight: "4"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/13032
imported:
- "2019"
_4images_image_id: "13032"
_4images_cat_id: "1182"
_4images_user_id: "424"
_4images_image_date: "2007-12-12T07:32:25"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13032 -->
Lift eingefahren