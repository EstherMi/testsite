---
layout: "image"
title: "Pixy Camera"
date: "2015-04-24T16:27:39"
picture: "pixysortieranlage11.jpg"
weight: "11"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/40887
imported:
- "2019"
_4images_image_id: "40887"
_4images_cat_id: "3071"
_4images_user_id: "2303"
_4images_image_date: "2015-04-24T16:27:39"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40887 -->
Hier geschieht die Prüfung der Farben.