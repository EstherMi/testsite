---
layout: "image"
title: "Vakuumgreifer"
date: "2015-04-24T16:27:39"
picture: "pixysortieranlage07.jpg"
weight: "7"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/40883
imported:
- "2019"
_4images_image_id: "40883"
_4images_cat_id: "3071"
_4images_user_id: "2303"
_4images_image_date: "2015-04-24T16:27:39"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40883 -->
