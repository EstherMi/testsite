---
layout: "image"
title: "Dreh-Eckenauswurf"
date: "2011-09-18T15:16:13"
picture: "industriemodell09.jpg"
weight: "11"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/31821
imported:
- "2019"
_4images_image_id: "31821"
_4images_cat_id: "2375"
_4images_user_id: "453"
_4images_image_date: "2011-09-18T15:16:13"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31821 -->
Hier drückt die Nockenscheibe den Hebel nach vorne und befördert die Räder von dem Drehteller.