---
layout: "image"
title: "Antriebe der Transportbänder"
date: "2011-09-18T15:16:13"
picture: "industriemodell24.jpg"
weight: "26"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/31836
imported:
- "2019"
_4images_image_id: "31836"
_4images_cat_id: "2375"
_4images_user_id: "453"
_4images_image_date: "2011-09-18T15:16:13"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31836 -->
