---
layout: "image"
title: "Antrieb vom Auswerfer"
date: "2011-09-18T15:16:13"
picture: "industriemodell21.jpg"
weight: "23"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/31833
imported:
- "2019"
_4images_image_id: "31833"
_4images_cat_id: "2375"
_4images_user_id: "453"
_4images_image_date: "2011-09-18T15:16:13"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31833 -->
