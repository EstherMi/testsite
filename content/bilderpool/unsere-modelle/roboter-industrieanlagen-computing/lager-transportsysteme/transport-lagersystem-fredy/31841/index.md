---
layout: "image"
title: "Antrieb für das Transportband"
date: "2011-09-18T15:16:14"
picture: "industriemodell29.jpg"
weight: "31"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/31841
imported:
- "2019"
_4images_image_id: "31841"
_4images_cat_id: "2375"
_4images_user_id: "453"
_4images_image_date: "2011-09-18T15:16:14"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31841 -->
