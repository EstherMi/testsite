---
layout: "image"
title: "Auswerfer"
date: "2011-09-18T15:16:13"
picture: "industriemodell18.jpg"
weight: "20"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/31830
imported:
- "2019"
_4images_image_id: "31830"
_4images_cat_id: "2375"
_4images_user_id: "453"
_4images_image_date: "2011-09-18T15:16:13"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31830 -->
Die folgenden Bilder zeigen wie der Auswerfer arbeitet.