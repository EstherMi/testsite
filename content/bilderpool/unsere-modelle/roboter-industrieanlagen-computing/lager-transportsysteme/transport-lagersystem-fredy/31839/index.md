---
layout: "image"
title: "Antriebe der Transportbänder"
date: "2011-09-18T15:16:13"
picture: "industriemodell27.jpg"
weight: "29"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/31839
imported:
- "2019"
_4images_image_id: "31839"
_4images_cat_id: "2375"
_4images_user_id: "453"
_4images_image_date: "2011-09-18T15:16:13"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31839 -->
Hier seht ihr die möglichkeit ein Schraub Zahnrad mittig für ein Transportband zu positionieren. Es kann nicht festgeschraubt werden, auf einer Seite des Transportbandes kommt also ein Rast Zahnrad auf die andere die Sparvariante.