---
layout: "image"
title: "Antriebe der Transportbänder"
date: "2011-09-18T15:16:13"
picture: "industriemodell26.jpg"
weight: "28"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/31838
imported:
- "2019"
_4images_image_id: "31838"
_4images_cat_id: "2375"
_4images_user_id: "453"
_4images_image_date: "2011-09-18T15:16:13"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31838 -->
