---
layout: "image"
title: "Antrieb vom Auswerfer"
date: "2011-09-18T15:16:13"
picture: "industriemodell23.jpg"
weight: "25"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/31835
imported:
- "2019"
_4images_image_id: "31835"
_4images_cat_id: "2375"
_4images_user_id: "453"
_4images_image_date: "2011-09-18T15:16:13"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31835 -->
