---
layout: "image"
title: "Transportwagen"
date: "2011-09-18T15:16:13"
picture: "industriemodell15.jpg"
weight: "17"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/31827
imported:
- "2019"
_4images_image_id: "31827"
_4images_cat_id: "2375"
_4images_user_id: "453"
_4images_image_date: "2011-09-18T15:16:13"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31827 -->
Der Wagen selber hat keinen Antrieb, deswegen koppelt er seine Achse dort in der Station ein und wird darüber angetrieben.