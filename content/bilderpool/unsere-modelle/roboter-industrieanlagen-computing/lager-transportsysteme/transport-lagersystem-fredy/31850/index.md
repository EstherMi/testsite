---
layout: "image"
title: "Rädchen Ausgabe"
date: "2011-09-18T15:16:14"
picture: "industriemodell38.jpg"
weight: "40"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/31850
imported:
- "2019"
_4images_image_id: "31850"
_4images_cat_id: "2375"
_4images_user_id: "453"
_4images_image_date: "2011-09-18T15:16:14"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31850 -->
Hier werden die Rädchen aus dem Lager befördert, der Greifer fährt von oben durch den Schlitz und die Räder fallen die Rutsche runter.