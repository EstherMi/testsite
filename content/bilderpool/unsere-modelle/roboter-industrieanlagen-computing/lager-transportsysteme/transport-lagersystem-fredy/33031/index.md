---
layout: "image"
title: "Tschüs"
date: "2011-10-01T21:36:32"
picture: "industrie2.jpg"
weight: "2"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/33031
imported:
- "2019"
_4images_image_id: "33031"
_4images_cat_id: "2375"
_4images_user_id: "453"
_4images_image_date: "2011-10-01T21:36:32"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33031 -->
das wars