---
layout: "comment"
hidden: true
title: "7418"
date: "2008-10-01T12:32:16"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
Wer hier von Hand mit dem Pinsel entstauben muß ist echt zu bedauern! Gibt es da keine Druckluft?