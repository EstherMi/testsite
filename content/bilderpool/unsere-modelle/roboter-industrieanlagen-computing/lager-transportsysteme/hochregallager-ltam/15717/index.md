---
layout: "image"
title: "52"
date: "2008-09-30T09:59:36"
picture: "hochregallagerltam52.jpg"
weight: "52"
konstrukteure: 
- "RS Components"
fotografen:
- "Claude Wolmering"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- details/15717
imported:
- "2019"
_4images_image_id: "15717"
_4images_cat_id: "1440"
_4images_user_id: "473"
_4images_image_date: "2008-09-30T09:59:36"
_4images_image_order: "52"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15717 -->
Hochregallager mit Ein- und Auslieferung, Prüfung und Sortierung der eingelagerten Produkten