---
layout: "overview"
title: "Paketwendeanlage"
date: 2019-12-17T19:06:13+01:00
legacy_id:
- categories/2114
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2114 --> 
Die Paketwendeanlage hat die Aufgabe, den Strichcode des Paketes zu erkennen und das Paket danach zur weiteren Bearbeitung weiterzugeben.  Die Anlage besteht aus 8 Motoren, von denen 7 Motoren in Rechts- und Linkslauf und variabler Drehzahl betrieben werden müssen. Des Weiteren sind 2 elektropneumatische Ventile zur Steuerung des Geifers und diverse Endtaster montiert.