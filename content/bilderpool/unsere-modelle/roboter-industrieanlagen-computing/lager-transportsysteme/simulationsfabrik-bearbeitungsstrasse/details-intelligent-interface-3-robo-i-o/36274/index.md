---
layout: "image"
title: "ROBO Interface + 3 ROBO I/O Extension"
date: "2012-12-15T13:32:46"
picture: "dftextak2.jpg"
weight: "2"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- details/36274
imported:
- "2019"
_4images_image_id: "36274"
_4images_cat_id: "2693"
_4images_user_id: "941"
_4images_image_date: "2012-12-15T13:32:46"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36274 -->
von links