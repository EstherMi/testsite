---
layout: "image"
title: "Regalbediengerät"
date: "2012-11-29T23:03:18"
picture: "simfabbearbeitungsstrassehochregallager44.jpg"
weight: "44"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- details/36218
imported:
- "2019"
_4images_image_id: "36218"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:18"
_4images_image_order: "44"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36218 -->
