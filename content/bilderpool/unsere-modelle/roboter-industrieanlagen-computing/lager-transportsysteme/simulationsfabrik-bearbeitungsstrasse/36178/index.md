---
layout: "image"
title: "Gesamtansicht von vorne 4"
date: "2012-11-29T23:03:17"
picture: "simfabbearbeitungsstrassehochregallager04.jpg"
weight: "4"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- details/36178
imported:
- "2019"
_4images_image_id: "36178"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:17"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36178 -->
