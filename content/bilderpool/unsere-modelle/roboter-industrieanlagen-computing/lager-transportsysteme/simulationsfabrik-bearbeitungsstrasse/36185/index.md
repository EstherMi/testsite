---
layout: "image"
title: "Linke Ecke oben"
date: "2012-11-29T23:03:17"
picture: "simfabbearbeitungsstrassehochregallager11.jpg"
weight: "11"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- details/36185
imported:
- "2019"
_4images_image_id: "36185"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:17"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36185 -->
von links nach rechts, Aufzugabnehmer, Scannerstation, Eckschieber 3, Pneumatischer Magnet-Umsetzer
im Hintergrund Kompressor, Eckschieber 4, Übergabestelle an Regalbediengerät (RBG)