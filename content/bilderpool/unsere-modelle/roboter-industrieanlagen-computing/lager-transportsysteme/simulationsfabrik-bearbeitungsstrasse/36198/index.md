---
layout: "image"
title: "Linke Ecke unten"
date: "2012-11-29T23:03:17"
picture: "simfabbearbeitungsstrassehochregallager24.jpg"
weight: "24"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- details/36198
imported:
- "2019"
_4images_image_id: "36198"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:17"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36198 -->
von rechts nach links, CNC2, Bohrstation, Eckschieber 2
im Hintergrund Stanze und Aufzug