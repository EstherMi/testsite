---
layout: "image"
title: "Linke Ecke hinten unten"
date: "2012-11-29T23:03:17"
picture: "simfabbearbeitungsstrassehochregallager28.jpg"
weight: "28"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- details/36202
imported:
- "2019"
_4images_image_id: "36202"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:17"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36202 -->
von rechts nach links, Stanze und Aufzug