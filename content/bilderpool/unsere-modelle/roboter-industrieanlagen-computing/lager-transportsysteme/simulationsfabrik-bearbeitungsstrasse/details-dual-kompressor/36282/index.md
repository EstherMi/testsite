---
layout: "image"
title: "Dual Kompressor im Detail"
date: "2012-12-16T11:08:15"
picture: "detailsdualkompressor3.jpg"
weight: "3"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- details/36282
imported:
- "2019"
_4images_image_id: "36282"
_4images_cat_id: "2692"
_4images_user_id: "941"
_4images_image_date: "2012-12-16T11:08:15"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36282 -->
Der Kompressor besteht aus zwei Pollin Luftpumpen und einem Drucklufttank, platzsparend senkrecht montiert. 
Als Adapter für die beiden unterschiedlich dicken Schläuche dient jeweils eine FT-Düse, wobei das sich verjüngende Ende aufgebohrt wurde.
In der "Simulationsfabrik mit Bearbeitungsstraße und Hochregallager" ist auf jeder Ebene so ein Kompressor verbaut. 
Im Vergleich zum blauen FT-Kompressor, geringe Stromaufnahme und vor allem leiser und deutlich weniger Vibrationen.
