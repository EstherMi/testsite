---
layout: "image"
title: "Frässtation CNC 1"
date: "2012-11-29T23:03:17"
picture: "simfabbearbeitungsstrassehochregallager07.jpg"
weight: "7"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- details/36181
imported:
- "2019"
_4images_image_id: "36181"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:17"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36181 -->
An der Frässtation dient das Förderband als Maschinentisch, die Fräse selbst hat zwei bewegliche Achsen und einen separaten Spindelmotor.