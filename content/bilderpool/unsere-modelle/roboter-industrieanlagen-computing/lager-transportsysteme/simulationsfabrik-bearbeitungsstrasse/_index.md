---
layout: "overview"
title: "Simulationsfabrik mit Bearbeitungsstraße und Hochregallager."
date: 2019-12-17T19:06:36+01:00
legacy_id:
- categories/2688
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2688 --> 
Die Anlage transportiert Holzwerkstücke auf Förderbändern und Schiebern, simuliert die Bearbeitung der Werkstücke an diversen Stationen und lagert dann die Werkstücke im Hochregallager ein. Das ganze geschieht im Kreislauf. Für jedes neu eingelagerte Werkstück wird ein anderes bereits lagerndes Werkstück ausgelagert und wieder an die Bearbeitungsstraße übergeben.
 
Die Anlage besteht aus zwei übereinander liegenden Ebenen, jeweils auf eine 8 mm Holzplatten montiert, wobei die Grundfläche unten ca. 78x37cm und oben 78x32cm beträgt. 

Die untere Ebene wird von einem Intelligent Interface verbunden mit drei ROBO I/O Extension gesteuert. Die Werkstücke werden von rechts nach links transportiert, beginnend mit der Werkstückrutsche oder mit dem Anlauf-Registerlager.  Danach geht's weiter zur Putzstation,  weiter zum Eckschieber 1, zur Schweißstation,  CNC Fräße 1, CNC Fräße 2, Bohrstation, dann Eckschieber 2, Stanze und zu Letzt mit dem Aufzug weiter zur oberen Ebene.

Die obere Ebene wird von drei ROBO TX gesteuert. Die Werkstücke werden hier von links nach rechts transportiert, beginnend mit dem Aufzugabnehmer, dann weiter durch die Scannerstation, zum Eckschieber 3 und weiter zum Magnet-Umsetzer. Dieser übergibt die Werkstücke zuerst rechts an die Werkstückwendeanlage und danach auf das Band hinter zum Eckschieber 4, der als Übergabestelle an das Regalbediengerät (RBG) fungiert. Das RBG lagert dann ein Werkstück in einem freien Lagerplatz ein und lagert ein anderes bereits lagerndes Werkstück danach über die Werkstückrutsche wieder aus. Der Der Der Kreislauf beginnt von vorne.

Weitere Bilder und auch ein Video folgen in Kürze.
-
Hier geht zu Bildern meiner alten Anlage:
http://www.ftcommunity.de/categories.php?cat_id=1625
