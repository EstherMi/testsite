---
layout: "comment"
hidden: true
title: "17598"
date: "2012-12-01T13:53:10"
uploadBy:
- "tz"
license: "unknown"
imported:
- "2019"
---
Der obere Teil des Umsetzers sitzt auf einem Schlitten, dieser wird von einer Hub-Zahnstange im ca. 45 Grad Winkel geführt.  Die beiden unteren Pneumatik- Zylinder heben den Schlitten um ca. 2 cm an, dieser und das aufgenommene Werkstück bleiben währenddessen in der Waagrechten. Jetzt erst kann sich der Umsetzer mitsamt dem Werkstück drehen.