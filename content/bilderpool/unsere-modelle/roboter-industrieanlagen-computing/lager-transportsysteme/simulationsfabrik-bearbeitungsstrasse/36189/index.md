---
layout: "image"
title: "Mitte oben"
date: "2012-11-29T23:03:17"
picture: "simfabbearbeitungsstrassehochregallager15.jpg"
weight: "15"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- details/36189
imported:
- "2019"
_4images_image_id: "36189"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:17"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36189 -->
von links nach rechts, Pneumatischer Magnet-Umsetzer, Werkstück-Wendeanlage
im Hintergrund Kompressor, Eckschieber 4, Übergabestelle an Regalbediengerät (RBG)