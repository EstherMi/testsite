---
layout: "image"
title: "Blick unter die drei ROBO TX"
date: "2012-11-29T23:03:18"
picture: "simfabbearbeitungsstrassehochregallager58.jpg"
weight: "58"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- details/36232
imported:
- "2019"
_4images_image_id: "36232"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:18"
_4images_image_order: "58"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36232 -->
Die ROBO TX lassen sich nach oben klappen, darunter befinden sich ein Teil der Verkabelung sowie die Magnetventile der Pneumatik.