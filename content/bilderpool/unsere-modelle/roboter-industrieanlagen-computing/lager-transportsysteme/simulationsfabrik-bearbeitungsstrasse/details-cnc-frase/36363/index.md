---
layout: "image"
title: "CNC Fräse / fertig verkabelt"
date: "2012-12-28T17:09:59"
picture: "detailscncfraese09.jpg"
weight: "9"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- details/36363
imported:
- "2019"
_4images_image_id: "36363"
_4images_cat_id: "2699"
_4images_user_id: "941"
_4images_image_date: "2012-12-28T17:09:59"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36363 -->
Der Motor XS unten sitzt an einem Hubgetriebe und bewegt die komplette Fräse vor und zurück.

Der in der Mitte verbaute S-Motor hebt und senkt den Fräskopf.
Direkt davor sitzt mittig der dazugehörige Endtaster.

Im Fräskopf selbst sitzt ein Motor XS der die Spindel antreibt.

Die Kabel sind aus alten IDE Festplattenkabeln selbstgemacht.

Die passenden Stecker gibts z.B. bei Conrad / Pollin / usw.