---
layout: "image"
title: "CNC Fräse Bauphase"
date: "2012-12-28T17:09:58"
picture: "detailscncfraese03.jpg"
weight: "3"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- details/36357
imported:
- "2019"
_4images_image_id: "36357"
_4images_cat_id: "2699"
_4images_user_id: "941"
_4images_image_date: "2012-12-28T17:09:58"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36357 -->
Im Fräskopf sitzt ein Motor XS der die Spindel antreibt.