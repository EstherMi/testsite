---
layout: "image"
title: "Einbauposition in der Anlage"
date: "2012-12-28T17:09:59"
picture: "detailscncfraese10.jpg"
weight: "10"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- details/36364
imported:
- "2019"
_4images_image_id: "36364"
_4images_cat_id: "2699"
_4images_user_id: "941"
_4images_image_date: "2012-12-28T17:09:59"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36364 -->
In der Mitte ist der Wannenstecker zu sehen, in den das Versorgungskabel der CNC Fräse eingesteckt wird.
Der Wannenstecker sitzt auf einem kleinen Stück Lochrasterplatine und ist auf der Rückseite mit Kabeln verlötet.

Rechts daneben ist der Endtaster für die X-Achse.

Unten sind die zwei parallelen Hub-Zahnstangen zu sehen, auf denen die CNC Fräse vor und zurück fahren kann. (X-Achse)

Diese Bild stammt aus der Bauphase.