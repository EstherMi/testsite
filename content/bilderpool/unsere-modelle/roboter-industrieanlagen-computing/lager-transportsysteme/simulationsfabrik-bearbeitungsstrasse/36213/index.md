---
layout: "image"
title: "Die drei ROBO TX"
date: "2012-11-29T23:03:18"
picture: "simfabbearbeitungsstrassehochregallager39.jpg"
weight: "39"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- details/36213
imported:
- "2019"
_4images_image_id: "36213"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:18"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36213 -->
Die drei ROBO TX steuern die obere Ebene