---
layout: "image"
title: "Mitte unten"
date: "2012-11-29T23:03:17"
picture: "simfabbearbeitungsstrassehochregallager22.jpg"
weight: "22"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- details/36196
imported:
- "2019"
_4images_image_id: "36196"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:17"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36196 -->
von rechts nach links, Schweißstation, CNC1, CNC2
im Hintergrund IF und Extensions