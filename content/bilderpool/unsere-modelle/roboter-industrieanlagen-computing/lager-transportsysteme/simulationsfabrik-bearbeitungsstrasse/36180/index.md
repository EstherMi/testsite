---
layout: "image"
title: "Unten mitte"
date: "2012-11-29T23:03:17"
picture: "simfabbearbeitungsstrassehochregallager06.jpg"
weight: "6"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- details/36180
imported:
- "2019"
_4images_image_id: "36180"
_4images_cat_id: "2688"
_4images_user_id: "941"
_4images_image_date: "2012-11-29T23:03:17"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36180 -->
von rechts nach links, Schweißstation, Frässtation CNC 1, Frässtation CNC 2