---
layout: "image"
title: "Hochregallager 8"
date: "2007-08-31T10:04:12"
picture: "hochregallagerstefanl08.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/11425
imported:
- "2019"
_4images_image_id: "11425"
_4images_cat_id: "1022"
_4images_user_id: "502"
_4images_image_date: "2007-08-31T10:04:12"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11425 -->
.... hier in den roten Eckbaustein hinein und zieht so die einzelnen Fächer heraus.