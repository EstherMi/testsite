---
layout: "image"
title: "Hochregallager 1"
date: "2007-08-31T10:04:12"
picture: "hochregallagerstefanl01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/11418
imported:
- "2019"
_4images_image_id: "11418"
_4images_cat_id: "1022"
_4images_user_id: "502"
_4images_image_date: "2007-08-31T10:04:12"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11418 -->
Hier mal ein etwas anderes Hochregallager. Es werden Bleche in den einzelnen Fächer gelagert welche nur ein und  ausgelagert werden können.