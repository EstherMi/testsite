---
layout: "image"
title: "Hochregallager 6"
date: "2007-08-31T10:04:12"
picture: "hochregallagerstefanl06.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/11423
imported:
- "2019"
_4images_image_id: "11423"
_4images_cat_id: "1022"
_4images_user_id: "502"
_4images_image_date: "2007-08-31T10:04:12"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11423 -->
Die Kette kann sich wenn er hochfährt in dem roten Kasten unten sammeln.