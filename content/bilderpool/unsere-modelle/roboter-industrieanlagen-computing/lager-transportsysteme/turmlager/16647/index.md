---
layout: "image"
title: "Antrieb der Schnecke"
date: "2008-12-16T18:07:12"
picture: "johannes23.jpg"
weight: "23"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/16647
imported:
- "2019"
_4images_image_id: "16647"
_4images_cat_id: "1506"
_4images_user_id: "636"
_4images_image_date: "2008-12-16T18:07:12"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16647 -->
