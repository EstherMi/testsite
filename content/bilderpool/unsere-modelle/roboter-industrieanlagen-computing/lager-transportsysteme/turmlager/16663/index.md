---
layout: "image"
title: "Gesamtansicht des gefüllten Lagers"
date: "2008-12-16T18:07:12"
picture: "johannes39.jpg"
weight: "39"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/16663
imported:
- "2019"
_4images_image_id: "16663"
_4images_cat_id: "1506"
_4images_user_id: "636"
_4images_image_date: "2008-12-16T18:07:12"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16663 -->
