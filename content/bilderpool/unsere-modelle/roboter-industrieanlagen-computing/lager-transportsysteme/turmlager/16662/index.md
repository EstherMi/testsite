---
layout: "image"
title: "Gefülltes Lager"
date: "2008-12-16T18:07:12"
picture: "johannes38.jpg"
weight: "38"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/16662
imported:
- "2019"
_4images_image_id: "16662"
_4images_cat_id: "1506"
_4images_user_id: "636"
_4images_image_date: "2008-12-16T18:07:12"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16662 -->
