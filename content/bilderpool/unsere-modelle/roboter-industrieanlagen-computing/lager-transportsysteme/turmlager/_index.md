---
layout: "overview"
title: "Turmlager"
date: 2019-12-17T19:06:00+01:00
legacy_id:
- categories/1506
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1506 --> 
von Johannes Westphal[br][br]Dieses Modell stellt ein komplettes Lager dar, das kreisförmig aufgebaut ist. Es besitzt 15 Fächer, die es zum Ein- und Auslagern ansteuern kann. Dabei kann jedes Fach auch ohne PC einzeln zum Auslagern ansgewählt werden. Die Ein- und Auslagerung erfolgt sehr präzise und mit relativ hoher Geschwindigkeit.[br][br]Technische Daten:[br]Länge: 80cm[br]Breite: 40cm[br]Höhe: 40cm[br]Anzahl der Fächer: 15[br][br]Verwendetes Material:[br]7 Taster[br]4 Motoren[br]3 Elektromagnetventile[br]6 Pneumatikzylinder[br]2 Fototransistoren[br]1 Potentiometer[br]4m Schlauch[br]8m Kabel