---
layout: "image"
title: "Gesamtansicht von oben"
date: "2008-12-16T18:07:11"
picture: "johannes03.jpg"
weight: "3"
konstrukteure: 
- "Johannes Westphal"
fotografen:
- "Johannes Westphal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes93"
license: "unknown"
legacy_id:
- details/16627
imported:
- "2019"
_4images_image_id: "16627"
_4images_cat_id: "1506"
_4images_user_id: "636"
_4images_image_date: "2008-12-16T18:07:11"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16627 -->
