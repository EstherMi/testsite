---
layout: "image"
title: "Detail Streichholzschachtel-Reservoir"
date: "2014-10-20T21:59:38"
picture: "4_-_Detail_Streichholzschachtel-Reservoir.jpg"
weight: "4"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/39714
imported:
- "2019"
_4images_image_id: "39714"
_4images_cat_id: "2979"
_4images_user_id: "724"
_4images_image_date: "2014-10-20T21:59:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39714 -->
