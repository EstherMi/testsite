---
layout: "image"
title: "Detail Ausstoßer"
date: "2014-10-20T21:59:38"
picture: "5_-_Detail_Ausstosser.jpg"
weight: "5"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/39715
imported:
- "2019"
_4images_image_id: "39715"
_4images_cat_id: "2979"
_4images_user_id: "724"
_4images_image_date: "2014-10-20T21:59:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39715 -->
Die Luftzylinder fahren aus und stoßen eine Schachtel aus.
Dann fahren sie wieder zurück und der Stapel rutscht nach unten.