---
layout: "image"
title: "Detail Impulstaster Hubgetriebe"
date: "2014-10-20T21:59:39"
picture: "9_-_Detail_Impulstaster_Hubgetriebe.jpg"
weight: "9"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/39719
imported:
- "2019"
_4images_image_id: "39719"
_4images_cat_id: "2979"
_4images_user_id: "724"
_4images_image_date: "2014-10-20T21:59:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39719 -->
Hiermit kann ich ermitteln, wo sich der Schieber befindet.
Vorher muß natürlich ein Endschalter angefahren worden sein.