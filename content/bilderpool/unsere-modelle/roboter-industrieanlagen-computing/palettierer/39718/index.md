---
layout: "image"
title: "Detail Schieber"
date: "2014-10-20T21:59:39"
picture: "8_-_Detail_Schieber.jpg"
weight: "8"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/39718
imported:
- "2019"
_4images_image_id: "39718"
_4images_cat_id: "2979"
_4images_user_id: "724"
_4images_image_date: "2014-10-20T21:59:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39718 -->
Der Schieber schiebt erst 2 Schachteln längs, dann 3 Schachteln quer (gedreht) auf den Fallboden.
Dieser öffnet sich und die Schachteln gleiten sanft auf die darunterliegende Palette.
Der Lift fährt 1 Lage nach unten.
Der Boden schließt sich wieder.
Dann kommen 3 Schachteln quer und 2 Schachteln längs.
Und das immer im Wechsel.