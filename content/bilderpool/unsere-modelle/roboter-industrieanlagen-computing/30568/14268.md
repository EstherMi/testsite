---
layout: "comment"
hidden: true
title: "14268"
date: "2011-05-16T17:58:45"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo marspau,
der Commodore C64 hat die CPU MOS 6510 1,023MHz in der NTSC-Vers. sowie 0,985MHz in der PAL-Vers. und der C128 MOS 8502 2MHz sowie Zilog Z80A 4 MHz. Lange ist es her, mußte nachsehen :o)
Gruß, Udo2