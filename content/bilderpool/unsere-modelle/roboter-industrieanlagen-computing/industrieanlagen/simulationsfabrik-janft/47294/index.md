---
layout: "image"
title: "Sortieranlage Schrägansicht"
date: "2018-02-15T18:59:05"
picture: "simulationsfabrikjanft08.jpg"
weight: "8"
konstrukteure: 
- "Jan"
fotografen:
- "Jan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- details/47294
imported:
- "2019"
_4images_image_id: "47294"
_4images_cat_id: "3499"
_4images_user_id: "1164"
_4images_image_date: "2018-02-15T18:59:05"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47294 -->
