---
layout: "image"
title: "Vakuuumerzeuger"
date: "2018-02-15T18:59:10"
picture: "simulationsfabrikjanft12.jpg"
weight: "12"
konstrukteure: 
- "Jan"
fotografen:
- "Jan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- details/47298
imported:
- "2019"
_4images_image_id: "47298"
_4images_cat_id: "3499"
_4images_user_id: "1164"
_4images_image_date: "2018-02-15T18:59:10"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47298 -->
Hierr kann man sehr gut den Endaschalter und den Impulszähler sehen. Der Erzeuger wird im Programm über die vierte Achse angesprochen