---
layout: "overview"
title: "Simulationsfabrik (Janft)"
date: 2019-12-17T19:02:07+01:00
legacy_id:
- categories/3499
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3499 --> 
Hier zu sehen ist meine Simulationsfabrik nach Vorbild der FT-Industriemodelle.
Sie besteht aus einer Sortieranlage, einem 3-Achs-Greifer und einem Brennofen.
