---
layout: "image"
title: "Brennofen Frontansicht"
date: "2018-02-15T18:59:05"
picture: "simulationsfabrikjanft04.jpg"
weight: "4"
konstrukteure: 
- "Jan"
fotografen:
- "Jan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Janft"
license: "unknown"
legacy_id:
- details/47290
imported:
- "2019"
_4images_image_id: "47290"
_4images_cat_id: "3499"
_4images_user_id: "1164"
_4images_image_date: "2018-02-15T18:59:05"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47290 -->
Funktionsweise:

Das Werkstück fällt über die kleine Rutsche auf das Förderband und wird in den Brennofen gefahren zum Brennen. Anschliessen wird es auf das kleine Förderband gefahren um an die Sortieranlage übergeben werden zu können. Währenddessen kann schon das nächste Werkstück in de Ofen fahren.