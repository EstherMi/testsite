---
layout: "image"
title: "vollautomatischeblechbearbeitungsanlage02.jpg"
date: "2015-01-04T07:47:31"
picture: "vollautomatischeblechbearbeitungsanlage02.jpg"
weight: "2"
konstrukteure: 
- "MiK11"
fotografen:
- "MiK11"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MiK11"
license: "unknown"
legacy_id:
- details/40161
imported:
- "2019"
_4images_image_id: "40161"
_4images_cat_id: "3017"
_4images_user_id: "1258"
_4images_image_date: "2015-01-04T07:47:31"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40161 -->
