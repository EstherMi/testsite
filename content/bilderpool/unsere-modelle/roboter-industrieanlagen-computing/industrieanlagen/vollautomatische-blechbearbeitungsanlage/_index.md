---
layout: "overview"
title: "Vollautomatische Blechbearbeitungsanlage"
date: 2019-12-17T19:01:57+01:00
legacy_id:
- categories/3017
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3017 --> 
Die vollautomatische Blechbearbeitungsanlage besteht aus einem Roboterarm mit Vakuumgreifer, einer Blechvorlage mit Lichtschranke, einer Reinigungsstation, einer Schweissstation und einem Auswurfförderband. Die Steuerungssoftware wurde bewusst nicht auf maximalen Durchsatz optimiert. Ein Video findet Ihr hier: http://youtu.be/ZVD5b5BQInY