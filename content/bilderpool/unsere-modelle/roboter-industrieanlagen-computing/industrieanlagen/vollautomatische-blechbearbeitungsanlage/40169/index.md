---
layout: "image"
title: "vollautomatischeblechbearbeitungsanlage10.jpg"
date: "2015-01-04T07:47:31"
picture: "vollautomatischeblechbearbeitungsanlage10.jpg"
weight: "9"
konstrukteure: 
- "MiK11"
fotografen:
- "MiK11"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MiK11"
license: "unknown"
legacy_id:
- details/40169
imported:
- "2019"
_4images_image_id: "40169"
_4images_cat_id: "3017"
_4images_user_id: "1258"
_4images_image_date: "2015-01-04T07:47:31"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40169 -->
