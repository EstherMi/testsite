---
layout: "image"
title: "Die Fräse"
date: "2012-07-30T18:30:30"
picture: "pneumatischebearbeitungsstrasse15.jpg"
weight: "19"
konstrukteure: 
- "Jan Werner"
fotografen:
- "Jan Werner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "werner"
license: "unknown"
legacy_id:
- details/35228
imported:
- "2019"
_4images_image_id: "35228"
_4images_cat_id: "2611"
_4images_user_id: "1196"
_4images_image_date: "2012-07-30T18:30:30"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35228 -->
Für diesen Motor war am TX leider kein Ausgang mehr frei...