---
layout: "image"
title: "Ansicht vorne"
date: "2012-07-30T18:30:16"
picture: "pneumatischebearbeitungsstrasse05.jpg"
weight: "9"
konstrukteure: 
- "Jan Werner"
fotografen:
- "Jan Werner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "werner"
license: "unknown"
legacy_id:
- details/35218
imported:
- "2019"
_4images_image_id: "35218"
_4images_cat_id: "2611"
_4images_user_id: "1196"
_4images_image_date: "2012-07-30T18:30:16"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35218 -->
