---
layout: "image"
title: "Verkabelung Gesamtansicht"
date: "2012-08-01T13:07:11"
picture: "pneumatischebearbeitungsstrasse1.jpg"
weight: "2"
konstrukteure: 
- "Jan Werner"
fotografen:
- "Jan Werner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "werner"
license: "unknown"
legacy_id:
- details/35233
imported:
- "2019"
_4images_image_id: "35233"
_4images_cat_id: "2611"
_4images_user_id: "1196"
_4images_image_date: "2012-08-01T13:07:11"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35233 -->
Ich habe (mehr oder weniger) alles bis auf die Elektrobauteile und Kabel abgebaut