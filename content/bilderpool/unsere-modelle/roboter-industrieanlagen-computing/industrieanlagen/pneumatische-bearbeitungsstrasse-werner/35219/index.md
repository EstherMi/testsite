---
layout: "image"
title: "Ansicht schräghinten"
date: "2012-07-30T18:30:16"
picture: "pneumatischebearbeitungsstrasse06.jpg"
weight: "10"
konstrukteure: 
- "Jan Werner"
fotografen:
- "Jan Werner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "werner"
license: "unknown"
legacy_id:
- details/35219
imported:
- "2019"
_4images_image_id: "35219"
_4images_cat_id: "2611"
_4images_user_id: "1196"
_4images_image_date: "2012-07-30T18:30:16"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35219 -->
