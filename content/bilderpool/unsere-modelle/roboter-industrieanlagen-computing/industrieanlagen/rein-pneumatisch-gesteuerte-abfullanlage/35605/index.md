---
layout: "image"
title: "Die Füllköpfe"
date: "2012-10-01T20:50:58"
picture: "reinpneumatischgesteuerteabfuellanlage14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/35605
imported:
- "2019"
_4images_image_id: "35605"
_4images_cat_id: "2642"
_4images_user_id: "104"
_4images_image_date: "2012-10-01T20:50:58"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35605 -->
Diese vier Füllköpfe müssen so abgesenkt werden, dass sie in die Flaschenöffnungen hinein ragen. Die Köpfe gleiten an den vorderen beiden Metallstangen frei auf und - durch ihr Eigengewicht - ab. Der im Inneren des Turms bei genauem Hinsehen erkennbare senkrecht eingebaute Pneumatikzylinder schiebt seinen eigenen BS30 auf einem eigenen Metallstangenpaar hoch und runter. Der BS30 trägt am unteren Ende einen Verbinder 15, der nur so weit eingeschoben ist, dass er den vorderen BS30 - den von den Füllköpfen nämlich - anheben kann.

Wenn der Pneumatikzylinder eingefahren wird, senkt sich also sein BS30. Der Füllkopfträger folgt durch sein Eigengewicht dieser Bewegung. Aber nur so weit, bis er z.B. auf den Flaschen oder sonst einer Begrenzung aufsitzt. Der Pneumatikzylinder kann ruhig noch etwas weiter nach unten gehen; er wird aber nicht den Füllkopfträger mit Gewalt nach unten ziehen und etwa Flaschen zerdrücken.

Die Füllköpfe sind einfach Pneumatikschläuche mit einigen Rastachsenkupplungen drüber gestülpt. In der echten Anlage waren das spitz zulaufende Edelstahlrohre mit einer seitlichen Öffnung, um die recht schwere Gewichte gleiten konnten, die unten auch konisch ausgeführt waren. Dadurch zentrierten diese Gewichte und die Rohre die Flaschenöffnungen sauber. Die Rohre drangen durch die Öffnungen in die Flaschen ein, während die Gewichte auf den Flaschen ruhten. Zur Seite hin strömte dann die gepumpte Flüssigkeit aus den Rohren ins Innere der Flaschen.

Eine kleine Anekdote: Mein Vater kam mal mit einem nagelneuen Anzug von einer Geschäftsreise zurück, während wir - die Familie Falk war komplett in den Familienbetrieb eingespannt - gerade am Abfüllen waren. Manche der Kunststoffflaschen waren nicht sauber durch den Spritzdruck gegangen und hatten im Inneren direkt unter der Öffnung Plastikreste. An denen verfingen sich manchmal beim wieder Hochziehen die seitlichen Öffnungen der Füllrohre. Dann passierte folgendes: Die gefüllte Flasche wurde mit den Füllrohren angehoben. Wenn man sie nicht rechtzeitig abnahm, begann schon der nächste Füllvorgang. Die pechschwarze Flüssigkeit spritzte dann - die Flasche war ja schon gefüllt - mit hohem Druck durch die Gegend. Genau da stand mein Vater, der gerade zu einer solchen Situation herein kam, das Problem sah und gleich eingreifen wollte. Die lauten Rufe meiner Mutter konnten ihn nicht abhalten, aber es war zu spät: eine Sekunde später war er von oben bis unten mit Premix vollgespritzt - und der neue Anzug nicht mehr zu retten. Große Begeisterung seitens meiner Mutter... nun gut! Das ist alles über 30 Jahre her.