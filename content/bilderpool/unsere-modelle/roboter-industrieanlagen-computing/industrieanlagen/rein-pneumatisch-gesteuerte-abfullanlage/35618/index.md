---
layout: "image"
title: "Anheben der Füllköpfe"
date: "2012-10-01T20:50:59"
picture: "reinpneumatischgesteuerteabfuellanlage27.jpg"
weight: "27"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/35618
imported:
- "2019"
_4images_image_id: "35618"
_4images_cat_id: "2642"
_4images_user_id: "104"
_4images_image_date: "2012-10-01T20:50:59"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35618 -->
Wenn das Zeitglied abläuft und also genug abgefüllt wurde, steuert sein Ventil diesen Zylinder wieder um, sodass der Füllkopfträger angehoben wird. In dessen oberen Endlage befindet sich wiederum ein Endlagenventil, welches bewirkt, dass die Stopperzylinder - eine Zeit lang - umgeschaltet werden. So laufen die gefüllten Flaschen ab, ohne dass gleich zu schnell neue nachkommen.