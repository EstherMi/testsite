---
layout: "image"
title: "Schlauch-Abklemmung"
date: "2012-10-01T20:50:59"
picture: "reinpneumatischgesteuerteabfuellanlage20.jpg"
weight: "20"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/35611
imported:
- "2019"
_4images_image_id: "35611"
_4images_cat_id: "2642"
_4images_user_id: "104"
_4images_image_date: "2012-10-01T20:50:59"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35611 -->
Diese von zwei einfachwirkenden Pneumatikzylindern betätigte Kniehebelmechanik drückt die beiden wasserführenden Schläuche mit dem Winkelstein 15° zuverlässig ab. Man braucht dazu ganz erhebliche Kraft, und auch dieser Aufbau schafft das nur mit den älteren hellblauen Pneumatikschläuchen. Die heutigen dunkelblauen sind viel zu "stabil", um davon abgedrückt zu werden.

Diese Baugruppe sollte dazu dienen, ein Nachtropfen der Füllköpfe zu vermeiden. Allerdings tropfen sie, vermutlich durch die Elastizität der Schläuche, trotzdem nach, wenn die Pumpe abgeschaltet und die Schläuche zu geklemmt sind. Die Lösung war (siehe spätere Bilder) ein etwas vorzeitiges Abschalten der Pumpe vor dem Anheben der Füllköpfe. Der Aufbau und die Kniehebelmechanik hier gefielen mir aber doch so gut, dass ich ihn drin gelassen hatte, obwohl damit tatsächlich nichts Nützliches oder gar Notwendiges zur Anlage beigetragen wird.

In der echten Anlage sorgten die auf den Füllrohren herabgleitenden Gewichte, die die seitlichen Öffnungen der Rohre abdichteten, dafür, dass nichts nennenswert nachtropfte.