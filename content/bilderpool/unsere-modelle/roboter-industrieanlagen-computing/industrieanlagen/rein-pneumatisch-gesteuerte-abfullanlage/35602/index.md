---
layout: "image"
title: "Die Zählschaltung"
date: "2012-10-01T20:50:58"
picture: "reinpneumatischgesteuerteabfuellanlage11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/35602
imported:
- "2019"
_4images_image_id: "35602"
_4images_cat_id: "2642"
_4images_user_id: "104"
_4images_image_date: "2012-10-01T20:50:58"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35602 -->
Ab Einfachbetätiger links unten kommt also Druckluft an, sobald die erste Flasche die Staudüse abdeckt. Dessen Gummibalg drückt den schwarzen BS15 nach rechts bis zum Anschlag. Der auf dem roten Baustein 5 * 15 * 30 per Verbinder 15 aufgesetzte (alte, 5 mm lange) Klemmring drückt den blauen Stößel des ganz genau justierten Ventils. Dadurch könnte Druckluft von dessen hinteren Eingang in seinen vorderen Ausgang zum nächsten Betätiger gelangen - aber eben erst, wenn auch welche da ist. Die kommt nämlich erst, wenn die zu zählende Flasche an der Staudüse vorbei ist und die Schwingfeder also wieder losgelassen wurde.

Erst dann also drückt der zweite Betätiger von links seinen Schalthebel nach rechts und betätigt das dritte Ventil. Das lässt Druckluft durch, wenn die Staudüse von der zweiten Flasche betätigt wird - beachtet den Schlauch von links unten zum Eingang des zweiten Ventils.

Auf diese Art werden die Halbschritte "Flasche da" und "Flasche durch" immer durch eine Kombination zweier solcher Schaltgruppen durchgereicht. Von dieser Gruppe aus zwei Betätigern mit zwei Ventilen gibt es insgesamt vier. Wenn das Signal wie beschrieben beim letzten der insgesamt acht Ventile ankommt, ist also die vierte Flasche am Detektor vorbei gelaufen und hat ihn losgelassen. Der Füllvorgang kann beginnen.

Die lange Bausteinreihe unten geht über alle acht Schaltgruppen. Sie wird nach abgeschlossenem Füllvorgang von zwei Pneumatikzylindern (einer hätte nicht genug Kraft) kurz nach ganz links geschoben und dann wieder nach rechts gezogen. Dabei drücken die gelben I-Streben 30 die ganzen nach rechts gedrückten Schalthebel auf ein Mal wieder nach links. So wird das Zählwerk wieder auf Null zurück gestellt und ist bereit für die nächste Vierergruppe von Flaschen.