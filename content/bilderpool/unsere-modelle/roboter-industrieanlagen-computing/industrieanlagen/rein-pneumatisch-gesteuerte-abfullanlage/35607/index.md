---
layout: "image"
title: "Füllen"
date: "2012-10-01T20:50:58"
picture: "reinpneumatischgesteuerteabfuellanlage16.jpg"
weight: "16"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/35607
imported:
- "2019"
_4images_image_id: "35607"
_4images_cat_id: "2642"
_4images_user_id: "104"
_4images_image_date: "2012-10-01T20:50:58"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35607 -->
Das im letzten Bild besprochene Ventil leitet die Druckluft an zwei Stellen, von denen eine der hier sichtbare Doppelbetätiger ist. Der drückt a) den Taster links, der die Füllpumpe einschaltet und b) das Ventil rechts, welches die später noch zu besprochene Schlauchabklemmung entlastet (es ist ein Öffner mit rotem Stößel).