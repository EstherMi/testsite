---
layout: "image"
title: "Förderbandantrieb"
date: "2012-10-01T20:50:58"
picture: "reinpneumatischgesteuerteabfuellanlage03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/35594
imported:
- "2019"
_4images_image_id: "35594"
_4images_cat_id: "2642"
_4images_user_id: "104"
_4images_image_date: "2012-10-01T20:50:58"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35594 -->
Das Förderband war ein fast weißes, ganz glattes Kunststoffband, welches einfach endlos durchlief. Die Flaschen wurden an der Füll- und Verschließstation per "Stopperzylinder" angehalten. Das ist nichts anderes, als ein Pneumatikzylinder, dessen Kolben einfach quer in die Förderbahn hinein ausgefahren wurde. Die Flaschen waren leicht und das Band rutschend genug, dass das genügte. Das Band rutschte dann also einfach unter den festgehaltenen Flaschen durch.