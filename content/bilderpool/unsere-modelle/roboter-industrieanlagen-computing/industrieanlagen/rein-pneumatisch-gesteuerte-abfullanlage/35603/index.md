---
layout: "image"
title: "Der Auslöser des Zählers"
date: "2012-10-01T20:50:58"
picture: "reinpneumatischgesteuerteabfuellanlage12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/35603
imported:
- "2019"
_4images_image_id: "35603"
_4images_cat_id: "2642"
_4images_user_id: "104"
_4images_image_date: "2012-10-01T20:50:58"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35603 -->
Links unten im Bild sieht man das letzte Ventil der Zählschaltung. Das startet dann das Absenken des Füllkopfträgers.