---
layout: "image"
title: "Signalverstärkung"
date: "2012-10-01T20:50:58"
picture: "reinpneumatischgesteuerteabfuellanlage09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/35600
imported:
- "2019"
_4images_image_id: "35600"
_4images_cat_id: "2642"
_4images_user_id: "104"
_4images_image_date: "2012-10-01T20:50:58"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35600 -->
Hier sieht man die von der Staudüse angesteuerte Ventilgruppe von der anderen Seite. Damit das ganze gut funktioniert, sind kurze Schlauchlängen notwendig. Je länger der Schlauch von der Staudüse zum Betätiger ist, desto mehr Druckluft muss ja wegen des größeren Volumens hineingepumpt werden, um den Betätiger noch auszulösen. Deshalb ist diese Ventilgruppe direkt bei der Staudüse montiert.