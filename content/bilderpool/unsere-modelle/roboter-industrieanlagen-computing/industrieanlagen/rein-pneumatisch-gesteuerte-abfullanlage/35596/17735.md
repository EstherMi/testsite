---
layout: "comment"
hidden: true
title: "17735"
date: "2013-01-21T13:16:13"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Doch, ich muss schon bis 4 zählen. Sonst könnte die Maschine ja füllen, bloß weil sich eine Flasche verhakt (außerdem sollte es ja so originalgetreu wie möglich sein, und das Original zählte auch). Das Zählen geschieht mit einer pneumatisch-mechanischen Ventilorgie an der Hinterseite des Modells.

Gruß,
Stefan