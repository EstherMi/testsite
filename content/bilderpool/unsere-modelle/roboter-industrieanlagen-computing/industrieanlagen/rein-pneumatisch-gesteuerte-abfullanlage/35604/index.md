---
layout: "image"
title: "Absenken der Füllköpfe"
date: "2012-10-01T20:50:58"
picture: "reinpneumatischgesteuerteabfuellanlage13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/35604
imported:
- "2019"
_4images_image_id: "35604"
_4images_cat_id: "2642"
_4images_user_id: "104"
_4images_image_date: "2012-10-01T20:50:58"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35604 -->
Der Ausgang des achten Zählventils geht auf den hier sichtbaren Doppelbetätiger (der auf einem der vorherigen Bilder schon einmal links neben der Zählschaltung sichtbar war). Dieser drückt wieder je einen Öffner und einen Schließer, an dessen Ausgängen die beiden Anschlüsse des senkrecht im Füllturm eingebauten Pneumatikzylinders angeschlossen sind. Bei mit Druckluft beaufschlagtem Betätiger senken sich deshalb die Füllköpfe. Den Zylinder sieht man auf den weiteren Bildern noch.

Damit das nicht schlagend, sondern schön langsam und gleichmäßig passiert, ist eine Kombination aus Drossel (gerade noch neben dem linken Statikträger erkennbar) und Rückschlagventil (etwas links von der Bildmitte) eingebaut. Die Drossel drosselt die Abluft (!) des untern Zylinderanschlusses und nicht etwa die Zuluft des oberen. Von oben kommt also sofort der volle Druck, aber die Abluft kann unten nur langsam entweichen. Damit bewegt sich der Füllkopfträger sehr schön anzusehen langsam und ruckelfrei nach unten - beide Seiten des Zylinderkolbens sind ja ständig zwischen Druckluft eingespannt.

Die Aufwärtsbewegung geht auch ohne Drossel langsam genug. Deshalb lässt das zur Drossel parallel geschaltete Rückschlagventil, die Zuluft zum untern Zylinderanschluss an der Drossel vorbei voll durch.