---
layout: "image"
title: "Kompressoren"
date: "2012-10-01T20:50:58"
picture: "reinpneumatischgesteuerteabfuellanlage04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/35595
imported:
- "2019"
_4images_image_id: "35595"
_4images_cat_id: "2642"
_4images_user_id: "104"
_4images_image_date: "2012-10-01T20:50:58"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35595 -->
Tatsächlich waren diese beiden neuen fischertechnik-Kompressoren (einer davon von Sven geliehen - Danke nochmals!) plus ein Pari Inhalierboy (ein Inhaliergerät) parallel geschaltet, um genügend Druckluft bereit zu stellen. Auf der Convention lieh mir Masked noch eine Pollin-Pumpe (Danke!), und mit den dann insgesamt vier Kompressoren konnte ich durchgehend ca. 0,28 bar Druck genießen. So lief die Anlage auf der Convention dann den ganzen Tag praktisch störungsfrei durch und füllte Flaschen ohne Ende.