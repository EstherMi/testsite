---
layout: "image"
title: "Flaschen in Wartestellung"
date: "2012-10-01T20:50:59"
picture: "reinpneumatischgesteuerteabfuellanlage31.jpg"
weight: "31"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/35622
imported:
- "2019"
_4images_image_id: "35622"
_4images_cat_id: "2642"
_4images_user_id: "104"
_4images_image_date: "2012-10-01T20:50:59"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35622 -->
Hier endlich sieht man nun die abgefüllten Stuhlprobenfläschchen ;-) Sie werden vom ersten Stopperzylinder gerade daran gehindert, in die Füllstation einzulaufen.