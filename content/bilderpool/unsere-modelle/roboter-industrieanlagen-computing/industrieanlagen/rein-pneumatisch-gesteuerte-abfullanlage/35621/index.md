---
layout: "image"
title: "Stopperzylinder in Ausfahrstellung"
date: "2012-10-01T20:50:59"
picture: "reinpneumatischgesteuerteabfuellanlage30.jpg"
weight: "30"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/35621
imported:
- "2019"
_4images_image_id: "35621"
_4images_cat_id: "2642"
_4images_user_id: "104"
_4images_image_date: "2012-10-01T20:50:59"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35621 -->
In dieser Stellung können die gefüllten Flaschen ablaufen. Die Stopperzylinder bleiben nach dem Erreichen der oberen Endlage der Füllköpfe so lange in dieser Stellung, bis das zweite Zeitglied schaltet. Diese Zeit ist so eingestellt, dass die Flaschen sicher den Füllbereich verlassen haben. Danach kehren die Stopperzylinder wieder in die Ausgangsstellung zurück und neue Flaschen können einlaufen.