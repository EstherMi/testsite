---
layout: "overview"
title: "Rein pneumatisch gesteuerte Abfüllanlage"
date: 2019-12-17T19:01:46+01:00
legacy_id:
- categories/2642
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2642 --> 
Ein soweit möglich originalgetreues Modell der echten Abfüllanlage, die in den 1970er Jahren in meinem elterlichen Betrieb im Einsatz war.