---
layout: "image"
title: "Die Flüssigkeitspumpe"
date: "2012-10-01T20:50:58"
picture: "reinpneumatischgesteuerteabfuellanlage17.jpg"
weight: "17"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/35608
imported:
- "2019"
_4images_image_id: "35608"
_4images_cat_id: "2642"
_4images_user_id: "104"
_4images_image_date: "2012-10-01T20:50:58"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35608 -->
Im Original wurde die Flüssigkeit völlig ohne Elektrik gepumpt: Ein ebenso großer Pneumatikzylinder wie der für das Anheben der Füllköpfe schob einen parallel gebauten für das Pumpen von Flüssigkeit geeigneten Zylinder mit sich, und der sog und drückte die Flüssigkeit durch Rückschlagventile immer in Richtung der Füllköpfe. Ein Hub dieses Zylinders pumpte die ca. vier Liter Flüssigkeit für eine Flaschengruppe durch.

Ich habe viele Versuche unternommen, eine brauchbare Flüssigkeitspumpe nur mit dem alten transparenten fischertechnik-Schlauch des ft-hobby-4-Kastens zu bauen. Immer kam aber entweder nur ein müdes Tröpfeln heraus, oder der Schlauch wurde bei hohen Drehzahlen des verwendeten Elektromotors nach kurzer Zeit einfach aus der Pumpe herausgezogen.

Letztlich verwendete ich meine Lemo-Solar-Pumpe. Die dient mir normalerweise als Druckluft-Kompressor, aber sie kann als Membranpumpe auch Flüssigkeiten pumpen. Da sie dabei sehr warm wird (ihre Drehzahl sinkt von einem Surren bei Luft auf nur ca. zwei Umdrehungen pro Sekunde bei Flüssigkeit), ist sie u.a. mittels zweier ft-Winkelachsen so gelagert, dass ihr Motor nirgends Kontakt zu fischertechnik-Teilen hat. Ich pumpe hier also - im Gegensatz zum Original - elektrisch.