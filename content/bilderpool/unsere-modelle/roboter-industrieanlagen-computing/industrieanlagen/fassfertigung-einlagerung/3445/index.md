---
layout: "image"
title: "regal3"
date: "2005-01-02T15:48:44"
picture: "regal3.JPG"
weight: "9"
konstrukteure: 
- "Rainer Pennekamp"
fotografen:
- "Rainer Pennekamp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/3445
imported:
- "2019"
_4images_image_id: "3445"
_4images_cat_id: "319"
_4images_user_id: "5"
_4images_image_date: "2005-01-02T15:48:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3445 -->
