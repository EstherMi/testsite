---
layout: "image"
title: "Band2"
date: "2005-01-02T15:48:44"
picture: "Band2.JPG"
weight: "2"
konstrukteure: 
- "Rainer Pennekamp"
fotografen:
- "Rainer Pennekamp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/3438
imported:
- "2019"
_4images_image_id: "3438"
_4images_cat_id: "319"
_4images_user_id: "5"
_4images_image_date: "2005-01-02T15:48:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3438 -->
