---
layout: "image"
title: "Erfassung der Fahrzeugkontur"
date: "2017-05-21T13:53:33"
picture: "pwa4.jpg"
weight: "4"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/45916
imported:
- "2019"
_4images_image_id: "45916"
_4images_cat_id: "3409"
_4images_user_id: "2228"
_4images_image_date: "2017-05-21T13:53:33"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45916 -->
Zwei Laserlichtschranken dienen dazu, die Fahrzeugkontur zu erfassen. Die Laser beleuchten die Fotowiderstände, das analoge Signal wird in ein digitales Signal umgerechnet