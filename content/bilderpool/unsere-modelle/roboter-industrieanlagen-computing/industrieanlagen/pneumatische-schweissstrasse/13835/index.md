---
layout: "image"
title: "absondern"
date: "2008-03-04T16:29:19"
picture: "pneumatischeschweissstrasse6.jpg"
weight: "6"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/13835
imported:
- "2019"
_4images_image_id: "13835"
_4images_cat_id: "1269"
_4images_user_id: "747"
_4images_image_date: "2008-03-04T16:29:19"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13835 -->
Schließlich dreht der Förderband-Motor das Förderband ein paar Impulse weiter und das Flugzeugteil landet in einer Box.