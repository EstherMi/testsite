---
layout: "image"
title: "Bearbeitungszentrum 10"
date: "2007-02-20T14:44:38"
picture: "bearbeitungszentrum10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/9109
imported:
- "2019"
_4images_image_id: "9109"
_4images_cat_id: "828"
_4images_user_id: "502"
_4images_image_date: "2007-02-20T14:44:38"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9109 -->
