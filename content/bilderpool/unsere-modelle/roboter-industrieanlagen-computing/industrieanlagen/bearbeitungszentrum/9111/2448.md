---
layout: "comment"
hidden: true
title: "2448"
date: "2007-02-20T16:15:46"
uploadBy:
- "remadus"
license: "unknown"
imported:
- "2019"
---
Kenn ich, das übliche Problem.

Das einzige, was da hilft, ist genauso großzügig zu bauen, wie das in der Industrie üblich ist. Der Schaltschrank steht da ja auch schön daneben, damit er nicht hindert, außerdem muß das Bedienpersonal rankönnen oder gar Ver- und Entsorgungsfahrzeuge usw.

Leider braucht es dann eine ganze Menge mehr Teile.