---
layout: "image"
title: "Bearbeitungszentrum 14"
date: "2007-02-20T14:44:44"
picture: "bearbeitungszentrum14.jpg"
weight: "14"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/9113
imported:
- "2019"
_4images_image_id: "9113"
_4images_cat_id: "828"
_4images_user_id: "502"
_4images_image_date: "2007-02-20T14:44:44"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9113 -->
