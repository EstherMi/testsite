---
layout: "image"
title: "Übersicht"
date: "2013-10-12T14:55:42"
picture: "industrieanlage03.jpg"
weight: "3"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- details/37703
imported:
- "2019"
_4images_image_id: "37703"
_4images_cat_id: "2802"
_4images_user_id: "791"
_4images_image_date: "2013-10-12T14:55:42"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37703 -->
Auf der rechten Seite erkennt man das Magazin, in welchem die Werkstücke bis zur Bearbeitung gelagert werden. Links sieht man den Ofen und das Rollenband. Außerdem sieht man in der oberen Mitte die selbsttragende Energiekette.
Ebenfalls ist die zweistöckige Anordnung zu erkennen.