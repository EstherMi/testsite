---
layout: "image"
title: "Detail fahrbahres Fließband"
date: "2013-10-12T14:55:42"
picture: "industrieanlage07.jpg"
weight: "7"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- details/37707
imported:
- "2019"
_4images_image_id: "37707"
_4images_cat_id: "2802"
_4images_user_id: "791"
_4images_image_date: "2013-10-12T14:55:42"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37707 -->
Das fahrbahre Fließband wird von einem Power Motor 1:30 auf zwei 4mm Achsen verschoben. Die Abschaltung in der jeweiligen Position erfolgt über zwei Taster. Das Band an sich wird von einem XS-Motor betrieben. Die Stromversorgung läuft über eine Energiekette. Die rote Kassette auf der rechten Seite dient dazu aussortierte Werkstücke aufzufangen.