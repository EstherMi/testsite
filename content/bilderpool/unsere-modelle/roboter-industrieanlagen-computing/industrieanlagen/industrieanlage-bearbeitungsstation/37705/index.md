---
layout: "image"
title: "Ansicht Stanze"
date: "2013-10-12T14:55:42"
picture: "industrieanlage05.jpg"
weight: "5"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- details/37705
imported:
- "2019"
_4images_image_id: "37705"
_4images_cat_id: "2802"
_4images_user_id: "791"
_4images_image_date: "2013-10-12T14:55:42"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37705 -->
Auf diesem Bild erkennt man die Stanze. Sie wird von einem S-Motor betrieben.