---
layout: "overview"
title: "Industrieanlage mit Bearbeitungsstation und Brennofen"
date: 2019-12-17T19:01:52+01:00
legacy_id:
- categories/2802
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2802 --> 
kleine Industrienanlage bestehend aus einem Fertigungsteil und einem Brennofen.
Die Steuerung der Fertigung erfolgt automatisch. Der Ofen und seine dazugehörigen Fließbänder werden manuell vom Bediener gesteuert.
Der Elektronische Teil befindet sich unter der Anlage.
Video: http://www.youtube.com/watch?v=5Lqh-RQF7dw