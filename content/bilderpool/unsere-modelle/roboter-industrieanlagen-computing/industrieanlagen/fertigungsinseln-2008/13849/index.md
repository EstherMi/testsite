---
layout: "image"
title: "Farbmessung"
date: "2008-03-07T07:03:14"
picture: "olli06.jpg"
weight: "6"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/13849
imported:
- "2019"
_4images_image_id: "13849"
_4images_cat_id: "1271"
_4images_user_id: "504"
_4images_image_date: "2008-03-07T07:03:14"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13849 -->
Jetzt im Rücken ist die Lampe für die Lichtschranke. Dann wenn diese von einer Kiste unterbrochen wird und die "Bohrmaschine" anfängt zu bohren geht auch die Lampe an und der Farbwert wird gemessen. Der Fototransistor ist an EX angeschlossen.