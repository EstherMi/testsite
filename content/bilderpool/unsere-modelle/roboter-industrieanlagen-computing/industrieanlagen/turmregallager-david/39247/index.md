---
layout: "image"
title: "Verkabelung"
date: "2014-08-20T14:19:12"
picture: "trl03.jpg"
weight: "3"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/39247
imported:
- "2019"
_4images_image_id: "39247"
_4images_cat_id: "2937"
_4images_user_id: "2228"
_4images_image_date: "2014-08-20T14:19:12"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39247 -->
Kabel verlaufen gebündelt unter dem Regal zur Maschine