---
layout: "image"
title: "Ansicht von oben"
date: "2014-08-20T14:19:12"
picture: "trl02.jpg"
weight: "2"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/39246
imported:
- "2019"
_4images_image_id: "39246"
_4images_cat_id: "2937"
_4images_user_id: "2228"
_4images_image_date: "2014-08-20T14:19:12"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39246 -->
Leider ist das Bild etwas verschwommen, dennoch ist die runde Form gut zu erkennen