---
layout: "image"
title: "Regalbedienung und Verkabelung"
date: "2014-08-20T14:19:12"
picture: "trl08.jpg"
weight: "8"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/39252
imported:
- "2019"
_4images_image_id: "39252"
_4images_cat_id: "2937"
_4images_user_id: "2228"
_4images_image_date: "2014-08-20T14:19:12"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39252 -->
Für das Verkabeln habe ich eine Energiekette und Spiralschläuche verwendet, sodass die Kabel nicht die Mechanik stören.