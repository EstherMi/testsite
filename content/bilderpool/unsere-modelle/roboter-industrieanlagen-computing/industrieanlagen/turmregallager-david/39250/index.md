---
layout: "image"
title: "Regal"
date: "2014-08-20T14:19:12"
picture: "trl06.jpg"
weight: "6"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/39250
imported:
- "2019"
_4images_image_id: "39250"
_4images_cat_id: "2937"
_4images_user_id: "2228"
_4images_image_date: "2014-08-20T14:19:12"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39250 -->
Allle 11 Regale sind nach diesem Beispiel aufgebaut. Da ich nur eine begrenzte Anzahl an U-Trägern habe, habe ich zwischen schwarz und gelb gewechselt.