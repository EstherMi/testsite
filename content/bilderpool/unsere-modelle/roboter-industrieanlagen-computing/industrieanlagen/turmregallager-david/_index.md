---
layout: "overview"
title: "Turmregallager (david)"
date: 2019-12-17T19:01:55+01:00
legacy_id:
- categories/2937
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2937 --> 
Dies ist mein Turmregallager, das ich auch schon am Fischertechnik FanClub Tag 2014 ausgestellt habe. Ein besonderes Merkmal ist die runde Form, die zum einen kurze Fahrwege der Regalbedienung, als auch platzsparende Lagerung ermöglicht...