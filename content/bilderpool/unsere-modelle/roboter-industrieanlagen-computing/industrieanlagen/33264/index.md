---
layout: "image"
title: "Bearbeitungszentrum 008"
date: "2011-10-20T17:10:01"
picture: "FT_Derk_008.jpg"
weight: "8"
konstrukteure: 
- "FT Derk"
fotografen:
- "Derk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- details/33264
imported:
- "2019"
_4images_image_id: "33264"
_4images_cat_id: "635"
_4images_user_id: "1289"
_4images_image_date: "2011-10-20T17:10:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33264 -->
