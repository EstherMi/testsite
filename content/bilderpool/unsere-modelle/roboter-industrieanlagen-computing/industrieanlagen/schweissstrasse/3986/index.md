---
layout: "image"
title: "Schweißstraße05"
date: "2005-04-17T11:48:45"
picture: "schweistrae05.jpg"
weight: "5"
konstrukteure: 
- "Frank Linde und Martin Romann"
fotografen:
- "Frank Linde"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DerMitDenBitsTanzt"
license: "unknown"
legacy_id:
- details/3986
imported:
- "2019"
_4images_image_id: "3986"
_4images_cat_id: "345"
_4images_user_id: "31"
_4images_image_date: "2005-04-17T11:48:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3986 -->
Schweißstraße mit vier 3-Achs-Robotern