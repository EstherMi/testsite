---
layout: "image"
title: "Schweißstraße18"
date: "2005-04-17T11:49:02"
picture: "schweistrae18.jpg"
weight: "18"
konstrukteure: 
- "Frank Linde und Martin Romann"
fotografen:
- "Frank Linde"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DerMitDenBitsTanzt"
license: "unknown"
legacy_id:
- details/3999
imported:
- "2019"
_4images_image_id: "3999"
_4images_cat_id: "345"
_4images_user_id: "31"
_4images_image_date: "2005-04-17T11:49:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3999 -->
Schweißstraße mit vier 3-Achs-Robotern