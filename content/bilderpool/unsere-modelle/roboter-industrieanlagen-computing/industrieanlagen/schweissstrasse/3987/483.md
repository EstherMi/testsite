---
layout: "comment"
hidden: true
title: "483"
date: "2005-04-17T17:59:54"
uploadBy:
- "DerMitDenBitsTanzt"
license: "unknown"
imported:
- "2019"
---
Roboter in AktionDer vordere Roboter verschweißt den Radkasten am Kotflügel, während der hintere Roboter die Fahrertür zuschweißt. Diesen kleinen  "Programmierfehler" hat der Leiter Produktion (im Hintergrund mit Blaumann und weißem Schutzhelm) glücklicherweise bis zum Ende der Serienfertigung nicht bemerkt.  ;-)