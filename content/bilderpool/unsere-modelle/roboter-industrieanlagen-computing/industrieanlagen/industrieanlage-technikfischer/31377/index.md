---
layout: "image"
title: "Sortierarm"
date: "2011-07-27T13:51:08"
picture: "bild11.jpg"
weight: "11"
konstrukteure: 
- "technikfischer"
fotografen:
- "technikfischer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- details/31377
imported:
- "2019"
_4images_image_id: "31377"
_4images_cat_id: "2334"
_4images_user_id: "1218"
_4images_image_date: "2011-07-27T13:51:08"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31377 -->
