---
layout: "image"
title: "Fertigungsinsel"
date: "2006-08-08T20:28:23"
picture: "Fuji-Bilder_002.jpg"
weight: "2"
konstrukteure: 
- "Jürgen Fischer"
fotografen:
- "Jürgen Fischer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jürgen Fischer"
license: "unknown"
legacy_id:
- details/6665
imported:
- "2019"
_4images_image_id: "6665"
_4images_cat_id: "647"
_4images_user_id: "341"
_4images_image_date: "2006-08-08T20:28:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6665 -->
Umlauf-Fertigungsinsel mit Säulen-Bohr-Senk und Fräsmaschine.Momentane Steuerung erfolgt noch durch das alte Interface 3.1 plus Fischertechnik - Fernbedienung für 2 Motoren.Es sind insgesamt 12 Motoren verbaut und ich muss mir wohl das neue Robo-Interface kaufen.