---
layout: "comment"
hidden: true
title: "1099"
date: "2006-05-17T04:50:21"
uploadBy:
- "DerMitDenBitsTanzt"
license: "unknown"
imported:
- "2019"
---
Zwei Zylinder füllen sechs Druckbehälter, deren Füllgrad durch das Manometer angezeigt und über einen mechanischen Druckschalter kontrolliert wird.