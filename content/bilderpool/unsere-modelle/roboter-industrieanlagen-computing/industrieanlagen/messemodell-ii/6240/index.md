---
layout: "image"
title: "Steuerungseinheit"
date: "2006-05-07T15:16:34"
picture: "BfMiS12.jpg"
weight: "8"
konstrukteure: 
- "Martin Romann (remadus) / Frank Linde (DerMitDenBitsTanzt)"
fotografen:
- "Frank Linde/DerMitDenBitsTanzt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DerMitDenBitsTanzt"
license: "unknown"
legacy_id:
- details/6240
imported:
- "2019"
_4images_image_id: "6240"
_4images_cat_id: "541"
_4images_user_id: "31"
_4images_image_date: "2006-05-07T15:16:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6240 -->
