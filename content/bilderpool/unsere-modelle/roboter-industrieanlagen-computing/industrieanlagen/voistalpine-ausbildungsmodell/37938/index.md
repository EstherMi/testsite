---
layout: "image"
title: "VoistAlpine 1"
date: "2013-12-23T13:36:41"
picture: "voistalpineausbildungsmodell1.jpg"
weight: "1"
konstrukteure: 
- "unbekannt"
fotografen:
- "Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- details/37938
imported:
- "2019"
_4images_image_id: "37938"
_4images_cat_id: "2823"
_4images_user_id: "10"
_4images_image_date: "2013-12-23T13:36:41"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37938 -->
In meiner langjährigen Tätigkeit im Außendienst ist mir noch nie ein Fischertechnik Modell begegnet, daß wirklich ein realen Zweck erfüllte.
Nun, letzte Woche bei VoistAlpine in Loeben (Österreich) stand doch tatsächlich ein Modell im Büro der Elektroplanung, an dem tatsächlich auch Auszubildende  Grundlagen der Automatisierung beigebracht bekommen. 
Angeblich hat es ein Diplomand es vor einigen Jahren gebaut.