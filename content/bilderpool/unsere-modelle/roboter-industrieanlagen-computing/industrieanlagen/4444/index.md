---
layout: "image"
title: "kleine_Fabrik"
date: "2005-06-13T10:54:25"
picture: "Bild_1.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/4444
imported:
- "2019"
_4images_image_id: "4444"
_4images_cat_id: "635"
_4images_user_id: "1"
_4images_image_date: "2005-06-13T10:54:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4444 -->
Dieses Modell ist der Anfang einer kleinen Fabrik.
Gebaut wurde dieses Modell vom Sohne meiner Freundin. Er ist 12 Jahre alt.
Programmiert hat er es ganz alleine in ROBO Pro.

Derzeit ist alles noch im Entwicklungsanfang.
Es folgt demnächst noch ein Schweißroboter.

Die Verkabelung muss auch noch irgendwann mal richtig sauber gemacht werden.

Vermutlich wird dieses Modell auch auf der Convention im September in Mörshausen zu sehen sein.

Gruß
Sven