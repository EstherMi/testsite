---
layout: "image"
title: "Taktstraße mit Sortierung 12"
date: "2009-04-24T08:32:34"
picture: "taktstrassemitsortierung12.jpg"
weight: "12"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- details/23767
imported:
- "2019"
_4images_image_id: "23767"
_4images_cat_id: "1625"
_4images_user_id: "941"
_4images_image_date: "2009-04-24T08:32:34"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23767 -->
Das Kabelmanagment hat mich einige graue Haare gekostet :)