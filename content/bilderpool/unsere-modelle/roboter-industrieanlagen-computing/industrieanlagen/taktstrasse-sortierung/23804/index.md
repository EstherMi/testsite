---
layout: "image"
title: "Taktstraße mit Sortierung 49"
date: "2009-04-24T08:32:35"
picture: "taktstrassemitsortierung49.jpg"
weight: "49"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- details/23804
imported:
- "2019"
_4images_image_id: "23804"
_4images_cat_id: "1625"
_4images_user_id: "941"
_4images_image_date: "2009-04-24T08:32:35"
_4images_image_order: "49"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23804 -->
Das Interface-kabel-vorfeld :)