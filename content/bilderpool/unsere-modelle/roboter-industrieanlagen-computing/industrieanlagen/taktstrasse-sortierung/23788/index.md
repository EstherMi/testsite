---
layout: "image"
title: "Taktstraße mit Sortierung 33"
date: "2009-04-24T08:32:34"
picture: "taktstrassemitsortierung33.jpg"
weight: "33"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- details/23788
imported:
- "2019"
_4images_image_id: "23788"
_4images_cat_id: "1625"
_4images_user_id: "941"
_4images_image_date: "2009-04-24T08:32:34"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23788 -->
Der Straße entlang, von links nach rechts.