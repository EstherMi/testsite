---
layout: "image"
title: "Taktstraße mit Sortierung 03"
date: "2009-04-24T08:32:34"
picture: "taktstrassemitsortierung03.jpg"
weight: "3"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- details/23758
imported:
- "2019"
_4images_image_id: "23758"
_4images_cat_id: "1625"
_4images_user_id: "941"
_4images_image_date: "2009-04-24T08:32:34"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23758 -->
Nahaufnahme der Schweißstation.

Diese wird mit einem Zylinder mit Rückholfeder über ein Magnetventil angesteuert und fährt den Schweißkopf zum Werkstück. 

Ein Taster auf der Unterseite aktiviert den Schweißkopf sobald der Zylinder maximal ausgefahren und der Schweißkopf nah am Werkstück ist.