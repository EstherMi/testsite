---
layout: "image"
title: "Taktstraße mit Sortierung 15"
date: "2009-04-24T08:32:34"
picture: "taktstrassemitsortierung15.jpg"
weight: "15"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- details/23770
imported:
- "2019"
_4images_image_id: "23770"
_4images_cat_id: "1625"
_4images_user_id: "941"
_4images_image_date: "2009-04-24T08:32:34"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23770 -->
Automatische Kompressor-endabschaltung.

Ein gekürztes "Watte/Ohrstäbchen" dient als Führung für eine "Kugelschreiberfeder" und macht dadurch den Taster schwergängiger.