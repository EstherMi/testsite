---
layout: "comment"
hidden: true
title: "9068"
date: "2009-04-27T13:45:40"
uploadBy:
- "tz"
license: "unknown"
imported:
- "2019"
---
Bis auf die Anschlusskabel der Magnetventile sind sogut wie alle Kabel und Schläuche extra auf die passende Länge zurechtgeschnitten. 
Da bei mir einiges an orginal FT-Kabel und Schläuche rumliegt, hatte ich auch kein Problem damit diese passend zu machen ;)

Insgesamt hat die Verkablung und die Schlauchverlegung mit dem dazugehörigen Puzzel/Knobelspiel, wie diese am besten zu verlegen sind, ca. 12 Stunden bzw. ein gemütliches Wochenende mit Pausen gedauert.
Dieser Aufwand hat sich aber gelohnt :)

Gruss, Tom