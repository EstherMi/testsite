---
layout: "image"
title: "Taktstraße mit Sortierung 25"
date: "2009-04-24T08:32:34"
picture: "taktstrassemitsortierung25.jpg"
weight: "25"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- details/23780
imported:
- "2019"
_4images_image_id: "23780"
_4images_cat_id: "1625"
_4images_user_id: "941"
_4images_image_date: "2009-04-24T08:32:34"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23780 -->
Noch mehr Kabel- und Schlauchmanagement