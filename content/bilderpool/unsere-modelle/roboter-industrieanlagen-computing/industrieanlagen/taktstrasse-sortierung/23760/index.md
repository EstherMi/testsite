---
layout: "image"
title: "Taktstraße mit Sortierung 05"
date: "2009-04-24T08:32:34"
picture: "taktstrassemitsortierung05.jpg"
weight: "5"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tz"
license: "unknown"
legacy_id:
- details/23760
imported:
- "2019"
_4images_image_id: "23760"
_4images_cat_id: "1625"
_4images_user_id: "941"
_4images_image_date: "2009-04-24T08:32:34"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23760 -->
Nahaufnahme des Pneumatikgreifers, links ist der Endtaster für die Pneumatikgreifer-Position (Band) zu erkennen.