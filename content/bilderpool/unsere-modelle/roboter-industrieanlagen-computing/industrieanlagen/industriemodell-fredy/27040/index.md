---
layout: "image"
title: "Übergänge"
date: "2010-05-02T13:55:55"
picture: "fischertechnikindustriemodell5.jpg"
weight: "5"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/27040
imported:
- "2019"
_4images_image_id: "27040"
_4images_cat_id: "1946"
_4images_user_id: "453"
_4images_image_date: "2010-05-02T13:55:55"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27040 -->
