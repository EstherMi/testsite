---
layout: "image"
title: "Antrieb für den Schlitten"
date: "2010-05-02T13:55:54"
picture: "fischertechnikindustriemodell3.jpg"
weight: "3"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/27038
imported:
- "2019"
_4images_image_id: "27038"
_4images_cat_id: "1946"
_4images_user_id: "453"
_4images_image_date: "2010-05-02T13:55:54"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27038 -->
Die Rastkupplung greift in die Rastachse auf dem Schlitten.