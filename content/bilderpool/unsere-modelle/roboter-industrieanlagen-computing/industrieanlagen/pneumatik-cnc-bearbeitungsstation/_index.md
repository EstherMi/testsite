---
layout: "overview"
title: "Pneumatik CNC-Bearbeitungsstation"
date: 2019-12-17T19:00:26+01:00
legacy_id:
- categories/362
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=362 --> 
CNC-Bearbeitungsstation mit Presse, Fräse, Härteofen und Kühlung. Aussortierung und Rückführung in den Kreislauf.