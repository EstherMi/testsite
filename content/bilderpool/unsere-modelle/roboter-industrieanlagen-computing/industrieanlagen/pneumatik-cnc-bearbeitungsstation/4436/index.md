---
layout: "image"
title: "Härteofen"
date: "2005-06-12T17:43:32"
picture: "Ofen_1.jpg"
weight: "23"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rainer_step5"
license: "unknown"
legacy_id:
- details/4436
imported:
- "2019"
_4images_image_id: "4436"
_4images_cat_id: "362"
_4images_user_id: "94"
_4images_image_date: "2005-06-12T17:43:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4436 -->
Materialaushärtung im Ofen. Die Temperaturstufen werden über LED´s in drei Stufen Simuliert. Anschließend wird das Werkstück in eine Kühlkammer gefahren.