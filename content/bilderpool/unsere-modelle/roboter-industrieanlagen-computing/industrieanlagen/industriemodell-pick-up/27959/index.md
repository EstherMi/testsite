---
layout: "image"
title: "Industriemodell Pick-up"
date: "2010-08-26T17:40:16"
picture: "pickup4.jpg"
weight: "4"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/27959
imported:
- "2019"
_4images_image_id: "27959"
_4images_cat_id: "2025"
_4images_user_id: "1162"
_4images_image_date: "2010-08-26T17:40:16"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27959 -->
Hier sieht man ihn von vorne, beim Abstellen der Tonne.