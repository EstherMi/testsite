---
layout: "image"
title: "Pick-up mit Förderband"
date: "2010-08-26T17:40:15"
picture: "pickup1.jpg"
weight: "1"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/27956
imported:
- "2019"
_4images_image_id: "27956"
_4images_cat_id: "2025"
_4images_user_id: "1162"
_4images_image_date: "2010-08-26T17:40:15"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27956 -->
Hier könnt ihr mein Industriemodell sehen. Es nennt sich Pick- up, weil er die gelben FT- Tonnen  aufhebt.  Die gelben Tonnen kommen auf dem Förderband hergefahren, und der Pick- up hebt die Tonnen mit dem E-Magnet hoch. Das kann er, weil die Tonnen oben am Deckel eine aufgeklebte Unterlegscheibe haben. Danach, hebt er sie hoch, dreht sie um 180° und stellt sie auf dem roten Tisch ab. 

Diese Version (ist vom 06.08.2010)ist noch nicht verkabelt, und es hat sich einiges Verändert, Bilder folgen. 

Ich hoffe ihr schreibt Kommentare und schreibt auch Verbesserungsvorschläge.

MfG
Endlich