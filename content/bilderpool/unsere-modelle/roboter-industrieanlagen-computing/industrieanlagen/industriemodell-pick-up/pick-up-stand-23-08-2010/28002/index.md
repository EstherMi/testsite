---
layout: "image"
title: "Förderband"
date: "2010-08-28T14:01:14"
picture: "pickup07.jpg"
weight: "7"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/28002
imported:
- "2019"
_4images_image_id: "28002"
_4images_cat_id: "2028"
_4images_user_id: "1162"
_4images_image_date: "2010-08-28T14:01:14"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28002 -->
Hier sieht man das Förderband von oben.