---
layout: "image"
title: "E-Magnet"
date: "2010-08-28T14:01:14"
picture: "pickup11.jpg"
weight: "11"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/28006
imported:
- "2019"
_4images_image_id: "28006"
_4images_cat_id: "2028"
_4images_user_id: "1162"
_4images_image_date: "2010-08-28T14:01:14"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28006 -->
Hier sieht man den E-Magnet der die Tonne anzieht, und der Schalter, der signalisiert, das eine Tonne angezogen wurde. (Leider ist er noch nicht angeschlossen)