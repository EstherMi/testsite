---
layout: "image"
title: "Tonne"
date: "2010-08-28T14:01:14"
picture: "pickup13.jpg"
weight: "13"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/28008
imported:
- "2019"
_4images_image_id: "28008"
_4images_cat_id: "2028"
_4images_user_id: "1162"
_4images_image_date: "2010-08-28T14:01:14"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28008 -->
Hier könnt ihr so eine gelbe Tonne von FT sehen. Leider ist auf ihr jetzt keine Unterlegscheibe.