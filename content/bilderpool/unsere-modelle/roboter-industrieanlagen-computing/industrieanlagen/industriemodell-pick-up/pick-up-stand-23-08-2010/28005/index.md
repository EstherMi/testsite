---
layout: "image"
title: "Pick-up"
date: "2010-08-28T14:01:14"
picture: "pickup10.jpg"
weight: "10"
konstrukteure: 
- "Endlich"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/28005
imported:
- "2019"
_4images_image_id: "28005"
_4images_cat_id: "2028"
_4images_user_id: "1162"
_4images_image_date: "2010-08-28T14:01:14"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28005 -->
Hier könnt ihr sehen, wie der Pick-up gerade eine Tonne anfährt und sie "picken" möchte.