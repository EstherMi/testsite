---
layout: "comment"
hidden: true
title: "15123"
date: "2011-09-17T09:10:18"
uploadBy:
- "scripter1"
license: "unknown"
imported:
- "2019"
---
Du hättest auch an den Minimot durch den Getriebeaufsatz anstatt ein Plastikzahnrad eine Metallachse mit einer Schnecke befestigen können. Das wäre dann zwar sehr langsam aber die Konkstruktion hätte sehr viel Kraft gehabt.