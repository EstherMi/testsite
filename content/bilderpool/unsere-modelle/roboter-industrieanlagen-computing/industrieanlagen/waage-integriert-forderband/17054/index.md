---
layout: "image"
title: "1. Waage"
date: "2009-01-18T18:10:34"
picture: "Frderband__Waage_1_01.jpg"
weight: "1"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Förderband", "Waage", "Kassette"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/17054
imported:
- "2019"
_4images_image_id: "17054"
_4images_cat_id: "1532"
_4images_user_id: "765"
_4images_image_date: "2009-01-18T18:10:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17054 -->
Nachdem Wahsager im Forum die Frage gestellt hat, ob es möglich ist, eine Waage zu bauen, die feststellt ob eine Kassette voll oder leer ist, habe ich die folgenden zwei Waagen gebaut. Ich möchte keinesfalls behaupten, daß die Konstruktionen ausgereift oder optimal sind. Mir ist es hauptsächlich darum gegangen, ob sie überhaupt möglich sind. Das Weiterentwickeln dieser Konstruktionen bzw. das Verwenden in „Industrieanlagen“ ist erwünscht.


Funktion:

Kassette wird mit dem Förderband transportiert, bis eine Lichtschranke unterbrochen wird; Förderband hält an.
Ein Meßgewicht wird so lange verschoben, bis das Förderband etwas kippt und einen Kontakt unterbricht. Je länger das dauert, desto leichter ist die Kassette.

Ungenauigkeiten können sich ergeben, da die Kassette mit der Vorderkante die Lichtschranke unterbricht, maßgebend für das ermittelte Gewicht ist allerdings der Schwerpunkt des Meßguts.

Nachteil: Förderband muß anhalten, der Meßvorgang dauert lange.
Vorteil: sehr genaue Messung möglich.

Benötigt werden:
2 Motore (Förderband, Meßmotor),
2 Taster (Endstellung Meßmotor und Impulszähler Meßmotor),
1 Kontakt (aus 2 Kontaktstücke 31305 gebildet) und
1 Lichtschranke.