---
layout: "image"
title: "1. Waage"
date: "2009-01-18T18:10:36"
picture: "Frderband__Waage_1_07.jpg"
weight: "7"
konstrukteure: 
- "Mirose"
fotografen:
- "Mirose"
keywords: ["Förderband", "Waage", "Kassette"]
uploadBy: "mirose"
license: "unknown"
legacy_id:
- details/17060
imported:
- "2019"
_4images_image_id: "17060"
_4images_cat_id: "1532"
_4images_user_id: "765"
_4images_image_date: "2009-01-18T18:10:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17060 -->
Ende der Messung bei einem mittelschweren Meßgut (Simulation)