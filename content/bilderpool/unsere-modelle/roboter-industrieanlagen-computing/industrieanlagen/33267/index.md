---
layout: "image"
title: "Bearbeitungszentrum 011"
date: "2011-10-20T17:10:01"
picture: "FT_Derk_011.jpg"
weight: "11"
konstrukteure: 
- "FT Derk"
fotografen:
- "Derk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- details/33267
imported:
- "2019"
_4images_image_id: "33267"
_4images_cat_id: "635"
_4images_user_id: "1289"
_4images_image_date: "2011-10-20T17:10:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33267 -->
