---
layout: "image"
title: "Flaschenfüll06.JPG"
date: "2004-01-07T20:37:05"
picture: "Flaschenfll06.jpg"
weight: "13"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2055
imported:
- "2019"
_4images_image_id: "2055"
_4images_cat_id: "206"
_4images_user_id: "4"
_4images_image_date: "2004-01-07T20:37:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2055 -->
In Station 4 werden Etiketten aufgeklebt und bedruckt. Die Flaschen kommen von rechts herein, der Taster rechts oben im Bild löst den Etikettenschwind-... äh, stempler aus. Links oben das Druckwerk für die Beschriftung.