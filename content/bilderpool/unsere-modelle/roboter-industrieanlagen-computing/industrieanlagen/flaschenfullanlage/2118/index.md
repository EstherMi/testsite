---
layout: "image"
title: "Flafue13.JPG"
date: "2004-02-20T12:21:09"
picture: "Flafue13.jpg"
weight: "2"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2118
imported:
- "2019"
_4images_image_id: "2118"
_4images_cat_id: "206"
_4images_user_id: "4"
_4images_image_date: "2004-02-20T12:21:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2118 -->
Abtropfstation (Veghel 2004)
Der rechte Greifer hat gerade eine Flasche abgestellt.