---
layout: "image"
title: "Flafue20.JPG"
date: "2004-02-20T12:21:09"
picture: "Flafue20.jpg"
weight: "6"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2122
imported:
- "2019"
_4images_image_id: "2122"
_4images_cat_id: "206"
_4images_user_id: "4"
_4images_image_date: "2004-02-20T12:21:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2122 -->
Station "Deckel aufsetzen", Veghel 2004

Die Station gabs früher auch schon, ist aber geändert worden. Die Deckel kommen von rechts und werden einzeln weitertransportiert, wenn gerade eine neue Flasche eingetroffen ist. Jeder schiebt seinen Vordermann in die Klemmvorrichtung (links, mit Feder). Dort wird der erste dann per Stößel von oben auf die Flasche gedrückt.