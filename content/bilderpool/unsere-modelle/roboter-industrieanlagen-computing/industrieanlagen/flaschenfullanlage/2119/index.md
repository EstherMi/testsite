---
layout: "image"
title: "Flafue14.JPG"
date: "2004-02-20T12:21:09"
picture: "Flafue14.jpg"
weight: "3"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2119
imported:
- "2019"
_4images_image_id: "2119"
_4images_cat_id: "206"
_4images_user_id: "4"
_4images_image_date: "2004-02-20T12:21:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2119 -->
Dieser Plastikstreifen ist alles was man braucht, um die Deckel in die richtige Position (Öffnung nach unten) zu drehen. Das geht so schnell, dass bei meiner Kamera zwischen Drücken des Auslösers und tatsächlichem "Schuß" schon alles passiert ist.
Der Deckel kommt von links. Wenn er falsch herum liegt, verfangen sich die beiden Zungen am Innenrand und halten so die Oberseite des Deckels auf, während die Unterseite vom Förderband weitertransportiert wird. Schwupps, es folgt eine Rolle rückwärts und der Deckel liegt richtig.