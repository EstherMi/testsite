---
layout: "image"
title: "Flaschenfuell14.jpg"
date: "2004-01-07T20:37:28"
picture: "Flaschenfll14.jpg"
weight: "21"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2063
imported:
- "2019"
_4images_image_id: "2063"
_4images_cat_id: "206"
_4images_user_id: "4"
_4images_image_date: "2004-01-07T20:37:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2063 -->
Hier nochmal die Schikane mit allen Einzelheiten. Die Flaschen fahren von 'unten' auf die Taster unten links zu. Wenn drei Flaschen da sind, versperrt der blaue Zylinder in Bildmitte unten den Weg für weitere Flaschen. Die Kästen warten im Hochregal auf ihren Einsatz. Gerade eben ist eine volle Kiste über das Förderband in Bildmitte nach rechts unten abgefahren und ist gerade noch im rechten unteren Eck erkennbar.