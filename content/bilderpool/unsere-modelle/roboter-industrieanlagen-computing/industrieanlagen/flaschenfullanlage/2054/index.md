---
layout: "image"
title: "Flaschenfüll05.JPG"
date: "2004-01-07T20:37:05"
picture: "Flaschenfll05.jpg"
weight: "12"
konstrukteure: 
- "Frans"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2054
imported:
- "2019"
_4images_image_id: "2054"
_4images_cat_id: "206"
_4images_user_id: "4"
_4images_image_date: "2004-01-07T20:37:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2054 -->
Hier die "Ausfahrt" aus der Füllstation (Station 3)