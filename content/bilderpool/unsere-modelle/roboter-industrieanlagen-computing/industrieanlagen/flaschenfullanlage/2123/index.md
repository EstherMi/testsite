---
layout: "image"
title: "Flafue19.JPG"
date: "2004-02-20T12:21:09"
picture: "Flafue19.jpg"
weight: "7"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2123
imported:
- "2019"
_4images_image_id: "2123"
_4images_cat_id: "206"
_4images_user_id: "4"
_4images_image_date: "2004-02-20T12:21:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2123 -->
Station "Deckel aufsetzen", Veghel 2004
Zur Beschreibung siehe 'Flafue20.JPG'.

Die Schnecke oben rechts dient nur zum manuellen Justieren der Vorrichtung. Die Achse da drinnen muss wohl aus einem P-Kolben stammen, 'normale' Achsen haben keine umlaufende Rille und lassen sich nicht quer in eine Baustein-Nut einschieben.