---
layout: "image"
title: "Flaschenfüll07.JPG"
date: "2004-01-07T20:37:05"
picture: "Flaschenfll07.jpg"
weight: "14"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2056
imported:
- "2019"
_4images_image_id: "2056"
_4images_cat_id: "206"
_4images_user_id: "4"
_4images_image_date: "2004-01-07T20:37:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2056 -->
Das Druckwerk für die Etiketten, mit einer Vorrichtung zum Festhalten der Flaschen, einem Farbband und dahinter einem Stempel, der vom Exzenter links außen bewegt wird.