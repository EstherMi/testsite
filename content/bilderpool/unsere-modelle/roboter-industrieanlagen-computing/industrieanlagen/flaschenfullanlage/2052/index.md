---
layout: "image"
title: "Flaschenfüll03.JPG"
date: "2004-01-07T20:37:04"
picture: "Flaschenfll03.jpg"
weight: "10"
konstrukteure: 
- "Frans"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2052
imported:
- "2019"
_4images_image_id: "2052"
_4images_cat_id: "206"
_4images_user_id: "4"
_4images_image_date: "2004-01-07T20:37:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2052 -->
Die optische Prüfung. Wer nach dem Waschen noch dreckig ist oder sonstige Macken hat, fliegt raus und wird auf das Abstellgleis im Vordergrund verfrachtet.