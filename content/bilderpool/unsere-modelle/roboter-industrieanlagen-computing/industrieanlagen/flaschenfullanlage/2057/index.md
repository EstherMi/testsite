---
layout: "image"
title: "Flaschenfüll08.JPG"
date: "2004-01-07T20:37:05"
picture: "Flaschenfll08.jpg"
weight: "15"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2057
imported:
- "2019"
_4images_image_id: "2057"
_4images_cat_id: "206"
_4images_user_id: "4"
_4images_image_date: "2004-01-07T20:37:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2057 -->
Hoppla, das Bild hätte VOR Nummer 7 gehört, aber das ist schon unterwegs.

Rechts die Etikettenklebestation, links das Druckwerk, das wie eine mechanische Schreibmaschine funktioniert: Papier (hier: Flasche mit Etikett) einspannen, Farbband weiterschalten, mit Typenhebel (hier: Stempel) draufhauen und... fertig.