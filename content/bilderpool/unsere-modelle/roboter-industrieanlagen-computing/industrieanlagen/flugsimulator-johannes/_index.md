---
layout: "overview"
title: "Flugsimulator (Johannes)"
date: 2019-12-17T19:00:49+01:00
legacy_id:
- categories/1285
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1285 --> 
Das ist ein Flugsimulator der ein Höhenruder, ein Querruder ein Propeller dessen Geschwindigkeit man verstellen kann und eine Warnung die den Piloten warnt, wenn er zu steil hoch fliegt.