---
layout: "image"
title: "Motor, der das Querruder antreibt"
date: "2008-03-22T22:21:06"
picture: "flugsimulator4.jpg"
weight: "5"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/14034
imported:
- "2019"
_4images_image_id: "14034"
_4images_cat_id: "1285"
_4images_user_id: "747"
_4images_image_date: "2008-03-22T22:21:06"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14034 -->
Hier sieht man den Motor, der das Querruder antreibt.