---
layout: "image"
title: "Motor, der das Höhenruder antreibt"
date: "2008-03-22T22:21:06"
picture: "flugsimulator3.jpg"
weight: "4"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/14033
imported:
- "2019"
_4images_image_id: "14033"
_4images_cat_id: "1285"
_4images_user_id: "747"
_4images_image_date: "2008-03-22T22:21:06"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14033 -->
Auf diesem Bild sieht man den Motor, der das Höhenruder antreibt. Die Untersetzung des Motors ist 50:1.