---
layout: "image"
title: "Trafo"
date: "2008-03-22T22:21:06"
picture: "flugsimulator6.jpg"
weight: "7"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/14036
imported:
- "2019"
_4images_image_id: "14036"
_4images_cat_id: "1285"
_4images_user_id: "747"
_4images_image_date: "2008-03-22T22:21:06"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14036 -->
Auf diesem Bild sieht man den Trafo, mit dem man die Geschwindigkeit des Propellers regulieren kann,