---
layout: "image"
title: "Lichtschranke"
date: "2008-03-22T22:21:06"
picture: "flugsimulator9.jpg"
weight: "10"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/14039
imported:
- "2019"
_4images_image_id: "14039"
_4images_cat_id: "1285"
_4images_user_id: "747"
_4images_image_date: "2008-03-22T22:21:06"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14039 -->
Wenn das Flugzeug zu steil hoch fliegt wird diese Lichtschranke durchbrochen. Dann blinkt die rote Warnlampe (siehe Bild 5) und der Summer summt (siehe Bild 7).