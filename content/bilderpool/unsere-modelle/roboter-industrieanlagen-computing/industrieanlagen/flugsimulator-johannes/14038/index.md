---
layout: "image"
title: "Streben"
date: "2008-03-22T22:21:06"
picture: "flugsimulator8.jpg"
weight: "9"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/14038
imported:
- "2019"
_4images_image_id: "14038"
_4images_cat_id: "1285"
_4images_user_id: "747"
_4images_image_date: "2008-03-22T22:21:06"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14038 -->
Auf diesem Bild sieht man die Streben, die das Gerüst stabilisieren.