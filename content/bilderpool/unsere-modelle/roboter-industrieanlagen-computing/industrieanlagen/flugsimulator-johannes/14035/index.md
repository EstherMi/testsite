---
layout: "image"
title: "Joystick"
date: "2008-03-22T22:21:06"
picture: "flugsimulator5.jpg"
weight: "6"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/14035
imported:
- "2019"
_4images_image_id: "14035"
_4images_cat_id: "1285"
_4images_user_id: "747"
_4images_image_date: "2008-03-22T22:21:06"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14035 -->
Das ist der Joystick, mit dem man das Flugzeug steuert.