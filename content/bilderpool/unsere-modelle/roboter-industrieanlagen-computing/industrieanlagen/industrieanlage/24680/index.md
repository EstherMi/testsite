---
layout: "image"
title: "Gesamtansicht 2"
date: "2009-07-29T23:29:11"
picture: "industrieanlage03.jpg"
weight: "3"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- details/24680
imported:
- "2019"
_4images_image_id: "24680"
_4images_cat_id: "492"
_4images_user_id: "791"
_4images_image_date: "2009-07-29T23:29:11"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24680 -->
