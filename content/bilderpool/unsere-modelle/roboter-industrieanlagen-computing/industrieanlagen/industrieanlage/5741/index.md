---
layout: "image"
title: "Schieber"
date: "2006-02-05T16:30:58"
picture: "DSC01416Schieber.jpg"
weight: "15"
konstrukteure: 
- "Felix"
fotografen:
- "Felix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "felix"
license: "unknown"
legacy_id:
- details/5741
imported:
- "2019"
_4images_image_id: "5741"
_4images_cat_id: "492"
_4images_user_id: "410"
_4images_image_date: "2006-02-05T16:30:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5741 -->
