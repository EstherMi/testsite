---
layout: "image"
title: "Wender auf Transferstraße"
date: "2006-02-05T16:30:57"
picture: "DSC01436_wender.jpg"
weight: "13"
konstrukteure: 
- "Felix"
fotografen:
- "Felix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "felix"
license: "unknown"
legacy_id:
- details/5739
imported:
- "2019"
_4images_image_id: "5739"
_4images_cat_id: "492"
_4images_user_id: "410"
_4images_image_date: "2006-02-05T16:30:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5739 -->
