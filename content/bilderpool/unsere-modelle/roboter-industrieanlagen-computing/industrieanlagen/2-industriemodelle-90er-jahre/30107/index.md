---
layout: "image"
title: "Industriemodell  1 Bearbeitungsstrasse"
date: "2011-02-25T13:50:52"
picture: "industriemodellederzigerjahre02.jpg"
weight: "2"
konstrukteure: 
- "Fa.Staudinger"
fotografen:
- "M.Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/30107
imported:
- "2019"
_4images_image_id: "30107"
_4images_cat_id: "2225"
_4images_user_id: "968"
_4images_image_date: "2011-02-25T13:50:52"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30107 -->
Hier wird ein Werkstück einem Magazin entnommen und über ein Transportband weiterbefördert.