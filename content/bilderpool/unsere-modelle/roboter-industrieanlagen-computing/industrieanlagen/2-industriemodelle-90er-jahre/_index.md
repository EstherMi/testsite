---
layout: "overview"
title: "2 Industriemodelle der 90er Jahre"
date: 2019-12-17T19:01:21+01:00
legacy_id:
- categories/2225
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2225 --> 
Diese beiden Modelle habe ich zufällig in einer Kleinanzeige eines beliebten Internetportals entdeckt und bin gestern 800 km gefahren um sie jetzt "meins" zu nennen.

Beide Modelle stammen laut den noch vorhandenen Unterlagen von der Firma Staudinger. und müßten etwa von 1992/94 sein. Beide Modell sind etwas veschmutzt,
machen aber technisch noch einen guten Eindruck.Es sind Unmengen von Relais,Lichtschranken, Magneten und viele Spezialteile verbaut,die ich in meiner FT Laufbahn
noch nie gesehen hatte.Besonders freue ich mich über die viele 24 V Minimotoren in der kleinen Bauform und das viele  Alu .
Beide Modell wollte ich hier noch mal zeigen,bevor ich sie demontiere. Allerdings ziehe ich vorher noch einen Bekannten hinzu,der sich in Mechatronic und Steuerungstechnik auskennt.
Oder hat jemand einen anderen Vorschlag? Ich verfüge als Orgelbauer nicht über das Fachwissen,derartige Anlagen wieder zum funktionieren zu bringen.

Gruß
Markus Wolf