---
layout: "image"
title: "Industriemodell 2"
date: "2011-02-25T13:50:52"
picture: "industriemodellederzigerjahre08.jpg"
weight: "8"
konstrukteure: 
- "Fa.Staudinger"
fotografen:
- "M.Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/30113
imported:
- "2019"
_4images_image_id: "30113"
_4images_cat_id: "2225"
_4images_user_id: "968"
_4images_image_date: "2011-02-25T13:50:52"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30113 -->
Hier ein " Kombiniertes Modell Transport und Sortierstrecke  und Dreiachsmodell" wie es im Datenblatt heisst.