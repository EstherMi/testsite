---
layout: "image"
title: "Industriemodell  1 Bearbeitungsstrasse"
date: "2011-02-25T13:50:52"
picture: "industriemodellederzigerjahre07.jpg"
weight: "7"
konstrukteure: 
- "Fa.Staudinger"
fotografen:
- "M.Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/30112
imported:
- "2019"
_4images_image_id: "30112"
_4images_cat_id: "2225"
_4images_user_id: "968"
_4images_image_date: "2011-02-25T13:50:52"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30112 -->
Hier das gesamte Steuerteil.