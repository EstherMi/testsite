---
layout: "image"
title: "Logistikzentrum"
date: "2016-07-25T16:45:59"
picture: "logzen01.jpg"
weight: "3"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/44000
imported:
- "2019"
_4images_image_id: "44000"
_4images_cat_id: "3256"
_4images_user_id: "2228"
_4images_image_date: "2016-07-25T16:45:59"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44000 -->
Das Logistikzentrum besteht aus einem Hochregallager und einem Logistikbereich zur Warenannahme und Warenausgabe. Waren (die gelben Werkstücke) werden vollautomatisch eingelagert und auf Wunsch wieder ausgelagert