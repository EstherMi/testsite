---
layout: "comment"
hidden: true
title: "22307"
date: "2016-07-26T18:34:31"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Fettes Kompliment. Diese Industrieanlagen sind ja immer "im Prinzip" ganz einfach. Aber die Kunst ist es halt, das Gesamtmodell dann auch wirklich zum reibungslos Funktionieren zu bringen, so toll sauber aufzubauen und zu verkabeln (da kann ich ja nur von träumen) und dann noch so gute Fotos - mit Beschreibungen! - hier einzustellen. 100 Punkte, würde ich sagen ;-)

Gruß,
Stefan