---
layout: "image"
title: "Antrieb des Regalbediengeräts"
date: "2016-07-25T16:46:11"
picture: "logzen11.jpg"
weight: "13"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/44010
imported:
- "2019"
_4images_image_id: "44010"
_4images_cat_id: "3256"
_4images_user_id: "2228"
_4images_image_date: "2016-07-25T16:46:11"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44010 -->
Das RBG wird über einen Encodermotor angetrieben. Dieser treibt das Z15 mit der Übersetzung 10:15 an. Die feinen Zähne des Z15 auf der Unterseite greifen in die Schiene und sorgen für die Bewegung des RBGs entlang des Regals. Das RBG kann dadurch sehr schnelle Bewegungen ausführen, die Software verhindert ein Ausbrechen durch Beschleunigen und Abbremsen der Motoren.