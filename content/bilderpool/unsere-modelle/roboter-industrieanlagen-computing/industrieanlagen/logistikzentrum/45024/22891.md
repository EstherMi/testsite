---
layout: "comment"
hidden: true
title: "22891"
date: "2017-01-09T20:47:02"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Hallo Dirk,

ich simuliere das chaotische Lagersystem, indem ich beim Einlagern einen zufälligen Platz im Lager auswähle. Im Programm wird unter diesem Platz dann die ID des eingelagerten Objekts hinterlegt. Das ist dann mehr oder weniger Chaos, da ja nur der TX den Platz des Objekts kennt. Die Elemente werden also nicht strukturiert oder nach Farbe sortiert eingelagert.

Grüße
David