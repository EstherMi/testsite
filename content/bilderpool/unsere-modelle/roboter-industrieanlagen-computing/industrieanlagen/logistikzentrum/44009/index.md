---
layout: "image"
title: "Schiene"
date: "2016-07-25T16:45:59"
picture: "logzen10.jpg"
weight: "12"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/44009
imported:
- "2019"
_4images_image_id: "44009"
_4images_cat_id: "3256"
_4images_user_id: "2228"
_4images_image_date: "2016-07-25T16:45:59"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44009 -->
Die Schienen bestehen aus Zahnstangen (oben), einer glatten Schiene (unten) und zwei Stangen zur Führung des RBGs.