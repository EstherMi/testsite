---
layout: "image"
title: "Pressstation"
date: "2008-04-12T09:19:10"
picture: "pneumatischefabrik09.jpg"
weight: "9"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/14224
imported:
- "2019"
_4images_image_id: "14224"
_4images_cat_id: "1316"
_4images_user_id: "747"
_4images_image_date: "2008-04-12T09:19:10"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14224 -->
Hier sieht man die Presse, die das Werkstück pneumatisch presst.