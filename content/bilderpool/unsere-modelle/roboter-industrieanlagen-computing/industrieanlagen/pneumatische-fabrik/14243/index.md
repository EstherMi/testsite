---
layout: "image"
title: "Akku"
date: "2008-04-12T09:19:10"
picture: "pneumatischefabrik28.jpg"
weight: "28"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/14243
imported:
- "2019"
_4images_image_id: "14243"
_4images_cat_id: "1316"
_4images_user_id: "747"
_4images_image_date: "2008-04-12T09:19:10"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14243 -->
Hier sieht man den Akku.