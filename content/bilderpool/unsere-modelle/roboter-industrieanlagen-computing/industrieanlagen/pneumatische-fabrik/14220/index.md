---
layout: "image"
title: "Schieben der Werkstücke"
date: "2008-04-12T09:19:10"
picture: "pneumatischefabrik05.jpg"
weight: "5"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/14220
imported:
- "2019"
_4images_image_id: "14220"
_4images_cat_id: "1316"
_4images_user_id: "747"
_4images_image_date: "2008-04-12T09:19:10"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14220 -->
Hier sieht man genau "wohin" das Werkstück geschoben wird.