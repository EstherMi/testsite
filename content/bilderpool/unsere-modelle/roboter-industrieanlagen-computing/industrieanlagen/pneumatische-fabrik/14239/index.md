---
layout: "image"
title: "Ampel"
date: "2008-04-12T09:19:10"
picture: "pneumatischefabrik24.jpg"
weight: "24"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/14239
imported:
- "2019"
_4images_image_id: "14239"
_4images_cat_id: "1316"
_4images_user_id: "747"
_4images_image_date: "2008-04-12T09:19:10"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14239 -->
Auf diesem Bild sieht man die Ampel. Sie ist gerade grün, weil die Lichtschranke durchbrochen ist. So können die Arbeiter sehen, ob noch genug Werkstücke vorhanden sind.