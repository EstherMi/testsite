---
layout: "image"
title: "Kabel"
date: "2008-04-12T09:19:10"
picture: "pneumatischefabrik22.jpg"
weight: "22"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/14237
imported:
- "2019"
_4images_image_id: "14237"
_4images_cat_id: "1316"
_4images_user_id: "747"
_4images_image_date: "2008-04-12T09:19:10"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14237 -->
Hier sieht man die Kabel von dem Kästchen-Wechsler.