---
layout: "image"
title: "Interface und Extension"
date: "2008-04-12T09:19:10"
picture: "pneumatischefabrik29.jpg"
weight: "29"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/14244
imported:
- "2019"
_4images_image_id: "14244"
_4images_cat_id: "1316"
_4images_user_id: "747"
_4images_image_date: "2008-04-12T09:19:10"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14244 -->
Auf diesem Bild sieht man das Interface und das Extension.