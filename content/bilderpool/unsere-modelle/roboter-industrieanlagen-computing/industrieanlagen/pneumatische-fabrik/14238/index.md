---
layout: "image"
title: "Warner"
date: "2008-04-12T09:19:10"
picture: "pneumatischefabrik23.jpg"
weight: "23"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/14238
imported:
- "2019"
_4images_image_id: "14238"
_4images_cat_id: "1316"
_4images_user_id: "747"
_4images_image_date: "2008-04-12T09:19:10"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14238 -->
Hier sieht man eine Lichtschranke. Wenn sie durchbrochen ist, sind genug Werkstücke vorhanden und wenn nicht, nicht.