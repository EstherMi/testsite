---
layout: "image"
title: "Etikettiermaschine"
date: "2012-01-12T16:52:51"
picture: "etikettirmaschine03.jpg"
weight: "3"
konstrukteure: 
- "Elmar Brix"
fotografen:
- "Elmar Brix"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ixer"
license: "unknown"
legacy_id:
- details/33887
imported:
- "2019"
_4images_image_id: "33887"
_4images_cat_id: "2507"
_4images_user_id: "1361"
_4images_image_date: "2012-01-12T16:52:51"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33887 -->
Steuereinheit. Da die Steuereinheit nicht mehr auf die Bauplatte der Etikettiermaschine passte, wurde sie separat aufgebaut und über ein vieladriges Flachbandkabel mit der Maschine verbunden.