---
layout: "image"
title: "Roboter-Aufzug-07"
date: "2012-02-18T13:28:18"
picture: "HUB-07.jpg"
weight: "7"
konstrukteure: 
- "SkobyMobil"
fotografen:
- "SkobyMobil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "SkobyMobil"
license: "unknown"
legacy_id:
- details/34220
imported:
- "2019"
_4images_image_id: "34220"
_4images_cat_id: "2533"
_4images_user_id: "1407"
_4images_image_date: "2012-02-18T13:28:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34220 -->
das ist die hintere Ansicht der Hebevorrichtung. Die Montage der hinteren Laufräder, sowie die Montage des Motors und des Hub-Getriebe sind hier zu sehen-