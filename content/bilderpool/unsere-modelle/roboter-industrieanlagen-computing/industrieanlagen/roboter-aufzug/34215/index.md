---
layout: "image"
title: "Roboter-Aufzug-02"
date: "2012-02-18T13:28:18"
picture: "HUB-02.jpg"
weight: "2"
konstrukteure: 
- "SkobyMobil"
fotografen:
- "SkobyMobil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "SkobyMobil"
license: "unknown"
legacy_id:
- details/34215
imported:
- "2019"
_4images_image_id: "34215"
_4images_cat_id: "2533"
_4images_user_id: "1407"
_4images_image_date: "2012-02-18T13:28:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34215 -->
...wenn er diese Vorrichtung erreicht hat, wird er in die Ebene -1- gehoben. Dort verläßt er die Vorrichtung und erledigt in dieser Ebene seine Arbeiten. Wenn dieses geschehen ist macht er sich auf den Weg zur nächsten Ebene. Dieser Roboter-Aufzug hat z.B. 3 Ebenen. Ebene -0- , -1- , -2- . Er ist selbsttragen. Montiert aus je 2 Laufschienen auf der rechten und linken Seite. In der Mitte der beiden Laufschienen verläuft eine Hub-Zahnstange für den Verfahrweg. Wenn man auf die Vorrichtung (in Ebene -2-) den Tech-In Roboter stellt, neigt sich der Aufzug so stark, dass man es vernachlässigen kann. Man darf nicht vergessen, dass man hier schon eine Arbeitshöhe von ca. 420mm hat.