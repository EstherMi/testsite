---
layout: "image"
title: "Roboter-Aufzug-01"
date: "2012-02-18T13:28:17"
picture: "HUB-01.jpg"
weight: "1"
konstrukteure: 
- "SkobyMobil"
fotografen:
- "SkobyMobil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "SkobyMobil"
license: "unknown"
legacy_id:
- details/34214
imported:
- "2019"
_4images_image_id: "34214"
_4images_cat_id: "2533"
_4images_user_id: "1407"
_4images_image_date: "2012-02-18T13:28:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34214 -->
der Teach-In Roboter und andere Industrie-Roboter haben eines gemeinsam- einen recht beschränkten Wirkungskreis. Sie können hoch und runter- und im Kreis. Der Bewegungsradius ist immer abhängig vom Arbeitsarm und dessen Greifer. Ich habe mir gedacht, dass man so einen Roboter doch einen größeren Radius zuordnen kann, ohne ihn großmächtig zu verändern. Die Idee: Der Roboter kommt auf einem Fahrgestell, angetrieben durch ein Hub-Getriebe, und in Spur gehalten durch Leer-Hubgetriebe auf Hub-Zahnstangen. Von seiner Ausgangsposition bewegt er sich auf die Hebevorrichtung...