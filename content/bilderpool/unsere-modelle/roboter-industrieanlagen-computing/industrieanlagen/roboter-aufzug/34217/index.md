---
layout: "image"
title: "Roboter-Aufzug-04"
date: "2012-02-18T13:28:18"
picture: "HUB-04.jpg"
weight: "4"
konstrukteure: 
- "SkobyMobil"
fotografen:
- "SkobyMobil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "SkobyMobil"
license: "unknown"
legacy_id:
- details/34217
imported:
- "2019"
_4images_image_id: "34217"
_4images_cat_id: "2533"
_4images_user_id: "1407"
_4images_image_date: "2012-02-18T13:28:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34217 -->
Hier der Anfahrtsweg (links) und die Hebe-Vorrichtung (rechts) ca. 15mm angehoben