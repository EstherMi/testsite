---
layout: "image"
title: "Bearbeitungszentrum 001"
date: "2011-10-20T17:10:01"
picture: "FT_Derk_001.jpg"
weight: "1"
konstrukteure: 
- "FT Derk"
fotografen:
- "Derk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Derk"
license: "unknown"
legacy_id:
- details/33257
imported:
- "2019"
_4images_image_id: "33257"
_4images_cat_id: "635"
_4images_user_id: "1289"
_4images_image_date: "2011-10-20T17:10:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33257 -->
