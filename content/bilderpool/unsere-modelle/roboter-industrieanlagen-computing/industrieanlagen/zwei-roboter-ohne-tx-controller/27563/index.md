---
layout: "image"
title: "3"
date: "2010-06-25T18:20:40"
picture: "zweiroboter3.jpg"
weight: "3"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27563
imported:
- "2019"
_4images_image_id: "27563"
_4images_cat_id: "1982"
_4images_user_id: "1082"
_4images_image_date: "2010-06-25T18:20:40"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27563 -->
Der Roboter aus dem Industrierobots Kasten mit kleine Veränderungen.