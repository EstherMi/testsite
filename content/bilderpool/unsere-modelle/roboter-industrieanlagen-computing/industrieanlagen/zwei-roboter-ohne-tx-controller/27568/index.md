---
layout: "image"
title: "8"
date: "2010-06-25T18:20:41"
picture: "zweiroboter8.jpg"
weight: "8"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27568
imported:
- "2019"
_4images_image_id: "27568"
_4images_cat_id: "1982"
_4images_user_id: "1082"
_4images_image_date: "2010-06-25T18:20:41"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27568 -->
Schloss mit Schlüssel