---
layout: "image"
title: "1"
date: "2010-06-25T18:20:39"
picture: "zweiroboter1.jpg"
weight: "1"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27561
imported:
- "2019"
_4images_image_id: "27561"
_4images_cat_id: "1982"
_4images_user_id: "1082"
_4images_image_date: "2010-06-25T18:20:39"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27561 -->
Hier die ganze Maschine. Der mittlere ist ein Roboter aus dem Industrierobots Kasten, rechts ist ein ausgedachter und Teilweise Handgesteuerter Roboter. Der große Roboter nimmt von links eine Gelbe Dose, übergibt diese dem rechten Roboter, und der legt sie auf eine Rampe auf der die Dose nach hinten rollt. Die Rampe ist zurzeit gerade nicht da.