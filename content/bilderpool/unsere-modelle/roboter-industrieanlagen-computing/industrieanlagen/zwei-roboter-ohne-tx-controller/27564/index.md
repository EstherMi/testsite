---
layout: "image"
title: "4"
date: "2010-06-25T18:20:40"
picture: "zweiroboter4.jpg"
weight: "4"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27564
imported:
- "2019"
_4images_image_id: "27564"
_4images_cat_id: "1982"
_4images_user_id: "1082"
_4images_image_date: "2010-06-25T18:20:40"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27564 -->
Der Teilweise Handbetriebene Roboter. links von ihm steht der große Roboter. 
1. Er klappt die Zange nach rechts, sodass sie waagerecht ist. 
2. Er klappt die zu (natürlich erst wenn die gelbe Tonne darin klemmt)
3. Er klappt die Zange wieder hoch
4. Er dreht sich nach hinten, man sieht hier auch den Drehkranz
5. Er macht hinten die Zange auf und die Gelbe Tonne rollt eine Rampe über den Kompressor im Hintergrund hinab. 
6. Der Roboter dreht sich wieder hoch, in die Stellung die auf dem Bild zu sehen ist