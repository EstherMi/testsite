---
layout: "image"
title: "2"
date: "2010-06-25T18:20:40"
picture: "zweiroboter2.jpg"
weight: "2"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27562
imported:
- "2019"
_4images_image_id: "27562"
_4images_cat_id: "1982"
_4images_user_id: "1082"
_4images_image_date: "2010-06-25T18:20:40"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27562 -->
Noch mal das ganze, diesmal flach von der Seite.