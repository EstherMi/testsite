---
layout: "image"
title: "Das Greifwerkzeug"
date: "2008-09-03T18:23:31"
picture: "produktionmaschine03.jpg"
weight: "3"
konstrukteure: 
- "alone"
fotografen:
- "alone"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "alone"
license: "unknown"
legacy_id:
- details/15163
imported:
- "2019"
_4images_image_id: "15163"
_4images_cat_id: "1389"
_4images_user_id: "741"
_4images_image_date: "2008-09-03T18:23:31"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15163 -->
Hier wird der Rohling Festgehalten und bearbeitet