---
layout: "image"
title: "Das Förderband 2"
date: "2008-09-03T18:23:32"
picture: "produktionmaschine11.jpg"
weight: "11"
konstrukteure: 
- "alone"
fotografen:
- "alone"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "alone"
license: "unknown"
legacy_id:
- details/15171
imported:
- "2019"
_4images_image_id: "15171"
_4images_cat_id: "1389"
_4images_user_id: "741"
_4images_image_date: "2008-09-03T18:23:32"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15171 -->
Hier sieht man die Obere Lage von unten
Wie man nur schwer erkennen kann, ist das Förderband in der Mitte auf einer "Schiene"