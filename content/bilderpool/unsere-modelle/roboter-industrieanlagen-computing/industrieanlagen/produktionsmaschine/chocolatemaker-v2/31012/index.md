---
layout: "image"
title: "Hauptschalter"
date: "2011-07-08T18:00:37"
picture: "chekmaker21.jpg"
weight: "21"
konstrukteure: 
- "scripter1"
fotografen:
- "scripter1"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "scripter1"
license: "unknown"
legacy_id:
- details/31012
imported:
- "2019"
_4images_image_id: "31012"
_4images_cat_id: "2304"
_4images_user_id: "1305"
_4images_image_date: "2011-07-08T18:00:37"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31012 -->
Eine weitere kleinere Spielerei: Wenn der Hauptschalter umgelegt ist, ist der Kabamaker, trotz laufemdem IF+EM, aus.