---
layout: "comment"
hidden: true
title: "14572"
date: "2011-07-09T23:58:15"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Gute Fotos brauchen zunächst mal viel Licht. Dann am besten von jedem Motiv mehrere Aufnahmen machen und alles unscharfe gnadenlos aussortieren. Und erst danach bringt's das Beschneiden, die Gammakorrektur und das Schärfen innerhalb des Publishers. Die Auflösung lass für die ft Community ruhig auf den Standardwerten, denn der Publisher würde für die Veröffentlichung nach hier eh nie die hier erlaubten Bildgrenzen von 800 Pixel in jeder Richtung überschreiten.

Gruß,
Stefan