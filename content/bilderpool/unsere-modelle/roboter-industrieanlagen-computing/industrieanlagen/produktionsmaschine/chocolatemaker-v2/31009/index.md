---
layout: "image"
title: "Mixer von oben"
date: "2011-07-08T18:00:37"
picture: "chekmaker18.jpg"
weight: "18"
konstrukteure: 
- "scripter1"
fotografen:
- "scripter1"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "scripter1"
license: "unknown"
legacy_id:
- details/31009
imported:
- "2019"
_4images_image_id: "31009"
_4images_cat_id: "2304"
_4images_user_id: "1305"
_4images_image_date: "2011-07-08T18:00:37"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31009 -->
Auf diesem Bild ist ein kleiner MiniMotor mit Hubgetriebe auf einer Zahnstange gut zu erkennen. Er bewegt den etwas größeren PowerMotor auf und ab.