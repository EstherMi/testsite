---
layout: "image"
title: "Münzeinwurf"
date: "2011-06-14T22:35:23"
picture: "chocolatemakerv1.jpg"
weight: "22"
konstrukteure: 
- "scripter1"
fotografen:
- "scripter1"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "scripter1"
license: "unknown"
legacy_id:
- details/30858
imported:
- "2019"
_4images_image_id: "30858"
_4images_cat_id: "2304"
_4images_user_id: "1305"
_4images_image_date: "2011-06-14T22:35:23"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30858 -->
Der Geldeinwurf (von Fanclubmodell Taschentuchspender) mit einer Ampel.