---
layout: "comment"
hidden: true
title: "14476"
date: "2011-06-18T22:29:54"
uploadBy:
- "scripter1"
license: "unknown"
imported:
- "2019"
---
Habe den kompletten Chocolatemaker (er macht übrigens Kaba & Eiskaffee zusammen) in den letzten Tagen mit einem Freund fertiggebaut! Ich feile nur noch ein wenig die Pulver-Einförderung aus, da im Moment noch eine große "Sauerei" entsteht. Aber Milch, Mixer, Geldeinwurf & Auswahl funktionieren Perfekt!

Ich werde bei Gelegenheit noch ein paar Bilder anstatt mit dem iPod mit unserer Cam machen. Dann ist die Qualität auch besser!