---
layout: "image"
title: "Kompressoren"
date: "2011-06-14T22:35:23"
picture: "chocolatemakerv4.jpg"
weight: "25"
konstrukteure: 
- "scripter1"
fotografen:
- "scripter1"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "scripter1"
license: "unknown"
legacy_id:
- details/30861
imported:
- "2019"
_4images_image_id: "30861"
_4images_cat_id: "2304"
_4images_user_id: "1305"
_4images_image_date: "2011-06-14T22:35:23"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30861 -->
Die beiden Kompressoren, den rechten habe ich selbst gebaut. Im Hintergrund ist noch ein Magnetventil zu sehen. Der Luftdruck dient später dazu, in eine Flasche geleitet zu werden und die Milch "herauszudrücken"