---
layout: "image"
title: "Kompressor #2"
date: "2011-07-08T18:00:37"
picture: "chekmaker08.jpg"
weight: "8"
konstrukteure: 
- "scripter1"
fotografen:
- "scripter1"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "scripter1"
license: "unknown"
legacy_id:
- details/30999
imported:
- "2019"
_4images_image_id: "30999"
_4images_cat_id: "2304"
_4images_user_id: "1305"
_4images_image_date: "2011-07-08T18:00:37"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30999 -->
Das ist der "original" ft-Kompressor. Im Hintergrund sieht man ein Magnetventil.