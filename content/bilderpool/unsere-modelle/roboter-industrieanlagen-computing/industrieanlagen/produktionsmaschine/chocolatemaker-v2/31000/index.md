---
layout: "image"
title: "Kompressor #2 von vorne"
date: "2011-07-08T18:00:37"
picture: "chekmaker09.jpg"
weight: "9"
konstrukteure: 
- "scripter1"
fotografen:
- "scripter1"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "scripter1"
license: "unknown"
legacy_id:
- details/31000
imported:
- "2019"
_4images_image_id: "31000"
_4images_cat_id: "2304"
_4images_user_id: "1305"
_4images_image_date: "2011-07-08T18:00:37"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31000 -->
Hier nochmal der ft-Kompressor in der Vorderansicht