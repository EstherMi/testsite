---
layout: "comment"
hidden: true
title: "14573"
date: "2011-07-09T23:59:32"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ich meine die nur einfach befestigten Grundbausteine, die den Motor und den Zylinder letztlich halten. Ob die nicht etwas Verstärkung gebrauchen könnten? Jedes bisschen Biegearbeit verschlingt nämlich Energie, die dann dem Pumpvorgang abgeht.

Gruß,
Stefan