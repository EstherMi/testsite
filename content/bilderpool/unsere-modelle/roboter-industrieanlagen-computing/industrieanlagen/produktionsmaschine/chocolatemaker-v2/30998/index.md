---
layout: "image"
title: "Kompressor #1"
date: "2011-07-08T18:00:37"
picture: "chekmaker07.jpg"
weight: "7"
konstrukteure: 
- "scripter1"
fotografen:
- "scripter1"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "scripter1"
license: "unknown"
legacy_id:
- details/30998
imported:
- "2019"
_4images_image_id: "30998"
_4images_cat_id: "2304"
_4images_user_id: "1305"
_4images_image_date: "2011-07-08T18:00:37"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30998 -->
Hier ist der "stärkere" Kompressor mit PowerMotor zu sehen.