---
layout: "image"
title: "Kette mit Pulverschaufeln"
date: "2011-07-08T18:00:37"
picture: "chekmaker15.jpg"
weight: "15"
konstrukteure: 
- "scripter1"
fotografen:
- "scripter1"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "scripter1"
license: "unknown"
legacy_id:
- details/31006
imported:
- "2019"
_4images_image_id: "31006"
_4images_cat_id: "2304"
_4images_user_id: "1305"
_4images_image_date: "2011-07-08T18:00:37"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31006 -->
Auf dieser Kette sind 2 gegenüberliegende Schaufeln befestigt, welche - sobald sie unter einer Lichtschranke durchgefahren sind - mit Kaba bzw. Eiskaffee gefüllt werden.