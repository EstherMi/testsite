---
layout: "image"
title: "Die Arbeit Paletten Station"
date: "2008-09-03T18:23:30"
picture: "produktionmaschine01.jpg"
weight: "1"
konstrukteure: 
- "alone"
fotografen:
- "alone"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "alone"
license: "unknown"
legacy_id:
- details/15161
imported:
- "2019"
_4images_image_id: "15161"
_4images_cat_id: "1389"
_4images_user_id: "741"
_4images_image_date: "2008-09-03T18:23:30"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15161 -->
Bauplatten 45x45 werden hier als Arbeitspaletten eingesetzt. Diese werden in den Aussparungen der Kette nach unten auf das Hauptförderband weitergeleitet.