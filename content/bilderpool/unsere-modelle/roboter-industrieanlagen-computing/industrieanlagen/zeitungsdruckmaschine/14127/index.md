---
layout: "image"
title: "Trockenstation"
date: "2008-03-27T11:16:20"
picture: "zeitungsdruckmaschine5.jpg"
weight: "5"
konstrukteure: 
- "pinkpanter"
fotografen:
- "pinkpanter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pinkpanter"
license: "unknown"
legacy_id:
- details/14127
imported:
- "2019"
_4images_image_id: "14127"
_4images_cat_id: "1300"
_4images_user_id: "760"
_4images_image_date: "2008-03-27T11:16:20"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14127 -->
