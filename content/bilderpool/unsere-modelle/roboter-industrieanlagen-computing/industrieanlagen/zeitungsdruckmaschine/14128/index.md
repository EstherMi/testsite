---
layout: "image"
title: "Gesamtansicht"
date: "2008-03-27T11:16:20"
picture: "zeitungsdruckmaschine6.jpg"
weight: "6"
konstrukteure: 
- "pinkpanter"
fotografen:
- "pinkpanter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pinkpanter"
license: "unknown"
legacy_id:
- details/14128
imported:
- "2019"
_4images_image_id: "14128"
_4images_cat_id: "1300"
_4images_user_id: "760"
_4images_image_date: "2008-03-27T11:16:20"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14128 -->
