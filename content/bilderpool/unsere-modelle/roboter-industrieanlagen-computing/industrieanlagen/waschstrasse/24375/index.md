---
layout: "image"
title: "Sicht auf das geschlossene Tor."
date: "2009-06-15T22:51:31"
picture: "waschstrasse02.jpg"
weight: "2"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- details/24375
imported:
- "2019"
_4images_image_id: "24375"
_4images_cat_id: "1671"
_4images_user_id: "791"
_4images_image_date: "2009-06-15T22:51:31"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24375 -->
