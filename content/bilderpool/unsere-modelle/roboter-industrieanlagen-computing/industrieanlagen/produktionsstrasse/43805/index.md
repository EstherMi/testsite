---
layout: "image"
title: "Der Auswurf"
date: "2016-06-28T18:57:39"
picture: "produktionsstrasse2.jpg"
weight: "2"
konstrukteure: 
- "Jori"
fotografen:
- "Jori"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jori"
license: "unknown"
legacy_id:
- details/43805
imported:
- "2019"
_4images_image_id: "43805"
_4images_cat_id: "3245"
_4images_user_id: "2465"
_4images_image_date: "2016-06-28T18:57:39"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43805 -->
Der Auswurf ist recht einfach gehalten, erfüllt aber seinen Zweck. Der unterste Stein liegt direkt auf dem Aluprofil auf, dass das Förderband seitlich begrenzt.