---
layout: "image"
title: "Die Düse II"
date: "2016-06-28T18:57:39"
picture: "produktionsstrasse4.jpg"
weight: "4"
konstrukteure: 
- "Jori"
fotografen:
- "Jori"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jori"
license: "unknown"
legacy_id:
- details/43807
imported:
- "2019"
_4images_image_id: "43807"
_4images_cat_id: "3245"
_4images_user_id: "2465"
_4images_image_date: "2016-06-28T18:57:39"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43807 -->
Die Düse nochmal aus einem anderen Winkel. Gut zu erkenn sind das Hubgetriebe und die Zahnstange. Die beiden Endlagentaster werden von einem Verbinder 15 auf dem BS 7,5 betätigt, an dem auch die Düse befestigt ist.