---
layout: "image"
title: "Der Antrieb des Förderbands"
date: "2016-06-28T18:57:39"
picture: "produktionsstrasse7.jpg"
weight: "7"
konstrukteure: 
- "Jori"
fotografen:
- "Jori"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jori"
license: "unknown"
legacy_id:
- details/43810
imported:
- "2019"
_4images_image_id: "43810"
_4images_cat_id: "3245"
_4images_user_id: "2465"
_4images_image_date: "2016-06-28T18:57:39"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43810 -->
Das Förderband wird über eine Überstzung 1:2 durch einen Encodermotor angetrieben. Der Motor sitzt direkt unter dem Zylinder für den Auswurf, was dazu führt das Öl aus dem freien Schauchanschluss auf den Motor tropft :(