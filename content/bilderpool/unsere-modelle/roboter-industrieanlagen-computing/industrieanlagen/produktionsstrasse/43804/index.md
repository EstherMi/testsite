---
layout: "image"
title: "Gesamtansicht"
date: "2016-06-28T18:57:39"
picture: "produktionsstrasse1.jpg"
weight: "1"
konstrukteure: 
- "Jori"
fotografen:
- "Jori"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jori"
license: "unknown"
legacy_id:
- details/43804
imported:
- "2019"
_4images_image_id: "43804"
_4images_cat_id: "3245"
_4images_user_id: "2465"
_4images_image_date: "2016-06-28T18:57:39"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43804 -->
Hier eine Gesamtansicht der Anlage.
Von rechts nach links sind der Einwurf, die Düse, der Ofen und der Auswurf zu sehen.