---
layout: "comment"
hidden: true
title: "22192"
date: "2016-06-29T06:50:51"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Schönes Modell, die Düse gefällt mir sehr gut!
Solange der Stein im Ofen ist, könnte das Förderband auch kurz anhalten, dann kommt dieser Prozess besser zur Geltung.

Gruß, David