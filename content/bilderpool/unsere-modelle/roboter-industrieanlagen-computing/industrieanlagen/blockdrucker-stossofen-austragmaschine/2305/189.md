---
layout: "comment"
hidden: true
title: "189"
date: "2004-03-12T14:32:47"
uploadBy:
- "Uwe Schmejkal"
license: "unknown"
imported:
- "2019"
---
MaßstabAch, vielleicht sollte man auch das Größenverhältnis mal erwähnen:  Die "Bauarbeiter" sind auch in der Realität so klein oder groß, wie man`s halt sehen möchte.