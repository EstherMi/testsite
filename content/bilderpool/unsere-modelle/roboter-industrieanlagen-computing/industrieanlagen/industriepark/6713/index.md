---
layout: "image"
title: "Industriepark 001"
date: "2006-08-28T23:28:03"
picture: "060828_Industriepark_001.jpg"
weight: "1"
konstrukteure: 
- "Micha Etz und ft"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- details/6713
imported:
- "2019"
_4images_image_id: "6713"
_4images_cat_id: "648"
_4images_user_id: "473"
_4images_image_date: "2006-08-28T23:28:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6713 -->
Das sind die Anfänge von meinem Industriepark. Rob1 übernimmt einen Stein, gibt ihn an die Taktstr. weiter, wird danach von Rob2 ("modern modifizierter Trainer") auf die Stanze übernommen, von dort geht es mit rob3 auf das Pneu Zentrum.