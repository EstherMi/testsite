---
layout: "image"
title: "MIP 035"
date: "2006-08-28T23:28:42"
picture: "B_060826_Industriemodule_12V_025.jpg"
weight: "28"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- details/6742
imported:
- "2019"
_4images_image_id: "6742"
_4images_cat_id: "648"
_4images_user_id: "473"
_4images_image_date: "2006-08-28T23:28:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6742 -->
