---
layout: "image"
title: "MIP 014"
date: "2006-08-28T23:28:21"
picture: "060828_Industriepark_014.jpg"
weight: "13"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- details/6725
imported:
- "2019"
_4images_image_id: "6725"
_4images_cat_id: "648"
_4images_user_id: "473"
_4images_image_date: "2006-08-28T23:28:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6725 -->
