---
layout: "image"
title: "Hebebüne mit Förderband"
date: "2008-04-02T14:33:03"
picture: "modelle9.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/14156
imported:
- "2019"
_4images_image_id: "14156"
_4images_cat_id: "1259"
_4images_user_id: "558"
_4images_image_date: "2008-04-02T14:33:03"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14156 -->
