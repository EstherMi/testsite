---
layout: "image"
title: "Starttaster"
date: "2008-02-19T17:20:50"
picture: "automatischetaktstrasse02.jpg"
weight: "11"
konstrukteure: 
- "Wusste leider niemand"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/13684
imported:
- "2019"
_4images_image_id: "13684"
_4images_cat_id: "1259"
_4images_user_id: "558"
_4images_image_date: "2008-02-19T17:20:50"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13684 -->
