---
layout: "image"
title: "Greifer"
date: "2009-11-16T20:40:40"
picture: "sortieranlagemitgreifer7.jpg"
weight: "7"
konstrukteure: 
- "Philipp Graffelder, Anton Schirg"
fotografen:
- "Philipp Graffelder"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pgas"
license: "unknown"
legacy_id:
- details/25786
imported:
- "2019"
_4images_image_id: "25786"
_4images_cat_id: "1808"
_4images_user_id: "1025"
_4images_image_date: "2009-11-16T20:40:40"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25786 -->
