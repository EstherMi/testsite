---
layout: "image"
title: "Gesammtansicht"
date: "2008-02-19T17:20:50"
picture: "automatischetaktstrasse06.jpg"
weight: "15"
konstrukteure: 
- "Wusste leider niemand"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/13688
imported:
- "2019"
_4images_image_id: "13688"
_4images_cat_id: "1259"
_4images_user_id: "558"
_4images_image_date: "2008-02-19T17:20:50"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13688 -->
