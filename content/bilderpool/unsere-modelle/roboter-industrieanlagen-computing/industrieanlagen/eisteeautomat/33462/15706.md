---
layout: "comment"
hidden: true
title: "15706"
date: "2011-11-16T14:08:12"
uploadBy:
- "Endlich"
license: "unknown"
imported:
- "2019"
---
Hallo Lukas,

ich hab den Eisteeautomat ja auch schon auf der Convention gesehen und war genauso begeistert wie die Besucher.
Du hast gerade die Idee mit den Drähten angesprochen, bei meinem Wasserspender habe ich auch mal da drüber nachgedacht, allerdings habe ich mir schnell wieder den Gedanken aus dem Kopf geschlagen, da es ja auch immer etwas mit Hygiene zu tun hat und zwei Drähte im Saft machen sich halt nicht so gut, außerdem fangen sie an zu oxidieren ( wie hier: http://www.ftcommunity.de/details.php?image_id=27945 ) und dann hast du schnell keinen Messerfolg mehr. 

Nehm doch einfach einen größeren Becher und fülle ihn nicht ganz voll, wie ich es bei meinem Wasserspender getan habe. 

MfG
Endlich