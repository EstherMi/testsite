---
layout: "comment"
hidden: true
title: "15739"
date: "2011-11-22T18:07:49"
uploadBy:
- "scripter1"
license: "unknown"
imported:
- "2019"
---
Hallo,

ja, das mit den Drähten war lediglich eine Idee, die mir auch ein paar Besucher auf der Convention vorgeschlagen haben. Aber danke für den Tipp, das werde ich dann auf jeden Fall NICHT ausprobieren :=) Wir hatten ja zuerst 200ml Becher (also 10x größer), aber der Nachteil ist, dass nach 2-3 Bechern die Wasserflasche leer ist. Herr Knobloch hatte uns vorgeschlagen, kleine Probierbecher zu benutzen, da - falls jemandem der Eistee nicht schmecken sollte - nicht zu viel übrig bleibt.. Da passiert dann auch nicht so viel, falls mal ein Becher umfallen sollte. Natürlich muss man den 200ml-Becher nicht ganz voll füllen - trotzdem haben sich die kleinen Becher bewährt.

lg
scripter1