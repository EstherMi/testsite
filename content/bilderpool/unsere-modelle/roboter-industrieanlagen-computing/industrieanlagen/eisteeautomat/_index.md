---
layout: "overview"
title: "Eisteeautomat"
date: 2019-12-17T19:01:36+01:00
legacy_id:
- categories/2480
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2480 --> 
Hier stelle ich euch meinen Eisteeautomat vor, der auch auf der Convention zu sehen war. Ich habe vor, alle 1-3 Monate neue Bilder zu veröffentlichen. Nähere Beschreibungen und die jeweiligen Funktionsweisen findet ihr in der Beschreibung der Bilder.