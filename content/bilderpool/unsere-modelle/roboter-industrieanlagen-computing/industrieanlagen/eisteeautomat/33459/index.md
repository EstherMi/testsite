---
layout: "image"
title: "Förderkette"
date: "2011-11-13T18:14:36"
picture: "eisteeautomat06.jpg"
weight: "6"
konstrukteure: 
- "Lukas Kamm (scripter1)"
fotografen:
- "Lukas Kamm (scripter1)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "scripter1"
license: "unknown"
legacy_id:
- details/33459
imported:
- "2019"
_4images_image_id: "33459"
_4images_cat_id: "2480"
_4images_user_id: "1305"
_4images_image_date: "2011-11-13T18:14:36"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33459 -->
An der Förderkette sind 2 selbstgebaute Schaufeln befestigt, welche durch den Behälter (dort wo später das Granulat reinkommt - habe ich aber für das Foto weggelassen) gezogen werden und somit den Eistee in einen Trichter auf der anderen Seite kippen. Das Granulat rutscht nach unten in den kleinen Becher. Die Position der Schaufeln wird durch eine Lichtschranke bestimmt.