---
layout: "image"
title: "Geldeinwurf und Mixer"
date: "2011-11-13T18:14:36"
picture: "eisteeautomat10.jpg"
weight: "10"
konstrukteure: 
- "Lukas Kamm (scripter1)"
fotografen:
- "Lukas Kamm (scripter1)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "scripter1"
license: "unknown"
legacy_id:
- details/33463
imported:
- "2019"
_4images_image_id: "33463"
_4images_cat_id: "2480"
_4images_user_id: "1305"
_4images_image_date: "2011-11-13T18:14:36"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33463 -->
Der S-Motor für den Rührer hat einen selbstgebauten Aufsatz (In eine Alu-Stange 2 Löcher gebohrt und ein Gewinde von der Seite hineingeschnitten). Der Mixer wird durch einen 2. S-Motor mit einem Hubgetriebe auf einer Hubstange an einem Alu-Profil auf und Abbewegt.