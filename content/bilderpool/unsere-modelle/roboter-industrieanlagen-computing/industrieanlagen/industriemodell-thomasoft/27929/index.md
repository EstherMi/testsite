---
layout: "image"
title: "industriemodellvonthomasoft21.jpg"
date: "2010-08-25T00:43:09"
picture: "industriemodellvonthomasoft21.jpg"
weight: "21"
konstrukteure: 
- "Thomasoft"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/27929
imported:
- "2019"
_4images_image_id: "27929"
_4images_cat_id: "2019"
_4images_user_id: "9"
_4images_image_date: "2010-08-25T00:43:09"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27929 -->
