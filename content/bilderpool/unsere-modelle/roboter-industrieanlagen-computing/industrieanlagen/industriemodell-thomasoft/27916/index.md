---
layout: "image"
title: "industriemodellvonthomasoft08.jpg"
date: "2010-08-25T00:42:59"
picture: "industriemodellvonthomasoft08.jpg"
weight: "8"
konstrukteure: 
- "Thomasoft"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/27916
imported:
- "2019"
_4images_image_id: "27916"
_4images_cat_id: "2019"
_4images_user_id: "9"
_4images_image_date: "2010-08-25T00:42:59"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27916 -->
