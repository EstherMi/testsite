---
layout: "image"
title: "industriemodellvonthomasoft02.jpg"
date: "2010-08-25T00:42:59"
picture: "industriemodellvonthomasoft02.jpg"
weight: "2"
konstrukteure: 
- "Thomasoft"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/27910
imported:
- "2019"
_4images_image_id: "27910"
_4images_cat_id: "2019"
_4images_user_id: "9"
_4images_image_date: "2010-08-25T00:42:59"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27910 -->
