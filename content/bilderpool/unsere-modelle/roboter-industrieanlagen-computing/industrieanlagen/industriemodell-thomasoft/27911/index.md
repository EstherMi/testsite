---
layout: "image"
title: "industriemodellvonthomasoft03.jpg"
date: "2010-08-25T00:42:59"
picture: "industriemodellvonthomasoft03.jpg"
weight: "3"
konstrukteure: 
- "Thomasoft"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/27911
imported:
- "2019"
_4images_image_id: "27911"
_4images_cat_id: "2019"
_4images_user_id: "9"
_4images_image_date: "2010-08-25T00:42:59"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27911 -->
