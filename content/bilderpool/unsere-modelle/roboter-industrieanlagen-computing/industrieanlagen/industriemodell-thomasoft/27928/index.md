---
layout: "image"
title: "industriemodellvonthomasoft20.jpg"
date: "2010-08-25T00:43:04"
picture: "industriemodellvonthomasoft20.jpg"
weight: "20"
konstrukteure: 
- "Thomasoft"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/27928
imported:
- "2019"
_4images_image_id: "27928"
_4images_cat_id: "2019"
_4images_user_id: "9"
_4images_image_date: "2010-08-25T00:43:04"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27928 -->
