---
layout: "image"
title: "Pneumatik-Roboter 5 Bild 3"
date: "2008-03-08T22:39:12"
picture: "Pneumatik-Roboter_5_Bild_3.jpg"
weight: "3"
konstrukteure: 
- "Andreas Gürten, Original von FT Experimenta Schulprogramm"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/13891
imported:
- "2019"
_4images_image_id: "13891"
_4images_cat_id: "1274"
_4images_user_id: "724"
_4images_image_date: "2008-03-08T22:39:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13891 -->
Detail Greifer.