---
layout: "comment"
hidden: true
title: "15896"
date: "2011-12-18T19:40:00"
uploadBy:
- "McDoofi"
license: "unknown"
imported:
- "2019"
---
Die Frage kann ich klären:

Das Modell schließt die Fernbedienung ein, das ist korrekt. Dann fragt es dich ob du die Funksteckdose Ein- oder Ausschalten willst, öffnet die Türe, schaltet das Förderband ein, fährt sie bis zur Rampe und drückt da dann per Zylinder den entsprechenden Knopf. Danach bitte dich ROBOPro innerhalb der nächsten 10 Sekunden die Fernbedienung wieder in den Tresor zu legen, da sich dieser dann schließt. Das Programm befindet sich übrigens in Downloads, ROBOPro.

Gruß McDoofi