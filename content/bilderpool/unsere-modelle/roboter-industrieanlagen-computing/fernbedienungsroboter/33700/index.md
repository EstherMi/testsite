---
layout: "image"
title: "Tresor"
date: "2011-12-18T10:29:46"
picture: "PC170603.jpg"
weight: "5"
konstrukteure: 
- "Christopher Kepes"
fotografen:
- "Christopher Kepes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "McDoofi"
license: "unknown"
legacy_id:
- details/33700
imported:
- "2019"
_4images_image_id: "33700"
_4images_cat_id: "2492"
_4images_user_id: "1401"
_4images_image_date: "2011-12-18T10:29:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33700 -->
Der Fernbedienugstresor