---
layout: "image"
title: "e-buc Bild 5"
date: "2011-10-24T21:37:28"
picture: "ebuc5.jpg"
weight: "17"
konstrukteure: 
- "Lennie"
fotografen:
- "Lennie"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- details/33320
imported:
- "2019"
_4images_image_id: "33320"
_4images_cat_id: "2467"
_4images_user_id: "592"
_4images_image_date: "2011-10-24T21:37:28"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33320 -->
Das Z30 ist direkt mit dem Deckel verbunden, das Z10 des Motors dreht das Z30. Der Deckel hat nach oben hin keinen Anschlag, der Cubesolver drohte bei früheren Versionen mit Anschlag immer mal dazu, sich zu zerlegen.