---
layout: "image"
title: "e-buc Bild 1"
date: "2011-12-12T17:15:29"
picture: "ebuc01.jpg"
weight: "1"
konstrukteure: 
- "Lennie"
fotografen:
- "Lennie"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- details/33632
imported:
- "2019"
_4images_image_id: "33632"
_4images_cat_id: "2467"
_4images_user_id: "592"
_4images_image_date: "2011-12-12T17:15:29"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33632 -->
Ich habe noch ein bisschen am Aussehen des Cbesolvers e-buc gearbeitet, man sieht jetzt von vorne keine Motoren mehr und die Gesamtkonstruktion ist auch noch schmaler geworden.