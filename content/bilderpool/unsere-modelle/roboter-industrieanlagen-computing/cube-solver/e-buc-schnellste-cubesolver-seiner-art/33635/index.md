---
layout: "image"
title: "e-buc Bild 4"
date: "2011-12-12T17:15:29"
picture: "ebuc04.jpg"
weight: "4"
konstrukteure: 
- "Lennie"
fotografen:
- "Lennie"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- details/33635
imported:
- "2019"
_4images_image_id: "33635"
_4images_cat_id: "2467"
_4images_user_id: "592"
_4images_image_date: "2011-12-12T17:15:29"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33635 -->
Verbaut sind weiterhin 4 Motoren, drei 20:1 und ein 8:1 für den Drehkranz.
Außerdem 7 Taster, zwei je für den Deckel und jeden der Schieber und einen für den Drehkranz.