---
layout: "image"
title: "e-buc Bild 2"
date: "2011-12-12T17:15:29"
picture: "ebuc02.jpg"
weight: "2"
konstrukteure: 
- "Lennie"
fotografen:
- "Lennie"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- details/33633
imported:
- "2019"
_4images_image_id: "33633"
_4images_cat_id: "2467"
_4images_user_id: "592"
_4images_image_date: "2011-12-12T17:15:29"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33633 -->
