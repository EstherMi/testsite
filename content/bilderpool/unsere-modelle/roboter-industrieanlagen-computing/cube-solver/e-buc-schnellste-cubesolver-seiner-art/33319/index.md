---
layout: "image"
title: "e-buc Bild 4"
date: "2011-10-24T21:37:28"
picture: "ebuc4.jpg"
weight: "16"
konstrukteure: 
- "Lennie"
fotografen:
- "Lennie"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- details/33319
imported:
- "2019"
_4images_image_id: "33319"
_4images_cat_id: "2467"
_4images_user_id: "592"
_4images_image_date: "2011-10-24T21:37:28"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33319 -->
Der Drehkranz wird direkt von einem 8:1 Powermotor mit einer Kette angetrieben. Die Kette ist nicht stramm. Die 30er Plastikstange an dem Motor habe ich unten gekürzt und sie dient nur dazu, dass das Z10 nicht von der Motorwelle rutscht.
Der Antrieb ist schnell, aber nicht sehr kraftvoll. Bei geschlossenem Deckel und eingelegtem Würfel dreht er sich nur bei der RoboPro Geschwindigkeit 8, also höchste Stufe.