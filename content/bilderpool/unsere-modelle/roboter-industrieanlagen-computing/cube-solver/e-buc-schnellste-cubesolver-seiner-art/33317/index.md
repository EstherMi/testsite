---
layout: "image"
title: "e-buc Bild 2"
date: "2011-10-24T21:37:28"
picture: "ebuc2.jpg"
weight: "14"
konstrukteure: 
- "Lennie"
fotografen:
- "Lennie"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- details/33317
imported:
- "2019"
_4images_image_id: "33317"
_4images_cat_id: "2467"
_4images_user_id: "592"
_4images_image_date: "2011-10-24T21:37:28"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33317 -->
Verbaut habe ich:
3x 20:1 Powermotor
1x 8:1 Powermotor
4x Lampen
7x Minitaster
1x Robo I/O Extension Module
1x Logitech Webcam