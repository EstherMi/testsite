---
layout: "image"
title: "e-buc Bild 7"
date: "2011-12-12T17:15:29"
picture: "ebuc07.jpg"
weight: "7"
konstrukteure: 
- "Lennie"
fotografen:
- "Lennie"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- details/33638
imported:
- "2019"
_4images_image_id: "33638"
_4images_cat_id: "2467"
_4images_user_id: "592"
_4images_image_date: "2011-12-12T17:15:29"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33638 -->
Die rote Platte ist der mechanische Endpunkt für den Deckel. Der war ja vorher ganz außen, an dieser Stelle muss ich mich keine Gedanken mehr um Gummistopper oder ähnliches machen.