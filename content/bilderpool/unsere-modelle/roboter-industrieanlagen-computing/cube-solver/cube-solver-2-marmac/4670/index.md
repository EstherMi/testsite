---
layout: "image"
title: "ftcs 015"
date: "2005-08-26T17:57:06"
picture: "ftcs_015.JPG"
weight: "15"
konstrukteure: 
- "Markus Mack"
fotografen:
- "Markus Mack"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/4670
imported:
- "2019"
_4images_image_id: "4670"
_4images_cat_id: "378"
_4images_user_id: "5"
_4images_image_date: "2005-08-26T17:57:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4670 -->
Ein verstohlener Blick in die Mechanik beim Drehen einer Scheibe: Im Deckel klemmen die oberen zwei "Stockwerke" fest. Die Kotflügel sorgen dafür, dass der Deckel beim zugehen sich nicht verhakt.
Im Drehkranz hat der Würfel etwas spiel (wegen dem Kippen notwenig), dies wird aber durch die Ansteuerung des Motors für den Drehkranz ausgeglichen.

Der (doppelte) Baustein 5 am Drehkranz dient als Auslöser für die Taster, es gibt an jeder der 4 Seiten des Drehkranzes einen solchen.