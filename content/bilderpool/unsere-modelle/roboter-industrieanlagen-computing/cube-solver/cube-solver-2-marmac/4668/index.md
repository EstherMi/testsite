---
layout: "image"
title: "ftcs 013"
date: "2005-08-26T17:57:06"
picture: "ftcs_013.JPG"
weight: "13"
konstrukteure: 
- "Markus Mack"
fotografen:
- "Markus Mack"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/4668
imported:
- "2019"
_4images_image_id: "4668"
_4images_cat_id: "378"
_4images_user_id: "5"
_4images_image_date: "2005-08-26T17:57:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4668 -->
