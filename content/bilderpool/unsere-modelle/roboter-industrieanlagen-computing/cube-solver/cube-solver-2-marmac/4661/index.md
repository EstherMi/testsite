---
layout: "image"
title: "ftcs 006"
date: "2005-08-26T17:57:06"
picture: "ftcs_006.JPG"
weight: "6"
konstrukteure: 
- "Markus Mack"
fotografen:
- "Markus Mack"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/4661
imported:
- "2019"
_4images_image_id: "4661"
_4images_cat_id: "378"
_4images_user_id: "5"
_4images_image_date: "2005-08-26T17:57:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4661 -->
Der Powermotor für den Deckel. Da das Z30 direkt am Deckel befestigt ist geht das Öffnen in 0,5s, das Schließen noch schneller.

<hr>

This is the mechanism that enables the robot to open and shut the "lid". Opening is as fast as 0.5 seconds, slamming the lid  shut works even faster.