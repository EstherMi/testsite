---
layout: "image"
title: "ftcs 008"
date: "2005-08-26T17:57:06"
picture: "ftcs_008.JPG"
weight: "8"
konstrukteure: 
- "Markus Mack"
fotografen:
- "Markus Mack"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/4663
imported:
- "2019"
_4images_image_id: "4663"
_4images_cat_id: "378"
_4images_user_id: "5"
_4images_image_date: "2005-08-26T17:57:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4663 -->
