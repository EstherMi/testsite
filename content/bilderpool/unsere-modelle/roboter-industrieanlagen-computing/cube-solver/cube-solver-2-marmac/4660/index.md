---
layout: "image"
title: "ftcs 005"
date: "2005-08-26T17:57:06"
picture: "ftcs_005.JPG"
weight: "5"
konstrukteure: 
- "Markus Mack"
fotografen:
- "Markus Mack"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/4660
imported:
- "2019"
_4images_image_id: "4660"
_4images_cat_id: "378"
_4images_user_id: "5"
_4images_image_date: "2005-08-26T17:57:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4660 -->
Hier ist nochmal das kippen zu sehen. Zu beachten ist die Feder am rechten Schieber: Dieser wird, wie schön zu sehen ist, beim Drücken gegen den Würfel selbst nach oben gedrückt, wo die Feder dagegenwirkt und so den Würfel zwischen Drehscheibe und Schier etwas "einklemmt", so dass er nicht wegrutscht.

Die Feder darf aber nicht zu stark sein, sonst zieht der rechte Schieber beim zurückziehen auch die Würfeloberkante mit zurück, was dazu führen kann, dass dieser wieder zurückkippt.