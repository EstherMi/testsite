---
layout: "image"
title: "ftcs 003"
date: "2005-08-26T17:57:06"
picture: "ftcs_003.JPG"
weight: "3"
konstrukteure: 
- "Markus Mack"
fotografen:
- "Markus Mack"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/4658
imported:
- "2019"
_4images_image_id: "4658"
_4images_cat_id: "378"
_4images_user_id: "5"
_4images_image_date: "2005-08-26T17:57:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4658 -->
2 Lampen und 2 LDRs für die Farberkennung, der eine ist für Kantenwürfel, der andere für Eckwürfel zuständig. Für die Erkennung werden Helligkeitswerte gemessen. Die Werte, die die LDRs liefern, sind dank der Störlichtkappen sehr konstant, so dass bei meinen Tests in dieser Konfiguration nur recht selten Fehler auftreten.

Anscheinend ist so eine Verwendung von roten/grünen Farbkappen nicht zwingend erforderlich; vor allem würde die Farberkennung wegen mehrfachmessungen mit verschiedenen farben um einiges länger dauern.

Gemessen wird nur, wenn der Deckel zu ist; Dieser muss deswegen zu jeder Messung geschlossen und erst danach wieder geöffnet werden, um den Würfel für die Messung der nächsten Felder weiterzudrehen.

<hr>

Two light dependand resistors (LDRs, front, facing up) are being used to recognize the colors of the cube's faces: They measure the  light intensity coming from two small lamps (back, facing towards the camera)  that is being reflected off the faces.
On this picture, the "lid" is open; in order to do any measurements the robot would have to shut the lid first.