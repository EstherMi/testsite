---
layout: "image"
title: "ftcs 014"
date: "2005-08-26T17:57:06"
picture: "ftcs_014.JPG"
weight: "14"
konstrukteure: 
- "Markus Mack"
fotografen:
- "Markus Mack"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/4669
imported:
- "2019"
_4images_image_id: "4669"
_4images_cat_id: "378"
_4images_user_id: "5"
_4images_image_date: "2005-08-26T17:57:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4669 -->
Der Drehkranz. Zu beachten die Taster links und rechts: Diese sind etwas versetzt angeordnet, es wird kurz vor dem Erreichen der jeweiligen Endstellung ein Taster zu früh ausgelöst, worauf die Geschwindigkeit leicht gedrosselt wird. Der Kranz dreht danach absichtlich etwas zu weit (bis zum anderen Taster), da der Würfel im Drehkranz etwas Spiel hat und nur so die Scheibe sauber gedreht werden kann. Danach Steuert der Motor den Drehkranz wieder kurz zurück, bis der 2. taster nicht mehr gedrückt ist. So endet der Drehkranz in einer sauberen Mittelstellung zwischen den Tastern wie auf dem Bild zu sehen.

<hr>

The Turntable. In order to do a 90 degreee turn, the robot will wait until one of the two pushbuttons is being activated, will then slow down the motor until the second pushbutton  is being pressed which means that it actually turns slightly too far, resulting in the side ending up more properly aligned.