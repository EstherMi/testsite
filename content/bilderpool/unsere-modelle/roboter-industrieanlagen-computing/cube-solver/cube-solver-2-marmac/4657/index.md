---
layout: "image"
title: "ftcs 002"
date: "2005-08-26T17:57:06"
picture: "ftcs_002.JPG"
weight: "2"
konstrukteure: 
- "Markus Mack"
fotografen:
- "Markus Mack"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/4657
imported:
- "2019"
_4images_image_id: "4657"
_4images_cat_id: "378"
_4images_user_id: "5"
_4images_image_date: "2005-08-26T17:57:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4657 -->
Der Würfel liegt auf der Drehscheibe, der Deckel ist offen. So kann der ganze Würfel mit dem Powermotor unten am Bild gedreht werden.

<hr>

The motor, in conjunction with a worm gear drive (on the lower-left corner of the picture) is being used to rotate the turntable. If, like shown, the "lid" is open, the whole cube will be rotated.