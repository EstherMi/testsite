---
layout: "image"
title: "Cube Wender"
date: "2005-04-16T17:56:47"
picture: "CubeWender.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ROBOProEntwickler"
license: "unknown"
legacy_id:
- details/3972
imported:
- "2019"
_4images_image_id: "3972"
_4images_cat_id: "342"
_4images_user_id: "200"
_4images_image_date: "2005-04-16T17:56:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3972 -->
Der Würfel wird von 4 pneumatisch ein- und ausfahrbaren Armen festgehalten. Die Arme sind drehbar, so dass sie den Würfel auch wenden können. Da es zwi paar Arme gibt, kann in einem Zug jede beliebige Fläche zum drehen nach unten gewendet werden.