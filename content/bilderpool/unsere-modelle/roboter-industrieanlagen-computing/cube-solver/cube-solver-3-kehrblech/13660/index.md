---
layout: "image"
title: "Farbscannanlage"
date: "2008-02-17T07:55:26"
picture: "cubesolver5.jpg"
weight: "7"
konstrukteure: 
- "Jan Käberich"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- details/13660
imported:
- "2019"
_4images_image_id: "13660"
_4images_cat_id: "1253"
_4images_user_id: "521"
_4images_image_date: "2008-02-17T07:55:26"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13660 -->
Mit dieser Anlage scannt der Roboter die Farben. Die neue Version funktioniert deutlich besser als diese hier.