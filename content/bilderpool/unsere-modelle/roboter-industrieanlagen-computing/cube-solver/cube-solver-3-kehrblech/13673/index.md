---
layout: "image"
title: "Version 2"
date: "2008-02-17T19:31:02"
picture: "cubesolver1_2.jpg"
weight: "3"
konstrukteure: 
- "Jan Käberich"
fotografen:
- "Käberich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- details/13673
imported:
- "2019"
_4images_image_id: "13673"
_4images_cat_id: "1253"
_4images_user_id: "521"
_4images_image_date: "2008-02-17T19:31:02"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13673 -->
Das ist jetzt die neue Version des Roboters. Viel verändert hat sich nicht, nur die Schieber wurden verstärkt, die Laserlichtschranke umgebaut
und er hat noch eine Anzeige bekommen, mit der die Farbmessung überprüft werden kann und angezeigt wird wieviele Züge noch ausgeführt
werden müssen, bis der Würfel geordnet ist.