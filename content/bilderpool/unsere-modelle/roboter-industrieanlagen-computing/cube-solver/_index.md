---
layout: "overview"
title: "Cube Solver"
date: 2019-12-17T18:58:11+01:00
legacy_id:
- categories/585
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=585 --> 
Maschinen, die Rubic's Cube "auflösen" können, d.h. den Zauberwürfel wieder in Ausgangsposition zurückbringen, so dass alle Seiten einfarbig sind.