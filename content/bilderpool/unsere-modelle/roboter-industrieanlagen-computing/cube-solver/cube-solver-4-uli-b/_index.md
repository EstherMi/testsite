---
layout: "overview"
title: "------ Cube Solver 4 (Uli B)"
date: 2019-12-17T18:58:31+01:00
legacy_id:
- categories/2305
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2305 --> 
Sie finden hier den Nachbau eines Cube Solvers.
Nach ca. 15 Jahren fischertechnik-Abstinenz habe ich als Wiedereinstieg einen Cube Solver ausgewählt und nachgebaut.