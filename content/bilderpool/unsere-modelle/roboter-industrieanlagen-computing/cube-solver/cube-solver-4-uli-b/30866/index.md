---
layout: "image"
title: "Cube Solver - 5"
date: "2011-06-14T22:35:34"
picture: "CS_5.jpg"
weight: "5"
konstrukteure: 
- "Ulrich Blankenhorn"
fotografen:
- "Ulrich Blankenhorn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ulib"
license: "unknown"
legacy_id:
- details/30866
imported:
- "2019"
_4images_image_id: "30866"
_4images_cat_id: "2305"
_4images_user_id: "1330"
_4images_image_date: "2011-06-14T22:35:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30866 -->
Diesen Cube Solver in Aktion finden Sie bei YouTube unter
http://www.youtube.com/watch?v=W4estyD5r5U