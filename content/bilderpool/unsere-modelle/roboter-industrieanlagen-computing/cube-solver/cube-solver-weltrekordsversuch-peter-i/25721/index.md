---
layout: "image"
title: "2.-0.5 2/3"
date: "2009-11-07T20:23:48"
picture: "PICT0044.jpg"
weight: "35"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- details/25721
imported:
- "2019"
_4images_image_id: "25721"
_4images_cat_id: "1790"
_4images_user_id: "998"
_4images_image_date: "2009-11-07T20:23:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25721 -->
