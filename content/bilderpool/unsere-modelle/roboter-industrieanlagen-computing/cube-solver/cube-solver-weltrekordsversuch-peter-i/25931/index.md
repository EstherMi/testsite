---
layout: "image"
title: "Drehkranzmotorisierung"
date: "2009-12-11T23:27:16"
picture: "cubesolver16.jpg"
weight: "16"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- details/25931
imported:
- "2019"
_4images_image_id: "25931"
_4images_cat_id: "1790"
_4images_user_id: "998"
_4images_image_date: "2009-12-11T23:27:16"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25931 -->
Die 3 Motoren brauche ich, damit der Drehkranz immer gleich schnell ist