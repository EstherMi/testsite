---
layout: "image"
title: "Version 2.0 2/2"
date: "2009-11-16T20:40:40"
picture: "20_4_-_Kopie.jpg"
weight: "33"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
keywords: ["Cube", "Solver", "Zauberwürfel", "Peter", "Pötzi", "hman13"]
uploadBy: "hman13"
license: "unknown"
legacy_id:
- details/25779
imported:
- "2019"
_4images_image_id: "25779"
_4images_cat_id: "1790"
_4images_user_id: "1100"
_4images_image_date: "2009-11-16T20:40:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25779 -->
Das graue Kasterl ist ein 275W Netzteil