---
layout: "image"
title: "Würfel beim Drehen"
date: "2009-12-11T23:27:17"
picture: "cubesolver34.jpg"
weight: "28"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hman13"
license: "unknown"
legacy_id:
- details/25949
imported:
- "2019"
_4images_image_id: "25949"
_4images_cat_id: "1790"
_4images_user_id: "1100"
_4images_image_date: "2009-12-11T23:27:17"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25949 -->
So sieht der Würfel aus wenn man beim Drehen den Strom abdreht und ihn rausnimmt