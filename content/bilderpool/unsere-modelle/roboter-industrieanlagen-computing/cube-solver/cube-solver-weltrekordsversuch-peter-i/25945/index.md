---
layout: "image"
title: "Kippen V"
date: "2009-12-11T23:27:17"
picture: "cubesolver30.jpg"
weight: "24"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- details/25945
imported:
- "2019"
_4images_image_id: "25945"
_4images_cat_id: "1790"
_4images_user_id: "998"
_4images_image_date: "2009-12-11T23:27:17"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25945 -->
Der linke Schieber fährt vor und danach fährt der rechte hinter