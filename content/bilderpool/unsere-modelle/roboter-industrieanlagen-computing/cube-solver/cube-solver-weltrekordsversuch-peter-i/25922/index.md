---
layout: "image"
title: "Rechter Schieber"
date: "2009-12-11T23:27:06"
picture: "cubesolver07.jpg"
weight: "7"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- details/25922
imported:
- "2019"
_4images_image_id: "25922"
_4images_cat_id: "1790"
_4images_user_id: "998"
_4images_image_date: "2009-12-11T23:27:06"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25922 -->
hier sind mir einfach die schwarzen Teile ausgegangen