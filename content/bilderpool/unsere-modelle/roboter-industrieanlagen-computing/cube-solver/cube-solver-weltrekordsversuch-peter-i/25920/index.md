---
layout: "image"
title: "Hinteransicht"
date: "2009-12-11T23:27:06"
picture: "cubesolver05.jpg"
weight: "5"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- details/25920
imported:
- "2019"
_4images_image_id: "25920"
_4images_cat_id: "1790"
_4images_user_id: "998"
_4images_image_date: "2009-12-11T23:27:06"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25920 -->
Der Linke Schieber(hier ist er rechts, weil ich von hinten fotografiert habe) ist halb ausgefahren