---
layout: "image"
title: "Cube Solver 2.-0.5 1/3"
date: "2009-11-07T20:23:47"
picture: "PICT0043.jpg"
weight: "34"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- details/25720
imported:
- "2019"
_4images_image_id: "25720"
_4images_cat_id: "1790"
_4images_user_id: "998"
_4images_image_date: "2009-11-07T20:23:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25720 -->
mehr Infos unter <a href="http://peterp.brandlehner.at">http://peterp.brandlehner.at</a>