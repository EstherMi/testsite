---
layout: "image"
title: "Drehkranz"
date: "2009-12-11T23:27:16"
picture: "cubesolver11.jpg"
weight: "11"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- details/25926
imported:
- "2019"
_4images_image_id: "25926"
_4images_cat_id: "1790"
_4images_user_id: "998"
_4images_image_date: "2009-12-11T23:27:16"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25926 -->
Der Drehkranz wird nur mit 5V betrieben, da mit 12V der Taster nur ungefähr 10ms gedrückt wird und das fürs Interface zu schnell ist

Der linke Schieber(hier rechts) ist halb ausgefahren