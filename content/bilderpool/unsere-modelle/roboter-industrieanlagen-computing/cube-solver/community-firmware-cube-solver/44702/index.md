---
layout: "image"
title: "Pusher-Pneumatik"
date: "2016-10-30T12:19:35"
picture: "IMG_4876.jpg"
weight: "11"
konstrukteure: 
- "Till Harbaum"
fotografen:
- "Till Harbaum"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Till Harbaum"
license: "unknown"
legacy_id:
- details/44702
imported:
- "2019"
_4images_image_id: "44702"
_4images_cat_id: "3329"
_4images_user_id: "2656"
_4images_image_date: "2016-10-30T12:19:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44702 -->
Die beiden Pneumatikzylinder des Pushers