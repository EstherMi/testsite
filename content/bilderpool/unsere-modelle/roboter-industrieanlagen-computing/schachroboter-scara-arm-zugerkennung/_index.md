---
layout: "overview"
title: "Schachroboter mit Scara Arm und Zugerkennung durch Bildverarbeitung"
date: 2019-12-17T19:09:11+01:00
legacy_id:
- categories/3545
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3545 --> 
Dieser Fischertechnik Roboter spielt Schach gegen einen Menschen. Der Zug des Menschen wird mit einem Alientek Omnivision OV7670 Kamera-Modul und Bildverarbeitung auf dem ATMEGA2560 erkannt. Der AVR prüft die Gültigkeit des Zuges und führt einen Gegenzug aus. Dies wird über die Software AVR Max 4.8 erledigt, eine Portierung der Micro-Max Schachmaschine von Harm-Geert Muller durch Andre Adrian. Dieser Gegenzug wird dann durch einen mit Schrittmotoren betriebenen Scara Arm ausgeführt