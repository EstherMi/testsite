---
layout: "image"
title: "Basisarm mit Aluprofilen von Misumi"
date: "2018-11-12T20:50:21"
picture: "C4.jpg"
weight: "4"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Misumi", "Alu"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/48403
imported:
- "2019"
_4images_image_id: "48403"
_4images_cat_id: "3545"
_4images_user_id: "579"
_4images_image_date: "2018-11-12T20:50:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48403 -->
