---
layout: "image"
title: "Kompressor und Basisarm"
date: "2018-11-12T20:50:21"
picture: "C7.jpg"
weight: "7"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/48406
imported:
- "2019"
_4images_image_id: "48406"
_4images_cat_id: "3545"
_4images_user_id: "579"
_4images_image_date: "2018-11-12T20:50:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48406 -->
