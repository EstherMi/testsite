---
layout: "image"
title: "Robot Lock Picker"
date: "2008-07-15T22:12:00"
picture: "lockcracker_2.jpg"
weight: "25"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["robot", "lock", "picker"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/14836
imported:
- "2019"
_4images_image_id: "14836"
_4images_cat_id: "125"
_4images_user_id: "585"
_4images_image_date: "2008-07-15T22:12:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14836 -->
This is the first version of the Lock Picker. I used two 9 v motors, touch sensor, a pile of ft elements, and the PCS Programmable Brick with the PCS Robotics Contoller. 

***google translation: 	
Dies ist die erste Version des Lock Picker. Ich habe zwei 9 v Motoren, Touch-Sensor, einen Haufen von FT-Elemente, und die PCS Programmable Brick mit dem PCS Robotics Controller.