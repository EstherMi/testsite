---
layout: "image"
title: "ein Waggon"
date: "2006-11-21T18:08:52"
picture: "Coesfeld_044.jpg"
weight: "5"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/7534
imported:
- "2019"
_4images_image_id: "7534"
_4images_cat_id: "719"
_4images_user_id: "130"
_4images_image_date: "2006-11-21T18:08:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7534 -->
