---
layout: "image"
title: "Kompressor"
date: "2007-03-23T23:43:25"
picture: "159_5927.jpg"
weight: "40"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/9773
imported:
- "2019"
_4images_image_id: "9773"
_4images_cat_id: "711"
_4images_user_id: "34"
_4images_image_date: "2007-03-23T23:43:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9773 -->
Kompressor der sehr sehr leise ist!