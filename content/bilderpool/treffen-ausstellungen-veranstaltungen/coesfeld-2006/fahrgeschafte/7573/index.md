---
layout: "image"
title: "Ein Riesenrad"
date: "2006-11-22T19:06:40"
picture: "Coesfeld_118.jpg"
weight: "32"
konstrukteure: 
- "TST"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/7573
imported:
- "2019"
_4images_image_id: "7573"
_4images_cat_id: "721"
_4images_user_id: "130"
_4images_image_date: "2006-11-22T19:06:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7573 -->
