---
layout: "image"
title: "Ein Rummelplatzmodell"
date: "2006-11-22T19:06:40"
picture: "Coesfeld_107.jpg"
weight: "28"
konstrukteure: 
- "Fam. Lammering"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/7569
imported:
- "2019"
_4images_image_id: "7569"
_4images_cat_id: "721"
_4images_user_id: "130"
_4images_image_date: "2006-11-22T19:06:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7569 -->
