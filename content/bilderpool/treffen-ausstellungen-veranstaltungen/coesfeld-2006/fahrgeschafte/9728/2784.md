---
layout: "comment"
hidden: true
title: "2784"
date: "2007-03-24T14:55:15"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
@fishfriends: Wenn Du so viele Bilder hast, dann lade sie doch mit dem Publisher hoch. Der kann sie drehen (1 Klick) und alle zusammen hochladen. Drastisch einfacher als einzeln.

Gruß,
Stefan