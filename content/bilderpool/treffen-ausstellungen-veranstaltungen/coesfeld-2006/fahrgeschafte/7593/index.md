---
layout: "image"
title: "Kirmesmodell"
date: "2006-11-22T19:06:41"
picture: "Coesfeld_133.jpg"
weight: "34"
konstrukteure: 
- "Fam. Roller"
fotografen:
- "stephan's Frau"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/7593
imported:
- "2019"
_4images_image_id: "7593"
_4images_cat_id: "721"
_4images_user_id: "130"
_4images_image_date: "2006-11-22T19:06:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7593 -->
