---
layout: "image"
title: "Ikarus"
date: "2006-11-22T19:06:41"
picture: "Coesfeld_134.jpg"
weight: "2"
konstrukteure: 
- "Fam. Jansen"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/7585
imported:
- "2019"
_4images_image_id: "7585"
_4images_cat_id: "1295"
_4images_user_id: "130"
_4images_image_date: "2006-11-22T19:06:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7585 -->
