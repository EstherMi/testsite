---
layout: "image"
title: "Herr Jansen erklärt sein Modell"
date: "2006-11-22T19:06:41"
picture: "Coesfeld_135.jpg"
weight: "3"
konstrukteure: 
- "Fam. Jansen"
fotografen:
- "stephan's Frau"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/7586
imported:
- "2019"
_4images_image_id: "7586"
_4images_cat_id: "1295"
_4images_user_id: "130"
_4images_image_date: "2006-11-22T19:06:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7586 -->
