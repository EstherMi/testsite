---
layout: "image"
title: "Kranspitze"
date: "2006-12-20T21:50:26"
picture: "Coesfeld_130.jpg"
weight: "6"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/7582
imported:
- "2019"
_4images_image_id: "7582"
_4images_cat_id: "1296"
_4images_user_id: "130"
_4images_image_date: "2006-12-20T21:50:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7582 -->
