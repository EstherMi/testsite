---
layout: "image"
title: "COE"
date: "2006-12-20T21:50:26"
picture: "158_5849.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/8089
imported:
- "2019"
_4images_image_id: "8089"
_4images_cat_id: "1296"
_4images_user_id: "34"
_4images_image_date: "2006-12-20T21:50:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8089 -->
