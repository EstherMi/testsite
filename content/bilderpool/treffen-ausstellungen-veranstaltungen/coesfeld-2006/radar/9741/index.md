---
layout: "image"
title: "Detail 6"
date: "2007-03-23T23:43:25"
picture: "158_5892.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/9741
imported:
- "2019"
_4images_image_id: "9741"
_4images_cat_id: "1299"
_4images_user_id: "34"
_4images_image_date: "2007-03-23T23:43:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9741 -->
