---
layout: "image"
title: "Spielautomat von Ludger"
date: "2006-11-21T18:32:27"
picture: "Coesfeld_060.jpg"
weight: "9"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/7546
imported:
- "2019"
_4images_image_id: "7546"
_4images_cat_id: "720"
_4images_user_id: "130"
_4images_image_date: "2006-11-21T18:32:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7546 -->
... die Spannung steigt ...