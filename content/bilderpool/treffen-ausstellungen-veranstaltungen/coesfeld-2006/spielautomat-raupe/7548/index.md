---
layout: "image"
title: "Magazin mit Spielsteinen"
date: "2006-11-21T18:32:27"
picture: "Coesfeld_071.jpg"
weight: "11"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/7548
imported:
- "2019"
_4images_image_id: "7548"
_4images_cat_id: "720"
_4images_user_id: "130"
_4images_image_date: "2006-11-21T18:32:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7548 -->
