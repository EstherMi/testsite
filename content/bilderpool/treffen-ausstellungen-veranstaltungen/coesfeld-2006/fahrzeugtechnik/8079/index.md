---
layout: "image"
title: "COE 47"
date: "2006-12-20T21:50:26"
picture: "158_5834.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/8079
imported:
- "2019"
_4images_image_id: "8079"
_4images_cat_id: "1298"
_4images_user_id: "34"
_4images_image_date: "2006-12-20T21:50:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8079 -->
