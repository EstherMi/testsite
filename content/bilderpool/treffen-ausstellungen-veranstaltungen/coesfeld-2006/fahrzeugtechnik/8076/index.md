---
layout: "image"
title: "COE 44"
date: "2006-12-20T21:50:26"
picture: "158_5830.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/8076
imported:
- "2019"
_4images_image_id: "8076"
_4images_cat_id: "1298"
_4images_user_id: "34"
_4images_image_date: "2006-12-20T21:50:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8076 -->
