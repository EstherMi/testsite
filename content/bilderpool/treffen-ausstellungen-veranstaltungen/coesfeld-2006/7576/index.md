---
layout: "image"
title: "fliegendes Flugzeug"
date: "2006-11-22T19:06:40"
picture: "Coesfeld_121.jpg"
weight: "73"
konstrukteure: 
- "TST"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/7576
imported:
- "2019"
_4images_image_id: "7576"
_4images_cat_id: "711"
_4images_user_id: "130"
_4images_image_date: "2006-11-22T19:06:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7576 -->
