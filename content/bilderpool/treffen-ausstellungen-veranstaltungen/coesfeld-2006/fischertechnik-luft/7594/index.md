---
layout: "image"
title: "Fam. Jansen bestaunt ihr Modell"
date: "2006-11-22T19:06:41"
picture: "Coesfeld_143.jpg"
weight: "5"
konstrukteure: 
- "Fam. Jansen"
fotografen:
- "stephan\'s Frau"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/7594
imported:
- "2019"
_4images_image_id: "7594"
_4images_cat_id: "718"
_4images_user_id: "130"
_4images_image_date: "2006-11-22T19:06:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7594 -->
