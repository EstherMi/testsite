---
layout: "image"
title: "Dragster"
date: "2006-11-21T18:32:27"
picture: "Coesfeld_084.jpg"
weight: "84"
konstrukteure: 
- "Josef Lammering"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/7555
imported:
- "2019"
_4images_image_id: "7555"
_4images_cat_id: "711"
_4images_user_id: "130"
_4images_image_date: "2006-11-21T18:32:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7555 -->
