---
layout: "image"
title: "FT-Treffen Vlissingen-2008"
date: "2008-02-24T15:23:08"
picture: "Fischertechnik-Vlissingen-2008_036.jpg"
weight: "42"
konstrukteure: 
- "NL-FT-Mitglied   Paul Niekerk"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/13774
imported:
- "2019"
_4images_image_id: "13774"
_4images_cat_id: "1265"
_4images_user_id: "22"
_4images_image_date: "2008-02-24T15:23:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13774 -->
