---
layout: "image"
title: "Wall-E"
date: "2016-03-10T20:29:35"
picture: "neumuenster16.jpg"
weight: "16"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/43048
imported:
- "2019"
_4images_image_id: "43048"
_4images_cat_id: "3200"
_4images_user_id: "2303"
_4images_image_date: "2016-03-10T20:29:35"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43048 -->
Wall-E im Schlafmodus.