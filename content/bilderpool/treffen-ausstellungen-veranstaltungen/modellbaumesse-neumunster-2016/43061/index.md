---
layout: "image"
title: "Kegelbahn"
date: "2016-03-10T20:29:35"
picture: "neumuenster29.jpg"
weight: "29"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/43061
imported:
- "2019"
_4images_image_id: "43061"
_4images_cat_id: "3200"
_4images_user_id: "2303"
_4images_image_date: "2016-03-10T20:29:35"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43061 -->
Ansteuerung komplette Kegelbahn über einen RoboTX Controller mit I2C 2x PCF8574 und 8-Kanal Relais 5V.