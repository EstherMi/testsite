---
layout: "overview"
title: "Modellbaumesse Neumünster 2016"
date: 2019-12-17T18:34:29+01:00
legacy_id:
- categories/3200
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3200 --> 
Hallo fischertechnik-Freunde,

hier die Bilder zur ersten fischertechnik-Ausstellung vom 05.und 06. März 2016  im hohen Norden
in Schleswig-Holstein. Wir haben guten Publikumsververkehr bekommen. 

Wir hatten regen Zuspruch und haben den einen oder anderen Besucher begeistern können. 

Unsrere Mitmachaktionen kamen gut beim Publikum an. Wie z.B. Freundschaftsbänder stricken,
Kegeln, Roboter und Kran steuern, etc.

