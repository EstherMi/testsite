---
layout: "image"
title: "Laser-Plotter"
date: "2016-03-10T20:29:35"
picture: "neumuenster19.jpg"
weight: "19"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/43051
imported:
- "2019"
_4images_image_id: "43051"
_4images_cat_id: "3200"
_4images_user_id: "2303"
_4images_image_date: "2016-03-10T20:29:35"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43051 -->
Der Laser verwendet eine in Excel konvertierte HPGL Datei aus Corel Draw.