---
layout: "image"
title: "Kegelbahn"
date: "2016-03-10T20:29:35"
picture: "neumuenster27.jpg"
weight: "27"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/43059
imported:
- "2019"
_4images_image_id: "43059"
_4images_cat_id: "3200"
_4images_user_id: "2303"
_4images_image_date: "2016-03-10T20:29:35"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43059 -->
Das Display gibt die Anzahl Würfe, Punkte und die Summe der Würfe aus.

Beispiel:
Wurf Punkt Sum
2/3  -   5   - 12