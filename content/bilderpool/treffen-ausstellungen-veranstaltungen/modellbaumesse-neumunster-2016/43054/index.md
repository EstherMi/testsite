---
layout: "image"
title: "Kegelbahn"
date: "2016-03-10T20:29:35"
picture: "neumuenster22.jpg"
weight: "22"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/43054
imported:
- "2019"
_4images_image_id: "43054"
_4images_cat_id: "3200"
_4images_user_id: "2303"
_4images_image_date: "2016-03-10T20:29:35"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43054 -->
Oben 9 Leuchtsteine als Anzeige. Darunter Display LCD1602 als Textanzeige.
Pudelrinne links und rechts und links die Kugelzuführung.