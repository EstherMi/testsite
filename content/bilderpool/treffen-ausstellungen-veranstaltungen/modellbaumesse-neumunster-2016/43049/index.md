---
layout: "image"
title: "Laser-Plotter + Kegelbahn"
date: "2016-03-10T20:29:35"
picture: "neumuenster17.jpg"
weight: "17"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/43049
imported:
- "2019"
_4images_image_id: "43049"
_4images_cat_id: "3200"
_4images_user_id: "2303"
_4images_image_date: "2016-03-10T20:29:35"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43049 -->
