---
layout: "image"
title: "Baukran, Wall-E"
date: "2016-03-10T20:29:35"
picture: "neumuenster12.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/43044
imported:
- "2019"
_4images_image_id: "43044"
_4images_cat_id: "3200"
_4images_user_id: "2303"
_4images_image_date: "2016-03-10T20:29:35"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43044 -->
