---
layout: "image"
title: "Flechtmaschine"
date: "2016-03-10T20:29:35"
picture: "neumuenster40.jpg"
weight: "40"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/43072
imported:
- "2019"
_4images_image_id: "43072"
_4images_cat_id: "3200"
_4images_user_id: "2303"
_4images_image_date: "2016-03-10T20:29:35"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43072 -->
