---
layout: "comment"
hidden: true
title: "21791"
date: "2016-03-11T16:28:28"
uploadBy:
- "DirkW"
license: "unknown"
imported:
- "2019"
---
Stimmt, der Laser verbrennt die Pappe. Je nach Vorschub wird stärker bzw.
schwächer eingebrannt.

Gruß
Dirk