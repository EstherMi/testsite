---
layout: "image"
title: "Kegelbahn"
date: "2016-03-10T20:29:35"
picture: "neumuenster30.jpg"
weight: "30"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/43062
imported:
- "2019"
_4images_image_id: "43062"
_4images_cat_id: "3200"
_4images_user_id: "2303"
_4images_image_date: "2016-03-10T20:29:35"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43062 -->
Seilrollen zum heben und senken der Kegel.

Die Kugeln werden über Kette mit Magnet geholt und auf der blauen Flexschine
abgelegt, diese führt nach vorne.