---
layout: "image"
title: "Kegelbahn"
date: "2016-03-10T20:29:35"
picture: "neumuenster25.jpg"
weight: "25"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/43057
imported:
- "2019"
_4images_image_id: "43057"
_4images_cat_id: "3200"
_4images_user_id: "2303"
_4images_image_date: "2016-03-10T20:29:35"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43057 -->
Kugelrücklauf. Gebogene Flexschiene. Mit Schwung laufen
die Kugeln in die Auffangrinne.