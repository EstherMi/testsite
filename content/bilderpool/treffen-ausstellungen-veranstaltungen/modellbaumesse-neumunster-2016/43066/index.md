---
layout: "image"
title: "Ralf in Action"
date: "2016-03-10T20:29:35"
picture: "neumuenster34.jpg"
weight: "34"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/43066
imported:
- "2019"
_4images_image_id: "43066"
_4images_cat_id: "3200"
_4images_user_id: "2303"
_4images_image_date: "2016-03-10T20:29:35"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43066 -->
