---
layout: "image"
title: "sps ipc drives 2005 032"
date: "2005-11-24T14:29:45"
picture: "sps_ipc_drives_2005_032.jpg"
weight: "20"
konstrukteure: 
- "Staudinger"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/5389
imported:
- "2019"
_4images_image_id: "5389"
_4images_cat_id: "591"
_4images_user_id: "1"
_4images_image_date: "2005-11-24T14:29:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5389 -->
