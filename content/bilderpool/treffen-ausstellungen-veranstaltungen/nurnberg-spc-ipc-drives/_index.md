---
layout: "overview"
title: "Nürnberg, SPC/IPC Drives"
date: 2019-12-17T18:18:08+01:00
legacy_id:
- categories/7
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=7 --> 
Alljährlich findet in Nürnberg eine Fachmesse über Speicherprogrammierbare Steuerungen statt, wo fischertechnik-Modelle zur Demonstration gezeigt werden. Lothar Vogt, der selbst Kuka-Roboter programmiert, hat uns seine Fotos der Messe zur Verfügung gestellt.