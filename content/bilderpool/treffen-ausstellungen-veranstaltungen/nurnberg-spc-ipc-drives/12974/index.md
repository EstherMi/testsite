---
layout: "image"
title: "25"
date: "2007-12-01T09:11:01"
picture: "Neuer_Ordner_025.jpg"
weight: "25"
konstrukteure: 
- "-?-"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- details/12974
imported:
- "2019"
_4images_image_id: "12974"
_4images_cat_id: "7"
_4images_user_id: "473"
_4images_image_date: "2007-12-01T09:11:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12974 -->
SPS/IPC Drives Nürnberg 2007