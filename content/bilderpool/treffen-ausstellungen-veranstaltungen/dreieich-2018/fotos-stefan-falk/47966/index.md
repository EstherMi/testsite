---
layout: "image"
title: "Plotter, Robotergreifer, Fahrzeuge"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention047.jpg"
weight: "47"
konstrukteure: 
- "Carel van Leeuwen"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47966
imported:
- "2019"
_4images_image_id: "47966"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "47"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47966 -->
