---
layout: "image"
title: "Große Kugelbahn"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention137.jpg"
weight: "137"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48056
imported:
- "2019"
_4images_image_id: "48056"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "137"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48056 -->
Bestimmt mit vielen technischen Details, die die Fotos alle nicht zeigen.