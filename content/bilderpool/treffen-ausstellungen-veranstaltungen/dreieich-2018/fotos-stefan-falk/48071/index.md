---
layout: "image"
title: "Rollende Kugel"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention152.jpg"
weight: "152"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48071
imported:
- "2019"
_4images_image_id: "48071"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "152"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48071 -->
Innerhalb der Kugel sitzt eine Bluetoothfernsteuerung, die einen Fahrmotor antreibt. Das schwere Gewicht unten in der Kugel kann dabei auch noch nach links und rechts verlagert werden, dass die Kugel sogar Kurven fahren kann.