---
layout: "image"
title: "Industrieanlage"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention098.jpg"
weight: "98"
konstrukteure: 
- "Tobias Brunk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48017
imported:
- "2019"
_4images_image_id: "48017"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "98"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48017 -->
Hier das Hochregallager davon