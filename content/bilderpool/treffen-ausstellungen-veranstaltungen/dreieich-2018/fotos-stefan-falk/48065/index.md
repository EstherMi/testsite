---
layout: "image"
title: "RGB-Spiel"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention146.jpg"
weight: "146"
konstrukteure: 
- "Techum"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48065
imported:
- "2019"
_4images_image_id: "48065"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "146"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48065 -->
Es werden unterschiedliche Farben erzeugt (rot, grün, blau mit Stärken zwischen 0 und 255). Man muss sich die angezeigte Farbe merken und so genau wie möglich durch Tippen "nachbauen". Je näher, desto besser!