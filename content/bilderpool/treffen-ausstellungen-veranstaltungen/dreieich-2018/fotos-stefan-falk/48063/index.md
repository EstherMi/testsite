---
layout: "image"
title: "Bausteingreifer"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention144.jpg"
weight: "144"
konstrukteure: 
- "Techum"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48063
imported:
- "2019"
_4images_image_id: "48063"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "144"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48063 -->
Offenbar Kameragesteuert?