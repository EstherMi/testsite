---
layout: "image"
title: "Digitalisierte Bau-Spiel-Bahn"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention045.jpg"
weight: "45"
konstrukteure: 
- "Paul van NIekerk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47964
imported:
- "2019"
_4images_image_id: "47964"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "45"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47964 -->
