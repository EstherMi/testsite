---
layout: "image"
title: "Hochregallager, Kugelbahn mit Trampolin"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention066.jpg"
weight: "66"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47985
imported:
- "2019"
_4images_image_id: "47985"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "66"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47985 -->
