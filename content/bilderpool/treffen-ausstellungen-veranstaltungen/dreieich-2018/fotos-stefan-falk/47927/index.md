---
layout: "image"
title: "Kleinstmotor"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention008.jpg"
weight: "8"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47927
imported:
- "2019"
_4images_image_id: "47927"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47927 -->
Wegen des senkrecht stehenden Magneten links ist der Miniatur-Elektromotor sogar selbstanalaufend! Rechts neben ihm ein Drehrichtungs-Gleichrichter: Egal, in welche Richtung sich der graue BS15 dreht, die nach hinten laufende Welle dreht sich immer gleich rum!