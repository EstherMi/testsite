---
layout: "image"
title: "Müllwagen"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention168.jpg"
weight: "168"
konstrukteure: 
- "Claus Ludwig"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48087
imported:
- "2019"
_4images_image_id: "48087"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "168"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48087 -->
