---
layout: "image"
title: "Klassik-Modelle Burg und Apollo-Rakete"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention065.jpg"
weight: "65"
konstrukteure: 
- "Franz Santjohanser"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47984
imported:
- "2019"
_4images_image_id: "47984"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "65"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47984 -->
