---
layout: "image"
title: "Kugelbahnmodell-Strecke"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention026.jpg"
weight: "26"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47945
imported:
- "2019"
_4images_image_id: "47945"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47945 -->
Die komplex verlaufende Kugelbahnstrecke wird in zwei Achsen bewegt, sodass die Kugel die komplette Bahn im Inneren der Kugel durchläuft.