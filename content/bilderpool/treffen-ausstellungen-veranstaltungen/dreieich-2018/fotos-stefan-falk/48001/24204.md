---
layout: "comment"
hidden: true
title: "24204"
date: "2018-09-27T22:46:58"
uploadBy:
- "bjoern"
license: "unknown"
imported:
- "2019"
---
Zum Zeitpunkt der Aufnahme kämpfte mein Sohn noch mit der Kugelzuführung. Etwas später hat uns Stefan dann mit neuen Federn ausgeholfen und die Konstruktion wurde angepasst.

So nebenbei ist das ein Flipper und kein Flißßer.

Auf jeden Fall gab es später noch eine fleissige Spieler. Auch wenn der natürlich bei weitem nicht so cool aussieht wie der Flipper von Dirk