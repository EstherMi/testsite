---
layout: "image"
title: "Tischtennisball-Weitergabe"
date: "2018-09-23T16:50:29"
picture: "fischertechnikconvention001.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47920
imported:
- "2019"
_4images_image_id: "47920"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:29"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47920 -->
