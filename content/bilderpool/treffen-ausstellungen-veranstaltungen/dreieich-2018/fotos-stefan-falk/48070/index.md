---
layout: "image"
title: "Baufahrzeug"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention151.jpg"
weight: "151"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48070
imported:
- "2019"
_4images_image_id: "48070"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "151"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48070 -->
