---
layout: "image"
title: "Zukünftiges Kirmesmodell"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention092.jpg"
weight: "92"
konstrukteure: 
- "Tobias Brunk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48011
imported:
- "2019"
_4images_image_id: "48011"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "92"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48011 -->
Die Gondeln sollen noch auf und ab wandern (siehe den Prospekt in der Hand; siehe nächstes auch Bild)