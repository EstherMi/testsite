---
layout: "image"
title: "Fahrzeug"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention148.jpg"
weight: "148"
konstrukteure: 
- "Techum"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48067
imported:
- "2019"
_4images_image_id: "48067"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "148"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48067 -->
Was das Ultraschallteil vorne macht, weiß ich leider nicht.