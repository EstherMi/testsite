---
layout: "image"
title: "Kameraüberwachte Fahrbahn"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention013.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47932
imported:
- "2019"
_4images_image_id: "47932"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47932 -->
Mensch fährt gegen Maschine auf der Carrerabahn. Kameras beobachten, wer besser fährt, und ein neuronales Netzwerk lernt draus, beim nächsten Mal noch besser zu fahren.