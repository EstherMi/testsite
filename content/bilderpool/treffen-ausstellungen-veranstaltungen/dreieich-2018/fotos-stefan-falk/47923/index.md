---
layout: "image"
title: "Wankelmotoren, Motor"
date: "2018-09-23T16:50:29"
picture: "fischertechnikconvention004.jpg"
weight: "4"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47923
imported:
- "2019"
_4images_image_id: "47923"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:29"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47923 -->
