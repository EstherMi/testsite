---
layout: "image"
title: "Noch ein 3D-Scanner"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention056.jpg"
weight: "56"
konstrukteure: 
- "Torsten Stuehn"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47975
imported:
- "2019"
_4images_image_id: "47975"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "56"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47975 -->
