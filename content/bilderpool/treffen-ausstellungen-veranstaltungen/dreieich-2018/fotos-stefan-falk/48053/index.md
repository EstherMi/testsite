---
layout: "image"
title: "Sprechender Roboter"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention134.jpg"
weight: "134"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48053
imported:
- "2019"
_4images_image_id: "48053"
_4images_cat_id: "3532"
_4images_user_id: "104"
_4images_image_date: "2018-09-23T16:50:30"
_4images_image_order: "134"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48053 -->
Im Roboter ist ein Bluetooth-Lautsprecher eingebaut, und so kann er vom Handy gesteuert z.B. "Guten Tag" sagen.