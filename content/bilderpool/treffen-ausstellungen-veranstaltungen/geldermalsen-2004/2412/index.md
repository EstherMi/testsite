---
layout: "image"
title: "eigenbau kran stareveld_1"
date: "2004-04-29T21:13:52"
picture: "eigenbau_kran_stareveld_1.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/2412
imported:
- "2019"
_4images_image_id: "2412"
_4images_cat_id: "224"
_4images_user_id: "144"
_4images_image_date: "2004-04-29T21:13:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2412 -->
Abgeleitet von der niemals gebauten MSG 100 (eine idee der firma Mammoet), hat Wim Stareveld seine eigene interpretation gebaut.