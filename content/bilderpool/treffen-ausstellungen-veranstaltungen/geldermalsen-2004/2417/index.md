---
layout: "image"
title: "manitowoc m21000_2"
date: "2004-04-29T21:13:52"
picture: "manitowoc_m21000_2.jpg"
weight: "22"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/2417
imported:
- "2019"
_4images_image_id: "2417"
_4images_cat_id: "224"
_4images_user_id: "144"
_4images_image_date: "2004-04-29T21:13:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2417 -->
Der Manitowoc nochmals, aber jetst von der seite.
Hinter der M21000 steht der Gottwald AK912 in maßstab 1:30. Der bauer: Anton Jansen.