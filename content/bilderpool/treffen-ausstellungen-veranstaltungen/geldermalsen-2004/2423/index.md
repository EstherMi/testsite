---
layout: "image"
title: "sammlung andires tieleman_1"
date: "2004-04-30T14:41:38"
picture: "sammlung_andires_tieleman_1.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/2423
imported:
- "2019"
_4images_image_id: "2423"
_4images_cat_id: "224"
_4images_user_id: "144"
_4images_image_date: "2004-04-30T14:41:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2423 -->
Andries Tieleman schlept immer sein pappie mit. Beide sind immer sehr hilfbereit, haben aber trotsdem eine menge modellen dabei.
Neuen, alten, modificierten, mit funk- oder infrarod fernsteuerung und eine menge ft 2004 kataloge.
Hoffentlich haben sich wieder leuten angemeldet für unseren Club.