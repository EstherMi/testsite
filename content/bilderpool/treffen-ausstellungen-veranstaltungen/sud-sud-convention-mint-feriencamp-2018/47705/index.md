---
layout: "image"
title: "Der Meister bei der Arbeit"
date: "2018-06-19T08:26:44"
picture: "mintka08.jpg"
weight: "8"
konstrukteure: 
- "ThanksForTheFish"
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/47705
imported:
- "2019"
_4images_image_id: "47705"
_4images_cat_id: "3519"
_4images_user_id: "1557"
_4images_image_date: "2018-06-19T08:26:44"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47705 -->
Jonglagekunst mit ft auf ganz hohem Niveau. Die Decke war gerade hoch genug dafür.