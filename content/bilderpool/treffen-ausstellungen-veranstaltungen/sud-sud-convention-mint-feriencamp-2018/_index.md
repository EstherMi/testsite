---
layout: "overview"
title: "Süd-Süd-Convention und MINT-Feriencamp Karlsruhe 2018"
date: 2019-12-17T18:36:33+01:00
legacy_id:
- categories/3519
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3519 --> 
Neben der Convention am 31.05.2018 fanden von 31.05. bis 02.06. in vier parallelen Tracks Workshops im Rahmen des MINT-Feriencamps 2018 statt.