---
layout: "image"
title: "Fußball-Roboter"
date: "2018-06-19T08:26:44"
picture: "mintka07.jpg"
weight: "7"
konstrukteure: 
- "Torsten Stuehn"
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/47704
imported:
- "2019"
_4images_image_id: "47704"
_4images_cat_id: "3519"
_4images_user_id: "1557"
_4images_image_date: "2018-06-19T08:26:44"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47704 -->
Gar nicht mal so unflott die kleinen Racker.