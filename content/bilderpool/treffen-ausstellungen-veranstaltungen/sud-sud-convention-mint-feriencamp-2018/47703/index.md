---
layout: "image"
title: "Fußball-Roboter"
date: "2018-06-19T08:26:44"
picture: "mintka06.jpg"
weight: "6"
konstrukteure: 
- "Torsten Stuehn"
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/47703
imported:
- "2019"
_4images_image_id: "47703"
_4images_cat_id: "3519"
_4images_user_id: "1557"
_4images_image_date: "2018-06-19T08:26:44"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47703 -->
Die Montage der Tore wurde nach der ersten Halbzeit erheblich verbessert. Vorher hat doch einer glatt das ganze Tor mitgenommen.