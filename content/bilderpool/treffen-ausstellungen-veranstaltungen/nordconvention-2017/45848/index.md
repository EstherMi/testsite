---
layout: "image"
title: "Kugelbahn"
date: "2017-05-15T12:07:39"
picture: "nordconvention38.jpg"
weight: "63"
konstrukteure: 
- "-?-"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/45848
imported:
- "2019"
_4images_image_id: "45848"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:39"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45848 -->
