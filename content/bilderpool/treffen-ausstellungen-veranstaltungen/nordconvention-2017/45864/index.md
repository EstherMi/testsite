---
layout: "image"
title: "ROBO Tx Explorer"
date: "2017-05-15T12:07:47"
picture: "nordconvention54.jpg"
weight: "79"
konstrukteure: 
- "Grau"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/45864
imported:
- "2019"
_4images_image_id: "45864"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:47"
_4images_image_order: "54"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45864 -->
