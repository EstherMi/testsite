---
layout: "image"
title: "Flipper 'Fluch der Karibik'"
date: "2017-05-15T12:07:35"
picture: "nordconvention22.jpg"
weight: "47"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/45832
imported:
- "2019"
_4images_image_id: "45832"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:35"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45832 -->
