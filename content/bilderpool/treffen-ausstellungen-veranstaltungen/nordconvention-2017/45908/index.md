---
layout: "image"
title: "Schachspiel aus ft"
date: "2017-05-17T16:39:47"
picture: "nordc21.jpg"
weight: "21"
konstrukteure: 
- "Ralf Geerken (Thanks for the fish)"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/45908
imported:
- "2019"
_4images_image_id: "45908"
_4images_cat_id: "3407"
_4images_user_id: "130"
_4images_image_date: "2017-05-17T16:39:47"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45908 -->
Klasse gebaut