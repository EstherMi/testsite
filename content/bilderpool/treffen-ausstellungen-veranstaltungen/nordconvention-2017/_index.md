---
layout: "overview"
title: "Nordconvention 2017"
date: 2019-12-17T18:35:17+01:00
legacy_id:
- categories/3407
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3407 --> 
Am 13.05.2017 fand die erste fischertechnik Nordconvention im Forum des Schulzentrum Mellendorf, Fritz-Sennheiser-Platz 2-3 in 30900 Wedemark-Mellendorf statt. 
Die Veranstaltung wurde organisiert von der ftcommunity in Kooperation mit dem Richardt-Brand-Heimatmuseumsverein.
Unterstützt wurde die Ausstellung von fischerfriendman mit einem Ausstellerfrühstück. Vielen Dank dafür.

Um 10:00 Uhr ging es los. Gezeigt wurden die unterschiedlichsten Modelle. Von Großmodellen, wie der 4,50 mHängebrücke bis hin zum 3-D Drucker, Flipper oder 
einer Tischtennisweitergabemaschine. Es war für jeden Besucher etwas Interessantes dabei. Um 17:00 Uhr wurde abgebaut und in gemütlicher Runde bei Ralf gegrillt
und gefachsimpelt.

Wir danken auch den Ausstellern für die tollen Modelle und die tolle Zusammenarbeit beim Auf- und Abbau.
Eine rundum gelugende Ausstellung. 

Grüße Dirk Wölffel und Ralf Geerken
