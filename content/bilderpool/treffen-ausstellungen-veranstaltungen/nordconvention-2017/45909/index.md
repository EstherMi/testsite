---
layout: "image"
title: "Lokomobil von Stephan"
date: "2017-05-17T16:39:47"
picture: "nordc22.jpg"
weight: "22"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/45909
imported:
- "2019"
_4images_image_id: "45909"
_4images_cat_id: "3407"
_4images_user_id: "130"
_4images_image_date: "2017-05-17T16:39:47"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45909 -->
