---
layout: "image"
title: "Technikgeschichte mit fischertechnik"
date: "2017-05-15T12:07:39"
picture: "nordconvention32.jpg"
weight: "57"
konstrukteure: 
- "Dirk F und Thomas P "
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/45842
imported:
- "2019"
_4images_image_id: "45842"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:39"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45842 -->
