---
layout: "image"
title: "Atomium Modell"
date: "2017-05-17T15:53:45"
picture: "nordc01.jpg"
weight: "1"
konstrukteure: 
- "Rob van Baal"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/45888
imported:
- "2019"
_4images_image_id: "45888"
_4images_cat_id: "3407"
_4images_user_id: "130"
_4images_image_date: "2017-05-17T15:53:45"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45888 -->
Das schöne Atomiummodell von Rob van Baal