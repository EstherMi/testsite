---
layout: "image"
title: "Müllwagen"
date: "2017-05-15T12:07:43"
picture: "nordconvention45.jpg"
weight: "70"
konstrukteure: 
- "Claus"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/45855
imported:
- "2019"
_4images_image_id: "45855"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:43"
_4images_image_order: "45"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45855 -->
