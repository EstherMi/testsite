---
layout: "image"
title: "Draufsicht aufs Sägewerk mit dem tollen Schornstein"
date: "2017-05-17T16:39:47"
picture: "nordc15.jpg"
weight: "15"
konstrukteure: 
- "Ingwer Varstens"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/45902
imported:
- "2019"
_4images_image_id: "45902"
_4images_cat_id: "3407"
_4images_user_id: "130"
_4images_image_date: "2017-05-17T16:39:47"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45902 -->
