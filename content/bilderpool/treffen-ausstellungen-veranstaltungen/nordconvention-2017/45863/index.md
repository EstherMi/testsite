---
layout: "image"
title: "Karussell"
date: "2017-05-15T12:07:47"
picture: "nordconvention53.jpg"
weight: "78"
konstrukteure: 
- "-?-"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/45863
imported:
- "2019"
_4images_image_id: "45863"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:47"
_4images_image_order: "53"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45863 -->
