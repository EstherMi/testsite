---
layout: "image"
title: "WALL-E"
date: "2017-05-15T12:07:39"
picture: "nordconvention37.jpg"
weight: "62"
konstrukteure: 
- "Ralf"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/45847
imported:
- "2019"
_4images_image_id: "45847"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:39"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45847 -->
