---
layout: "image"
title: "Alte Kästen"
date: "2017-05-15T12:07:47"
picture: "nordconvention56.jpg"
weight: "81"
konstrukteure: 
- "Ralf"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/45866
imported:
- "2019"
_4images_image_id: "45866"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:47"
_4images_image_order: "56"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45866 -->
