---
layout: "image"
title: "Riesenrad"
date: "2017-05-17T16:39:47"
picture: "nordc24.jpg"
weight: "24"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/45911
imported:
- "2019"
_4images_image_id: "45911"
_4images_cat_id: "3407"
_4images_user_id: "130"
_4images_image_date: "2017-05-17T16:39:47"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45911 -->
Leider nicht ganz fertig geworden aber hat sich gedreht, wenn auch nur von Hand