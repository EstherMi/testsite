---
layout: "image"
title: "Kugelbahn"
date: "2017-05-15T12:07:47"
picture: "nordconvention59.jpg"
weight: "84"
konstrukteure: 
- "Martin"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/45869
imported:
- "2019"
_4images_image_id: "45869"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:47"
_4images_image_order: "59"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45869 -->
