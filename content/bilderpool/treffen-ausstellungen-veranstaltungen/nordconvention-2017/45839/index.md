---
layout: "image"
title: "3-D Druck Handyhalter aus thingiverse"
date: "2017-05-15T12:07:36"
picture: "nordconvention29.jpg"
weight: "54"
konstrukteure: 
- "thingiverse"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/45839
imported:
- "2019"
_4images_image_id: "45839"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:36"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45839 -->
