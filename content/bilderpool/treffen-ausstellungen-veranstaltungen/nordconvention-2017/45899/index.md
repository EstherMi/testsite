---
layout: "image"
title: "Das Dach vom Sägewerk"
date: "2017-05-17T16:39:47"
picture: "nordc12.jpg"
weight: "12"
konstrukteure: 
- "Ingwer Carstens"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/45899
imported:
- "2019"
_4images_image_id: "45899"
_4images_cat_id: "3407"
_4images_user_id: "130"
_4images_image_date: "2017-05-17T16:39:47"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45899 -->
