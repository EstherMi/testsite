---
layout: "image"
title: "Sägewerk  Riemenantrieb"
date: "2017-05-15T12:07:32"
picture: "nordconvention14.jpg"
weight: "39"
konstrukteure: 
- "Ingwer"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/45824
imported:
- "2019"
_4images_image_id: "45824"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:32"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45824 -->
