---
layout: "image"
title: "Schnecke"
date: "2017-05-15T12:07:51"
picture: "nordconvention62.jpg"
weight: "87"
konstrukteure: 
- "Ingwer"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/45872
imported:
- "2019"
_4images_image_id: "45872"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:51"
_4images_image_order: "62"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45872 -->
