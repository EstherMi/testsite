---
layout: "image"
title: "fischertechnik Keller"
date: "2017-05-15T12:07:55"
picture: "nordconvention74.jpg"
weight: "99"
konstrukteure: 
- "Martin und Stephan"
fotografen:
- "Silke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/45884
imported:
- "2019"
_4images_image_id: "45884"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:55"
_4images_image_order: "74"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45884 -->
