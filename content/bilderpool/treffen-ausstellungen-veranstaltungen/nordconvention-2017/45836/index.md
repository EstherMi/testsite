---
layout: "image"
title: "3-D Drucker"
date: "2017-05-15T12:07:36"
picture: "nordconvention26.jpg"
weight: "51"
konstrukteure: 
- "Dirk +Ralf"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/45836
imported:
- "2019"
_4images_image_id: "45836"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:36"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45836 -->
3-D Drucker mit 3-D Print Control und community Firmware.