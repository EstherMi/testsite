---
layout: "image"
title: "Aufbau Riesenrad 2,30 m"
date: "2017-05-15T12:07:26"
picture: "nordconvention04.jpg"
weight: "29"
konstrukteure: 
- "Stephan"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/45814
imported:
- "2019"
_4images_image_id: "45814"
_4images_cat_id: "3407"
_4images_user_id: "2303"
_4images_image_date: "2017-05-15T12:07:26"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45814 -->
