---
layout: "image"
title: "Noch eine Seitenkugel"
date: "2017-05-17T15:53:45"
picture: "nordc07.jpg"
weight: "7"
konstrukteure: 
- "Rob van Baal"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/45894
imported:
- "2019"
_4images_image_id: "45894"
_4images_cat_id: "3407"
_4images_user_id: "130"
_4images_image_date: "2017-05-17T15:53:45"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45894 -->
