---
layout: "image"
title: "Überblick 2"
date: "2015-09-23T09:52:30"
picture: "strassenfest07.jpg"
weight: "7"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- details/41900
imported:
- "2019"
_4images_image_id: "41900"
_4images_cat_id: "3116"
_4images_user_id: "381"
_4images_image_date: "2015-09-23T09:52:30"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41900 -->
