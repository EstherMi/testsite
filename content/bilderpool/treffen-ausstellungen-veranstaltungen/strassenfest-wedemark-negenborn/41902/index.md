---
layout: "image"
title: "Schach"
date: "2015-09-23T09:52:30"
picture: "strassenfest09.jpg"
weight: "9"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- details/41902
imported:
- "2019"
_4images_image_id: "41902"
_4images_cat_id: "3116"
_4images_user_id: "381"
_4images_image_date: "2015-09-23T09:52:30"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41902 -->
Hiermit kann endlich mal gespielt werden.