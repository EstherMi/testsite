---
layout: "image"
title: "Der Glasmurmelkiller"
date: "2015-09-23T09:52:30"
picture: "strassenfest08.jpg"
weight: "8"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- details/41901
imported:
- "2019"
_4images_image_id: "41901"
_4images_cat_id: "3116"
_4images_user_id: "381"
_4images_image_date: "2015-09-23T09:52:30"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41901 -->
Bitte kommt selbst drauf wie hier ausschließlich Metallkugeln an den Metallachsen entlanglaufen können.