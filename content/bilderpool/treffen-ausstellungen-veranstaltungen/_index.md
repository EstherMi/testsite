---
layout: "overview"
title: "Treffen, Ausstellungen, Veranstaltungen"
date: 2019-12-17T18:10:58+01:00
legacy_id:
- categories/1301
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1301 --> 
Immer öfter finden Treffen von fischertechnik-Fans statt. Jeder bringt sein Modell mit, es wird gefachsimpelt, und natürlich auch fotografiert. Die Fotos gibt's hier.