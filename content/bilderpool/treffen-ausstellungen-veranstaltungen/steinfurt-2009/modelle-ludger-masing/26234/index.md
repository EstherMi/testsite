---
layout: "image"
title: "Hier darf gespielt werden"
date: "2010-02-07T14:25:18"
picture: "DSCN3162.jpg"
weight: "3"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/26234
imported:
- "2019"
_4images_image_id: "26234"
_4images_cat_id: "1867"
_4images_user_id: "184"
_4images_image_date: "2010-02-07T14:25:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26234 -->
