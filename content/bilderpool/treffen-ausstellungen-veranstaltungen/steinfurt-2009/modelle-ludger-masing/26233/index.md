---
layout: "image"
title: "?"
date: "2010-02-07T14:25:18"
picture: "DSCN3161.jpg"
weight: "2"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/26233
imported:
- "2019"
_4images_image_id: "26233"
_4images_cat_id: "1867"
_4images_user_id: "184"
_4images_image_date: "2010-02-07T14:25:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26233 -->
