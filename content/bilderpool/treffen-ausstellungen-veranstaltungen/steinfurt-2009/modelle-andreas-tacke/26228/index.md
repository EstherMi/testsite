---
layout: "image"
title: "Modelle"
date: "2010-02-07T14:25:17"
picture: "DSCN3157.jpg"
weight: "4"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/26228
imported:
- "2019"
_4images_image_id: "26228"
_4images_cat_id: "1866"
_4images_user_id: "184"
_4images_image_date: "2010-02-07T14:25:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26228 -->
