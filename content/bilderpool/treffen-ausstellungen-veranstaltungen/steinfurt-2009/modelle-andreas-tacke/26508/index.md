---
layout: "image"
title: "Service Station"
date: "2010-02-22T20:23:47"
picture: "S6002666_verkleinert.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Bernhard Lehner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Bernhard Lehner"
license: "unknown"
legacy_id:
- details/26508
imported:
- "2019"
_4images_image_id: "26508"
_4images_cat_id: "1866"
_4images_user_id: "1028"
_4images_image_date: "2010-02-22T20:23:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26508 -->
