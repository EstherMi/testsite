---
layout: "image"
title: "ferngesteuerter Putzroboter"
date: "2009-12-03T17:11:08"
picture: "S6002669_verkleinert.jpg"
weight: "2"
konstrukteure: 
- "Bernhard Lehner"
fotografen:
- "Birgitta Lehner"
keywords: ["Bernhard", "Lehner", "Putzroboter", "fernsteuern"]
uploadBy: "Bernhard Lehner"
license: "unknown"
legacy_id:
- details/25885
imported:
- "2019"
_4images_image_id: "25885"
_4images_cat_id: "1821"
_4images_user_id: "1028"
_4images_image_date: "2009-12-03T17:11:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25885 -->
Ich steuere meinen Putzroboter durch die Ausstellung.