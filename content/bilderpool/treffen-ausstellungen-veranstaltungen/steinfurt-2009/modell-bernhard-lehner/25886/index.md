---
layout: "image"
title: "Ich führe meinen Putzroboter auf dem Boden vor."
date: "2009-12-04T20:34:51"
picture: "S6002677_verkleinert.jpg"
weight: "1"
konstrukteure: 
- "Bernhard Lehner"
fotografen:
- "Birgitta Lehner"
keywords: ["Putzroboter", "Bernhard", "Lehner", "Boden", "vorführen"]
uploadBy: "Bernhard Lehner"
license: "unknown"
legacy_id:
- details/25886
imported:
- "2019"
_4images_image_id: "25886"
_4images_cat_id: "1821"
_4images_user_id: "1028"
_4images_image_date: "2009-12-04T20:34:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25886 -->
