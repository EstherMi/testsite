---
layout: "image"
title: "Turm"
date: "2010-02-07T14:25:17"
picture: "DSCN3146.jpg"
weight: "4"
konstrukteure: 
- "Rob van Oostenbrugge"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/26223
imported:
- "2019"
_4images_image_id: "26223"
_4images_cat_id: "1864"
_4images_user_id: "184"
_4images_image_date: "2010-02-07T14:25:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26223 -->
