---
layout: "image"
title: "Tänzer"
date: "2010-02-07T14:25:17"
picture: "DSCN3136.jpg"
weight: "5"
konstrukteure: 
- "Rob van Oostenbrugge"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/26224
imported:
- "2019"
_4images_image_id: "26224"
_4images_cat_id: "1864"
_4images_user_id: "184"
_4images_image_date: "2010-02-07T14:25:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26224 -->
