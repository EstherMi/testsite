---
layout: "image"
title: "Schloß Zapfenstein"
date: "2016-10-01T15:02:07"
picture: "modellevonfamiliebusch4.jpg"
weight: "18"
konstrukteure: 
- "Franz Santjohanser"
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/44483
imported:
- "2019"
_4images_image_id: "44483"
_4images_cat_id: "3281"
_4images_user_id: "1557"
_4images_image_date: "2016-10-01T15:02:07"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44483 -->
Imposant. Neuschwanstein kann einpacken.