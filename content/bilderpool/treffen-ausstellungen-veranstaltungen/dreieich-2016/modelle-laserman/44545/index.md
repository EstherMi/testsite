---
layout: "image"
title: "Gegenläufig drehende Räder"
date: "2016-10-03T19:55:15"
picture: "modellevonlaserman1.jpg"
weight: "1"
konstrukteure: 
- "laserman"
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/44545
imported:
- "2019"
_4images_image_id: "44545"
_4images_cat_id: "3288"
_4images_user_id: "1557"
_4images_image_date: "2016-10-03T19:55:15"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44545 -->
Sehr dekorativ, entspannend, hypnotisch und etwas schwindelerregend anzusehen.