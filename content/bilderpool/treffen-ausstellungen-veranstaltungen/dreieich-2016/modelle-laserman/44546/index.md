---
layout: "image"
title: "Wellenmaschine"
date: "2016-10-03T19:55:15"
picture: "modellevonlaserman2.jpg"
weight: "2"
konstrukteure: 
- "laserman"
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/44546
imported:
- "2019"
_4images_image_id: "44546"
_4images_cat_id: "3288"
_4images_user_id: "1557"
_4images_image_date: "2016-10-03T19:55:15"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44546 -->
Auch hier liegt der Reiz in der Bewegung.