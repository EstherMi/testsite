---
layout: "image"
title: "Getriebe"
date: "2016-10-03T10:59:01"
picture: "geometer2.jpg"
weight: "2"
konstrukteure: 
- "Geometer"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/44541
imported:
- "2019"
_4images_image_id: "44541"
_4images_cat_id: "3311"
_4images_user_id: "724"
_4images_image_date: "2016-10-03T10:59:01"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44541 -->
Video-Link:
https://www.youtube.com/watch?v=wmY78J86oq8
und
https://www.youtube.com/watch?v=jwmn9pi0rkA