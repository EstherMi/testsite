---
layout: "image"
title: "Kugelbahn_mit_Binärzähler"
date: "2016-10-03T10:59:01"
picture: "trapp1.jpg"
weight: "1"
konstrukteure: 
- "Thomas Wenk"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/44538
imported:
- "2019"
_4images_image_id: "44538"
_4images_cat_id: "3310"
_4images_user_id: "724"
_4images_image_date: "2016-10-03T10:59:01"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44538 -->
Video-Link:
https://www.youtube.com/watch?v=lKG4xHhndyY