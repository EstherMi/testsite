---
layout: "image"
title: "Plakat"
date: "2016-10-03T19:55:15"
picture: "dreieich01.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/44547
imported:
- "2019"
_4images_image_id: "44547"
_4images_cat_id: "3281"
_4images_user_id: "1557"
_4images_image_date: "2016-10-03T19:55:15"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44547 -->
Das war sie also, die Convention 2016. Eigentlich ist es ja das Plakat vom letzten Jahr - dezent angepaßt.