---
layout: "image"
title: "Schraubenaufzug"
date: "2016-10-03T19:55:15"
picture: "dreieich07.jpg"
weight: "10"
konstrukteure: 
- "cpuetter"
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/44553
imported:
- "2019"
_4images_image_id: "44553"
_4images_cat_id: "3281"
_4images_user_id: "1557"
_4images_image_date: "2016-10-03T19:55:15"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44553 -->
[url=http://www.ftcommunity.de/categories.php?cat_id=3104]Kugelbahn mit Schneckenaufzug[/url], hier war er mal live zu erleben - elegant!