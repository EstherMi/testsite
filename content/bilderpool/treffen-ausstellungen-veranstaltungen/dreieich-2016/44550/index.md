---
layout: "image"
title: "Blick in die Halle 3/3"
date: "2016-10-03T19:55:15"
picture: "dreieich04.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/44550
imported:
- "2019"
_4images_image_id: "44550"
_4images_cat_id: "3281"
_4images_user_id: "1557"
_4images_image_date: "2016-10-03T19:55:15"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44550 -->
... und so wirkt das Ganze dann recht übersichtlich.