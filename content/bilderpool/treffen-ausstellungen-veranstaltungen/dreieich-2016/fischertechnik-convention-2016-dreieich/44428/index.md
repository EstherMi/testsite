---
layout: "image"
title: "fischertechnikconventiondreieich22.jpg"
date: "2016-10-01T15:02:06"
picture: "fischertechnikconventiondreieich22.jpg"
weight: "22"
konstrukteure: 
- "Claus-Werner Ludwig"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/44428
imported:
- "2019"
_4images_image_id: "44428"
_4images_cat_id: "3284"
_4images_user_id: "22"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44428 -->
