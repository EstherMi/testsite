---
layout: "image"
title: "fischertechnikconventiondreieich27.jpg"
date: "2016-10-01T15:02:06"
picture: "fischertechnikconventiondreieich27.jpg"
weight: "27"
konstrukteure: 
- "Tino Werner"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/44433
imported:
- "2019"
_4images_image_id: "44433"
_4images_cat_id: "3284"
_4images_user_id: "22"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44433 -->
