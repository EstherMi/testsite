---
layout: "image"
title: "fischertechnikconventiondreieich39.jpg"
date: "2016-10-01T15:02:06"
picture: "fischertechnikconventiondreieich39.jpg"
weight: "39"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/44445
imported:
- "2019"
_4images_image_id: "44445"
_4images_cat_id: "3284"
_4images_user_id: "22"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44445 -->
