---
layout: "image"
title: "Die Formel 1 der Gravitation"
date: "2016-10-01T15:02:06"
picture: "modellevonthanksforthefish1.jpg"
weight: "4"
konstrukteure: 
- "ThanksForTheFish"
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/44479
imported:
- "2019"
_4images_image_id: "44479"
_4images_cat_id: "3286"
_4images_user_id: "1557"
_4images_image_date: "2016-10-01T15:02:06"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44479 -->
Zwei Rennwagen mit unterschiedlichem Gewicht werden gleichzeitig gestartet. Welcher überquert wohl zuerst die Ziellinie?
Das kommt Euch bekannt vor? Ja wir hatten das schon mal hier (ich finde es nur gerade nicht). Es selbst zu sehen ist aber trotzdem immer wieder verblüffend.