---
layout: "image"
title: "Team Harald"
date: "2016-10-02T17:43:47"
picture: "modellevonharald3.jpg"
weight: "3"
konstrukteure: 
- "Harald"
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/44512
imported:
- "2019"
_4images_image_id: "44512"
_4images_cat_id: "3300"
_4images_user_id: "1557"
_4images_image_date: "2016-10-02T17:43:47"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44512 -->
Das Messebauteam hat jetzt Pause. Erst zum Abbau werden die Jungs wieder in Aktion treten.