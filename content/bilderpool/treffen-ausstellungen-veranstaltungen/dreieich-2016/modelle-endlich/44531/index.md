---
layout: "image"
title: "Traubenvollernter"
date: "2016-10-03T10:59:01"
picture: "endlich2.jpg"
weight: "2"
konstrukteure: 
- "Endlich"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/44531
imported:
- "2019"
_4images_image_id: "44531"
_4images_cat_id: "3308"
_4images_user_id: "724"
_4images_image_date: "2016-10-03T10:59:01"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44531 -->
Die Maschine ist gut 4 Meter hoch. Die Trauben werden in einem Behälter gesammelt. Ab und zu wird dieser in einen größeren Anhänger entleert.