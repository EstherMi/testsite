---
layout: "image"
title: "Rockkonzert"
date: "2016-10-03T19:55:15"
picture: "dreieich09.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/44555
imported:
- "2019"
_4images_image_id: "44555"
_4images_cat_id: "3281"
_4images_user_id: "1557"
_4images_image_date: "2016-10-03T19:55:15"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44555 -->
Eine zweistöckige Show, oben der Gitarrist, unten der Mädchenschwarm - äh - Sänger.