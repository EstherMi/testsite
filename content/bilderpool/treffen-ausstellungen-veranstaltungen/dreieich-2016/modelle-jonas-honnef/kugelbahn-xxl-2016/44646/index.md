---
layout: "image"
title: "Kugelbahn XXL 2016"
date: "2016-10-23T20:57:10"
picture: "kugelbahnxxl01.jpg"
weight: "1"
konstrukteure: 
- "Jonas Honnef"
fotografen:
- "Jonas Honnef"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jonas Honnef"
license: "unknown"
legacy_id:
- details/44646
imported:
- "2019"
_4images_image_id: "44646"
_4images_cat_id: "3323"
_4images_user_id: "2649"
_4images_image_date: "2016-10-23T20:57:10"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44646 -->
Kugelbahn XXL 2016 "von vorne"


Größe: 120 cm x 55 cm x 90 cm 

Attraktionen:  - Ketten-Aufzug         - Schienen-Aufzug                     - Wendel-Treppe        - Schienen-Weiche
                      - Kugel-Wechsler      - Windrad mit Kugel-Aufzug       - Vakuumgreifer
                      - Looping                  - Stangen-Treppe                      - Kugel-Trichter
                      - Schrägbrett            - Zylinder-Treppe                       - Riesenrad

Controller:                           2 (TX / TXT)
Motoren:                             6
Kompressoren:                    2
3/2 Wege Magnetventile:   3
Zylinder:                              6
Fototransistoren:                 8
Lampen / Rainbow LED:  13
Schienenlänge:                 16 Meter
