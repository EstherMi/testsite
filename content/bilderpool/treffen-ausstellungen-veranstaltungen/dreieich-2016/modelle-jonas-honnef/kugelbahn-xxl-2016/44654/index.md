---
layout: "image"
title: "Kugelbahn XXL 2016"
date: "2016-10-23T20:57:10"
picture: "kugelbahnxxl09.jpg"
weight: "9"
konstrukteure: 
- "Jonas Honnef"
fotografen:
- "Jonas Honnef"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jonas Honnef"
license: "unknown"
legacy_id:
- details/44654
imported:
- "2019"
_4images_image_id: "44654"
_4images_cat_id: "3323"
_4images_user_id: "2649"
_4images_image_date: "2016-10-23T20:57:10"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44654 -->
Kugelbahn XXL 2016 "Kugel-Trichter"