---
layout: "image"
title: "3D-Scanner (2)"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim27.jpg"
weight: "44"
konstrukteure: 
- "Torsten Stuehn"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48275
imported:
- "2019"
_4images_image_id: "48275"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48275 -->
