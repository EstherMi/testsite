---
layout: "image"
title: "Arbeitsplatz Thiemo und Jan"
date: "2018-10-25T19:35:15"
picture: "FTC_Fotos22.jpg"
weight: "13"
konstrukteure: 
- "Thiemo Rech, Jan Teufel"
fotografen:
- "Erlebnismuseum Fördertechnik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Erlebnismuseum Fördertechnik Sinsheim"
license: "unknown"
legacy_id:
- details/48317
imported:
- "2019"
_4images_image_id: "48317"
_4images_cat_id: "3541"
_4images_user_id: "2887"
_4images_image_date: "2018-10-25T19:35:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48317 -->
Teilnehmer der Workshops stellen ihre Erfindungen vor.