---
layout: "image"
title: "3D-Drucker"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim25.jpg"
weight: "42"
konstrukteure: 
- "Torsten Stuehn"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48273
imported:
- "2019"
_4images_image_id: "48273"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48273 -->
