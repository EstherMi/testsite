---
layout: "image"
title: "Große Kugelbahn"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim32.jpg"
weight: "49"
konstrukteure: 
- "Thomas Wenk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48280
imported:
- "2019"
_4images_image_id: "48280"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48280 -->
Mit dem Erbauer rechts davor.