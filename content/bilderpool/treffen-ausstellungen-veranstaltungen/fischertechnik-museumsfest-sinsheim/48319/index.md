---
layout: "image"
title: "Impressionen offene Werkstatt"
date: "2018-10-25T19:35:15"
picture: "FTC_Fotos24.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "Erlebnismuseum Fördertechnik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Erlebnismuseum Fördertechnik Sinsheim"
license: "unknown"
legacy_id:
- details/48319
imported:
- "2019"
_4images_image_id: "48319"
_4images_cat_id: "3541"
_4images_user_id: "2887"
_4images_image_date: "2018-10-25T19:35:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48319 -->
jeden Sonntag zwischen 12:00 Uhr und 16:00 Uhr