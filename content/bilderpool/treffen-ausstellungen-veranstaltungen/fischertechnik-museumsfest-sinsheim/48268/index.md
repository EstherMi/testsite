---
layout: "image"
title: "Quadrokopter"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim20.jpg"
weight: "37"
konstrukteure: 
- "Familie Salameh"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48268
imported:
- "2019"
_4images_image_id: "48268"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48268 -->
