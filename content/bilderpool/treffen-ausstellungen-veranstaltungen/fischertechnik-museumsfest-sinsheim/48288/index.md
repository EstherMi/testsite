---
layout: "image"
title: "Funktionsmodelle zur Schrägseilbrücke - Hysterese beim Seilausgleich"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim40.jpg"
weight: "57"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48288
imported:
- "2019"
_4images_image_id: "48288"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48288 -->
Die roten Teile bewegen sich auf und ab, nehmen aber nur mit Schaltabstand die Kette an den beiden Raupengliedern mit.