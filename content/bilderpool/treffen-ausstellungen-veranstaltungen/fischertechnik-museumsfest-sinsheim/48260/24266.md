---
layout: "comment"
hidden: true
title: "24266"
date: "2018-11-01T19:03:01"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
"Da alle Kugeln auf der gleichen Höhe starten und ankommen, haben sie alle am Anfang und am Ende die gleiche Energieverteilung [...] Daher kommen alle Kugeln gleichzeitig an."
Nun, das mag ich so nicht ohne [b]Einspruch[/b] hinnehmen. Stefan Falk hat es beim Bild vorher bereits völlig korrekt beschrieben. Die Kugeln kommen nach Durchlaufen der gleichen Höhendifferenz mit ziemlich genau den gleichen Geschwindigkeiten durch das Ziel (theoretisch sogar exakt). Nur: Deswegen sind die noch lange nicht alle gleichzeitig an der Ziellinie!

Schön ausführlich ist das auf Jürgen's Kugelbahnseite beschrieben: http://www.kugelbahn.info/deutsch/schnell/einl.html
Und in Pirmasens gibt es das Dynamikum, da kann man das sogar auch ausprobieren wenn man gerade kein fischertechnik zur Hand hat.

Grüsse
H.A.R.R.Y.