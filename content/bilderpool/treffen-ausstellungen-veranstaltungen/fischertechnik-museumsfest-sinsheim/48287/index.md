---
layout: "image"
title: "Funktionsmodelle zur Schrägseilbrücke - Hysterese beim Seilausgleich"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim39.jpg"
weight: "56"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48287
imported:
- "2019"
_4images_image_id: "48287"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48287 -->
Rein mechanisch realisiert gibt es einen Schaltabstand zwischen "Seil muss geregelt werden" und "Alles in Ordnung". Sehr fein gemacht.