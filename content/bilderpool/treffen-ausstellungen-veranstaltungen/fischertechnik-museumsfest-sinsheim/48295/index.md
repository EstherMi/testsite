---
layout: "image"
title: "Polarkoordinaten-Plotter"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim47.jpg"
weight: "64"
konstrukteure: 
- "David Holtz"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48295
imported:
- "2019"
_4images_image_id: "48295"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "47"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48295 -->
