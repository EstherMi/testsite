---
layout: "image"
title: "Rasenmäher und Putzroboter"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim13.jpg"
weight: "30"
konstrukteure: 
- "Familie Salameh"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48261
imported:
- "2019"
_4images_image_id: "48261"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48261 -->
