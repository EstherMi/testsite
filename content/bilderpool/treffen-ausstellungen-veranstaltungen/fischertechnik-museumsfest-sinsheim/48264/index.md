---
layout: "image"
title: "Putzroboter - von unten"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim16.jpg"
weight: "33"
konstrukteure: 
- "Familie Salameh"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48264
imported:
- "2019"
_4images_image_id: "48264"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48264 -->
Der Roboter hat tatsächlich ein Staubsammelndes Tuch auf seiner Unterseite.