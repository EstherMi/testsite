---
layout: "overview"
title: "fischertechnik Museumsfest Sinsheim"
date: 2019-12-17T18:37:09+01:00
legacy_id:
- categories/3541
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3541 --> 
Am Samstag, 20.10.2018 fand von 10:00 - 17:00 eine fischertechnik-Ausstellung im Erlebnismuseum Fördertechnik (direkt neben dem Technikmuseum) Sinsheim statt. Die jeweiligen Konstrukteure mögen bitte ihre Namen eintragen in den Kommentaren Beschreibungen hinzufügen - Danke!