---
layout: "image"
title: "Funktionsmodelle zur Schrägseilbrücke"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim37.jpg"
weight: "54"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48285
imported:
- "2019"
_4images_image_id: "48285"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48285 -->
Die machen einige technische Details anschaulich.