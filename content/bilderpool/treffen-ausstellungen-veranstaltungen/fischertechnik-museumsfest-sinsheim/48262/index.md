---
layout: "image"
title: "Rasenmäher"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim14.jpg"
weight: "31"
konstrukteure: 
- "Familie Salameh"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48262
imported:
- "2019"
_4images_image_id: "48262"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48262 -->
