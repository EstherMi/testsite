---
layout: "image"
title: "Bluetooth-Rennparcours"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim01.jpg"
weight: "18"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48249
imported:
- "2019"
_4images_image_id: "48249"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48249 -->
