---
layout: "image"
title: "offene Werkstatt"
date: "2018-10-25T19:35:14"
picture: "FTC_Fotos15.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "Erlebnismuseum Fördertechnik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Erlebnismuseum Fördertechnik Sinsheim"
license: "unknown"
legacy_id:
- details/48314
imported:
- "2019"
_4images_image_id: "48314"
_4images_cat_id: "3541"
_4images_user_id: "2887"
_4images_image_date: "2018-10-25T19:35:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48314 -->
Imressionen der offenen Werkstatt im Erlebnismuseum Fördertechnik