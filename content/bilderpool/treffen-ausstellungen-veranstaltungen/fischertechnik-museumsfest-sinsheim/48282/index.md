---
layout: "image"
title: "Große Kugelbahn"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim34.jpg"
weight: "51"
konstrukteure: 
- "Thomas Wenk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48282
imported:
- "2019"
_4images_image_id: "48282"
_4images_cat_id: "3541"
_4images_user_id: "104"
_4images_image_date: "2018-10-22T12:52:12"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48282 -->
300 Kugeln sind da im Umlauf!