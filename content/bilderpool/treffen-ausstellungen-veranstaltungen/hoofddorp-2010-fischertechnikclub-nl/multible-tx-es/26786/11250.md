---
layout: "comment"
hidden: true
title: "11250"
date: "2010-03-22T13:10:37"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
Hmm, 14ms with "only" 5 slaves. So we break the 10ms grid with more then 3 slaves :o

With the announced 8 possible slaves we get a 22,4ms for a full communication cycle then?