---
layout: "image"
title: "Detail of the first frame (req. ext4)"
date: "2010-03-21T18:38:01"
picture: "ad2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- details/26787
imported:
- "2019"
_4images_image_id: "26787"
_4images_cat_id: "1903"
_4images_user_id: "716"
_4images_image_date: "2010-03-21T18:38:01"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26787 -->
The protocol is disclosed in the documentation of the FtMscLib on the ft website.
The 5th nyte indicates that it is a message from the Master and the 9th byte indicates that it is destined for Ext.4.
Also visisble are the TransactionID (bytes 13-14) and the SessionID (bytes 15-16).
Byte 17 reveals that it concerns an output command / input request.