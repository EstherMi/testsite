---
layout: "image"
title: "Detail container picker"
date: "2010-03-15T19:09:51"
picture: "container.jpg"
weight: "3"
konstrukteure: 
- "Herman Mels"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/26723
imported:
- "2019"
_4images_image_id: "26723"
_4images_cat_id: "1906"
_4images_user_id: "371"
_4images_image_date: "2010-03-15T19:09:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26723 -->
