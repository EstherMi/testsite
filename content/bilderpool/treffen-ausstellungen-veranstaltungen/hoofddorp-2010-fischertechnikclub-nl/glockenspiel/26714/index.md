---
layout: "image"
title: "mechanische Noten mit Ketten"
date: "2010-03-15T19:09:50"
picture: "tingel1.jpg"
weight: "7"
konstrukteure: 
- "Marcel Bosch"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/26714
imported:
- "2019"
_4images_image_id: "26714"
_4images_cat_id: "1904"
_4images_user_id: "371"
_4images_image_date: "2010-03-15T19:09:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26714 -->
