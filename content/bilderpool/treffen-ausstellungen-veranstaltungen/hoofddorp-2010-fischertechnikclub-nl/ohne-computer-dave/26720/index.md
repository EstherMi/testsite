---
layout: "image"
title: "Ping-Pong-Ball bewegt ohne computer Kontrolle"
date: "2010-03-15T19:09:50"
picture: "pingpongDave.jpg"
weight: "4"
konstrukteure: 
- "Dave Gabeler"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/26720
imported:
- "2019"
_4images_image_id: "26720"
_4images_cat_id: "1905"
_4images_user_id: "371"
_4images_image_date: "2010-03-15T19:09:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26720 -->
