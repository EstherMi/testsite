---
layout: "image"
title: "Schaufenster 14"
date: "2006-12-20T21:50:26"
picture: "157_5777.jpg"
weight: "14"
konstrukteure: 
- "Holger Howey"
fotografen:
- "Holger Howey"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/8019
imported:
- "2019"
_4images_image_id: "8019"
_4images_cat_id: "747"
_4images_user_id: "34"
_4images_image_date: "2006-12-20T21:50:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8019 -->
