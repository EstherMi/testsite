---
layout: "image"
title: "Schaufenster 4 außen"
date: "2005-11-05T15:49:19"
picture: "142_4254.jpg"
weight: "4"
konstrukteure: 
- "Holger Howey"
fotografen:
- "Holger Howey"
keywords: ["Schaufenster", "Riesenrad"]
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/5183
imported:
- "2019"
_4images_image_id: "5183"
_4images_cat_id: "434"
_4images_user_id: "34"
_4images_image_date: "2005-11-05T15:49:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5183 -->
Schaufenster von außen gesehen.