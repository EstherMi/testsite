---
layout: "image"
title: "Schaufenster 3"
date: "2005-11-05T15:49:19"
picture: "142_4253.jpg"
weight: "3"
konstrukteure: 
- "Holger Howey"
fotografen:
- "Holger Howey"
keywords: ["Autos", "Raupe"]
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/5182
imported:
- "2019"
_4images_image_id: "5182"
_4images_cat_id: "434"
_4images_user_id: "34"
_4images_image_date: "2005-11-05T15:49:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5182 -->
Schaufenster von innen.