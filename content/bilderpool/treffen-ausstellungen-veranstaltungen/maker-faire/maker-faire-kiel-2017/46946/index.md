---
layout: "image"
title: "Truck in Aktion"
date: "2017-11-20T19:26:24"
picture: "makerfairkiel19.jpg"
weight: "19"
konstrukteure: 
- "Christian"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/46946
imported:
- "2019"
_4images_image_id: "46946"
_4images_cat_id: "3473"
_4images_user_id: "2303"
_4images_image_date: "2017-11-20T19:26:24"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46946 -->
