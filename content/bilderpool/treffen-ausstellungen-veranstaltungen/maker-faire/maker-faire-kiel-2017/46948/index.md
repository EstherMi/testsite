---
layout: "image"
title: "Würfel"
date: "2017-11-20T19:26:31"
picture: "makerfairkiel21.jpg"
weight: "21"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/46948
imported:
- "2019"
_4images_image_id: "46948"
_4images_cat_id: "3473"
_4images_user_id: "2303"
_4images_image_date: "2017-11-20T19:26:31"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46948 -->
