---
layout: "comment"
hidden: true
title: "23828"
date: "2017-11-27T08:28:47"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
BS7,5 (37468, ab 1975) erzürnen hier schon die Gemeinde? Ein 3-Nut BS5 (38428) von 1989 ist auch noch dabei. Na und?

Ich finde es ist ein schönes Modell <Punkt>

@ Harald: bei Gelegenheit erklärst Du mir mal bitte wie man zwei BS7,5 durch einen BS5-2Z ersetzt. Egal wie ich es drehe und wende, mir fehlen da immer 10 mm und oft sind auch noch die Zapfen im Weg.

Gruß
H.A.R.R.Y.