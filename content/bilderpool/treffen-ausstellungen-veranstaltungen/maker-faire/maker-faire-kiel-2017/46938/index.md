---
layout: "image"
title: "Krabbe 3D Druck"
date: "2017-11-20T19:26:24"
picture: "makerfairkiel11.jpg"
weight: "11"
konstrukteure: 
- "thingiverse"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/46938
imported:
- "2019"
_4images_image_id: "46938"
_4images_cat_id: "3473"
_4images_user_id: "2303"
_4images_image_date: "2017-11-20T19:26:24"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46938 -->
