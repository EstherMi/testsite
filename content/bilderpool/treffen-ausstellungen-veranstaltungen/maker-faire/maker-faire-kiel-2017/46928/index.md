---
layout: "image"
title: "Unser Stand"
date: "2017-11-20T19:26:07"
picture: "makerfairkiel01.jpg"
weight: "1"
konstrukteure: 
- "ftc"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/46928
imported:
- "2019"
_4images_image_id: "46928"
_4images_cat_id: "3473"
_4images_user_id: "2303"
_4images_image_date: "2017-11-20T19:26:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46928 -->
Video auf YouTube ftc:
https://www.youtube.com/watch?v=ZiVUMj6Hnr0