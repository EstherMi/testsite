---
layout: "image"
title: "Bein Hexapod"
date: "2017-09-01T18:06:06"
picture: "makerffaire6.jpg"
weight: "11"
konstrukteure: 
- "Thingiverse"
fotografen:
- "DirkW"
keywords: ["Hexapod"]
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/46233
imported:
- "2019"
_4images_image_id: "46233"
_4images_cat_id: "3428"
_4images_user_id: "2303"
_4images_image_date: "2017-09-01T18:06:06"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46233 -->
