---
layout: "image"
title: "makerffaire1.jpg"
date: "2017-09-01T18:06:06"
picture: "makerffaire1.jpg"
weight: "3"
konstrukteure: 
- "Thingiverse"
fotografen:
- "DirkW"
keywords: ["Hexapod"]
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/46229
imported:
- "2019"
_4images_image_id: "46229"
_4images_cat_id: "3428"
_4images_user_id: "2303"
_4images_image_date: "2017-09-01T18:06:06"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46229 -->
Servo-Grundplatte