---
layout: "image"
title: "makerfairehannover2.jpg"
date: "2017-08-28T14:50:33"
picture: "makerfairehannover2.jpg"
weight: "18"
konstrukteure: 
- "fischertechnik-Fans"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46213
imported:
- "2019"
_4images_image_id: "46213"
_4images_cat_id: "3428"
_4images_user_id: "104"
_4images_image_date: "2017-08-28T14:50:33"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46213 -->
