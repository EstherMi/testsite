---
layout: "image"
title: "makerfaireb2.jpg"
date: "2017-08-29T19:05:31"
picture: "makerfaireb2.jpg"
weight: "14"
konstrukteure: 
- "fischertechnik-Fans"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46221
imported:
- "2019"
_4images_image_id: "46221"
_4images_cat_id: "3428"
_4images_user_id: "104"
_4images_image_date: "2017-08-29T19:05:31"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46221 -->
