---
layout: "image"
title: "Tic Tac Toe"
date: "2017-09-01T18:50:22"
picture: "delta1_2.jpg"
weight: "5"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/46236
imported:
- "2019"
_4images_image_id: "46236"
_4images_cat_id: "3428"
_4images_user_id: "2303"
_4images_image_date: "2017-09-01T18:50:22"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46236 -->
