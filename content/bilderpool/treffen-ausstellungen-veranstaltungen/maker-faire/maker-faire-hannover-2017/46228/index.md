---
layout: "image"
title: "Hexapod"
date: "2017-09-01T17:50:11"
picture: "makerfaire2_2.jpg"
weight: "7"
konstrukteure: 
- "Thingiverse"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/46228
imported:
- "2019"
_4images_image_id: "46228"
_4images_cat_id: "3428"
_4images_user_id: "2303"
_4images_image_date: "2017-09-01T17:50:11"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46228 -->
