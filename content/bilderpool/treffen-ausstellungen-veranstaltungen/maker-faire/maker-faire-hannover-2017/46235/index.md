---
layout: "image"
title: "Delta-Roboter"
date: "2017-09-01T18:35:38"
picture: "delta1.jpg"
weight: "4"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/46235
imported:
- "2019"
_4images_image_id: "46235"
_4images_cat_id: "3428"
_4images_user_id: "2303"
_4images_image_date: "2017-09-01T18:35:38"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46235 -->
Einfach aufgebauter Delta-Roboter gesteuert über ein Arduino-Shield.
Spielt über einen Nunchuk Tic Tac Toe. Das war der Hit auf der Ausstellung.