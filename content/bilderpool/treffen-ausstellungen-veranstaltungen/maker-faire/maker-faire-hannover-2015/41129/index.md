---
layout: "image"
title: "makerfaire32.jpg"
date: "2015-06-07T21:33:45"
picture: "makerfaire32.jpg"
weight: "42"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/41129
imported:
- "2019"
_4images_image_id: "41129"
_4images_cat_id: "3083"
_4images_user_id: "1"
_4images_image_date: "2015-06-07T21:33:45"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41129 -->
