---
layout: "image"
title: "makerfaire009"
date: "2015-06-08T21:30:57"
picture: "makerfaire09_2.jpg"
weight: "9"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: ["Maker", "Faire"]
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/41182
imported:
- "2019"
_4images_image_id: "41182"
_4images_cat_id: "3083"
_4images_user_id: "2303"
_4images_image_date: "2015-06-08T21:30:57"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41182 -->
Pixy-Robot Rennbahn