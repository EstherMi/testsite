---
layout: "image"
title: "makerfaire004"
date: "2015-06-08T21:30:57"
picture: "makerfaire04_2.jpg"
weight: "4"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/41177
imported:
- "2019"
_4images_image_id: "41177"
_4images_cat_id: "3083"
_4images_user_id: "2303"
_4images_image_date: "2015-06-08T21:30:57"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41177 -->
