---
layout: "image"
title: "makerfaire001"
date: "2015-06-08T21:30:57"
picture: "makerfaire01_2.jpg"
weight: "1"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/41174
imported:
- "2019"
_4images_image_id: "41174"
_4images_cat_id: "3083"
_4images_user_id: "2303"
_4images_image_date: "2015-06-08T21:30:57"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41174 -->
Der Robotikbereich
