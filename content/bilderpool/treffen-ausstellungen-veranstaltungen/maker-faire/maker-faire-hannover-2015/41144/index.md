---
layout: "image"
title: "makerfaire47.jpg"
date: "2015-06-07T21:33:45"
picture: "makerfaire47.jpg"
weight: "57"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/41144
imported:
- "2019"
_4images_image_id: "41144"
_4images_cat_id: "3083"
_4images_user_id: "1"
_4images_image_date: "2015-06-07T21:33:45"
_4images_image_order: "47"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41144 -->
