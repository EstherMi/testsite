---
layout: "image"
title: "makerfaire52.jpg"
date: "2015-06-07T21:33:45"
picture: "makerfaire52.jpg"
weight: "62"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/41149
imported:
- "2019"
_4images_image_id: "41149"
_4images_cat_id: "3083"
_4images_user_id: "1"
_4images_image_date: "2015-06-07T21:33:45"
_4images_image_order: "52"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41149 -->
