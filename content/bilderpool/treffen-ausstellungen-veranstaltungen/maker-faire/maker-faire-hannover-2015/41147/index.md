---
layout: "image"
title: "makerfaire50.jpg"
date: "2015-06-07T21:33:45"
picture: "makerfaire50.jpg"
weight: "60"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/41147
imported:
- "2019"
_4images_image_id: "41147"
_4images_cat_id: "3083"
_4images_user_id: "1"
_4images_image_date: "2015-06-07T21:33:45"
_4images_image_order: "50"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41147 -->
