---
layout: "image"
title: "makerfaire003"
date: "2015-06-08T21:30:57"
picture: "makerfaire03_2.jpg"
weight: "3"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/41176
imported:
- "2019"
_4images_image_id: "41176"
_4images_cat_id: "3083"
_4images_user_id: "2303"
_4images_image_date: "2015-06-08T21:30:57"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41176 -->
