---
layout: "image"
title: "makerfaire007"
date: "2015-06-08T21:30:57"
picture: "makerfaire07_2.jpg"
weight: "7"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/41180
imported:
- "2019"
_4images_image_id: "41180"
_4images_cat_id: "3083"
_4images_user_id: "2303"
_4images_image_date: "2015-06-08T21:30:57"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41180 -->
WALL-E