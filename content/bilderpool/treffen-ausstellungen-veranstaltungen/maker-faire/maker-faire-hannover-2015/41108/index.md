---
layout: "image"
title: "makerfaire11.jpg"
date: "2015-06-07T21:33:45"
picture: "makerfaire11.jpg"
weight: "21"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/41108
imported:
- "2019"
_4images_image_id: "41108"
_4images_cat_id: "3083"
_4images_user_id: "1"
_4images_image_date: "2015-06-07T21:33:45"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41108 -->
