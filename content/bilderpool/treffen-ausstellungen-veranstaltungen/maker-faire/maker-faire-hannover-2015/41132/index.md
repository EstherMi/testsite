---
layout: "image"
title: "makerfaire35.jpg"
date: "2015-06-07T21:33:45"
picture: "makerfaire35.jpg"
weight: "45"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/41132
imported:
- "2019"
_4images_image_id: "41132"
_4images_cat_id: "3083"
_4images_user_id: "1"
_4images_image_date: "2015-06-07T21:33:45"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41132 -->
