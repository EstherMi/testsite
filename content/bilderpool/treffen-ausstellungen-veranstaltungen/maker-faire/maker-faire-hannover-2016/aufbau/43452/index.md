---
layout: "image"
title: "makerfaire4.jpg"
date: "2016-05-30T19:31:22"
picture: "makerfaire4.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/43452
imported:
- "2019"
_4images_image_id: "43452"
_4images_cat_id: "3230"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:31:22"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43452 -->
