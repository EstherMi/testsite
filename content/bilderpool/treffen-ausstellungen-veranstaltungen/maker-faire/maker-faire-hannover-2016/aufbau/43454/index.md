---
layout: "image"
title: "makerfaire6.jpg"
date: "2016-05-30T19:31:22"
picture: "makerfaire6.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/43454
imported:
- "2019"
_4images_image_id: "43454"
_4images_cat_id: "3230"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:31:22"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43454 -->
