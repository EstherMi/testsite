---
layout: "image"
title: "makerfaire2.jpg"
date: "2016-05-30T19:31:22"
picture: "makerfaire2.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/43450
imported:
- "2019"
_4images_image_id: "43450"
_4images_cat_id: "3230"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:31:22"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43450 -->
