---
layout: "image"
title: "grillen2.jpg"
date: "2016-05-30T19:40:46"
picture: "grillen2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/43655
imported:
- "2019"
_4images_image_id: "43655"
_4images_cat_id: "3229"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:40:46"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43655 -->
von links nach rechts:
Masked, Sven, DirkW