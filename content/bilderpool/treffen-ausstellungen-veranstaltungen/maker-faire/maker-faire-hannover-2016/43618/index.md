---
layout: "image"
title: "makerfaire164.jpg"
date: "2016-05-30T19:38:20"
picture: "makerfaire164.jpg"
weight: "161"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/43618
imported:
- "2019"
_4images_image_id: "43618"
_4images_cat_id: "3228"
_4images_user_id: "1"
_4images_image_date: "2016-05-30T19:38:20"
_4images_image_order: "164"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43618 -->
