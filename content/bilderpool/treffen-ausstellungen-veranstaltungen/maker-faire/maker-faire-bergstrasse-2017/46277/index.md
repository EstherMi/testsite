---
layout: "image"
title: "Drohne in Aktion"
date: "2017-09-21T17:04:31"
picture: "makerfairebergstrasse06.jpg"
weight: "24"
konstrukteure: 
- "Magnus Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/46277
imported:
- "2019"
_4images_image_id: "46277"
_4images_cat_id: "3433"
_4images_user_id: "1126"
_4images_image_date: "2017-09-21T17:04:31"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46277 -->
Dass fischertechnik auch fliegen kann, bewies Magnus mit seiner Drohne im Innenhof der Maker Faire.