---
layout: "image"
title: "3D-Scanner"
date: "2017-09-21T17:04:31"
picture: "makerfairebergstrasse03.jpg"
weight: "21"
konstrukteure: 
- "Torsten Stuehn"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/46274
imported:
- "2019"
_4images_image_id: "46274"
_4images_cat_id: "3433"
_4images_user_id: "1126"
_4images_image_date: "2017-09-21T17:04:31"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46274 -->
Torstens 3D-Scanner (realisiert in Python auf einem TXT mit Community-Firmware) hatte Premiere - und war einer der Hingucker: Die kleine Badeente wurde gescannt...