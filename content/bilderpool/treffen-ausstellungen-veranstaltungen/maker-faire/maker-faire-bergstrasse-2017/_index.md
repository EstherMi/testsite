---
layout: "overview"
title: "Maker Faire Bergstraße 2017"
date: 2019-12-17T18:34:02+01:00
legacy_id:
- categories/3433
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3433 --> 
Vierzehn kleine (und etwas größere) fischertechnikerlein hatten sich unter der Federführung und organisierenden Hand von Esther bei der Maker Faire Bergstraße in Bensheim eingefunden - und brachten vom 16.-17.09.2017 hunderte von Kinder- (und ehemaligen Kinder-) augen zum Leuchten.