---
layout: "image"
title: "3D-Scanner und Erbauer"
date: "2017-10-02T20:30:44"
picture: "klein-009.jpg"
weight: "1"
konstrukteure: 
- "Torsten"
fotografen:
- "Esther"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "EstherM"
license: "unknown"
legacy_id:
- details/46739
imported:
- "2019"
_4images_image_id: "46739"
_4images_cat_id: "3433"
_4images_user_id: "2781"
_4images_image_date: "2017-10-02T20:30:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46739 -->
