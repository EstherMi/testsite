---
layout: "comment"
hidden: true
title: "23644"
date: "2017-09-25T14:24:18"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Lieber Dirk,
da ist dann wohl eher ein Blindenhund nötig. Die Sehhilfe hatte ich auf der Nase als ich den Kommentar schrieb.
Und Du hast tatsächlich Recht. Frag' ich doch eben meinen Sohnemann ob er auf dem Bild ein Ü-Ei sieht. Erste Antwort "Nein", 10s später, "Doch, DA!".
Gruß
H.A.R.R.Y.