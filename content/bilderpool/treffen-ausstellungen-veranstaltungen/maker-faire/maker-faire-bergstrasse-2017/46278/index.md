---
layout: "image"
title: "Greifautomat"
date: "2017-09-21T17:04:31"
picture: "makerfairebergstrasse07.jpg"
weight: "25"
konstrukteure: 
- "Stefan Fuss"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/46278
imported:
- "2019"
_4images_image_id: "46278"
_4images_cat_id: "3433"
_4images_user_id: "1126"
_4images_image_date: "2017-09-21T17:04:31"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46278 -->
Renner des Stands und konkurrenzloser Besuchermagnet war Stefans Greifautomat - kein Kind, das nicht versuchte, sich ein Ü-Ei zu sichern...