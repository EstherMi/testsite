---
layout: "image"
title: "Keksdrucker"
date: "2017-09-21T17:04:31"
picture: "makerfairebergstrasse08.jpg"
weight: "26"
konstrukteure: 
- "Stefan Fuss"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/46279
imported:
- "2019"
_4images_image_id: "46279"
_4images_cat_id: "3433"
_4images_user_id: "1126"
_4images_image_date: "2017-09-21T17:04:31"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46279 -->
Kugelrunde Kinderaugen gab es beim Keksdrucker: Drei-Farb-Druck auf Bahlsen-Keks!