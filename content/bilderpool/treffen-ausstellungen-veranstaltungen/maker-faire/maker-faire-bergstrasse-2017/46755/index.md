---
layout: "image"
title: "Noch mehr Fachgespräche"
date: "2017-10-02T21:00:58"
picture: "klein-0011.jpg"
weight: "17"
konstrukteure: 
- "Das Standbau-Team"
fotografen:
- "Esther"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "EstherM"
license: "unknown"
legacy_id:
- details/46755
imported:
- "2019"
_4images_image_id: "46755"
_4images_cat_id: "3433"
_4images_user_id: "2781"
_4images_image_date: "2017-10-02T21:00:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46755 -->
