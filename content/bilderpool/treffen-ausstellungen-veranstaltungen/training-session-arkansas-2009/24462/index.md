---
layout: "image"
title: "Belt Drives"
date: "2009-06-27T19:56:56"
picture: "ft_pulleydrives.jpg"
weight: "5"
konstrukteure: 
- "Student"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Arkansas"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/24462
imported:
- "2019"
_4images_image_id: "24462"
_4images_cat_id: "1678"
_4images_user_id: "585"
_4images_image_date: "2009-06-27T19:56:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24462 -->
This student built and combined several belt drives.