---
layout: "image"
title: "Small Arch"
date: "2009-06-27T19:56:56"
picture: "ft_sm_arch.jpg"
weight: "6"
konstrukteure: 
- "Student"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Arkansas", "PCS", "AOR"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/24463
imported:
- "2019"
_4images_image_id: "24463"
_4images_cat_id: "1678"
_4images_user_id: "585"
_4images_image_date: "2009-06-27T19:56:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24463 -->
This student constructed a small arch using a series of Angle Blocks.