---
layout: "image"
title: "Kirmesmodell 1"
date: "2007-11-29T19:38:09"
picture: "puetter1.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- details/12910
imported:
- "2019"
_4images_image_id: "12910"
_4images_cat_id: "1170"
_4images_user_id: "298"
_4images_image_date: "2007-11-29T19:38:09"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12910 -->
