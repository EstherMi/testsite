---
layout: "image"
title: "Modell"
date: "2007-11-29T17:35:20"
picture: "olli13.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/12886
imported:
- "2019"
_4images_image_id: "12886"
_4images_cat_id: "1170"
_4images_user_id: "504"
_4images_image_date: "2007-11-29T17:35:20"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12886 -->
Kirmesding