---
layout: "image"
title: "Kirmesmodell 2"
date: "2007-11-29T19:38:10"
picture: "puetter2.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "cpuetter"
license: "unknown"
legacy_id:
- details/12911
imported:
- "2019"
_4images_image_id: "12911"
_4images_cat_id: "1170"
_4images_user_id: "298"
_4images_image_date: "2007-11-29T19:38:10"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12911 -->
