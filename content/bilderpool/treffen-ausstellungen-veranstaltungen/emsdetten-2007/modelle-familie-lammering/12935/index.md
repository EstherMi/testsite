---
layout: "image"
title: "Kirmesmodell"
date: "2007-11-30T12:25:13"
picture: "kirmesmodelle2.jpg"
weight: "2"
konstrukteure: 
- "Familie Lammering"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/12935
imported:
- "2019"
_4images_image_id: "12935"
_4images_cat_id: "1170"
_4images_user_id: "453"
_4images_image_date: "2007-11-30T12:25:13"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12935 -->
