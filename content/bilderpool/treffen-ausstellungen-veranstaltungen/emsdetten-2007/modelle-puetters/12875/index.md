---
layout: "image"
title: "Kran"
date: "2007-11-29T17:35:20"
picture: "olli02.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/12875
imported:
- "2019"
_4images_image_id: "12875"
_4images_cat_id: "1166"
_4images_user_id: "504"
_4images_image_date: "2007-11-29T17:35:20"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12875 -->
Der Kran den man selber steuern konnte