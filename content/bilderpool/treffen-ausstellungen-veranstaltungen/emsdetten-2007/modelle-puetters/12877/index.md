---
layout: "image"
title: "Magnet"
date: "2007-11-29T17:35:20"
picture: "olli04.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/12877
imported:
- "2019"
_4images_image_id: "12877"
_4images_cat_id: "1166"
_4images_user_id: "504"
_4images_image_date: "2007-11-29T17:35:20"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12877 -->
Vom Kran