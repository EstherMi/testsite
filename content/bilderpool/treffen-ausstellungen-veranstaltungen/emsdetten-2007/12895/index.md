---
layout: "image"
title: "LKW"
date: "2007-11-29T17:35:20"
picture: "olli22.jpg"
weight: "3"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/12895
imported:
- "2019"
_4images_image_id: "12895"
_4images_cat_id: "1158"
_4images_user_id: "504"
_4images_image_date: "2007-11-29T17:35:20"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12895 -->
LKW