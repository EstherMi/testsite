---
layout: "image"
title: "Trainings-Roboter"
date: "2007-11-30T12:30:19"
picture: "brickwedde4.jpg"
weight: "4"
konstrukteure: 
- "Brickwedde"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/12939
imported:
- "2019"
_4images_image_id: "12939"
_4images_cat_id: "1167"
_4images_user_id: "453"
_4images_image_date: "2007-11-30T12:30:19"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12939 -->
