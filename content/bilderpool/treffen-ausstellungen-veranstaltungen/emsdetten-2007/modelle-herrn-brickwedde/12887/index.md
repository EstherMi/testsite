---
layout: "image"
title: "Spielbrett"
date: "2007-11-29T17:35:20"
picture: "olli14.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/12887
imported:
- "2019"
_4images_image_id: "12887"
_4images_cat_id: "1167"
_4images_user_id: "504"
_4images_image_date: "2007-11-29T17:35:20"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12887 -->
Das Spielbrett vom Herrn Brickwedde