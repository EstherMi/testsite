---
layout: "image"
title: "Führerhaus"
date: "2007-11-29T17:35:20"
picture: "olli29.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/12902
imported:
- "2019"
_4images_image_id: "12902"
_4images_cat_id: "1163"
_4images_user_id: "504"
_4images_image_date: "2007-11-29T17:35:20"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12902 -->
Vom Mobilkran.