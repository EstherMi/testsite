---
layout: "image"
title: "Raupe"
date: "2007-11-26T16:28:10"
picture: "modellevonludger5.jpg"
weight: "9"
konstrukteure: 
- "Ludger"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/12837
imported:
- "2019"
_4images_image_id: "12837"
_4images_cat_id: "1159"
_4images_user_id: "453"
_4images_image_date: "2007-11-26T16:28:10"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12837 -->
Hier fallen sie in den LKW und werden wieder in das "Erbsen" Lager gebracht.