---
layout: "image"
title: "Raupe"
date: "2007-11-26T16:28:10"
picture: "modellevonludger3.jpg"
weight: "7"
konstrukteure: 
- "Ludger"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/12835
imported:
- "2019"
_4images_image_id: "12835"
_4images_cat_id: "1159"
_4images_user_id: "453"
_4images_image_date: "2007-11-26T16:28:10"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12835 -->
....und hat die Erbsen in den Trichter geschüttet.