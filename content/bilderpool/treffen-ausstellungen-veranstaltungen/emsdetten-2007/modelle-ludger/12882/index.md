---
layout: "image"
title: "Transporter"
date: "2007-11-29T17:35:20"
picture: "olli09.jpg"
weight: "3"
konstrukteure: 
- "ludger-ftc"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/12882
imported:
- "2019"
_4images_image_id: "12882"
_4images_cat_id: "1159"
_4images_user_id: "504"
_4images_image_date: "2007-11-29T17:35:20"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12882 -->
Von Ludger