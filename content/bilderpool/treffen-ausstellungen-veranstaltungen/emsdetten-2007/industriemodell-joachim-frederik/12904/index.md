---
layout: "image"
title: "Industriemaschine"
date: "2007-11-29T17:35:20"
picture: "olli31.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/12904
imported:
- "2019"
_4images_image_id: "12904"
_4images_cat_id: "1169"
_4images_user_id: "504"
_4images_image_date: "2007-11-29T17:35:20"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12904 -->
Von Fredy und Joachim