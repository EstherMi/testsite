---
layout: "image"
title: "Modell von Joachim und Frederik"
date: "2007-11-29T19:57:30"
picture: "industriemodell05.jpg"
weight: "5"
konstrukteure: 
- "Joachim und Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/12924
imported:
- "2019"
_4images_image_id: "12924"
_4images_cat_id: "1169"
_4images_user_id: "453"
_4images_image_date: "2007-11-29T19:57:30"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12924 -->
