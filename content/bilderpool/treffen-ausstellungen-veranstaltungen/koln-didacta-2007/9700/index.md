---
layout: "image"
title: "Kran"
date: "2007-03-23T22:21:21"
picture: "162_6229.jpg"
weight: "34"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/9700
imported:
- "2019"
_4images_image_id: "9700"
_4images_cat_id: "876"
_4images_user_id: "34"
_4images_image_date: "2007-03-23T22:21:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9700 -->
Ein Modell vom einem der neuen UT Kästen von LPE. Man beachte die roten!!! geraden ft Teile für die Flachträger 120.
Man kann nur hoffen das die auch in rot produziert werden, die würden sich sehr gut zum ft-Weihnachtsschmuck 2006 machen :-)
Hier sind sie nur rot lackiert.