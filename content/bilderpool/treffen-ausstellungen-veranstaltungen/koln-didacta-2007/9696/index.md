---
layout: "image"
title: "Robo Connect Box3"
date: "2007-03-23T22:21:21"
picture: "162_6233.jpg"
weight: "30"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/9696
imported:
- "2019"
_4images_image_id: "9696"
_4images_cat_id: "876"
_4images_user_id: "34"
_4images_image_date: "2007-03-23T22:21:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9696 -->
