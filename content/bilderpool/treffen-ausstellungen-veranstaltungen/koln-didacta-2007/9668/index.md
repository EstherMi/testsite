---
layout: "image"
title: "SPS Steuerung mit Händetrockner"
date: "2007-03-23T22:21:21"
picture: "162_6265.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey"
keywords: ["SPS", "Steuerung", "mit", "Händetrockner"]
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/9668
imported:
- "2019"
_4images_image_id: "9668"
_4images_cat_id: "876"
_4images_user_id: "34"
_4images_image_date: "2007-03-23T22:21:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9668 -->
