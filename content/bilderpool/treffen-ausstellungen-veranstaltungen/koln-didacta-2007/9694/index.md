---
layout: "image"
title: "Robo Connect Box"
date: "2007-03-23T22:21:21"
picture: "162_6235.jpg"
weight: "28"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/9694
imported:
- "2019"
_4images_image_id: "9694"
_4images_cat_id: "876"
_4images_user_id: "34"
_4images_image_date: "2007-03-23T22:21:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9694 -->
Robo Connect Box für den Anschluß eines alten seriellen, parallelen ft Interfaces oder einer Multibox von Knobloch.
Vorteil ist, man braucht keine weiteren Treiber. Steuerung erfolgt über RoboPro oder die ftLib. Windows "sieht" nur ein USB Gerät, es ist also für alle Windows arten geeignet und halt alle neuen schnellen Rechner.