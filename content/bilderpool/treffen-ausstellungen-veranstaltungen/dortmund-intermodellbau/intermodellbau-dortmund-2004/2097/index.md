---
layout: "image"
title: "Robo Kartons"
date: "2004-02-11T11:20:13"
picture: "109_0931.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/2097
imported:
- "2019"
_4images_image_id: "2097"
_4images_cat_id: "242"
_4images_user_id: "34"
_4images_image_date: "2004-02-11T11:20:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2097 -->
Robo Kartons an der Wand bei der didacta in Köln