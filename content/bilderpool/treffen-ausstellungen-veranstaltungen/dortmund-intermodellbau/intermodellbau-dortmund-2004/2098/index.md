---
layout: "image"
title: "Industrieanlage"
date: "2004-02-11T11:20:13"
picture: "109_0932.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/2098
imported:
- "2019"
_4images_image_id: "2098"
_4images_cat_id: "242"
_4images_user_id: "34"
_4images_image_date: "2004-02-11T11:20:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2098 -->
Hier eine Industrieanlage original von fischertechnik im vertrieb (kein Staudinger)
3D Säulenrobi und jede Menge Bearbeitungsstationen. Die Umlaufrichtung ist von unten rechts im Uhrzeigersinn