---
layout: "image"
title: "Neuheiten"
date: "2004-02-11T11:20:13"
picture: "109_0934.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/2100
imported:
- "2019"
_4images_image_id: "2100"
_4images_cat_id: "242"
_4images_user_id: "34"
_4images_image_date: "2004-02-11T11:20:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2100 -->
Modelle aus den neuen Kästen