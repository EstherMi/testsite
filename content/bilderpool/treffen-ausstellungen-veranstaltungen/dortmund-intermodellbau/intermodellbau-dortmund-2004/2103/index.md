---
layout: "image"
title: "Industrieanlage2"
date: "2004-02-11T11:20:29"
picture: "109_0938.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/2103
imported:
- "2019"
_4images_image_id: "2103"
_4images_cat_id: "242"
_4images_user_id: "34"
_4images_image_date: "2004-02-11T11:20:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2103 -->
Hier die Industrieanlage von der Eite gesehen. In der Mitte rechts (oberhalb vom Händetrockner) kommen die fertigen Bauteile raus.