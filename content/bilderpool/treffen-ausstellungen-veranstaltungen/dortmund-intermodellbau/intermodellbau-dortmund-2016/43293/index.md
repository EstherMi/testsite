---
layout: "image"
title: "ft:c-Stand"
date: "2016-04-25T20:24:10"
picture: "intermodellbau18.jpg"
weight: "18"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/43293
imported:
- "2019"
_4images_image_id: "43293"
_4images_cat_id: "3215"
_4images_user_id: "2303"
_4images_image_date: "2016-04-25T20:24:10"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43293 -->
Wall-E mit seinem Kumpel, den er über die Objekterkennung 
der Pixy-Camera verfolgt.
