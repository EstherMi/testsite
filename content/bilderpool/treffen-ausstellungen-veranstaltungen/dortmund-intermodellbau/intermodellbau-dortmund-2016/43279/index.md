---
layout: "image"
title: "ft:c-Stand"
date: "2016-04-25T20:24:10"
picture: "intermodellbau04.jpg"
weight: "4"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/43279
imported:
- "2019"
_4images_image_id: "43279"
_4images_cat_id: "3215"
_4images_user_id: "2303"
_4images_image_date: "2016-04-25T20:24:10"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43279 -->
Brückenkran Hebegeschirr.