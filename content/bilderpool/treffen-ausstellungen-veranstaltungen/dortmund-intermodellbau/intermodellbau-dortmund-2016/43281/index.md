---
layout: "image"
title: "ft:c-Stand"
date: "2016-04-25T20:24:10"
picture: "intermodellbau06.jpg"
weight: "6"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/43281
imported:
- "2019"
_4images_image_id: "43281"
_4images_cat_id: "3215"
_4images_user_id: "2303"
_4images_image_date: "2016-04-25T20:24:10"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43281 -->
Wiener Riesenrad