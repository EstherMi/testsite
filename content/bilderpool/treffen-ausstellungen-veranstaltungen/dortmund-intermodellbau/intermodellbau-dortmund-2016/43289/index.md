---
layout: "image"
title: "ft:c-Stand"
date: "2016-04-25T20:24:10"
picture: "intermodellbau14.jpg"
weight: "14"
konstrukteure: 
- "geometer"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/43289
imported:
- "2019"
_4images_image_id: "43289"
_4images_cat_id: "3215"
_4images_user_id: "2303"
_4images_image_date: "2016-04-25T20:24:10"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43289 -->
Leuchtturm-Modell ohne Schleifring.