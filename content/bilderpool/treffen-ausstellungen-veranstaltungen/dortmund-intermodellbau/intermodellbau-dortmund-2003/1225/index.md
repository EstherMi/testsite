---
layout: "image"
title: "Der Parcour"
date: "2003-07-08T17:12:53"
picture: "Der_Parcour_2.jpg"
weight: "1"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Stephan Wenkers"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/1225
imported:
- "2019"
_4images_image_id: "1225"
_4images_cat_id: "141"
_4images_user_id: "130"
_4images_image_date: "2003-07-08T17:12:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1225 -->
