---
layout: "image"
title: "Boise Bot Competition"
date: "2009-09-22T21:44:14"
picture: "sm_ftbot2.jpg"
weight: "52"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Boise", "Bot", "Competition"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/25084
imported:
- "2019"
_4images_image_id: "25084"
_4images_cat_id: "2010"
_4images_user_id: "585"
_4images_image_date: "2009-09-22T21:44:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25084 -->
Howdy, I entered several robots into the first Boise Bot Competition. My ft entry 
into the maze competition was a modified mobile robot integrated with the BRAIN. 
The entry did very well!  

***google translation****

	
Hallo, Trat ich in mehreren Robotern in den ersten Boise Bot Wettbewerb. 
Mein ft Eintritt in das Labyrinth des Wettbewerbs war eine modifizierte mobile Roboter mit der BRAIN integriert. Der Eintrag war sehr gut!