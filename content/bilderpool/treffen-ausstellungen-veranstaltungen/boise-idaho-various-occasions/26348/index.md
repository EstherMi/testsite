---
layout: "image"
title: "Community Schools Class"
date: "2010-02-13T15:24:19"
picture: "sm_ujs_chain.jpg"
weight: "30"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["PCS", "Community", "Schools"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/26348
imported:
- "2019"
_4images_image_id: "26348"
_4images_cat_id: "2010"
_4images_user_id: "585"
_4images_image_date: "2010-02-13T15:24:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26348 -->
These are images of a "Mechanical Engineering with fischertechnik" class I am teaching at Community Schools!