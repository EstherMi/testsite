---
layout: "comment"
hidden: true
title: "12472"
date: "2010-10-07T00:15:01"
uploadBy:
- "ft_idaho"
license: "unknown"
imported:
- "2019"
---
Thanks. I was far behind in the pack at fifth place, and only participated in this trial - I brought the robot for my presentation and did not have the ability to reprogram it. I will field a much better robot for next year's competition.