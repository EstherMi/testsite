---
layout: "image"
title: "Boise Bot Competition"
date: "2009-09-22T21:44:14"
picture: "boise_bot_poster.jpg"
weight: "50"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["***google", "translation****", "Hallo", "Trat", "ich", "in", "mehreren", "Robotern", "in", "den", "ersten", "Boise", "Bot", "Wettbewerb.", "Dies", "ist", "das", "Plakat", "für", "die", "Veranstaltung."]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/25082
imported:
- "2019"
_4images_image_id: "25082"
_4images_cat_id: "2010"
_4images_user_id: "585"
_4images_image_date: "2009-09-22T21:44:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25082 -->
Howdy, I entered several robots into the first Boise Bot Competition. 

This is the poster for the event.