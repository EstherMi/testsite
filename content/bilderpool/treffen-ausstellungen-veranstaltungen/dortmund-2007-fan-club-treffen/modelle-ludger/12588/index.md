---
layout: "image"
title: "Raupe"
date: "2007-11-10T15:28:05"
picture: "raupe1.jpg"
weight: "1"
konstrukteure: 
- "Ludger"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/12588
imported:
- "2019"
_4images_image_id: "12588"
_4images_cat_id: "1135"
_4images_user_id: "453"
_4images_image_date: "2007-11-10T15:28:05"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12588 -->
