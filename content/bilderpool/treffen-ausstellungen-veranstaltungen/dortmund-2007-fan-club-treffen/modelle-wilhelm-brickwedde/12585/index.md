---
layout: "image"
title: "Antriebe"
date: "2007-11-10T15:28:05"
picture: "wilhelmbrickwedde08.jpg"
weight: "8"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/12585
imported:
- "2019"
_4images_image_id: "12585"
_4images_cat_id: "1134"
_4images_user_id: "453"
_4images_image_date: "2007-11-10T15:28:05"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12585 -->
