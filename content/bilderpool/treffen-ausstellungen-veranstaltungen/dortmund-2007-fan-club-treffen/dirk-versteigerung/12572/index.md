---
layout: "image"
title: "Dirk und die Versteigerung"
date: "2007-11-10T15:10:47"
picture: "dirkunddieversteigerung1.jpg"
weight: "1"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/12572
imported:
- "2019"
_4images_image_id: "12572"
_4images_cat_id: "1133"
_4images_user_id: "453"
_4images_image_date: "2007-11-10T15:10:47"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12572 -->
Er wollte den Kasten für das Start Gebot von 30 Euro verkaufen. Es hat aber kein Besucher geboten, also hat er ihn wieder mit nach Hause genommen.