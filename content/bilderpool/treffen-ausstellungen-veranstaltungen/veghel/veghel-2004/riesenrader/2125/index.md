---
layout: "image"
title: "RR03.JPG"
date: "2004-02-20T12:21:10"
picture: "RR03.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2125
imported:
- "2019"
_4images_image_id: "2125"
_4images_cat_id: "217"
_4images_user_id: "4"
_4images_image_date: "2004-02-20T12:21:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2125 -->
Veghel 2004

Die Achse ist ein Plastikrohr mit ca. 2 cm Durchmesser.