---
layout: "image"
title: "RR10.JPG"
date: "2004-02-20T12:21:29"
picture: "RR10.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2128
imported:
- "2019"
_4images_image_id: "2128"
_4images_cat_id: "217"
_4images_user_id: "4"
_4images_image_date: "2004-02-20T12:21:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2128 -->
Veghel 2004, eins von zwei Riesenrädern.

Das Radfahrgestell ist bei näherem Hinsehen doch eher Atrappe, denn ohne weiteres Zerlegen wird die Grundkonstruktion nicht straßentauglich.