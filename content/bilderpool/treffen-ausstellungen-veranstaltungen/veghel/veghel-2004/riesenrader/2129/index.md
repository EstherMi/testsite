---
layout: "image"
title: "RR11.JPG"
date: "2004-02-20T12:21:29"
picture: "RR11.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2129
imported:
- "2019"
_4images_image_id: "2129"
_4images_cat_id: "217"
_4images_user_id: "4"
_4images_image_date: "2004-02-20T12:21:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2129 -->
Veghel 2004, eins von zwei Riesenrädern.

Der Antrieb erfolgt durch ein Reibrad direkt auf den äußeren Ring.