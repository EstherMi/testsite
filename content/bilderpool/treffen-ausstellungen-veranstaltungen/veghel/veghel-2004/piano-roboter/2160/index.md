---
layout: "image"
title: "Pianoroboter 7"
date: "2004-02-20T12:22:45"
picture: "Pianoroboter_7.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Roboter", "Piano", "Orgel", "Pneumatik", "Hände", "Finger"]
uploadBy: "DerMitDenBitsTanzt"
license: "unknown"
legacy_id:
- details/2160
imported:
- "2019"
_4images_image_id: "2160"
_4images_cat_id: "428"
_4images_user_id: "31"
_4images_image_date: "2004-02-20T12:22:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2160 -->
Rückansicht Schulter