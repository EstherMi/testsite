---
layout: "image"
title: "Pianoroboter 1"
date: "2004-02-20T12:22:45"
picture: "Pianoroboter_1.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DerMitDenBitsTanzt"
license: "unknown"
legacy_id:
- details/2153
imported:
- "2019"
_4images_image_id: "2153"
_4images_cat_id: "428"
_4images_user_id: "31"
_4images_image_date: "2004-02-20T12:22:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2153 -->
Interface