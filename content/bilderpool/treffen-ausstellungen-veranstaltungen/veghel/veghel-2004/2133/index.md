---
layout: "image"
title: "Autokran02.JPG"
date: "2004-02-20T12:21:29"
picture: "Autokran02.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2133
imported:
- "2019"
_4images_image_id: "2133"
_4images_cat_id: "204"
_4images_user_id: "4"
_4images_image_date: "2004-02-20T12:21:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2133 -->
Sieht alles aus, als müßte es so sein, und doch steckt bei Fremdteilen immer eine Menge Bastelei dahinter.