---
layout: "image"
title: "Riesenrad 6"
date: "2004-02-20T12:22:55"
picture: "Riesenrad_6.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Kirmes", "Riesenrad"]
uploadBy: "DerMitDenBitsTanzt"
license: "unknown"
legacy_id:
- details/2176
imported:
- "2019"
_4images_image_id: "2176"
_4images_cat_id: "426"
_4images_user_id: "31"
_4images_image_date: "2004-02-20T12:22:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2176 -->
