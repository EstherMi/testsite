---
layout: "image"
title: "Kitzelrob01.JPG"
date: "2004-02-20T12:22:45"
picture: "Kitzelrob01.jpg"
weight: "1"
konstrukteure: 
- "Frank Linde (DerMitDenBitsTanzt)"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2140
imported:
- "2019"
_4images_image_id: "2140"
_4images_cat_id: "427"
_4images_user_id: "4"
_4images_image_date: "2004-02-20T12:22:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2140 -->
Da isser, der TV-Star (in spe).

Ist schon toll, wenn man mit  6 Freiheitsgraden gekitzelt wird!