---
layout: "image"
title: "Kitzelrob03.JPG"
date: "2004-02-20T12:22:45"
picture: "Kitzelrob03.jpg"
weight: "3"
konstrukteure: 
- "Frank Linde (DerMitDenBitsTanzt)"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2142
imported:
- "2019"
_4images_image_id: "2142"
_4images_cat_id: "427"
_4images_user_id: "4"
_4images_image_date: "2004-02-20T12:22:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2142 -->
Frank macht noch ein paar bessere Aufnahmen, bei mir gab's in den Detailaufnahmen Probleme mit dem Kontrast zwischen dunklem Plastik und glänzendem Alu.