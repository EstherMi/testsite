---
layout: "image"
title: "Veghel_023.jpg"
date: "2006-03-26T15:32:38"
picture: "Veghel_023.jpg"
weight: "2"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5956
imported:
- "2019"
_4images_image_id: "5956"
_4images_cat_id: "517"
_4images_user_id: "4"
_4images_image_date: "2006-03-26T15:32:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5956 -->
