---
layout: "image"
title: "Veghel_007.jpg"
date: "2006-03-26T15:12:52"
picture: "Veghel_007.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Damen"
keywords: ["Fahrgeschäft", "Kirmesmodell", "Frisbee"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5942
imported:
- "2019"
_4images_image_id: "5942"
_4images_cat_id: "516"
_4images_user_id: "4"
_4images_image_date: "2006-03-26T15:12:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5942 -->
