---
layout: "image"
title: "Veghel_008.jpg"
date: "2006-03-26T15:13:49"
picture: "Veghel_008.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Damen"
keywords: ["Fahrgeschäft", "Kirmesmodell", "Frisbee"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5943
imported:
- "2019"
_4images_image_id: "5943"
_4images_cat_id: "516"
_4images_user_id: "4"
_4images_image_date: "2006-03-26T15:13:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5943 -->
