---
layout: "image"
title: "Radwippe1"
date: "2011-08-08T21:55:12"
picture: "IMG_4218.JPG"
weight: "1"
konstrukteure: 
- "NN"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/31552
imported:
- "2019"
_4images_image_id: "31552"
_4images_cat_id: "2123"
_4images_user_id: "4"
_4images_image_date: "2011-08-08T21:55:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31552 -->
