---
layout: "image"
title: "KastenRoboter"
date: "2010-11-17T20:40:39"
picture: "IMGP0172.jpg"
weight: "83"
konstrukteure: 
- "Frederik"
fotografen:
- "lars b."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lars"
license: "unknown"
legacy_id:
- details/29286
imported:
- "2019"
_4images_image_id: "29286"
_4images_cat_id: "2123"
_4images_user_id: "1177"
_4images_image_date: "2010-11-17T20:40:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29286 -->
