---
layout: "image"
title: "Abbau am Ende"
date: "2010-11-17T19:47:54"
picture: "Abbau_2.jpg"
weight: "70"
konstrukteure: 
- "-?-"
fotografen:
- "lars b."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lars"
license: "unknown"
legacy_id:
- details/29273
imported:
- "2019"
_4images_image_id: "29273"
_4images_cat_id: "2123"
_4images_user_id: "1177"
_4images_image_date: "2010-11-17T19:47:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29273 -->
