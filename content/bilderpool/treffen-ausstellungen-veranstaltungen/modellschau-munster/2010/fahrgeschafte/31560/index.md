---
layout: "image"
title: "Music Express 2"
date: "2011-08-08T22:07:32"
picture: "IMG_4244.JPG"
weight: "3"
konstrukteure: 
- "Triceratops"
fotografen:
- "Harald Steinhaus"
keywords: ["Fahrgeschäft"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/31560
imported:
- "2019"
_4images_image_id: "31560"
_4images_cat_id: "2349"
_4images_user_id: "4"
_4images_image_date: "2011-08-08T22:07:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31560 -->
