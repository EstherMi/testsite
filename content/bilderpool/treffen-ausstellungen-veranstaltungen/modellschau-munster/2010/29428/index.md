---
layout: "image"
title: "Brückenleger02"
date: "2010-12-05T19:58:21"
picture: "IMG_4267.JPG"
weight: "11"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/29428
imported:
- "2019"
_4images_image_id: "29428"
_4images_cat_id: "2123"
_4images_user_id: "4"
_4images_image_date: "2010-12-05T19:58:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29428 -->
(Wie man unschwer erkennt: mit meinem neuen und gar so hochgelobten Blitzgerät bin ich noch lange nicht Freund geworden.)