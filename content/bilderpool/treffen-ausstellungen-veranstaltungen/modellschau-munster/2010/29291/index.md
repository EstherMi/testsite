---
layout: "image"
title: "Laufroboter"
date: "2010-11-17T20:50:21"
picture: "Laufroboter.jpg"
weight: "88"
konstrukteure: 
- "-?-"
fotografen:
- "lars b."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lars"
license: "unknown"
legacy_id:
- details/29291
imported:
- "2019"
_4images_image_id: "29291"
_4images_cat_id: "2123"
_4images_user_id: "1177"
_4images_image_date: "2010-11-17T20:50:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29291 -->
