---
layout: "image"
title: "Fußballroboter"
date: "2010-11-17T20:24:50"
picture: "Fuballroboter.jpg"
weight: "80"
konstrukteure: 
- "Carel van Leeuwen"
fotografen:
- "lars b."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lars"
license: "unknown"
legacy_id:
- details/29283
imported:
- "2019"
_4images_image_id: "29283"
_4images_cat_id: "2123"
_4images_user_id: "1177"
_4images_image_date: "2010-11-17T20:24:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29283 -->
