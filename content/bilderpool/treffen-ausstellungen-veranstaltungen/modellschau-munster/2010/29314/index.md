---
layout: "image"
title: "Roboterarm05"
date: "2010-11-20T23:44:10"
picture: "Roboterarm05.jpg"
weight: "62"
konstrukteure: 
- "dmdbt"
fotografen:
- "lars b."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lars"
license: "unknown"
legacy_id:
- details/29314
imported:
- "2019"
_4images_image_id: "29314"
_4images_cat_id: "2123"
_4images_user_id: "1177"
_4images_image_date: "2010-11-20T23:44:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29314 -->
