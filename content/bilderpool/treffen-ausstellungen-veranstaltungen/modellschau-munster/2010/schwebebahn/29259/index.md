---
layout: "image"
title: "Roter Zug auf der Stecke"
date: "2010-11-15T17:29:12"
picture: "schwebebahn08.jpg"
weight: "9"
konstrukteure: 
- "Alteg"
fotografen:
- "Alteg"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Alteg"
license: "unknown"
legacy_id:
- details/29259
imported:
- "2019"
_4images_image_id: "29259"
_4images_cat_id: "2348"
_4images_user_id: "1185"
_4images_image_date: "2010-11-15T17:29:12"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29259 -->
