---
layout: "image"
title: "Roter Zug"
date: "2010-11-15T17:29:12"
picture: "schwebebahn09_2.jpg"
weight: "12"
konstrukteure: 
- "Alteg"
fotografen:
- "Alteg"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Alteg"
license: "unknown"
legacy_id:
- details/29269
imported:
- "2019"
_4images_image_id: "29269"
_4images_cat_id: "2348"
_4images_user_id: "1185"
_4images_image_date: "2010-11-15T17:29:12"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29269 -->
- Steuerung über IR- Control-Set
- Antrieb:1 XM-Motor
- 3 angetriebene Seilrollen