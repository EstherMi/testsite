---
layout: "image"
title: "Abbau am Ende"
date: "2010-11-17T19:47:54"
picture: "Abbau.jpg"
weight: "69"
konstrukteure: 
- "lars b."
fotografen:
- "lars b."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lars"
license: "unknown"
legacy_id:
- details/29272
imported:
- "2019"
_4images_image_id: "29272"
_4images_cat_id: "2123"
_4images_user_id: "1177"
_4images_image_date: "2010-11-17T19:47:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29272 -->
