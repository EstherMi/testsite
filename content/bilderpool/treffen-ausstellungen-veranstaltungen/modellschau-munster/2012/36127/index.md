---
layout: "image"
title: "Martinum Gymnasium"
date: "2012-11-20T21:40:42"
picture: "hbz18.jpg"
weight: "18"
konstrukteure: 
- "-?-"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/36127
imported:
- "2019"
_4images_image_id: "36127"
_4images_cat_id: "2687"
_4images_user_id: "182"
_4images_image_date: "2012-11-20T21:40:42"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36127 -->
