---
layout: "image"
title: "Modelle Brickwedde"
date: "2012-11-20T21:40:42"
picture: "hbz14.jpg"
weight: "14"
konstrukteure: 
- "Wilhelm Brickwedde"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/36123
imported:
- "2019"
_4images_image_id: "36123"
_4images_cat_id: "2687"
_4images_user_id: "182"
_4images_image_date: "2012-11-20T21:40:42"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36123 -->
