---
layout: "image"
title: "Freefall Tower"
date: "2012-11-20T21:40:43"
picture: "hbz29.jpg"
weight: "29"
konstrukteure: 
- "Wilhelm Brickwedde"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/36138
imported:
- "2019"
_4images_image_id: "36138"
_4images_cat_id: "2687"
_4images_user_id: "182"
_4images_image_date: "2012-11-20T21:40:43"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36138 -->
