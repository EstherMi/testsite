---
layout: "image"
title: "KVG Hiltrup"
date: "2012-11-20T21:40:42"
picture: "hbz15.jpg"
weight: "15"
konstrukteure: 
- "KVG Hiltrup"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/36124
imported:
- "2019"
_4images_image_id: "36124"
_4images_cat_id: "2687"
_4images_user_id: "182"
_4images_image_date: "2012-11-20T21:40:42"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36124 -->
