---
layout: "image"
title: "Tresor"
date: "2012-11-20T21:40:43"
picture: "hbz53.jpg"
weight: "53"
konstrukteure: 
- "Dominik Tacke"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/36162
imported:
- "2019"
_4images_image_id: "36162"
_4images_cat_id: "2687"
_4images_user_id: "182"
_4images_image_date: "2012-11-20T21:40:43"
_4images_image_order: "53"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36162 -->
