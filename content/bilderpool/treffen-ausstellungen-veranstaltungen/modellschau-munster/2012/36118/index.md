---
layout: "image"
title: "Robi und Rennbahn"
date: "2012-11-20T21:40:42"
picture: "hbz09.jpg"
weight: "9"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/36118
imported:
- "2019"
_4images_image_id: "36118"
_4images_cat_id: "2687"
_4images_user_id: "182"
_4images_image_date: "2012-11-20T21:40:42"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36118 -->
