---
layout: "image"
title: "die Kuchentheke noch leer"
date: "2012-11-20T21:40:43"
picture: "hbz21.jpg"
weight: "21"
konstrukteure: 
- "-?-"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/36130
imported:
- "2019"
_4images_image_id: "36130"
_4images_cat_id: "2687"
_4images_user_id: "182"
_4images_image_date: "2012-11-20T21:40:43"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36130 -->
