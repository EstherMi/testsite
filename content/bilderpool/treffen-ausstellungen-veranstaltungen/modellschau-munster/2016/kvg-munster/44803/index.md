---
layout: "image"
title: "Lemkamp"
date: "2016-11-28T17:06:39"
picture: "modellschaumuenster27.jpg"
weight: "27"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Higgsteilchen"
license: "unknown"
legacy_id:
- details/44803
imported:
- "2019"
_4images_image_id: "44803"
_4images_cat_id: "3339"
_4images_user_id: "2670"
_4images_image_date: "2016-11-28T17:06:39"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44803 -->
