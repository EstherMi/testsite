---
layout: "image"
title: "Brockhaus"
date: "2016-11-28T17:06:39"
picture: "modellschaumuenster06.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Higgsteilchen"
license: "unknown"
legacy_id:
- details/44782
imported:
- "2019"
_4images_image_id: "44782"
_4images_cat_id: "3339"
_4images_user_id: "2670"
_4images_image_date: "2016-11-28T17:06:39"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44782 -->
