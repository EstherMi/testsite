---
layout: "image"
title: "Schwebebahndepot mit Wendeschleife"
date: "2015-01-16T08:13:18"
picture: "schwebebahnalteg05.jpg"
weight: "5"
konstrukteure: 
- "Gereon Altenbeck"
fotografen:
- "Gereon Altenbeck"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Alteg"
license: "unknown"
legacy_id:
- details/40343
imported:
- "2019"
_4images_image_id: "40343"
_4images_cat_id: "3025"
_4images_user_id: "1185"
_4images_image_date: "2015-01-16T08:13:18"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40343 -->
Schwebebahndepot mit Wendeschleife