---
layout: "image"
title: "Schwebebahnstation 2"
date: "2015-01-16T08:13:18"
picture: "schwebebahnalteg07.jpg"
weight: "7"
konstrukteure: 
- "Gereon Altenbeck"
fotografen:
- "Gereon Altenbeck"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Alteg"
license: "unknown"
legacy_id:
- details/40345
imported:
- "2019"
_4images_image_id: "40345"
_4images_cat_id: "3025"
_4images_user_id: "1185"
_4images_image_date: "2015-01-16T08:13:18"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40345 -->
Schwebebahnstation 2