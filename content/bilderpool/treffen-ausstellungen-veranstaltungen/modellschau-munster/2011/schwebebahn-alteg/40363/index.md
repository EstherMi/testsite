---
layout: "image"
title: "Schwebebahndepot mit Wendeschleife"
date: "2015-01-16T08:13:18"
picture: "schwebebahnalteg25.jpg"
weight: "25"
konstrukteure: 
- "Gereon Altenbeck"
fotografen:
- "Gereon Altenbeck"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Alteg"
license: "unknown"
legacy_id:
- details/40363
imported:
- "2019"
_4images_image_id: "40363"
_4images_cat_id: "3025"
_4images_user_id: "1185"
_4images_image_date: "2015-01-16T08:13:18"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40363 -->
Schwebebahndepot mit Wendeschleife