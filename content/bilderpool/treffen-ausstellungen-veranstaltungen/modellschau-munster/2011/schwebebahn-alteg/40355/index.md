---
layout: "image"
title: "Drehscheibe Verriegelung"
date: "2015-01-16T08:13:18"
picture: "schwebebahnalteg17.jpg"
weight: "17"
konstrukteure: 
- "Gereon Altenbeck"
fotografen:
- "Gereon Altenbeck"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Alteg"
license: "unknown"
legacy_id:
- details/40355
imported:
- "2019"
_4images_image_id: "40355"
_4images_cat_id: "3025"
_4images_user_id: "1185"
_4images_image_date: "2015-01-16T08:13:18"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40355 -->
Verriegelung der Drehscheibe mit Pneumatikzylindern