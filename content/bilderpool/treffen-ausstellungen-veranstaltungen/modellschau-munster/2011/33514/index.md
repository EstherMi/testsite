---
layout: "image"
title: "Münster 2011"
date: "2011-11-14T09:36:17"
picture: "fischertechnikmodellschau32.jpg"
weight: "32"
konstrukteure: 
- "Marcus Wolf"
fotografen:
- "Fredy"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/33514
imported:
- "2019"
_4images_image_id: "33514"
_4images_cat_id: "2479"
_4images_user_id: "453"
_4images_image_date: "2011-11-14T09:36:17"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33514 -->
