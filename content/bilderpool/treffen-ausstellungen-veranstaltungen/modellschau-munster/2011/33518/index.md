---
layout: "image"
title: "Münster 2011"
date: "2011-11-14T09:36:17"
picture: "fischertechnikmodellschau36.jpg"
weight: "36"
konstrukteure: 
- "Ludger"
fotografen:
- "Fredy"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/33518
imported:
- "2019"
_4images_image_id: "33518"
_4images_cat_id: "2479"
_4images_user_id: "453"
_4images_image_date: "2011-11-14T09:36:17"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33518 -->
