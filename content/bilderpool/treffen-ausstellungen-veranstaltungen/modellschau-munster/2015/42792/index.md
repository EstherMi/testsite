---
layout: "image"
title: "Modellschau Münster 22.11.15"
date: "2016-01-26T22:16:51"
picture: "muenster52.jpg"
weight: "52"
konstrukteure: 
- "Triceratops"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/42792
imported:
- "2019"
_4images_image_id: "42792"
_4images_cat_id: "3124"
_4images_user_id: "182"
_4images_image_date: "2016-01-26T22:16:51"
_4images_image_order: "52"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42792 -->
