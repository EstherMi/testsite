---
layout: "image"
title: "muenster77.jpg"
date: "2016-01-26T22:16:51"
picture: "muenster77.jpg"
weight: "77"
konstrukteure: 
- "-?-"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/42817
imported:
- "2019"
_4images_image_id: "42817"
_4images_cat_id: "3124"
_4images_user_id: "182"
_4images_image_date: "2016-01-26T22:16:51"
_4images_image_order: "77"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42817 -->
