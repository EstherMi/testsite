---
layout: "image"
title: "Brückenkran"
date: "2015-11-28T11:42:24"
picture: "muenster17.jpg"
weight: "18"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/42414
imported:
- "2019"
_4images_image_id: "42414"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42414 -->
Segment für Brücke