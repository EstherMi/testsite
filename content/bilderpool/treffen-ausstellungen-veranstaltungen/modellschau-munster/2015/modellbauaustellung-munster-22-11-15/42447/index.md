---
layout: "image"
title: "6-Achs Roboterarm Encoder"
date: "2015-11-28T11:42:24"
picture: "muenster50.jpg"
weight: "51"
konstrukteure: 
- "Frank Linde"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/42447
imported:
- "2019"
_4images_image_id: "42447"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "50"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42447 -->
