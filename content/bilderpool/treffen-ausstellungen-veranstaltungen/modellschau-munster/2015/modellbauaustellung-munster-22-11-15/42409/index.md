---
layout: "image"
title: "fischertechnik Industrie Trainingsmodelle Heft"
date: "2015-11-28T11:42:24"
picture: "muenster12.jpg"
weight: "13"
konstrukteure: 
- "fischertechnik"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/42409
imported:
- "2019"
_4images_image_id: "42409"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42409 -->
Hochregallager. Heft von Dirk Kutsch.