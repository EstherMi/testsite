---
layout: "image"
title: "Turbine Nachbau"
date: "2015-11-28T11:42:24"
picture: "muenster68.jpg"
weight: "69"
konstrukteure: 
- "-?-"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/42465
imported:
- "2019"
_4images_image_id: "42465"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "68"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42465 -->
