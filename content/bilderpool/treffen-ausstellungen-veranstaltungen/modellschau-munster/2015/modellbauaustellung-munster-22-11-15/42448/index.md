---
layout: "image"
title: "6-Achs Roboterarm Greifer"
date: "2015-11-28T11:42:24"
picture: "muenster51.jpg"
weight: "52"
konstrukteure: 
- "Frank Linde"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/42448
imported:
- "2019"
_4images_image_id: "42448"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "51"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42448 -->
