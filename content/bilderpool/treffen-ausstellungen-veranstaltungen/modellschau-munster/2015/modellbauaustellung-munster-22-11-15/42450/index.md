---
layout: "image"
title: "Sonderteile"
date: "2015-11-28T11:42:24"
picture: "muenster53.jpg"
weight: "54"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/42450
imported:
- "2019"
_4images_image_id: "42450"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "53"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42450 -->
