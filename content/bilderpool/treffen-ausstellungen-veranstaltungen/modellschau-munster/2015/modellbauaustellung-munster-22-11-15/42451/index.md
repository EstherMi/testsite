---
layout: "image"
title: "Pong-Spiel"
date: "2015-11-28T11:42:24"
picture: "muenster54.jpg"
weight: "55"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/42451
imported:
- "2019"
_4images_image_id: "42451"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "54"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42451 -->
