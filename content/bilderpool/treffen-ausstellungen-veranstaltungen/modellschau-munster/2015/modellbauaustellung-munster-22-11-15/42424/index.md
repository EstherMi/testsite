---
layout: "image"
title: "Glücksspielautomat seitlich"
date: "2015-11-28T11:42:24"
picture: "muenster27.jpg"
weight: "28"
konstrukteure: 
- "-?-"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/42424
imported:
- "2019"
_4images_image_id: "42424"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42424 -->
