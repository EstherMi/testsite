---
layout: "image"
title: "Glücksrad Detail"
date: "2015-11-28T11:42:24"
picture: "muenster38.jpg"
weight: "39"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/42435
imported:
- "2019"
_4images_image_id: "42435"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42435 -->
