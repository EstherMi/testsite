---
layout: "image"
title: "fischertechnik Industrie Trainingsmodelle Heft"
date: "2015-11-28T11:42:24"
picture: "muenster13.jpg"
weight: "14"
konstrukteure: 
- "fischertechnik"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/42410
imported:
- "2019"
_4images_image_id: "42410"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42410 -->
Industriemodell. Heft von Dirk Kutsch