---
layout: "image"
title: "Glücksspiel"
date: "2015-11-28T11:42:24"
picture: "muenster39.jpg"
weight: "40"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/42436
imported:
- "2019"
_4images_image_id: "42436"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42436 -->
Man bekommt aus einem Magazin weisse und schwarze Steine.