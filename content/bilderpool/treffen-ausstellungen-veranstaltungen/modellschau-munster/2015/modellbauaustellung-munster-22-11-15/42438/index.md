---
layout: "image"
title: "Glücksspiel mit Raupe"
date: "2015-11-28T11:42:24"
picture: "muenster41.jpg"
weight: "42"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/42438
imported:
- "2019"
_4images_image_id: "42438"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42438 -->
Die Raupe fährt vor und zurück. Wenn ein weißer Stein in den Ausgabeschacht fällt,
dann gewinnt man ein Goodie.