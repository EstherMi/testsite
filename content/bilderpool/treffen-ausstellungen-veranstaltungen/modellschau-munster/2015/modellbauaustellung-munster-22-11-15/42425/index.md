---
layout: "image"
title: "Glücksspielautomat hinten"
date: "2015-11-28T11:42:24"
picture: "muenster28.jpg"
weight: "29"
konstrukteure: 
- "-?-"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/42425
imported:
- "2019"
_4images_image_id: "42425"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42425 -->
