---
layout: "image"
title: "Bearbeitungszentrum Steuerung"
date: "2015-11-28T11:42:24"
picture: "muenster46.jpg"
weight: "47"
konstrukteure: 
- "Heinrich Fuchs"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/42443
imported:
- "2019"
_4images_image_id: "42443"
_4images_cat_id: "3156"
_4images_user_id: "2303"
_4images_image_date: "2015-11-28T11:42:24"
_4images_image_order: "46"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42443 -->
