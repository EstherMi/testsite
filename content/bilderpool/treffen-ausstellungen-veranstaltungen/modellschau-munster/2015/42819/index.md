---
layout: "image"
title: "Modellschau Münster 22.11.15 Abbau"
date: "2016-01-26T22:16:51"
picture: "muenster79.jpg"
weight: "79"
konstrukteure: 
- "-?-"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/42819
imported:
- "2019"
_4images_image_id: "42819"
_4images_cat_id: "3124"
_4images_user_id: "182"
_4images_image_date: "2016-01-26T22:16:51"
_4images_image_order: "79"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42819 -->
