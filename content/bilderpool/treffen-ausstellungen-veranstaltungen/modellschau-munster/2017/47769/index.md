---
layout: "image"
title: "kvgftmodellschau05.jpg"
date: "2018-08-22T19:49:19"
picture: "kvgftmodellschau05.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "Kai Baumgart"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Higgsteilchen"
license: "unknown"
legacy_id:
- details/47769
imported:
- "2019"
_4images_image_id: "47769"
_4images_cat_id: "3528"
_4images_user_id: "2670"
_4images_image_date: "2018-08-22T19:49:19"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47769 -->
