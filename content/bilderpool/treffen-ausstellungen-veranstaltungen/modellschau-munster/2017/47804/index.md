---
layout: "image"
title: "kvgftmodellschau40.jpg"
date: "2018-08-22T19:49:20"
picture: "kvgftmodellschau40.jpg"
weight: "40"
konstrukteure: 
- "-?-"
fotografen:
- "Kai Baumgart"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Higgsteilchen"
license: "unknown"
legacy_id:
- details/47804
imported:
- "2019"
_4images_image_id: "47804"
_4images_cat_id: "3528"
_4images_user_id: "2670"
_4images_image_date: "2018-08-22T19:49:20"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47804 -->
