---
layout: "image"
title: "kvgftmodellschau50.jpg"
date: "2018-08-22T19:49:20"
picture: "kvgftmodellschau50.jpg"
weight: "50"
konstrukteure: 
- "-?-"
fotografen:
- "Kai Baumgart"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Higgsteilchen"
license: "unknown"
legacy_id:
- details/47814
imported:
- "2019"
_4images_image_id: "47814"
_4images_cat_id: "3528"
_4images_user_id: "2670"
_4images_image_date: "2018-08-22T19:49:20"
_4images_image_order: "50"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47814 -->
