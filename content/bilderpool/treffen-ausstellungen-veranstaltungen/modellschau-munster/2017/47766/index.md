---
layout: "image"
title: "kvgftmodellschau02.jpg"
date: "2018-08-22T19:49:19"
picture: "kvgftmodellschau02.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Kai Baumgart"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Higgsteilchen"
license: "unknown"
legacy_id:
- details/47766
imported:
- "2019"
_4images_image_id: "47766"
_4images_cat_id: "3528"
_4images_user_id: "2670"
_4images_image_date: "2018-08-22T19:49:19"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47766 -->
