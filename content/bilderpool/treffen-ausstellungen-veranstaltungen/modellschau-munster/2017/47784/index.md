---
layout: "image"
title: "kvgftmodellschau20.jpg"
date: "2018-08-22T19:49:20"
picture: "kvgftmodellschau20.jpg"
weight: "20"
konstrukteure: 
- "-?-"
fotografen:
- "Kai Baumgart"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Higgsteilchen"
license: "unknown"
legacy_id:
- details/47784
imported:
- "2019"
_4images_image_id: "47784"
_4images_cat_id: "3528"
_4images_user_id: "2670"
_4images_image_date: "2018-08-22T19:49:20"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47784 -->
