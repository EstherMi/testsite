---
layout: "image"
title: "Der neue Robot"
date: "2007-02-03T00:28:46"
picture: "toyfair06.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "Aki-kun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Aki-kun"
license: "unknown"
legacy_id:
- details/8794
imported:
- "2019"
_4images_image_id: "8794"
_4images_cat_id: "802"
_4images_user_id: "508"
_4images_image_date: "2007-02-03T00:28:46"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8794 -->
Vorne gut zu erkennen der Abstandssensor.
Reichweite 3cm bis 5m
Übergibt den Wert als cm zum Interface