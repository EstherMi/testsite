---
layout: "image"
title: "Die RC-Cars"
date: "2007-02-03T00:28:46"
picture: "toyfair02.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "Aki-kun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Aki-kun"
license: "unknown"
legacy_id:
- details/8790
imported:
- "2019"
_4images_image_id: "8790"
_4images_cat_id: "802"
_4images_user_id: "508"
_4images_image_date: "2007-02-03T00:28:46"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8790 -->
In dem Paket sind Hindernisse, Start und Ziel, eine Zielfigur mit Flagge, Ein Auto und Fernbedienung.

Das Auto besteht aus einem Chassi mit dem man ein paar wenige Modelle bauen kann.