---
layout: "image"
title: "Chassis von unten"
date: "2007-02-03T00:28:46"
picture: "toyfair03.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "Aki-kun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Aki-kun"
license: "unknown"
legacy_id:
- details/8791
imported:
- "2019"
_4images_image_id: "8791"
_4images_cat_id: "802"
_4images_user_id: "508"
_4images_image_date: "2007-02-03T00:28:46"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8791 -->
Leider massiv unscharf. Mußte mich grad beeilen und dann vergessen nochmal zu machen.

Man erkennt den "Servo". Die Innereien sind nicht zerlegbar. Zumindest nicht dazu gedacht.