---
layout: "image"
title: "Robo explorer"
date: "2007-02-05T17:49:54"
picture: "100_3424.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "tomak"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tomak"
license: "unknown"
legacy_id:
- details/8891
imported:
- "2019"
_4images_image_id: "8891"
_4images_cat_id: "802"
_4images_user_id: "548"
_4images_image_date: "2007-02-05T17:49:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8891 -->
