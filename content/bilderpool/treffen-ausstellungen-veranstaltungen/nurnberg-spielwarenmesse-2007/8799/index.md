---
layout: "image"
title: "FischerTipp 2"
date: "2007-02-03T12:02:40"
picture: "toyfair11.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "Aki-kun"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Aki-kun"
license: "unknown"
legacy_id:
- details/8799
imported:
- "2019"
_4images_image_id: "8799"
_4images_cat_id: "802"
_4images_user_id: "508"
_4images_image_date: "2007-02-03T12:02:40"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8799 -->
