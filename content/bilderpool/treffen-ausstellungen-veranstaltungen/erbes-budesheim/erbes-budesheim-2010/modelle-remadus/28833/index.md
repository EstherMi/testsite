---
layout: "image"
title: "Martin Romann"
date: "2010-10-02T23:55:12"
picture: "erbesbudesheim06.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Peter, Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/28833
imported:
- "2019"
_4images_image_id: "28833"
_4images_cat_id: "2050"
_4images_user_id: "22"
_4images_image_date: "2010-10-02T23:55:12"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28833 -->
Standuhr