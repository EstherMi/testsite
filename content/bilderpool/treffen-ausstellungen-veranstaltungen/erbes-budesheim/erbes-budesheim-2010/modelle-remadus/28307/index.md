---
layout: "image"
title: "Pendeluhr"
date: "2010-09-26T16:07:13"
picture: "eb25.jpg"
weight: "29"
konstrukteure: 
- "Remadus"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/28307
imported:
- "2019"
_4images_image_id: "28307"
_4images_cat_id: "2050"
_4images_user_id: "558"
_4images_image_date: "2010-09-26T16:07:13"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28307 -->
