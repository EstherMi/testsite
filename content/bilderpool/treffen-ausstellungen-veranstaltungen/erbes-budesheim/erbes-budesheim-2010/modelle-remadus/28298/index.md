---
layout: "image"
title: "Pendeluhr"
date: "2010-09-26T16:06:54"
picture: "eb16.jpg"
weight: "27"
konstrukteure: 
- "Remadus"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/28298
imported:
- "2019"
_4images_image_id: "28298"
_4images_cat_id: "2050"
_4images_user_id: "558"
_4images_image_date: "2010-09-26T16:06:54"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28298 -->
