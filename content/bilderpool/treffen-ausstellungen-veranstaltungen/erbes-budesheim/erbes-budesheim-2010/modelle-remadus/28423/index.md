---
layout: "image"
title: "Standuhr"
date: "2010-09-27T18:07:26"
picture: "ser5.jpg"
weight: "11"
konstrukteure: 
- "remadus"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/28423
imported:
- "2019"
_4images_image_id: "28423"
_4images_cat_id: "2050"
_4images_user_id: "1162"
_4images_image_date: "2010-09-27T18:07:26"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28423 -->
