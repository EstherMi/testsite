---
layout: "image"
title: "Hochregallager"
date: "2010-09-27T19:56:20"
picture: "fischertechnikconventioninerbesbuedesheim036.jpg"
weight: "3"
konstrukteure: 
- "fitec"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28459
imported:
- "2019"
_4images_image_id: "28459"
_4images_cat_id: "2075"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:20"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28459 -->
