---
layout: "image"
title: "Residents Villa Frank, my workplace (3)"
date: "2010-09-27T23:33:15"
picture: "msrds03.jpg"
weight: "3"
konstrukteure: 
- "Carel van Leeuwen"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/28659
imported:
- "2019"
_4images_image_id: "28659"
_4images_cat_id: "2072"
_4images_user_id: "136"
_4images_image_date: "2010-09-27T23:33:15"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28659 -->
The models I took with me