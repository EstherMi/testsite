---
layout: "image"
title: "Lovely Koblenz"
date: "2010-09-27T23:33:15"
picture: "msrds28.jpg"
weight: "28"
konstrukteure: 
- "Carel van Leeuwen"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/28684
imported:
- "2019"
_4images_image_id: "28684"
_4images_cat_id: "2072"
_4images_user_id: "136"
_4images_image_date: "2010-09-27T23:33:15"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28684 -->
Our trip back to Enschede