---
layout: "image"
title: "Presentation about MS-RDS and fischertechnik interfaces"
date: "2010-09-27T23:33:15"
picture: "msrds04.jpg"
weight: "4"
konstrukteure: 
- "Carel van Leeuwen"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/28660
imported:
- "2019"
_4images_image_id: "28660"
_4images_cat_id: "2072"
_4images_user_id: "136"
_4images_image_date: "2010-09-27T23:33:15"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28660 -->
Carel is busy with the setup