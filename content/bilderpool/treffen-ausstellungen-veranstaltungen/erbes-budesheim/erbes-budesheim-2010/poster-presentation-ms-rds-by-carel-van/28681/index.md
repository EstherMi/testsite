---
layout: "image"
title: "The roller coaster"
date: "2010-09-27T23:33:15"
picture: "msrds25.jpg"
weight: "25"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/28681
imported:
- "2019"
_4images_image_id: "28681"
_4images_cat_id: "2072"
_4images_user_id: "136"
_4images_image_date: "2010-09-27T23:33:15"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28681 -->
The roller coaster or big dipper