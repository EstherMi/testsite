---
layout: "image"
title: "The French corner (3)"
date: "2010-09-27T23:33:15"
picture: "msrds21.jpg"
weight: "21"
konstrukteure: 
- "Rei_vilo"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/28677
imported:
- "2019"
_4images_image_id: "28677"
_4images_cat_id: "2072"
_4images_user_id: "136"
_4images_image_date: "2010-09-27T23:33:15"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28677 -->
Parlez-vous aussi Francais?