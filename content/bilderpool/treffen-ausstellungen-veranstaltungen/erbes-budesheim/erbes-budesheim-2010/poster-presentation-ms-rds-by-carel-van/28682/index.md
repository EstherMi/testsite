---
layout: "image"
title: "Ft-convention Impression"
date: "2010-09-27T23:33:15"
picture: "msrds26.jpg"
weight: "26"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/28682
imported:
- "2019"
_4images_image_id: "28682"
_4images_cat_id: "2072"
_4images_user_id: "136"
_4images_image_date: "2010-09-27T23:33:15"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28682 -->
Impression of the participators