---
layout: "image"
title: "Presentation about MS-RDS and fischertechnik interfaces"
date: "2010-09-27T23:33:15"
picture: "msrds07.jpg"
weight: "7"
konstrukteure: 
- "Carel van Leeuwen"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/28663
imported:
- "2019"
_4images_image_id: "28663"
_4images_cat_id: "2072"
_4images_user_id: "136"
_4images_image_date: "2010-09-27T23:33:15"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28663 -->
The example model.
Robo Explorer controlled by:
-) WIImote (WIIcontroller)
-) Xbox360 controller
-) fischertechnik IrDa controller (old model)

Also the Radio Call mechanism is fully implemented.