---
layout: "image"
title: "The tip-Company"
date: "2010-09-27T19:56:20"
picture: "fischertechnikconventioninerbesbuedesheim045.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28468
imported:
- "2019"
_4images_image_id: "28468"
_4images_cat_id: "2074"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:20"
_4images_image_order: "45"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28468 -->
