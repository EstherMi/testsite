---
layout: "image"
title: "Spiegelteleskop"
date: "2010-09-26T12:23:16"
picture: "spiegelteleskop1.jpg"
weight: "5"
konstrukteure: 
- "Knobloch"
fotografen:
- "Heiko"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/28271
imported:
- "2019"
_4images_image_id: "28271"
_4images_cat_id: "2053"
_4images_user_id: "453"
_4images_image_date: "2010-09-26T12:23:16"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28271 -->
