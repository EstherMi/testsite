---
layout: "image"
title: "Spiegelteleskop"
date: "2010-09-27T19:56:23"
picture: "fischertechnikconventioninerbesbuedesheim100.jpg"
weight: "1"
konstrukteure: 
- "Fa. Knobloch"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28523
imported:
- "2019"
_4images_image_id: "28523"
_4images_cat_id: "2053"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:23"
_4images_image_order: "100"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28523 -->
