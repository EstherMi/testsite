---
layout: "image"
title: "Der Aufgang zum Teleskop"
date: "2010-09-27T19:56:24"
picture: "fischertechnikconventioninerbesbuedesheim103.jpg"
weight: "4"
konstrukteure: 
- "Fa. Knobloch"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28526
imported:
- "2019"
_4images_image_id: "28526"
_4images_cat_id: "2053"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:24"
_4images_image_order: "103"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28526 -->
