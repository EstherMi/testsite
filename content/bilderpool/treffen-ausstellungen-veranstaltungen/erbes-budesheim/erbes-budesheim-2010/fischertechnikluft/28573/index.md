---
layout: "image"
title: "Andreas Gürten (Laserman)"
date: "2010-09-27T19:56:25"
picture: "fischertechnikconventioninerbesbuedesheim150.jpg"
weight: "22"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28573
imported:
- "2019"
_4images_image_id: "28573"
_4images_cat_id: "2063"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:25"
_4images_image_order: "150"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28573 -->
