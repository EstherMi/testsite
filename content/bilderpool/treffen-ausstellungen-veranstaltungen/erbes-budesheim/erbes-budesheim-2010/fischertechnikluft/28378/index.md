---
layout: "image"
title: "Thomas Kaisers Mikrokopter"
date: "2010-09-26T19:45:22"
picture: "fischertechnikluft13.jpg"
weight: "41"
konstrukteure: 
- "-?-"
fotografen:
- "Christian Knobloch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "C-Knobloch"
license: "unknown"
legacy_id:
- details/28378
imported:
- "2019"
_4images_image_id: "28378"
_4images_cat_id: "2063"
_4images_user_id: "997"
_4images_image_date: "2010-09-26T19:45:22"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28378 -->
