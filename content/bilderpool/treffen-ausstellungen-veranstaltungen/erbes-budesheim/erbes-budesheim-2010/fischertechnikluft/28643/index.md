---
layout: "image"
title: "Thomas Falkenberg und Michael Sengschmied"
date: "2010-09-27T19:56:26"
picture: "fischertechnikconventioninerbesbuedesheim220.jpg"
weight: "25"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28643
imported:
- "2019"
_4images_image_id: "28643"
_4images_cat_id: "2063"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:26"
_4images_image_order: "220"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28643 -->
