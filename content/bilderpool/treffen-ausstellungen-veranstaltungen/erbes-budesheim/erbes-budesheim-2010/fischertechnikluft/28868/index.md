---
layout: "image"
title: "FT-Luft-Aufnahme"
date: "2010-10-02T23:55:13"
picture: "erbesbudesheim41.jpg"
weight: "5"
konstrukteure: 
- "Thomas Kaijser"
fotografen:
- "Peter, Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/28868
imported:
- "2019"
_4images_image_id: "28868"
_4images_cat_id: "2063"
_4images_user_id: "22"
_4images_image_date: "2010-10-02T23:55:13"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28868 -->
FT-Luft-Aufnahme