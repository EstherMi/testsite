---
layout: "image"
title: "FT-Luft-Aufnahme"
date: "2010-10-02T23:55:13"
picture: "erbesbudesheim40.jpg"
weight: "4"
konstrukteure: 
- "Thomas Kaijser"
fotografen:
- "Peter, Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/28867
imported:
- "2019"
_4images_image_id: "28867"
_4images_cat_id: "2063"
_4images_user_id: "22"
_4images_image_date: "2010-10-02T23:55:13"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28867 -->
FT-Luft-Aufnahme