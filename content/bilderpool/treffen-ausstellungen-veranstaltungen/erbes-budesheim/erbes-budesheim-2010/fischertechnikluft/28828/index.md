---
layout: "image"
title: "FT-Luft in der nähe Erbes-Budesheim entlang der Rhein"
date: "2010-10-02T23:55:12"
picture: "erbesbudesheim01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Peter, Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/28828
imported:
- "2019"
_4images_image_id: "28828"
_4images_cat_id: "2063"
_4images_user_id: "22"
_4images_image_date: "2010-10-02T23:55:12"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28828 -->
FT-Luft in der nähe Erbes-Budesheim entlang der Rhein