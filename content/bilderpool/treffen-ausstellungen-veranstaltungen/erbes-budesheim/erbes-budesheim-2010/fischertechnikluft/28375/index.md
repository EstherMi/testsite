---
layout: "image"
title: "fachsimpeln..."
date: "2010-09-26T19:45:22"
picture: "fischertechnikluft10.jpg"
weight: "38"
konstrukteure: 
- "-?-"
fotografen:
- "Christian Knobloch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "C-Knobloch"
license: "unknown"
legacy_id:
- details/28375
imported:
- "2019"
_4images_image_id: "28375"
_4images_cat_id: "2063"
_4images_user_id: "997"
_4images_image_date: "2010-09-26T19:45:22"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28375 -->
...über 3D Technik

Herr Dr. Kreft hatte eine 3D Kamera und hat damit auch Fotos gemacht. Leider hatte ich zufälliger weise keinen 3D Bildschirm dabei, sonst hätte man sich die auch betrachten können *g*

Sehr interessant was es alles gibt...