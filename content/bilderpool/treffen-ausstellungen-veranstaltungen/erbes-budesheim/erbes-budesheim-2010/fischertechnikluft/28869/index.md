---
layout: "image"
title: "FT-Luft-Aufnahme"
date: "2010-10-02T23:55:13"
picture: "erbesbudesheim42.jpg"
weight: "6"
konstrukteure: 
- "Thomas Kaijser"
fotografen:
- "Peter, Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/28869
imported:
- "2019"
_4images_image_id: "28869"
_4images_cat_id: "2063"
_4images_user_id: "22"
_4images_image_date: "2010-10-02T23:55:13"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28869 -->
FT-Luft-Aufnahme