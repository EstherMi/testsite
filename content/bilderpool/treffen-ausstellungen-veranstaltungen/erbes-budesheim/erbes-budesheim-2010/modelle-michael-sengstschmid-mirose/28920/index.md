---
layout: "image"
title: "greed is good"
date: "2010-10-03T15:00:57"
picture: "APP-2010-025031.jpg"
weight: "2"
konstrukteure: 
- "Michael Sengstschmid (mirose)"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/28920
imported:
- "2019"
_4images_image_id: "28920"
_4images_cat_id: "2068"
_4images_user_id: "371"
_4images_image_date: "2010-10-03T15:00:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28920 -->
