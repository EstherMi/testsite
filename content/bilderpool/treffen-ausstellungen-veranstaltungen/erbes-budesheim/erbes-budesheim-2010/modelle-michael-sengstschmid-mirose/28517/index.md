---
layout: "image"
title: "Auffangbehälter des Münzensortierers"
date: "2010-09-27T19:56:22"
picture: "fischertechnikconventioninerbesbuedesheim094.jpg"
weight: "11"
konstrukteure: 
- "Michael Sengschmied"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28517
imported:
- "2019"
_4images_image_id: "28517"
_4images_cat_id: "2068"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:22"
_4images_image_order: "94"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28517 -->
