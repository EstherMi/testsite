---
layout: "image"
title: "Querschnitt durch die Ausstellung"
date: "2010-09-27T19:56:21"
picture: "fischertechnikconventioninerbesbuedesheim064.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28487
imported:
- "2019"
_4images_image_id: "28487"
_4images_cat_id: "2068"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:21"
_4images_image_order: "64"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28487 -->
