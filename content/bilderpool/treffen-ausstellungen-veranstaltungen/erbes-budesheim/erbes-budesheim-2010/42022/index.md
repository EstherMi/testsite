---
layout: "image"
title: "Link zum Video von der FT-Convention 2010"
date: "2015-10-01T18:18:47"
picture: "Video_von_der_FT-Convention_2010.png"
weight: "1"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/42022
imported:
- "2019"
_4images_image_id: "42022"
_4images_cat_id: "2048"
_4images_user_id: "724"
_4images_image_date: "2015-10-01T18:18:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42022 -->
Video von der FT-Convention 2020 in Erbes-Büdesheim:
https://www.youtube.com/watch?v=eH0fe2Yiwnw