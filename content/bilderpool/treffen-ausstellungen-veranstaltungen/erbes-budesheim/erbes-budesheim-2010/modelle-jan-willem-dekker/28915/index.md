---
layout: "image"
title: "..."
date: "2010-10-03T15:00:56"
picture: "APP-2010-025028.jpg"
weight: "2"
konstrukteure: 
- "Jan Willem Dekker"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/28915
imported:
- "2019"
_4images_image_id: "28915"
_4images_cat_id: "2080"
_4images_user_id: "371"
_4images_image_date: "2010-10-03T15:00:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28915 -->
