---
layout: "image"
title: "Verschiedene Trucks"
date: "2010-09-27T19:56:20"
picture: "fischertechnikconventioninerbesbuedesheim023.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28446
imported:
- "2019"
_4images_image_id: "28446"
_4images_cat_id: "2080"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:20"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28446 -->
