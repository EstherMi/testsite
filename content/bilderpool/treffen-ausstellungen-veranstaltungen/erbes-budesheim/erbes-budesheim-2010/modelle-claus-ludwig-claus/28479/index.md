---
layout: "image"
title: "Drehmaschine"
date: "2010-09-27T19:56:21"
picture: "fischertechnikconventioninerbesbuedesheim056.jpg"
weight: "9"
konstrukteure: 
- "Claus Ludwig (claus)"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28479
imported:
- "2019"
_4images_image_id: "28479"
_4images_cat_id: "2061"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:21"
_4images_image_order: "56"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28479 -->
