---
layout: "image"
title: "Müllwagen mit Müllbehältern"
date: "2010-09-27T19:56:26"
picture: "fischertechnikconventioninerbesbuedesheim203.jpg"
weight: "14"
konstrukteure: 
- "Claus Ludwig"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28626
imported:
- "2019"
_4images_image_id: "28626"
_4images_cat_id: "2061"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:26"
_4images_image_order: "203"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28626 -->
