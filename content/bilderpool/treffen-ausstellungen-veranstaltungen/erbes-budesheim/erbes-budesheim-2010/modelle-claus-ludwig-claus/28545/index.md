---
layout: "image"
title: "Truck"
date: "2010-09-27T19:56:24"
picture: "fischertechnikconventioninerbesbuedesheim122.jpg"
weight: "11"
konstrukteure: 
- "Claus Ludwig (claus)"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28545
imported:
- "2019"
_4images_image_id: "28545"
_4images_cat_id: "2061"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:24"
_4images_image_order: "122"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28545 -->
