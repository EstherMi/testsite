---
layout: "image"
title: "Führerhaus"
date: "2010-09-27T19:56:21"
picture: "fischertechnikconventioninerbesbuedesheim054.jpg"
weight: "7"
konstrukteure: 
- "Claus Ludwig (claus)"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28477
imported:
- "2019"
_4images_image_id: "28477"
_4images_cat_id: "2061"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:21"
_4images_image_order: "54"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28477 -->
