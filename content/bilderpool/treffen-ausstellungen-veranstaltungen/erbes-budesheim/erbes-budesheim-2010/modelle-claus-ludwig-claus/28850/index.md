---
layout: "image"
title: "Claus Ludwig"
date: "2010-10-02T23:55:13"
picture: "erbesbudesheim23.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Peter, Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/28850
imported:
- "2019"
_4images_image_id: "28850"
_4images_cat_id: "2061"
_4images_user_id: "22"
_4images_image_date: "2010-10-02T23:55:13"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28850 -->
Traktoren und andere Modellen