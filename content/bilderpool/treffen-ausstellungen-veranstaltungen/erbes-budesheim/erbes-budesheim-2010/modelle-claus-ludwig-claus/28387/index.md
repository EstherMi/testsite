---
layout: "image"
title: "Raupenkran"
date: "2010-09-26T21:52:02"
picture: "Raupenkran_-_Claus_Ludwig_claus.jpg"
weight: "23"
konstrukteure: 
- "Claus Ludwig (claus)"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/28387
imported:
- "2019"
_4images_image_id: "28387"
_4images_cat_id: "2061"
_4images_user_id: "1126"
_4images_image_date: "2010-09-26T21:52:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28387 -->
