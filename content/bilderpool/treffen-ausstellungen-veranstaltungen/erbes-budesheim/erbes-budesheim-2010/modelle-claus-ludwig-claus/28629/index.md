---
layout: "image"
title: "Verschiedene Greifer"
date: "2010-09-27T19:56:26"
picture: "fischertechnikconventioninerbesbuedesheim206.jpg"
weight: "17"
konstrukteure: 
- "Claus Ludwig"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28629
imported:
- "2019"
_4images_image_id: "28629"
_4images_cat_id: "2061"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:26"
_4images_image_order: "206"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28629 -->
