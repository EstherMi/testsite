---
layout: "image"
title: "Claus Ludwig"
date: "2010-10-02T23:55:10"
picture: "2010-Erbes-Budesheim_056.jpg"
weight: "1"
konstrukteure: 
- "Claus Ludwig"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/28813
imported:
- "2019"
_4images_image_id: "28813"
_4images_cat_id: "2061"
_4images_user_id: "22"
_4images_image_date: "2010-10-02T23:55:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28813 -->
