---
layout: "image"
title: "Cube Solver"
date: "2010-09-26T21:23:29"
picture: "Cube_Solver_-_Volker-James_Mnchhof_qincym.jpg"
weight: "6"
konstrukteure: 
- "Volker-James Münchhof (qincym)"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/28385
imported:
- "2019"
_4images_image_id: "28385"
_4images_cat_id: "2066"
_4images_user_id: "1126"
_4images_image_date: "2010-09-26T21:23:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28385 -->
