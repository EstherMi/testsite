---
layout: "image"
title: "Rubik's Cube"
date: "2010-09-27T19:56:22"
picture: "fischertechnikconventioninerbesbuedesheim088.jpg"
weight: "5"
konstrukteure: 
- "Volker James Münchhof"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28511
imported:
- "2019"
_4images_image_id: "28511"
_4images_cat_id: "2066"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:22"
_4images_image_order: "88"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28511 -->
Die Anlage nochmal im Detail