---
layout: "image"
title: "Containerkran"
date: "2010-09-26T16:06:48"
picture: "eb07.jpg"
weight: "13"
konstrukteure: 
- "Arjen Neijsen (JMN)"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/28289
imported:
- "2019"
_4images_image_id: "28289"
_4images_cat_id: "2051"
_4images_user_id: "558"
_4images_image_date: "2010-09-26T16:06:48"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28289 -->
