---
layout: "image"
title: "Getränkeautomat"
date: "2010-09-27T19:56:26"
picture: "fischertechnikconventioninerbesbuedesheim209.jpg"
weight: "21"
konstrukteure: 
- "Endlich"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28632
imported:
- "2019"
_4images_image_id: "28632"
_4images_cat_id: "2058"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:26"
_4images_image_order: "209"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28632 -->
