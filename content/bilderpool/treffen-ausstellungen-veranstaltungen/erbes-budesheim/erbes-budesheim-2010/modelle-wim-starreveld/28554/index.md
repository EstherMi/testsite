---
layout: "image"
title: "Ausleger des Krans"
date: "2010-09-27T19:56:24"
picture: "fischertechnikconventioninerbesbuedesheim131.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28554
imported:
- "2019"
_4images_image_id: "28554"
_4images_cat_id: "2078"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:24"
_4images_image_order: "131"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28554 -->
