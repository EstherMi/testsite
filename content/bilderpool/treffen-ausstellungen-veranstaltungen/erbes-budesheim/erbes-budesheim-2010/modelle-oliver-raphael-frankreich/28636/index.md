---
layout: "image"
title: "Sortieranlage (?) (4)"
date: "2010-09-27T19:56:26"
picture: "fischertechnikconventioninerbesbuedesheim213.jpg"
weight: "11"
konstrukteure: 
- "rei_vilo"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28636
imported:
- "2019"
_4images_image_id: "28636"
_4images_cat_id: "2076"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:26"
_4images_image_order: "213"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28636 -->
