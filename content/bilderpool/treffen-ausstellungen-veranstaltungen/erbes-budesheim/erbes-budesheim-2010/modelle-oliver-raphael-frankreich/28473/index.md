---
layout: "image"
title: "Sortieranlage für Räder"
date: "2010-09-27T19:56:21"
picture: "fischertechnikconventioninerbesbuedesheim050.jpg"
weight: "7"
konstrukteure: 
- "rei_vilo"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28473
imported:
- "2019"
_4images_image_id: "28473"
_4images_cat_id: "2076"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:21"
_4images_image_order: "50"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28473 -->
