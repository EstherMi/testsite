---
layout: "image"
title: "Sortieranlage (?) (6)"
date: "2010-09-27T19:56:26"
picture: "fischertechnikconventioninerbesbuedesheim215.jpg"
weight: "13"
konstrukteure: 
- "rei_vilo"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28638
imported:
- "2019"
_4images_image_id: "28638"
_4images_cat_id: "2076"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:26"
_4images_image_order: "215"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28638 -->
