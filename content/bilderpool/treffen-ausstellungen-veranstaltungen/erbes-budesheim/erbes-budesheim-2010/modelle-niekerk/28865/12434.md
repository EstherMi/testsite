---
layout: "comment"
hidden: true
title: "12434"
date: "2010-10-04T14:54:05"
uploadBy:
- "laserman"
license: "unknown"
imported:
- "2019"
---
Hallo Andreas (Rozek),

dem was Peter gesagt hat, schließe ich mich an.
Ich fände es auch super, wenn es noch weitere Modelle zum drucken gäbe.

Vielleicht kannst Du auch sagen, mit welchem Programm man G-Code Dateien erzeugen kann, bzw. welche Parameter Du selbst verwendet hast.

Vielen Dank und viele Grüße, Andreas.