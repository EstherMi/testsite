---
layout: "image"
title: "Baueinheit des 3d-Druckers"
date: "2010-09-27T19:56:20"
picture: "fischertechnikconventioninerbesbuedesheim030.jpg"
weight: "7"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28453
imported:
- "2019"
_4images_image_id: "28453"
_4images_cat_id: "2102"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:20"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28453 -->
