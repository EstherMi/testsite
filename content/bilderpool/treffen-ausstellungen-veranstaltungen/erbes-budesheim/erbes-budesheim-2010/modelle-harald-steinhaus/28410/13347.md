---
layout: "comment"
hidden: true
title: "13347"
date: "2011-01-24T17:01:41"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Aber sicher ist er auf dem Bild abgebildet. Wenn er nicht zu sehen ist, beweist das nur die perfekte Tarnung :-)