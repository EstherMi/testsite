---
layout: "image"
title: "Harmonic-Drive"
date: "2010-09-27T19:56:21"
picture: "fischertechnikconventioninerbesbuedesheim058.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28481
imported:
- "2019"
_4images_image_id: "28481"
_4images_cat_id: "2060"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:21"
_4images_image_order: "58"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28481 -->
