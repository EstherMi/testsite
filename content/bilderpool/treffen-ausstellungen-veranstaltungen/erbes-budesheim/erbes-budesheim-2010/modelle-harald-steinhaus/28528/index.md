---
layout: "image"
title: "Panzer"
date: "2010-09-27T19:56:24"
picture: "fischertechnikconventioninerbesbuedesheim105.jpg"
weight: "16"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28528
imported:
- "2019"
_4images_image_id: "28528"
_4images_cat_id: "2060"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:24"
_4images_image_order: "105"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28528 -->
