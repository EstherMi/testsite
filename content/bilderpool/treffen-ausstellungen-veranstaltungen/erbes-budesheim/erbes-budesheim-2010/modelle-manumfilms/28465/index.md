---
layout: "image"
title: "Roboter ???"
date: "2010-09-27T19:56:20"
picture: "fischertechnikconventioninerbesbuedesheim042.jpg"
weight: "9"
konstrukteure: 
- "manumffilms"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28465
imported:
- "2019"
_4images_image_id: "28465"
_4images_cat_id: "2087"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:20"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28465 -->
