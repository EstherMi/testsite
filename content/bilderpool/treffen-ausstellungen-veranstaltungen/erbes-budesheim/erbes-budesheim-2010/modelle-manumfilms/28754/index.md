---
layout: "image"
title: "linke Seite"
date: "2010-09-29T15:01:11"
picture: "modellevonmanuelankmanumffilms5.jpg"
weight: "5"
konstrukteure: 
- "manumffilms"
fotografen:
- "--"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "manuMFfilms"
license: "unknown"
legacy_id:
- details/28754
imported:
- "2019"
_4images_image_id: "28754"
_4images_cat_id: "2087"
_4images_user_id: "934"
_4images_image_date: "2010-09-29T15:01:11"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28754 -->
7-Achs-Roboterarm + Greifer, gesteuert via Webcam - LEFT