---
layout: "image"
title: "Unimog"
date: "2010-09-27T19:56:21"
picture: "fischertechnikconventioninerbesbuedesheim072.jpg"
weight: "7"
konstrukteure: 
- "Peter Damen (peterholland)"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28495
imported:
- "2019"
_4images_image_id: "28495"
_4images_cat_id: "2059"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:21"
_4images_image_order: "72"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28495 -->
