---
layout: "image"
title: "Fritz Roller erklärt das Funtionieren einer Inundatie-Waaierschleuse"
date: "2010-10-02T23:55:11"
picture: "2010-Erbes-Budesheim_063.jpg"
weight: "3"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/28816
imported:
- "2019"
_4images_image_id: "28816"
_4images_cat_id: "2059"
_4images_user_id: "22"
_4images_image_date: "2010-10-02T23:55:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28816 -->
