---
layout: "image"
title: "Magnetimpulsanlage mit Zählwerk"
date: "2010-09-27T19:56:24"
picture: "fischertechnikconventioninerbesbuedesheim114.jpg"
weight: "10"
konstrukteure: 
- "Frederik"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28537
imported:
- "2019"
_4images_image_id: "28537"
_4images_cat_id: "2073"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:24"
_4images_image_order: "114"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28537 -->
