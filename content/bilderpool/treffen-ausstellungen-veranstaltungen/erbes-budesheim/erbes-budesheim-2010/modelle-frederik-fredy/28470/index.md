---
layout: "image"
title: "Streichholzschachtelspender"
date: "2010-09-27T19:56:20"
picture: "fischertechnikconventioninerbesbuedesheim047.jpg"
weight: "1"
konstrukteure: 
- "Frederik (Fredy)"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28470
imported:
- "2019"
_4images_image_id: "28470"
_4images_cat_id: "2073"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:20"
_4images_image_order: "47"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28470 -->
