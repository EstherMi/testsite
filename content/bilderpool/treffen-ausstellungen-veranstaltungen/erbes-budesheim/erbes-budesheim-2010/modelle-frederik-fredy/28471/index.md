---
layout: "image"
title: "Schreiber"
date: "2010-09-27T19:56:20"
picture: "fischertechnikconventioninerbesbuedesheim048.jpg"
weight: "2"
konstrukteure: 
- "Frederik (Fredy)"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28471
imported:
- "2019"
_4images_image_id: "28471"
_4images_cat_id: "2073"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:20"
_4images_image_order: "48"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28471 -->
