---
layout: "image"
title: "Boxen-Wechsler"
date: "2010-09-27T19:56:24"
picture: "fischertechnikconventioninerbesbuedesheim110.jpg"
weight: "6"
konstrukteure: 
- "Frederik"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28533
imported:
- "2019"
_4images_image_id: "28533"
_4images_cat_id: "2073"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:24"
_4images_image_order: "110"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28533 -->
