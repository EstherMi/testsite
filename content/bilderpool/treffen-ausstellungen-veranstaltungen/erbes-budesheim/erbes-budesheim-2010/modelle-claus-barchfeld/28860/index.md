---
layout: "image"
title: "Claus Barchfeld"
date: "2010-10-02T23:55:13"
picture: "erbesbudesheim33.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "Peter, Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/28860
imported:
- "2019"
_4images_image_id: "28860"
_4images_cat_id: "2052"
_4images_user_id: "22"
_4images_image_date: "2010-10-02T23:55:13"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28860 -->
Robotermodelle