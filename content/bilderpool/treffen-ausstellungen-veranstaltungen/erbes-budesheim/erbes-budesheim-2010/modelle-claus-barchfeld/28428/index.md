---
layout: "image"
title: "Roboterarme"
date: "2010-09-27T19:56:19"
picture: "fischertechnikconventioninerbesbuedesheim005.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28428
imported:
- "2019"
_4images_image_id: "28428"
_4images_cat_id: "2052"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:19"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28428 -->
Verschiedene Arten von Roboterarmen