---
layout: "image"
title: "3-Etagen-Aufzug"
date: "2010-09-27T19:56:25"
picture: "fischertechnikconventioninerbesbuedesheim166.jpg"
weight: "2"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28589
imported:
- "2019"
_4images_image_id: "28589"
_4images_cat_id: "2089"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:25"
_4images_image_order: "166"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28589 -->
