---
layout: "image"
title: "Erbsentransportieranlage"
date: "2010-09-27T19:56:24"
picture: "fischertechnikconventioninerbesbuedesheim116.jpg"
weight: "7"
konstrukteure: 
- "Sven Engelke (sven)"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28539
imported:
- "2019"
_4images_image_id: "28539"
_4images_cat_id: "2082"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:24"
_4images_image_order: "116"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28539 -->
