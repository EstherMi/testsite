---
layout: "image"
title: "Cristian Knobloch"
date: "2010-10-02T23:55:12"
picture: "erbesbudesheim14.jpg"
weight: "23"
konstrukteure: 
- "Cristian Knobloch"
fotografen:
- "Peter, Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/28841
imported:
- "2019"
_4images_image_id: "28841"
_4images_cat_id: "2049"
_4images_user_id: "22"
_4images_image_date: "2010-10-02T23:55:12"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28841 -->
Firestorm Megacoaster