---
layout: "image"
title: "Cristian Knobloch"
date: "2010-10-02T23:55:12"
picture: "erbesbudesheim13.jpg"
weight: "22"
konstrukteure: 
- "Cristian Knobloch"
fotografen:
- "Peter, Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/28840
imported:
- "2019"
_4images_image_id: "28840"
_4images_cat_id: "2049"
_4images_user_id: "22"
_4images_image_date: "2010-10-02T23:55:12"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28840 -->
Firestorm Megacoaster