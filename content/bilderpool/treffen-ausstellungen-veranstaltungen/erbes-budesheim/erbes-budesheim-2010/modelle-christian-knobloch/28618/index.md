---
layout: "image"
title: "Firestorm-Megacoaster"
date: "2010-09-27T19:56:26"
picture: "fischertechnikconventioninerbesbuedesheim195.jpg"
weight: "42"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28618
imported:
- "2019"
_4images_image_id: "28618"
_4images_cat_id: "2049"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:26"
_4images_image_order: "195"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28618 -->
Die Wagen vor der Einfahrt in die Station in Warteposition