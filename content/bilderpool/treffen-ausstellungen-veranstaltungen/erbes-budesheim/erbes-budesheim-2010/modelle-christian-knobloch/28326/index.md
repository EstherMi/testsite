---
layout: "image"
title: "Achterbahn"
date: "2010-09-26T17:31:41"
picture: "achterbahn12.jpg"
weight: "89"
konstrukteure: 
- "C.Knobloch"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/28326
imported:
- "2019"
_4images_image_id: "28326"
_4images_cat_id: "2049"
_4images_user_id: "1162"
_4images_image_date: "2010-09-26T17:31:41"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28326 -->
