---
layout: "image"
title: "Aufstieg des Firestorm- Megacoasters"
date: "2010-09-27T19:56:25"
picture: "fischertechnikconventioninerbesbuedesheim140.jpg"
weight: "40"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28563
imported:
- "2019"
_4images_image_id: "28563"
_4images_cat_id: "2049"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:25"
_4images_image_order: "140"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28563 -->
