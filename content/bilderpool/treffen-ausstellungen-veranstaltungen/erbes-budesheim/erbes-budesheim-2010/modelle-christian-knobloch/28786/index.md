---
layout: "image"
title: "Magnetträger"
date: "2010-09-30T19:17:29"
picture: "firestorm_upload03.jpg"
weight: "30"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "Christian Knobloch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "C-Knobloch"
license: "unknown"
legacy_id:
- details/28786
imported:
- "2019"
_4images_image_id: "28786"
_4images_cat_id: "2049"
_4images_user_id: "997"
_4images_image_date: "2010-09-30T19:17:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28786 -->
