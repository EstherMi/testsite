---
layout: "image"
title: "Dunst nach der Convention"
date: "2010-09-30T19:17:29"
picture: "firestorm_upload07.jpg"
weight: "35"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "Christian Knobloch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "C-Knobloch"
license: "unknown"
legacy_id:
- details/28791
imported:
- "2019"
_4images_image_id: "28791"
_4images_cat_id: "2049"
_4images_user_id: "997"
_4images_image_date: "2010-09-30T19:17:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28791 -->
Spielerei mit der Nebelmaschine