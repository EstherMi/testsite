---
layout: "image"
title: "Der Wagen"
date: "2010-09-26T14:33:44"
picture: "firestorm4.jpg"
weight: "76"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "Tobias Horst (tobs9578)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- details/28282
imported:
- "2019"
_4images_image_id: "28282"
_4images_cat_id: "2049"
_4images_user_id: "1007"
_4images_image_date: "2010-09-26T14:33:44"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28282 -->
