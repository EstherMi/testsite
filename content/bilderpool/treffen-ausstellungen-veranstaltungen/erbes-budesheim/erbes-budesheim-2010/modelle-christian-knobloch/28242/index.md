---
layout: "image"
title: "Achterbahn"
date: "2010-09-26T11:55:16"
picture: "FTC2010_11.jpg"
weight: "58"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "Sebastian Schräder"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Emnet"
license: "unknown"
legacy_id:
- details/28242
imported:
- "2019"
_4images_image_id: "28242"
_4images_cat_id: "2049"
_4images_user_id: "714"
_4images_image_date: "2010-09-26T11:55:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28242 -->
