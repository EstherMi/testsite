---
layout: "image"
title: "Achterbahn"
date: "2010-09-26T17:31:35"
picture: "achterbahn08.jpg"
weight: "83"
konstrukteure: 
- "C.Knobloch"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/28322
imported:
- "2019"
_4images_image_id: "28322"
_4images_cat_id: "2049"
_4images_user_id: "1162"
_4images_image_date: "2010-09-26T17:31:35"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28322 -->
