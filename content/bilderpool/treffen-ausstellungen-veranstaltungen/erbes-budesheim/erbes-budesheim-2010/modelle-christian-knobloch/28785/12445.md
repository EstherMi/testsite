---
layout: "comment"
hidden: true
title: "12445"
date: "2010-10-05T15:56:47"
uploadBy:
- "sebastian..."
license: "unknown"
imported:
- "2019"
---
Hallo,
ich bin schon sehr lange dabei, einen free fall tower zu bauen (http://www.ftcommunity.de/categories.php?cat_id=1862) und benutze Neodym Magnete und Alu Schienen aus dem Baumarkt. Neodym Magnete gibt es bei Ebay in allen möglichen Größen und anziehungskräften. Momentan habe ich an der Gondel vier 50 Kilo Magneten und vier 25 Kilo Magneten. Nach circa 20 bis 30 cm hat die massige Gondel Auf die niedrigste Geschwindigkeit abgebremst, und wird engültig von Federn gebremst. Zumindest das funktioniert super ;-D