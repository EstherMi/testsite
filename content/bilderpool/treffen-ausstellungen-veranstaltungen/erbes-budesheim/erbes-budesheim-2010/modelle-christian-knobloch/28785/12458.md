---
layout: "comment"
hidden: true
title: "12458"
date: "2010-10-05T21:34:18"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Hallo Sebastian,

Vielen Dank für deine Link und Info.
Ich hatte schon viel gesucht bei alle Bilder der FT-Community, doch leider mit dem falschen Stichwörter.  Die Wirbelstrombremse einer Freifallturme ist für mich das wichtigste und du hast es schon geschafft nach mehrere Versuchen, habe ich gesehen.

Ich möchte mir meine pneumatische Shot ''n Drop-Tower ändern in eine echte Freifall-Turm mit Wirbelstrombremsen. 
http://www.ftcommunity.de/categories.php?cat_id=1214 

Dieses Wochenende habe ich einige Versuchen gemacht mit an 2 Seiten 9 st Magneten
http://www.supermagnete.de/Q-15-15-08-N
Q-15-15-08-N 
15 x 15 x 8 mm
Gewicht 14 g
vernickelt (Ni-Cu-Ni)
Magnetisierung: N42
Haftkraft: ca. 7,6 kg/st

und ein Alu-Profil  U20x20x2mm  

Die Gondel Bremmst, aber noch zu wenig. 
Ich hatte eigentlich vor mit dickere Aluprofilen zu versuchen......aber deine Erfahrungen mit Magneten mit Hafkräfte bis zum 25 und 50 kg sind wahrscheinlich Erfolgreicher.

Hast du verschiedende Alu-Schienen-Abmessungen Versucht ? ................

Hast du die Federn unten montiert ?.................
Reichen die kurze  FT-Federn genug?................
Für verschiedene FT-Modellen nutze ich selber manchmal Stahlfedern.

Grüss,

Peter
Poederoyen NL