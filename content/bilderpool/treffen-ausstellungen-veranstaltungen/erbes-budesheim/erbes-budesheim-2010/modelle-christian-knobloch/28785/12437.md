---
layout: "comment"
hidden: true
title: "12437"
date: "2010-10-04T21:12:17"
uploadBy:
- "C-Knobloch"
license: "unknown"
imported:
- "2019"
---
es  kommt auf die elektrische leitfähigkeit an. Bei Cu und Al waren durchaus große unterschiede bemerkbar. Du kannst es ja mal mit Aluminium probieren, aber wenn es aus dem Baumarkt kommt ist nicht garantiert das es wirklich reines Alu ist.

Bei einem FreeFall Tower hast du den Vorteil das du viel mehr Magnete verwenden kannst. Vielleicht geht es auch mit Alu.

Viel Glück!

Gruß,
Christian