---
layout: "image"
title: "Achterbahn"
date: "2010-09-26T17:31:35"
picture: "achterbahn03.jpg"
weight: "74"
konstrukteure: 
- "C.Knobloch"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/28317
imported:
- "2019"
_4images_image_id: "28317"
_4images_cat_id: "2049"
_4images_user_id: "1162"
_4images_image_date: "2010-09-26T17:31:35"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28317 -->
