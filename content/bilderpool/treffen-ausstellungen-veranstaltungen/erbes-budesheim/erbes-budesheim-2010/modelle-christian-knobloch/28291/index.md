---
layout: "image"
title: "Achterbahn"
date: "2010-09-26T16:06:48"
picture: "eb09.jpg"
weight: "84"
konstrukteure: 
- "C.Knobloch"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/28291
imported:
- "2019"
_4images_image_id: "28291"
_4images_cat_id: "2049"
_4images_user_id: "558"
_4images_image_date: "2010-09-26T16:06:48"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28291 -->
