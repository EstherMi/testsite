---
layout: "image"
title: "Achterbahn"
date: "2010-09-26T12:03:26"
picture: "achterbahn3.jpg"
weight: "72"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "Heiko"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/28249
imported:
- "2019"
_4images_image_id: "28249"
_4images_cat_id: "2049"
_4images_user_id: "453"
_4images_image_date: "2010-09-26T12:03:26"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28249 -->
