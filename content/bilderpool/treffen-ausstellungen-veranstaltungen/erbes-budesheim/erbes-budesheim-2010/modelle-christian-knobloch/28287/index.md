---
layout: "image"
title: "Achterbahn"
date: "2010-09-26T16:06:48"
picture: "eb05.jpg"
weight: "78"
konstrukteure: 
- "C.Knobloch"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/28287
imported:
- "2019"
_4images_image_id: "28287"
_4images_cat_id: "2049"
_4images_user_id: "558"
_4images_image_date: "2010-09-26T16:06:48"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28287 -->
