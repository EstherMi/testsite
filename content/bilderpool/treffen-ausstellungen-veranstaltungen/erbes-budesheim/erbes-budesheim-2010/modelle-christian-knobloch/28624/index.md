---
layout: "image"
title: "Die Bahnen"
date: "2010-09-27T19:56:26"
picture: "fischertechnikconventioninerbesbuedesheim201.jpg"
weight: "48"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28624
imported:
- "2019"
_4images_image_id: "28624"
_4images_cat_id: "2049"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:26"
_4images_image_order: "201"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28624 -->
