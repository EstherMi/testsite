---
layout: "image"
title: "Modelle auf der FT-Convention"
date: "2010-09-28T17:22:39"
picture: "ddgh32.jpg"
weight: "7"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/28740
imported:
- "2019"
_4images_image_id: "28740"
_4images_cat_id: "2064"
_4images_user_id: "1162"
_4images_image_date: "2010-09-28T17:22:39"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28740 -->
