---
layout: "comment"
hidden: true
title: "17503"
date: "2012-11-01T16:49:17"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Bennik,
habe leider erst heute deinen Kommentar entdeckt. Ich bin bei meinen beiden Portalrobotern aber wegen Zeitmangel erst der Fortentwicklung der Achsengenauigkeit. Ich weiss zwar nicht, welche Getriebe du zum Antrieb der Achsen anwendest. Dennoch darf ich dir sagen, dass zu 99% in erster Linie wir Konstrukteure mit der Statik und Dynamik unserer Modelle an den Abweichungen beteiligt sind ...
Bei meinem 3D-XYZ-GES (Gleitführung mit Schneckengetriebe) waren/sind es z.B. Verdrehungen der Schneckenteile im mittleren Bereich der Schneckenwellen und bei meinem 3D-XYZ-REK (Rollenführung mit Kettengetriebe) waren es z.B. die Klemmungen der Zahnräder auf den M-Achsen ...
Mein noch einfaches Programm mit den bewusst vielen Richtungsänderungen führt solche Ursachen stets ans Tageslicht ...
Gruss, Udo2