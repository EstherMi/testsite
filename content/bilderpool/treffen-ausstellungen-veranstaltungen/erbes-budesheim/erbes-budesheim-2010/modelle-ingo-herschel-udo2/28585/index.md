---
layout: "image"
title: "Drehteile"
date: "2010-09-27T19:56:25"
picture: "fischertechnikconventioninerbesbuedesheim162.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28585
imported:
- "2019"
_4images_image_id: "28585"
_4images_cat_id: "2064"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:25"
_4images_image_order: "162"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28585 -->
