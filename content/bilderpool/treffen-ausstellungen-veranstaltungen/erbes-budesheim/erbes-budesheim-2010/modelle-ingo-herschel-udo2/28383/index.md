---
layout: "image"
title: "Leitspindel-Drehmaschine"
date: "2010-09-26T20:45:13"
picture: "Leitspindel-Drehmaschine_-_Ingo_Herschel_Udo2.jpg"
weight: "13"
konstrukteure: 
- "Ingo Herschel (Udo2)"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/28383
imported:
- "2019"
_4images_image_id: "28383"
_4images_cat_id: "2064"
_4images_user_id: "1126"
_4images_image_date: "2010-09-26T20:45:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28383 -->
Detaillierte Beschreibung unter http://www.ftcommunity.de/categories.php?cat_id=2041