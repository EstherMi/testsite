---
layout: "image"
title: "Reitstock der Drehmaschine mit Drehteil"
date: "2010-09-27T19:56:25"
picture: "fischertechnikconventioninerbesbuedesheim164.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28587
imported:
- "2019"
_4images_image_id: "28587"
_4images_cat_id: "2064"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:25"
_4images_image_order: "164"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28587 -->
