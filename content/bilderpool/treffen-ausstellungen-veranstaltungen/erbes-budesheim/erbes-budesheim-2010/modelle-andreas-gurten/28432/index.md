---
layout: "image"
title: "Pneumatikmodell"
date: "2010-09-27T19:56:19"
picture: "fischertechnikconventioninerbesbuedesheim009.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28432
imported:
- "2019"
_4images_image_id: "28432"
_4images_cat_id: "2088"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:19"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28432 -->
