---
layout: "image"
title: "Pneumatisches Modell (2)"
date: "2010-09-27T19:56:25"
picture: "fischertechnikconventioninerbesbuedesheim152.jpg"
weight: "8"
konstrukteure: 
- "Andreas Gürten (laserman)"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28575
imported:
- "2019"
_4images_image_id: "28575"
_4images_cat_id: "2088"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:25"
_4images_image_order: "152"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28575 -->
