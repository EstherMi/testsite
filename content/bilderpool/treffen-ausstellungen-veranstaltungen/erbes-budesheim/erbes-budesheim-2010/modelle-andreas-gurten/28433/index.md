---
layout: "image"
title: "Pneuma-Cube"
date: "2010-09-27T19:56:19"
picture: "fischertechnikconventioninerbesbuedesheim010.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28433
imported:
- "2019"
_4images_image_id: "28433"
_4images_cat_id: "2088"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:19"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28433 -->
