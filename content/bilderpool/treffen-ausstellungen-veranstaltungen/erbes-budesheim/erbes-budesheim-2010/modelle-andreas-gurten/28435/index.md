---
layout: "image"
title: "Zeichenschablone"
date: "2010-09-27T19:56:19"
picture: "fischertechnikconventioninerbesbuedesheim012.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28435
imported:
- "2019"
_4images_image_id: "28435"
_4images_cat_id: "2088"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:19"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28435 -->
Zeichnet eine Ellipse