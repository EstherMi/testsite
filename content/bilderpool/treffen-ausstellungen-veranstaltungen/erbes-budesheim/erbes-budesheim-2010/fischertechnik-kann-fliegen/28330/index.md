---
layout: "image"
title: "Fischertechnik kann fliegen"
date: "2010-09-26T17:34:37"
picture: "ssfs01.jpg"
weight: "2"
konstrukteure: 
- "Thomas Kaiser"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/28330
imported:
- "2019"
_4images_image_id: "28330"
_4images_cat_id: "2056"
_4images_user_id: "1162"
_4images_image_date: "2010-09-26T17:34:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28330 -->
