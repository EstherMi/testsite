---
layout: "image"
title: "Fischertechnik kann fliegen"
date: "2010-09-26T17:34:37"
picture: "ssfs07.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/28336
imported:
- "2019"
_4images_image_id: "28336"
_4images_cat_id: "2056"
_4images_user_id: "1162"
_4images_image_date: "2010-09-26T17:34:37"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28336 -->
