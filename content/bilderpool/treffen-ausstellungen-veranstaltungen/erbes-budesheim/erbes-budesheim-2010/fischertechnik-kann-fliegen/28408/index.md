---
layout: "image"
title: "ft fliegt.JPG"
date: "2010-09-27T17:27:14"
picture: "IMG_3960_ft_fliegt.JPG"
weight: "1"
konstrukteure: 
- "thkais"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/28408
imported:
- "2019"
_4images_image_id: "28408"
_4images_cat_id: "2056"
_4images_user_id: "4"
_4images_image_date: "2010-09-27T17:27:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28408 -->
