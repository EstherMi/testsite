---
layout: "image"
title: "Blinkanlage auf dem Sendmast"
date: "2010-09-27T19:56:21"
picture: "fischertechnikconventioninerbesbuedesheim065.jpg"
weight: "6"
konstrukteure: 
- "Dieter Meckel"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28488
imported:
- "2019"
_4images_image_id: "28488"
_4images_cat_id: "2090"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:21"
_4images_image_order: "65"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28488 -->
