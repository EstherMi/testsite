---
layout: "image"
title: "Radlader mit Pneumatik und Sortieranlage"
date: "2010-09-27T19:56:18"
picture: "fischertechnikconventioninerbesbuedesheim002.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28425
imported:
- "2019"
_4images_image_id: "28425"
_4images_cat_id: "2090"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:18"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28425 -->
Konstrukteur: Dieter Meckel