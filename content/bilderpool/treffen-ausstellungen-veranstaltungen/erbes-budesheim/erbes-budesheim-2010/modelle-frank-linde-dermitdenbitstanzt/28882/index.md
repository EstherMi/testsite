---
layout: "image"
title: "When is this available for other fans???"
date: "2010-10-03T15:00:55"
picture: "6.jpg"
weight: "7"
konstrukteure: 
- "Frank Linde (DermitdenBitstanzt)"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/28882
imported:
- "2019"
_4images_image_id: "28882"
_4images_cat_id: "2067"
_4images_user_id: "371"
_4images_image_date: "2010-10-03T15:00:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28882 -->
