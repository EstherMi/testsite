---
layout: "image"
title: "Roboterarm"
date: "2010-09-26T21:52:02"
picture: "Roboter_-_Frank_Linde_DmdBt.jpg"
weight: "15"
konstrukteure: 
- "Frank Linde (DermitdenBitstanzt)"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/28386
imported:
- "2019"
_4images_image_id: "28386"
_4images_cat_id: "2067"
_4images_user_id: "1126"
_4images_image_date: "2010-09-26T21:52:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28386 -->
