---
layout: "image"
title: "..."
date: "2010-10-03T15:00:55"
picture: "FTconvention2010-20100925-13-59-00-DSC_3131.jpg"
weight: "11"
konstrukteure: 
- "Frank Linde (DermitdenBitstanzt)"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/28886
imported:
- "2019"
_4images_image_id: "28886"
_4images_cat_id: "2067"
_4images_user_id: "371"
_4images_image_date: "2010-10-03T15:00:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28886 -->
