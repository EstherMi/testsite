---
layout: "image"
title: "Ballgreifer"
date: "2010-09-27T19:56:21"
picture: "fischertechnikconventioninerbesbuedesheim074.jpg"
weight: "21"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28497
imported:
- "2019"
_4images_image_id: "28497"
_4images_cat_id: "2055"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:21"
_4images_image_order: "74"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28497 -->
