---
layout: "image"
title: "Einzelheiten der Dampfmaschine"
date: "2010-09-27T19:56:24"
picture: "fischertechnikconventioninerbesbuedesheim108.jpg"
weight: "27"
konstrukteure: 
- "Jan Willem Dekker"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28531
imported:
- "2019"
_4images_image_id: "28531"
_4images_cat_id: "2055"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:24"
_4images_image_order: "108"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28531 -->
