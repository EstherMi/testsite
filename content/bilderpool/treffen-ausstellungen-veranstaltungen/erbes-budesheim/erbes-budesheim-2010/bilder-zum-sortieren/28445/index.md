---
layout: "image"
title: "Fahr-Roboter"
date: "2010-09-27T19:56:20"
picture: "fischertechnikconventioninerbesbuedesheim022.jpg"
weight: "19"
konstrukteure: 
- "Frank Hermann"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28445
imported:
- "2019"
_4images_image_id: "28445"
_4images_cat_id: "2055"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:20"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28445 -->
