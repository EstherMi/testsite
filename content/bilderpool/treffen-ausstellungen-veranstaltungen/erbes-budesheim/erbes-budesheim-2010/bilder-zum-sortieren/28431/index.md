---
layout: "image"
title: "Auslegerkran"
date: "2010-09-27T19:56:19"
picture: "fischertechnikconventioninerbesbuedesheim008.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28431
imported:
- "2019"
_4images_image_id: "28431"
_4images_cat_id: "2055"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:19"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28431 -->
