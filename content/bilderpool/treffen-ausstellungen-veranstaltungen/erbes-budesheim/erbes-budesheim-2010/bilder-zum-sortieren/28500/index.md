---
layout: "image"
title: "Detail vom 3D-Drucker"
date: "2010-09-27T19:56:21"
picture: "fischertechnikconventioninerbesbuedesheim077.jpg"
weight: "24"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28500
imported:
- "2019"
_4images_image_id: "28500"
_4images_cat_id: "2055"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:21"
_4images_image_order: "77"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28500 -->
Ist mir nicht bekannt