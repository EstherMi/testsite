---
layout: "image"
title: "Andreas Tacke"
date: "2010-10-02T23:55:12"
picture: "erbesbudesheim18.jpg"
weight: "1"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Peter, Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/28845
imported:
- "2019"
_4images_image_id: "28845"
_4images_cat_id: "2057"
_4images_user_id: "22"
_4images_image_date: "2010-10-02T23:55:12"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28845 -->
Kopplung