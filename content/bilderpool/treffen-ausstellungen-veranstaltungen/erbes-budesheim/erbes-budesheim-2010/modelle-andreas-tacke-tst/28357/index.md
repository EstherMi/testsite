---
layout: "image"
title: "Geländewagen"
date: "2010-09-26T18:46:21"
picture: "Gelndewagen_-_Andreas_Tacke.jpg"
weight: "2"
konstrukteure: 
- "Andreas Tacke (TST)"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/28357
imported:
- "2019"
_4images_image_id: "28357"
_4images_cat_id: "2057"
_4images_user_id: "1126"
_4images_image_date: "2010-09-26T18:46:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28357 -->
