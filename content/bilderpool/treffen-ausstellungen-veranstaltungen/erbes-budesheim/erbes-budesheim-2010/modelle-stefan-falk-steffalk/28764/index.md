---
layout: "image"
title: "Sprungschanze"
date: "2010-09-29T20:02:42"
picture: "steffalk4.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Christian Knobloch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "C-Knobloch"
license: "unknown"
legacy_id:
- details/28764
imported:
- "2019"
_4images_image_id: "28764"
_4images_cat_id: "2062"
_4images_user_id: "997"
_4images_image_date: "2010-09-29T20:02:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28764 -->
