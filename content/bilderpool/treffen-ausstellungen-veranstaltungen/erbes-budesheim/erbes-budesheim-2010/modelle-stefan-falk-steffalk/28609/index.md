---
layout: "image"
title: "Anzeige der mechanischen Digitaluhr"
date: "2010-09-27T19:56:26"
picture: "fischertechnikconventioninerbesbuedesheim186.jpg"
weight: "25"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28609
imported:
- "2019"
_4images_image_id: "28609"
_4images_cat_id: "2062"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:26"
_4images_image_order: "186"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28609 -->
