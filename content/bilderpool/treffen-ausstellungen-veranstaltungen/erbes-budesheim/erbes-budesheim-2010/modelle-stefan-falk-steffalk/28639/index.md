---
layout: "image"
title: "Elektronische Henne"
date: "2010-09-27T19:56:26"
picture: "fischertechnikconventioninerbesbuedesheim216.jpg"
weight: "26"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28639
imported:
- "2019"
_4images_image_id: "28639"
_4images_cat_id: "2062"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:26"
_4images_image_order: "216"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28639 -->
