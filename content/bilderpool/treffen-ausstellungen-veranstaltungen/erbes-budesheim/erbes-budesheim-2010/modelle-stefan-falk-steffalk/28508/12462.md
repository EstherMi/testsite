---
layout: "comment"
hidden: true
title: "12462"
date: "2010-10-05T22:00:59"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
As a matter of fact, nearly *everyone* asked that. We are already thinking of a successor offering that feature. :-)

Best Regards,
Stefan