---
layout: "image"
title: "Stefan Falk"
date: "2010-09-27T19:56:21"
picture: "fischertechnikconventioninerbesbuedesheim084.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28507
imported:
- "2019"
_4images_image_id: "28507"
_4images_cat_id: "2062"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:21"
_4images_image_order: "84"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28507 -->
