---
layout: "image"
title: "Modell von Stefan Falk"
date: "2010-09-27T19:56:25"
picture: "fischertechnikconventioninerbesbuedesheim159.jpg"
weight: "22"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28582
imported:
- "2019"
_4images_image_id: "28582"
_4images_cat_id: "2062"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:25"
_4images_image_order: "159"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28582 -->
