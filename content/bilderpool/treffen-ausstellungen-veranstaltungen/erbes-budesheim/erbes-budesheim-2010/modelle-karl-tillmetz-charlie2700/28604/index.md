---
layout: "image"
title: "Container-Terminal"
date: "2010-09-27T19:56:26"
picture: "fischertechnikconventioninerbesbuedesheim181.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28604
imported:
- "2019"
_4images_image_id: "28604"
_4images_cat_id: "2069"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:26"
_4images_image_order: "181"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28604 -->
Container am Greifer