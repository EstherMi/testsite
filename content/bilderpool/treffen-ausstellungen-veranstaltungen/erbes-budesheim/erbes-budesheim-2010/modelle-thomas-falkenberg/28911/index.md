---
layout: "image"
title: "close up"
date: "2010-10-03T15:00:56"
picture: "APP-2010-025024.jpg"
weight: "1"
konstrukteure: 
- "Thomas Falkenberg"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/28911
imported:
- "2019"
_4images_image_id: "28911"
_4images_cat_id: "2077"
_4images_user_id: "371"
_4images_image_date: "2010-10-03T15:00:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28911 -->
