---
layout: "image"
title: "Bauteile einer Abraumanlage"
date: "2010-09-27T19:56:26"
picture: "fischertechnikconventioninerbesbuedesheim189.jpg"
weight: "10"
konstrukteure: 
- "Thomas Falkenberg"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28612
imported:
- "2019"
_4images_image_id: "28612"
_4images_cat_id: "2077"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:26"
_4images_image_order: "189"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28612 -->
