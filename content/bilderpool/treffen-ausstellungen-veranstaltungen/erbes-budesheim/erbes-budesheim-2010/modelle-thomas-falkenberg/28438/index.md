---
layout: "image"
title: "Prototyp eines Abräumbaggers"
date: "2010-09-27T19:56:20"
picture: "fischertechnikconventioninerbesbuedesheim015.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/28438
imported:
- "2019"
_4images_image_id: "28438"
_4images_cat_id: "2077"
_4images_user_id: "374"
_4images_image_date: "2010-09-27T19:56:20"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28438 -->
Noch in der Planung