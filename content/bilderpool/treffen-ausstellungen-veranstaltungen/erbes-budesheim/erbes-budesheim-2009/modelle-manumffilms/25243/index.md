---
layout: "image"
title: "manus Roboterarm"
date: "2009-09-23T20:48:31"
picture: "convention028.jpg"
weight: "4"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/25243
imported:
- "2019"
_4images_image_id: "25243"
_4images_cat_id: "1731"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:31"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25243 -->
