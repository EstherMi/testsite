---
layout: "image"
title: "Dame-Computer"
date: "2009-09-23T20:48:31"
picture: "convention078.jpg"
weight: "3"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/25293
imported:
- "2019"
_4images_image_id: "25293"
_4images_cat_id: "1733"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:31"
_4images_image_order: "78"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25293 -->
