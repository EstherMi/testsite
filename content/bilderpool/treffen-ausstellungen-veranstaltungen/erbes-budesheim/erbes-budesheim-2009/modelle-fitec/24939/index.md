---
layout: "image"
title: "Industrieanlage"
date: "2009-09-19T21:41:26"
picture: "3.jpg"
weight: "8"
konstrukteure: 
- "Nils"
fotografen:
- "Nils"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/24939
imported:
- "2019"
_4images_image_id: "24939"
_4images_cat_id: "1722"
_4images_user_id: "456"
_4images_image_date: "2009-09-19T21:41:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24939 -->
Die Anlage sollte rote und gelbe Kassetten sortieren.