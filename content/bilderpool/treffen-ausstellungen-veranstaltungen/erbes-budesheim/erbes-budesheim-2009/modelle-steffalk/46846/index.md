---
layout: "image"
title: "Nicht so recht funktionierender Drucker"
date: "2017-10-26T17:42:18"
picture: "druckerprototyp1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/46846
imported:
- "2019"
_4images_image_id: "46846"
_4images_cat_id: "1772"
_4images_user_id: "104"
_4images_image_date: "2017-10-26T17:42:18"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46846 -->
Ich fand noch ein Foto des Möchtegern-Druckers von der Convention 2009 in Erbes-Büdesheim. Der sollte vom Rechner gesteuert sowas ähnliches wie Schreiben können: Das Papier wird unten gebogen und damit ohne Verwerfungen wie bei den schicken HP-Plottern durchgezogen. In dem Dreieck in der Bildmitte wird ein Stift eingespannt. Der kann mit dem S-Motor in der Mitte nach links und rechts verfahren werden, und gleichzeitig über die Mechanik mit den vier Elektromagneten links und rechts etwas (in Papierrichtung) vor und zurück geschoben werden. Damit kann er von einer mittigen Grundlinie aus eine Linie oberhalb und eine unterhalb erreichen. Genauso kann er von weiteren Magneten etwas nach links oder rechts ausschlagen, und aufs Papier gedrückt oder abgehoben werden. Damit sollte er Zeichen durch Zeichnen von Linien in den 9 Punkten einer 3x3-Matrix zu Papier bringen können.

Die Software (sieht man auf den vorherigen Bildern) funktionierte gut, aber die Mechanik nicht: Die erreichbaren Hübe waren viel zu klein. Ein Zeichen wäre so einen mm hoch gewesen oder so, und da kann man natürlich nichts lesen.