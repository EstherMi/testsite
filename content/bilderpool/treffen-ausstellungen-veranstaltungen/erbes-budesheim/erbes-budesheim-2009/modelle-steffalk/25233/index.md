---
layout: "image"
title: "Der Möchtegern-Drucker"
date: "2009-09-23T20:48:30"
picture: "convention018.jpg"
weight: "4"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/25233
imported:
- "2019"
_4images_image_id: "25233"
_4images_cat_id: "1772"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:30"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25233 -->
Die Mechanik wurde in der Nacht vor der Convention nochmal umgebaut, weil die E-Magnete zwar am Netzteil, nicht aber am RoboInt genug Kraft hatten. Deshalb hinkte die Hardware der Software stark hinterher.