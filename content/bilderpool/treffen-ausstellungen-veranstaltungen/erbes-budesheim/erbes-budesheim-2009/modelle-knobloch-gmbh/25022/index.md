---
layout: "image"
title: "Erbes-Büdesheim-2009 (Knobloch)"
date: "2009-09-21T20:50:46"
picture: "erbesbuedesheim20.jpg"
weight: "7"
konstrukteure: 
- "FT-Experte"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/25022
imported:
- "2019"
_4images_image_id: "25022"
_4images_cat_id: "1787"
_4images_user_id: "22"
_4images_image_date: "2009-09-21T20:50:46"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25022 -->
Telescoop