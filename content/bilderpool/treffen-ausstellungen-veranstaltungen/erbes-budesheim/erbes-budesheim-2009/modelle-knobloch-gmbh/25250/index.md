---
layout: "image"
title: "Radarstation"
date: "2009-09-23T20:48:31"
picture: "convention035.jpg"
weight: "6"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/25250
imported:
- "2019"
_4images_image_id: "25250"
_4images_cat_id: "1787"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:31"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25250 -->
