---
layout: "image"
title: "Knobloch GmbH - Radarstation"
date: "2009-10-08T17:22:55"
picture: "verschiedene28.jpg"
weight: "3"
konstrukteure: 
- "Ralf Knobloch"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/25525
imported:
- "2019"
_4images_image_id: "25525"
_4images_cat_id: "1787"
_4images_user_id: "409"
_4images_image_date: "2009-10-08T17:22:55"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25525 -->
Radarstation - Drehkranz