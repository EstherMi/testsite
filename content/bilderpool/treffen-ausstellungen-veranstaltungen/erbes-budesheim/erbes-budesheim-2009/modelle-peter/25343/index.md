---
layout: "image"
title: "Peter Damens Pneumacube"
date: "2009-09-23T20:48:33"
picture: "convention128.jpg"
weight: "4"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/25343
imported:
- "2019"
_4images_image_id: "25343"
_4images_cat_id: "1774"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:33"
_4images_image_order: "128"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25343 -->
Funktioniert prima und erstaunlich schnell!