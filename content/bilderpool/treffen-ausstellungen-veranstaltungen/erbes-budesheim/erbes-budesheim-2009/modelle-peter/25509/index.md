---
layout: "image"
title: "peterholland - FT-Kugel"
date: "2009-10-08T17:22:54"
picture: "verschiedene12.jpg"
weight: "2"
konstrukteure: 
- "Peter Damen"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/25509
imported:
- "2019"
_4images_image_id: "25509"
_4images_cat_id: "1774"
_4images_user_id: "409"
_4images_image_date: "2009-10-08T17:22:54"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25509 -->
"schade", dass das Wetter so gut war, sonst hätte Peter mit dem Auto kommen müssen und hätte noch mehr tolle Modelle mitgebracht.