---
layout: "image"
title: "peterholland - Pneumakube"
date: "2009-10-08T17:22:54"
picture: "verschiedene11.jpg"
weight: "1"
konstrukteure: 
- "Peter Damen"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/25508
imported:
- "2019"
_4images_image_id: "25508"
_4images_cat_id: "1774"
_4images_user_id: "409"
_4images_image_date: "2009-10-08T17:22:54"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25508 -->
Pneumakube