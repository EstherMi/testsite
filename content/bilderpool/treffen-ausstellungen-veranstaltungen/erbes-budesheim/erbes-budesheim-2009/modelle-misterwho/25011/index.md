---
layout: "image"
title: "Erbes-Büdesheim-2009"
date: "2009-09-21T20:50:45"
picture: "erbesbuedesheim09.jpg"
weight: "8"
konstrukteure: 
- "FT-Experte"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/25011
imported:
- "2019"
_4images_image_id: "25011"
_4images_cat_id: "1735"
_4images_user_id: "22"
_4images_image_date: "2009-09-21T20:50:45"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25011 -->
4W-Antrieb