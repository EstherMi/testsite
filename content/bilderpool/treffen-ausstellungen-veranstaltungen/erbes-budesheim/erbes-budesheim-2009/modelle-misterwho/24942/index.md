---
layout: "image"
title: "Übersicht Joachim space"
date: "2009-09-19T21:55:25"
picture: "DSC_0006.jpg"
weight: "10"
konstrukteure: 
- "Joachim Jacobi"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/24942
imported:
- "2019"
_4images_image_id: "24942"
_4images_cat_id: "1735"
_4images_user_id: "371"
_4images_image_date: "2009-09-19T21:55:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24942 -->
