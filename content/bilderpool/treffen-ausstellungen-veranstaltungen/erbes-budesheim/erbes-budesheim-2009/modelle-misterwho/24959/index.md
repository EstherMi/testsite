---
layout: "image"
title: "Suchroboter"
date: "2009-09-19T22:06:09"
picture: "conv1.jpg"
weight: "12"
konstrukteure: 
- "MisterWho"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/24959
imported:
- "2019"
_4images_image_id: "24959"
_4images_cat_id: "1735"
_4images_user_id: "456"
_4images_image_date: "2009-09-19T22:06:09"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24959 -->
