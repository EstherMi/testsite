---
layout: "image"
title: "TSTs Sonderteile"
date: "2009-09-23T20:48:31"
picture: "convention058.jpg"
weight: "11"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/25273
imported:
- "2019"
_4images_image_id: "25273"
_4images_cat_id: "1746"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:31"
_4images_image_order: "58"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25273 -->
Malteserrad