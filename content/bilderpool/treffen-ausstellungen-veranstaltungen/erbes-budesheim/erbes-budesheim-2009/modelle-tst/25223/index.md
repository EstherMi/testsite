---
layout: "image"
title: "Verschiedenes"
date: "2009-09-23T20:48:30"
picture: "convention008.jpg"
weight: "1"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/25223
imported:
- "2019"
_4images_image_id: "25223"
_4images_cat_id: "1746"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:30"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25223 -->
