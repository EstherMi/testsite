---
layout: "image"
title: "Modelle von Karl Tillmetz"
date: "2009-09-21T20:50:47"
picture: "erbesbuedesheim35.jpg"
weight: "9"
konstrukteure: 
- "FT-Experte"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/25037
imported:
- "2019"
_4images_image_id: "25037"
_4images_cat_id: "1734"
_4images_user_id: "22"
_4images_image_date: "2009-09-21T20:50:47"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25037 -->
Container-Terminal