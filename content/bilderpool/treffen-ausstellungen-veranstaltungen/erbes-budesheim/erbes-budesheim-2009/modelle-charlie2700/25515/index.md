---
layout: "image"
title: "charlie2700 - Classic-Line 'Puffing Billy'"
date: "2009-10-08T17:22:55"
picture: "verschiedene18.jpg"
weight: "1"
konstrukteure: 
- "Karl Tillmetz"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/25515
imported:
- "2019"
_4images_image_id: "25515"
_4images_cat_id: "1734"
_4images_user_id: "409"
_4images_image_date: "2009-10-08T17:22:55"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25515 -->
Sieht aus wie ein Museumsstück