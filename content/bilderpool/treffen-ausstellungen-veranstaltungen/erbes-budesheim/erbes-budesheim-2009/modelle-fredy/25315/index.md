---
layout: "image"
title: "Fredys mobiler Roboter"
date: "2009-09-23T20:48:31"
picture: "convention100.jpg"
weight: "6"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/25315
imported:
- "2019"
_4images_image_id: "25315"
_4images_cat_id: "1739"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:31"
_4images_image_order: "100"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25315 -->
