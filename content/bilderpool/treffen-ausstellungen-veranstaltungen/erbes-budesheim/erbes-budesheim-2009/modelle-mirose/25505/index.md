---
layout: "image"
title: "mirose - Münzsortierer /-zähler"
date: "2009-10-08T17:22:54"
picture: "verschiedene08.jpg"
weight: "1"
konstrukteure: 
- "Michael Sengstschmid"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/25505
imported:
- "2019"
_4images_image_id: "25505"
_4images_cat_id: "1738"
_4images_user_id: "409"
_4images_image_date: "2009-10-08T17:22:54"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25505 -->
Münzsortierer /-zähler