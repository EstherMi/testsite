---
layout: "image"
title: "Geldspielautomat"
date: "2009-09-23T20:48:31"
picture: "convention067.jpg"
weight: "9"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/25282
imported:
- "2019"
_4images_image_id: "25282"
_4images_cat_id: "1738"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:31"
_4images_image_order: "67"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25282 -->
