---
layout: "image"
title: "Michael Sengstschmid"
date: "2009-09-21T20:50:47"
picture: "erbesbuedesheim44.jpg"
weight: "10"
konstrukteure: 
- "FT-Experte"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/25046
imported:
- "2019"
_4images_image_id: "25046"
_4images_cat_id: "1738"
_4images_user_id: "22"
_4images_image_date: "2009-09-21T20:50:47"
_4images_image_order: "44"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25046 -->
Münzsortierer