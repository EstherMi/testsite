---
layout: "image"
title: "Fritz Roller?"
date: "2009-11-02T21:41:44"
picture: "verschiedene21.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/25669
imported:
- "2019"
_4images_image_id: "25669"
_4images_cat_id: "1721"
_4images_user_id: "409"
_4images_image_date: "2009-11-02T21:41:44"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25669 -->
Bitte Konstrukteur nachtragen, stand auf Tisch 4c