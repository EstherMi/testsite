---
layout: "image"
title: "niekerk - Lauf - Roboter"
date: "2009-11-02T21:41:44"
picture: "verschiedene07.jpg"
weight: "2"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/25655
imported:
- "2019"
_4images_image_id: "25655"
_4images_cat_id: "1789"
_4images_user_id: "409"
_4images_image_date: "2009-11-02T21:41:44"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25655 -->
