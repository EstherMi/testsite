---
layout: "image"
title: "niekerk - Roborama Roboter"
date: "2009-11-02T21:41:44"
picture: "verschiedene06.jpg"
weight: "1"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/25654
imported:
- "2019"
_4images_image_id: "25654"
_4images_cat_id: "1789"
_4images_user_id: "409"
_4images_image_date: "2009-11-02T21:41:44"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25654 -->
