---
layout: "image"
title: "close up Gabellichtschranke"
date: "2009-09-19T22:03:48"
picture: "DSC_0019-1.jpg"
weight: "8"
konstrukteure: 
- "DMDBT"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/24958
imported:
- "2019"
_4images_image_id: "24958"
_4images_cat_id: "1748"
_4images_user_id: "371"
_4images_image_date: "2009-09-19T22:03:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24958 -->
