---
layout: "image"
title: "Prototyp einer neuen Laufmaschine"
date: "2009-09-23T20:48:33"
picture: "convention132.jpg"
weight: "48"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/25347
imported:
- "2019"
_4images_image_id: "25347"
_4images_cat_id: "1721"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:33"
_4images_image_order: "132"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25347 -->
