---
layout: "image"
title: "speedy68s Achterbahn"
date: "2009-09-23T20:48:31"
picture: "convention036.jpg"
weight: "1"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/25251
imported:
- "2019"
_4images_image_id: "25251"
_4images_cat_id: "1724"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:31"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25251 -->
