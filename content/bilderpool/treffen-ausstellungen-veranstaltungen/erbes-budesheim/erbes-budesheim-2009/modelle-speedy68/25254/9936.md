---
layout: "comment"
hidden: true
title: "9936"
date: "2009-09-24T23:11:57"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Thomas,
ja das Vorbild hat senkrechte Türme. Dann wird dir eine Abweichung davon mit geneigten Türmen wohl nicht erspart bleiben. Der Reibungsverlust in der Neigung des Turmes dürfte aber dann im ersten Bogen gegenüber vom senkrechten etwas kompensiert werden. Mit kleinem Spiel laufen die Wagen am besten.
Gruß Ingo