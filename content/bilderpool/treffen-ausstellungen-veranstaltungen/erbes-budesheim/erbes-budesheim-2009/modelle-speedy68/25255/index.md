---
layout: "image"
title: "speedy68s Achterbahn"
date: "2009-09-23T20:48:31"
picture: "convention040.jpg"
weight: "5"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/25255
imported:
- "2019"
_4images_image_id: "25255"
_4images_cat_id: "1724"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:31"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25255 -->
Auch nette Ideen zur Motorbefestigung: Vorne die Platte 15x45 mit je zwei Zapfen auf jeder Seite, hinten mit Winkelsteinen festgehalten.