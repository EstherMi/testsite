---
layout: "image"
title: "schnaggels Binärzähler"
date: "2009-09-23T20:48:31"
picture: "convention092.jpg"
weight: "5"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/25307
imported:
- "2019"
_4images_image_id: "25307"
_4images_cat_id: "1737"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:31"
_4images_image_order: "92"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25307 -->
Eine E-Tec-Orgie :-)