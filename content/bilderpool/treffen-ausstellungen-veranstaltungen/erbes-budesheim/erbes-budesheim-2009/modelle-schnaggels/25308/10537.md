---
layout: "comment"
hidden: true
title: "10537"
date: "2010-01-14T16:49:07"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Da ich zu dem Foto von schnaggel's Aufbau keinen Kommentar schrieb, antworte ich mal: Das sind E-Tec-Moduln, die als Flip-Flop eingestellt und hintereinander geschaltet wurden. Das ergibt einen Binärzähler.

Gruß,
Stefan