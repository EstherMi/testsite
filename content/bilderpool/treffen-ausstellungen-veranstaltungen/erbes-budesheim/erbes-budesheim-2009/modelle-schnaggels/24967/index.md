---
layout: "image"
title: "Binärzähler"
date: "2009-09-19T22:07:23"
picture: "conv3.jpg"
weight: "12"
konstrukteure: 
- "schnaggels"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/24967
imported:
- "2019"
_4images_image_id: "24967"
_4images_cat_id: "1737"
_4images_user_id: "456"
_4images_image_date: "2009-09-19T22:07:23"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24967 -->
