---
layout: "image"
title: "schnaggels Boot, Hexapod"
date: "2009-09-23T20:48:31"
picture: "convention088.jpg"
weight: "2"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/25303
imported:
- "2019"
_4images_image_id: "25303"
_4images_cat_id: "1737"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:31"
_4images_image_order: "88"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25303 -->
