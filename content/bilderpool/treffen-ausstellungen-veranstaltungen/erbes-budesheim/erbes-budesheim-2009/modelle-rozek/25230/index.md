---
layout: "image"
title: "3D-Drucker"
date: "2009-09-23T20:48:30"
picture: "convention015.jpg"
weight: "5"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/25230
imported:
- "2019"
_4images_image_id: "25230"
_4images_cat_id: "1751"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:30"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25230 -->
