---
layout: "image"
title: "rozek - FTIStrap"
date: "2009-10-08T17:22:55"
picture: "verschiedene25.jpg"
weight: "2"
konstrukteure: 
- "Andreas Rozek"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/25522
imported:
- "2019"
_4images_image_id: "25522"
_4images_cat_id: "1751"
_4images_user_id: "409"
_4images_image_date: "2009-10-08T17:22:55"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25522 -->
Bemerkenswerte Qualität!