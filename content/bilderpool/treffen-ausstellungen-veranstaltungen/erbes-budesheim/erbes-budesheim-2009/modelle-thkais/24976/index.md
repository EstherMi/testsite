---
layout: "image"
title: "Displays"
date: "2009-09-19T22:18:55"
picture: "conv4.jpg"
weight: "25"
konstrukteure: 
- "thkais"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/24976
imported:
- "2019"
_4images_image_id: "24976"
_4images_cat_id: "1741"
_4images_user_id: "456"
_4images_image_date: "2009-09-19T22:18:55"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24976 -->
