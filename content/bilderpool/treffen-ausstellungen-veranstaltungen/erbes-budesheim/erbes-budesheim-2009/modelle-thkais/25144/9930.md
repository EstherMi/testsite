---
layout: "comment"
hidden: true
title: "9930"
date: "2009-09-24T18:15:18"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Wenn ich richtig weiß, ist das

- ganz unten 1 Kugel => 1 Uhr bzw. 13 Uhr
- mittig 5 Kugeln => 5 Kugeln * 5 min/Kugel = 25 Minuten
- oben 1 Kugel >= 1 Minute

Gibt zusammen 13:26. Ich würde sagen, Udo2 hat recht.

Gruß,
Stefan