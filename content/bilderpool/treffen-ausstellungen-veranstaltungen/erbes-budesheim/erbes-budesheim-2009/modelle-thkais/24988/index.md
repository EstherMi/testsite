---
layout: "image"
title: "Kugelbahn: Clock??"
date: "2009-09-19T23:09:05"
picture: "DSC_0024.jpg"
weight: "21"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/24988
imported:
- "2019"
_4images_image_id: "24988"
_4images_cat_id: "1741"
_4images_user_id: "371"
_4images_image_date: "2009-09-19T23:09:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24988 -->
