---
layout: "image"
title: "Intelligent Interface"
date: "2009-09-19T22:18:54"
picture: "conv1.jpg"
weight: "22"
konstrukteure: 
- "thkais"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/24972
imported:
- "2019"
_4images_image_id: "24972"
_4images_cat_id: "1741"
_4images_user_id: "456"
_4images_image_date: "2009-09-19T22:18:54"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24972 -->
