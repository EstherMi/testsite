---
layout: "image"
title: "Thkais Kugeluhr"
date: "2009-09-23T20:48:32"
picture: "convention112.jpg"
weight: "8"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/25327
imported:
- "2019"
_4images_image_id: "25327"
_4images_cat_id: "1741"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:32"
_4images_image_order: "112"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25327 -->
