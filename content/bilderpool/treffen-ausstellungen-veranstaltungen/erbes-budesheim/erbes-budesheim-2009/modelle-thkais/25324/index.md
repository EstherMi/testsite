---
layout: "image"
title: "Thkais Fernsteuerung"
date: "2009-09-23T20:48:32"
picture: "convention109.jpg"
weight: "5"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/25324
imported:
- "2019"
_4images_image_id: "25324"
_4images_cat_id: "1741"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:32"
_4images_image_order: "109"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25324 -->
