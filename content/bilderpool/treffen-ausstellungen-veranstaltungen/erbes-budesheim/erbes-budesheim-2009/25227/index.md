---
layout: "image"
title: "Riesenrad"
date: "2009-09-23T20:48:30"
picture: "convention012.jpg"
weight: "26"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/25227
imported:
- "2019"
_4images_image_id: "25227"
_4images_cat_id: "1721"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:30"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25227 -->
