---
layout: "image"
title: "Kompressor"
date: "2009-09-23T20:48:32"
picture: "convention104.jpg"
weight: "38"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/25319
imported:
- "2019"
_4images_image_id: "25319"
_4images_cat_id: "1721"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:32"
_4images_image_order: "104"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25319 -->
