---
layout: "image"
title: "Motor"
date: "2009-09-23T21:52:35"
picture: "Motorconv.jpg"
weight: "22"
konstrukteure: 
- "Fischertechnik/Knobloch"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/25354
imported:
- "2019"
_4images_image_id: "25354"
_4images_cat_id: "1721"
_4images_user_id: "453"
_4images_image_date: "2009-09-23T21:52:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25354 -->
Auf der Convention gab es für jeden Aussteller eine Tüte mit lauter netten Dingen geschenkt, unter Anderem diese beiden bedruckten Teile. Danke an Ralf und sein Team dafür!
Außerdem konnte man sie auch kaufen, der S-Motor kostete 8 Euro das Stück, der Flachstein 0,20 Euro. (Bild von Frederik, Text von Masked)