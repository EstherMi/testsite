---
layout: "image"
title: "hleuz - ROBOTIC Universal Set"
date: "2009-10-08T17:22:55"
picture: "verschiedene22.jpg"
weight: "1"
konstrukteure: 
- "Henning Leuz"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/25519
imported:
- "2019"
_4images_image_id: "25519"
_4images_cat_id: "1788"
_4images_user_id: "409"
_4images_image_date: "2009-10-08T17:22:55"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25519 -->
ROBOTIC Universal Set