---
layout: "image"
title: "BRAIN-Roboter vs. TX-Roboter"
date: "2009-09-23T20:48:33"
picture: "convention125.jpg"
weight: "4"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/25340
imported:
- "2019"
_4images_image_id: "25340"
_4images_cat_id: "1788"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:33"
_4images_image_order: "125"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25340 -->
