---
layout: "image"
title: "Erbes-Budesheim-2009"
date: "2009-09-21T20:50:47"
picture: "erbesbuedesheim41.jpg"
weight: "43"
konstrukteure: 
- "FT-Experte"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/25043
imported:
- "2019"
_4images_image_id: "25043"
_4images_cat_id: "1775"
_4images_user_id: "22"
_4images_image_date: "2009-09-21T20:50:47"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25043 -->
Erbes-Büdesheim-2009