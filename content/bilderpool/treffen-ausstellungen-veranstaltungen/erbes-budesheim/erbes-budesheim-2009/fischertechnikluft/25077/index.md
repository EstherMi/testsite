---
layout: "image"
title: "At the end of the day"
date: "2009-09-22T21:44:14"
picture: "ftconvention10.jpg"
weight: "15"
konstrukteure: 
- "Frits Roller |; Carel van Leeuwen"
fotografen:
- "Carel van Leeuwen Enschede Netherlands"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/25077
imported:
- "2019"
_4images_image_id: "25077"
_4images_cat_id: "1775"
_4images_user_id: "136"
_4images_image_date: "2009-09-22T21:44:14"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25077 -->
around 16  o'clock. At the left two models fromf Frits. At the right the Eco windmill and MS-Robotics Studio demo form Carel. Controlling a FT-model with a WII-remote and a X360 console.