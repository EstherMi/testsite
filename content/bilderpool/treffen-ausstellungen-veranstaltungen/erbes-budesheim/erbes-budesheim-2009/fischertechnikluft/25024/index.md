---
layout: "image"
title: "Felix Geerken"
date: "2009-09-21T20:50:46"
picture: "erbesbuedesheim22.jpg"
weight: "38"
konstrukteure: 
- "FT-Experte"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/25024
imported:
- "2019"
_4images_image_id: "25024"
_4images_cat_id: "1775"
_4images_user_id: "22"
_4images_image_date: "2009-09-21T20:50:46"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25024 -->
FT-Tip