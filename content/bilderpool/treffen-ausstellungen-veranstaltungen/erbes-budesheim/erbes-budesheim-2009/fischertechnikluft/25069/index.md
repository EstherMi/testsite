---
layout: "image"
title: "Almost empty hall"
date: "2009-09-22T21:44:14"
picture: "ftconvention02.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen Enschede Netherlands"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/25069
imported:
- "2019"
_4images_image_id: "25069"
_4images_cat_id: "1775"
_4images_user_id: "136"
_4images_image_date: "2009-09-22T21:44:14"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25069 -->
around 9 o'clock.