---
layout: "image"
title: "Parcours"
date: "2009-09-23T20:48:29"
picture: "convention005.jpg"
weight: "5"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/25220
imported:
- "2019"
_4images_image_id: "25220"
_4images_cat_id: "1775"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:29"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25220 -->
Ich hab es nicht selbst gesehen, aber ich glaube, dies hier waren wohl die Fahrzeuge für die veranstalteten Wettrennen.