---
layout: "image"
title: "Almost empty hall"
date: "2009-09-22T21:44:14"
picture: "ftconvention04.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen Enschede Netherlands"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/25071
imported:
- "2019"
_4images_image_id: "25071"
_4images_cat_id: "1775"
_4images_user_id: "136"
_4images_image_date: "2009-09-22T21:44:14"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25071 -->
around 9 o'clock.