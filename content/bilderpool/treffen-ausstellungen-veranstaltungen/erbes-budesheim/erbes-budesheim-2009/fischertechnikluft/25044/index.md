---
layout: "image"
title: "Michael Sengstschmid"
date: "2009-09-21T20:50:47"
picture: "erbesbuedesheim42.jpg"
weight: "44"
konstrukteure: 
- "FT-Experte"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/25044
imported:
- "2019"
_4images_image_id: "25044"
_4images_cat_id: "1775"
_4images_user_id: "22"
_4images_image_date: "2009-09-21T20:50:47"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25044 -->
Münzsortierer