---
layout: "image"
title: "Verpflegungsbereich"
date: "2009-09-23T20:48:29"
picture: "convention001.jpg"
weight: "1"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/25216
imported:
- "2019"
_4images_image_id: "25216"
_4images_cat_id: "1775"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:29"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25216 -->
