---
layout: "image"
title: "Rauben-antrieb mit Druckfederung"
date: "2009-09-21T20:50:47"
picture: "erbesbuedesheim29.jpg"
weight: "19"
konstrukteure: 
- "FT-Experte"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/25031
imported:
- "2019"
_4images_image_id: "25031"
_4images_cat_id: "1723"
_4images_user_id: "22"
_4images_image_date: "2009-09-21T20:50:47"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25031 -->
Erbes-Büdesheim-2009