---
layout: "image"
title: "Roboterarm"
date: "2009-09-19T21:59:15"
picture: "conv3.jpg"
weight: "24"
konstrukteure: 
- "Severin"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/24949
imported:
- "2019"
_4images_image_id: "24949"
_4images_cat_id: "1723"
_4images_user_id: "456"
_4images_image_date: "2009-09-19T21:59:15"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24949 -->
