---
layout: "comment"
hidden: true
title: "9901"
date: "2009-09-22T07:36:52"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Sehr clever und sauber gebaut! Tolle Idee!

Ist aber eher eine Zugfederung, keine Druckfederung.

Gruß, Thomas