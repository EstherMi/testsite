---
layout: "image"
title: "Rauben-antrieb mit Druckfederung"
date: "2009-09-21T20:50:46"
picture: "erbesbuedesheim26.jpg"
weight: "18"
konstrukteure: 
- "FT-Experte"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/25028
imported:
- "2019"
_4images_image_id: "25028"
_4images_cat_id: "1723"
_4images_user_id: "22"
_4images_image_date: "2009-09-21T20:50:46"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25028 -->
Erbes-Büdesheim-2009