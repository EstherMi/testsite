---
layout: "image"
title: "ROBOProEntwickler"
date: "2009-10-08T17:22:55"
picture: "verschiedene33.jpg"
weight: "3"
konstrukteure: 
- "Michael Sögtrop"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/25530
imported:
- "2019"
_4images_image_id: "25530"
_4images_cat_id: "1755"
_4images_user_id: "409"
_4images_image_date: "2009-10-08T17:22:55"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25530 -->
ROBOProEntwickler