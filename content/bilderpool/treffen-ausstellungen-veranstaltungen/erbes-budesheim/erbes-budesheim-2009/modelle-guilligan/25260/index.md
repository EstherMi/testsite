---
layout: "image"
title: "Dirk Kutschs Riesenkran"
date: "2009-09-23T20:48:31"
picture: "convention045.jpg"
weight: "7"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/25260
imported:
- "2019"
_4images_image_id: "25260"
_4images_cat_id: "1727"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:31"
_4images_image_order: "45"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25260 -->
