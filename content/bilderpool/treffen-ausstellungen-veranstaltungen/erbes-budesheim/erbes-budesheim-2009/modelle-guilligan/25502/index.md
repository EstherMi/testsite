---
layout: "image"
title: "Guilligan"
date: "2009-10-08T17:22:54"
picture: "verschiedene05.jpg"
weight: "3"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/25502
imported:
- "2019"
_4images_image_id: "25502"
_4images_cat_id: "1727"
_4images_user_id: "409"
_4images_image_date: "2009-10-08T17:22:54"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25502 -->
Samstag nach dem Abendessen. Geschafft! Ist auch noch alles da?