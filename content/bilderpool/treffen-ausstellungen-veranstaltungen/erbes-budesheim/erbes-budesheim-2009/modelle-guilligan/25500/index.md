---
layout: "image"
title: "Guilligan - Kran"
date: "2009-10-08T17:22:54"
picture: "verschiedene03.jpg"
weight: "1"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/25500
imported:
- "2019"
_4images_image_id: "25500"
_4images_cat_id: "1727"
_4images_user_id: "409"
_4images_image_date: "2009-10-08T17:22:54"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25500 -->
Gigantisch!