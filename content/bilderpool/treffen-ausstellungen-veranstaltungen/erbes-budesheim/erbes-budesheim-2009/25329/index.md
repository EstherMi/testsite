---
layout: "image"
title: "Tierpark"
date: "2009-09-23T20:48:32"
picture: "convention114.jpg"
weight: "40"
konstrukteure: 
- "Convention-Aussteller"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/25329
imported:
- "2019"
_4images_image_id: "25329"
_4images_cat_id: "1721"
_4images_user_id: "104"
_4images_image_date: "2009-09-23T20:48:32"
_4images_image_order: "114"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25329 -->
fischertechnik, fischer Tip und anderes Spielzeug ergänzen sich also prima.