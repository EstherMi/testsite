---
layout: "image"
title: "DetailsToTheMax2.jpg"
date: "2012-10-20T19:54:32"
picture: "IMG_8300.JPG"
weight: "6"
konstrukteure: 
- "Jan-Willem Dekker"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/35883
imported:
- "2019"
_4images_image_id: "35883"
_4images_cat_id: "2639"
_4images_user_id: "4"
_4images_image_date: "2012-10-20T19:54:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35883 -->
Da hat jedes Teilchen sein Plätzchen. Was mich noch interessieren würde: wie kommt die Druckluft in den drehenden Aufbau?