---
layout: "image"
title: "6-Achs-Roboter von Severin"
date: "2012-10-03T10:59:00"
picture: "convention13_2.jpg"
weight: "1"
konstrukteure: 
- "Severin"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/35732
imported:
- "2019"
_4images_image_id: "35732"
_4images_cat_id: "2647"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:59:00"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35732 -->
