---
layout: "image"
title: "ftconvention67.jpg"
date: "2012-10-01T20:51:00"
picture: "ftconvention67.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/35690
imported:
- "2019"
_4images_image_id: "35690"
_4images_cat_id: "2647"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:00"
_4images_image_order: "67"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35690 -->
