---
layout: "image"
title: "Roboter Greifhand"
date: "2012-10-01T20:51:00"
picture: "ftconvention68.jpg"
weight: "5"
konstrukteure: 
- "Severin"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/35691
imported:
- "2019"
_4images_image_id: "35691"
_4images_cat_id: "2647"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:00"
_4images_image_order: "68"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35691 -->
