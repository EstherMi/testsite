---
layout: "image"
title: "Karlsruher Spiegelwagen - Magnus Fox"
date: "2012-10-03T10:59:00"
picture: "convention30.jpg"
weight: "2"
konstrukteure: 
- "Magnus Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/35749
imported:
- "2019"
_4images_image_id: "35749"
_4images_cat_id: "2655"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:59:00"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35749 -->
