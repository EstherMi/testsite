---
layout: "comment"
hidden: true
title: "17322"
date: "2012-10-01T15:34:19"
uploadBy:
- "DenkMal"
license: "unknown"
imported:
- "2019"
---
Die Kreise ausprobieren klappte nicht, ausrechnen und bauen war auch nicht wirklich besser. Erst beides zusammen passte dann. Deshalb hatte ich, nach leidvoller Erfahrung mit mehreren Umbauten, die Verbindungen Speiche/Kreis verschiebbar gemacht. Dieser Kreis fehlte ursprünglich, aber die Speichen bogen sich zu sehr durch.