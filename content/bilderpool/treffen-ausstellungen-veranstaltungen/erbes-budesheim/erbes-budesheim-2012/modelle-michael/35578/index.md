---
layout: "image"
title: "Riesiges Riesenrad"
date: "2012-10-01T11:53:10"
picture: "ftconventionerbesbuedesheim16.jpg"
weight: "18"
konstrukteure: 
- "DenkMal"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/35578
imported:
- "2019"
_4images_image_id: "35578"
_4images_cat_id: "2656"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T11:53:10"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35578 -->
Nochmal ein Bild des Wellenlagers. Schön stabil gebaut.