---
layout: "image"
title: "Riesenrad von Michael Stratmann - Gondeln"
date: "2012-10-03T10:59:00"
picture: "convention25.jpg"
weight: "1"
konstrukteure: 
- "Michael Stratmann"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/35744
imported:
- "2019"
_4images_image_id: "35744"
_4images_cat_id: "2656"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:59:00"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35744 -->
