---
layout: "image"
title: "Flaschenabfüllanlage von Stefan Falk"
date: "2012-10-03T10:58:59"
picture: "convention06_2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/35725
imported:
- "2019"
_4images_image_id: "35725"
_4images_cat_id: "2650"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:58:59"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35725 -->
