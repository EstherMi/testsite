---
layout: "image"
title: "Farbsortiermaschine von Sylvia Falk"
date: "2012-10-03T10:58:59"
picture: "convention04_2.jpg"
weight: "1"
konstrukteure: 
- "Sylvia Falk"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/35723
imported:
- "2019"
_4images_image_id: "35723"
_4images_cat_id: "2650"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:58:59"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35723 -->
