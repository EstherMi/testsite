---
layout: "image"
title: "Alberreien"
date: "2012-10-01T20:51:00"
picture: "ftconvention76.jpg"
weight: "8"
konstrukteure: 
- "Claus. W. Ludwig und Ralf Geerken"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/35699
imported:
- "2019"
_4images_image_id: "35699"
_4images_cat_id: "2663"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:00"
_4images_image_order: "76"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35699 -->
Hier mal von gaaanz nah auf dem Unimog.