---
layout: "image"
title: "Trippel-Trappel"
date: "2012-10-01T20:51:00"
picture: "ftconvention79.jpg"
weight: "11"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/35702
imported:
- "2019"
_4images_image_id: "35702"
_4images_cat_id: "2663"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:00"
_4images_image_order: "79"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35702 -->
Der kleine Robbi, in verbesserter Ausführung, durfte natürlich auf der diejährigen Convention auch nicht fehlen.