---
layout: "image"
title: "DSC09241"
date: "2012-10-20T23:33:50"
picture: "conv129.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Familie Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/36015
imported:
- "2019"
_4images_image_id: "36015"
_4images_cat_id: "2663"
_4images_user_id: "1162"
_4images_image_date: "2012-10-20T23:33:50"
_4images_image_order: "129"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36015 -->
