---
layout: "image"
title: "Portalkran von Sebastian Schräder"
date: "2012-10-03T10:59:00"
picture: "convention19.jpg"
weight: "14"
konstrukteure: 
- "Sebastian Schräder"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/35738
imported:
- "2019"
_4images_image_id: "35738"
_4images_cat_id: "2639"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:59:00"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35738 -->
