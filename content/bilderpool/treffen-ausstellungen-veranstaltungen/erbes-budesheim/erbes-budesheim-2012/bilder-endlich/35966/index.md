---
layout: "image"
title: "DSC09170"
date: "2012-10-20T23:33:49"
picture: "conv080.jpg"
weight: "63"
konstrukteure: 
- "-?-"
fotografen:
- "Familie Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/35966
imported:
- "2019"
_4images_image_id: "35966"
_4images_cat_id: "2680"
_4images_user_id: "1162"
_4images_image_date: "2012-10-20T23:33:49"
_4images_image_order: "80"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35966 -->
