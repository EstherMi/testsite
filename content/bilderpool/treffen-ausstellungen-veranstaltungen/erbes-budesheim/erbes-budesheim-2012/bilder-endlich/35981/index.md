---
layout: "image"
title: "DSC09189"
date: "2012-10-20T23:33:50"
picture: "conv095.jpg"
weight: "76"
konstrukteure: 
- "-?-"
fotografen:
- "Familie Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/35981
imported:
- "2019"
_4images_image_id: "35981"
_4images_cat_id: "2680"
_4images_user_id: "1162"
_4images_image_date: "2012-10-20T23:33:50"
_4images_image_order: "95"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35981 -->
