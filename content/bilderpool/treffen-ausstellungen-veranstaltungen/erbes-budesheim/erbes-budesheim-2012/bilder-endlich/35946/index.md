---
layout: "image"
title: "DSC09147"
date: "2012-10-20T23:33:49"
picture: "conv060.jpg"
weight: "46"
konstrukteure: 
- "-?-"
fotografen:
- "Familie Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/35946
imported:
- "2019"
_4images_image_id: "35946"
_4images_cat_id: "2680"
_4images_user_id: "1162"
_4images_image_date: "2012-10-20T23:33:49"
_4images_image_order: "60"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35946 -->
