---
layout: "image"
title: "DetailsToTheMax.jpg"
date: "2012-10-20T19:51:33"
picture: "IMG_8104.JPG"
weight: "5"
konstrukteure: 
- "Jan-Willem Dekker"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/35882
imported:
- "2019"
_4images_image_id: "35882"
_4images_cat_id: "2639"
_4images_user_id: "4"
_4images_image_date: "2012-10-20T19:51:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35882 -->
Details ohne Ende. Und alles, aber auch alles, wird hinterher auf die Transportfahrzeuge verladen.