---
layout: "image"
title: "Kipplader von Strobel"
date: "2012-10-03T10:59:00"
picture: "convention37.jpg"
weight: "3"
konstrukteure: 
- "Strobel"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/35756
imported:
- "2019"
_4images_image_id: "35756"
_4images_cat_id: "2658"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:59:00"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35756 -->
