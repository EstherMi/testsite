---
layout: "image"
title: "Zeppelin 2"
date: "2015-10-01T13:37:10"
picture: "Zeppelin_2.jpg"
weight: "18"
konstrukteure: 
- "Masked"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/42015
imported:
- "2019"
_4images_image_id: "42015"
_4images_cat_id: "3121"
_4images_user_id: "724"
_4images_image_date: "2015-10-01T13:37:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42015 -->
