---
layout: "image"
title: "Riesenbagger original"
date: "2015-10-01T13:37:10"
picture: "Riesenbagger_original.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/42009
imported:
- "2019"
_4images_image_id: "42009"
_4images_cat_id: "3121"
_4images_user_id: "724"
_4images_image_date: "2015-10-01T13:37:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42009 -->
