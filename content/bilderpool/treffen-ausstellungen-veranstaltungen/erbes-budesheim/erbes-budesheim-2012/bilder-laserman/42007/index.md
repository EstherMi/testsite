---
layout: "image"
title: "Riesenbagger vorne"
date: "2015-10-01T13:37:10"
picture: "Riesenbagger_vorne.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/42007
imported:
- "2019"
_4images_image_id: "42007"
_4images_cat_id: "3121"
_4images_user_id: "724"
_4images_image_date: "2015-10-01T13:37:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42007 -->
