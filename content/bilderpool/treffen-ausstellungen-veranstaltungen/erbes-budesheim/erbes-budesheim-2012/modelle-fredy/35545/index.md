---
layout: "image"
title: "Enloskugelbahn1"
date: "2012-09-29T21:24:41"
picture: "convention04.jpg"
weight: "1"
konstrukteure: 
- "Fredy"
fotografen:
- "fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- details/35545
imported:
- "2019"
_4images_image_id: "35545"
_4images_cat_id: "2653"
_4images_user_id: "1113"
_4images_image_date: "2012-09-29T21:24:41"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35545 -->
Eine von Fredys genialen Kugelbahnen