---
layout: "image"
title: "Enloskugelbahn2"
date: "2012-09-29T21:24:41"
picture: "convention05.jpg"
weight: "2"
konstrukteure: 
- "Fredy"
fotografen:
- "fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- details/35546
imported:
- "2019"
_4images_image_id: "35546"
_4images_cat_id: "2653"
_4images_user_id: "1113"
_4images_image_date: "2012-09-29T21:24:41"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35546 -->
Eine von Fredys genialen Kugelbahnen