---
layout: "image"
title: "Traktor von Claus Ludwig"
date: "2012-10-03T10:58:59"
picture: "convention01_2.jpg"
weight: "5"
konstrukteure: 
- "Claus Ludwig"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/35720
imported:
- "2019"
_4images_image_id: "35720"
_4images_cat_id: "2664"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:58:59"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35720 -->
