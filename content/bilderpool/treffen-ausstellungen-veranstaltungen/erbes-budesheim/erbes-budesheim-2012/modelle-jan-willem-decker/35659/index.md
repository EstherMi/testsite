---
layout: "image"
title: "Kinderflieger"
date: "2012-10-01T20:50:59"
picture: "ftconvention36.jpg"
weight: "9"
konstrukteure: 
- "Jan Willem Dekker"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/35659
imported:
- "2019"
_4images_image_id: "35659"
_4images_cat_id: "2671"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:50:59"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35659 -->
