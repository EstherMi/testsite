---
layout: "image"
title: "DSC09134"
date: "2012-10-20T23:33:49"
picture: "conv048.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Familie Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/35934
imported:
- "2019"
_4images_image_id: "35934"
_4images_cat_id: "2671"
_4images_user_id: "1162"
_4images_image_date: "2012-10-20T23:33:49"
_4images_image_order: "48"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35934 -->
