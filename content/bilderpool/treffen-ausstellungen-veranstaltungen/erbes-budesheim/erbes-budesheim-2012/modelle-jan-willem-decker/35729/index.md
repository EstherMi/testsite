---
layout: "image"
title: "Kinderkarussel von Jan-Willem Decker"
date: "2012-10-03T10:59:00"
picture: "convention10_2.jpg"
weight: "6"
konstrukteure: 
- "Jan-Willem Decker"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/35729
imported:
- "2019"
_4images_image_id: "35729"
_4images_cat_id: "2671"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:59:00"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35729 -->
