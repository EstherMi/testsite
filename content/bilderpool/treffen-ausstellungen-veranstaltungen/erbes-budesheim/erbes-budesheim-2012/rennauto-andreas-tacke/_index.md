---
layout: "overview"
title: "Rennauto von Andreas Tacke"
date: 2019-12-17T18:27:26+01:00
legacy_id:
- categories/2644
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2644 --> 
Außerhalb der Kernzeiten zeigte Andreas Tacke ein rückstoßgetriebenes Rennauto, dass an einer Schnur entlang fuhr und vergleichsweise hohe Geschwindigkeiten erzielte. 
Für mich zugleich die Herausforderung, während der Fahrt (!) einige Bilder zu ergattern.