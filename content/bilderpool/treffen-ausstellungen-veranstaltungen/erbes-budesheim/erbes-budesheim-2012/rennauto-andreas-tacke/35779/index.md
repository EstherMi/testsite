---
layout: "image"
title: "TSTs Rennauto 7"
date: "2012-10-03T21:24:35"
picture: "P1050110_verkleinert.jpg"
weight: "4"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Bernhard Lehner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Bernhard Lehner"
license: "unknown"
legacy_id:
- details/35779
imported:
- "2019"
_4images_image_id: "35779"
_4images_cat_id: "2644"
_4images_user_id: "1028"
_4images_image_date: "2012-10-03T21:24:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35779 -->
Die letzte Bilderserie: Das Auto von vorne/oben.