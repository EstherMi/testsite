---
layout: "image"
title: "Zeppelin1"
date: "2012-09-29T21:24:41"
picture: "convention10.jpg"
weight: "6"
konstrukteure: 
- "Masked"
fotografen:
- "fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- details/35551
imported:
- "2019"
_4images_image_id: "35551"
_4images_cat_id: "2648"
_4images_user_id: "1113"
_4images_image_date: "2012-09-29T21:24:41"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35551 -->
Ein Zeppelin, aus vier großen Spezialluftballons, die mit Helium gefüllt sind. Zeitweise auch mit einer Kamera über den Ständen unterwegs. Die Höhe und Richtung werden über zwei horizontal drehbare Minimotoren gesteuert.