---
layout: "image"
title: "Bagger von innen.jpg"
date: "2012-10-20T21:04:06"
picture: "IMG_8174.JPG"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/35886
imported:
- "2019"
_4images_image_id: "35886"
_4images_cat_id: "2639"
_4images_user_id: "4"
_4images_image_date: "2012-10-20T21:04:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35886 -->
Klotzen, nicht kleckern. Über die Z10 zur Linken werden zwei Powermots gekoppelt. Unten ist die Rastachse verstiftet (wahrscheinlich aus gutem Grund), und das graue Kardan wird mittels Kupferdraht am Zerspringen gehindert.