---
layout: "image"
title: "Fahrsimulator2.jpg"
date: "2012-10-20T19:42:49"
picture: "IMG_8068.JPG"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/35880
imported:
- "2019"
_4images_image_id: "35880"
_4images_cat_id: "2639"
_4images_user_id: "4"
_4images_image_date: "2012-10-20T19:42:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35880 -->
Hier geht die Bahn in eine Linkskurve -- die Plattform zeigt es auch, mit Neigung und mit Drehung.
Die aufgeräumte Verlegung der P-Schläuche hat einen Sonderpreis verdient.