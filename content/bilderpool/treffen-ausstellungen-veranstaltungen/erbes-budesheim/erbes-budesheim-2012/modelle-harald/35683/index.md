---
layout: "image"
title: "Panzer"
date: "2012-10-01T20:51:00"
picture: "ftconvention60.jpg"
weight: "19"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/35683
imported:
- "2019"
_4images_image_id: "35683"
_4images_cat_id: "2652"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:00"
_4images_image_order: "60"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35683 -->
