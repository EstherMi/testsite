---
layout: "image"
title: "Die 2CV oder auch 'ENTE'"
date: "2012-10-01T20:51:00"
picture: "ftconvention53.jpg"
weight: "12"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/35676
imported:
- "2019"
_4images_image_id: "35676"
_4images_cat_id: "2652"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:00"
_4images_image_order: "53"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35676 -->
