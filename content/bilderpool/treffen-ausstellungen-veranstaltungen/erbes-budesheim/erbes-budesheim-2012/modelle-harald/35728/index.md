---
layout: "image"
title: "Schnellboot von Harald Steinhaus"
date: "2012-10-03T10:58:59"
picture: "convention09_2.jpg"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/35728
imported:
- "2019"
_4images_image_id: "35728"
_4images_cat_id: "2652"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:58:59"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35728 -->
