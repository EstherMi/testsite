---
layout: "image"
title: "Fahrwerk von Haralds Stealth-Bomber"
date: "2012-10-03T10:59:01"
picture: "convention52.jpg"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/35771
imported:
- "2019"
_4images_image_id: "35771"
_4images_cat_id: "2652"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:59:01"
_4images_image_order: "52"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35771 -->
