---
layout: "image"
title: "Mammut Kran"
date: "2012-10-01T20:50:59"
picture: "ftconvention31.jpg"
weight: "17"
konstrukteure: 
- "W. Starreveld"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/35654
imported:
- "2019"
_4images_image_id: "35654"
_4images_cat_id: "2662"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:50:59"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35654 -->
Interessante Seilführung