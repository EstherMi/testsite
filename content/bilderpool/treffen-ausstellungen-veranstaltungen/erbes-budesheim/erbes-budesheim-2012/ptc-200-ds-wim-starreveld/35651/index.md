---
layout: "image"
title: "Der Ausleger"
date: "2012-10-01T20:50:59"
picture: "ftconvention28.jpg"
weight: "14"
konstrukteure: 
- "W. Starreveld"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/35651
imported:
- "2019"
_4images_image_id: "35651"
_4images_cat_id: "2662"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:50:59"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35651 -->
Im Hintergrund das grosse Riesenrad von der Seite