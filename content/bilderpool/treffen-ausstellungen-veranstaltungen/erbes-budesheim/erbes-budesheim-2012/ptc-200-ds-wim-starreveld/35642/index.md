---
layout: "image"
title: "Der Konstrukteur an seinem Modell"
date: "2012-10-01T20:50:59"
picture: "ftconvention19.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/35642
imported:
- "2019"
_4images_image_id: "35642"
_4images_cat_id: "2662"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:50:59"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35642 -->
