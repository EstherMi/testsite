---
layout: "image"
title: "Die Lager vom Ausleger"
date: "2012-10-01T20:50:59"
picture: "ftconvention22.jpg"
weight: "8"
konstrukteure: 
- "W. Starreveld"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/35645
imported:
- "2019"
_4images_image_id: "35645"
_4images_cat_id: "2662"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:50:59"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35645 -->
Hier mal etwas näher zu sehen.