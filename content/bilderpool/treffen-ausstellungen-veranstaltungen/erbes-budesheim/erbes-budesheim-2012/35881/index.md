---
layout: "image"
title: "barrierefrei.jpg"
date: "2012-10-20T19:45:53"
picture: "IMG_8080.JPG"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/35881
imported:
- "2019"
_4images_image_id: "35881"
_4images_cat_id: "2639"
_4images_user_id: "4"
_4images_image_date: "2012-10-20T19:45:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35881 -->
Das Riesenrad hat zweierlei Gondeln: solche mit schmalen und solche mit breiten Türen. Da passen auch Rollstühle hindurch. Es gibt am Riesenrad natürlich auch eine Auffahrtrampe, damit man bis zum "Bahnsteig" kommt.