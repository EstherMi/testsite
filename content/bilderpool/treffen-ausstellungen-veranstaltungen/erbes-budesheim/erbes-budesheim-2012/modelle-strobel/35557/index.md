---
layout: "image"
title: "KlappbrückeSystem Strobel 4"
date: "2012-09-29T21:24:59"
picture: "convention16.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- details/35557
imported:
- "2019"
_4images_image_id: "35557"
_4images_cat_id: "2670"
_4images_user_id: "1113"
_4images_image_date: "2012-09-29T21:24:59"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35557 -->
Siehe KlappbrückeSystem Strobel1