---
layout: "image"
title: "Zugmaschine von Strobel"
date: "2012-10-03T10:59:00"
picture: "convention33.jpg"
weight: "1"
konstrukteure: 
- "Strobel"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/35752
imported:
- "2019"
_4images_image_id: "35752"
_4images_cat_id: "2670"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:59:00"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35752 -->
