---
layout: "image"
title: "ft Truck"
date: "2012-10-01T11:53:10"
picture: "ftconventionerbesbuedesheim23.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/35585
imported:
- "2019"
_4images_image_id: "35585"
_4images_cat_id: "2670"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T11:53:10"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35585 -->
Sehr schöner und funktioneller Truck