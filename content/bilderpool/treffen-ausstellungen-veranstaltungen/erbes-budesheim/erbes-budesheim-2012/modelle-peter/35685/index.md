---
layout: "image"
title: "Fischertechnik Sommer Austellung-2012"
date: "2012-10-01T20:51:00"
picture: "ftconvention62.jpg"
weight: "5"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/35685
imported:
- "2019"
_4images_image_id: "35685"
_4images_cat_id: "2668"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:00"
_4images_image_order: "62"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35685 -->
Leider hab ich die nur im geschlossenen Zustand gesehen.