---
layout: "image"
title: "Kettenflieger"
date: "2012-10-01T20:51:00"
picture: "ftconvention44.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/35667
imported:
- "2019"
_4images_image_id: "35667"
_4images_cat_id: "2649"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:00"
_4images_image_order: "44"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35667 -->
Hier sieht man die Hubmechanik die das Karussel in die Höhe hebt.