---
layout: "image"
title: "DSC09143"
date: "2012-10-20T23:33:49"
picture: "conv057.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Familie Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/35943
imported:
- "2019"
_4images_image_id: "35943"
_4images_cat_id: "2649"
_4images_user_id: "1162"
_4images_image_date: "2012-10-20T23:33:49"
_4images_image_order: "57"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35943 -->
