---
layout: "image"
title: "Riesen Minenbaggermodell"
date: "2012-10-01T20:51:01"
picture: "ftconvention94.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/35717
imported:
- "2019"
_4images_image_id: "35717"
_4images_cat_id: "2661"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:01"
_4images_image_order: "94"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35717 -->
Hier ist der Baggerarm in höchster Position.