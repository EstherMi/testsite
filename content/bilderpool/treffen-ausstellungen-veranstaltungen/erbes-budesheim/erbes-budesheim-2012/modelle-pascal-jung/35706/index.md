---
layout: "image"
title: "Prospektbild vom neuen Minenbagger"
date: "2012-10-01T20:51:00"
picture: "ftconvention83.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/35706
imported:
- "2019"
_4images_image_id: "35706"
_4images_cat_id: "2661"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:00"
_4images_image_order: "83"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35706 -->
Hier das Bild von der Schürflöffelausführung.