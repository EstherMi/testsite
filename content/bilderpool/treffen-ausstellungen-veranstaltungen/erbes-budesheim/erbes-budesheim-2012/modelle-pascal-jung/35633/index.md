---
layout: "image"
title: "Hinweisschild"
date: "2012-10-01T20:50:59"
picture: "ftconvention10.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/35633
imported:
- "2019"
_4images_image_id: "35633"
_4images_cat_id: "2661"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:50:59"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35633 -->
Klappbrücke System "Strobel"