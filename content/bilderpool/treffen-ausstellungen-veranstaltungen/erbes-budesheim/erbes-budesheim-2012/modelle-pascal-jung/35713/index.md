---
layout: "image"
title: "Der Hebemechaniusmus"
date: "2012-10-01T20:51:00"
picture: "ftconvention90.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/35713
imported:
- "2019"
_4images_image_id: "35713"
_4images_cat_id: "2661"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:00"
_4images_image_order: "90"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35713 -->
Klasse gebaut mit Schneckenantrieb. Soviele hätte ich nicht ...