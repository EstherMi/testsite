---
layout: "image"
title: "Schaufel des Liebherr r9800 von Pascal Jung"
date: "2012-10-03T10:59:00"
picture: "convention12_2.jpg"
weight: "2"
konstrukteure: 
- "Pascal Jung"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/35731
imported:
- "2019"
_4images_image_id: "35731"
_4images_cat_id: "2661"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:59:00"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35731 -->
