---
layout: "image"
title: "Münzsortierer2"
date: "2012-09-29T21:24:41"
picture: "convention02.jpg"
weight: "3"
konstrukteure: 
- "lasermann"
fotografen:
- "fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- details/35543
imported:
- "2019"
_4images_image_id: "35543"
_4images_cat_id: "2651"
_4images_user_id: "1113"
_4images_image_date: "2012-09-29T21:24:41"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35543 -->
Siehe Münzsortierer1