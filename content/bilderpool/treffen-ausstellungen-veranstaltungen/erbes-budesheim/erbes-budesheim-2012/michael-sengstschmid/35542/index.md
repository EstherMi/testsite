---
layout: "image"
title: "Münzsortierer1"
date: "2012-09-29T21:24:41"
picture: "convention01.jpg"
weight: "2"
konstrukteure: 
- "lasermann"
fotografen:
- "fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fish"
license: "unknown"
legacy_id:
- details/35542
imported:
- "2019"
_4images_image_id: "35542"
_4images_cat_id: "2651"
_4images_user_id: "1113"
_4images_image_date: "2012-09-29T21:24:41"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35542 -->
Ein gegenüber der letzten Convention verbesserter Münzsortierer, jetzt mit einem Rund mit Löchern durch die die Münzen fallen.

Ergänzung:
Man die runde, rote Scheibe mit den Löchern oben im Bild, hiermit wird sortiert. Die Löcher sind unterschiedlich groß, sodass durch das erste Loch nur die 1ct Münzen fallen, durch das zweite nur 2ct, dann 10ct usw. Die Maschine funktioniert fehlerfrei! Die Münzen werden von Loch zu Loch geschoben, bis sie in eins hineinfallen.