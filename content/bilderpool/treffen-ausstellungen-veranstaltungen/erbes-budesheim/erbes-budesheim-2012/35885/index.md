---
layout: "image"
title: "Bagger von unten1.jpg"
date: "2012-10-20T21:00:17"
picture: "IMG_8171.JPG"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/35885
imported:
- "2019"
_4images_image_id: "35885"
_4images_cat_id: "2639"
_4images_user_id: "4"
_4images_image_date: "2012-10-20T21:00:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35885 -->
Ein Blick von unten in den Monsterbagger. Links oben die Kiste mit Ballast.