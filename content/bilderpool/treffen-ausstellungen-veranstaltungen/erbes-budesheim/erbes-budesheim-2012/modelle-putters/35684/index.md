---
layout: "image"
title: "Kran mit verschiebbarem Gegengewicht"
date: "2012-10-01T20:51:00"
picture: "ftconvention61.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/35684
imported:
- "2019"
_4images_image_id: "35684"
_4images_cat_id: "2666"
_4images_user_id: "130"
_4images_image_date: "2012-10-01T20:51:00"
_4images_image_order: "61"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35684 -->
