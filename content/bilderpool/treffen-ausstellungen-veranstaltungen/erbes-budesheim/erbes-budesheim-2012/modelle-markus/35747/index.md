---
layout: "image"
title: "Prater von Markus Wolf - barrierefreier Zugang"
date: "2012-10-03T10:59:00"
picture: "convention28.jpg"
weight: "4"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/35747
imported:
- "2019"
_4images_image_id: "35747"
_4images_cat_id: "2654"
_4images_user_id: "1126"
_4images_image_date: "2012-10-03T10:59:00"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35747 -->
