---
layout: "image"
title: "RoboCup-Roboter"
date: "2014-10-04T23:18:43"
picture: "modellederfischertechnikagdesbismarckgymnasiumska5.jpg"
weight: "5"
konstrukteure: 
- "Schüler der fischertechnik-AG des Bismarck-Gymnasiums Karlsruhe"
fotografen:
- "Dirk Fox, Johann Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/39658
imported:
- "2019"
_4images_image_id: "39658"
_4images_cat_id: "2970"
_4images_user_id: "1126"
_4images_image_date: "2014-10-04T23:18:43"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39658 -->
Näheres im [url="http://www.fischertechnik-ag.de"]Wiki der fischertechnik-AG am Bismarck-Gymnasium Karlsruhe[/url]