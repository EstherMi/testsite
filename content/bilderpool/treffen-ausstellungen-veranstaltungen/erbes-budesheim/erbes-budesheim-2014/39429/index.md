---
layout: "image"
title: "Quadrokopter"
date: "2014-09-28T13:06:32"
picture: "ebbilderseverin52.jpg"
weight: "42"
konstrukteure: 
- "thkais"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/39429
imported:
- "2019"
_4images_image_id: "39429"
_4images_cat_id: "2951"
_4images_user_id: "558"
_4images_image_date: "2014-09-28T13:06:32"
_4images_image_order: "52"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39429 -->
