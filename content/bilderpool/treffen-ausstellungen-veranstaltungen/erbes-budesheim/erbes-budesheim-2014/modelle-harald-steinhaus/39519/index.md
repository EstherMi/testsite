---
layout: "image"
title: "Portalhubwagen I"
date: "2014-10-03T16:42:19"
picture: "modellevonharaldsteinhaus2.jpg"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Dirk Fox, Johann Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/39519
imported:
- "2019"
_4images_image_id: "39519"
_4images_cat_id: "2961"
_4images_user_id: "1126"
_4images_image_date: "2014-10-03T16:42:19"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39519 -->
