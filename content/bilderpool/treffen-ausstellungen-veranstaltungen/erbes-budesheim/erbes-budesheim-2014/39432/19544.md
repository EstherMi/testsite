---
layout: "comment"
hidden: true
title: "19544"
date: "2014-09-28T15:10:41"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Beim Danke-Sagen an Ralf und sein Team. Thomas Kaiser erzählt aus dem Nähkästchen, was der E-Tec-Nachfolger im neuen Electronics-Kasten alles für fantastische Features hat. Thomas hat den maßgeblich entwickelt und echt reingepackt, was nur geht - eine absolute Spitzenleistung!