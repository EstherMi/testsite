---
layout: "comment"
hidden: true
title: "19551"
date: "2014-09-28T21:09:00"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Das hat der Stefan sehr fein beobachtet. 4mm-Alu Rund, Vollmaterial (gut sortierter Baumarkt), gelagert in Klemmhülsen 35980.

Der Höhenunterschied zwischen der ansteigenden Rampe nach dem Looping (links vorne im Bild der graue Arm vom Oktogon) bis zum Eintritt ins Mühlenhäuschen (unter dem roten Dach) beträgt (rechnerisch) 2,5mm. Die Bahnlänge dieses Teilstückes ist etwa 2,5m.

Danke für das tolle Foto - und das es schon so schnell online ist ...