---
layout: "image"
title: "FT-Convention 2014: 20-Meter-Turm"
date: "2014-09-28T13:06:32"
picture: "margau3.jpg"
weight: "15"
konstrukteure: 
- "DenkMal"
fotografen:
- "margau"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "margau"
license: "unknown"
legacy_id:
- details/39375
imported:
- "2019"
_4images_image_id: "39375"
_4images_cat_id: "2966"
_4images_user_id: "1805"
_4images_image_date: "2014-09-28T13:06:32"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39375 -->
Hier sieht man den auf dem Sportplatz aufgebauten 20-Meter-Turm aus Fischertechnik