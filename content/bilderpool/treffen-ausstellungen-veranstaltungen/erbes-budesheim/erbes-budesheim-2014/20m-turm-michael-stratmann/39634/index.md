---
layout: "image"
title: "Der Konstrukteur"
date: "2014-10-04T15:59:57"
picture: "turmaufbau5.jpg"
weight: "9"
konstrukteure: 
- "DenkMal"
fotografen:
- "Martin W. (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/39634
imported:
- "2019"
_4images_image_id: "39634"
_4images_cat_id: "2966"
_4images_user_id: "373"
_4images_image_date: "2014-10-04T15:59:57"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39634 -->
macht auch mal ein Foto