---
layout: "image"
title: "Anbau des 'Fundaments'"
date: "2014-10-04T23:18:43"
picture: "turmvonmichaelstratmann4.jpg"
weight: "8"
konstrukteure: 
- "Michael Stratmann"
fotografen:
- "Dirk Fox, Johann Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/39648
imported:
- "2019"
_4images_image_id: "39648"
_4images_cat_id: "2966"
_4images_user_id: "1126"
_4images_image_date: "2014-10-04T23:18:43"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39648 -->
