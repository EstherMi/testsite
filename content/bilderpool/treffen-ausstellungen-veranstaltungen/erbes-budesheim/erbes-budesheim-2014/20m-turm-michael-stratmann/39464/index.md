---
layout: "image"
title: "20m Turm im Auto"
date: "2014-09-29T17:27:54"
picture: "20140927_233033.jpg"
weight: "14"
konstrukteure: 
- "Michael Stratmann"
fotografen:
- "Michael Stratmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DenkMal"
license: "unknown"
legacy_id:
- details/39464
imported:
- "2019"
_4images_image_id: "39464"
_4images_cat_id: "2966"
_4images_user_id: "1470"
_4images_image_date: "2014-09-29T17:27:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39464 -->
Dank der vielen helfenden Hände wurde der Turm für den Heimweg zu meinem bisher kleinsten Modell!