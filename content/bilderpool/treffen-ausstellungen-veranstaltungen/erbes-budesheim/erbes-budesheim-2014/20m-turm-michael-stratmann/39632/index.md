---
layout: "image"
title: "Entscheidung gefallen"
date: "2014-10-04T15:59:57"
picture: "turmaufbau3.jpg"
weight: "5"
konstrukteure: 
- "DenkMal"
fotografen:
- "Martin W. (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/39632
imported:
- "2019"
_4images_image_id: "39632"
_4images_cat_id: "2966"
_4images_user_id: "373"
_4images_image_date: "2014-10-04T15:59:57"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39632 -->
4 Absperrstangen und mehrere Dutzend Kabelbinder lösen das Problem.
Der Turm wurde von oben nach unten aufgebaut, indem er etwa einen Meter angehoben wurde, dann einen neues Modul drunter gestellt und verbunden wurde und wieder von vorne.
Benjamin hält den Turm fest, damit er in die offene Richtung nicht umkippen kann. Später war er dann beim frei hängenden Turm für die Schwingungsdämpfung zuständig, während der Rest S-Riegel setzte.