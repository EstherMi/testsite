---
layout: "image"
title: "Letztes Modul"
date: "2014-10-04T15:59:57"
picture: "turmaufbau6.jpg"
weight: "11"
konstrukteure: 
- "DenkMal"
fotografen:
- "Martin W. (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/39635
imported:
- "2019"
_4images_image_id: "39635"
_4images_cat_id: "2966"
_4images_user_id: "373"
_4images_image_date: "2014-10-04T15:59:57"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39635 -->
Das letzte Modul steht und ist verbunden. Michael, Thomas und Benjamin kontrollieren, ob alle S-Riegel drin sind. Noch schwebt der Turm etwas bzw. steht noch nicht auf seinem eigenen Beinen.