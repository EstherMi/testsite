---
layout: "image"
title: "Turm mit Teleskopkran"
date: "2014-10-04T23:18:43"
picture: "turmvonmichaelstratmann1.jpg"
weight: "2"
konstrukteure: 
- "Michael Stratmann"
fotografen:
- "Dirk Fox, Johann Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/39645
imported:
- "2019"
_4images_image_id: "39645"
_4images_cat_id: "2966"
_4images_user_id: "1126"
_4images_image_date: "2014-10-04T23:18:43"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39645 -->
