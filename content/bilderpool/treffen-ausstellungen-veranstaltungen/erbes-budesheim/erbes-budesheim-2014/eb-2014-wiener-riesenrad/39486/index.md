---
layout: "image"
title: "EB 2014 Das Riesenrad aus dem Wiener Prater"
date: "2014-10-02T09:00:25"
picture: "ebwienerriesenrad7.jpg"
weight: "18"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/39486
imported:
- "2019"
_4images_image_id: "39486"
_4images_cat_id: "2956"
_4images_user_id: "968"
_4images_image_date: "2014-10-02T09:00:25"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39486 -->
Den Antrieb werde ich wohl noch mal umbauen und einen Motor dazu bauen.
Der einzelne Motor hat den Tag zwar geschafft, wurde aber reichlich warm.