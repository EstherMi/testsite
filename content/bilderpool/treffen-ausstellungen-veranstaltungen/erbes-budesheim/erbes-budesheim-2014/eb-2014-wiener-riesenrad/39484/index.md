---
layout: "image"
title: "EB 2014 Das Riesenrad aus dem Wiener Prater"
date: "2014-10-02T09:00:25"
picture: "ebwienerriesenrad5.jpg"
weight: "16"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/39484
imported:
- "2019"
_4images_image_id: "39484"
_4images_cat_id: "2956"
_4images_user_id: "968"
_4images_image_date: "2014-10-02T09:00:25"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39484 -->
Das Rad wird von einem umlaufenden Seil angetrieben.
Hier die Seilspanner . In den grauen Kästchen stecken Bleigewichte.