---
layout: "image"
title: "Pneumatischer Bausteinauswerfer"
date: "2014-10-03T22:04:09"
picture: "modellestefanfalk4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Gudula Kiehle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/39549
imported:
- "2019"
_4images_image_id: "39549"
_4images_cat_id: "2964"
_4images_user_id: "1126"
_4images_image_date: "2014-10-03T22:04:09"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39549 -->
