---
layout: "image"
title: "Talstation Schlepplift"
date: "2014-10-03T16:42:19"
picture: "kubelbahnvonrenetrapp07.jpg"
weight: "24"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "Dirk Fox, Gudula Kiehle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/39528
imported:
- "2019"
_4images_image_id: "39528"
_4images_cat_id: "2962"
_4images_user_id: "1126"
_4images_image_date: "2014-10-03T16:42:19"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39528 -->
