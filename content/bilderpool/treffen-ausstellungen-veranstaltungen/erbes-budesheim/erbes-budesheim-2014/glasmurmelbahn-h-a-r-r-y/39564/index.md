---
layout: "image"
title: "Der Schlepplift - Talstation"
date: "2014-10-04T09:49:34"
picture: "glasmurmelbahn13.jpg"
weight: "13"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["Kugelbahn", "16mm", "Glasmurmel", "glas", "marble", "rolling", "ball", "marble", "track", "fischertechnik"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/39564
imported:
- "2019"
_4images_image_id: "39564"
_4images_cat_id: "2962"
_4images_user_id: "1557"
_4images_image_date: "2014-10-04T09:49:34"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39564 -->
Gleich neben der Mühle beginnt der Schlepplift. In der Talstation steigen die Murmeln in den Lift ein. Genauer gesagt bleiben die Murmeln an der tiefsten Stelle liegen und werden vom herunterklappenden Schleppschlitten mitgenommen. Eine einfache Schleuse (von der vorderen rechten Säule verdeckt) verhindert, dass sich zu viele Murmeln im Mitnahmebereich drängeln. Die Funktion der Schleuse wird mit der Seilwinde optimal eingestellt.

Oops, da ist ja gerade Stau im Mühlenauslauf! Nicht schlimm, der löst sich gleich wieder von alleine auf.

---

Aside the mill the drag lift starts. At the valley station the marbles enter the lift. The marbles stop there movement at the lowest position and the buckets hinge down to take them to the journey. A simple mechanism forms an inlet lock. This way the count of marbles entering the lift can be controlled. The winch (blue rope) allows for adjusting the lock mechanism.

Some marbles just jam back into the mill but this will clear away by itself soon.