---
layout: "image"
title: "Gesamtansicht Kugelbahn"
date: "2014-10-03T16:42:19"
picture: "kubelbahnvonrenetrapp01.jpg"
weight: "18"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "Dirk Fox, Gudula Kiehle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/39522
imported:
- "2019"
_4images_image_id: "39522"
_4images_cat_id: "2962"
_4images_user_id: "1126"
_4images_image_date: "2014-10-03T16:42:19"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39522 -->
