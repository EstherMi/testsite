---
layout: "image"
title: "Blick durch das westliche Seitenportal nach Osten"
date: "2014-10-04T09:49:34"
picture: "glasmurmelbahn05.jpg"
weight: "5"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["Kugelbahn", "16mm", "Glasmurmel", "Torbogen", "Mühlrad", "glas", "marble", "rolling", "ball", "marble", "track", "fischertechnik"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/39556
imported:
- "2019"
_4images_image_id: "39556"
_4images_cat_id: "2962"
_4images_user_id: "1557"
_4images_image_date: "2014-10-04T09:49:34"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39556 -->
Ham' se Lust auf 'nen kleinen 'Rundgang'?

---

Would you like to take a small 'walkabout'?