---
layout: "image"
title: "Das Heberad"
date: "2014-10-04T09:49:34"
picture: "glasmurmelbahn07.jpg"
weight: "7"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["Kugelbahn", "16mm", "Glasmurmel", "Rauf", "Hohlrad", "48-Eck", "glas", "marble", "rolling", "ball", "marble", "track", "fischertechnik"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/39558
imported:
- "2019"
_4images_image_id: "39558"
_4images_cat_id: "2962"
_4images_user_id: "1557"
_4images_image_date: "2014-10-04T09:49:34"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39558 -->
Die Murmeln werden von rechts unten ins Rad geleitet. Sie rollen in eine der 24 Kammern und werden nach oben mitgenommen. Ganz kurz vor dem Zenith verlassen die Murmeln das Rad wieder. Das Rad dreht sich im Uhrzeigersinn.

---

The downside arriving marbles are guided from the right hand side to the wheel. Each marble just rolls into one of the 24 chambers and gets lifted to the top. Just a moment before they arrive at the zenith position the marbles leave the wheel to the back. The wheel turns clockwise.