---
layout: "image"
title: "Die Südseite"
date: "2014-10-04T09:49:34"
picture: "glasmurmelbahn01.jpg"
weight: "1"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["Kugelbahn", "16mm", "Glasmurmel", "glas", "marble", "rolling", "ball", "marble", "track", "marble", "run", "fischertechnik"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/39552
imported:
- "2019"
_4images_image_id: "39552"
_4images_cat_id: "2962"
_4images_user_id: "1557"
_4images_image_date: "2014-10-04T09:49:34"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39552 -->
Die Glasmurmelbahn von Ihrer schönsten Seite. Links ist das Hebewerk mit dem markanten Heberad. Von dort geht es mit 75% Gefälle zum Looping. Aus dem Looping heraus werden die Murmeln mit 42% Steigung wieder abgebremst und laufen durch die 180°-Kurve des Oktogons (ganz rechts). Die Bahn geht dann mit einer langen Geraden durch die Hilfsstütze nach links. Dort umrundet eine weitere, engere, 180°-Kehre den Fuß der Bergstation und geht wieder kerzengerade von links unter das Dach des Mühlenhäuschens. Aus dem Mühlenhäuschen kommen die Murmeln nach links heraus und werden über die Verteilwippe abwechselnd zurück zum Heberad oder zum Schlepplift geleitet. Der Schlepplift bringt die Murmeln zur Bergstation und über eine 180°-Kurve rollen diese zum Ausstieg des Heberades. Dort schließt sich der Kreis.

Das Foto entstand unmittelbar vor dem Start der Hebeaggregate - die Murmeln liegen noch schön nach Farbe sortiert in der Zuführung des Heberades.
----------------
Huub van Niekerk made a short clip at the Convention 2014: https://www.youtube.com/watch?v=MN5DGxAYMCg
----------------
English description (not 1:1 translated):
This is a marble run for glas marbles. On the left hand side there are the two lifters installed. In the front a big hollow wheel turns and lifts marbles to a steep descend (75%). The marbles then run through the looping and climb up a 42° braking slope. Each marble then runs through a level 180° bend and returns to the left side. There (not visible) a seconde narrower 180° bend makes them run to the right again. All ends up at the red roof. Underneath there is a wheel as in a water driven mill. The marbels pass the wheel and leave it nearly at the bottom level. A dividing device sends half of the marbles back to the big lifting wheel, the remaining share is sent to the background into the diagonal lifter. It resembles to a bucket excavator and lifts the marbles up the inclined track. At the top the marbles are guided to the output of the lifting wheel. So both lifters share the same tracks.