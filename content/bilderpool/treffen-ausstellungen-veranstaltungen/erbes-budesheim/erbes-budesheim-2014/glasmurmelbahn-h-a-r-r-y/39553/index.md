---
layout: "image"
title: "Die Ostseite"
date: "2014-10-04T09:49:34"
picture: "glasmurmelbahn02.jpg"
weight: "2"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["Kugelbahn", "16mm", "Glasmurmel", "glas", "marble", "rolling", "ball", "marble", "track", "fischertechnik"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/39553
imported:
- "2019"
_4images_image_id: "39553"
_4images_cat_id: "2962"
_4images_user_id: "1557"
_4images_image_date: "2014-10-04T09:49:34"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39553 -->
Der Globus wird von der Kette aus dem Mühlenhäuschen heraus angetrieben; geduldige Beobachter konnten es erkennen: Die kleine Erde dreht sich sehr langsam in Richtung Osten.

---

The small terrestrial globe is driven by the mill via the long chain. Patient visitors could notice the slow rotation to the east.