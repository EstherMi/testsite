---
layout: "image"
title: "Stahlwalzstraße Walzgerüst"
date: "2015-10-01T13:37:10"
picture: "Stahlwalzstrae_Walzgerst.jpg"
weight: "28"
konstrukteure: 
- "Die Mädels"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/41963
imported:
- "2019"
_4images_image_id: "41963"
_4images_cat_id: "3119"
_4images_user_id: "724"
_4images_image_date: "2015-10-01T13:37:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41963 -->
