---
layout: "image"
title: "Fischertechnik Saudi Arabia"
date: "2014-10-03T22:04:09"
picture: "besucheraussaudiarabia2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/39534
imported:
- "2019"
_4images_image_id: "39534"
_4images_cat_id: "2963"
_4images_user_id: "22"
_4images_image_date: "2014-10-03T22:04:09"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39534 -->
