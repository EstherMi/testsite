---
layout: "image"
title: "Rochen"
date: "2014-10-03T16:42:19"
picture: "modellevonpeterdamen2.jpg"
weight: "2"
konstrukteure: 
- "Peter Damen"
fotografen:
- "Johann Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/39509
imported:
- "2019"
_4images_image_id: "39509"
_4images_cat_id: "2959"
_4images_user_id: "1126"
_4images_image_date: "2014-10-03T16:42:19"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39509 -->
