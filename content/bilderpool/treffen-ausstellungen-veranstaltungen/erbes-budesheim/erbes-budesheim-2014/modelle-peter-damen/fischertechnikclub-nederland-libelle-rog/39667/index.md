---
layout: "image"
title: "Details"
date: "2014-10-05T15:26:33"
picture: "fischertechnikclubnederlandlibellepijlstaartrog4.jpg"
weight: "4"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Bert Rook + Rob van Baal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/39667
imported:
- "2019"
_4images_image_id: "39667"
_4images_cat_id: "2973"
_4images_user_id: "22"
_4images_image_date: "2014-10-05T15:26:33"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39667 -->
Met de vleugel-uitslag-amplitude kan de stuwkracht worden geregeld. 

Het verdraaien van de vleugelhoek bepaalt de stuwrichting. De vleugelhoek-verstelling per vleugel vindt plaats door XS-motoren via een RoboPro-programma afhankelijk of de Libelle naar boven of beneden vliegt. 

Youtube-links : 
https://www.youtube.com/watch?v=OvUo6Us2geo&list=UUvBlHQzqD-ISw8MaTccrfOQ&index=1 = Libelle Fischertechnik 

https://www.youtube.com/watch?v=m1UJYAVVplU&list=UUvBlHQzqD-ISw8MaTccrfOQ&index=2 = Pijlstaartrog Fischertechnik 
