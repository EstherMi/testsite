---
layout: "image"
title: "immer interessante + schöne Modellen (Dirk Fox)"
date: "2014-10-04T12:38:27"
picture: "fischertechniktreffenerbesbudesheim21.jpg"
weight: "21"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/39589
imported:
- "2019"
_4images_image_id: "39589"
_4images_cat_id: "2965"
_4images_user_id: "22"
_4images_image_date: "2014-10-04T12:38:27"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39589 -->
