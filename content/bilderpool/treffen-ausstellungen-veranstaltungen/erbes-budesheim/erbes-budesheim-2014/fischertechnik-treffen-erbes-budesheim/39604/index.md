---
layout: "image"
title: "immer interessante + schöne Modelle   (Michael Sengstschmied)"
date: "2014-10-04T12:38:27"
picture: "fischertechniktreffenerbesbudesheim36.jpg"
weight: "36"
konstrukteure: 
- "Michael Sengstschied"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/39604
imported:
- "2019"
_4images_image_id: "39604"
_4images_cat_id: "2965"
_4images_user_id: "22"
_4images_image_date: "2014-10-04T12:38:27"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39604 -->
