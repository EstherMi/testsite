---
layout: "image"
title: "Fischertechnik-Treffen-Erbes-Budesheim-2014"
date: "2014-10-04T12:38:27"
picture: "fischertechniktreffenerbesbudesheim30.jpg"
weight: "30"
konstrukteure: 
- "Sarah und Martin Hutter"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/39598
imported:
- "2019"
_4images_image_id: "39598"
_4images_cat_id: "2965"
_4images_user_id: "22"
_4images_image_date: "2014-10-04T12:38:27"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39598 -->
