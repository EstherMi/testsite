---
layout: "image"
title: "immer interessante + schöne Modelle   (Michael  -MickyW)"
date: "2014-10-04T12:38:27"
picture: "fischertechniktreffenerbesbudesheim38.jpg"
weight: "38"
konstrukteure: 
- "Micheal W"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/39606
imported:
- "2019"
_4images_image_id: "39606"
_4images_cat_id: "2965"
_4images_user_id: "22"
_4images_image_date: "2014-10-04T12:38:27"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39606 -->
