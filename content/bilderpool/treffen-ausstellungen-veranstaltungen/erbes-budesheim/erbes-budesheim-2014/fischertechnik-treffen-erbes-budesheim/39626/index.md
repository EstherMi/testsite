---
layout: "image"
title: "Fischertechnik-Treffen-Erbes-Budesheim-2014"
date: "2014-10-04T12:38:28"
picture: "fischertechniktreffenerbesbudesheim58.jpg"
weight: "58"
konstrukteure: 
- "Jens Lemkamp (LemkJen)"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/39626
imported:
- "2019"
_4images_image_id: "39626"
_4images_cat_id: "2965"
_4images_user_id: "22"
_4images_image_date: "2014-10-04T12:38:28"
_4images_image_order: "58"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39626 -->
