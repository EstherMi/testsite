---
layout: "image"
title: "immer interessante + schöne Modellen (Thomas Püttmann)"
date: "2014-10-04T12:38:27"
picture: "fischertechniktreffenerbesbudesheim28.jpg"
weight: "28"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/39596
imported:
- "2019"
_4images_image_id: "39596"
_4images_cat_id: "2965"
_4images_user_id: "22"
_4images_image_date: "2014-10-04T12:38:27"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39596 -->
