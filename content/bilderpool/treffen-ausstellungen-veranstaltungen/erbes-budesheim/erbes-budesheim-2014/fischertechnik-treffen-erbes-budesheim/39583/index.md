---
layout: "image"
title: "After-Fischertechnik-Treffen"
date: "2014-10-04T12:38:27"
picture: "fischertechniktreffenerbesbudesheim15.jpg"
weight: "15"
konstrukteure: 
- "Firma Knobloch"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/39583
imported:
- "2019"
_4images_image_id: "39583"
_4images_cat_id: "2965"
_4images_user_id: "22"
_4images_image_date: "2014-10-04T12:38:27"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39583 -->
Vielen Dank an Ralf und sein tolles Team für die großartige Organisation und Durchführung.
Dem gesamten Knobloch-Team sei für die Unglaubliche Arbeit von der Vorbereitung bis zum Abendessen herzlich gedankt ! 

Ohne die gute Vorbereitung hatte die "Pijlstaart-rog" + die Libelle nicht fliegen können... oder doch...
Die Seilbruch der Libelle verursachte Freitag Abent bim Installieren eine 0,5m Freifall... glücklich um 23.00u funktionierte Alles wieder.
