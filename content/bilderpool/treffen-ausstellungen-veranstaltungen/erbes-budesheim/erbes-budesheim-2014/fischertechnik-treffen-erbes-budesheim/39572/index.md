---
layout: "image"
title: "Fischertechnik-Treffen-Erbes-Budesheim-2014"
date: "2014-10-04T12:38:27"
picture: "fischertechniktreffenerbesbudesheim04.jpg"
weight: "4"
konstrukteure: 
- "Michael Stratmann"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/39572
imported:
- "2019"
_4images_image_id: "39572"
_4images_cat_id: "2965"
_4images_user_id: "22"
_4images_image_date: "2014-10-04T12:38:27"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39572 -->
....Besucher aus Saudi Arabia