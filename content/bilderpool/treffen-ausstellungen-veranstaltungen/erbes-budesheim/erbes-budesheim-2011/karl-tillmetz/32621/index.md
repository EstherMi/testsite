---
layout: "image"
title: "Kirmesmodell"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim094.jpg"
weight: "4"
konstrukteure: 
- "Karl Tillmetz"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32621
imported:
- "2019"
_4images_image_id: "32621"
_4images_cat_id: "2411"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "94"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32621 -->
