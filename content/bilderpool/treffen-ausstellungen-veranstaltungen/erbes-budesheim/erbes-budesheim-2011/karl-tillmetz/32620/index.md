---
layout: "image"
title: "Kirmesmodelle"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim093.jpg"
weight: "3"
konstrukteure: 
- "Karl Tillmetz"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32620
imported:
- "2019"
_4images_image_id: "32620"
_4images_cat_id: "2411"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "93"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32620 -->
Beim linken durfte man selber über mehrere selbstgebaute Joysticks verschiedene Motoren ansteuern. Dabei wurden die linken und rechten Rotationsträger unterschiedlich gedreht. Der hier obere Teil des hier rechten Arms kann einklappen, und der Träger mit den Leuten drauf steht dann total schräg. Ich möchte bitte nie in so was drinsitzen :-) Siehe die nächsten beiden Bilder.