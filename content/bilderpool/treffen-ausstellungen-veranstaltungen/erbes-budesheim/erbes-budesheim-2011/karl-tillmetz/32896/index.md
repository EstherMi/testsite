---
layout: "image"
title: "Mega-X-Spin"
date: "2011-09-27T21:30:11"
picture: "MegaXSpin.jpg"
weight: "1"
konstrukteure: 
- "Karl Tillmetz"
fotografen:
- "Jutta Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- details/32896
imported:
- "2019"
_4images_image_id: "32896"
_4images_cat_id: "2411"
_4images_user_id: "1088"
_4images_image_date: "2011-09-27T21:30:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32896 -->
