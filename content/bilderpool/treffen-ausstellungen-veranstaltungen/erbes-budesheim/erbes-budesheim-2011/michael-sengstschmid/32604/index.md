---
layout: "image"
title: "Riesen-Seilbahn"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim077.jpg"
weight: "37"
konstrukteure: 
- "mirose"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32604
imported:
- "2019"
_4images_image_id: "32604"
_4images_cat_id: "2407"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "77"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32604 -->
Detailaufnahme der Gondelaufhängung. Äußerst wirklichkeitsgetreu, kann wohl sogar vom Seil abgehoben werden.