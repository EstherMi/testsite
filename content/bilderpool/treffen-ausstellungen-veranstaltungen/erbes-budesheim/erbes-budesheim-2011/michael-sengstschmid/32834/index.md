---
layout: "image"
title: "Sessellift Michael Sengstschmid"
date: "2011-09-27T20:47:57"
picture: "sesselliftmichaelsengstschmid07.jpg"
weight: "12"
konstrukteure: 
- "Michael Sengstschmid"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/32834
imported:
- "2019"
_4images_image_id: "32834"
_4images_cat_id: "2407"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T20:47:57"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32834 -->
