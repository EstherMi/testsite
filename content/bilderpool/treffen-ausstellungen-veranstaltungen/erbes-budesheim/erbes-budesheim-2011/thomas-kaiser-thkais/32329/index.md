---
layout: "image"
title: "DSC06101"
date: "2011-09-25T20:36:34"
picture: "modelle155.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/32329
imported:
- "2019"
_4images_image_id: "32329"
_4images_cat_id: "2431"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:34"
_4images_image_order: "155"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32329 -->
