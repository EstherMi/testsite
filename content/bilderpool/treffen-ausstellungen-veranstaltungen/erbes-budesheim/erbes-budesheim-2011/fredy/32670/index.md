---
layout: "image"
title: "Fredys Industriemodell mit Miniatur-Hochregallager"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim143.jpg"
weight: "1"
konstrukteure: 
- "Frederik (Fredy)"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32670
imported:
- "2019"
_4images_image_id: "32670"
_4images_cat_id: "2388"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "143"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32670 -->
