---
layout: "image"
title: "Kopiergerät"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim146.jpg"
weight: "4"
konstrukteure: 
- "Frederik (Fredy)"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32673
imported:
- "2019"
_4images_image_id: "32673"
_4images_cat_id: "2388"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "146"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32673 -->
Nachbau eines ft-Modells der Rubrik "Aktuelles zum Nachbauen" eines alten ft-Clubheftes: Eine Lichtschranke (im Hintergrund), die die Vorlage abtastet, und der Stift, der die Kopie wiedergibt, werden durch denselben Antrieb synchron hin und her bewegt, und immer an einem Ende gibt es einen kleinen Papiervorschub.