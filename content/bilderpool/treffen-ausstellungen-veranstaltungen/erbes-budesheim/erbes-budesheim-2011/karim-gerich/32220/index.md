---
layout: "image"
title: "DSC05926"
date: "2011-09-25T20:36:33"
picture: "modelle046.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/32220
imported:
- "2019"
_4images_image_id: "32220"
_4images_cat_id: "2443"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "46"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32220 -->
