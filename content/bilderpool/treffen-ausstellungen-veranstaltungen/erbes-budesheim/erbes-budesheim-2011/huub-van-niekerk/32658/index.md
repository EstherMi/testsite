---
layout: "image"
title: "Anfänge einer großen Laufmaschine"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim131.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32658
imported:
- "2019"
_4images_image_id: "32658"
_4images_cat_id: "2445"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "131"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32658 -->
