---
layout: "image"
title: "Industriemodell (Auftragsarbeit der Knobloch GmbH)"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim070.jpg"
weight: "6"
konstrukteure: 
- "Knobloch GmbH"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32597
imported:
- "2019"
_4images_image_id: "32597"
_4images_cat_id: "2398"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "70"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32597 -->
