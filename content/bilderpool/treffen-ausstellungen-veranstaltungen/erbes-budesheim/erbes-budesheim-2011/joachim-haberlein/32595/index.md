---
layout: "image"
title: "Industriemodell (Auftragsarbeit der Knobloch GmbH)"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim068.jpg"
weight: "4"
konstrukteure: 
- "Knobloch GmbH"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32595
imported:
- "2019"
_4images_image_id: "32595"
_4images_cat_id: "2398"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "68"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32595 -->
