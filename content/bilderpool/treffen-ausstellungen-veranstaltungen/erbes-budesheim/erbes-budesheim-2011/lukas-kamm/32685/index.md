---
layout: "image"
title: "Erfrischungsgetränk-Maschine"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim158.jpg"
weight: "2"
konstrukteure: 
- "Lukas Kamm"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32685
imported:
- "2019"
_4images_image_id: "32685"
_4images_cat_id: "2413"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "158"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32685 -->
Links die Rührstation, in der Mitte die Pulverzuführung, rechts wird per Druckluft Flüssigkeit aus der Flasche ins Glas gepresst (Abschaltung per Ultraschall-Entfernungssensor). Das Glas wird auf der Schiene entlang gefahren.