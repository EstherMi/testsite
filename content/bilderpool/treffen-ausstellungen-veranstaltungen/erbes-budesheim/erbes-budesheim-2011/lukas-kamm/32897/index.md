---
layout: "image"
title: "Eisteemaschine"
date: "2011-09-27T21:33:41"
picture: "Eisteemaschine.jpg"
weight: "1"
konstrukteure: 
- "Lukas Kamm"
fotografen:
- "Jutta Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- details/32897
imported:
- "2019"
_4images_image_id: "32897"
_4images_cat_id: "2413"
_4images_user_id: "1088"
_4images_image_date: "2011-09-27T21:33:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32897 -->
