---
layout: "image"
title: "DSC06059"
date: "2011-09-25T20:36:34"
picture: "modelle135.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/32309
imported:
- "2019"
_4images_image_id: "32309"
_4images_cat_id: "2413"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:34"
_4images_image_order: "135"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32309 -->
