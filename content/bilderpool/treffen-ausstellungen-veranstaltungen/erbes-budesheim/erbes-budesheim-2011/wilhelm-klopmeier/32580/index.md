---
layout: "image"
title: "Herrn Klopmeiers zyklisch variable Getriebe"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim053.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32580
imported:
- "2019"
_4images_image_id: "32580"
_4images_cat_id: "2418"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "53"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32580 -->
Und Anwendungsbeispiele dieser patentierten Raffinesse mit ganz unterschiedlichen Charakteristiken. Von links nach rechts:

1. Besonders effizienter Antrieb eines Fahrrads

2. Ölförderpumpe, die beim Hochziehen (pumpen) lange Zeit mit optimaler (maximal für die Quelle machbarer) Geschwindigkeit läuft und dann sehr schnell wieder heruntergeht, um die Förderleistung zu optimieren

3. Einfacher Aufbau mit variabler Drehgeschwindigkeit zwischen 0 (Stillstand) und dem doppelten der Eingangswelle

4. Effiziente Presse: Schnell gepresst, nur kurz unten (damit die Presse selbst nicht so viel Wärme vom Werkstück abnimmt und dadurch leidet) und lange Zeit praktisch stillstehend oben (um das Wechseln des Werkstücks zu ermöglichen) - das erlaubt höhere Taktzahlen als beim klassischen Exzenterantrieb.

5. Komplexeres Getriebe mit zwei Arbeitsgängen je Eingangswellenumdrehung. Es sind sogar Arbeitsgänge mit zeitweilig negativer Drehzahl (rückwärts) einstellbar.