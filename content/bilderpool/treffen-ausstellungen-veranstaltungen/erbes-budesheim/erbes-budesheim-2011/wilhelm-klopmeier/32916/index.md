---
layout: "image"
title: "Getriebe mit zyklisch variables Übersetzungsverhältnis"
date: "2011-09-27T21:57:07"
picture: "zyklischvariablesgetriebe1.jpg"
weight: "1"
konstrukteure: 
- "Wilhelm Klopmeijer"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/32916
imported:
- "2019"
_4images_image_id: "32916"
_4images_cat_id: "2418"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T21:57:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32916 -->
Vorteil des von Wilhelm Klopmeier entwickelten Getriebes ist es, dass aus einem gleichförmigen Antrieb ein ungleichförmiger Abtrieb erzeugt werden kann. Was so kompliziert klingt, lässt sich ganz einfach anwenden. So zum Beispiel in Fahrrädern: 
 
Die Krafteinleitung eines Pedalantriebes kann dadurch biomechanisch verbessert werden. So wird eine gleichmäßigere Trittgeschwindigkeit und eine Verbesserung der Krafteinleitung erreicht. Für das Patent hat Klopmeier schon Kontakte zu Herstellern geknüpft.
