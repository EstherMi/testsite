---
layout: "image"
title: "Getriebe mit zyklisch variables Übersetzungsverhältnis"
date: "2011-09-27T21:57:07"
picture: "zyklischvariablesgetriebe6.jpg"
weight: "6"
konstrukteure: 
- "Wilhelm Klopmeijer"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/32921
imported:
- "2019"
_4images_image_id: "32921"
_4images_cat_id: "2418"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T21:57:07"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32921 -->
Einsatz des variablen Getriebes bei der Ölpumpe

Mit dem Einsatz des variablen Getriebes bei der Ölpumpe kann entweder die aktuelle Förderkapazität aus der Quelle erhöht werden oder eine schwächer werdende Quelle über einen längeren Zeitraum genutzt werden. Die gleichmäßige Hubgeschwindigkeit ermöglicht das Füllen des Pumpenraumes - die Pumpe darf niemals "Vakuum" saugen. Die genannten 40% sind ein theoretischer Wert, bei dem ein erschwertes Nachfließen wegen der durchschnittlich größeren Förderleistung nicht berücksichtigt ist. Die beiliegende Kurzbeschreibung erklärt das Prinzip. In Kuwait hatte ich ausführliche Diskussionen zum Sachverhalt - letztlich haben alle das Prinzip bestätigt. 
Eine Verbesserung des hydraulischen Wirkungsgrades war nicht das Ziel der Überlegungen. Aber als angenehmer Nebeneffekt muß sich auch hier wegen Verkleinerung der Spitzengeschwindigkeiten eine Verbesserung ergeben (zähflüssiges Öl und Bohrungstiefen bis ca. 1000m). Ich habe aber bisher nicht überschlagen, um welche Größenordnung es sich hier handeln mag.

MfG
Klopmeier   ( 7-12-2007 )
