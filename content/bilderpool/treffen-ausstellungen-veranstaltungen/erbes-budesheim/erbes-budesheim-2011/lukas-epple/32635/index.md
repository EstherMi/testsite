---
layout: "image"
title: "Schiffschaukel"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim108.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32635
imported:
- "2019"
_4images_image_id: "32635"
_4images_cat_id: "2428"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "108"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32635 -->
