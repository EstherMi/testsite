---
layout: "image"
title: "Dragster"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim141.jpg"
weight: "3"
konstrukteure: 
- "MisterWho"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32668
imported:
- "2019"
_4images_image_id: "32668"
_4images_cat_id: "2389"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "141"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32668 -->
