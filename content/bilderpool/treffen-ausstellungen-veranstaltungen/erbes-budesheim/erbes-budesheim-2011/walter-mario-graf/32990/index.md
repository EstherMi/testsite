---
layout: "image"
title: "Eisenbahn6222"
date: "2011-09-30T16:31:21"
picture: "IMG_6222.JPG"
weight: "4"
konstrukteure: 
- "Volker-Mario Graf"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/32990
imported:
- "2019"
_4images_image_id: "32990"
_4images_cat_id: "2405"
_4images_user_id: "4"
_4images_image_date: "2011-09-30T16:31:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32990 -->
