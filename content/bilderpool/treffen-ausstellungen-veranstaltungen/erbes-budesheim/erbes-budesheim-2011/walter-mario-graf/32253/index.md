---
layout: "image"
title: "DSC05966"
date: "2011-09-25T20:36:33"
picture: "modelle079.jpg"
weight: "21"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/32253
imported:
- "2019"
_4images_image_id: "32253"
_4images_cat_id: "2405"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "79"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32253 -->
