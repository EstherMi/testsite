---
layout: "image"
title: "Eisenbahn_6224.JPG"
date: "2011-09-30T16:34:59"
picture: "IMG_6224.JPG"
weight: "6"
konstrukteure: 
- "Volker-Mario Graf"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/32992
imported:
- "2019"
_4images_image_id: "32992"
_4images_cat_id: "2405"
_4images_user_id: "4"
_4images_image_date: "2011-09-30T16:34:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32992 -->
