---
layout: "image"
title: "Eisenbahn_6228"
date: "2011-09-30T16:36:38"
picture: "IMG_6228.JPG"
weight: "10"
konstrukteure: 
- "Volker-Mario Graf"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/32996
imported:
- "2019"
_4images_image_id: "32996"
_4images_cat_id: "2405"
_4images_user_id: "4"
_4images_image_date: "2011-09-30T16:36:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32996 -->
