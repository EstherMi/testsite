---
layout: "image"
title: "Clubmodell 'Adler'"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim084.jpg"
weight: "6"
konstrukteure: 
- "quincym"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32611
imported:
- "2019"
_4images_image_id: "32611"
_4images_cat_id: "2427"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "84"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32611 -->
