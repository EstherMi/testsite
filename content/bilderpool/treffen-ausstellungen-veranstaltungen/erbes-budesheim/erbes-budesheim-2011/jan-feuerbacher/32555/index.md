---
layout: "image"
title: "Kirmesmodell"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim028.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32555
imported:
- "2019"
_4images_image_id: "32555"
_4images_cat_id: "2442"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32555 -->
Die drei blauen Sitze rotieren schnell, das ganze rotiert nochmal, und es wird über eine sehr interessante Flaschenzugmechanik als Ganzes gehoben und gesenkt: Die gelben waagerechten Streben halten und führen einen inneren U-Träger, der von den Flaschenzügen rechts hochgezogen wird.