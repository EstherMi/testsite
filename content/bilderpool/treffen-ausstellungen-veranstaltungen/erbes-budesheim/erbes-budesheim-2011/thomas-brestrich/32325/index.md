---
layout: "image"
title: "DSC06081"
date: "2011-09-25T20:36:34"
picture: "modelle151.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/32325
imported:
- "2019"
_4images_image_id: "32325"
_4images_cat_id: "2423"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:34"
_4images_image_order: "151"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32325 -->
