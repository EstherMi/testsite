---
layout: "image"
title: "DSC06080"
date: "2011-09-25T20:36:34"
picture: "modelle150.jpg"
weight: "16"
konstrukteure: 
- "schnaggels"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/32324
imported:
- "2019"
_4images_image_id: "32324"
_4images_cat_id: "2423"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:34"
_4images_image_order: "150"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32324 -->
