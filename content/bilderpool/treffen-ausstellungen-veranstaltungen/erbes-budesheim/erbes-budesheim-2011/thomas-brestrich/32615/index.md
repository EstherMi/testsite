---
layout: "image"
title: "Nachbau des Micro-Trucks von thomas004"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim088.jpg"
weight: "11"
konstrukteure: 
- "thomas004/Thomas Kaiser"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32615
imported:
- "2019"
_4images_image_id: "32615"
_4images_cat_id: "2423"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "88"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32615 -->
