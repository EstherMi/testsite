---
layout: "image"
title: "Pneumatisches Hexapod von Thomas Bestrich (Schnaggels)"
date: "2011-09-26T17:47:41"
picture: "dm008.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/32701
imported:
- "2019"
_4images_image_id: "32701"
_4images_cat_id: "2423"
_4images_user_id: "374"
_4images_image_date: "2011-09-26T17:47:41"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32701 -->
