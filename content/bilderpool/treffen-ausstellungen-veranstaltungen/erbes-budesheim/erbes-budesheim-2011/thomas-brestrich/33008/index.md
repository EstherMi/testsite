---
layout: "image"
title: "Löschboot 2"
date: "2011-09-30T18:09:49"
picture: "Lschboot_2.jpg"
weight: "3"
konstrukteure: 
- "Thomas Brestrich"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/33008
imported:
- "2019"
_4images_image_id: "33008"
_4images_cat_id: "2423"
_4images_user_id: "1126"
_4images_image_date: "2011-09-30T18:09:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33008 -->
