---
layout: "image"
title: "DSC06145"
date: "2011-09-25T20:36:34"
picture: "modelle159.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/32333
imported:
- "2019"
_4images_image_id: "32333"
_4images_cat_id: "2392"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:34"
_4images_image_order: "159"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32333 -->
