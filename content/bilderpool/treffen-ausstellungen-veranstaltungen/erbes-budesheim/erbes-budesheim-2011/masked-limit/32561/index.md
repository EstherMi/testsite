---
layout: "image"
title: "ft:c-Poster"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim034.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32561
imported:
- "2019"
_4images_image_id: "32561"
_4images_cat_id: "2392"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32561 -->
Links cpuetter, rechts Limit, der Schöpfer des Plakats: Tausende Modelle ganz klein zu einem Mosaik zusammengesetzt. Die Plakate können noch bestellt werden; siehe Forum!