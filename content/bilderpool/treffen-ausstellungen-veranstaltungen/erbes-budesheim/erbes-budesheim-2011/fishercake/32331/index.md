---
layout: "image"
title: "DSC06140"
date: "2011-09-25T20:36:34"
picture: "modelle157.jpg"
weight: "7"
konstrukteure: 
- "Irgent jemand"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/32331
imported:
- "2019"
_4images_image_id: "32331"
_4images_cat_id: "2446"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:34"
_4images_image_order: "157"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32331 -->
