---
layout: "image"
title: "Riesenrad"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim157.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32684
imported:
- "2019"
_4images_image_id: "32684"
_4images_cat_id: "2424"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "157"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32684 -->
Leider erst beim Abbau fotografiert.