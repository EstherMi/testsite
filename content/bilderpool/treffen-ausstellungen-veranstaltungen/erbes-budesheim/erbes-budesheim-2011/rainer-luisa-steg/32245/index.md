---
layout: "image"
title: "DSC05955"
date: "2011-09-25T20:36:33"
picture: "modelle071.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/32245
imported:
- "2019"
_4images_image_id: "32245"
_4images_cat_id: "2424"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "71"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32245 -->
