---
layout: "image"
title: "Firestorm Megacoaster 2 (36)"
date: "2011-09-25T17:42:28"
picture: "modelle35.jpg"
weight: "103"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/32159
imported:
- "2019"
_4images_image_id: "32159"
_4images_cat_id: "2382"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T17:42:28"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32159 -->
