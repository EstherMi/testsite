---
layout: "image"
title: "Firestorm Megacoaster 2 (23)"
date: "2011-09-25T17:42:28"
picture: "modelle22.jpg"
weight: "82"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/32146
imported:
- "2019"
_4images_image_id: "32146"
_4images_cat_id: "2382"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T17:42:28"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32146 -->
