---
layout: "image"
title: "cknoblochfirestormmegacoaster22.jpg"
date: "2011-09-25T07:45:30"
picture: "cknoblochfirestormmegacoaster22.jpg"
weight: "81"
konstrukteure: 
- "C- Knobloch Firestorm Megacoaster"
fotografen:
- "tobs9578"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- details/32109
imported:
- "2019"
_4images_image_id: "32109"
_4images_cat_id: "2382"
_4images_user_id: "1007"
_4images_image_date: "2011-09-25T07:45:30"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32109 -->
