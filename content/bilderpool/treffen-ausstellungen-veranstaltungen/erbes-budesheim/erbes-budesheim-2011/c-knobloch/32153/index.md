---
layout: "image"
title: "Firestorm Megacoaster 2 (30)"
date: "2011-09-25T17:42:28"
picture: "modelle29.jpg"
weight: "96"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/32153
imported:
- "2019"
_4images_image_id: "32153"
_4images_cat_id: "2382"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T17:42:28"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32153 -->
