---
layout: "image"
title: "cknoblochfirestormmegacoaster07.jpg"
date: "2011-09-25T07:45:30"
picture: "cknoblochfirestormmegacoaster07.jpg"
weight: "44"
konstrukteure: 
- "C- Knobloch Firestorm Megacoaster"
fotografen:
- "tobs9578"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- details/32094
imported:
- "2019"
_4images_image_id: "32094"
_4images_cat_id: "2382"
_4images_user_id: "1007"
_4images_image_date: "2011-09-25T07:45:30"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32094 -->
