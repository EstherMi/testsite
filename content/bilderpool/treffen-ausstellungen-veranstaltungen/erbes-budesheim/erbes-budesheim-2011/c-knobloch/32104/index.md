---
layout: "image"
title: "cknoblochfirestormmegacoaster17.jpg"
date: "2011-09-25T07:45:30"
picture: "cknoblochfirestormmegacoaster17.jpg"
weight: "71"
konstrukteure: 
- "C- Knobloch Firestorm Megacoaster"
fotografen:
- "tobs9578"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- details/32104
imported:
- "2019"
_4images_image_id: "32104"
_4images_cat_id: "2382"
_4images_user_id: "1007"
_4images_image_date: "2011-09-25T07:45:30"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32104 -->
