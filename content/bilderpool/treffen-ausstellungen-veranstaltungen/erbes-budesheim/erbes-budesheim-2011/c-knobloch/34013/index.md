---
layout: "image"
title: "ft Convention EB 2011"
date: "2012-01-24T19:04:09"
picture: "conventon05.jpg"
weight: "2"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/34013
imported:
- "2019"
_4images_image_id: "34013"
_4images_cat_id: "2382"
_4images_user_id: "130"
_4images_image_date: "2012-01-24T19:04:09"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34013 -->
Der Fahrgast freut sich über die rasante Fahrt im ft Mega Coaster.