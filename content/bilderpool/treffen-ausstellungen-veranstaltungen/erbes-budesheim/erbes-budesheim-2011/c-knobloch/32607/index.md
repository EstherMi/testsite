---
layout: "image"
title: "firestorm 2 Achterbahn"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim080.jpg"
weight: "17"
konstrukteure: 
- "Ralf Knoblochs Nachwuchs"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32607
imported:
- "2019"
_4images_image_id: "32607"
_4images_cat_id: "2382"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "80"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32607 -->
