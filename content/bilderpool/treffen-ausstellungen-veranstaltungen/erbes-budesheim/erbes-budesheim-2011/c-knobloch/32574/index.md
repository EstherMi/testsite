---
layout: "image"
title: "firestorm 2 Achterbahn"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim047.jpg"
weight: "10"
konstrukteure: 
- "Ralf Knoblochs Nachwuchs"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32574
imported:
- "2019"
_4images_image_id: "32574"
_4images_cat_id: "2382"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "47"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32574 -->
Die Schienen sind jetzt nicht mehr die roten ft-Schienen, sondern zwei Stränge wie in echt, von selbstgemachten Spezialteilen mit den ft-Trägern verbunden.