---
layout: "image"
title: "Brückenlegepanzer 'Biber'"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim136.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32663
imported:
- "2019"
_4images_image_id: "32663"
_4images_cat_id: "2396"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "136"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32663 -->
Mit ordentlich Butter bei die Fische. Wenn schon, denn schon. :-)