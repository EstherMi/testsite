---
layout: "image"
title: "FT-Brückenlegepanzer-Biber"
date: "2011-09-27T21:29:54"
picture: "brueckenlegepanzerbiber04.jpg"
weight: "4"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/32878
imported:
- "2019"
_4images_image_id: "32878"
_4images_cat_id: "2412"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T21:29:54"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32878 -->
