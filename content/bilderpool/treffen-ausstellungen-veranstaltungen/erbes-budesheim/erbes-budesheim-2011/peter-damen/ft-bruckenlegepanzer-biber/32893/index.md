---
layout: "image"
title: "FT-Brückenlegepanzer-Biber"
date: "2011-09-27T21:30:04"
picture: "brueckenlegepanzerbiber19.jpg"
weight: "19"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/32893
imported:
- "2019"
_4images_image_id: "32893"
_4images_cat_id: "2412"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T21:30:04"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32893 -->
