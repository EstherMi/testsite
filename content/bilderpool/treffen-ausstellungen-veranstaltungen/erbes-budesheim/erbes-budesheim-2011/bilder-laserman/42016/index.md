---
layout: "image"
title: "Kugeluhr"
date: "2015-10-01T13:37:10"
picture: "Kugeluhr.jpg"
weight: "1"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/42016
imported:
- "2019"
_4images_image_id: "42016"
_4images_cat_id: "3122"
_4images_user_id: "724"
_4images_image_date: "2015-10-01T13:37:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42016 -->
