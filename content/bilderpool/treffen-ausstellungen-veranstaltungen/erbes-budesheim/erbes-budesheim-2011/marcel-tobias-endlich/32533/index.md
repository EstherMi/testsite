---
layout: "image"
title: "conventionerbesbuedesheim006.jpg"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim006.jpg"
weight: "4"
konstrukteure: 
- "Marcel Endlich"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32533
imported:
- "2019"
_4images_image_id: "32533"
_4images_cat_id: "2383"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32533 -->
