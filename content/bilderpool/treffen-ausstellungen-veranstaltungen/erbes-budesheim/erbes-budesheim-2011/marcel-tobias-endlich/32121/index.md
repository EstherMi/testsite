---
layout: "image"
title: "Tisch von Tobias"
date: "2011-09-25T15:25:21"
picture: "modelle4.jpg"
weight: "18"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/32121
imported:
- "2019"
_4images_image_id: "32121"
_4images_cat_id: "2383"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T15:25:21"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32121 -->
Truck mit Tieflader