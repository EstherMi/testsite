---
layout: "image"
title: "DSC06086"
date: "2011-09-25T20:36:34"
picture: "modelle152.jpg"
weight: "22"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/32326
imported:
- "2019"
_4images_image_id: "32326"
_4images_cat_id: "2381"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:34"
_4images_image_order: "152"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32326 -->
