---
layout: "image"
title: "Bau- und Spiel-Ecke im Nebenraum"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim056.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32583
imported:
- "2019"
_4images_image_id: "32583"
_4images_cat_id: "2381"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "56"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32583 -->
Sogar dieser Geräteraum wurde freigeräumt, um genug Platz für alle zu bekommen!