---
layout: "image"
title: "Pneumacube"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim153.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32680
imported:
- "2019"
_4images_image_id: "32680"
_4images_cat_id: "2381"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "153"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32680 -->
Dieser Würfel kann sich pneumatisch auseinander- und wieder zusammenfalten.