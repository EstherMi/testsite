---
layout: "image"
title: "ft-Ausstellungsmodell"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim092.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32619
imported:
- "2019"
_4images_image_id: "32619"
_4images_cat_id: "2381"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "92"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32619 -->
