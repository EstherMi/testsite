---
layout: "image"
title: "DSC05858"
date: "2011-09-25T20:36:33"
picture: "modelle012.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/32186
imported:
- "2019"
_4images_image_id: "32186"
_4images_cat_id: "2416"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32186 -->
