---
layout: "image"
title: "DSC06078"
date: "2011-09-25T20:36:34"
picture: "modelle148.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/32322
imported:
- "2019"
_4images_image_id: "32322"
_4images_cat_id: "2425"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:34"
_4images_image_order: "148"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32322 -->
