---
layout: "image"
title: "DSC05851"
date: "2011-09-25T20:36:33"
picture: "modelle007.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/32181
imported:
- "2019"
_4images_image_id: "32181"
_4images_cat_id: "2425"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32181 -->
