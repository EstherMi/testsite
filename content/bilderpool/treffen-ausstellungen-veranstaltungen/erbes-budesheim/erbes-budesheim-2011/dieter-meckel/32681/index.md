---
layout: "image"
title: "Kniegelenk-Presse (hobby-2) und Ölförderpumpe"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim154.jpg"
weight: "5"
konstrukteure: 
- "Dinomania01 (Dieter Meckel)"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32681
imported:
- "2019"
_4images_image_id: "32681"
_4images_cat_id: "2387"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "154"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32681 -->
