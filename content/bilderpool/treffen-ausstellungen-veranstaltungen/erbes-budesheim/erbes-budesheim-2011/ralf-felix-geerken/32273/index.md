---
layout: "image"
title: "DSC06010"
date: "2011-09-25T20:36:33"
picture: "modelle099.jpg"
weight: "23"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/32273
imported:
- "2019"
_4images_image_id: "32273"
_4images_cat_id: "2399"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "99"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32273 -->
