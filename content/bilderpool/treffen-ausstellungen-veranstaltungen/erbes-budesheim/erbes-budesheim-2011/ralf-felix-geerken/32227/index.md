---
layout: "image"
title: "DSC05936"
date: "2011-09-25T20:36:33"
picture: "modelle053.jpg"
weight: "19"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/32227
imported:
- "2019"
_4images_image_id: "32227"
_4images_cat_id: "2399"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "53"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32227 -->
