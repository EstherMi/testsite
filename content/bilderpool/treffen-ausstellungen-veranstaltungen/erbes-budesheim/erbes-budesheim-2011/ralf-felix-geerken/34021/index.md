---
layout: "image"
title: "ft Convention EB 2011"
date: "2012-01-24T19:04:09"
picture: "conventon13.jpg"
weight: "6"
konstrukteure: 
- "Ralf und Felix Geerken"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/34021
imported:
- "2019"
_4images_image_id: "34021"
_4images_cat_id: "2399"
_4images_user_id: "130"
_4images_image_date: "2012-01-24T19:04:09"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34021 -->
