---
layout: "image"
title: "Kirmesmodell"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim002.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32529
imported:
- "2019"
_4images_image_id: "32529"
_4images_cat_id: "2384"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32529 -->
