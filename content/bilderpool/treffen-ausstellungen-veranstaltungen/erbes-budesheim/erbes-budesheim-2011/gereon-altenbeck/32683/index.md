---
layout: "image"
title: "Riesen-Schwebebahn"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim156.jpg"
weight: "26"
konstrukteure: 
- "Alteg"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32683
imported:
- "2019"
_4images_image_id: "32683"
_4images_cat_id: "2401"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "156"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32683 -->
Diese Bahn ist viele Meter lang (Unmengen von Statik...). Hier im Vordergrund kann die Bahn wieder rumgedreht werden, sodass sie immer "vorwärts" fahren kann.