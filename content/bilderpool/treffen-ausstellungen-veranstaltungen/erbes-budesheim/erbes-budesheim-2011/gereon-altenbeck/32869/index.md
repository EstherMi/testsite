---
layout: "image"
title: "Schwebebahn Gereon Altenbeck"
date: "2011-09-27T21:23:10"
picture: "schwebebahngereonaltenbeck14.jpg"
weight: "20"
konstrukteure: 
- "Gereon Altenbeck"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/32869
imported:
- "2019"
_4images_image_id: "32869"
_4images_cat_id: "2401"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T21:23:10"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32869 -->
