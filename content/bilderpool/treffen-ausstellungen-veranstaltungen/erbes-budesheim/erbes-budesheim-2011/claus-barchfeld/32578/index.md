---
layout: "image"
title: "Roboterhand mit 45° gedrehtem Zahnrad (2)"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim051.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32578
imported:
- "2019"
_4images_image_id: "32578"
_4images_cat_id: "2406"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "51"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32578 -->
und in dieser Stellung zeigt der Arm nach unten, die Hand also waagerecht.