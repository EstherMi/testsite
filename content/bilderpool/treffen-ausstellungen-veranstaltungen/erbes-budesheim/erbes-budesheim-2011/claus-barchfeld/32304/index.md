---
layout: "image"
title: "DSC06052"
date: "2011-09-25T20:36:34"
picture: "modelle130.jpg"
weight: "18"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/32304
imported:
- "2019"
_4images_image_id: "32304"
_4images_cat_id: "2406"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:34"
_4images_image_order: "130"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32304 -->
