---
layout: "image"
title: "Robotermodelle + PVC     Claus Barchfeld"
date: "2011-09-27T23:24:31"
picture: "robotermodellepvc2.jpg"
weight: "2"
konstrukteure: 
- "Claus Barchfeld"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/32925
imported:
- "2019"
_4images_image_id: "32925"
_4images_cat_id: "2406"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T23:24:31"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32925 -->
