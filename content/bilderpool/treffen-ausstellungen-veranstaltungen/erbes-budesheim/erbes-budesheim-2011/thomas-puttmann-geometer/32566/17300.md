---
layout: "comment"
hidden: true
title: "17300"
date: "2012-09-28T18:58:23"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
@Bennik

Du hast zwei Dinge übersehen:

a) bin nicht ich der Konstrukteur, sondern habe nur Thomas' Modell mal aufgenommen
b) vor allem aber wird das Planeterium in allen Details ganz wundervoll beschrieben, und zwar in der ft:pedia 4/2011! Siehe http://www.ftcommunity.de/ftpedia :-)))

Gruß,
Stefan