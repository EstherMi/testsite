---
layout: "image"
title: "Geometers Planetarium"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim039.jpg"
weight: "10"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32566
imported:
- "2019"
_4images_image_id: "32566"
_4images_cat_id: "2410"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32566 -->
