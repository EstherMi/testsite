---
layout: "image"
title: "Planetarium"
date: "2011-09-26T17:47:41"
picture: "dm071.jpg"
weight: "17"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/32764
imported:
- "2019"
_4images_image_id: "32764"
_4images_cat_id: "2410"
_4images_user_id: "374"
_4images_image_date: "2011-09-26T17:47:41"
_4images_image_order: "71"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32764 -->
