---
layout: "image"
title: "Geometers Planetarium"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim040.jpg"
weight: "11"
konstrukteure: 
- "Thomas Püttmann"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32567
imported:
- "2019"
_4images_image_id: "32567"
_4images_cat_id: "2410"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32567 -->
