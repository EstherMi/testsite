---
layout: "image"
title: "6-Achsroboter   Severin"
date: "2011-09-27T23:24:31"
picture: "achsroboterseverinboth1.jpg"
weight: "1"
konstrukteure: 
- "Severin"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/32935
imported:
- "2019"
_4images_image_id: "32935"
_4images_cat_id: "2419"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T23:24:31"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32935 -->
