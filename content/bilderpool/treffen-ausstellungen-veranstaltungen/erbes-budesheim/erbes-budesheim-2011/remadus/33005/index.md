---
layout: "image"
title: "Gitterwerk"
date: "2011-09-30T17:27:11"
picture: "IMG_6290.JPG"
weight: "3"
konstrukteure: 
- "Remadus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/33005
imported:
- "2019"
_4images_image_id: "33005"
_4images_cat_id: "2403"
_4images_user_id: "4"
_4images_image_date: "2011-09-30T17:27:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33005 -->
Ein richtiges Kunstwerk. Sogar mit Fettfleck (aber nur auf des Fotografens Linse)