---
layout: "image"
title: "Blick hinab in die Movetura"
date: "2011-09-27T21:23:10"
picture: "Blick_hinab_in_die_Movetura.jpg"
weight: "7"
konstrukteure: 
- "Martin Romann"
fotografen:
- "Jutta Püttmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "geometer"
license: "unknown"
legacy_id:
- details/32872
imported:
- "2019"
_4images_image_id: "32872"
_4images_cat_id: "2403"
_4images_user_id: "1088"
_4images_image_date: "2011-09-27T21:23:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32872 -->
