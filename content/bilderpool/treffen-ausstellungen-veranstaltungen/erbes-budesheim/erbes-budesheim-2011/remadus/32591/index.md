---
layout: "image"
title: "Standuhr"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim064.jpg"
weight: "15"
konstrukteure: 
- "Martin Romann (remadus)"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32591
imported:
- "2019"
_4images_image_id: "32591"
_4images_cat_id: "2403"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "64"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32591 -->
Hoffentlich hat noch jemand bessere Fotos davon. Es ist aber wohl dieselbe Uhr, die im Bilderpool vorgestellt wurde.