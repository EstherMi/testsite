---
layout: "image"
title: "Sumo-Roboter"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim133.jpg"
weight: "4"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32660
imported:
- "2019"
_4images_image_id: "32660"
_4images_cat_id: "2441"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "133"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32660 -->
