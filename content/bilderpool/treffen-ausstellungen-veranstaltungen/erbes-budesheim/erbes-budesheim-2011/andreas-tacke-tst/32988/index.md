---
layout: "image"
title: "Kugeluhr-mod"
date: "2011-09-30T16:15:10"
picture: "IMG_6168.JPG"
weight: "2"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Harald Steinhaus"
keywords: ["modding"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/32988
imported:
- "2019"
_4images_image_id: "32988"
_4images_cat_id: "2395"
_4images_user_id: "4"
_4images_image_date: "2011-09-30T16:15:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32988 -->
