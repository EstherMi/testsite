---
layout: "image"
title: "Kugeluhr"
date: "2011-09-30T16:12:37"
picture: "IMG_6162.JPG"
weight: "1"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Harald Steinhaus"
keywords: ["modding"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/32987
imported:
- "2019"
_4images_image_id: "32987"
_4images_cat_id: "2395"
_4images_user_id: "4"
_4images_image_date: "2011-09-30T16:12:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32987 -->
So ein klein bisschen wurde da auch geschnippelt.