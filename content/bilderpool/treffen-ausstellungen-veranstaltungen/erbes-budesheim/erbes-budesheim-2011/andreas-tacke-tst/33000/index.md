---
layout: "image"
title: "P-Hubgetriebe2"
date: "2011-09-30T16:47:38"
picture: "IMG_6263.JPG"
weight: "4"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Harald Steinhaus"
keywords: ["modding"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/33000
imported:
- "2019"
_4images_image_id: "33000"
_4images_cat_id: "2395"
_4images_user_id: "4"
_4images_image_date: "2011-09-30T16:47:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33000 -->
Das Hubgetriebe legt sich von beiden Seiten in die Nuten des Alu-Trägers. Prototyp von TST, im Kran von Dirk Kutsch.
Die Bohrung im aufrecht stehenden Alu verbuche ich mal als "Modding"...