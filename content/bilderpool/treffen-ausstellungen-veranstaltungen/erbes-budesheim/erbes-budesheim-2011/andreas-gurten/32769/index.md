---
layout: "image"
title: "Pneumatischer Ballgreifer"
date: "2011-09-26T17:47:41"
picture: "dm076.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/32769
imported:
- "2019"
_4images_image_id: "32769"
_4images_cat_id: "2409"
_4images_user_id: "374"
_4images_image_date: "2011-09-26T17:47:41"
_4images_image_order: "76"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32769 -->
