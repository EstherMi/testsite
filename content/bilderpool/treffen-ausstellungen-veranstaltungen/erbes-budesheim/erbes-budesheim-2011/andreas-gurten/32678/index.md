---
layout: "image"
title: "Oval-Zeichner"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim151.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32678
imported:
- "2019"
_4images_image_id: "32678"
_4images_cat_id: "2409"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "151"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32678 -->
