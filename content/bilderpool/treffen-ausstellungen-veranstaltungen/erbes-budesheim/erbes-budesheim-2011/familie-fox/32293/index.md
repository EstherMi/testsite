---
layout: "image"
title: "DSC06039"
date: "2011-09-25T20:36:33"
picture: "modelle119.jpg"
weight: "31"
konstrukteure: 
- "Johann Fox"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/32293
imported:
- "2019"
_4images_image_id: "32293"
_4images_cat_id: "2386"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "119"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32293 -->
