---
layout: "image"
title: "conventionerbesbuedesheim019.jpg"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim019.jpg"
weight: "19"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32546
imported:
- "2019"
_4images_image_id: "32546"
_4images_cat_id: "2386"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32546 -->
