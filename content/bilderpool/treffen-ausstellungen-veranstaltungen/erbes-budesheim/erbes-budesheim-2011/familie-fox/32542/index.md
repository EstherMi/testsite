---
layout: "image"
title: "Hubschrauber"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim015.jpg"
weight: "15"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32542
imported:
- "2019"
_4images_image_id: "32542"
_4images_cat_id: "2386"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32542 -->
Sehr originalgetreu nach dem ersten Hubschrauber nachgebaut, der auch einen Heckrotor hatte, wenn ich mir das richtig gemerkt habe.