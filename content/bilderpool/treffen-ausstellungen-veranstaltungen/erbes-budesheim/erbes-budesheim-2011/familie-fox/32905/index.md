---
layout: "image"
title: "Hubschrauber  Dirk Fox"
date: "2011-09-27T21:51:59"
picture: "hubschrauber01.jpg"
weight: "4"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/32905
imported:
- "2019"
_4images_image_id: "32905"
_4images_cat_id: "2386"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T21:51:59"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32905 -->
