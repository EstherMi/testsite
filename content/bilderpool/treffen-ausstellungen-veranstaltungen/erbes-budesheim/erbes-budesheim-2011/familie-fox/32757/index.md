---
layout: "image"
title: "Miniplotter"
date: "2011-09-26T17:47:41"
picture: "dm064.jpg"
weight: "22"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/32757
imported:
- "2019"
_4images_image_id: "32757"
_4images_cat_id: "2386"
_4images_user_id: "374"
_4images_image_date: "2011-09-26T17:47:41"
_4images_image_order: "64"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32757 -->
