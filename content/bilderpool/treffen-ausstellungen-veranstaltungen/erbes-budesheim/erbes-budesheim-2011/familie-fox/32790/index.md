---
layout: "image"
title: "Hubschrauber"
date: "2011-09-26T17:47:41"
picture: "dm097.jpg"
weight: "28"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/32790
imported:
- "2019"
_4images_image_id: "32790"
_4images_cat_id: "2386"
_4images_user_id: "374"
_4images_image_date: "2011-09-26T17:47:41"
_4images_image_order: "97"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32790 -->
Der Rotor