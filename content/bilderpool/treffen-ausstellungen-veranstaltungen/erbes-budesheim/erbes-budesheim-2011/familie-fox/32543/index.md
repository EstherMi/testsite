---
layout: "image"
title: "Hubschrauber-Rotorantrieb"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim016.jpg"
weight: "16"
konstrukteure: 
- "Johann Fox"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32543
imported:
- "2019"
_4images_image_id: "32543"
_4images_cat_id: "2386"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32543 -->
Die Taumelscheibe wird von den Seilrollen geführt (Details siehe ft:pedia 3/2011).