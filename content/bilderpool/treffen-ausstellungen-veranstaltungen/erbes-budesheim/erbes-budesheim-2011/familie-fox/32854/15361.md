---
layout: "comment"
hidden: true
title: "15361"
date: "2011-10-05T17:46:29"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Hallo Johan + Dirk, 

- Die Pedale dienen zum Heckrotor

- Wie in einem echten Hubschrauber kann nun die Steuerung über einen in X- und Y-Richtung beweglichen Steuerknüppel (Cyclic) erfolgen. 

- Wo im Bild gibt es aber der  Hebel (Collective) zur gleichzeitigen Verstellung der Blattneigung aller drei Rotorblättern durch Verkürzung aller drei Zugseile ???? 
Wir haben in Erbes-Budemheim daruber gesprochen......aber wie das in diesem Modell funktioniert bin ich leider vergessen..........  

Grüss, 

Peter 
Poederoyen NL