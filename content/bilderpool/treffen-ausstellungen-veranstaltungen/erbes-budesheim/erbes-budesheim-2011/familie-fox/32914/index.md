---
layout: "image"
title: "Hubschrauber  Dirk Fox"
date: "2011-09-27T21:52:07"
picture: "hubschrauber10.jpg"
weight: "13"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/32914
imported:
- "2019"
_4images_image_id: "32914"
_4images_cat_id: "2386"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T21:52:07"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32914 -->
