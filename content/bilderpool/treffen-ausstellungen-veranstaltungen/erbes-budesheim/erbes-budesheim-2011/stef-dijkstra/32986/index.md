---
layout: "image"
title: "Hochregelar Stef Dijkstra"
date: "2011-09-28T17:25:57"
picture: "hochregelarstefdijkstra4.jpg"
weight: "4"
konstrukteure: 
- "Stef Dijkstra"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/32986
imported:
- "2019"
_4images_image_id: "32986"
_4images_cat_id: "2422"
_4images_user_id: "22"
_4images_image_date: "2011-09-28T17:25:57"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32986 -->
