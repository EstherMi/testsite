---
layout: "image"
title: "Baukran und Düsenjet"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim054.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32581
imported:
- "2019"
_4images_image_id: "32581"
_4images_cat_id: "2404"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "54"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32581 -->
