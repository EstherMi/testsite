---
layout: "image"
title: "DSC05946"
date: "2011-09-25T20:36:33"
picture: "modelle063.jpg"
weight: "18"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/32237
imported:
- "2019"
_4images_image_id: "32237"
_4images_cat_id: "2390"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "63"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32237 -->
