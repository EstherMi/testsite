---
layout: "image"
title: "DSC05950"
date: "2011-09-25T20:36:33"
picture: "modelle067.jpg"
weight: "22"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/32241
imported:
- "2019"
_4images_image_id: "32241"
_4images_cat_id: "2390"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "67"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32241 -->
