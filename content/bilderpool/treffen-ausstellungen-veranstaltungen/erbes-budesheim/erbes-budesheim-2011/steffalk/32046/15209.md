---
layout: "comment"
hidden: true
title: "15209"
date: "2011-09-26T18:14:33"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Das linke ist die grafische Benutzeroberfläche für den Kran, und ist hochmodern!

Jede Funktion am Kran hat eine Miniaturdarstellung (oder sagen wir mal: ein echtes 3D-Icon :-D ), auf das man nicht klickt, sondern mit dem man ganz im Trend der Zeit eine Handgeste ausführt und voilà - schon bewegt sich das zugehörige Element im Modell. Z.B. stehen die vier Räder ganz hinten links für das Fahrwerk. Also: die Rädchen vor/zurück schieben -- Fahrzeug fährt vor / zurück.

Gruß,
Harald