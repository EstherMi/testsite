---
layout: "image"
title: "Kranwagen"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim024.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32551
imported:
- "2019"
_4images_image_id: "32551"
_4images_cat_id: "2390"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32551 -->
Meine Tochter vertritt mich gerade, erklärt den Kranwagen und führt ihn vor.