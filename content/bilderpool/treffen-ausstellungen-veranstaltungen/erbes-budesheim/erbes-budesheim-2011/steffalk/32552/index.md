---
layout: "image"
title: "Kranwagen"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim025.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32552
imported:
- "2019"
_4images_image_id: "32552"
_4images_cat_id: "2390"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32552 -->
Featureliste: Allradantrieb (auch vorne kräftig), Allrad-Einzelradaufhängung, Allrad-Federung, Lenkung, vier ausfahrbare Stützen (die das Fahrwerk tatsächlich anheben, allerdings nur ohne Kranarm drauf so weit, dass die Räder frei hängen), Drehbarer Aufbau, aufstellbarer Kranarm, dreifach ausfahrbarer Teleskoparm, doppelter Flaschenzug beim Kranhaken; man muss kein (kaum) Seil zugeben, wenn der Arm ausfährt. "Intuitive grafische Benutzeroberfläche" zur Steuerung (das in grauem ft gehaltene Steuerpult hat dem Kran nachempfundene Elemente, damit man gleich sieht, welcher Schieber was steuert).