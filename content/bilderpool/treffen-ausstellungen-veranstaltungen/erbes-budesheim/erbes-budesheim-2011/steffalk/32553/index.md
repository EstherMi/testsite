---
layout: "image"
title: "Kranwagen"
date: "2011-09-26T10:10:20"
picture: "conventionerbesbuedesheim026.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32553
imported:
- "2019"
_4images_image_id: "32553"
_4images_cat_id: "2390"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:20"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32553 -->
