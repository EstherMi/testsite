---
layout: "image"
title: "Autokran  Stefan Falk"
date: "2011-09-27T23:24:31"
picture: "autokran3.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/32934
imported:
- "2019"
_4images_image_id: "32934"
_4images_cat_id: "2390"
_4images_user_id: "22"
_4images_image_date: "2011-09-27T23:24:31"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32934 -->
