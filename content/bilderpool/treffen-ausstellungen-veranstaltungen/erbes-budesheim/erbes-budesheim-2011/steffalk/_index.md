---
layout: "overview"
title: "Steffalk"
date: 2019-12-17T18:28:45+01:00
legacy_id:
- categories/2390
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2390 --> 
Autokran - Dreiachsig mit Einzelradaufhängung,
Allradantrieb, Federung, Lenkung, Stützen, drehbarem,
aufstellbarem und dreifach ausfahrbarem Kranarm