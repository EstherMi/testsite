---
layout: "image"
title: "Autokran von Stefan Falk (stefalk)"
date: "2011-09-26T17:47:41"
picture: "dm077.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/32770
imported:
- "2019"
_4images_image_id: "32770"
_4images_cat_id: "2390"
_4images_user_id: "374"
_4images_image_date: "2011-09-26T17:47:41"
_4images_image_order: "77"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32770 -->
