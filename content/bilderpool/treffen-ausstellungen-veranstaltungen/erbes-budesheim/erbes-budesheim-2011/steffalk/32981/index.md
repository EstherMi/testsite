---
layout: "image"
title: "Noch 2 Kranwagen-Bilder"
date: "2011-09-28T17:25:57"
picture: "nochkranwagenbilder1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "mirnia 24.9.2011"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32981
imported:
- "2019"
_4images_image_id: "32981"
_4images_cat_id: "2390"
_4images_user_id: "104"
_4images_image_date: "2011-09-28T17:25:57"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32981 -->
Ein netter Convention-Besucher hatte mir freundlicherweise seine Bilder gemailt - Danke!