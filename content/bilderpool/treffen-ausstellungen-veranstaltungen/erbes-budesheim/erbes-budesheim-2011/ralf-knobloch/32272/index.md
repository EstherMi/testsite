---
layout: "image"
title: "DSC06007"
date: "2011-09-25T20:36:33"
picture: "modelle098.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/32272
imported:
- "2019"
_4images_image_id: "32272"
_4images_cat_id: "2432"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "98"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32272 -->
