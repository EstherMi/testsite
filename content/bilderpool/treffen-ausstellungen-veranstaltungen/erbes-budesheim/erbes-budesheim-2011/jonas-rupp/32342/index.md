---
layout: "image"
title: "DSC06160"
date: "2011-09-25T20:36:34"
picture: "modelle168.jpg"
weight: "42"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/32342
imported:
- "2019"
_4images_image_id: "32342"
_4images_cat_id: "2400"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:34"
_4images_image_order: "168"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32342 -->
