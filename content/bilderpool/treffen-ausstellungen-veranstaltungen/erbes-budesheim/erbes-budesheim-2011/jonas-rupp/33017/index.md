---
layout: "image"
title: "Paketzentrum mit Rundregallager"
date: "2011-09-30T19:49:20"
picture: "paketzentrummitrundregallager09.jpg"
weight: "9"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "Stefan/Jonas Rupp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jorobo"
license: "unknown"
legacy_id:
- details/33017
imported:
- "2019"
_4images_image_id: "33017"
_4images_cat_id: "2400"
_4images_user_id: "1030"
_4images_image_date: "2011-09-30T19:49:20"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33017 -->
Modell beim Aufbau