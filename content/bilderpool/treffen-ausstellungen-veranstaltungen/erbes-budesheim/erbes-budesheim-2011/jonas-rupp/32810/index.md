---
layout: "image"
title: "Hochregal"
date: "2011-09-26T17:47:41"
picture: "dm117.jpg"
weight: "19"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/32810
imported:
- "2019"
_4images_image_id: "32810"
_4images_cat_id: "2400"
_4images_user_id: "374"
_4images_image_date: "2011-09-26T17:47:41"
_4images_image_order: "117"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32810 -->
Anlage zum einlagern von Paletten in das Hochregallager