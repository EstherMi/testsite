---
layout: "image"
title: "DSC05916"
date: "2011-09-25T20:36:33"
picture: "modelle042.jpg"
weight: "26"
konstrukteure: 
- "Jonas Rupp (jorobo)"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/32216
imported:
- "2019"
_4images_image_id: "32216"
_4images_cat_id: "2400"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32216 -->
