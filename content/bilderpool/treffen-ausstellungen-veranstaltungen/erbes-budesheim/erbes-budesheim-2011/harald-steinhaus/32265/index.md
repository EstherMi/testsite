---
layout: "image"
title: "DSC05994"
date: "2011-09-25T20:36:33"
picture: "modelle091.jpg"
weight: "27"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/32265
imported:
- "2019"
_4images_image_id: "32265"
_4images_cat_id: "2393"
_4images_user_id: "1162"
_4images_image_date: "2011-09-25T20:36:33"
_4images_image_order: "91"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32265 -->
