---
layout: "image"
title: "Kettenkarussell"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim121.jpg"
weight: "14"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32648
imported:
- "2019"
_4images_image_id: "32648"
_4images_cat_id: "2393"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "121"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32648 -->
Per Seilzug konnte das Karussell am Träger in der Mitte hochgezogen werden. Durch das Bogenstück oben ändert sich dabei dann auch nur seine Lage im Raum. Vermutlich, damit den Gästen auch ja so richtig schlecht wird. ;-)