---
layout: "image"
title: "Katamaran"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim125.jpg"
weight: "18"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32652
imported:
- "2019"
_4images_image_id: "32652"
_4images_cat_id: "2393"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "125"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32652 -->
