---
layout: "image"
title: "Haralds Gruselkabinett"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim118.jpg"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32645
imported:
- "2019"
_4images_image_id: "32645"
_4images_cat_id: "2393"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "118"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32645 -->
Gemoddete Teile ohne Ende. Raffiniert gemacht. Harald halt. Schleichwerbung: Details siehe ft:pedia 3/2011!