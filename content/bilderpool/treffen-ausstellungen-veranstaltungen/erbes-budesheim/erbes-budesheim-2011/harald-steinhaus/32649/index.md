---
layout: "image"
title: "Kettenkarussell"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim122.jpg"
weight: "15"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32649
imported:
- "2019"
_4images_image_id: "32649"
_4images_cat_id: "2393"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "122"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32649 -->
Blick auf die Antriebseinheit