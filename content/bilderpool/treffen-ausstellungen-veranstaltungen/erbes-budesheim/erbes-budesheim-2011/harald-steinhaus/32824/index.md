---
layout: "image"
title: "Fahrgeschäft II"
date: "2011-09-27T20:47:57"
picture: "Fahrgeschft_2.jpg"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/32824
imported:
- "2019"
_4images_image_id: "32824"
_4images_cat_id: "2393"
_4images_user_id: "1126"
_4images_image_date: "2011-09-27T20:47:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32824 -->
Fahrgeschäft