---
layout: "image"
title: "Katamaran"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim126.jpg"
weight: "19"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32653
imported:
- "2019"
_4images_image_id: "32653"
_4images_cat_id: "2393"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "126"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32653 -->
Mit Zieleinrichtung für die Wasserspritze