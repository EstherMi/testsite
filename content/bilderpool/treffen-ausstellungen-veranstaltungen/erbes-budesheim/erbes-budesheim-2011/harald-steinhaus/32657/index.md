---
layout: "image"
title: "Fünfeck-Konstruktionen"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim130.jpg"
weight: "23"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32657
imported:
- "2019"
_4images_image_id: "32657"
_4images_cat_id: "2393"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "130"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32657 -->
