---
layout: "image"
title: "Kettenkarussell"
date: "2011-09-26T10:10:21"
picture: "conventionerbesbuedesheim123.jpg"
weight: "16"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/32650
imported:
- "2019"
_4images_image_id: "32650"
_4images_cat_id: "2393"
_4images_user_id: "104"
_4images_image_date: "2011-09-26T10:10:21"
_4images_image_order: "123"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32650 -->
Die andere Seite der Antriebseinheit