---
layout: "image"
title: "Brücken (Michael Stratmann, Johann Fox)"
date: "2013-09-29T21:54:09"
picture: "convention09.jpg"
weight: "103"
konstrukteure: 
- "Fox/Stratmann"
fotografen:
- "Lars"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lars"
license: "unknown"
legacy_id:
- details/37452
imported:
- "2019"
_4images_image_id: "37452"
_4images_cat_id: "2784"
_4images_user_id: "1177"
_4images_image_date: "2013-09-29T21:54:09"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37452 -->
Bilder von der Convention 2013
Gesamtansicht
Bild 1 von 1
.
Modell:            links: Tower Bridge (Johann Fox),

Konstrukteur:  siehe oben
Fotograf:         Lars
.
Hinterlasst einen Kommentar, wenn ihr Fehler gefunden oder euch aufgefallen ist, dass in der Beschreibung was fehlt.