---
layout: "overview"
title: "Erbes-Büdesheim 2013"
date: 2019-12-17T18:30:16+01:00
legacy_id:
- categories/2784
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2784 --> 
Die Fischertechnik Convention 2013 fand am 28.09 von 10 bis 16 Uhr in Erbes Büdesheim statt und wurde von Knobloch organisiert.