---
layout: "image"
title: "Fräsmaschine mit Werkzeugwechsler"
date: "2013-09-30T23:47:38"
picture: "DSC00261_800x800.jpg"
weight: "94"
konstrukteure: 
- "Thomas Kaltenbrunner (Kalti)"
fotografen:
- "MickyW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- details/37471
imported:
- "2019"
_4images_image_id: "37471"
_4images_cat_id: "2784"
_4images_user_id: "1806"
_4images_image_date: "2013-09-30T23:47:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37471 -->
