---
layout: "image"
title: "Rummel - Geldspielgerät"
date: "2013-09-30T23:47:38"
picture: "DSC00258_800x800.jpg"
weight: "92"
konstrukteure: 
- "-?-"
fotografen:
- "MickyW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- details/37469
imported:
- "2019"
_4images_image_id: "37469"
_4images_cat_id: "2784"
_4images_user_id: "1806"
_4images_image_date: "2013-09-30T23:47:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37469 -->
