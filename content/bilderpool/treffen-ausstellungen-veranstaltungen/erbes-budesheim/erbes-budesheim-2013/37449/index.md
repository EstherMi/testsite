---
layout: "image"
title: "Rasenmäher (von H.A.R.R.Y.)"
date: "2013-09-29T21:54:09"
picture: "convention06.jpg"
weight: "102"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lars"
license: "unknown"
legacy_id:
- details/37449
imported:
- "2019"
_4images_image_id: "37449"
_4images_cat_id: "2784"
_4images_user_id: "1177"
_4images_image_date: "2013-09-29T21:54:09"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37449 -->
Bilder von der Convention 2013
-
Bild 2 von 2
.
Modell:            Rasenmäher
Konstrukteur:  H.A.R.R.Y.
Fotograf:         Lars
.
Hinterlasst einen Kommentar, wenn ihr Fehler gefunden oder euch aufgefallen ist, dass in der Beschreibung was fehlt.