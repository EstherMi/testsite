---
layout: "image"
title: "long_way.jpg"
date: "2013-09-30T17:55:00"
picture: "IMG_9795.jpg"
weight: "6"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/37460
imported:
- "2019"
_4images_image_id: "37460"
_4images_cat_id: "2796"
_4images_user_id: "4"
_4images_image_date: "2013-09-30T17:55:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37460 -->
Manchmal sind die Wege weit...