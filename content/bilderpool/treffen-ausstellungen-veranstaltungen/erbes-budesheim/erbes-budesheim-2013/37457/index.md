---
layout: "image"
title: "Hochregallager (von Tobias Endlich)"
date: "2013-09-29T21:54:21"
picture: "convention14.jpg"
weight: "105"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lars"
license: "unknown"
legacy_id:
- details/37457
imported:
- "2019"
_4images_image_id: "37457"
_4images_cat_id: "2784"
_4images_user_id: "1177"
_4images_image_date: "2013-09-29T21:54:21"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37457 -->
Bilder von der Convention 2013
Detailaufnahme
Bild 1 von 1
.
Modell:            Industrieanlage
Konstrukteur:  Tobiasl Endlich
Fotograf:         Lars
.
Hinterlasst einen Kommentar, wenn ihr Fehler gefunden oder euch aufgefallen ist, dass in der Beschreibung was fehlt.