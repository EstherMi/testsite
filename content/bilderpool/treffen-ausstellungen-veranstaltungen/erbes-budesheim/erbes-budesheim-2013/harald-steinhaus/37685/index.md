---
layout: "image"
title: "IMG_9820.JPG"
date: "2013-10-06T16:10:12"
picture: "IMG_9820.JPG"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/37685
imported:
- "2019"
_4images_image_id: "37685"
_4images_cat_id: "2800"
_4images_user_id: "4"
_4images_image_date: "2013-10-06T16:10:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37685 -->
Der Tarnkappenbomber war auch mit von der Partie. Der volle Tarnmodus ist schon beeindruckend.