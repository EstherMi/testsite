---
layout: "image"
title: "IMG_9551.JPG"
date: "2013-10-06T15:15:13"
picture: "IMG_9551.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/37680
imported:
- "2019"
_4images_image_id: "37680"
_4images_cat_id: "2800"
_4images_user_id: "4"
_4images_image_date: "2013-10-06T15:15:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37680 -->
Die Zugmaschine für den "Jumping". Das Getriebe hinten drauf dient zum Verschieben des Mastes beim Auf- und Abbau: die schwarze V-Steine links und rechts (der linke ist zu erkennen) werden über die grauen Kardanhälften am "Mittelbau" (das ist der Wagen dahinter, mit den zentralen Elementen des Karussels) geschoben und drehen dann die Schnecken, auf denen der Mast verschoben wird.