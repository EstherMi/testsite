---
layout: "image"
title: "eb096.jpg"
date: "2013-10-03T09:29:06"
picture: "eb096.jpg"
weight: "4"
konstrukteure: 
- "Leonidas"
fotografen:
- "Daniel A."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/37601
imported:
- "2019"
_4images_image_id: "37601"
_4images_cat_id: "2789"
_4images_user_id: "1"
_4images_image_date: "2013-10-03T09:29:06"
_4images_image_order: "96"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37601 -->
