---
layout: "image"
title: "Link zum Video von der FT-Convention 2013"
date: "2015-10-01T18:18:47"
picture: "Video_von_der_FT-Convention_2013.png"
weight: "1"
konstrukteure: 
- "Andreas Gürten"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/42025
imported:
- "2019"
_4images_image_id: "42025"
_4images_cat_id: "2784"
_4images_user_id: "724"
_4images_image_date: "2015-10-01T18:18:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42025 -->
Video von der FT-Convention 2013:
https://youtu.be/MpP8uHmwoik