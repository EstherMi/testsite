---
layout: "image"
title: "Break Dance (von Wilhelm Brickweede)"
date: "2013-09-29T21:54:09"
picture: "convention01.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Lars"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lars"
license: "unknown"
legacy_id:
- details/37444
imported:
- "2019"
_4images_image_id: "37444"
_4images_cat_id: "2791"
_4images_user_id: "1177"
_4images_image_date: "2013-09-29T21:54:09"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37444 -->
Bilder von der Convention 2013
Langzeitbelichtung
Bild 1 von 1
.
Modell:            Break Dance
Konstrukteur:  Wilhelm Brickweede
Fotograf:         Lars
.
Hinterlasst einen Kommentar, wenn ihr Fehler gefunden oder euch aufgefallen ist, dass in der Beschreibung was fehlt.