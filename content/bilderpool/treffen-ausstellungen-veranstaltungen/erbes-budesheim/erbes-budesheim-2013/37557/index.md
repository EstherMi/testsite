---
layout: "image"
title: "eb052.jpg"
date: "2013-10-03T09:29:06"
picture: "eb052.jpg"
weight: "27"
konstrukteure: 
- "Thomas Kaltenbrunner (Kalti)"
fotografen:
- "Daniel A."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/37557
imported:
- "2019"
_4images_image_id: "37557"
_4images_cat_id: "2784"
_4images_user_id: "1"
_4images_image_date: "2013-10-03T09:29:06"
_4images_image_order: "52"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37557 -->
