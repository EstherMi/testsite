---
layout: "image"
title: "Rückseite Kuckucksuhr"
date: "2013-09-30T23:47:38"
picture: "DSC00277_800x800.jpg"
weight: "6"
konstrukteure: 
- "PeterHolland"
fotografen:
- "MickyW"
keywords: ["Uhr", "Kuckucksuhr"]
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- details/37480
imported:
- "2019"
_4images_image_id: "37480"
_4images_cat_id: "2787"
_4images_user_id: "1806"
_4images_image_date: "2013-09-30T23:47:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37480 -->
