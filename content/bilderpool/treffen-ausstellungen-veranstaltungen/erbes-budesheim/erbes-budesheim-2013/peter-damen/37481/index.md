---
layout: "image"
title: "Fin-Ray Robotergreifer"
date: "2013-09-30T23:47:38"
picture: "DSC00279_800x800.jpg"
weight: "7"
konstrukteure: 
- "PeterHolland"
fotografen:
- "MickyW"
keywords: ["Roboter", "Fin-Ray", "Greifer"]
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- details/37481
imported:
- "2019"
_4images_image_id: "37481"
_4images_cat_id: "2787"
_4images_user_id: "1806"
_4images_image_date: "2013-09-30T23:47:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37481 -->
