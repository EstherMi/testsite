---
layout: "comment"
hidden: true
title: "18350"
date: "2013-10-02T11:40:42"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Hallo Peter,

das ist bestimmt ein Klassemodell, aber ein paar Detailbeschreibungen wären vermutlich noch hilfreicher, als dieselbe Beschriftung unter jedem Bild anzubringen. Kannst Du noch ein bisschen was zu den Bildern erklären bitte?

Gruß,
Stefan