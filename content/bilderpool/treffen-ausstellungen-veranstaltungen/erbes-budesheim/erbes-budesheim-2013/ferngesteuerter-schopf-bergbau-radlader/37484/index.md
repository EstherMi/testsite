---
layout: "image"
title: "Ferngesteuerter Schopf-Bergbau-Radlader"
date: "2013-10-01T23:57:42"
picture: "schopfbergbauradlader2.jpg"
weight: "2"
konstrukteure: 
- "Jörg und Erik Busch"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/37484
imported:
- "2019"
_4images_image_id: "37484"
_4images_cat_id: "2785"
_4images_user_id: "22"
_4images_image_date: "2013-10-01T23:57:42"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37484 -->
pneumatischer, proportionaler Knicklenkung und pneumatischer Schaufelsteuerung