---
layout: "image"
title: "Alte FT-Modelle"
date: "2013-09-30T23:47:38"
picture: "DSC00256_800x800.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "MickyW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- details/37467
imported:
- "2019"
_4images_image_id: "37467"
_4images_cat_id: "2799"
_4images_user_id: "1806"
_4images_image_date: "2013-09-30T23:47:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37467 -->
