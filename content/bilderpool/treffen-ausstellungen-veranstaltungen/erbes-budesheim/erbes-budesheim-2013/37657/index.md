---
layout: "image"
title: "eb152.jpg"
date: "2013-10-03T09:29:06"
picture: "eb152.jpg"
weight: "80"
konstrukteure: 
- "-?-"
fotografen:
- "Daniel A."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/37657
imported:
- "2019"
_4images_image_id: "37657"
_4images_cat_id: "2784"
_4images_user_id: "1"
_4images_image_date: "2013-10-03T09:29:06"
_4images_image_order: "152"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37657 -->
