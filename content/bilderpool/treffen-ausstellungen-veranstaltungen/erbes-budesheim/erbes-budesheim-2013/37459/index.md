---
layout: "image"
title: "Roboterarm (von Severin)"
date: "2013-09-29T21:54:21"
picture: "convention16.jpg"
weight: "107"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lars"
license: "unknown"
legacy_id:
- details/37459
imported:
- "2019"
_4images_image_id: "37459"
_4images_cat_id: "2784"
_4images_user_id: "1177"
_4images_image_date: "2013-09-29T21:54:21"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37459 -->
Bilder von der Convention 2013
Gesamtansicht
Bild 1 von 1
.
Modell:            Roboterarm
Konstrukteur:  Severin
Fotograf:         Lars
.
Hinterlasst einen Kommentar, wenn ihr Fehler gefunden oder euch aufgefallen ist, dass in der Beschreibung was fehlt.