---
layout: "image"
title: "Die Stützen vom Mobilkran"
date: "2003-07-08T16:05:24"
picture: "Kran.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/1216
imported:
- "2019"
_4images_image_id: "1216"
_4images_cat_id: "445"
_4images_user_id: "130"
_4images_image_date: "2003-07-08T16:05:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1216 -->
