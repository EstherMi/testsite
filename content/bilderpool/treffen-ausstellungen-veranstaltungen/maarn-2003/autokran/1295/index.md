---
layout: "image"
title: "Autokran3"
date: "2003-08-04T09:17:35"
picture: "kran3.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/1295
imported:
- "2019"
_4images_image_id: "1295"
_4images_cat_id: "445"
_4images_user_id: "34"
_4images_image_date: "2003-08-04T09:17:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1295 -->
Wenn man sich die Radstellung etwas genauer anschaut, sieht man, das alle unterschiedliche Winkel haben. Wie geht das? Ganz einfach. Von der Mitte des Fahrzeuges läuft eine Alustange bis nach Vorne. Zur Mitte hin(das eine Ende) ist sie in einem Gelek gelagert und vorne (andere Ende kann die Alustange mit einem Minimot und Hubgetriebe nach links und recht bewegt werden. Die Rader sind mit ihrer Lenkung nun einfach an dem Alu angebracht. Je weiter das Rad sich vom Gelek befindet desto größer ist der Radeinschlag. Einfach genial.