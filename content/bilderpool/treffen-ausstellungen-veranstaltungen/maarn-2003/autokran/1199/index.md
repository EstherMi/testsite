---
layout: "image"
title: "derrickkraan"
date: "2003-06-22T18:01:58"
picture: "derrickkraan02.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pvd"
license: "unknown"
legacy_id:
- details/1199
imported:
- "2019"
_4images_image_id: "1199"
_4images_cat_id: "445"
_4images_user_id: "7"
_4images_image_date: "2003-06-22T18:01:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1199 -->
Derrickkran , mit etwa diesselbe Riesenhöhe als der "Mammoetkran" .
Steht auf ein sehr langer LKW der 20 Reifen von 65 mm  hat, und dann noch ein kreuzformiger Untengestell mit sehr viel ganz lange Alustaben dabei .  Dieses enorm grosses Grundflach ist ja notwendig als Stabilisierung wenn man die Abmessungen der Derrickkran dabei seht.