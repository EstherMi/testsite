---
layout: "image"
title: "Vier Zylinder Motor"
date: "2003-07-10T14:03:57"
picture: "4zylindermotor.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/1248
imported:
- "2019"
_4images_image_id: "1248"
_4images_cat_id: "138"
_4images_user_id: "34"
_4images_image_date: "2003-07-10T14:03:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1248 -->
