---
layout: "image"
title: "Mammoetkran mit 10KG Belastung"
date: "2003-07-08T17:12:53"
picture: "Kran_mit_Gewicht_2.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/1233
imported:
- "2019"
_4images_image_id: "1233"
_4images_cat_id: "447"
_4images_user_id: "130"
_4images_image_date: "2003-07-08T17:12:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1233 -->
