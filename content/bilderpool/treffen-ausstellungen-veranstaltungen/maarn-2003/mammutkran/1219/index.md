---
layout: "image"
title: "Mammoetkran mit 10KG Belastung"
date: "2003-07-08T16:05:24"
picture: "Kran_mit_Gewicht.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/1219
imported:
- "2019"
_4images_image_id: "1219"
_4images_cat_id: "447"
_4images_user_id: "130"
_4images_image_date: "2003-07-08T16:05:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1219 -->
Hier hebt der Kran mal eben locker 10 KG Gewicht.