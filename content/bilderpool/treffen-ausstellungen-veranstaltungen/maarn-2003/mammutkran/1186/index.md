---
layout: "image"
title: "Mammoetkraan"
date: "2003-06-16T19:47:29"
picture: "mammoet02.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Kran", "Mammoetkran"]
uploadBy: "pvd"
license: "unknown"
legacy_id:
- details/1186
imported:
- "2019"
_4images_image_id: "1186"
_4images_cat_id: "447"
_4images_user_id: "7"
_4images_image_date: "2003-06-16T19:47:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1186 -->
Ein Modell das der Name "Mammoetkraan"  zurecht verdient .  Es ist ein Nachbau auf Skale  von eine echt bestehende Kran , mit selben Name.    Dieses Modell kann 10 Kg heben (!) , die Gegengewichte sind auch ganz schwer.   
 Im Vorgrund : die Flaschenfabrik vom Herrn Leurs .   Und links im Hintergrund (gelb) steht der Free Fall Tower vom Herrn Dijkstra ( Modell steht auf seine Homepage , link  dieser Page via :  http://www.fischertechnikclub.nl )