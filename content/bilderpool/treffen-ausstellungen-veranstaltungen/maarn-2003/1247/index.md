---
layout: "image"
title: "Verkauf2"
date: "2003-07-10T14:03:57"
picture: "verkauf2.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/1247
imported:
- "2019"
_4images_image_id: "1247"
_4images_cat_id: "138"
_4images_user_id: "34"
_4images_image_date: "2003-07-10T14:03:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1247 -->
Beim zweiten großen Verkaufsstand sind natürlich auch besondere Raritäten dabei. Wie hier die noch letzten zu habenen Spezial 2000 Kästen. Zumindest hab ich sie in Deutschland lange nicht mehr gesehen. Sie kommen aus einer Sonderproduktion für Singapur, wo sie in der Ausbildung eingesetzt werden.