---
layout: "image"
title: "Raupenfahrzeug2"
date: "2003-07-31T18:41:47"
picture: "raupenfahrzeug2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/1277
imported:
- "2019"
_4images_image_id: "1277"
_4images_cat_id: "444"
_4images_user_id: "34"
_4images_image_date: "2003-07-31T18:41:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1277 -->
Leider etwas unscharf. Man kann aber die zwei Mini-Mots, den Accupack und den IR Empfänger sehen. Die äußeren Antriebsräder sind die Deckel vom Differenzial vom Minimot einfach festgeklebt.