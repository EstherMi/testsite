---
layout: "image"
title: "Graue Teile"
date: "2003-07-10T14:03:57"
picture: "grau_verkauf.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/1246
imported:
- "2019"
_4images_image_id: "1246"
_4images_cat_id: "138"
_4images_user_id: "34"
_4images_image_date: "2003-07-10T14:03:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1246 -->
Hier der ausverkauf der letzten grauen Teile des NL-ft-Importeurs. Da ist dann der große Teil meines Taschengeldes der nächsten drei Monate geblieben.