---
layout: "image"
title: "Flaschen Abfüllanlage10"
date: "2003-08-04T09:17:35"
picture: "abfuellanlage10.jpg"
weight: "1"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/1297
imported:
- "2019"
_4images_image_id: "1297"
_4images_cat_id: "443"
_4images_user_id: "34"
_4images_image_date: "2003-08-04T09:17:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1297 -->
Bis zur Deckelaufgabe noch mal ein Überblick.