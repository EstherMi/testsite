---
layout: "image"
title: "Flaschenanlage"
date: "2003-07-07T13:57:10"
picture: "Flaschenanlage.jpg"
weight: "16"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/1204
imported:
- "2019"
_4images_image_id: "1204"
_4images_cat_id: "443"
_4images_user_id: "130"
_4images_image_date: "2003-07-07T13:57:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1204 -->
