---
layout: "image"
title: "Flaschen Abfüllanlage15"
date: "2003-08-04T09:17:35"
picture: "abfuellanlage15.jpg"
weight: "5"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/1301
imported:
- "2019"
_4images_image_id: "1301"
_4images_cat_id: "443"
_4images_user_id: "34"
_4images_image_date: "2003-08-04T09:17:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1301 -->
Hir nochmal die Rückseite. Ach ja - die Flaschen bekommen nicht nur einen Deckel, nein, auch noch ein Etikett!