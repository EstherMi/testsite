---
layout: "image"
title: "Flaschenanlage1"
date: "2003-07-07T13:58:41"
picture: "Flaschenanlage_1.jpg"
weight: "17"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/1211
imported:
- "2019"
_4images_image_id: "1211"
_4images_cat_id: "443"
_4images_user_id: "130"
_4images_image_date: "2003-07-07T13:58:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1211 -->
