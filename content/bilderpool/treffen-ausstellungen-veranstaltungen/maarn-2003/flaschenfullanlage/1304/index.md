---
layout: "image"
title: "Flaschen Abfüllanlage5"
date: "2003-08-04T09:17:36"
picture: "abfuellanlage5.jpg"
weight: "8"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/1304
imported:
- "2019"
_4images_image_id: "1304"
_4images_cat_id: "443"
_4images_user_id: "34"
_4images_image_date: "2003-08-04T09:17:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1304 -->
Die Aufgabe der Flaschen