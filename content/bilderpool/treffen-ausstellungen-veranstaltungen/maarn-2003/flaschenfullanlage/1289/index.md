---
layout: "image"
title: "Flaschen Abfüllanlage"
date: "2003-08-03T22:26:21"
picture: "abfuellanlage1.jpg"
weight: "12"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/1289
imported:
- "2019"
_4images_image_id: "1289"
_4images_cat_id: "443"
_4images_user_id: "34"
_4images_image_date: "2003-08-03T22:26:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1289 -->
Diese Flaschen Abfüllanlage ist eine gigantische Anlage die ohne PC arbeitet. Nur mit den ft-Silberlingen (E-Bausteine)!
Hier vorne werden die Flaschen aufgesellt (leider grad leer) und vom Schieber auf das Band geschoben.