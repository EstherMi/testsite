---
layout: "image"
title: "ftconventiondreiech094.jpg"
date: "2017-09-25T13:48:00"
picture: "ftconventiondreiech094.jpg"
weight: "86"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/46378
imported:
- "2019"
_4images_image_id: "46378"
_4images_cat_id: "3435"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:48:00"
_4images_image_order: "94"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46378 -->
