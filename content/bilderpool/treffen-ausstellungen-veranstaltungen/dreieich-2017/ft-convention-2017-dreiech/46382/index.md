---
layout: "image"
title: "ftconventiondreiech098.jpg"
date: "2017-09-25T13:48:00"
picture: "ftconventiondreiech098.jpg"
weight: "90"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/46382
imported:
- "2019"
_4images_image_id: "46382"
_4images_cat_id: "3435"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:48:00"
_4images_image_order: "98"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46382 -->
