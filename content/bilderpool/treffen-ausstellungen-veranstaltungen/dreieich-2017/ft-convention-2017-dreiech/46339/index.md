---
layout: "image"
title: "ftconventiondreiech055.jpg"
date: "2017-09-25T13:47:43"
picture: "ftconventiondreiech055.jpg"
weight: "55"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/46339
imported:
- "2019"
_4images_image_id: "46339"
_4images_cat_id: "3435"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:47:43"
_4images_image_order: "55"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46339 -->
