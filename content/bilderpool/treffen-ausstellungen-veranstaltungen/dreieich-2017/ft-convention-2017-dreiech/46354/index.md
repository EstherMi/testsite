---
layout: "image"
title: "ftconventiondreiech070.jpg"
date: "2017-09-25T13:47:48"
picture: "ftconventiondreiech070.jpg"
weight: "70"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/46354
imported:
- "2019"
_4images_image_id: "46354"
_4images_cat_id: "3435"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:47:48"
_4images_image_order: "70"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46354 -->
