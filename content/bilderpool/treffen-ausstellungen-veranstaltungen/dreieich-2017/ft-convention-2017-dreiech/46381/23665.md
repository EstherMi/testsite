---
layout: "comment"
hidden: true
title: "23665"
date: "2017-09-29T16:38:29"
uploadBy:
- "TiloRust"
license: "unknown"
imported:
- "2019"
---
Ein Arbeiter kontrolliert am Turm 2 / Bergstation des XXL-Express / Schrägseilbrücke eine der Fahrbahnbeleuchtungen, klar mit persönlicher Schutzausrüstung.