---
layout: "image"
title: "ftconventiondreiech086.jpg"
date: "2017-09-25T13:47:56"
picture: "ftconventiondreiech086.jpg"
weight: "78"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/46370
imported:
- "2019"
_4images_image_id: "46370"
_4images_cat_id: "3435"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:47:56"
_4images_image_order: "86"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46370 -->
