---
layout: "image"
title: "Flipper 'Pirates of the Carribean'"
date: "2017-10-02T17:32:28"
picture: "modelledirkwoelffel3.jpg"
weight: "3"
konstrukteure: 
- "Dirk Wölffel"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/46633
imported:
- "2019"
_4images_image_id: "46633"
_4images_cat_id: "3442"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:28"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46633 -->
Ausführliche Vorstellung in ft:pedia 2/2017.