---
layout: "image"
title: "Flipper 'Pirates of the Carribean'"
date: "2017-10-02T17:32:28"
picture: "modelledirkwoelffel2.jpg"
weight: "2"
konstrukteure: 
- "Dirk Wölffel"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/46632
imported:
- "2019"
_4images_image_id: "46632"
_4images_cat_id: "3442"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:28"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46632 -->
Ausführliche Vorstellung in ft:pedia 2/2017.