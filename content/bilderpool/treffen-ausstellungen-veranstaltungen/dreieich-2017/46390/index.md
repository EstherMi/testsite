---
layout: "image"
title: "Gelber S-Statikstein im Schwebebahndepot"
date: "2017-09-25T13:48:07"
picture: "IMG_20170923_153927_r.jpg"
weight: "73"
konstrukteure: 
- "Thomas Kaiser (und vermutlich S-Statikstein-Monteure Max-Leo Amberg und Leon/olagino)"
fotografen:
- "Martin Giger"
keywords: ["schwebebahn", "depot"]
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/46390
imported:
- "2019"
_4images_image_id: "46390"
_4images_cat_id: "3434"
_4images_user_id: "445"
_4images_image_date: "2017-09-25T13:48:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46390 -->
Das Bild zeigt die Oberseite des Depots. Unten in der Mitte sieht man die Depotweiche eingerastet auf das leere Depotgleis 3. Die Ausfahrt des Depots ist nach links. Im Hintergrund werden zusätzliche Lichtblenden zwischen den Lichtkommunikationsschnittstellen der geparkten Züge montiert um Fehler bei benachbarten Zügen zu reduzieren.