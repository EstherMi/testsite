---
layout: "image"
title: "Stapler"
date: "2017-09-30T13:03:40"
picture: "ftconvs09.jpg"
weight: "68"
konstrukteure: 
- "Holtz"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/46551
imported:
- "2019"
_4images_image_id: "46551"
_4images_cat_id: "3434"
_4images_user_id: "4"
_4images_image_date: "2017-09-30T13:03:40"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46551 -->
Mit selbstgemachtem Controller, der vom Handy aus gesteuert wird. Der Controller steuert zwei Servos (Lenkung vorn und hinten, mit allem und scharf: die Achsen werden einzeln oder kombiniert oder im Hundegang gelenkt), Antrieb, Licht und Kompressor. 

Davon hätte ich gerne die Infos zum Nachbauen!