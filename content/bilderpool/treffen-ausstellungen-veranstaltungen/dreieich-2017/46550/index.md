---
layout: "image"
title: "Exoten"
date: "2017-09-30T13:03:40"
picture: "ftconvs08.jpg"
weight: "67"
konstrukteure: 
- "ft"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/46550
imported:
- "2019"
_4images_image_id: "46550"
_4images_cat_id: "3434"
_4images_user_id: "4"
_4images_image_date: "2017-09-30T13:03:40"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46550 -->
Durchsichtige Platten 60x180. Wo die mal drin waren, weiß man nicht.