---
layout: "image"
title: "Transformer - Action"
date: "2017-10-02T17:32:39"
picture: "modellevonjenslemkamp3.jpg"
weight: "3"
konstrukteure: 
- "Jens Lemkamp"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/46673
imported:
- "2019"
_4images_image_id: "46673"
_4images_cat_id: "3449"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:39"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46673 -->
Wer mag da tauschen? (Noch lachen sie...)