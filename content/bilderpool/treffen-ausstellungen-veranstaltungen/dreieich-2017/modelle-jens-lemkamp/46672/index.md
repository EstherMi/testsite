---
layout: "image"
title: "Transformer - Ruhestellung"
date: "2017-10-02T17:32:39"
picture: "modellevonjenslemkamp2.jpg"
weight: "2"
konstrukteure: 
- "Jens Lemkamp"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/46672
imported:
- "2019"
_4images_image_id: "46672"
_4images_cat_id: "3449"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:39"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46672 -->
Noch klagt niemand über Kopfschmerzen...