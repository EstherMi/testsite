---
layout: "image"
title: "Saterday afternoon, back home"
date: "2017-09-25T13:48:21"
picture: "ftconventionchinesestriumphalarch21.jpg"
weight: "21"
konstrukteure: 
- "Michael Stratmann"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/46411
imported:
- "2019"
_4images_image_id: "46411"
_4images_cat_id: "3436"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:48:21"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46411 -->
