---
layout: "image"
title: "ftconventionchinesestriumphalarch05.jpg"
date: "2017-09-25T13:48:15"
picture: "ftconventionchinesestriumphalarch05.jpg"
weight: "5"
konstrukteure: 
- "Michael Stratmann"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/46395
imported:
- "2019"
_4images_image_id: "46395"
_4images_cat_id: "3436"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:48:15"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46395 -->
