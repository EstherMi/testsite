---
layout: "image"
title: "The arch with the constructor/designer"
date: "2017-09-25T13:48:07"
picture: "ftconventionchinesestriumphalarch01.jpg"
weight: "1"
konstrukteure: 
- "Michael Stratmann"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/46391
imported:
- "2019"
_4images_image_id: "46391"
_4images_cat_id: "3436"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:48:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46391 -->
