---
layout: "image"
title: "Fryday evening"
date: "2017-09-25T13:48:15"
picture: "ftconventionchinesestriumphalarch07.jpg"
weight: "7"
konstrukteure: 
- "Michael Stratmann"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/46397
imported:
- "2019"
_4images_image_id: "46397"
_4images_cat_id: "3436"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:48:15"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46397 -->
