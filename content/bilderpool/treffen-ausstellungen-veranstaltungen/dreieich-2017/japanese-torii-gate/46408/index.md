---
layout: "image"
title: "ftconventionchinesestriumphalarch18.jpg"
date: "2017-09-25T13:48:21"
picture: "ftconventionchinesestriumphalarch18.jpg"
weight: "18"
konstrukteure: 
- "Michael Stratmann"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/46408
imported:
- "2019"
_4images_image_id: "46408"
_4images_cat_id: "3436"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:48:21"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46408 -->
