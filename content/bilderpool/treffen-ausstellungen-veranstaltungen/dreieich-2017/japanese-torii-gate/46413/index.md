---
layout: "image"
title: "Saterday afternoon, back home"
date: "2017-09-25T13:48:21"
picture: "ftconventionchinesestriumphalarch23.jpg"
weight: "23"
konstrukteure: 
- "Michael Stratmann"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/46413
imported:
- "2019"
_4images_image_id: "46413"
_4images_cat_id: "3436"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:48:21"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46413 -->
