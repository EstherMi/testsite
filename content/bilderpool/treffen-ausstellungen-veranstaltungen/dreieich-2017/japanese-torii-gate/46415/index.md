---
layout: "image"
title: "Saterday afternoon, back home"
date: "2017-09-25T13:48:27"
picture: "ftconventionchinesestriumphalarch25.jpg"
weight: "25"
konstrukteure: 
- "Michael Stratmann"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/46415
imported:
- "2019"
_4images_image_id: "46415"
_4images_cat_id: "3436"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:48:27"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46415 -->
