---
layout: "image"
title: "Saturday morning"
date: "2017-09-25T13:48:15"
picture: "ftconventionchinesestriumphalarch11.jpg"
weight: "11"
konstrukteure: 
- "Michael Stratmann"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/46401
imported:
- "2019"
_4images_image_id: "46401"
_4images_cat_id: "3436"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:48:15"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46401 -->
