---
layout: "image"
title: "ftconventionchinesestriumphalarch03.jpg"
date: "2017-09-25T13:48:07"
picture: "ftconventionchinesestriumphalarch03.jpg"
weight: "3"
konstrukteure: 
- "Michael Stratmann"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/46393
imported:
- "2019"
_4images_image_id: "46393"
_4images_cat_id: "3436"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:48:07"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46393 -->
