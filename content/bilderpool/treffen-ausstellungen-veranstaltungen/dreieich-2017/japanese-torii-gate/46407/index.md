---
layout: "image"
title: "The arch is ready"
date: "2017-09-25T13:48:21"
picture: "ftconventionchinesestriumphalarch17.jpg"
weight: "17"
konstrukteure: 
- "Michael Stratmann"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/46407
imported:
- "2019"
_4images_image_id: "46407"
_4images_cat_id: "3436"
_4images_user_id: "136"
_4images_image_date: "2017-09-25T13:48:21"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46407 -->
