---
layout: "image"
title: "XXL-Express / Schrägseilbrücke"
date: "2017-09-30T11:52:18"
picture: "aIMG_2867.jpg"
weight: "1"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/46521
imported:
- "2019"
_4images_image_id: "46521"
_4images_cat_id: "3437"
_4images_user_id: "2638"
_4images_image_date: "2017-09-30T11:52:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46521 -->
Spannweite: 3,60m
Länge über alles: 5,25m
Turmhöhe: 1,30m
Bahnhöhe über Grund: 40cm
Automatische Fahrt, Seilspannung, Seillängen-Regelung, Kabelfernbedienung, LED Beleuchtung