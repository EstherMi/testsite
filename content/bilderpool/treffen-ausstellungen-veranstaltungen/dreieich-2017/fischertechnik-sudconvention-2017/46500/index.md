---
layout: "image"
title: "Transformer"
date: "2017-09-27T18:24:57"
picture: "dreieich76.jpg"
weight: "84"
konstrukteure: 
- "Jens Lemkamp"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/46500
imported:
- "2019"
_4images_image_id: "46500"
_4images_cat_id: "3437"
_4images_user_id: "2303"
_4images_image_date: "2017-09-27T18:24:57"
_4images_image_order: "76"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46500 -->
