---
layout: "image"
title: "Fahrroboter"
date: "2017-09-27T18:24:30"
picture: "dreieich27.jpg"
weight: "35"
konstrukteure: 
- "Fam.Busch"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/46451
imported:
- "2019"
_4images_image_id: "46451"
_4images_cat_id: "3437"
_4images_user_id: "2303"
_4images_image_date: "2017-09-27T18:24:30"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46451 -->
