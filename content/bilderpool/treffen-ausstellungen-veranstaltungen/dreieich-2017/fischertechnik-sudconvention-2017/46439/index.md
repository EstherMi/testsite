---
layout: "image"
title: "Schloss fischertechnik"
date: "2017-09-27T18:24:25"
picture: "dreieich15.jpg"
weight: "23"
konstrukteure: 
- "Franz Santjohannser"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/46439
imported:
- "2019"
_4images_image_id: "46439"
_4images_cat_id: "3437"
_4images_user_id: "2303"
_4images_image_date: "2017-09-27T18:24:25"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46439 -->
