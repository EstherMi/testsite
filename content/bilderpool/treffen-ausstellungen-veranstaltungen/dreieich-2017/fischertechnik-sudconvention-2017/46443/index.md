---
layout: "image"
title: "Greifautomat"
date: "2017-09-27T18:24:25"
picture: "dreieich19.jpg"
weight: "27"
konstrukteure: 
- "Stefan Fuss"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/46443
imported:
- "2019"
_4images_image_id: "46443"
_4images_cat_id: "3437"
_4images_user_id: "2303"
_4images_image_date: "2017-09-27T18:24:25"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46443 -->
