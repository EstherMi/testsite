---
layout: "image"
title: "Eiffelturm"
date: "2017-09-27T18:24:07"
picture: "dreieich01.jpg"
weight: "9"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/46425
imported:
- "2019"
_4images_image_id: "46425"
_4images_cat_id: "3437"
_4images_user_id: "2303"
_4images_image_date: "2017-09-27T18:24:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46425 -->
Größe 90 x 94 cm, 2,30m hoch.