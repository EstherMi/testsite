---
layout: "image"
title: "Eiffelturm"
date: "2017-09-27T18:24:18"
picture: "dreieich04.jpg"
weight: "12"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/46428
imported:
- "2019"
_4images_image_id: "46428"
_4images_cat_id: "3437"
_4images_user_id: "2303"
_4images_image_date: "2017-09-27T18:24:18"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46428 -->
2. Plattform mit Kamera.