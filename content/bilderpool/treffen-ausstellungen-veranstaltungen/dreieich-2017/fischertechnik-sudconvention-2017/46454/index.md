---
layout: "image"
title: "Malmaschine"
date: "2017-09-27T18:24:30"
picture: "dreieich30.jpg"
weight: "38"
konstrukteure: 
- "Fam.Busch"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/46454
imported:
- "2019"
_4images_image_id: "46454"
_4images_cat_id: "3437"
_4images_user_id: "2303"
_4images_image_date: "2017-09-27T18:24:30"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46454 -->
