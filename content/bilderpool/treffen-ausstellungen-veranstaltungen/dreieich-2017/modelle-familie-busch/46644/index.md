---
layout: "image"
title: "Pistenbully (Jörg)"
date: "2017-10-02T17:32:28"
picture: "modellefamiliebusch9.jpg"
weight: "9"
konstrukteure: 
- "Familie Busch"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/46644
imported:
- "2019"
_4images_image_id: "46644"
_4images_cat_id: "3443"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:28"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46644 -->
Ausführliche Beschreibung in ft:pedia 1/2015