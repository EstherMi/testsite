---
layout: "image"
title: "Schrägseilbrücke"
date: "2017-09-30T18:49:56"
picture: "ftconvs12.jpg"
weight: "71"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/46554
imported:
- "2019"
_4images_image_id: "46554"
_4images_cat_id: "3434"
_4images_user_id: "4"
_4images_image_date: "2017-09-30T18:49:56"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46554 -->
Die Talstation hat es in sich: sie zieht wechselweise an den Enden des Zugseils (die Bergstation hat nur eine feste Rolle), und sie hält das Seil immer unter gleichem Zug. Das auch dann, wenn sich die Brücke durchbiegt und bei unterschiedlichem Verhältnis von Drehwinkel der Seiltrommel zur Länge des auf- oder abgewickelten Seils.