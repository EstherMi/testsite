---
layout: "image"
title: "Ein-Schienen-Maus mit Umpoler"
date: "2017-10-02T17:32:53"
picture: "modellestefanfalk4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/46717
imported:
- "2019"
_4images_image_id: "46717"
_4images_cat_id: "3458"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:53"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46717 -->
