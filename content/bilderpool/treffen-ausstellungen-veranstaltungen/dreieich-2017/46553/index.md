---
layout: "image"
title: "Ballweitergabe"
date: "2017-09-30T18:49:56"
picture: "ftconvs11.jpg"
weight: "70"
konstrukteure: 
- "NN"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/46553
imported:
- "2019"
_4images_image_id: "46553"
_4images_cat_id: "3434"
_4images_user_id: "4"
_4images_image_date: "2017-09-30T18:49:56"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46553 -->
Links und rechts fahren zwei Wannen auf und ab, die je 3 Bälle aufnehmen können. Be- und entladen wird durch seitliches Kippen der Wannen.