---
layout: "image"
title: "Panzer-Turm"
date: "2017-10-02T17:32:39"
picture: "modelleharaldsteinhaus4.jpg"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/46666
imported:
- "2019"
_4images_image_id: "46666"
_4images_cat_id: "3448"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:39"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46666 -->
Blick unter die Haube