---
layout: "image"
title: "Panzer - offen"
date: "2017-10-02T17:32:28"
picture: "modelleharaldsteinhaus2.jpg"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/46664
imported:
- "2019"
_4images_image_id: "46664"
_4images_cat_id: "3448"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:28"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46664 -->
Detailansicht: Turm und Antrieb/Federung