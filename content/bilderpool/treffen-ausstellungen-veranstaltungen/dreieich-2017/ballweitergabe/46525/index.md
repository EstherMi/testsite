---
layout: "image"
title: "Tischtennisball-Weitergabemaschine Station: Eier-Finger"
date: "2017-09-30T11:52:18"
picture: "aIMG_2878.jpg"
weight: "11"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/46525
imported:
- "2019"
_4images_image_id: "46525"
_4images_cat_id: "3439"
_4images_user_id: "2638"
_4images_image_date: "2017-09-30T11:52:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46525 -->
