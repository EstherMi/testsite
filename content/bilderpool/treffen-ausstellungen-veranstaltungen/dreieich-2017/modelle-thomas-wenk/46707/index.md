---
layout: "image"
title: "Gesamtansicht Kugelbahn"
date: "2017-10-02T17:32:52"
picture: "modellethomaswenk1.jpg"
weight: "1"
konstrukteure: 
- "Thomas Wenk"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/46707
imported:
- "2019"
_4images_image_id: "46707"
_4images_cat_id: "3457"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:52"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46707 -->
