---
layout: "image"
title: "Riesenkugelbahn"
date: "2017-10-02T17:32:52"
picture: "modellethomaswenk3.jpg"
weight: "3"
konstrukteure: 
- "Thomas Wenk"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/46709
imported:
- "2019"
_4images_image_id: "46709"
_4images_cat_id: "3457"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:52"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46709 -->
