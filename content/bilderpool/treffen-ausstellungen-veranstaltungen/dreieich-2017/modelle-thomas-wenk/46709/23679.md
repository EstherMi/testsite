---
layout: "comment"
hidden: true
title: "23679"
date: "2017-10-10T11:02:38"
uploadBy:
- "ThomasW"
license: "unknown"
imported:
- "2019"
---
Dieser Lift läuft relativ schnell und transportiert in 20 Sekunden etwa 60 Kugeln, dann macht er Pause, bis der kurze Abschnitt des Wartebereichs wieder gefüllt ist.
Die 2016er Ausgabe dieses Lift war nur 2 Spuren (ca. 30mm) breit, das führte immer wieder zu Problemen im Einlaufbereich. Deshalb ist der 2017er-Lift 3 Spuren breit und die Probleme sind weg. Die Drehrichtung der Spirale wurde ebenfalls geändert, um den Einlauf einfacher zu gestalten.