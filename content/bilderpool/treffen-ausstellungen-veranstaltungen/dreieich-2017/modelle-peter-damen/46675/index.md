---
layout: "image"
title: "Gartenbahn mit Draisine"
date: "2017-10-02T17:32:39"
picture: "modellepeterdamen1.jpg"
weight: "1"
konstrukteure: 
- "Peter Damen"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/46675
imported:
- "2019"
_4images_image_id: "46675"
_4images_cat_id: "3450"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:39"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46675 -->
... und IR-Fernsteuerung