---
layout: "image"
title: "Formel 1 - Ferrari"
date: "2017-10-02T17:32:52"
picture: "modellehanswijnsouw1.jpg"
weight: "1"
konstrukteure: 
- "Hans Wijnsouw"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/46696
imported:
- "2019"
_4images_image_id: "46696"
_4images_cat_id: "3454"
_4images_user_id: "1126"
_4images_image_date: "2017-10-02T17:32:52"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46696 -->
