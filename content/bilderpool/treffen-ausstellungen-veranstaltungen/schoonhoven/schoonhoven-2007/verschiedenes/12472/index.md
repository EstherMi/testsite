---
layout: "image"
title: "Kleinmodelle"
date: "2007-11-04T20:23:27"
picture: "verschiedenes11.jpg"
weight: "15"
konstrukteure: 
- "Jos van Baal"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/12472
imported:
- "2019"
_4images_image_id: "12472"
_4images_cat_id: "1117"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T20:23:27"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12472 -->
