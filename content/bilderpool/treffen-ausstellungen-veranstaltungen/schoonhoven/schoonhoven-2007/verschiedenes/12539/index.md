---
layout: "image"
title: "Flieger060.JPG"
date: "2007-11-08T19:15:20"
picture: "Flieger060.JPG"
weight: "2"
konstrukteure: 
- "Jos van Baal"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/12539
imported:
- "2019"
_4images_image_id: "12539"
_4images_cat_id: "1117"
_4images_user_id: "4"
_4images_image_date: "2007-11-08T19:15:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12539 -->
Der 8-Blatt-Propeller hat was (auch wenn 4 Blätter rückwärts sitzen). Da könnte ich glatt einen neuen Flieger als Projekt ins Auge fassen.