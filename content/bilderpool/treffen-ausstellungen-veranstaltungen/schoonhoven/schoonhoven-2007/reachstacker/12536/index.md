---
layout: "image"
title: "Reachstacker10.JPG"
date: "2007-11-08T18:59:15"
picture: "Reachstacker10.JPG"
weight: "3"
konstrukteure: 
- "jmn"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/12536
imported:
- "2019"
_4images_image_id: "12536"
_4images_cat_id: "1115"
_4images_user_id: "4"
_4images_image_date: "2007-11-08T18:59:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12536 -->
Wie beim Vorbild kann das Führerhaus nach vorn und hinten verschoben werden (ist hier nicht zu erkennen) und hat eine seitliche Schiebetür.