---
layout: "image"
title: "Reachstacker20.JPG"
date: "2007-11-08T18:56:21"
picture: "Reachstacker20.JPG"
weight: "2"
konstrukteure: 
- "jmn"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/12535
imported:
- "2019"
_4images_image_id: "12535"
_4images_cat_id: "1115"
_4images_user_id: "4"
_4images_image_date: "2007-11-08T18:56:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12535 -->
Die Vorderachse des Reachstackers beruht auf der Hinterachse des Terek MT3600B von Peter Krijnen (siehe z.B. http://www.ftcommunity.de/details.php?image_id=3927 )