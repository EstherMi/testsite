---
layout: "image"
title: "Warenautomat07.JPG"
date: "2007-11-05T21:58:26"
picture: "Warenautomat07.JPG"
weight: "7"
konstrukteure: 
- "Evert Hardendood"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/12525
imported:
- "2019"
_4images_image_id: "12525"
_4images_cat_id: "1122"
_4images_user_id: "4"
_4images_image_date: "2007-11-05T21:58:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12525 -->
Die Rückwand hat es in sich: da ist eine ordentliche Menge Elektronik und Elektromechanik versammelt.