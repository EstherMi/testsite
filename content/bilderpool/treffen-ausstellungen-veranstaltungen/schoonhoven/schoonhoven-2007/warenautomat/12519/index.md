---
layout: "image"
title: "Warenautomat01.JPG"
date: "2007-11-05T21:45:49"
picture: "Warenautomat01.JPG"
weight: "1"
konstrukteure: 
- "Evert Hardendood"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/12519
imported:
- "2019"
_4images_image_id: "12519"
_4images_cat_id: "1122"
_4images_user_id: "4"
_4images_image_date: "2007-11-05T21:45:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12519 -->
Hier gibt es für 20 Cent die Auswahl zwischen drei Sorten von Schokoriegeln. Gewählt wird mit den Tasten oberhalb der Warenschächte. Wer sich vertan hat, kann sein Geld mit der "Abbruch"-Taste (unter dem Münzeinwurf, der mit dem Pfeil kenntlich gemacht ist) zurückfordern.