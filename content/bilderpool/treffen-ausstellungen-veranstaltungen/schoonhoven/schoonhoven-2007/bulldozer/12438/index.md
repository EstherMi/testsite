---
layout: "image"
title: "Heck"
date: "2007-11-04T20:02:18"
picture: "bulldozer2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/12438
imported:
- "2019"
_4images_image_id: "12438"
_4images_cat_id: "1113"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T20:02:18"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12438 -->
