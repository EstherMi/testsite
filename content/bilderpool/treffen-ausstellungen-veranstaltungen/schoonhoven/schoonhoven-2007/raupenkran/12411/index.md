---
layout: "image"
title: "3-fach Teleskop"
date: "2007-11-04T19:45:05"
picture: "raupenkran19.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/12411
imported:
- "2019"
_4images_image_id: "12411"
_4images_cat_id: "1109"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T19:45:05"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12411 -->
