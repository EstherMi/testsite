---
layout: "image"
title: "Winden"
date: "2007-11-04T19:45:04"
picture: "raupenkran05.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/12397
imported:
- "2019"
_4images_image_id: "12397"
_4images_cat_id: "1109"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T19:45:04"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12397 -->
