---
layout: "image"
title: "Teleskopmechanismus"
date: "2007-11-04T19:45:04"
picture: "raupenkran02.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/12394
imported:
- "2019"
_4images_image_id: "12394"
_4images_cat_id: "1109"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T19:45:04"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12394 -->
Durch Gewindestangen M4 wird der Hauptausleger dreifach (in vier Segmenten) teleskopiert. Wenn ich es richtig verstanden habe, sorgen acht Gewindestangen für die Führung des Auslegers.