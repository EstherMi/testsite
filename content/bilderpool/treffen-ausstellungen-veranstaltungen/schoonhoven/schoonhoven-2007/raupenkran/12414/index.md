---
layout: "image"
title: "Teleskopmechanismus"
date: "2007-11-04T19:45:05"
picture: "raupenkran22.jpg"
weight: "22"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/12414
imported:
- "2019"
_4images_image_id: "12414"
_4images_cat_id: "1109"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T19:45:05"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12414 -->
