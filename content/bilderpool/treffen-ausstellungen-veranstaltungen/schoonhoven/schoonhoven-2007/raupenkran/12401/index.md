---
layout: "image"
title: "Führerkabine"
date: "2007-11-04T19:45:04"
picture: "raupenkran09.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/12401
imported:
- "2019"
_4images_image_id: "12401"
_4images_cat_id: "1109"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T19:45:04"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12401 -->
Die Kabine ist pneumatisch neigbar.