---
layout: "image"
title: "Screenshot Bedienfeld"
date: "2007-11-04T20:22:10"
picture: "minensuchroboter1.jpg"
weight: "2"
konstrukteure: 
- "Andries Tieleman"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/12442
imported:
- "2019"
_4images_image_id: "12442"
_4images_cat_id: "1114"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T20:22:10"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12442 -->
