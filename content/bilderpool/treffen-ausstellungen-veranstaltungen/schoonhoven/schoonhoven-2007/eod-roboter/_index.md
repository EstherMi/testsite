---
layout: "overview"
title: "EOD-Roboter"
date: 2019-12-17T18:17:36+01:00
legacy_id:
- categories/1114
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1114 --> 
EOD steht für "Explosive Ordnance Disposal" -- sprich, Entschärfung von Sprengkörpern. Das kann mit verschiedenen Methoden geschehen: Wegbringen, Aufbohren, Zersägen, Aufsprengen (mit Wasser), zur Not auch Beschießen.