---
layout: "image"
title: "Erste und zweite Bewegungsachse"
date: "2007-11-04T20:22:10"
picture: "minensuchroboter3.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/12444
imported:
- "2019"
_4images_image_id: "12444"
_4images_cat_id: "1114"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T20:22:10"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12444 -->
