---
layout: "image"
title: "EOD-Robo020.JPG"
date: "2007-11-05T20:47:49"
picture: "EOD-Robo020.JPG"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/12518
imported:
- "2019"
_4images_image_id: "12518"
_4images_cat_id: "1114"
_4images_user_id: "4"
_4images_image_date: "2007-11-05T20:47:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12518 -->
