---
layout: "image"
title: "Sockel Detail 3"
date: "2007-11-04T20:02:17"
picture: "aleliftingdevice11.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/12425
imported:
- "2019"
_4images_image_id: "12425"
_4images_cat_id: "1110"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T20:02:17"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12425 -->
