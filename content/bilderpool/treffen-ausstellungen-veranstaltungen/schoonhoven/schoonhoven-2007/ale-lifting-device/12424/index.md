---
layout: "image"
title: "Sockel Detail 2"
date: "2007-11-04T20:02:17"
picture: "aleliftingdevice10.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/12424
imported:
- "2019"
_4images_image_id: "12424"
_4images_cat_id: "1110"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T20:02:17"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12424 -->
