---
layout: "image"
title: "Gegengewicht"
date: "2007-11-04T20:02:17"
picture: "aleliftingdevice02.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/12416
imported:
- "2019"
_4images_image_id: "12416"
_4images_cat_id: "1110"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T20:02:17"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12416 -->
Der Turm aus Statik-Teilen dreht sich gegenüber der roten Grundplatte. Das Gewicht steht somit fest.