---
layout: "image"
title: "Autos"
date: "2007-11-04T20:02:18"
picture: "autofabrik1.jpg"
weight: "2"
konstrukteure: 
- "Ralph Roetman"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/12434
imported:
- "2019"
_4images_image_id: "12434"
_4images_cat_id: "1112"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T20:02:18"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12434 -->
