---
layout: "image"
title: "Autofabrik101.JPG"
date: "2007-11-08T19:08:36"
picture: "Autofabrik101.JPG"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/12537
imported:
- "2019"
_4images_image_id: "12537"
_4images_cat_id: "1112"
_4images_user_id: "4"
_4images_image_date: "2007-11-08T19:08:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12537 -->
Ein Auto ist gerade in Arbeit. Der Alu-Stein 30 auf dem Schneckentrieb ist etwas besonderes: es ist einer mit der Gravur "15 jaar fischertechnicclub nl", den nur Mitglieder des Clubs bekommen. Er ist also eher ein "Gedenkstein" als ein Baustein. Hat ihm aber hier nichts geholfen; auch Gedenksteine müssen mitarbeiten.