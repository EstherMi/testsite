---
layout: "image"
title: "Rohling und Transport"
date: "2007-11-04T20:02:18"
picture: "autofabrik2.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/12435
imported:
- "2019"
_4images_image_id: "12435"
_4images_cat_id: "1112"
_4images_user_id: "9"
_4images_image_date: "2007-11-04T20:02:18"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12435 -->
