---
layout: "image"
title: "Patrick Crombach"
date: "2009-11-07T20:23:46"
picture: "fischertechniktreffenschoonhoven10.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/25701
imported:
- "2019"
_4images_image_id: "25701"
_4images_cat_id: "1803"
_4images_user_id: "22"
_4images_image_date: "2009-11-07T20:23:46"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25701 -->
FT-Hovercraft