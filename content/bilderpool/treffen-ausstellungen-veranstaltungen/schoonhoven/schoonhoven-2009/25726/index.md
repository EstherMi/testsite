---
layout: "image"
title: "3D-Struktur"
date: "2009-11-08T13:54:27"
picture: "schoonhoven_2009_073.jpg"
weight: "3"
konstrukteure: 
- "FT-Freunden"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/25726
imported:
- "2019"
_4images_image_id: "25726"
_4images_cat_id: "1803"
_4images_user_id: "22"
_4images_image_date: "2009-11-08T13:54:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25726 -->
