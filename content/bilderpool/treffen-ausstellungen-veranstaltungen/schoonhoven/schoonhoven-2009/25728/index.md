---
layout: "image"
title: "Organisator Clemens Jansen + FT-Clubheft-NL-Redacteur Rob van Baal"
date: "2009-11-08T13:54:28"
picture: "schoonhoven_2009_089.jpg"
weight: "5"
konstrukteure: 
- "FT-Freunden"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/25728
imported:
- "2019"
_4images_image_id: "25728"
_4images_cat_id: "1803"
_4images_user_id: "22"
_4images_image_date: "2009-11-08T13:54:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25728 -->
Organisator Clemens Jansen + FT-Clubheft-NL-Redacteur Rob van Baal beim Modellen Peter Damen:

- Pneumatik Muskeln-Motor
- Inundatie-waaiersluizen Nieuwe Hollandse Waterlinie
- Pneumatik Kubik
- FT-Kugel-robot
- FT-Heftruck (Forklift)
- Mechanische und Pneumatische 3D-Rüssel (3D-Slurf)