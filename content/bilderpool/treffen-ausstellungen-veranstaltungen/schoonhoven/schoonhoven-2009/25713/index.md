---
layout: "image"
title: "Rob van Oostenbrugge"
date: "2009-11-07T20:23:47"
picture: "fischertechniktreffenschoonhoven22.jpg"
weight: "27"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/25713
imported:
- "2019"
_4images_image_id: "25713"
_4images_cat_id: "1803"
_4images_user_id: "22"
_4images_image_date: "2009-11-07T20:23:47"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25713 -->
FT-Heftruck Anhänger   (Forklift)