---
layout: "image"
title: "Modellen Peter Damen"
date: "2009-11-07T20:23:47"
picture: "fischertechniktreffenschoonhoven26.jpg"
weight: "31"
konstrukteure: 
- "Peter Damen, Poederoyen NL"
fotografen:
- "Peter Damen, Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/25717
imported:
- "2019"
_4images_image_id: "25717"
_4images_cat_id: "1803"
_4images_user_id: "22"
_4images_image_date: "2009-11-07T20:23:47"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25717 -->
- Pneumatik Muskeln-Motor
- Inundatie-waaiersluizen Nieuwe Hollandse Waterlinie
- Pneumatik Kubik
- FT-Kugel-robot
- FT-Heftruck   (Forklift)
- Mechanische und Pneumatische 3D-Rüssel  (3D-Slurf)