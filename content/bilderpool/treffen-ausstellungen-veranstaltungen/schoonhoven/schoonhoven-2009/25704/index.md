---
layout: "image"
title: "Patrick Crombach"
date: "2009-11-07T20:23:47"
picture: "fischertechniktreffenschoonhoven13.jpg"
weight: "18"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/25704
imported:
- "2019"
_4images_image_id: "25704"
_4images_cat_id: "1803"
_4images_user_id: "22"
_4images_image_date: "2009-11-07T20:23:47"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25704 -->
FT-Hovercraft