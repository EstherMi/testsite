---
layout: "overview"
title: "Schoonhoven"
date: 2019-12-17T18:15:34+01:00
legacy_id:
- categories/4
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=4 --> 
im holländischen Schoonhoven wird seit über 10 Jahren ein Treffen von Technikmodellbauern abgehalten.

Die Modelle sind dort so zahlreich wie verblüffend, [a href="http://utopia.knoware.nl/users/cdeweerd/n18.html"]der FCNL[/a] gibt weitere Informationen.