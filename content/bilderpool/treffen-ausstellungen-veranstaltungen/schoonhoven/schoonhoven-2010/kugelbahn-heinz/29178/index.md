---
layout: "image"
title: "FT-Treffen-Schoonhoven-Thema"
date: "2010-11-06T23:40:15"
picture: "fischertechnikbijeenkomstschoonhovennov72.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/29178
imported:
- "2019"
_4images_image_id: "29178"
_4images_cat_id: "2345"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:40:15"
_4images_image_order: "72"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29178 -->
FT-Treffen-Schoonhoven-Thema