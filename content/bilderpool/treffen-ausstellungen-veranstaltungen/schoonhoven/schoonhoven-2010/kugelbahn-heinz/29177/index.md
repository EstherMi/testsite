---
layout: "image"
title: "FT-Treffen-Schoonhoven-Thema"
date: "2010-11-06T23:40:15"
picture: "fischertechnikbijeenkomstschoonhovennov71.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/29177
imported:
- "2019"
_4images_image_id: "29177"
_4images_cat_id: "2345"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:40:15"
_4images_image_order: "71"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29177 -->
FT-Treffen-Schoonhoven-Thema