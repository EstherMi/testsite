---
layout: "image"
title: "Herman Mels"
date: "2010-11-06T23:39:19"
picture: "fischertechnikbijeenkomstschoonhovennov24.jpg"
weight: "24"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/29130
imported:
- "2019"
_4images_image_id: "29130"
_4images_cat_id: "2116"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:39:19"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29130 -->
Herman Mels