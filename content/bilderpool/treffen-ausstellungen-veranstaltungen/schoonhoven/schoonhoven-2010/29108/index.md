---
layout: "image"
title: "3D-Drucker Paul Niekerk"
date: "2010-11-06T23:38:53"
picture: "fischertechnikbijeenkomstschoonhovennov02.jpg"
weight: "2"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/29108
imported:
- "2019"
_4images_image_id: "29108"
_4images_cat_id: "2116"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:38:53"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29108 -->
3D-Drucker Paul Niekerk