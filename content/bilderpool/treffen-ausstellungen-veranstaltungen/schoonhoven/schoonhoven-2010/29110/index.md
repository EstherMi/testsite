---
layout: "image"
title: "Peter Krijnen + Anton Janssen"
date: "2010-11-06T23:38:53"
picture: "fischertechnikbijeenkomstschoonhovennov04.jpg"
weight: "4"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/29110
imported:
- "2019"
_4images_image_id: "29110"
_4images_cat_id: "2116"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:38:53"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29110 -->
Peter Krijnen + Anton Janssen