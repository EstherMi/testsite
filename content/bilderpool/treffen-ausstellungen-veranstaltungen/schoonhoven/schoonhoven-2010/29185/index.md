---
layout: "image"
title: "FT-Treffen-Schoonhoven-Thema"
date: "2010-11-06T23:40:15"
picture: "fischertechnikbijeenkomstschoonhovennov79.jpg"
weight: "40"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/29185
imported:
- "2019"
_4images_image_id: "29185"
_4images_cat_id: "2116"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:40:15"
_4images_image_order: "79"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29185 -->
FT-Treffen-Schoonhoven-Thema