---
layout: "image"
title: "Peter Krijnen + Anton Janssen"
date: "2010-11-06T23:38:53"
picture: "fischertechnikbijeenkomstschoonhovennov05.jpg"
weight: "5"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/29111
imported:
- "2019"
_4images_image_id: "29111"
_4images_cat_id: "2116"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:38:53"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29111 -->
Peter Krijnen + Anton Janssen