---
layout: "image"
title: "Kubus Rob Tovenaar"
date: "2010-11-06T23:39:27"
picture: "fischertechnikbijeenkomstschoonhovennov35.jpg"
weight: "4"
konstrukteure: 
- "Rob Tovenaar"
fotografen:
- "peterholland"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/29141
imported:
- "2019"
_4images_image_id: "29141"
_4images_cat_id: "2343"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:39:27"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29141 -->
Kubus Rob Tovenaar..........Model van de dag