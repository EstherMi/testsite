---
layout: "image"
title: "Kubus Rob Tovenaar"
date: "2010-11-06T23:39:45"
picture: "fischertechnikbijeenkomstschoonhovennov47.jpg"
weight: "16"
konstrukteure: 
- "Rob Tovenaar"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/29153
imported:
- "2019"
_4images_image_id: "29153"
_4images_cat_id: "2343"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:39:45"
_4images_image_order: "47"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29153 -->
Kubus Rob Tovenaar