---
layout: "image"
title: "Kubus Rob Tovenaar"
date: "2010-11-06T23:39:27"
picture: "fischertechnikbijeenkomstschoonhovennov32.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/29138
imported:
- "2019"
_4images_image_id: "29138"
_4images_cat_id: "2343"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:39:27"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29138 -->
Kubus Rob Tovenaar