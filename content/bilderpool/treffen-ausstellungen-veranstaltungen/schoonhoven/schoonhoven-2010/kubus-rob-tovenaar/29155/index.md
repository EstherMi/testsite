---
layout: "image"
title: "Kubus Rob Tovenaar"
date: "2010-11-06T23:39:46"
picture: "fischertechnikbijeenkomstschoonhovennov49.jpg"
weight: "18"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/29155
imported:
- "2019"
_4images_image_id: "29155"
_4images_cat_id: "2343"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:39:46"
_4images_image_order: "49"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29155 -->
Kubus Rob Tovenaar