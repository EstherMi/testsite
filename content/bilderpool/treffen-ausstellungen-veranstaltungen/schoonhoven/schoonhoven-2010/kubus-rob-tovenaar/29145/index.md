---
layout: "image"
title: "Kubus Rob Tovenaar"
date: "2010-11-06T23:39:28"
picture: "fischertechnikbijeenkomstschoonhovennov39.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/29145
imported:
- "2019"
_4images_image_id: "29145"
_4images_cat_id: "2343"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:39:28"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29145 -->
Kubus Rob Tovenaar