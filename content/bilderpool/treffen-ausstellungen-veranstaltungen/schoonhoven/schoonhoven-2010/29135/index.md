---
layout: "image"
title: "Rob Tovenaar"
date: "2010-11-06T23:39:19"
picture: "fischertechnikbijeenkomstschoonhovennov29.jpg"
weight: "29"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/29135
imported:
- "2019"
_4images_image_id: "29135"
_4images_cat_id: "2116"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:39:19"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29135 -->
Rob Tovenaar