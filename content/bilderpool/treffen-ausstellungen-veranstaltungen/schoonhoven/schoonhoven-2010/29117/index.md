---
layout: "image"
title: "Peter Krijnen + Anton Janssen"
date: "2010-11-06T23:39:09"
picture: "fischertechnikbijeenkomstschoonhovennov11.jpg"
weight: "11"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/29117
imported:
- "2019"
_4images_image_id: "29117"
_4images_cat_id: "2116"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:39:09"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29117 -->
Peter Krijnen + Anton Janssen