---
layout: "image"
title: "Herman Mels"
date: "2010-11-06T23:39:19"
picture: "fischertechnikbijeenkomstschoonhovennov21.jpg"
weight: "21"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/29127
imported:
- "2019"
_4images_image_id: "29127"
_4images_cat_id: "2116"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:39:19"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29127 -->
Herman Mels