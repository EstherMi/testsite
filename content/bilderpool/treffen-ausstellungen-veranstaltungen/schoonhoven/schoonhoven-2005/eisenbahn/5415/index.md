---
layout: "image"
title: "Bahn46.JPG"
date: "2005-11-28T18:56:15"
picture: "Bahn46.JPG"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
keywords: ["Gleis", "Bahn", "Lokomotive"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5415
imported:
- "2019"
_4images_image_id: "5415"
_4images_cat_id: "469"
_4images_user_id: "4"
_4images_image_date: "2005-11-28T18:56:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5415 -->
