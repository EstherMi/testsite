---
layout: "image"
title: "Bahn47.JPG"
date: "2005-11-28T18:56:35"
picture: "Bahn47.JPG"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5416
imported:
- "2019"
_4images_image_id: "5416"
_4images_cat_id: "469"
_4images_user_id: "4"
_4images_image_date: "2005-11-28T18:56:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5416 -->
