---
layout: "image"
title: "Photo interruptor:  sawing"
date: "2005-11-25T12:08:11"
picture: "DSCF0039a.jpg"
weight: "4"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Paul van Niekerk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "niekerk"
license: "unknown"
legacy_id:
- details/5404
imported:
- "2019"
_4images_image_id: "5404"
_4images_cat_id: "582"
_4images_user_id: "385"
_4images_image_date: "2005-11-25T12:08:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5404 -->
With a small metal saw, the electrical contacts are separated.