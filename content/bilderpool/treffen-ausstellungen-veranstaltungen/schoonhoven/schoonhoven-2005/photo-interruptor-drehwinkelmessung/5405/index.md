---
layout: "image"
title: "Photo interruptor soldered"
date: "2005-11-25T12:08:11"
picture: "DSCF0040a.jpg"
weight: "5"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Paul van Niekerk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "niekerk"
license: "unknown"
legacy_id:
- details/5405
imported:
- "2019"
_4images_image_id: "5405"
_4images_cat_id: "582"
_4images_user_id: "385"
_4images_image_date: "2005-11-25T12:08:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5405 -->
Soldering must be done using a rather small soldering tool, but is not difficult.