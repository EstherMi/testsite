---
layout: "image"
title: "Ruderboot.JPG"
date: "2005-11-28T19:02:32"
picture: "Ruderboot.JPG"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5418
imported:
- "2019"
_4images_image_id: "5418"
_4images_cat_id: "436"
_4images_user_id: "4"
_4images_image_date: "2005-11-28T19:02:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5418 -->
Klein und fein und einfach hübsch anzusehen. Ich meine allerdings, dass der Steuermann am falschen Ende sitzt.