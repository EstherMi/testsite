---
layout: "image"
title: "Troja03.JPG"
date: "2005-11-07T20:15:10"
picture: "Troja03.JPG"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5273
imported:
- "2019"
_4images_image_id: "5273"
_4images_cat_id: "452"
_4images_user_id: "4"
_4images_image_date: "2005-11-07T20:15:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5273 -->
