---
layout: "image"
title: "Troja01.JPG"
date: "2005-11-07T20:14:35"
picture: "Troja01.JPG"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
keywords: ["Trojan", "Horse"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5272
imported:
- "2019"
_4images_image_id: "5272"
_4images_cat_id: "452"
_4images_user_id: "4"
_4images_image_date: "2005-11-07T20:14:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5272 -->
Ein gelungenes Beispiel dafür, dass man mit fischertechnik auch weniger "technische" Sachen bauen kann. Außerdem ist sehr schön zu sehen, dass man mit ft-Statik nicht an rechte Winkel gebunden ist.