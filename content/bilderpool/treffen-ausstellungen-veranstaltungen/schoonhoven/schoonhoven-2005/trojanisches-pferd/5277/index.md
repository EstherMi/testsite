---
layout: "image"
title: "Troja06.JPG"
date: "2005-11-07T20:19:14"
picture: "Troja06.JPG"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5277
imported:
- "2019"
_4images_image_id: "5277"
_4images_cat_id: "452"
_4images_user_id: "4"
_4images_image_date: "2005-11-07T20:19:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5277 -->
