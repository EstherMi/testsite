---
layout: "image"
title: "Troja05.JPG"
date: "2005-11-07T20:18:38"
picture: "Troja05.JPG"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5276
imported:
- "2019"
_4images_image_id: "5276"
_4images_cat_id: "452"
_4images_user_id: "4"
_4images_image_date: "2005-11-07T20:18:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5276 -->
