---
layout: "image"
title: "Capriolo-141.JPG"
date: "2005-11-06T19:33:22"
picture: "P9120141.JPG"
weight: "2"
konstrukteure: 
- "Fa. Mondial"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5235
imported:
- "2019"
_4images_image_id: "5235"
_4images_cat_id: "439"
_4images_user_id: "4"
_4images_image_date: "2005-11-06T19:33:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5235 -->
Der Capriolo "Cyber Space", aufgebaut auf dem Münchner Oktoberfest 2005.