---
layout: "image"
title: "Capriolo-CS612.JPG"
date: "2005-11-06T19:33:21"
picture: "CS612.JPG"
weight: "1"
konstrukteure: 
- "Fa. Mondial"
fotografen:
- "Harald Steinhaus"
keywords: ["Fahrgeschäft", "Ride"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5234
imported:
- "2019"
_4images_image_id: "5234"
_4images_cat_id: "439"
_4images_user_id: "4"
_4images_image_date: "2005-11-06T19:33:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5234 -->
Das ist der Mittelbau eines "Capriolo" der Firma Mondial, fertig zum Transport (München, Oktoberfest  2004).