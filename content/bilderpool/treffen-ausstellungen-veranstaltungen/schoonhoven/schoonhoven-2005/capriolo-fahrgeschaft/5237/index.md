---
layout: "image"
title: "Capriolo60.JPG"
date: "2005-11-06T19:33:22"
picture: "Capriolo60.JPG"
weight: "4"
konstrukteure: 
- "Jan-Willem Dekker"
fotografen:
- "Harald Steinhaus"
keywords: ["Fahrgeschäft", "Rides"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5237
imported:
- "2019"
_4images_image_id: "5237"
_4images_cat_id: "439"
_4images_user_id: "4"
_4images_image_date: "2005-11-06T19:33:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5237 -->
