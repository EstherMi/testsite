---
layout: "image"
title: "Schoonhoven 06"
date: "2005-11-06T19:14:54"
picture: "Schoon_06.jpg"
weight: "16"
konstrukteure: 
- "Peter Damen"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- details/5231
imported:
- "2019"
_4images_image_id: "5231"
_4images_cat_id: "453"
_4images_user_id: "10"
_4images_image_date: "2005-11-06T19:14:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5231 -->
