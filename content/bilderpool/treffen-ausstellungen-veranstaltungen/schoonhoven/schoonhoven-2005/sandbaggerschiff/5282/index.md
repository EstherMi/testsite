---
layout: "image"
title: "HopperDredge04.JPG"
date: "2005-11-10T22:06:19"
picture: "HopperDredge04.JPG"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5282
imported:
- "2019"
_4images_image_id: "5282"
_4images_cat_id: "453"
_4images_user_id: "4"
_4images_image_date: "2005-11-10T22:06:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5282 -->
