---
layout: "image"
title: "Autofabrik01.JPG"
date: "2005-11-06T20:49:38"
picture: "Autofabrik01.JPG"
weight: "1"
konstrukteure: 
- "Manfred Busch"
fotografen:
- "Harald Steinhaus"
keywords: ["Industrieanlagen", "Autofabrik"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5238
imported:
- "2019"
_4images_image_id: "5238"
_4images_cat_id: "441"
_4images_user_id: "4"
_4images_image_date: "2005-11-06T20:49:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5238 -->
Aus dem Styroporblock links wird demnächst ein Auto. Die Palette ist durch Sein oder Nicht-Sein von BS15-Loch an bestimmten Stellen codiert und legt dadurch fest, welches Fahrzeugmodell entstehen wird. Die Karosserieform entsteht durch die Bahn, die ein elektrisch beheizter Schneidedraht im Styroporblock nimmt.