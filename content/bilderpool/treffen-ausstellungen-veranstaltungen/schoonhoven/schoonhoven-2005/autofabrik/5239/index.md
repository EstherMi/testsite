---
layout: "image"
title: "Autofabrik02.JPG"
date: "2005-11-06T20:51:09"
picture: "Autofabrik02.JPG"
weight: "2"
konstrukteure: 
- "Manfred Busch"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5239
imported:
- "2019"
_4images_image_id: "5239"
_4images_cat_id: "441"
_4images_user_id: "4"
_4images_image_date: "2005-11-06T20:51:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5239 -->
Der Block fährt demnächst in die Schneidestation ein. Die Position wird durch Lichtschranken mit Laserdioden ermittelt.