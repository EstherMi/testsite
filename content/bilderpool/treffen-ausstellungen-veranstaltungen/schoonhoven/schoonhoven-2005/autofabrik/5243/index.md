---
layout: "image"
title: "Autofabrik06.JPG"
date: "2005-11-06T21:00:37"
picture: "Autofabrik06.JPG"
weight: "6"
konstrukteure: 
- "Manfred Busch"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5243
imported:
- "2019"
_4images_image_id: "5243"
_4images_cat_id: "441"
_4images_user_id: "4"
_4images_image_date: "2005-11-06T21:00:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5243 -->
Die hinteren Radkästen sind jetzt auch ausgeschnitten. Die Karosserie ist es schon längst, nur das Abfallteil liegt noch obendrauf.