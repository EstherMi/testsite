---
layout: "image"
title: "Autofabrik20.JPG"
date: "2005-11-06T21:05:50"
picture: "Autofabrik20.JPG"
weight: "11"
konstrukteure: 
- "Manfred Busch"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5248
imported:
- "2019"
_4images_image_id: "5248"
_4images_cat_id: "441"
_4images_user_id: "4"
_4images_image_date: "2005-11-06T21:05:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5248 -->
Die Laser sind die gleichen wie in gewöhnlichen Laserpointern, nur in ft-Gehäuse um-montiert.