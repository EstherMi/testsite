---
layout: "image"
title: "Autofabrik07.JPG"
date: "2005-11-06T21:01:18"
picture: "Autofabrik07.JPG"
weight: "7"
konstrukteure: 
- "Manfred Busch"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5244
imported:
- "2019"
_4images_image_id: "5244"
_4images_cat_id: "441"
_4images_user_id: "4"
_4images_image_date: "2005-11-06T21:01:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5244 -->
