---
layout: "image"
title: "Die Roboter von Huub"
date: "2008-12-12T22:53:58"
picture: "IMG_1094.jpg"
weight: "1"
konstrukteure: 
- "Huub van Niekerk"
fotografen:
- "Paul van Niekerk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "niekerk"
license: "unknown"
legacy_id:
- details/16581
imported:
- "2019"
_4images_image_id: "16581"
_4images_cat_id: "1460"
_4images_user_id: "385"
_4images_image_date: "2008-12-12T22:53:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16581 -->
