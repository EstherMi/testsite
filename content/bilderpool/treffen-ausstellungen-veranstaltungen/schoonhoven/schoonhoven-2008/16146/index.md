---
layout: "image"
title: "Anton Jansen   Telescoopkraan"
date: "2008-11-01T22:33:44"
picture: "FT-Schoonhoven-2008_060.jpg"
weight: "56"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/16146
imported:
- "2019"
_4images_image_id: "16146"
_4images_cat_id: "1460"
_4images_user_id: "22"
_4images_image_date: "2008-11-01T22:33:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16146 -->
