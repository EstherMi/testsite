---
layout: "image"
title: "Verschiedene Roboter"
date: "2008-12-12T22:53:58"
picture: "IMG_1095.jpg"
weight: "2"
konstrukteure: 
- "Huub & Paul van Niekerk"
fotografen:
- "Paul van Niekerk"
keywords: ["Roborama", "servo", "robot", "contest"]
uploadBy: "niekerk"
license: "unknown"
legacy_id:
- details/16582
imported:
- "2019"
_4images_image_id: "16582"
_4images_cat_id: "1460"
_4images_user_id: "385"
_4images_image_date: "2008-12-12T22:53:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16582 -->
Here's a Mini Sumo Robot "Pokémon" and a Roborama Robot "T-rex", both for contests. Note that for the mini sumo, there's strict requirements on the size (10 x 10 cm) and weight (500 gram) of the robot. Also shown is a track driven by the digital Dynamixel servos. In the background a walking robot under construction.