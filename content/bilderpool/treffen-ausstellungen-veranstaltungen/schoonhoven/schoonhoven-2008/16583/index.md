---
layout: "image"
title: "Dynamixel AX12+ in combination with fischertechnik"
date: "2008-12-12T22:53:59"
picture: "IMG_1096.jpg"
weight: "3"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Paul van Niekerk"
keywords: ["Dynamixel", "AX12+", "servo", "AVR", "Butterfly", "robot"]
uploadBy: "niekerk"
license: "unknown"
legacy_id:
- details/16583
imported:
- "2019"
_4images_image_id: "16583"
_4images_cat_id: "1460"
_4images_user_id: "385"
_4images_image_date: "2008-12-12T22:53:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16583 -->
The Dynamixel AX12+ is a new digital servo. The easiest way to combine this with fischertechnik is shown in the picture. The servo is controlled via a bi-directional half duplex serial bus by the AVR Butterfly controller.