---
layout: "image"
title: "Robot with AVR Butterfly"
date: "2008-12-12T22:53:59"
picture: "IMG_1097.jpg"
weight: "4"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Paul van Niekerk"
keywords: ["Robot", "AVR", "butterfly", "Dynamixel", "AX12+", "servo"]
uploadBy: "niekerk"
license: "unknown"
legacy_id:
- details/16584
imported:
- "2019"
_4images_image_id: "16584"
_4images_cat_id: "1460"
_4images_user_id: "385"
_4images_image_date: "2008-12-12T22:53:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16584 -->
The AVR Butterfly contains an 8 bit AVR Atmega169 controller from Atmel, a display, a mini joystick, and more. It controls two Dynamixel AX12+ digital servos from its USI port.