---
layout: "image"
title: "Clemens Jansen Around the World"
date: "2008-11-01T22:33:44"
picture: "FT-Schoonhoven-2008_028.jpg"
weight: "9"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/16099
imported:
- "2019"
_4images_image_id: "16099"
_4images_cat_id: "1460"
_4images_user_id: "22"
_4images_image_date: "2008-11-01T22:33:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16099 -->
