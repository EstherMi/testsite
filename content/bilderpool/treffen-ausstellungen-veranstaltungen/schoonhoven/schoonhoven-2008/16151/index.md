---
layout: "image"
title: "Familie Dijkstra  Flugzeuge"
date: "2008-11-01T22:33:45"
picture: "FT-Schoonhoven-2008_063_2.jpg"
weight: "61"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/16151
imported:
- "2019"
_4images_image_id: "16151"
_4images_cat_id: "1460"
_4images_user_id: "22"
_4images_image_date: "2008-11-01T22:33:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16151 -->
