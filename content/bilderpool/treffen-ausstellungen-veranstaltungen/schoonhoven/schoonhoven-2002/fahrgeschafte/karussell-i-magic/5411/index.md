---
layout: "image"
title: "Magic804.JPG"
date: "2005-11-28T17:16:39"
picture: "Magic804.JPG"
weight: "2"
konstrukteure: 
- "Fa. Huss"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5411
imported:
- "2019"
_4images_image_id: "5411"
_4images_cat_id: "79"
_4images_user_id: "4"
_4images_image_date: "2005-11-28T17:16:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5411 -->
