---
layout: "image"
title: "DSC00636"
date: "2003-04-26T15:55:55"
picture: "DSC00636.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "n.b."
keywords: ["Marionette", "Handpuppe"]
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/659
imported:
- "2019"
_4images_image_id: "659"
_4images_cat_id: "75"
_4images_user_id: "1"
_4images_image_date: "2003-04-26T15:55:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=659 -->
