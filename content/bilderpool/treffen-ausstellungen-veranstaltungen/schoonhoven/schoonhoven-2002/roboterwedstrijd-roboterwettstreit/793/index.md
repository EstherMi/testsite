---
layout: "image"
title: "DCP 2482"
date: "2003-04-27T13:07:39"
picture: "DCP_2482.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/793
imported:
- "2019"
_4images_image_id: "793"
_4images_cat_id: "85"
_4images_user_id: "1"
_4images_image_date: "2003-04-27T13:07:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=793 -->
