---
layout: "image"
title: "photo61"
date: "2004-11-16T09:16:34"
picture: "photo61.jpg"
weight: "4"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen (?)"
keywords: ["Schnecke", "35977"]
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/3206
imported:
- "2019"
_4images_image_id: "3206"
_4images_cat_id: "277"
_4images_user_id: "1"
_4images_image_date: "2004-11-16T09:16:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3206 -->
