---
layout: "comment"
hidden: true
title: "328"
date: "2004-11-16T15:03:03"
uploadBy:
- "DerMitDenBitsTanzt"
license: "unknown"
imported:
- "2019"
---
Parkstellung des Roboters für den RücktransportAufrecht passt das Ungetüm nicht in den Kofferraum, also wird es auf dem Fahrgestell zur Ruhe gebettet.