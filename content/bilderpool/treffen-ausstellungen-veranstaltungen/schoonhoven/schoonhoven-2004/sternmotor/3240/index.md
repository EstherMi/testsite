---
layout: "image"
title: "frank - 19"
date: "2004-11-18T17:20:27"
picture: "frank - 19.jpg"
weight: "1"
konstrukteure: 
- "Ronald van Ewijk"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/3240
imported:
- "2019"
_4images_image_id: "3240"
_4images_cat_id: "300"
_4images_user_id: "9"
_4images_image_date: "2004-11-18T17:20:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3240 -->
