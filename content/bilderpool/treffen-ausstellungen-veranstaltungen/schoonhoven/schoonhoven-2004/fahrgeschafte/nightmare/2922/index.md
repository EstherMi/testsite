---
layout: "image"
title: "DSC00978"
date: "2004-11-08T20:46:43"
picture: "DSC00978.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "n.b."
keywords: ["Fahrgeschäfte", "Kirmes"]
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/2922
imported:
- "2019"
_4images_image_id: "2922"
_4images_cat_id: "289"
_4images_user_id: "9"
_4images_image_date: "2004-11-08T20:46:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2922 -->
