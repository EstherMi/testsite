---
layout: "image"
title: "frank - 64"
date: "2004-11-18T17:20:27"
picture: "frank - 64.jpg"
weight: "6"
konstrukteure: 
- "Jan-Willem Dekker"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/3285
imported:
- "2019"
_4images_image_id: "3285"
_4images_cat_id: "306"
_4images_user_id: "9"
_4images_image_date: "2004-11-18T17:20:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3285 -->
