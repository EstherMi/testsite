---
layout: "image"
title: "Starlift08.JPG"
date: "2003-11-11T20:17:16"
picture: "Starlift08.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/1990
imported:
- "2019"
_4images_image_id: "1990"
_4images_cat_id: "411"
_4images_user_id: "4"
_4images_image_date: "2003-11-11T20:17:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1990 -->
Die Zylinder sind erstens als Notbremse da, zweitens öffnen sie auch die Haltebügel für die Fahrgäste, wenn die Gondel ganz abgesenkt wird.