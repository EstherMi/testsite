---
layout: "image"
title: "Starlift06.JPG"
date: "2003-11-11T20:15:27"
picture: "Starlift06.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/1986
imported:
- "2019"
_4images_image_id: "1986"
_4images_cat_id: "411"
_4images_user_id: "4"
_4images_image_date: "2003-11-11T20:15:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1986 -->
