---
layout: "image"
title: "Starlift01.JPG"
date: "2003-11-11T20:15:27"
picture: "Starlift01.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/1984
imported:
- "2019"
_4images_image_id: "1984"
_4images_cat_id: "411"
_4images_user_id: "4"
_4images_image_date: "2003-11-11T20:15:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1984 -->
