---
layout: "image"
title: "S-Bagg06.JPG"
date: "2003-11-10T21:10:18"
picture: "S-Bagg06.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/1947
imported:
- "2019"
_4images_image_id: "1947"
_4images_cat_id: "413"
_4images_user_id: "4"
_4images_image_date: "2003-11-10T21:10:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1947 -->
Lenkung: Die oberen Kettenfahrgestelle werden durch den Schneckentrieb (unten horizontal) nach innen/außen geschwenkt.