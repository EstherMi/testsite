---
layout: "image"
title: "S-Bagg10.JPG"
date: "2003-11-10T21:10:18"
picture: "S-Bagg10.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/1950
imported:
- "2019"
_4images_image_id: "1950"
_4images_cat_id: "413"
_4images_user_id: "4"
_4images_image_date: "2003-11-10T21:10:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1950 -->
Oberes und unteres Förderband lassen sich teleskopartig gegeneinander verschieben. Vom oberen sieht man hier nur einen Achsstummel in Bildmitte rechts.