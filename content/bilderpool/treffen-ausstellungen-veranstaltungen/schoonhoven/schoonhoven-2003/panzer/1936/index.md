---
layout: "image"
title: "Panzer02.JPG"
date: "2003-11-10T21:00:25"
picture: "Panzer02.jpg"
weight: "2"
konstrukteure: 
- "Ronald van Ewijk"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/1936
imported:
- "2019"
_4images_image_id: "1936"
_4images_cat_id: "415"
_4images_user_id: "4"
_4images_image_date: "2003-11-10T21:00:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1936 -->
