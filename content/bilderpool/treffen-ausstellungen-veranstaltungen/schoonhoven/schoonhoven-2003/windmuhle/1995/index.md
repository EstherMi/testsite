---
layout: "image"
title: "WindM04.JPG"
date: "2003-11-11T20:17:16"
picture: "WindM04.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/1995
imported:
- "2019"
_4images_image_id: "1995"
_4images_cat_id: "409"
_4images_user_id: "4"
_4images_image_date: "2003-11-11T20:17:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1995 -->
Der Kollergang dient hier dazu, die Sägeblätter über Kurbelwellen anzutreiben. Das Differenzial treibt die Förderbänder für die Holzbalken an.