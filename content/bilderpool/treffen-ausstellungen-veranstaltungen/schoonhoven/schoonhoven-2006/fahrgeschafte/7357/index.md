---
layout: "image"
title: "fischertechnikschoonh41.jpg"
date: "2006-11-04T22:49:58"
picture: "fischertechnikschoonh41.jpg"
weight: "14"
konstrukteure: 
- "Frits Roller"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/7357
imported:
- "2019"
_4images_image_id: "7357"
_4images_cat_id: "1123"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:58"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7357 -->
