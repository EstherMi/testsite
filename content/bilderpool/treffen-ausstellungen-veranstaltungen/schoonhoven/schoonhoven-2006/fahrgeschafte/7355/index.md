---
layout: "image"
title: "fischertechnikschoonh39.jpg"
date: "2006-11-04T22:49:58"
picture: "fischertechnikschoonh39.jpg"
weight: "13"
konstrukteure: 
- "Frits Roller"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/7355
imported:
- "2019"
_4images_image_id: "7355"
_4images_cat_id: "1123"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:58"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7355 -->
