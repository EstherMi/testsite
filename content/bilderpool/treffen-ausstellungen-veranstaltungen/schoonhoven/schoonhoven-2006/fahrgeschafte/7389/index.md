---
layout: "image"
title: "fischertechnikschoonh73.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh73.jpg"
weight: "16"
konstrukteure: 
- "Clemens Jansen"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/7389
imported:
- "2019"
_4images_image_id: "7389"
_4images_cat_id: "1123"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "73"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7389 -->
