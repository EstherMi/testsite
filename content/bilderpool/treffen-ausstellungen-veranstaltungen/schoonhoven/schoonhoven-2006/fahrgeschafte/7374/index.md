---
layout: "image"
title: "fischertechnikschoonh58.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh58.jpg"
weight: "15"
konstrukteure: 
- "Michel Schouten"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/7374
imported:
- "2019"
_4images_image_id: "7374"
_4images_cat_id: "1123"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "58"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7374 -->
