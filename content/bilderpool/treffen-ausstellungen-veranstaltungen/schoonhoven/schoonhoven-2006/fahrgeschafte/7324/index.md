---
layout: "image"
title: "fischertechnikschoonh08.jpg"
date: "2006-11-04T22:49:45"
picture: "fischertechnikschoonh08.jpg"
weight: "1"
konstrukteure: 
- "W. Brickwedde"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/7324
imported:
- "2019"
_4images_image_id: "7324"
_4images_cat_id: "1123"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:45"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7324 -->
