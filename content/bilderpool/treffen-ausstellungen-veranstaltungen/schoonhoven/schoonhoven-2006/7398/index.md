---
layout: "image"
title: "fischertechnikschoonh82.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh82.jpg"
weight: "58"
konstrukteure: 
- "W. Brickwedde"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/7398
imported:
- "2019"
_4images_image_id: "7398"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "82"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7398 -->
