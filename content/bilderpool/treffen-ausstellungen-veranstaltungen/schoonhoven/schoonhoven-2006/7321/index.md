---
layout: "image"
title: "fischertechnikschoonh05.jpg"
date: "2006-11-04T22:49:45"
picture: "fischertechnikschoonh05.jpg"
weight: "15"
konstrukteure: 
- "Max Buiting"
fotografen:
- "Peter Damen"
keywords: ["Speichenrad"]
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/7321
imported:
- "2019"
_4images_image_id: "7321"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:45"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7321 -->
