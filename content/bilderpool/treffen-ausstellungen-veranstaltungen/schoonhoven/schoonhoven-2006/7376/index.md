---
layout: "image"
title: "fischertechnikschoonh60.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh60.jpg"
weight: "43"
konstrukteure: 
- "Andries Tieleman"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/7376
imported:
- "2019"
_4images_image_id: "7376"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "60"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7376 -->
