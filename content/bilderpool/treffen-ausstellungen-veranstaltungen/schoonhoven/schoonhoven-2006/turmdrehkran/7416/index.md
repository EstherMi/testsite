---
layout: "image"
title: "Turmdrehkran"
date: "2006-11-06T17:16:53"
picture: "schoonhovenmw06.jpg"
weight: "4"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "MisterWho (Joachim Jacobi)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- details/7416
imported:
- "2019"
_4images_image_id: "7416"
_4images_cat_id: "1124"
_4images_user_id: "8"
_4images_image_date: "2006-11-06T17:16:53"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7416 -->
