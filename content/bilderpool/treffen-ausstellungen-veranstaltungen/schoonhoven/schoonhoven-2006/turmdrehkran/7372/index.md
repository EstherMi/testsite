---
layout: "image"
title: "fischertechnikschoonh56.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh56.jpg"
weight: "10"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/7372
imported:
- "2019"
_4images_image_id: "7372"
_4images_cat_id: "1124"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "56"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7372 -->
