---
layout: "image"
title: "fischertechnikschoonh57.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh57.jpg"
weight: "11"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/7373
imported:
- "2019"
_4images_image_id: "7373"
_4images_cat_id: "1124"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "57"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7373 -->
