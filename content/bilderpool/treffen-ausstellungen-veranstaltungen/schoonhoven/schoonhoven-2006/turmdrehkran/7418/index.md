---
layout: "image"
title: "Turmdrehkran"
date: "2006-11-06T17:16:53"
picture: "schoonhovenmw08.jpg"
weight: "6"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "MisterWho (Joachim Jacobi)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- details/7418
imported:
- "2019"
_4images_image_id: "7418"
_4images_cat_id: "1124"
_4images_user_id: "8"
_4images_image_date: "2006-11-06T17:16:53"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7418 -->
Zwei Betonplatten, damit der Kran auch schön stabil steht