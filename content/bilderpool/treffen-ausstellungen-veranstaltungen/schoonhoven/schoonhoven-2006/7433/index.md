---
layout: "image"
title: "CD Player"
date: "2006-11-06T17:16:53"
picture: "schoonhovenmw23.jpg"
weight: "7"
konstrukteure: 
- "Jos van Baal"
fotografen:
- "MisterWho (Joachim Jacobi)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- details/7433
imported:
- "2019"
_4images_image_id: "7433"
_4images_cat_id: "701"
_4images_user_id: "8"
_4images_image_date: "2006-11-06T17:16:53"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7433 -->
Na ob da Töne rauskommen? ;-)