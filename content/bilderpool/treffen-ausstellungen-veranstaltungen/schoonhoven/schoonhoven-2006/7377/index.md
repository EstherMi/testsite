---
layout: "image"
title: "fischertechnikschoonh61.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh61.jpg"
weight: "44"
konstrukteure: 
- "Wim Starreveld"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/7377
imported:
- "2019"
_4images_image_id: "7377"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "61"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7377 -->
