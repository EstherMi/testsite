---
layout: "image"
title: "fischertechnikschoonh80.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh80.jpg"
weight: "56"
konstrukteure: 
- "W. Brickwedde"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/7396
imported:
- "2019"
_4images_image_id: "7396"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "80"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7396 -->
