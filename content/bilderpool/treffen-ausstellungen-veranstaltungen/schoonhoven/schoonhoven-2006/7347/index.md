---
layout: "image"
title: "fischertechnikschoonh31.jpg"
date: "2006-11-04T22:49:58"
picture: "fischertechnikschoonh31.jpg"
weight: "22"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/7347
imported:
- "2019"
_4images_image_id: "7347"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:58"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7347 -->
