---
layout: "image"
title: "fischertechnikschoonh21.jpg"
date: "2006-11-04T22:49:58"
picture: "fischertechnikschoonh21.jpg"
weight: "1"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/7337
imported:
- "2019"
_4images_image_id: "7337"
_4images_cat_id: "1126"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:58"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7337 -->
