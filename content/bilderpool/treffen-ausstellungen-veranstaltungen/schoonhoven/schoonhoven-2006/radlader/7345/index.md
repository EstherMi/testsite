---
layout: "image"
title: "fischertechnikschoonh29.jpg"
date: "2006-11-04T22:49:58"
picture: "fischertechnikschoonh29.jpg"
weight: "5"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/7345
imported:
- "2019"
_4images_image_id: "7345"
_4images_cat_id: "1126"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:58"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7345 -->
