---
layout: "image"
title: "Give Away Automat"
date: "2006-11-06T17:16:53"
picture: "schoonhovenmw20.jpg"
weight: "4"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "MisterWho (Joachim Jacobi)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- details/7430
imported:
- "2019"
_4images_image_id: "7430"
_4images_cat_id: "701"
_4images_user_id: "8"
_4images_image_date: "2006-11-06T17:16:53"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7430 -->
