---
layout: "image"
title: "fischertechnikschoonh49.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh49.jpg"
weight: "37"
konstrukteure: 
- "Herman Mels"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/7365
imported:
- "2019"
_4images_image_id: "7365"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "49"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7365 -->
