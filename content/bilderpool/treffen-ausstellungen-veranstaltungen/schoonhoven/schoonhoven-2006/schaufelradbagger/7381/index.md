---
layout: "image"
title: "fischertechnikschoonh65.jpg"
date: "2006-11-04T22:49:59"
picture: "fischertechnikschoonh65.jpg"
weight: "1"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/7381
imported:
- "2019"
_4images_image_id: "7381"
_4images_cat_id: "1128"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:59"
_4images_image_order: "65"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7381 -->
