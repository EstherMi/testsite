---
layout: "image"
title: "fischertechnikschoonh15.jpg"
date: "2006-11-04T22:49:58"
picture: "fischertechnikschoonh15.jpg"
weight: "19"
konstrukteure: 
- "Louis van Campen"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/7331
imported:
- "2019"
_4images_image_id: "7331"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:58"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7331 -->
