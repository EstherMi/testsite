---
layout: "image"
title: "fischertechnikschoonh11.jpg"
date: "2006-11-04T22:49:58"
picture: "fischertechnikschoonh11.jpg"
weight: "1"
konstrukteure: 
- "Louis van Campen"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/7327
imported:
- "2019"
_4images_image_id: "7327"
_4images_cat_id: "1127"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:58"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7327 -->
