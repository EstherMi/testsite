---
layout: "image"
title: "fischertechnikschoonh01.jpg"
date: "2006-11-04T22:49:45"
picture: "fischertechnikschoonh01.jpg"
weight: "11"
konstrukteure: 
- "Max Buiting"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/7317
imported:
- "2019"
_4images_image_id: "7317"
_4images_cat_id: "701"
_4images_user_id: "5"
_4images_image_date: "2006-11-04T22:49:45"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7317 -->
