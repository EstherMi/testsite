---
layout: "image"
title: "Autofabrik"
date: "2006-11-06T17:16:53"
picture: "schoonhovenmw18.jpg"
weight: "10"
konstrukteure: 
- "Frans Leurs"
fotografen:
- "MisterWho (Joachim Jacobi)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- details/7428
imported:
- "2019"
_4images_image_id: "7428"
_4images_cat_id: "1125"
_4images_user_id: "8"
_4images_image_date: "2006-11-06T17:16:53"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7428 -->
9.Station: Einbau der Frontscheibe. Fertig!