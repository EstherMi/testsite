---
layout: "image"
title: "Wettkampf-Version des Roboters"
date: "2015-02-11T08:20:05"
picture: "robocupqualifikationsturniermannheim10.jpg"
weight: "10"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/40527
imported:
- "2019"
_4images_image_id: "40527"
_4images_cat_id: "3036"
_4images_user_id: "1126"
_4images_image_date: "2015-02-11T08:20:05"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40527 -->
Team RoBoss