---
layout: "image"
title: "Wettkampf-Version des Roboters"
date: "2015-02-11T08:20:05"
picture: "robocupqualifikationsturniermannheim09.jpg"
weight: "9"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Dirk Fox"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dirk Fox"
license: "unknown"
legacy_id:
- details/40526
imported:
- "2019"
_4images_image_id: "40526"
_4images_cat_id: "3036"
_4images_user_id: "1126"
_4images_image_date: "2015-02-11T08:20:05"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40526 -->
Team RoBoss