---
layout: "overview"
title: "RoboCup 2015 - Qualifikationsturnier Mannheim"
date: 2019-12-17T18:33:23+01:00
legacy_id:
- categories/3036
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3036 --> 
Im Februar 2015 nahmen zwei Teams der fischertechnik-AG des Bismarck-Gymnasiums Karlsruhe am RoboCup-Qualifikationsturnier in Mannheim teil.
Zwar konnte sich keines für den Deutschland-Cup in Magdeburg qualifizieren - beide erreichten aber respektable Punktzahlen in ihren Wettläufen.