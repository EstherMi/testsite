---
layout: "image"
title: "Schritt für Schritt"
date: "2004-09-21T13:30:28"
picture: "Komm_Tanz_mit_mir.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Uwe Schmejkal"
license: "unknown"
legacy_id:
- details/2626
imported:
- "2019"
_4images_image_id: "2626"
_4images_cat_id: "266"
_4images_user_id: "54"
_4images_image_date: "2004-09-21T13:30:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2626 -->
