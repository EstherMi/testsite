---
layout: "image"
title: "Gesamt-B"
date: "2004-12-23T11:04:05"
picture: "Gesamt-B.JPG"
weight: "2"
konstrukteure: 
- "Thomas Habig"
fotografen:
- "Thomas Habig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/3426
imported:
- "2019"
_4images_image_id: "3426"
_4images_cat_id: "265"
_4images_user_id: "1"
_4images_image_date: "2004-12-23T11:04:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3426 -->
