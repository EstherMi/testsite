---
layout: "image"
title: "Solar-Hubschrauber"
date: "2004-09-29T19:02:19"
picture: "Ultra01.jpg"
weight: "10"
konstrukteure: 
- "Holger Howey"
fotografen:
- "-?-"
keywords: ["Solar", "Hubschrauber"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2652
imported:
- "2019"
_4images_image_id: "2652"
_4images_cat_id: "261"
_4images_user_id: "4"
_4images_image_date: "2004-09-29T19:02:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2652 -->
Ein bißchen Sonnenlicht, und schon hebt er ab.

Das Rotorblatt ist fest mit dem Motorgehäuse verbunden. Somit dreht sich der Motor mit; die Welle ist starr am Rumpf des Hubschraubers befestigt.