---
layout: "image"
title: "IMGP4815"
date: "2004-10-01T21:19:04"
picture: "IMGP4815.jpg"
weight: "6"
konstrukteure: 
- "Holger Howey"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/2675
imported:
- "2019"
_4images_image_id: "2675"
_4images_cat_id: "261"
_4images_user_id: "1"
_4images_image_date: "2004-10-01T21:19:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2675 -->
