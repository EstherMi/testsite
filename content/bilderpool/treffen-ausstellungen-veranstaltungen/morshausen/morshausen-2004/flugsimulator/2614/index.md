---
layout: "image"
title: "Der FLUSI"
date: "2004-09-21T13:30:08"
picture: "Der_3D_FLUSI.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Uwe Schmejkal"
license: "unknown"
legacy_id:
- details/2614
imported:
- "2019"
_4images_image_id: "2614"
_4images_cat_id: "250"
_4images_user_id: "54"
_4images_image_date: "2004-09-21T13:30:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2614 -->
