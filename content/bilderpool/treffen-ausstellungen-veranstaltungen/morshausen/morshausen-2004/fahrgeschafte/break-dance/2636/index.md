---
layout: "image"
title: "Unterbau Breakdancer"
date: "2004-09-21T15:55:05"
picture: "Unterbau_Break_Dance_ft01.jpg"
weight: "3"
konstrukteure: 
- "Fam. Brickwedde"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/2636
imported:
- "2019"
_4images_image_id: "2636"
_4images_cat_id: "246"
_4images_user_id: "130"
_4images_image_date: "2004-09-21T15:55:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2636 -->
Deutlich kann man hier den Unterbau vom Breakdance erkennen.