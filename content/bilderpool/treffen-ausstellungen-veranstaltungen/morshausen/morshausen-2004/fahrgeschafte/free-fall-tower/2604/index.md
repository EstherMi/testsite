---
layout: "image"
title: "Antriebsmotor vom Power Tower"
date: "2004-09-20T15:35:22"
picture: "Antrieb_Power_Tower_ft02.jpg"
weight: "15"
konstrukteure: 
- "Fam. Brickwedde"
fotografen:
- "Stephan Wenkers"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/2604
imported:
- "2019"
_4images_image_id: "2604"
_4images_cat_id: "247"
_4images_user_id: "130"
_4images_image_date: "2004-09-20T15:35:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2604 -->
Sehr kompakter Antrieb.