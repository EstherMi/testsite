---
layout: "image"
title: "pvd 093"
date: "2004-11-10T20:48:20"
picture: "pvd_093.jpg"
weight: "1"
konstrukteure: 
- "Fam. Brickwedde"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MarMac"
license: "unknown"
legacy_id:
- details/3069
imported:
- "2019"
_4images_image_id: "3069"
_4images_cat_id: "247"
_4images_user_id: "5"
_4images_image_date: "2004-11-10T20:48:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3069 -->
