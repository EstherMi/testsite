---
layout: "image"
title: "Der Powertower von Brickweddes"
date: "2004-09-21T15:55:05"
picture: "Power_Tower_Brickwedde_ft03.jpg"
weight: "13"
konstrukteure: 
- "Fam. Brickwedde"
fotografen:
- "Stephan Wenkers"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/2633
imported:
- "2019"
_4images_image_id: "2633"
_4images_cat_id: "247"
_4images_user_id: "130"
_4images_image_date: "2004-09-21T15:55:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2633 -->
Die Gondel in der Warteposition.