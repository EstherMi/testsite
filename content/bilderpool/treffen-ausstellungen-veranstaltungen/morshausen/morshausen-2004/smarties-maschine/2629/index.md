---
layout: "image"
title: "Viele, viele bunte Smarties"
date: "2004-09-21T13:30:28"
picture: "Smarties_aus_den_Niederlanden.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Uwe Schmejkal"
license: "unknown"
legacy_id:
- details/2629
imported:
- "2019"
_4images_image_id: "2629"
_4images_cat_id: "248"
_4images_user_id: "54"
_4images_image_date: "2004-09-21T13:30:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2629 -->
