---
layout: "image"
title: "Expertengespräche"
date: "2004-09-21T13:30:08"
picture: "Experten_unter_sich.jpg"
weight: "24"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Uwe Schmejkal"
license: "unknown"
legacy_id:
- details/2620
imported:
- "2019"
_4images_image_id: "2620"
_4images_cat_id: "263"
_4images_user_id: "54"
_4images_image_date: "2004-09-21T13:30:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2620 -->
