---
layout: "image"
title: "Die Eröffnung"
date: "2004-09-21T13:30:07"
picture: "Die_Erffnung.jpg"
weight: "23"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Uwe Schmejkal"
license: "unknown"
legacy_id:
- details/2612
imported:
- "2019"
_4images_image_id: "2612"
_4images_cat_id: "263"
_4images_user_id: "54"
_4images_image_date: "2004-09-21T13:30:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2612 -->
