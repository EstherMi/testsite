---
layout: "image"
title: "IMGP4821"
date: "2004-10-01T21:19:05"
picture: "IMGP4821.jpg"
weight: "1"
konstrukteure: 
- "Joachim Jakobi"
fotografen:
- "Joachim Jakobi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/2678
imported:
- "2019"
_4images_image_id: "2678"
_4images_cat_id: "258"
_4images_user_id: "1"
_4images_image_date: "2004-10-01T21:19:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2678 -->
