---
layout: "image"
title: "Das etwas andere Fahrgeschäft"
date: "2004-09-29T20:48:06"
picture: "CWL01.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2661
imported:
- "2019"
_4images_image_id: "2661"
_4images_cat_id: "255"
_4images_user_id: "4"
_4images_image_date: "2004-09-29T20:48:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2661 -->
Da kann ich nur sagen: sowas gibts auf dem Oktoberfest nicht. Bei den Fahrgeschäften dort dreht sich die Fahrgastkabine immer um -1- Achse, die vielleicht an einem Arm um eine andere dreht, welche möglicherweise ihrerseits an einem Arm um eine weitere dreht. Aber auf einer Bahn umlaufen tut dort außer den Geisterbahnen gar nichts.