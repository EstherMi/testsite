---
layout: "image"
title: "Rübenvollernter"
date: "2004-09-29T20:10:33"
picture: "Wwwwm04.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2659
imported:
- "2019"
_4images_image_id: "2659"
_4images_cat_id: "255"
_4images_user_id: "4"
_4images_image_date: "2004-09-29T20:10:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2659 -->
Mehr Details von der wrritze-wrrohren Wrroiwe-Wrropp-Maschin (for non-Germans: die knall-rote Rüben-Rupf-Maschine).

Blick von hinten auf die diversen Antriebe. Einfach *geil* wie das alles so ineinander paßt. Aber nicht jeder weiß, wieviel Mühe es kostet, so etwas auf die Beine zu stellen.