---
layout: "image"
title: "Rübenvollernter"
date: "2004-09-29T20:10:33"
picture: "Wwwwm03.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2658
imported:
- "2019"
_4images_image_id: "2658"
_4images_cat_id: "255"
_4images_user_id: "4"
_4images_image_date: "2004-09-29T20:10:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2658 -->
Mehr Details von der wrritze-wrrohren Wrroiwe-Wrropp-Maschin (for non-Germans: die knall-rote Rüben-Rupf-Maschine).

Die rechte Fahrzeugseite. Links der Krautbunker, rechts oben der Rübenbunker, darunter der Rodekopf; das Ganze umspannt von der Förderkette.