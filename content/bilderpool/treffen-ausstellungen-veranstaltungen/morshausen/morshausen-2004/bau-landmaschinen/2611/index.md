---
layout: "image"
title: "Ein schöner Bagger"
date: "2004-09-20T15:35:22"
picture: "Bagger_ft01.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/2611
imported:
- "2019"
_4images_image_id: "2611"
_4images_cat_id: "255"
_4images_user_id: "130"
_4images_image_date: "2004-09-20T15:35:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2611 -->
