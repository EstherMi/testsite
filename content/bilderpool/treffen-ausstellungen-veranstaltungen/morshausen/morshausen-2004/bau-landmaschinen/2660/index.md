---
layout: "image"
title: "Rübenvollernter"
date: "2004-09-29T20:48:06"
picture: "Wwwwm06.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2660
imported:
- "2019"
_4images_image_id: "2660"
_4images_cat_id: "255"
_4images_user_id: "4"
_4images_image_date: "2004-09-29T20:48:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2660 -->
Mehr Details von der wrritze-wrrohren Wrroiwe-Wrropp-Maschin (for non-Germans: die knall-rote Rüben-Rupf-Maschine).

Hier das Getriebe vorn links im Detail. Der Antrieb erfolgt über das Förderband (Kette rechts/unten). Die Pickup läuft anders herum als das Förderband, daher der ganze Aufwand.

--- Und am Ende kann die Pickup auch noch mittels Ratsche abgesenkt werden!? Die Fotos zeigen das nicht deutlich genug, und soo genau hab ich auch nicht hingeschaut.