---
layout: "image"
title: "Interface Testmodell"
date: "2004-09-21T15:55:05"
picture: "Testmodell_ft01.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/2637
imported:
- "2019"
_4images_image_id: "2637"
_4images_cat_id: "257"
_4images_user_id: "130"
_4images_image_date: "2004-09-21T15:55:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2637 -->
