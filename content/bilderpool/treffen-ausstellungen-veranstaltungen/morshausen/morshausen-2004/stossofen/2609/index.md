---
layout: "image"
title: "Der Brennofen"
date: "2004-09-20T15:35:22"
picture: "Brennofen_ft01.jpg"
weight: "12"
konstrukteure: 
- "U. Schmejkal"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/2609
imported:
- "2019"
_4images_image_id: "2609"
_4images_cat_id: "257"
_4images_user_id: "130"
_4images_image_date: "2004-09-20T15:35:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2609 -->
Schöne Details, sieht sehr gut aus.