---
layout: "image"
title: "Marmacs Projektionssystem"
date: "2007-09-16T22:07:26"
picture: "conventionsteffalk015.jpg"
weight: "15"
konstrukteure: 
- "marmac"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/11596
imported:
- "2019"
_4images_image_id: "11596"
_4images_cat_id: "1044"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:26"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11596 -->
Projektor (hinten) und Kamera (kleiner, davor)