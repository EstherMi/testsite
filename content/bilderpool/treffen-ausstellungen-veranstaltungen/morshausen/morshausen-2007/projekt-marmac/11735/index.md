---
layout: "image"
title: "rrb45.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb45.jpg"
weight: "7"
konstrukteure: 
- "MarMac"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/11735
imported:
- "2019"
_4images_image_id: "11735"
_4images_cat_id: "1044"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "45"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11735 -->
