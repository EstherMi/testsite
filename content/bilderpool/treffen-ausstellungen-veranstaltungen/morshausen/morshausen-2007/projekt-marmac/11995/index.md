---
layout: "image"
title: "Beamer und Kamera"
date: "2007-09-25T09:50:51"
picture: "touchpannel1.jpg"
weight: "1"
konstrukteure: 
- "Marmac"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/11995
imported:
- "2019"
_4images_image_id: "11995"
_4images_cat_id: "1044"
_4images_user_id: "127"
_4images_image_date: "2007-09-25T09:50:51"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11995 -->
