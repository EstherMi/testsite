---
layout: "image"
title: "rrb44.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb44.jpg"
weight: "6"
konstrukteure: 
- "MarMac"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/11734
imported:
- "2019"
_4images_image_id: "11734"
_4images_cat_id: "1044"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "44"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11734 -->
