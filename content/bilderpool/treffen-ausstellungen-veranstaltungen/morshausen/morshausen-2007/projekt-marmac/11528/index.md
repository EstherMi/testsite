---
layout: "image"
title: "Bildschirm"
date: "2007-09-16T17:09:08"
picture: "beamer1.jpg"
weight: "11"
konstrukteure: 
- "marmac"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/11528
imported:
- "2019"
_4images_image_id: "11528"
_4images_cat_id: "1044"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T17:09:08"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11528 -->
