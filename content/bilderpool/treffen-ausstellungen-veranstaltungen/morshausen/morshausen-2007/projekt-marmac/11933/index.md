---
layout: "image"
title: "touch133.JPG"
date: "2007-09-23T19:23:49"
picture: "touch133.JPG"
weight: "2"
konstrukteure: 
- "Marmac"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/11933
imported:
- "2019"
_4images_image_id: "11933"
_4images_cat_id: "1044"
_4images_user_id: "4"
_4images_image_date: "2007-09-23T19:23:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11933 -->
Beamer und Kamera, die das Bild senden und die Fingerposition ermitteln.