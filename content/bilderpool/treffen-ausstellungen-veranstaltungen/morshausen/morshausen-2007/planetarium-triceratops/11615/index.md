---
layout: "image"
title: "Planetarium"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk034.jpg"
weight: "9"
konstrukteure: 
- "Triceratops"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/11615
imported:
- "2019"
_4images_image_id: "11615"
_4images_cat_id: "1052"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "34"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11615 -->
Ein einfach nur wunderschönes Modell. Sogar der variable Abstand Sonne-Erde ist nachgebildet, weil der Mittelpunkt der Erde nicht über dem Drehpunkt des äußeren Teils liegt. Tolle Details: Ein Zahnrad Z7, bestehend aus 7 Kettengliedern um eine Rastkupplung, mit doppelseitigem Klebeband fixiert, und ein Z22, bestehend aus 22 Kettengliedern um ft-Naben. Alle Übersetzungsverhältnisse sind total fein ausgetüftelt - und dokumentiert! In Hintergrund Herr Jansen vor dem Tisch mit vielen Kirmesmodellen.