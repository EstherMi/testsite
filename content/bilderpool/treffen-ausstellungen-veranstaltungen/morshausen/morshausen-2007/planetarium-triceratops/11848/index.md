---
layout: "image"
title: "Z7 + Z22"
date: "2007-09-18T11:47:03"
picture: "PICT5620.jpg"
weight: "1"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/11848
imported:
- "2019"
_4images_image_id: "11848"
_4images_cat_id: "1052"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:47:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11848 -->
