---
layout: "image"
title: "rrb52.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb52.jpg"
weight: "3"
konstrukteure: 
- "Triceratops"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/11742
imported:
- "2019"
_4images_image_id: "11742"
_4images_cat_id: "1052"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "52"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11742 -->
