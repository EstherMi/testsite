---
layout: "image"
title: "Sitze"
date: "2007-09-16T19:38:31"
picture: "tower1.jpg"
weight: "7"
konstrukteure: 
- "fabse"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/11552
imported:
- "2019"
_4images_image_id: "11552"
_4images_cat_id: "1048"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T19:38:31"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11552 -->
