---
layout: "image"
title: "rrb14.jpg"
date: "2007-09-17T17:12:11"
picture: "rrb14.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "Richard R. Budding"
keywords: ["icare"]
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/11704
imported:
- "2019"
_4images_image_id: "11704"
_4images_cat_id: "1048"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:11"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11704 -->
