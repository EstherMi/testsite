---
layout: "image"
title: "Gondel"
date: "2007-09-16T19:38:31"
picture: "tower5.jpg"
weight: "11"
konstrukteure: 
- "fabse"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/11556
imported:
- "2019"
_4images_image_id: "11556"
_4images_cat_id: "1048"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T19:38:31"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11556 -->
