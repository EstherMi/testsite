---
layout: "image"
title: "Roboterarm"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk097.jpg"
weight: "17"
konstrukteure: 
- "Severin"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/11678
imported:
- "2019"
_4images_image_id: "11678"
_4images_cat_id: "1045"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "97"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11678 -->
