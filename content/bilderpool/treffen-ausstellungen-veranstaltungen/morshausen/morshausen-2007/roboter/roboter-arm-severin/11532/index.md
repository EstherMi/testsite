---
layout: "image"
title: "Greifarm"
date: "2007-09-16T17:09:08"
picture: "roboterarm02.jpg"
weight: "7"
konstrukteure: 
- "Severin"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/11532
imported:
- "2019"
_4images_image_id: "11532"
_4images_cat_id: "1045"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T17:09:08"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11532 -->
