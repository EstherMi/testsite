---
layout: "image"
title: "Dosen-Einsammel-Roboter"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk061.jpg"
weight: "17"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/11642
imported:
- "2019"
_4images_image_id: "11642"
_4images_cat_id: "1054"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "61"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11642 -->
Die Dose wird durch eine raffiniert einfache Mechanik ergriffen und oben in den Roboter eingeworfen.