---
layout: "image"
title: "Labyrinthroboter"
date: "2007-09-16T22:07:26"
picture: "conventionsteffalk028.jpg"
weight: "16"
konstrukteure: 
- "MisterWho"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/11609
imported:
- "2019"
_4images_image_id: "11609"
_4images_cat_id: "1054"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:26"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11609 -->
