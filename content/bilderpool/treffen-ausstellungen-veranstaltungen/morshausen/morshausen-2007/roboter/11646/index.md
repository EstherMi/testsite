---
layout: "image"
title: "Roboter auf großem Fuß"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk065.jpg"
weight: "21"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/11646
imported:
- "2019"
_4images_image_id: "11646"
_4images_cat_id: "1054"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "65"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11646 -->
Dieser Roboter fährt auf sehr großen Reifen und erkennt seine Umgebung über den außen verlaufenden Ring.