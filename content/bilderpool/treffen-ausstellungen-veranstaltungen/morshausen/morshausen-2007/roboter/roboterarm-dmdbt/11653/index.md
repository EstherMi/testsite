---
layout: "image"
title: "Federmännchen"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk072.jpg"
weight: "12"
konstrukteure: 
- "Tobias Linde"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/11653
imported:
- "2019"
_4images_image_id: "11653"
_4images_cat_id: "1035"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "72"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11653 -->
