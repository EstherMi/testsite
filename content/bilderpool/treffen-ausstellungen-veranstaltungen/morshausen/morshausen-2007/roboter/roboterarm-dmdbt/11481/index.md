---
layout: "image"
title: "Roboterarm"
date: "2007-09-16T16:53:32"
picture: "greifarm6.jpg"
weight: "11"
konstrukteure: 
- "Frank"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/11481
imported:
- "2019"
_4images_image_id: "11481"
_4images_cat_id: "1035"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T16:53:32"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11481 -->
