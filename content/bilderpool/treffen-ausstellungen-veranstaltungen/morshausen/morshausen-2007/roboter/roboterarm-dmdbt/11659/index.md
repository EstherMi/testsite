---
layout: "image"
title: "Roboterarm mit dezentraler Mikrocontroller-Steuerung"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk078.jpg"
weight: "18"
konstrukteure: 
- "dmdbt"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/11659
imported:
- "2019"
_4images_image_id: "11659"
_4images_cat_id: "1035"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "78"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11659 -->
