---
layout: "image"
title: "rrb09.jpg"
date: "2007-09-17T17:12:11"
picture: "rrb09.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/11699
imported:
- "2019"
_4images_image_id: "11699"
_4images_cat_id: "1054"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:11"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11699 -->
