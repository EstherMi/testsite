---
layout: "image"
title: "rrb71.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb71.jpg"
weight: "10"
konstrukteure: 
- "dmdbt"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/11761
imported:
- "2019"
_4images_image_id: "11761"
_4images_cat_id: "1054"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "71"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11761 -->
