---
layout: "image"
title: "Roboter"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk064.jpg"
weight: "20"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/11645
imported:
- "2019"
_4images_image_id: "11645"
_4images_cat_id: "1054"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "64"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11645 -->
Hier das Display mit der GUI. Der Mauszeiger steht auf dem mit "3" beschrifteten Feld links unten im Display und wird vom rechts erkennbaren Joystick gesteuert.