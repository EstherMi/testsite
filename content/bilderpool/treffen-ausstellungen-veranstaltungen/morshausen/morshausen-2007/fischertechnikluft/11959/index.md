---
layout: "image"
title: "Abendessen Samstag"
date: "2007-09-24T22:40:57"
picture: "abendessen2.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/11959
imported:
- "2019"
_4images_image_id: "11959"
_4images_cat_id: "1036"
_4images_user_id: "373"
_4images_image_date: "2007-09-24T22:40:57"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11959 -->
dmdbt, fabse, heiko, marmac (hinter dmdbt ist noch ein Stückchen ThanksForTheFish zu sehen)