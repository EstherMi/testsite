---
layout: "image"
title: "Abendessen Samstag"
date: "2007-09-24T22:40:57"
picture: "abendessen6.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/11963
imported:
- "2019"
_4images_image_id: "11963"
_4images_cat_id: "1036"
_4images_user_id: "373"
_4images_image_date: "2007-09-24T22:40:57"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11963 -->
Angelika und Stefan Falk