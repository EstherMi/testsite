---
layout: "image"
title: "Greifer-Spiel"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk103.jpg"
weight: "18"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/11684
imported:
- "2019"
_4images_image_id: "11684"
_4images_cat_id: "1066"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "103"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11684 -->
