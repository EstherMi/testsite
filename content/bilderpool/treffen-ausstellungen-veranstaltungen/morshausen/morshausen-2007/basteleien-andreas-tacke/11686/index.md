---
layout: "image"
title: "Geländewagen"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk105.jpg"
weight: "19"
konstrukteure: 
- "TST"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/11686
imported:
- "2019"
_4images_image_id: "11686"
_4images_cat_id: "1066"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "105"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11686 -->
Der war *sehr* geländegängig und kräftig!