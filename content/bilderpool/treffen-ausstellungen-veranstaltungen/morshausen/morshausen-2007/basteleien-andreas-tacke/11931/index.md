---
layout: "image"
title: "tst034.JPG"
date: "2007-09-23T19:13:55"
picture: "tst034.JPG"
weight: "12"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/11931
imported:
- "2019"
_4images_image_id: "11931"
_4images_cat_id: "1066"
_4images_user_id: "4"
_4images_image_date: "2007-09-23T19:13:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11931 -->
Diverse Basteleien von Andreas. Was es mit dem boah-ey-absolut-total-fetten Zylinder im Vordergrund auf sich hat, habe ich vergessen zu fragen. Nach meinen Erfahrungen mit Eigenbauzylindern reicht dieses Kaliber locker aus, um ft-Teile zu zerbröseln.