---
layout: "image"
title: "rrb77.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb77.jpg"
weight: "17"
konstrukteure: 
- "TST"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/11767
imported:
- "2019"
_4images_image_id: "11767"
_4images_cat_id: "1066"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "77"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11767 -->
