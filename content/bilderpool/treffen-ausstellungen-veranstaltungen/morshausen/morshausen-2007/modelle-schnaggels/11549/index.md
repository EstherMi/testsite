---
layout: "image"
title: "Robo Max"
date: "2007-09-16T19:38:31"
picture: "robomax3.jpg"
weight: "9"
konstrukteure: 
- "schnaggels"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/11549
imported:
- "2019"
_4images_image_id: "11549"
_4images_cat_id: "1047"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T19:38:31"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11549 -->
