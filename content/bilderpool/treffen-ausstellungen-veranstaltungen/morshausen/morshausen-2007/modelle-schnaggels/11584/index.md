---
layout: "image"
title: "RoboMax-Innereien"
date: "2007-09-16T22:07:26"
picture: "conventionsteffalk003.jpg"
weight: "10"
konstrukteure: 
- "schnaggels"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/11584
imported:
- "2019"
_4images_image_id: "11584"
_4images_cat_id: "1047"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:26"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11584 -->
