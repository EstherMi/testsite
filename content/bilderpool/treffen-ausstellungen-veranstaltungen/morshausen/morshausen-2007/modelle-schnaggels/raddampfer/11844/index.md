---
layout: "image"
title: "Boot 2"
date: "2007-09-18T11:42:59"
picture: "PICT5549.jpg"
weight: "2"
konstrukteure: 
- "schnaggels"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/11844
imported:
- "2019"
_4images_image_id: "11844"
_4images_cat_id: "1601"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:42:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11844 -->
