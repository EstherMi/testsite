---
layout: "image"
title: "Boot 4"
date: "2007-09-18T11:43:23"
picture: "PICT5552.jpg"
weight: "4"
konstrukteure: 
- "schnaggels"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/11846
imported:
- "2019"
_4images_image_id: "11846"
_4images_cat_id: "1601"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:43:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11846 -->
