---
layout: "image"
title: "Schiff"
date: "2007-09-16T19:38:31"
picture: "robomax2.jpg"
weight: "14"
konstrukteure: 
- "schnaggels"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/11548
imported:
- "2019"
_4images_image_id: "11548"
_4images_cat_id: "1601"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T19:38:31"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11548 -->
