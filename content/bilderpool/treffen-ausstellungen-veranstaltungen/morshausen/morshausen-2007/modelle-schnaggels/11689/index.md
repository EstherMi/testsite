---
layout: "image"
title: "Treppe-Robot"
date: "2007-09-16T22:07:27"
picture: "FT-Mrshausen-2007_172.jpg"
weight: "6"
konstrukteure: 
- "Thomas Brestrich"
fotografen:
- "Peter Damen (Alias PeterHolland)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/11689
imported:
- "2019"
_4images_image_id: "11689"
_4images_cat_id: "1047"
_4images_user_id: "22"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11689 -->
