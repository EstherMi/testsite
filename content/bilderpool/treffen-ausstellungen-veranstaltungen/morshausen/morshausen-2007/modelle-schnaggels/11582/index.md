---
layout: "image"
title: "RoboMax"
date: "2007-09-16T22:07:26"
picture: "conventionsteffalk001.jpg"
weight: "7"
konstrukteure: 
- "schnaggels"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/11582
imported:
- "2019"
_4images_image_id: "11582"
_4images_cat_id: "1047"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:26"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11582 -->
... jetzt lenkbar