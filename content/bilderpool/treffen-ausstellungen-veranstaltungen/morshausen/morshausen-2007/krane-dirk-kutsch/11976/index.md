---
layout: "image"
title: "Raupenantrieb"
date: "2007-09-25T09:26:10"
picture: "kran07.jpg"
weight: "7"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/11976
imported:
- "2019"
_4images_image_id: "11976"
_4images_cat_id: "1040"
_4images_user_id: "127"
_4images_image_date: "2007-09-25T09:26:10"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11976 -->
