---
layout: "image"
title: "rrb26.jpg"
date: "2007-09-17T17:12:11"
picture: "rrb26.jpg"
weight: "26"
konstrukteure: 
- "-?-"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/11716
imported:
- "2019"
_4images_image_id: "11716"
_4images_cat_id: "1040"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:11"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11716 -->
