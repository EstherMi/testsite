---
layout: "image"
title: "rrb23.jpg"
date: "2007-09-17T17:12:11"
picture: "rrb23.jpg"
weight: "23"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/11713
imported:
- "2019"
_4images_image_id: "11713"
_4images_cat_id: "1040"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:11"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11713 -->
