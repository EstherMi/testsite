---
layout: "image"
title: "Schaufel"
date: "2007-09-25T09:26:52"
picture: "kran12.jpg"
weight: "12"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/11981
imported:
- "2019"
_4images_image_id: "11981"
_4images_cat_id: "1040"
_4images_user_id: "127"
_4images_image_date: "2007-09-25T09:26:52"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11981 -->
