---
layout: "image"
title: "Baggerschaufel"
date: "2007-09-16T22:07:26"
picture: "conventionsteffalk009.jpg"
weight: "56"
konstrukteure: 
- "guilligan"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/11590
imported:
- "2019"
_4images_image_id: "11590"
_4images_cat_id: "1040"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:26"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11590 -->
Big is beautiful ;-)