---
layout: "image"
title: "rrb42.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb42.jpg"
weight: "42"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/11732
imported:
- "2019"
_4images_image_id: "11732"
_4images_cat_id: "1040"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "42"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11732 -->
