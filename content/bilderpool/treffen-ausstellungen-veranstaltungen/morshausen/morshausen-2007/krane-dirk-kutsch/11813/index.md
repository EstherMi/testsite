---
layout: "image"
title: "Aufbau 4"
date: "2007-09-18T11:08:59"
picture: "PICT5594.jpg"
weight: "19"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/11813
imported:
- "2019"
_4images_image_id: "11813"
_4images_cat_id: "1040"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:08:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11813 -->
