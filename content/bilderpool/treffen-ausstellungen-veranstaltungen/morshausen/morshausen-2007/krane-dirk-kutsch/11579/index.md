---
layout: "image"
title: "Kräne von Dirk Kutsch"
date: "2007-09-16T22:07:26"
picture: "FT-Mrshausen-2007_169.jpg"
weight: "45"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Damen (Alias Peter Holland)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/11579
imported:
- "2019"
_4images_image_id: "11579"
_4images_cat_id: "1040"
_4images_user_id: "22"
_4images_image_date: "2007-09-16T22:07:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11579 -->
