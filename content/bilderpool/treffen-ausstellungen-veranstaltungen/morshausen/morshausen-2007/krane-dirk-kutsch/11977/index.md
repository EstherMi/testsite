---
layout: "image"
title: "Raupen"
date: "2007-09-25T09:26:10"
picture: "kran08.jpg"
weight: "8"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/11977
imported:
- "2019"
_4images_image_id: "11977"
_4images_cat_id: "1040"
_4images_user_id: "127"
_4images_image_date: "2007-09-25T09:26:10"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11977 -->
