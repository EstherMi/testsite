---
layout: "image"
title: "Steuerung"
date: "2007-09-16T16:59:44"
picture: "kraene4.jpg"
weight: "49"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/11516
imported:
- "2019"
_4images_image_id: "11516"
_4images_cat_id: "1040"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T16:59:44"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11516 -->
