---
layout: "image"
title: "Kran"
date: "2007-09-25T09:26:10"
picture: "kran03.jpg"
weight: "3"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/11972
imported:
- "2019"
_4images_image_id: "11972"
_4images_cat_id: "1040"
_4images_user_id: "127"
_4images_image_date: "2007-09-25T09:26:10"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11972 -->
