---
layout: "image"
title: "Aufbau 3"
date: "2007-09-18T11:08:41"
picture: "PICT5592.jpg"
weight: "18"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/11812
imported:
- "2019"
_4images_image_id: "11812"
_4images_cat_id: "1040"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:08:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11812 -->
