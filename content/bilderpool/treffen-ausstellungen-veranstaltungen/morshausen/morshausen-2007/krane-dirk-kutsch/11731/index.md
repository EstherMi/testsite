---
layout: "image"
title: "rrb41.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb41.jpg"
weight: "41"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/11731
imported:
- "2019"
_4images_image_id: "11731"
_4images_cat_id: "1040"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11731 -->
