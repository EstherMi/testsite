---
layout: "comment"
hidden: true
title: "4144"
date: "2007-09-25T22:45:54"
uploadBy:
- "Guilligan"
license: "unknown"
imported:
- "2019"
---
In der Mitte des Bildes ist der Hebel zu erkennen der für den reibungslosen Auf- und Abbau des Auslegers zuständig ist. Der obere Teil mit dem 30° Baustein hängt sich in eine Kurbelwelle rein und unten wo sich die 30er Stahlachse befindet legen sich die 3 Rollen rein die für das Hubwerk des Auslegers verantwortlich sind.