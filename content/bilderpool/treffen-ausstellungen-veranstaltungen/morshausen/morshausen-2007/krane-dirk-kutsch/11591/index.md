---
layout: "image"
title: "Zugschaufel"
date: "2007-09-16T22:07:26"
picture: "conventionsteffalk010.jpg"
weight: "57"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/11591
imported:
- "2019"
_4images_image_id: "11591"
_4images_cat_id: "1040"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:26"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11591 -->
