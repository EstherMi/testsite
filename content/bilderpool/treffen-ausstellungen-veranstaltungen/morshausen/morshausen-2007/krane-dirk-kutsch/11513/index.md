---
layout: "image"
title: "Kran"
date: "2007-09-16T16:59:44"
picture: "kraene1.jpg"
weight: "46"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/11513
imported:
- "2019"
_4images_image_id: "11513"
_4images_cat_id: "1040"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T16:59:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11513 -->
