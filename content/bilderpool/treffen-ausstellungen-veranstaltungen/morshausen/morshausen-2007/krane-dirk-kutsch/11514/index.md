---
layout: "image"
title: "Schaufel"
date: "2007-09-16T16:59:44"
picture: "kraene2.jpg"
weight: "47"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/11514
imported:
- "2019"
_4images_image_id: "11514"
_4images_cat_id: "1040"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T16:59:44"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11514 -->
