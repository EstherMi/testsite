---
layout: "image"
title: "Kräne van Dirk Kutsch"
date: "2007-09-16T22:07:25"
picture: "FT-Mrshausen-2007_167.jpg"
weight: "44"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Damen (Alias Peter Holland)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/11578
imported:
- "2019"
_4images_image_id: "11578"
_4images_cat_id: "1040"
_4images_user_id: "22"
_4images_image_date: "2007-09-16T22:07:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11578 -->
