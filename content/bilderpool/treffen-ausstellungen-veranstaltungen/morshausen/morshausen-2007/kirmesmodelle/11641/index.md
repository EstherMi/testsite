---
layout: "image"
title: "Kirmesmodelle"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk060.jpg"
weight: "20"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/11641
imported:
- "2019"
_4images_image_id: "11641"
_4images_cat_id: "1058"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "60"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11641 -->
Das oben unüblich hängende Männchen ist Absicht. Die Erklärung: Der Junge, der das konstruiert hat, hat Spaß daran, Dinge so zu ändern, wie sie normalerweise nicht sind.