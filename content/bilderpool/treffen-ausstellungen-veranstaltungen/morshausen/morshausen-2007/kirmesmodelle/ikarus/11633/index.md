---
layout: "image"
title: "Kirmesmodelle"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk052.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/11633
imported:
- "2019"
_4images_image_id: "11633"
_4images_cat_id: "1069"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "52"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11633 -->
Es geht nur ein Seil von unten nach oben, welches über zwei Flaschenzüge auf insgesamt vier ziehende Seilabschnitte verteilt wird.