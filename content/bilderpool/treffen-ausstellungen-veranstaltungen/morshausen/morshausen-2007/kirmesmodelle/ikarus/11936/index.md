---
layout: "image"
title: "kikar115.JPG"
date: "2007-09-23T19:45:44"
picture: "kikar115.JPG"
weight: "7"
konstrukteure: 
- "Fam. Jansen"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/11936
imported:
- "2019"
_4images_image_id: "11936"
_4images_cat_id: "1069"
_4images_user_id: "4"
_4images_image_date: "2007-09-23T19:45:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11936 -->
Einfach raffiniert, wie die Stromübertragung gelöst ist: in den Nuten des Aluprofils stecken lackierte Messingstangen. Außen fehlt die Lackschicht, und dort schleifen Federkontakte entlang.