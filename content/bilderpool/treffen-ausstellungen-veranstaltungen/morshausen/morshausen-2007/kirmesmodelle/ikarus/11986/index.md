---
layout: "image"
title: "Ikarus"
date: "2007-09-25T09:37:28"
picture: "ikarus1.jpg"
weight: "1"
konstrukteure: 
- "Familie Jansen"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/11986
imported:
- "2019"
_4images_image_id: "11986"
_4images_cat_id: "1069"
_4images_user_id: "127"
_4images_image_date: "2007-09-25T09:37:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11986 -->
