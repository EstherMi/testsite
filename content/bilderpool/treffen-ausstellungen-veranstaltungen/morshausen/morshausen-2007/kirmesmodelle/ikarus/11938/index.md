---
layout: "image"
title: "kikar117.JPG"
date: "2007-09-23T19:47:25"
picture: "kikar117.JPG"
weight: "9"
konstrukteure: 
- "Fam. Jansen"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/11938
imported:
- "2019"
_4images_image_id: "11938"
_4images_cat_id: "1069"
_4images_user_id: "4"
_4images_image_date: "2007-09-23T19:47:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11938 -->
Sehr beeindruckend ist auch die Leichtbaukonstruktion der Fahrgastgondeln.