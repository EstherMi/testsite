---
layout: "image"
title: "Werbetafel für Profi I'm Walking"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk047.jpg"
weight: "10"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/11628
imported:
- "2019"
_4images_image_id: "11628"
_4images_cat_id: "1058"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "47"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11628 -->
Um die runde große Scheibe ist ft-Kette gespannt. Die beiden Laufroboter werden von der Rückseite aus gedreht. Das sieht dann so aus, als ob sie auf dem Rand der Scheibe entlang krabbeln.