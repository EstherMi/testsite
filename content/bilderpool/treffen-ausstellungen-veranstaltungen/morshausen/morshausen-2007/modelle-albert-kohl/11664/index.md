---
layout: "image"
title: "Gleislege-Zug"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk083.jpg"
weight: "2"
konstrukteure: 
- "Albert Kohl"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/11664
imported:
- "2019"
_4images_image_id: "11664"
_4images_cat_id: "1057"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "83"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11664 -->
Dieses Großmodell eines Gleislege-Zugs (wenn ich das richtig verstanden habe) ist mehrere Meter lang!