---
layout: "image"
title: "Industriemodell 1"
date: "2007-09-18T11:50:04"
picture: "PICT5532.jpg"
weight: "1"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/11852
imported:
- "2019"
_4images_image_id: "11852"
_4images_cat_id: "1039"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:50:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11852 -->
