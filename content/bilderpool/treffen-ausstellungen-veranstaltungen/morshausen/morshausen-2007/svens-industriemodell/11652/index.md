---
layout: "image"
title: "Bearbeitungspark"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk071.jpg"
weight: "9"
konstrukteure: 
- "Sven"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/11652
imported:
- "2019"
_4images_image_id: "11652"
_4images_cat_id: "1039"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "71"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11652 -->
Der Industrieroboter entnimmt Tischtennisbälle von der Kugelbahn links im Hintergrund und legt sie auf den Takt-Sterntisch vorne. Der Schweißroboter schließlich bringt Schweißpunkte an. Alles erdbebensicher auf einer großen Platte verschraubt, so dass die Abstände auch einen Autotransport überstehen.