---
layout: "image"
title: "Schild"
date: "2007-09-16T16:53:32"
picture: "industriemodell6.jpg"
weight: "8"
konstrukteure: 
- "sven"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/11512
imported:
- "2019"
_4images_image_id: "11512"
_4images_cat_id: "1039"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T16:53:32"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11512 -->
