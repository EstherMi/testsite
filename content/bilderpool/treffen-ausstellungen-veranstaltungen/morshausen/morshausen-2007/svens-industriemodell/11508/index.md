---
layout: "image"
title: "Stecker"
date: "2007-09-16T16:53:32"
picture: "industriemodell2.jpg"
weight: "4"
konstrukteure: 
- "sven"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/11508
imported:
- "2019"
_4images_image_id: "11508"
_4images_cat_id: "1039"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T16:53:32"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11508 -->
