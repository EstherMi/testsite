---
layout: "image"
title: "Industriemodell 2"
date: "2007-09-18T11:50:17"
picture: "PICT5534.jpg"
weight: "2"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/11853
imported:
- "2019"
_4images_image_id: "11853"
_4images_cat_id: "1039"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:50:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11853 -->
