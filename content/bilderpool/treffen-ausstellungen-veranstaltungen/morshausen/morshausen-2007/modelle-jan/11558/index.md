---
layout: "image"
title: "Spiele Roboter"
date: "2007-09-16T19:48:09"
picture: "ft_convention_07_002.jpg"
weight: "9"
konstrukteure: 
- "Jan"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/11558
imported:
- "2019"
_4images_image_id: "11558"
_4images_cat_id: "1050"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T19:48:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11558 -->
