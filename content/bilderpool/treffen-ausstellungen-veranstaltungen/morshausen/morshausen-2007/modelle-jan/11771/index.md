---
layout: "image"
title: "rrb81.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb81.jpg"
weight: "8"
konstrukteure: 
- "kehrblech"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/11771
imported:
- "2019"
_4images_image_id: "11771"
_4images_cat_id: "1050"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "81"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11771 -->
