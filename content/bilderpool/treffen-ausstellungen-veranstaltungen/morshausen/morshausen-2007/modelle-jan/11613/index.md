---
layout: "image"
title: "Tic-Tac-Toe-Roboter"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk032.jpg"
weight: "11"
konstrukteure: 
- "Kehrblech"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/11613
imported:
- "2019"
_4images_image_id: "11613"
_4images_cat_id: "1050"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11613 -->
