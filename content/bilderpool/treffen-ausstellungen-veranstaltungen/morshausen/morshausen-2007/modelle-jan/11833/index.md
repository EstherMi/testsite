---
layout: "image"
title: "2"
date: "2007-09-18T11:33:27"
picture: "PICT5638.jpg"
weight: "2"
konstrukteure: 
- "kehrblech"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/11833
imported:
- "2019"
_4images_image_id: "11833"
_4images_cat_id: "1050"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:33:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11833 -->
