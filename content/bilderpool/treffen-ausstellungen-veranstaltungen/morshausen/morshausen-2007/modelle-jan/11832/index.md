---
layout: "image"
title: "1"
date: "2007-09-18T11:33:15"
picture: "PICT5637.jpg"
weight: "1"
konstrukteure: 
- "kehrblech"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/11832
imported:
- "2019"
_4images_image_id: "11832"
_4images_cat_id: "1050"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:33:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11832 -->
