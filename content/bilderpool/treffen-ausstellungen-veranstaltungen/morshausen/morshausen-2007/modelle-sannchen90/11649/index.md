---
layout: "image"
title: "Kran"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk068.jpg"
weight: "7"
konstrukteure: 
- "sannchen90"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/11649
imported:
- "2019"
_4images_image_id: "11649"
_4images_cat_id: "1049"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "68"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11649 -->
... und Fachgespräche im Hintergrund.