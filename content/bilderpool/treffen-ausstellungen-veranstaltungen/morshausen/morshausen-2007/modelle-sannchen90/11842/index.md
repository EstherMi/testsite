---
layout: "image"
title: "Bagger 4"
date: "2007-09-18T11:41:21"
picture: "PICT5646.jpg"
weight: "4"
konstrukteure: 
- "Michael Orlik"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/11842
imported:
- "2019"
_4images_image_id: "11842"
_4images_cat_id: "1049"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:41:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11842 -->
