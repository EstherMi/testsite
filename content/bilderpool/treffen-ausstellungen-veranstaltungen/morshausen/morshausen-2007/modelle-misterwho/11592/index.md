---
layout: "image"
title: "Radarteleskop"
date: "2007-09-16T22:07:26"
picture: "conventionsteffalk011.jpg"
weight: "6"
konstrukteure: 
- "MisterWho"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/11592
imported:
- "2019"
_4images_image_id: "11592"
_4images_cat_id: "1043"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:26"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11592 -->
