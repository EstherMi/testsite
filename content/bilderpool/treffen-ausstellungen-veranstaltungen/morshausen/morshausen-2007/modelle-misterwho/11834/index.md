---
layout: "image"
title: "MisterWho mit Schüssel"
date: "2007-09-18T11:35:05"
picture: "PICT5683.jpg"
weight: "2"
konstrukteure: 
- "Jojo Jacobi"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/11834
imported:
- "2019"
_4images_image_id: "11834"
_4images_cat_id: "1043"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:35:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11834 -->
