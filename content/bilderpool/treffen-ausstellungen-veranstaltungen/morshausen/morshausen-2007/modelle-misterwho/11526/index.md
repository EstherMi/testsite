---
layout: "image"
title: "Roboter"
date: "2007-09-16T16:59:44"
picture: "misterwho1.jpg"
weight: "4"
konstrukteure: 
- "MisterWho"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/11526
imported:
- "2019"
_4images_image_id: "11526"
_4images_cat_id: "1043"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T16:59:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11526 -->
