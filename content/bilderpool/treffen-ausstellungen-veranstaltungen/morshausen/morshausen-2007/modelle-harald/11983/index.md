---
layout: "image"
title: "Tarnkappenbomber"
date: "2007-09-25T09:30:25"
picture: "tarnkappenbomber1.jpg"
weight: "1"
konstrukteure: 
- "Harald"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/11983
imported:
- "2019"
_4images_image_id: "11983"
_4images_cat_id: "1041"
_4images_user_id: "127"
_4images_image_date: "2007-09-25T09:30:25"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11983 -->
Das meistgesuchte Modell hier von Heiko aufgenommen ;-)