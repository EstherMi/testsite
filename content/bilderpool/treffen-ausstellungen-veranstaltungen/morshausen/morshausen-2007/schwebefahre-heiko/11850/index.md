---
layout: "image"
title: "Endlich mal wieder Statik"
date: "2007-09-18T11:48:51"
picture: "PICT5685.jpg"
weight: "1"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/11850
imported:
- "2019"
_4images_image_id: "11850"
_4images_cat_id: "1042"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:48:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11850 -->
