---
layout: "image"
title: "rrb90.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb90.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/11780
imported:
- "2019"
_4images_image_id: "11780"
_4images_cat_id: "1037"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "90"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11780 -->
