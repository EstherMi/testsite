---
layout: "image"
title: "rrb82.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb82.jpg"
weight: "4"
konstrukteure: 
- "Steffalk"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/11772
imported:
- "2019"
_4images_image_id: "11772"
_4images_cat_id: "1037"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "82"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11772 -->
