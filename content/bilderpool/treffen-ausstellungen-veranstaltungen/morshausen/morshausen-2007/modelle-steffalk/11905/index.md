---
layout: "image"
title: "7Seg150.JPG"
date: "2007-09-22T13:17:18"
picture: "7Seg150.JPG"
weight: "3"
konstrukteure: 
- "Steffalk"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/11905
imported:
- "2019"
_4images_image_id: "11905"
_4images_cat_id: "1037"
_4images_user_id: "4"
_4images_image_date: "2007-09-22T13:17:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11905 -->
Das gelbe sind die Segmente, von denen jede Dezimalstelle 7 hat (4 aufrecht, 3 quer).