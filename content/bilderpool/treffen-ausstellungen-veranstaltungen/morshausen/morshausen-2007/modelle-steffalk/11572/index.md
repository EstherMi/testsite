---
layout: "image"
title: "Meister Stefan"
date: "2007-09-16T22:07:25"
picture: "FT-Mrshausen-2007_189.jpg"
weight: "13"
konstrukteure: 
- "Steffalk"
fotografen:
- "Peter Damen (alias PeterHolland)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/11572
imported:
- "2019"
_4images_image_id: "11572"
_4images_cat_id: "1037"
_4images_user_id: "22"
_4images_image_date: "2007-09-16T22:07:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11572 -->
