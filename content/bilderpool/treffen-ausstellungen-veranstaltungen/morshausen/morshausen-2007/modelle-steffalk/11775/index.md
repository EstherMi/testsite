---
layout: "image"
title: "rrb85.jpg"
date: "2007-09-17T17:12:12"
picture: "rrb85.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/11775
imported:
- "2019"
_4images_image_id: "11775"
_4images_cat_id: "1037"
_4images_user_id: "371"
_4images_image_date: "2007-09-17T17:12:12"
_4images_image_order: "85"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11775 -->
