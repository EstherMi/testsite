---
layout: "image"
title: "Roboter"
date: "2007-09-16T17:09:08"
picture: "elektronik1.jpg"
weight: "5"
konstrukteure: 
- "thkais"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/11541
imported:
- "2019"
_4images_image_id: "11541"
_4images_cat_id: "1046"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T17:09:08"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11541 -->
