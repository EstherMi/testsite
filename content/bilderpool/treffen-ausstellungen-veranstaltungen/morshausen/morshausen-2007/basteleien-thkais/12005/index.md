---
layout: "image"
title: "Roboter"
date: "2007-09-25T10:17:07"
picture: "roboter1.jpg"
weight: "2"
konstrukteure: 
- "thkais"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/12005
imported:
- "2019"
_4images_image_id: "12005"
_4images_cat_id: "1046"
_4images_user_id: "127"
_4images_image_date: "2007-09-25T10:17:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12005 -->
