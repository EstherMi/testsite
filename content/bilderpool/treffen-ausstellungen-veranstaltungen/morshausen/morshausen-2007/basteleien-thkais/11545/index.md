---
layout: "image"
title: "Kartenleser für das Robo IF"
date: "2007-09-16T17:09:08"
picture: "elektronik5.jpg"
weight: "9"
konstrukteure: 
- "thkais"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/11545
imported:
- "2019"
_4images_image_id: "11545"
_4images_cat_id: "1046"
_4images_user_id: "453"
_4images_image_date: "2007-09-16T17:09:08"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11545 -->
