---
layout: "image"
title: "Motorhaube"
date: "2007-09-18T11:22:25"
picture: "PICT5722.jpg"
weight: "15"
konstrukteure: 
- "Claus-Werner Ludwig"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/11823
imported:
- "2019"
_4images_image_id: "11823"
_4images_cat_id: "1056"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:22:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11823 -->
