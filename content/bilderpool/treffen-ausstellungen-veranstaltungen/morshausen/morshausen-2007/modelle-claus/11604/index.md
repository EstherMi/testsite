---
layout: "image"
title: "LKW-Fahrgestell trägt Boot"
date: "2007-09-16T22:07:26"
picture: "conventionsteffalk023.jpg"
weight: "23"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/11604
imported:
- "2019"
_4images_image_id: "11604"
_4images_cat_id: "1056"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:26"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11604 -->
