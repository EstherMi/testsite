---
layout: "image"
title: "cz142.JPG"
date: "2007-09-24T21:06:31"
picture: "cz142.JPG"
weight: "5"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/11952
imported:
- "2019"
_4images_image_id: "11952"
_4images_cat_id: "1056"
_4images_user_id: "4"
_4images_image_date: "2007-09-24T21:06:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11952 -->
Details an der Unterseite der Zugmaschine. Die Powermots treiben je eine Achse an. Die Differenziale haben Achsverlängerungen angeschweißt bekommen.