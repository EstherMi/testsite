---
layout: "image"
title: "Tieflader mit gelenkten Achsen"
date: "2007-09-18T11:15:46"
picture: "PICT5578.jpg"
weight: "10"
konstrukteure: 
- "Claus-Werner Ludwig"
fotografen:
- "Heiko Engelke"
keywords: ["Lenkachse"]
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/11818
imported:
- "2019"
_4images_image_id: "11818"
_4images_cat_id: "1056"
_4images_user_id: "9"
_4images_image_date: "2007-09-18T11:15:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11818 -->
