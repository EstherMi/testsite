---
layout: "image"
title: "cz056.JPG"
date: "2007-09-24T21:09:27"
picture: "cz056.JPG"
weight: "7"
konstrukteure: 
- "Claus-W. Ludwig"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/11954
imported:
- "2019"
_4images_image_id: "11954"
_4images_cat_id: "1056"
_4images_user_id: "4"
_4images_image_date: "2007-09-24T21:09:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11954 -->
Unter der Ladefläche liegen Akku und Blinkelektronik.