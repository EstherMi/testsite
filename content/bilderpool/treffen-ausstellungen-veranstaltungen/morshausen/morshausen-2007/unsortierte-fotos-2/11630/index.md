---
layout: "image"
title: "Laufroboter"
date: "2007-09-16T22:07:27"
picture: "conventionsteffalk049.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/11630
imported:
- "2019"
_4images_image_id: "11630"
_4images_cat_id: "1053"
_4images_user_id: "104"
_4images_image_date: "2007-09-16T22:07:27"
_4images_image_order: "49"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11630 -->
