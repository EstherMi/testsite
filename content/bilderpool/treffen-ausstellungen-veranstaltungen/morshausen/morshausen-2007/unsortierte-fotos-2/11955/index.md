---
layout: "image"
title: "hexa101.JPG"
date: "2007-09-24T21:21:01"
picture: "hexa101.JPG"
weight: "1"
konstrukteure: 
- "ft?"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/11955
imported:
- "2019"
_4images_image_id: "11955"
_4images_cat_id: "1053"
_4images_user_id: "4"
_4images_image_date: "2007-09-24T21:21:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11955 -->
Ein Flugsimulator auf einem Hexapod-Gestell mit 6 Hubgetrieben.