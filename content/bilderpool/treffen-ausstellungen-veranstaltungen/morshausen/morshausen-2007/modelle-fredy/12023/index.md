---
layout: "image"
title: "Programm2"
date: "2007-09-27T20:22:13"
picture: "programme2.jpg"
weight: "7"
konstrukteure: 
- "Fredy"
fotografen:
- "Limit"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Limit"
license: "unknown"
legacy_id:
- details/12023
imported:
- "2019"
_4images_image_id: "12023"
_4images_cat_id: "1071"
_4images_user_id: "430"
_4images_image_date: "2007-09-27T20:22:13"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12023 -->
Das noch komplexere Unterprogramm für die Sortiermaschine