---
layout: "image"
title: "Industriemodell"
date: "2007-09-25T09:15:03"
picture: "industriemodell4.jpg"
weight: "12"
konstrukteure: 
- "Fredy"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/11969
imported:
- "2019"
_4images_image_id: "11969"
_4images_cat_id: "1071"
_4images_user_id: "127"
_4images_image_date: "2007-09-25T09:15:03"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11969 -->
