---
layout: "image"
title: "Joystick"
date: "2007-10-03T16:36:25"
picture: "robter3.jpg"
weight: "3"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/12120
imported:
- "2019"
_4images_image_id: "12120"
_4images_cat_id: "1059"
_4images_user_id: "453"
_4images_image_date: "2007-10-03T16:36:25"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12120 -->
