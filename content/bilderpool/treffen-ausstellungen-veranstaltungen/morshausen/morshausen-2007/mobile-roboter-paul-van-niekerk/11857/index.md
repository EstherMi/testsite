---
layout: "image"
title: "mobiler Roboter"
date: "2007-09-18T12:45:08"
picture: "mobilerroboter3.jpg"
weight: "10"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/11857
imported:
- "2019"
_4images_image_id: "11857"
_4images_cat_id: "1059"
_4images_user_id: "127"
_4images_image_date: "2007-09-18T12:45:08"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11857 -->
