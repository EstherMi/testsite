---
layout: "image"
title: "IMGP3831"
date: "2003-09-21T13:32:49"
picture: "IMGP3831.jpg"
weight: "21"
konstrukteure: 
- "MaLie"
fotografen:
- "NN"
keywords: ["Fahrgeschäft", "Kirmesmodell", "Fallturm"]
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/1501
imported:
- "2019"
_4images_image_id: "1501"
_4images_cat_id: "149"
_4images_user_id: "1"
_4images_image_date: "2003-09-21T13:32:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1501 -->
