---
layout: "comment"
hidden: true
title: "98"
date: "2003-09-30T08:42:48"
uploadBy:
- "DerMitDenBitsTanzt"
license: "unknown"
imported:
- "2019"
---
Roboterarm als Standmodell"Standmodell" leider im doppelten Sinne: vom Fahrwerk getrennt auf dem Tisch stehend und mangels fertiger Steuersoftware mit stehenden Motoren. Es handelt sich um einen 6-Achsen-Knickarm-Roboter (oder wie einige Profis zählen: 5 Achsen plus Werkzeug) mit einem Gesamtgewicht von ca. 3,3 kg. Die Achsen 1 und 2 werden von je einem Power Motor (schwarz, 8:1) und die Achsen 3 bis 6 von je einem Mini Motor mit Vorsatzgetriebe bewegt.