---
layout: "image"
title: "IMGP3862"
date: "2003-09-28T09:48:23"
picture: "IMGP3862.jpg"
weight: "5"
konstrukteure: 
- "Stephan Wenkers"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/1594
imported:
- "2019"
_4images_image_id: "1594"
_4images_cat_id: "154"
_4images_user_id: "1"
_4images_image_date: "2003-09-28T09:48:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1594 -->
