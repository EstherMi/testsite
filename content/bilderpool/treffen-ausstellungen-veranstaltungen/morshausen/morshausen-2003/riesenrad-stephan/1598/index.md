---
layout: "image"
title: "DSCF0015"
date: "2003-09-28T09:48:43"
picture: "DSCF0015.jpg"
weight: "6"
konstrukteure: 
- "Stephan Wenkers"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/1598
imported:
- "2019"
_4images_image_id: "1598"
_4images_cat_id: "154"
_4images_user_id: "1"
_4images_image_date: "2003-09-28T09:48:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1598 -->
