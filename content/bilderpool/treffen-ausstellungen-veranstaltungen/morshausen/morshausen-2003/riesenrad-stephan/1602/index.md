---
layout: "image"
title: "IMGP3863"
date: "2003-09-28T09:48:44"
picture: "IMGP3863.JPG"
weight: "10"
konstrukteure: 
- "Stephan Wenkers"
fotografen:
- "NN"
keywords: ["Riesenrad", "Fahrgeschäft", "Kirmesmodell"]
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/1602
imported:
- "2019"
_4images_image_id: "1602"
_4images_cat_id: "154"
_4images_user_id: "1"
_4images_image_date: "2003-09-28T09:48:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1602 -->
