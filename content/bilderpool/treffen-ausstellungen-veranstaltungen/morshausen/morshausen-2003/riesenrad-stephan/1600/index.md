---
layout: "image"
title: "DSCF0078"
date: "2003-09-28T09:48:44"
picture: "DSCF0078.jpg"
weight: "8"
konstrukteure: 
- "Stephan Wenkers"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/1600
imported:
- "2019"
_4images_image_id: "1600"
_4images_cat_id: "154"
_4images_user_id: "1"
_4images_image_date: "2003-09-28T09:48:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1600 -->
