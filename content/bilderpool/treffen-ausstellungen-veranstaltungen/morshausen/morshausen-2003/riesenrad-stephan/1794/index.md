---
layout: "image"
title: "Die Kassenhäuschen und der Strahlenkranz"
date: "2003-10-08T15:42:08"
picture: "Kassen_huschen_vom_RR_1.jpg"
weight: "2"
konstrukteure: 
- "Stephan"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/1794
imported:
- "2019"
_4images_image_id: "1794"
_4images_cat_id: "154"
_4images_user_id: "130"
_4images_image_date: "2003-10-08T15:42:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1794 -->
Hier kann man schön den Eingang mit den Kassenhäuschen und dem alles überspannenden Strahlenkranz sehen.