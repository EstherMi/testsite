---
layout: "image"
title: "DSCF0063"
date: "2003-09-28T09:48:23"
picture: "DSCF0063.jpg"
weight: "4"
konstrukteure: 
- "Stephan Wenkers"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/1591
imported:
- "2019"
_4images_image_id: "1591"
_4images_cat_id: "154"
_4images_user_id: "1"
_4images_image_date: "2003-09-28T09:48:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1591 -->
