---
layout: "image"
title: "IMGP3865"
date: "2003-09-28T09:48:23"
picture: "IMGP3865.JPG"
weight: "5"
konstrukteure: 
- "Holger Howey"
fotografen:
- "NN"
keywords: ["Fahrgeschäft", "Kirmesmodell", "Riesenrad"]
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/1595
imported:
- "2019"
_4images_image_id: "1595"
_4images_cat_id: "153"
_4images_user_id: "1"
_4images_image_date: "2003-09-28T09:48:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1595 -->
