---
layout: "image"
title: "DSCF0020"
date: "2003-09-28T09:56:47"
picture: "DSCF0020.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "NN"
keywords: ["Traktor", "Fendt", "Geräteträger"]
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/1676
imported:
- "2019"
_4images_image_id: "1676"
_4images_cat_id: "166"
_4images_user_id: "1"
_4images_image_date: "2003-09-28T09:56:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1676 -->
