---
layout: "image"
title: "So viel Funktion auf einmal ..."
date: "2003-10-08T17:55:04"
picture: "mhdrescher_harald_rckseite.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Hartmut Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/1815
imported:
- "2019"
_4images_image_id: "1815"
_4images_cat_id: "166"
_4images_user_id: "9"
_4images_image_date: "2003-10-08T17:55:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1815 -->
