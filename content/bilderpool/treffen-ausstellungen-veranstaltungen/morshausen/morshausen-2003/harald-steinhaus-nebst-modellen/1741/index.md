---
layout: "image"
title: "Mähdrescher"
date: "2003-09-28T14:48:40"
picture: "scan_10.jpg"
weight: "16"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/1741
imported:
- "2019"
_4images_image_id: "1741"
_4images_cat_id: "166"
_4images_user_id: "9"
_4images_image_date: "2003-09-28T14:48:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1741 -->
