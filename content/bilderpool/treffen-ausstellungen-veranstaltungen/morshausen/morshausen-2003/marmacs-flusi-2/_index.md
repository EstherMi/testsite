---
layout: "overview"
title: "MarMacs FluSi 2"
date: 2019-12-17T18:11:36+01:00
legacy_id:
- categories/170
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=170 --> 
Pädagogisch wertvolles Kinderspielzeug, das viel bespielt, aber leider nur wenig fotografiert wurde.