---
layout: "image"
title: "IMGP3841"
date: "2003-09-28T10:07:21"
picture: "IMGP3841.JPG"
weight: "1"
konstrukteure: 
- "Thomas Habig"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/1728
imported:
- "2019"
_4images_image_id: "1728"
_4images_cat_id: "176"
_4images_user_id: "1"
_4images_image_date: "2003-09-28T10:07:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1728 -->
