---
layout: "image"
title: "n2-0056"
date: "2003-09-28T09:47:40"
picture: "n2-0056.JPG"
weight: "11"
konstrukteure: 
- "Claus-Werner Ludwig"
fotografen:
- "NN"
keywords: ["Traktor"]
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/1588
imported:
- "2019"
_4images_image_id: "1588"
_4images_cat_id: "152"
_4images_user_id: "1"
_4images_image_date: "2003-09-28T09:47:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1588 -->
