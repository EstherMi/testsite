---
layout: "image"
title: "Bergauf"
date: "2003-10-03T15:01:46"
picture: "P9200015.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/1778
imported:
- "2019"
_4images_image_id: "1778"
_4images_cat_id: "173"
_4images_user_id: "4"
_4images_image_date: "2003-10-03T15:01:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1778 -->
