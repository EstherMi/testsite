---
layout: "overview"
title: "Heikos Dosen-Stapel-Roboter"
date: 2019-12-17T18:11:28+01:00
legacy_id:
- categories/162
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=162 --> 
Roboterarm mit fünf Bewegungsachsen und Joysticksteuerung.