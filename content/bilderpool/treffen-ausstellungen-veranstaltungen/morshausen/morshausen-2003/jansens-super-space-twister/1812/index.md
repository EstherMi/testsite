---
layout: "image"
title: "Jansen und Bürgermeister Wagner"
date: "2003-10-08T17:53:40"
picture: "kirmes_jansen_wagner.jpg"
weight: "3"
konstrukteure: 
- "Fam. Jansen"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/1812
imported:
- "2019"
_4images_image_id: "1812"
_4images_cat_id: "168"
_4images_user_id: "9"
_4images_image_date: "2003-10-08T17:53:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1812 -->
