---
layout: "image"
title: "Rote Flachträger 120!"
date: "2006-10-29T15:28:42"
picture: "ft2.jpg"
weight: "4"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Rob van Baal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "robvanbaal"
license: "unknown"
legacy_id:
- details/7250
imported:
- "2019"
_4images_image_id: "7250"
_4images_cat_id: "696"
_4images_user_id: "379"
_4images_image_date: "2006-10-29T15:28:42"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7250 -->
