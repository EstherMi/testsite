---
layout: "image"
title: "Neue Modelle 2006"
date: "2006-10-29T15:28:42"
picture: "ft1.jpg"
weight: "3"
konstrukteure: 
- "fischertechnik GmbH"
fotografen:
- "Rob van Baal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "robvanbaal"
license: "unknown"
legacy_id:
- details/7249
imported:
- "2019"
_4images_image_id: "7249"
_4images_cat_id: "696"
_4images_user_id: "379"
_4images_image_date: "2006-10-29T15:28:42"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7249 -->
