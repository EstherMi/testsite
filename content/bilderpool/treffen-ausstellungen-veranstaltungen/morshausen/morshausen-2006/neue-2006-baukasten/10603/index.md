---
layout: "image"
title: "Neue Baukästen 2006"
date: "2007-05-31T09:44:07"
picture: "neuek1.jpg"
weight: "1"
konstrukteure: 
- "ft"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- details/10603
imported:
- "2019"
_4images_image_id: "10603"
_4images_cat_id: "696"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:44:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10603 -->
Aufgebaute Modelle