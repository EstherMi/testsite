---
layout: "image"
title: "Umlagerung"
date: "2007-05-31T09:43:46"
picture: "holzfarbrik2.jpg"
weight: "2"
konstrukteure: 
- "Familie Janssen"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- details/10594
imported:
- "2019"
_4images_image_id: "10594"
_4images_cat_id: "667"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:43:46"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10594 -->
mit einem Roboterarm werden die Holzstüke auf ein Laufband gelegt