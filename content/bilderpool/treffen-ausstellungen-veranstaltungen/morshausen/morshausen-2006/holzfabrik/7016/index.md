---
layout: "image"
title: "Holzfab_08.JPG"
date: "2006-09-26T18:46:42"
picture: "Holzfab_08.JPG"
weight: "20"
konstrukteure: 
- "Fam. Jansen"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/7016
imported:
- "2019"
_4images_image_id: "7016"
_4images_cat_id: "667"
_4images_user_id: "4"
_4images_image_date: "2006-09-26T18:46:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7016 -->
Die Sortierstation etwas genauer.