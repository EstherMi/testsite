---
layout: "image"
title: "Holzfab_05.JPG"
date: "2006-09-26T18:43:57"
picture: "Holzfab_05.JPG"
weight: "17"
konstrukteure: 
- "Fam. Jansen"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/7013
imported:
- "2019"
_4images_image_id: "7013"
_4images_cat_id: "667"
_4images_user_id: "4"
_4images_image_date: "2006-09-26T18:43:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7013 -->
