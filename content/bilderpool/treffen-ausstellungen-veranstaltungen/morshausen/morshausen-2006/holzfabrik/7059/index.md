---
layout: "image"
title: "Die Belegschaft der Holzfabrik"
date: "2006-10-02T02:44:55"
picture: "Mrshausen_132.jpg"
weight: "10"
konstrukteure: 
- "Fam. Jansen"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/7059
imported:
- "2019"
_4images_image_id: "7059"
_4images_cat_id: "667"
_4images_user_id: "130"
_4images_image_date: "2006-10-02T02:44:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7059 -->
