---
layout: "image"
title: "Universum"
date: "2006-10-29T14:54:37"
picture: "cwl6.jpg"
weight: "1"
konstrukteure: 
- "Claus-Werner Ludwig"
fotografen:
- "Rob van Baal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "robvanbaal"
license: "unknown"
legacy_id:
- details/7248
imported:
- "2019"
_4images_image_id: "7248"
_4images_cat_id: "659"
_4images_user_id: "379"
_4images_image_date: "2006-10-29T14:54:37"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7248 -->
