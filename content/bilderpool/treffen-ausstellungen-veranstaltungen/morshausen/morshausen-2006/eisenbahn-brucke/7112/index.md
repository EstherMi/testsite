---
layout: "image"
title: "convention4.jpg"
date: "2006-10-03T11:32:49"
picture: "convention4.jpg"
weight: "13"
konstrukteure: 
- "Thomas Habig"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/7112
imported:
- "2019"
_4images_image_id: "7112"
_4images_cat_id: "665"
_4images_user_id: "1"
_4images_image_date: "2006-10-03T11:32:49"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7112 -->
