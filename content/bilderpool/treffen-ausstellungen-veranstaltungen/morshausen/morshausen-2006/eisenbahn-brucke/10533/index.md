---
layout: "image"
title: "Güterzug"
date: "2007-05-28T11:17:00"
picture: "eisenbahnbruecke1.jpg"
weight: "1"
konstrukteure: 
- "Thomas Habig"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- details/10533
imported:
- "2019"
_4images_image_id: "10533"
_4images_cat_id: "665"
_4images_user_id: "109"
_4images_image_date: "2007-05-28T11:17:00"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10533 -->
Güterzug wartet vor Signal