---
layout: "image"
title: "Messemodell"
date: "2007-04-04T10:29:46"
picture: "Fischertechnik_Convention_2006_in_Mrshausen_021.jpg"
weight: "5"
konstrukteure: 
- "Martin Romann"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/9979
imported:
- "2019"
_4images_image_id: "9979"
_4images_cat_id: "666"
_4images_user_id: "453"
_4images_image_date: "2007-04-04T10:29:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9979 -->
