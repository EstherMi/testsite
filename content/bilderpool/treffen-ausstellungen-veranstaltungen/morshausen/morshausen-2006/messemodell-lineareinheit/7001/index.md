---
layout: "image"
title: "Labyrinthroboter_5"
date: "2006-09-25T23:17:17"
picture: "remadus7.jpg"
weight: "17"
konstrukteure: 
- "Martin Romann"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/7001
imported:
- "2019"
_4images_image_id: "7001"
_4images_cat_id: "666"
_4images_user_id: "127"
_4images_image_date: "2006-09-25T23:17:17"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7001 -->
