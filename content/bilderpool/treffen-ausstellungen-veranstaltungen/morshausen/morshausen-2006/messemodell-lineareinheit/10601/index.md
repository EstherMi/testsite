---
layout: "image"
title: "Messemodell"
date: "2007-05-31T09:44:07"
picture: "messemodell4.jpg"
weight: "4"
konstrukteure: 
- "Martin Romann"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- details/10601
imported:
- "2019"
_4images_image_id: "10601"
_4images_cat_id: "666"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:44:07"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10601 -->
Luftdruckmesser