---
layout: "image"
title: "Labyrinthroboter_4"
date: "2006-09-25T23:17:17"
picture: "remadus4.jpg"
weight: "14"
konstrukteure: 
- "Martin Romann"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/6998
imported:
- "2019"
_4images_image_id: "6998"
_4images_cat_id: "666"
_4images_user_id: "127"
_4images_image_date: "2006-09-25T23:17:17"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6998 -->
