---
layout: "image"
title: "Antonov"
date: "2007-05-31T09:43:46"
picture: "flugzeuge4.jpg"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- details/10592
imported:
- "2019"
_4images_image_id: "10592"
_4images_cat_id: "664"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:43:46"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10592 -->
Kockpit