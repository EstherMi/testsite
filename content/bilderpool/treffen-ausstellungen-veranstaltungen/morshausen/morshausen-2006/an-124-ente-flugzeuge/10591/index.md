---
layout: "image"
title: "Antonov"
date: "2007-05-31T09:43:46"
picture: "flugzeuge3.jpg"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Reiner Stüven"
keywords: ["modding"]
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- details/10591
imported:
- "2019"
_4images_image_id: "10591"
_4images_cat_id: "664"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:43:46"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10591 -->
Tragflächen, Leitwerk und Antrieb