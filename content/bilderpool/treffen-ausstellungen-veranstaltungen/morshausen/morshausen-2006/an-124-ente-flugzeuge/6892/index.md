---
layout: "image"
title: "Hubschrauber"
date: "2006-09-24T01:20:24"
picture: "jpeg13.jpg"
weight: "26"
konstrukteure: 
- "Harald"
fotografen:
- "Heiko Engelke"
keywords: ["Hubschrauber"]
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/6892
imported:
- "2019"
_4images_image_id: "6892"
_4images_cat_id: "664"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T01:20:24"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6892 -->
