---
layout: "image"
title: "Propellerflugzeug"
date: "2007-05-31T09:43:46"
picture: "flugzeuge1.jpg"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- details/10589
imported:
- "2019"
_4images_image_id: "10589"
_4images_cat_id: "664"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:43:46"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10589 -->
mit einziehbaren Fahrwerk