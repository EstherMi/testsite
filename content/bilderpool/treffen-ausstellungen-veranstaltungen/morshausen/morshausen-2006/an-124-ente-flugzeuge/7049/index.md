---
layout: "image"
title: "Details der Antonov"
date: "2006-10-01T14:11:26"
picture: "Mrshausen_030.jpg"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/7049
imported:
- "2019"
_4images_image_id: "7049"
_4images_cat_id: "664"
_4images_user_id: "130"
_4images_image_date: "2006-10-01T14:11:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7049 -->
