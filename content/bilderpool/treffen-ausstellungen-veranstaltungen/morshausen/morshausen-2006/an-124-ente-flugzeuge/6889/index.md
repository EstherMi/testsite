---
layout: "image"
title: "Ente"
date: "2006-09-24T01:20:13"
picture: "jpeg10.jpg"
weight: "24"
konstrukteure: 
- "Harald"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/6889
imported:
- "2019"
_4images_image_id: "6889"
_4images_cat_id: "664"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T01:20:13"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6889 -->
