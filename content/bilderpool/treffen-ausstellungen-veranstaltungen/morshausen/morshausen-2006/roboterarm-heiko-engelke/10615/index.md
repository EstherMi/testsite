---
layout: "image"
title: "Displayanzeige zu Roboterarm"
date: "2007-05-31T09:44:39"
picture: "roboarm2.jpg"
weight: "2"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- details/10615
imported:
- "2019"
_4images_image_id: "10615"
_4images_cat_id: "663"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:44:39"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10615 -->
auf dem Rechner