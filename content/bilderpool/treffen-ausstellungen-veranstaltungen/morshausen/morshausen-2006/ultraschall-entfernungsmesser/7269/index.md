---
layout: "image"
title: "Ultraschall-Entfernungsmesser"
date: "2006-10-29T19:01:47"
picture: "rb1.jpg"
weight: "4"
konstrukteure: 
- "Richard Budding"
fotografen:
- "Rob van Baal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "robvanbaal"
license: "unknown"
legacy_id:
- details/7269
imported:
- "2019"
_4images_image_id: "7269"
_4images_cat_id: "687"
_4images_user_id: "379"
_4images_image_date: "2006-10-29T19:01:47"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7269 -->
