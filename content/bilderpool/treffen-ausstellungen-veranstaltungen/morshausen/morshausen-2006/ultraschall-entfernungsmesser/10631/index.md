---
layout: "image"
title: "Entfernungsanzeige"
date: "2007-05-31T09:45:20"
picture: "ultraschall3.jpg"
weight: "3"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- details/10631
imported:
- "2019"
_4images_image_id: "10631"
_4images_cat_id: "687"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:45:20"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10631 -->
mit Lampen