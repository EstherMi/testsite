---
layout: "image"
title: "Pneumatikbagger"
date: "2007-05-31T09:44:07"
picture: "pneumatik3.jpg"
weight: "3"
konstrukteure: 
- "Michael Orlik"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- details/10606
imported:
- "2019"
_4images_image_id: "10606"
_4images_cat_id: "675"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:44:07"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10606 -->
