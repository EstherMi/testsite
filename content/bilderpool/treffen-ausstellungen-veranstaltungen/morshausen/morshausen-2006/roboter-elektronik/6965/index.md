---
layout: "image"
title: "LKW_2"
date: "2006-09-24T22:49:47"
picture: "thkais3.jpg"
weight: "5"
konstrukteure: 
- "Thomas Kaiser"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/6965
imported:
- "2019"
_4images_image_id: "6965"
_4images_cat_id: "662"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T22:49:47"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6965 -->
