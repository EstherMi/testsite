---
layout: "image"
title: "Display am Robopro Interface"
date: "2006-09-24T22:49:47"
picture: "thkais1.jpg"
weight: "3"
konstrukteure: 
- "Thomas Kaiser"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/6963
imported:
- "2019"
_4images_image_id: "6963"
_4images_cat_id: "662"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T22:49:47"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6963 -->
