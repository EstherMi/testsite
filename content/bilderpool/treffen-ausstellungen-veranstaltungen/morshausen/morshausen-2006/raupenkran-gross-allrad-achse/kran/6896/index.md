---
layout: "image"
title: "Kran_4"
date: "2006-09-24T01:42:49"
picture: "kran04.jpg"
weight: "11"
konstrukteure: 
- "Kutsch"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/6896
imported:
- "2019"
_4images_image_id: "6896"
_4images_cat_id: "679"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T01:42:49"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6896 -->
