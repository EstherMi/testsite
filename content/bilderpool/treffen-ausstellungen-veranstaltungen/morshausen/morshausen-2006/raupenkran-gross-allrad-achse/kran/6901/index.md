---
layout: "image"
title: "Kran_9"
date: "2006-09-24T01:42:50"
picture: "kran09.jpg"
weight: "16"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/6901
imported:
- "2019"
_4images_image_id: "6901"
_4images_cat_id: "679"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T01:42:50"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6901 -->
