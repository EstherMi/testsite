---
layout: "image"
title: "Kran_14"
date: "2006-09-24T01:43:20"
picture: "kran14.jpg"
weight: "21"
konstrukteure: 
- "Kutsch"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/6906
imported:
- "2019"
_4images_image_id: "6906"
_4images_cat_id: "679"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T01:43:20"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6906 -->
