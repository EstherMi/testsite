---
layout: "image"
title: "Kran_10"
date: "2006-09-24T01:42:50"
picture: "kran10.jpg"
weight: "17"
konstrukteure: 
- "Kutsch"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/6902
imported:
- "2019"
_4images_image_id: "6902"
_4images_cat_id: "679"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T01:42:50"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6902 -->
