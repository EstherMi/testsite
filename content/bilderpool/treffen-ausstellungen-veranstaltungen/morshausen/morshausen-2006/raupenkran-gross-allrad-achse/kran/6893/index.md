---
layout: "image"
title: "Kran_1"
date: "2006-09-24T01:42:49"
picture: "kran01.jpg"
weight: "8"
konstrukteure: 
- "Kutsch"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/6893
imported:
- "2019"
_4images_image_id: "6893"
_4images_cat_id: "679"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T01:42:49"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6893 -->
