---
layout: "image"
title: "Kran_33"
date: "2006-09-24T01:43:34"
picture: "kran33.jpg"
weight: "40"
konstrukteure: 
- "Kutsch"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/6925
imported:
- "2019"
_4images_image_id: "6925"
_4images_cat_id: "679"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T01:43:34"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6925 -->
