---
layout: "image"
title: "Kran_11"
date: "2006-09-24T01:43:20"
picture: "kran11.jpg"
weight: "18"
konstrukteure: 
- "Kutsch"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/6903
imported:
- "2019"
_4images_image_id: "6903"
_4images_cat_id: "679"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T01:43:20"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6903 -->
