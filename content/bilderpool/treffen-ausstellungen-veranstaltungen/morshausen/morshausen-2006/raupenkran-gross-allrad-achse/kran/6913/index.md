---
layout: "image"
title: "Kran_21"
date: "2006-09-24T01:43:27"
picture: "kran21.jpg"
weight: "28"
konstrukteure: 
- "Kutsch"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/6913
imported:
- "2019"
_4images_image_id: "6913"
_4images_cat_id: "679"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T01:43:27"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6913 -->
