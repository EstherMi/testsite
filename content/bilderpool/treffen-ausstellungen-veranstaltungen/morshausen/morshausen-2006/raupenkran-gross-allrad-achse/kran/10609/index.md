---
layout: "image"
title: "Raupenkran groß Antrieb"
date: "2007-05-31T09:44:39"
picture: "raupenkrangross2.jpg"
weight: "2"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- details/10609
imported:
- "2019"
_4images_image_id: "10609"
_4images_cat_id: "679"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:44:39"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10609 -->
Raupenantrieb