---
layout: "image"
title: "RM14"
date: "2006-10-05T20:05:50"
picture: "RoboMax_014.jpg"
weight: "14"
konstrukteure: 
- "Thomas"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- details/7148
imported:
- "2019"
_4images_image_id: "7148"
_4images_cat_id: "670"
_4images_user_id: "473"
_4images_image_date: "2006-10-05T20:05:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7148 -->
