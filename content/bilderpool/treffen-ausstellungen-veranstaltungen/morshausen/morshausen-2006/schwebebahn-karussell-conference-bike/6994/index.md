---
layout: "image"
title: "Karussell"
date: "2006-09-25T23:12:49"
picture: "ralf3.jpg"
weight: "30"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/6994
imported:
- "2019"
_4images_image_id: "6994"
_4images_cat_id: "680"
_4images_user_id: "127"
_4images_image_date: "2006-09-25T23:12:49"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6994 -->
