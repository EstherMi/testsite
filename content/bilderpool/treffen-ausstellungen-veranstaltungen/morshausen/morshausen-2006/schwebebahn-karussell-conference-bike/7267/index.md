---
layout: "image"
title: "Schwebebahn"
date: "2006-10-29T19:01:47"
picture: "rg8.jpg"
weight: "25"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Rob van Baal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "robvanbaal"
license: "unknown"
legacy_id:
- details/7267
imported:
- "2019"
_4images_image_id: "7267"
_4images_cat_id: "680"
_4images_user_id: "379"
_4images_image_date: "2006-10-29T19:01:47"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7267 -->
