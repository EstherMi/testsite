---
layout: "image"
title: "Zepelin"
date: "2007-05-31T09:45:03"
picture: "schwebebahn04.jpg"
weight: "4"
konstrukteure: 
- "Ralf geerken"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- details/10619
imported:
- "2019"
_4images_image_id: "10619"
_4images_cat_id: "680"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:45:03"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10619 -->
