---
layout: "comment"
hidden: true
title: "2918"
date: "2007-04-05T17:03:25"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Stimmt, die linke Box ist nicht angeschlossen (die rechte schon). Da kann ich nur vermuten, dass sie kaputt ist und ein provisorischer Ersatz eingesetzt wurde.

Gruß,
Harald