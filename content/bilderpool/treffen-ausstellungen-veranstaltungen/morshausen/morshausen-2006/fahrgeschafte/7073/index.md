---
layout: "image"
title: "Fuhrpark_08.JPG"
date: "2006-10-02T16:10:47"
picture: "Fuhrpark_08.JPG"
weight: "15"
konstrukteure: 
- "Jan-Willem Dekker"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/7073
imported:
- "2019"
_4images_image_id: "7073"
_4images_cat_id: "668"
_4images_user_id: "4"
_4images_image_date: "2006-10-02T16:10:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7073 -->
