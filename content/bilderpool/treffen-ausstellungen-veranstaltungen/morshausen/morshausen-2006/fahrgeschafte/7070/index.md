---
layout: "image"
title: "Fuhrpark_04.JPG"
date: "2006-10-02T16:08:02"
picture: "Fuhrpark_04.JPG"
weight: "12"
konstrukteure: 
- "Jan-Willem Dekker"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/7070
imported:
- "2019"
_4images_image_id: "7070"
_4images_cat_id: "668"
_4images_user_id: "4"
_4images_image_date: "2006-10-02T16:08:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7070 -->
