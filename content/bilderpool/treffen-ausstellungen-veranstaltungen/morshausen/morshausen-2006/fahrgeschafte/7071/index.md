---
layout: "image"
title: "Fuhrpark_05.JPG"
date: "2006-10-02T16:08:19"
picture: "Fuhrpark_05.JPG"
weight: "13"
konstrukteure: 
- "Jan-Willem Dekker"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/7071
imported:
- "2019"
_4images_image_id: "7071"
_4images_cat_id: "668"
_4images_user_id: "4"
_4images_image_date: "2006-10-02T16:08:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7071 -->
