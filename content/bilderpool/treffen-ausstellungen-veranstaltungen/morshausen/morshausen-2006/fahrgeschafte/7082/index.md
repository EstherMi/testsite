---
layout: "image"
title: "WippTripp52.JPG"
date: "2006-10-02T16:15:21"
picture: "WippTripp52.JPG"
weight: "24"
konstrukteure: 
- "Jan-Willem Dekker"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/7082
imported:
- "2019"
_4images_image_id: "7082"
_4images_cat_id: "668"
_4images_user_id: "4"
_4images_image_date: "2006-10-02T16:15:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7082 -->
