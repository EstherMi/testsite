---
layout: "image"
title: "Kinderkarussell"
date: "2007-05-31T09:43:24"
picture: "fahrgeschaefte2.jpg"
weight: "2"
konstrukteure: 
- "Jan-Willem Dekker"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- details/10587
imported:
- "2019"
_4images_image_id: "10587"
_4images_cat_id: "668"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:43:24"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10587 -->
