---
layout: "image"
title: "Riesenrad"
date: "2007-05-31T09:43:24"
picture: "fahrgeschaefte3.jpg"
weight: "3"
konstrukteure: 
- "Jan-Willem Dekker"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- details/10588
imported:
- "2019"
_4images_image_id: "10588"
_4images_cat_id: "668"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:43:24"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10588 -->
