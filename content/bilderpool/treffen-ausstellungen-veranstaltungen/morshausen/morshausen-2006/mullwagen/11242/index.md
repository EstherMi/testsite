---
layout: "image"
title: "Müllwagen"
date: "2007-08-03T16:12:02"
picture: "Mrshausen-2006_047_2.jpg"
weight: "1"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/11242
imported:
- "2019"
_4images_image_id: "11242"
_4images_cat_id: "672"
_4images_user_id: "22"
_4images_image_date: "2007-08-03T16:12:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11242 -->
