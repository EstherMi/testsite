---
layout: "image"
title: "Die Mülleimer geht hoch"
date: "2006-10-29T14:54:37"
picture: "cwl3.jpg"
weight: "6"
konstrukteure: 
- "Claus-Werner Ludwig"
fotografen:
- "Rob van Baal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "robvanbaal"
license: "unknown"
legacy_id:
- details/7245
imported:
- "2019"
_4images_image_id: "7245"
_4images_cat_id: "672"
_4images_user_id: "379"
_4images_image_date: "2006-10-29T14:54:37"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7245 -->
