---
layout: "image"
title: "Die Mülleimer"
date: "2006-10-29T14:54:37"
picture: "cwl2.jpg"
weight: "5"
konstrukteure: 
- "Claus-Werner Ludwig"
fotografen:
- "Rob van Baal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "robvanbaal"
license: "unknown"
legacy_id:
- details/7244
imported:
- "2019"
_4images_image_id: "7244"
_4images_cat_id: "672"
_4images_user_id: "379"
_4images_image_date: "2006-10-29T14:54:37"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7244 -->
