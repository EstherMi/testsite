---
layout: "image"
title: "Pneumatik Bearbeitungszentrum"
date: "2006-10-29T14:54:37"
picture: "ap1.jpg"
weight: "2"
konstrukteure: 
- "Alfred Pettera"
fotografen:
- "Rob van Baal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "robvanbaal"
license: "unknown"
legacy_id:
- details/7240
imported:
- "2019"
_4images_image_id: "7240"
_4images_cat_id: "695"
_4images_user_id: "379"
_4images_image_date: "2006-10-29T14:54:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7240 -->
