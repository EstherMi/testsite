---
layout: "image"
title: "Industriemodell_2"
date: "2006-09-24T00:59:47"
picture: "jpeg2.jpg"
weight: "6"
konstrukteure: 
- "Fredy"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/6879
imported:
- "2019"
_4images_image_id: "6879"
_4images_cat_id: "678"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T00:59:47"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6879 -->
