---
layout: "image"
title: "Stacker57.JPG"
date: "2006-09-26T18:18:34"
picture: "Staker57.JPG"
weight: "3"
konstrukteure: 
- "Siegfried Kloster"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/7005
imported:
- "2019"
_4images_image_id: "7005"
_4images_cat_id: "669"
_4images_user_id: "4"
_4images_image_date: "2006-09-26T18:18:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7005 -->
