---
layout: "image"
title: "Verseilmaschine"
date: "2006-09-24T10:08:44"
picture: "Mrshausen-2006_050.jpg"
weight: "9"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Peter Damen ("
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/6952
imported:
- "2019"
_4images_image_id: "6952"
_4images_cat_id: "676"
_4images_user_id: "22"
_4images_image_date: "2006-09-24T10:08:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6952 -->
