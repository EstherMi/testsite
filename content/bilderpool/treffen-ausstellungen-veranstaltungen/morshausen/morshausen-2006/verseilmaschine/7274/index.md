---
layout: "image"
title: "Verseilmaschine"
date: "2006-10-29T19:02:12"
picture: "se2.jpg"
weight: "6"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Rob van Baal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "robvanbaal"
license: "unknown"
legacy_id:
- details/7274
imported:
- "2019"
_4images_image_id: "7274"
_4images_cat_id: "676"
_4images_user_id: "379"
_4images_image_date: "2006-10-29T19:02:12"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7274 -->
