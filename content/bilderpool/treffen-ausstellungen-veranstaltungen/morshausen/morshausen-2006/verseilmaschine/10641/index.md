---
layout: "image"
title: "Verseilmaschine"
date: "2007-05-31T09:45:33"
picture: "verseilmaschine2.jpg"
weight: "2"
konstrukteure: 
- "Sven Engelke & Ralf Geerken"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- details/10641
imported:
- "2019"
_4images_image_id: "10641"
_4images_cat_id: "676"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:45:33"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10641 -->
Aufwicklung