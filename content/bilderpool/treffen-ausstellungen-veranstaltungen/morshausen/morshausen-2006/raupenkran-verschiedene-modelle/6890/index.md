---
layout: "image"
title: "Traktor"
date: "2006-09-24T01:20:24"
picture: "jpeg11.jpg"
weight: "35"
konstrukteure: 
- "Holger Howey"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/6890
imported:
- "2019"
_4images_image_id: "6890"
_4images_cat_id: "660"
_4images_user_id: "127"
_4images_image_date: "2006-09-24T01:20:24"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6890 -->
