---
layout: "image"
title: "Schwebebahn"
date: "2007-05-31T09:45:20"
picture: "verschmodelle4.jpg"
weight: "4"
konstrukteure: 
- "Holger Howey"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- details/10635
imported:
- "2019"
_4images_image_id: "10635"
_4images_cat_id: "660"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:45:20"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10635 -->
von Oben