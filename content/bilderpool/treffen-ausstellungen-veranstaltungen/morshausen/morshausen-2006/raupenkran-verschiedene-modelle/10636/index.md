---
layout: "image"
title: "Schwebebahn"
date: "2007-05-31T09:45:33"
picture: "verschmodelle5.jpg"
weight: "5"
konstrukteure: 
- "Holger Howey"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- details/10636
imported:
- "2019"
_4images_image_id: "10636"
_4images_cat_id: "660"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:45:33"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10636 -->
von Rechts