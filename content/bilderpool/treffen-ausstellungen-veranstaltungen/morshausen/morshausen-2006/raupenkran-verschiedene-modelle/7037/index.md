---
layout: "image"
title: "Holgers Kran"
date: "2006-10-01T14:11:26"
picture: "Mrshausen_009.jpg"
weight: "12"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/7037
imported:
- "2019"
_4images_image_id: "7037"
_4images_cat_id: "660"
_4images_user_id: "130"
_4images_image_date: "2006-10-01T14:11:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7037 -->
Eine Menge Grundbauplatten als Ballast.