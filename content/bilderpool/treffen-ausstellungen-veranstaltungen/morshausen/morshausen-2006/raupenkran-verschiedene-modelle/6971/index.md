---
layout: "image"
title: "Kran_1"
date: "2006-09-25T22:45:19"
picture: "holger1.jpg"
weight: "19"
konstrukteure: 
- "Holger Howey"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/6971
imported:
- "2019"
_4images_image_id: "6971"
_4images_cat_id: "660"
_4images_user_id: "127"
_4images_image_date: "2006-09-25T22:45:19"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6971 -->
