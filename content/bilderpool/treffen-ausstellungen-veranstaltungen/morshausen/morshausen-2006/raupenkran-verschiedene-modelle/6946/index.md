---
layout: "image"
title: "Schwebebahn Holger Howey"
date: "2006-09-24T10:08:44"
picture: "Mrshausen-2006_068.jpg"
weight: "30"
konstrukteure: 
- "Holger Howey"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/6946
imported:
- "2019"
_4images_image_id: "6946"
_4images_cat_id: "660"
_4images_user_id: "22"
_4images_image_date: "2006-09-24T10:08:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6946 -->
