---
layout: "image"
title: "Containerterminal_4"
date: "2006-09-25T22:52:56"
picture: "warwel4.jpg"
weight: "18"
konstrukteure: 
- "Jürgen Warwel"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/6983
imported:
- "2019"
_4images_image_id: "6983"
_4images_cat_id: "673"
_4images_user_id: "127"
_4images_image_date: "2006-09-25T22:52:56"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6983 -->
