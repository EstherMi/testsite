---
layout: "image"
title: "Containerterminal"
date: "2007-05-31T09:43:24"
picture: "containerterminal09.jpg"
weight: "9"
konstrukteure: 
- "Jürgen Warwel"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- details/10584
imported:
- "2019"
_4images_image_id: "10584"
_4images_cat_id: "673"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:43:24"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10584 -->
