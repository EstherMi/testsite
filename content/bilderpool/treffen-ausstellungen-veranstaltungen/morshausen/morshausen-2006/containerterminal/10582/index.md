---
layout: "image"
title: "Containerterminal"
date: "2007-05-31T09:43:24"
picture: "containerterminal07.jpg"
weight: "7"
konstrukteure: 
- "Jürgen Warwel"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- details/10582
imported:
- "2019"
_4images_image_id: "10582"
_4images_cat_id: "673"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:43:24"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10582 -->
Volle Höhe