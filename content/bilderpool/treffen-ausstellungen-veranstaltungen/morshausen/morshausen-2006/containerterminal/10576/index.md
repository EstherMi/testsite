---
layout: "image"
title: "Containerterminal"
date: "2007-05-31T09:43:07"
picture: "containerterminal01.jpg"
weight: "1"
konstrukteure: 
- "Jürgen Warwel"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- details/10576
imported:
- "2019"
_4images_image_id: "10576"
_4images_cat_id: "673"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:43:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10576 -->
Container stapeln