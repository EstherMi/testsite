---
layout: "image"
title: "Containerterminal"
date: "2007-05-31T09:43:07"
picture: "containerterminal03.jpg"
weight: "3"
konstrukteure: 
- "Jürgen Warwel"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- details/10578
imported:
- "2019"
_4images_image_id: "10578"
_4images_cat_id: "673"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:43:07"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10578 -->
