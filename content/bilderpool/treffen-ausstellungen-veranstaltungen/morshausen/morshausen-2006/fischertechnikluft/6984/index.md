---
layout: "image"
title: "Fabse mit Kaffeetasse"
date: "2006-09-25T22:57:21"
picture: "luft1.jpg"
weight: "7"
konstrukteure: 
- "Fischertechnikluft"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/6984
imported:
- "2019"
_4images_image_id: "6984"
_4images_cat_id: "677"
_4images_user_id: "127"
_4images_image_date: "2006-09-25T22:57:21"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6984 -->
