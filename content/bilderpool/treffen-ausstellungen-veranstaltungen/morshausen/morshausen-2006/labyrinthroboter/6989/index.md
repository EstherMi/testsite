---
layout: "image"
title: "Roboter im Labyrint_1"
date: "2006-09-25T23:01:37"
picture: "who2.jpg"
weight: "2"
konstrukteure: 
- "MisterWho"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fabse"
license: "unknown"
legacy_id:
- details/6989
imported:
- "2019"
_4images_image_id: "6989"
_4images_cat_id: "671"
_4images_user_id: "127"
_4images_image_date: "2006-09-25T23:01:37"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6989 -->
