---
layout: "image"
title: "Bild005"
date: "2003-05-03T10:34:42"
picture: "Bild005.jpg"
weight: "1"
konstrukteure: 
- "NN"
fotografen:
- "NN"
keywords: ["Hubschrauber"]
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/1011
imported:
- "2019"
_4images_image_id: "1011"
_4images_cat_id: "96"
_4images_user_id: "1"
_4images_image_date: "2003-05-03T10:34:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1011 -->
