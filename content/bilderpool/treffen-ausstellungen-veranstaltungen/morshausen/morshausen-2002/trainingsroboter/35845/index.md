---
layout: "image"
title: "Training Roboter modified"
date: "2012-10-08T12:22:15"
picture: "DSC01347.jpg"
weight: "2"
konstrukteure: 
- "Marspau & R.R.Bidding"
fotografen:
- "Marspau"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "marspau"
license: "unknown"
legacy_id:
- details/35845
imported:
- "2019"
_4images_image_id: "35845"
_4images_cat_id: "108"
_4images_user_id: "416"
_4images_image_date: "2012-10-08T12:22:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35845 -->
Details of the pulse wheels. 
the original #32367 with 36 bars could not be use with RoboPro,it was 
missing pulses.So i made bar patterns on paper wrapp around with 16 bars. 
Those one works well 

Details zu den Impuls-Räder. 
das Original # 32367 mit 36 &#8203;&#8203;Bars konnte nicht mit RoboPro werden zu verwenden, war es
 fehlende pulses.So i made bar Muster auf Papier Wickel um mit 16 bar. 
Wer ein gut funktioniert