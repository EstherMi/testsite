---
layout: "comment"
hidden: true
title: "17423"
date: "2012-10-08T19:07:04"
uploadBy:
- "marspau"
license: "unknown"
imported:
- "2019"
---
The description at the bottom of the picture is the wrong one. 
The Fotograf should be: Marspau. 
The Konstrukteur should be: Marspau & R.R.Budding. 

Die Beschreibung auf der Unterseite des Bildes ist die falsche. 
Der Fotograf sollte sein: Marspau. 
Der Konstrukteur sollte sein: Marspau & RRBudding