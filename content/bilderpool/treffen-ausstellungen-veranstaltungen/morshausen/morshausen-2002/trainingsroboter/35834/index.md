---
layout: "image"
title: "Training Roboter modified"
date: "2012-10-07T21:42:33"
picture: "DSC01346.jpg"
weight: "4"
konstrukteure: 
- "Marspau & R.R.Budding"
fotografen:
- "Marspau"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "marspau"
license: "unknown"
legacy_id:
- details/35834
imported:
- "2019"
_4images_image_id: "35834"
_4images_cat_id: "108"
_4images_user_id: "416"
_4images_image_date: "2012-10-07T21:42:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35834 -->
My Training Roboter,with electronik circuits to allow the  original 5V
 optical readers #32357,to be use with 9V outpouts of the TXC.
 Thanks to Richard R.Budding,for the conception of the circuit.

 
Mein Training Roboter, mit integrierter Elektronik-Schaltungen, damit der ursprüngliche 5V
  optische Leser # 32357, um den Einsatz mit 9V outpouts der TXC sein.
  Dank Richard R.Budding, für die Konzeption der Schaltung.