---
layout: "image"
title: "Training Roboter II with Gripper"
date: "2011-08-16T01:55:11"
picture: "DSC00833.jpg"
weight: "7"
konstrukteure: 
- "marspau"
fotografen:
- "Marspau"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "marspau"
license: "unknown"
legacy_id:
- details/31584
imported:
- "2019"
_4images_image_id: "31584"
_4images_cat_id: "108"
_4images_user_id: "416"
_4images_image_date: "2011-08-16T01:55:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31584 -->
Detail of the fixation of the gripper to the arm.


Detail der Befestigung des Greifers auf dem Arm