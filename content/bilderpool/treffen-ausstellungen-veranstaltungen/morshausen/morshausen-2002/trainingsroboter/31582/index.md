---
layout: "image"
title: "Training Roboter II with Gripper"
date: "2011-08-15T16:40:18"
picture: "DSC00835.jpg"
weight: "8"
konstrukteure: 
- "Marspau"
fotografen:
- "marspau"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "marspau"
license: "unknown"
legacy_id:
- details/31582
imported:
- "2019"
_4images_image_id: "31582"
_4images_cat_id: "108"
_4images_user_id: "416"
_4images_image_date: "2011-08-15T16:40:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31582 -->
This is the same training roboter II,with a modern gripper.
The original gripper I could not program with RoboPro.
So I built a gripper,inspired by one of the models of the Automation Robots.
This is easier to program with RoBoPro


Dies ist die gleiche Ausbildung roboter II, mit einer modernen Greifer.
 Die ursprüngliche Greifer konnte ich nicht mit RoboPro Programm.
 Also baute ich einen Greifer, von einem der Modelle der Automation Roboter inspiriert.
 Dies ist einfacher zu programmieren mit RoboPro.