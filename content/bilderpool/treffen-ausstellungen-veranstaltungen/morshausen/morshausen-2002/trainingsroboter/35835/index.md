---
layout: "image"
title: "Training Roboter modified"
date: "2012-10-07T23:20:05"
picture: "DSC01348.jpg"
weight: "5"
konstrukteure: 
- "Marspau & R.R.Budding"
fotografen:
- "Marspau"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "marspau"
license: "unknown"
legacy_id:
- details/35835
imported:
- "2019"
_4images_image_id: "35835"
_4images_cat_id: "108"
_4images_user_id: "416"
_4images_image_date: "2012-10-07T23:20:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35835 -->
Under those black wrappings are the adapter circuits. 

Unter diesen schwarzen Verpackungen sind die Adapter Schaltungen