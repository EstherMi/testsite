---
layout: "image"
title: "Training Roboter II with Gripper"
date: "2011-08-15T16:40:18"
picture: "DSC00834.jpg"
weight: "9"
konstrukteure: 
- "Marspau"
fotografen:
- "Marspau"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "marspau"
license: "unknown"
legacy_id:
- details/31583
imported:
- "2019"
_4images_image_id: "31583"
_4images_cat_id: "108"
_4images_user_id: "416"
_4images_image_date: "2011-08-15T16:40:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31583 -->
Details of the pulse counter
and the limit switch.


Details der Impulszähler
 und der Endschalter