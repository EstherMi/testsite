---
layout: "comment"
hidden: true
title: "23606"
date: "2017-09-04T16:32:59"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
An axial piston pump has a number of pistons (usually an odd number) arranged in a circular array within a housing which is commonly referred to as a cylinder block, rotor or barrel. This cylinder block is driven to rotate about its axis of symmetry by an integral shaft that is, more or less, aligned with the pumping pistons.

Interesssante Links zum Nachbau-Versuch :
https://www.youtube.com/watch?v=-GZ5GEIrHkg&list=PLB96E97514E04836E&index=7

http://www.mekanizmalar.com/fixed_swashplate_rotating_barrel.html

Gruss, 

Peter Poederoyen NL