---
layout: "image"
title: "Drehmaschine"
date: "2009-10-01T19:18:52"
picture: "ftconventioninmoehrhausen04.jpg"
weight: "4"
konstrukteure: 
- "Verschiedene"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/25454
imported:
- "2019"
_4images_image_id: "25454"
_4images_cat_id: "1782"
_4images_user_id: "374"
_4images_image_date: "2009-10-01T19:18:52"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25454 -->
Spannbackenfutter der Drehmaschine