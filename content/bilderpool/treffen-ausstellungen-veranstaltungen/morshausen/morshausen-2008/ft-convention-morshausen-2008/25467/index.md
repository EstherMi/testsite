---
layout: "image"
title: "Flugzeug von Harald"
date: "2009-10-01T19:18:53"
picture: "ftconventioninmoehrhausen17.jpg"
weight: "17"
konstrukteure: 
- "Verschiedene"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/25467
imported:
- "2019"
_4images_image_id: "25467"
_4images_cat_id: "1782"
_4images_user_id: "374"
_4images_image_date: "2009-10-01T19:18:53"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25467 -->
Der hintere Tragflügel