---
layout: "image"
title: "Spitze mit Ausleger eines Krans"
date: "2009-10-01T19:18:53"
picture: "ftconventioninmoehrhausen13.jpg"
weight: "13"
konstrukteure: 
- "Verschiedene"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/25463
imported:
- "2019"
_4images_image_id: "25463"
_4images_cat_id: "1782"
_4images_user_id: "374"
_4images_image_date: "2009-10-01T19:18:53"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25463 -->
