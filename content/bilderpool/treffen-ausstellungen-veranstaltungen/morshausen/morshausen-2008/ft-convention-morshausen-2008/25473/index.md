---
layout: "image"
title: "Containergreifer"
date: "2009-10-01T19:18:53"
picture: "ftconventioninmoehrhausen23.jpg"
weight: "23"
konstrukteure: 
- "Verschiedene"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/25473
imported:
- "2019"
_4images_image_id: "25473"
_4images_cat_id: "1782"
_4images_user_id: "374"
_4images_image_date: "2009-10-01T19:18:53"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25473 -->
Der Greifer hat den Container erfasst