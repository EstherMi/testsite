---
layout: "image"
title: "Ein tolles gerät ohne zweck."
date: "2008-09-22T15:37:55"
picture: "moershausen25.jpg"
weight: "3"
konstrukteure: 
- "Rob van Oostenbrugge"
fotografen:
- "Rob van Oostenbrugge"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Tovenaar"
license: "unknown"
legacy_id:
- details/15449
imported:
- "2019"
_4images_image_id: "15449"
_4images_cat_id: "1412"
_4images_user_id: "814"
_4images_image_date: "2008-09-22T15:37:55"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15449 -->
