---
layout: "image"
title: "Ballweiterreicher"
date: "2008-09-22T07:43:46"
picture: "Ballweiterreicher.jpg"
weight: "2"
konstrukteure: 
- "Fritz Roller"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/15402
imported:
- "2019"
_4images_image_id: "15402"
_4images_cat_id: "1412"
_4images_user_id: "724"
_4images_image_date: "2008-09-22T07:43:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15402 -->
