---
layout: "image"
title: "Moershausen Models"
date: "2008-10-03T18:59:06"
picture: "m_brick_sorter-3.jpg"
weight: "2"
konstrukteure: 
- "Other"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Moershausen", "2008"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/15815
imported:
- "2019"
_4images_image_id: "15815"
_4images_cat_id: "1425"
_4images_user_id: "585"
_4images_image_date: "2008-10-03T18:59:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15815 -->
These are some pics of models displayed at Moershausen 2008. Thought to share.