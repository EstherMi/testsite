---
layout: "image"
title: "Kaiser Wilhelm Brücke"
date: "2008-09-23T07:43:24"
picture: "convention21.jpg"
weight: "3"
konstrukteure: 
- "Stephan"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/15489
imported:
- "2019"
_4images_image_id: "15489"
_4images_cat_id: "1415"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:24"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15489 -->
