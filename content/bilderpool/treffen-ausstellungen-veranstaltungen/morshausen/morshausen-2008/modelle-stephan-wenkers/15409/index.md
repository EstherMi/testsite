---
layout: "image"
title: "Stephan Wenkers mit Kaiser Wilhelm Brücke"
date: "2008-09-22T07:43:46"
picture: "Stephan_Wenkers_mit_Kaiser_Wilhelm_Brcke.jpg"
weight: "5"
konstrukteure: 
- "Stephan Wenkers"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/15409
imported:
- "2019"
_4images_image_id: "15409"
_4images_cat_id: "1415"
_4images_user_id: "724"
_4images_image_date: "2008-09-22T07:43:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15409 -->
