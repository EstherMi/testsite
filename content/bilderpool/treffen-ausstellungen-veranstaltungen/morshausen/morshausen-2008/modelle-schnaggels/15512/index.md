---
layout: "image"
title: "Boot"
date: "2008-09-23T07:43:24"
picture: "convention44.jpg"
weight: "7"
konstrukteure: 
- "schnaggels"
fotografen:
- "speedy68"
keywords: ["Malteser"]
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/15512
imported:
- "2019"
_4images_image_id: "15512"
_4images_cat_id: "1421"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:24"
_4images_image_order: "44"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15512 -->
