---
layout: "image"
title: "Top Spin von Speedy 68"
date: "2008-09-23T07:43:23"
picture: "ts2.jpg"
weight: "6"
konstrukteure: 
- "Thomas Falkenberg (Speedy 68)"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/15467
imported:
- "2019"
_4images_image_id: "15467"
_4images_cat_id: "1413"
_4images_user_id: "130"
_4images_image_date: "2008-09-23T07:43:23"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15467 -->
Klasse gebaut die Gondel.