---
layout: "image"
title: "Schalter"
date: "2008-10-25T14:26:41"
picture: "speed1.jpg"
weight: "1"
konstrukteure: 
- "Speedy"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/16068
imported:
- "2019"
_4images_image_id: "16068"
_4images_cat_id: "1413"
_4images_user_id: "453"
_4images_image_date: "2008-10-25T14:26:41"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16068 -->
