---
layout: "image"
title: "Winkelfuß von Kirmesmodell"
date: "2008-09-22T07:43:46"
picture: "Winkelfu_von_Kirmesmodell.jpg"
weight: "13"
konstrukteure: 
- "Speedy68"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/15407
imported:
- "2019"
_4images_image_id: "15407"
_4images_cat_id: "1413"
_4images_user_id: "724"
_4images_image_date: "2008-09-22T07:43:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15407 -->
