---
layout: "image"
title: "Links Stefan Roth von Fisherfriendwoman"
date: "2008-09-22T07:43:46"
picture: "Links_Stefan_Roth_von_Fisherfriendwoman.jpg"
weight: "11"
konstrukteure: 
- "Speedy68"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/15405
imported:
- "2019"
_4images_image_id: "15405"
_4images_cat_id: "1413"
_4images_user_id: "724"
_4images_image_date: "2008-09-22T07:43:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15405 -->
