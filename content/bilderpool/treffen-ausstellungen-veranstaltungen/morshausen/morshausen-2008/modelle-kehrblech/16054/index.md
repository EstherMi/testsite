---
layout: "image"
title: "4 Gewinnt"
date: "2008-10-25T14:26:26"
picture: "kehrblech1.jpg"
weight: "1"
konstrukteure: 
- "kehrblech"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/16054
imported:
- "2019"
_4images_image_id: "16054"
_4images_cat_id: "1410"
_4images_user_id: "453"
_4images_image_date: "2008-10-25T14:26:26"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16054 -->
