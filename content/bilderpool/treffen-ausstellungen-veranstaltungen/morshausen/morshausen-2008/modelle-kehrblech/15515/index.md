---
layout: "image"
title: "4 Gewinnt Roboter"
date: "2008-09-23T07:43:24"
picture: "convention47.jpg"
weight: "3"
konstrukteure: 
- "Kehrblech"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/15515
imported:
- "2019"
_4images_image_id: "15515"
_4images_cat_id: "1410"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:24"
_4images_image_order: "47"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15515 -->
