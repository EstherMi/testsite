---
layout: "image"
title: "4 Gewinnt Roboter aus der Nähe"
date: "2008-09-22T07:43:46"
picture: "4_Gewinnt_Roboter.jpg"
weight: "6"
konstrukteure: 
- "Kehrblech"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/15398
imported:
- "2019"
_4images_image_id: "15398"
_4images_cat_id: "1410"
_4images_user_id: "724"
_4images_image_date: "2008-09-22T07:43:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15398 -->
