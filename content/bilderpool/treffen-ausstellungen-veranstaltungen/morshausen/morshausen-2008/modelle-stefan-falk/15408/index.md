---
layout: "image"
title: "Stefan Falk mit Digitaluhr - Rechts ist Severin"
date: "2008-09-22T07:43:46"
picture: "Stefan_Falk_mit_Digitaluhr_-_Rechts_ist_Severin.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/15408
imported:
- "2019"
_4images_image_id: "15408"
_4images_cat_id: "1414"
_4images_user_id: "724"
_4images_image_date: "2008-09-22T07:43:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15408 -->
