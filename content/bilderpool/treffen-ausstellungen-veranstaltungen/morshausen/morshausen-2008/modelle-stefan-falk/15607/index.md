---
layout: "image"
title: "Digitaluhr"
date: "2008-09-25T17:47:41"
picture: "conv02.jpg"
weight: "2"
konstrukteure: 
- "Steffalk"
fotografen:
- "Heiko"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/15607
imported:
- "2019"
_4images_image_id: "15607"
_4images_cat_id: "1414"
_4images_user_id: "453"
_4images_image_date: "2008-09-25T17:47:41"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15607 -->
