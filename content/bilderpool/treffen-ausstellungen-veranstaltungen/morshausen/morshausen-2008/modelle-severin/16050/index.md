---
layout: "image"
title: "Plotter"
date: "2008-10-25T14:26:26"
picture: "severin1.jpg"
weight: "1"
konstrukteure: 
- "Severin"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/16050
imported:
- "2019"
_4images_image_id: "16050"
_4images_cat_id: "1436"
_4images_user_id: "453"
_4images_image_date: "2008-10-25T14:26:26"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16050 -->
