---
layout: "comment"
hidden: true
title: "7406"
date: "2008-10-01T10:02:37"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hatte den von mir aus dem Original-Baukasten aufgebauten Robo Explorer laufen. Weiche Beläge zum Wenden, Kunststoffachsen und runter springende Impulsräder ...
So schnell habe ich noch kein ft-Modell wieder zerlegt.
Das Thema zum mechanischen Kettenantrieb macht die ft-fans kreativ munter.
Das ist hier eine sehr gute Lösung!
Gruss, Udo2