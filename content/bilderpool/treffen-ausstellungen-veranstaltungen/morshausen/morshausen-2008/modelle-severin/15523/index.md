---
layout: "image"
title: "Sechsachsroboter"
date: "2008-09-23T07:43:24"
picture: "convention55.jpg"
weight: "11"
konstrukteure: 
- "Severin"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/15523
imported:
- "2019"
_4images_image_id: "15523"
_4images_cat_id: "1436"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:24"
_4images_image_order: "55"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15523 -->
