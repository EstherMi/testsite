---
layout: "image"
title: "Kabelaufwickler"
date: "2008-09-22T07:43:46"
picture: "Kabelaufwickler_von_Thanks_for_the_fish.jpg"
weight: "4"
konstrukteure: 
- "Thanks for the fish"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/15411
imported:
- "2019"
_4images_image_id: "15411"
_4images_cat_id: "1416"
_4images_user_id: "724"
_4images_image_date: "2008-09-22T07:43:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15411 -->
