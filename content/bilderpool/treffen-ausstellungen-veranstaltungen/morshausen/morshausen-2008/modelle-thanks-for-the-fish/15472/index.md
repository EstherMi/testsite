---
layout: "image"
title: "Convention 2008"
date: "2008-09-23T07:43:23"
picture: "convention04.jpg"
weight: "1"
konstrukteure: 
- "Verschiedene"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/15472
imported:
- "2019"
_4images_image_id: "15472"
_4images_cat_id: "1416"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:23"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15472 -->
