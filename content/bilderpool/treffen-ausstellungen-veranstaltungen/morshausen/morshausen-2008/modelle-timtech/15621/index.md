---
layout: "image"
title: "Kräne"
date: "2008-09-25T17:47:42"
picture: "conv16.jpg"
weight: "1"
konstrukteure: 
- "fitec/timtech"
fotografen:
- "Heiko"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/15621
imported:
- "2019"
_4images_image_id: "15621"
_4images_cat_id: "1419"
_4images_user_id: "453"
_4images_image_date: "2008-09-25T17:47:42"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15621 -->
