---
layout: "image"
title: "2 Endschalter für 1 Motor ohne Interface - Mit Diode"
date: "2008-09-22T07:43:48"
picture: "2_Endschalter_fr_1_Motor_ohne_Interface_-_Mit_Diode.jpg"
weight: "4"
konstrukteure: 
- "Timtech"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/15419
imported:
- "2019"
_4images_image_id: "15419"
_4images_cat_id: "1419"
_4images_user_id: "724"
_4images_image_date: "2008-09-22T07:43:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15419 -->
Eingesetzt zur Abschaltung einer Richtung beim Kran.