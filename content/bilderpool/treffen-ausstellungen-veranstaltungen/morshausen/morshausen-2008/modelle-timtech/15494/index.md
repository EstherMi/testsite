---
layout: "image"
title: "Sortiermaschine"
date: "2008-09-23T07:43:24"
picture: "convention26.jpg"
weight: "3"
konstrukteure: 
- "Timtech"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/15494
imported:
- "2019"
_4images_image_id: "15494"
_4images_cat_id: "1419"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:24"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15494 -->
