---
layout: "image"
title: "Fahrwerk"
date: "2008-10-25T14:26:56"
picture: "dirk1.jpg"
weight: "1"
konstrukteure: 
- "Dirk"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/16072
imported:
- "2019"
_4images_image_id: "16072"
_4images_cat_id: "1408"
_4images_user_id: "453"
_4images_image_date: "2008-10-25T14:26:56"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16072 -->
