---
layout: "image"
title: "Tieflader"
date: "2008-09-23T07:43:24"
picture: "convention70.jpg"
weight: "18"
konstrukteure: 
- "gulligan"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/15538
imported:
- "2019"
_4images_image_id: "15538"
_4images_cat_id: "1408"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:24"
_4images_image_order: "70"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15538 -->
die hinteren Räder lenken auch mit