---
layout: "image"
title: "LKW"
date: "2008-09-23T07:43:23"
picture: "convention11.jpg"
weight: "3"
konstrukteure: 
- "Claus"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/15479
imported:
- "2019"
_4images_image_id: "15479"
_4images_cat_id: "1402"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:23"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15479 -->
Er wurde mit dem neuen Control Set gesteuert