---
layout: "image"
title: "Drehbank"
date: "2008-09-23T07:43:23"
picture: "convention09.jpg"
weight: "1"
konstrukteure: 
- "Claus"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/15477
imported:
- "2019"
_4images_image_id: "15477"
_4images_cat_id: "1402"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:23"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15477 -->
So wie ich mich erinnern kann hat die Drehbank 18 Gänge, die man mittels der Hebel während dem Betrieb umschalten kann. Mit Vorschub für den Schlitten...
Vielleicht kann ja Claus noch eine genauere Beschreibung abgeben.