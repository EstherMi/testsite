---
layout: "image"
title: "Truck mit Wahsager und Speedy68"
date: "2008-09-22T07:43:47"
picture: "Truck_mit_Wahsager_und_Speedy68.jpg"
weight: "7"
konstrukteure: 
- "Wahsager"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/15416
imported:
- "2019"
_4images_image_id: "15416"
_4images_cat_id: "1418"
_4images_user_id: "724"
_4images_image_date: "2008-09-22T07:43:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15416 -->
