---
layout: "comment"
hidden: true
title: "22319"
date: "2016-07-28T09:00:41"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Es geht die Kunde,
Aus profundem Munde,
Daß ein gewisser [b] thkais [/b],
Das gebaut hat mit ganz viel Schweiß.

Rob und Masked bauen zwar auch tolle Sachen,
Trotzdem, kann mal ein Admin die Änderungen machen?

Grüße
H.A.R.R.Y.