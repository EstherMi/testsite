---
layout: "image"
title: "Quadrokopter 1"
date: "2008-09-22T15:37:55"
picture: "moershausen02.jpg"
weight: "6"
konstrukteure: 
- "thkais"
fotografen:
- "Rob van Oostenbrugge"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Tovenaar"
license: "unknown"
legacy_id:
- details/15426
imported:
- "2019"
_4images_image_id: "15426"
_4images_cat_id: "1399"
_4images_user_id: "814"
_4images_image_date: "2008-09-22T15:37:55"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15426 -->
