---
layout: "image"
title: "Turn-Up75.JPG"
date: "2008-09-23T10:08:05"
picture: "Turn-Up75.JPG"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/15559
imported:
- "2019"
_4images_image_id: "15559"
_4images_cat_id: "1398"
_4images_user_id: "4"
_4images_image_date: "2008-09-23T10:08:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15559 -->
