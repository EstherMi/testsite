---
layout: "image"
title: "Seilbahn mit Triceratops"
date: "2008-09-22T07:43:47"
picture: "Seilbahn_mit_Triceratops.jpg"
weight: "11"
konstrukteure: 
- "Triceratops"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/15414
imported:
- "2019"
_4images_image_id: "15414"
_4images_cat_id: "1417"
_4images_user_id: "724"
_4images_image_date: "2008-09-22T07:43:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15414 -->
