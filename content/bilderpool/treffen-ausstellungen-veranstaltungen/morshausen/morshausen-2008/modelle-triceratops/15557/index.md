---
layout: "image"
title: "Uhr06.jpg"
date: "2008-09-23T10:06:38"
picture: "Triceratops_Uhr_06.JPG"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/15557
imported:
- "2019"
_4images_image_id: "15557"
_4images_cat_id: "1417"
_4images_user_id: "4"
_4images_image_date: "2008-09-23T10:06:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15557 -->
