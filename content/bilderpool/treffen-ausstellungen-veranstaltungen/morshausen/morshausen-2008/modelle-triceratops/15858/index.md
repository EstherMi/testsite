---
layout: "image"
title: "Moershausen Models"
date: "2008-10-10T08:58:48"
picture: "m_ski_lift.jpg"
weight: "4"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Moershausen", "2008"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/15858
imported:
- "2019"
_4images_image_id: "15858"
_4images_cat_id: "1417"
_4images_user_id: "585"
_4images_image_date: "2008-10-10T08:58:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15858 -->
These are some pics of models displayed at Moershausen 2008. Thought to share.