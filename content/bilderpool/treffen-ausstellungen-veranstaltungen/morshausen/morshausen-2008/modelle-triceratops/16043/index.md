---
layout: "image"
title: "Steuerung"
date: "2008-10-25T14:26:12"
picture: "triceratops1.jpg"
weight: "1"
konstrukteure: 
- "Triceratops"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/16043
imported:
- "2019"
_4images_image_id: "16043"
_4images_cat_id: "1417"
_4images_user_id: "453"
_4images_image_date: "2008-10-25T14:26:12"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16043 -->
