---
layout: "image"
title: "Hochregallager mit Fitec"
date: "2008-09-22T07:43:45"
picture: "Fitec_mit_Hochregallager.jpg"
weight: "16"
konstrukteure: 
- "Fitec"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/15393
imported:
- "2019"
_4images_image_id: "15393"
_4images_cat_id: "1407"
_4images_user_id: "724"
_4images_image_date: "2008-09-22T07:43:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15393 -->
