---
layout: "image"
title: "Moershausen Model"
date: "2008-10-07T22:29:13"
picture: "m_cranes.jpg"
weight: "11"
konstrukteure: 
- "Other"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Moershausen", "2008"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/15834
imported:
- "2019"
_4images_image_id: "15834"
_4images_cat_id: "1407"
_4images_user_id: "585"
_4images_image_date: "2008-10-07T22:29:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15834 -->
These are some pics of models displayed at Moershausen 2008. Thought to share.