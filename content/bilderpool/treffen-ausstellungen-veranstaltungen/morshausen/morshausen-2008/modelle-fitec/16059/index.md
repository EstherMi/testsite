---
layout: "image"
title: "Industriestraße"
date: "2008-10-25T14:26:26"
picture: "fitec02.jpg"
weight: "2"
konstrukteure: 
- "fitec"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/16059
imported:
- "2019"
_4images_image_id: "16059"
_4images_cat_id: "1407"
_4images_user_id: "453"
_4images_image_date: "2008-10-25T14:26:26"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16059 -->
