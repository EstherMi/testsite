---
layout: "image"
title: "Trecker"
date: "2008-10-25T14:26:41"
picture: "fitec05.jpg"
weight: "5"
konstrukteure: 
- "fitec"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/16062
imported:
- "2019"
_4images_image_id: "16062"
_4images_cat_id: "1407"
_4images_user_id: "453"
_4images_image_date: "2008-10-25T14:26:41"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16062 -->
