---
layout: "image"
title: "Kräne"
date: "2008-09-23T07:43:24"
picture: "convention30.jpg"
weight: "15"
konstrukteure: 
- "Timtech oder/und fitec ?"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/15498
imported:
- "2019"
_4images_image_id: "15498"
_4images_cat_id: "1407"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:24"
_4images_image_order: "30"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15498 -->
