---
layout: "image"
title: "Ball aus Statiksteinen"
date: "2008-09-22T07:43:47"
picture: "Ball_aus_Statiksteinen.jpg"
weight: "9"
konstrukteure: 
- "Tovenaar"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/15413
imported:
- "2019"
_4images_image_id: "15413"
_4images_cat_id: "1405"
_4images_user_id: "724"
_4images_image_date: "2008-09-22T07:43:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15413 -->
