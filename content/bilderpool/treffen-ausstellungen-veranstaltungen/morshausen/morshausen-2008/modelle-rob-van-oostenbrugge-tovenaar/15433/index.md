---
layout: "image"
title: "Kinder 1"
date: "2008-09-22T15:37:55"
picture: "moershausen09.jpg"
weight: "14"
konstrukteure: 
- "Rob van Oostenbrugge"
fotografen:
- "Rob van Oostenbrugge"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Tovenaar"
license: "unknown"
legacy_id:
- details/15433
imported:
- "2019"
_4images_image_id: "15433"
_4images_cat_id: "1405"
_4images_user_id: "814"
_4images_image_date: "2008-09-22T15:37:55"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15433 -->
Weiss du noch als du klein war