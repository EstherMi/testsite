---
layout: "image"
title: "Moershausen 2008"
date: "2008-10-07T22:42:03"
picture: "m_machines.jpg"
weight: "4"
konstrukteure: 
- "Other"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Moershausen", "2008"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/15838
imported:
- "2019"
_4images_image_id: "15838"
_4images_cat_id: "1405"
_4images_user_id: "585"
_4images_image_date: "2008-10-07T22:42:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15838 -->
hese are some pics of models displayed at Moershausen 2008. Thought to share.