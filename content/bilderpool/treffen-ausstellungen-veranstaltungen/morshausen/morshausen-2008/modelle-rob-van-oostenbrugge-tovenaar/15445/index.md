---
layout: "image"
title: "So klein"
date: "2008-09-22T15:37:55"
picture: "moershausen21.jpg"
weight: "20"
konstrukteure: 
- "Rob van Oostenbrugge"
fotografen:
- "Rob van Oostenbrugge"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Tovenaar"
license: "unknown"
legacy_id:
- details/15445
imported:
- "2019"
_4images_image_id: "15445"
_4images_cat_id: "1405"
_4images_user_id: "814"
_4images_image_date: "2008-09-22T15:37:55"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15445 -->
