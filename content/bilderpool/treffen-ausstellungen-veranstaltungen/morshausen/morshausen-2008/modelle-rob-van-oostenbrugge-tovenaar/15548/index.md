---
layout: "image"
title: "em-Pendel"
date: "2008-09-23T09:47:31"
picture: "em-Pendel61.JPG"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/15548
imported:
- "2019"
_4images_image_id: "15548"
_4images_cat_id: "1405"
_4images_user_id: "4"
_4images_image_date: "2008-09-23T09:47:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15548 -->
