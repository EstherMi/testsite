---
layout: "image"
title: "Kugelbahn"
date: "2008-09-21T22:19:53"
picture: "tovenaar3.jpg"
weight: "23"
konstrukteure: 
- "Rob van Oostenbrugge"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/15388
imported:
- "2019"
_4images_image_id: "15388"
_4images_cat_id: "1405"
_4images_user_id: "130"
_4images_image_date: "2008-09-21T22:19:53"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15388 -->
Die schöne Kugelbahn lief auch wunderbar.