---
layout: "image"
title: "Industriemodell"
date: "2008-09-23T07:43:24"
picture: "convention38.jpg"
weight: "3"
konstrukteure: 
- "Fredy"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/15626
imported:
- "2019"
_4images_image_id: "15626"
_4images_cat_id: "1422"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:24"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15626 -->
