---
layout: "image"
title: "Schleuse77.JPG"
date: "2008-09-23T10:04:30"
picture: "Schleuse77.jpg"
weight: "4"
konstrukteure: 
- "laserman"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/15556
imported:
- "2019"
_4images_image_id: "15556"
_4images_cat_id: "1423"
_4images_user_id: "4"
_4images_image_date: "2008-09-23T10:04:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15556 -->
