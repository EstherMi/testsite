---
layout: "image"
title: "Zeichenmaschiene"
date: "2008-09-23T07:43:24"
picture: "convention62.jpg"
weight: "6"
konstrukteure: 
- "MisterWho"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/15530
imported:
- "2019"
_4images_image_id: "15530"
_4images_cat_id: "1424"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:24"
_4images_image_order: "62"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15530 -->
