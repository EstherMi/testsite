---
layout: "image"
title: "Die Nase von Haralds Superflieger"
date: "2008-09-22T15:37:55"
picture: "flieger1.jpg"
weight: "16"
konstrukteure: 
- "Harald"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/15450
imported:
- "2019"
_4images_image_id: "15450"
_4images_cat_id: "1409"
_4images_user_id: "130"
_4images_image_date: "2008-09-22T15:37:55"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15450 -->
Schön gebaut...