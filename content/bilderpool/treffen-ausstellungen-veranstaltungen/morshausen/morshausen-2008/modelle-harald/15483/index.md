---
layout: "image"
title: "Flugzeug"
date: "2008-09-23T07:43:23"
picture: "convention15.jpg"
weight: "10"
konstrukteure: 
- "Harald"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/15483
imported:
- "2019"
_4images_image_id: "15483"
_4images_cat_id: "1409"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:23"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15483 -->
