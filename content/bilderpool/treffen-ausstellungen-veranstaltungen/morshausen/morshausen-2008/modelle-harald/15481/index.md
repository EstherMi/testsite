---
layout: "image"
title: "Modell von Harald"
date: "2008-09-23T07:43:23"
picture: "convention13.jpg"
weight: "8"
konstrukteure: 
- "Harald"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/15481
imported:
- "2019"
_4images_image_id: "15481"
_4images_cat_id: "1409"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:23"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15481 -->
