---
layout: "image"
title: "Flugzeug"
date: "2008-09-23T07:43:24"
picture: "convention16.jpg"
weight: "11"
konstrukteure: 
- "Harald"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/15484
imported:
- "2019"
_4images_image_id: "15484"
_4images_cat_id: "1409"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:24"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15484 -->
