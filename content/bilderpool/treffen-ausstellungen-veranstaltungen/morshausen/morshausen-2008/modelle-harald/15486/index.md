---
layout: "image"
title: "Flugzeug"
date: "2008-09-23T07:43:24"
picture: "convention18.jpg"
weight: "13"
konstrukteure: 
- "Harald"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/15486
imported:
- "2019"
_4images_image_id: "15486"
_4images_cat_id: "1409"
_4images_user_id: "409"
_4images_image_date: "2008-09-23T07:43:24"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15486 -->
