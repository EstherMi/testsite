---
layout: "image"
title: "Flieger-Heck"
date: "2008-09-23T09:53:43"
picture: "HS-Flieger83.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/15550
imported:
- "2019"
_4images_image_id: "15550"
_4images_cat_id: "1409"
_4images_user_id: "4"
_4images_image_date: "2008-09-23T09:53:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15550 -->
Die Heckrampe ist heruntergelassen. Das schwarze Seil betätigt den Taster links, wenn die Rampe oben ist.
Unter dem Dach entlang verläuft der Transportkran, dessen Laufkatze am weißen Seil gezogen wird.