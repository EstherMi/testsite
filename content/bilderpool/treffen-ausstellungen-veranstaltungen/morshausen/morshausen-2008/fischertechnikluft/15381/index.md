---
layout: "image"
title: "Leuchtbälle (Ralf)"
date: "2008-09-21T21:34:08"
picture: "conv14.jpg"
weight: "66"
konstrukteure: 
- "Martin (Masked)"
fotografen:
- "Martin (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/15381
imported:
- "2019"
_4images_image_id: "15381"
_4images_cat_id: "1403"
_4images_user_id: "373"
_4images_image_date: "2008-09-21T21:34:08"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15381 -->
Für Alle die nicht übernachtet haben, hier nochmal ein paar Bilder von Ralf (ThanksForTheFish) und mir (Masked) und unseren Spielzeugen. Die Bilder kommen eigentlich nur in richtig groß wirklich richtig gut. Danke an Heiko fürs Fotografieren!