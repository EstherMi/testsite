---
layout: "image"
title: "Zuschauer 2"
date: "2008-09-21T21:34:08"
picture: "conv09.jpg"
weight: "61"
konstrukteure: 
- "Martin (Masked)"
fotografen:
- "Martin (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/15376
imported:
- "2019"
_4images_image_id: "15376"
_4images_cat_id: "1403"
_4images_user_id: "373"
_4images_image_date: "2008-09-21T21:34:08"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15376 -->
Staunendes Publikum zum Zweiten