---
layout: "image"
title: "Thomas Kaiser beim Steuern des Quattrocopters"
date: "2008-09-22T15:37:54"
picture: "Thomas_Kaiser_beim_Steuern_des_Quattrocopters.jpg"
weight: "52"
konstrukteure: 
- "-?-"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/15424
imported:
- "2019"
_4images_image_id: "15424"
_4images_cat_id: "1403"
_4images_user_id: "724"
_4images_image_date: "2008-09-22T15:37:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15424 -->
