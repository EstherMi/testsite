---
layout: "image"
title: "Abendessen Samstag"
date: "2008-09-21T21:34:08"
picture: "conv12.jpg"
weight: "64"
konstrukteure: 
- "Martin (Masked)"
fotografen:
- "Martin (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/15379
imported:
- "2019"
_4images_image_id: "15379"
_4images_cat_id: "1403"
_4images_user_id: "373"
_4images_image_date: "2008-09-21T21:34:08"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15379 -->
Thomas sorgt dafür, das er sein T-Shirt auch weiterhin anziehen kann ;-)