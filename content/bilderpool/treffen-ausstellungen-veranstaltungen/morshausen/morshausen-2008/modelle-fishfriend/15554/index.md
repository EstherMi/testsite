---
layout: "image"
title: "Mopeds69.JPG"
date: "2008-09-23T10:03:21"
picture: "Mopeds69.jpg"
weight: "3"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/15554
imported:
- "2019"
_4images_image_id: "15554"
_4images_cat_id: "1432"
_4images_user_id: "4"
_4images_image_date: "2008-09-23T10:03:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15554 -->
