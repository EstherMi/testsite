---
layout: "image"
title: "Mopeds72.JPG"
date: "2008-09-23T10:03:48"
picture: "Mopeds72.jpg"
weight: "4"
konstrukteure: 
- "Holger Howey (fishfriend)"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/15555
imported:
- "2019"
_4images_image_id: "15555"
_4images_cat_id: "1432"
_4images_user_id: "4"
_4images_image_date: "2008-09-23T10:03:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15555 -->
