---
layout: "image"
title: "Harvester 4"
date: "2005-09-26T22:19:55"
picture: "Harvester_4.jpg"
weight: "16"
konstrukteure: 
- "Herr Kohl"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/5010
imported:
- "2019"
_4images_image_id: "5010"
_4images_cat_id: "389"
_4images_user_id: "130"
_4images_image_date: "2005-09-26T22:19:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5010 -->
