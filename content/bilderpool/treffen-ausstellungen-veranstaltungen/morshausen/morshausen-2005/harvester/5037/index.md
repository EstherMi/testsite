---
layout: "image"
title: "Harvester"
date: "2005-09-27T16:45:41"
picture: "P8252520.jpg"
weight: "13"
konstrukteure: 
- "Alfred Koch (?)"
fotografen:
- "Joachim Jacobi / MisterWho"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- details/5037
imported:
- "2019"
_4images_image_id: "5037"
_4images_cat_id: "389"
_4images_user_id: "8"
_4images_image_date: "2005-09-27T16:45:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5037 -->
Harvester, der im Original Bäume im Wald fällt.