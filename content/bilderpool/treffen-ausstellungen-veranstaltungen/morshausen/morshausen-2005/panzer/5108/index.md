---
layout: "image"
title: "Panzer18.JPG"
date: "2005-10-19T22:07:07"
picture: "Panzer18.jpg"
weight: "2"
konstrukteure: 
- "Markus Mack"
fotografen:
- "Harald Steinhaus"
keywords: ["Panzer", "Tank"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5108
imported:
- "2019"
_4images_image_id: "5108"
_4images_cat_id: "404"
_4images_user_id: "4"
_4images_image_date: "2005-10-19T22:07:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5108 -->
