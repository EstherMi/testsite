---
layout: "image"
title: "Panzer20.JPG"
date: "2005-10-19T22:07:07"
picture: "Panzer20.jpg"
weight: "4"
konstrukteure: 
- "Markus Mack"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5110
imported:
- "2019"
_4images_image_id: "5110"
_4images_cat_id: "404"
_4images_user_id: "4"
_4images_image_date: "2005-10-19T22:07:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5110 -->
