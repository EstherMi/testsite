---
layout: "image"
title: "Ballweitergabe"
date: "2005-09-27T16:45:41"
picture: "P8252515.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "Joachim Jacobi / MisterWho"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- details/5033
imported:
- "2019"
_4images_image_id: "5033"
_4images_cat_id: "402"
_4images_user_id: "8"
_4images_image_date: "2005-09-27T16:45:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5033 -->
Der Tischtennisball wurde von einem Greifer zum anderen weitergereicht.