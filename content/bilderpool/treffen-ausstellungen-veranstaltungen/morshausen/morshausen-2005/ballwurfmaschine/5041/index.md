---
layout: "image"
title: "Ballwurfmaschine"
date: "2005-09-30T21:20:27"
picture: "Ballwurfmaschine.jpg"
weight: "1"
konstrukteure: 
- "Remadus"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/5041
imported:
- "2019"
_4images_image_id: "5041"
_4images_cat_id: "387"
_4images_user_id: "130"
_4images_image_date: "2005-09-30T21:20:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5041 -->
