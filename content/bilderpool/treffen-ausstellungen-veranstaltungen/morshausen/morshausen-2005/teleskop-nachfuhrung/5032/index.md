---
layout: "image"
title: "Martins Webcam Teleskop"
date: "2005-09-27T16:45:41"
picture: "P8252514.jpg"
weight: "5"
konstrukteure: 
- "remadus"
fotografen:
- "Joachim Jacobi / MisterWho"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MisterWho"
license: "unknown"
legacy_id:
- details/5032
imported:
- "2019"
_4images_image_id: "5032"
_4images_cat_id: "503"
_4images_user_id: "8"
_4images_image_date: "2005-09-27T16:45:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5032 -->
