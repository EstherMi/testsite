---
layout: "image"
title: "conv2005 sven001"
date: "2005-09-25T19:31:07"
picture: "conv2005_sven001.jpg"
weight: "6"
konstrukteure: 
- "Lothar Vogt"
fotografen:
- "Sven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/4938
imported:
- "2019"
_4images_image_id: "4938"
_4images_cat_id: "398"
_4images_user_id: "1"
_4images_image_date: "2005-09-25T19:31:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4938 -->
