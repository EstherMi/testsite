---
layout: "image"
title: "Kirmesmodell Move It"
date: "2005-09-26T23:48:51"
picture: "Kirmesmodell_Move_It.jpg"
weight: "6"
konstrukteure: 
- "Fam. Jansen"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/5022
imported:
- "2019"
_4images_image_id: "5022"
_4images_cat_id: "392"
_4images_user_id: "130"
_4images_image_date: "2005-09-26T23:48:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5022 -->
