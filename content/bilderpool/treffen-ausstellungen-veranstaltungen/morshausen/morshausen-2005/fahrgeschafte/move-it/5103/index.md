---
layout: "image"
title: "MoveIt60.JPG"
date: "2005-10-19T22:07:07"
picture: "MoveIt60.jpg"
weight: "1"
konstrukteure: 
- "Fam. Janssen"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5103
imported:
- "2019"
_4images_image_id: "5103"
_4images_cat_id: "392"
_4images_user_id: "4"
_4images_image_date: "2005-10-19T22:07:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5103 -->
