---
layout: "image"
title: "Kermis-NN47.JPG"
date: "2005-11-03T14:28:00"
picture: "Kermis-NN47.JPG"
weight: "1"
konstrukteure: 
- "Fam. Jansen"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5174
imported:
- "2019"
_4images_image_id: "5174"
_4images_cat_id: "433"
_4images_user_id: "4"
_4images_image_date: "2005-11-03T14:28:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5174 -->
(Da waren noch ein paar Fotos übrig...)

Das Vorbild zu diesem Modell kommt erstmals dieses Jahr auf die Kirmesplätze. Einen Namen hat es auch, den weiß ich aber nicht mehr.