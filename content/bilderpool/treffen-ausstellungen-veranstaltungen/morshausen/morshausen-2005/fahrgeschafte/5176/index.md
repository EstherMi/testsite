---
layout: "image"
title: "Kermis-NN49.JPG"
date: "2005-11-03T14:43:41"
picture: "Kermis-NN49.JPG"
weight: "3"
konstrukteure: 
- "Fam. Jansen"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5176
imported:
- "2019"
_4images_image_id: "5176"
_4images_cat_id: "433"
_4images_user_id: "4"
_4images_image_date: "2005-11-03T14:43:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5176 -->
Der Unterbau. Die Kurbel am vorderen Fuß dient zur Neigungsverstellung der Gondeln, Der Motor am hinteren Fuß treibt nur den Tragarm an, alle weiteren Bewegungen der Fahrgäste geschehen durch trickreiche Mechanik.