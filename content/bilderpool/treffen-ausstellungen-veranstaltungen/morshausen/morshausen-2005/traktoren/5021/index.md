---
layout: "image"
title: "Schlüter Traktor"
date: "2005-09-26T23:48:51"
picture: "Schlter_Trecker.jpg"
weight: "7"
konstrukteure: 
- "Claus Werner Ludwig"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/5021
imported:
- "2019"
_4images_image_id: "5021"
_4images_cat_id: "388"
_4images_user_id: "130"
_4images_image_date: "2005-09-26T23:48:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5021 -->
