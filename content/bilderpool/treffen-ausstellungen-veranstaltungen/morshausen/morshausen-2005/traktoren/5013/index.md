---
layout: "image"
title: "Schlüter Traktor"
date: "2005-09-26T22:19:55"
picture: "Conv2005_CL03.jpg"
weight: "4"
konstrukteure: 
- "Claus Ludwig"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- details/5013
imported:
- "2019"
_4images_image_id: "5013"
_4images_cat_id: "388"
_4images_user_id: "10"
_4images_image_date: "2005-09-26T22:19:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5013 -->
