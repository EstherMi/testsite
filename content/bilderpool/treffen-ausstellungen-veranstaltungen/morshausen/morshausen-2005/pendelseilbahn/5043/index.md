---
layout: "image"
title: "Gondelhalterung"
date: "2005-09-30T21:20:27"
picture: "Gondelhalterung.jpg"
weight: "2"
konstrukteure: 
- "Thomas Habig (Triceratops)"
fotografen:
- "stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/5043
imported:
- "2019"
_4images_image_id: "5043"
_4images_cat_id: "393"
_4images_user_id: "130"
_4images_image_date: "2005-09-30T21:20:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5043 -->
