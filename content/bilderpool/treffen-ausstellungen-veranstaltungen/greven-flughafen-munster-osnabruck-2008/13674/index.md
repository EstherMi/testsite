---
layout: "image"
title: "Raum"
date: "2008-02-17T19:31:02"
picture: "greven1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/13674
imported:
- "2019"
_4images_image_id: "13674"
_4images_cat_id: "1258"
_4images_user_id: "504"
_4images_image_date: "2008-02-17T19:31:02"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13674 -->
Ausstellung im Flughafen.