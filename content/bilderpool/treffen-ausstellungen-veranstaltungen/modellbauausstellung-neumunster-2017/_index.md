---
layout: "overview"
title: "Modellbauausstellung Neumünster 2017"
date: 2019-12-17T18:35:13+01:00
legacy_id:
- categories/3381
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3381 --> 
Unser Stand war diesmal 16 meter lang und 5 meter breit.
Dadurch hatten wir mehr Platz für unsere Modelle und die Besucher haben sich besser verteilt.

Die Stimmung im Team war super. Jeder hatte seine Modelle mitgebracht.

Bei uns war Mitmachen angesagt. Bulldozer fahren, Flippern, Anhänger angeln,
mit dem WALL-E spielen, eine Verlosung  und Freundschaftsbänder stricken.