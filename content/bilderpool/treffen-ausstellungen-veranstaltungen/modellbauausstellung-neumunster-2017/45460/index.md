---
layout: "image"
title: "Die Aussteller"
date: "2017-03-08T16:28:26"
picture: "neumuenster01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/45460
imported:
- "2019"
_4images_image_id: "45460"
_4images_cat_id: "3381"
_4images_user_id: "2303"
_4images_image_date: "2017-03-08T16:28:26"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45460 -->
Ralf, Holger, Finn Lukas, Dirk, Frank, Ben Jonas, Christine. (Grau fehlt).