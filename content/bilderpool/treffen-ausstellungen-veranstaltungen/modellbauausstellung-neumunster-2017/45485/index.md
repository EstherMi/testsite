---
layout: "image"
title: "Kugelbahn"
date: "2017-03-08T17:29:59"
picture: "neumuenster26.jpg"
weight: "26"
konstrukteure: 
- "Familie Alpen"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/45485
imported:
- "2019"
_4images_image_id: "45485"
_4images_cat_id: "3381"
_4images_user_id: "2303"
_4images_image_date: "2017-03-08T17:29:59"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45485 -->
