---
layout: "image"
title: "Modellbauaustellung Neumünster"
date: "2017-03-08T16:28:27"
picture: "neumuenster04.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/45463
imported:
- "2019"
_4images_image_id: "45463"
_4images_cat_id: "3381"
_4images_user_id: "2303"
_4images_image_date: "2017-03-08T16:28:27"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45463 -->
