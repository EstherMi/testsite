---
layout: "image"
title: "Eelde 2005_13"
date: "2005-06-15T21:10:11"
picture: "EELDE_2005_13.jpg"
weight: "25"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/4457
imported:
- "2019"
_4images_image_id: "4457"
_4images_cat_id: "449"
_4images_user_id: "144"
_4images_image_date: "2005-06-15T21:10:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4457 -->
Detail der drehkranz.
Da soll ein grossen rollenlager drin sein.