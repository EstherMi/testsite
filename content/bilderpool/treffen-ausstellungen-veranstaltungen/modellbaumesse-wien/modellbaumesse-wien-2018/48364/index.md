---
layout: "image"
title: "Modellbaumesse Wien 2018"
date: "2018-11-02T16:38:59"
picture: "modellbaumessewien1.jpg"
weight: "1"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/48364
imported:
- "2019"
_4images_image_id: "48364"
_4images_cat_id: "3543"
_4images_user_id: "968"
_4images_image_date: "2018-11-02T16:38:59"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48364 -->
Dieses Jahr mal wieder in Wien zu Gast bei der Firma Modellbau Kirchert aus Wien. Der Schwerpunkt lag tatsächlich bei Präsentation und Verkauf von Fischertechnik.
Den Spruch : "...guck mal,Lego " habe ich nur sehr selten von einigen Kindern gehört :)