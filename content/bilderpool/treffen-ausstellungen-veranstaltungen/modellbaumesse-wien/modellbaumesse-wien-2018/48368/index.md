---
layout: "image"
title: "Modellbaumesse Wien 2018"
date: "2018-11-02T16:38:59"
picture: "modellbaumessewien5.jpg"
weight: "5"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/48368
imported:
- "2019"
_4images_image_id: "48368"
_4images_cat_id: "3543"
_4images_user_id: "968"
_4images_image_date: "2018-11-02T16:38:59"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48368 -->
Hinter dem bunten Ballon gabs ein stets gut gefüllte Bastelecke mit Fischertip.