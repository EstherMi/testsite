---
layout: "image"
title: "Modelbaumesse Wien_Stand der Fima Kirchert Modellbau Wien"
date: "2015-10-29T21:09:24"
picture: "modellbaumessewien13.jpg"
weight: "13"
konstrukteure: 
- "Fischertechnik TEAM AUSTRIA"
fotografen:
- "Roland Enzenhofer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- details/42171
imported:
- "2019"
_4images_image_id: "42171"
_4images_cat_id: "3142"
_4images_user_id: "1688"
_4images_image_date: "2015-10-29T21:09:24"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42171 -->
Viele interessierte Besucher!