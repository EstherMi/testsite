---
layout: "image"
title: "ftconventionapril009.jpg"
date: "2018-05-01T11:06:36"
picture: "ftconventionapril009.jpg"
weight: "21"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/47480
imported:
- "2019"
_4images_image_id: "47480"
_4images_cat_id: "3508"
_4images_user_id: "136"
_4images_image_date: "2018-05-01T11:06:36"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47480 -->
