---
layout: "image"
title: "Die Lokomobil aus der Classic Line"
date: "2018-05-06T00:28:48"
picture: "ftnordconvebntion09.jpg"
weight: "9"
konstrukteure: 
- "Stephan Wenkers"
fotografen:
- "Karina Wenkers"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/47589
imported:
- "2019"
_4images_image_id: "47589"
_4images_cat_id: "3508"
_4images_user_id: "130"
_4images_image_date: "2018-05-06T00:28:48"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47589 -->
Hab ich vor einigen Jahren mal live in Aktion gesehen.