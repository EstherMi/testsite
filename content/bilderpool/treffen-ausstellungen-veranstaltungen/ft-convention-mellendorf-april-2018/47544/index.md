---
layout: "image"
title: "ftconventionapril073.jpg"
date: "2018-05-01T11:06:37"
picture: "ftconventionapril073.jpg"
weight: "85"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/47544
imported:
- "2019"
_4images_image_id: "47544"
_4images_cat_id: "3508"
_4images_user_id: "136"
_4images_image_date: "2018-05-01T11:06:37"
_4images_image_order: "73"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47544 -->
