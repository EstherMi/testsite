---
layout: "image"
title: "ftconventionapril054.jpg"
date: "2018-05-01T11:06:36"
picture: "ftconventionapril054.jpg"
weight: "66"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/47525
imported:
- "2019"
_4images_image_id: "47525"
_4images_cat_id: "3508"
_4images_user_id: "136"
_4images_image_date: "2018-05-01T11:06:36"
_4images_image_order: "54"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47525 -->
