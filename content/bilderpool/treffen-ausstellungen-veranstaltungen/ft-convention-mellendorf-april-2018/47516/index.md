---
layout: "image"
title: "ftconventionapril045.jpg"
date: "2018-05-01T11:06:36"
picture: "ftconventionapril045.jpg"
weight: "57"
konstrukteure: 
- "Stephan Wenkers"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/47516
imported:
- "2019"
_4images_image_id: "47516"
_4images_cat_id: "3508"
_4images_user_id: "136"
_4images_image_date: "2018-05-01T11:06:36"
_4images_image_order: "45"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47516 -->
