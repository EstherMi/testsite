---
layout: "image"
title: "ftconventionapril059.jpg"
date: "2018-05-01T11:06:36"
picture: "ftconventionapril059.jpg"
weight: "71"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/47530
imported:
- "2019"
_4images_image_id: "47530"
_4images_cat_id: "3508"
_4images_user_id: "136"
_4images_image_date: "2018-05-01T11:06:36"
_4images_image_order: "59"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47530 -->
