---
layout: "image"
title: "ftconventionapril085.jpg"
date: "2018-05-01T11:06:37"
picture: "ftconventionapril085.jpg"
weight: "97"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/47556
imported:
- "2019"
_4images_image_id: "47556"
_4images_cat_id: "3508"
_4images_user_id: "136"
_4images_image_date: "2018-05-01T11:06:37"
_4images_image_order: "85"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47556 -->
