---
layout: "image"
title: "ftconventionapril074.jpg"
date: "2018-05-01T11:06:37"
picture: "ftconventionapril074.jpg"
weight: "86"
konstrukteure: 
- "Stephan Wenkers"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/47545
imported:
- "2019"
_4images_image_id: "47545"
_4images_cat_id: "3508"
_4images_user_id: "136"
_4images_image_date: "2018-05-01T11:06:37"
_4images_image_order: "74"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47545 -->
