---
layout: "image"
title: "Challenge"
date: "2018-05-01T11:06:37"
picture: "ftconventionapril081.jpg"
weight: "93"
konstrukteure: 
- "Carel van Leeuwen"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/47552
imported:
- "2019"
_4images_image_id: "47552"
_4images_cat_id: "3508"
_4images_user_id: "136"
_4images_image_date: "2018-05-01T11:06:37"
_4images_image_order: "81"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47552 -->
This is the target group of fischertechnik, children who have fun.