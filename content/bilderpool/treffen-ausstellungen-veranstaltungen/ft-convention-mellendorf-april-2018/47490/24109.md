---
layout: "comment"
hidden: true
title: "24109"
date: "2018-06-16T21:08:03"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Danke! Das Schiff dreht sich auch noch? Bin ich froh, dass ich da nicht drin sitze ;-)

Laufen die Motore in den Steigungen irgendwie gesteuert langsamer/schneller?

Gruß,
Stefan