---
layout: "image"
title: "Wiener Riesenrad Modell"
date: "2018-05-06T00:28:48"
picture: "ftnordconvebntion06.jpg"
weight: "6"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Karina Wenkers"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/47586
imported:
- "2019"
_4images_image_id: "47586"
_4images_cat_id: "3508"
_4images_user_id: "130"
_4images_image_date: "2018-05-06T00:28:48"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47586 -->
Genau wie das Original im Wiener Prater.