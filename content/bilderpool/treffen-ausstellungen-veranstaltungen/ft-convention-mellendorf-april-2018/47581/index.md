---
layout: "image"
title: "Stephan und sein Riesenrad Modell"
date: "2018-05-06T00:28:48"
picture: "ftnordconvebntion01.jpg"
weight: "1"
konstrukteure: 
- "Stephan Wenkers"
fotografen:
- "Karina Wenkers"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/47581
imported:
- "2019"
_4images_image_id: "47581"
_4images_cat_id: "3508"
_4images_user_id: "130"
_4images_image_date: "2018-05-06T00:28:48"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47581 -->
Da kann man gut die Größe erkennen.