---
layout: "image"
title: "ftconventionapril007.jpg"
date: "2018-05-01T11:06:36"
picture: "ftconventionapril007.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/47478
imported:
- "2019"
_4images_image_id: "47478"
_4images_cat_id: "3508"
_4images_user_id: "136"
_4images_image_date: "2018-05-01T11:06:36"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47478 -->
