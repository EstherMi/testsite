---
layout: "image"
title: "Mondrakete Saturn V"
date: "2018-05-06T00:28:48"
picture: "ftnordconvebntion08.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "Karina Wenkers"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/47588
imported:
- "2019"
_4images_image_id: "47588"
_4images_cat_id: "3508"
_4images_user_id: "130"
_4images_image_date: "2018-05-06T00:28:48"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47588 -->
Klasse gebautes Modell der Mondrakete Saturn V. Mit Startrampe bzw. Transportraupenschlepper.