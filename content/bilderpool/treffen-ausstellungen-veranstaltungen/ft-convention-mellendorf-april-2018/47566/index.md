---
layout: "image"
title: "ftconventionapril095.jpg"
date: "2018-05-01T11:06:37"
picture: "ftconventionapril095.jpg"
weight: "107"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/47566
imported:
- "2019"
_4images_image_id: "47566"
_4images_cat_id: "3508"
_4images_user_id: "136"
_4images_image_date: "2018-05-01T11:06:37"
_4images_image_order: "95"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47566 -->
