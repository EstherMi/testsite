---
layout: "image"
title: "ftconventionapril061.jpg"
date: "2018-05-01T11:06:36"
picture: "ftconventionapril061.jpg"
weight: "73"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/47532
imported:
- "2019"
_4images_image_id: "47532"
_4images_cat_id: "3508"
_4images_user_id: "136"
_4images_image_date: "2018-05-01T11:06:36"
_4images_image_order: "61"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47532 -->
