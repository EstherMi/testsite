---
layout: "image"
title: "ftconventionapril060.jpg"
date: "2018-05-01T11:06:36"
picture: "ftconventionapril060.jpg"
weight: "72"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/47531
imported:
- "2019"
_4images_image_id: "47531"
_4images_cat_id: "3508"
_4images_user_id: "136"
_4images_image_date: "2018-05-01T11:06:36"
_4images_image_order: "60"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47531 -->
