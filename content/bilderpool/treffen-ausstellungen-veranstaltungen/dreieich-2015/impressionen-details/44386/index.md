---
layout: "image"
title: "Hallenübersicht (unser Classik Kran im Vordergrund)"
date: "2016-09-21T16:47:03"
picture: "IMG_0541.jpg"
weight: "1"
konstrukteure: 
- "diverse"
fotografen:
- "Jan und Tilo Rust"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/44386
imported:
- "2019"
_4images_image_id: "44386"
_4images_cat_id: "3280"
_4images_user_id: "2638"
_4images_image_date: "2016-09-21T16:47:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44386 -->
