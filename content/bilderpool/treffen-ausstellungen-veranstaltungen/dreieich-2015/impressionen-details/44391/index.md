---
layout: "image"
title: "Der Classik Kran (Höhe ca. 2m, Auslage ca. 150cm)"
date: "2016-09-21T16:47:03"
picture: "IMG_0499.jpg"
weight: "6"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Tilo Rust"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/44391
imported:
- "2019"
_4images_image_id: "44391"
_4images_cat_id: "3280"
_4images_user_id: "2638"
_4images_image_date: "2016-09-21T16:47:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44391 -->
Der Kran wurde (fast) ausschließlich mit Classik-Teilen begaut. Der Ausleger ist frei drehbar (Stromübertragung mit 3 Schleifringen, Achse und Platte). Traglast ca. 400g (bei max. AUslage). Kabel-Fernsteuerung mit Centronics-Stecker für alle Funktionen. Mit Baubude und Zubehör.