---
layout: "image"
title: "Das Depot (noch ohne Wendeschleife)"
date: "2015-10-06T18:38:55"
picture: "olagino13.jpg"
weight: "13"
konstrukteure: 
- "Teile von Masked, Bau von Getriebesand, sjost und olagino"
fotografen:
- "olagino"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- details/42074
imported:
- "2019"
_4images_image_id: "42074"
_4images_cat_id: "3127"
_4images_user_id: "2042"
_4images_image_date: "2015-10-06T18:38:55"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42074 -->
Da das