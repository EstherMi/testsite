---
layout: "image"
title: "IMG_2142.JPG"
date: "2015-10-05T21:13:33"
picture: "IMG_2142mit.JPG"
weight: "19"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/42047
imported:
- "2019"
_4images_image_id: "42047"
_4images_cat_id: "3127"
_4images_user_id: "4"
_4images_image_date: "2015-10-05T21:13:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42047 -->
Da geht es auf eine Haltestelle zu.