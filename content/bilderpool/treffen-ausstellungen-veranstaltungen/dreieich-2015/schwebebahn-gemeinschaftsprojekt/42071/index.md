---
layout: "image"
title: "Ein Schwebebahnzug im Detail"
date: "2015-10-06T18:38:55"
picture: "olagino10.jpg"
weight: "10"
konstrukteure: 
- "Getriebesand"
fotografen:
- "olagino"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- details/42071
imported:
- "2019"
_4images_image_id: "42071"
_4images_cat_id: "3127"
_4images_user_id: "2042"
_4images_image_date: "2015-10-06T18:38:55"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42071 -->
Hier ist ein Schwebebahnzug im Detail zu sehen. Die Züge konnten entweder per Fernbedienung oder über Bluetooth und einen Robo TX Controller betrieben werden. Dabei waren der Akku und der Conroller im vorderen bzw. hinteren Teil lokalisiert. Und natürlich konnten auch die Türen angesteuert werden.