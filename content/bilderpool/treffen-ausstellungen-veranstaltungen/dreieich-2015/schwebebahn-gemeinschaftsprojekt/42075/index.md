---
layout: "image"
title: "Gesamtansicht der Schwebebahn"
date: "2015-10-06T18:38:55"
picture: "olagino14.jpg"
weight: "14"
konstrukteure: 
- "Getriebesand, sjost, tobs9578, Volker James Münchhof, olagino"
fotografen:
- "olagino"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- details/42075
imported:
- "2019"
_4images_image_id: "42075"
_4images_cat_id: "3127"
_4images_user_id: "2042"
_4images_image_date: "2015-10-06T18:38:55"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42075 -->
Hier kann man die Schwebebahn in der ganzen Pracht bewundern