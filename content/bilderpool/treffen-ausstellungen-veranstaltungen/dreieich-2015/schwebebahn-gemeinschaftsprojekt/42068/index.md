---
layout: "image"
title: "ft:c-Logo in voller Pracht"
date: "2015-10-06T18:38:55"
picture: "olagino07.jpg"
weight: "7"
konstrukteure: 
- "Getriebesand"
fotografen:
- "olagino"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- details/42068
imported:
- "2019"
_4images_image_id: "42068"
_4images_cat_id: "3127"
_4images_user_id: "2042"
_4images_image_date: "2015-10-06T18:38:55"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42068 -->
Natürlich darf auf einer Veranstaltung des ft:c nicht das Logo fehlen.