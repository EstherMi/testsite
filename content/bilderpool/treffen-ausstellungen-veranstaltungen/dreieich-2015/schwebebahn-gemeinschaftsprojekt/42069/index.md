---
layout: "image"
title: "Station von olagino (Gesamtansicht)"
date: "2015-10-06T18:38:55"
picture: "olagino08.jpg"
weight: "8"
konstrukteure: 
- "olagino"
fotografen:
- "olagino"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- details/42069
imported:
- "2019"
_4images_image_id: "42069"
_4images_cat_id: "3127"
_4images_user_id: "2042"
_4images_image_date: "2015-10-06T18:38:55"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42069 -->
Hier ist die Zwischenstation von mir (olagino) zu sehen. Sie stellte die Zwischenstation zwischen dem Depot und der Wendeschleife dar.