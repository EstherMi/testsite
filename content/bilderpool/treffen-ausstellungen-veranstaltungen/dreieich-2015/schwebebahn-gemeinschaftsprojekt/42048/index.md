---
layout: "image"
title: "IMG_2145.JPG"
date: "2015-10-05T21:15:16"
picture: "IMG_2145mit.JPG"
weight: "20"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/42048
imported:
- "2019"
_4images_image_id: "42048"
_4images_cat_id: "3127"
_4images_user_id: "4"
_4images_image_date: "2015-10-05T21:15:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42048 -->
Hier kommt eine weitere Haltestelle und dahinter die Endstation mit Drehvorrichtung.