---
layout: "image"
title: "Wendeanlage von sjost"
date: "2015-10-06T18:38:55"
picture: "olagino02.jpg"
weight: "2"
konstrukteure: 
- "sjost"
fotografen:
- "olagino"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Leon"
license: "unknown"
legacy_id:
- details/42063
imported:
- "2019"
_4images_image_id: "42063"
_4images_cat_id: "3127"
_4images_user_id: "2042"
_4images_image_date: "2015-10-06T18:38:55"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42063 -->
In der Wendeanlage konnten - um einen nicht-endenden Zugverkehr zu gewährleisten - Züge einfahren und gewendet werden. Die Drehscheibe stellte dabei eine große Herausforderung dar, da die Gleisenden genau aufeinanderpassen mussten.