---
layout: "image"
title: "IMG_2140.JPG"
date: "2015-10-05T21:12:37"
picture: "IMG_2140mit.JPG"
weight: "18"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/42046
imported:
- "2019"
_4images_image_id: "42046"
_4images_cat_id: "3127"
_4images_user_id: "4"
_4images_image_date: "2015-10-05T21:12:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42046 -->
Hier geht es aus dem Depot heraus auf die Strecke. Es gibt zwei Arten von Stützen: im Vordergrund sind sie U-förmig und die beiden Gleise hängen eng nebeneinander. Weiter im Hintergrund sieht man T-förmige Stützen, bei denen die Gleise weiter auseinander geführt sind. Zur Versteifung haben sich die Mitbauer diverse Sachen einfallen lassen.