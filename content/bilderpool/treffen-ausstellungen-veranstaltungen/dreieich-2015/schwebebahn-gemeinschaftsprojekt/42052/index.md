---
layout: "image"
title: "IMG_2158.JPG"
date: "2015-10-05T21:29:27"
picture: "IMG_2158.JPG"
weight: "24"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/42052
imported:
- "2019"
_4images_image_id: "42052"
_4images_cat_id: "3127"
_4images_user_id: "4"
_4images_image_date: "2015-10-05T21:29:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42052 -->
Der rote Zug von Nahem. Unter den Waggons sind der Akku (vorne), die Tür-Mechanik (mitte) und der TX-Controller (hinten) untergebracht. Den Antrieb besorgt ein XM-Motor auf dem mittleren Waggon.