---
layout: "image"
title: "Tretrad-Kran"
date: "2015-10-01T13:37:10"
picture: "Tretrad-Kran.jpg"
weight: "18"
konstrukteure: 
- "Dirk Fox"
fotografen:
- "Andreas Gürten"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/41932
imported:
- "2019"
_4images_image_id: "41932"
_4images_cat_id: "3118"
_4images_user_id: "724"
_4images_image_date: "2015-10-01T13:37:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41932 -->
