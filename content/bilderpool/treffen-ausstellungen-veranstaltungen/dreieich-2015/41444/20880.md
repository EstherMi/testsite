---
layout: "comment"
hidden: true
title: "20880"
date: "2015-07-23T12:17:53"
uploadBy:
- "Masked"
license: "unknown"
imported:
- "2019"
---
Mahlzeit die Herren,
das Plakat soll vor Allem hier in und um Dreieich in Läden, Kindergärten, etc. hängen. Und zwar 2 oder 3 Wochen vor der Convention. Da brauchts also keine Jahreszahl.
Und ja, es ist minimalistisch. Eure fertigen Plakate für nächstes Jahr nehme ich aber gerne in druckfertiger Form in Empfang. ;-)
Grüße,
Martin