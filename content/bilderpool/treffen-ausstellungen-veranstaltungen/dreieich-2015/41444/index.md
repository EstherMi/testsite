---
layout: "image"
title: "Convention-Plakat 2015"
date: "2015-07-15T17:37:38"
picture: "conplakat1.jpg"
weight: "5"
konstrukteure: 
- "Martin W. (Masked)"
fotografen:
- "Martin W. (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/41444
imported:
- "2019"
_4images_image_id: "41444"
_4images_cat_id: "3097"
_4images_user_id: "373"
_4images_image_date: "2015-07-15T17:37:38"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41444 -->
So sieht das Plakat zur Convention 2015 aus.
Größe: DIN-A3
Zum Selberdrucken und Aufhängen:
Download volle Auflösung: http://www.ftcommunity.de/data/downloads/conventionplakate/plakatconventiongross.pdf
Download reduzierte Qualität: http://www.ftcommunity.de/data/downloads/conventionplakate/plakatconventionklein.pdf