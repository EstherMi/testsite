---
layout: "image"
title: "ferngesteuertes Flugzeug"
date: "2008-11-17T21:08:46"
picture: "modelbaumessesued11.jpg"
weight: "11"
konstrukteure: 
- "Marius"
fotografen:
- "Marius"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/16300
imported:
- "2019"
_4images_image_id: "16300"
_4images_cat_id: "1470"
_4images_user_id: "845"
_4images_image_date: "2008-11-17T21:08:46"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16300 -->
