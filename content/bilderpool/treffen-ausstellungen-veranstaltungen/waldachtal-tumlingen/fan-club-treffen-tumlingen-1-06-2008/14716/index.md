---
layout: "image"
title: "Techniksiskussion Radlader"
date: "2008-06-20T16:07:14"
picture: "Technikdiskussion_Radlader.jpg"
weight: "2"
konstrukteure: 
- "jw"
fotografen:
- "Hr. Peterra"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- details/14716
imported:
- "2019"
_4images_image_id: "14716"
_4images_cat_id: "1345"
_4images_user_id: "107"
_4images_image_date: "2008-06-20T16:07:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14716 -->
