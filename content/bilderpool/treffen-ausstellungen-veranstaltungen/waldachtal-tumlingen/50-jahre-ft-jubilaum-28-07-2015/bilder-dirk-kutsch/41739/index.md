---
layout: "image"
title: "Platten-Sortier-Anlage"
date: "2015-08-06T14:28:36"
picture: "dirk13.jpg"
weight: "13"
konstrukteure: 
- "Bennik"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/41739
imported:
- "2019"
_4images_image_id: "41739"
_4images_cat_id: "3111"
_4images_user_id: "389"
_4images_image_date: "2015-08-06T14:28:36"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41739 -->
Eine wirklich sehr schöne Anlage mit vielen kleinen Details.