---
layout: "image"
title: "Farbgranulat für die ft Bausteine"
date: "2015-08-06T14:28:36"
picture: "dirk06.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/41732
imported:
- "2019"
_4images_image_id: "41732"
_4images_cat_id: "3111"
_4images_user_id: "389"
_4images_image_date: "2015-08-06T14:28:36"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41732 -->
Dieser Wagen mit den Boxen stand in der Lehrwerkstatt hinter der Spritzgussmaschine