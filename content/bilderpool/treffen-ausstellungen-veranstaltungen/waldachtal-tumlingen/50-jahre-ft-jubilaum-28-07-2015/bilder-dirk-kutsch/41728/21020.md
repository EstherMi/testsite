---
layout: "comment"
hidden: true
title: "21020"
date: "2015-08-16T01:51:41"
uploadBy:
- "Lurchi"
license: "unknown"
imported:
- "2019"
---
Hallo,

hab' das zitierte Clubheft gerade nochmals im "Netz" gefunden:

http://www.fischertechnik-museum.ch/doc/FanClub/Club_06_1970.pdf

Es scheint eine Art "Brücken-Teil-Setzmaschine" gewesen zu sein.

Alles in Allem sehr schön!

Gruß

Lurchi