---
layout: "image"
title: "Brücke mit Kran"
date: "2015-08-06T14:28:36"
picture: "dirk02.jpg"
weight: "2"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/41728
imported:
- "2019"
_4images_image_id: "41728"
_4images_cat_id: "3111"
_4images_user_id: "389"
_4images_image_date: "2015-08-06T14:28:36"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41728 -->
Diese Brücke ist ein Nachbau aus dem Club Heft April 1970.