---
layout: "image"
title: "Schaufelradbagger 289"
date: "2015-08-06T14:28:36"
picture: "dirk19.jpg"
weight: "19"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/41745
imported:
- "2019"
_4images_image_id: "41745"
_4images_cat_id: "3111"
_4images_user_id: "389"
_4images_image_date: "2015-08-06T14:28:36"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41745 -->
