---
layout: "image"
title: "Platten-Sortier-Anlage"
date: "2015-08-06T14:28:36"
picture: "dirk14.jpg"
weight: "14"
konstrukteure: 
- "Bennik"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/41740
imported:
- "2019"
_4images_image_id: "41740"
_4images_cat_id: "3111"
_4images_user_id: "389"
_4images_image_date: "2015-08-06T14:28:36"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41740 -->
