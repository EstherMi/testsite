---
layout: "image"
title: "Detail 1/2"
date: "2015-08-06T14:28:36"
picture: "dirk07.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/41733
imported:
- "2019"
_4images_image_id: "41733"
_4images_cat_id: "3111"
_4images_user_id: "389"
_4images_image_date: "2015-08-06T14:28:36"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41733 -->
Hier sieht man wie fischer ins Detail geht...