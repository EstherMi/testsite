---
layout: "image"
title: "Ring Kran von Wim"
date: "2015-08-06T14:28:36"
picture: "dirk04.jpg"
weight: "4"
konstrukteure: 
- "Wim Starreveldt"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/41730
imported:
- "2019"
_4images_image_id: "41730"
_4images_cat_id: "3111"
_4images_user_id: "389"
_4images_image_date: "2015-08-06T14:28:36"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41730 -->
