---
layout: "image"
title: "Bonbon-Automat (2)"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk047.jpg"
weight: "47"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41530
imported:
- "2019"
_4images_image_id: "41530"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "47"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41530 -->
Man muss den Roboterarm mit dem Unterdruck-Greifer steuern und kann sich so ein Bonbon auf die Ausgaberutsche legen.