---
layout: "comment"
hidden: true
title: "20946"
date: "2015-07-30T17:07:24"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Soweit ich das gesehen habe ja. Da musste niemand manuell eingreifen. Die Sensorik dafür befindet sich vermutlich im beweglichen Teil.
Gruß,
Stefan