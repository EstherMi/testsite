---
layout: "image"
title: "Unimog"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk073.jpg"
weight: "73"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41556
imported:
- "2019"
_4images_image_id: "41556"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "73"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41556 -->
Mit Allradantrieb, Allradfederung, Portalachsen und pneumatisch aufstellbarer Kippmulde.