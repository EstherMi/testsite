---
layout: "image"
title: "Gerhards LED-Modell, Akku-Ladegerät, Selbstbausteuerung"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk110.jpg"
weight: "110"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41593
imported:
- "2019"
_4images_image_id: "41593"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "110"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41593 -->
Von links nach rechts:

- Über eine starke Übersetzung mittels selbstgemachter großer Zahnräder werden Dauermagnete schnell rotiert. Die induzieren in einem ft-Elektromagneten eine Spannung, die für die Ansteuerung von LEDs genügt.

- Das Akku-Ladegerät aus ft:pedia 2015-1

- Ein selbstgebautes Steuerungsmodul