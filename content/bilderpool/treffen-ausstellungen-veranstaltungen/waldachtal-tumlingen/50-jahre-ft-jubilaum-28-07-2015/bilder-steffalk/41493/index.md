---
layout: "image"
title: "Alfred Petteras Kirmesmodell (2)"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk010.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41493
imported:
- "2019"
_4images_image_id: "41493"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41493 -->
... und durch den untersetzt via Kurbel betätigten Polwendeschalter ändern sie von Zeit zu Zeit ihre Drehrichtungen.