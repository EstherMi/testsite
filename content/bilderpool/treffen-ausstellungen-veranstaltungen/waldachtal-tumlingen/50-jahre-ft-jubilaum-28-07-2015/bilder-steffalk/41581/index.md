---
layout: "image"
title: "Flip-Flop-Techniken"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk098.jpg"
weight: "98"
konstrukteure: 
- "Hans Wijnsouw"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41581
imported:
- "2019"
_4images_image_id: "41581"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "98"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41581 -->
und schließlich mit einem entsprechend programmierten Computerinterface (hier einem Robo-Interface)