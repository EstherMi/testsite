---
layout: "image"
title: "Peters Modelle"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk019.jpg"
weight: "19"
konstrukteure: 
- "Peter Damen"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41502
imported:
- "2019"
_4images_image_id: "41502"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41502 -->
Das Modell aus grauer Statik rechts im Bild ist eine Fin-Ray-Konstruktion.