---
layout: "image"
title: "Martins Riesenrad"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk084.jpg"
weight: "84"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41567
imported:
- "2019"
_4images_image_id: "41567"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "84"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41567 -->
Das rote Runde in Bildmitte unter dem hinteren Riesenrad-Ring ist der Antrieb.