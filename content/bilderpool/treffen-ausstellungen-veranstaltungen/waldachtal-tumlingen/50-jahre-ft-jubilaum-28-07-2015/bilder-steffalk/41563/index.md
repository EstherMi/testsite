---
layout: "image"
title: "Schulprojekt"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk080.jpg"
weight: "80"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41563
imported:
- "2019"
_4images_image_id: "41563"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "80"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41563 -->
... und auf dem Tisch einen Gegenstand ergreift - um ihn woanders hin zu bringen!