---
layout: "comment"
hidden: true
title: "20936"
date: "2015-07-29T18:04:31"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Link zum adaptive Greifer :
http://www.ftcommunity.de/categories.php?cat_id=2775

Der adaptive Greifer mit Fin-Ray-Effect funktioniert auch wie einer Fischschwanzflosse. 
Zwei flexible Bänder laufen wie ein Dreieck in der Spitze zusammen. 
Zwischenstege in regelmässigen Abständen sind über Gelenke mit den Bändern verbunden. 
Durch diesen flexiblen, aber festen Verbund passen sich die Greiffinger der Kontur eines Werkstücks an. 

Link zum Youtube: 

https://www.youtube.com/watch?v=NtDFw9uO-bs&index=4&list=UUvBlHQzqD-ISw8MaTccrfOQ          = Qualle Fischertechnik 

https://www.youtube.com/watch?v=RjhEi15VK-4&index=7&list=UUvBlHQzqD-ISw8MaTccrfOQ          = Smartbird Earthflight Fischertechnik 

https://www.youtube.com/watch?v=OvUo6Us2geo&list=UUvBlHQzqD-ISw8MaTccrfOQ&index=1      = Libelle Fischertechnik 

https://www.youtube.com/watch?v=m1UJYAVVplU&list=UUvBlHQzqD-ISw8MaTccrfOQ&index=2       = Pijlstaartrog Fischertechnik

Gruss,

Peter
Poederoyen NL