---
layout: "image"
title: "Haralds Modelle"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk162.jpg"
weight: "162"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41645
imported:
- "2019"
_4images_image_id: "41645"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "162"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41645 -->
Einschließlich Teile wie Schleifringen, Getriebe und Naben aus dem 3D-Drucker.