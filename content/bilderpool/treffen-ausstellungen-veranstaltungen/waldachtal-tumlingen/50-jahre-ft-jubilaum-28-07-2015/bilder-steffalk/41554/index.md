---
layout: "image"
title: "H.A.R.R.Y.s Kugelbahn"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk071.jpg"
weight: "71"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41554
imported:
- "2019"
_4images_image_id: "41554"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "71"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41554 -->
Ein Kugeltrichter, wie er geformt sein soll. Nicht einfach schräg nach unten.