---
layout: "image"
title: "Schulprojekt"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk077.jpg"
weight: "77"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41560
imported:
- "2019"
_4images_image_id: "41560"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "77"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41560 -->
In einer achten (!) Klasse mussten sich die Schüler jeweils selbst eine Aufgabe stellen. Der Konstrukteur erbaute einen Roboter, der auf dem Boden fahren konnte, anhand von Papiercodes (hinten im Bild) erkannte, wo er anhalten musste, ...