---
layout: "image"
title: "Reitbereich, Springreiten, Papierflieger-Startmaschine"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk149.jpg"
weight: "149"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41632
imported:
- "2019"
_4images_image_id: "41632"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "149"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41632 -->
Beim Springreiten konnten tatsächlich die Streben abgeworfen werden - wie in echt.