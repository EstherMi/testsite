---
layout: "image"
title: "Schiff"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk059.jpg"
weight: "59"
konstrukteure: 
- "Arjen Neijsen"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41542
imported:
- "2019"
_4images_image_id: "41542"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "59"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41542 -->
Details auf dem Deck