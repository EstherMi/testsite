---
layout: "image"
title: "Schiff"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk061.jpg"
weight: "61"
konstrukteure: 
- "Arjen Neijsen"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41544
imported:
- "2019"
_4images_image_id: "41544"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "61"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41544 -->
Pneumatischer Baggerarm, der sich bis weit unter den Schiffsrumpf absenken kann.