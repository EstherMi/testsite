---
layout: "image"
title: "Bonbon-Automat (1)"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk046.jpg"
weight: "46"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41529
imported:
- "2019"
_4images_image_id: "41529"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "46"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41529 -->
Gebaut von einem jungen fischertechniker mit dem Sinn fürs Praktische!