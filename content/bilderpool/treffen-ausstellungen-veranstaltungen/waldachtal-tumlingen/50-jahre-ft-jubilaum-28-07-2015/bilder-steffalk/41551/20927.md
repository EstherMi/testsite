---
layout: "comment"
hidden: true
title: "20927"
date: "2015-07-29T15:03:30"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Hallo zusammen,

das Heberad (also das mit den Glasmurmeln) funktioniert ohne Probleme auch mit Kugeln bis runter zu 12mm Durchmesser. Der Schienenabstand müßte dann halt angepaßt oder gegen Flexschienen getauscht werden, sonst sind keine Änderungen nötig.

Grüße
H.A.R.R.Y.