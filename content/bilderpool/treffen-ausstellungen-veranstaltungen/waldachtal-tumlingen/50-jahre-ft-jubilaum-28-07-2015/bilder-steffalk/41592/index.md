---
layout: "image"
title: "Brückenbau"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk109.jpg"
weight: "109"
konstrukteure: 
- "Dirk Kutch"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41592
imported:
- "2019"
_4images_image_id: "41592"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "109"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41592 -->
Ein Blick durch die gebaute Brücke. Das Ur-Modell wurde wohl zur Verdeutlichung der Möglichkeiten der gerade neu herausgekommenen fischertechnik-Statikbauelemente und -kästen auf der 1970er Nürnberger Spielwarenmesse gebaut und gezeigt, wenn ich Artur Fischers Editorial im besagten Clubheft richtig deute.