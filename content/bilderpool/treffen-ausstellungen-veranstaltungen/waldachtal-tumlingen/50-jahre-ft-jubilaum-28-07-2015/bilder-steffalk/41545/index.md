---
layout: "image"
title: "Flipper, Kugelbahn"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk062.jpg"
weight: "62"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41545
imported:
- "2019"
_4images_image_id: "41545"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "62"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41545 -->
Der Monster-Flipper! Details ohne Ende, super gebaut, Lautsprecher dran, und schaut euch das PINBALL-Bild an! Das Modell wurde (nur mit der Rückwand heruntergeklappt) von zwei Leuten als Ganzes hereingetragen. Nein... hereingeschleppt!