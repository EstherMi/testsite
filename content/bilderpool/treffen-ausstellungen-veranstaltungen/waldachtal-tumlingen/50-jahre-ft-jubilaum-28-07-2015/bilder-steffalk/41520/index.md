---
layout: "image"
title: "Nagellack-Karussell"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk037.jpg"
weight: "37"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41520
imported:
- "2019"
_4images_image_id: "41520"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41520 -->
Von der fischertechnikerin für die praktisch veranlagte Dame!