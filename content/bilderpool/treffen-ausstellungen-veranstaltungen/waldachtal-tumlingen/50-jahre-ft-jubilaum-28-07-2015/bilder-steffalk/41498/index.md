---
layout: "image"
title: "Kirmesmodelle"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk015.jpg"
weight: "15"
konstrukteure: 
- "Jan-Willem Dekker"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41498
imported:
- "2019"
_4images_image_id: "41498"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41498 -->
Mit einer tollen Mechanik unterhalb des sich drehenden Teils: Der Arm dreht sich, darauf dreht sich die Plattform, und darin auch noch die Sitzgruppen.