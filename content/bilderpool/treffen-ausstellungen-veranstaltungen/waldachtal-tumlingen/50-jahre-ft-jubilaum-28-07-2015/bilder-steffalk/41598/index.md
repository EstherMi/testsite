---
layout: "image"
title: "Traktor, Putzmaschine"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk115.jpg"
weight: "115"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41598
imported:
- "2019"
_4images_image_id: "41598"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "115"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41598 -->
