---
layout: "image"
title: "Handbetriebener Flipper"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk121.jpg"
weight: "121"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41604
imported:
- "2019"
_4images_image_id: "41604"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "121"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41604 -->
Ideen muss man haben, dann geht sowas auch mit Handbetrieb und macht bestimmt viel Spaß!