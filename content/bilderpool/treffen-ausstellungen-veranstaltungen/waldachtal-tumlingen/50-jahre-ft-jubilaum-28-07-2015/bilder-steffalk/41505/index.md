---
layout: "image"
title: "Baumaschinen"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk022.jpg"
weight: "22"
konstrukteure: 
- "Claus Ludwig"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41505
imported:
- "2019"
_4images_image_id: "41505"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41505 -->
