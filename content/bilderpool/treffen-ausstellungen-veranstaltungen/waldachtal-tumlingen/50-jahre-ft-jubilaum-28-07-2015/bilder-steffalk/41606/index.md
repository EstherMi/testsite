---
layout: "image"
title: "Cooles Auto, Überhitzungsschutz"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk123.jpg"
weight: "123"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41606
imported:
- "2019"
_4images_image_id: "41606"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "123"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41606 -->
Ein futuristisches Fahrzeug, und eine klassisch gebaute Wärmefühlung: Am Balken oberhalb der Kerze befindet sich ein Ur-NTC-Widerstand aus dem ec-Elektronikkasten. Sobald der zu warm wird, schaltet das Relais den Ventilator und den Summer daneben an, und die Kerze wird ausgepustet. Für genügend Kerzennachschub sorgt das Lager rechts. Damit's schneller geht, heizte der Konstrukteur dem NTC auch mit einem großen Feuerzeug ein ;-)