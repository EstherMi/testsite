---
layout: "image"
title: "Flip-Flop-Techniken"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk088.jpg"
weight: "88"
konstrukteure: 
- "Hans Wijnsouw"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41571
imported:
- "2019"
_4images_image_id: "41571"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "88"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41571 -->
Jeweils ein Flip-Flop (1-bit-Speicher) mit allen fischertechnik-Technikgenerationen gebaut...