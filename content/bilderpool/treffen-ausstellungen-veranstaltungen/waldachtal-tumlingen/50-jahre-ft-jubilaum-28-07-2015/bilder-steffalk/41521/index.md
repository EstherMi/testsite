---
layout: "image"
title: "Hebekunst (1)"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk038.jpg"
weight: "38"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41521
imported:
- "2019"
_4images_image_id: "41521"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "38"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41521 -->
Der Antrieb erfolgt per Motor, aber das Umschalten der Hebe-Hebel pneumatisch.