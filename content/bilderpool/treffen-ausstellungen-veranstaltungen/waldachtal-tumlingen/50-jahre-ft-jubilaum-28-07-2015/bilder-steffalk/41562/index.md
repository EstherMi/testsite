---
layout: "image"
title: "Schulprojekt"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk079.jpg"
weight: "79"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41562
imported:
- "2019"
_4images_image_id: "41562"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "79"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41562 -->
... bis dieser über die Tischkante ragt...