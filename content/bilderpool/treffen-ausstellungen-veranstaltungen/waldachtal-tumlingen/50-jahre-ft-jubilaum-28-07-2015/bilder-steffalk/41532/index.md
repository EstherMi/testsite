---
layout: "image"
title: "Seilbahn (Bergstation)"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk049.jpg"
weight: "49"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41532
imported:
- "2019"
_4images_image_id: "41532"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "49"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41532 -->
