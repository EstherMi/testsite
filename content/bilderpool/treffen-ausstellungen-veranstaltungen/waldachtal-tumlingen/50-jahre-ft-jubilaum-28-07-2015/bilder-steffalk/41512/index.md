---
layout: "image"
title: "Fahrzeuge"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk029.jpg"
weight: "29"
konstrukteure: 
- "Claus Ludwig"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41512
imported:
- "2019"
_4images_image_id: "41512"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41512 -->
