---
layout: "image"
title: "Holz-Umschlagsplatz, Prototyp für Säge"
date: "2015-07-28T14:55:05"
picture: "bildervonsteffalk044.jpg"
weight: "44"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41527
imported:
- "2019"
_4images_image_id: "41527"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:05"
_4images_image_order: "44"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41527 -->
Rechts im Bild ein Schwungrad mit den älteren ft-Dauermagneten als Schwungmasse, die per Exzenter die den auf und ab schnellenden Sägenträger antreibt.