---
layout: "comment"
hidden: true
title: "20944"
date: "2015-07-30T11:37:51"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Kann denn der TXC als I²C-SLAVE arbeiten? Das extra USB-Kabel neben dem Fritz!-Stick sieht mir eher so aus, als ob der TXC am USB des RasPi hängt. Bennik kann uns das sicher genauer sagen.

Wo wir schon beim Fragen sind:
Welches OS läuft da auf dem Pi und wo bekomme ich den zugehörigen Treiber für den Fritz!-Stick her?

Grüße
H.A.R.R.Y.