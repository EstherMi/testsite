---
layout: "image"
title: "Roboterfahrzeug"
date: "2015-07-28T14:55:06"
picture: "bildervonsteffalk120.jpg"
weight: "120"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/41603
imported:
- "2019"
_4images_image_id: "41603"
_4images_cat_id: "3103"
_4images_user_id: "104"
_4images_image_date: "2015-07-28T14:55:06"
_4images_image_order: "120"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41603 -->
Der Gag daran ist aber, dass das Fahrzeug per Smartphone-App ferngesteuert wird - und das funktioniert ganz hervorragend!

Rechts im Bild die kleine Mini-BSB-Bahn, deren Teile man mitsamt Anleitungsblatt bei der Spritzgussvorführung geschenkt bekam.