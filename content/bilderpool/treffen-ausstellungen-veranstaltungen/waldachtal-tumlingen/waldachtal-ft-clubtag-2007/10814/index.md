---
layout: "image"
title: "Clubtag"
date: "2007-06-10T21:01:08"
picture: "ft-Clubtag_-_07.jpg"
weight: "4"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/10814
imported:
- "2019"
_4images_image_id: "10814"
_4images_cat_id: "980"
_4images_user_id: "9"
_4images_image_date: "2007-06-10T21:01:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10814 -->
