---
layout: "image"
title: "Anodentransporter (Aluminiumwerk)"
date: "2007-06-10T21:09:40"
picture: "ft-Clubtag_-_40.jpg"
weight: "14"
konstrukteure: 
- "Albert Kohl"
fotografen:
- "Markus Mack"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/10846
imported:
- "2019"
_4images_image_id: "10846"
_4images_cat_id: "1306"
_4images_user_id: "9"
_4images_image_date: "2007-06-10T21:09:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10846 -->
