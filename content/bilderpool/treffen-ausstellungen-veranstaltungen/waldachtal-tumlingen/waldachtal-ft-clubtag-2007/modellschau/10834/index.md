---
layout: "image"
title: "Brücke Detail"
date: "2007-06-10T21:06:44"
picture: "ft-Clubtag_-_28.jpg"
weight: "2"
konstrukteure: 
- "Albert Kohl"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/10834
imported:
- "2019"
_4images_image_id: "10834"
_4images_cat_id: "1306"
_4images_user_id: "9"
_4images_image_date: "2007-06-10T21:06:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10834 -->
