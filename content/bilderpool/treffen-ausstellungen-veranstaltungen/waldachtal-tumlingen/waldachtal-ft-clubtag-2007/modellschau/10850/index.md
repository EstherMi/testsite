---
layout: "image"
title: "Farb-LCD (RoboInt, thkais-Basic)"
date: "2007-06-10T21:11:39"
picture: "ft-Clubtag_-_44.jpg"
weight: "17"
konstrukteure: 
- "thkais"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/10850
imported:
- "2019"
_4images_image_id: "10850"
_4images_cat_id: "1306"
_4images_user_id: "9"
_4images_image_date: "2007-06-10T21:11:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10850 -->
