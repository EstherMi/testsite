---
layout: "image"
title: "Tiefbohrgerät"
date: "2007-06-10T21:09:06"
picture: "ft-Clubtag_-_37.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/10843
imported:
- "2019"
_4images_image_id: "10843"
_4images_cat_id: "1306"
_4images_user_id: "9"
_4images_image_date: "2007-06-10T21:09:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10843 -->
