---
layout: "image"
title: "Boot"
date: "2007-06-10T21:04:08"
picture: "ft-Clubtag_-_16.jpg"
weight: "4"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/10823
imported:
- "2019"
_4images_image_id: "10823"
_4images_cat_id: "1305"
_4images_user_id: "9"
_4images_image_date: "2007-06-10T21:04:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10823 -->
