---
layout: "image"
title: "Spuren 3"
date: "2007-06-10T21:06:24"
picture: "ft-Clubtag_-_27.jpg"
weight: "14"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/10833
imported:
- "2019"
_4images_image_id: "10833"
_4images_cat_id: "1305"
_4images_user_id: "9"
_4images_image_date: "2007-06-10T21:06:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10833 -->
