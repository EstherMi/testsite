---
layout: "image"
title: "Steuerpult Robo Explorer"
date: "2007-06-10T21:05:23"
picture: "ft-Clubtag_-_21.jpg"
weight: "9"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/10828
imported:
- "2019"
_4images_image_id: "10828"
_4images_cat_id: "1305"
_4images_user_id: "9"
_4images_image_date: "2007-06-10T21:05:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10828 -->
