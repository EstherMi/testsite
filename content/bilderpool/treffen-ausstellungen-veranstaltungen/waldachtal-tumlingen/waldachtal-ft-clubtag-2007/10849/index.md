---
layout: "image"
title: "Thkais, Harald, pilami, H. Engelke, Harold Jaarsma"
date: "2007-06-10T21:10:53"
picture: "ft-Clubtag_-_43.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/10849
imported:
- "2019"
_4images_image_id: "10849"
_4images_cat_id: "980"
_4images_user_id: "9"
_4images_image_date: "2007-06-10T21:10:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10849 -->
