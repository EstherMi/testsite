---
layout: "image"
title: "fischertechnik im Ortsmuseum Waldachtal"
date: "2005-05-30T20:17:49"
picture: "fischertechnik_u-t_Bauksten.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Hathi"
license: "unknown"
legacy_id:
- details/4311
imported:
- "2019"
_4images_image_id: "4311"
_4images_cat_id: "596"
_4images_user_id: "201"
_4images_image_date: "2005-05-30T20:17:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4311 -->
