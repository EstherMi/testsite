---
layout: "image"
title: "fischertechnik im Ortsmuseum Waldachtal"
date: "2005-05-30T20:17:49"
picture: "fischertechnik_Schulbaukasten_e3.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Hathi"
license: "unknown"
legacy_id:
- details/4309
imported:
- "2019"
_4images_image_id: "4309"
_4images_cat_id: "596"
_4images_user_id: "201"
_4images_image_date: "2005-05-30T20:17:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4309 -->
