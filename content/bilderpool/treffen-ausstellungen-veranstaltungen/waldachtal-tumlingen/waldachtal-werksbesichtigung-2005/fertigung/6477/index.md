---
layout: "image"
title: "Fertigung"
date: "2006-06-20T21:35:52"
picture: "werksbesichtigung018.jpg"
weight: "1"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/6477
imported:
- "2019"
_4images_image_id: "6477"
_4images_cat_id: "594"
_4images_user_id: "104"
_4images_image_date: "2006-06-20T21:35:52"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6477 -->
Hier kommen Bilder aus der Fertigungshalle eines Zulieferers, der für die fischerwerke fischertechnik-Teile produziert. Wir wurden mit dem Bus hingefahren und wieder zurückgebracht. Dieses kleine Werk liegt ein paar Kilometer von den fischerwerken entfernt. Hier die Halle im Überblick von einer Treppe aus fotografiert.