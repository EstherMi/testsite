---
layout: "image"
title: "Fertigung"
date: "2006-06-20T21:35:53"
picture: "werksbesichtigung043.jpg"
weight: "26"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/6502
imported:
- "2019"
_4images_image_id: "6502"
_4images_cat_id: "594"
_4images_user_id: "104"
_4images_image_date: "2006-06-20T21:35:53"
_4images_image_order: "43"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6502 -->
Der Herr im brauen Hemd im Vordergrund ist Herr Wolfarth von fischertechnik.