---
layout: "image"
title: "Fertigung"
date: "2006-06-20T21:35:52"
picture: "werksbesichtigung019.jpg"
weight: "2"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/6478
imported:
- "2019"
_4images_image_id: "6478"
_4images_cat_id: "594"
_4images_user_id: "104"
_4images_image_date: "2006-06-20T21:35:52"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6478 -->
Spritzgussmaschinen...