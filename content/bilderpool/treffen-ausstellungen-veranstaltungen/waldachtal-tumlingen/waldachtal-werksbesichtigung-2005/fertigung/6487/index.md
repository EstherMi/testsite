---
layout: "image"
title: "Fertigung"
date: "2006-06-20T21:35:53"
picture: "werksbesichtigung028.jpg"
weight: "11"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/6487
imported:
- "2019"
_4images_image_id: "6487"
_4images_cat_id: "594"
_4images_user_id: "104"
_4images_image_date: "2006-06-20T21:35:53"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6487 -->
