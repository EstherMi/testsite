---
layout: "image"
title: "Im Artur Fischer-Museum"
date: "2006-06-20T21:35:53"
picture: "werksbesichtigung053.jpg"
weight: "7"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/6512
imported:
- "2019"
_4images_image_id: "6512"
_4images_cat_id: "595"
_4images_user_id: "104"
_4images_image_date: "2006-06-20T21:35:53"
_4images_image_order: "53"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6512 -->
