---
layout: "image"
title: "Besuch im Artur Fischer Museum"
date: "2005-05-29T10:34:59"
picture: "SANY001045.jpg"
weight: "57"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["mari", "mari", "mari", "mari", "mari"]
uploadBy: "mari"
license: "unknown"
legacy_id:
- details/4266
imported:
- "2019"
_4images_image_id: "4266"
_4images_cat_id: "595"
_4images_user_id: "189"
_4images_image_date: "2005-05-29T10:34:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4266 -->
