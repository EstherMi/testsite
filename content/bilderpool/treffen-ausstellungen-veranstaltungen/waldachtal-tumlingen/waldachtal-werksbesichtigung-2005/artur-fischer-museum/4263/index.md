---
layout: "image"
title: "Besuch im Artur Fischer-Museum"
date: "2005-05-28T22:55:39"
picture: "SANY001042.jpg"
weight: "70"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["mari", "mari", "mari", "mari", "mari"]
uploadBy: "mari"
license: "unknown"
legacy_id:
- details/4263
imported:
- "2019"
_4images_image_id: "4263"
_4images_cat_id: "595"
_4images_user_id: "189"
_4images_image_date: "2005-05-28T22:55:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4263 -->
