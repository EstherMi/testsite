---
layout: "image"
title: "Schaukasten in der Nähe des Museums"
date: "2006-06-20T21:35:53"
picture: "werksbesichtigung102.jpg"
weight: "19"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/6561
imported:
- "2019"
_4images_image_id: "6561"
_4images_cat_id: "593"
_4images_user_id: "104"
_4images_image_date: "2006-06-20T21:35:53"
_4images_image_order: "102"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6561 -->
