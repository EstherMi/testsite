---
layout: "image"
title: "Schaukasten in der Nähe des Museums"
date: "2006-06-20T21:35:53"
picture: "werksbesichtigung105.jpg"
weight: "22"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/6564
imported:
- "2019"
_4images_image_id: "6564"
_4images_cat_id: "593"
_4images_user_id: "104"
_4images_image_date: "2006-06-20T21:35:53"
_4images_image_order: "105"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6564 -->
