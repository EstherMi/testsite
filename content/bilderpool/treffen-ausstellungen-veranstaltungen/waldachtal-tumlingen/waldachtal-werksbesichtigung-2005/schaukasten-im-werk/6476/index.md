---
layout: "image"
title: "Schautafeln im Empfangsraum"
date: "2006-06-20T21:35:52"
picture: "werksbesichtigung017.jpg"
weight: "17"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/6476
imported:
- "2019"
_4images_image_id: "6476"
_4images_cat_id: "593"
_4images_user_id: "104"
_4images_image_date: "2006-06-20T21:35:52"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6476 -->
