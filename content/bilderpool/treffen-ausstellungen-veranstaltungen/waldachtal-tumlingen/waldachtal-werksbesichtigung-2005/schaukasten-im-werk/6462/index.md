---
layout: "image"
title: "Schautafeln im Empfangsraum"
date: "2006-06-20T21:35:52"
picture: "werksbesichtigung003.jpg"
weight: "3"
konstrukteure: 
- "fischerwerke"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/6462
imported:
- "2019"
_4images_image_id: "6462"
_4images_cat_id: "593"
_4images_user_id: "104"
_4images_image_date: "2006-06-20T21:35:52"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6462 -->
