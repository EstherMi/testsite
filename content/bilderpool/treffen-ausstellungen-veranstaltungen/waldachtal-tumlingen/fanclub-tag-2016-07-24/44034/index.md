---
layout: "image"
title: "Brücke10"
date: "2016-07-27T18:03:54"
picture: "Brcke10.jpg"
weight: "11"
konstrukteure: 
- "Michael Stratmann"
fotografen:
- "FT"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft"
license: "unknown"
legacy_id:
- details/44034
imported:
- "2019"
_4images_image_id: "44034"
_4images_cat_id: "3255"
_4images_user_id: "560"
_4images_image_date: "2016-07-27T18:03:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44034 -->
