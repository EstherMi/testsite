---
layout: "image"
title: "3D Druck"
date: "2016-07-25T14:24:24"
picture: "ftfct35.jpg"
weight: "47"
konstrukteure: 
- "-?-"
fotografen:
- "david / patrick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/43995
imported:
- "2019"
_4images_image_id: "43995"
_4images_cat_id: "3255"
_4images_user_id: "2228"
_4images_image_date: "2016-07-25T14:24:24"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43995 -->
Auch die kleine Schachtel wurde sehr präzise gedruckt