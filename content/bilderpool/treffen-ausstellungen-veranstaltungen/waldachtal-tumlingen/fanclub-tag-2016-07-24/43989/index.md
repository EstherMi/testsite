---
layout: "image"
title: "3-Achsroboter"
date: "2016-07-25T14:24:24"
picture: "ftfct29.jpg"
weight: "41"
konstrukteure: 
- "-?-"
fotografen:
- "david / patrick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/43989
imported:
- "2019"
_4images_image_id: "43989"
_4images_cat_id: "3255"
_4images_user_id: "2228"
_4images_image_date: "2016-07-25T14:24:24"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43989 -->
Dieser Roboterarm wird über einen Arduino Mega mit einem eigens für den Einsatz im Fischertechnikmodell entwickelten Shield gesteuert, verpackt im schwarzen Gehäuse