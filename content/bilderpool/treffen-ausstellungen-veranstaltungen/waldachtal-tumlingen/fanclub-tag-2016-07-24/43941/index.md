---
layout: "image"
title: "Kinderkarussell, voll im Stil von 196x"
date: "2016-07-24T20:10:12"
picture: "fct16.jpg"
weight: "66"
konstrukteure: 
- "-?-"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/43941
imported:
- "2019"
_4images_image_id: "43941"
_4images_cat_id: "3255"
_4images_user_id: "4"
_4images_image_date: "2016-07-24T20:10:12"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43941 -->
