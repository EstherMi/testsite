---
layout: "image"
title: "Holzverarbeitung"
date: "2016-07-24T20:10:12"
picture: "fct26.jpg"
weight: "75"
konstrukteure: 
- "-?-"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/43951
imported:
- "2019"
_4images_image_id: "43951"
_4images_cat_id: "3255"
_4images_user_id: "4"
_4images_image_date: "2016-07-24T20:10:12"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43951 -->
