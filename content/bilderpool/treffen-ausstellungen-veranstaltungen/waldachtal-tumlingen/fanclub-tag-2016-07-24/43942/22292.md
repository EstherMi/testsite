---
layout: "comment"
hidden: true
title: "22292"
date: "2016-07-25T23:11:05"
uploadBy:
- "Dirk Fox"
license: "unknown"
imported:
- "2019"
---
Ok, wenn selbst so etwas mit fischertechnik geht und obendrein so wunderschön aussieht - wozu braucht der Mensch überhaupt noch Lego?
Gruß, Dirk