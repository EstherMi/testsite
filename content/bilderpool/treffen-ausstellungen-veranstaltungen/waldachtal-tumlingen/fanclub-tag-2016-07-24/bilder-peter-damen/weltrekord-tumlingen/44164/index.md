---
layout: "image"
title: "Weltrekord in Tumlingen"
date: "2016-08-02T19:02:31"
picture: "weltrekordintumlingen1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/44164
imported:
- "2019"
_4images_image_id: "44164"
_4images_cat_id: "3265"
_4images_user_id: "22"
_4images_image_date: "2016-08-02T19:02:31"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44164 -->
Montag 25 Juli 2016 im Zeitung :"Wachtachtal und Umgebung".