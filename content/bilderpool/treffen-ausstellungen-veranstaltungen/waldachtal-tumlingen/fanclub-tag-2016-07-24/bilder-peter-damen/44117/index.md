---
layout: "image"
title: "RoboPro  ftc-Community  Slide"
date: "2016-08-01T19:00:30"
picture: "fischertechnikfanclubtagtumlingen29.jpg"
weight: "29"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/44117
imported:
- "2019"
_4images_image_id: "44117"
_4images_cat_id: "3264"
_4images_user_id: "22"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44117 -->
