---
layout: "image"
title: "Santjohanser"
date: "2016-08-01T19:00:30"
picture: "fischertechnikfanclubtagtumlingen10.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/44098
imported:
- "2019"
_4images_image_id: "44098"
_4images_cat_id: "3264"
_4images_user_id: "22"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44098 -->
