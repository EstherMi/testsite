---
layout: "image"
title: "Eine der wenige Modellen aus Austria"
date: "2016-08-01T19:00:30"
picture: "fischertechnikfanclubtagtumlingen65.jpg"
weight: "65"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/44153
imported:
- "2019"
_4images_image_id: "44153"
_4images_cat_id: "3264"
_4images_user_id: "22"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "65"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44153 -->
