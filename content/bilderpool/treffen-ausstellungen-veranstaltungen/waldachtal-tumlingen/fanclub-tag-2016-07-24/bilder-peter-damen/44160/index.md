---
layout: "image"
title: "Fischertechnik -Fanclubtag-2016 After...."
date: "2016-08-01T19:00:30"
picture: "fischertechnikfanclubtagtumlingen72.jpg"
weight: "72"
konstrukteure: 
- "-?-"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/44160
imported:
- "2019"
_4images_image_id: "44160"
_4images_cat_id: "3264"
_4images_user_id: "22"
_4images_image_date: "2016-08-01T19:00:30"
_4images_image_order: "72"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44160 -->
