---
layout: "image"
title: "Hoverboard"
date: "2016-07-24T20:10:12"
picture: "fct14.jpg"
weight: "64"
konstrukteure: 
- "-?-"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/43939
imported:
- "2019"
_4images_image_id: "43939"
_4images_cat_id: "3255"
_4images_user_id: "4"
_4images_image_date: "2016-07-24T20:10:12"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43939 -->
Schwer zu fotografieren, weil ständig unterwegs und von unerfahrenen Piloten gesteuert, die beim Kavalierstart die Zelle in Rotation versetzen anstatt die Räder zum Drehen zu bringen.