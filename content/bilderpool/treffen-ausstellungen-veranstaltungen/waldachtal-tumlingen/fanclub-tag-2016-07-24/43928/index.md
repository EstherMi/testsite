---
layout: "image"
title: "Omni-Wheel"
date: "2016-07-24T20:10:12"
picture: "fct03.jpg"
weight: "54"
konstrukteure: 
- "-?-"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/43928
imported:
- "2019"
_4images_image_id: "43928"
_4images_cat_id: "3255"
_4images_user_id: "4"
_4images_image_date: "2016-07-24T20:10:12"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43928 -->
Da muss irgendwo eine Quelle sein: mit der Sorte waren gleich mehrere Fahrzeuge ausgestattet.