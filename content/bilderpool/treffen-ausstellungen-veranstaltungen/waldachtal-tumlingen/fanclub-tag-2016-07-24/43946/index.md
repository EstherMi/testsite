---
layout: "image"
title: "Und ft kann DOCH fliegen"
date: "2016-07-24T20:10:12"
picture: "fct21.jpg"
weight: "70"
konstrukteure: 
- "-?-"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/43946
imported:
- "2019"
_4images_image_id: "43946"
_4images_cat_id: "3255"
_4images_user_id: "4"
_4images_image_date: "2016-07-24T20:10:12"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43946 -->
oder: gebt mir Triebwerke, und ich bringe auch Klaviere zum Fliegen.