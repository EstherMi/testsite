---
layout: "image"
title: "Kompaktkran"
date: "2016-07-25T14:24:24"
picture: "ftfct23.jpg"
weight: "36"
konstrukteure: 
- "-?-"
fotografen:
- "david / patrick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/43983
imported:
- "2019"
_4images_image_id: "43983"
_4images_cat_id: "3255"
_4images_user_id: "2228"
_4images_image_date: "2016-07-25T14:24:24"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43983 -->
Irgendwoher kenne ich diesen Kran: https://ftcommunity.de/details.php?image_id=39166#col3