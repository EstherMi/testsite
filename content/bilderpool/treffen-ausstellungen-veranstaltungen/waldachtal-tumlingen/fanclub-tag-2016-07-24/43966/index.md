---
layout: "image"
title: "Industrieanlage"
date: "2016-07-25T14:24:24"
picture: "ftfct06.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "david / patrick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/43966
imported:
- "2019"
_4images_image_id: "43966"
_4images_cat_id: "3255"
_4images_user_id: "2228"
_4images_image_date: "2016-07-25T14:24:24"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43966 -->
