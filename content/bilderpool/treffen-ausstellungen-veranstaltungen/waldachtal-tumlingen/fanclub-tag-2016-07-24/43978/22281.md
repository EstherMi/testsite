---
layout: "comment"
hidden: true
title: "22281"
date: "2016-07-25T17:59:45"
uploadBy:
- "ThomasW"
license: "unknown"
imported:
- "2019"
---
Der Binärzähler besteht aus 12 Stufen, kann somit Werte von 0 bis 4095 (2^12-1) darstellen.
Jede 4096ste Kugel landet dann in der untersten Flexschiene und bleibt dort.
Bei dieser Aufnahme (kurz nach 15 Uhr) war der Zählerstand bei etwas über 20000 (5*4096 plus die Werte aller Hebel, die nach rechts schauen).
Am Ende waren's dann mehr als 24000 Kugeln.