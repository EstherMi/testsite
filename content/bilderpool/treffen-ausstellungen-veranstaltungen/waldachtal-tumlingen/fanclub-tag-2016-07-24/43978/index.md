---
layout: "image"
title: "Binäraddierer"
date: "2016-07-25T14:24:24"
picture: "ftfct18.jpg"
weight: "31"
konstrukteure: 
- "Thomas Wenk"
fotografen:
- "david / patrick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/43978
imported:
- "2019"
_4images_image_id: "43978"
_4images_cat_id: "3255"
_4images_user_id: "2228"
_4images_image_date: "2016-07-25T14:24:24"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43978 -->
Binäraddierer mit einer nützlichen Funktion: er zählt die Kugeln, die bereits den Parcour durchlaufen haben