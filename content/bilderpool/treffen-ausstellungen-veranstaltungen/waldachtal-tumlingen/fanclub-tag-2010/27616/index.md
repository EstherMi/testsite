---
layout: "image"
title: "Computing-Präsentation"
date: "2010-07-04T22:06:45"
picture: "FanClubTag2010_14.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "Martin W."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/27616
imported:
- "2019"
_4images_image_id: "27616"
_4images_cat_id: "1989"
_4images_user_id: "373"
_4images_image_date: "2010-07-04T22:06:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27616 -->
Hier hielt Janosch Kuffner den ganzen Tag über die Stellung, machte nicht einmal eine Mittagspause und war für sämtliche Fragen rund um Computing und die beiden aktuellen Interfaces LT Controller und TX Controller der richtige Ansprechpartner.