---
layout: "image"
title: "Preisvergabe am Ende"
date: "2010-07-04T22:09:26"
picture: "FanClubTag2010_16.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "Martin W."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/27618
imported:
- "2019"
_4images_image_id: "27618"
_4images_cat_id: "1989"
_4images_user_id: "373"
_4images_image_date: "2010-07-04T22:09:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27618 -->
Über den Tag hinweg konnten die Kids Torwandschießen und die Fühlwand erkunden. Wer die meisten Punkte hatte, bekam einen Super Fun Park, Platz 2 und 3 einen Universal mit Motor und alle Anderen einen Basic Carts oder Bikes zur Auswahl.