---
layout: "image"
title: "FT Fan Tag 2010"
date: "2010-07-05T16:39:56"
picture: "ftfanclubtag13.jpg"
weight: "13"
konstrukteure: 
- "Albert Kohl"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/27631
imported:
- "2019"
_4images_image_id: "27631"
_4images_cat_id: "1990"
_4images_user_id: "1162"
_4images_image_date: "2010-07-05T16:39:56"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27631 -->
Tunnelbohrer