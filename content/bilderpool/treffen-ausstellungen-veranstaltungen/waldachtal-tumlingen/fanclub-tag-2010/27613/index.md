---
layout: "image"
title: "Brickweddes Senior"
date: "2010-07-04T22:01:29"
picture: "FanClubTag2010_09.jpg"
weight: "7"
konstrukteure: 
- "Wilhelm Brickwedde Senior"
fotografen:
- "Martin W."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/27613
imported:
- "2019"
_4images_image_id: "27613"
_4images_cat_id: "1989"
_4images_user_id: "373"
_4images_image_date: "2010-07-04T22:01:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27613 -->
Diverse Luftgetriebene Motoren, Generatoren, usw. Ein paar echte Schmankerl. Sowas sehe ich immer wieder gerne.