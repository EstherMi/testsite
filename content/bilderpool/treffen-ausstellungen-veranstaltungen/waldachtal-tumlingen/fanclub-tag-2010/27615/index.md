---
layout: "image"
title: "Fotobeweis"
date: "2010-07-04T22:04:58"
picture: "FanClubTag2010_13.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "Martin W."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/27615
imported:
- "2019"
_4images_image_id: "27615"
_4images_cat_id: "1989"
_4images_user_id: "373"
_4images_image_date: "2010-07-04T22:04:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27615 -->
Für Alle, die es schon immer vermuteten: Ja, Thomas Kaiser (thkais) arbeitet jetzt bei Knobloch.