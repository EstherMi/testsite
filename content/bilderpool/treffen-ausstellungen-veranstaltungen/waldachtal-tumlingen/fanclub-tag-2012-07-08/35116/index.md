---
layout: "image"
title: "FanClubTag 2012"
date: "2012-07-10T15:34:25"
picture: "praefix09.jpg"
weight: "9"
konstrukteure: 
- "Elias Martin"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/35116
imported:
- "2019"
_4images_image_id: "35116"
_4images_cat_id: "2605"
_4images_user_id: "1162"
_4images_image_date: "2012-07-10T15:34:25"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35116 -->
Der Konstrukteur mit seiner CNC-Fräse