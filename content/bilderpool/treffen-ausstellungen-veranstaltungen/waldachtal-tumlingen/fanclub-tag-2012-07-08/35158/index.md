---
layout: "image"
title: "FanClubTag 2012"
date: "2012-07-10T15:34:26"
picture: "praefix51.jpg"
weight: "51"
konstrukteure: 
- "Thomas Kaltenbrunner (Kalti)"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/35158
imported:
- "2019"
_4images_image_id: "35158"
_4images_cat_id: "2605"
_4images_user_id: "1162"
_4images_image_date: "2012-07-10T15:34:26"
_4images_image_order: "51"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35158 -->
4D-Kino von Thomas Kaltenbrunner.
Auf dem Bildschirm lief die passende Streckenführung ab und der Wagen bewegte sich dazu. Die Streckenführung wurde mit dem PC-Spiel "Rollercoastertycoon 3" erstellt.