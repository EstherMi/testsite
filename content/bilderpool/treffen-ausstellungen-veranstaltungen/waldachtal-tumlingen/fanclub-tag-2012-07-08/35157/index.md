---
layout: "image"
title: "FanClubTag 2012"
date: "2012-07-10T15:34:26"
picture: "praefix50.jpg"
weight: "50"
konstrukteure: 
- "Thomas Kaltenbrunner (Kalti)"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/35157
imported:
- "2019"
_4images_image_id: "35157"
_4images_cat_id: "2605"
_4images_user_id: "1162"
_4images_image_date: "2012-07-10T15:34:26"
_4images_image_order: "50"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35157 -->
4D-Kino von Thomas Kaltenbrunner, rechts davon die Kompressoreinheit