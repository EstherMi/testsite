---
layout: "image"
title: "FanClubTag 2012"
date: "2012-07-10T15:34:26"
picture: "praefix53.jpg"
weight: "53"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/35160
imported:
- "2019"
_4images_image_id: "35160"
_4images_cat_id: "2605"
_4images_user_id: "1162"
_4images_image_date: "2012-07-10T15:34:26"
_4images_image_order: "53"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35160 -->
Dominoaufsteller von Thomas Kaltenbrunner