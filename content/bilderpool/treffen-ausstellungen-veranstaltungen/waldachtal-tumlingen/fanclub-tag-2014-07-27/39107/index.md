---
layout: "image"
title: "Portalkran"
date: "2014-07-31T07:07:13"
picture: "DSC00520_bearb.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "MickyW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- details/39107
imported:
- "2019"
_4images_image_id: "39107"
_4images_cat_id: "2924"
_4images_user_id: "1806"
_4images_image_date: "2014-07-31T07:07:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39107 -->
