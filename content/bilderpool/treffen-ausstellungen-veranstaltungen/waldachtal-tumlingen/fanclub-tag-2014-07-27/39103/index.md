---
layout: "image"
title: "Kran"
date: "2014-07-30T10:52:23"
picture: "DSC00522_bearb.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "MickyW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- details/39103
imported:
- "2019"
_4images_image_id: "39103"
_4images_cat_id: "2924"
_4images_user_id: "1806"
_4images_image_date: "2014-07-30T10:52:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39103 -->
