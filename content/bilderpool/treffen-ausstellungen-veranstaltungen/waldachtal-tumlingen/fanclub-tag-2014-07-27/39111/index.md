---
layout: "image"
title: "Glücksrad"
date: "2014-07-31T17:00:42"
picture: "DSC00542_bearb.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "MickyW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- details/39111
imported:
- "2019"
_4images_image_id: "39111"
_4images_cat_id: "2924"
_4images_user_id: "1806"
_4images_image_date: "2014-07-31T17:00:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39111 -->
