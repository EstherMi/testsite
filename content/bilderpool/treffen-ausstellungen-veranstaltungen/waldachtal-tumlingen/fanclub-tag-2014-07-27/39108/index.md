---
layout: "image"
title: "Neue Fernsteuerung via Bluetooth auf Android"
date: "2014-07-31T07:07:13"
picture: "DSC00537_bearb.jpg"
weight: "2"
konstrukteure: 
- "Florian und Benedikt"
fotografen:
- "MickyW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- details/39108
imported:
- "2019"
_4images_image_id: "39108"
_4images_cat_id: "2924"
_4images_user_id: "1806"
_4images_image_date: "2014-07-31T07:07:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39108 -->
Leider ist das Foto nicht sehr aussagekräftig. Vielleicht hat ja noch jemand ein etwas grösseres Foto von dem Datencode.