---
layout: "image"
title: "Fräsmaschine II"
date: "2014-07-31T17:00:42"
picture: "DSC00544_bearb.jpg"
weight: "7"
konstrukteure: 
- "kalti"
fotografen:
- "MickyW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- details/39113
imported:
- "2019"
_4images_image_id: "39113"
_4images_cat_id: "2924"
_4images_user_id: "1806"
_4images_image_date: "2014-07-31T17:00:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39113 -->
