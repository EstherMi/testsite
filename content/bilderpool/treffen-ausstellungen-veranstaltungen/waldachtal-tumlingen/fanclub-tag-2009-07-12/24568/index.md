---
layout: "image"
title: "Computing-Einführung"
date: "2009-07-12T17:00:17"
picture: "fanclubtag32.jpg"
weight: "36"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/24568
imported:
- "2019"
_4images_image_id: "24568"
_4images_cat_id: "1688"
_4images_user_id: "104"
_4images_image_date: "2009-07-12T17:00:17"
_4images_image_order: "32"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24568 -->
Es gab eine Einführung, wie das mit dem ft-Computing überhaupt funktioniert, und eine kleine Demo des TX mit RoboPro.