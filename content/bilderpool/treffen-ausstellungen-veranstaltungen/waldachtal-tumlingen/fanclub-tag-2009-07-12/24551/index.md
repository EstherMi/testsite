---
layout: "image"
title: "Luftturbine 2"
date: "2009-07-12T17:00:16"
picture: "fanclubtag15.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/24551
imported:
- "2019"
_4images_image_id: "24551"
_4images_cat_id: "1688"
_4images_user_id: "104"
_4images_image_date: "2009-07-12T17:00:16"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24551 -->
Hier eine Variante mit größeren Turbinenblättern. Die bewegt den Aufzug ebenfalls sehr schnell.