---
layout: "image"
title: "Noch mehr Generatoren"
date: "2009-07-12T17:00:16"
picture: "fanclubtag18.jpg"
weight: "22"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/24554
imported:
- "2019"
_4images_image_id: "24554"
_4images_cat_id: "1688"
_4images_user_id: "104"
_4images_image_date: "2009-07-12T17:00:16"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24554 -->
Hier wird der Antrieb sogar von einem Ventilator gekühlt, weil der Motor ansonsten knallheiße werden würde.