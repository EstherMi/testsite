---
layout: "image"
title: "Luftturbine 1"
date: "2009-07-12T17:00:16"
picture: "fanclubtag14.jpg"
weight: "18"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/24550
imported:
- "2019"
_4images_image_id: "24550"
_4images_cat_id: "1688"
_4images_user_id: "104"
_4images_image_date: "2009-07-12T17:00:16"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24550 -->
Pressluft treibt das Z40 an, wodurch der Generator links Strom abgibt. Mit so viel Druck dreht sich das sehr schnell.