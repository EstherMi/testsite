---
layout: "image"
title: "Spurensucher"
date: "2009-07-12T16:59:53"
picture: "fanclubtag02.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/24538
imported:
- "2019"
_4images_image_id: "24538"
_4images_cat_id: "1688"
_4images_user_id: "104"
_4images_image_date: "2009-07-12T16:59:53"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24538 -->
Es sind Ur-ft-Motoren mit Stufengetrieben verbaut.