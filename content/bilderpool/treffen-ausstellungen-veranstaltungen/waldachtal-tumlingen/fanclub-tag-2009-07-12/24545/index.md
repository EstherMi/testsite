---
layout: "image"
title: "Automatischer Toilettenpapierspender"
date: "2009-07-12T16:59:54"
picture: "fanclubtag09.jpg"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/24545
imported:
- "2019"
_4images_image_id: "24545"
_4images_cat_id: "1688"
_4images_user_id: "104"
_4images_image_date: "2009-07-12T16:59:54"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24545 -->
Eine schöne Idee mit ganz wenig Material umgesetzt, so soll's sein.