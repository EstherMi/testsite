---
layout: "image"
title: "Fallturm"
date: "2009-07-12T17:00:17"
picture: "fanclubtag24.jpg"
weight: "28"
konstrukteure: 
- "Fam. Brickwedde"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/24560
imported:
- "2019"
_4images_image_id: "24560"
_4images_cat_id: "1688"
_4images_user_id: "104"
_4images_image_date: "2009-07-12T17:00:17"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24560 -->
Ein Riesenteil mit starken Motoren, die den großen Passagierwagen in geschätzt einer halben Sekunde (!) von ganz unten nach ganz oben heben können.