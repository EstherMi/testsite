---
layout: "image"
title: "Lorenwagen"
date: "2009-07-12T16:59:54"
picture: "fanclubtag04.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/24540
imported:
- "2019"
_4images_image_id: "24540"
_4images_cat_id: "1688"
_4images_user_id: "104"
_4images_image_date: "2009-07-12T16:59:54"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24540 -->
Ich hab das Bild um 180° gedreht, damit man die Schrift lesen kann.