---
layout: "image"
title: "Noch ein Generator"
date: "2009-07-12T17:00:16"
picture: "fanclubtag17.jpg"
weight: "21"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/24553
imported:
- "2019"
_4images_image_id: "24553"
_4images_cat_id: "1688"
_4images_user_id: "104"
_4images_image_date: "2009-07-12T17:00:16"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24553 -->
Was genau die Messgeräte messen, weiß ich leider nicht.