---
layout: "image"
title: "FanClubTag2011"
date: "2011-07-28T21:44:15"
picture: "fct03.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/31406
imported:
- "2019"
_4images_image_id: "31406"
_4images_cat_id: "2337"
_4images_user_id: "1162"
_4images_image_date: "2011-07-28T21:44:15"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31406 -->
Der Stand von mir und meinem Bruder, mit dem Wasserspender, Münzautomat, der Raupe und dem Boot.