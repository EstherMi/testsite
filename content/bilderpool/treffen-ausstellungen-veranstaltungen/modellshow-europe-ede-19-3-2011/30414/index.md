---
layout: "image"
title: "EDE 25"
date: "2011-04-02T23:50:38"
picture: "ede25.jpg"
weight: "25"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/30414
imported:
- "2019"
_4images_image_id: "30414"
_4images_cat_id: "2260"
_4images_user_id: "144"
_4images_image_date: "2011-04-02T23:50:38"
_4images_image_order: "25"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30414 -->
Details der Manitowoc: die ganse construction zum ausfarhen der Ballastträger