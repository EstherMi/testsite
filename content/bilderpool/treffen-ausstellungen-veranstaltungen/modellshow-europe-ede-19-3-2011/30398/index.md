---
layout: "image"
title: "EDE 09"
date: "2011-04-02T23:50:38"
picture: "ede09.jpg"
weight: "9"
konstrukteure: 
- "Jan-Willen Dekker"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/30398
imported:
- "2019"
_4images_image_id: "30398"
_4images_cat_id: "2260"
_4images_user_id: "144"
_4images_image_date: "2011-04-02T23:50:38"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30398 -->
Kinder und ein Kirmis