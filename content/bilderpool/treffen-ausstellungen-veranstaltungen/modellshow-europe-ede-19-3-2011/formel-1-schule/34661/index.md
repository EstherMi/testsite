---
layout: "image"
title: "Ansicht2"
date: "2012-03-18T20:23:50"
picture: "renner5.jpg"
weight: "5"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/34661
imported:
- "2019"
_4images_image_id: "34661"
_4images_cat_id: "2557"
_4images_user_id: "182"
_4images_image_date: "2012-03-18T20:23:50"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34661 -->
Hier ist die CO2 Patrone zu sehen die das Fahrzeug antreibt.