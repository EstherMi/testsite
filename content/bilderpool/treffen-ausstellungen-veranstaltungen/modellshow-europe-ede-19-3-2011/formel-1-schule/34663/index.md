---
layout: "image"
title: "Von unten"
date: "2012-03-18T20:23:50"
picture: "renner7.jpg"
weight: "7"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/34663
imported:
- "2019"
_4images_image_id: "34663"
_4images_cat_id: "2557"
_4images_user_id: "182"
_4images_image_date: "2012-03-18T20:23:50"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34663 -->
Hier zu sehen ist die Fürhrungsnut.
In dieser Nute wird das Seil geführt an dem die Autos fahren