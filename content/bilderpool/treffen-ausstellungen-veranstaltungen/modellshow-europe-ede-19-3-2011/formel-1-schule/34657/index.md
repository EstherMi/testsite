---
layout: "image"
title: "Auf der Rennstrecke"
date: "2012-03-18T20:23:50"
picture: "renner1.jpg"
weight: "1"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/34657
imported:
- "2019"
_4images_image_id: "34657"
_4images_cat_id: "2557"
_4images_user_id: "182"
_4images_image_date: "2012-03-18T20:23:50"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34657 -->
Heute ist fischertechnik bei der Formel 1 gestartet.
Ich habe als technischer Berater ein Formel 1 Projekt einer Schule unterstützt.
http://www.f1inschools.de/
Dabei ist auch ein von mir gebautes Auto aus ft gestartet.