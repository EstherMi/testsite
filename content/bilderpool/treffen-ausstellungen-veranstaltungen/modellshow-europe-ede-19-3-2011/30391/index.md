---
layout: "image"
title: "EDE 02"
date: "2011-04-02T23:50:37"
picture: "ede02.jpg"
weight: "2"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/30391
imported:
- "2019"
_4images_image_id: "30391"
_4images_cat_id: "2260"
_4images_user_id: "144"
_4images_image_date: "2011-04-02T23:50:37"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30391 -->
Demag CC4800 II maßstab 1:16