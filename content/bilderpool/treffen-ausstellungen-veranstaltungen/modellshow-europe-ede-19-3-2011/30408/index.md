---
layout: "image"
title: "EDE 19"
date: "2011-04-02T23:50:38"
picture: "ede19.jpg"
weight: "19"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/30408
imported:
- "2019"
_4images_image_id: "30408"
_4images_cat_id: "2260"
_4images_user_id: "144"
_4images_image_date: "2011-04-02T23:50:38"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30408 -->
Details der Manitowoc: noch 2 Seilwinden