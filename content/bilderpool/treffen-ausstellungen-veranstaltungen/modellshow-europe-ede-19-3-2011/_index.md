---
layout: "overview"
title: "Modellshow Europe  EDE 19-3-2011"
date: 2019-12-17T18:33:09+01:00
legacy_id:
- categories/2260
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2260 --> 
Zum 19ten Mal wurde diesen Show organisiert.
Zum Xten Mal waren dieses Jahr sogar 6 Mitglieder des fischertechnikclubs-NL dabei. Auf gut 26m Tischlänge (80cm Breite) wurden 3 Große Kräne, 2 Brückelegepanzer und eine Kirmes aufgebaut.