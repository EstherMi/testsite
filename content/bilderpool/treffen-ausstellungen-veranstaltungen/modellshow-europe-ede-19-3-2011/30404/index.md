---
layout: "image"
title: "EDE 15"
date: "2011-04-02T23:50:38"
picture: "ede15.jpg"
weight: "15"
konstrukteure: 
- "Anton Jansen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/30404
imported:
- "2019"
_4images_image_id: "30404"
_4images_cat_id: "2260"
_4images_user_id: "144"
_4images_image_date: "2011-04-02T23:50:38"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30404 -->
Der Haken von der Manitowoc