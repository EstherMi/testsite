---
layout: "image"
title: "Hall Cafe de Buren in Boekelo"
date: "2007-05-15T14:49:47"
picture: "boekelo01.jpg"
weight: "1"
konstrukteure: 
- "fischertechnikclub Nederland"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/10410
imported:
- "2019"
_4images_image_id: "10410"
_4images_cat_id: "950"
_4images_user_id: "136"
_4images_image_date: "2007-05-15T14:49:47"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10410 -->
fichertechnikclub event 2007 Boekelo