---
layout: "image"
title: "help, information and products"
date: "2007-05-15T14:50:01"
picture: "boekelo20.jpg"
weight: "20"
konstrukteure: 
- "fischertechnikclub Nederland"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/10429
imported:
- "2019"
_4images_image_id: "10429"
_4images_cat_id: "950"
_4images_user_id: "136"
_4images_image_date: "2007-05-15T14:50:01"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10429 -->
fischertechnik Nederland (Freetime) was also present with help, information and products.