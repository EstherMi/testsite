---
layout: "image"
title: "Childeren in TiP corner"
date: "2007-05-15T14:49:48"
picture: "boekelo06.jpg"
weight: "6"
konstrukteure: 
- "fischertechnikclub Nederland"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/10415
imported:
- "2019"
_4images_image_id: "10415"
_4images_cat_id: "950"
_4images_user_id: "136"
_4images_image_date: "2007-05-15T14:49:48"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10415 -->
With thanks to fischertechnik Nederland (Harold Jaarsma).
TiP was alos a great succes.