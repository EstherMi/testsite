---
layout: "overview"
title: "Boekelo 2007"
date: 2019-12-17T18:20:32+01:00
legacy_id:
- categories/950
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=950 --> 
Event fischertechnikclub Nederland on the 12th of May in Boekelo near Enschede Netherlands.[br]At the hall of Cafe de Buren[br][br]Organisation commity :[br]Carel van Leeuwen, Franc Pots, Alex Schelfhorst, Lucas Weel[br][br]With the help from[br]Harold Jaarsma (fischertechnik Nederland), Johan Lankheet[br]Family Jansen and  family Tielemans (manifestatiecommisie fischertechnikclub Nederland)[br]Rob van Baal en Dave Gabeler (edactie clubblad fischertechnikclub Nederland)[br]and other members.