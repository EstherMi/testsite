---
layout: "image"
title: "Impression of the event"
date: "2007-05-15T14:50:12"
picture: "boekelo29.jpg"
weight: "29"
konstrukteure: 
- "fischertechnikclub Nederland"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/10438
imported:
- "2019"
_4images_image_id: "10438"
_4images_cat_id: "950"
_4images_user_id: "136"
_4images_image_date: "2007-05-15T14:50:12"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10438 -->
fichertechnikclub event 2007 Boekelo