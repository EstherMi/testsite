---
layout: "image"
title: "Impression of the event"
date: "2007-05-15T14:50:12"
picture: "boekelo27.jpg"
weight: "27"
konstrukteure: 
- "fischertechnikclub Nederland"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/10436
imported:
- "2019"
_4images_image_id: "10436"
_4images_cat_id: "950"
_4images_user_id: "136"
_4images_image_date: "2007-05-15T14:50:12"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10436 -->
fichertechnikclub event 2007 Boekelo