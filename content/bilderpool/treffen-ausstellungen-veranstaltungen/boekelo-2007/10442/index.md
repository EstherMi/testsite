---
layout: "image"
title: "The end of this event"
date: "2007-05-15T14:50:18"
picture: "boekelo33.jpg"
weight: "33"
konstrukteure: 
- "fischertechnikclub Nederland"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/10442
imported:
- "2019"
_4images_image_id: "10442"
_4images_cat_id: "950"
_4images_user_id: "136"
_4images_image_date: "2007-05-15T14:50:18"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10442 -->
After some houres the fichertechnikclub event 2007 Boekelo ended with an empty hall.
See you next year in Twente.