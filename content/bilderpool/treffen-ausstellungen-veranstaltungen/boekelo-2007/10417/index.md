---
layout: "image"
title: "Impression of the event"
date: "2007-05-15T14:49:48"
picture: "boekelo08.jpg"
weight: "8"
konstrukteure: 
- "fischertechnikclub Nederland"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/10417
imported:
- "2019"
_4images_image_id: "10417"
_4images_cat_id: "950"
_4images_user_id: "136"
_4images_image_date: "2007-05-15T14:49:48"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10417 -->
fichertechnikclub event 2007 Boekelo