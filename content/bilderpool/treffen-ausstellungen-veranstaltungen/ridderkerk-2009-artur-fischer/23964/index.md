---
layout: "image"
title: "Artur Fischer, Clemens Jansen + Andries Tieleman (video FT-Club-NL)"
date: "2009-05-10T11:33:49"
picture: "2009-RidderkerkArtur-Fischer_039.jpg"
weight: "16"
konstrukteure: 
- "Diverse"
fotografen:
- "Peter Damen (Poederoyen-NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/23964
imported:
- "2019"
_4images_image_id: "23964"
_4images_cat_id: "1645"
_4images_user_id: "22"
_4images_image_date: "2009-05-10T11:33:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23964 -->
