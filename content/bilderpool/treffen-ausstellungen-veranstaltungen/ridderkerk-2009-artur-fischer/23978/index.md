---
layout: "image"
title: "3D-Printer, Gabelstapler (heftruck), FT-Truck, Rotatie-display + Wehre-Dokumentation"
date: "2009-05-10T11:50:51"
picture: "2009-RidderkerkArtur-Fischer_080.jpg"
weight: "30"
konstrukteure: 
- "Peter Damen (Poederoyen-NL)"
fotografen:
- "Peter Damen (Poederoyen-NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/23978
imported:
- "2019"
_4images_image_id: "23978"
_4images_cat_id: "1645"
_4images_user_id: "22"
_4images_image_date: "2009-05-10T11:50:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23978 -->
