---
layout: "image"
title: "Alles fertig um Besuch Artur Fischer"
date: "2009-05-10T11:37:28"
picture: "2009-RidderkerkArtur-Fischer_062.jpg"
weight: "21"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Peter Damen (Poederoyen-NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/23969
imported:
- "2019"
_4images_image_id: "23969"
_4images_cat_id: "1645"
_4images_user_id: "22"
_4images_image_date: "2009-05-10T11:37:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23969 -->
