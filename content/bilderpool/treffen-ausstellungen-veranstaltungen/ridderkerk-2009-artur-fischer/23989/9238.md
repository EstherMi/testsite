---
layout: "comment"
hidden: true
title: "9238"
date: "2009-05-10T19:24:51"
uploadBy:
- "robvanbaal"
license: "unknown"
imported:
- "2019"
---
Als Geschenk und Danke für alle gute Kontakte seit fast 20 Jahren, haben wir als club eine Medaille bekommen aus der Serie "Erfinder & Tüftler aus Baden-Württemberg"; und wohl die Ausgabe mit Prof. Dr. h. c. Artur Fischer. Ausfürung in 205 gram Feinsilber. Ein tolles Geschenk.