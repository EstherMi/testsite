---
layout: "image"
title: "Alles fertig zum Besuch Artur Fischer"
date: "2009-05-10T11:33:49"
picture: "2009-RidderkerkArtur-Fischer_039_2.jpg"
weight: "18"
konstrukteure: 
- "Wim Starreveld"
fotografen:
- "Peter Damen (Poederoyen-NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/23966
imported:
- "2019"
_4images_image_id: "23966"
_4images_cat_id: "1645"
_4images_user_id: "22"
_4images_image_date: "2009-05-10T11:33:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23966 -->
