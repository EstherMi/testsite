---
layout: "image"
title: "Spielstation 'Bruder' (andere Hälfte)"
date: "2016-06-06T15:12:49"
picture: "badenwuerttembergspielt10.jpg"
weight: "10"
konstrukteure: 
- "101Entertainment"
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/43688
imported:
- "2019"
_4images_image_id: "43688"
_4images_cat_id: "3235"
_4images_user_id: "1557"
_4images_image_date: "2016-06-06T15:12:49"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43688 -->
Und damit auch wirklich genug da ist geht das noch auf der anderen Raumseite weiter.