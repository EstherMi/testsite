---
layout: "image"
title: "Location (Schönwetter)"
date: "2016-06-06T15:12:49"
picture: "badenwuerttembergspielt06.jpg"
weight: "6"
konstrukteure: 
- "Verschiedene Architekten"
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/43684
imported:
- "2019"
_4images_image_id: "43684"
_4images_cat_id: "3235"
_4images_user_id: "1557"
_4images_image_date: "2016-06-06T15:12:49"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43684 -->
Für schönes Wetter war dieser Standplatz ausgeguckt. Mit dem Brunnen nebendran richtig nett.