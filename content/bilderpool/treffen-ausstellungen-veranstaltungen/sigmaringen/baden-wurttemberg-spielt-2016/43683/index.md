---
layout: "image"
title: "Donaupegel"
date: "2016-06-06T15:12:49"
picture: "badenwuerttembergspielt05.jpg"
weight: "5"
konstrukteure: 
- "Verschiedene Architekten"
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/43683
imported:
- "2019"
_4images_image_id: "43683"
_4images_cat_id: "3235"
_4images_user_id: "1557"
_4images_image_date: "2016-06-06T15:12:49"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43683 -->
Für die heftigen Schauer der letzten Tage sieht das doch eigentlich ganz normal aus.

Und von wegen "blaue Donau". Ganz schön grünlich die Brühe und die Temperatur lädt auch nicht wirklich zum Baden ein.