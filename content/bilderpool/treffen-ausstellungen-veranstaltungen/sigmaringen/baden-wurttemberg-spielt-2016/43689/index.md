---
layout: "image"
title: "Spielstationen 'Schleich' und 'Playmobil'"
date: "2016-06-06T15:12:49"
picture: "badenwuerttembergspielt11.jpg"
weight: "11"
konstrukteure: 
- "101Entertainment"
fotografen:
- "R. Trapp"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/43689
imported:
- "2019"
_4images_image_id: "43689"
_4images_cat_id: "3235"
_4images_user_id: "1557"
_4images_image_date: "2016-06-06T15:12:49"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43689 -->
Welches Kinderherz schlägt da nicht höher?

Und ausser den Genannten waren da auch noch "Kosmos" und einige andere bekannte Marken vertreten.