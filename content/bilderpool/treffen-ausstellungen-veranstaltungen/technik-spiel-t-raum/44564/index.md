---
layout: "image"
title: "Die Vitrine vor der Bibliothek"
date: "2016-10-08T13:56:23"
picture: "technikspieltraum4.jpg"
weight: "4"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- details/44564
imported:
- "2019"
_4images_image_id: "44564"
_4images_cat_id: "3314"
_4images_user_id: "381"
_4images_image_date: "2016-10-08T13:56:23"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44564 -->
Dieses ist eine erste Werbung für die kommende Weihnachtsausstellung "Technik.Spiel.(T)Raum" im Richard-Brandt-Museum der Wedemark