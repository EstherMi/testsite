---
layout: "image"
title: "Die Drahtzieher waren am Werk"
date: "2016-10-25T14:48:43"
picture: "ingwershaengebruecketeil3.jpg"
weight: "3"
konstrukteure: 
- "Ingwer Carstens"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- details/44683
imported:
- "2019"
_4images_image_id: "44683"
_4images_cat_id: "3326"
_4images_user_id: "381"
_4images_image_date: "2016-10-25T14:48:43"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44683 -->
Der ganze Stolz von jahrelanger Arbeit.