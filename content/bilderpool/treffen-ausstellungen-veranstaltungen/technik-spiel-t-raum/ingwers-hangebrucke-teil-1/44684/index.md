---
layout: "image"
title: "Sie passt gerade so ins Bastelzimmer"
date: "2016-10-25T14:48:43"
picture: "ingwershaengebruecketeil4.jpg"
weight: "4"
konstrukteure: 
- "Ingwer Carstens"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- details/44684
imported:
- "2019"
_4images_image_id: "44684"
_4images_cat_id: "3326"
_4images_user_id: "381"
_4images_image_date: "2016-10-25T14:48:43"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44684 -->
Für den Transport wird Ingwer sie in 10 Tagen in mehrere Teile zerlegen, sodass wir sie mit meinem PKW-Anhänger die 40km ins Museum transportieren können.