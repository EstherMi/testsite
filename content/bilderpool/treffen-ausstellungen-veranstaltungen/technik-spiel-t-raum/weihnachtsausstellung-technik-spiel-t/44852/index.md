---
layout: "image"
title: "Kirmesgeschäft"
date: "2016-12-05T22:02:52"
picture: "technikspieltraum20.jpg"
weight: "20"
konstrukteure: 
- "Claus Ludwig"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/44852
imported:
- "2019"
_4images_image_id: "44852"
_4images_cat_id: "3340"
_4images_user_id: "2303"
_4images_image_date: "2016-12-05T22:02:52"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44852 -->
