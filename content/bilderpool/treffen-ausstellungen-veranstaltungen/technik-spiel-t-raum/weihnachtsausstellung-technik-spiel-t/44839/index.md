---
layout: "image"
title: "Turmbergbahn"
date: "2016-12-05T22:02:52"
picture: "technikspieltraum07.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/44839
imported:
- "2019"
_4images_image_id: "44839"
_4images_cat_id: "3340"
_4images_user_id: "2303"
_4images_image_date: "2016-12-05T22:02:52"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44839 -->
