---
layout: "image"
title: "Die Bastelecke"
date: "2016-12-05T22:02:52"
picture: "technikspieltraum29.jpg"
weight: "29"
konstrukteure: 
- "-?-"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/44861
imported:
- "2019"
_4images_image_id: "44861"
_4images_cat_id: "3340"
_4images_user_id: "2303"
_4images_image_date: "2016-12-05T22:02:52"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44861 -->
