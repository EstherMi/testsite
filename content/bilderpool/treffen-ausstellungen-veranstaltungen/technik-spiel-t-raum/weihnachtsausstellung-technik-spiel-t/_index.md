---
layout: "overview"
title: "Weihnachtsausstellung Technik.Spiel.(T)raum"
date: 2019-12-17T18:35:08+01:00
legacy_id:
- categories/3340
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3340 --> 
Hallo zusammen,

hier ein paar Bilder von der Weihnachtsausstellung Technik.Spiel.(T)raum im Richard-Brandt-Heimatmuseum
Gottfried-August-Bürger-Straße 3 in 30900 Wedemark im Ortsteil Bissendorf.

Die Bilder stammen vom Samstag 03.12.2016. An diesem Tag hatten wir 180 Besucher im Zeitraum von 15:00 - 21:00 Uhr.
Es herrschte Wohnzimmerathmosphäre. Viele Besucher kamen vom Weihnachtsmarkt und waren bestimmt mehr als 20 min
dort. Große Begeisterung fand der 3-D Drucker und verschiedene Modelle.

Wer mag kann gerne vorbei schauen. Die Ausstellung läuft noch bis zum Januar 2017, mehr dazu findet ihr auch unter:

https://forum.ftcommunity.de/viewtopic.php?f=4&t=3728&p=27514&hilit=RAUM#p27514

Grüße
Dirk und Ralf
