---
layout: "image"
title: "Das oberste Fach"
date: "2016-10-08T13:56:23"
picture: "technikspieltraum3.jpg"
weight: "3"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- details/44563
imported:
- "2019"
_4images_image_id: "44563"
_4images_cat_id: "3314"
_4images_user_id: "381"
_4images_image_date: "2016-10-08T13:56:23"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44563 -->
fischergeometric Schrägflächige Körper
fischertechnik 400S