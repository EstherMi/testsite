---
layout: "image"
title: "Lipper Modellbautage 2015"
date: "2015-02-05T16:28:24"
picture: "lippermodellbautage04.jpg"
weight: "4"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wol"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/40441
imported:
- "2019"
_4images_image_id: "40441"
_4images_cat_id: "3033"
_4images_user_id: "968"
_4images_image_date: "2015-02-05T16:28:24"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40441 -->
Immer viel Betrieb am Riesenrad. Interessant ist oft die Reaktionen der Besucher.
Fragen wie : " Haben Sie das selbst gebaut ? ", "Gibt es das als Baukasten ? ",
" Woher haben Sie denn die Baupläne ? " u.s.w. kamen ständig.