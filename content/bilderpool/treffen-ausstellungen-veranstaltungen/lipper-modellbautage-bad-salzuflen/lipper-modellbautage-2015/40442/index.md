---
layout: "image"
title: "Lipper Modellbautage 2015"
date: "2015-02-05T16:28:24"
picture: "lippermodellbautage05.jpg"
weight: "5"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wol"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/40442
imported:
- "2019"
_4images_image_id: "40442"
_4images_cat_id: "3033"
_4images_user_id: "968"
_4images_image_date: "2015-02-05T16:28:24"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40442 -->
Die Kugelbahn war wieder der Hit.
Natürlich vor allem die Kinder waren fasziniert.
Wahrscheinlich hat es damit zu tun , das alles so langsam läuft,
bunt und nachvollziehbar ist ?