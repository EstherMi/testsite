---
layout: "image"
title: "Lipper Modelbautage"
date: "2014-01-29T18:19:42"
picture: "lippermodellbautagebadsalzuflen3.jpg"
weight: "3"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/38142
imported:
- "2019"
_4images_image_id: "38142"
_4images_cat_id: "2838"
_4images_user_id: "968"
_4images_image_date: "2014-01-29T18:19:42"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38142 -->
Die Kugelbahn war das am meisten frequentierte Modell.