---
layout: "image"
title: "Lipper Modedllbautage 2018"
date: "2018-01-25T16:43:57"
picture: "lippermodellbautage4.jpg"
weight: "4"
konstrukteure: 
- "Fam.Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/47193
imported:
- "2019"
_4images_image_id: "47193"
_4images_cat_id: "3491"
_4images_user_id: "968"
_4images_image_date: "2018-01-25T16:43:57"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47193 -->
Hier mein Faun Elefant mit Brückenkran und LGB Bahn sowie Kugelbahn im Hintergrund.