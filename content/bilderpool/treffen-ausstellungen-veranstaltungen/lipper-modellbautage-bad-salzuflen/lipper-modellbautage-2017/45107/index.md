---
layout: "image"
title: "Lipper Modellbautage 2017"
date: "2017-01-30T17:08:11"
picture: "lippermodellbautage3.jpg"
weight: "3"
konstrukteure: 
- "Fam. Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/45107
imported:
- "2019"
_4images_image_id: "45107"
_4images_cat_id: "3359"
_4images_user_id: "968"
_4images_image_date: "2017-01-30T17:08:11"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45107 -->
Die Ewigkeitsmaschine wurde oft bestaunt. Genauso interessant auch, viele  haben garnicht gepeilt, was das sein soll, trotz
beiliegender Beschschreibung. Der Spruch : ".....das ist kapput, das dreht sich ja garnicht mehr ." war mehrmals zu hören :)