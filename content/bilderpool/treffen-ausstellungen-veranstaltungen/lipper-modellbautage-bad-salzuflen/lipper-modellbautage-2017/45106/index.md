---
layout: "image"
title: "Lipper Modellbautage 2017"
date: "2017-01-30T17:08:11"
picture: "lippermodellbautage2.jpg"
weight: "2"
konstrukteure: 
- "Fam. Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/45106
imported:
- "2019"
_4images_image_id: "45106"
_4images_cat_id: "3359"
_4images_user_id: "968"
_4images_image_date: "2017-01-30T17:08:11"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45106 -->
Dieses Jahr zum ersten mal ohne echtes Großmodell. Aber die Resonanz war wieder sehr gut.
Die Tischtennisballbahn,der Tagebaubagger ,div. Trucks, Kugelbahnen und Fahrgeschäfte
sorgten für großes Interesse.