---
layout: "overview"
title: "Lipper Modellbautage 2017"
date: 2019-12-17T18:33:18+01:00
legacy_id:
- categories/3359
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3359 --> 
Dieses Jahr habe ich zum dritten mal an den" Lipper Modellbautagen" teilgenommen.
Die Resonanz war wieder sehr positiv. Mittlerweile läßt der Veranstalter auch die "Werbung " am Stand kommentarlos durchgehen.
Das hat mich aber trotzdem nicht vor den: "Oh guck  mal.... Lego " Spüchen bewahrt.
Im Gegensatz zu meiner ersten Ausstellung 2014 ziehe ich einfach mal eine positive Bilanz
und werde nächstes Jahr vorraussichtlich wieder dabei sein.
Vieleicht fühlt sich ja der eine oder andere bemüßigt, uns zu untertstützen. Werde zu dem Thema im Forum zu gegebener
Zeit noch mal nachfragen.

