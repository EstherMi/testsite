---
layout: "overview"
title: "Lipper Modellbautage ,Bad Salzuflen"
date: 2019-12-17T18:33:16+01:00
legacy_id:
- categories/2838
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2838 --> 
Lipper Modellbautage 2014

Wie schon angekündigt, hier mal ein Fazit meiner FT Präsentation bei den Lipper Modellbautagen.
Das ganze fand statt auf ca.12.000 qm²  einer Halle. Schwerpunktmäßig wurden Eisenbahnen in allen Größen gezeigt. Dazu Schiffsmodelle, Trucks, Agrarmodellbau mit entsprechenden Vorführparcours und natürlich alle erdenklichen Heli und Flugzeugaustel ler sowie Händler zu allen Themenbereichen.
Ich hatte mich selbst um die Teilnahme bemüht und nachdem ich den Veranstalter davon überzeugt hatte das ich nichts verkaufe, fand ich am Donnerstag  Mittag die 6 bestellten Tische und reichlich Strom vor. Nach 6 Std war der Aufbau getan und am Freitag morgen ging es um 10 Uhr los.

Der Freitag war eher noch zögerlich . Samstag und Sonntag hingegen waren sehr gut besucht und ich hatte kaum die Möglichkeit  meinen Stand zu verlassen . An dem ganzen Wochenende waren ca.
17.000 Besuchen in der Halle. Geschätz kamen vone denen  ca. die Hälfte auch mal bei mir am Stand vorbei. Viele gingen  weiter aber mehr noch blieben stehen und schauten.
Wenn man also da so sitz und es kommen im Laufe dieser 3 Tage mehre tausend Menschen vorbei,...da kann man schon Studien betreiben.

Ich hatte ca 30 Exponate verschiedener Größe ausgestellt, wovon 6 Austellungsstücke für das Publikum selbst zu betätigen waren. Die waren immer sehr beliebt  .

Ich werde mal versuchen, meinen subjektiven Eindruck mittels aufgeschnappter Aussagen um mich herum zu vermitteln. 

1)	" Oh guck mal ,FISCHERTECHNIK " (die älteren)
2)	"Was ist das denn ? " (viele Kinder)
3)	"Oh guck mal LEGO"(noch mehr Kinder, worauf der Vater sagt: "das ist FISCHERTECHNIK" )
4)	"Ich wußte gar nicht das es das noch gibt " 
5)	"Das habe wir auch noch irgendwo "
6)	"Das hatte ich als Kind auch mal"
7)	"Ich wusste gar, nicht was man da alles mit bauen kann "
8)	"Wo kann man das denn noch kaufen? " (mein Standartsatz. "Amazon,Knobloch")
9)	"Bekommt man da auch noch Einzelteile ?",( meine Antwort: Knobloch,FFW)
10)	 usw ,usw, 

Für Marketingleute wäre das ein gefundenes Fressen gewesen. Mein persönliches Fazit:
die Marke "FISCHERTECHNIK" ist in den Köpfen wie "JEEP,TESAFILM,LEGO oder sonst was. Sehr viele kennen die Marke ,trotzt dem ist man auf so einer Veranstaltung ein völliger Exot. 

Für mich hat sich das ganze in der Weise gelohnt, als  das ich auf einem nicht FT Treffen ganz viele begeisterte Gesichter von Jung und Alt gesehen habe. 
Der Aufwand war allerdings beträchtlich. 1,5 Tage Urlaub, 400 gefahrene km und das ganze Wochenende weg. Dazu kam noch ein VÖLLIGES Desintresse des Veranstalters .
Somit verbuche ich das ganz unter "Außer Spesen nix gewesen " :) und werde das wahrscheinlich nicht nochmal machen.

Da freut man sich doch wieder auf EB un MS :)

Gruß
Markus Wolf


