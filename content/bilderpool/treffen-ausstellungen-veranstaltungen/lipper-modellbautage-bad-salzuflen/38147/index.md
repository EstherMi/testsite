---
layout: "image"
title: "Lipper Modelbautage"
date: "2014-01-29T18:19:42"
picture: "lippermodellbautagebadsalzuflen8.jpg"
weight: "8"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/38147
imported:
- "2019"
_4images_image_id: "38147"
_4images_cat_id: "2838"
_4images_user_id: "968"
_4images_image_date: "2014-01-29T18:19:42"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38147 -->
Mit so viel Interesse habe ich nicht gerechnet.In den 3 Tagen waren ca. 17.000 Besucher in der Halle.
Die Zahl derer die bei mir geschaut haben dürfte sich im Bereich von mehreren tsd. bewegt haben.