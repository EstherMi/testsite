---
layout: "image"
title: "Packstation"
date: "2007-11-08T23:06:54"
picture: "seevetalhittfeld05.jpg"
weight: "5"
konstrukteure: 
- "ft"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- details/12545
imported:
- "2019"
_4images_image_id: "12545"
_4images_cat_id: "1130"
_4images_user_id: "109"
_4images_image_date: "2007-11-08T23:06:54"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12545 -->
Am unteren Ende des Trichters wurde ein Beutel über gestreift und mit Hilfe von ft-Pneumatik festgehalten.