---
layout: "image"
title: "Packstation"
date: "2007-11-08T23:06:53"
picture: "seevetalhittfeld03.jpg"
weight: "3"
konstrukteure: 
- "ft"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- details/12543
imported:
- "2019"
_4images_image_id: "12543"
_4images_cat_id: "1130"
_4images_user_id: "109"
_4images_image_date: "2007-11-08T23:06:53"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12543 -->
Aufbewarungsboxen zum selber packen eines Modells.
Vor den Boxen liegen 3 Bretter mit Mulden, jede Mulde für ein ft-Teil, damit man nicht die Teile zählen muss.
die Mulden werden entsprechend ihrer Bezeichnung mit ft-Teil bestückt.
Am Ende wird alles in den Trichter gekippt und der gefüllte Beutel in einen Karton mit Anleitung gepackt.