---
layout: "image"
title: "ft Ferris Wheel and the Brain"
date: "2009-05-14T23:07:48"
picture: "sm_fw_1.jpg"
weight: "43"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/24026
imported:
- "2019"
_4images_image_id: "24026"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-05-14T23:07:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24026 -->
ft Ferris Wheel and the BRAIN!