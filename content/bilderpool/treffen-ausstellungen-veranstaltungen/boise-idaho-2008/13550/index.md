---
layout: "image"
title: "Building at Boise State University"
date: "2008-02-04T22:57:16"
picture: "eday_08_plans_b.jpg"
weight: "118"
konstrukteure: 
- "Students"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Discover", "Engineering", "Boise", "State", "University", "National", "Engineers", "Week"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/13550
imported:
- "2019"
_4images_image_id: "13550"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2008-02-04T22:57:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13550 -->
PCS Edventures hosted a ft building room at Boise State University's Science Extravaganza (2-2-08) as part of Discover Engineering. These are pictures of students constructing simple ft models.  

(Google Translation) PCS Edventures Gastgeber einer ft Gebäude Zimmer in Boise 
State University's Science Extravaganza (2-2-08) als Teil der Entdecken Engineering. Es sind Bilder von Studenten
Bau von einfachen Modellen FT.