---
layout: "image"
title: "Amelia Constructing the Rotating Swing"
date: "2009-06-02T18:45:24"
picture: "amelia_ft_swing.jpg"
weight: "26"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Amelia", "PCS", "Edventures"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/24154
imported:
- "2019"
_4images_image_id: "24154"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-06-02T18:45:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24154 -->
Mhy daughter spent the day with me at the office. We build ft models of course! Thought to share.