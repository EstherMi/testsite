---
layout: "image"
title: "ft Walker"
date: "2008-11-21T19:43:23"
picture: "ft_walker_4b.jpg"
weight: "71"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["walking", "robot"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/16471
imported:
- "2019"
_4images_image_id: "16471"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2008-11-21T19:43:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16471 -->
These are various walkers I built over the year. Thought to share.