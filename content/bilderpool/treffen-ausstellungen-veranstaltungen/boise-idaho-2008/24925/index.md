---
layout: "image"
title: "Dragster 2"
date: "2009-09-17T16:40:15"
picture: "sm_dragster_09.jpg"
weight: "21"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["dragster", "PCS", "BRAIN"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/24925
imported:
- "2019"
_4images_image_id: "24925"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-09-17T16:40:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24925 -->
This is a new version of the ft dragster integrated with the PCS BRAIN. 

***google translation***
	
Dies ist eine neue Version des ft Dragster mit dem PCS BRAIN integriert.