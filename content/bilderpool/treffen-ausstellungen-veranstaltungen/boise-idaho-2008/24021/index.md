---
layout: "image"
title: "Smart House Model"
date: "2009-05-14T16:34:28"
picture: "sm_smart_house_v1.jpg"
weight: "42"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["PCS", "BRAIN"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/24021
imported:
- "2019"
_4images_image_id: "24021"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-05-14T16:34:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24021 -->
A model we are integrating with the PCS BRAIN. Doorbell, porchlight, internal lights, automatic door, and air fan all included.