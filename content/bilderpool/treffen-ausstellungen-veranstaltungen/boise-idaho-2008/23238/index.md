---
layout: "image"
title: "Barbara Morgan and ft models"
date: "2009-02-28T07:56:58"
picture: "sm_morgan_group.jpg"
weight: "51"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Astronaut", "Barbara", "Morgan"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/23238
imported:
- "2019"
_4images_image_id: "23238"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-02-28T07:56:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23238 -->
Astronaut Barbara Morgan visited our offices and was examining our ft models! Thought to share.