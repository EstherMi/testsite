---
layout: "comment"
hidden: true
title: "8657"
date: "2009-03-03T21:24:09"
uploadBy:
- "ft_idaho"
license: "unknown"
imported:
- "2019"
---
Howdy, 

Barbara Morgan logged over 305 hours in space. 

She is a teacher (from Idaho) who was the backup candidate for the NASA Teacher in Space Program. She trained with Christa McAuliffe and the Challenger crew at NASA. 

http://www.jsc.nasa.gov/Bios/htmlbios/morgan.html