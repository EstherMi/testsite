---
layout: "image"
title: "Governor's Innovation Summit"
date: "2009-09-24T23:27:28"
picture: "ft_booth2.jpg"
weight: "20"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Idaho", "Governor's", "Innovation", "Summit"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/25356
imported:
- "2019"
_4images_image_id: "25356"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-09-24T23:27:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25356 -->
The PCS Booth at the Governor's Innovation Summit ... featuring fischertechnik and the the PCS BRAIN!