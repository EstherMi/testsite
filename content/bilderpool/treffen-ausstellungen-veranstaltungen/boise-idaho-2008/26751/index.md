---
layout: "image"
title: "Classroom Project"
date: "2010-03-19T22:46:11"
picture: "sm_brain_ft_3.jpg"
weight: "2"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["PCS", "Brain"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/26751
imported:
- "2019"
_4images_image_id: "26751"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2010-03-19T22:46:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26751 -->
Conducted a building session at Washington Elementary!