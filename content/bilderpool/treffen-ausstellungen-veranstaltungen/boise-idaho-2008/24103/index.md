---
layout: "image"
title: "Amelia Builds"
date: "2009-05-26T13:53:28"
picture: "a_ft_1.jpg"
weight: "36"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Amelia"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/24103
imported:
- "2019"
_4images_image_id: "24103"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-05-26T13:53:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24103 -->
My daughter building with ft over Memorial Day weekend, 2009. Thought to share.