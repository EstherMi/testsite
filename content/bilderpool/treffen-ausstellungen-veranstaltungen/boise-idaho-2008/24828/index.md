---
layout: "image"
title: "Amelia build the tractor"
date: "2009-08-25T00:48:17"
picture: "sm_ft_amelia_c.jpg"
weight: "23"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Amelia", "Universal", "II"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/24828
imported:
- "2019"
_4images_image_id: "24828"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-08-25T00:48:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24828 -->
Amelia builds a second model from U II.