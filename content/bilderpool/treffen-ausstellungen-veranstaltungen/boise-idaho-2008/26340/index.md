---
layout: "image"
title: "Robotic Sliding Door"
date: "2010-02-13T15:23:27"
picture: "ft_sliding_door.jpg"
weight: "13"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["PCS", "Brain"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/26340
imported:
- "2019"
_4images_image_id: "26340"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2010-02-13T15:23:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26340 -->
A PCS robotics lab programmed this automatic door.