---
layout: "comment"
hidden: true
title: "7161"
date: "2008-09-16T11:03:13"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ok, da hat Google ein bisschen daneben gelangt. Für die nicht-englischsprechenden: "Thought to share" soll wohl sowas wie "Ich dachte ich zeig Euch das mal" heißen, und nicht "Vermutlich Aktie." ;-)

Gruß,
Stefan