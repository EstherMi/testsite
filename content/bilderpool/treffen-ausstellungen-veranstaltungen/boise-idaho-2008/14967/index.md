---
layout: "image"
title: "ERC Summer Camp"
date: "2008-07-30T10:17:34"
picture: "erc_belt_drive.jpg"
weight: "80"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["PCS", "robot"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/14967
imported:
- "2019"
_4images_image_id: "14967"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2008-07-30T10:17:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14967 -->
These are images of students in a Robotics Summer Camp in Port of Spain, Trinidad & Tobago. They are using ft in the PCS Edventures in Robotics Challenge. 

Thought to share. 

Thought to share. 

***google translation:
Es sind Bilder von Studenten in einer Robotik Sommercamp in Port of Spain, Trinidad & Tobago. Sie sind mit ft in der PCS Edventures Robotik-Challenge.

Vermutlich Aktie.