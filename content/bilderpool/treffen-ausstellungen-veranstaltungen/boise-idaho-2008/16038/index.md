---
layout: "image"
title: "August at Play"
date: "2008-10-25T09:37:17"
picture: "aug_1.jpg"
weight: "73"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["August"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/16038
imported:
- "2019"
_4images_image_id: "16038"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2008-10-25T09:37:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16038 -->
My son playing with models in my office.