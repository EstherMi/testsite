---
layout: "image"
title: "Battery Test"
date: "2009-05-30T09:12:54"
picture: "motor_test_6.jpg"
weight: "27"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["PCS", "BRAIN"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/24126
imported:
- "2019"
_4images_image_id: "24126"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-05-30T09:12:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24126 -->
Testing the PCS BRAIN with a single rechargeable 9v battery.