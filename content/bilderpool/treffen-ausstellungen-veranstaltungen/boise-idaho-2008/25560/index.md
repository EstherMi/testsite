---
layout: "image"
title: "Building in Boise"
date: "2009-10-18T16:08:02"
picture: "sm_shelly.jpg"
weight: "18"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Boise"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/25560
imported:
- "2019"
_4images_image_id: "25560"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-10-18T16:08:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25560 -->
Lots of building in the office today! Thought to share.