---
layout: "image"
title: "Building with the PCS BRAIN"
date: "2009-01-31T00:06:29"
picture: "sm_build_b.jpg"
weight: "56"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["PCS", "BRAIN"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/17219
imported:
- "2019"
_4images_image_id: "17219"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-01-31T00:06:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17219 -->
Building with ft and the PCS BRAIN. Thought to share.