---
layout: "image"
title: "ft Sighting ITEA 2008"
date: "2008-02-29T10:12:00"
picture: "sm_ft_2.jpg"
weight: "90"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["PCS", "Edventures", "ITEA", "2008", "Salt", "Lake", "City"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/13808
imported:
- "2019"
_4images_image_id: "13808"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2008-02-29T10:12:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13808 -->
These are ft sightings at the 2008 ITEA conference in Salt Lake City.  

Google Translation: Diese sind ft Sichtungen in der ITEA-Konferenz 2008 in Salt Lake City.