---
layout: "image"
title: "Sunday School Robotics"
date: "2010-03-15T16:56:39"
picture: "sm_ft_ucc5.jpg"
weight: "6"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["PCS", "Brain"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/26702
imported:
- "2019"
_4images_image_id: "26702"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2010-03-15T16:56:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26702 -->
Taught a robotics class in Sunday School at my church.