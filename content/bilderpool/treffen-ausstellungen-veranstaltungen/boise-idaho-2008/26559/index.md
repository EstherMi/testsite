---
layout: "image"
title: "Robot Magazine Image"
date: "2010-02-27T10:39:07"
picture: "robot_image_1.jpg"
weight: "9"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Robot", "Magazine", "PCS", "Brain"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/26559
imported:
- "2019"
_4images_image_id: "26559"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2010-02-27T10:39:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26559 -->
I love this image from Robot Magazine! 4 year old Declan builds with ft!