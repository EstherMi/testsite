---
layout: "image"
title: "LED Guessing Game"
date: "2009-05-19T17:24:07"
picture: "led_game_b1.jpg"
weight: "39"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["LED", "Guessing", "Game", "PCS", "BRAIN"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/24059
imported:
- "2019"
_4images_image_id: "24059"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-05-19T17:24:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24059 -->
A second picture. This one depicts one of the plates flipped up to reveal the lit LED.