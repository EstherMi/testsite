---
layout: "image"
title: "Vote"
date: "2008-11-05T06:09:23"
picture: "Vote2.jpg"
weight: "72"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Vote"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/16173
imported:
- "2019"
_4images_image_id: "16173"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2008-11-05T06:09:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16173 -->
For the American audience. Today is election day.