---
layout: "image"
title: "LED Guess-O-Matic"
date: "2009-05-21T12:10:09"
picture: "smaller_s5_lg_fini.jpg"
weight: "37"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["LED", "GUESS-O-MATIC", "PCS", "BRAIN"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/24060
imported:
- "2019"
_4images_image_id: "24060"
_4images_cat_id: "1242"
_4images_user_id: "585"
_4images_image_date: "2009-05-21T12:10:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24060 -->
My first entry into www.instructables.com "Get the LED Out" contest.