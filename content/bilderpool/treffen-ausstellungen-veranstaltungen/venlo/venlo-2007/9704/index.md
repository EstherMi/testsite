---
layout: "image"
title: "Kamera 2"
date: "2007-03-23T23:43:25"
picture: "162_6274.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Holger Howey"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/9704
imported:
- "2019"
_4images_image_id: "9704"
_4images_cat_id: "855"
_4images_user_id: "34"
_4images_image_date: "2007-03-23T23:43:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9704 -->
