---
layout: "image"
title: "Industrieroboter 3"
date: "2003-04-22T16:21:52"
picture: "Industrieroboter 3.jpg"
weight: "3"
konstrukteure: 
- "Ronald van Ewijk"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/289
imported:
- "2019"
_4images_image_id: "289"
_4images_cat_id: "39"
_4images_user_id: "1"
_4images_image_date: "2003-04-22T16:21:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=289 -->
