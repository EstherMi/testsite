---
layout: "image"
title: "Helikopter 2"
date: "2003-04-22T16:40:07"
picture: "Helikopter 2.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/330
imported:
- "2019"
_4images_image_id: "330"
_4images_cat_id: "48"
_4images_user_id: "1"
_4images_image_date: "2003-04-22T16:40:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=330 -->
