---
layout: "image"
title: "Grosses Rad 4"
date: "2003-04-22T16:40:07"
picture: "Grosses_Rad_4.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "NN"
keywords: ["Speichenrad"]
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/328
imported:
- "2019"
_4images_image_id: "328"
_4images_cat_id: "48"
_4images_user_id: "1"
_4images_image_date: "2003-04-22T16:40:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=328 -->
