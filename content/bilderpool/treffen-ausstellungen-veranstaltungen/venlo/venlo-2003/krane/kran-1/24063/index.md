---
layout: "image"
title: "PortalKrane (Marspau)"
date: "2009-05-21T22:29:47"
picture: "Krane_1_001.jpg"
weight: "1"
konstrukteure: 
- "Marspau"
fotografen:
- "Andree"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "marspau"
license: "unknown"
legacy_id:
- details/24063
imported:
- "2019"
_4images_image_id: "24063"
_4images_cat_id: "40"
_4images_user_id: "416"
_4images_image_date: "2009-05-21T22:29:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24063 -->
This is a robotized version of the old
Portalkran # 39219.
 I tried to  respect the most possible
the original FT design.

Dies ist eine robotized Version des alten
Portalkran # 39219.
Ich versuchte, die am meisten möglich respektieren
Das ursprüngliche FT-Design.