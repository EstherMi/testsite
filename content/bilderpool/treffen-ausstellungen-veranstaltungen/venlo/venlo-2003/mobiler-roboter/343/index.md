---
layout: "image"
title: "Mobiler Roboter 7"
date: "2003-04-22T16:40:07"
picture: "Mobiler Roboter 7.jpg"
weight: "7"
konstrukteure: 
- "Frank Linde (DerMitDenBitsTanzt)"
fotografen:
- "Frank Linde (DerMitDenBitsTanzt)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/343
imported:
- "2019"
_4images_image_id: "343"
_4images_cat_id: "43"
_4images_user_id: "1"
_4images_image_date: "2003-04-22T16:40:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=343 -->
