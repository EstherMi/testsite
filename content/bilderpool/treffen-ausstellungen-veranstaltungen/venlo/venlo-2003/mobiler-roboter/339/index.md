---
layout: "image"
title: "Mobiler Roboter 3"
date: "2003-04-22T16:40:07"
picture: "Mobiler Roboter 3.jpg"
weight: "3"
konstrukteure: 
- "Frank Linde (DerMitDenBitsTanzt)"
fotografen:
- "Frank Linde (DerMitDenBitsTanzt)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/339
imported:
- "2019"
_4images_image_id: "339"
_4images_cat_id: "43"
_4images_user_id: "1"
_4images_image_date: "2003-04-22T16:40:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=339 -->
