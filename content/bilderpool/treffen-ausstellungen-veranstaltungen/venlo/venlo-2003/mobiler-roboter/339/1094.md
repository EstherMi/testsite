---
layout: "comment"
hidden: true
title: "1094"
date: "2006-05-17T04:22:13"
uploadBy:
- "DerMitDenBitsTanzt"
license: "unknown"
imported:
- "2019"
---
Zwischen allzu vielen Besucherbeinen drehte sich der Roboter teilweise etwas hektisch; hier hat er aber gerade eine "Lichtung" entdeckt.