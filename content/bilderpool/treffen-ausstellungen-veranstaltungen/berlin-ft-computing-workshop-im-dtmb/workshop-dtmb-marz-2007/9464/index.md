---
layout: "image"
title: "berlin48.jpg"
date: "2007-03-13T20:02:07"
picture: "berlin48.jpg"
weight: "45"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/9464
imported:
- "2019"
_4images_image_id: "9464"
_4images_cat_id: "870"
_4images_user_id: "1"
_4images_image_date: "2007-03-13T20:02:07"
_4images_image_order: "48"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9464 -->
