---
layout: "image"
title: "Boise Bot Competion 2010"
date: "2010-10-10T12:29:55"
picture: "sm_bot6.jpg"
weight: "6"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Boise", "Bot", "Competion", "2010", "PCS", "BRAIN"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/28970
imported:
- "2019"
_4images_image_id: "28970"
_4images_cat_id: "2105"
_4images_user_id: "585"
_4images_image_date: "2010-10-10T12:29:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28970 -->
These are images from the Boise Bot Competion 2010. We took second place!