---
layout: "image"
title: "Antonio mit dem Löschwagen"
date: "2016-02-29T21:09:00"
picture: "xxx1.jpg"
weight: "10"
konstrukteure: 
- "Antonio"
fotografen:
- "giliprimero"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "giliprimero"
license: "unknown"
legacy_id:
- details/42954
imported:
- "2019"
_4images_image_id: "42954"
_4images_cat_id: "3193"
_4images_user_id: "2439"
_4images_image_date: "2016-02-29T21:09:00"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42954 -->
Ein Projekt der ft-AG der GSM