---
layout: "image"
title: "Ein Projekt der ft-AG der GSM"
date: "2016-04-08T21:38:56"
picture: "einprojektderftagdergsm02.jpg"
weight: "2"
konstrukteure: 
- "ft-AG"
fotografen:
- "giliprimero"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "giliprimero"
license: "unknown"
legacy_id:
- details/43265
imported:
- "2019"
_4images_image_id: "43265"
_4images_cat_id: "3214"
_4images_user_id: "2439"
_4images_image_date: "2016-04-08T21:38:56"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43265 -->
