---
layout: "image"
title: "Ein Projekt der ft-AG der GSM"
date: "2016-04-08T21:38:56"
picture: "einprojektderftagdergsm04.jpg"
weight: "4"
konstrukteure: 
- "Hanna"
fotografen:
- "giliprimero"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "giliprimero"
license: "unknown"
legacy_id:
- details/43267
imported:
- "2019"
_4images_image_id: "43267"
_4images_cat_id: "3214"
_4images_user_id: "2439"
_4images_image_date: "2016-04-08T21:38:56"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43267 -->
