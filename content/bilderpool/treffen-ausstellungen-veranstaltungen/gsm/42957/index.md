---
layout: "image"
title: "Thomas"
date: "2016-02-29T21:42:47"
picture: "xxx3.jpg"
weight: "13"
konstrukteure: 
- "Thomas"
fotografen:
- "giliprimero"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "giliprimero"
license: "unknown"
legacy_id:
- details/42957
imported:
- "2019"
_4images_image_id: "42957"
_4images_cat_id: "3193"
_4images_user_id: "2439"
_4images_image_date: "2016-02-29T21:42:47"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42957 -->
Thomas mit dem Rennwagen aus der GP-Serie