---
layout: "image"
title: "ft-AG3.jpg"
date: "2016-04-30T22:39:32"
picture: "x3.jpg"
weight: "3"
konstrukteure: 
- "giliprimero"
fotografen:
- "giliprimero"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "giliprimero"
license: "unknown"
legacy_id:
- details/43330
imported:
- "2019"
_4images_image_id: "43330"
_4images_cat_id: "3217"
_4images_user_id: "2439"
_4images_image_date: "2016-04-30T22:39:32"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43330 -->
