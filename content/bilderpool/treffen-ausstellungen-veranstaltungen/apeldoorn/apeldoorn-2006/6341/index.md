---
layout: "image"
title: "Apeldorn 53"
date: "2006-06-01T21:29:46"
picture: "Apeldoorn_053.jpg"
weight: "5"
konstrukteure: 
- "Dave Gabeler (Modell Pettera)"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- details/6341
imported:
- "2019"
_4images_image_id: "6341"
_4images_cat_id: "552"
_4images_user_id: "10"
_4images_image_date: "2006-06-01T21:29:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6341 -->
