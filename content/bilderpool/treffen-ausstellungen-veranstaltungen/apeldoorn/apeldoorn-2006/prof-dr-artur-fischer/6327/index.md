---
layout: "image"
title: "Apeldorn 39"
date: "2006-06-01T21:29:46"
picture: "Apeldoorn_039.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "Lothar Vogt      Pilami"
keywords: ["Artur", "Fischer"]
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- details/6327
imported:
- "2019"
_4images_image_id: "6327"
_4images_cat_id: "560"
_4images_user_id: "10"
_4images_image_date: "2006-06-01T21:29:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6327 -->
