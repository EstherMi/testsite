---
layout: "comment"
hidden: true
title: "8760"
date: "2009-03-11T16:51:11"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Mit dem heutigen Robo Interface funktioniert es schon sehr gut mit "Channel A" oder "Channel B" als analogen A1 Spannungs-eingang und ein Impulszähler.

Gruss,

Peter Damen
Poederoyen NL