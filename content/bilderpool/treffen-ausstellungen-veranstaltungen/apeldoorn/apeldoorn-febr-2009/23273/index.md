---
layout: "image"
title: "Rob van Baal & Zoon,  Eiffeltoren"
date: "2009-02-28T21:47:57"
picture: "2009-Febr-FT-Apeldoorn_040.jpg"
weight: "23"
konstrukteure: 
- "Rob van Baal & Zoon"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/23273
imported:
- "2019"
_4images_image_id: "23273"
_4images_cat_id: "1582"
_4images_user_id: "22"
_4images_image_date: "2009-02-28T21:47:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23273 -->
