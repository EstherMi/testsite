---
layout: "image"
title: "“Around the World” in Apeldoorn"
date: "2009-02-28T22:34:42"
picture: "FT-Schoonhoven-2008_033.jpg"
weight: "12"
konstrukteure: 
- "Clemens Jansen, “Around the World”"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/23290
imported:
- "2019"
_4images_image_id: "23290"
_4images_cat_id: "2012"
_4images_user_id: "22"
_4images_image_date: "2009-02-28T22:34:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23290 -->
