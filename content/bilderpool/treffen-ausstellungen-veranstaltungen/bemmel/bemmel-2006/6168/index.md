---
layout: "image"
title: "W. S."
date: "2006-04-29T11:14:33"
picture: "Bemmel_06_2.jpg"
weight: "2"
konstrukteure: 
- "w.s."
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- details/6168
imported:
- "2019"
_4images_image_id: "6168"
_4images_cat_id: "532"
_4images_user_id: "107"
_4images_image_date: "2006-04-29T11:14:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6168 -->
In der Ruhe liegt die Kraft...

Wim baut schon seit Jahren stetig und sehr erfolgreich an seinem Kran. Jetzt will er in die Höhe. Viel Glück...