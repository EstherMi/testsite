---
layout: "image"
title: "Kran 1"
date: "2006-05-07T15:16:33"
picture: "IMG_2517_1.jpg"
weight: "1"
konstrukteure: 
- "Anton Jansen - NL"
fotografen:
- "Rob  van Baal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "robvanbaal"
license: "unknown"
legacy_id:
- details/6232
imported:
- "2019"
_4images_image_id: "6232"
_4images_cat_id: "581"
_4images_user_id: "379"
_4images_image_date: "2006-05-07T15:16:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6232 -->
