---
layout: "image"
title: "Montage CTA Brücke"
date: "2006-04-30T11:47:10"
picture: "Bemmel_06_5.jpg"
weight: "6"
konstrukteure: 
- "jw"
fotografen:
- "jw"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jw"
license: "unknown"
legacy_id:
- details/6188
imported:
- "2019"
_4images_image_id: "6188"
_4images_cat_id: "580"
_4images_user_id: "107"
_4images_image_date: "2006-04-30T11:47:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6188 -->
