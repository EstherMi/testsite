---
layout: "image"
title: "Container Terminal 2"
date: "2006-05-07T15:16:33"
picture: "IMG_2525_1.jpg"
weight: "2"
konstrukteure: 
- "Jürgen Warwel"
fotografen:
- "Rob  van Baal"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "robvanbaal"
license: "unknown"
legacy_id:
- details/6219
imported:
- "2019"
_4images_image_id: "6219"
_4images_cat_id: "580"
_4images_user_id: "379"
_4images_image_date: "2006-05-07T15:16:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6219 -->
