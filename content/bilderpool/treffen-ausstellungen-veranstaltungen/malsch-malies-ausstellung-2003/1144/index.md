---
layout: "image"
title: "Meine Austellung 1"
date: "2003-05-30T22:56:03"
picture: "Bild_01.jpg"
weight: "1"
konstrukteure: 
- "Markus Liebenstein"
fotografen:
- "Markus Liebenstein"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MaLie"
license: "unknown"
legacy_id:
- details/1144
imported:
- "2019"
_4images_image_id: "1144"
_4images_cat_id: "121"
_4images_user_id: "26"
_4images_image_date: "2003-05-30T22:56:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1144 -->
Ich hatte mal vom 30. Sep. &#8211; 19. Okt. 2002 eine Austellung (natürlich mit ft) bei einem Immobilienmakler ( http://www.hirzmann.de , info@hirzmann.de ) in unserem Ort (69254 Malsch). Die meisten Modelle sind von Bauanleitung gebaut, wie man auch auf den Bildern erkennen kann. 
Es sind glaube ich, wenn ich richtig gezählt habe 18 Modelle. Hab&#8216; also schon einige Teile von fischertechnik. ;-)