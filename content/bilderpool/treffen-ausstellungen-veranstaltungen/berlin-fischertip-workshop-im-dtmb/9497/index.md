---
layout: "image"
title: "fischertip18.jpg"
date: "2007-03-13T20:09:00"
picture: "fischertip18.jpg"
weight: "18"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/9497
imported:
- "2019"
_4images_image_id: "9497"
_4images_cat_id: "871"
_4images_user_id: "1"
_4images_image_date: "2007-03-13T20:09:00"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9497 -->
