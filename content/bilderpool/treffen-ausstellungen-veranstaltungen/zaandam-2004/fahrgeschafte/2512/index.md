---
layout: "image"
title: "ZAANDAM_45"
date: "2004-06-06T17:55:50"
picture: "ZAANDAM_45.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/2512
imported:
- "2019"
_4images_image_id: "2512"
_4images_cat_id: "587"
_4images_user_id: "144"
_4images_image_date: "2004-06-06T17:55:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2512 -->
Antrieb der Seilwinden für der Freefall Tower von Stef Dijkstra.