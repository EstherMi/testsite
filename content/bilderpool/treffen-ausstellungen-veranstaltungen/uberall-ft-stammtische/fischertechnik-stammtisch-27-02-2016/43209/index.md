---
layout: "image"
title: "LED-Baugruppe"
date: "2016-03-26T18:02:05"
picture: "fischertechnikstammtischinkarlsruhe06.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/43209
imported:
- "2019"
_4images_image_id: "43209"
_4images_cat_id: "3209"
_4images_user_id: "104"
_4images_image_date: "2016-03-26T18:02:05"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43209 -->
Auch siehe ft:pedia 2016-1! Also wenn das so weitergeht... :-)