---
layout: "overview"
title: "fischertechnik-Stammtisch 27.02.2016  in Karlsruhe"
date: 2019-12-17T18:21:43+01:00
legacy_id:
- categories/3209
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3209 --> 
Wir trafen uns in Dirk Fox' Büroräumen, aßen gestifteten Kuchen und fachsimpelten :-)