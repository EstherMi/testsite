---
layout: "image"
title: "Konrad auf dem Chefsessel"
date: "2016-03-26T18:02:05"
picture: "fischertechnikstammtischinkarlsruhe02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/43205
imported:
- "2019"
_4images_image_id: "43205"
_4images_cat_id: "3209"
_4images_user_id: "104"
_4images_image_date: "2016-03-26T18:02:05"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43205 -->
