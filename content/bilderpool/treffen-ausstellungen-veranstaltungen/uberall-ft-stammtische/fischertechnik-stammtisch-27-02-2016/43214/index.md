---
layout: "image"
title: "Planimeter"
date: "2016-03-26T21:14:26"
picture: "fischertechnikstammtischinkarlsruhe11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/43214
imported:
- "2019"
_4images_image_id: "43214"
_4images_cat_id: "3209"
_4images_user_id: "104"
_4images_image_date: "2016-03-26T21:14:26"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43214 -->
Zur Beschreibung bitte auch in der ft:pedia nachlesen