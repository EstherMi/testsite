---
layout: "image"
title: "Consul, the Educated Monkey"
date: "2016-03-26T18:02:05"
picture: "fischertechnikstammtischinkarlsruhe09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/43212
imported:
- "2019"
_4images_image_id: "43212"
_4images_cat_id: "3209"
_4images_user_id: "104"
_4images_image_date: "2016-03-26T18:02:05"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43212 -->
Auch den gab es schoon in der ft:pedia ausführlich beschrieben.