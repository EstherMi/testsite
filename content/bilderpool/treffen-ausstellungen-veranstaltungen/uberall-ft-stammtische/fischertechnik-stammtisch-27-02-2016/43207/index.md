---
layout: "image"
title: "Hypozykloidgetriebe"
date: "2016-03-26T18:02:05"
picture: "fischertechnikstammtischinkarlsruhe04.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/43207
imported:
- "2019"
_4images_image_id: "43207"
_4images_cat_id: "3209"
_4images_user_id: "104"
_4images_image_date: "2016-03-26T18:02:05"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43207 -->
Eine Untersetzung ~ 400:1 im superkompakt-Format - siehe ft:pedia 2016-1