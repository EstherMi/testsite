---
layout: "image"
title: "stammtisch56.jpg"
date: "2015-11-07T22:18:55"
picture: "stammtisch56.jpg"
weight: "56"
konstrukteure: 
- "Triceratops"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/42353
imported:
- "2019"
_4images_image_id: "42353"
_4images_cat_id: "3151"
_4images_user_id: "1"
_4images_image_date: "2015-11-07T22:18:55"
_4images_image_order: "56"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42353 -->
