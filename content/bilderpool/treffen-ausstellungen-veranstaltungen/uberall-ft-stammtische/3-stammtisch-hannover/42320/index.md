---
layout: "image"
title: "stammtisch23.jpg"
date: "2015-11-07T22:18:55"
picture: "stammtisch23.jpg"
weight: "23"
konstrukteure: 
- "Ingwer Carstens"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/42320
imported:
- "2019"
_4images_image_id: "42320"
_4images_cat_id: "3151"
_4images_user_id: "1"
_4images_image_date: "2015-11-07T22:18:55"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42320 -->
