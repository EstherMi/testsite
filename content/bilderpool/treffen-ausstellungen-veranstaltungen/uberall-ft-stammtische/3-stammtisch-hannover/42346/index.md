---
layout: "image"
title: "stammtisch49.jpg"
date: "2015-11-07T22:18:55"
picture: "stammtisch49.jpg"
weight: "49"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/42346
imported:
- "2019"
_4images_image_id: "42346"
_4images_cat_id: "3151"
_4images_user_id: "1"
_4images_image_date: "2015-11-07T22:18:55"
_4images_image_order: "49"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42346 -->
