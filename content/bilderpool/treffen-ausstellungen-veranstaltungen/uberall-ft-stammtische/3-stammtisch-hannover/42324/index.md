---
layout: "image"
title: "stammtisch27.jpg"
date: "2015-11-07T22:18:55"
picture: "stammtisch27.jpg"
weight: "27"
konstrukteure: 
- "Ingwer Carstens"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/42324
imported:
- "2019"
_4images_image_id: "42324"
_4images_cat_id: "3151"
_4images_user_id: "1"
_4images_image_date: "2015-11-07T22:18:55"
_4images_image_order: "27"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42324 -->
