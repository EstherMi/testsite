---
layout: "image"
title: "stammtisch40.jpg"
date: "2015-11-07T22:18:55"
picture: "stammtisch40.jpg"
weight: "40"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/42337
imported:
- "2019"
_4images_image_id: "42337"
_4images_cat_id: "3151"
_4images_user_id: "1"
_4images_image_date: "2015-11-07T22:18:55"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42337 -->
