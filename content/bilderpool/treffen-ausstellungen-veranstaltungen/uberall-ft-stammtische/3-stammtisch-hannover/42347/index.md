---
layout: "image"
title: "stammtisch50.jpg"
date: "2015-11-07T22:18:55"
picture: "stammtisch50.jpg"
weight: "50"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/42347
imported:
- "2019"
_4images_image_id: "42347"
_4images_cat_id: "3151"
_4images_user_id: "1"
_4images_image_date: "2015-11-07T22:18:55"
_4images_image_order: "50"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42347 -->
