---
layout: "image"
title: "stammtisch55.jpg"
date: "2015-11-07T22:18:55"
picture: "stammtisch55.jpg"
weight: "55"
konstrukteure: 
- "Triceratops"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/42352
imported:
- "2019"
_4images_image_id: "42352"
_4images_cat_id: "3151"
_4images_user_id: "1"
_4images_image_date: "2015-11-07T22:18:55"
_4images_image_order: "55"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42352 -->
