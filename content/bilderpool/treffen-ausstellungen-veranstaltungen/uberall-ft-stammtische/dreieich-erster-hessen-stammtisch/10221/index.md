---
layout: "image"
title: "Schnaggels und Thkais"
date: "2007-04-29T19:59:11"
picture: "hessen4.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "Masked (Martin)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/10221
imported:
- "2019"
_4images_image_id: "10221"
_4images_cat_id: "925"
_4images_user_id: "373"
_4images_image_date: "2007-04-29T19:59:11"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10221 -->
Das Foto spiegelt übrigens nicht die Stimmung während des Treffens wieder, ich hab nur im falschen Moment abgedrückt...