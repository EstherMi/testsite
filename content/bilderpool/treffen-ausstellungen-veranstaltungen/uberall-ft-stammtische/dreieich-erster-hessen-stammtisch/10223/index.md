---
layout: "image"
title: "Handydisplay"
date: "2007-04-29T22:38:25"
picture: "dsc01101_resize.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Thomas Brestrich (schnaggels)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- details/10223
imported:
- "2019"
_4images_image_id: "10223"
_4images_cat_id: "925"
_4images_user_id: "120"
_4images_image_date: "2007-04-29T22:38:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10223 -->
Genauere Details zum Typ folgen, hier schon mal kurz: Gibt es wohl unter 10€ und die Anschlüsse sind sogar gut lötbar! (Kein SMD Connector...) Ein ATMEL mit etwas mehr Speicher darf es zur Ansteuerung ruhig sein!