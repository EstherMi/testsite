---
layout: "image"
title: "Wie geht das nur?"
date: "2008-10-28T18:08:56"
picture: "DSC01803A.jpg"
weight: "4"
konstrukteure: 
- "/"
fotografen:
- "Volker-James Muenchhof"
keywords: ["Stammtisch"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- details/16094
imported:
- "2019"
_4images_image_id: "16094"
_4images_cat_id: "1459"
_4images_user_id: "41"
_4images_image_date: "2008-10-28T18:08:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16094 -->
Natürlich wurden auch Modelle begutachtet. Hier ein Industrie-Förderband aus dem Fundus von Schnaggels.