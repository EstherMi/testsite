---
layout: "image"
title: "Essen fassen..."
date: "2008-10-28T18:08:56"
picture: "DSC01800A.jpg"
weight: "2"
konstrukteure: 
- "/"
fotografen:
- "Volker-James Muenchhof"
keywords: ["Stammtisch"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- details/16092
imported:
- "2019"
_4images_image_id: "16092"
_4images_cat_id: "1459"
_4images_user_id: "41"
_4images_image_date: "2008-10-28T18:08:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16092 -->
Erst einmal stärken, damit die Ideen fließen...