---
layout: "image"
title: "Bitte lächeln"
date: "2008-10-28T18:08:56"
picture: "DSC01802A.jpg"
weight: "3"
konstrukteure: 
- "/"
fotografen:
- "Michaels Frauchen"
keywords: ["Stammtisch"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- details/16093
imported:
- "2019"
_4images_image_id: "16093"
_4images_cat_id: "1459"
_4images_user_id: "41"
_4images_image_date: "2008-10-28T18:08:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16093 -->
Diesmal sehen die Bilder wenigstens so aus, als ob wir Spaß gehabt hätten...
Natürlich hatten wir den auch. Ungläubiges Staunen über die Unmengen an ft, die in manchen Wohnungen lagern... ;-)