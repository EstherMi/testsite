---
layout: "image"
title: "Stammtisch_HH2"
date: "2015-01-17T22:00:11"
picture: "stammtischhamburg2.jpg"
weight: "2"
konstrukteure: 
- "Martin W. (Masked)"
fotografen:
- "Martin W. (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/40379
imported:
- "2019"
_4images_image_id: "40379"
_4images_cat_id: "3026"
_4images_user_id: "373"
_4images_image_date: "2015-01-17T22:00:11"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40379 -->
...und der andere Teil, der uns eben noch den Hinterkopf zudrehte. Sorry für die Bildqualität, der Blitz fehlte...