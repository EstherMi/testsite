---
layout: "image"
title: "Strickmaschine"
date: "2015-01-17T22:00:11"
picture: "stammtischhamburg3.jpg"
weight: "3"
konstrukteure: 
- "Martin W. (Masked)"
fotografen:
- "Martin W. (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/40380
imported:
- "2019"
_4images_image_id: "40380"
_4images_cat_id: "3026"
_4images_user_id: "373"
_4images_image_date: "2015-01-17T22:00:11"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40380 -->
Ralf Geerken hat eine Strickmaschine nach dem Vorbild von der Convention 2014 gebaut. Unterschied ist die automatische, mechanische mitlaufende Spannung des fertigen Strickseils. Ein paar Kinder vom Nachbartisch hatten wir damit am Anfang direkt "geerbt", den Eltern war das irgendwie peinlich...