---
layout: "comment"
hidden: true
title: "20077"
date: "2015-01-19T20:30:30"
uploadBy:
- "DasKasperle"
license: "unknown"
imported:
- "2019"
---
Schmunzel.

Wenn ich ehrlich bin, ist es mir auch immer wieder peinlich, wenn meine Kinder (bei anderen) neugierig sind. Komisch, denn eigentlich weiss ich doch als Pädagoge, dass es nichts besseres gibt, als die intrinsische kraft der Neugier.

Und ich hoffe ich werde die Strickmaschine auch mal "live" erleben um meine Neugier zu befriedigen. :)