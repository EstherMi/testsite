---
layout: "image"
title: "fischertechnik Stammtisch Wiesbaden - Die Hütte"
date: "2012-08-04T18:07:54"
picture: "DSC01100.jpg"
weight: "16"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "Christian Knobloch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "C-Knobloch"
license: "unknown"
legacy_id:
- details/35257
imported:
- "2019"
_4images_image_id: "35257"
_4images_cat_id: "2612"
_4images_user_id: "997"
_4images_image_date: "2012-08-04T18:07:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35257 -->
