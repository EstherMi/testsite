---
layout: "image"
title: "Flechtmaschine"
date: "2015-05-13T09:39:55"
picture: "ftstammtischhannover14.jpg"
weight: "14"
konstrukteure: 
- "ThanksForTheFish"
fotografen:
- "Rolf B"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rolf B"
license: "unknown"
legacy_id:
- details/40980
imported:
- "2019"
_4images_image_id: "40980"
_4images_cat_id: "3077"
_4images_user_id: "1419"
_4images_image_date: "2015-05-13T09:39:55"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40980 -->
Grundprinzip für eine Flechtmaschine.