---
layout: "image"
title: "Lanz Eilbulldog"
date: "2015-05-13T09:39:55"
picture: "ftstammtischhannover03.jpg"
weight: "3"
konstrukteure: 
- "Claus Ludwig"
fotografen:
- "Rolf B"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rolf B"
license: "unknown"
legacy_id:
- details/40969
imported:
- "2019"
_4images_image_id: "40969"
_4images_cat_id: "3077"
_4images_user_id: "1419"
_4images_image_date: "2015-05-13T09:39:55"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40969 -->
Hat mich sehr gefreut dieses Modell malin natura erleben zu dürfen.