---
layout: "image"
title: "LKW mit Aufieger"
date: "2015-05-13T09:39:55"
picture: "ftstammtischhannover10.jpg"
weight: "10"
konstrukteure: 
- "Ingwer"
fotografen:
- "Rolf B"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rolf B"
license: "unknown"
legacy_id:
- details/40976
imported:
- "2019"
_4images_image_id: "40976"
_4images_cat_id: "3077"
_4images_user_id: "1419"
_4images_image_date: "2015-05-13T09:39:55"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40976 -->
Noch ein Appetitmacher von Ingwer.