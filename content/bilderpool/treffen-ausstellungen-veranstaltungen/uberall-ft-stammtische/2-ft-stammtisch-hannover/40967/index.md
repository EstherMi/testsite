---
layout: "image"
title: "Jeder Stammtisch braucht ein Schild"
date: "2015-05-13T09:39:55"
picture: "ftstammtischhannover01.jpg"
weight: "1"
konstrukteure: 
- "Ingwer"
fotografen:
- "Rolf B"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rolf B"
license: "unknown"
legacy_id:
- details/40967
imported:
- "2019"
_4images_image_id: "40967"
_4images_cat_id: "3077"
_4images_user_id: "1419"
_4images_image_date: "2015-05-13T09:39:55"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40967 -->
Ingwers gesammelte Winkelsteine.
Wer dabei war weiß was ich meine ;)