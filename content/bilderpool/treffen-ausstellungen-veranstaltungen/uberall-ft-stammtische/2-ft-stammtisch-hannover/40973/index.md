---
layout: "image"
title: "Zugmaschine"
date: "2015-05-13T09:39:55"
picture: "ftstammtischhannover07.jpg"
weight: "7"
konstrukteure: 
- "Claus Ludwig"
fotografen:
- "Rolf B"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rolf B"
license: "unknown"
legacy_id:
- details/40973
imported:
- "2019"
_4images_image_id: "40973"
_4images_cat_id: "3077"
_4images_user_id: "1419"
_4images_image_date: "2015-05-13T09:39:55"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40973 -->
Der Radantrieb an die IR-Fernsteueung angeschlossen funktionierte wirklich gut.