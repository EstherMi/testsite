---
layout: "comment"
hidden: true
title: "20663"
date: "2015-05-18T21:28:18"
uploadBy:
- "Claus"
license: "unknown"
imported:
- "2019"
---
Hallo Peter,

ja es funktoniert gut im 1. und im 2. Gang. Der 3. Gang geht wegen dem Überlastungsschutz nicht. Für einen LKW ist mir der 3. Gang auch viel zu schnell, so dass ich ihn nicht nutze.

Gruß Claus