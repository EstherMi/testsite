---
layout: "image"
title: "fischertechnik Fahrturm Details"
date: "2017-11-06T16:08:24"
picture: "stammtisch10.jpg"
weight: "10"
konstrukteure: 
- "Herbert"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/46896
imported:
- "2019"
_4images_image_id: "46896"
_4images_cat_id: "3470"
_4images_user_id: "2303"
_4images_image_date: "2017-11-06T16:08:24"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46896 -->
