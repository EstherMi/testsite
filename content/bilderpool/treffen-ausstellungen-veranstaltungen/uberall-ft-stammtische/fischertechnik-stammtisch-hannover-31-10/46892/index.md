---
layout: "image"
title: "7-Segmentanzeige aus Fahrstuhl"
date: "2017-11-06T16:08:24"
picture: "stammtisch06.jpg"
weight: "6"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/46892
imported:
- "2019"
_4images_image_id: "46892"
_4images_cat_id: "3470"
_4images_user_id: "2303"
_4images_image_date: "2017-11-06T16:08:24"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46892 -->
