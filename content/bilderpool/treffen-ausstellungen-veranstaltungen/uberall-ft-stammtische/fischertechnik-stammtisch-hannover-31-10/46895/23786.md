---
layout: "comment"
hidden: true
title: "23786"
date: "2017-11-06T22:23:27"
uploadBy:
- "ThanksForTheFish"
license: "unknown"
imported:
- "2019"
---
Der Fahrturm von Herbert Hirt hat eindrucksvoll dargestellt wie potenzielle Energie wieder über einen Generator (sprich Motor) in elektrische Energie umgewandelt wird.