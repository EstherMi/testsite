---
layout: "image"
title: "Fachgesimpel"
date: "2017-11-06T16:08:24"
picture: "stammtisch03.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/46889
imported:
- "2019"
_4images_image_id: "46889"
_4images_cat_id: "3470"
_4images_user_id: "2303"
_4images_image_date: "2017-11-06T16:08:24"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46889 -->
