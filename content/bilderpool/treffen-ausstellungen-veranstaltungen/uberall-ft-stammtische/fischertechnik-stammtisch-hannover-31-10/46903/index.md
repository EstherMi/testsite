---
layout: "image"
title: "fischertechnik Blume"
date: "2017-11-06T16:08:42"
picture: "stammtisch17.jpg"
weight: "17"
konstrukteure: 
- "Stammtischler"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/46903
imported:
- "2019"
_4images_image_id: "46903"
_4images_cat_id: "3470"
_4images_user_id: "2303"
_4images_image_date: "2017-11-06T16:08:42"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46903 -->
