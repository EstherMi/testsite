---
layout: "image"
title: "stammtisch02.jpg"
date: "2016-04-03T09:57:32"
picture: "stammtisch02.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/43229
imported:
- "2019"
_4images_image_id: "43229"
_4images_cat_id: "3211"
_4images_user_id: "1"
_4images_image_date: "2016-04-03T09:57:32"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43229 -->
