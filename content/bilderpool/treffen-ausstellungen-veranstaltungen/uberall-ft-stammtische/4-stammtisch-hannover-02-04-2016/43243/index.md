---
layout: "image"
title: "stammtisch16.jpg"
date: "2016-04-03T09:57:32"
picture: "stammtisch16.jpg"
weight: "16"
konstrukteure: 
- "Triceratops"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/43243
imported:
- "2019"
_4images_image_id: "43243"
_4images_cat_id: "3211"
_4images_user_id: "1"
_4images_image_date: "2016-04-03T09:57:32"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43243 -->
