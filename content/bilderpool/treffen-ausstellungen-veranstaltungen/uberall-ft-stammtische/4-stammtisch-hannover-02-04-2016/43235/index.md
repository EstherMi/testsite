---
layout: "image"
title: "stammtisch08.jpg"
date: "2016-04-03T09:57:32"
picture: "stammtisch08.jpg"
weight: "8"
konstrukteure: 
- "ThansForTheFish"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/43235
imported:
- "2019"
_4images_image_id: "43235"
_4images_cat_id: "3211"
_4images_user_id: "1"
_4images_image_date: "2016-04-03T09:57:32"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43235 -->
