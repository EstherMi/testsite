---
layout: "image"
title: "Jan Niklas"
date: "2016-04-05T11:46:23"
picture: "stammtisch2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "ThanksForTheFish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- details/43254
imported:
- "2019"
_4images_image_id: "43254"
_4images_cat_id: "3213"
_4images_user_id: "381"
_4images_image_date: "2016-04-05T11:46:23"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43254 -->
Er hatte Vater, Mutter, Opa, Oma, eine wirklich tolle Stimmung und obendrein noch ein wirklich hervorragendes Modell mitgebracht.