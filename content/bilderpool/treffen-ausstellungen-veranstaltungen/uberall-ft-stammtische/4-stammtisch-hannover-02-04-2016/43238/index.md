---
layout: "image"
title: "stammtisch11.jpg"
date: "2016-04-03T09:57:32"
picture: "stammtisch11.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/43238
imported:
- "2019"
_4images_image_id: "43238"
_4images_cat_id: "3211"
_4images_user_id: "1"
_4images_image_date: "2016-04-03T09:57:32"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43238 -->
