---
layout: "comment"
hidden: true
title: "21903"
date: "2016-04-03T14:28:23"
uploadBy:
- "ThanksForTheFish"
license: "unknown"
imported:
- "2019"
---
Hier ist eine Rennstrecke für ein physikalisches Experiment zu sehen.
2 unterschiedlich schwere, aber ansonsten gleiche Rennwagen fahren ein Schräge herunter - welcher zuerst unten ankommt hat gewonnen.
Die Zuschauer dürfen vorher schätzen welcher von beiden das wohl sein wird.
Da man nicht sehen kann, welcher Wagen zuerst unten ankommt, habe ich mir dafür eine andere menschliche Wahrnehmung ausgesucht. Und zwar das Hörvermögen. Die Wagen sind seitlich mit einer Metallachse ausgestattet, die bei Berührung mit den ft-Klangröhren einen Ton erzeugt. Jeweils einen unterschiedlich hohen Ton für die Rennwagen.
Angetrieben werden die Wagen nur durch die Schwerkraft und werden über einen Auslösemechanismus von einem davorstehenden (vorzugsweise) Kind gleichzeitig gestartet. Das Gehör kann dann drei Fälle deutlich unterscheiden:
1. Fall: Der schwerere Wagen ist zuerst unten. Der helle Ton erklingt zuerst.
2. Fall: Der leichtere Wagen ist zuerst unten. Der dunkle Ton erklingt zuerst.
3. Fall: Beide Wagen kommen gleichzeitig unten an. Ein gemischter Ton erklingt.

Na was schätzt ihr was passieren wird?