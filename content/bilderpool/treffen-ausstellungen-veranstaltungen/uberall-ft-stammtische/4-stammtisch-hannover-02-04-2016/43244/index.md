---
layout: "image"
title: "stammtisch17.jpg"
date: "2016-04-03T09:57:32"
picture: "stammtisch17.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/43244
imported:
- "2019"
_4images_image_id: "43244"
_4images_cat_id: "3211"
_4images_user_id: "1"
_4images_image_date: "2016-04-03T09:57:32"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43244 -->
