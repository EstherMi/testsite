---
layout: "image"
title: "Katapult"
date: "2008-11-21T17:42:29"
picture: "ft65.jpg"
weight: "6"
konstrukteure: 
- "Dirk"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/16457
imported:
- "2019"
_4images_image_id: "16457"
_4images_cat_id: "1481"
_4images_user_id: "453"
_4images_image_date: "2008-11-21T17:42:29"
_4images_image_order: "65"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16457 -->
