---
layout: "image"
title: "kessel16103"
date: "2008-11-23T13:55:29"
picture: "Amelsb16103.JPG"
weight: "1"
konstrukteure: 
- "Dirk Kutsch"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/16477
imported:
- "2019"
_4images_image_id: "16477"
_4images_cat_id: "1481"
_4images_user_id: "4"
_4images_image_date: "2008-11-23T13:55:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16477 -->
Die Kesselbrücke, mit jeder Menge gelenkter Achsen, Soundmodul, Teleskopauszug für variable Breite und absenkbarem Kasten.