---
layout: "image"
title: "Roboter"
date: "2008-11-21T17:41:14"
picture: "ft01.jpg"
weight: "1"
konstrukteure: 
- "MisterWho"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/16393
imported:
- "2019"
_4images_image_id: "16393"
_4images_cat_id: "1479"
_4images_user_id: "453"
_4images_image_date: "2008-11-21T17:41:14"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16393 -->
