---
layout: "image"
title: "Amelsb16122.JPG"
date: "2008-11-23T14:10:59"
picture: "Amelsb16122.JPG"
weight: "3"
konstrukteure: 
- "Ralf Unruh"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/16485
imported:
- "2019"
_4images_image_id: "16485"
_4images_cat_id: "1489"
_4images_user_id: "4"
_4images_image_date: "2008-11-23T14:10:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16485 -->
Zwei Vakuumbüchsen mit Gummimembran, man könnte sagen: "umgekehrt wirksame P-Betätiger".