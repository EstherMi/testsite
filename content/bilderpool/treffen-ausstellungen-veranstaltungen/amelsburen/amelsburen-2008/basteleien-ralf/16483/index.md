---
layout: "image"
title: "Amelsb16251.JPG"
date: "2008-11-23T14:07:00"
picture: "Amelsb16251.JPG"
weight: "1"
konstrukteure: 
- "Ralf Unruh"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/16483
imported:
- "2019"
_4images_image_id: "16483"
_4images_cat_id: "1489"
_4images_user_id: "4"
_4images_image_date: "2008-11-23T14:07:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16483 -->
Prima Leuchtdioden-Adaption an ft. Die Wüfel im Hintergrund sind Vierfach-LEDs.