---
layout: "image"
title: "LKW"
date: "2008-11-21T17:42:29"
picture: "ft54.jpg"
weight: "4"
konstrukteure: 
- "Brickwedde"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/16446
imported:
- "2019"
_4images_image_id: "16446"
_4images_cat_id: "1487"
_4images_user_id: "453"
_4images_image_date: "2008-11-21T17:42:29"
_4images_image_order: "54"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16446 -->
