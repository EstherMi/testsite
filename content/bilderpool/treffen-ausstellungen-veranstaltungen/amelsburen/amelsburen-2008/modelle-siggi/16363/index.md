---
layout: "image"
title: "Siggi ist wieder am frickeln."
date: "2008-11-21T16:54:19"
picture: "ftausstellungamelsbueren03.jpg"
weight: "2"
konstrukteure: 
- "Siggi"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/16363
imported:
- "2019"
_4images_image_id: "16363"
_4images_cat_id: "1487"
_4images_user_id: "130"
_4images_image_date: "2008-11-21T16:54:19"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16363 -->
