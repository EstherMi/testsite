---
layout: "image"
title: "ftamel08_0071"
date: "2008-11-17T21:09:19"
picture: "amel21.jpg"
weight: "5"
konstrukteure: 
- "FT, Nachbau durch frickelsiggi"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/16322
imported:
- "2019"
_4images_image_id: "16322"
_4images_cat_id: "1487"
_4images_user_id: "504"
_4images_image_date: "2008-11-17T21:09:19"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16322 -->
