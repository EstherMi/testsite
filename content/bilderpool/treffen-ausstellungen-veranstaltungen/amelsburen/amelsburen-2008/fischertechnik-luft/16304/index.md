---
layout: "image"
title: "interessiert"
date: "2008-11-17T21:08:46"
picture: "amel03.jpg"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/16304
imported:
- "2019"
_4images_image_id: "16304"
_4images_cat_id: "1480"
_4images_user_id: "504"
_4images_image_date: "2008-11-17T21:08:46"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16304 -->
