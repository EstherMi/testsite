---
layout: "image"
title: "Spielecke"
date: "2008-11-17T21:09:02"
picture: "amel15.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/16316
imported:
- "2019"
_4images_image_id: "16316"
_4images_cat_id: "1480"
_4images_user_id: "504"
_4images_image_date: "2008-11-17T21:09:02"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16316 -->
