---
layout: "image"
title: "Amelsb16211.JPG"
date: "2008-11-23T13:59:14"
picture: "Amelsb16211.JPG"
weight: "1"
konstrukteure: 
- "n/a"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/16480
imported:
- "2019"
_4images_image_id: "16480"
_4images_cat_id: "1480"
_4images_user_id: "4"
_4images_image_date: "2008-11-23T13:59:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16480 -->
Die Fachwelt staunt.