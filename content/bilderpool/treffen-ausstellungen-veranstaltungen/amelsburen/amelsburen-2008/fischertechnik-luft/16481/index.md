---
layout: "image"
title: "Amelsb16216.JPG"
date: "2008-11-23T14:00:42"
picture: "Amelsb16216.JPG"
weight: "2"
konstrukteure: 
- "n/a"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/16481
imported:
- "2019"
_4images_image_id: "16481"
_4images_cat_id: "1480"
_4images_user_id: "4"
_4images_image_date: "2008-11-23T14:00:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16481 -->
Der Give-Away-Automat war der unangefochtene Publikumsmagnet Nr. 1.