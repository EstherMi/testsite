---
layout: "image"
title: "Zwei Konstrukteure unter sich"
date: "2008-11-21T16:54:31"
picture: "ftausstellungamelsbueren13.jpg"
weight: "4"
konstrukteure: 
- "Gulligan (Dirk Kutsch)"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/16373
imported:
- "2019"
_4images_image_id: "16373"
_4images_cat_id: "1480"
_4images_user_id: "130"
_4images_image_date: "2008-11-21T16:54:31"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16373 -->
