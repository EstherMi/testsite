---
layout: "image"
title: "Roboter"
date: "2008-11-21T17:41:15"
picture: "ft02.jpg"
weight: "2"
konstrukteure: 
- "MisterWho"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/16394
imported:
- "2019"
_4images_image_id: "16394"
_4images_cat_id: "1479"
_4images_user_id: "453"
_4images_image_date: "2008-11-21T17:41:15"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16394 -->
