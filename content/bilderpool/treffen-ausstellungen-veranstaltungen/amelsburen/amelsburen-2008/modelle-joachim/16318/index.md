---
layout: "image"
title: "ftamel08_0053"
date: "2008-11-17T21:09:02"
picture: "amel17.jpg"
weight: "7"
konstrukteure: 
- "Joachim Jacobi"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/16318
imported:
- "2019"
_4images_image_id: "16318"
_4images_cat_id: "1486"
_4images_user_id: "504"
_4images_image_date: "2008-11-17T21:09:02"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16318 -->
