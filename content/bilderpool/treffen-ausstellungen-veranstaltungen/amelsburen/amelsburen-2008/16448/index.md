---
layout: "image"
title: "TST s Modelle"
date: "2008-11-21T17:42:29"
picture: "ft56.jpg"
weight: "5"
konstrukteure: 
- "TST"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/16448
imported:
- "2019"
_4images_image_id: "16448"
_4images_cat_id: "1479"
_4images_user_id: "453"
_4images_image_date: "2008-11-21T17:42:29"
_4images_image_order: "56"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16448 -->
