---
layout: "image"
title: "Blick auf den Aufzug im Pfeiler"
date: "2008-11-21T16:54:31"
picture: "ftausstellungamelsbueren19.jpg"
weight: "6"
konstrukteure: 
- "Rob van Baal"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/16379
imported:
- "2019"
_4images_image_id: "16379"
_4images_cat_id: "1478"
_4images_user_id: "130"
_4images_image_date: "2008-11-21T16:54:31"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16379 -->
Schön konstruiert, fast wie in echt.