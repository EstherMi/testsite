---
layout: "image"
title: "Industriemodell"
date: "2008-11-21T17:41:36"
picture: "ft20.jpg"
weight: "1"
konstrukteure: 
- "Olli"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/16412
imported:
- "2019"
_4images_image_id: "16412"
_4images_cat_id: "1485"
_4images_user_id: "453"
_4images_image_date: "2008-11-21T17:41:36"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16412 -->
