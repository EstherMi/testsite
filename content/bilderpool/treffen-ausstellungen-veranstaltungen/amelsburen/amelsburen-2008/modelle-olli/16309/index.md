---
layout: "image"
title: "joystick"
date: "2008-11-17T21:08:46"
picture: "amel08.jpg"
weight: "3"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/16309
imported:
- "2019"
_4images_image_id: "16309"
_4images_cat_id: "1485"
_4images_user_id: "504"
_4images_image_date: "2008-11-17T21:08:46"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16309 -->
Damit konnte man den Roboter steuern.