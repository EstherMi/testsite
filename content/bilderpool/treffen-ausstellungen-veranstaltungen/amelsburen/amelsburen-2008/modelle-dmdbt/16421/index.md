---
layout: "image"
title: "Roboter"
date: "2008-11-21T17:42:29"
picture: "ft29.jpg"
weight: "4"
konstrukteure: 
- "dmdbt"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/16421
imported:
- "2019"
_4images_image_id: "16421"
_4images_cat_id: "1484"
_4images_user_id: "453"
_4images_image_date: "2008-11-21T17:42:29"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16421 -->
