---
layout: "image"
title: "Feuerwehrmann"
date: "2008-11-21T17:42:29"
picture: "ft61.jpg"
weight: "8"
konstrukteure: 
- "TST"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/16453
imported:
- "2019"
_4images_image_id: "16453"
_4images_cat_id: "1474"
_4images_user_id: "453"
_4images_image_date: "2008-11-21T17:42:29"
_4images_image_order: "61"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16453 -->
