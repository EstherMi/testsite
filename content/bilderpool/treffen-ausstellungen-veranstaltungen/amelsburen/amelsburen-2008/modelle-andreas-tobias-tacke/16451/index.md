---
layout: "image"
title: "Nutzfahrzeug"
date: "2008-11-21T17:42:29"
picture: "ft59.jpg"
weight: "6"
konstrukteure: 
- "TST"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/16451
imported:
- "2019"
_4images_image_id: "16451"
_4images_cat_id: "1474"
_4images_user_id: "453"
_4images_image_date: "2008-11-21T17:42:29"
_4images_image_order: "59"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16451 -->
