---
layout: "image"
title: "Kirmes-Modell"
date: "2008-11-21T17:41:15"
picture: "ft10.jpg"
weight: "10"
konstrukteure: 
- "Fam. Lammering"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/16402
imported:
- "2019"
_4images_image_id: "16402"
_4images_cat_id: "1490"
_4images_user_id: "453"
_4images_image_date: "2008-11-21T17:41:15"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16402 -->
