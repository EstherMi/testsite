---
layout: "image"
title: "Amelsb15120.JPG"
date: "2008-11-23T14:25:14"
picture: "Amelsb15120.JPG"
weight: "4"
konstrukteure: 
- "Lammering"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/16493
imported:
- "2019"
_4images_image_id: "16493"
_4images_cat_id: "1490"
_4images_user_id: "4"
_4images_image_date: "2008-11-23T14:25:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16493 -->
