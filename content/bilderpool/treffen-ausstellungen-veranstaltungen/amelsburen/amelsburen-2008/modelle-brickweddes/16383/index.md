---
layout: "image"
title: "Eine Gondel.."
date: "2008-11-21T16:54:44"
picture: "ftausstellungamelsbueren23.jpg"
weight: "3"
konstrukteure: 
- "Fam. Brickwedde"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/16383
imported:
- "2019"
_4images_image_id: "16383"
_4images_cat_id: "1476"
_4images_user_id: "130"
_4images_image_date: "2008-11-21T16:54:44"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16383 -->
vom grossen Riesenrad. Sogar Sitze sind vorhanden. Schön gebaut, gefällt mir.