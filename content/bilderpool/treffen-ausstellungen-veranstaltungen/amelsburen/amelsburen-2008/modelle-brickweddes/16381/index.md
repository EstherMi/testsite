---
layout: "image"
title: "Riesenrad en gros"
date: "2008-11-21T16:54:44"
picture: "ftausstellungamelsbueren21.jpg"
weight: "1"
konstrukteure: 
- "Fam. Brickwedde"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/16381
imported:
- "2019"
_4images_image_id: "16381"
_4images_cat_id: "1476"
_4images_user_id: "130"
_4images_image_date: "2008-11-21T16:54:44"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16381 -->
Sehr schönes Teil von einem Riesenrad