---
layout: "image"
title: "Riesenrad"
date: "2008-11-21T17:42:29"
picture: "ft41.jpg"
weight: "9"
konstrukteure: 
- "Brickwedde"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/16433
imported:
- "2019"
_4images_image_id: "16433"
_4images_cat_id: "1476"
_4images_user_id: "453"
_4images_image_date: "2008-11-21T17:42:29"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16433 -->
