---
layout: "image"
title: "Das hintere Ende von Ludgers Roadtrain"
date: "2008-11-21T16:54:19"
picture: "ftausstellungamelsbueren07.jpg"
weight: "4"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/16367
imported:
- "2019"
_4images_image_id: "16367"
_4images_cat_id: "1473"
_4images_user_id: "130"
_4images_image_date: "2008-11-21T16:54:19"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16367 -->
Den bekam ich nicht im ganzen auf Bild weil er so lang war.