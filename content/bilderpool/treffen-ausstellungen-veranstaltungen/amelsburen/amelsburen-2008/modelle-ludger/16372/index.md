---
layout: "image"
title: "Des Klempners wichtigtes Utensil.."
date: "2008-11-21T16:54:31"
picture: "ftausstellungamelsbueren12.jpg"
weight: "10"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/16372
imported:
- "2019"
_4images_image_id: "16372"
_4images_cat_id: "1473"
_4images_user_id: "130"
_4images_image_date: "2008-11-21T16:54:31"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16372 -->
...der Schweissbrenner. Hier sieht man die Gasflaschen.