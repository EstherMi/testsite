---
layout: "image"
title: "Bulldozer-Spiel"
date: "2008-11-21T17:41:36"
picture: "ft12.jpg"
weight: "11"
konstrukteure: 
- "Ludger"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/16404
imported:
- "2019"
_4images_image_id: "16404"
_4images_cat_id: "1473"
_4images_user_id: "453"
_4images_image_date: "2008-11-21T17:41:36"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16404 -->
