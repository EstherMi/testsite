---
layout: "image"
title: "Industrea Dozer von Ludger"
date: "2008-11-21T16:54:19"
picture: "ftausstellungamelsbueren05.jpg"
weight: "2"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/16365
imported:
- "2019"
_4images_image_id: "16365"
_4images_cat_id: "1473"
_4images_user_id: "130"
_4images_image_date: "2008-11-21T16:54:19"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16365 -->
Nochmal der Dozer von Ludger!