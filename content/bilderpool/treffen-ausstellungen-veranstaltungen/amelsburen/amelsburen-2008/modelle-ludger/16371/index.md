---
layout: "image"
title: "...und eins und zwei..."
date: "2008-11-21T16:54:31"
picture: "ftausstellungamelsbueren11.jpg"
weight: "8"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/16371
imported:
- "2019"
_4images_image_id: "16371"
_4images_cat_id: "1473"
_4images_user_id: "130"
_4images_image_date: "2008-11-21T16:54:31"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16371 -->
schönes kleines Ruderboot.