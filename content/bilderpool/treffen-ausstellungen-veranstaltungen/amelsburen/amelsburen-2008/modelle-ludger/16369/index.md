---
layout: "image"
title: "Ludgers Roadtrain"
date: "2008-11-21T16:54:19"
picture: "ftausstellungamelsbueren09.jpg"
weight: "6"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/16369
imported:
- "2019"
_4images_image_id: "16369"
_4images_cat_id: "1473"
_4images_user_id: "130"
_4images_image_date: "2008-11-21T16:54:19"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16369 -->
