---
layout: "image"
title: "Roboter"
date: "2008-11-17T21:08:46"
picture: "amel07.jpg"
weight: "3"
konstrukteure: 
- "Frederik"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/16308
imported:
- "2019"
_4images_image_id: "16308"
_4images_cat_id: "1482"
_4images_user_id: "504"
_4images_image_date: "2008-11-17T21:08:46"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16308 -->
Fredys Roboter