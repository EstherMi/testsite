---
layout: "image"
title: "Brücke"
date: "2008-11-21T17:42:29"
picture: "ft62.jpg"
weight: "1"
konstrukteure: 
- "Stephan"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/16454
imported:
- "2019"
_4images_image_id: "16454"
_4images_cat_id: "1483"
_4images_user_id: "453"
_4images_image_date: "2008-11-21T17:42:29"
_4images_image_order: "62"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16454 -->
