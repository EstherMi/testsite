---
layout: "image"
title: "ftamel08_0064"
date: "2008-11-17T21:09:19"
picture: "amel20.jpg"
weight: "3"
konstrukteure: 
- "Stephan"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/16321
imported:
- "2019"
_4images_image_id: "16321"
_4images_cat_id: "1483"
_4images_user_id: "504"
_4images_image_date: "2008-11-17T21:09:19"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16321 -->
