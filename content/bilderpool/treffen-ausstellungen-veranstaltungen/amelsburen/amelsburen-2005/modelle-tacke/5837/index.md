---
layout: "image"
title: "Gesamtansicht"
date: "2006-03-06T09:00:25"
picture: "Tacke1.jpg"
weight: "1"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Andreas Tacke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/5837
imported:
- "2019"
_4images_image_id: "5837"
_4images_cat_id: "478"
_4images_user_id: "182"
_4images_image_date: "2006-03-06T09:00:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5837 -->
