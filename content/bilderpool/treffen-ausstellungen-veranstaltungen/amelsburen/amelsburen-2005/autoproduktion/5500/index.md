---
layout: "image"
title: "Amelsbüren 4"
date: "2005-12-17T13:44:47"
picture: "Amelsbren_04.jpg"
weight: "1"
konstrukteure: 
- "Manfred Busch"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- details/5500
imported:
- "2019"
_4images_image_id: "5500"
_4images_cat_id: "476"
_4images_user_id: "10"
_4images_image_date: "2005-12-17T13:44:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5500 -->
