---
layout: "image"
title: "Schneidern der Karosserie"
date: "2005-12-16T16:02:07"
picture: "Bild1972.jpg"
weight: "6"
konstrukteure: 
- "Manfred Busch"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- details/5483
imported:
- "2019"
_4images_image_id: "5483"
_4images_cat_id: "476"
_4images_user_id: "109"
_4images_image_date: "2005-12-16T16:02:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5483 -->
