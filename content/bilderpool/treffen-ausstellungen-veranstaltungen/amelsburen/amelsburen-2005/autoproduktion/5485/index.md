---
layout: "image"
title: "Montage der Räder"
date: "2005-12-16T16:02:07"
picture: "Bild1974.jpg"
weight: "8"
konstrukteure: 
- "Manfred Busch"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- details/5485
imported:
- "2019"
_4images_image_id: "5485"
_4images_cat_id: "476"
_4images_user_id: "109"
_4images_image_date: "2005-12-16T16:02:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5485 -->
