---
layout: "image"
title: "Entfernen der Radkästen"
date: "2005-12-16T16:02:07"
picture: "Bild1973.jpg"
weight: "7"
konstrukteure: 
- "Manfred Busch"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- details/5484
imported:
- "2019"
_4images_image_id: "5484"
_4images_cat_id: "476"
_4images_user_id: "109"
_4images_image_date: "2005-12-16T16:02:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5484 -->
