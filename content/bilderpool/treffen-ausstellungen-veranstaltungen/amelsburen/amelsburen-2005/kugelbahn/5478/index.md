---
layout: "image"
title: "Kugelbahn mit Transport der Kugel 2"
date: "2005-12-16T16:01:58"
picture: "Bild1993.jpg"
weight: "2"
konstrukteure: 
- "unbekannt"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- details/5478
imported:
- "2019"
_4images_image_id: "5478"
_4images_cat_id: "474"
_4images_user_id: "109"
_4images_image_date: "2005-12-16T16:01:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5478 -->
