---
layout: "image"
title: "Überblick hintere Reihe"
date: "2005-12-16T16:01:46"
picture: "Bild1986.jpg"
weight: "11"
konstrukteure: 
- "unbekannt"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- details/5467
imported:
- "2019"
_4images_image_id: "5467"
_4images_cat_id: "472"
_4images_user_id: "109"
_4images_image_date: "2005-12-16T16:01:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5467 -->
