---
layout: "image"
title: "Die Raupe schiebt die Räder zusammen"
date: "2005-12-16T16:02:07"
picture: "Bild1977.jpg"
weight: "3"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- details/5488
imported:
- "2019"
_4images_image_id: "5488"
_4images_cat_id: "477"
_4images_user_id: "109"
_4images_image_date: "2005-12-16T16:02:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5488 -->
