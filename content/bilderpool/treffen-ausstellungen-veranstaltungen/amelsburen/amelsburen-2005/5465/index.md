---
layout: "image"
title: "Roboter mit beweglichem Kopf"
date: "2005-12-16T16:01:46"
picture: "Bild1978.jpg"
weight: "9"
konstrukteure: 
- "unbekannt"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- details/5465
imported:
- "2019"
_4images_image_id: "5465"
_4images_cat_id: "472"
_4images_user_id: "109"
_4images_image_date: "2005-12-16T16:01:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5465 -->
