---
layout: "image"
title: "Industrieanlage"
date: "2005-12-16T16:01:58"
picture: "Bild1964.jpg"
weight: "7"
konstrukteure: 
- "Brickwedde"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- details/5472
imported:
- "2019"
_4images_image_id: "5472"
_4images_cat_id: "473"
_4images_user_id: "109"
_4images_image_date: "2005-12-16T16:01:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5472 -->
