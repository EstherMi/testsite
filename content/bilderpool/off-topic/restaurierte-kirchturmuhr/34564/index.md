---
layout: "image"
title: "Ansicht von links"
date: "2012-03-05T07:01:56"
picture: "restauriertekirchturmuhr07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/34564
imported:
- "2019"
_4images_image_id: "34564"
_4images_cat_id: "2553"
_4images_user_id: "104"
_4images_image_date: "2012-03-05T07:01:56"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34564 -->
Wir blicken von der linken Seite übers Viertelstundenschlagwerk auf die Uhr.