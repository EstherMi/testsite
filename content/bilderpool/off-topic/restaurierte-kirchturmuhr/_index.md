---
layout: "overview"
title: "Restaurierte Kirchturmuhr"
date: 2019-12-17T18:10:34+01:00
legacy_id:
- categories/2553
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2553 --> 
Bilder einer Kirchturmuhr, die mittlerweile in einem Treppenhaus gut zu erkunden ist.