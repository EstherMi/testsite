---
layout: "image"
title: "Stundenschlagwerk"
date: "2012-03-05T07:01:56"
picture: "restauriertekirchturmuhr05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/34562
imported:
- "2019"
_4images_image_id: "34562"
_4images_cat_id: "2553"
_4images_user_id: "104"
_4images_image_date: "2012-03-05T07:01:56"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34562 -->
Dieses Werk war dafür zuständig, immer zur vollen Stunde zu läuten.