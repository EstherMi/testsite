---
layout: "image"
title: "ft bra with model"
date: "2009-10-18T18:29:48"
picture: "bra_model.jpg"
weight: "24"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["2009", "bra", "project"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/25561
imported:
- "2019"
_4images_image_id: "25561"
_4images_cat_id: "312"
_4images_user_id: "585"
_4images_image_date: "2009-10-18T18:29:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25561 -->
This is Joe modelling my entry for the 2009 Bra Project.