---
layout: "image"
title: "Ein Quad als Schneemobil 4/5"
date: "2011-02-20T21:26:44"
picture: "a4.jpg"
weight: "4"
konstrukteure: 
- "can-am"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/30099
imported:
- "2019"
_4images_image_id: "30099"
_4images_cat_id: "2224"
_4images_user_id: "389"
_4images_image_date: "2011-02-20T21:26:44"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30099 -->
Das Quad von can-am ist mit einem Apache Track System ausgerüstet