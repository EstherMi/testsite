---
layout: "image"
title: "Cyclograph   Einstellungen für drehen vom Zeichentisch."
date: "2008-05-03T15:20:29"
picture: "meccanozeichenmaschine02.jpg"
weight: "4"
konstrukteure: 
- "J Weststrate   (meccano gilde nederland)"
fotografen:
- "pvd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pvd"
license: "unknown"
legacy_id:
- details/14435
imported:
- "2019"
_4images_image_id: "14435"
_4images_cat_id: "1333"
_4images_user_id: "7"
_4images_image_date: "2008-05-03T15:20:29"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14435 -->
Diese Einstellungen hier   befinden sich  gegenüber  die Einstellungen  der X Richtung vom zentralen Saul .  (der Saul besteht aus 4 Achsen , die ein Vierkant bilden ,  mit - geschatzt  -  etwas 3 Zentimeter als Seite-abmessung.