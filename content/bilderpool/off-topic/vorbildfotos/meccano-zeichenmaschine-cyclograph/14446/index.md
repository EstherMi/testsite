---
layout: "image"
title: "Cyclograph  Gesammtansicht (unten)"
date: "2008-05-03T17:47:45"
picture: "meccanozeichenmaschine1_2.jpg"
weight: "3"
konstrukteure: 
- "J Weststrate   (meccano gilde nederland) pvd"
fotografen:
- "pvd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pvd"
license: "unknown"
legacy_id:
- details/14446
imported:
- "2019"
_4images_image_id: "14446"
_4images_cat_id: "1333"
_4images_user_id: "7"
_4images_image_date: "2008-05-03T17:47:45"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14446 -->
Hier sieht man fast die ganze Maschine (etwa 70 bis 80zm hoch ) Der drehende Tisch worauf das Zeichenpapier geklemmt ist ,  ist hier ganz oben rechts noch etwas zu sehen .  Alles lauft über ein Motor  ,  der sich ganz unten befindet