---
layout: "image"
title: "Pantograph ,  der eigentlich schreibende Teil .(3)"
date: "2008-05-03T17:47:45"
picture: "meccanozeichenmaschine4.jpg"
weight: "10"
konstrukteure: 
- "J Weststrate   (meccano gilde nederland) pvd"
fotografen:
- "pvd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pvd"
license: "unknown"
legacy_id:
- details/14449
imported:
- "2019"
_4images_image_id: "14449"
_4images_cat_id: "1333"
_4images_user_id: "7"
_4images_image_date: "2008-05-03T17:47:45"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14449 -->
Das Ding das man hier ganz oben im Bild  sieht , ist einfach eine Lampe  die an der Maschine montiert is .