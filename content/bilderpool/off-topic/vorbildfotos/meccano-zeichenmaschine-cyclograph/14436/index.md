---
layout: "image"
title: "Cyclograph    Mechanik im Untenteil"
date: "2008-05-03T15:20:29"
picture: "meccanozeichenmaschine03.jpg"
weight: "7"
konstrukteure: 
- "J Weststrate   (meccano gilde nederland)"
fotografen:
- "pvd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pvd"
license: "unknown"
legacy_id:
- details/14436
imported:
- "2019"
_4images_image_id: "14436"
_4images_cat_id: "1333"
_4images_user_id: "7"
_4images_image_date: "2008-05-03T15:20:29"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14436 -->
noch mal ein Sicht  auf die Mechanike  unten .  Man bemerkt  die Geschindigkeitsregelung vom Motor   (Mitte vom Foto  hier) . Vorne im FOto  steht eine andere Meccano-Maschine ,  die aber nichts mit diesem Zeichengerät zu tun hat .