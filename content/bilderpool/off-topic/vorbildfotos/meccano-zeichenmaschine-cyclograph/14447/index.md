---
layout: "image"
title: "Pantograph ,  der eigentlich schreibende Teil (1)"
date: "2008-05-03T17:47:45"
picture: "meccanozeichenmaschine2.jpg"
weight: "6"
konstrukteure: 
- "J Weststrate   (meccano gilde nederland) pvd"
fotografen:
- "pvd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pvd"
license: "unknown"
legacy_id:
- details/14447
imported:
- "2019"
_4images_image_id: "14447"
_4images_cat_id: "1333"
_4images_user_id: "7"
_4images_image_date: "2008-05-03T17:47:45"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14447 -->
befindet sich ganz oben in dieser Maschine .  Es wird mit einem sehr feinen Parker Kugelschreiber gezeichnet .