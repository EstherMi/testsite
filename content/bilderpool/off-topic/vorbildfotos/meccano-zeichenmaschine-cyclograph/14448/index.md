---
layout: "image"
title: "Pantograph ,  der eigentlich schreibende Teil (2)"
date: "2008-05-03T17:47:45"
picture: "meccanozeichenmaschine3.jpg"
weight: "8"
konstrukteure: 
- "J Weststrate   (meccano gilde nederland) pvd"
fotografen:
- "pvd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pvd"
license: "unknown"
legacy_id:
- details/14448
imported:
- "2019"
_4images_image_id: "14448"
_4images_cat_id: "1333"
_4images_user_id: "7"
_4images_image_date: "2008-05-03T17:47:45"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14448 -->
Noch ein Sicht von ganz oben