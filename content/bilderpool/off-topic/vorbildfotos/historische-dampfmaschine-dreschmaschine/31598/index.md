---
layout: "image"
title: "Detail"
date: "2011-08-19T17:11:45"
picture: "historischedampfmaschinemitdreschmaschine09.jpg"
weight: "9"
konstrukteure: 
- "Historisch"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/31598
imported:
- "2019"
_4images_image_id: "31598"
_4images_cat_id: "2354"
_4images_user_id: "104"
_4images_image_date: "2011-08-19T17:11:45"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31598 -->
Ein Blick in die Maschine vom der Dampfmaschine abgewandten Ende aus. Vielleicht kann ein Landmaschinenkundiger etwas dazu schreiben, was das genau jeweils ist. Der Holzträger scheint mir jedenfalls nicht absichtlich gebrochen zu sein.