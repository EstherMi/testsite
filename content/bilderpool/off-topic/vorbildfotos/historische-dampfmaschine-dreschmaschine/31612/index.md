---
layout: "image"
title: "Beschreibungen (1)"
date: "2011-08-19T17:11:45"
picture: "historischedampfmaschinemitdreschmaschine23.jpg"
weight: "23"
konstrukteure: 
- "Historisch"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/31612
imported:
- "2019"
_4images_image_id: "31612"
_4images_cat_id: "2354"
_4images_user_id: "104"
_4images_image_date: "2011-08-19T17:11:45"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31612 -->
