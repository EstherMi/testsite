---
layout: "image"
title: "Detail"
date: "2011-08-19T17:11:45"
picture: "historischedampfmaschinemitdreschmaschine14.jpg"
weight: "14"
konstrukteure: 
- "Historisch"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/31603
imported:
- "2019"
_4images_image_id: "31603"
_4images_cat_id: "2354"
_4images_user_id: "104"
_4images_image_date: "2011-08-19T17:11:45"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31603 -->
Ein Blick in die rechteckige Öffnung unten etwas rechts von der Mitte des vorherigen Bildes.