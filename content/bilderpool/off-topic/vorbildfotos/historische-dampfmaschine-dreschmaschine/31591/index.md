---
layout: "image"
title: "Pleuel"
date: "2011-08-19T17:11:45"
picture: "historischedampfmaschinemitdreschmaschine02.jpg"
weight: "2"
konstrukteure: 
- "Historisch"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/31591
imported:
- "2019"
_4images_image_id: "31591"
_4images_cat_id: "2354"
_4images_user_id: "104"
_4images_image_date: "2011-08-19T17:11:45"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31591 -->
Führung des Pleuels