---
layout: "overview"
title: "Historische Dampfmaschine mit Dreschmaschine"
date: 2019-12-17T18:10:30+01:00
legacy_id:
- categories/2354
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2354 --> 
Im Urlaub waren wir im Heimatdorfmuseum in Tittling im bayerischen Wald. Da ist ein komplettes historisches Dorf aufgebaut, und in einer Scheune standen ein Prachtexemplar einer Dampfmaschine, die über einen Riemenantrieb eine mobile Dreschmaschine - einen Vorläufer des Mähdreschers - antrieb. Vielleicht mag's ja jemand nachbauen.