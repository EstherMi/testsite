---
layout: "image"
title: "Schwungrad"
date: "2011-08-19T17:11:45"
picture: "historischedampfmaschinemitdreschmaschine04.jpg"
weight: "4"
konstrukteure: 
- "Historisch"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/31593
imported:
- "2019"
_4images_image_id: "31593"
_4images_cat_id: "2354"
_4images_user_id: "104"
_4images_image_date: "2011-08-19T17:11:45"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31593 -->
Interessant finde ich die Stange, die von der Schwungradachse nach links unten verläuft - ob das eine Bremse ist?