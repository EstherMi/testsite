---
layout: "image"
title: "Dreschmaschine"
date: "2011-08-19T17:11:45"
picture: "historischedampfmaschinemitdreschmaschine08.jpg"
weight: "8"
konstrukteure: 
- "Historisch"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/31597
imported:
- "2019"
_4images_image_id: "31597"
_4images_cat_id: "2354"
_4images_user_id: "104"
_4images_image_date: "2011-08-19T17:11:45"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31597 -->
Der Riemen kommt von der Dampfmaschine, die weiter rechts steht.