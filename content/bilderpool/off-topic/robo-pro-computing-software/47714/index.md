---
layout: "image"
title: "ftc-Auftritt"
date: "2018-06-24T16:46:34"
picture: "Unsere_Controller.jpg"
weight: "1"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- details/47714
imported:
- "2019"
_4images_image_id: "47714"
_4images_cat_id: "3521"
_4images_user_id: "2635"
_4images_image_date: "2018-06-24T16:46:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47714 -->
Den Auftritt der ftcommunity finde ich nach wie vor sehr ansprechend und angenehm sachlich. Dennoch ist eine Weiterentwicklung sinnvoll.
Mit Blick auf das Forum stellt man sofort fest, wie die Interessen der Besucher gelagert sind:
Die Beiträge zum Thema &#8222;Robo Pro / Computing / Software&#8220; überwiegen deutlich!
Ich meine, das sollte auch die Startseite der ftcommunity widerspiegeln.
Vorschlag:
- Im Bilderpool die neue Kategorie Robo Pro / Computing / Software einführen.
- In dieser Kategorie werden wie bisher Bilder hochgeladen. Der Text zu den jeweiligen Beiträgen wird so beschränkt, dass er zusammen mit dem Bild auf der Startseite veröffentlicht werden kann.
- Diese Beiträge können auch gerne von fischertechnik direkt kommen.
Zusätzlich zu den 6 Bildern aus dem Bilderpool wird dann rechts neben der neuesten ft:pedia ein Bild zusammen mit dem Text veröffentlicht, ebenfalls per Zufall ausgewählt.

Statt &#8222;Unsere Controller&#8220; könnte man natürlich auch Robo Pro / Computing / Software als Titel wählen.

Was meint ihr?