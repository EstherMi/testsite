---
layout: "image"
title: "Preisausschreiben - Gewinn 1"
date: "2011-09-27T19:44:27"
picture: "helena1.jpg"
weight: "1"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- details/32815
imported:
- "2019"
_4images_image_id: "32815"
_4images_cat_id: "2379"
_4images_user_id: "936"
_4images_image_date: "2011-09-27T19:44:27"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32815 -->
Hier die versprochenen Fotos. Wir haben den Gewinn heute per Post erhalten und knapp 3 Stunden später stand das Modell #3. Wie zu erwarten ist Helena sehr interessiert.