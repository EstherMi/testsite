---
layout: "image"
title: "Nachwuchs 5"
date: "2011-09-23T20:11:38"
picture: "nachwuchs5.jpg"
weight: "9"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- details/31913
imported:
- "2019"
_4images_image_id: "31913"
_4images_cat_id: "2379"
_4images_user_id: "936"
_4images_image_date: "2011-09-23T20:11:38"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31913 -->
Das ist ganz schön interessant...