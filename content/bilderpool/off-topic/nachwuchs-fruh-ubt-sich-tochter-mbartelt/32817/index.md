---
layout: "image"
title: "Preisausschreiben - Gewinn 3"
date: "2011-09-27T19:44:27"
picture: "helena3.jpg"
weight: "3"
konstrukteure: 
- "Manfred Bartelt"
fotografen:
- "Manfred Bartelt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mbartelt"
license: "unknown"
legacy_id:
- details/32817
imported:
- "2019"
_4images_image_id: "32817"
_4images_cat_id: "2379"
_4images_user_id: "936"
_4images_image_date: "2011-09-27T19:44:27"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32817 -->
Oh, und da oben passiert ja auch etwas.