---
layout: "image"
title: "Engraved Block"
date: "2010-02-13T15:23:27"
picture: "ftbrick_2.jpg"
weight: "20"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Engraved", "Building", "Block", "30"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/26339
imported:
- "2019"
_4images_image_id: "26339"
_4images_cat_id: "312"
_4images_user_id: "585"
_4images_image_date: "2010-02-13T15:23:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26339 -->
This is my Yule Gift from Laura. We are thinking about offering engraved blocks at Bricks-by-the-Bay.