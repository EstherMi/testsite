---
layout: "image"
title: "Juggler"
date: "2008-06-10T16:51:11"
picture: "ft_juggler_2.jpg"
weight: "32"
konstrukteure: 
- "Laura"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Laura", "Juggler"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/14670
imported:
- "2019"
_4images_image_id: "14670"
_4images_cat_id: "312"
_4images_user_id: "585"
_4images_image_date: "2008-06-10T16:51:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14670 -->
Laura built this. She claims it is an artistic skiing juggler. 

***google translation: Laura gebaut. Sie behauptet, es ist eine künstlerische Ski-Jongleur.