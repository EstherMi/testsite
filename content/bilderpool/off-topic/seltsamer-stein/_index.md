---
layout: "overview"
title: "Seltsamer Stein"
date: 2019-12-17T18:09:36+01:00
legacy_id:
- categories/829
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=829 --> 
Das ist ein Stein, den ich und mein Freund im Bach gefunden haben. Wir haben schon überall nachgeguckt, aber nix über die Herkunft herausgefunden. Wisst ihr was?