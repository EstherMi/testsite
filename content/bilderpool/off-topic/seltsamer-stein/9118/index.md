---
layout: "image"
title: "Seltsamer Stein"
date: "2007-02-20T19:54:06"
picture: "SeltsamerStein4.jpg"
weight: "4"
konstrukteure: 
- "natur"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/9118
imported:
- "2019"
_4images_image_id: "9118"
_4images_cat_id: "829"
_4images_user_id: "456"
_4images_image_date: "2007-02-20T19:54:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9118 -->
