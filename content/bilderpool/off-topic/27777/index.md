---
layout: "image"
title: "ft Scorpion on Peek"
date: "2010-07-25T09:08:23"
picture: "CIMG1286.jpg"
weight: "15"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Instructable", "Peek", "Scorpion"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/27777
imported:
- "2019"
_4images_image_id: "27777"
_4images_cat_id: "312"
_4images_user_id: "585"
_4images_image_date: "2010-07-25T09:08:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27777 -->
I created plans for the Scorpion, and converted them into a PDF format. Then I emailed them to my Peek, and I am now viewing the building instructions on the tiny Peek screen! Yowza!