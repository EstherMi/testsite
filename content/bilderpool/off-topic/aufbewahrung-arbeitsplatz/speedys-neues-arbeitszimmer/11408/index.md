---
layout: "image"
title: "Ft-Zimmer  2  (leer)"
date: "2007-08-27T17:21:13"
picture: "DSC02986.jpg"
weight: "7"
konstrukteure: 
- "speedy68"
fotografen:
- "speedy68"
keywords: ["Aufbewahrung", "Arbeitsplatz", "Fischertechnik", "Zimmer"]
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/11408
imported:
- "2019"
_4images_image_id: "11408"
_4images_cat_id: "1024"
_4images_user_id: "409"
_4images_image_date: "2007-08-27T17:21:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11408 -->
mein neues Ft-Zimmer im leeren Zustand