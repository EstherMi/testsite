---
layout: "image"
title: "Ft-Zimmer"
date: "2007-08-29T19:19:06"
picture: "DSC03100.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "speedy68"
keywords: ["Aufbewahrung", "Fischertechnik", "Zimmer"]
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/11410
imported:
- "2019"
_4images_image_id: "11410"
_4images_cat_id: "1024"
_4images_user_id: "409"
_4images_image_date: "2007-08-29T19:19:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11410 -->
