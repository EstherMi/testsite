---
layout: "image"
title: "Ft-Zimmer  3  (leer)"
date: "2007-08-27T17:21:13"
picture: "DSC02990.jpg"
weight: "8"
konstrukteure: 
- "speedy68"
fotografen:
- "speedy68"
keywords: ["Aufbewahrung", "Arbeitsplatz", "Fischertechnik", "Zimmer"]
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/11409
imported:
- "2019"
_4images_image_id: "11409"
_4images_cat_id: "1024"
_4images_user_id: "409"
_4images_image_date: "2007-08-27T17:21:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11409 -->
mein neues Ft-Zimmer im leeren Zustand
- Zimmergröße: 4,5 x 2,15 Meter