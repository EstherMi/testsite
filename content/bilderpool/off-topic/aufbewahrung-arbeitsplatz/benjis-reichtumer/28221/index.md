---
layout: "image"
title: "Schublade 2"
date: "2010-09-25T12:43:26"
picture: "Vastgelegd_2008-2-2_00007.jpg"
weight: "15"
konstrukteure: 
- "Ben Halverkamps"
fotografen:
- "Ben Halverkamps"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Benji"
license: "unknown"
legacy_id:
- details/28221
imported:
- "2019"
_4images_image_id: "28221"
_4images_cat_id: "2047"
_4images_user_id: "764"
_4images_image_date: "2010-09-25T12:43:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28221 -->
