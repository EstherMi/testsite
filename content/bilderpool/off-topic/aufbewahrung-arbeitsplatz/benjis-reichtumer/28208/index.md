---
layout: "image"
title: "Mini-Motoren"
date: "2010-09-25T12:15:35"
picture: "100_0148A.jpg"
weight: "2"
konstrukteure: 
- "Ben Halverkamps"
fotografen:
- "Ben Halverkamps"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Benji"
license: "unknown"
legacy_id:
- details/28208
imported:
- "2019"
_4images_image_id: "28208"
_4images_cat_id: "2047"
_4images_user_id: "764"
_4images_image_date: "2010-09-25T12:15:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28208 -->
Verschiedene Mini-Motoren