---
layout: "image"
title: "Bau und Statica Platten"
date: "2010-09-25T13:34:25"
picture: "Vastgelegd_2008-2-2_00016.jpg"
weight: "24"
konstrukteure: 
- "Ben Halverkamps"
fotografen:
- "Ben Halverkamps"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Benji"
license: "unknown"
legacy_id:
- details/28230
imported:
- "2019"
_4images_image_id: "28230"
_4images_cat_id: "2047"
_4images_user_id: "764"
_4images_image_date: "2010-09-25T13:34:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28230 -->
