---
layout: "image"
title: "Schublade1"
date: "2010-09-25T12:43:26"
picture: "Vastgelegd_2008-2-2_00006.jpg"
weight: "14"
konstrukteure: 
- "Ben Halverkamps"
fotografen:
- "Ben Halverkamps"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Benji"
license: "unknown"
legacy_id:
- details/28220
imported:
- "2019"
_4images_image_id: "28220"
_4images_cat_id: "2047"
_4images_user_id: "764"
_4images_image_date: "2010-09-25T12:43:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28220 -->
Die Schalter, sensoren und Magneten sind aus diese Schublade rausgeholt.