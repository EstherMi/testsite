---
layout: "image"
title: "Die Gesamtansicht des Radladers mit der Kompressoreinheit"
date: "2010-04-11T23:16:19"
picture: "neuebildervonmeinenmodellen09.jpg"
weight: "9"
konstrukteure: 
- "Dieter Meckel"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/26931
imported:
- "2019"
_4images_image_id: "26931"
_4images_cat_id: "1931"
_4images_user_id: "374"
_4images_image_date: "2010-04-11T23:16:19"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26931 -->
Eigene Variante des Modells, die von der offiziellen Bauanleitung abweicht