---
layout: "image"
title: "Das alte Modell von dem Radlader, dass schon auf der FT-Convention 2009 zu sehen war."
date: "2010-04-11T23:16:18"
picture: "neuebildervonmeinenmodellen03.jpg"
weight: "3"
konstrukteure: 
- "Dieter Meckel"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/26925
imported:
- "2019"
_4images_image_id: "26925"
_4images_cat_id: "1931"
_4images_user_id: "374"
_4images_image_date: "2010-04-11T23:16:18"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26925 -->
