---
layout: "image"
title: "Der Schaufelradlader aus dem Kasten PFROF-PNEUMATIC II"
date: "2010-04-11T23:16:18"
picture: "neuebildervonmeinenmodellen07.jpg"
weight: "7"
konstrukteure: 
- "Dieter Meckel"
fotografen:
- "Dieter Meckel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Dinomania01"
license: "unknown"
legacy_id:
- details/26929
imported:
- "2019"
_4images_image_id: "26929"
_4images_cat_id: "1931"
_4images_user_id: "374"
_4images_image_date: "2010-04-11T23:16:18"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26929 -->
Das fertige Modell