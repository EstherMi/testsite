---
layout: "image"
title: "Lagerplatz."
date: "2014-10-10T16:26:16"
picture: "bauundlagerplatzvonyellofish02.jpg"
weight: "2"
konstrukteure: 
- "Yell-O-Fish"
fotografen:
- "Yell-O-Fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Yell-O-Fish"
license: "unknown"
legacy_id:
- details/39681
imported:
- "2019"
_4images_image_id: "39681"
_4images_cat_id: "2975"
_4images_user_id: "1883"
_4images_image_date: "2014-10-10T16:26:16"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39681 -->
Das Baumaterial.