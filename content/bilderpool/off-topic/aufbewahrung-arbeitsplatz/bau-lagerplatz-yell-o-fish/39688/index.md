---
layout: "image"
title: "6. Elektronik, Optik, Schalter, Licht"
date: "2014-10-10T16:26:16"
picture: "bauundlagerplatzvonyellofish09.jpg"
weight: "9"
konstrukteure: 
- "Yell-O-Fish"
fotografen:
- "Yell-O-Fish"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Yell-O-Fish"
license: "unknown"
legacy_id:
- details/39688
imported:
- "2019"
_4images_image_id: "39688"
_4images_cat_id: "2975"
_4images_user_id: "1883"
_4images_image_date: "2014-10-10T16:26:16"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39688 -->
Schublade 6