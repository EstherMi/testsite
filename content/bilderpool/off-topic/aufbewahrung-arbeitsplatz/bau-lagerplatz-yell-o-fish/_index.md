---
layout: "overview"
title: "Bau- und Lagerplatz von Yell-O-Fish"
date: 2019-12-17T18:09:30+01:00
legacy_id:
- categories/2975
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2975 --> 
Inspiriert durch die Bilder von Markus Wolf (Vielen Dank auch an dieser Stelle für die tolle Idee und die super Umsetzung!) habe ich unsere Bauecke im Keller durch einen selbstgebauten Schubladenschrank ergänzt.
Der Schrank hat Maße von ca. 104 x 60 x 93 cm (BxTxH). Die Schubladen sind in der Höhe jeweils drei Mal 4,5 / 6,5 und 10,5 Zentimeter hoch. In die oberen drei passen genau 20 Sortierwannen hinen. In die mittleren drei ebenfalls, da sind nach oben aber noch zwei Zentimeter zusätzlicher Platz, falls mal etwas überstehen sollte. Und in die unteren drei Schubladen passen jeweils vier Boxen 1000 plus zusätzliche Dinge, die etwas Höhe benötigen. Bisher ist nur mittelgrob sortiert, die Feinarbeit folgt in den nächsten Wochen.
In den anderen Boxen befinden sich die Statik-Träger sowie Bauanleitungen, überschüssige Ersatzteile und weitere, unbenutzte Baukästen.