---
layout: "image"
title: "Platten"
date: "2006-12-29T15:44:44"
picture: "stefansreich6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Lehnerer"
fotografen:
- "Stefan Lehnerer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "StefanL"
license: "unknown"
legacy_id:
- details/8170
imported:
- "2019"
_4images_image_id: "8170"
_4images_cat_id: "754"
_4images_user_id: "502"
_4images_image_date: "2006-12-29T15:44:44"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8170 -->
