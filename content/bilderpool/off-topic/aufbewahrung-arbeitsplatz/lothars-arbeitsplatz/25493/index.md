---
layout: "image"
title: "Arbeitsplatz 1"
date: "2009-10-06T18:48:32"
picture: "AP1.jpg"
weight: "6"
konstrukteure: 
- "Lothar Vogt      Pilami"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- details/25493
imported:
- "2019"
_4images_image_id: "25493"
_4images_cat_id: "1784"
_4images_user_id: "10"
_4images_image_date: "2009-10-06T18:48:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25493 -->
Mein bewährter Planschrank ist nun rot geworden. Die Kleinmagazine stehen nun nebeneinander in griffbereiter Höhe.
Für die Boxen werde ich mir noch ein Regal bauen. An der Beleuchtung muss ich noch etwas feilen - aber das kommt noch