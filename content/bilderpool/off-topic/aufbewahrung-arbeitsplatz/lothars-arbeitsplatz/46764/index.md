---
layout: "image"
title: "Kleinteile 2"
date: "2017-10-11T20:54:40"
picture: "KleinSort2_klein.jpg"
weight: "5"
konstrukteure: 
- "Lothar Vogt      Pilami"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- details/46764
imported:
- "2019"
_4images_image_id: "46764"
_4images_cat_id: "1784"
_4images_user_id: "10"
_4images_image_date: "2017-10-11T20:54:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46764 -->
