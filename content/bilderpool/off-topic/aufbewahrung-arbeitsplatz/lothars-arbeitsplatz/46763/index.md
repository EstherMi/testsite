---
layout: "image"
title: "Diverse Boxen"
date: "2017-10-11T16:59:09"
picture: "Box1_klein.jpg"
weight: "4"
konstrukteure: 
- "Lothar Vogt      Pilami"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- details/46763
imported:
- "2019"
_4images_image_id: "46763"
_4images_cat_id: "1784"
_4images_user_id: "10"
_4images_image_date: "2017-10-11T16:59:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46763 -->
Alle Boxen sind voll bestückt