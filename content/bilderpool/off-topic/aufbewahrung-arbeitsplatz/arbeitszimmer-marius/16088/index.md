---
layout: "image"
title: "Mein Arbeitsplatz"
date: "2008-10-27T19:55:18"
picture: "meinarbeitsplatz1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Marius"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marius"
license: "unknown"
legacy_id:
- details/16088
imported:
- "2019"
_4images_image_id: "16088"
_4images_cat_id: "1458"
_4images_user_id: "845"
_4images_image_date: "2008-10-27T19:55:18"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16088 -->
Das ist mein Arbeitsplatz