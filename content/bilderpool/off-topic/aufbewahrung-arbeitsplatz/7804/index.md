---
layout: "image"
title: "Planschrank"
date: "2006-12-09T18:07:42"
picture: "Fischertechnik_Zimmer.jpg"
weight: "10"
konstrukteure: 
- "Lothar Vogt      Pilami"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- details/7804
imported:
- "2019"
_4images_image_id: "7804"
_4images_cat_id: "333"
_4images_user_id: "10"
_4images_image_date: "2006-12-09T18:07:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7804 -->
Jetzt mal ein aktuelles Bild meines "Fischertechnik-Zimmers" mit dem neuen (alten) Planschrank.
Der Schrank hat 10 Schubläden, in denen je 23 Sortiereinsätze aus den 1000er Kästen hineinpassen. Also reichlich Platz.