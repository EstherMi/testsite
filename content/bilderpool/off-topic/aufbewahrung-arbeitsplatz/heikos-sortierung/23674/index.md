---
layout: "image"
title: "Schublade 4 oben"
date: "2009-04-13T00:22:26"
picture: "Heikos_Sortierung-009.jpg"
weight: "7"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/23674
imported:
- "2019"
_4images_image_id: "23674"
_4images_cat_id: "1616"
_4images_user_id: "9"
_4images_image_date: "2009-04-13T00:22:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23674 -->
Links: Statik 1. Die Fächer sind 9cm hoch, so dass bei guter Volumennutzung unglaublich viel hineinpasst. Hier: Die Statik aus zwei Starlifters, ein Super Cranes, zwei Master, ein Mechanic + Static etc..

Rechts: Selten gebrauchte rote Kleinteile, S-Riegel, Alu.