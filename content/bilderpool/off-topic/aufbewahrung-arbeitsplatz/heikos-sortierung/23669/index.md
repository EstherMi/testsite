---
layout: "image"
title: "Schublade 1"
date: "2009-04-13T00:14:31"
picture: "Heikos_Sortierung-002.jpg"
weight: "2"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: ["fischertechnik", "aufbewahrung", "arbeitsplatz", "sortierung", "einzelteile", "schüttgut"]
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/23669
imported:
- "2019"
_4images_image_id: "23669"
_4images_cat_id: "1616"
_4images_user_id: "9"
_4images_image_date: "2009-04-13T00:14:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23669 -->
Links: „Schüttgut” -- alles, was zu mühsam ist zum Sortieren. Die wichtigste Schublade überhaupt.

Rechts: Grundbausteine. Die Bausteine 30 (vorn) sind deshalb so wenig, weil am hinteren Ende noch viele weitere platzsparend zusammengebaut auf Vorrat sind.

Als Trennwand-Material bewährt sich die Wellpappe, in der man die Möbelstücke kauft. In dieser Schublade ist sie nichtmal festgeklebt, sondern ich kann die Fächer in der Größe anpassen. Stabil wird das, sobald genügend Bausteine drinliegen.