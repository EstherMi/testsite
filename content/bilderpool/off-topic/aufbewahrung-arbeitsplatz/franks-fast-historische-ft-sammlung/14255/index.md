---
layout: "image"
title: "große Teile"
date: "2008-04-13T16:49:29"
picture: "franksfasthistorischeftsammlung7.jpg"
weight: "7"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/14255
imported:
- "2019"
_4images_image_id: "14255"
_4images_cat_id: "1317"
_4images_user_id: "729"
_4images_image_date: "2008-04-13T16:49:29"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14255 -->
große Teile, Bauplatten und alles was nicht in die kleinen Sortierkästen passt