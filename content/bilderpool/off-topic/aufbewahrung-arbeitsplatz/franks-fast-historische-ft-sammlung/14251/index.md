---
layout: "image"
title: "Statik"
date: "2008-04-13T16:49:29"
picture: "franksfasthistorischeftsammlung3.jpg"
weight: "3"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/14251
imported:
- "2019"
_4images_image_id: "14251"
_4images_cat_id: "1317"
_4images_user_id: "729"
_4images_image_date: "2008-04-13T16:49:29"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14251 -->
Statik, das meiste noch aus den 60'ern