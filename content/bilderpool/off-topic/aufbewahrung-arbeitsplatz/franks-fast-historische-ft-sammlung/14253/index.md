---
layout: "image"
title: "Elektrik+Getriebe"
date: "2008-04-13T16:49:29"
picture: "franksfasthistorischeftsammlung5.jpg"
weight: "5"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/14253
imported:
- "2019"
_4images_image_id: "14253"
_4images_cat_id: "1317"
_4images_user_id: "729"
_4images_image_date: "2008-04-13T16:49:29"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14253 -->
Elektrik, Getriebe, Rastachsen natürlich nicht aus den 60'ern