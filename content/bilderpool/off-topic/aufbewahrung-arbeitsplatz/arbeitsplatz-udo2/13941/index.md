---
layout: "image"
title: "[2/2] Kellerwerkstatt, Mechanik I"
date: "2008-03-17T18:09:46"
picture: "arbeitsplatz2.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/13941
imported:
- "2019"
_4images_image_id: "13941"
_4images_cat_id: "1280"
_4images_user_id: "723"
_4images_image_date: "2008-03-17T18:09:46"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13941 -->
Aus meiner mechanischen Modellbauwerkstatt Abteilung rot-schwarz optisch passend zur ft CLASSIC LINE. Beide Maschinchen biete ich zum Kauf auch einzeln gern an ft-Freunde. Interessiert? Dann meldet euch für/mit Infos über meine eMail-Adresse.
31.03.2008: Angebot vergriffen