---
layout: "image"
title: "[3/5] ft-Arbeitsplatz, Etappe 2"
date: "2008-10-07T15:00:25"
picture: "arbeitsplatzvonudo3.jpg"
weight: "6"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/15826
imported:
- "2019"
_4images_image_id: "15826"
_4images_cat_id: "1280"
_4images_user_id: "723"
_4images_image_date: "2008-10-07T15:00:25"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15826 -->
Teileregal:
Ausschnitt aus dem "Bereich alte Teile".