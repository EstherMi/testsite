---
layout: "image"
title: "Ein Sprinter füllt sich"
date: "2010-01-12T15:48:35"
picture: "Fischertechnik_Umzug_004.jpg"
weight: "4"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- details/26070
imported:
- "2019"
_4images_image_id: "26070"
_4images_cat_id: "1842"
_4images_user_id: "473"
_4images_image_date: "2010-01-12T15:48:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26070 -->
Im gegensatz zu den sonstigen umzugskisten habe ich mir hier besonders viel Mühe gegeben und alles einzeln getragen. Jetzt fängt schon der Punkt der Reue an. An diesem Punkt der Füllung vbin ich schon 41x aus dem Keller gekommen.