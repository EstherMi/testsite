---
layout: "image"
title: "der zweite Sprinter kommt (allerdings nur noch halb voll )"
date: "2010-01-12T15:48:41"
picture: "Fischertechnik_Umzug_020.jpg"
weight: "19"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- details/26085
imported:
- "2019"
_4images_image_id: "26085"
_4images_cat_id: "1842"
_4images_user_id: "473"
_4images_image_date: "2010-01-12T15:48:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26085 -->
