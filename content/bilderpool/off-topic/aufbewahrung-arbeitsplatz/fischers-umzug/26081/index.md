---
layout: "image"
title: "Angekommen im Sammeldurchgangszimmer"
date: "2010-01-12T15:48:41"
picture: "Fischertechnik_Umzug_015.jpg"
weight: "15"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- details/26081
imported:
- "2019"
_4images_image_id: "26081"
_4images_cat_id: "1842"
_4images_user_id: "473"
_4images_image_date: "2010-01-12T15:48:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26081 -->
