---
layout: "image"
title: "Studio von PK_1"
date: "2005-03-04T14:32:00"
picture: "PICT0001.jpg"
weight: "1"
konstrukteure: 
- "Peter Krijnen"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/3681
imported:
- "2019"
_4images_image_id: "3681"
_4images_cat_id: "442"
_4images_user_id: "144"
_4images_image_date: "2005-03-04T14:32:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3681 -->
Im Forum auf der ft homepage haben wir mal geredet über wie wir unsereren Goldschatz aufbewahren und wie unseren arbeitsplatz ausseht.
Auf die nächsten 14 Foto's ist zu sehen wie es bei mir zur zeit ausseht.

Gruß Peter