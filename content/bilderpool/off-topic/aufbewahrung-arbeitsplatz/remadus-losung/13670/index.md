---
layout: "image"
title: "Neue Ft-Schränke"
date: "2008-02-17T15:39:32"
picture: "Ft-Schrank.jpg"
weight: "1"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: ["ikea", "helmer"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/13670
imported:
- "2019"
_4images_image_id: "13670"
_4images_cat_id: "1257"
_4images_user_id: "46"
_4images_image_date: "2008-02-17T15:39:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13670 -->
Hier sind jetzt zwanzig(!) Kästen (Box 1000) Fischertechnik untergebracht, wobei der fünfte Schrank rechts noch leer ist.

Dieses Modell eines dänischen Großlieferanten eignet sich nach gelinder Modifikation perfekt für Fischertechnik.

Die Sortimentswannen, die bisher in den Boxen standen, habe ich beibehalten, teilweise wurde aus einigen der Boden herausgesägt, um doppelt hohe Sortiereinsätze zu bauen.