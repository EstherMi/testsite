---
layout: "image"
title: "hobby 1, hobby 2, hobby S"
date: "2008-07-13T16:36:16"
picture: "steffalkswohnzimmerfussbodenstand10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/14824
imported:
- "2019"
_4images_image_id: "14824"
_4images_cat_id: "1354"
_4images_user_id: "104"
_4images_image_date: "2008-07-13T16:36:16"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14824 -->
Vornedran liegen noch ein paar längere Achsen, die ganz langen 50-cm-Achsen von Conrad und 12 Statikschienen (2 übrigens aus meinem guten alten ft 200S, 4 aus dem ft 400S und 6 mal bei Knobloch nachgekauft).