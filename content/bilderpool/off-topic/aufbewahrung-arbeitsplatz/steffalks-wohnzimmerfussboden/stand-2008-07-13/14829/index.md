---
layout: "image"
title: "fischertechnik 1000-Sortierkasten 9"
date: "2008-07-13T16:36:16"
picture: "steffalkswohnzimmerfussbodenstand15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/14829
imported:
- "2019"
_4images_image_id: "14829"
_4images_cat_id: "1354"
_4images_user_id: "104"
_4images_image_date: "2008-07-13T16:36:16"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14829 -->
Weitere Statik, Platten, fertige Führerhäuser (immer noch Igitt), Motore.