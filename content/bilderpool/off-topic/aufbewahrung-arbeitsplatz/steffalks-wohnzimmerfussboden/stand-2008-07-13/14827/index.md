---
layout: "image"
title: "fischertechnik 1000-Sortierkasten 7"
date: "2008-07-13T16:36:16"
picture: "steffalkswohnzimmerfussbodenstand13.jpg"
weight: "13"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/14827
imported:
- "2019"
_4images_image_id: "14827"
_4images_cat_id: "1354"
_4images_user_id: "104"
_4images_image_date: "2008-07-13T16:36:16"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14827 -->
Streben, Platten, die alten kleinen Reifen und die guten Kurven- und Nockenscheiben. Zwei 9-V-Batteriekästen mussten neben den I-Streben 120 Platz nehmen. Noch ein Schmankerl? Die auf der Unterseite der Streben befindlichen "Fisch"-Symbole zeigen *alle* nach außen, auf diesem Bild also nach rechts. Jaja, ich weiß, was Ihr sagen wollt... ;-)