---
layout: "overview"
title: "Stand 2008-07-13"
date: 2019-12-17T18:07:07+01:00
legacy_id:
- categories/1354
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1354 --> 
So sieht\'s aus, wenn ich auf dem Boden rumkrabbeln muss. In absehbarer Zeit kann ich hoffentlich einen ständig aufgebauten Arbeitsplatz einrichten, bei dem ich auf einem Stuhl sitzen kann.