---
layout: "image"
title: "Linke Seite"
date: "2008-07-13T16:36:15"
picture: "steffalkswohnzimmerfussbodenstand02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/14816
imported:
- "2019"
_4images_image_id: "14816"
_4images_cat_id: "1354"
_4images_user_id: "104"
_4images_image_date: "2008-07-13T16:36:15"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14816 -->
Viele Standardteile, Elektromechanik, Elektronik, Pneumatik.