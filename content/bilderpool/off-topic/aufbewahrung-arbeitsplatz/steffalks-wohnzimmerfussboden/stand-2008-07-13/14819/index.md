---
layout: "image"
title: "fischertechnik 1000-Sortierkasten 1"
date: "2008-07-13T16:36:15"
picture: "steffalkswohnzimmerfussbodenstand05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/14819
imported:
- "2019"
_4images_image_id: "14819"
_4images_cat_id: "1354"
_4images_user_id: "104"
_4images_image_date: "2008-07-13T16:36:15"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14819 -->
Von links außen über die hobby-Kästen nach rechts außen fotografiert.