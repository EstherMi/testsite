---
layout: "image"
title: "fischertechnik 1000-Sortierkasten 5"
date: "2008-07-13T16:36:16"
picture: "steffalkswohnzimmerfussbodenstand09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/14823
imported:
- "2019"
_4images_image_id: "14823"
_4images_cat_id: "1354"
_4images_user_id: "104"
_4images_image_date: "2008-07-13T16:36:16"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14823 -->
Gelenke (sowohl die neueren als auch die alten 45-mm-Gelenkbausteine und Federgelenkbausteine), IR-Control-Set mit Empfänger 2, Elektromechanik mit Zählwerken, Dauer- und Elektromagneten, Lampen, Fotozellen, Pneumatik. Auch hier gilt mein Dank Dirk Haizmann, der mir außer den drei Magnetventilen (siehe vorheriges Bild) auch drei Pneumatikzylinder geschenkt hatte. Mit den Zylindern, öffnenden und schließenden Festo-Ventilen, Einfach- und Doppelbetätigern usw. ist dies eines meiner Lieblingsfotos.