---
layout: "image"
title: "Frontseite: hobby-Kästen, Digital-Praktikum"
date: "2008-07-13T16:36:15"
picture: "steffalkswohnzimmerfussbodenstand03.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/14817
imported:
- "2019"
_4images_image_id: "14817"
_4images_cat_id: "1354"
_4images_user_id: "104"
_4images_image_date: "2008-07-13T16:36:15"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14817 -->
Vornedran liegen die 50-cm-Conrad-Achsen und ein 30adriges Flachbandkabel.