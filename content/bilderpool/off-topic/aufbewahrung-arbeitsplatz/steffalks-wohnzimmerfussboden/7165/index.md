---
layout: "image"
title: "hobby 1, 2 und S"
date: "2006-10-10T19:04:09"
picture: "steffalkswohnzimmerfussboden10.jpg"
weight: "10"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/7165
imported:
- "2019"
_4images_image_id: "7165"
_4images_cat_id: "688"
_4images_user_id: "104"
_4images_image_date: "2006-10-10T19:04:09"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7165 -->
Die eingepackten Achsen sind 50-cm-Achsen von Knobloch.