---
layout: "image"
title: "Interfaces, Netzteile, Alus"
date: "2006-10-10T19:04:26"
picture: "steffalkswohnzimmerfussboden17.jpg"
weight: "17"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/7172
imported:
- "2019"
_4images_image_id: "7172"
_4images_cat_id: "688"
_4images_user_id: "104"
_4images_image_date: "2006-10-10T19:04:26"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7172 -->
Im roten Blechkasten befinden sich die Kabel, 1 Schraubendreher und Papierrollen von alten Rechenmaschinen, teileweise für das schöne Modell "Autotrainer" aus einem alten Clubheft.