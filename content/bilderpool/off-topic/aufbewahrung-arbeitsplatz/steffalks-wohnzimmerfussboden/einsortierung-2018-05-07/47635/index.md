---
layout: "image"
title: "Schrank 4 Schublade 6 (unten) verdeckte Lage"
date: "2018-05-07T22:44:05"
picture: "einsortierung39.jpg"
weight: "39"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47635
imported:
- "2019"
_4images_image_id: "47635"
_4images_cat_id: "3511"
_4images_user_id: "104"
_4images_image_date: "2018-05-07T22:44:05"
_4images_image_order: "39"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47635 -->
Ein Schraubstock (von meiner Tochter selbst gemacht, Danke!), Convention-Teile, weitere Kabel für die Netduinos, Widerstandssortiment.