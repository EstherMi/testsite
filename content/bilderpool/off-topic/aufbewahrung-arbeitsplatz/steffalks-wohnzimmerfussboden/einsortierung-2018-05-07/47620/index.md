---
layout: "image"
title: "Schrank 2 Schublade 2"
date: "2018-05-07T22:44:05"
picture: "einsortierung24.jpg"
weight: "24"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47620
imported:
- "2019"
_4images_image_id: "47620"
_4images_cat_id: "3511"
_4images_user_id: "104"
_4images_image_date: "2018-05-07T22:44:05"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47620 -->
Alle I-Streben von 15 bis 120, Riegelsteine, seltener gebrauchte Statikbauteile. Links daneben neuere (matte) Statikschienen.