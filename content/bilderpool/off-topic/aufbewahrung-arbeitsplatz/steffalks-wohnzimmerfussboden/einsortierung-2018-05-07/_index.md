---
layout: "overview"
title: "Einsortierung 2018-05-07"
date: 2019-12-17T18:07:15+01:00
legacy_id:
- categories/3511
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3511 --> 
Der letzte bei mir dokumentierte Stand (wurde nie auf die ftc hochgeladen) stammte von 2015. Seitdem kam mehr dazu, was eine fast komplett neue Sortierung erforderlich machte.