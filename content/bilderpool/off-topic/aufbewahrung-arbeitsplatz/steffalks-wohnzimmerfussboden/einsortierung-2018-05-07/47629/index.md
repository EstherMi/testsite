---
layout: "image"
title: "Schrank 4 Schublade 2"
date: "2018-05-07T22:44:05"
picture: "einsortierung33.jpg"
weight: "33"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47629
imported:
- "2019"
_4images_image_id: "47629"
_4images_cat_id: "3511"
_4images_user_id: "104"
_4images_image_date: "2018-05-07T22:44:05"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47629 -->
Pneumatik: Zylinder, Einfach- und Doppelbetätiger, blaue (Öffner) und rote (Schließer) Festoventile, T-Stücke, Stopfen, Rückschlagventile, Staudüsen, Verteiler, Kompressoren, Kompressorteile für Selbstbaukompressoren mit S- und Ur-Motoren, Rollenhebel für die Festoventile, Handventile, Drosseln, Magnetventile, eine pneumatische Drehdurchführung (von einem netten Holländer geschenkt, Danke!), Lufttanks, eine Lemo-Solar-Membranpumpe, ein Manometer, Solarzellen und -motoren, Luftschrauben.