---
layout: "image"
title: "Schrank 3 Schublade 5 sichtbare Lage"
date: "2018-05-07T22:44:05"
picture: "einsortierung28.jpg"
weight: "28"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47624
imported:
- "2019"
_4images_image_id: "47624"
_4images_cat_id: "3511"
_4images_user_id: "104"
_4images_image_date: "2018-05-07T22:44:05"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47624 -->
Statikträger 120, Flachträger 120, Flach- und Bogenstücke.