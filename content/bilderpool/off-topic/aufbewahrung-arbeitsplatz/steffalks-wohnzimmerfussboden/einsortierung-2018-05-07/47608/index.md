---
layout: "image"
title: "Schrank 1 Schublade 5 sichtbare Lage"
date: "2018-05-07T22:44:05"
picture: "einsortierung12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47608
imported:
- "2019"
_4images_image_id: "47608"
_4images_cat_id: "3511"
_4images_user_id: "104"
_4images_image_date: "2018-05-07T22:44:05"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47608 -->
Zahnräder, Winkelzahnräder, Innenzahnräder, Drehscheiben, O-Ringe dafür, Rast-Differenziale, Vorstuferäder incl. Halter und Reifen, Speichenräder, Drehkränze, aus geplatzten Bausteinen 5 und 15 stammende Innereien (Zapfen mit Metallstift), Kurbeln (die einzelne ist länger als die anderen), Schwungräder, Maltesergetriebe (Danke, Roland!), große Differenziale.