---
layout: "image"
title: "Schrank 2 Schublade 6 (unten) verdeckte Lage"
date: "2018-05-07T22:44:05"
picture: "einsortierung22.jpg"
weight: "22"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47618
imported:
- "2019"
_4images_image_id: "47618"
_4images_cat_id: "3511"
_4images_user_id: "104"
_4images_image_date: "2018-05-07T22:44:05"
_4images_image_order: "22"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47618 -->
Ein leerer Sortierkasten 500 und der Bootskörper aus einem Solar-Baukasten.