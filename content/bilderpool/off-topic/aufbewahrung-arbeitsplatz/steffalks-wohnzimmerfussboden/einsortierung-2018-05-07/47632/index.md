---
layout: "image"
title: "Schrank 4 Schublade 5 sichtbare Lage"
date: "2018-05-07T22:44:05"
picture: "einsortierung36.jpg"
weight: "36"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47632
imported:
- "2019"
_4images_image_id: "47632"
_4images_cat_id: "3511"
_4images_user_id: "104"
_4images_image_date: "2018-05-07T22:44:05"
_4images_image_order: "36"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47632 -->
Computing (Parallel-Interface, Robo Connect Box, Robo Interface, RF DataLink, USB-Kabel, Schrittmotoren), IR-Fernsteuerungen, Akkus, Ladegerät und Batteriehalter. Das gelbe Kästchen ist ein perfekt funktionierender von Thomas Kaiser selbst gebauter und mir geschenkter USB-nach-V24-Konverter (Danke!).