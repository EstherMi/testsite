---
layout: "image"
title: "Schrank 3 Schublade 6 (unten) verdeckte Lage"
date: "2018-05-07T22:44:05"
picture: "einsortierung31.jpg"
weight: "31"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47627
imported:
- "2019"
_4images_image_id: "47627"
_4images_cat_id: "3511"
_4images_user_id: "104"
_4images_image_date: "2018-05-07T22:44:05"
_4images_image_order: "31"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47627 -->
Bunte Verkleidungsplatten mit Zapfen, Winkelklammern, Türen, Kamin und Giebelteile für die Clipsplatten.