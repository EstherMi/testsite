---
layout: "image"
title: "Schrank 4 Schublade 4"
date: "2018-05-07T22:44:05"
picture: "einsortierung35.jpg"
weight: "35"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47631
imported:
- "2019"
_4images_image_id: "47631"
_4images_cat_id: "3511"
_4images_user_id: "104"
_4images_image_date: "2018-05-07T22:44:05"
_4images_image_order: "35"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47631 -->
Netzteile von fischertechnik (die zwei unteren mit echtem 50-Hz-Wechselspannungsausgang für 50-Hz-Uhren) und von Fremdherstellern, Netzschaltgerät, Power Controller und die kleinen Demo-Spritzgusseisenbahnen von einem Fan-Club-Tag.