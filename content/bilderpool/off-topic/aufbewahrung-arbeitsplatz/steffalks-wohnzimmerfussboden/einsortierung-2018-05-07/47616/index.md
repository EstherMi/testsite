---
layout: "image"
title: "Schrank 2 Schublade 5"
date: "2018-05-07T22:44:05"
picture: "einsortierung20.jpg"
weight: "20"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47616
imported:
- "2019"
_4images_image_id: "47616"
_4images_cat_id: "3511"
_4images_user_id: "104"
_4images_image_date: "2018-05-07T22:44:05"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47616 -->
Bauplatten 1000 (solche mit Bodenplatte), schwarze und eine graue Bauplatte 500.