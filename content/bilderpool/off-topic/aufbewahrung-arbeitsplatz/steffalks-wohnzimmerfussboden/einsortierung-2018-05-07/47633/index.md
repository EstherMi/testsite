---
layout: "image"
title: "Schrank 4 Schublade 5 verdeckte Lage"
date: "2018-05-07T22:44:05"
picture: "einsortierung37.jpg"
weight: "37"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47633
imported:
- "2019"
_4images_image_id: "47633"
_4images_cat_id: "3511"
_4images_user_id: "104"
_4images_image_date: "2018-05-07T22:44:05"
_4images_image_order: "37"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47633 -->
Robo-I/O-Extensions, V24-Kabel, Flachkabel für das Parallelinterface und zwei Ur-Batteriestäbe, die sich unterhalb der kleinen roten Batteriehalter (siehe voriges Bild) befinden.