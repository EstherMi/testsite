---
layout: "image"
title: "Einzelteile 9"
date: "2006-10-10T19:04:26"
picture: "steffalkswohnzimmerfussboden15.jpg"
weight: "15"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/7170
imported:
- "2019"
_4images_image_id: "7170"
_4images_cat_id: "688"
_4images_user_id: "104"
_4images_image_date: "2006-10-10T19:04:26"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7170 -->
Statik, Fertigteile (Führerhäuser, igitt), Motoren