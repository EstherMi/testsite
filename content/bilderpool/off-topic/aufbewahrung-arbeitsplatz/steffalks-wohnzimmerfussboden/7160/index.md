---
layout: "image"
title: "Einzelteile 1"
date: "2006-10-10T19:04:09"
picture: "steffalkswohnzimmerfussboden05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/7160
imported:
- "2019"
_4images_image_id: "7160"
_4images_cat_id: "688"
_4images_user_id: "104"
_4images_image_date: "2006-10-10T19:04:09"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7160 -->
Von links vorne über die hobby-Kästen nach rechts vorne fotografiert. Das weiße runde Döschen links oben enthält Vaseline zur Schmierung von Achsen.