---
layout: "image"
title: "20 - Schublade rechts 6 verdeckte Lage"
date: "2009-07-19T20:07:12"
picture: "steffalksarbeitsplatz21.jpg"
weight: "21"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/24663
imported:
- "2019"
_4images_image_id: "24663"
_4images_cat_id: "1693"
_4images_user_id: "104"
_4images_image_date: "2009-07-19T20:07:12"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24663 -->
Weitere Elektronik-Einzelteile und Kabel für die 80er-Jahre-Elektronikbausteine, Computing-Sensoren, Akkus, IR-Fernsteuerung, Lichtleitstäbe, Silikonschlauch, Pneumatikschläuche.