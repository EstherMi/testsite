---
layout: "image"
title: "11 - Schublade links 6 verdeckte Lage"
date: "2009-07-19T20:07:12"
picture: "steffalksarbeitsplatz11.jpg"
weight: "11"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/24653
imported:
- "2019"
_4images_image_id: "24653"
_4images_cat_id: "1693"
_4images_user_id: "104"
_4images_image_date: "2009-07-19T20:07:12"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24653 -->
Darunter weitere Verkleidungsteile, Seilrollen, Lenkräder, Sitze, Männchen und fertige Führerhäuser.