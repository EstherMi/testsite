---
layout: "image"
title: "19 - Schublade rechts 6 sichtbare Lage"
date: "2009-07-19T20:07:12"
picture: "steffalksarbeitsplatz20.jpg"
weight: "20"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/24662
imported:
- "2019"
_4images_image_id: "24662"
_4images_cat_id: "1693"
_4images_user_id: "104"
_4images_image_date: "2009-07-19T20:07:12"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24662 -->
Der Rest: Pneumatiktanks, Kabel, Stromversorgung. Links oben ein 30adriges Flachbandkabel. Die drei Rechenmaschinenrollen in der Kunststofftüte mit der schwarzen Linie darauf sind mit schwarzer Wasserfarbe als Straße bemalte Fahrbahnen für das alte Modell "Autotrainer" aus dem Clubheft 1972-4.