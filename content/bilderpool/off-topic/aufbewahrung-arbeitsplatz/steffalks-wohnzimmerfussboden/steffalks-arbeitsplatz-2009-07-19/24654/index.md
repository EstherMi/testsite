---
layout: "image"
title: "12 - rechter Schrank"
date: "2009-07-19T20:07:12"
picture: "steffalksarbeitsplatz12.jpg"
weight: "12"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/24654
imported:
- "2019"
_4images_image_id: "24654"
_4images_cat_id: "1693"
_4images_user_id: "104"
_4images_image_date: "2009-07-19T20:07:12"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24654 -->
Der rechte Schrank enthält Motoren, Statik, Pneumatik, Elektromechanik, Elektronik, Stromversorgung.