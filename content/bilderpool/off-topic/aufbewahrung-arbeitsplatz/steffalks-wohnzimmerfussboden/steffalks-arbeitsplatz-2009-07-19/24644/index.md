---
layout: "image"
title: "02 - Linker Schrank"
date: "2009-07-19T20:07:11"
picture: "steffalksarbeitsplatz02.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/24644
imported:
- "2019"
_4images_image_id: "24644"
_4images_cat_id: "1693"
_4images_user_id: "104"
_4images_image_date: "2009-07-19T20:07:11"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24644 -->
Der Linke Schrank enthält von oben nach unten:

- Grundbausteine
- Rote Bausteine
- Räder und Achsen
- Zahnräder usw.
- Große Platten
- Kleinere Verkleidungsplatten

... und anderes.