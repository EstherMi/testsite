---
layout: "image"
title: "07 - Schublade links 4 verdeckte Lage"
date: "2009-07-19T20:07:12"
picture: "steffalksarbeitsplatz07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/24649
imported:
- "2019"
_4images_image_id: "24649"
_4images_cat_id: "1693"
_4images_user_id: "104"
_4images_image_date: "2009-07-19T20:07:12"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24649 -->
Von Schublade 4 ist nur die hintere Reihe zweilagig. In der unteren Lage sind weitere Raupenketten, Gummiraupen, Drehkränze, Planierraupenschaufeln und der fischertechnik Kraftmesser gelagert.