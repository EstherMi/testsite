---
layout: "image"
title: "09 - Schublade links 5 verdeckte Lage"
date: "2009-07-19T20:07:12"
picture: "steffalksarbeitsplatz09.jpg"
weight: "9"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/24651
imported:
- "2019"
_4images_image_id: "24651"
_4images_cat_id: "1693"
_4images_user_id: "104"
_4images_image_date: "2009-07-19T20:07:12"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24651 -->
Weitere Grundplatten, die restlichen U-Träger, Vorstuferäder, Kurven- und Nockenscheiben - und die ganz alten Pappteile aus dem ft 200/400 (die 3 Raketenverkleidungsteile des ft 400 habe ich leider nicht mehr). Mit denen konnte man einen hübschen Radarschirm, einen kleinen Betonmischer und eine Sieb-/Lostrommel bauen (siehe die quadratische blaue Anleitung zu den damaligen Grundkästen von ft).