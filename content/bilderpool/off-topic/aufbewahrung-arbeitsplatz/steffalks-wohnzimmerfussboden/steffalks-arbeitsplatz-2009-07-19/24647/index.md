---
layout: "image"
title: "05 - Schublade links 3"
date: "2009-07-19T20:07:11"
picture: "steffalksarbeitsplatz05.jpg"
weight: "5"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/24647
imported:
- "2019"
_4images_image_id: "24647"
_4images_cat_id: "1693"
_4images_user_id: "104"
_4images_image_date: "2009-07-19T20:07:11"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24647 -->
Naben (und zwar gleich geöffnet, weil man sie ja sowieso fast immer erst mal offen benötigt), Metall- und Kunststoffachsen, Schnecken, Kardangelenke, Rastachsenteile incl. Rast-Z10. Vornedran 50-cm-Achsen von Conrad (deshalb fungieren bei dieser Schublade nicht Kästchen, sondern Päckchen von Sortiereinsatz-Trennstegen als Abstandshalter), links nebendran auch längere Metallachsen.