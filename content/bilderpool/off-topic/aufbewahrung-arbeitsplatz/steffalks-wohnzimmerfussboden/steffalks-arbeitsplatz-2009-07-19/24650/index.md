---
layout: "image"
title: "08 - Schublade links 5 sichtbare Lage"
date: "2009-07-19T20:07:12"
picture: "steffalksarbeitsplatz08.jpg"
weight: "8"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/24650
imported:
- "2019"
_4images_image_id: "24650"
_4images_cat_id: "1693"
_4images_user_id: "104"
_4images_image_date: "2009-07-19T20:07:12"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24650 -->
Bauplatten, alles was Reifen heißt incl. der zugehörigen Gummis. Links daneben weitere lange Metallachsen.