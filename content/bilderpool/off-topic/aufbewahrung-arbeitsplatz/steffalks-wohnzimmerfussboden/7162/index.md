---
layout: "image"
title: "Einzelteile 3"
date: "2006-10-10T19:04:09"
picture: "steffalkswohnzimmerfussboden07.jpg"
weight: "7"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/7162
imported:
- "2019"
_4images_image_id: "7162"
_4images_cat_id: "688"
_4images_user_id: "104"
_4images_image_date: "2006-10-10T19:04:09"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7162 -->
Achsen, verschiedene BS15, 80er-Jahre-Elektronikteile, Silikonschlauch (vom hobby-4) und alle bis auf einen Schraubendreher (einer steckt bei den Kabeln)