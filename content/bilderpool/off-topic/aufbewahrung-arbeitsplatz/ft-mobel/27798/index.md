---
layout: "image"
title: "FT Möbel"
date: "2010-08-06T22:39:46"
picture: "ftmoebel01.jpg"
weight: "1"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/27798
imported:
- "2019"
_4images_image_id: "27798"
_4images_cat_id: "2004"
_4images_user_id: "968"
_4images_image_date: "2010-08-06T22:39:46"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27798 -->
Nachdem vor zwei Jahren unser Sohnemann noch dazu kam,war´s mit meinem "Spielzimmer" schnell vorbei.Dafür räumt man natürlich gerne ,aber ich mußte meine FT wieder anderweitg unterbringen.Zum bauen mußte dann immer der Tapeziertisch ran und das war sehr umständlich und führte dazu,das ich nur noch wenig gemacht habe.