---
layout: "image"
title: "FT Möbel"
date: "2010-08-06T22:39:47"
picture: "ftmoebel13.jpg"
weight: "13"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/27810
imported:
- "2019"
_4images_image_id: "27810"
_4images_cat_id: "2004"
_4images_user_id: "968"
_4images_image_date: "2010-08-06T22:39:47"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27810 -->
Das Ganze hat an Material ca 300 € gekostet. Die Schubladenschienen mit Selbsteinzug kosteten allein140 € .Dazu kommen noch ca. 60 Arbeitstunden.Vor allem das hier sichtbare Fächersystem war baulich doch recht auwendig. Die Sortierung ist auch noch völlig unstrukturiert weil ich noch ein größeres Modell im Bau habe und noch nicht weiß,wieviel Platz ich wirklich benötige.
Aber ich hoffe das jetzt zwischendurch immer mal was machen kann und mein im April 2007 begonnener Tagebaubagger mal fertig wird.