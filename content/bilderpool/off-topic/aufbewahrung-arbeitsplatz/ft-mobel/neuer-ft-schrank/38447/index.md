---
layout: "image"
title: "Neuer FT Schubladenschrank"
date: "2014-03-09T17:20:54"
picture: "neuerftschrank4.jpg"
weight: "4"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/38447
imported:
- "2019"
_4images_image_id: "38447"
_4images_cat_id: "2866"
_4images_user_id: "968"
_4images_image_date: "2014-03-09T17:20:54"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38447 -->
So sieht meine komplette Ecke jetzt aus :)