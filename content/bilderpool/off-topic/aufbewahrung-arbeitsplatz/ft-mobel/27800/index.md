---
layout: "image"
title: "FT Möbel"
date: "2010-08-06T22:39:46"
picture: "ftmoebel03.jpg"
weight: "3"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/27800
imported:
- "2019"
_4images_image_id: "27800"
_4images_cat_id: "2004"
_4images_user_id: "968"
_4images_image_date: "2010-08-06T22:39:46"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27800 -->
Hier ist das Teil positioniert und  dieTischfüße sind eingesteckt .