---
layout: "image"
title: "FT Möbel"
date: "2010-08-06T22:39:47"
picture: "ftmoebel09.jpg"
weight: "9"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/27806
imported:
- "2019"
_4images_image_id: "27806"
_4images_cat_id: "2004"
_4images_user_id: "968"
_4images_image_date: "2010-08-06T22:39:47"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27806 -->
Schublade 5 ,Statik