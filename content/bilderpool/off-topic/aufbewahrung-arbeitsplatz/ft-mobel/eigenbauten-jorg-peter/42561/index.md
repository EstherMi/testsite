---
layout: "image"
title: "Regal 6"
date: "2015-12-22T22:42:30"
picture: "ftc_regal_06.jpg"
weight: "7"
konstrukteure: 
- "Jörg-Peter"
fotografen:
- "Jörg-Peter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jörg-peter"
license: "unknown"
legacy_id:
- details/42561
imported:
- "2019"
_4images_image_id: "42561"
_4images_cat_id: "3164"
_4images_user_id: "2236"
_4images_image_date: "2015-12-22T22:42:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42561 -->
Diese Sortier-Einsätze gibt es entweder zusammen mit den Käsen oder lose. Die Qualität ist sehr ordentlich. Bevor man größere Stückzahlen kauft, lohnt ein Preisvergleich.