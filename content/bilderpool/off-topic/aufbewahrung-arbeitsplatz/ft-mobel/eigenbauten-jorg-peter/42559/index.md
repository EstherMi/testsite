---
layout: "image"
title: "Regal 4"
date: "2015-12-22T22:42:30"
picture: "ftc_regal_04.jpg"
weight: "5"
konstrukteure: 
- "Jörg-Peter"
fotografen:
- "Jörg-Peter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jörg-peter"
license: "unknown"
legacy_id:
- details/42559
imported:
- "2019"
_4images_image_id: "42559"
_4images_cat_id: "3164"
_4images_user_id: "2236"
_4images_image_date: "2015-12-22T22:42:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42559 -->
So sieht die Teilesammlung dann aus. In den Sortierkästen sind lose Einsätze, die es in vielen verschiedenen Größen gibt. Dass man sie rausnehmen kann, ist zum Entnehmen vor allem kleiner Teile sehr praktisch.