---
layout: "image"
title: "Einfaches Regal 1"
date: "2015-12-23T20:21:23"
picture: "ftc_regal_00.jpg"
weight: "1"
konstrukteure: 
- "Jörg-Peter"
fotografen:
- "Jörg-Peter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jörg-peter"
license: "unknown"
legacy_id:
- details/42565
imported:
- "2019"
_4images_image_id: "42565"
_4images_cat_id: "3164"
_4images_user_id: "2236"
_4images_image_date: "2015-12-23T20:21:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42565 -->
Das ist die einfache Variante der von mir vorgestellten Möbel für die Aufbewahrung von fischertechnik. Sie lässt sich einfach nachbauen und natürlich nach Belieben modifizieren. Einen detaillierten Bauplan mit Schritt-für-Schritt-Anleitung gibt es hier: http://ftcommunity.de/downloads.php?kategorie=ft%3Apedia+Dateien