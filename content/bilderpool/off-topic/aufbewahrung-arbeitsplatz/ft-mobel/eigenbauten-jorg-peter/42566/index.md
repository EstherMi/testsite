---
layout: "image"
title: "Rollcontainer befüllt"
date: "2015-12-23T20:21:23"
picture: "ftc_container_01.jpg"
weight: "2"
konstrukteure: 
- "Jörg-Peter"
fotografen:
- "Jörg-Peter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jörg-peter"
license: "unknown"
legacy_id:
- details/42566
imported:
- "2019"
_4images_image_id: "42566"
_4images_cat_id: "3164"
_4images_user_id: "2236"
_4images_image_date: "2015-12-23T20:21:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42566 -->
So sieht der selbst gebaute Rollcontainer aus, wenn er mit den von mir gewählten Sortierboxen des Herstellers Allit (System Europlus 37) bestückt ist. Das Möbel sieht simpel aus, der Nachbau ist aber nicht ganz trivial. Detaillierte Informationen werden in der Rubrik Downloads (http://ftcommunity.de/downloads.php?kategorie=ft%3Apedia+Dateien) veröffentlicht. Einen Beitrag zum Thema fischertechnik-Aufbewahrung mit den Pros und Contras der verschiedenen Strategien erscheint in der ft:pedia 4/2015.