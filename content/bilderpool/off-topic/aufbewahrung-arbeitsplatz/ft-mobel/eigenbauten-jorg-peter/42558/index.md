---
layout: "image"
title: "Einfaches Regal 3"
date: "2015-12-22T22:42:30"
picture: "ftc_regal_03.jpg"
weight: "4"
konstrukteure: 
- "Jörg-Peter"
fotografen:
- "Jörg-Peter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jörg-peter"
license: "unknown"
legacy_id:
- details/42558
imported:
- "2019"
_4images_image_id: "42558"
_4images_cat_id: "3164"
_4images_user_id: "2236"
_4images_image_date: "2015-12-22T22:42:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42558 -->
Für die ganze Montage reichen einige Schrauben, normaler Leim und ein Akkuschrauber.