---
layout: "image"
title: "Rollcontainer noch nicht befüllt"
date: "2015-12-22T22:42:30"
picture: "ftc_container_02_2.jpg"
weight: "10"
konstrukteure: 
- "Jörg-Peter"
fotografen:
- "Jörg-Peter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jörg-peter"
license: "unknown"
legacy_id:
- details/42564
imported:
- "2019"
_4images_image_id: "42564"
_4images_cat_id: "3164"
_4images_user_id: "2236"
_4images_image_date: "2015-12-22T22:42:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42564 -->
So sieht der Container aus, bevor er gefüllt wird. Das Möbelstück ist etwa 70x70 cm groß, ein detaillierter Bauplan wird unter http://ftcommunity.de/downloads.php?kategorie=ft%3Apedia+Dateien veröffenticht. Zum Bauen zieht man sich den Container einfach vor und hat dann oben gleich auch noch eine Fläche von ganz ordentlicher Größe. Im Kinderzimmer muss er aus Sicherheitsgründen mit Rollen versehen werden, sie sich feststellen lassen.