---
layout: "image"
title: "FT Möbel"
date: "2010-08-06T22:39:47"
picture: "ftmoebel08.jpg"
weight: "8"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/27805
imported:
- "2019"
_4images_image_id: "27805"
_4images_cat_id: "2004"
_4images_user_id: "968"
_4images_image_date: "2010-08-06T22:39:47"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27805 -->
Schublade 4 ,Achsen, etc.