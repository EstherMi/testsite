---
layout: "image"
title: "Pneumatik"
date: "2010-10-28T15:07:32"
picture: "endlich11.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/29068
imported:
- "2019"
_4images_image_id: "29068"
_4images_cat_id: "2112"
_4images_user_id: "1162"
_4images_image_date: "2010-10-28T15:07:32"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29068 -->
mit Ventilen, Kleinteilen, Schlächen, Wattestäbchen, Kompressorkubeln, Zylinder, Rückschlagventilen