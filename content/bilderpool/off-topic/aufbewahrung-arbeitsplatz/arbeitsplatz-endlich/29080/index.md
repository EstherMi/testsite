---
layout: "image"
title: "...Teile.."
date: "2010-10-28T15:07:33"
picture: "endlich23.jpg"
weight: "23"
konstrukteure: 
- "-?-"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/29080
imported:
- "2019"
_4images_image_id: "29080"
_4images_cat_id: "2112"
_4images_user_id: "1162"
_4images_image_date: "2010-10-28T15:07:33"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29080 -->
Kleinteile: Verbinder, S-Riegel, kleine Streben, Rollenlager, Statik,...