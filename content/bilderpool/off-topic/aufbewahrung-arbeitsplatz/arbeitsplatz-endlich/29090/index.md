---
layout: "image"
title: "Koffer mit Einsatz"
date: "2010-10-28T15:07:33"
picture: "endlich33.jpg"
weight: "33"
konstrukteure: 
- "BTI"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/29090
imported:
- "2019"
_4images_image_id: "29090"
_4images_cat_id: "2112"
_4images_user_id: "1162"
_4images_image_date: "2010-10-28T15:07:33"
_4images_image_order: "33"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29090 -->
