---
layout: "image"
title: "Vitrine offen"
date: "2010-10-28T15:07:32"
picture: "endlich04.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/29061
imported:
- "2019"
_4images_image_id: "29061"
_4images_cat_id: "2112"
_4images_user_id: "1162"
_4images_image_date: "2010-10-28T15:07:32"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29061 -->
Bauplatten, akku, restliche Statikteile, Werkzeugkoffer, Box 500