---
layout: "image"
title: "Achsen-Sortierung"
date: "2010-03-28T14:49:50"
picture: "bauecke3.jpg"
weight: "3"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/26832
imported:
- "2019"
_4images_image_id: "26832"
_4images_cat_id: "1918"
_4images_user_id: "373"
_4images_image_date: "2010-03-28T14:49:50"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26832 -->
In einem kleinen Obi-Sortierkasten (3er-Pack für 4 oder 5 Euro) lassen sich die Achsen wunderbar unterbringen. Auch mittelgroße Mengen finden noch einen Platz.
Hier nur der Kasten für die Plastikachsen, einen identischen Kasten gibts auch für die Metallachsen.