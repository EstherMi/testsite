---
layout: "image"
title: "Transportfertig"
date: "2016-08-20T23:05:37"
picture: "minimalsammlung01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/44304
imported:
- "2019"
_4images_image_id: "44304"
_4images_cat_id: "3269"
_4images_user_id: "373"
_4images_image_date: "2016-08-20T23:05:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44304 -->
