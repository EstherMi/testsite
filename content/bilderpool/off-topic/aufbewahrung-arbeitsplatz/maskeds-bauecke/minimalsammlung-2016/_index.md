---
layout: "overview"
title: "Minimalsammlung 2016"
date: 2019-12-17T18:07:45+01:00
legacy_id:
- categories/3269
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3269 --> 
Ab September ziehe ich für meine Masterarbeit um. Da ich mein gesamtes ft definitiv nicht mitnehmen kann, habe ich versucht, eine Minimalsammlung zusammenzustellen.

Es sollte von möglichst allen aktuellen Teilen eine ausreichende Anzahl für "normale" Modelle vorhanden sein. Eigentlich wollte ich mich auf eine Box 1000 beschränken, das stellte sich aber bald als nicht sinnvoll möglich heraus. Deshalb sind es jetzt 2 Boxen 1000 geworden.

Was habe ich vergessen, was haltet ihr für völlig überflüssig, was ist zu viel?