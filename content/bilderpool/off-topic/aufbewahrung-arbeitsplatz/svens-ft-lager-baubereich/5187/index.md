---
layout: "image"
title: "sven bastelecke 003"
date: "2005-11-05T16:00:38"
picture: "sven_bastelecke_003.jpg"
weight: "3"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/5187
imported:
- "2019"
_4images_image_id: "5187"
_4images_cat_id: "435"
_4images_user_id: "1"
_4images_image_date: "2005-11-05T16:00:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5187 -->
