---
layout: "image"
title: "Ansicht 3"
date: "2009-06-24T22:42:41"
picture: "P6170061.jpg"
weight: "3"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- details/24454
imported:
- "2019"
_4images_image_id: "24454"
_4images_cat_id: "1677"
_4images_user_id: "381"
_4images_image_date: "2009-06-24T22:42:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24454 -->
Die Tür zum Ölkeller