---
layout: "image"
title: "Teleskop Mobilkran"
date: "2015-11-07T12:46:13"
picture: "fischertechnikteamaustriararitaetenkabinett75.jpg"
weight: "75"
konstrukteure: 
- "FISCHERTECHNIK TEAM AUSTRIA"
fotografen:
- "Roland Enzenhofer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- details/42292
imported:
- "2019"
_4images_image_id: "42292"
_4images_cat_id: "3150"
_4images_user_id: "1688"
_4images_image_date: "2015-11-07T12:46:13"
_4images_image_order: "75"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42292 -->
