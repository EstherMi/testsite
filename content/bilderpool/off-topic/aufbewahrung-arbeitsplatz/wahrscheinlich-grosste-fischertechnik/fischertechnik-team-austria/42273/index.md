---
layout: "image"
title: "Die Mondfähre aus 1969"
date: "2015-11-07T12:46:13"
picture: "fischertechnikteamaustriararitaetenkabinett56.jpg"
weight: "56"
konstrukteure: 
- "FISCHERTECHNIK TEAM AUSTRIA"
fotografen:
- "Roland Enzenhofer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- details/42273
imported:
- "2019"
_4images_image_id: "42273"
_4images_cat_id: "3150"
_4images_user_id: "1688"
_4images_image_date: "2015-11-07T12:46:13"
_4images_image_order: "56"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42273 -->
