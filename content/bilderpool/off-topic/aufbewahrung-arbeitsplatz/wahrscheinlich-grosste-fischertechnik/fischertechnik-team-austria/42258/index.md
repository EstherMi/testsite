---
layout: "image"
title: "Aufbau der Fischertechnik Vitrine"
date: "2015-11-07T12:46:13"
picture: "fischertechnikteamaustriararitaetenkabinett41.jpg"
weight: "41"
konstrukteure: 
- "FISCHERTECHNIK TEAM AUSTRIA"
fotografen:
- "Roland Enzenhofer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- details/42258
imported:
- "2019"
_4images_image_id: "42258"
_4images_cat_id: "3150"
_4images_user_id: "1688"
_4images_image_date: "2015-11-07T12:46:13"
_4images_image_order: "41"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42258 -->
