---
layout: "image"
title: "Economatics Discovery und Smart Box"
date: "2015-11-10T11:36:04"
picture: "economatics17.jpg"
weight: "23"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- details/42373
imported:
- "2019"
_4images_image_id: "42373"
_4images_cat_id: "3152"
_4images_user_id: "1688"
_4images_image_date: "2015-11-10T11:36:04"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42373 -->
