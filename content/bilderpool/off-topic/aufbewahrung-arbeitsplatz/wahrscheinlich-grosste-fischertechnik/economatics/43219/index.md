---
layout: "image"
title: "Fischertechnik PIC Buggy Oberseite"
date: "2016-04-01T21:25:26"
picture: "Buggy_Oberseite_s.jpg"
weight: "2"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/43219
imported:
- "2019"
_4images_image_id: "43219"
_4images_cat_id: "3152"
_4images_user_id: "579"
_4images_image_date: "2016-04-01T21:25:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43219 -->
Fischertechnik PIC Buggy Oberseite

siehe auch Video-Link: 

http://youtu.be/rc8h1piBmmw