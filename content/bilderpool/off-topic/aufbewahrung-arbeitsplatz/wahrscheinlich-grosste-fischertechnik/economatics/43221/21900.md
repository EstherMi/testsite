---
layout: "comment"
hidden: true
title: "21900"
date: "2016-04-02T12:58:50"
uploadBy:
- "uffi"
license: "unknown"
imported:
- "2019"
---
Jau, Jens, hast es richtig gesehen. Rad-Encoder müsste man nachrüsten. Das war halt auf ganz einfachem Niveau gehalten, um Schülern Mikrocontroller näher zu bringen. 

Vier Pins sind noch ungenutzt, man könnte also zwei für die Rad-Encoder und zwei für den Spurensucher verwenden...

Gruß, Dirk