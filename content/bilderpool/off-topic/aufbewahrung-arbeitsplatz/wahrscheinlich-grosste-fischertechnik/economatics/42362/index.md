---
layout: "image"
title: "Economatics PIC-Logicator"
date: "2015-11-10T11:36:03"
picture: "economatics06.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- details/42362
imported:
- "2019"
_4images_image_id: "42362"
_4images_cat_id: "3152"
_4images_user_id: "1688"
_4images_image_date: "2015-11-10T11:36:03"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42362 -->
