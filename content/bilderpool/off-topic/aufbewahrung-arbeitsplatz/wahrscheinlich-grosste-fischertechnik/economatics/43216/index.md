---
layout: "image"
title: "Steuerplatine vom Economatics Fischertechnik PIC Buggy"
date: "2016-03-31T17:25:08"
picture: "FTBUG_top_s.jpg"
weight: "5"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["PIC", "16F628", "Economatics", "Buggy"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/43216
imported:
- "2019"
_4images_image_id: "43216"
_4images_cat_id: "3152"
_4images_user_id: "579"
_4images_image_date: "2016-03-31T17:25:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43216 -->
Hier sieht man die Steuer-Platine des Economatics Fischertechnik PIC Buggy mit dem Mikrocontroller von Microchip PIC 16F628-04/P. Dieser µC läuft mit einem 4 MHz Keramik-Resonator, aus dem eine 1 MHz Instruction Clock abgeleitet wird. Die Performance liegt also nur bei 1 MIPS. 

Als Motortreiber-IC kommt ein L293D zum Einsatz. Die Enable-Eingänge hängen fest verdrahtet an +5V. Eine PWM Steuerung der Motoren ist aber noch per Software über die 4 Inputs des L293D möglich.

Hier der Link zu einem Video des Buggy nach der Belebung mit C-Code. Features:
- PWM Steuerung beider Motoren getrennt in 10 Stufen über Timer2 Interrupts per SW
- Piezo-Lautsprecher mit Tonhöhen-Steuerung
- Sirene zwischen 400 und 600Hz als Warnsignal

http://youtu.be/rc8h1piBmmw