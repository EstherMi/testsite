---
layout: "image"
title: "FT Vitrine"
date: "2015-01-08T17:51:45"
picture: "ftraritaetenundmodelle40.jpg"
weight: "42"
konstrukteure: 
- "Roland Enzenhofer (allsystemgmbh)"
fotografen:
- "Roland Enzenhofer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- details/40249
imported:
- "2019"
_4images_image_id: "40249"
_4images_cat_id: "3021"
_4images_user_id: "1688"
_4images_image_date: "2015-01-08T17:51:45"
_4images_image_order: "40"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40249 -->
Hobby, Silberlinge...