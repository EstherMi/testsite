---
layout: "image"
title: "FT Modelle"
date: "2015-01-08T17:51:45"
picture: "ftraritaetenundmodelle20.jpg"
weight: "22"
konstrukteure: 
- "Roland Enzenhofer (allsystemgmbh)"
fotografen:
- "Roland Enzenhofer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- details/40229
imported:
- "2019"
_4images_image_id: "40229"
_4images_cat_id: "3021"
_4images_user_id: "1688"
_4images_image_date: "2015-01-08T17:51:45"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40229 -->
Modelle aus den 80er Jahren