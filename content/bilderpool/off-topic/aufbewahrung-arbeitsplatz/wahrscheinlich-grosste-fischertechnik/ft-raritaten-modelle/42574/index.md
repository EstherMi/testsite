---
layout: "image"
title: "Pumpstation"
date: "2015-12-27T10:20:57"
picture: "100_1491.jpg"
weight: "2"
konstrukteure: 
- "Fischertechnik"
fotografen:
- "Bernd Langer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- details/42574
imported:
- "2019"
_4images_image_id: "42574"
_4images_cat_id: "3021"
_4images_user_id: "2496"
_4images_image_date: "2015-12-27T10:20:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42574 -->
