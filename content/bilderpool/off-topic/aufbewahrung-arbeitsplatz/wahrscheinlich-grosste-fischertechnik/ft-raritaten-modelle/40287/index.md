---
layout: "image"
title: "FT Serie Eins Junior"
date: "2015-01-08T17:51:45"
picture: "ftraritaetenundmodelle78.jpg"
weight: "80"
konstrukteure: 
- "Roland Enzenhofer (allsystemgmbh)"
fotografen:
- "Roland Enzenhofer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- details/40287
imported:
- "2019"
_4images_image_id: "40287"
_4images_cat_id: "3021"
_4images_user_id: "1688"
_4images_image_date: "2015-01-08T17:51:45"
_4images_image_order: "78"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40287 -->
Sorterbahn SPS gesteuert...