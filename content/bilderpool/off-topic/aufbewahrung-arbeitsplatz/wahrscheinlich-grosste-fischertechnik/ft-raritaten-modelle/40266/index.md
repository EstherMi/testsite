---
layout: "image"
title: "FT Regal"
date: "2015-01-08T17:51:45"
picture: "ftraritaetenundmodelle57.jpg"
weight: "59"
konstrukteure: 
- "Roland Enzenhofer (allsystemgmbh)"
fotografen:
- "Roland Enzenhofer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- details/40266
imported:
- "2019"
_4images_image_id: "40266"
_4images_cat_id: "3021"
_4images_user_id: "1688"
_4images_image_date: "2015-01-08T17:51:45"
_4images_image_order: "57"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40266 -->
Regal aus den 80ern...