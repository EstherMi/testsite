---
layout: "image"
title: "FT Werbemodelle"
date: "2015-01-08T17:51:45"
picture: "ftraritaetenundmodelle19.jpg"
weight: "21"
konstrukteure: 
- "Roland Enzenhofer (allsystemgmbh)"
fotografen:
- "Roland Enzenhofer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- details/40228
imported:
- "2019"
_4images_image_id: "40228"
_4images_cat_id: "3021"
_4images_user_id: "1688"
_4images_image_date: "2015-01-08T17:51:45"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40228 -->
Dübel Renner