---
layout: "comment"
hidden: true
title: "19944"
date: "2015-01-08T19:40:50"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Ist das "das" Modell (das, was im Clubheft fotografiert war), oder ist das ein Nachbau? Und wenn es ein Nachbau ist, ist der original oder nach Augenmaß vom Clubheft erstellt?
Gruß, Stefan