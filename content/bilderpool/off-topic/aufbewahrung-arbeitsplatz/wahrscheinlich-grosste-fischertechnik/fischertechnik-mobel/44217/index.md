---
layout: "image"
title: "FT-Möbel"
date: "2016-08-13T11:30:49"
picture: "fischertechnikmoebel021.jpg"
weight: "21"
konstrukteure: 
- "-?-"
fotografen:
- "Roland Enzenhofer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- details/44217
imported:
- "2019"
_4images_image_id: "44217"
_4images_cat_id: "3267"
_4images_user_id: "1688"
_4images_image_date: "2016-08-13T11:30:49"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44217 -->
Der BBC Buggy