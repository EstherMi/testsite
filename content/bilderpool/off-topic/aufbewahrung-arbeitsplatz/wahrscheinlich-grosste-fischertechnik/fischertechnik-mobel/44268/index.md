---
layout: "image"
title: "FT-Möbel"
date: "2016-08-13T11:30:50"
picture: "fischertechnikmoebel072.jpg"
weight: "72"
konstrukteure: 
- "-?-"
fotografen:
- "Roland Enzenhofer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- details/44268
imported:
- "2019"
_4images_image_id: "44268"
_4images_cat_id: "3267"
_4images_user_id: "1688"
_4images_image_date: "2016-08-13T11:30:50"
_4images_image_order: "72"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44268 -->
Modelle 1966