---
layout: "image"
title: "FT-Möbel"
date: "2016-08-13T11:30:49"
picture: "fischertechnikmoebel001.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Roland Enzenhofer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- details/44197
imported:
- "2019"
_4images_image_id: "44197"
_4images_cat_id: "3267"
_4images_user_id: "1688"
_4images_image_date: "2016-08-13T11:30:49"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44197 -->
Neue Maßmöbel für das Fischertechnik Raritätenkabinett in Österreich