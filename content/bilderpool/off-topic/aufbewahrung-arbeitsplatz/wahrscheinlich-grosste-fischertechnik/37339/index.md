---
layout: "image"
title: "Industrieprogramm 6000/1"
date: "2013-09-08T11:13:31"
picture: "ftmodellsammlung24.jpg"
weight: "24"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- details/37339
imported:
- "2019"
_4images_image_id: "37339"
_4images_cat_id: "2778"
_4images_user_id: "1688"
_4images_image_date: "2013-09-08T11:13:31"
_4images_image_order: "24"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37339 -->
Fischertechnik Industrieprogramm 6000/1