---
layout: "image"
title: "Fischertechnik im Fernsehn"
date: "2014-03-22T10:01:51"
picture: "fischerbeintv4.jpg"
weight: "4"
konstrukteure: 
- "Markus Wolf"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/38479
imported:
- "2019"
_4images_image_id: "38479"
_4images_cat_id: "2871"
_4images_user_id: "968"
_4images_image_date: "2014-03-22T10:01:51"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38479 -->
Regiebesprechung im Wohnzimmer :)