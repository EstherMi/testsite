---
layout: "image"
title: "Fischertechnik Sammlung von Heinz aus Graz"
date: "2016-06-09T11:17:03"
picture: "fischertechnikausoesterreich12.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- details/43722
imported:
- "2019"
_4images_image_id: "43722"
_4images_cat_id: "3238"
_4images_user_id: "1688"
_4images_image_date: "2016-06-09T11:17:03"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43722 -->
Archiv der Sonderklasse