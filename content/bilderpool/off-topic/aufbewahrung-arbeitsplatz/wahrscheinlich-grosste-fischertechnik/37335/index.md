---
layout: "image"
title: "Hobby Kästen"
date: "2013-09-08T11:13:31"
picture: "ftmodellsammlung20.jpg"
weight: "20"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- details/37335
imported:
- "2019"
_4images_image_id: "37335"
_4images_cat_id: "2778"
_4images_user_id: "1688"
_4images_image_date: "2013-09-08T11:13:31"
_4images_image_order: "20"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37335 -->
Hobby Serie komplett unbespielt mit Hobby Labor