---
layout: "image"
title: "Teleskop Mobilkran"
date: "2013-09-08T11:13:31"
picture: "ftmodellsammlung07.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- details/37322
imported:
- "2019"
_4images_image_id: "37322"
_4images_cat_id: "2778"
_4images_user_id: "1688"
_4images_image_date: "2013-09-08T11:13:31"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37322 -->
Teleskop Mobilkran mit Fernsteuerung "der riecht auch noch neu" obwohl auch bereits 30 Jahre alt