---
layout: "image"
title: "Bücherbox"
date: "2013-09-08T11:13:31"
picture: "ftmodellsammlung10.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- details/37325
imported:
- "2019"
_4images_image_id: "37325"
_4images_cat_id: "2778"
_4images_user_id: "1688"
_4images_image_date: "2013-09-08T11:13:31"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37325 -->
Bücherbox mit Raritäten insbesondere dem ganzen Hobbybüchern unbespielt bzw. ungelesen und foliert.