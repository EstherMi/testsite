---
layout: "image"
title: "Roboter-Armee (Top View)"
date: "2015-02-16T17:29:12"
picture: "CIMG9655a.jpg"
weight: "2"
konstrukteure: 
- "Reinhold Schertler"
fotografen:
- "Reinhold Schertler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Striker01"
license: "unknown"
legacy_id:
- details/40548
imported:
- "2019"
_4images_image_id: "40548"
_4images_cat_id: "2848"
_4images_user_id: "1689"
_4images_image_date: "2015-02-16T17:29:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40548 -->
Fischertechnik 30300 Trainings-Roboter 
Fischertechnik 30554 1986 Teach In Roboter 
Fischertechnik 30572 1985 Trainingsroboter