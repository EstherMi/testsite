---
layout: "comment"
hidden: true
title: "18712"
date: "2014-02-15T19:11:57"
uploadBy:
- "allsystemgmbh"
license: "unknown"
imported:
- "2019"
---
Hallo Stefan,

ich habe mit 6 Jahren den ersten FT Kasten100V bekommen danach noch einen 200er und einen 400S  und einige Zusatzkästen.... die sind alle stark bespielt worden und gibt es heute noch...nach 40 Jahren.
Vor einigen Jahren entdeckte ich die Kästen wieder und ich entwickelte den Plan alles was ich mir damals noch so alles an Fischertechnik gewünscht hätte zu kaufen... und wenn möglich NEU bzw. unbespielt.
Mittlerweile umfasst meine Sammlung mehr als ich mir damals gewünscht hätte und stelle manchmal selbstkritisch betrachtet ein gewisses "Fischertechnik Suchtverhalten" fest.
Macht aber nichts... diese Sucht ist nicht schädlich, außer für die Brieftasche ;-) deshalb lass ich mich nicht therapieren und sammle weiter... und mache natürlich auch ab und zu Fotos.

Schöne Grüße aus Österreich
Roland