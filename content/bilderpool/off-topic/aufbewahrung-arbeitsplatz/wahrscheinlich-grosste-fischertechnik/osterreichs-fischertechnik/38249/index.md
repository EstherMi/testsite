---
layout: "image"
title: "Österreichs Fischertechnik 'Raritätenkabinett'"
date: "2014-02-15T13:51:10"
picture: "oesterreichsfischertechnikraritaetenkabinett19.jpg"
weight: "22"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- details/38249
imported:
- "2019"
_4images_image_id: "38249"
_4images_cat_id: "2848"
_4images_user_id: "1688"
_4images_image_date: "2014-02-15T13:51:10"
_4images_image_order: "19"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38249 -->
FT Erste Teilebox für den Handel zum  Verkauf von Einzelteilen...