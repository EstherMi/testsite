---
layout: "image"
title: "schwimmLager79.JPG"
date: "2012-06-28T18:22:51"
picture: "schwimmLager79.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/35086
imported:
- "2019"
_4images_image_id: "35086"
_4images_cat_id: "333"
_4images_user_id: "4"
_4images_image_date: "2012-06-28T18:22:51"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35086 -->
"Schwimmende Lagerung" einmal anders. 

Sommerzeit ist gelbe-ft-Streben-Bruchzeit: der gelbe Kunststoff wird mit der Zeit spröde und bricht. Auf diese Art entstehen ungewollt auch I-Streben 105 mit Loch, wie sie da obenauf liegen. Abhilfe schafft ein ausgiebiges Bad im Wasser. Oder man lagert sie gleich in Wasserbecken: die Magazinboxen sind hinreichend voll gezapft. 

Man gewöhnt sich auch ziemlich schnell an, beim Öffnen und Schließen behutsam vorzugehen.