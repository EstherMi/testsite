---
layout: "image"
title: "Beschriftung"
date: "2007-01-16T21:01:30"
picture: "DSCN1189.jpg"
weight: "7"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/8487
imported:
- "2019"
_4images_image_id: "8487"
_4images_cat_id: "333"
_4images_user_id: "184"
_4images_image_date: "2007-01-16T21:01:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8487 -->
Die Firma pok aus Berlin bietet tolle selbstklebende Einsteckhüllen an.