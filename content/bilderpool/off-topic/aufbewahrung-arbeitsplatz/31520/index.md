---
layout: "image"
title: "Sorteer-oplossing Cees Nobel"
date: "2010-11-06T23:40:15"
picture: "fischertechnikbijeenkomstschoonhovennov75.jpg"
weight: "6"
konstrukteure: 
- "Cees Nobel"
fotografen:
- "peterholland"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/31520
imported:
- "2019"
_4images_image_id: "31520"
_4images_cat_id: "333"
_4images_user_id: "22"
_4images_image_date: "2010-11-06T23:40:15"
_4images_image_order: "75"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31520 -->
Sorteer-oplossing Cees Nobel (Schoonhoven 2010)