---
layout: "image"
title: "Strom"
date: "2007-02-11T15:18:39"
picture: "magier6.jpg"
weight: "6"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/8944
imported:
- "2019"
_4images_image_id: "8944"
_4images_cat_id: "759"
_4images_user_id: "445"
_4images_image_date: "2007-02-11T15:18:39"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8944 -->
Drei mal. Einmal wechsel 4,5V, einmal grauer Trafo, einmal das neue Netzgerät.