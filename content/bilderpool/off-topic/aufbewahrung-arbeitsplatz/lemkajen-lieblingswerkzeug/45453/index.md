---
layout: "image"
title: "Mein Lieblingswerkzeug zum ft - Bauen"
date: "2017-03-05T19:28:27"
picture: "IMG_1102_2.jpg"
weight: "2"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/45453
imported:
- "2019"
_4images_image_id: "45453"
_4images_cat_id: "3380"
_4images_user_id: "1359"
_4images_image_date: "2017-03-05T19:28:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45453 -->
