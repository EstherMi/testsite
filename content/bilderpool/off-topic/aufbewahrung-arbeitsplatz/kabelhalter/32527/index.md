---
layout: "image"
title: "Detailansicht"
date: "2011-09-26T00:22:25"
picture: "kabelhaltermasked2.jpg"
weight: "6"
konstrukteure: 
- "Martin W. (Masked)"
fotografen:
- "Martin W. (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/32527
imported:
- "2019"
_4images_image_id: "32527"
_4images_cat_id: "2358"
_4images_user_id: "373"
_4images_image_date: "2011-09-26T00:22:25"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=32527 -->
Bodenplatte 30x90 (hier 32861) und 4 Bausteine 30, schon fertig. Der Abstand zwischen den BS entspricht genau einer quer eingebauten Federnocke, kann man auch weglassen.

Naja, und doppelseitiges Klebeband zum Befestigen am Schrank.