---
layout: "image"
title: "Kabelhalter 2"
date: "2011-08-20T14:58:02"
picture: "Kabelhalter_2.jpg"
weight: "8"
konstrukteure: 
- "thomas004"
fotografen:
- "thomas004"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "thomas004"
license: "unknown"
legacy_id:
- details/31635
imported:
- "2019"
_4images_image_id: "31635"
_4images_cat_id: "2358"
_4images_user_id: "328"
_4images_image_date: "2011-08-20T14:58:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31635 -->
