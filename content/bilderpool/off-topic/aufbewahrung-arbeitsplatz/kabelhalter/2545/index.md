---
layout: "image"
title: "Kabelsalat1.jpg"
date: "2004-07-13T13:26:02"
picture: "Kabelsalat1.jpg"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2545
imported:
- "2019"
_4images_image_id: "2545"
_4images_cat_id: "2358"
_4images_user_id: "4"
_4images_image_date: "2004-07-13T13:26:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2545 -->
ist zwar kein Klapparatismus, aber trotzdem nützlich:

Der Schnitt einer Stichsäge ist gerade so breit, dass ein ft-Kabel prima hineinpasst.