---
layout: "image"
title: "Kabelaufbewahrung"
date: "2011-12-27T15:29:10"
picture: "kabelaufbewahrung1.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/33812
imported:
- "2019"
_4images_image_id: "33812"
_4images_cat_id: "2358"
_4images_user_id: "104"
_4images_image_date: "2011-12-27T15:29:10"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33812 -->
Ich glaube, ich habe endlich eine wirklich praktische Möglichkeit gefunden, Kabel aufzubewahren. Dieses Bild zeigt sämtliche meine einadrigen Leitungen (die zweiadringen kommen vielleicht auch noch dran). Die Grundidee ist, die Kabel an beiden Enden (locker) einzuspannen, so dass man...

- sofort sieht von welcher Länge man noch was hat,

- das gewünschte Kabel ohne jeden Kabelsalat schnell entnehmen und vor allem

- beim Abbau eines Modells auch sofort wieder an die richtige Stelle zurücklegen kann (man muss das Kabel ja nur kurz gegenhalten, um zu sehen, wohin es gehört.

- Außerdem passen diese beiden Platten so auch noch in eine der doppelt hohen Schubladen meiner Ikea "Alex" Schränke.

Man braucht allerdings, wie man sieht, etwas Material. Das ist aber in meinem Fall aus folgenden Gründen überhaupt nicht schlimm - im Gegenteil!

a) Die beiden Großbauplatten sind die aus der eh eher ungeliebten "Zapfenkiller"-Serie, die ich eh nur im Notfall verbauen würde.

b) Die vielen grauen Grundbausteine sind jahrzehntealte mit ausgeleiertem Zapfen, die man eh kaum noch zum Bauen verwenden konnte. Davon habe ich eine ganze Menge, aber ich hab es nie übers Herz gebracht, sie wegzuschmeißen. Hier haben sie doch noch einen sinnvollen Einsatzzweck gefunden.

c) Dadurch wurde in meinen Schubladen Platz frei für die im letzten Jahr hinzugekommenen neuen Bausteine, und ich muss hoffentlich nicht meine ganze Einsortierung über den Haufen werfen.