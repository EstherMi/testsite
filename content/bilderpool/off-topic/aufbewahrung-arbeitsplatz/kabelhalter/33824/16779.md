---
layout: "comment"
hidden: true
title: "16779"
date: "2012-04-25T23:44:58"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Miteinander,
könnte es sein, daß Endlich hier die Tüftler etwas vorgeführt hat? :o)
Aber jetzt mal etwas Spaß im Ernst:
In der Konstruktionsmethodik gibt es dazu zwei hilfreiche Algorithmen:
1. Warum kompliziert, wenn es auch einfach geht.
2. Eine gute Konstruktion ist so billig wie möglich.
Natürlich hat man mit ft-Teilen schnell was zusammengesteckt, was sich aber nur ft-Fans leisten können, die dazu ft-Teile "übrig" haben ....
Die Pappenlösung läßt sich ja noch weiterentwickeln, z.B. mit Wellpappe. Also sollten wir Endlich dazu etwas Mut machen.
Gruß, Udo2