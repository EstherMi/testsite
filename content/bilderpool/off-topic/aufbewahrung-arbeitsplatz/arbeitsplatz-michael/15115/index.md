---
layout: "image"
title: "Arbeitsplatz (4)"
date: "2008-08-27T21:17:54"
picture: "Arbeitsplatz_4.jpg"
weight: "5"
konstrukteure: 
- "-Michael-"
fotografen:
- "-Michael-"
keywords: ["-Michael-", "Michael", "Arbeitsplatz"]
uploadBy: "-Michael-"
license: "unknown"
legacy_id:
- details/15115
imported:
- "2019"
_4images_image_id: "15115"
_4images_cat_id: "1377"
_4images_user_id: "820"
_4images_image_date: "2008-08-27T21:17:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15115 -->
Schachtel 1