---
layout: "image"
title: "Metallachsen"
date: "2014-05-18T19:01:36"
picture: "meinarbeitsundlagerplatz11.jpg"
weight: "11"
konstrukteure: 
- "MartinB"
fotografen:
- "MartinB"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MartinB"
license: "unknown"
legacy_id:
- details/38784
imported:
- "2019"
_4images_image_id: "38784"
_4images_cat_id: "2899"
_4images_user_id: "1696"
_4images_image_date: "2014-05-18T19:01:36"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38784 -->
meine kompletten Metallachsen