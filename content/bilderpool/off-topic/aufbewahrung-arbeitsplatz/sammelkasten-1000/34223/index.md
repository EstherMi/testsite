---
layout: "image"
title: "so ist es"
date: "2012-02-18T13:28:18"
picture: "mit.jpg"
weight: "1"
konstrukteure: 
- "SkobyMobil"
fotografen:
- "SkobyMobil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "SkobyMobil"
license: "unknown"
legacy_id:
- details/34223
imported:
- "2019"
_4images_image_id: "34223"
_4images_cat_id: "2535"
_4images_user_id: "1407"
_4images_image_date: "2012-02-18T13:28:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34223 -->
so sehen unsere Sammelkasten eigentlich aus. Nur wohin mit den langen Bauteilen?