---
layout: "image"
title: "Umbau 06"
date: "2009-09-08T21:04:52"
picture: "umbauboxfuerdiebrennstoffzelle6.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- details/24896
imported:
- "2019"
_4images_image_id: "24896"
_4images_cat_id: "1714"
_4images_user_id: "895"
_4images_image_date: "2009-09-08T21:04:52"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24896 -->
So sieht jetzt das Aufbewahrungssystem aus, wenn es mit den Teilen aus dem "Profi Hydro Cell Kit" bestückt ist.

Im mittleren, linken Fach sieht man ein Solarmodul, ft# 136239, das mit der schon beschriebenen Klebeschablone bearbeitetet wurde.

