---
layout: "image"
title: "Umbau 05"
date: "2009-09-08T21:04:52"
picture: "umbauboxfuerdiebrennstoffzelle5.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- details/24895
imported:
- "2019"
_4images_image_id: "24895"
_4images_cat_id: "1714"
_4images_user_id: "895"
_4images_image_date: "2009-09-08T21:04:52"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24895 -->
Die Brennstoffzelle kann jetzt von unten in die bearbeitete Box 3 hineinragen.