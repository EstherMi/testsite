---
layout: "image"
title: "Vorrat 3"
date: "2011-12-25T14:16:51"
picture: "ftbaubereich29.jpg"
weight: "29"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/33783
imported:
- "2019"
_4images_image_id: "33783"
_4images_cat_id: "2497"
_4images_user_id: "1"
_4images_image_date: "2011-12-25T14:16:51"
_4images_image_order: "29"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33783 -->
Ein Vorrat an verschweißten Tütchen für alle Fälle.