---
layout: "image"
title: "Vorrat 2"
date: "2011-12-25T14:16:51"
picture: "ftbaubereich28.jpg"
weight: "28"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/33782
imported:
- "2019"
_4images_image_id: "33782"
_4images_cat_id: "2497"
_4images_user_id: "1"
_4images_image_date: "2011-12-25T14:16:51"
_4images_image_order: "28"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33782 -->
Ein Vorrat an verschweißten Tütchen für alle Fälle.