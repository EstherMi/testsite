---
layout: "image"
title: "weitere Interfaces"
date: "2011-12-25T14:16:51"
picture: "ftbaubereich43.jpg"
weight: "43"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/33797
imported:
- "2019"
_4images_image_id: "33797"
_4images_cat_id: "2497"
_4images_user_id: "1"
_4images_image_date: "2011-12-25T14:16:51"
_4images_image_order: "43"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33797 -->
parallel, C64 oder Intelligent Interfaces + Intelligent Extension