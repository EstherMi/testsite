---
layout: "image"
title: "Das Alu-Lager"
date: "2011-12-25T14:16:51"
picture: "ftbaubereich18.jpg"
weight: "18"
konstrukteure: 
- "Sven Engelke"
fotografen:
- "Sven Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/33772
imported:
- "2019"
_4images_image_id: "33772"
_4images_cat_id: "2497"
_4images_user_id: "1"
_4images_image_date: "2011-12-25T14:16:51"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33772 -->
In dieser Schublade befindet sich das Alu-Lager.
Wie man sieht, Aluprofile in den verschiedensten Längen.
