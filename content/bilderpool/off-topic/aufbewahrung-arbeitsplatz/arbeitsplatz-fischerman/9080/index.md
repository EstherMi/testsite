---
layout: "image"
title: "Arbeitsplatz 2"
date: "2007-02-19T21:26:33"
picture: "Bild14.jpg"
weight: "35"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- details/9080
imported:
- "2019"
_4images_image_id: "9080"
_4images_cat_id: "826"
_4images_user_id: "426"
_4images_image_date: "2007-02-19T21:26:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9080 -->
Pentium III 700 Mhz aufgepeppt auf 800 Mhz für PSP aktivitäten und was noch so an Software geprüft wird bevor es auf einen meiner beiden besten PC´s drauf kommt!