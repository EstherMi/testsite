---
layout: "image"
title: "Rundblick der Werkstatt Bild 5"
date: "2007-02-19T21:26:40"
picture: "Rundblick23.jpg"
weight: "44"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- details/9089
imported:
- "2019"
_4images_image_id: "9089"
_4images_cat_id: "826"
_4images_user_id: "426"
_4images_image_date: "2007-02-19T21:26:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9089 -->
