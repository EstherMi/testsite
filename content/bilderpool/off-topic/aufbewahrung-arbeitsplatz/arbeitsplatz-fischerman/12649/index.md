---
layout: "image"
title: "Mein ganzer stolz"
date: "2007-11-11T08:33:13"
picture: "Nahaufnahme_Bauteile_LED_Koffer1.jpg"
weight: "9"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- details/12649
imported:
- "2019"
_4images_image_id: "12649"
_4images_cat_id: "826"
_4images_user_id: "426"
_4images_image_date: "2007-11-11T08:33:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12649 -->
Das ist der braune Koffer den ihr gerade gesehen habt. Hier drinne denke ich befinden sich genügend LED´s um alle hier im Forum umzurüsten :-)