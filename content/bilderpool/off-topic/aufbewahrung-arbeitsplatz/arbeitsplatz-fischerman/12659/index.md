---
layout: "image"
title: "und auch unter dem Tisch vom Messplatz"
date: "2007-11-11T08:33:14"
picture: "Arbeitsplatz_unter_Oszi.jpg"
weight: "19"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- details/12659
imported:
- "2019"
_4images_image_id: "12659"
_4images_cat_id: "826"
_4images_user_id: "426"
_4images_image_date: "2007-11-11T08:33:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12659 -->
ist nicht mehr so viel Platz.

Jede menge Bauteile