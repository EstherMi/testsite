---
layout: "image"
title: "ft-Kanister.jpg"
date: "2009-05-04T18:32:12"
picture: "ft-Kanister.jpg"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/23836
imported:
- "2019"
_4images_image_id: "23836"
_4images_cat_id: "1679"
_4images_user_id: "4"
_4images_image_date: "2009-05-04T18:32:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23836 -->
Es soll ja Leute geben, bei denen die ft-Motoren Schüttgut sind. Ganz soviel Zeugs hab ich nicht, aber ein paar Teilenummern kommen doch auf größere Stückzahlen.