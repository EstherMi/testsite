---
layout: "image"
title: "6er Einsatz"
date: "2011-07-05T21:10:09"
picture: "einsatz2.jpg"
weight: "2"
konstrukteure: 
- "Tobias Horst"
fotografen:
- "Tobias Horst"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- details/30989
imported:
- "2019"
_4images_image_id: "30989"
_4images_cat_id: "2315"
_4images_user_id: "1007"
_4images_image_date: "2011-07-05T21:10:09"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30989 -->
