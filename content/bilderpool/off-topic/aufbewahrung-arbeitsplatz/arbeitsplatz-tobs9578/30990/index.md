---
layout: "image"
title: "8er Einsatz"
date: "2011-07-05T21:10:09"
picture: "einsatz3.jpg"
weight: "3"
konstrukteure: 
- "Tobias Horst"
fotografen:
- "Tobias Horst"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tobs9578"
license: "unknown"
legacy_id:
- details/30990
imported:
- "2019"
_4images_image_id: "30990"
_4images_cat_id: "2315"
_4images_user_id: "1007"
_4images_image_date: "2011-07-05T21:10:09"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30990 -->
