---
layout: "image"
title: "FT Tabletts"
date: "2018-04-25T21:47:00"
picture: "fttabletts1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/47468
imported:
- "2019"
_4images_image_id: "47468"
_4images_cat_id: "3507"
_4images_user_id: "968"
_4images_image_date: "2018-04-25T21:47:00"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47468 -->
Seiten und Griffe Kiefer, Boden 6mm Birkensperrholz,
lackiert.