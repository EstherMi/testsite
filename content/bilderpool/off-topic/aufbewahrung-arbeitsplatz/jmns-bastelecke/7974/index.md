---
layout: "image"
title: "Ubersicht 2"
date: "2006-12-20T00:23:02"
picture: "jmnsbastelecke2.jpg"
weight: "2"
konstrukteure: 
- "JMN"
fotografen:
- "JMN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/7974
imported:
- "2019"
_4images_image_id: "7974"
_4images_cat_id: "744"
_4images_user_id: "162"
_4images_image_date: "2006-12-20T00:23:02"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7974 -->
Alle Schubladen mit den Faltkran und Muhlauto daneben drauf gestellt.