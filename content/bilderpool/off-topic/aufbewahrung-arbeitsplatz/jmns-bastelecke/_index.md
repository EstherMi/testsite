---
layout: "overview"
title: "JMNs Bastelecke"
date: 2019-12-17T18:07:23+01:00
legacy_id:
- categories/744
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=744 --> 
Hier seht man einige Bilder von mein Arbeitsplatz. Wir sind grade umgezogen und dort gibts entlich ein Zimmer frei fur fischertechnik.[br]Alles ist in Schubladen eingepackt. Dieses system ist von PAS+ und kann immer grösser gebaut werden. Leider sind nicht alle Schubladen in die gleiche Farben, aber das ist eigentlich nicht das Problem. Einige Schubladen sind noch leer, aber einige sind jetzt noch zu voll. Ich muss das alles nochmal besser einpacken.