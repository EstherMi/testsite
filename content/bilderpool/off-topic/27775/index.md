---
layout: "image"
title: "Instructables - Gift Exchange"
date: "2010-07-25T09:08:22"
picture: "CIMG1328.jpg"
weight: "13"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Instructables", "Scorpion"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/27775
imported:
- "2019"
_4images_image_id: "27775"
_4images_cat_id: "312"
_4images_user_id: "585"
_4images_image_date: "2010-07-25T09:08:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27775 -->
This is the original kit I mailed off today as part of the Instructables Author Gift Exchange. I hope the recipient likes it!