---
layout: "image"
title: "Amelia and the Crane"
date: "2008-05-17T10:09:59"
picture: "amelia_crane2.jpg"
weight: "33"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["Crane", "96778", "Cranes"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/14541
imported:
- "2019"
_4images_image_id: "14541"
_4images_cat_id: "312"
_4images_user_id: "585"
_4images_image_date: "2008-05-17T10:09:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14541 -->
My daughter constructing a crane from #96778. (Just got an Asus machine and I am testing it out).

google translation:
Meine Tochter den Bau eines Krans aus # 96778. (Gerade ein Asus Maschine und ich bin Test it out).