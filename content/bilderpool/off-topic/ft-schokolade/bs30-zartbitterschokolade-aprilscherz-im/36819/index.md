---
layout: "image"
title: "Schoko-BS30 (1)"
date: "2013-04-02T15:32:13"
picture: "P1070850_verkleinert.jpg"
weight: "1"
konstrukteure: 
- "bflehner"
fotografen:
- "bflehner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Bernhard Lehner"
license: "unknown"
legacy_id:
- details/36819
imported:
- "2019"
_4images_image_id: "36819"
_4images_cat_id: "2733"
_4images_user_id: "1028"
_4images_image_date: "2013-04-02T15:32:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36819 -->
Die Gussform mit Deckel, die Schokolade ist schon einige Zeit angetrocknet.