---
layout: "image"
title: "Schoko-BS30 (3)"
date: "2013-04-02T15:32:14"
picture: "P1070853_verkleinert.jpg"
weight: "3"
konstrukteure: 
- "bflehner"
fotografen:
- "bflehner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Bernhard Lehner"
license: "unknown"
legacy_id:
- details/36821
imported:
- "2019"
_4images_image_id: "36821"
_4images_cat_id: "2733"
_4images_user_id: "1028"
_4images_image_date: "2013-04-02T15:32:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36821 -->
Ohne Boden.