---
layout: "image"
title: "Versuch die Schraube zu gefrieren"
date: "2011-01-29T22:57:02"
picture: "peltierelement6.jpg"
weight: "6"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/29824
imported:
- "2019"
_4images_image_id: "29824"
_4images_cat_id: "2195"
_4images_user_id: "558"
_4images_image_date: "2011-01-29T22:57:02"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29824 -->
