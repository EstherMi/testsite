---
layout: "image"
title: "Verschiedene gefrorene Sachen"
date: "2011-01-29T22:57:02"
picture: "peltierelement1.jpg"
weight: "1"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/29819
imported:
- "2019"
_4images_image_id: "29819"
_4images_cat_id: "2195"
_4images_user_id: "558"
_4images_image_date: "2011-01-29T22:57:02"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29819 -->
