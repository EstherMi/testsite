---
layout: "image"
title: "Ohne Schraube"
date: "2011-01-29T22:57:02"
picture: "peltierelement7.jpg"
weight: "7"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/29825
imported:
- "2019"
_4images_image_id: "29825"
_4images_cat_id: "2195"
_4images_user_id: "558"
_4images_image_date: "2011-01-29T22:57:02"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29825 -->
