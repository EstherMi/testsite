---
layout: "image"
title: "2009 Bra Project"
date: "2009-10-19T19:04:24"
picture: "sm_ft_bra_3.jpg"
weight: "21"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["2009", "Bra", "Project"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/25562
imported:
- "2019"
_4images_image_id: "25562"
_4images_cat_id: "312"
_4images_user_id: "585"
_4images_image_date: "2009-10-19T19:04:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25562 -->
This is Joe modelling my entry for the 2009 Bra Project. Thought to share.