---
layout: "image"
title: "Softbox mit Stativ"
date: "2018-11-06T11:03:21"
picture: "fotografieren15.jpg"
weight: "15"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/48385
imported:
- "2019"
_4images_image_id: "48385"
_4images_cat_id: "3544"
_4images_user_id: "2303"
_4images_image_date: "2018-11-06T11:03:21"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48385 -->
