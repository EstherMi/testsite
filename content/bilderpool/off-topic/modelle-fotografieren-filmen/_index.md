---
layout: "overview"
title: "Modelle fotografieren und filmen"
date: 2019-12-17T18:10:37+01:00
legacy_id:
- categories/3544
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3544 --> 
Hallo fischertechniker,

heute möchte ich euch meine Foto- und Filmecke vorstellen.

Da ich, wie viele von euch, im Bastelzimmer immer ein Platzproblem habe, möchte ich euch hier meine Lösung vorstellen.
Der Vorteil dieser Lösung ist, das die Foto- und Filmecke, schnell aufgebaut ist. Ansonsten dient der Platz zum Abstellen der
fertigen Modelle. 