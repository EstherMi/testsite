---
layout: "image"
title: "Kamerahalterung mit Gegenstück auf der Kamera"
date: "2018-11-06T11:03:28"
picture: "fotografieren21.jpg"
weight: "21"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/48391
imported:
- "2019"
_4images_image_id: "48391"
_4images_cat_id: "3544"
_4images_user_id: "2303"
_4images_image_date: "2018-11-06T11:03:28"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48391 -->
