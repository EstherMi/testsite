---
layout: "image"
title: "LED-Panel"
date: "2018-11-06T11:03:10"
picture: "fotografieren07.jpg"
weight: "7"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/48377
imported:
- "2019"
_4images_image_id: "48377"
_4images_cat_id: "3544"
_4images_user_id: "2303"
_4images_image_date: "2018-11-06T11:03:10"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48377 -->
Das LED-Panel ist von der Firma Anten. Es ist 30x120 cm groß.

