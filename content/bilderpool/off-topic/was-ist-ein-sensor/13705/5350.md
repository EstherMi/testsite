---
layout: "comment"
hidden: true
title: "5350"
date: "2008-02-19T19:39:54"
uploadBy:
- "FtfanClub"
license: "unknown"
imported:
- "2019"
---
Das ist ein Initiator...schweineteuer !

In der Regel reagiert er auf alle Metalle, die im Abstand bis zu 1, 2cm, vorbeigeführt werden. Halte mal eine 4mm Schraube dran, die kannst Du dann verbauen und schon kannst Du metallisches von Kunsstoff sortieren. Wenn Du ihn nicht brauchst, her damit :-)))

Wenn Du ein Datenblatt brauchst, scanne ich dir gerne. Du kannst ihn wie ein Fotosensor anschließen

Gruß Micha