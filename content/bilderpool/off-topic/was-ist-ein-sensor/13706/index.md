---
layout: "image"
title: "Das Teil ist in Kunstharz eingegossen."
date: "2008-02-19T17:20:51"
picture: "wasistdasfuereinsensor2.jpg"
weight: "3"
konstrukteure: 
- "Fischertechnik"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/13706
imported:
- "2019"
_4images_image_id: "13706"
_4images_cat_id: "1261"
_4images_user_id: "558"
_4images_image_date: "2008-02-19T17:20:51"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13706 -->
