---
layout: "image"
title: "Nochmal von Oben"
date: "2008-02-19T17:20:51"
picture: "wasistdasfuereinsensor4.jpg"
weight: "5"
konstrukteure: 
- "Fischertechnik"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/13708
imported:
- "2019"
_4images_image_id: "13708"
_4images_cat_id: "1261"
_4images_user_id: "558"
_4images_image_date: "2008-02-19T17:20:51"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13708 -->
