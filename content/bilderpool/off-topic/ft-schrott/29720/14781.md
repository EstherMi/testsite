---
layout: "comment"
hidden: true
title: "14781"
date: "2011-08-03T16:50:31"
uploadBy:
- "heiko"
license: "unknown"
imported:
- "2019"
---
Sekundenkleber funktioniert. Ich habe jedenfalls ein paarmal Sensoren an ft-Bausteine damit festgeklebt -- vermutlich kann man damit auch ft-Bausteine untereinander verbinden. Sicherlich reizvoll für die Stellen, an denen man par tout keine stabile Verbindung hinbekommt.

Wenn man's geschickt anstellt, ist das so entstandene 'Modul' sogar wiederverwendbar ... wieso nicht?