---
layout: "image"
title: "Schablone 02"
date: "2009-08-09T23:39:21"
picture: "klebe2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- details/24724
imported:
- "2019"
_4images_image_id: "24724"
_4images_cat_id: "1699"
_4images_user_id: "895"
_4images_image_date: "2009-08-09T23:39:21"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24724 -->
Aus den Teilen:

12 Stück Baustein 30, z.B. ft# 31003
 8 Stück Baustein 5, z.B. ft# 37237
 6 Stück Federnocken, z.B. ft# 31982

lässt sich eine Klebeschablone erstellen, die ein massgenaues Aufkleben 
der Bauplatten 15x60 mit 4 Zapfen ermöglicht.