---
layout: "image"
title: "Schablone 05"
date: "2009-08-09T23:39:21"
picture: "klebe5.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- details/24727
imported:
- "2019"
_4images_image_id: "24727"
_4images_cat_id: "1699"
_4images_user_id: "895"
_4images_image_date: "2009-08-09T23:39:21"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24727 -->
Die beiden Halbzeuge, ich nenne sie "Stempel", werden wie die 
Abbildung zeigt, mit den 6 Federnocken versehen.

Der vordere Stempel hat nur aussen zwei Federnocken.