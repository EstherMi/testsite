---
layout: "image"
title: "Schablone 06"
date: "2009-08-09T23:39:21"
picture: "klebe6.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- details/24728
imported:
- "2019"
_4images_image_id: "24728"
_4images_cat_id: "1699"
_4images_user_id: "895"
_4images_image_date: "2009-08-09T23:39:21"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24728 -->
Danach werden die beiden Stempel auf die beiden, von dem breiten 
Abdeckstreifen befreiten, Klebestreifen aufgedrückt.

Der vordere Stempel ist bereits mit dem Klebestreifen bestückt.