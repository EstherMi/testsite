---
layout: "image"
title: "ft tshirt (ft Princess)"
date: "2007-12-20T17:36:16"
picture: "tshirt_a.jpg"
weight: "35"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["t-shirt", "ft", "princess"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/13119
imported:
- "2019"
_4images_image_id: "13119"
_4images_cat_id: "312"
_4images_user_id: "585"
_4images_image_date: "2007-12-20T17:36:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13119 -->
This is a t-shirt designed by Laura (and posted on Zazzle.com). It is being modelled by my daughter Amelia. She is a ft princess! ( google translation- Dies ist ein T-Shirt entworfen von Laura (und auf Zazzle.com). Es wird nach dem Vorbild meiner Tochter Amelia. Sie ist eine m Prinzessin.)