---
layout: "image"
title: "Nahaufnahme"
date: "2010-09-19T18:19:46"
picture: "loetzubehoer2.jpg"
weight: "2"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/28193
imported:
- "2019"
_4images_image_id: "28193"
_4images_cat_id: "2045"
_4images_user_id: "104"
_4images_image_date: "2010-09-19T18:19:46"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28193 -->
Um alle Lötprofis erschaudern zu lassen (ich hatte seit 30 Jahren oder so keinen Lötkolben mehr benutzt).