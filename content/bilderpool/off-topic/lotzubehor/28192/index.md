---
layout: "image"
title: "Dritte Hand"
date: "2010-09-19T18:19:45"
picture: "loetzubehoer1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/28192
imported:
- "2019"
_4images_image_id: "28192"
_4images_cat_id: "2045"
_4images_user_id: "104"
_4images_image_date: "2010-09-19T18:19:45"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28192 -->
Um ft-Kabel an Buchsen zu löten, habe ich mir das hier als dritte Hand gebaut. Die vier Metallachsen halten die Buchse umso fester, je weiter die Kette in Richtung der Gelenksteine geschoben wird. Auf jeden Fall ist das weit genug, so dass die Wärme nicht bis zur Kette gelangt. Die Kontaktfläche zwischen der Buchse und den Achsen ist minimal.