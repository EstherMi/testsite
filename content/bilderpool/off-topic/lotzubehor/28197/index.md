---
layout: "image"
title: "Um dieses Netzteil geht's"
date: "2010-09-19T18:19:47"
picture: "loetzubehoer6.jpg"
weight: "6"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/28197
imported:
- "2019"
_4images_image_id: "28197"
_4images_cat_id: "2045"
_4images_user_id: "104"
_4images_image_date: "2010-09-19T18:19:47"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28197 -->
Diese Netzteile hat mir thkais mal empfohlen, um das RoboInt und I/O-Extensions zu betreiben. Die können u.a. 9 V liefern, regeln bei 1 A ab, die Polarität ist einstellbar, und sie haben verschiedene zweipolige Rundstecker mitgeliefert, von denen einer genau ins RoboInt passt. Klein und flach sind sie auch, was will man mehr.