---
layout: "image"
title: "Zange"
date: "2010-09-19T18:19:46"
picture: "loetzubehoer4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/28195
imported:
- "2019"
_4images_image_id: "28195"
_4images_cat_id: "2045"
_4images_user_id: "104"
_4images_image_date: "2010-09-19T18:19:46"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28195 -->
Um die Zugentlastung zu zu klemmen, verwende ich diese Zange. 2 * 2 Kettenglieder ergeben ein Gelenk (diese Idee wurde schon in einem ganz frühen Clubheft vorgestellt).