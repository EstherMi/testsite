---
layout: "image"
title: "Modellflug Banner"
date: "2017-01-30T20:18:01"
picture: "modellflugbanner1.jpg"
weight: "1"
konstrukteure: 
- "Hubert Reddeker"
fotografen:
- "Hubert Reddeker"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/45110
imported:
- "2019"
_4images_image_id: "45110"
_4images_cat_id: "3360"
_4images_user_id: "968"
_4images_image_date: "2017-01-30T20:18:01"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45110 -->
Solche Sachen findet man beim stöbern in den Dias aus der Kindheit.
Dieses Foto entstand 1973.
Da verfügte klein Markus, zehnjährig, schon über 3 Jahre Ft Erfahrung.
Ich bin es übrigens selbst, der da sitzt.
Der Flieger, ein" Udet Flamingo" meines Vaters war für Bannerschlepp ausgelegt.
Da kamen dann in der Regel selbstgemachte Werbebanner der Paderborner oder Herforder Brauerei
dran und wurde beim jährlichen Flugtag vorgeführt. Das wurde dann wiederum   von den Brauereien
mit Kistenbier gesponsort.
Das FT Banner war natürlich ein Exot.... aber FT war damals auch schon ein Begriff.