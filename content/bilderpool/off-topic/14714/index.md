---
layout: "image"
title: "Fotoplatine"
date: "2008-06-20T16:07:13"
picture: "Fotoplatine.jpg"
weight: "29"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/14714
imported:
- "2019"
_4images_image_id: "14714"
_4images_cat_id: "312"
_4images_user_id: "456"
_4images_image_date: "2008-06-20T16:07:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14714 -->
Hallo,
wie einige vielleicht schon wissen stelle ich meine Leiterplatten nun auch selbst her. Allerdings geschah das immer mit der Bügelmethode. Nun habe ich heute das erste mal eine Platine belichtet und entwickelt (viel bessere Qualität!) und nun auch geätzt. Bzw ich habe damit angefangen. Nun will und will das Kupfer nicht verschwinden. An den Rändern ist das schon passiert, aber mehr nicht. Ich habe das nun schon seit Stunden probiert, sogar das Ätzwasser erneuert....
Was habe ich falsch gemacht? Wieso klappt das nicht?
Hoffe ihr könnt helfen.