---
layout: "image"
title: "Pixy-Camera"
date: "2016-07-19T15:45:56"
picture: "500600_Pixy-Camera.jpg"
weight: "14"
konstrukteure: 
- "Dirk Wölffel"
fotografen:
- "Dirk Wölffel"
keywords: ["ftdesigner", "Pixy", "Pixy-Camera", "Kamera"]
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/43908
imported:
- "2019"
_4images_image_id: "43908"
_4images_cat_id: "843"
_4images_user_id: "2303"
_4images_image_date: "2016-07-19T15:45:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43908 -->
Bauteil für ftdesigner aus Downloaddatei ftdesigner_bauteile_teil_2

Download unter:
https://ftcommunity.de/data/downloads/ftdesignerdateien/ftdesigner_bauteile_teil_2.zip