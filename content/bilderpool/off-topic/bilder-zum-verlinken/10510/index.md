---
layout: "image"
title: "Schrittmotor"
date: "2007-05-25T21:23:34"
picture: "Schrittmotor3.jpg"
weight: "87"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/10510
imported:
- "2019"
_4images_image_id: "10510"
_4images_cat_id: "843"
_4images_user_id: "456"
_4images_image_date: "2007-05-25T21:23:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10510 -->
Die 4 Anschlüsse.