---
layout: "overview"
title: "ft Electronics App"
date: 2019-12-17T18:09:55+01:00
legacy_id:
- categories/3184
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3184 --> 
Bildschirmfotos einer in Arbeit befindlichen App für Windows 8.1 oder höher und Windows Phone 8.1 oder höher. Die App bietet ein einfach zu benutzendes Nachschlagewerk über die Funktionen und DIP-Stellungen der E-Tec-, Electronics- und MiniBots-Moduln.