---
layout: "image"
title: "Robo Pro Programm zur Fernsteuerung eines Roboters"
date: "2015-01-05T16:39:32"
picture: "RoboProProgramm_fertig.png"
weight: "22"
konstrukteure: 
- "robbi2011"
fotografen:
- "robbi2011s PC"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "robbi2011"
license: "unknown"
legacy_id:
- details/40188
imported:
- "2019"
_4images_image_id: "40188"
_4images_cat_id: "843"
_4images_user_id: "1442"
_4images_image_date: "2015-01-05T16:39:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40188 -->
Programm zur Fersteuerung (mittels IR-Fernbedienung) eines Roboters.