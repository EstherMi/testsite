---
layout: "image"
title: "cfw BenoiTxt - Bild 6"
date: "2017-02-17T16:01:01"
picture: "BenoiTxt08.jpeg"
weight: "8"
konstrukteure: 
- "Peter Habermehl"
fotografen:
- "Peter Habermehl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- details/45256
imported:
- "2019"
_4images_image_id: "45256"
_4images_cat_id: "3369"
_4images_user_id: "2488"
_4images_image_date: "2017-02-17T16:01:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45256 -->
