---
layout: "image"
title: "BenoiTxt - Start"
date: "2017-02-17T16:01:01"
picture: "BenoiTxt01.jpg"
weight: "1"
konstrukteure: 
- "Peter Habermehl"
fotografen:
- "Peter Habermehl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- details/45249
imported:
- "2019"
_4images_image_id: "45249"
_4images_cat_id: "3369"
_4images_user_id: "2488"
_4images_image_date: "2017-02-17T16:01:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45249 -->
Dieses schmucklose user interface ist das Portal in eine faszinierende Welt...