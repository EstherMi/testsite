---
layout: "image"
title: "cfw BenoiTxt - Bild 3"
date: "2017-02-17T16:01:01"
picture: "BenoiTxt05.jpeg"
weight: "5"
konstrukteure: 
- "Peter Habermehl"
fotografen:
- "Peter Habermehl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- details/45253
imported:
- "2019"
_4images_image_id: "45253"
_4images_cat_id: "3369"
_4images_user_id: "2488"
_4images_image_date: "2017-02-17T16:01:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45253 -->
... oder solche ...

Es gibt eine kleine Auswahl an Farbpaletten, die die Bildwirkung ganz massiv beeinflussen.