---
layout: "image"
title: "Schwarze Kette"
date: "2007-04-26T15:36:23"
picture: "DSC07862.jpg"
weight: "89"
konstrukteure: 
- "Martin Giger"
fotografen:
- "Martin Giger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/10173
imported:
- "2019"
_4images_image_id: "10173"
_4images_cat_id: "843"
_4images_user_id: "445"
_4images_image_date: "2007-04-26T15:36:23"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10173 -->
Diese Kette gleicht der Kette von ft, nur ist sie schwarz. Die Kette ist vom Brio Builder System, Das ist eine mischung aus Lego Technik, Bilofix und Matador, ein kleines bisschen ft ist auch eingebaut.