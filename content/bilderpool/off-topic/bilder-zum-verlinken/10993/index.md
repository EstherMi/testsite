---
layout: "image"
title: "Unterschiedliche Farben"
date: "2007-06-30T15:51:01"
picture: "pmunterschfarben1.jpg"
weight: "76"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/10993
imported:
- "2019"
_4images_image_id: "10993"
_4images_cat_id: "843"
_4images_user_id: "558"
_4images_image_date: "2007-06-30T15:51:01"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10993 -->
Das sind 3 Powermotoren 50:1. Der Dunkle stammt aus dem Mobile Robots II (der 2. ist eingebaut im Robo) die zwei hellen aus dem Robomobil Set. Gibt es einen Unterschied? Das Getriebe ist identisch.