---
layout: "image"
title: "Paket_Teil6"
date: "2013-12-01T18:42:43"
picture: "DSC00319_publish.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "MickyW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- details/37866
imported:
- "2019"
_4images_image_id: "37866"
_4images_cat_id: "2813"
_4images_user_id: "1806"
_4images_image_date: "2013-12-01T18:42:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37866 -->
In der linken Bildhälfte viele Teile die ich nicht zuordnen kann.
Sonst ist eigentlich alles bekannt oder intuitiv klar.