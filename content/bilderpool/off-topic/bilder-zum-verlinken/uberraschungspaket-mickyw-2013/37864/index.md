---
layout: "image"
title: "Paket_Teil4"
date: "2013-12-01T18:42:43"
picture: "DSC00316_publish.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "MickyW"
keywords: ["Kugelbahn", "Schlauch", "Silikonschlauch"]
uploadBy: "MickyW"
license: "unknown"
legacy_id:
- details/37864
imported:
- "2019"
_4images_image_id: "37864"
_4images_cat_id: "2813"
_4images_user_id: "1806"
_4images_image_date: "2013-12-01T18:42:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37864 -->
Kugelbahnteile, Silikonschläuche