---
layout: "image"
title: "zug03.jpg"
date: "2014-10-10T19:10:48"
picture: "zug03.jpg"
weight: "3"
konstrukteure: 
- "Getriebesand"
fotografen:
- "Getriebesand"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Getriebesand"
license: "unknown"
legacy_id:
- details/39695
imported:
- "2019"
_4images_image_id: "39695"
_4images_cat_id: "2976"
_4images_user_id: "1635"
_4images_image_date: "2014-10-10T19:10:48"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39695 -->
Ein TX mit Reedkontakt  steuert den Zug über Magnete an der Station.