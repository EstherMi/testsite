---
layout: "image"
title: "or-Element ungleich direkter Verbindung"
date: "2015-05-19T18:37:36"
picture: "or_ist_ungleich_direkter_Verbindung.jpg"
weight: "20"
konstrukteure: 
- "Julian/RoboPro-Entwickler"
fotografen:
- "Julian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Julian"
license: "unknown"
legacy_id:
- details/40997
imported:
- "2019"
_4images_image_id: "40997"
_4images_cat_id: "843"
_4images_user_id: "2271"
_4images_image_date: "2015-05-19T18:37:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40997 -->
Wie man auf diesem Bild erkennen kann ist es nicht egal ob man zum verknüpfen zweier Eingänge ein oder-Element verwendet oder einfach die Pfeile zu verbinden.
siehe auch http://forum.ftcommunity.de/viewtopic.php?f=8&t=2663&start=140