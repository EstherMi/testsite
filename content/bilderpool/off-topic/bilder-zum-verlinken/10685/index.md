---
layout: "image"
title: "Antriebsrad waagerechte Achse mit Umlenkrollen"
date: "2007-06-03T19:07:32"
picture: "plotter8.jpg"
weight: "84"
konstrukteure: 
- "thkais"
fotografen:
- "thkais"
keywords: ["Plottermechanik", "Commodore", "1520"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- details/10685
imported:
- "2019"
_4images_image_id: "10685"
_4images_cat_id: "843"
_4images_user_id: "41"
_4images_image_date: "2007-06-03T19:07:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10685 -->
Unten sieht man einen Teil der Umlenkrollen der waagerechten Achse. Das Stahlseil ist "endlos" um die Rolle gelegt und wird mit der Feder entsprechend gespannt.
Alle Achsen werden über Schrittmotoren angetrieben