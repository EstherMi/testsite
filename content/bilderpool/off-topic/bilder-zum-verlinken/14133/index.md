---
layout: "image"
title: "Bilder zu 'Robo Explorer mit Kamera Betrieb'"
date: "2008-03-28T06:59:19"
picture: "EmpfngerBild1.jpg"
weight: "66"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- details/14133
imported:
- "2019"
_4images_image_id: "14133"
_4images_cat_id: "843"
_4images_user_id: "426"
_4images_image_date: "2008-03-28T06:59:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14133 -->
Hier mal der Empfänger!!!
Wie man sehen kann ein sauberes Bild selbst wenn im Hintergrund Robo-RF arbeitet!!

Achtung!! Die waagerechten streifen kommen irgendwie beim Fotografieren zu stande das Bild hat in wirklichkeit keine Streifen!!!

Habe die Kamera zu versuchszwecken auf eine Fischertechnik Tüte an meiner Wand gerichtet!!!