---
layout: "image"
title: "Economatics_ Alu Grundplatte für Pneumatikmodelle"
date: "2017-01-08T19:57:18"
picture: "Econaomatics_ALU_Grundplatte.jpg"
weight: "4"
konstrukteure: 
- "Roland Enzenhofer"
fotografen:
- "Roland Enzenhofer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- details/45023
imported:
- "2019"
_4images_image_id: "45023"
_4images_cat_id: "843"
_4images_user_id: "1688"
_4images_image_date: "2017-01-08T19:57:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45023 -->
Die Pneumatikserie von Economatics war sehr interessant...