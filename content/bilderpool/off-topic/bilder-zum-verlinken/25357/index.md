---
layout: "image"
title: "Veranstaltungsort Modellschau Steinfurt 2009"
date: "2009-09-25T15:37:08"
picture: "Bild_Halle.jpg"
weight: "49"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/25357
imported:
- "2019"
_4images_image_id: "25357"
_4images_cat_id: "843"
_4images_user_id: "184"
_4images_image_date: "2009-09-25T15:37:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25357 -->
Das ist die Halle in der die Modellschau stattfinden wird.
Wilhelm hat hier ein schönens Bild gemacht.