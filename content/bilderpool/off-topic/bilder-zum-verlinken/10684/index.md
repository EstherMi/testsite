---
layout: "image"
title: "Stift-Revolver (2)"
date: "2007-06-03T19:07:32"
picture: "plotter7.jpg"
weight: "83"
konstrukteure: 
- "thkais"
fotografen:
- "thkais"
keywords: ["Plottermechanik", "Commodore", "1520"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- details/10684
imported:
- "2019"
_4images_image_id: "10684"
_4images_cat_id: "843"
_4images_user_id: "41"
_4images_image_date: "2007-06-03T19:07:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10684 -->
Hier der Stift-Revolver in der Wechsel-Position. Über eine Art Kugelschreiber-Mechanik wird der Revolver in drei Schritten um eine Stiftposition weitergedreht. In der Mitte des Revolvers sieht man einen (vermutlich magnetischen) Metallstift, der zusammen mit einem Reed-Kontakt (links) die Referenzposition für die Farbe ermittelt.