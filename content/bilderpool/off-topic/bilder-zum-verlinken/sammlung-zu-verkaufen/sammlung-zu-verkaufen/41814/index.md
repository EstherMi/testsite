---
layout: "image"
title: "MODELL 16: Bezeichnung: Planierraupe"
date: "2015-08-16T18:51:31"
picture: "Nr._16_Bild_1.jpg"
weight: "62"
konstrukteure: 
- "Hans - Joachim Siegel"
fotografen:
- "Stephan & Marcus Siegel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marcus66"
license: "unknown"
legacy_id:
- details/41814
imported:
- "2019"
_4images_image_id: "41814"
_4images_cat_id: "3114"
_4images_user_id: "2471"
_4images_image_date: "2015-08-16T18:51:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41814 -->
MODELL 16: Bezeichnung: Planierraupe // Länge: ca. 28 cm Breite: ca. 18 cm Höhe: ca. 15 cm Besonderes:       - /-