---
layout: "image"
title: "MODELL 11:  VERKAUFT!!!"
date: "2015-08-16T18:51:31"
picture: "Nr._11_Bild_3.jpg"
weight: "42"
konstrukteure: 
- "Hans - Joachim Siegel"
fotografen:
- "Stephan & Marcus Siegel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marcus66"
license: "unknown"
legacy_id:
- details/41794
imported:
- "2019"
_4images_image_id: "41794"
_4images_cat_id: "3114"
_4images_user_id: "2471"
_4images_image_date: "2015-08-16T18:51:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41794 -->
MODELL 11: : VERKAUFT!!!