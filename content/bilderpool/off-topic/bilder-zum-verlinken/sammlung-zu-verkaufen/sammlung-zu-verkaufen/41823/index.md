---
layout: "image"
title: "MODELL 18:  Bezeichnung: Schienendrehkran"
date: "2015-08-16T18:51:31"
picture: "Nr._18_Bild_2.jpg"
weight: "71"
konstrukteure: 
- "Hans - Joachim Siegel"
fotografen:
- "Stephan & Marcus Siegel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Marcus66"
license: "unknown"
legacy_id:
- details/41823
imported:
- "2019"
_4images_image_id: "41823"
_4images_cat_id: "3114"
_4images_user_id: "2471"
_4images_image_date: "2015-08-16T18:51:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41823 -->
