---
layout: "image"
title: "ELO Sonderheft 255"
date: "2013-11-01T11:05:54"
picture: "ELO_Sonderheft_255_ft_1.jpg"
weight: "26"
konstrukteure: 
- "Franzis Verlag"
fotografen:
- "Franzis Verlag"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- details/37778
imported:
- "2019"
_4images_image_id: "37778"
_4images_cat_id: "843"
_4images_user_id: "109"
_4images_image_date: "2013-11-01T11:05:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37778 -->
Titelbild des gesuchten ELO Sonderheftes.
Leider ist nicht viel zu erkennen.
Hoffen wir mal dass das Heft jemand hat und einscannen kann und wir die Erlaubnis vom Verlag bekommen das Buch hier zum download dann einstellen können.