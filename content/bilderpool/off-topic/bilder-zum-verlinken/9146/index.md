---
layout: "image"
title: "Platine"
date: "2007-02-25T17:14:42"
picture: "Forumstefan_002.jpg"
weight: "97"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/9146
imported:
- "2019"
_4images_image_id: "9146"
_4images_cat_id: "843"
_4images_user_id: "453"
_4images_image_date: "2007-02-25T17:14:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9146 -->
