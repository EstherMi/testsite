---
layout: "image"
title: "lunamobil 2"
date: "2008-04-12T09:19:09"
picture: "luna_2.jpg"
weight: "63"
konstrukteure: 
- "-?-"
fotografen:
- "Oliver Recktenwald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "olliillo"
license: "unknown"
legacy_id:
- details/14215
imported:
- "2019"
_4images_image_id: "14215"
_4images_cat_id: "843"
_4images_user_id: "331"
_4images_image_date: "2008-04-12T09:19:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14215 -->
