---
layout: "image"
title: "Transistor als Inverter"
date: "2016-05-31T15:32:28"
picture: "Inverter.jpg"
weight: "17"
konstrukteure: 
- "Jürgen Schumann"
fotografen:
- "Jürgen Schumann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ju-Suma"
license: "unknown"
legacy_id:
- details/43663
imported:
- "2019"
_4images_image_id: "43663"
_4images_cat_id: "843"
_4images_user_id: "2430"
_4images_image_date: "2016-05-31T15:32:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43663 -->
Um das "Signal" des Fototransistors umzudrehen, kann ein einfacher Transistor als Schalter dahinter gesetzt werden. Der Vorwiderstand ist dabei unbedingt notwendig.
Hier der Umbau der Badezimmerlüftung aus Profi Electronics. Allerdings leuchtet die LED jetzt nur wenn die Lichtschranke unterbrochen ist.