---
layout: "image"
title: "Plotter 1520 - Übersicht"
date: "2007-06-03T19:07:01"
picture: "plotter1_2.jpg"
weight: "77"
konstrukteure: 
- "thkais"
fotografen:
- "thkais"
keywords: ["Plottermechanik", "Commodore", "1520"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- details/10677
imported:
- "2019"
_4images_image_id: "10677"
_4images_cat_id: "843"
_4images_user_id: "41"
_4images_image_date: "2007-06-03T19:07:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10677 -->
Ein sehr kleines, feines Gerät. War damals sauteuer. Ich hab auch leider festgestellt, warum der in der Schublade steht - ich muss ihn wohl mal auseinander nehmen...