---
layout: "image"
title: "Facit03"
date: "2008-10-20T08:42:13"
picture: "Facit03.jpg"
weight: "9"
konstrukteure: 
- "Facit"
fotografen:
- "HLGR"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "HLGR"
license: "unknown"
legacy_id:
- details/16017
imported:
- "2019"
_4images_image_id: "16017"
_4images_cat_id: "1456"
_4images_user_id: "832"
_4images_image_date: "2008-10-20T08:42:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16017 -->
