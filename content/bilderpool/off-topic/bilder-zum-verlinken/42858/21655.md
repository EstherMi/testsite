---
layout: "comment"
hidden: true
title: "21655"
date: "2016-02-08T22:10:13"
uploadBy:
- "Martin Wanke"
license: "unknown"
imported:
- "2019"
---
Sehr kreativ. Auch in den Farben. Fehlt noch eine Kombination aus 3 Winkelträgern: schwarz - rot - gelb (am besten von oben nach unten)   :-)