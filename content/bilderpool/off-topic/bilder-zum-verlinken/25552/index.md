---
layout: "image"
title: "Regal"
date: "2009-10-13T19:55:02"
picture: "Regal.jpg"
weight: "48"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/25552
imported:
- "2019"
_4images_image_id: "25552"
_4images_cat_id: "843"
_4images_user_id: "184"
_4images_image_date: "2009-10-13T19:55:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25552 -->
Ein Regal extra angefertigt um viele Box(en) 1000 aufzunehmen.