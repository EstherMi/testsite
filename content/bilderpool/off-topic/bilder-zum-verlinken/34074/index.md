---
layout: "image"
title: "TRL Fehler bei Positionierung"
date: "2012-02-03T19:26:35"
picture: "trl1.jpg"
weight: "33"
konstrukteure: 
- "FischerPapa"
fotografen:
- "FischerPapa"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FischerPapa"
license: "unknown"
legacy_id:
- details/34074
imported:
- "2019"
_4images_image_id: "34074"
_4images_cat_id: "843"
_4images_user_id: "1127"
_4images_image_date: "2012-02-03T19:26:35"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34074 -->
Das ist das Unterprogramm "PosEncoder" meiner TRL-Software Version 3.0. Sie hat Probleme beim Anfahren der gewünschten Position.