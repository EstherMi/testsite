---
layout: "image"
title: "TXTShow Webinterface I - Alben verwalten"
date: "2017-02-03T20:02:53"
picture: "txtshow-webint1.png"
weight: "7"
konstrukteure: 
- "Peter Habermehl"
fotografen:
- "Peter Habermehl"
keywords: ["TXTShow", "cfw"]
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- details/45119
imported:
- "2019"
_4images_image_id: "45119"
_4images_cat_id: "3361"
_4images_user_id: "2488"
_4images_image_date: "2017-02-03T20:02:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45119 -->
Das Webinterface von TXTShow

Hier kann man ebenfalls Alben anlegen und löschen.
Wenn man ein Album auswählt, gelangt man in die Bilderübersicht des jeweiligen Albums.