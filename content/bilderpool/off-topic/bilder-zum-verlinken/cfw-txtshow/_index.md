---
layout: "overview"
title: "cfw TXTShow"
date: 2019-12-17T18:09:55+01:00
legacy_id:
- categories/3361
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3361 --> 
Eine kleine App zum Verwalten von Fotos auf dem TXT.
Es können mit der USB-Kamera Fotos gemacht werden, die übers Webinterface der App auch heruntergeladen werden können.