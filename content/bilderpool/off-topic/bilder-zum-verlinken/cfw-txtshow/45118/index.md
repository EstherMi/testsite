---
layout: "image"
title: "TXTShow Kamera"
date: "2017-02-03T20:02:53"
picture: "txtshow05.png"
weight: "6"
konstrukteure: 
- "Peter Habermehl"
fotografen:
- "Peter Habermehl"
keywords: ["TXTShow", "cfw"]
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- details/45118
imported:
- "2019"
_4images_image_id: "45118"
_4images_cat_id: "3361"
_4images_user_id: "2488"
_4images_image_date: "2017-02-03T20:02:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45118 -->
Das Foto-Fenster:
Mit + bzw. - kann die Kameraanzeige gezoomt werden, um besser fokussieren zu können.
Der Zoom hat keine Auswirkung auf das gemachte Foto.
Mit "Snap" wird die Aufnahme im aktuellen Album gespeichert.