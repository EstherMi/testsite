---
layout: "image"
title: "Misumi 15 mm Aluprofile: Verbindung mit Fischertechnik"
date: "2016-11-14T17:07:00"
picture: "Alu_S-Riegel.jpg"
weight: "12"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Alu", "Aluminium", "Profile", "Statik", "Anbau", "S-Riegel"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/44765
imported:
- "2019"
_4images_image_id: "44765"
_4images_cat_id: "843"
_4images_user_id: "579"
_4images_image_date: "2016-11-14T17:07:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44765 -->
Ich hab einen von diesen schwarzen Statik-Verbindungssteinen (32850 S-Riegelstein 15x15 schwarz) in die Nut des Alu Profils geschoben und vorsichtig um 90 Grad gedreht. Das funktioniert und der sitzt dann fest.

Die roten Statik-Verbinder (36323 S-Riegel 4 rot) halten auch gut in der Nut, wenn sie nach Einführen in die Nut um 90 Grad gedreht werden. 

Damit kann man alle Statik-Bauelemente anbauen. Ich denke, das ist die schnellste und einfachste Lösung.