---
layout: "comment"
hidden: true
title: "20073"
date: "2015-01-18T21:39:38"
uploadBy:
- "Pilami"
license: "unknown"
imported:
- "2019"
---
Ergänzung:
Wenn ich mich richtig erinnere war das Ganze eine "Stopuhr" die akustisch, per Klatschen ausgelöst wurde und über die Lichtschranke gestoppt wurde. Die Taster dienten zum Rücksetzen.