---
layout: "image"
title: "LKW-Fahrwerk aus Clubheft 1969-1"
date: "2011-10-16T12:07:13"
picture: "lkw1.jpg"
weight: "35"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/33181
imported:
- "2019"
_4images_image_id: "33181"
_4images_cat_id: "843"
_4images_user_id: "104"
_4images_image_date: "2011-10-16T12:07:13"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33181 -->
Hier ein anderes Bild des LKW-Fahrwerks. Es wurde im ft-Clubheft 1969-1 abgebildet. Der Text links unten lautet:

Dieses Großmodell eines Sattelschleppers war ein vielbestaunter Anziehungspunkt auf der Spielwarenmesse in Nürnberg.
Das funktionsfähige Modell ist mit einem Allradgetriebe ausgestattet.
Die Kardanwellen übertragen die Kräfte zu den Rädern. Differential, Federung, Lenkung, Beleuchtung einschließlich Blinkanlage - alles ist vorhanden und funktioniert. Die Steuerung erfolgt über Schaltwalzen aus dem elektromechanik-Baukasten.
Ein Super-Modell, an dem auch wir unsere Freude haben, denn es beweist wieder einmal:
fischertechnik - System ohne Grenzen.