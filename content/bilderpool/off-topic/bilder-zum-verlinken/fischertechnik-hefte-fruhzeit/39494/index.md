---
layout: "image"
title: "FT Hefte der Frühzeit"
date: "2014-10-02T09:00:35"
picture: "fischertechnikheftederfruehzeit6.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "Markus Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "markus wolf"
license: "unknown"
legacy_id:
- details/39494
imported:
- "2019"
_4images_image_id: "39494"
_4images_cat_id: "2957"
_4images_user_id: "968"
_4images_image_date: "2014-10-02T09:00:35"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39494 -->
Die Inhaltsangaben für beide Hefte.