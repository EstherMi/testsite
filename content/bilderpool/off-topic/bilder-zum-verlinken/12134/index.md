---
layout: "image"
title: "Experimentier-Labor (43.1079 ST)"
date: "2007-10-05T06:10:53"
picture: "pdft_DI002315.jpg"
weight: "67"
konstrukteure: 
- "-?-"
fotografen:
- "Fischer-Werke ; Peter Derks"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterderks"
license: "unknown"
legacy_id:
- details/12134
imported:
- "2019"
_4images_image_id: "12134"
_4images_cat_id: "843"
_4images_user_id: "363"
_4images_image_date: "2007-10-05T06:10:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12134 -->
