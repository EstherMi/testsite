---
layout: "image"
title: "nook Screensaver Model"
date: "2010-05-22T10:49:08"
picture: "n_ft_f.jpg"
weight: "18"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["nook"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/27289
imported:
- "2019"
_4images_image_id: "27289"
_4images_cat_id: "312"
_4images_user_id: "585"
_4images_image_date: "2010-05-22T10:49:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27289 -->
This is a ft model I am using for a screensaver on my Barnes & Noble nook ereader.