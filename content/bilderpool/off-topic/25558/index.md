---
layout: "image"
title: "2009 Bra Project"
date: "2009-10-18T16:08:01"
picture: "sm_ft_bra_1.jpg"
weight: "22"
konstrukteure: 
- "Richard Mussler-Wright"
fotografen:
- "Richard Mussler-Wright"
keywords: ["2009", "Bra", "Project"]
uploadBy: "ft_idaho"
license: "unknown"
legacy_id:
- details/25558
imported:
- "2019"
_4images_image_id: "25558"
_4images_cat_id: "312"
_4images_user_id: "585"
_4images_image_date: "2009-10-18T16:08:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25558 -->
This is my entry in the 2009 Bra Project. 

The BSU Women's Center in Boise Idaho hosts an annual Bra Project art auction. This year's fundraiser will benefit St. Luke's Mountain States Tumor Institute mammography program to provide screening for underinsured and low-income women in our community. The art bras may be made of any material. I choose ft. 

More information is at The Bra Project 2009: http://www.facebook.com/event.php?eid=169897336340&ref=mf


***google translation***
Dies ist mein Beitrag in der 2009-Bra-Projekt.

Die BSU Women's Center in Boise, Idaho ist Gastgeber des jährlichen Bra Projekt Kunstauktion. Die diesjährige Spendenaktion profitieren St. Luke's Mountain States Tumor Institute Mammographie-Screening-Programm für unterversichert und niedrige Einkommen bieten Frauen in unserer Gesellschaft. Die Kunst-BHs kann aus einem Material 

gefertigt sein. Ich wähle ft.

Mehr Informationen finden Sie auf der Bra-Projekt 2009: http://www.facebook.com/event.php?eid=169897336340&ref=mf