---
layout: "image"
title: "Die ersten Dokumente sind sichtbar"
date: "2013-06-11T23:13:53"
picture: "meineneuerrungenenkaesten02.jpg"
weight: "2"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/37097
imported:
- "2019"
_4images_image_id: "37097"
_4images_cat_id: "2754"
_4images_user_id: "130"
_4images_image_date: "2013-06-11T23:13:53"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37097 -->
Schöne alte Bauanleitungen bzw. Prospektmaterial