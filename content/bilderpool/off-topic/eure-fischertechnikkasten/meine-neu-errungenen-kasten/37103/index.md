---
layout: "image"
title: "Statikkästen und der gute alte Jet"
date: "2013-06-11T23:13:53"
picture: "meineneuerrungenenkaesten08.jpg"
weight: "8"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/37103
imported:
- "2019"
_4images_image_id: "37103"
_4images_cat_id: "2754"
_4images_user_id: "130"
_4images_image_date: "2013-06-11T23:13:53"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37103 -->
