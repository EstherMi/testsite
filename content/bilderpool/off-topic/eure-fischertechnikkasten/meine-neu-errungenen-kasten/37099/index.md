---
layout: "image"
title: "Weils so schön war gleich noch welche"
date: "2013-06-11T23:13:53"
picture: "meineneuerrungenenkaesten04.jpg"
weight: "4"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/37099
imported:
- "2019"
_4images_image_id: "37099"
_4images_cat_id: "2754"
_4images_user_id: "130"
_4images_image_date: "2013-06-11T23:13:53"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37099 -->
