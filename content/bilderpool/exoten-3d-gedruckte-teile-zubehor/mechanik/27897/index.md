---
layout: "image"
title: "Zahnrad Z22 m0,5"
date: "2010-08-22T19:17:58"
picture: "Z22_M05.jpg"
weight: "6"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Andreas Tacke"
keywords: ["Nabe", "Z15", "TST", "Zahnrad"]
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/27897
imported:
- "2019"
_4images_image_id: "27897"
_4images_cat_id: "465"
_4images_user_id: "182"
_4images_image_date: "2010-08-22T19:17:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27897 -->
Hallo
Ich habe eine Nabe entwickelt womit sich ein Z15 Zahnrad 35695 fest auf einer Metallachse befestigen läßt. Zum 2. hat man so ein Zahnrad Z22 Modul 0,5. Im Vordergrund ist das Nabenteil zu sehen.
Gerade beim Antrieb von Förderbändern ist dieses Teil sehr hilfreich.