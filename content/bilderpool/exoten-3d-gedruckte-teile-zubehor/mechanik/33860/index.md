---
layout: "image"
title: "Freilaufhülse"
date: "2012-01-08T15:08:26"
picture: "freilaufhuelse1.jpg"
weight: "3"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/33860
imported:
- "2019"
_4images_image_id: "33860"
_4images_cat_id: "465"
_4images_user_id: "182"
_4images_image_date: "2012-01-08T15:08:26"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33860 -->
Wer kannt das Problem nicht.
Da hat mann ein Modell gebaut und stellt fest das das ein Freilauf nicht schlecht wäre um die Mechanik zu schonen.
Oder aber man braucht einen Freilauf um die Funktion hin zu bekommen.
Abhilfe kann da so eine Freilaufhülse bringen.
An der einen Seite läßt sich eine 4mm-Achse durch die Madenschraube fixieren, an der anderen Seite wird die Achse in den Freilauf gesteckt.
Je nach Einbaurichtung läßt sich ein Freilauf mit oder gegen den Uhrzeigersinn herstellen.

Verbaut habe ich hier eine Freilaufhülse HF0406 von INA.