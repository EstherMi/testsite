---
layout: "image"
title: "Arbeitsschritte"
date: "2015-05-02T19:51:16"
picture: "Foto_2.jpg"
weight: "2"
konstrukteure: 
- "Julian Bußmeier"
fotografen:
- "Julian Bußmeier"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "JulianBussmeier"
license: "unknown"
legacy_id:
- details/40939
imported:
- "2019"
_4images_image_id: "40939"
_4images_cat_id: "3074"
_4images_user_id: "1474"
_4images_image_date: "2015-05-02T19:51:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40939 -->
Benötigte Materialien
&#8226;	fischertechnik Pneumatik- Kolben
&#8226;	M4 Schrauben Länge mindestens 30mm
&#8226;	Sekundenkleber vorzugsweise: Pattex Sekundenkleber Modellbau Plastik 
&#8226;	Pucksäge mit Eisensägeblatt    
&#8226;	verschiedene Feilen 
&#8226;	Cuttermesser 
&#8226;	Bohrmaschine  
&#8226;	Entgrater bzw. Kegelbohrer  
&#8226;	Schraubstock 
Arbeitsschritte
1.	Es hat sich bewährt vorsichtig an den Klebestellen des Kopfes, bzw. des Fußes zu biegen, um diese zu lösen. 
2.	Entfernen der Schlauchanschlüsse, diese werden nicht mehr benötigt.
3.	Das durchsichtige Zylinderrohr auf 4mm Länge kürzen, ggf. beischleifen. 
4.	Entfernung der Kolbenstangenführung im Zylinderkopf und des Quaders auf der Nut des Fußes.
5.	Klebereste und Grat entfernen.
6.	Schraubenkopf flachschleifen.
7.	Schraube mit Unterlegscheiben einsetzen und mit Muttern kontern. 
8.	Alles zusammenstecken, Maße kontrollieren.
9.	Leichtlauf der Schraube prüfen.
10.	Alles zusammenkleben, gemäß Anleitung, ACHTUNG Schraube nicht fixieren. 
11.	Durch nochmaliges Nachkleben von Zwischenräumen wird eine höhere Festigkeit erreicht.
Tipp: Um 8 Achshalter herzustellen, habe ich 10 ft-Pneumatik- Kolben benötigt.