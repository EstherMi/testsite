---
layout: "image"
title: "Endantrieb 1:4 mit Planetensatz"
date: "2016-01-24T18:52:37"
picture: "dz5.jpg"
weight: "5"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/42739
imported:
- "2019"
_4images_image_id: "42739"
_4images_cat_id: "3183"
_4images_user_id: "4"
_4images_image_date: "2016-01-24T18:52:37"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42739 -->
Ein Planetensatz passt dann zusammen, wenn die Summe der Zähne auf dem Sonnenrad plus zweifache Summe der Zähne auf einem Planeten genau die Zähne des innenverzahnten Außenzahnrads ergibt. Dafür gibt es beim Z40 zwei Lösungen mit ft-Zahnrädern: 

Sonne = Z10, Planeten = 2 * Z15; 10 + 2 * 15 = 40
Sonne = Z20, Planeten = 2 * Z10; 20 + 2 * 10 = 40

Mit dem feststehenden Planeten Z10 und dem Z20 in der Mitte ergeben sich Achsabstände im ft-Raster. Das Sonnenrad läuft leer mit und muss "nur" das Planetenrad nach innen hin abstützen.