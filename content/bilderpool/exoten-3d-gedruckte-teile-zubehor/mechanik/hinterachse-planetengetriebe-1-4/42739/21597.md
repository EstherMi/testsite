---
layout: "comment"
hidden: true
title: "21597"
date: "2016-01-25T20:30:29"
uploadBy:
- "Martin Wanke"
license: "unknown"
imported:
- "2019"
---
@David: Cool. Wenn beliebige Fischertechnikteile mit Fischertechnikteilen herstellbar sind, gibt es quasi keine Fremdteile mehr und alles ist erlaubt...