---
layout: "comment"
hidden: true
title: "12655"
date: "2010-10-30T21:03:19"
uploadBy:
- "Triceratops"
license: "unknown"
imported:
- "2019"
---
Hallo

Ist das Copyright denn abgeklärt? Immerhin handelt es sich um den
Nachbau eines originalen FT-Teils. Selbst wenn der stilisierte Fisch
darauf fehlt, sehe ich es ansonsten etwas problematisch. Die Idee
ansich allerdings ist gut. Und wenn das alles schön paßt, dann um-
so besser.

Gruß, Thomas