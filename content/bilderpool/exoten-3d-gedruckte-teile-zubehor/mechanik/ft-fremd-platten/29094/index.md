---
layout: "image"
title: "FT-Fremdplatten"
date: "2010-10-30T18:28:20"
picture: "FT-Platten-4.jpg"
weight: "4"
konstrukteure: 
- "Harald Krafthöfer"
fotografen:
- "Peterholland"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/29094
imported:
- "2019"
_4images_image_id: "29094"
_4images_cat_id: "2113"
_4images_user_id: "22"
_4images_image_date: "2010-10-30T18:28:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29094 -->
FT-Fremd-Platten gibt es bei :
Harald Krafthöfer
www.schmalspurgartenbahn.de