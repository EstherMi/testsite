---
layout: "image"
title: "neues Rast-Zahnrad Z20 rot (Art-Nr.137677)"
date: "2010-02-13T15:24:20"
picture: "neuesrastzahnradzrotartnr5.jpg"
weight: "6"
konstrukteure: 
- "ft"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/26364
imported:
- "2019"
_4images_image_id: "26364"
_4images_cat_id: "1878"
_4images_user_id: "409"
_4images_image_date: "2010-02-13T15:24:20"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26364 -->
Vorderansicht.
In so einem Fall, wo beim Antrieb das Aluprofil mal im weg ist, kann man es so durch das neue Zahnrad gut lösen.