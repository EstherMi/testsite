---
layout: "image"
title: "Stellring 4mm"
date: "2014-04-16T16:25:43"
picture: "Stellring4mmAlu.jpg"
weight: "3"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38567
imported:
- "2019"
_4images_image_id: "38567"
_4images_cat_id: "2879"
_4images_user_id: "1729"
_4images_image_date: "2014-04-16T16:25:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38567 -->
Ein Stellring dient zum Fixieren eine Achse gegen seitliches Verrutschen.
Dieser Stellring passt exakt zu Fischertechnik:
- vergrößerte Toleranz des Innendurchmessers:
Da die fischertechnik Metallachsen an den Enden meistens etwas dicker als 4mm sind, können diese Achsen oft nur mit Gewalt in Standard 4mm Bohrungen gesteckt werden. Dieser Stellring hat eine Bohrung von 4.03 mm, was zwar noch einen festen Sitz ermöglicht, die Achsen können i.d.R. aber ohne Kraftaufwand eingeschoben werden
- Bauteilbreite
Der Stellring ist abgestimmt auf das halbe Fischertechnik Rastmaß 7,5 mm. Die Breite des Stellrings beträgt 7,4 mm; er fixiert die Achse damit in beide Längsrichtungen, kann sich aber noch drehen.