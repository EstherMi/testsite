---
layout: "image"
title: "Achsverbinder 4mm/4mm"
date: "2014-04-22T20:23:04"
picture: "Coupler-4-4.jpg"
weight: "1"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38634
imported:
- "2019"
_4images_image_id: "38634"
_4images_cat_id: "2879"
_4images_user_id: "1729"
_4images_image_date: "2014-04-22T20:23:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38634 -->
Mit diesem Adapter lassen sich 4mm Achsen miteinander verbinden. Schlupf gehört damit der Vergangenheit an!
- Die Länge des Adapters ist 14,9 mm und passt damit ins 15 mm ft Raster.
- Durchmesser der Bohrung: 4.03 mm
- Material: Aluminium --> sehr leicht!

Übrigens: Meine Teile lassen sich jetzt auch über www.fischerfriendsman.de/ bestellen!