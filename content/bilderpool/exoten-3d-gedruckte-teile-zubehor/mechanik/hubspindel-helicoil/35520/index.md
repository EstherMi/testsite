---
layout: "image"
title: "helicoil3.jpg"
date: "2012-09-15T13:53:09"
picture: "IMG_7996a.jpg"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/35520
imported:
- "2019"
_4images_image_id: "35520"
_4images_cat_id: "2634"
_4images_user_id: "4"
_4images_image_date: "2012-09-15T13:53:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35520 -->
Auf dem Weg zur verbesserten Anordnung. 
Links unten ein unverändertes Rastkegelrad, daneben ein Rastkegelrad, aufgebohrt und mit m4-Helicoil versehen. Es ist außerdem noch außen etwas schlanker gedreht worden, damit es im 7mm-Loch des BS15 leichter dreht. Der auf 7 mm bis in halbe Tiefe aufgebohrte BS15 ist oben zu sehen. Weil Plastik auf Plastik viel Reibung hat (und weil das Rastzahnrad rückseitig immer noch etwas konisch ist und sich ins Loch klemmt, und weil der 7 mm-Bohrer kein Loch mit flacher Sohle erzeugt), gibt es noch eine Unterlegscheibe, auf der die Kegelzähne nachher aufliegen.