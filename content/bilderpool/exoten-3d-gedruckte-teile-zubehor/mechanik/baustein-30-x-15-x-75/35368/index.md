---
layout: "image"
title: "Baustein 30x15x7,5"
date: "2012-08-26T20:28:54"
picture: "bausteinxx1.jpg"
weight: "4"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/35368
imported:
- "2019"
_4images_image_id: "35368"
_4images_cat_id: "2623"
_4images_user_id: "182"
_4images_image_date: "2012-08-26T20:28:54"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35368 -->
Ich habe mal aus den Resten der Bauplatten die ich für die Motoranbauplatten nehme ein neues Bauteil entwickelt.
Der Baustein ist 30 x15 x7,5mm groß.
Wie zu sehen hat er unterschiedliche Nuten auf den beiden Seiten.
Für die Verwendung habe ich auch noch keine Idee, aber da wird sich sicher was finden.......