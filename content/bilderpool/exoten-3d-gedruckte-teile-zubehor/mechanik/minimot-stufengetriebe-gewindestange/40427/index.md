---
layout: "image"
title: "Minimot-Stufengetriebe mit Gewindestange"
date: "2015-01-30T15:17:13"
picture: "minimotstufengetriebemitgewindestange1.jpg"
weight: "1"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/40427
imported:
- "2019"
_4images_image_id: "40427"
_4images_cat_id: "3031"
_4images_user_id: "2321"
_4images_image_date: "2015-01-30T15:17:13"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40427 -->
Im Gehäuse sind ja zwei Stege, zwischen denen die Gewindestange ein wenig klemmt, aber sie dreht sich trotzdem noch halbwegs leicht. Obwohl M4 hat sie wohl etwas weniger als 4mm Durchmesser.
Dadurch passt auch die Riegelscheibe in die Zahnräder. Mit einer ft-Achse (minimal dicker) würde sie zu stramm am Zwischenzahnrad sitzen. 
Die eine Schraubenmutter habe ich flachgefeilt, dabei aber zwei Zapfen stehenlassen, die in die beiden Aussparungen in der Riegelscheibe an der Stange fassen. Das rechte Ende der Gewindestange ist flachgefeilt, so dass Rastelemente draufpassen. Sie ragt sogar noch durch das Zahnrad hindurch, von hinten (nicht sichtbar) ist auch noch eine Mutter aufgeschraubt und drückt das Zahnrad fest.