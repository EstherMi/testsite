---
layout: "image"
title: "Aktor mit Rollenlagern"
date: "2014-06-10T06:55:41"
picture: "bowdenzug4.jpg"
weight: "4"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38922
imported:
- "2019"
_4images_image_id: "38922"
_4images_cat_id: "2911"
_4images_user_id: "1729"
_4images_image_date: "2014-06-10T06:55:41"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38922 -->
Ein Aktor, aufgebaut mit Rollenlagern.
Im Prinzip die gleiche Funktion wie der Aktor mit dem Baustein 15.
Benötigt weniger Teile und weniger Platz.
Die Hülle kann aber nicht befestigt werden. Der Aktor ist damit nur für Zugkräfte geeignet.
In diesem Fall müsste man eigentlich noch eine Feder einbauen, die den Aktor zurückstellt.