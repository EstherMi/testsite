---
layout: "image"
title: "Verbinden von Metallachsen"
date: "2011-02-04T15:24:20"
picture: "mmmm2.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/29860
imported:
- "2019"
_4images_image_id: "29860"
_4images_cat_id: "2199"
_4images_user_id: "1162"
_4images_image_date: "2011-02-04T15:24:20"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29860 -->
Nun, steckt ihr die erste Metallachse zur Hälfte in die Lüsterklemme und schraubt sie fest.