---
layout: "image"
title: "XM Motor"
date: "2010-02-13T15:24:20"
picture: "xmmotor3.jpg"
weight: "5"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/26355
imported:
- "2019"
_4images_image_id: "26355"
_4images_cat_id: "1876"
_4images_user_id: "182"
_4images_image_date: "2010-02-13T15:24:20"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26355 -->
Seitenansicht