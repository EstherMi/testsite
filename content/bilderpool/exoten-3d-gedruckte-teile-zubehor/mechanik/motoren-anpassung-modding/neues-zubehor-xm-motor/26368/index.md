---
layout: "image"
title: "XM Motor mit Schnecke"
date: "2010-02-13T22:21:36"
picture: "xmmotor3_2.jpg"
weight: "6"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/26368
imported:
- "2019"
_4images_image_id: "26368"
_4images_cat_id: "1876"
_4images_user_id: "182"
_4images_image_date: "2010-02-13T22:21:36"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26368 -->
Hier mit angebauter Schnecke 35109.
Mit Messingbuchse und Madenschraube versehen läßt sie sich auf dem XM Motor befestigen.