---
layout: "image"
title: "Motor mit angebrachter Drehscheibe"
date: "2015-01-06T16:10:22"
picture: "ideefuermotormitabgebrochenerschnecke4.jpg"
weight: "4"
konstrukteure: 
- "Ich"
fotografen:
- "Ich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MrFlokiflott"
license: "unknown"
legacy_id:
- details/40192
imported:
- "2019"
_4images_image_id: "40192"
_4images_cat_id: "3019"
_4images_user_id: "2342"
_4images_image_date: "2015-01-06T16:10:22"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40192 -->
Dann einfach die Drehscheibe drauf und fertig.