---
layout: "image"
title: "Motor ohne Schnecke"
date: "2015-01-06T16:10:22"
picture: "ideefuermotormitabgebrochenerschnecke2.jpg"
weight: "2"
konstrukteure: 
- "Ich"
fotografen:
- "Ich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MrFlokiflott"
license: "unknown"
legacy_id:
- details/40190
imported:
- "2019"
_4images_image_id: "40190"
_4images_cat_id: "3019"
_4images_user_id: "2342"
_4images_image_date: "2015-01-06T16:10:22"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40190 -->
Hier nochmal der Motor ohne die Schnecke