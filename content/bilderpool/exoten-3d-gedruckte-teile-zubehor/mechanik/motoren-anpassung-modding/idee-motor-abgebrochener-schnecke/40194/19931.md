---
layout: "comment"
hidden: true
title: "19931"
date: "2015-01-06T20:33:16"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo MrFlokiflott,
deine Überlegungen könnten interessant sein. Beachte aber bitte - obwohl das Bild zunächst nur optisch gesehen einen ausreichenden Rundlauf der Scheibe 60 vermittelt - Masse und Größe sowie Massenträgheit des Klemmteils bei den bekannten Drehzahlen des Motors. Die Schnecke ist als Standard nicht umsonst als "kleines" Masseteil zur Untersetzung auf den Motorzapfen aufgesetzt. Ein längerer Lauf dieses Konstruktes ist jedenfalls für den Motor nicht zu empfehlen.
Gruß Udo2