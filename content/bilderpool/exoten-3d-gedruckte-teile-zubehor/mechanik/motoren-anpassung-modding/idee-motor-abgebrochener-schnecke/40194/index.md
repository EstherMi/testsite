---
layout: "image"
title: "Drehender Motor"
date: "2015-01-06T16:10:22"
picture: "ideefuermotormitabgebrochenerschnecke6.jpg"
weight: "6"
konstrukteure: 
- "Ich"
fotografen:
- "Ich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "MrFlokiflott"
license: "unknown"
legacy_id:
- details/40194
imported:
- "2019"
_4images_image_id: "40194"
_4images_cat_id: "3019"
_4images_user_id: "2342"
_4images_image_date: "2015-01-06T16:10:22"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40194 -->
Nochmal zum Beweis, die am Motor angebrachte Drehscheibe dreht sich.