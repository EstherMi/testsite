---
layout: "image"
title: "Powermotorplatte1a"
date: "2009-01-07T21:22:46"
picture: "Platte1a.jpg"
weight: "5"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Andreas Tacke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/16944
imported:
- "2019"
_4images_image_id: "16944"
_4images_cat_id: "1216"
_4images_user_id: "182"
_4images_image_date: "2009-01-07T21:22:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16944 -->
Winterzeit ist Bastelzeit
Ich habe hier mal eine stabile Powermotorplatte gemacht. Damit kann man den Motor vernünftig im Modell einbauen und er kann mit 2 Schrauben befestigt werden.