---
layout: "image"
title: "Powermotorplatte 2a"
date: "2009-01-07T21:22:47"
picture: "Platte2a.jpg"
weight: "7"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Andreas Tacke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/16946
imported:
- "2019"
_4images_image_id: "16946"
_4images_cat_id: "1216"
_4images_user_id: "182"
_4images_image_date: "2009-01-07T21:22:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16946 -->
Winterzeit ist Bastelzeit
Ich habe hier mal eine stabile Powermotorplatte gemacht. Damit kann man den Motor vernünftig im Modell einbauen und er kann mit 2 Schrauben befestigt werden.Hier die Variante mit der roten Platte.