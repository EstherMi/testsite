---
layout: "image"
title: "Powermotorhalter Differential"
date: "2008-02-02T10:03:47"
picture: "Bild3.jpg"
weight: "9"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Andreas Tacke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/13507
imported:
- "2019"
_4images_image_id: "13507"
_4images_cat_id: "1216"
_4images_user_id: "182"
_4images_image_date: "2008-02-02T10:03:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13507 -->
Ich habe hier mal eine stabile Halterung für den Powermotor am Differential entwickelt. Kompakt und sehr stabil. Er wird mit 2 Schrauben am Motor befestigt.