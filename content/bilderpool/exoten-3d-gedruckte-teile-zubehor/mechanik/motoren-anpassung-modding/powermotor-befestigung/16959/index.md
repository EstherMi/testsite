---
layout: "image"
title: "Powermotor mit M Schnecke"
date: "2009-01-09T22:16:25"
picture: "PlatteSchnecke1.jpg"
weight: "2"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Andreas Tacke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/16959
imported:
- "2019"
_4images_image_id: "16959"
_4images_cat_id: "1216"
_4images_user_id: "182"
_4images_image_date: "2009-01-09T22:16:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16959 -->
Hier zu sehen mit einem Schneckenhalter