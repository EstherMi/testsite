---
layout: "overview"
title: "Motoren - Anpassung und Modding"
date: 2019-12-17T18:01:14+01:00
legacy_id:
- categories/2766
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2766 --> 
Wer einen fremden Motor an ft anpasst oder einen ft-Motor "anfasst", ist ein Teilemodder und wird mit Fingerschnittwunden und Plastik-Sägespänen bestraft.