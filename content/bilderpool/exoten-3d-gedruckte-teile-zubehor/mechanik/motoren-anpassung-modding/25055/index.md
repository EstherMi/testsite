---
layout: "image"
title: "Second picture, other angle"
date: "2009-09-22T12:49:33"
picture: "DSC_0009.jpg"
weight: "3"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/25055
imported:
- "2019"
_4images_image_id: "25055"
_4images_cat_id: "2766"
_4images_user_id: "371"
_4images_image_date: "2009-09-22T12:49:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25055 -->
