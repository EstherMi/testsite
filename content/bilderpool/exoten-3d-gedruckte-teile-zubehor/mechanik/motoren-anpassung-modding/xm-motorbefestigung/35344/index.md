---
layout: "image"
title: "Anbauplatte XM back"
date: "2012-08-18T17:38:09"
picture: "xmanbauplatte1.jpg"
weight: "3"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/35344
imported:
- "2019"
_4images_image_id: "35344"
_4images_cat_id: "2619"
_4images_user_id: "182"
_4images_image_date: "2012-08-18T17:38:09"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35344 -->
Am XM Motor fehlt ja eine Befestigungsmöglichkeit an der Vorderseite.
Durch diese Platte läßt sich nun auch dort anbauen.