---
layout: "image"
title: "XM Platte mit Differential"
date: "2012-08-21T21:09:19"
picture: "xmplatte1.jpg"
weight: "2"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/35350
imported:
- "2019"
_4images_image_id: "35350"
_4images_cat_id: "2619"
_4images_user_id: "182"
_4images_image_date: "2012-08-21T21:09:19"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35350 -->
Wie hier zu sehen paßt die Platte perfekt ins Raster um auch das Differential stabil zu befestigen