---
layout: "image"
title: "Zahnrad"
date: "2008-12-05T20:19:52"
picture: "richtigeschrittmotorenundfischertechnik4.jpg"
weight: "4"
konstrukteure: 
- "Christian Korn"
fotografen:
- "Christian Korn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- details/16547
imported:
- "2019"
_4images_image_id: "16547"
_4images_cat_id: "1495"
_4images_user_id: "853"
_4images_image_date: "2008-12-05T20:19:52"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16547 -->
Das Zahnrad paßt perfekt zu den Ft-Zahnrädern.