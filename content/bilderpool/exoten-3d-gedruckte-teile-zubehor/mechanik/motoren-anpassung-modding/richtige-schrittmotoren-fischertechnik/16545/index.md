---
layout: "image"
title: "Ansteuerung"
date: "2008-12-05T20:19:51"
picture: "richtigeschrittmotorenundfischertechnik2.jpg"
weight: "2"
konstrukteure: 
- "Christian Korn"
fotografen:
- "Christian Korn"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "flyingcat"
license: "unknown"
legacy_id:
- details/16545
imported:
- "2019"
_4images_image_id: "16545"
_4images_cat_id: "1495"
_4images_user_id: "853"
_4images_image_date: "2008-12-05T20:19:51"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16545 -->
... eine Schrittmotor-Ansteuerung, zum Beispiel diese hier. Erhältlich bei www.robotikhardware.de (Achtung: die haben mir über 11 Euro für ein Paket abgeknöpft, das dann mit Hermes verschickt wurde... viel zu teuer!).
Diese Ansteuerung bietet Anschlüsse für zwei Schrittmotoren, einstellbaren Strangstrom, Ansteuerung per I2C, Taktsignalen (brauchbar für Ansteuerung per Robo-Interface) sowie RS232 (besser, damit kann man dann einfach sagen: "Strangstrom x, Geschwindigkeit y, fahre z Impulse" und das Ding macht den Rest) - und RS232 geht natürlich auch über das Robo-Interface. Hier ist noch meine provisorische RS232-Verkabelung zu sehen.