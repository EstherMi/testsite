---
layout: "overview"
title: "'Richtige' Schrittmotoren und Fischertechnik"
date: 2019-12-17T18:01:22+01:00
legacy_id:
- categories/1495
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1495 --> 
Ich wollte mal einen Schrittmotor mit deutlich mehr Leistung als die Knobloch-Variante an Fischertechnik anbinden. Diese Bilder zeigen, wie\'s geht.