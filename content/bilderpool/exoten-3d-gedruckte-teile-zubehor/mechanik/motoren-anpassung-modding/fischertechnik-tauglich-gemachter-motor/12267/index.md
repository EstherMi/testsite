---
layout: "image"
title: "Ft-tauglich gemachter Mot. (1)"
date: "2007-10-20T23:49:47"
picture: "fischertechniktauglichgemachtermotor1.jpg"
weight: "2"
konstrukteure: 
- "nula"
fotografen:
- "nula"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "nula"
license: "unknown"
legacy_id:
- details/12267
imported:
- "2019"
_4images_image_id: "12267"
_4images_cat_id: "1095"
_4images_user_id: "592"
_4images_image_date: "2007-10-20T23:49:47"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12267 -->
Hier sieht man den Motor, den ich verfischertechnikisiert habe.

Siehe auch => http://www.ftcommunity.de/categories.php?cat_id=1080