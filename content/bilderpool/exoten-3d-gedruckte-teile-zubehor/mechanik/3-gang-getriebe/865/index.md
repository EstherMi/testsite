---
layout: "image"
title: "3gang03"
date: "2003-04-27T17:15:39"
picture: "3gang03.jpg"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Ziehkeil", "Ziehkeilgetriebe"]
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/865
imported:
- "2019"
_4images_image_id: "865"
_4images_cat_id: "24"
_4images_user_id: "1"
_4images_image_date: "2003-04-27T17:15:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=865 -->
