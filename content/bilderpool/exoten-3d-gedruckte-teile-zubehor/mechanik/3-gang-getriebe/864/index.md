---
layout: "image"
title: "3gang02"
date: "2003-04-27T17:15:39"
picture: "3gang02.jpg"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/864
imported:
- "2019"
_4images_image_id: "864"
_4images_cat_id: "24"
_4images_user_id: "1"
_4images_image_date: "2003-04-27T17:15:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=864 -->
