---
layout: "image"
title: "Dreigang52.JPG"
date: "2007-10-28T13:26:43"
picture: "Dreigang52.JPG"
weight: "2"
konstrukteure: 
- "Michael (sannchen90)"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/12368
imported:
- "2019"
_4images_image_id: "12368"
_4images_cat_id: "24"
_4images_user_id: "4"
_4images_image_date: "2007-10-28T13:26:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12368 -->
Die Zahnräder von Michael. Oben Z20, Z19, Z18, unten Z10, Z11, Z12. Die kleinen Zahnräder konnte ich von innen einspannen und dann die Zahn-Stirnseiten anfasen, bei den großen hat das nicht geklappt.

An den Zahnflanken habe ich nichts bearbeitet; die hat der Michael 100%ig sauber hingekriegt. Danke!