---
layout: "image"
title: "Pijlstaartrog-Fischertechnik -oben   +  links 'Fischertechnik-Smartbird-Earth-Flight'"
date: "2014-01-02T20:26:17"
picture: "pijlstaartrog26.jpg"
weight: "26"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/37975
imported:
- "2019"
_4images_image_id: "37975"
_4images_cat_id: "2825"
_4images_user_id: "22"
_4images_image_date: "2014-01-02T20:26:17"
_4images_image_order: "26"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37975 -->
Die Kinematik der Silbermöwe habe ich nachgebaut in meiner "Fischertechnik-Smartbird-Earth-Flight".
Diese gibt es unter :
http://www.ftcommunity.de/details.php?image_id=36410
