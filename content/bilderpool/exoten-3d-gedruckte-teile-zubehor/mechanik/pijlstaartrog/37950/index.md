---
layout: "image"
title: "Pijlstaartrog-Fischertechnik    -oben"
date: "2014-01-02T20:26:17"
picture: "pijlstaartrog01.jpg"
weight: "1"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/37950
imported:
- "2019"
_4images_image_id: "37950"
_4images_cat_id: "2825"
_4images_user_id: "22"
_4images_image_date: "2014-01-02T20:26:17"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37950 -->
Ich habe die Kinematik einer Pijlstaartrog in Fischertechnik nachgebaut.
Der Fin Ray Effekt habe ich dieses mal gemacht mit eine Kombination von Fischertechnik + Polystyreen-Gold-Spiegel-Platten.

Der Fin Ray Effekt® ist eine von der funktionellen Anatomie der Fischflosse abgeleitete Konstruktion. Diese ermöglicht, den Flossenantrieb des natürlichen Vorbilds nahezu perfekt zu imitieren. 
Die Flossenstrahlen, die in der englischen Sprache als "Fin Ray" bezeichnet werden, machen es allein durch die Mechanik der Flosse möglich, den Flügel zu krümmen und die entstehenden Kräfte gleichmäßig über den gesamten Flügel zu verteilen, wodurch ein hocheffizienter Antrieb entsteht.
Für meine Pijlstaartrog-Fischertechnik nutze ich eine 20 Euro Polystyreen Platte Gold Spiegel 1000x500x1,5mm.

Die Kinematik der Silbermöwe habe ich nachgebaut in meiner "Fischertechnik-Smartbird-Earth-Flight".
Diese gibt es unter :
http://www.ftcommunity.de/details.php?image_id=36410


Meine Fischertechnik-Qualle mit Fin-Ray-Effect + pneumatik Muskel (Decke) gibt es unter :
http://www.ftcommunity.de/details.php?image_id=37251


Meine Adaptive Greifer gibt es unter :
http://www.ftcommunity.de/categories.php?cat_id=2775
Der adaptive Greifer mit Fin-Ray-Effect funktioniert auch wie einer Fischschwanzflosse. 
Zwei flexible Bänder laufen wie ein Dreieck in der Spitze zusammen. 
Zwischenstege in regelmässigen Abständen sind über Gelenke mit den Bändern verbunden. 
Durch diesen flexiblen, aber festen Verbund passen sich die Greiffinger der Kontur eines Werkstücks an.
