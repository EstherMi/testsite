---
layout: "image"
title: "Detail Finray Schwanz"
date: "2016-01-10T19:54:50"
picture: "stingraypijlstaartrogv07.jpg"
weight: "7"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/42716
imported:
- "2019"
_4images_image_id: "42716"
_4images_cat_id: "3180"
_4images_user_id: "22"
_4images_image_date: "2016-01-10T19:54:50"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42716 -->
