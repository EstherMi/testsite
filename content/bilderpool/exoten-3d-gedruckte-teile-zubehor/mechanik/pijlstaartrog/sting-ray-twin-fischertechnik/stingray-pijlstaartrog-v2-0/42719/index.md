---
layout: "image"
title: "Andere Möglichkeiten mit Pololu Micro Metal Gearmotors -1"
date: "2016-01-10T19:54:50"
picture: "stingraypijlstaartrogv10.jpg"
weight: "10"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/42719
imported:
- "2019"
_4images_image_id: "42719"
_4images_cat_id: "3180"
_4images_user_id: "22"
_4images_image_date: "2016-01-10T19:54:50"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42719 -->
micromotoren gibt es bei

https://iprototype.nl/products/robotics/servo-motors/micro-metalen-gearmotor-hp-100:1

oder :

https://www.pololu.com/category/60/micro-metal-gearmotors

Noch billiger bei :
 
http://nl.aliexpress.com/item/N20-DC12V-100RPM-Gear-Motor-High-Torque-Electric-Gear-Box-Motor/32524083373.html?spm=2114.13010608.0.65.NMlkHy 




Compact brackets :
 https://www.pololu.com/product/989


Dmv Conrad-art.nr:   297194-62 Messing-Rohr-Profil 4 mm buiten + 3.1 mm binnen.


Dreh-Zylinder Antrieb Alternativen M4, M5 + M6 + Info gibt es unter :
http://www.ftcommunity.de/categories.php?cat_id=2940