---
layout: "comment"
hidden: true
title: "21553"
date: "2016-01-11T20:10:33"
uploadBy:
- "Martin Wanke"
license: "unknown"
imported:
- "2019"
---
Hallo Peter,
eigentlich ist das ja eine recht simple Mechanik. Umso faszinierender ist die Wirkung! So einen Antrieb muss ich mal nachbauen, damit kann ich bestimmt meine Kinder begeistern.
LG Martin