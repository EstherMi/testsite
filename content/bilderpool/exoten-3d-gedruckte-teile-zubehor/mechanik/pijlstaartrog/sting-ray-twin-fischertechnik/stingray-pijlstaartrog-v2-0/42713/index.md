---
layout: "image"
title: "Detail Antrieb Finray Schwanz -2"
date: "2016-01-10T19:54:50"
picture: "stingraypijlstaartrogv04.jpg"
weight: "4"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/42713
imported:
- "2019"
_4images_image_id: "42713"
_4images_cat_id: "3180"
_4images_user_id: "22"
_4images_image_date: "2016-01-10T19:54:50"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42713 -->
Der Fin Ray Effekt® ist eine von der funktionellen Anatomie der Fischflosse abgeleitete Konstruktion. Diese ermöglicht, den Flossenantrieb des natürlichen Vorbilds nahezu perfekt zu imitieren. 
Die Flossenstrahlen, die in der englischen Sprache als "Fin Ray" bezeichnet werden, machen es allein durch die Mechanik der Flosse möglich, den Flügel und Schwanz zu krümmen und die entstehenden Kräfte gleichmäßig über den gesamten Flügel und Schwanz zu verteilen, wodurch ein hocheffizienter Antrieb entsteht.


Meine Adaptive Greifer gibt es unter : 
http://www.ftcommunity.de/categories.php?cat_id=2775 
Der adaptive Greifer mit Fin-Ray-Effect funktioniert auch wie einer Fischschwanzflosse. 
Zwei flexible Bänder laufen wie ein Dreieck in der Spitze zusammen. 
Zwischenstege in regelmässigen Abständen sind über Gelenke mit den Bändern verbunden. 
Durch diesen flexiblen, aber festen Verbund passen sich die Greiffinger der Kontur eines Werkstücks an.

Interessant ist auch der Zerr- oder Lachspiegel :
https://www.youtube.com/watch?v=hk6pu6vlWG8