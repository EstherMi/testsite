---
layout: "overview"
title: "Stingray-Pijlstaartrog-V2.0"
date: 2019-12-17T18:01:29+01:00
legacy_id:
- categories/3180
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3180 --> 
Ich habe die Kinematik einer Pijlstaartrog in Fischertechnik nachgebaut und jetzt in 2016 verbessert.

Der Fin Ray Effekt® ist eine von der funktionellen Anatomie der Fischflosse abgeleitete Konstruktion. Diese ermöglicht, den Flossenantrieb des natürlichen Vorbilds nahezu perfekt zu imitieren. 
Die Flossenstrahlen, die in der englischen Sprache als "Fin Ray" bezeichnet werden, machen es allein durch die Mechanik der Flosse möglich, den Flügel und Schwanz zu krümmen und die entstehenden Kräfte gleichmäßig über den gesamten Flügel und Schwanz zu verteilen, wodurch ein hocheffizienter Antrieb entsteht.
