---
layout: "overview"
title: "Sting-ray Twin Fischertechnik"
date: 2019-12-17T18:01:29+01:00
legacy_id:
- categories/3015
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3015 --> 
Ich habe die Kinematik der Sting-ray Twin in Fischertechnik nachgebaut.
Der Fin Ray Effekt habe ich dieses mal gemacht mit eine Kombination von Fischertechnik + Polystyreen-Gold-Spiegel-Platten.

Der Fin Ray Effekt® ist eine von der funktionellen Anatomie der Fischflosse abgeleitete Konstruktion. Diese ermöglicht, den Flossenantrieb des natürlichen Vorbilds nahezu perfekt zu imitieren. 
Die Flossenstrahlen, die in der englischen Sprache als "Fin Ray" bezeichnet werden, machen es allein durch die Mechanik der Flosse möglich, den Flügel zu krümmen und die entstehenden Kräfte gleichmäßig über den gesamten Flügel zu verteilen, wodurch ein hocheffizienter Antrieb entsteht.



