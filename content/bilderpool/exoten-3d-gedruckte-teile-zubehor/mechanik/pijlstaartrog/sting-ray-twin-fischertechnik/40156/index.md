---
layout: "image"
title: "Sting-ray Twin Fischertechnik"
date: "2015-01-02T16:37:51"
picture: "stingraytwinfischertechnik11.jpg"
weight: "11"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/40156
imported:
- "2019"
_4images_image_id: "40156"
_4images_cat_id: "3015"
_4images_user_id: "22"
_4images_image_date: "2015-01-02T16:37:51"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40156 -->
Der Fin Ray Effekt® ist eine von der funktionellen Anatomie der Fischflosse abgeleitete Konstruktion. Diese ermöglicht, den Flossenantrieb des natürlichen Vorbilds nahezu perfekt zu imitieren. 
Die Flossenstrahlen, die in der englischen Sprache als "Fin Ray" bezeichnet werden, machen es allein durch die Mechanik der Flosse möglich, den Flügel zu krümmen und die entstehenden Kräfte gleichmäßig über den gesamten Flügel zu verteilen, wodurch ein hocheffizienter Antrieb entsteht.
