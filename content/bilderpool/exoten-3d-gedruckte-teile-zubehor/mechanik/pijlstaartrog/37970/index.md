---
layout: "image"
title: "Pijlstaartrog-Fischertechnik  -unten"
date: "2014-01-02T20:26:17"
picture: "pijlstaartrog21.jpg"
weight: "21"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/37970
imported:
- "2019"
_4images_image_id: "37970"
_4images_cat_id: "2825"
_4images_user_id: "22"
_4images_image_date: "2014-01-02T20:26:17"
_4images_image_order: "21"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37970 -->
