---
layout: "image"
title: "Pijlstaartrog-Fischertechnik  -unten -hinten"
date: "2014-01-02T20:26:17"
picture: "pijlstaartrog23.jpg"
weight: "23"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/37972
imported:
- "2019"
_4images_image_id: "37972"
_4images_cat_id: "2825"
_4images_user_id: "22"
_4images_image_date: "2014-01-02T20:26:17"
_4images_image_order: "23"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37972 -->
Detail