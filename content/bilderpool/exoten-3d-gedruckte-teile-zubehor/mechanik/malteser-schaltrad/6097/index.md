---
layout: "image"
title: "Malteserschaltrad"
date: "2006-04-14T22:36:19"
picture: "GrennderungDSC02404.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "charly"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "charly"
license: "unknown"
legacy_id:
- details/6097
imported:
- "2019"
_4images_image_id: "6097"
_4images_cat_id: "542"
_4images_user_id: "115"
_4images_image_date: "2006-04-14T22:36:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6097 -->
Prototyp Malteserschaltrad mit selbstgebautem  Antriebsrad