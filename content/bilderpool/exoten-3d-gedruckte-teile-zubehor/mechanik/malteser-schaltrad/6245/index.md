---
layout: "image"
title: "Maltezer Schaltrad lösung Antrieb 1"
date: "2006-05-07T16:20:01"
picture: "DSCN4517_800.jpg"
weight: "5"
konstrukteure: 
- "Arjen Neijsen"
fotografen:
- "Arjen Neijsen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/6245
imported:
- "2019"
_4images_image_id: "6245"
_4images_cat_id: "542"
_4images_user_id: "162"
_4images_image_date: "2006-05-07T16:20:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6245 -->
Ich habe eine Lösung fur denn Antrieb von Maltezer Schaltrad gefunden. Ganz einfach.
Nehme ein Felge (32883) und schneide ein Teil weg. Dann noch eine Plastik band rund kleben (1mm dick) und fertig ist es. Auch ist es ein billige Lösung.