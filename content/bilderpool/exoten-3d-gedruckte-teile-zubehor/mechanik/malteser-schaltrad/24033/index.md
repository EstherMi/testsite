---
layout: "image"
title: "Malteser Schaltrad 4"
date: "2009-05-17T17:56:43"
picture: "Bild4.jpg"
weight: "4"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Andreas Tacke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/24033
imported:
- "2019"
_4images_image_id: "24033"
_4images_cat_id: "542"
_4images_user_id: "182"
_4images_image_date: "2009-05-17T17:56:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24033 -->
Hier ist das Schaltrad zu sehen. Unter Verwendung der Nockenscheibe und einer gekürzten Kunststoffachse ist der Aufbau fertig.