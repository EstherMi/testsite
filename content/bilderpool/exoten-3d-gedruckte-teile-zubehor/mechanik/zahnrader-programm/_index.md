---
layout: "overview"
title: "Zahnräder-Programm"
date: 2019-12-17T18:01:12+01:00
legacy_id:
- categories/2566
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2566 --> 
Beispiele für Zahnradvorlagen, die mit dem Postscript-Programm angefertigt wurden, das sich in der ft:c unter Downloads/Software befindet.