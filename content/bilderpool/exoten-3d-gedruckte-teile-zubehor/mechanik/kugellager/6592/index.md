---
layout: "image"
title: "Kugellager für Baustein 37925"
date: "2006-07-02T10:12:19"
picture: "DSCN0834.jpg"
weight: "3"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: ["modding"]
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/6592
imported:
- "2019"
_4images_image_id: "6592"
_4images_cat_id: "568"
_4images_user_id: "184"
_4images_image_date: "2006-07-02T10:12:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6592 -->
Die Kugellager die ich hier eingebaut habe, habe ich bei Oppermann gekauft.

Zwei Stück kosteten zu diesem Zeitpunkt 69 Cent.

Sie passen ohne Änderungen am Baustein vorzunehmen in diesen rein. Einfach reindrücken - hält super.