---
layout: "comment"
hidden: true
title: "3148"
date: "2007-05-09T13:00:25"
uploadBy:
- "remadus"
license: "unknown"
imported:
- "2019"
---
Ich habe mir jetzt auch eine ganze Reihe dieser Lagerböcke zurechtgemacht. Allerdings vergrößere ich mit einem passenden Schleifkörper die Bohrung der Schneckenmutter, so daß die Kugellager nicht so arg fest klemmen. Womöglich reißt sonst irgendwann die Schneckenmutter.

Ich setze auch nur ein Kugellager ein, weil ich die Wellen ja mit zwei dieser Lagerböcke ausrüste. Vier Kugellager auf einer Welle tut ja nicht not.

Super Idee von dir.