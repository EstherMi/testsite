---
layout: "comment"
hidden: true
title: "14557"
date: "2011-07-06T18:55:04"
uploadBy:
- "ludger-ftc"
license: "unknown"
imported:
- "2019"
---
Hallo Leute,
habe gerade mal wieder auf den Seiten von Oppermann herumgestöbert.
Dabei habe ich gesehen das die Kugellager mittlerweile echt teuer sind.
Ein Bliesterpack (2 Kugellager) kostete damals 69 Cent, bei einer größeren Menge 44 Cent.
Heute kostet ein Blisterpack 1,95€ und Rabatte werden NICHT mehr gegeben.
Vielleicht liegt es ja auch am Text. Da steht jetzt drin das die für fischertechnik geeignet sind ......

liebe Grüße ludger