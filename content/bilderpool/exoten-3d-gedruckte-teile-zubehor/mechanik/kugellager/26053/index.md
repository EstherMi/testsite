---
layout: "image"
title: "Kugelgelagerter BS 15 mit Loch [1/2]"
date: "2010-01-10T16:09:48"
picture: "kugellagerpeter1.jpg"
weight: "1"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- details/26053
imported:
- "2019"
_4images_image_id: "26053"
_4images_cat_id: "568"
_4images_user_id: "998"
_4images_image_date: "2010-01-10T16:09:48"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26053 -->
Hier habe ich das Innenleben des Bausteines entfernt und stattdessen Kugellager eingesetzt
Den verbliebenen Hohlraum habe ich mit Kunstharz vergossen