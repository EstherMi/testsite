---
layout: "image"
title: "Interessante HEWA – FLEC  Bauteile im FT-Raster"
date: "2012-01-22T14:41:48"
picture: "HEWA-FlLEC-Bauteile-FT-Raster_014.jpg"
weight: "3"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/33988
imported:
- "2019"
_4images_image_id: "33988"
_4images_cat_id: "2516"
_4images_user_id: "22"
_4images_image_date: "2012-01-22T14:41:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33988 -->
eckige 4mm Achsen