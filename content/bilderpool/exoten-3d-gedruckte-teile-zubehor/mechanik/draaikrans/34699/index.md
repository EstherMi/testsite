---
layout: "image"
title: "Andere kant"
date: "2012-03-28T19:29:30"
picture: "draaikrans_004.jpg"
weight: "3"
konstrukteure: 
- "Ruurd"
fotografen:
- "Ruurd"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/34699
imported:
- "2019"
_4images_image_id: "34699"
_4images_cat_id: "2562"
_4images_user_id: "838"
_4images_image_date: "2012-03-28T19:29:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=34699 -->
Andere zijde van de draaikrans hier wordt de aandrijfing weer gesplitst.