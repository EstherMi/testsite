---
layout: "image"
title: "Snow05.jpg"
date: "2006-02-12T15:56:47"
picture: "Snow05.jpg"
weight: "18"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5763
imported:
- "2019"
_4images_image_id: "5763"
_4images_cat_id: "431"
_4images_user_id: "4"
_4images_image_date: "2006-02-12T15:56:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5763 -->
