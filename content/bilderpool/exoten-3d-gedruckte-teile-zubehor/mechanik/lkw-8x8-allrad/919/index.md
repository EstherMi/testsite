---
layout: "image"
title: "8x8 01"
date: "2003-04-27T17:46:14"
picture: "8x8_01.jpg"
weight: "21"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sven"
license: "unknown"
legacy_id:
- details/919
imported:
- "2019"
_4images_image_id: "919"
_4images_cat_id: "431"
_4images_user_id: "1"
_4images_image_date: "2003-04-27T17:46:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=919 -->
