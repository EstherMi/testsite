---
layout: "image"
title: "8x8-140.jpg"
date: "2006-02-13T18:03:13"
picture: "8x8-140.jpg"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5769
imported:
- "2019"
_4images_image_id: "5769"
_4images_cat_id: "431"
_4images_user_id: "4"
_4images_image_date: "2006-02-13T18:03:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5769 -->
Hier ist alles beisammen. Links die Schalter für Zusatzgeräte nebst Zylinder zum Abschalten des Kompressors, der gegen einen Gummiring arbeitet.

In der Mitte links der Antriebsmotor, rechts davon die Schwungscheibe des Kompressors. Unter dem Rückschlagventil liegt der Kompressorzylinder.