---
layout: "image"
title: "8x8-125.jpg"
date: "2006-02-13T17:59:16"
picture: "8x8-125.jpg"
weight: "10"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Allrad", "Eigenbau", "Kardan"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5768
imported:
- "2019"
_4images_image_id: "5768"
_4images_cat_id: "431"
_4images_user_id: "4"
_4images_image_date: "2006-02-13T17:59:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5768 -->
Die Kardanwellen enthalten in der Mitte ein Stück, das sich teleskopartig auseinanderziehen lässt. Das wird im Gelände gebraucht, aber auch schon beim Zusammenbau des Ganzen.

Hier habe ich noch die Variante mit zwei D-Profilen verwendet, die mit den flachen Seiten aufeinander ein 5 mm-Rundloch ausfüllen. Das obere ist rechts eingeklebt, das untere ist am Kardangelenk auf der linken Seite fest.