---
layout: "image"
title: "8x8-124.jpg"
date: "2006-02-13T17:55:54"
picture: "8x8-124.jpg"
weight: "9"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Allrad", "Eigenbau"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5767
imported:
- "2019"
_4images_image_id: "5767"
_4images_cat_id: "431"
_4images_user_id: "4"
_4images_image_date: "2006-02-13T17:55:54"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5767 -->
Die Radaufnahmen sind aus zwei schwarzen Kunststoff-L-Profilen zusammengeklebt, darauf kommt noch eine weiße Platte mit etwas größerer Bohrung, in die der Wulst der ft-Felge hineinpasst.

Die Kardanwellen enthalten in der Mitte ein Stück, das sich teleskopartig auseinanderziehen lässt. Das wird im Gelände gebraucht, aber auch schon beim Zusammenbau des Ganzen.

Der Rückstrahler oben rechts ist auch Eigenbau (aus einem zersägten Reflektor, mit wenig Kleber aufgeklebt. Bei zuviel Kleber laufen die Winkel voll und das Licht wird verschluckt).