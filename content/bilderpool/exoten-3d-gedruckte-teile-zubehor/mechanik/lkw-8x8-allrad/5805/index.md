---
layout: "image"
title: "8x8-203.JPG"
date: "2006-03-04T16:05:06"
picture: "8x8-203.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5805
imported:
- "2019"
_4images_image_id: "5805"
_4images_cat_id: "431"
_4images_user_id: "4"
_4images_image_date: "2006-03-04T16:05:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5805 -->
Die untere Platte war nur 1 mm stark, deshalb musste auf 2 mm aufgefüttert werden, damit die S-Riegel stramm sitzen.