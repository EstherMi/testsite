---
layout: "image"
title: "8x8-204.JPG"
date: "2006-03-04T16:09:24"
picture: "8x8-204.JPG"
weight: "5"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/5806
imported:
- "2019"
_4images_image_id: "5806"
_4images_cat_id: "431"
_4images_user_id: "4"
_4images_image_date: "2006-03-04T16:09:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5806 -->
Die Kardanwellen aus 5 mm Messingrohr haben in der Mitte ein Teleskopteil.

Die schwarzen Stummel sind von "Plastik-Schweißdraht 4 mm" abgeschnitten worden. Dieses Material dient eigentlich dazu, die Fugen zwischen Linoleum-Fußbodenplatten zu schließen.