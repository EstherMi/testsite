---
layout: "image"
title: "Anwendungbeispiel 2 Rundzahnstange"
date: "2005-05-09T21:51:21"
picture: "Rundzahnstange_002.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "charly"
license: "unknown"
legacy_id:
- details/4112
imported:
- "2019"
_4images_image_id: "4112"
_4images_cat_id: "350"
_4images_user_id: "115"
_4images_image_date: "2005-05-09T21:51:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4112 -->
