---
layout: "image"
title: "Große Greifzange"
date: "2008-02-29T19:33:28"
picture: "Zangegromail.jpg"
weight: "12"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Andreas Tacke"
keywords: ["Greifzange", "TST", "Getriebe"]
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/13810
imported:
- "2019"
_4images_image_id: "13810"
_4images_cat_id: "1237"
_4images_user_id: "182"
_4images_image_date: "2008-02-29T19:33:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13810 -->
Hier mal eine große Greifzange mit dem von mir entwickelten Greifzangengetriebe. Die maßimale Greifbreite beträgt ca 100mm. Also hervorragent zum Stapeln der FT Kassetten geeignet.