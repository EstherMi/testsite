---
layout: "comment"
hidden: true
title: "8964"
date: "2009-04-11T10:52:46"
uploadBy:
- "charly"
license: "unknown"
imported:
- "2019"
---
Ich arbeite zwar im richtigen Leben als Industriemechaniker, aber die Sachen hier entstehen in meinem Hobbykeller auf einer popeligen, kleinen Proxxon Dreh/Fräs-Kombination vom Flohmarkt.
Gruß,
Charly