---
layout: "image"
title: "Greifzange Antrieb"
date: "2009-04-10T07:57:11"
picture: "kb06.jpg"
weight: "6"
konstrukteure: 
- "Charly  Karl-Hans Brielmann"
fotografen:
- "Charly  Karl-Hans Brielmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "charly"
license: "unknown"
legacy_id:
- details/23653
imported:
- "2019"
_4images_image_id: "23653"
_4images_cat_id: "1237"
_4images_user_id: "115"
_4images_image_date: "2009-04-10T07:57:11"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23653 -->
Hier sieht man den Federzug, dahinter muss man sich den Seizug denken.