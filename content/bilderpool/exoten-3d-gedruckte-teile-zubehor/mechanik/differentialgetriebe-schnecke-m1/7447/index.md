---
layout: "image"
title: "zweite Einheit"
date: "2006-11-12T18:26:46"
picture: "DSCN1124.jpg"
weight: "3"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/7447
imported:
- "2019"
_4images_image_id: "7447"
_4images_cat_id: "702"
_4images_user_id: "184"
_4images_image_date: "2006-11-12T18:26:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7447 -->
Die Einheiten werden mittels Rastachsen miteinander verbunden.
Treten größere Kräfte auf, können auch zwei Verbindungen hergestellt werden.