---
layout: "image"
title: "erste Einheit"
date: "2006-11-12T18:26:46"
picture: "DSCN1120.jpg"
weight: "2"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/7446
imported:
- "2019"
_4images_image_id: "7446"
_4images_cat_id: "702"
_4images_user_id: "184"
_4images_image_date: "2006-11-12T18:26:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7446 -->
Der Antrieb erfolgt hier noch mittels Kurbel. Das Modell stellt in erster Linie ein Schauobjekt dar, ist aber voll funktionstüchtig.