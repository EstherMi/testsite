---
layout: "overview"
title: "Laufschienen Steckverbindung (für Achterbahnkurve)"
date: 2019-12-17T18:01:00+01:00
legacy_id:
- categories/1795
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1795 --> 
Für meine Achterbahn, die ich gerade baue, war es notwendig die Schienen zusätzlich verbinden zu können, damit die Drehung für Kurven ohne Kanten durchfahren wird.
Mit kleinen Nägeln, bei denen die Köpfe abgesägt und in kleine, gebohrte Löcher gesteckt wurden, ging dies am besten. Von aussen sieht man sie nicht einmal, da sie in der Schiene verlaufen