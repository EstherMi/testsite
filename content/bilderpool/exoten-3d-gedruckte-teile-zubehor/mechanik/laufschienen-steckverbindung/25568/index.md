---
layout: "image"
title: "Beim Bohren"
date: "2009-10-21T18:40:40"
picture: "PICT0095.jpg"
weight: "5"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "Christian Knobloch"
keywords: ["Steckverbindung", "Achterbahn", "Laufschiene", "bohren"]
uploadBy: "C-Knobloch"
license: "unknown"
legacy_id:
- details/25568
imported:
- "2019"
_4images_image_id: "25568"
_4images_cat_id: "1795"
_4images_user_id: "997"
_4images_image_date: "2009-10-21T18:40:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25568 -->
man sieht, dass die "geköpften" Nägel ohne Probleme passen