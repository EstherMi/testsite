---
layout: "image"
title: "Drehung mit Steckverbindung"
date: "2009-10-21T18:40:40"
picture: "PICT0072.jpg"
weight: "3"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "Christian Knobloch"
keywords: ["Steckverbindung", "Laufschiene", "Achterbahn"]
uploadBy: "C-Knobloch"
license: "unknown"
legacy_id:
- details/25566
imported:
- "2019"
_4images_image_id: "25566"
_4images_cat_id: "1795"
_4images_user_id: "997"
_4images_image_date: "2009-10-21T18:40:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25566 -->
Hier gibt es nun Dank der nicht sichtbaren Steckverbindung keine Übergänge