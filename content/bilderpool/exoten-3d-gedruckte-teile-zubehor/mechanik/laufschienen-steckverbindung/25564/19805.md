---
layout: "comment"
hidden: true
title: "19805"
date: "2014-12-18T17:20:47"
uploadBy:
- "NBGer"
license: "unknown"
imported:
- "2019"
---
Hi,
ich bin gerade zufällig auf Deine Idee gestossen! Das ist zwar schon 5 Jahre her, daß Du das gepostet hast, aber immer noch aktuell! Ich vermute, von fischertechnik gibt es keine entsprechende Standard--Lösung.
Die Idee ist für mich interessant, da ich ein Modell einer Förderanlage machen möchte. Und zwar eine EHB (Elektorohängebahn). Da will ich im Laufe des Jahres 2015 ein schönes Modell hinbekommen.
Ich bin da beruflich vorbelastet. Ich habe an der Entwicklung und Inbetriebnahme der Systemkomponenten dieser Anlage mitgearbeitet (schon ne Zeitlang her): http://www.logistik-journal.de/index.cfm?pid=1408&pk=40373#.VJL9t5CcKA

Die Niederländer unter den ft-community Mitgliedern kennen bestimmt alle die Aalsmeer Blumenversteigerung!

Grüsse, Richard