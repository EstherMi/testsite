---
layout: "image"
title: "Laufschiene mit Steckverbindung"
date: "2009-10-21T18:40:39"
picture: "PICT0064.jpg"
weight: "1"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "Christian Knobloch"
keywords: ["Laufschiene", "Steckverbindung", "Achterbahn"]
uploadBy: "C-Knobloch"
license: "unknown"
legacy_id:
- details/25564
imported:
- "2019"
_4images_image_id: "25564"
_4images_cat_id: "1795"
_4images_user_id: "997"
_4images_image_date: "2009-10-21T18:40:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25564 -->
Wenn man z.B. eine Achterbahnkurve realisieren möchte, ist dies ideal um den Laufwiederstand bei der notwendigen 90° Drehung zu verringern