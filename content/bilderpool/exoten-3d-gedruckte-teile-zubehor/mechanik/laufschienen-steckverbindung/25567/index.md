---
layout: "image"
title: "Schiene mit Loch"
date: "2009-10-21T18:40:40"
picture: "PICT0082.jpg"
weight: "4"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "Christian Knobloch"
keywords: ["Schiene", "Laufschiene", "Achterbahn", "bohren", "Steckverbindung"]
uploadBy: "C-Knobloch"
license: "unknown"
legacy_id:
- details/25567
imported:
- "2019"
_4images_image_id: "25567"
_4images_cat_id: "1795"
_4images_user_id: "997"
_4images_image_date: "2009-10-21T18:40:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25567 -->
Die Löcher haben einen Durchmesser von 1,5mm