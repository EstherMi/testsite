---
layout: "image"
title: "Drehung ohne Steckverbindung"
date: "2009-10-21T18:40:39"
picture: "PICT0067.jpg"
weight: "2"
konstrukteure: 
- "Christian Knobloch"
fotografen:
- "Christian Knobloch"
keywords: ["Drehung", "Achterbahn", "Stecverbindung", "Laufschiene"]
uploadBy: "C-Knobloch"
license: "unknown"
legacy_id:
- details/25565
imported:
- "2019"
_4images_image_id: "25565"
_4images_cat_id: "1795"
_4images_user_id: "997"
_4images_image_date: "2009-10-21T18:40:39"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25565 -->
Ohne die Steckverbindung gibt es böse Kanten, wobei dieses Bild etwas übertrieben ist