---
layout: "image"
title: "Kardangelenk mit Gelenkstein"
date: "2015-02-28T08:31:52"
picture: "kleineskardangelenkfuersteckachsen4.jpg"
weight: "4"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/40604
imported:
- "2019"
_4images_image_id: "40604"
_4images_cat_id: "3046"
_4images_user_id: "2321"
_4images_image_date: "2015-02-28T08:31:52"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40604 -->
So, jetzt gehts Richtung Frontantrieb. Oben mit Strohalm als Lenkmanschette - weils grade so schön passt.