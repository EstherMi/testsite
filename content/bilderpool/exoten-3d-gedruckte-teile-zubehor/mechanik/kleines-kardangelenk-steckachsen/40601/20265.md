---
layout: "comment"
hidden: true
title: "20265"
date: "2015-03-01T10:47:01"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Wahnsinn!

Woraus sind denn die Stifte, die da raus schauen? Und, wie hast du deren gegenseitige Durchdringung mitten im Würfel gelöst? Stecken die nur so 1,5 mm drin?

Gruß,
Harald