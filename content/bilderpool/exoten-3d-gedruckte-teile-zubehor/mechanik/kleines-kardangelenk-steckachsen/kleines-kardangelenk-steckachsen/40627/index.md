---
layout: "image"
title: "Ansatz für Frontantriebs-Modul"
date: "2015-03-13T20:49:40"
picture: "kleineskardangelenkfuersteckachsen02.jpg"
weight: "2"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/40627
imported:
- "2019"
_4images_image_id: "40627"
_4images_cat_id: "3049"
_4images_user_id: "2321"
_4images_image_date: "2015-03-13T20:49:40"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40627 -->
So kann man den Achsschenkel aufhängen. Ich habe S-Streben für den Differentialkäfig genommen, weil damit das Rad etwas weiter schwenken kann als wenn ich normal breite Bausteine verwendet hätte.