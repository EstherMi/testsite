---
layout: "image"
title: "Kardangelenk - Zusammenbau 2"
date: "2015-03-13T20:49:40"
picture: "kleineskardangelenkfuersteckachsen13.jpg"
weight: "13"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/40638
imported:
- "2019"
_4images_image_id: "40638"
_4images_cat_id: "3049"
_4images_user_id: "2321"
_4images_image_date: "2015-03-13T20:49:40"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40638 -->
Die Nadeln kann man aneinender vorbeipressen, weil die Löcher im Würfel knapp den doppelten Durchmesser der Nadeln haben. 
Nur noch Nadeln abflexen - und fertig :-)