---
layout: "image"
title: "Kardangelenk in gemoddetem Gelenkstein für Frontantrieb"
date: "2015-03-13T20:49:40"
picture: "kleineskardangelenkfuersteckachsen01.jpg"
weight: "1"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/40626
imported:
- "2019"
_4images_image_id: "40626"
_4images_cat_id: "3049"
_4images_user_id: "2321"
_4images_image_date: "2015-03-13T20:49:40"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40626 -->
Nächster Versuch für den Frontantrieb. Klein, aber nur mit Schnitzmesser und Kleber.