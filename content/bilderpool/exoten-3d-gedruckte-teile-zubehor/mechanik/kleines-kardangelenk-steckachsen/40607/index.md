---
layout: "image"
title: "Kardangelenk mit Lagerstück und Knotenplatte für Frontantrieb"
date: "2015-02-28T08:31:52"
picture: "kleineskardangelenkfuersteckachsen7.jpg"
weight: "7"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/40607
imported:
- "2019"
_4images_image_id: "40607"
_4images_cat_id: "3046"
_4images_user_id: "2321"
_4images_image_date: "2015-02-28T08:31:52"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40607 -->
Das ist im Moment mein Favorit, weil ohne Teile-Modding. Die Knotenplatte soll als Lenkhebel zur Befestigung der Spurstange dienen.