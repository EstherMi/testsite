---
layout: "image"
title: "Mitnehmer"
date: "2010-08-28T09:49:15"
picture: "Mitnehmer.jpg"
weight: "5"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Andreas Tacke"
keywords: ["Mitnehmer", "Schnecke"]
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/27982
imported:
- "2019"
_4images_image_id: "27982"
_4images_cat_id: "465"
_4images_user_id: "182"
_4images_image_date: "2010-08-28T09:49:15"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27982 -->
Ich habe hier einen Mitnehmer entwickelt womit sich ein Schneckenteil 37926 fest auf einer Metallachse befestigen läßt. Man kann natürlich auch nur einen Mitnehmer einsetzen. Außerdem läßt sich mit diesem Mitnehmer ein Walzenrad 35386 befestigen. Was haltet Ihr davon ?