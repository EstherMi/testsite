---
layout: "image"
title: "Felge mit Nabe"
date: "2009-02-16T19:03:02"
picture: "IMG_7748.jpg"
weight: "18"
konstrukteure: 
- "thkais"
fotografen:
- "thkais"
keywords: ["Großreifen", "Felge"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- details/17430
imported:
- "2019"
_4images_image_id: "17430"
_4images_cat_id: "366"
_4images_user_id: "41"
_4images_image_date: "2009-02-16T19:03:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17430 -->
Wie man sieht, passt die Nabe exakt in die angefertigte Ausdrehung.