---
layout: "image"
title: "Felge von hinten"
date: "2009-02-16T19:03:02"
picture: "IMG_7747.jpg"
weight: "17"
konstrukteure: 
- "thkais"
fotografen:
- "thkais"
keywords: ["Großreifen", "Felge"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- details/17429
imported:
- "2019"
_4images_image_id: "17429"
_4images_cat_id: "366"
_4images_user_id: "41"
_4images_image_date: "2009-02-16T19:03:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17429 -->
Hier musste einiges weg, üblicherweise ist hier ein 6-kt Mitnehmer. Nun passt hier eine ft-Nabe hinein.