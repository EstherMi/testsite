---
layout: "image"
title: "Felgenbau aus FT"
date: "2005-06-19T16:07:01"
picture: "Felge_Monsterreifen.jpg"
weight: "26"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chevyfahrer"
license: "unknown"
legacy_id:
- details/4494
imported:
- "2019"
_4images_image_id: "4494"
_4images_cat_id: "366"
_4images_user_id: "103"
_4images_image_date: "2005-06-19T16:07:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4494 -->
Hier eine der vielen Möglichkeiten die Reifen mit Ft-Teilen zu verbinden.2x Drehscheibe und 1x Speichenrad.
Die Idee ist glaub ich eine Gemeinschaftsproduktion Pilami/Friggelsiggi