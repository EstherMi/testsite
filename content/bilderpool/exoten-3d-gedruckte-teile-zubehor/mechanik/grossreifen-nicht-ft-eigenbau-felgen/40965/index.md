---
layout: "image"
title: "Felge3D_1714.JPG"
date: "2015-05-12T20:41:59"
picture: "Felge3D_1714.JPG"
weight: "4"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/40965
imported:
- "2019"
_4images_image_id: "40965"
_4images_cat_id: "366"
_4images_user_id: "4"
_4images_image_date: "2015-05-12T20:41:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40965 -->
Es geht eng zu da drinnen. Sogar die schwarze Lagerhülse 36819 geht nicht mehr in "ganz" da hinein. Sie ist viergeteilt, und nur zwei Teile stecken in den Aufnahmen hinten und vorne.

Beim Zusammenbau muss die starre, tragende Achse (in Felgenmitte) zuletzt eingeschoben werden; beim Zerlegen muss man sie als erstes heraus ziehen.