---
layout: "image"
title: "1. Montage"
date: "2008-09-24T22:21:35"
picture: "reifen03.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/15575
imported:
- "2019"
_4images_image_id: "15575"
_4images_cat_id: "1428"
_4images_user_id: "389"
_4images_image_date: "2008-09-24T22:21:35"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15575 -->
Als erstes eine Drehscheibe hineinstecken
und jeweils gegenüber eine Clipsplatte aufstecken.