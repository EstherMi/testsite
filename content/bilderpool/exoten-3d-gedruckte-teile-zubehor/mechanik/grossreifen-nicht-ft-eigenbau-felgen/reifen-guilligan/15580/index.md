---
layout: "image"
title: "6. Montage"
date: "2008-09-24T22:21:36"
picture: "reifen08.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/15580
imported:
- "2019"
_4images_image_id: "15580"
_4images_cat_id: "1428"
_4images_user_id: "389"
_4images_image_date: "2008-09-24T22:21:36"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15580 -->
Dann das 2. Zahnrad aufstecken und festziehen.