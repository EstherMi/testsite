---
layout: "image"
title: "Das ist der Reifen von Tamiya"
date: "2008-09-24T22:21:35"
picture: "reifen01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/15573
imported:
- "2019"
_4images_image_id: "15573"
_4images_cat_id: "1428"
_4images_user_id: "389"
_4images_image_date: "2008-09-24T22:21:35"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15573 -->
Das ist der Reifen von Tamiya mit ca.:
88mm Durchmesser und 
30mm Breite

bestellen kann man den bei Modellbau Haertle.