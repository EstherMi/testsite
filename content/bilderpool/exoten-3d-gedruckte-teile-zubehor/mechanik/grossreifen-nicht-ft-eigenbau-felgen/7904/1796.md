---
layout: "comment"
hidden: true
title: "1796"
date: "2006-12-25T23:44:42"
uploadBy:
- "Jettaheizer"
license: "unknown"
imported:
- "2019"
---
Hallo Harald,

falls Du ernsthaft Interesse an einem Großmodell einer A380 hast, such mal in einschlägigen (Flug-)Modellbauforen nach Peter Michel. Der hat die Maschine mit 5,7m Spannweite und 70kg Abfluggewicht gebaut. Kosten: in etwa ein Mittelklassewagen. Die Räder haben allerdings nur etwa 100mm Durchmesser.... ;o)

Gruß,
Franz