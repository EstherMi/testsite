---
layout: "image"
title: "Felge3D_1715.JPG"
date: "2015-05-12T20:37:02"
picture: "Felge3D_1715.JPG"
weight: "3"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Modding"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/40964
imported:
- "2019"
_4images_image_id: "40964"
_4images_cat_id: "366"
_4images_user_id: "4"
_4images_image_date: "2015-05-12T20:37:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40964 -->
Zum Antrieb verwende ich ein Zahnrad Z10 mit Modul 1, das aus der Rastkurbel 35071 mittels scharfer Klinge entstanden ist. Aus dem 3D-Drucker ginge das zur Not auch, allerdings ist ABS ziemlich spröde und bei einer Rastaufnahme 4 mm bleibt sehr wenig "Fleisch" unter den Zähnen übrig, d.h. das Zahnrad ginge leicht zu Bruch. Da ist mir das zähe ft-Material doch lieber.

Die Rastkurbel steht bei Knobloch auf *svr*, und ist neben der schwarzen Schnecke das einzige Element im ft-Sortiment mit dem Modul 1. Und dabei könnte man damit sooo schöne Planetengetriebe bauen...