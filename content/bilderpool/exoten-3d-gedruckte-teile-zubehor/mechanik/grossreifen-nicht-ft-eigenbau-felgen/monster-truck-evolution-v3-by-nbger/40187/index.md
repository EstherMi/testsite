---
layout: "image"
title: "Detail Radinnere"
date: "2015-01-04T07:47:31"
picture: "GrossaufnahmeRadInnere.jpg"
weight: "4"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: ["Monsterreifen", "Untersetzung", "Vorgelegegetriebe"]
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/40187
imported:
- "2019"
_4images_image_id: "40187"
_4images_cat_id: "3016"
_4images_user_id: "1729"
_4images_image_date: "2015-01-04T07:47:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40187 -->
Hier noch mal in einer anderen Perspektive. Man sieht gut, wie tief die Zahnräder für die Untersetzung im Radinneren liegen. Zur Radinnenwand ist es allerdings äußerst knapp. Da sind nur Zentel-Millimeter Luft!
Wegen der Zahnräder bekomme ich allerdings den Drehpunkt der Lenkung nicht mehr ins Radinnere. Aber daran arbeite ich noch.....