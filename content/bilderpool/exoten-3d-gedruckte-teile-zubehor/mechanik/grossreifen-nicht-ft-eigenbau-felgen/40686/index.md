---
layout: "image"
title: "Detail Antrieb"
date: "2015-03-27T17:30:38"
picture: "Detail_Antrieb.jpg"
weight: "11"
konstrukteure: 
- "thkais"
fotografen:
- "thkais"
keywords: ["Großreifen", "3D-Druck", "Felge"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- details/40686
imported:
- "2019"
_4images_image_id: "40686"
_4images_cat_id: "366"
_4images_user_id: "41"
_4images_image_date: "2015-03-27T17:30:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40686 -->
Ein Detail-Foto des Antriebs. Eine Portal-Achse mit maximaler Bodenfreiheit ist kein Problem...