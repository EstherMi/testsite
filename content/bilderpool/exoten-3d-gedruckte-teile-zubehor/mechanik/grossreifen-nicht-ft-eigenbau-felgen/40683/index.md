---
layout: "image"
title: "Felge von vorn"
date: "2015-03-27T17:30:38"
picture: "Felge_vorn.jpg"
weight: "8"
konstrukteure: 
- "thkais"
fotografen:
- "thkais"
keywords: ["Großreifen", "3D-Druck", "Felge"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- details/40683
imported:
- "2019"
_4images_image_id: "40683"
_4images_cat_id: "366"
_4images_user_id: "41"
_4images_image_date: "2015-03-27T17:30:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40683 -->
Neues vom 3D-Drucker - eine ft-kompatible Felge...