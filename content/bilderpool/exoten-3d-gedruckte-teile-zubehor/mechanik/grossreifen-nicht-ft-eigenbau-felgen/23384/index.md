---
layout: "image"
title: "Detail Getriebe"
date: "2009-03-06T17:04:35"
picture: "IMG_7758.jpg"
weight: "15"
konstrukteure: 
- "thkais"
fotografen:
- "thkais"
keywords: ["Großreifen", "Planetengetriebe", "Reifen"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- details/23384
imported:
- "2019"
_4images_image_id: "23384"
_4images_cat_id: "366"
_4images_user_id: "41"
_4images_image_date: "2009-03-06T17:04:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23384 -->
Hier noch eine Detailaufnahme des Planetengetriebes