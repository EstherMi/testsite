---
layout: "image"
title: "Zugehöriges Zahnrad"
date: "2009-03-06T17:04:34"
picture: "IMG_7756.jpg"
weight: "13"
konstrukteure: 
- "thkais"
fotografen:
- "thkais"
keywords: ["Großreifen", "Planetengetriebe", "Reifen"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- details/23382
imported:
- "2019"
_4images_image_id: "23382"
_4images_cat_id: "366"
_4images_user_id: "41"
_4images_image_date: "2009-03-06T17:04:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23382 -->
Da das originale Zahnrad etwas viel aufträgt, habe ich auch gleich ein schmales Z10 ausgeschnitten