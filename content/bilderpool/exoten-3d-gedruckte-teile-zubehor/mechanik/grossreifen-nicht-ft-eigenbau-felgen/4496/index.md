---
layout: "image"
title: "Reifen mit Antrieb"
date: "2005-06-19T16:07:06"
picture: "ReifenZ40.jpg"
weight: "28"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chevyfahrer"
license: "unknown"
legacy_id:
- details/4496
imported:
- "2019"
_4images_image_id: "4496"
_4images_cat_id: "366"
_4images_user_id: "103"
_4images_image_date: "2005-06-19T16:07:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4496 -->
Da ein Antrieb direkt über die Stahlachse zu wenig Drehmoment bringt flansche ich ein Z40 an und stecke eine Stahlachse durch eines der Löcher.Die Achse sollte durch alle Komponenten durchgehen.Dadurch ergibt sich zumindest an dieser Stelle ein schlupffreier Antrieb.