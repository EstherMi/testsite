---
layout: "image"
title: "Kulissenstein für ft 2"
date: "2011-09-03T00:29:54"
picture: "kulissenstein2.jpg"
weight: "2"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/31740
imported:
- "2019"
_4images_image_id: "31740"
_4images_cat_id: "2366"
_4images_user_id: "182"
_4images_image_date: "2011-09-03T00:29:54"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31740 -->
Er paßt wie hier zu sehen genau in die Nute.