---
layout: "image"
title: "Kulissenstein für ft 4"
date: "2011-09-03T00:29:54"
picture: "kulissenstein4.jpg"
weight: "4"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/31742
imported:
- "2019"
_4images_image_id: "31742"
_4images_cat_id: "2366"
_4images_user_id: "182"
_4images_image_date: "2011-09-03T00:29:54"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31742 -->
Natürlich kann man dann auch mit einer handelsüblichen Schraube eine Platte oder ähnliches befestigen.