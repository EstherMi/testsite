---
layout: "image"
title: "Kulissenstein für ft"
date: "2011-09-03T00:29:54"
picture: "kulissenstein1.jpg"
weight: "1"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/31739
imported:
- "2019"
_4images_image_id: "31739"
_4images_cat_id: "2366"
_4images_user_id: "182"
_4images_image_date: "2011-09-03T00:29:54"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31739 -->
Ich habe da einen Kulissenstein für ft entwickelt.
Er besteht aus einer 4mm Messingwelle und ist 11mm lang.
In der Mitte befindet sich ein M3 Gewinde.