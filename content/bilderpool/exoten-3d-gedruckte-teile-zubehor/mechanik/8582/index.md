---
layout: "image"
title: "Notrutsche.JPG"
date: "2007-01-21T00:31:30"
picture: "Notrutsche.JPG"
weight: "12"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Schweißen", "Kunststoff", "Hovercraft", "Schlauchboot"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/8582
imported:
- "2019"
_4images_image_id: "8582"
_4images_cat_id: "465"
_4images_user_id: "4"
_4images_image_date: "2007-01-21T00:31:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8582 -->
Das sind erste Versuchsmuster für die Notrutschen der A380. Oben eine Tüte vom Gemüsehändler, unten die Verpackung von Schwarzbrot Marke "Pema" (nicht speziell ausgesucht, aber sie lag gerade so da). Versuche mit einer Gefriertüte und einer Einkaufstüte stehen noch aus.

Die Verarbeitung ist absolut simpel, aber ein bisschen Übung braucht man schon:
Man nehme einen Stapel Papier als nachgiebige Unterlage, lege darauf die Plastikfolie (zweilagig), darüber noch ein einzelnes Blatt Papier, auf letzteres zeichnet man das Muster der gewünschten Schweißbahnen, und dieses Muster fährt man mit einem Lötkolben ab. Den Lötkolben ganz flach halten und beim Ziehen nur leicht andrücken. Entscheidend ist, dass man "gar niemals niemals nie nicht" stehen bleibt. Hält man einmal an, brennt der Lötkolben ein Loch in die Folie. Das kann man abdichten, indem man einen Kreis drum herum schweißt, aber das Loch bleibt.

Die richtige Geschwindigkeit hängt sehr von Folie und Lötkolben ab, das muss man austesten. Lieber zu schnell als zu langsam. Wenn es anfängt zu stinken, ist man zu langsam. Wichtig ist auch, dass man das obere Blatt Papier nur ein einziges Mal verwendet. Papier enthält immer ein paar Prozent Wasser, und dieses Wasser verdunstet natürlich, wenn man mit dem Lötkolben drüberfährt. Das getrocknete Papier verhält sich anders als Papier, das noch "feucht" ist, und außerdem wird es wellig. Beim Drüberfahren über eine schon benutzte Stelle bleibt man mit dem Lötkolben hängen und schon ist ein Loch in die Folie gebrutzelt.


Übungsprogramm: erst mal nur gerade Streifen schweißen, dann auch unterbrochene Streifen (die Luft soll ja von Kammer zu Kammer strömen können), zuletzt gekrümmte Bahnen.