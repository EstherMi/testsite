---
layout: "overview"
title: "Zahnräder aus meinem Bastelkeller"
date: 2019-12-17T18:01:40+01:00
legacy_id:
- categories/3045
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3045 --> 
Seit Weihnachten bastle ich in meinem Hobbykeller an Vorrichtungen für selbstgefräste Zahnräder für mein liebstes Baukastensystem, hier zeige ich Euch meine ersten Ergebnisse.
 