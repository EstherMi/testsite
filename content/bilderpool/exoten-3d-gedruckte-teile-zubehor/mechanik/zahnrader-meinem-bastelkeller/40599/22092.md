---
layout: "comment"
hidden: true
title: "22092"
date: "2016-06-08T12:35:26"
uploadBy:
- "NBGer"
license: "unknown"
imported:
- "2019"
---
Hallo,
ich bin auch begeistert!
Ich habe auch eine ziemlich gute CNC Portalfräse, aber (noch) keine Drehmaschine.
Mit dem Gedanken, Zahnräder selbst zu machen, habe ich auch schon mal gespielt, aber mangels Erfahrung noch nicht probiert!
Idealerweise möchte ich mal schlupffreie Kegelzahnräder machen. Und es sollte einen Außendurchmesser < 15 mm haben, damit es besser eingebaut werden kann. Keine Ahnung, ob man sowas selber machen kann.
Aber mach doch mal ein paar Photos von deinem Werkstatt-Equipment. z.B. dein Direkt-Teilapparat!
Das wird wohl meine nächste Anschaffung. Ich möchte den dann aber mit Stepper-Motor motorisieren. (Bei einem Z96 96 mal per Hand den nächsten Winkel einstellen, ist wahrscheinlich etwas zeitraubend ;) )