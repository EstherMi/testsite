---
layout: "image"
title: "Z 48"
date: "2015-02-22T21:47:09"
picture: "zahnraederausmeinembastelkeller4.jpg"
weight: "4"
konstrukteure: 
- "charly"
fotografen:
- "charly"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "charly"
license: "unknown"
legacy_id:
- details/40597
imported:
- "2019"
_4images_image_id: "40597"
_4images_cat_id: "3045"
_4images_user_id: "115"
_4images_image_date: "2015-02-22T21:47:09"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40597 -->
Ebenfalls für Winkelpositionierung mit Encodermotor, halb so schnell, dafür Auflösung von 1/10 Grad.. Hier wurde ein abgedrehter Spurkranz zur Nabenaufnahme eingeklebt. Vier Gewinde M4 auf einem 45er Lochkreis sind für Anbaumöglichkeiten vorgesehen.