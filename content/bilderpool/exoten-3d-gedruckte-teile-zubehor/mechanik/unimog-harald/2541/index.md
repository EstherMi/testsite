---
layout: "image"
title: "Unimog01.JPG"
date: "2004-07-13T13:26:02"
picture: "Unimog01.jpg"
weight: "7"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Allrad", "Unimog"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/2541
imported:
- "2019"
_4images_image_id: "2541"
_4images_cat_id: "238"
_4images_user_id: "4"
_4images_image_date: "2004-07-13T13:26:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2541 -->
Die ersten Teile für den Allradantrieb eines ft-Unimog:
- Innenzahnrad Z20 (passend in die ft-Felge 45),
- Zahnrad Z7 (es musste zwischen den Wellenstummel und das Innenzahnrad passen)
- Achshalter, die Lenkachse zeigt nach Montage auf den Mittelpunkt der Aufstandsfläche, daher um ca. 22,5° schräggestellt
- Gleichlaufgelenk = zwei Kardangelenke direkt hintereinander