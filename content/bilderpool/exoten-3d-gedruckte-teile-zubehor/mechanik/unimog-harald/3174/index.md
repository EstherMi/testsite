---
layout: "image"
title: "Unimog13.JPG"
date: "2004-11-15T19:46:44"
picture: "Unimog13.jpg"
weight: "4"
konstrukteure: 
- "Harald"
fotografen:
- "-?-"
keywords: ["Allrad", "Unimog"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/3174
imported:
- "2019"
_4images_image_id: "3174"
_4images_cat_id: "238"
_4images_user_id: "4"
_4images_image_date: "2004-11-15T19:46:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3174 -->
Beide Achsen kommen ohne Teleskopwellen aus, weil sie genau im Drehpunkt des zugehörigen Kardangelenks gelagert sind. Das ist beim Original-Unimog auch so. Dessen "Schubrohre" (die Röhren, in denen die Antriebswellen laufen) werden hier durch die 2+2 (2 hinten, 2 vorn) grauen BS 30 nachgebildet.