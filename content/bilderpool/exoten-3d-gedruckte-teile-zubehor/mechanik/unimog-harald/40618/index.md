---
layout: "image"
title: "Pneumatische Kupplung"
date: "2015-03-07T18:08:18"
picture: "bastel1.jpg"
weight: "1"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/40618
imported:
- "2019"
_4images_image_id: "40618"
_4images_cat_id: "238"
_4images_user_id: "4"
_4images_image_date: "2015-03-07T18:08:18"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40618 -->
Das schwarze Kettenrad Z20 (39164,*nml*) hat keinen Federclip und läuft deshalb lose auf der Achse. Mittels Luftdruck wird es aber zwischen der hellblauen Membran und der Flachnabe eingeklemmt und nimmt dann die ganze Achse mit.

Für die Luftzufuhr habe ich im Zickzack Löcher gebohrt (mittig axial, quer nach außen, von 3/4 des Radius wieder in axialer Richtung) und die unerwünschten Löcher mit schwarzen Drehteilen wieder verschlossen. 

Im Querloch steckt ein Messingrohr 2 mm, das den Plexiglasträger mit der Achse verstiftet. Die Achse ist ein Messingrohr 4 mm. Sie ist am anderen Ende zugeklebt, hier vorne kommt ein Silikonschlauch (mit Schmiermittel und einem Kabelbinder als Manschette) drüber. 

Das gelbe ist ein bisschen Knetmasse, das durch die noch freie Bohrung bis hinter die Membran praktiziert wurde. Es sorgt dafür, dass die Membran nicht vollflächig ums Loch anliegen kann und damit überhaupt erst mal Druckluft dort eindringen kann.