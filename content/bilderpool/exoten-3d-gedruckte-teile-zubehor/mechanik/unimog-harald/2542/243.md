---
layout: "comment"
hidden: true
title: "243"
date: "2004-08-01T20:50:29"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
John Deere & CoGenau da, bei John Deere, habe ich auch hingeschaut und abgeschätzt, wo denn genau die Drehachse auf den Boden zeigt!


Zu den Zuckerrüben und den Pflügen und anderen Anbausachen:

Wir müssen wohl mal eine Norm aufstellen für die Dreipunkt-Anbaugeräte (also so etwas wie Kategorie I / II / III) und die Drehrichtung der Zapfwellen, damit die Traktoren und Geräte kompatibel werden.


Gruß,
Harald