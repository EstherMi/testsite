---
layout: "image"
title: "Federung"
date: "2006-05-01T14:20:04"
picture: "Jakob_ft-Bilder1.jpg"
weight: "13"
konstrukteure: 
- "Jakob Wolf"
fotografen:
- "Jakob Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FFBBII"
license: "unknown"
legacy_id:
- details/6189
imported:
- "2019"
_4images_image_id: "6189"
_4images_cat_id: "465"
_4images_user_id: "420"
_4images_image_date: "2006-05-01T14:20:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6189 -->
Die Feder habe ich aus einem Softer-Magazin ausgebaut.