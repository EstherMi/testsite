---
layout: "image"
title: "Freilaufmechanismus"
date: "2007-02-24T19:39:48"
picture: "freilaufmechanismus1.jpg"
weight: "11"
konstrukteure: 
- "Max Buiting"
fotografen:
- "Clubblatt ft Club NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "robvanbaal"
license: "unknown"
legacy_id:
- details/9144
imported:
- "2019"
_4images_image_id: "9144"
_4images_cat_id: "465"
_4images_user_id: "379"
_4images_image_date: "2007-02-24T19:39:48"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9144 -->
Ist ist ein altes Bericht aus das Clubblad des ftClub NL (1998) wo Max Buiting ein Modell von ein Freilaufmechanismus sehen last. Also Kraftübertragung gehen nur in einer Drehrichtung und es dreht durch in der anderen Richtung. 
Die Bilder sollen genügend deutlich sein es nach zu bauen. Wenn den noch fragen gibt zur NL Text, schreibe mich einfach an.