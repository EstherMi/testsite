---
layout: "image"
title: "In en uitschuifbare as"
date: "2014-01-27T18:35:31"
picture: "P1270082.jpg"
weight: "3"
konstrukteure: 
- "Ruurd"
fotografen:
- "Chef8"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chef8"
license: "unknown"
legacy_id:
- details/38132
imported:
- "2019"
_4images_image_id: "38132"
_4images_cat_id: "2837"
_4images_user_id: "838"
_4images_image_date: "2014-01-27T18:35:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38132 -->
Voorzien van rast koppelings huls