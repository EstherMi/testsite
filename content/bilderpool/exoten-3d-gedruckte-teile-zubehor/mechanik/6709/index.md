---
layout: "image"
title: "Fendt300-74"
date: "2004-11-01T10:20:22"
picture: "Fendt300-74.jpg"
weight: "17"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Traktor"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/6709
imported:
- "2019"
_4images_image_id: "6709"
_4images_cat_id: "465"
_4images_user_id: "4"
_4images_image_date: "2004-11-01T10:20:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6709 -->
Die Lenkwürfel habe ich etwas beschnitten, damit 1. der Lenkeinschlag größer wird (engere Kurven) und 2. die Felgen mit der Mutter nach außen besser draufpassen. Damit kommt auch der Drehpunkt beim Einschlagen näher an den Aufstandspunkt des Reifens heran, der Reifen dreht also eher auf der Stelle als um einen Punkt auf dem Boden herumzurollen.