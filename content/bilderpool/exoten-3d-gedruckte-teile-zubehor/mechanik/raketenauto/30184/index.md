---
layout: "image"
title: "Raketenauto"
date: "2011-03-03T15:41:32"
picture: "auto4.jpg"
weight: "4"
konstrukteure: 
- "Endlich"
fotografen:
- "M.Endlich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Endlich"
license: "unknown"
legacy_id:
- details/30184
imported:
- "2019"
_4images_image_id: "30184"
_4images_cat_id: "2241"
_4images_user_id: "1162"
_4images_image_date: "2011-03-03T15:41:32"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30184 -->
Die (ausgebrannte) Rakete, die das Auto nach vorne geschoben hat