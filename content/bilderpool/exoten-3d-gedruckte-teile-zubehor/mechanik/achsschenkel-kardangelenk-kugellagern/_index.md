---
layout: "overview"
title: "Achsschenkel mit Kardangelenk und Kugellagern"
date: 2019-12-17T18:00:56+01:00
legacy_id:
- categories/1074
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1074 --> 
45mm breiter Achsschenkel mit schwarzem Kardangelenk und Schneckenmuttern mit Kugellagern.