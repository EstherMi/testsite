---
layout: "overview"
title: "BSB"
date: 2019-12-17T18:01:04+01:00
legacy_id:
- categories/1921
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1921 --> 
Hatte bei einer Auktion ein paar Teile übrig und sah erstmal keine Chance für neue oder gebrauchte zu erwerben.  Aber  ich  hab ein Auge für evtl. passende Teile. So mußte die Abdeckung  einer Brausearmatur und ein Deckel eines Oelfläschchen und ein Möbelstopfen erstmal herhalten.