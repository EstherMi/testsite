---
layout: "image"
title: "EMMA3"
date: "2010-03-29T21:56:05"
picture: "bsb3.jpg"
weight: "2"
konstrukteure: 
- "Mike"
fotografen:
- "Mike"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "mike"
license: "unknown"
legacy_id:
- details/26850
imported:
- "2019"
_4images_image_id: "26850"
_4images_cat_id: "1921"
_4images_user_id: "1051"
_4images_image_date: "2010-03-29T21:56:05"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26850 -->
Hier schon mit Tendern und Zubehör - allerdings nur mit zwei Rädern pro Seite
