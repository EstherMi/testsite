---
layout: "image"
title: "Kegelrad 3"
date: "2010-01-01T14:38:46"
picture: "kegelrad3.jpg"
weight: "1"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Andreas Tacke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/26007
imported:
- "2019"
_4images_image_id: "26007"
_4images_cat_id: "1829"
_4images_user_id: "182"
_4images_image_date: "2010-01-01T14:38:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26007 -->
Hier ist zu sehen wie der Powermotor mit dem Kegelrad ins Raster paßt, paßt perfekt.