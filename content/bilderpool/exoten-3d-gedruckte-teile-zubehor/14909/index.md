---
layout: "image"
title: "Nabenschlüssel"
date: "2008-07-18T10:56:59"
picture: "Schlssel2.jpg"
weight: "32"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Andreas Tacke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/14909
imported:
- "2019"
_4images_image_id: "14909"
_4images_cat_id: "463"
_4images_user_id: "182"
_4images_image_date: "2008-07-18T10:56:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14909 -->
Dies ist ein Schlüssel zur Montage der Nabenmuttern. Er ist aus V2A gefertigt und diehnt dazu die Muttern richtig fest zu ziehen bzw. an schwer zugänglichen Stellen die Mutter fest zu halten.