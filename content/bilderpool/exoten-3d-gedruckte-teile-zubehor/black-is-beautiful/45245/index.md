---
layout: "image"
title: "Blacklight oder was?"
date: "2017-02-16T23:31:26"
picture: "bib6.jpg"
weight: "6"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- details/45245
imported:
- "2019"
_4images_image_id: "45245"
_4images_cat_id: "3368"
_4images_user_id: "381"
_4images_image_date: "2017-02-16T23:31:26"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45245 -->
Schwarzlicht wäre hier die richtige Bezeichnung - trifft es aber nicht wirklich.