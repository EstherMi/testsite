---
layout: "comment"
hidden: true
title: "20363"
date: "2015-03-14T19:12:58"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Festo hat auch ein "standard"-Alternativ: 

CN-M5-PK2 FES 19.521 (ca. 2 Euro/St) 

LCN-M5-PK2 FES 19.523 (ca. 3 Euro/St) 

FT-Baustein 15 mit Bohrung 32.064 

----------------------------------------------------------------------


Alternativ :
http://www.ftcommunity.de/details.php?image_id=7356#col3


Gruss, 

Peter Damen 
Poederoyen, Holland