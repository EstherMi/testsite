---
layout: "comment"
hidden: true
title: "815"
date: "2005-11-28T16:01:22"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Neid...Tjaaaa, jetzt sehe ich auch, wozu der große Durchlass in der Spindel gut ist! Meine Drehe hat nur 1 cm, und da kriege ich die Plexiglas-Röhrchen natürlich nicht mehr durch, sondern muss die Dinger erst mal grob ablängen, einspannen, abtrennen, ausspannen, einspannen, abtrennen usw, usw.

Der Dorn im Reitstock ist doch feststehend, oder dreht er mit? Bei feststehendem Dorn würde sich das Röhrchen doch irgendwann festschmelzen?