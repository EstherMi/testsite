---
layout: "image"
title: "Rohre"
date: "2005-11-27T18:04:01"
picture: "IMG_1710.jpg"
weight: "5"
konstrukteure: 
- "Michael Orlik//Sannchen90"
fotografen:
- "Michael Orlik//Sannchen90"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- details/5409
imported:
- "2019"
_4images_image_id: "5409"
_4images_cat_id: "464"
_4images_user_id: "6"
_4images_image_date: "2005-11-27T18:04:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5409 -->
Zum Abstechen der Rohre benutze ich auch wieder den Reitstock mit der Körnerspitze. Die Spitze wird auf das Maß eingerichtet und muß beim Abstechen der Rohre zurückgefahren werden, da sonst das Rohr klemmt. Hilfreich ist dazu eine Skala auf der Pinole. Der Support bleibt immer in der gleichen Position, ist mit der Leitspindel arretiert.

Funktioniert auch nur wenn man eine Drehspindel mit einem Durchmesser von min. 15mm hat.