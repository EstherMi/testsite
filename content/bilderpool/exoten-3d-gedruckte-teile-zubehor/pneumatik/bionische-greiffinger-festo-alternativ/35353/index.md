---
layout: "image"
title: "Bionische Greiffinger (Festo) + Fischertechnik"
date: "2012-08-24T20:58:37"
picture: "bionischegreiffingerfestodrosselventilalternativ3.jpg"
weight: "4"
konstrukteure: 
- "Peterholland"
fotografen:
- "Peterholland"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/35353
imported:
- "2019"
_4images_image_id: "35353"
_4images_cat_id: "2620"
_4images_user_id: "22"
_4images_image_date: "2012-08-24T20:58:37"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35353 -->
Der adaptive Greifer funktioniert wie einer Fischschwanzflosse. 
Zwei flexible Bänder laufen wie ein Dreieck in der Spitze zusammen.
Zwischenstege in regelmässigen Abständen sind über Gelenke mit den Bändern verbunden.
Durch diesen flexiblen, aber festen Verbund passen sich die Greiffinger der Kontur eines Werkstücks an.

Der bionische Greifer (Festo) wirdt im 3D Selective Laser Sintering-Verfahren aus Polyamidpulver hergestellt.