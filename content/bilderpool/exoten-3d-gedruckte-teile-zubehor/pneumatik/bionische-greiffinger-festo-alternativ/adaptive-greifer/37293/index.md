---
layout: "image"
title: "Adaptive Greifer"
date: "2013-09-01T19:58:43"
picture: "adaptivegreifer7.jpg"
weight: "7"
konstrukteure: 
- "Peterholland"
fotografen:
- "Peterholland"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/37293
imported:
- "2019"
_4images_image_id: "37293"
_4images_cat_id: "2775"
_4images_user_id: "22"
_4images_image_date: "2013-09-01T19:58:43"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37293 -->
Mit ein pneumatik Zylinder ist eine automatische Schließvorgang einfach möglich.

Statt ein pneumatiek Greifer-Antrieb wäre ein Antrieb mit einer Spindel auch möglich. Beim Schließvorgang muss man in RoboPro dann die Motorspannung überwachen. Schließt sich die Zange, steigt der Motorstrom stark an und die Spannung bricht ein. Damit lässt sich die Zange schnell und an unterschiedlichen Positionen abschalten. 
So lassen sich unterschiedliche geformte Gegenstände sicher greifen. Auch die Eischale geht nicht zu Bruch.
Notwendig ist aber eine stabilisierte Versorgungsspannung damit die Spannung reproduzierbar ist.
