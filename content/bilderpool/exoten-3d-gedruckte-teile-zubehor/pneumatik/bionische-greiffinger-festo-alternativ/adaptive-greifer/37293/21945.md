---
layout: "comment"
hidden: true
title: "21945"
date: "2016-04-20T20:43:09"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Pollin hat diese Luftpumpe neu im Programm :
http://www.pollin.de/shop/dt/ODI5OTY2OTk-/Bauelemente_Bauteile/Pumpen/Luftpumpe_DAYPOWER_LP36_12_12_V_.html

- Die Pumpe bringt bei 12V eine 4er-Kombo der klassischen blauen FT-Tanks in 4 Sek. auf 0,8bar - 1s pro Tank!
- Sie schafft es, bei einem geöffneten Tankanschluss, die Tanks dauerhaft auf 0,5bar zu halten!
- Sie läuft bei 1,3V (!) zuverlässig an, ab ca. 5-6V auch mit sinnvoller Förderleistung und ist schön leise.


----------------------------------------------------------------------------- 

Interessantes Alternativ (hohere Druck + Vakuum !) und funktioniert gut : 
https://iprototype.nl/docs/Vacuumpomp-specsheet.jpeg

https://iprototype.nl/products/robotics/servo-motors/Vacuumpomp 

------------------------------------------------------------------------- 

Zie ook :    http://forum.ftcommunity.de/viewtopic.php?f=15&t=3538&p=24757#p24757