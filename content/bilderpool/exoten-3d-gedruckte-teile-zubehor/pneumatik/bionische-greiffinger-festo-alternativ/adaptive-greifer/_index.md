---
layout: "overview"
title: "Adaptive Greifer"
date: 2019-12-17T18:00:18+01:00
legacy_id:
- categories/2775
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2775 --> 
Der adaptive Greifer funktioniert wie einer Fischschwanzflosse. 
Zwei flexible Bänder laufen wie ein Dreieck in der Spitze zusammen. 
Zwischenstege in regelmässigen Abständen sind über Gelenke mit den Bändern verbunden. 
Durch diesen flexiblen, aber festen Verbund passen sich die Greiffinger der Kontur eines Werkstücks an.
