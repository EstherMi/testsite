---
layout: "overview"
title: "FT-Muskelmotor"
date: 2019-12-17T18:00:08+01:00
legacy_id:
- categories/1709
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1709 --> 
Ein Nocken und ein gesperrtes Freilauflager übertragen die lineare Bewegung - erzeugt wie bei einer Muskelkontraktion - auf eine Welle und setzen diese in eine Drehbewegung um. [br][br]Bei Druckentlastung fährt der Pneumatischer Muskel wieder in seine Ursprungslänge zurück. Dank des entriegelten Freilauflagers wird diese Bewegung nicht auf die Welle übertragen. [br]Bei ehrere pneumatische Muskeln lässt sich eine gleichmäßige Drehbewegung erzeugen. 