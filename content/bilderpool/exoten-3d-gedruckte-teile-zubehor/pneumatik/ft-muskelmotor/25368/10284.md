---
layout: "comment"
hidden: true
title: "10284"
date: "2009-11-21T13:14:48"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Directer Link zum 3-Way-Ventilen 11.12.3.BE.12.Q.8.8 ( Sensortechnics ):
http://www.sensortechnics.com/download/series11-259.pdf

Gruss, 

Peter Damen 
Poederoyen NL