---
layout: "image"
title: "Muskel-3 nochmals"
date: "2009-08-30T09:28:51"
picture: "ftmuskelmotor6.jpg"
weight: "8"
konstrukteure: 
- "Peter Damen"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/24869
imported:
- "2019"
_4images_image_id: "24869"
_4images_cat_id: "1709"
_4images_user_id: "22"
_4images_image_date: "2009-08-30T09:28:51"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24869 -->
Ein Nocken und ein gesperrtes Freilauflager übertragen die lineare Bewegung - erzeugt wie bei einer Muskelkontraktion - auf eine Welle und setzen diese in eine Drehbewegung um. 

Bei Druckentlastung fährt der Pneumatischer Muskel wieder in seine Ursprungslänge zurück. Dank des entriegelten Freilauflagers wird diese Bewegung nicht auf die Welle übertragen. 
Bei ehrere pneumatische Muskeln lässt sich eine gleichmäßige Drehbewegung erzeugen.