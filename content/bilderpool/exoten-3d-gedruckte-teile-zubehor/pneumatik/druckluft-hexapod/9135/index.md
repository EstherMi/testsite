---
layout: "image"
title: "Übersicht"
date: "2007-02-23T10:51:38"
picture: "01-Hexapod.jpg"
weight: "1"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/9135
imported:
- "2019"
_4images_image_id: "9135"
_4images_cat_id: "830"
_4images_user_id: "46"
_4images_image_date: "2007-02-23T10:51:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9135 -->
Sechs Zylinder ergeben mit den beiden Dreiecken oben und unten einen annähernd oktaedrischen Rahmen.

Der einfache Aufbau erfordert aber eine etwas aufwändigere Steuerung, was sich in 12 Ventilen bemerkbar macht. Bedient werden die Zylinder bei diesem Demo-Modell einzeln manuell. Das Hexapod arbeitet sehr schnell, dafür kann es natürlich nicht exakt positionieren.

Der Druckluftbedarf ist enorm. Der im Hintergrund arbeitende Kompressor hält nicht mehr Schritt, wenn man auf der kleinen Tastatur Toccata und Fuge spielt oder abwechselnd alle Zylinder ein- und ausfährt.