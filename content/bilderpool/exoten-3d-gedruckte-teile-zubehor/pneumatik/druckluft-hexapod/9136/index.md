---
layout: "image"
title: "Hexapod"
date: "2007-02-23T10:51:38"
picture: "02-Hexapod.jpg"
weight: "2"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/9136
imported:
- "2019"
_4images_image_id: "9136"
_4images_cat_id: "830"
_4images_user_id: "46"
_4images_image_date: "2007-02-23T10:51:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9136 -->
Hier der Aufbau in der Grundstellung. Dadurch, daß man für die Zylinder keine extra Endschalter braucht, ist der mechanische Aufbau höchst elegant und einfach.

Die Verschlauchung macht da ein wenig Kummer, ist aber dennoch zu schaffen. 2 m Schlauch sind schnell verbaut.