---
layout: "image"
title: "Fischertechnik-Qualle  mit  40cm transparante Bal (2-delig Ballkit-code MT400TNL)"
date: "2013-08-31T23:18:49"
picture: "qualletransparantebal2.jpg"
weight: "2"
konstrukteure: 
- "Peterholland"
fotografen:
- "Peterholland"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/37284
imported:
- "2019"
_4images_image_id: "37284"
_4images_cat_id: "2774"
_4images_user_id: "22"
_4images_image_date: "2013-08-31T23:18:49"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37284 -->
Mit 8 hell-blaue Leds aus einer alte Taschenlampe gibt es dann noch mehr Effekt !