---
layout: "image"
title: "Detail Flaschenadapter"
date: "2011-07-04T12:19:52"
picture: "sigg2.jpg"
weight: "2"
konstrukteure: 
- "Martin W. (Masked)"
fotografen:
- "Martin W. (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/30987
imported:
- "2019"
_4images_image_id: "30987"
_4images_cat_id: "2316"
_4images_user_id: "373"
_4images_image_date: "2011-07-04T12:19:52"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30987 -->
Jeder Maschinenbautechniker, -geselle usw. darf sich entsetzt abwenden, aber ich habe keine entsprechende Ausbildung und auch keine so richtig passenden Werkzeuge. Die weißen "Streifen" sind Einschlüsse im Material des Stopfens, die beim Abschleifen zu Tage gekommen sind.
Vielleicht kann das ja jemand mit entsprechender Ahnung und Gerätschaften (TST?) nochmal machen? Evtl. lohnt sich ja sogar eine Kleinserie?