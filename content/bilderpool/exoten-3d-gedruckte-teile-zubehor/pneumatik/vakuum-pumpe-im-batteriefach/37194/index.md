---
layout: "image"
title: "Oesen und Deckel"
date: "2013-07-25T17:33:04"
picture: "vakuumpumpeimbatteriefach09.jpg"
weight: "9"
konstrukteure: 
- "Kai"
fotografen:
- "DasKasperle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- details/37194
imported:
- "2019"
_4images_image_id: "37194"
_4images_cat_id: "2764"
_4images_user_id: "1677"
_4images_image_date: "2013-07-25T17:33:04"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37194 -->
Mit dem Lötkolben Löcher in die Deckel gebrannt, durch die die Ösen mit den Steckern verbunden werden.