---
layout: "image"
title: "Schaumstoff als Schallisololator 2"
date: "2013-07-25T17:33:04"
picture: "vakuumpumpeimbatteriefach05.jpg"
weight: "5"
konstrukteure: 
- "Kai"
fotografen:
- "DasKasperle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- details/37190
imported:
- "2019"
_4images_image_id: "37190"
_4images_cat_id: "2764"
_4images_user_id: "1677"
_4images_image_date: "2013-07-25T17:33:04"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37190 -->
Könnte ein wenig dünner sein.
Stärke/Dicke 2,5 - 3 mm (besser wäre kleiner 2mm)