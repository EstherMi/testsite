---
layout: "image"
title: "Oesen"
date: "2013-07-25T17:33:04"
picture: "vakuumpumpeimbatteriefach07.jpg"
weight: "7"
konstrukteure: 
- "Kai"
fotografen:
- "DasKasperle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- details/37192
imported:
- "2019"
_4images_image_id: "37192"
_4images_cat_id: "2764"
_4images_user_id: "1677"
_4images_image_date: "2013-07-25T17:33:04"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37192 -->
Der Schalter wird nicht weggeworfen. Den kann man wiederverwenden.
Das leere Batteriefach wird mit Gewichten gefüllt.
