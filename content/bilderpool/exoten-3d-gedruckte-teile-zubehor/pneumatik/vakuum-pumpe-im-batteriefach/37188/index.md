---
layout: "image"
title: "Unterschahle mit Pumpe zum erstenmal vereint"
date: "2013-07-25T17:33:04"
picture: "vakuumpumpeimbatteriefach03.jpg"
weight: "3"
konstrukteure: 
- "Kai"
fotografen:
- "DasKasperle"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- details/37188
imported:
- "2019"
_4images_image_id: "37188"
_4images_cat_id: "2764"
_4images_user_id: "1677"
_4images_image_date: "2013-07-25T17:33:04"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37188 -->
Die Stutzen lassen sich abschrauben und um 4x 90° drehen.