---
layout: "image"
title: "Pneumatischer Muskel"
date: "2009-08-01T16:35:50"
picture: "zelfbouwpneumatischespierpneumatischermuskel2.jpg"
weight: "8"
konstrukteure: 
- "Peter Damen, Poederoyen NL"
fotografen:
- "Peter Damen, Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/24691
imported:
- "2019"
_4images_image_id: "24691"
_4images_cat_id: "1695"
_4images_user_id: "22"
_4images_image_date: "2009-08-01T16:35:50"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24691 -->
.	Interessant zijn : 
o	Website Shadow Robot Company:  http://www.shadowrobot.com/
o	http://www.shadowrobot.com/shop.shtml
o	http://de.manu-systems.com/Shadow.shtml

