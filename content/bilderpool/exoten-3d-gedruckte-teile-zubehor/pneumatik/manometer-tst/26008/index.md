---
layout: "image"
title: "Manometer Einbau"
date: "2010-01-01T14:38:47"
picture: "manometer1.jpg"
weight: "3"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/26008
imported:
- "2019"
_4images_image_id: "26008"
_4images_cat_id: "1834"
_4images_user_id: "182"
_4images_image_date: "2010-01-01T14:38:47"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26008 -->
Ich habe hier ein Gehäuse gefertigt um ein Standart Manometer ordentlich zu verpacken.