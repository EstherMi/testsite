---
layout: "image"
title: "Einzelteile Manometergehäuse"
date: "2010-01-01T14:38:47"
picture: "manometer2.jpg"
weight: "4"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/26009
imported:
- "2019"
_4images_image_id: "26009"
_4images_cat_id: "1834"
_4images_user_id: "182"
_4images_image_date: "2010-01-01T14:38:47"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26009 -->
Hier sind die Einzelteile zu sehen.
Rechts eine Standartkassette von ft
Mitte das Manometer
Links der Deckel. Hierbei handelt es sich um einen Kappe für ein 60er Vierkantrohr, paßt genau in die Kassette :))