---
layout: "comment"
hidden: true
title: "12997"
date: "2010-12-22T07:45:34"
uploadBy:
- "alfred.s"
license: "unknown"
imported:
- "2019"
---
Hallo Ho & Si,

stimmt schon, Silikonoel ist gut zu den Kunststoffen, aber es hat einen Nachteil gegenueber dem Weissoel: Es ist sehr fluessig. Daher geht das Oel auch leicht aus den Zylindern wieder raus, was erstens zu einer Sabberei rund um einen offenen Stutzen fuehren kann und zweitens zu einem recht bald wieder trockenen Zylinder fuehrt. Das ist zumindest meine Erfahrung mit Silikonoel und ft-Zylindern.

Gruesse
Alfred