---
layout: "image"
title: "Trichter"
date: "2008-09-12T22:45:53"
picture: "anleitungzumoelenvonzylindern5.jpg"
weight: "5"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/15228
imported:
- "2019"
_4images_image_id: "15228"
_4images_cat_id: "1393"
_4images_user_id: "747"
_4images_image_date: "2008-09-12T22:45:53"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15228 -->
Zuerst muss man den Schlauch auf den Zylinder stecken und mit einer Pipette ein bisschen Paraffinum Perliquidum hereinfüllen.