---
layout: "image"
title: "Schrägstellung"
date: "2008-09-12T22:45:53"
picture: "anleitungzumoelenvonzylindern4.jpg"
weight: "4"
konstrukteure: 
- "Johannes"
fotografen:
- "Johannes"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Johannes"
license: "unknown"
legacy_id:
- details/15227
imported:
- "2019"
_4images_image_id: "15227"
_4images_cat_id: "1393"
_4images_user_id: "747"
_4images_image_date: "2008-09-12T22:45:53"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15227 -->
Man legt das Ganze am besten etwas schräg auf einen Block o.ä., damit das Öl in den Schlauch herunter und in den Zylinder hereinfließt.