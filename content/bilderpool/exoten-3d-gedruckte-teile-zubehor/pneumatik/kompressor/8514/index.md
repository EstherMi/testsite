---
layout: "image"
title: "Manometer"
date: "2007-01-19T10:50:50"
picture: "kompressor5.jpg"
weight: "18"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/8514
imported:
- "2019"
_4images_image_id: "8514"
_4images_cat_id: "18"
_4images_user_id: "453"
_4images_image_date: "2007-01-19T10:50:50"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8514 -->
Der Kompressor bringt ungefähr 0,2 bar
Ich nehmme den auch für mein Industriemodell.