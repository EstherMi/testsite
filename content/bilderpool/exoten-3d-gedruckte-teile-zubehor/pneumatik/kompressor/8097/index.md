---
layout: "image"
title: "Kompressor2"
date: "2006-12-21T15:29:24"
picture: "Kompressor2.jpg"
weight: "20"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Andreas Tacke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/8097
imported:
- "2019"
_4images_image_id: "8097"
_4images_cat_id: "18"
_4images_user_id: "182"
_4images_image_date: "2006-12-21T15:29:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8097 -->
Hier ist der einstellbare Druckschalter zu sehen. Über eine Schraube im Baustein 15 mit Bohrung läßt sich die Federspannung und somit der Druck einstellen.