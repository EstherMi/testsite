---
layout: "image"
title: "Geregelter Kompressor"
date: "2006-12-04T21:21:11"
picture: "Kompressor_2.jpg"
weight: "22"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/7705
imported:
- "2019"
_4images_image_id: "7705"
_4images_cat_id: "18"
_4images_user_id: "46"
_4images_image_date: "2006-12-04T21:21:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7705 -->
Der Kompressor ist ein 24V-Typ von KNF, den ich mit einem Luftfilter ausgestattet habe. Daneben ein Manometer und unter den Drucktanks der elektronische Regler.

Der Regler ist etwas aufwendiger: abgleichbarer Halbleitersensor in einer 1 mA Konstantstromschleife, Signalverstärkung ca. 1:200 und Flip-Flop-Baugruppe mit unabhängig einstellbarer Ein- und Ausschaltschwelle. Die Elektronik arbeitet völlig unbeeindruckt von Spannungsschwankungen im Bereich 6 bis 15 Volt.

Im Moment ist er so eingestellt, daß sich der Kompressor bei 0,55 bar ausschaltet und bei 0,45 bar wieder anspringt. Damit brauche ich keinen weiteren Druckregler.