---
layout: "image"
title: "Staudinger kompressor"
date: "2008-02-03T20:59:07"
picture: "DSC06888_002.jpg"
weight: "8"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- details/13516
imported:
- "2019"
_4images_image_id: "13516"
_4images_cat_id: "18"
_4images_user_id: "473"
_4images_image_date: "2008-02-03T20:59:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13516 -->
