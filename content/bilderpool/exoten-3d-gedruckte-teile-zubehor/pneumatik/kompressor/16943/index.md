---
layout: "image"
title: "Kompressor mit Powermotor"
date: "2009-01-07T21:22:46"
picture: "KompressorNEU.jpg"
weight: "7"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Andreas Tacke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/16943
imported:
- "2019"
_4images_image_id: "16943"
_4images_cat_id: "18"
_4images_user_id: "182"
_4images_image_date: "2009-01-07T21:22:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16943 -->
Hier eine weitere Baustufe meines Kompressors. Dank des neuen Teil aus dem Kehrmaschinen Kasten ist die Verwendung der fischertechnikplatte möglich geworden.