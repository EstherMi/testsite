---
layout: "image"
title: "Membankompressor"
date: "2004-06-13T17:20:45"
picture: "Kompressor.jpg"
weight: "29"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["kompressor"]
uploadBy: "charly"
license: "unknown"
legacy_id:
- details/2529
imported:
- "2019"
_4images_image_id: "2529"
_4images_cat_id: "18"
_4images_user_id: "115"
_4images_image_date: "2004-06-13T17:20:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=2529 -->
Mit Selbstbau-Membrandruckschalter