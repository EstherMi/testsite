---
layout: "image"
title: "lemo4.jpg"
date: "2006-03-30T07:52:28"
picture: "dsc00614_resize.jpg"
weight: "26"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich (schnaggels)"
keywords: ["lemo", "druckschalter", "pumpe", "kompressor", "pneumatik", "flip-flop", "flipflop", "überdruck"]
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- details/5989
imported:
- "2019"
_4images_image_id: "5989"
_4images_cat_id: "18"
_4images_user_id: "120"
_4images_image_date: "2006-03-30T07:52:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5989 -->
Druck ist wieder abgefallen, Sensor öffnet und das FF schaltet die Pumpe wieder ein. Schalter muß auf B zwecks Sensor 1 Signal Umkehr.Drehregler 1 etwa auf 1. Regler 2 auf 0, Eingang egal (ist quasi immer aktiv)