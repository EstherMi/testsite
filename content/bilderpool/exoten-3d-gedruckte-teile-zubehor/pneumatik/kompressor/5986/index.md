---
layout: "image"
title: "dsc00611_resize.jpg"
date: "2006-03-30T07:52:28"
picture: "dsc00611_resize.jpg"
weight: "23"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich (schnaggels)"
keywords: ["lemo", "manometer", "pumpe", "kompressor", "pneumatik", "druckabschaltung"]
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- details/5986
imported:
- "2019"
_4images_image_id: "5986"
_4images_cat_id: "18"
_4images_user_id: "120"
_4images_image_date: "2006-03-30T07:52:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5986 -->
Die günstigere Lemo-Pumpe ist schon am FT-Akku recht stark und wurde hier bei 1 bar gestoppt.