---
layout: "comment"
hidden: true
title: "1241"
date: "2006-08-23T15:03:06"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
schau mal unter Sonderposten - 28€ ist für die Leistung ein recht "günstiges" Angebot!
MEMBRAN - KOLBENPUMPE für Druck und Vakuum
Technische Daten: Motor (BÜHLER 31Ø x 54 mm) Nennspannung 12 Volt, Stromaufnahme 160 - 250 mA, max. Druck 1,2 bar, Endvakuum -430 mbar, Trockenlaufsicher,selbstsaugend, Medium: Luft oder Wasser (500 mL/Min.), Gewicht 188 Gramm, Maße: (LxBxH) 76 x 30 x 53 mm, Anschlusstüllen 4 mm Ø