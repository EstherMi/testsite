---
layout: "image"
title: "Kompressor mit XM Motor und Druckschalter 3"
date: "2010-01-03T11:41:08"
picture: "kompressor3_2.jpg"
weight: "3"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/26012
imported:
- "2019"
_4images_image_id: "26012"
_4images_cat_id: "18"
_4images_user_id: "182"
_4images_image_date: "2010-01-03T11:41:08"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26012 -->
Hier der Minitaster der über den Hebel geschaltet wird.
Durch verschieben des Tasters kann der Druck eingestellt werden.
Man kann Ihn z.B. so einstellen das er bei 0,6 bar ab und bei 0,4 bar wieder ein schaltet.