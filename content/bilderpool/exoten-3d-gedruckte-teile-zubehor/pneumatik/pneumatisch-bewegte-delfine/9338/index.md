---
layout: "image"
title: "Pneumatik Dolfijnen, wave, dolfijn 1 oder 2 leidend"
date: "2007-03-07T14:18:25"
picture: "Pneumatik-Dolfijnen-Wave_003.jpg"
weight: "2"
konstrukteure: 
- "Peter Damen (Poederoyen Holland)"
fotografen:
- "Peter Damen (Poederoyen Holland)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/9338
imported:
- "2019"
_4images_image_id: "9338"
_4images_cat_id: "861"
_4images_user_id: "22"
_4images_image_date: "2007-03-07T14:18:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9338 -->
Das Robopro-programm gibt es beim Downloads