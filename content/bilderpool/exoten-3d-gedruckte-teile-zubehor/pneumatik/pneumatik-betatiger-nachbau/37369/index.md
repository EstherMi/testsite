---
layout: "image"
title: "Betätiger drucklos"
date: "2013-09-11T12:40:09"
picture: "Bettiger_drucklos.jpg"
weight: "5"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["Pneumatik", "pneumatischer", "Betätiger"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/37369
imported:
- "2019"
_4images_image_id: "37369"
_4images_cat_id: "311"
_4images_user_id: "1557"
_4images_image_date: "2013-09-11T12:40:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37369 -->
Ein frühes Experimentierstadium zeigt den Betätiger drucklos.