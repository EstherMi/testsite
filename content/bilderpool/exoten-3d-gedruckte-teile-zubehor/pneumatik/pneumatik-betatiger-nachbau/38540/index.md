---
layout: "image"
title: "Pneumatik-Drossel Nachbau"
date: "2014-04-12T09:33:09"
picture: "IMG_4425.jpg"
weight: "1"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lukas99h."
license: "unknown"
legacy_id:
- details/38540
imported:
- "2019"
_4images_image_id: "38540"
_4images_cat_id: "311"
_4images_user_id: "1631"
_4images_image_date: "2014-04-12T09:33:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38540 -->
Nachdem es die originalen Fischertechnikdrosseln leider nicht mehr gibt und wenn doch nur für viel Geld, habe ich es mal probiert diese nach zubauen.
Teile:
1x Schraube ( 3 oder 4 mm)
1x Rastadapter
Im Vergleich zu den Originalen kostet diese Version wirklich nichts!!