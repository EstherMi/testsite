---
layout: "comment"
hidden: true
title: "18911"
date: "2014-04-12T09:58:01"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Die Idee mit dem Rastadapter ist Klasse. Und das Rückschlagventil sieht auch "dazu gehörend" aus. Schick!
Gruß,
Stefan