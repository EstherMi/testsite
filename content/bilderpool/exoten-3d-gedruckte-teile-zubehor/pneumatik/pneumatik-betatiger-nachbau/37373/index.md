---
layout: "image"
title: "Kolbenstange"
date: "2013-09-11T12:40:09"
picture: "Kolbenstange.jpg"
weight: "9"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["Pneumatik", "pneumatischer", "Betätiger"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/37373
imported:
- "2019"
_4images_image_id: "37373"
_4images_cat_id: "311"
_4images_user_id: "1557"
_4images_image_date: "2013-09-11T12:40:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37373 -->
Die Kolbenstange besteht ausnahmslos aus ft-Teilen. Das Rad (36573) übernimmt die Kraft der Rollmembran, drückt - locker auf der Metallachse sitzend - gegen die Achsverschraubung, die Achsverschraubung überträgt die Kraft auf die Metallachse. Das Rad hat einen etwas größeren Durchmesser als die Achsverschraubung und da F = p * A gibt das etwas mehr Kraft bei gleichem Druck. 

Außerdem gibt es keine scharfen Kanten (z.B. an den Zahnflanken der Achsverschraubung). Zum Luftpumpenadapter paßt der Durchmesser des Rades obendrein auch besser.