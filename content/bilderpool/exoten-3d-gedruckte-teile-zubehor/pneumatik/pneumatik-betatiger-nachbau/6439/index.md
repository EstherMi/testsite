---
layout: "image"
title: "Betätiger.JPG"
date: "2006-06-20T15:17:25"
picture: "Bettiger.JPG"
weight: "24"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/6439
imported:
- "2019"
_4images_image_id: "6439"
_4images_cat_id: "311"
_4images_user_id: "4"
_4images_image_date: "2006-06-20T15:17:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6439 -->
Es geht auch einfacher...

Anstelle der Düse blau (31632) geht auch Messingrohr mit eingeklebtem Plastikrohr aus einem Wattestäbchen. Der O-Ring stammt aus einem Sortiment von Dichtungsringen für Armbanduhren und ist noch nicht das Gelbe vom Ei. Unter Druck schiebt sich Fingerkuppe vom Rad 23 herunter. Da muss also etwas strammeres drauf, ggf noch mit Klebstoff nachhelfen.