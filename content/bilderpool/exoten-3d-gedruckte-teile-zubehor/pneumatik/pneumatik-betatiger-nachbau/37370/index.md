---
layout: "image"
title: "... mit Druck beaufschlagt ..."
date: "2013-09-11T12:40:09"
picture: "Bettiger_druckbeaufschlagt.jpg"
weight: "6"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/37370
imported:
- "2019"
_4images_image_id: "37370"
_4images_cat_id: "311"
_4images_user_id: "1557"
_4images_image_date: "2013-09-11T12:40:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37370 -->
Da ist jetzt Druck drauf, die Rollmembran wölbt sich heraus, die Kolbenstange drückt nach rechts gegen die beiden Federn (36147).