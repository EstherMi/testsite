---
layout: "comment"
hidden: true
title: "17955"
date: "2013-04-22T21:37:06"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Ach wenn sie es nur lesen und des Betätigers Wiederauferstehung verfolgen würden! Dessen rund herum eingefaltete Membran mit dem großen Hub kriegt man nicht hin, und alles andere führt immer auf größere Durchmesser. Zum Drucken von Gummi reicht es bei den 3D-Druckern leider noch nicht.

Gruß,
Harald