---
layout: "image"
title: "P-Betätiger 1"
date: "2013-04-18T20:27:44"
picture: "bild1.jpg"
weight: "21"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/36845
imported:
- "2019"
_4images_image_id: "36845"
_4images_cat_id: "311"
_4images_user_id: "1624"
_4images_image_date: "2013-04-18T20:27:44"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36845 -->
Ich bin Haralds Idee mal nachgegangen und habe mir auch so einen Betätiger gebaut. Auf dem Schlauchanschluss (Stängel von einem Wattestäbchen) kleben einige Lagen Tesafilm, auf die das Rad 23 gesteckt wurde. Die Membran ist mit den ganz vielem Tesa gegen das Verrutschen gesichert. Das Ganze habe ich dann mit drei "komischen quadratischen Doppelklebematten oder so was" auf einer Platte 15x15 befestigt. Diese "komischen quadratischen Doppelklebematten oder so was" habe ich irgendwo bei meiner Modellbahn gefunden... weiß leider nicht mehr, wo die her sind, vielleicht kriegt man diese "komischen quadratischen Doppelklebematten oder so was" beim OBI... Halten tut's aber nicht so gut :-(