---
layout: "image"
title: "Pneumatik-Drossel Nachbau2"
date: "2014-04-12T15:33:37"
picture: "IMG_4430.jpg"
weight: "3"
konstrukteure: 
- "Lukas"
fotografen:
- "Lukas"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lukas99h."
license: "unknown"
legacy_id:
- details/38542
imported:
- "2019"
_4images_image_id: "38542"
_4images_cat_id: "311"
_4images_user_id: "1631"
_4images_image_date: "2014-04-12T15:33:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38542 -->
Hier sieht man die Drossel an sich. Ich habe nur die Schraube oben eingeschraubt und unten wo der Schlauch durch muss mit einem Akkuschrauber das Loch erweitert so das der Schlauch durch passt.