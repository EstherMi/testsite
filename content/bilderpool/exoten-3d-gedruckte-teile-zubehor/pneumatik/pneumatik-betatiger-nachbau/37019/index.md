---
layout: "image"
title: "P-Betätiger klein"
date: "2013-06-02T15:57:28"
picture: "bild1_2.jpg"
weight: "20"
konstrukteure: 
- "Phil"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/37019
imported:
- "2019"
_4images_image_id: "37019"
_4images_cat_id: "311"
_4images_user_id: "1624"
_4images_image_date: "2013-06-02T15:57:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37019 -->
Ein fehlgeschlagener Versuch, den Nachbau-Betätiger kleiner zu kriegen. Die Membran wird von den beiden BS 15 aud dem BS 15 mit Loch gehalten. Beim Aufschieben der BS 15 auf den Lochbaustein ist mir die Membran leider zerrissen.
Aber ich glaube nicht, dass der Betätiger auch mit einer unbeschädigten Membran zufriedenstellend funktioniert, weil der Hub viel zu klein ist. Um einen Taster zu betätigen, bräuchte man außerdem einen sehr großen Druck. Die Luft entweicht aber, bevor sie die Membran weit genug gegen den Taster drückt - und es pfeift.