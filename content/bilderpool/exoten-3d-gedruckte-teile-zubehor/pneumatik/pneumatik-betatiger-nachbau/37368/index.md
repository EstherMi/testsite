---
layout: "image"
title: "Improvisierter pneumatischer Betätiger mit Luftpumpe und Schalter"
date: "2013-09-11T12:40:09"
picture: "Bettiger_mit_Luftpumpe.jpg"
weight: "4"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["Pneumatik", "pneumatischer", "Betätiger"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/37368
imported:
- "2019"
_4images_image_id: "37368"
_4images_cat_id: "311"
_4images_user_id: "1557"
_4images_image_date: "2013-09-11T12:40:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37368 -->
Inspiriert von Euren zahlreichen Versuchen einen penumatischen Betätiger nachzubauen hat es mich auch gepackt. Hier ein funktionsfähiger Aufbau mit einer Rollmembran. Leider braucht es dazu einige nft-Teile.

Hinten ist der Luftspeicher, davor eine Luftpumpe von Pollin. Der Dreiwege-Hahn links stammt aus dem medizinischen Bedarf. Vorne ist der eigentliche Betätiger, der die Kolbenstange nach rechts auf den Taster drückt. Der Taster unterbricht bei einem bestimmten Luftdruck die Stromzufuhr zur Pumpe. Durch minimale Undichtigkeiten entweicht der Luftdruck langsam, der Taster drückt den Betätiger zurück und schaltet dabei auch die Pumpe wieder ein. Der Druck steigt langsam an...