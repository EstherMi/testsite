---
layout: "image"
title: "Überdruck!"
date: "2013-09-11T12:40:09"
picture: "Bettiger_berdruck.jpg"
weight: "7"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["Pneumatik", "pneumatischer", "Betätiger"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/37371
imported:
- "2019"
_4images_image_id: "37371"
_4images_cat_id: "311"
_4images_user_id: "1557"
_4images_image_date: "2013-09-11T12:40:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37371 -->
Uups, da habe ich den Kompressor erst im letzten Augenblick abgeschaltet. Der Taster ist auf diesem Bild noch nicht angeschlossen.