---
layout: "image"
title: "Mini-Drucklufttank Schlauch"
date: "2015-04-18T21:33:57"
picture: "miniluftdrucktank4.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DasKasperle"
license: "unknown"
legacy_id:
- details/40805
imported:
- "2019"
_4images_image_id: "40805"
_4images_cat_id: "3066"
_4images_user_id: "1677"
_4images_image_date: "2015-04-18T21:33:57"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40805 -->
Den Pneumatikschlauch wird ein 2-3 mal mit dem Isolierkabel (2cm breit, das auf dem Foto ist schmaler, hab das andere fürs Foto nicht gefunden)
