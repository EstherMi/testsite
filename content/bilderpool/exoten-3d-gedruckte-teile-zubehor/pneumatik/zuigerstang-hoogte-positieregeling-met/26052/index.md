---
layout: "image"
title: "Zuigerstang Hoogte-Positieregeling met US-Sensor"
date: "2010-01-09T18:42:38"
picture: "zuigerstanghoogtepositieregelingmetussensor2.jpg"
weight: "2"
konstrukteure: 
- "Peter Poederoyen NL"
fotografen:
- "Peter Poederoyen NL"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/26052
imported:
- "2019"
_4images_image_id: "26052"
_4images_cat_id: "1839"
_4images_user_id: "22"
_4images_image_date: "2010-01-09T18:42:38"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26052 -->
Zuigerstang Hoogte-Positieregeling met US-Sensor

Conclusies m.b.t. verder experimenteren tbv Fin-Ray Lachspiegel : 
&#8226;	Een (co-) sinusvormige beweging in de tijd is in RoboPro goed programmeerbaar. 
&#8226;	Pneumatische (co-) sinus-tijd-beweging voor een enkelwerkende terugverende cilinder is mogelijk met beperkt perslucht-gebruik.
&#8226;	Pneumatische cosinus-tijd-beweging voor een dubbelwerkende cilinder, met terugslagkleppen t.b.v. positie-behoud, is mogelijk  doch vergt (te) veel perslucht.
&#8226;	Elektrische aandrijving met vaste snelheid en een passende rusttijd in eind-posities geeft toch een mooier vloeiend natuurlijke golf-effect voor de Fin-Ray Lachspiegel.

Link : 
http://www.ftcommunity.de/categories.php?cat_id=2852