---
layout: "image"
title: "Mini-Kompressor nachbau mit Lemo-Solar Membranpumpe"
date: "2006-01-22T11:09:35"
picture: "Draufsicht_2.jpg"
weight: "6"
konstrukteure: 
- "Reiner"
fotografen:
- "Reiner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "78081"
license: "unknown"
legacy_id:
- details/5641
imported:
- "2019"
_4images_image_id: "5641"
_4images_cat_id: "487"
_4images_user_id: "405"
_4images_image_date: "2006-01-22T11:09:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5641 -->
Sorry leider hatte sich bei den letzten Bildern der Fehlerteufel eingeschlichen !!!

Hier nun die richtigen Bilder!!!

Als erstes habe ich also mir die Bauanleitung des mini-Kompressors zur Hand genommen und dann so modifiziert das die Lemo-Solar Membranpumpe mit auf die Platte paßt! Bei erstmaligem betrieb am Interface mußte ich dann aber feststellen das bei mehreren angeschlossenen Motoren der Motor schwächelt.(Eigentlich komisch da ich von Conrad Elektronik ein 9V/4200mA Netzteil am Interface benutze). Aber nun ja da ich ja auch noch 3 I/O Extension Module habe gesagt getan, habe ich kurzerhand den Kompressor sprich Membranpumpe + Endschalter für Druckabschaltung an das Erwieterungsmodul angeklemmt und mit dem Interface verbunden alerdings habe ich das Erweiterungsmodul mit einem extra Netzteil mit 9V/1000mA versorgt!
So arbeitet der Kompressor immer einwandfrei und liefert auch genug druck bei 9V!!!

P.S. die Druckluftleitung vom Kompressor habe ich deswegen aufgeteilt damit bei druckabfall nicht die Luft erst durch den einen in den anderen Druckbehälter muß sondern auch am Ausgangsdruckbehälter direkt wieder der volle druck zur verfügung steht habe alles durchprobiert das war meiner meinung nach die beste Lösung!! Auf jeden fall lohnt sich die Investition in die Membranpumpe für 69€