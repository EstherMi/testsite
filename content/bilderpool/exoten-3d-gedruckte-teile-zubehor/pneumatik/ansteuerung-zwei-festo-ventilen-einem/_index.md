---
layout: "overview"
title: "Ansteuerung von zwei Festo-Ventilen mit einem Elektromagneten"
date: 2019-12-17T18:00:03+01:00
legacy_id:
- categories/1370
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1370 --> 
Für die Digitaluhr in der Version 2 muss ich mit einem Elektromagneten zwei der leider nicht mehr hergestellten Festo-Ventile ansteuern: je einen Öffner (mit blauem Stößel) und einen Schließer (mit rotem Stößel). Die ersten beiden hier vorgestellten Varianten funktionierten nicht zuverlässig, weil der Elektromagnet einfach nicht die notwendige Kraft aufbringt, beide Ventile gleichzeitig vollständig gegen den Luftdruck durchzuschalten. Die dritte Variante sollte das Gewünschte leisten.