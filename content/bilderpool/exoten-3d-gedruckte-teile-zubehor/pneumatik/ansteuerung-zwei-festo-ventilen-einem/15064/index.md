---
layout: "image"
title: "Direkte Betätigung (1)"
date: "2008-08-22T19:50:13"
picture: "magnetventile1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/15064
imported:
- "2019"
_4images_image_id: "15064"
_4images_cat_id: "1370"
_4images_user_id: "104"
_4images_image_date: "2008-08-22T19:50:13"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15064 -->
Diese Variante ist sehr ähnlich aufgebaut wie die in der Anleitung zum Pneumatik/Pneumatik Plus-Kasten aufgeführte zur Ansteuerung nur eines Ventils. Der BS7,5 und der BS30 unten sind unterhalb der Ventile (hier also nicht sichtbar) mit einer roten Platte 15x45 mit zwei Zapfen verbunden. Der Magnet sollte an der Rückschlussplatte und damit den BS30 mit der roten Platte 15x30 drauf nach oben ziehen und so beide Ventile betätigen. Die Stößel der Ventile zeigen nach unten und sollten also von der roten Platte 15x30 eingedrückt werden.

Leider funktionierte das nicht zuverlässig. Je nach aktuellem Luftdruck (und ich brauche nur 0,3 - 0,4 bar) wir eines der Ventile nicht eingedrückt, das bewegliche Teil steht dann schräg und die Baugruppe verliert sehr schnell Luft.