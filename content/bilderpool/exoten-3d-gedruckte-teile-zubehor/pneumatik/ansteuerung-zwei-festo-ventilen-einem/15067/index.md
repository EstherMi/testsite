---
layout: "image"
title: "Schräge Betätigung (2)"
date: "2008-08-22T19:50:14"
picture: "magnetventile4.jpg"
weight: "4"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/15067
imported:
- "2019"
_4images_image_id: "15067"
_4images_cat_id: "1370"
_4images_user_id: "104"
_4images_image_date: "2008-08-22T19:50:14"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15067 -->
Leider erwies sich aber auch hier, dass auch diese Anordnung immer noch nicht genug Kraft aufbringt, wenn die Ventile unter Luftdruck stehen. Es ergibt sich durch die Kraft, mit der die Ventile ihre Stößel nach außen drücken, eine zusätzliche Reibung beim Hereinziehen der Winkelsteine rechtwinklig. Die Ventile wurden nicht sauber durchgeschaltet und verloren wieder Luft.