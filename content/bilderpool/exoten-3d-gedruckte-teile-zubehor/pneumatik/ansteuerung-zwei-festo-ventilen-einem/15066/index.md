---
layout: "image"
title: "Schräge Betätigung (1)"
date: "2008-08-22T19:50:14"
picture: "magnetventile3.jpg"
weight: "3"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/15066
imported:
- "2019"
_4images_image_id: "15066"
_4images_cat_id: "1370"
_4images_user_id: "104"
_4images_image_date: "2008-08-22T19:50:14"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15066 -->
Da die Kraft für die direkte Betätigung nicht ausreichte, versuchte ich eine schräge Betätigung. Die Hoffnung war, dass der nun notwendige längere Hubweg möglich war und die wegen der Schräge notwendige Kraft zum zuverlässigen Schalten der Ventile nun ausreichte. Außerdem sollte diese Anordnung den Vorteil haben, sich selbst zu zentrieren. Die immer noch verwendete Zugplatte, hier eine 15x30 mit zwei Zapfen, die die beiden BS7,5 verbindet, ist hier gegen verdrehen durch die beiden Winkelelemente und gegen herausfallen durch die Überlappung mit den beiden BS30 gleichschenklig geschützt. Die beiden BS30 rechtwinklig (die auch nicht mehr hergestellt werden und aus den ft-Anfangszeiten stammen) würden die Ventile eindrücken.

Das Eindrücken war (ohne Luftdruck) sogar so stark, dass die Ventile nach der vollständigen Betätigung weiter, und zwar auseinander gedrückt wurden. Die Winkelelemente wurden gegenüber der schwarzen Bauplatte leicht auseinander gedreht. Tatsächlich reichte das zusammen mit der Rückstellkraft der beiden Ventile aus, um nach Wegfall der Spannung am Magneten die Anordnung wieder ganz zurück zu schieben, so dass beide Ventile wieder vollständig unbetätigt sind.