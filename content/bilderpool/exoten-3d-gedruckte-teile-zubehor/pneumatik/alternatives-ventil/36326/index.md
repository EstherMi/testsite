---
layout: "image"
title: "02 Alternatives Ventil"
date: "2012-12-18T21:38:26"
picture: "ventil2.jpg"
weight: "2"
konstrukteure: 
- "-Matthias-"
fotografen:
- "-Matthias-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-Matthias-"
license: "unknown"
legacy_id:
- details/36326
imported:
- "2019"
_4images_image_id: "36326"
_4images_cat_id: "2695"
_4images_user_id: "860"
_4images_image_date: "2012-12-18T21:38:26"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36326 -->
Um einen Zylinder ohne Feder ein- und auszufahren, braucht man zwei ft-Ventile oder vier von diesen.

Wenn der Zylinder nach unten geht, kann die Luft nicht durch das rechte Ventil entweichen, da dieses geschlossen ist. Deshalb braucht man noch ein zweites, das immer den anderen Zustand annimmt (also links auf, wenn rechts zu). 

Kurzes Video: http://www.youtube.com/watch?v=NPYvVA3zlxU