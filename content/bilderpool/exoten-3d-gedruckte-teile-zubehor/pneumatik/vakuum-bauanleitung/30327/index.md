---
layout: "image"
title: "Vakuumspeicher"
date: "2011-03-26T21:28:35"
picture: "vakuum06.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/30327
imported:
- "2019"
_4images_image_id: "30327"
_4images_cat_id: "2256"
_4images_user_id: "453"
_4images_image_date: "2011-03-26T21:28:35"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30327 -->
Mit einer spitzen Schere o.ä. kann man 3 kleine Löcher in den Deckel machen, durch die Löchen kommen drei (Wattestäbchen-) Röhrchen.
Hier besonders auf Dichtheit achten! 

Das Rückschlagventil wird folgendermaßen angeschlossen:

Loch	Anschluss im Glas	Anschluss außen am Glas
1	Ausgang des Rückschlagventils	Offen
2	Eingang des Rückschalgventils	Kompressor
3	Offen	Über Ventil: Saugnapf,
außerdem "Sicherung" (s.u.)


 

Statt einem Nutellaglas ist z.B. auch ein Behälter der Marke "Curver" sehr gut geeignet.

Der Trick an der Sache ist weitgehend der, dass das Rückschlagventil "falschherum" verwendet wird:
Die Luft wird damit nicht in einen Luftspeicher gepumpt, sondern aus einem solchen heraus.
Man kann von daher praktisch jede Art von Kompressor verwenden. 

Achtung: Abschaltmechanismus bei zu hohem Unterdruck verwenden!
Dazu ist z.B. die "Sicherung" von Möglichkeit 3 (s.u.) verwendbar, ein Zylinder mit Feder wird am unteren Anschluss direkt an den Vakuumspeicher angeschlossen und löst, wenn der Unterdruck zu stark wird, einen Taster aus, der den Kompressor abschaltet.
Diese Sicherung kann z.B. über ein T-Stück in die Leitung vom Anschluss 3 des Vakuumspeichers zum Ventil für den Saugnapf gelegt werden.