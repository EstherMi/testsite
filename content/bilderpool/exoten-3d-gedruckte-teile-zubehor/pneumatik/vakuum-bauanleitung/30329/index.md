---
layout: "image"
title: "Pumpe"
date: "2011-03-26T21:28:35"
picture: "vakuum08.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/30329
imported:
- "2019"
_4images_image_id: "30329"
_4images_cat_id: "2256"
_4images_user_id: "453"
_4images_image_date: "2011-03-26T21:28:35"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30329 -->
Funktionsweise:
Der grüne Zylinder ist der Antriebszylinder, der ständig abwechselnd Luft ansaugt und zurückpresst. 

Wird Luft gepresst:
Die Luft drückt von oben auf Ventil 1, dieses sperrt somit.
Bei Ventil 2 wird die Stahlkugel angehoben und die Luft entweicht nach außen. 

Wird Luft angesaugt:
Ventil 2 sperrt, und somit wird die Kugel von Ventil 1 angesaugt und das Ventil leitet:
Luft wird aus dem angeschlossenen Vakuumspeicher gesaugt.
Durch das Gewicht der Kugeln dichten die Ventile sofort nach der Betätigung wieder selbständig ab. 

Sehr wichtig: Die Unterdrucksicherung
(Auf dem unteren Bild links zu sehen):
Ein Federzylinder wird am unteren Anschluss mit dem Vakuumspeicher verbunden:
Wird die Feder durch starken Unterdruck heruntergezogen, wird ein Taster gedrückt, der die Stromzufuhr des Kompressors unterbricht.
Lässt der Unterdruck nach, wird der Taster durch die feder wieder deaktiviert und der Motor läuft wieder an.
Achtung: Diese Sicherung ist unbedingt notwendig!
Wenn man sie weglässt kann der Antriebszylinder durch den zu hohen Unterdruck zerstört werden.