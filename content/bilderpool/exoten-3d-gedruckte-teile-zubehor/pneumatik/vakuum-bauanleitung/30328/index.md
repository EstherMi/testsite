---
layout: "image"
title: "Pumpe"
date: "2011-03-26T21:28:35"
picture: "vakuum07.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/30328
imported:
- "2019"
_4images_image_id: "30328"
_4images_cat_id: "2256"
_4images_user_id: "453"
_4images_image_date: "2011-03-26T21:28:35"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30328 -->
Vakuum mit Pumpe und Selbstbau-Rückschlagventil: 

Hierzu ist eine kräftige Pumpe angesagt, diese kann z.B. so aussehen:
Achtung: Nur mit Unterdrucksicherung betreiben!

Die 2 Selbstbauventile:
Dafür braucht man:
2 Stücke Schlauch von etwa 1cm und 2 Stahlkugeln mit einem Durchmesser von etwa 2mm.
Wie auf dem Bild zu sehen müssen sie Senkrecht angeordnet werden.
Der Schlauchinnendurchmesser muss größer als der Durchmesser der Stahlkugel sein:
Bei Druck von oben oder Unterdruck von unten muss die Kugel das Ventil abdichten, bei Unterdruck von oben oder Druck von unten muss die Kugel "schweben" und das Ventil die Luft durchlassen. 

Anschluss:
Nur der untere Anschluss des grünen Antriebszylinders ist belegt, er ist mit der Oberseite des Ventils 1 (das linke auf dem Bild) und der Unterseite des Ventils 2 (das rechte) verbunden.
Die Unterseite von Ventil 1 ist mit dem "Vakuumspeicher" verbunden, die Oberseite von Ventil 2 ist offen.
Die Vakuumspeicher sind untereinander in Verbindung, es geht ein Schlauch zur "Sicherung" (s.u.) und zum Ventil mit dem Saugnapf ab.