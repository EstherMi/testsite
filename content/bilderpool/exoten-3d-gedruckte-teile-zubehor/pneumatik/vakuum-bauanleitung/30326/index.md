---
layout: "image"
title: "Vakuumspeicher"
date: "2011-03-26T21:28:35"
picture: "vakuum05.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/30326
imported:
- "2019"
_4images_image_id: "30326"
_4images_cat_id: "2256"
_4images_user_id: "453"
_4images_image_date: "2011-03-26T21:28:35"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30326 -->
Wichtig: Die Pappunterlage des Deckels nicht entfernen, sie dient als Dichtung! (Siehe Bild weiter unten)

Als Verbindung zwischen Außen- und Innenseite des Glases kann man z.B. Wattestäbchen verwenden, diese haben genau die richtige Größe: