---
layout: "image"
title: "Exzenter"
date: "2013-01-05T15:41:06"
picture: "exzenter1.jpg"
weight: "10"
konstrukteure: 
- "Jan Werner"
fotografen:
- "Jan Werner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "werner"
license: "unknown"
legacy_id:
- details/36408
imported:
- "2019"
_4images_image_id: "36408"
_4images_cat_id: "463"
_4images_user_id: "1196"
_4images_image_date: "2013-01-05T15:41:06"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36408 -->
Die Löcher des Exzenters liegen 11mm auseinander