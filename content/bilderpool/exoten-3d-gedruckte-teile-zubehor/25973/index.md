---
layout: "image"
title: "Meine erste Teilemodifikation 2/2"
date: "2009-12-21T19:36:11"
picture: "teilemodifikation2.jpg"
weight: "27"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- details/25973
imported:
- "2019"
_4images_image_id: "25973"
_4images_cat_id: "463"
_4images_user_id: "998"
_4images_image_date: "2009-12-21T19:36:11"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25973 -->
Verwendung als Kranhaken