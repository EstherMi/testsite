---
layout: "image"
title: "Gabellichtschranke mit Sharp GP 1A52HR J00F"
date: "2008-02-13T18:49:21"
picture: "FLB_top_m.jpg"
weight: "2"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Gabellichtschranke", "Drehwinkel", "Position", "Rotation", "Geschwindigkeit", "transparente", "schwarze", "Segmentscheibe", "Segment"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/13645
imported:
- "2019"
_4images_image_id: "13645"
_4images_cat_id: "694"
_4images_user_id: "579"
_4images_image_date: "2008-02-13T18:49:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13645 -->
Für transparente/schwarze Segmentscheibe zum Messen von Drehwinkel, Position, Rotation und Geschwindigkeit.

Versorgung: +5V, GND
Output: digital "high" oder "low"

siehe auch

[url]http://home.arcor.de/uffmann/Electronics.html[/url]