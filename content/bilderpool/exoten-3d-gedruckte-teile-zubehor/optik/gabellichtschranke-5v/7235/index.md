---
layout: "image"
title: "With FT cable and isolation..."
date: "2006-10-28T08:17:34"
picture: "schanke_003.jpg"
weight: "6"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/7235
imported:
- "2019"
_4images_image_id: "7235"
_4images_cat_id: "694"
_4images_user_id: "371"
_4images_image_date: "2006-10-28T08:17:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7235 -->
yes... more like a cable... no PCB needed.