---
layout: "image"
title: "Diode und DC-DC Wandler (3V)"
date: "2013-04-21T08:03:03"
picture: "IMG_9840.jpg"
weight: "2"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: ["Laserdiode", "DC", "DC", "Wandler"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/36854
imported:
- "2019"
_4images_image_id: "36854"
_4images_cat_id: "2737"
_4images_user_id: "1359"
_4images_image_date: "2013-04-21T08:03:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36854 -->
Die Diode im Betrieb mit 3V Spannungsversorgung