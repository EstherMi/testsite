---
layout: "image"
title: "X-Achse"
date: "2011-01-30T14:03:27"
picture: "scanner04.jpg"
weight: "4"
konstrukteure: 
- "Martin S"
fotografen:
- "Martin S"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin S"
license: "unknown"
legacy_id:
- details/29829
imported:
- "2019"
_4images_image_id: "29829"
_4images_cat_id: "2196"
_4images_user_id: "1264"
_4images_image_date: "2011-01-30T14:03:27"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29829 -->
Da sieht man die X-Achse
Der Taster links oben ist der Endtaster, der drunter ist der Impulstaster.