---
layout: "image"
title: "Versuch N. 1"
date: "2011-01-30T14:03:27"
picture: "scanner08.jpg"
weight: "8"
konstrukteure: 
- "Martin S"
fotografen:
- "Martin S"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin S"
license: "unknown"
legacy_id:
- details/29833
imported:
- "2019"
_4images_image_id: "29833"
_4images_cat_id: "2196"
_4images_user_id: "1264"
_4images_image_date: "2011-01-30T14:03:27"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29833 -->
Bei meinem ersten Versuch habe ich diesen Bild verwendet.