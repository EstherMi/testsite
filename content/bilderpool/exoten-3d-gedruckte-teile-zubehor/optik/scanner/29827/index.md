---
layout: "image"
title: "Scannkopf"
date: "2011-01-30T14:03:27"
picture: "scanner02.jpg"
weight: "2"
konstrukteure: 
- "Martin S"
fotografen:
- "Martin S"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin S"
license: "unknown"
legacy_id:
- details/29827
imported:
- "2019"
_4images_image_id: "29827"
_4images_cat_id: "2196"
_4images_user_id: "1264"
_4images_image_date: "2011-01-30T14:03:27"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29827 -->
Das ist der Scannkopf der das Bild abfährt