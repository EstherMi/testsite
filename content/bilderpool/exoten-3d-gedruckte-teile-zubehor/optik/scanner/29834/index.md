---
layout: "image"
title: "Resultat vom ersten Versuch"
date: "2011-01-30T14:03:27"
picture: "scanner09.jpg"
weight: "9"
konstrukteure: 
- "Martin S"
fotografen:
- "Martin S"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin S"
license: "unknown"
legacy_id:
- details/29834
imported:
- "2019"
_4images_image_id: "29834"
_4images_cat_id: "2196"
_4images_user_id: "1264"
_4images_image_date: "2011-01-30T14:03:27"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29834 -->
Das Bild ist noch etwas verzogen aber sonst ganz ordentlich (für meine Verhälnisse)