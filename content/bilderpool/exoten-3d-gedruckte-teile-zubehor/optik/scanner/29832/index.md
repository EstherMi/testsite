---
layout: "image"
title: "Anschlüsse"
date: "2011-01-30T14:03:27"
picture: "scanner07.jpg"
weight: "7"
konstrukteure: 
- "Martin S"
fotografen:
- "Martin S"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin S"
license: "unknown"
legacy_id:
- details/29832
imported:
- "2019"
_4images_image_id: "29832"
_4images_cat_id: "2196"
_4images_user_id: "1264"
_4images_image_date: "2011-01-30T14:03:27"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29832 -->
Der Anschluss ganz rechts ist für die Stromversorgung