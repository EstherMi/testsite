---
layout: "image"
title: "Farbsensor"
date: "2011-01-30T14:03:27"
picture: "scanner03.jpg"
weight: "3"
konstrukteure: 
- "Martin S"
fotografen:
- "Martin S"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin S"
license: "unknown"
legacy_id:
- details/29828
imported:
- "2019"
_4images_image_id: "29828"
_4images_cat_id: "2196"
_4images_user_id: "1264"
_4images_image_date: "2011-01-30T14:03:27"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29828 -->
Das ist der Farbsensor mit einer kleinen Linse drauf damit der Lichtkegel kleiner wird.