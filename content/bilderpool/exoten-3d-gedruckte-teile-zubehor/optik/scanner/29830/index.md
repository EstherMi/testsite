---
layout: "image"
title: "Y-Achse"
date: "2011-01-30T14:03:27"
picture: "scanner05.jpg"
weight: "5"
konstrukteure: 
- "Martin S"
fotografen:
- "Martin S"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin S"
license: "unknown"
legacy_id:
- details/29830
imported:
- "2019"
_4images_image_id: "29830"
_4images_cat_id: "2196"
_4images_user_id: "1264"
_4images_image_date: "2011-01-30T14:03:27"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29830 -->
Links ist der Endtaster, rechts davon (etwas schwer zu erkennen)  der Impulstaster.