---
layout: "comment"
hidden: true
title: "7767"
date: "2008-11-14T18:45:43"
uploadBy:
- "remadus"
license: "unknown"
imported:
- "2019"
---
Hallo,

die Sache mit dem Fototransistor geht sehr wohl, wenn du die Werkstücke nacheinander mit einer roten, einer grünen und einer blauen LED anleuchtest. Jedesmal den Helligkeitswert aufnehmen und ins Verhältnis zueinander setzen. Schon kannst du dutzende von Farben unterscheiden.

Als dann
Remadus