---
layout: "image"
title: "Ein Selbstbauspiegel"
date: "2009-06-15T22:51:46"
picture: "selbstbauspiegel1.jpg"
weight: "1"
konstrukteure: 
- "ChiemgauN"
fotografen:
- "ChiemgauN"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ChiemgauN"
license: "unknown"
legacy_id:
- details/24392
imported:
- "2019"
_4images_image_id: "24392"
_4images_cat_id: "1672"
_4images_user_id: "969"
_4images_image_date: "2009-06-15T22:51:46"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24392 -->
Eine rote Bauplatte (38241) mit einem aufgeklebten Spiegel. Der Spiegel hält bombenfest (nach 6h torcknen).