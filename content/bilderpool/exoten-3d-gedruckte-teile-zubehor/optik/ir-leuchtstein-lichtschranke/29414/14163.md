---
layout: "comment"
hidden: true
title: "14163"
date: "2011-04-21T17:05:09"
uploadBy:
- "tz"
license: "unknown"
imported:
- "2019"
---
Nein, kein spezieller IR-Fototransistor :)

Das geht absolut problemlos mit dem original FT Fototransistor 

und ist wesentlich weniger störanfällig als mit den FT-Lampen.

kann ich nur empfehlen :)