---
layout: "image"
title: "IR Leuchtstein Nahaufnahme 2"
date: "2010-12-05T15:11:00"
picture: "IMG_0002_LD_274_330ohm.jpg"
weight: "2"
konstrukteure: 
- "tz"
fotografen:
- "tz"
keywords: ["IR", "Leuchtstein", "Lichtschranke", "Infrarot"]
uploadBy: "tz"
license: "unknown"
legacy_id:
- details/29415
imported:
- "2019"
_4images_image_id: "29415"
_4images_cat_id: "2138"
_4images_user_id: "941"
_4images_image_date: "2010-12-05T15:11:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29415 -->
Eigenbau IR Leuchtstein für eine Lichtschranke. 

Bestehend aus einer IR-Diode LD 274 und einem 330 Ohm Widerstand.

Gebaut als Ersatz für die Fischertechnik Stecklampen. Dadurch erreicht man weniger Stromverbrauch, praktisch keine Wäremeentwicklung und natürlich keinen sichtbaren Lichtschein mehr.

Wird an allen Lichtschranken in meiner Taktstrasse mit Sortierung verwendet.

Siehe Bilder: http://ftcommunity.de/categories.php?cat_id=1625

Siehe Video: http://www.youtube.com/watch?v=O8NaB7HjB5U