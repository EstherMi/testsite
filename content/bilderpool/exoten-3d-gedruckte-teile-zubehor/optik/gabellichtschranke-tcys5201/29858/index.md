---
layout: "image"
title: "Segmentscheiben für Lichtschranke TCYS 5201"
date: "2011-02-03T22:24:29"
picture: "segmentscheiben1.jpg"
weight: "1"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/29858
imported:
- "2019"
_4images_image_id: "29858"
_4images_cat_id: "2115"
_4images_user_id: "182"
_4images_image_date: "2011-02-03T22:24:29"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29858 -->
Ich habe hier verschiedene Segmentscheiben die ich für die Gabellichtschranke TCYS 5201 nutzen möchte.
Sie haben einen Außendurchmesser von 30mm, und mit der Messingnabe einen ID von 4mm.

Von links nach rechts:
4 , 8 , 19 , 31 , 47 er Teilungen

Zu Erklärung:

19er ergibt bei der M1,5 Schnecke pro Impulse eine Wegstrecke von 0,248mm
47er ergibt bei der M1,5 Schnecke pro Impulse eine Wegstrecke von 0,1mm

31er ergibt beim Drehkranz ( in Verbindung mit der Schnecke) pro Impulse 0,2°

Ich habe diese Teilungen gewählt um beim Programmieren die Tatsächlichen Strecken besser in Impulse umzurechnen.