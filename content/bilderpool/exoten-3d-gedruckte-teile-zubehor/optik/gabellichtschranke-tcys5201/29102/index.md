---
layout: "image"
title: "Abfrage"
date: "2010-10-31T17:32:22"
picture: "Bild4.jpg"
weight: "3"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Andreas Tacke"
keywords: ["Gabellichtschranke"]
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/29102
imported:
- "2019"
_4images_image_id: "29102"
_4images_cat_id: "2115"
_4images_user_id: "182"
_4images_image_date: "2010-10-31T17:32:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29102 -->
Durch den 5mm breiten Ausschnitt kann man auch mit einer Kette Impulse abfragen. Somit läßt sich mit einer gespannten Kette eine absolut Messung bei Linearbewegungen machen,