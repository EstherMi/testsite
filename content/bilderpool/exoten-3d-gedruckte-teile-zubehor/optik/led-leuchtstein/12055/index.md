---
layout: "image"
title: "Anleitung zum Bau  des Led-Leuchtsteines (1)"
date: "2007-09-29T15:48:13"
picture: "1.Schritt.jpg"
weight: "29"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: ["Anleitung", "bauen", "bau", "led", "Leuchtstein", "löten", "widerstand", "licht", "lötkolben"]
uploadBy: "Sebo"
license: "unknown"
legacy_id:
- details/12055
imported:
- "2019"
_4images_image_id: "12055"
_4images_cat_id: "1073"
_4images_user_id: "650"
_4images_image_date: "2007-09-29T15:48:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12055 -->
Hier der erste Teil zum Nachbauen des Led-
Leuchtsteines.
Benötigtes Material und Werkzeug:
-eine beliebige Led (20mA Stromaufname)
-einen Widerstand mit 470Ohm
-einen ft-Leuchtstein
-etwas Lötzinn
-Lötkolben
-Seitenschneider und ggf. eine Zange

Zuerst kürzt und verbiegt man die Drähte der jeweiligen Komponenten wie auf dem Bild. Der längere Draht der Led wird auf ungefähr 2-3mm gekürzt.  So auch ein Ende des Widerstandes. In diesem Fall kann man sagen, je kürzer, desto besser, da dann mehr Platz für die Leuchtkappe vorhanden ist. Allerdings wird es in dieser Hinsicht schwerer zu löten sein.
Anschließend verzinnt man die Enden aller Drähte.