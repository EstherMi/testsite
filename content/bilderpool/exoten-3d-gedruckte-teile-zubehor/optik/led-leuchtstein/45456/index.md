---
layout: "image"
title: "LED Leuchtsteine - Serienproduktion läuft"
date: "2017-03-06T21:07:12"
picture: "IMG_20161222_142231.jpg"
weight: "2"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: ["LED", "leuchtsteine", "Eigenbau"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/45456
imported:
- "2019"
_4images_image_id: "45456"
_4images_cat_id: "1073"
_4images_user_id: "2638"
_4images_image_date: "2017-03-06T21:07:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45456 -->
Jan (7) hat natürlich schon das Löten entdeckt und kennt sich mit der Spitzzange bestens aus.
Hier bereitet er in Serie die 20 LEDs und die Widerstände vor, damit wir sie einlöten können.