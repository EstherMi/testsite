---
layout: "image"
title: "Platine"
date: "2017-02-05T15:27:45"
picture: "ledimftleuchtstein2.jpg"
weight: "2"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- details/45122
imported:
- "2019"
_4images_image_id: "45122"
_4images_cat_id: "3362"
_4images_user_id: "2240"
_4images_image_date: "2017-02-05T15:27:45"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45122 -->
Eine Diode als Verpolschutz und ein Infineon BCR401R LED Treiber