---
layout: "comment"
hidden: true
title: "22985"
date: "2017-02-05T19:58:37"
uploadBy:
- "uffi"
license: "unknown"
imported:
- "2019"
---
Da hast Du eine gute Idee gehabt und eine tolle Lösung gefunden!!! Endlich muss man nicht mehr in den Leuchtsteinen löten! Und braucht trotzdem keine alten Steckbirnen! Hut ab, toll gemacht!!! Gruß, Dirk!