---
layout: "comment"
hidden: true
title: "21442"
date: "2015-12-25T18:30:06"
uploadBy:
- "Triceratops"
license: "unknown"
imported:
- "2019"
---
Hallo

Widerstände können bei Langzeitbetrieb (und ggf. im Grenzbereich arbeitend) schon recht warm werden - vor allem in Kombination mit Leistungs-LEDs (40 bis 100 mA). Daher setze ich Widerstände i.d.R. extern und kann LEDs bei Bedarf dann kaskadieren.

Übrigens: Widerstände der Bauform 0207 (Metallschichtwiderstände haben hier eine größere Leistungsausbeute) passen auch in die Buchsen der Leuchtsteine komplett hinein. Mit losem Draht gefummelt und gestopft lassen sich dann sogar ganze LED-Flächen darstellen.

Hat jemand zufällig in Münster die Tricolore an meinem Modell fotografiert? Das wäre ein Beispiel dafür.

Gruß, Thomas