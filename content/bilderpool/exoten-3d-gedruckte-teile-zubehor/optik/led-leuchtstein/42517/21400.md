---
layout: "comment"
hidden: true
title: "21400"
date: "2015-12-15T14:59:26"
uploadBy:
- "Severin"
license: "unknown"
imported:
- "2019"
---
Mit 0805 kommt man schnell an die Leistungsgrenze des Widerstands. Ausgehend von einer 20mA LED und 2,5V für die LED sind das bei 9V schon 0,13W im Widerstand. 0805 hält meist 1/8W aus und arbeitet deshalb hier schon an der Grenze. Für 12V ist das dann zu wenig.