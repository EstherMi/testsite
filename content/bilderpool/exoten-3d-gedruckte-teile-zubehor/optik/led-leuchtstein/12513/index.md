---
layout: "image"
title: "Led-Leuchtstein mit gelber Kappe (4)"
date: "2007-11-05T19:42:44"
picture: "Led_gelb.jpg"
weight: "21"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: ["Led", "Leuchtstein", "Selbstbau", "Eigenbau", "Licht", "Beleuchtung", "Löten", "Leuchtkappe"]
uploadBy: "Sebo"
license: "unknown"
legacy_id:
- details/12513
imported:
- "2019"
_4images_image_id: "12513"
_4images_cat_id: "1073"
_4images_user_id: "650"
_4images_image_date: "2007-11-05T19:42:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12513 -->
Links: ausgeschaltete Led mit gelber Kappe
Rechts: eingeschaltete Led mit gelber Kappe