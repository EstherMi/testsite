---
layout: "image"
title: "LED Streifen Rückseite"
date: "2017-02-11T19:43:45"
picture: "erledimstatikwinkeltraeger2.jpg"
weight: "2"
konstrukteure: 
- "Joni2000"
fotografen:
- "Joni2000"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Joni2000"
license: "unknown"
legacy_id:
- details/45136
imported:
- "2019"
_4images_image_id: "45136"
_4images_cat_id: "3364"
_4images_user_id: "2240"
_4images_image_date: "2017-02-11T19:43:45"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45136 -->
Die Platine ist eine Eigenentwicklung, mit zwei Infineon BCR401R LED Treibern und Dioden als Verpolschutz.
Über die Lötbrücken können die einzelnen LEDs zusammen geschalten werden.
Entweder laufen alle vier mit einem Kabel oder immer zwei zusammen.
Der Strom kann von 10mA - 60mA eingestellt werden.