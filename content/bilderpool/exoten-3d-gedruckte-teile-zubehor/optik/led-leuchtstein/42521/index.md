---
layout: "image"
title: "Led im Leuchtstein."
date: "2015-12-16T18:14:58"
picture: "IMG_5157.jpg"
weight: "8"
konstrukteure: 
- "Bernd Langer"
fotografen:
- "Bernd Langer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- details/42521
imported:
- "2019"
_4images_image_id: "42521"
_4images_cat_id: "1073"
_4images_user_id: "2496"
_4images_image_date: "2015-12-16T18:14:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42521 -->
Oben mit Leuchtkappe .
Unten mit Rastleuchtkappe.
Auf Youtube hab ich ein Video hochgeladen.Da kann man die 
Blink-, Flacker-und Farbwechsler Led sehn.