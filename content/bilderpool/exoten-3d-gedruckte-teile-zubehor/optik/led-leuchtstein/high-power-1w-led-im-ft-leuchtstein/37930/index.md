---
layout: "image"
title: "Sie leuchtet!"
date: "2013-12-15T21:11:13"
picture: "bild4.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/37930
imported:
- "2019"
_4images_image_id: "37930"
_4images_cat_id: "2821"
_4images_user_id: "1624"
_4images_image_date: "2013-12-15T21:11:13"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37930 -->
In Wirklichkeit leuchtet sie heller, dieses Bild täuscht nur, weil ich es mit Blitz aufgenommen habe.