---
layout: "image"
title: "Einfacher LED Halter"
date: "2008-02-05T17:14:32"
picture: "7.jpg"
weight: "8"
konstrukteure: 
- "Holger Howey"
fotografen:
- "Holger Howey"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/13559
imported:
- "2019"
_4images_image_id: "13559"
_4images_cat_id: "699"
_4images_user_id: "34"
_4images_image_date: "2008-02-05T17:14:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13559 -->
Hier mal auf einem Baustein 5