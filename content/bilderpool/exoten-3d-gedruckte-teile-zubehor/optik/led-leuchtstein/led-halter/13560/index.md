---
layout: "image"
title: "LED Halter in einem Lochbaustein 15"
date: "2008-02-05T17:14:32"
picture: "10.jpg"
weight: "9"
konstrukteure: 
- "Holger Howey"
fotografen:
- "Holger Howey"
keywords: ["3mm", "LED", "Halter", "Baustein", "15", "Lochbaustein", "Klipp", "Ledklipp"]
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/13560
imported:
- "2019"
_4images_image_id: "13560"
_4images_cat_id: "699"
_4images_user_id: "34"
_4images_image_date: "2008-02-05T17:14:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13560 -->
Hier mal eine andere Variante.
3mm LED in einem Lochbaustein 15 mit einem LED Klip. Etwas pressen fertig.
Die LED kann man auch wieder raus nehmen ohne die Stecker abzusrauben.