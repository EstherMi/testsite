---
layout: "image"
title: "LED Halter 4"
date: "2008-02-05T17:14:32"
picture: "3.jpg"
weight: "4"
konstrukteure: 
- "Holger Howey"
fotografen:
- "Holger Howey"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/13555
imported:
- "2019"
_4images_image_id: "13555"
_4images_cat_id: "699"
_4images_user_id: "34"
_4images_image_date: "2008-02-05T17:14:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13555 -->
Man kann den Widerstand auch an den Stecker machen.