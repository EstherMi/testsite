---
layout: "image"
title: "Einfacher LED Halter 2"
date: "2008-02-05T17:14:32"
picture: "6.jpg"
weight: "7"
konstrukteure: 
- "Holger Howey"
fotografen:
- "Holger Howey"
keywords: ["Einfacher", "3mm", "LED", "Halter"]
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/13558
imported:
- "2019"
_4images_image_id: "13558"
_4images_cat_id: "699"
_4images_user_id: "34"
_4images_image_date: "2008-02-05T17:14:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13558 -->
Man kann den Verbinder ganz normal einbauen. Auch die LED kann man wieder raus nehmen.