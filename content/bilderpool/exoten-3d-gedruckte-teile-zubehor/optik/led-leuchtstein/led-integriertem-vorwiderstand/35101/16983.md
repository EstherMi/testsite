---
layout: "comment"
hidden: true
title: "16983"
date: "2012-07-13T14:56:57"
uploadBy:
- "Masked"
license: "unknown"
imported:
- "2019"
---
1000 Stück? So viele brauchts da doch garnicht.. Wie oben geschrieben, bei 50 Stück wären wir schon bei ungefähr 1 Euro pro Platine.
Wenn ich Platinen erstellen könnte, würde ichs ja mal tun ;-)
Prinzipiell würde ich mich anbieten, da organisierend mitzuwirken. Auch E-Technik-Wissen ist für sowas genügend vorhanden. Nur Platinen erstellen kann ich leider nicht.
Grüße,
Martin