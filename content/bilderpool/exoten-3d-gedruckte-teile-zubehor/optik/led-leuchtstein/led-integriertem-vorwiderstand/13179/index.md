---
layout: "image"
title: "Alternative zu einem Summer"
date: "2007-12-30T20:28:31"
picture: "ledmitintegriertemvorwiderstand5.jpg"
weight: "13"
konstrukteure: 
- "WERWEX"
fotografen:
- "WERWEX"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "werwex"
license: "unknown"
legacy_id:
- details/13179
imported:
- "2019"
_4images_image_id: "13179"
_4images_cat_id: "1190"
_4images_user_id: "689"
_4images_image_date: "2007-12-30T20:28:31"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13179 -->
Elektromagnetischer Summer, ca. 85dB.
2,3kHz / 8-15 VDC, Durchmesser: 12mm
