---
layout: "image"
title: "LED mit integriertem Vorwiderstand"
date: "2007-12-30T20:28:31"
picture: "ledmitintegriertemvorwiderstand3.jpg"
weight: "11"
konstrukteure: 
- "WERWEX"
fotografen:
- "WERWEX"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "werwex"
license: "unknown"
legacy_id:
- details/13177
imported:
- "2019"
_4images_image_id: "13177"
_4images_cat_id: "1190"
_4images_user_id: "689"
_4images_image_date: "2007-12-30T20:28:31"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13177 -->
Das Beispiel zeigt eine LED mit integriertem Vorwiderstand ausgelegt für 12 Volt.
Eingebaut ein einem Leuchtstein.
Gibt es bei Rei****.de für 0,12  ? in grün, gelb und rot 5 mm oder 3 mm.
