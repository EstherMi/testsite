---
layout: "image"
title: "LED"
date: "2008-07-26T16:23:19"
picture: "Bild_74.jpg"
weight: "6"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: ["LED"]
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- details/14962
imported:
- "2019"
_4images_image_id: "14962"
_4images_cat_id: "1190"
_4images_user_id: "791"
_4images_image_date: "2008-07-26T16:23:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14962 -->
LED von der Seite gesehen