---
layout: "image"
title: "LED mit integriertem Vorwiderstand"
date: "2007-12-30T20:28:31"
picture: "ledmitintegriertemvorwiderstand4.jpg"
weight: "12"
konstrukteure: 
- "WERWEX"
fotografen:
- "WERWEX"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "werwex"
license: "unknown"
legacy_id:
- details/13178
imported:
- "2019"
_4images_image_id: "13178"
_4images_cat_id: "1190"
_4images_user_id: "689"
_4images_image_date: "2007-12-30T20:28:31"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13178 -->
Das Beispiel zeigt den einfachen Einbau einer LED mit integriertem Vorwiderstand in einem Leuchtstein.