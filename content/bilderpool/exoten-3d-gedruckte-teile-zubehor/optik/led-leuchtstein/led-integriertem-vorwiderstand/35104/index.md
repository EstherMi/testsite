---
layout: "image"
title: "LED kurz vor dem zusammen stecken"
date: "2012-07-07T09:52:30"
picture: "ftLED4.jpg"
weight: "4"
konstrukteure: 
- "Gerhard Birkenstock"
fotografen:
- "Gerhard Birkenstock"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "gggb"
license: "unknown"
legacy_id:
- details/35104
imported:
- "2019"
_4images_image_id: "35104"
_4images_cat_id: "1190"
_4images_user_id: "1524"
_4images_image_date: "2012-07-07T09:52:30"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35104 -->
