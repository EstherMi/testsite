---
layout: "image"
title: "LED"
date: "2008-02-16T13:52:45"
picture: "ledolli1.jpg"
weight: "11"
konstrukteure: 
- "Oliver Kötter"
fotografen:
- "Oliver Kötter"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Olli"
license: "unknown"
legacy_id:
- details/13654
imported:
- "2019"
_4images_image_id: "13654"
_4images_cat_id: "1073"
_4images_user_id: "504"
_4images_image_date: "2008-02-16T13:52:45"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13654 -->
LED in einem Leuchtbaustein. Es ist eine 5mm LED mit 15000 mcd. Abstrahlwinkel ungefähr 22°. Die LED braucht 3,2 - 3,4 V und zieht 20mA. Der Vorwiderstand hat 330 Ohm, was ein bisschen zu viel ist, da die LED mit einem 290 Ohm Widerstand voll ausgelastet wäre.

An dem Leuchtbaustein wurde nichts modifiziert. Es passen alle Leuchtkappen drauf. Theoretisch könnte man sie einfach wieder rausziehen, weil sie nur gesteckt ist. WACKELT ABER TROTZDEM NICHTS!