---
layout: "image"
title: "Led im Leuchtstein"
date: "2015-12-15T18:24:09"
picture: "IMG_5152.jpg"
weight: "9"
konstrukteure: 
- "Bernd Langer"
fotografen:
- "Bernd Langer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- details/42518
imported:
- "2019"
_4images_image_id: "42518"
_4images_cat_id: "1073"
_4images_user_id: "2496"
_4images_image_date: "2015-12-15T18:24:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42518 -->
Von links nach rechts.Warmweisse Flacker Led, Weiße diffuse Blink Led mit 2,5 Hz, Warmweisse Linsen Led, warmweiße diffuse Led