---
layout: "comment"
hidden: true
title: "11305"
date: "2010-04-03T00:54:10"
uploadBy:
- "Triceratops"
license: "unknown"
imported:
- "2019"
---
Hallo

Ich habe mal eine alte FT-Lampe auseinandergepult (dabei unbedingt Schutzbrille tragen!!!) und danach in die Fassung eine LED hineingefriemelt. Die ließe sich jetzt beliebig wechseln.

Eine andere Variante bieten 10mm-LEDs. Deren Anschlußdrähte werden auf etwa 5 mm gekürzt und danach mit dünnem Draht versehen. Dieser wiederum läßt sich mit etwas Geschick (wie bei Nadel und Faden) mit meinem feinen Schraubendreher und Pinzette um die Röhrchen im Leuchtstein fädeln - überstehende Drahtreste entfernen. Auch das hält sicher und läßt sich bei Bedarf später rückstandsfrei wieder entfernen.

Gruß, Thomas