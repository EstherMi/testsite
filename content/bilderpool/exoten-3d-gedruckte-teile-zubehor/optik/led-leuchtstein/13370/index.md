---
layout: "image"
title: "LED"
date: "2008-01-22T18:10:20"
picture: "LED2.jpg"
weight: "16"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/13370
imported:
- "2019"
_4images_image_id: "13370"
_4images_cat_id: "1073"
_4images_user_id: "456"
_4images_image_date: "2008-01-22T18:10:20"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13370 -->
Von hinten, sieht man die Buchsen-Anschlüsse.