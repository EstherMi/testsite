---
layout: "image"
title: "Anleitung zum Bau des Led-Leuchtsteines (2)"
date: "2007-09-30T00:26:10"
picture: "2._Schritt.jpg"
weight: "28"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: ["Led", "Leuchtstein", "Anleitung", "Eigenbau", "Löten", "Widerstand", "Lampe", "Leuchte", "Licht"]
uploadBy: "Sebo"
license: "unknown"
legacy_id:
- details/12057
imported:
- "2019"
_4images_image_id: "12057"
_4images_cat_id: "1073"
_4images_user_id: "650"
_4images_image_date: "2007-09-30T00:26:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12057 -->
Nachdem alle Enden verzinnt wurden, folgt nun das Zusammenfügen unter der Hitze des Lötkolbens. 1-2 Sekunden und fertig ist die Verbindung. 

Doch Vorsicht: Nicht verbrennen !

Den dritten Teil findet ihr drei Bilder weiter.