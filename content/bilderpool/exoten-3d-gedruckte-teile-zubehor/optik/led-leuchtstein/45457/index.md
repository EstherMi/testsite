---
layout: "image"
title: "LED Leuchtsteine - die ersten Zwischenergebnisse"
date: "2017-03-06T21:07:12"
picture: "IMG_20161222_150046.jpg"
weight: "3"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: ["led", "leuchtsteine", "eigenbau"]
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/45457
imported:
- "2019"
_4images_image_id: "45457"
_4images_cat_id: "1073"
_4images_user_id: "2638"
_4images_image_date: "2017-03-06T21:07:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45457 -->
Links unten: die Widerstände sind an den LEDs festgelötet und vorgebogen.
Rechts: die Beine werden ziwschen Leuchtstein und den Buchsen eingesteckt, der Kontakt reicht um das Ganze zu halten.