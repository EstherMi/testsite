---
layout: "image"
title: "Led-Leuchtstein Hauptansicht (1)"
date: "2007-09-27T20:22:13"
picture: "DSCF1687_neu.jpg"
weight: "30"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: ["Led", "Leuchtstein", "Elektronik", "Widerstand", "löten", "Eigenbau"]
uploadBy: "Sebo"
license: "unknown"
legacy_id:
- details/12030
imported:
- "2019"
_4images_image_id: "12030"
_4images_cat_id: "1073"
_4images_user_id: "650"
_4images_image_date: "2007-09-27T20:22:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12030 -->
Hier sieht man die  erste Hauptansicht meines  Eigenbaues einer Leuchtdiode in einen ft-Leuchtstein.(nicht zu sehen ist der Widerstand)
Eckdaten:
Led Leuchtfarbe: weiß
    Lichtstärke: 18.000mcd
    Bauform: 5mm
    Gehäuseausführung: klar
    Abstrahlwinkel: 20 Grad
    Stomaufnahme: 20mA

Widerstand 470Ohm