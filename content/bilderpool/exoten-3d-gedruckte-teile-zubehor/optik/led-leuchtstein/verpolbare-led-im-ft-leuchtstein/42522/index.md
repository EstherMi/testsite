---
layout: "image"
title: "LED"
date: "2015-12-19T12:41:27"
picture: "vlifl1.jpg"
weight: "1"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/42522
imported:
- "2019"
_4images_image_id: "42522"
_4images_cat_id: "3161"
_4images_user_id: "2228"
_4images_image_date: "2015-12-19T12:41:27"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42522 -->
verpolbare LED auf einer 12mm x 12mm großen Platine:
 - SMD Widerstand 300 Ohm (120 Ohm + 180 Ohm)
 - Gleichrichterschaltung aus 4 SMD Dioden
 - relative helle, weiße LED mit 20° Abstrahlwinkel (vgl. Conrad: https://www.conrad.de/de/led-bedrahtet-weiss-rund-5-mm-10000-mcd-20-20-ma-32-v-led-5-10000w-176724.html)