---
layout: "image"
title: "Herstellung"
date: "2015-12-19T12:41:27"
picture: "vlifl5.jpg"
weight: "5"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/42526
imported:
- "2019"
_4images_image_id: "42526"
_4images_cat_id: "3161"
_4images_user_id: "2228"
_4images_image_date: "2015-12-19T12:41:27"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42526 -->
Aufgrund der kleinen Serie (15 Stück) wurden die für die LED benötigten Platinen an einer CNC Fräße ausgefräßt.