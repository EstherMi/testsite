---
layout: "image"
title: "Herstellung"
date: "2015-12-19T12:41:27"
picture: "vlifl6.jpg"
weight: "6"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/42527
imported:
- "2019"
_4images_image_id: "42527"
_4images_cat_id: "3161"
_4images_user_id: "2228"
_4images_image_date: "2015-12-19T12:41:27"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42527 -->
Die CNC fräßt zunächst Trennlinien für die späteren Leiterbahnen in die oberste Kupferschicht und bohrt im Anschluss die benötigen Löcher und fräßt die Platinen aus.