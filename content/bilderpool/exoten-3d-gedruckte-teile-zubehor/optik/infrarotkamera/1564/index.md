---
layout: "image"
title: "Scan um 240 Grad"
date: "2003-09-27T11:33:05"
picture: "240Grad.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Livebild"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/1564
imported:
- "2019"
_4images_image_id: "1564"
_4images_cat_id: "183"
_4images_user_id: "46"
_4images_image_date: "2003-09-27T11:33:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1564 -->
Weitwinkelaufnahme des Spitzbodens. Dank des Scanners sind Ultraweitwinkelbilder möglich.