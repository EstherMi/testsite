---
layout: "image"
title: "Scanner - Y-Achse"
date: "2003-09-27T11:33:05"
picture: "Scanner-Y-Trieb.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "ich"
keywords: ["Scanner", "Optik", "Sensor", "Endschalter"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/1562
imported:
- "2019"
_4images_image_id: "1562"
_4images_cat_id: "183"
_4images_user_id: "46"
_4images_image_date: "2003-09-27T11:33:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1562 -->
Der Minimot macht die Höhenachse. Die Schaltscheibe stoppt den Motor nach einer Umdrehung