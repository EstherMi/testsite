---
layout: "overview"
title: "Infrarotkamera"
date: 2019-12-17T18:05:00+01:00
legacy_id:
- categories/183
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=183 --> 
Infrarotfotografie mit einem einzigen Sensor.