---
layout: "image"
title: "Zustellachse"
date: "2003-08-27T15:27:45"
picture: "Drehkranz-Getriebedetail.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/1354
imported:
- "2019"
_4images_image_id: "1354"
_4images_cat_id: "183"
_4images_user_id: "46"
_4images_image_date: "2003-08-27T15:27:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=1354 -->
Seitlich verschiebliche Getriebeachse zum Einstellen des Getriebespiels