---
layout: "image"
title: "Sharp Distanzsensor an Robo-Interface"
date: "2006-09-03T09:58:17"
picture: "Anschlu_A1-SignalA2-MasseM1-Motor_rechtslauf.jpg"
weight: "5"
konstrukteure: 
- "Remadus hat die Platine entwickelt!!!"
fotografen:
- "Reiner Stähler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- details/6771
imported:
- "2019"
_4images_image_id: "6771"
_4images_cat_id: "650"
_4images_user_id: "426"
_4images_image_date: "2006-09-03T09:58:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6771 -->
Hier noch eine möglichkeit die 9V Versorgungsspannung über den Motor Ausgang zu holen Roten Stecker an M1 (02) und im Interface Diagnoseprogramm (02) oder Motor rechtslauf einschalten und schon verändert sich je nach abstand zum Sharp Sensor der Wert bei A1