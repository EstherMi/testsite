---
layout: "image"
title: "Schaltbild des Adapters"
date: "2006-09-01T21:39:36"
picture: "Adapterbild.jpg"
weight: "6"
konstrukteure: 
- "Remadus"
fotografen:
- "Remadus"
keywords: ["Spannungsregler", "Tiefpaß", "Pufferkondensator"]
uploadBy: "remadus"
license: "unknown"
legacy_id:
- details/6765
imported:
- "2019"
_4images_image_id: "6765"
_4images_cat_id: "650"
_4images_user_id: "46"
_4images_image_date: "2006-09-01T21:39:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6765 -->
Dieser Adapter setzt sich zusammen aus dem LM293 Spannungsregler (5V, low drop), einem Entstörkondensator 0,1 µF, einem Elko 100 µF und einem Tiefpaß aus dem Widerstand 47k und einem Kondensator 1 µF. Der Adapter eignet sich für Eingangsspannungen von 6 bis 12 Volt.