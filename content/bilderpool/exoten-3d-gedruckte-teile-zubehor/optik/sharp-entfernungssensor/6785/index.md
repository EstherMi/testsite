---
layout: "image"
title: "SMD Netzgerät für Abstandssensor"
date: "2006-09-09T22:28:04"
picture: "101MSDCF_001.jpg"
weight: "1"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- details/6785
imported:
- "2019"
_4images_image_id: "6785"
_4images_cat_id: "650"
_4images_user_id: "473"
_4images_image_date: "2006-09-09T22:28:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6785 -->
von Conrad Elektronik Art-Nr. 140821
Abstandssensor von Sharp 4-30cm und 10-80cm