---
layout: "image"
title: "Anschluß Sharp Distanzsensor am Robo-Interface"
date: "2006-09-03T09:58:16"
picture: "Anschlu_A1-SignalA2-Masse-9V_Spannungsversorgung.jpg"
weight: "3"
konstrukteure: 
- "Remadus hat die Platine entwickelt!!!"
fotografen:
- "Reiner Stähler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- details/6769
imported:
- "2019"
_4images_image_id: "6769"
_4images_cat_id: "650"
_4images_user_id: "426"
_4images_image_date: "2006-09-03T09:58:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6769 -->
Habe hier mal Bilder gemacht nachdem ich von Remadus die Platine nachgebaut habe, damit die nicht so Elektroniker Leute auch mal wissen wie man das ganze dann anschließt.

Das Robo-Interface wird über das Netzteil betrieben dann habe ich links an der Seite bei + (roter Stecker) die Versorgungsspannung für die Platine abgegriffen. Und dann die Masse für Platine und Sharp Sensor (grüner Stecker) auf A1 unterer Kontakt angeklemmt und zu guter letzt die Signalleitung (gelber Stecker) Auf A1 oben angeklemmt ! Anschlüsse A1 oberer + unterer - sind so festgelegt und müssen auch so beschaltet werden sonst gibt es nen Kurzschluß!

Gruß Reiner