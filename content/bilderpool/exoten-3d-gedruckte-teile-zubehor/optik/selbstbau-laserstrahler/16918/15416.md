---
layout: "comment"
hidden: true
title: "15416"
date: "2011-10-12T21:34:23"
uploadBy:
- "qincym"
license: "unknown"
imported:
- "2019"
---
Ich habe gestern erfahren (und überprüft), daß der Laserstrahler OLP503P nicht mehr bei der Fa. Conrad bestellt werden kann. Als alternative Bestellmöglichkeit bietet sich die Fa. Arndt electronic GmbH (www.europelaser.de, für ca. 25 Euro) in Berlin an

Viele Grüße
Volker-James