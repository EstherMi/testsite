---
layout: "image"
title: "Laserstrahler 12"
date: "2009-01-07T15:12:38"
picture: "laserstrahler13.jpg"
weight: "21"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- details/16929
imported:
- "2019"
_4images_image_id: "16929"
_4images_cat_id: "1523"
_4images_user_id: "895"
_4images_image_date: "2009-01-07T15:12:38"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16929 -->
Deutlich sind auf diesem Bild die Bohrungen der Bausteine 30, ft# 32880, zu sehen, die alle 90 Grad mit dem Laserstrahl eine Linie bilden und so den Laserstrahl auf den Fototransistor, ft# 36134, treffen lassen.