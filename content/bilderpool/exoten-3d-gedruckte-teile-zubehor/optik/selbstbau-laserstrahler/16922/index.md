---
layout: "image"
title: "Lasersrahler 05"
date: "2009-01-07T15:12:38"
picture: "laserstrahler06.jpg"
weight: "14"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- details/16922
imported:
- "2019"
_4images_image_id: "16922"
_4images_cat_id: "1523"
_4images_user_id: "895"
_4images_image_date: "2009-01-07T15:12:38"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16922 -->
Der Störlichttubus kann nicht ohne des erforderliche Gegenstück, z.B. der Fotosensor ft# 31361, in fischertechnik verbaut werden. Um den Störlichttubus mit einem Baustein 5 mm, ft# 37237, zu verkleben, wurde eine "Klebeschablone" gebaut.