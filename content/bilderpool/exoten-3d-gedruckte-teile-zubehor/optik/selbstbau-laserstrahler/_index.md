---
layout: "overview"
title: "Selbstbau Laserstrahler"
date: 2019-12-17T18:05:21+01:00
legacy_id:
- categories/1523
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1523 --> 
Selbstbau eines Punkt-Laserstrahlers für den Einbau und Betrieb in fischertechnik Modellen für Lichtschranken und Auslösung von Schaltvorgängen.