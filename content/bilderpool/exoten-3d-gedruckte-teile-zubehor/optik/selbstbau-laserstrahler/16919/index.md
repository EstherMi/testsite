---
layout: "image"
title: "Laserstrahler 02"
date: "2009-01-07T15:12:38"
picture: "laserstrahler03.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- details/16919
imported:
- "2019"
_4images_image_id: "16919"
_4images_cat_id: "1523"
_4images_user_id: "895"
_4images_image_date: "2009-01-07T15:12:38"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16919 -->
Der Punkt-Laserstrahler kann umso besser in den Störlichttubus eingepresst werden, wenn ein ca. 8 mm langes Stück Schrumpfschlauch aufgeschoben wird.