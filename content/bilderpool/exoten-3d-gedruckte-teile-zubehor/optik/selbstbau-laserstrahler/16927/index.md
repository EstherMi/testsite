---
layout: "image"
title: "Laserstrahler 10"
date: "2009-01-07T15:12:38"
picture: "laserstrahler11.jpg"
weight: "19"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "qincym"
license: "unknown"
legacy_id:
- details/16927
imported:
- "2019"
_4images_image_id: "16927"
_4images_cat_id: "1523"
_4images_user_id: "895"
_4images_image_date: "2009-01-07T15:12:38"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16927 -->
Das Bild zeigt den selbstgebauten Punkt-Laserstrahler mit der aufgesetzten Störlichtkappe aus der entgegengesezten Blickrichtung.

Nochmals der Hinweis auf die aufgeklebten Warnhinweise zur Laserklasse 2 und dass das Gerät kein Kinderspielzeug mehr ist!