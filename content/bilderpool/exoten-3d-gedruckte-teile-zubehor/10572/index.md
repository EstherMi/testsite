---
layout: "image"
title: "Sonne, Mond und Erde"
date: "2007-05-31T09:43:06"
picture: "diverse4.jpg"
weight: "37"
konstrukteure: 
- "-?-"
fotografen:
- "Reiner Stüven"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sulu007"
license: "unknown"
legacy_id:
- details/10572
imported:
- "2019"
_4images_image_id: "10572"
_4images_cat_id: "463"
_4images_user_id: "109"
_4images_image_date: "2007-05-31T09:43:06"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10572 -->
