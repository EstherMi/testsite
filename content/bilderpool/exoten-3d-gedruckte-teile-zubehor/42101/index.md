---
layout: "image"
title: "Herr und Frau Fischer"
date: "2015-10-19T18:53:44"
picture: "Fischerfrau.jpg"
weight: "8"
konstrukteure: 
- "Peter Habermehl"
fotografen:
- "Peter Habermehl"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "PHabermehl"
license: "unknown"
legacy_id:
- details/42101
imported:
- "2019"
_4images_image_id: "42101"
_4images_cat_id: "463"
_4images_user_id: "2488"
_4images_image_date: "2015-10-19T18:53:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42101 -->
Zur Diskussion hier: 
http://forum.ftcommunity.de/viewtopic.php?f=21&t=3153

Ein schneller Vorschlag, wie Frau Fischer aussehen könnte:

Kopf:
- Haare an der Stirn etwas runder, Haare insgesamt länger, Ohren bedeckt, ggf. andere Haarfarben: hellbraun, blond
- Kinn schmaler, abgerundet
- Augenbrauen "weiblicher"
- Nase zierlicher

Oberkörper:
- Oberkörper etwas kürzer
- Tailliert
- KEINE 3D-Brüste!!!!!

Oberarme abgerundet, damit der Schulterbereich etwas schmaler wirkt.
Unterarme, Hände, Oberschenkel ident. zum männl. Partner

Unterschenkel etwas gekürzt.

Und nicht zuletzt: blaßrosa Bluse :-)