---
layout: "comment"
hidden: true
title: "14538"
date: "2011-07-02T04:11:18"
uploadBy:
- "Triceratops"
license: "unknown"
imported:
- "2019"
---
Hallo

Wenn der mit den drei Rillen 1,5 mm größer ist, dann müßte man daraus mit 22 Kettengliedern ein Z22 generieren können. Mit den anderen ginge in dieser Weise nur ein Z21. Hat das mal jemand probiert?

Gruß, Thomas