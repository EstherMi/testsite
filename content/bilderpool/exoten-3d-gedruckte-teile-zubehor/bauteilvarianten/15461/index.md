---
layout: "image"
title: "Streben"
date: "2008-09-22T21:33:17"
picture: "ft-streben_002.jpg"
weight: "18"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- details/15461
imported:
- "2019"
_4images_image_id: "15461"
_4images_cat_id: "1119"
_4images_user_id: "473"
_4images_image_date: "2008-09-22T21:33:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15461 -->
