---
layout: "image"
title: "Verschiedene Varianten des U-Trägers 150 rot"
date: "2007-11-05T15:54:01"
picture: "IMG_0028.jpg"
weight: "72"
konstrukteure: 
- "Burkhard Eins"
fotografen:
- "Burkhard Eins"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Burkhard Eins"
license: "unknown"
legacy_id:
- details/12484
imported:
- "2019"
_4images_image_id: "12484"
_4images_cat_id: "1119"
_4images_user_id: "611"
_4images_image_date: "2007-11-05T15:54:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12484 -->
Verschiedene Varianten des U-Trägers 150 rot (von links nach rechts):

1.+2.) 32968.1 U-Träger 150 rot, alte Bauform mit nur einer Mittelbohrung

3.+4.) 32968.2 U-Träger 150 rot, neuere Bauform mit 9 Mittelbohrungen