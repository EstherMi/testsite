---
layout: "image"
title: "Bauplatten Fehlproduktion"
date: "2008-01-13T22:29:28"
picture: "P2.jpg"
weight: "32"
konstrukteure: 
- "Howey"
fotografen:
- "Howey"
keywords: ["Bauplatten", "Fehlproduktion"]
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/13327
imported:
- "2019"
_4images_image_id: "13327"
_4images_cat_id: "1119"
_4images_user_id: "34"
_4images_image_date: "2008-01-13T22:29:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13327 -->
Hallo...
Unten liegen sie bündig. Nahaufnahme vom Bild P1.
Die rechte Platte ist um 2mm kürzer.
Wenn man in jede Nut einen Baustein gibt biegt sie etwas durch.

Gruß
Holger Howey