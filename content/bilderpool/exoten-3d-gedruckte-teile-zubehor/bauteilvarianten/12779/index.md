---
layout: "image"
title: "Radachse 36586.JPG"
date: "2007-11-18T22:04:36"
picture: "Radachse_36586.JPG"
weight: "54"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/12779
imported:
- "2019"
_4images_image_id: "12779"
_4images_cat_id: "1119"
_4images_user_id: "4"
_4images_image_date: "2007-11-18T22:04:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12779 -->
Die neuere Version (oben) hat einen Überstand zwischen Achse und Stein. Wozu der aber gut sein soll?