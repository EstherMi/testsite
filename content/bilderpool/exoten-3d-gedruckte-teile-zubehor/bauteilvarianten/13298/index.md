---
layout: "image"
title: "Seiltrommel"
date: "2008-01-09T17:47:17"
picture: "Seiltrommel.jpg"
weight: "37"
konstrukteure: 
- "Pesche65"
fotografen:
- "Pesche65"
keywords: ["Seiltrommel"]
uploadBy: "Pesche65"
license: "unknown"
legacy_id:
- details/13298
imported:
- "2019"
_4images_image_id: "13298"
_4images_cat_id: "1119"
_4images_user_id: "660"
_4images_image_date: "2008-01-09T17:47:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13298 -->
Seiltrommel