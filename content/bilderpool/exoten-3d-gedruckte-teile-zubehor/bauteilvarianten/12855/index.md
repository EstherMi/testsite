---
layout: "image"
title: "Winkellasche 31670.JPG"
date: "2007-11-27T18:40:33"
picture: "Winkellasche_31670.JPG"
weight: "52"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/12855
imported:
- "2019"
_4images_image_id: "12855"
_4images_cat_id: "1119"
_4images_user_id: "4"
_4images_image_date: "2007-11-27T18:40:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12855 -->
Sorry, die Schärfe könnte besser sein, aber das notwendige ist zu erkennen.