---
layout: "comment"
hidden: true
title: "16395"
date: "2012-02-15T14:06:04"
uploadBy:
- "thomas004"
license: "unknown"
imported:
- "2019"
---
Ich habe nur neue Kästen gekauft und weiß daher, wie viele Teile in welchem Kasten waren. Ich führe da ne Liste. Ich weiß also auch, wenn mal ein Kleinteil mehr drin ist, was ja oft passiert.

Jetzt muss ich nur noch schauen, in welchem meiner Kästen 5 (oder 4+1) Riegelscheiben drin waren. Laut FT-Datenbank können das nur Profi Sensoric, Profi Cartech, Basic Vehicles oder Basic Mechanics sein.

Ergebnisse gibt's heute Abend!

Gruß, Thomas