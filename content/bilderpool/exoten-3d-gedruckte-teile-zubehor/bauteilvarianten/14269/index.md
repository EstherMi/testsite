---
layout: "image"
title: "Motor-adapter"
date: "2008-04-15T18:37:24"
picture: "Adapter1.jpg"
weight: "27"
konstrukteure: 
- "Ben Halverkamps"
fotografen:
- "Ben Halverkamps"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Benji"
license: "unknown"
legacy_id:
- details/14269
imported:
- "2019"
_4images_image_id: "14269"
_4images_cat_id: "1119"
_4images_user_id: "764"
_4images_image_date: "2008-04-15T18:37:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14269 -->
