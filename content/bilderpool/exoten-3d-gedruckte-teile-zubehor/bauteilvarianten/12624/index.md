---
layout: "image"
title: "Verschiedene Varianten der Kemmbuchse 10"
date: "2007-11-11T08:33:12"
picture: "IMG_0271.jpg"
weight: "61"
konstrukteure: 
- "Burkhard Eins"
fotografen:
- "Burkhard Eins"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Burkhard Eins"
license: "unknown"
legacy_id:
- details/12624
imported:
- "2019"
_4images_image_id: "12624"
_4images_cat_id: "1119"
_4images_user_id: "611"
_4images_image_date: "2007-11-11T08:33:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12624 -->
Verschiedene Varianten der Klemmbuchse 10 (von links nach rechts)

1.Paar: 31023.1 Klemmbuchse 10 rot, 
beide Stirnseiten mit C-förmiger Vertiefung eine mit Feder bestückt

2.Paar: 31023.2 Klemmbuchse 10 rot,
eine Stirnseite mit Feder bestückter C-förmiger Vertiefung, andere Stirnseite geschlossen ohne Entlastungseinschnitt

3.Paar: 31023.3 Klemmbuchse 10 rot,
wie 31023.2, jedoch mit Entlastungseinschnitt

4.Paar: 31023.4 Klemmbuchse 10 neurot,
wie 31023.3, jedoch in Neurot, deutlich blasser als 31023.3 (auf dem Bild nicht zu erkennen)