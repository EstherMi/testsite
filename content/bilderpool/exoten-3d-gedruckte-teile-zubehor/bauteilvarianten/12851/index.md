---
layout: "image"
title: "Lenkklaue 35998.JPG"
date: "2007-11-27T18:33:24"
picture: "Lenkklaue_35998.JPG"
weight: "48"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/12851
imported:
- "2019"
_4images_image_id: "12851"
_4images_cat_id: "1119"
_4images_user_id: "4"
_4images_image_date: "2007-11-27T18:33:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12851 -->
Rechts außen ist kein Original, sondern ein bearbeitetes Teil (0,5 mm von den inneren Stegen entfernt) Verwendungszweck siehe hier: http://www.ftcommunity.de/details.php?image_id=5595 . Es gibt aber auch welche ganz ohne diesen Steg.