---
layout: "image"
title: "Verschiedene Mitnehmer und deren Einsatzmöglichkeiten"
date: "2007-11-11T08:33:12"
picture: "IMG_0144.jpg"
weight: "60"
konstrukteure: 
- "Burkhard Eins"
fotografen:
- "Burkhard Eins"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Burkhard Eins"
license: "unknown"
legacy_id:
- details/12622
imported:
- "2019"
_4images_image_id: "12622"
_4images_cat_id: "1119"
_4images_user_id: "611"
_4images_image_date: "2007-11-11T08:33:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12622 -->
Das Modell zeigt eine Einsatzmöglichkeit des Mitnehmers.

Im Vordergrund (von links nach rechts) die verschiedenen Mitnehmerausführungen.

1.) 31712.1 Mitnehmer rot (sehr selten!)
2.) 31712.2 Mitnehmer schwarz