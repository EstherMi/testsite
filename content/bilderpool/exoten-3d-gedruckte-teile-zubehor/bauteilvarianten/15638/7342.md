---
layout: "comment"
hidden: true
title: "7342"
date: "2008-09-28T07:23:57"
uploadBy:
- "HLGR"
license: "unknown"
imported:
- "2019"
---
@Udo2:
Im Fischertechnik-Forum findest Du unter "An Fischertechnik/Baustein15/30 ohne Zapfen" ausführliche Antworten auf Deine Fragen.

Hier nur soviel:
FT ist interessiert, prüft gerade die Möglichkeiten, und wird möglicher Weise einen Baukasten mit dem Stein herausbringen.


@Michael:
Pictures by PC 3.0.
Gut, dass Du nicht gefragt hast, was das Programm kostet.
Es ist ein professionelles Programm, das wir in der Firma haben, für das man sich seeeeehr viel Fischertechnik kaufen könnte.

Inzwischen hat aber unsere Mechanik-Abteilung ein viel besseres (Solid Works).
Die Bilder, die man damit machen kann, kann man von Fotos nicht mehr unterscheiden.

@Stephan:
Natürlich kann das Werkzeug nicht nur aus zwei Teilen bestehen.
Das Werkzeug muß natürlich ausziebare Stifte haben, was aber auch schon beim normalen Baustein 15 der Fall ist.
Sonst wäre die hinter Nut auch nicht möglich.

Gruß,
Holger