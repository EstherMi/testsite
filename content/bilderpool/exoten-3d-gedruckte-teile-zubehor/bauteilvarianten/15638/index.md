---
layout: "image"
title: "Bauteil 15 ohne Zapfen"
date: "2008-09-27T11:34:21"
picture: "B15_1.jpg"
weight: "12"
konstrukteure: 
- "HLGR"
fotografen:
- "HLGR"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "HLGR"
license: "unknown"
legacy_id:
- details/15638
imported:
- "2019"
_4images_image_id: "15638"
_4images_cat_id: "1119"
_4images_user_id: "832"
_4images_image_date: "2008-09-27T11:34:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15638 -->
3D-Ansicht des von mir neu konstruierten Bauteils