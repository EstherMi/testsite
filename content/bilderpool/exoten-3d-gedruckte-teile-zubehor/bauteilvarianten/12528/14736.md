---
layout: "comment"
hidden: true
title: "14736"
date: "2011-07-29T21:58:19"
uploadBy:
- "Lurchi"
license: "unknown"
imported:
- "2019"
---
Hallo,

ist jemandem schon aufgefallen, dass in den ganz neuen Kästen (ab ca. 2010) wieder die erste Variante (also die ohne  stirnseitige Bohrungen) verwendet wird.

Welchen Grund könnte dies haben?

Gruß

Lurchi