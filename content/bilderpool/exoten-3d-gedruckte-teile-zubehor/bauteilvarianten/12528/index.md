---
layout: "image"
title: "Verschiedene Rollenlager"
date: "2007-11-07T21:01:38"
picture: "IMG_0084.jpg"
weight: "65"
konstrukteure: 
- "Burkhard Eins"
fotografen:
- "Burkhard Eins"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Burkhard Eins"
license: "unknown"
legacy_id:
- details/12528
imported:
- "2019"
_4images_image_id: "12528"
_4images_cat_id: "1119"
_4images_user_id: "611"
_4images_image_date: "2007-11-07T21:01:38"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12528 -->
Verschiedene Rollenlager (von links nach rechts):

1.) 37636.1 Rollenlager rot, alte Bauform (es fehlen die beiden stirnseitigen Bohrungen auf der Kopfplatte neben dem Zapfen)

2.) 37636.2 Rollenlager rot, neuere Bauform mit Stirnbohrungen

3.) 37636.3 Rollenlager neurot. Dabei wirkt die Struktur des neuroten Kunststoffs fast marmorartig und etwas blass.