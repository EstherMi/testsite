---
layout: "comment"
hidden: true
title: "14739"
date: "2011-07-29T23:10:52"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
a) Was ist denn der Unterschied zwischen den beiden rechten? Nur die Oberflächenstruktur oder noch etwas?

b) @Lurchi: Aufgefallen nicht, weil ich noch keine neuen Kästen haben. Evtl. brauchte ft mehr Stabilität. Wahrscheinlicher finde ich aber, dass die neue Spritzgussform kaputt gegangen ist, ft die alte noch hatte, und die zusätzlichen Achsführungen in aktuellen Kästenmodellen nicht benötigt werden.

Gruß,
Stefan