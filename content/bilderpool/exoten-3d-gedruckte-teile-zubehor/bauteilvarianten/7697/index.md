---
layout: "image"
title: "Ur-Version des Z40"
date: "2006-12-04T16:39:28"
picture: "urversionz1.jpg"
weight: "76"
konstrukteure: 
- "fischer-Werke"
fotografen:
- "Jörg"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/7697
imported:
- "2019"
_4images_image_id: "7697"
_4images_cat_id: "1119"
_4images_user_id: "104"
_4images_image_date: "2006-12-04T16:39:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7697 -->
Jörg (Lorenzen im Forum?) hatte mir das Foto gesandt und ich stelle es hier für ihn ein. Es handelt sich um eine ganz frühe Version des Z40 mit breiten Zähnen, wie man sieht.