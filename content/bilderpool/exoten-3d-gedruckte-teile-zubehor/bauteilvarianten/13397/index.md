---
layout: "image"
title: "Verbinder 45 in grau"
date: "2008-01-26T13:12:32"
picture: "DSCN2061.jpg"
weight: "30"
konstrukteure: 
- "ft"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/13397
imported:
- "2019"
_4images_image_id: "13397"
_4images_cat_id: "1119"
_4images_user_id: "184"
_4images_image_date: "2008-01-26T13:12:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13397 -->
Verbinder in grau, alte Bauart