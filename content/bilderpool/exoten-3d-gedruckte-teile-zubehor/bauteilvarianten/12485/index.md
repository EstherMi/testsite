---
layout: "image"
title: "Verschiedene Varianten der großen Statikplatte"
date: "2007-11-05T15:54:02"
picture: "IMG_0073.jpg"
weight: "73"
konstrukteure: 
- "Burkhard Eins"
fotografen:
- "Burkhard Eins"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Burkhard Eins"
license: "unknown"
legacy_id:
- details/12485
imported:
- "2019"
_4images_image_id: "12485"
_4images_cat_id: "1119"
_4images_user_id: "611"
_4images_image_date: "2007-11-05T15:54:02"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12485 -->
Verschiedene Varianten der großen Statikplatte:

untere linke Platte: 36321.1 Platte 180 x 90 x 2 rot, alte Bauform

obere rechte Platte: 36321.2 Platte 180 x 90 x 2 rot, neuere Bauform mit matt abgesetzten Statikbohrungen

Anmerkung: Statikplatten der alten Bauform wirken nicht ganz so brillant als die neueren. Durch die fehlende matte Absetzung der Statikbohrungen werden diese Platten, fälschlicherweise, oftmals für Plagiate gehalten.