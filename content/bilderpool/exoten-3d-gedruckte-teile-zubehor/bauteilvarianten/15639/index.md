---
layout: "image"
title: "Bauteil 15 ohne Zapfen Schnitt 1"
date: "2008-09-27T11:34:21"
picture: "B15_2.jpg"
weight: "13"
konstrukteure: 
- "HLGR"
fotografen:
- "HLGR"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "HLGR"
license: "unknown"
legacy_id:
- details/15639
imported:
- "2019"
_4images_image_id: "15639"
_4images_cat_id: "1119"
_4images_user_id: "832"
_4images_image_date: "2008-09-27T11:34:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15639 -->
3D-Schnitt 1