---
layout: "image"
title: "Mögliches Bauteil 30 ohne Zapfen"
date: "2008-10-03T19:46:03"
picture: "Bauteil_30_ohne_Zapfen.jpg"
weight: "10"
konstrukteure: 
- "HLGR"
fotografen:
- "HLGR"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "HLGR"
license: "unknown"
legacy_id:
- details/15818
imported:
- "2019"
_4images_image_id: "15818"
_4images_cat_id: "1119"
_4images_user_id: "832"
_4images_image_date: "2008-10-03T19:46:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15818 -->
Passend zu meinem Bauteil 15 ohne Zapfen ist dies jetzt eine entsprechende Variante des Baustein 30.