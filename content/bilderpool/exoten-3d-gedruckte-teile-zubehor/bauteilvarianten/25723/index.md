---
layout: "image"
title: "Power-Controller"
date: "2009-11-08T13:54:27"
picture: "powercontroller1.jpg"
weight: "9"
konstrukteure: 
- "ft"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/25723
imported:
- "2019"
_4images_image_id: "25723"
_4images_cat_id: "1119"
_4images_user_id: "409"
_4images_image_date: "2009-11-08T13:54:27"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25723 -->
Beim rechten Power-Controller handelt es sich vermutlich um die erste Version dieser Art. Die Buchsen für den Motorausgang sind vorne an der Stirnseite.Beim Linken Power Controller sind die Buchsen für den Motorausgang vorne auf der Oberseite. Die Löcher an der Stirnseite sind zwar noch vorhanden, aber nicht belegt.