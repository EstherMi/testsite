---
layout: "image"
title: "Federnocken"
date: "2008-10-03T21:21:00"
picture: "Federnocken.jpg"
weight: "11"
konstrukteure: 
- "Fischertechnik"
fotografen:
- "HLGR"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "HLGR"
license: "unknown"
legacy_id:
- details/15819
imported:
- "2019"
_4images_image_id: "15819"
_4images_cat_id: "1119"
_4images_user_id: "832"
_4images_image_date: "2008-10-03T21:21:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15819 -->
Federnocken mit und ohne Delle im Zapfen