---
layout: "comment"
hidden: true
title: "7493"
date: "2008-10-05T23:38:22"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Miteinander,
der Kreuznocken mit Delle ist eine querschnittsoptimierte Ausführung, weil sonst ohne im Bereich des Zapfens ein höher Materialquerschnitt zu verzeichnen ist als an der Feder. Spritzgußteile sollen, was praktisch oft nur annähernd konstruiert werden kann, wegen dem sonst unterschiedlichen Schwinden bei der Abkühlung gleiche Querschnitte haben. Ein Ausgleich ist während der Haltezeit unter Spritzdruck nur begrenzt möglich. Ich hatte diese Teile 2008 gekauft in aktuellen Baukästen dabei.
Grüße euch, Ingo