---
layout: "image"
title: "Winkellaschen"
date: "2014-11-09T17:21:24"
picture: "P1030113.jpg"
weight: "2"
konstrukteure: 
- "fischertechnik"
fotografen:
- "H.A.R.R.Y."
keywords: ["Winkellasche", "S-Winkellasche", "Spitzes", "Ende", "Flaches", "Ende", "35738", "31670"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/39789
imported:
- "2019"
_4images_image_id: "39789"
_4images_cat_id: "1119"
_4images_user_id: "1557"
_4images_image_date: "2014-11-09T17:21:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39789 -->
Ein Ensemble aus meiner Sammlung. Die Teilenummern schenke ich mir jetzt, ich blicke da nicht wirklich durch.

Ob sich, auch hier, jemand findet, der die Teile ihren Produktionsjahren (von => bis) zuordnen kann?