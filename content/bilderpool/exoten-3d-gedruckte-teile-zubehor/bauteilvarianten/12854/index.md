---
layout: "image"
title: "T-Stück 31642.JPG"
date: "2007-11-27T18:38:35"
picture: "T-Stck_31642.JPG"
weight: "51"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/12854
imported:
- "2019"
_4images_image_id: "12854"
_4images_cat_id: "1119"
_4images_user_id: "4"
_4images_image_date: "2007-11-27T18:38:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12854 -->
Da weiß ich nicht, was alt und was neu ist. Jedenfalls sind die Sockel unterschiedlich hoch.