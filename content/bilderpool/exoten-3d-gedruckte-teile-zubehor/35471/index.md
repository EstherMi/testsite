---
layout: "image"
title: "Clipsplatte 90x30; VDI3400"
date: "2012-09-08T18:23:45"
picture: "clipsplattevdi1.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/35471
imported:
- "2019"
_4images_image_id: "35471"
_4images_cat_id: "463"
_4images_user_id: "162"
_4images_image_date: "2012-09-08T18:23:45"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35471 -->
clipsplatte 90x30 mit darauf stehend: VDI 3400, das Fischer Logo und dan noch 5 Rechtecke mit da oben die nummern; 21, 24, 27, 30, 33.

Die Zahlen beschreiben die jeweilige Oberflächerauheit