---
layout: "overview"
title: "Magnetanhängerkupplung"
date: 2019-12-17T18:05:53+01:00
legacy_id:
- categories/2129
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2129 --> 
Anhängerkupplungen mit einem diametral magnetisierten Ringmagnet.
Außendurchmesser 10mm, Innendurchmesser 5mm, Höhe 5mm

Gekauft habe ich sie bei www.supermagnete.de

http://www.supermagnete.de/R-10-05-05-DN

Durch die diametrale Magnetisierung drehen sich die Magnete immer in die richtige Stellung.
D.h. man kann die Wagen z.B. von der Bauspielbahn vorwärts und rückwärts an den vorhergehenden Wagen anhängen.