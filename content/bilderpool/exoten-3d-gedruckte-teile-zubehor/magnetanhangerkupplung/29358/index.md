---
layout: "image"
title: "Kupplungsidee 2"
date: "2010-11-24T22:26:24"
picture: "SDC11022.jpg"
weight: "3"
konstrukteure: 
- "Ralf Geerken"
fotografen:
- "Ralf Geerken"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ThanksForTheFish"
license: "unknown"
legacy_id:
- details/29358
imported:
- "2019"
_4images_image_id: "29358"
_4images_cat_id: "2129"
_4images_user_id: "381"
_4images_image_date: "2010-11-24T22:26:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29358 -->
Endlich kann man die Bauspielbahn ohne großartige Fummelei aneinanderhängen.