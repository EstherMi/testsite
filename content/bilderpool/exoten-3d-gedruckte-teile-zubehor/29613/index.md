---
layout: "image"
title: "3D-Studie Hochseeschlepper, [2/3] Schottel-Ruderpropeller (SRP)"
date: "2011-01-05T19:20:46"
picture: "dstudiehochseeschlepper2.jpg"
weight: "18"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2, 2D-Kopie aus 3D-Fenster"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/29613
imported:
- "2019"
_4images_image_id: "29613"
_4images_cat_id: "463"
_4images_user_id: "723"
_4images_image_date: "2011-01-05T19:20:46"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29613 -->
Ansicht der Auf- und Einbauten des Modells ohne Schiffsrumpf von unten:
Zu besseren Ansicht sind die vier Bodenplatten 30x90 ausgeblendet. Beide Schiffsschrauben haben einen eigenen Antriebsmotor über m1,0-Z-Getriebe. Der mittlere Motor kann über eine noch nicht montierte m1,5-Kette die beiden Schiffsschrauben paarweise in beiden Richtungen bis zu 180° schwenken. Dazu sind die beiden Ruderpropeller als Hohlwellen ausgelegt, in denen sich die Schraubenantriebe frei drehen können. Die 3D-Teile XM- und Encoder-Motor sind hier ebenfalls eine 1:1-Bauteilsimulation. 
Diese Schottel-Ruderpropeller (SRP) - entwickelt und gebaut 1950 in der Schottel-Werft in Spay am Rhein - haben im Gegensatz zu offenen Schiffsschrauben einen höheren Wirkungsgrad, da hier Wasseranteile nicht frei verwirbelt, sondern durch einen umhüllenden Konus gepreßt werden. Die hohe Wendigkeit der damit ausgerüsteten Schiffe erfordert kein konventionelles Ruderblatt. Da bei Schleppern ihre Anordnung nebeneinander im vorderen Drittel der Rumpflänge auch bekannt ist, werde ich das zunächst in einer geometrischen Studie auch mal versuchen.