---
layout: "image"
title: "30493_I'm walking_Bauanleitung_Seite15_Ausschnitt"
date: "2009-05-30T09:12:55"
picture: "ergaenzunsseitenzupdfbauanleitungen4.jpg"
weight: "4"
konstrukteure: 
- "ft"
fotografen:
- "Ed"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ed"
license: "unknown"
legacy_id:
- details/24132
imported:
- "2019"
_4images_image_id: "24132"
_4images_cat_id: "1656"
_4images_user_id: "43"
_4images_image_date: "2009-05-30T09:12:55"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24132 -->
Erläuterung zur Fortbewegung auf vier Beinen I.