---
layout: "image"
title: "30493_I'm walking_Bauanleitung_Seite10_Ausschnitt"
date: "2009-05-30T09:12:55"
picture: "ergaenzunsseitenzupdfbauanleitungen2.jpg"
weight: "2"
konstrukteure: 
- "ft"
fotografen:
- "Ed"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ed"
license: "unknown"
legacy_id:
- details/24130
imported:
- "2019"
_4images_image_id: "24130"
_4images_cat_id: "1656"
_4images_user_id: "43"
_4images_image_date: "2009-05-30T09:12:55"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24130 -->
Erläuterung zur Fortbewegung auf zwei Beinen