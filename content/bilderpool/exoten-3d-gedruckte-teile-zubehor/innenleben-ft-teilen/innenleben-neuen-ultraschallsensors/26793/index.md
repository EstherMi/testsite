---
layout: "image"
title: "New 3-wire Ultra Sound Distance Sensor"
date: "2010-03-23T19:36:36"
picture: "innenlebendesneueultraschallsensor1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Ad"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- details/26793
imported:
- "2019"
_4images_image_id: "26793"
_4images_cat_id: "1914"
_4images_user_id: "716"
_4images_image_date: "2010-03-23T19:36:36"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26793 -->
New 3-wire Ultra Sound Distance Sensor