---
layout: "image"
title: "Polwendeschalter"
date: "2007-04-29T19:59:11"
picture: "platinen18.jpg"
weight: "19"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/10216
imported:
- "2019"
_4images_image_id: "10216"
_4images_cat_id: "2007"
_4images_user_id: "445"
_4images_image_date: "2007-04-29T19:59:11"
_4images_image_order: "18"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10216 -->
Es hat keine Platine, aber irgendwie passt er hier am besten hin.