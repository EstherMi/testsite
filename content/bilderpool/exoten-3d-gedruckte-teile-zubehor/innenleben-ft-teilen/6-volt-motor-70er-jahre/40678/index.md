---
layout: "image"
title: "6 volt Motor ohne Rückwand"
date: "2015-03-22T10:53:43"
picture: "motor01.jpg"
weight: "10"
konstrukteure: 
- "Pim Sturm"
fotografen:
- "Pim Sturm"
keywords: ["Motor"]
uploadBy: "pim_s"
license: "unknown"
legacy_id:
- details/40678
imported:
- "2019"
_4images_image_id: "40678"
_4images_cat_id: "3055"
_4images_user_id: "2398"
_4images_image_date: "2015-03-22T10:53:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40678 -->
