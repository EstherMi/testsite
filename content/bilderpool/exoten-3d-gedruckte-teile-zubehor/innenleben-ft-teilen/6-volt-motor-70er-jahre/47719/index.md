---
layout: "image"
title: "3. Motor von links komplett zerlegt"
date: "2018-07-13T18:04:58"
picture: "M3.jpg"
weight: "3"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/47719
imported:
- "2019"
_4images_image_id: "47719"
_4images_cat_id: "3055"
_4images_user_id: "579"
_4images_image_date: "2018-07-13T18:04:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47719 -->
Fehlerursache ist eine verschmorte Wicklung, die einen Widerstand kleiner 0,5 Ohm hat, während die beiden anderen Wicklungen bei > 6 Ohm liegen. Dieser Motor war nicht zu retten, aber die Rückplatte mit den Schleifkontakten des Kommutators habe ich in den 2. Motor von links eingebaut (dort war ein Schleifkontakt defekt).