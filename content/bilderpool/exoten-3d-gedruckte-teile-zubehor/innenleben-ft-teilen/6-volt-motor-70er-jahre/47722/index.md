---
layout: "image"
title: "Motoren ausgebaut"
date: "2018-07-13T18:04:58"
picture: "M6.jpg"
weight: "6"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/47722
imported:
- "2019"
_4images_image_id: "47722"
_4images_cat_id: "3055"
_4images_user_id: "579"
_4images_image_date: "2018-07-13T18:04:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47722 -->
Diese beiden konnten gerettet werden.