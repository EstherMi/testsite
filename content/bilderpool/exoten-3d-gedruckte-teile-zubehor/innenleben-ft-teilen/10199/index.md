---
layout: "image"
title: "Power Controller"
date: "2007-04-29T19:59:10"
picture: "platinen01.jpg"
weight: "2"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/10199
imported:
- "2019"
_4images_image_id: "10199"
_4images_cat_id: "2007"
_4images_user_id: "445"
_4images_image_date: "2007-04-29T19:59:10"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10199 -->
Dieses Foto wurde in einem am Abend geschossen, darum die schlechte Qualität.