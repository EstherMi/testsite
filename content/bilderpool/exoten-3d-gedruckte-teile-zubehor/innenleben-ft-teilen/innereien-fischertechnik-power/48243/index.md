---
layout: "image"
title: "Der Controller"
date: "2018-10-17T21:54:48"
picture: "innereiendesfischertechnikpowercontrollers1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48243
imported:
- "2019"
_4images_image_id: "48243"
_4images_cat_id: "3540"
_4images_user_id: "104"
_4images_image_date: "2018-10-17T21:54:48"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48243 -->
Das ist mein Controller. Es gibt wohl noch mindestens eine andere Fassung mit einem seitlichen Festspannungsausgang.