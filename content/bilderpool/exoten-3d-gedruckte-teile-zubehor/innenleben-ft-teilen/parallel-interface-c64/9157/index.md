---
layout: "image"
title: "Rückseite Version 2"
date: "2007-02-26T09:03:37"
picture: "image4.jpg"
weight: "6"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Thomas Brestrich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- details/9157
imported:
- "2019"
_4images_image_id: "9157"
_4images_cat_id: "845"
_4images_user_id: "120"
_4images_image_date: "2007-02-26T09:03:37"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9157 -->
vom Parallel-Interface für den C64