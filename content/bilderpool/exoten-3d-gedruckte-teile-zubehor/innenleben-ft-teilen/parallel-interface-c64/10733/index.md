---
layout: "image"
title: "Unterseite von C64 Interface-Platine"
date: "2007-06-07T13:45:12"
picture: "unten.jpg"
weight: "2"
konstrukteure: 
- "Fischertechnik"
fotografen:
- "Volker Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "willybaer"
license: "unknown"
legacy_id:
- details/10733
imported:
- "2019"
_4images_image_id: "10733"
_4images_cat_id: "845"
_4images_user_id: "609"
_4images_image_date: "2007-06-07T13:45:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10733 -->
Bezeichnung auf der Platine
48013-70400 (01)