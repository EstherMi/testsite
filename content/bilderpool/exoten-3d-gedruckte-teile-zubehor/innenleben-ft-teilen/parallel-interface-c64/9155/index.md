---
layout: "image"
title: "Vorderseite Version 1"
date: "2007-02-26T09:03:37"
picture: "image2.jpg"
weight: "4"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Thomas Brestrich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- details/9155
imported:
- "2019"
_4images_image_id: "9155"
_4images_cat_id: "845"
_4images_user_id: "120"
_4images_image_date: "2007-02-26T09:03:37"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9155 -->
vom Parallel-Interface für den C64
Bitte nicht an der fehlenden Diode stören :) Der Schaden stammt noch vom Vorbesitzer und wird noch behoben...