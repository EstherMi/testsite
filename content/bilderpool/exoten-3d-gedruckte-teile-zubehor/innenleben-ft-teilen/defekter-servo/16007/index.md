---
layout: "image"
title: "Servo Getriebe"
date: "2008-10-18T13:05:26"
picture: "servo1.jpg"
weight: "1"
konstrukteure: 
- "Severin"
fotografen:
- "Severin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Severin"
license: "unknown"
legacy_id:
- details/16007
imported:
- "2019"
_4images_image_id: "16007"
_4images_cat_id: "1455"
_4images_user_id: "558"
_4images_image_date: "2008-10-18T13:05:26"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16007 -->
Hier sieht man das Zahnrad das bei den meisten aufgegeben hat.