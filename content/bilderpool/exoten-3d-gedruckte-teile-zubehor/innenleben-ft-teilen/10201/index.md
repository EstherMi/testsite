---
layout: "image"
title: "Robo RF Data Link"
date: "2007-04-29T19:59:11"
picture: "platinen03.jpg"
weight: "4"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/10201
imported:
- "2019"
_4images_image_id: "10201"
_4images_cat_id: "2007"
_4images_user_id: "445"
_4images_image_date: "2007-04-29T19:59:11"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10201 -->
Von der anderen Seite.