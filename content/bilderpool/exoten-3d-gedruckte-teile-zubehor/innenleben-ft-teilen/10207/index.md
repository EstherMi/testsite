---
layout: "image"
title: "RF Data Link für Robo Interface"
date: "2007-04-29T19:59:11"
picture: "platinen09.jpg"
weight: "10"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/10207
imported:
- "2019"
_4images_image_id: "10207"
_4images_cat_id: "2007"
_4images_user_id: "445"
_4images_image_date: "2007-04-29T19:59:11"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10207 -->
Im Hintergrund die Robo IF Hülle.