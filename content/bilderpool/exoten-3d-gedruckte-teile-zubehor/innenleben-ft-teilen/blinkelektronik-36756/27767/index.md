---
layout: "image"
title: "Platine andere Seite"
date: "2010-07-16T23:32:21"
picture: "blinkelektronik3.jpg"
weight: "3"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/27767
imported:
- "2019"
_4images_image_id: "27767"
_4images_cat_id: "2000"
_4images_user_id: "373"
_4images_image_date: "2010-07-16T23:32:21"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27767 -->
Was ich noch garnicht wusste, ist dass da noch Extra-LEDs drin sind, die gleichzeitig mit der jeweiligen Lampe mitblinken. Wofür das gut sein soll, kann ich nicht so ganz nachvollziehen, vielleicht kann mich ja einer von euch erhellen.
Sehr interessant finde ich auch den Blinkrythmus. Die Lampen blinken nicht abwechseln, sondern gleichzeitig mit einem leicht unterschiedlichen Tempo. Dadurch entsteht - nach etwas Betriebsdauer - ein sehr realistisches, unregelmäßiges Wechselblinken wie in echt - perfekt für Blaulichtfahrzeuge.