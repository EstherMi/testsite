---
layout: "image"
title: "Siemens Transformer 6,8V"
date: "2007-04-29T19:59:11"
picture: "platinen16.jpg"
weight: "17"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/10214
imported:
- "2019"
_4images_image_id: "10214"
_4images_cat_id: "2007"
_4images_user_id: "445"
_4images_image_date: "2007-04-29T19:59:11"
_4images_image_order: "16"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10214 -->
Ein ziemliches wirrwar da drinnen, und raus bringst du nur den Trafo persönlich.