---
layout: "image"
title: "33_messaufbau"
date: "2008-01-26T13:12:33"
picture: "33_messaufbau.jpg"
weight: "34"
konstrukteure: 
- "IGARASHI"
fotografen:
- "Peter Krijnen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "pk"
license: "unknown"
legacy_id:
- details/13431
imported:
- "2019"
_4images_image_id: "13431"
_4images_cat_id: "993"
_4images_user_id: "144"
_4images_image_date: "2008-01-26T13:12:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13431 -->
Powermotor mit bauplatte 15x30, fototransistor und lampe.
Der fototransistor am h4GB die von der 5Volt stabilisator gespeist werd.
Ausgang am eingang einer Frequenszähler.
Für die drehzahl, die gemessene frequents mit 60 multiplisieren, ergibt die drehzahl in RPM.