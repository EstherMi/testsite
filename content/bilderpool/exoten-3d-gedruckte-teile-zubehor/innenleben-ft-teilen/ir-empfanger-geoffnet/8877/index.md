---
layout: "image"
title: "IR Empfänger"
date: "2007-02-04T12:35:36"
picture: "irempfaenger1.jpg"
weight: "1"
konstrukteure: 
- "Michael K."
fotografen:
- "Michael K."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Wert"
license: "unknown"
legacy_id:
- details/8877
imported:
- "2019"
_4images_image_id: "8877"
_4images_cat_id: "806"
_4images_user_id: "366"
_4images_image_date: "2007-02-04T12:35:36"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8877 -->
Der IR-Empfänger, hier geschlossen zu sehen