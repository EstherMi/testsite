---
layout: "image"
title: "ft-Akku-Lader ältere Version - Bild 3"
date: "2007-07-13T17:46:55"
picture: "ftladegeraete5.jpg"
weight: "8"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Thomas Brestrich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- details/11060
imported:
- "2019"
_4images_image_id: "11060"
_4images_cat_id: "1000"
_4images_user_id: "120"
_4images_image_date: "2007-07-13T17:46:55"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11060 -->
Das Steckernetzteil stellt nur eine ungeregelte Gleichspannung von etwa 15V bereit, Aufbau nahezu identisch zum ft-Netzteil mit 9V nur ohne Strombegrenzung