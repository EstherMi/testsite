---
layout: "image"
title: "ft-Akku-Lader aktuelle Version - Bild 5"
date: "2007-07-13T17:46:55"
picture: "ftladegeraete2.jpg"
weight: "5"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Thomas Brestrich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- details/11057
imported:
- "2019"
_4images_image_id: "11057"
_4images_cat_id: "1000"
_4images_user_id: "120"
_4images_image_date: "2007-07-13T17:46:55"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11057 -->
IC oben "24A1LTM TL494CN", IC links von Motorola "MC33340P XAA 0018", IC rechts nicht lesbar (ich tippe auf Festspannungsregler)