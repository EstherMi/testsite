---
layout: "image"
title: "ft-Akku-Lader ältere Version - Bild 4"
date: "2007-07-13T17:46:55"
picture: "ftladegeraete6.jpg"
weight: "9"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Thomas Brestrich"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- details/11061
imported:
- "2019"
_4images_image_id: "11061"
_4images_cat_id: "1000"
_4images_user_id: "120"
_4images_image_date: "2007-07-13T17:46:55"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11061 -->
SMD IC rechts Aufschrift "33340 XAXA", links "72AF LM78L 12ACM"