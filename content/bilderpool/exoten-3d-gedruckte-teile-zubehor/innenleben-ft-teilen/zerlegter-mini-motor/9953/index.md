---
layout: "image"
title: "Motor"
date: "2007-04-04T10:29:46"
picture: "motor_001.jpg"
weight: "1"
konstrukteure: 
- "Vormann Frederik"
fotografen:
- "Vormann Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/9953
imported:
- "2019"
_4images_image_id: "9953"
_4images_cat_id: "893"
_4images_user_id: "453"
_4images_image_date: "2007-04-04T10:29:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9953 -->
Ich habe hier noch einmal versucht den Motor mit seinem Typenzeichen zu knipsen.
Es ist nicht besinders gut geworden, aber man erkennt das runde Zeichen. Die Nummer hinten auf dem Silbernem Streife ist 3743.