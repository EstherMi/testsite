---
layout: "image"
title: "Motor zusammen gebaut"
date: "2007-04-02T10:19:04"
picture: "motor5.jpg"
weight: "6"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/9881
imported:
- "2019"
_4images_image_id: "9881"
_4images_cat_id: "893"
_4images_user_id: "453"
_4images_image_date: "2007-04-02T10:19:04"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9881 -->
Hier ist der Hintere Deckel wieder drauf.