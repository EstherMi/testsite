---
layout: "overview"
title: "Missratene Teile"
date: 2019-12-17T18:03:56+01:00
legacy_id:
- categories/646
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=646 --> 
Fertigungsfehler, die es geschafft haben, durch die Qualitätssicherung hindurch zu schlüpfen.