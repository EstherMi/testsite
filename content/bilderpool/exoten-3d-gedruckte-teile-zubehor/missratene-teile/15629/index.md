---
layout: "image"
title: "Missratene Bausteine"
date: "2008-09-26T08:06:05"
picture: "sonderteile2.jpg"
weight: "4"
konstrukteure: 
- "Fischerwerke"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/15629
imported:
- "2019"
_4images_image_id: "15629"
_4images_cat_id: "646"
_4images_user_id: "409"
_4images_image_date: "2008-09-26T08:06:05"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15629 -->
beim linken Baustein war doch schon ein Zapfen drin!