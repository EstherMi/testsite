---
layout: "image"
title: "MoMo01.jpg"
date: "2005-05-19T21:52:53"
picture: "MoMo01.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4162
imported:
- "2019"
_4images_image_id: "4162"
_4images_cat_id: "646"
_4images_user_id: "4"
_4images_image_date: "2005-05-19T21:52:53"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4162 -->
Sachen gibts!

Das sieht ganz nach einem Montagsmodell (Rosenmontag?) aus: der Zapfen ist genau 45° verdreht montiert und fest ist er auch. Der Zapfen gegenüber sitzt so, wie er sein soll.