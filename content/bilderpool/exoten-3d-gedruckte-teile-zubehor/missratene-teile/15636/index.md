---
layout: "image"
title: "Komische Teile"
date: "2008-09-26T08:06:06"
picture: "sonderteile9.jpg"
weight: "9"
konstrukteure: 
- "Fischerwerke"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/15636
imported:
- "2019"
_4images_image_id: "15636"
_4images_cat_id: "646"
_4images_user_id: "409"
_4images_image_date: "2008-09-26T08:06:06"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15636 -->
Der Durchmesser des linken Teiles ist 12mm. Sollte das mal eine Bauplatte 15 x15 werden ?
Aber das linke Teil sieht zu perfekt dazu aus.