---
layout: "image"
title: "Wer hat da geklebt?"
date: "2008-09-26T08:06:06"
picture: "sonderteile6.jpg"
weight: "6"
konstrukteure: 
- "Fischerwerke"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/15633
imported:
- "2019"
_4images_image_id: "15633"
_4images_cat_id: "646"
_4images_user_id: "409"
_4images_image_date: "2008-09-26T08:06:06"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15633 -->
Gab es die Bauplatten so original, oder hat da einer geklebt ?