---
layout: "comment"
hidden: true
title: "7504"
date: "2008-10-07T08:56:25"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Sehr nett, aber hier unter der Rubrik Bauteilvarianten sollten wir damit wieder aufhören, in der Fertigung mißlungene Bauteile oder gar Ausschußteile zu präsentieren. Solche Teile sind keine Bauteilvarianten und hier schlichtweg fehl am Platz.