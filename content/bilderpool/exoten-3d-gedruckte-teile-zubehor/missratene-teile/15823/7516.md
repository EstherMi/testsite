---
layout: "comment"
hidden: true
title: "7516"
date: "2008-10-08T07:20:44"
uploadBy:
- "HLGR"
license: "unknown"
imported:
- "2019"
---
Das Foto habe ich mit einem 105 mm Macroobjektiv und einem Zwischenring gemacht.

Mit meinem Retroadapter sind noch ganz andere Vergrößerungen möglich.

http://www.fotocommunity.de/pc/pc/mypics/635751