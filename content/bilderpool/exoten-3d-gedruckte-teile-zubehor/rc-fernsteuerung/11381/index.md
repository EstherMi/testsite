---
layout: "image"
title: "ft-Dragster"
date: "2007-08-13T17:04:26"
picture: "PICT1951.jpg"
weight: "1"
konstrukteure: 
- "Bumpf"
fotografen:
- "Walter-Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/11381
imported:
- "2019"
_4images_image_id: "11381"
_4images_cat_id: "875"
_4images_user_id: "424"
_4images_image_date: "2007-08-13T17:04:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11381 -->
ft-Dragster mit 2 Geschwindigkeiten. Der Akku liefert 6 und 9 Volt.