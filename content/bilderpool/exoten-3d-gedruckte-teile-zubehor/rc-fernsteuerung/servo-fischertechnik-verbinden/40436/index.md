---
layout: "image"
title: "Standardservoeinbau (Seitenansicht)"
date: "2015-02-02T17:12:59"
picture: "StdServoEingebautSeite.jpg"
weight: "2"
konstrukteure: 
- "rfeneberg"
fotografen:
- "rfeneberg"
keywords: ["Servo", "RC"]
uploadBy: "rfeneberg"
license: "unknown"
legacy_id:
- details/40436
imported:
- "2019"
_4images_image_id: "40436"
_4images_cat_id: "1211"
_4images_user_id: "347"
_4images_image_date: "2015-02-02T17:12:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40436 -->
Einbau eines Standardservos (Seitenansicht)