---
layout: "image"
title: "1"
date: "2008-01-13T22:29:28"
picture: "1.jpg"
weight: "3"
konstrukteure: 
- "Howey"
fotografen:
- "Howey"
keywords: ["Servo", "fischertechnik", "Baustein", "verbinden"]
uploadBy: "fishfriend"
license: "unknown"
legacy_id:
- details/13328
imported:
- "2019"
_4images_image_id: "13328"
_4images_cat_id: "1211"
_4images_user_id: "34"
_4images_image_date: "2008-01-13T22:29:28"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13328 -->
Hallo...
Hier die Bauteile:
Normales RC Servo von Conrad ca 3 Euro
Servo Befestigungsschraube 15mm lang mit breitem Kopf (alternativ evt mit Unterlegscheibe)
Ausgleichshülse von 3mm auf 4mm (geht auch mit Messingrohr 4mm (1cm absägen)
32321 Baustein 15 mit Ansenkung
Gruß
Holger Howey