---
layout: "comment"
hidden: true
title: "22611"
date: "2016-10-20T11:31:08"
uploadBy:
- "uffi"
license: "unknown"
imported:
- "2019"
---
Hallo David, danke für Deine ausführliche Beschreibung!

Ich habe mit verschiedenen IR-Empfänger IC gearbeitet und hatte folgende Fehlerraten:

- SFH5110-38 (Osram): ca. 5-7% Parity Fehler

- TSSP4P38: ca. 20% Parity Fehler sowie unerkannte Doppelfehler, die ein korrektes Parity-Bit haben. Dieses IC ist aber eigentlich für Entfernungsmessungen gedacht und nicht als Empfänger für Fernbedienungen, da es einen speziellen logarithmischen AGC hat.

Ähnlich schlecht lief es auch mit einem SFH5110-36 (36 kHz Empfänger, der ist ja auch nicht wirklich geeignet für 38 kHz Modulation) .

Ich nehme mal an, dass am Robo-Interface ebenfalls ein 36kHz Empfänger verbaut ist, da der ja für die alte Fernbedienung aus 30344 mit RC5 Code gedacht war. Deshalb wundern mich die niedrigen Fehlerraten von Ad.

Die TSOP34338 bekommt man leider nur bei den Distributoren, also Digi-Key, Farnell, Rutronik etc. und ich vermute, dass man da privat nicht bestellen kann...

Alternativ könnte man noch einen TSOP4138 mit AGC1 nehmen (gleiche Zeitkonstante wie bei AGC3), die haben allerdings keine Unterdrückung von Störsignalen durch Fluoreszenzlampen.

Gruß, Dirk