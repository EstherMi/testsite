---
layout: "comment"
hidden: true
title: "22743"
date: "2016-11-13T08:43:48"
uploadBy:
- "uffi"
license: "unknown"
imported:
- "2019"
---
Bin jetzt an einen TSOP34538 rangekommen, der ist ähnlich wie der TSOP34338, nur mit stärkerer Störunterdrückung für Energiesparlampen.

Die Fehlerhäufigkeit ist damit unter 1%