---
layout: "image"
title: "IR Empfänger im Gehäuse"
date: "2016-05-06T10:15:54"
picture: "aire1.jpg"
weight: "5"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/43335
imported:
- "2019"
_4images_image_id: "43335"
_4images_cat_id: "3219"
_4images_user_id: "2228"
_4images_image_date: "2016-05-06T10:15:54"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43335 -->
Der IR Empfänger ist in einem Kunststoffgehäuse untergebracht. In die Oberseite wurden die notwendigen Beschriftungen und Aussparungen für die Anschlüsse, den Reset Taster, einen DIP Schalter, den TSOP 4838 IR Receiver, Servo / Sensor Pins und die Anzeige-LED 13 gefräst.
Das Shield bietet zwei Motoranschlüsse, einen OUTPUT bis zu 800mA und drei Servo- bzw. Sensoranschlüsse (beispielsweise für Fahrerassistenzsysteme). Die Spannung an den Ausgängen beträgt 5-12 Volt (je nach angelegter Spannung), an den Servopins ist sie auf 5V begrenzt.