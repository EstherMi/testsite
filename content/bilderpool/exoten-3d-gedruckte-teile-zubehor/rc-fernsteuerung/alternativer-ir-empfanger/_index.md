---
layout: "overview"
title: "Alternativer IR Empfänger"
date: 2019-12-17T18:04:34+01:00
legacy_id:
- categories/3219
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3219 --> 
Dies ist ein alternativer Infrarot Empfänger für 38kHz Signale bestehend aus einem Arduino Uno SMD Edition und einem eigens dafür entwickelten Shield. Er kann das Signal des fischertechnik Control Sets demodulieren, grundsätzlich funktioniert dies auch mit Signalen einer beliebigen Universalfernbedienung beispielsweise von TV Geräten. 