---
layout: "image"
title: "Nano RC-Funk-Empfänger Shield mit NRF24 verlötet"
date: "2016-05-30T14:53:40"
picture: "RC_mit_NRF24.jpg"
weight: "2"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Nano", "RC-Funk-Empfänger", "Shield", "NRF24"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/43447
imported:
- "2019"
_4images_image_id: "43447"
_4images_cat_id: "3219"
_4images_user_id: "579"
_4images_image_date: "2016-05-30T14:53:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43447 -->
Nano RC-Funk-Empfänger Shield mit NRF24 verlötet