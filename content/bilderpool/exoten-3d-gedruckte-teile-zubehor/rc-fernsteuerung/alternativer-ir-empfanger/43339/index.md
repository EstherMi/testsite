---
layout: "image"
title: "Gehäuse"
date: "2016-05-06T10:15:54"
picture: "aire5.jpg"
weight: "9"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/43339
imported:
- "2019"
_4images_image_id: "43339"
_4images_cat_id: "3219"
_4images_user_id: "2228"
_4images_image_date: "2016-05-06T10:15:54"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43339 -->
Das Gehäuse wird wie abgebildet zusammengesteckt. Dies ist erstaunlich stabil, Kleben ist nicht erforderlich.