---
layout: "image"
title: "Nano RC-2,4GHz-Funkempfänger / Ansichten"
date: "2016-05-30T14:53:40"
picture: "RC_gedreht.jpg"
weight: "3"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Nano", "RC", "2", "4GHz", "Funkempfänger", "Funkfernsteuerung"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/43448
imported:
- "2019"
_4images_image_id: "43448"
_4images_cat_id: "3219"
_4images_user_id: "579"
_4images_image_date: "2016-05-30T14:53:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43448 -->
Nano RC-2,4GHz-Funkempfänger / Ansichten

Das Motor-Shield mit dem VNH2SP30 Motortreiber ist noch nicht dabei. Das wird über ein Flachbandkabel mit 3 Steuerleitungen und Masse an den Arduino Nano angeschlossen.