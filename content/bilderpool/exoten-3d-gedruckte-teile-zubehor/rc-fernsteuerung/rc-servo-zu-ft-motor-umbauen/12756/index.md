---
layout: "image"
title: "Beispiel: RC-Servo als Radantrieb"
date: "2007-11-16T17:33:55"
picture: "beispiel_antrieb.jpg"
weight: "2"
konstrukteure: 
- "stefanft"
fotografen:
- "stefanft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stefanft"
license: "unknown"
legacy_id:
- details/12756
imported:
- "2019"
_4images_image_id: "12756"
_4images_cat_id: "1143"
_4images_user_id: "672"
_4images_image_date: "2007-11-16T17:33:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12756 -->
Ein simpler Achsantrieb per umgebautem RC-Servo. Die Geschwindigkeit ist bei Verwendung eines Proportionalkanals amSender stufenlos regelbar, bei Verwendung eines Schaltkanals natürlich nicht. Bei Verwendung mehrerer Servos an verschiedenen Achsen empfielht sich natürlich ein programmierbarer Sender, bei dem man 2 Servos auf einen Steuerknüppel/-schalter legen kann.