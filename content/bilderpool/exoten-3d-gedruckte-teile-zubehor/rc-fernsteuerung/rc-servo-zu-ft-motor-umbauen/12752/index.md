---
layout: "image"
title: "Die ft-kompatible Kupplung"
date: "2007-11-14T18:06:46"
picture: "rcservozuftmotorumbauen4.jpg"
weight: "7"
konstrukteure: 
- "stefanft"
fotografen:
- "stefanft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stefanft"
license: "unknown"
legacy_id:
- details/12752
imported:
- "2019"
_4images_image_id: "12752"
_4images_cat_id: "1143"
_4images_user_id: "672"
_4images_image_date: "2007-11-14T18:06:46"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12752 -->
Nach Zusammenbau des Servos muss natürlich noch ein ft-kompatibler Anschluß für den Antrieb her. Möglichkeiten, passende ft-Bauteile an Ruderhebel/-scheibe des Servos zu befestigen, gibt es endlos viele. Ich habe hier einfach ein ft Rastritzel Z10 (ft Nr. 35945) genommen und an der Unterseite kreisrund so ausgefräst, dass es genau auf die Achse des Servos passt. Nach dem Aufsetzen des ft-Rastritzels auf die Servo-Achse wird einfach mit einem 1 mm Bohrer durch Ritzel und Servo-Achse gebohrt.