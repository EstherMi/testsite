---
layout: "image"
title: "Zu bearbeitendes Ritzel von unten"
date: "2007-11-14T18:06:46"
picture: "rcservozuftmotorumbauen3.jpg"
weight: "6"
konstrukteure: 
- "stefanft"
fotografen:
- "stefanft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stefanft"
license: "unknown"
legacy_id:
- details/12751
imported:
- "2019"
_4images_image_id: "12751"
_4images_cat_id: "1143"
_4images_user_id: "672"
_4images_image_date: "2007-11-14T18:06:46"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12751 -->
Auf der Unterseite ist das Ritzel, bei dem an der Oberseite die "Nase" entfernt wurde, so ausgeformt, dass es auf die Achse des Drehpotis im Servo (siehe Bild 2 - die Achse links oben auf dem Servo) passt. Bei Drehung des Servo-Motors dreht dieses Ritzel das Poti mit - dadurch bekommt der Sender gesagt, wie die aktuelle Stellung des Servos ist. Für die Verwendung des Servos als ft-Motor ist diese Rückmeldung aber unerwünscht! Sie würde dazu führen, dass sich der Motor nur um gut 180° dreht.

Deswegen wird die kreuzförmige Aufnahme für die Achse des Potis einfach kreisrund so groß ausgefräst, so dass sich nach dem Zusammenbau des Servos dieses Ritzel drehen kann, ohne dabei die Achse des Potis zu bewegen. 

Nachdem das erledigt ist, wird das Servo wieder mit Gehäuse zusammen gesetzt. Nun bitte ein Test an der RC-Fernbedienung: das so modifizierte Servo sollte sich an einem Proportional- oder Schaltkanal "endlos" in die gewünschte Richtung drehen, ohne dass es dabei in der Servo-Mechanik "knirscht".