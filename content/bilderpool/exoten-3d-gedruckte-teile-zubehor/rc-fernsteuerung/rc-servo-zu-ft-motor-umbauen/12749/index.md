---
layout: "image"
title: "Endergebnis - ft-Motor aus RC-Servo mit ft-Rastkupplung"
date: "2007-11-14T18:06:45"
picture: "rcservozuftmotorumbauen1.jpg"
weight: "4"
konstrukteure: 
- "stefanft"
fotografen:
- "stefanft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stefanft"
license: "unknown"
legacy_id:
- details/12749
imported:
- "2019"
_4images_image_id: "12749"
_4images_cat_id: "1143"
_4images_user_id: "672"
_4images_image_date: "2007-11-14T18:06:45"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12749 -->
So sieht's aus, wenn's fertig ist: Motor mit ft- Rastkupplung zur Befestigung von was-auch-immer. Die ft-kompatible Befestigung des Servos mit ft S-Riegeln ist übernommen von Alfred S. (Danke !!!), dessen RC-Projekt auf dieser Website zu sehen ist.