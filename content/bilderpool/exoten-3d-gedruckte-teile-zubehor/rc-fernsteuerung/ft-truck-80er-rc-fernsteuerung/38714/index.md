---
layout: "image"
title: "Schaltung Servoelektronik mit FT-Servo der 80er"
date: "2014-04-27T16:09:00"
picture: "schaltung.jpg"
weight: "15"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/38714
imported:
- "2019"
_4images_image_id: "38714"
_4images_cat_id: "2878"
_4images_user_id: "1359"
_4images_image_date: "2014-04-27T16:09:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38714 -->
wie ich vorher schon beschrieben habe, verwende ich die Elektronik eines ausgeschlachteten Modellbauservos. 
Die beiden Leitungen (rot und blau rechts von der Platine) habe ich mit 2 Buchsen versehen (im Schrumpfschlauch) und führen zum Motoranschluss des FT-Servos.

Links das Dreierpaket (Gelb/Rot/Schwarz) geht zum Anschluss des RC-Empfängers (hier Robbe / Futaba, analoges positives Impulssignal)

die drei weiteren Anschlüsse (Grün(gn) Gelb(gb) Rot(rt) waren vor dem Umbau am Poti des ausgeschlachteten Servos angeschlossen (gelb Mittelabgriff, gn rechts, rot links)
jetzt gehen sie an einen Spannungsteiler, bestehend aus 5k Trimmer (zw. Grün und Gelb) und Poti-Anschluss des FT-Servos (dunkelroter 3poliger Flachstecker, blau und Schwarz), wobei ein Pol frei bleibt.
Die beiden anderen gehen auf gelb und blau der Servoelektronik.

sollte sich der Motor in die falsche Richtung drehen, bzw. ständig an den Endanschlag fahren, ist die Polung des Motos zu wechseln und ggf. der 5K-Trimmer, welcher gleichzeitig auch eine genaue Mitteleinstellung ermöglicht, abzugleichen.

Achtung!! andere Servoelektroniken haben natürlich ggf. andere Kabelfarben!