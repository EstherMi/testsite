---
layout: "image"
title: "Stromversorgung"
date: "2014-04-13T18:18:16"
picture: "IMG_0007.jpg"
weight: "22"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/38549
imported:
- "2019"
_4images_image_id: "38549"
_4images_cat_id: "2878"
_4images_user_id: "1359"
_4images_image_date: "2014-04-13T18:18:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38549 -->
Fahrakku ist der 9V ft Akkupack, für Empfänger und Servo hab ich ein 4,8V Akkupack verwendet, altrnativ geht natürlich auch 4x NiCd Zelle im Motorantriebsgehäuse für diesen Zweck
27.4.: das war jetzt der Prototype, die neue Version in den folgenden Bildern ist eleganter verkabelt..!