---
layout: "image"
title: "Empfänger im Powerblockgehäuse"
date: "2014-04-27T16:09:00"
picture: "IMG_0105.jpg"
weight: "11"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/38710
imported:
- "2019"
_4images_image_id: "38710"
_4images_cat_id: "2878"
_4images_user_id: "1359"
_4images_image_date: "2014-04-27T16:09:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38710 -->
den Empfänger  habe ich da nur einfach reingelegt, selbst die mit eingelegte, zusammengewickelte Antenne ergibt noch ordentliche Empfangseigenschaften