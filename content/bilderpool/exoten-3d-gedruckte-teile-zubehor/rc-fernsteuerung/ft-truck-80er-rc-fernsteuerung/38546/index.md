---
layout: "image"
title: "Antrieb unten"
date: "2014-04-13T18:18:16"
picture: "IMG_0004.jpg"
weight: "19"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: ["Universalfahrzeug", "RC", "Fernlenkung", "Fernlenkset"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/38546
imported:
- "2019"
_4images_image_id: "38546"
_4images_cat_id: "2878"
_4images_user_id: "1359"
_4images_image_date: "2014-04-13T18:18:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38546 -->
Servo, Motorantrieb und Differential sind "klassisch" montiert, siehe Anleitung Universalfahrzeug, (30481 / 1984)