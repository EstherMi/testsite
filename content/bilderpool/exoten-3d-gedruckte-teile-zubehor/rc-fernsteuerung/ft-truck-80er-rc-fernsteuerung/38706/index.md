---
layout: "image"
title: "andere Seite der Servoelektronik mit dem von mir hinzugefügten Trimmer"
date: "2014-04-27T16:09:00"
picture: "IMG_0101.jpg"
weight: "7"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/38706
imported:
- "2019"
_4images_image_id: "38706"
_4images_cat_id: "2878"
_4images_user_id: "1359"
_4images_image_date: "2014-04-27T16:09:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38706 -->
dieser Trimmer und das Poti im Inneren des FT-Servos bilden einen Spannungsteiler mit Mittelabgriff am alten Mittelanschluss des alten ausgeschalchteten Servos