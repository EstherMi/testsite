---
layout: "comment"
hidden: true
title: "16226"
date: "2012-01-30T10:42:05"
uploadBy:
- "alfred.s"
license: "unknown"
imported:
- "2019"
---
Die Fernsteuerung bestand damals aus einem Standard 2-Kanal-Sender eines Motorseglers plus einem bei Conrad gekauften Standard 2-Kanal-Empfaengers plus Motorsteuerung und Servo. Also alles Standard PPM-RC Bauteile. Der ft-Servo aus dem neuen IR-Control-set ist zu diesen RC-Teilen kompatibel, die Lenkung koennte man inzwischen also auch mit dem ft-Servo realisieren.

Gruesse
Alfred