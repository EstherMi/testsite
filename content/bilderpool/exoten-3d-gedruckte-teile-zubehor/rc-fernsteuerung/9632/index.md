---
layout: "image"
title: "Alu-Ruderhörner Original und mit 4,5mm-Loch"
date: "2007-03-20T18:02:59"
picture: "Alu-Ruderhrner_Original_und_aufgebohrt.jpg"
weight: "16"
konstrukteure: 
- "Svabenicky Alfred"
fotografen:
- "Svabenicky Alfred"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "alfred.s"
license: "unknown"
legacy_id:
- details/9632
imported:
- "2019"
_4images_image_id: "9632"
_4images_cat_id: "875"
_4images_user_id: "390"
_4images_image_date: "2007-03-20T18:02:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9632 -->
