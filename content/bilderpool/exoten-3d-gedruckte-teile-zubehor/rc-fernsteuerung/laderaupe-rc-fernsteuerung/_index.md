---
layout: "overview"
title: "Laderaupe mit RC-Fernsteuerung"
date: 2019-12-17T18:04:28+01:00
legacy_id:
- categories/1153
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1153 --> 
Eine einfache Laderaupe, die mit einer 3-Kanal RC-Fernsteuerung bedient wird. Als Antriebsmotoren werden umgebaute RC-Servos verwendet, wie hier in einem anderen Projekt beschrieben.