---
layout: "image"
title: "Chassis"
date: "2007-11-22T17:42:53"
picture: "laderaupemitrcfernsteuerung1.jpg"
weight: "1"
konstrukteure: 
- "stefanft"
fotografen:
- "stefanft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stefanft"
license: "unknown"
legacy_id:
- details/12793
imported:
- "2019"
_4images_image_id: "12793"
_4images_cat_id: "1153"
_4images_user_id: "672"
_4images_image_date: "2007-11-22T17:42:53"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12793 -->
Das Chassis mit Antriebsmotoren (umgebaute RC-Servos) und RC-Empfänger. Je ein Servo für den Antrieb der Ketten; das um eine Achse drehbar gelagerte Servo vorn betreibt die Schaufel der Laderaupe.