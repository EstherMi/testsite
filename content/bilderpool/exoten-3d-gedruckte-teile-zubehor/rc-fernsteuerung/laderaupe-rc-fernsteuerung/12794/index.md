---
layout: "image"
title: "Fertiges Modell 1"
date: "2007-11-22T17:42:54"
picture: "laderaupemitrcfernsteuerung2.jpg"
weight: "2"
konstrukteure: 
- "stefanft"
fotografen:
- "stefanft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stefanft"
license: "unknown"
legacy_id:
- details/12794
imported:
- "2019"
_4images_image_id: "12794"
_4images_cat_id: "1153"
_4images_user_id: "672"
_4images_image_date: "2007-11-22T17:42:54"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=12794 -->
Das Führerhaus beherbergt RC-Empfänger und Akku. Über den ft-Taster am Dach des Führerhauses wird der Empfänger ein-/ausgeschaltet.