---
layout: "image"
title: "2-Kanal-RC-Empfänger auf Bauplatte 30x15"
date: "2007-03-20T16:49:26"
picture: "Empfaenger_mit_Bauplatte.jpg"
weight: "10"
konstrukteure: 
- "Svabenicky Alfred"
fotografen:
- "Svabenicky Alfred"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "alfred.s"
license: "unknown"
legacy_id:
- details/9626
imported:
- "2019"
_4images_image_id: "9626"
_4images_cat_id: "875"
_4images_user_id: "390"
_4images_image_date: "2007-03-20T16:49:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=9626 -->
