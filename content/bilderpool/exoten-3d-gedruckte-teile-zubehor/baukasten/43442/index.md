---
layout: "image"
title: "Fischergeometric 5000"
date: "2016-05-30T14:53:40"
picture: "Geo_1_klein.jpg"
weight: "1"
konstrukteure: 
- "Fischertechnik"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- details/43442
imported:
- "2019"
_4images_image_id: "43442"
_4images_cat_id: "3171"
_4images_user_id: "10"
_4images_image_date: "2016-05-30T14:53:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43442 -->
