---
layout: "image"
title: "Raspi08.jpg"
date: "2016-09-06T20:55:11"
picture: "Raspi08.jpg"
weight: "8"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/44338
imported:
- "2019"
_4images_image_id: "44338"
_4images_cat_id: "3272"
_4images_user_id: "4"
_4images_image_date: "2016-09-06T20:55:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44338 -->
Damit sich die Ecken nicht vom Bett lösen, muss ein großflächiger Haftgrund da hin. Mit 0,2 mm Höhe bekomme ich auf meinem Velleman genau 1 Lage PLA dort hin.