---
layout: "image"
title: "Kiste mit Widerstand 03"
date: "2017-01-07T20:22:20"
picture: "kistemitwiderstand3.jpg"
weight: "3"
konstrukteure: 
- "Wolfgang"
fotografen:
- "Wolfgang"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "webstar"
license: "unknown"
legacy_id:
- details/45012
imported:
- "2019"
_4images_image_id: "45012"
_4images_cat_id: "3348"
_4images_user_id: "1355"
_4images_image_date: "2017-01-07T20:22:20"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45012 -->
Vordere Sicht des Deckels mit den Buchsen