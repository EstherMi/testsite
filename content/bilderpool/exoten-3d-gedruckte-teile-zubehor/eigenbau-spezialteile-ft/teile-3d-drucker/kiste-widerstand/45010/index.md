---
layout: "image"
title: "Kiste mit Widerstand 01"
date: "2017-01-07T20:22:20"
picture: "kistemitwiderstand1.jpg"
weight: "1"
konstrukteure: 
- "Wolfgang"
fotografen:
- "Wolfgang"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "webstar"
license: "unknown"
legacy_id:
- details/45010
imported:
- "2019"
_4images_image_id: "45010"
_4images_cat_id: "3348"
_4images_user_id: "1355"
_4images_image_date: "2017-01-07T20:22:20"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45010 -->
Hier sieht man die offene Kiste. Auf den 4 Seiten sind je zwei Nuten, in denen man dann Fischertechnikteile befestigen kann.