---
layout: "image"
title: "RaspiCAD02.jpg"
date: "2016-01-30T10:57:40"
picture: "RaspiCAD02-800.jpg"
weight: "11"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["Raspi", "Raspberry", "Halterung"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/42842
imported:
- "2019"
_4images_image_id: "42842"
_4images_cat_id: "3272"
_4images_user_id: "4"
_4images_image_date: "2016-01-30T10:57:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42842 -->
Halterung für einen Raspberry 2. Um rundherum alle Anschlüsse zugängig zu haben, muss man die ft-Streben auf der Unterseite längs (d.h. entlang der langen Seiten) verlaufen lassen.