---
layout: "image"
title: "Stromverteiler Gehäuse"
date: "2018-06-13T16:43:05"
picture: "stromverteiler2.jpg"
weight: "2"
konstrukteure: 
- "DirkW"
fotografen:
- "DirkW"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "DirkW"
license: "unknown"
legacy_id:
- details/47689
imported:
- "2019"
_4images_image_id: "47689"
_4images_cat_id: "3518"
_4images_user_id: "2303"
_4images_image_date: "2018-06-13T16:43:05"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47689 -->
Das Gehäuse als *.stl findet ihr unter:
https://ftcommunity.de/data/downloads/3ddruckdateien/stromverteiler.zip