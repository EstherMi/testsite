---
layout: "comment"
hidden: true
title: "22507"
date: "2016-09-11T21:05:42"
uploadBy:
- "Phil"
license: "unknown"
imported:
- "2019"
---
Als Halter für die Drahtrampen auf dem Flipper. Der Draht ist 2mm dick, genau den Durchmesser haben auch die kleinen Nuten (sind 4 davon, 2 Halb- und an den Ecken 2 Viertelkreise). In diese wird dann der Draht geklebt. Auf dieser Drahtanordnung wird dann die Kugel rollen.
Das hier ist aber nur ein Prototyp, um zu schauen wie das mit dem Zapfen funktuiniert.

P