---
layout: "image"
title: "Passt genau!"
date: "2016-09-10T14:26:54"
picture: "bild2.jpg"
weight: "4"
konstrukteure: 
- "3D-Drucker"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/44345
imported:
- "2019"
_4images_image_id: "44345"
_4images_cat_id: "3272"
_4images_user_id: "1624"
_4images_image_date: "2016-09-10T14:26:54"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44345 -->
Der Zapfen hat beim ersten Versuch genau in die Nut gepasst!