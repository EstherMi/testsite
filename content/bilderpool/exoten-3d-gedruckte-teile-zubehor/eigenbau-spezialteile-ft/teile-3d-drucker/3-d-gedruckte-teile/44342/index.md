---
layout: "image"
title: "Zahnkranz"
date: "2016-09-09T19:32:07"
picture: "dgedruckteteile1.jpg"
weight: "1"
konstrukteure: 
- "allsystemgmbh"
fotografen:
- "allsystemgmbh"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "allsystemgmbh"
license: "unknown"
legacy_id:
- details/44342
imported:
- "2019"
_4images_image_id: "44342"
_4images_cat_id: "3274"
_4images_user_id: "1688"
_4images_image_date: "2016-09-09T19:32:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44342 -->
stl.File via Thingiverse
http://www.thingiverse.com/thing:551631
