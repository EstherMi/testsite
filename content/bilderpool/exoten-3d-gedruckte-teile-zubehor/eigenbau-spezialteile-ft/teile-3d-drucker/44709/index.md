---
layout: "image"
title: "TXT Kamera Zahnrad / Bild 2 von 2"
date: "2016-10-30T12:19:36"
picture: "Zahnrad_2.jpg"
weight: "2"
konstrukteure: 
- "Wolfgang"
fotografen:
- "Wolfgang"
keywords: ["TXT", "Kamera", "Zahnrad"]
uploadBy: "webstar"
license: "unknown"
legacy_id:
- details/44709
imported:
- "2019"
_4images_image_id: "44709"
_4images_cat_id: "3272"
_4images_user_id: "1355"
_4images_image_date: "2016-10-30T12:19:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44709 -->
Durch den Beitrag 
https://forum.ftcommunity.de/viewtopic.php?f=6&t=3419&p=23662&hilit=Digitalkamera#p23458

und dem dazu gehörigen Video 
https://www.youtube.com/watch?v=Zgj_jSWz3dk&feature=youtu.be

habe ich ein Zahnrad konstruiert, welches auf die TXT &#8211; Kamera drauf passt.