---
layout: "overview"
title: "BSB Tenderlok"
date: 2019-12-17T18:06:04+01:00
legacy_id:
- categories/3509
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3509 --> 
Mein Sohn und ich haben versucht mit dem 3D-Drucker die BSB-Tenderlok aus dem Jahr 1980 zu bauen.

Unter https://www.youtube.com/watch?v=5jByyONMSr4 seht ihr die BSB-Dampflok und die von uns gebaute BSB-Tenderlok.
