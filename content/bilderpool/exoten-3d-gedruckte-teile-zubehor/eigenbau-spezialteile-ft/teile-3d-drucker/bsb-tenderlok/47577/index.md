---
layout: "image"
title: "Kesselteil von der Seite"
date: "2018-05-02T20:36:08"
picture: "bsbtenderlok4.jpg"
weight: "4"
konstrukteure: 
- "Wolfgang"
fotografen:
- "Wolfgang"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "webstar"
license: "unknown"
legacy_id:
- details/47577
imported:
- "2019"
_4images_image_id: "47577"
_4images_cat_id: "3509"
_4images_user_id: "1355"
_4images_image_date: "2018-05-02T20:36:08"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47577 -->
Der Kessel von der Seite