---
layout: "image"
title: "Zusammengebaut von der Seite"
date: "2018-05-02T20:36:08"
picture: "bsbtenderlok1.jpg"
weight: "1"
konstrukteure: 
- "Wolfgang"
fotografen:
- "Wolfgang"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "webstar"
license: "unknown"
legacy_id:
- details/47574
imported:
- "2019"
_4images_image_id: "47574"
_4images_cat_id: "3509"
_4images_user_id: "1355"
_4images_image_date: "2018-05-02T20:36:08"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47574 -->
Hier seht ihr die BSB-Tenderlok von der Seite. Den Kessel, Rauchfang und andere Teile haben wir selber konstruiert und mit dem 3D-Drucker ausgedruckt