---
layout: "image"
title: "Ferdsch!"
date: "2017-01-01T21:43:29"
picture: "gl6.jpg"
weight: "9"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/45001
imported:
- "2019"
_4images_image_id: "45001"
_4images_cat_id: "3346"
_4images_user_id: "4"
_4images_image_date: "2017-01-01T21:43:29"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45001 -->
zu deutsch: Fertig :-)

Die Planeten sind außen montiert und nehmen deswegen innen keinen Platz weg.

Der Motor für Geradeausfahrt ist quer an der Rückwand montiert. Die Messingachse führt quer durch beide Planetengetriebe hindurch und treibt die Z10-Sonnenräder gleichsinnig an.

Das Lenkgetriebe besteht aus zwei Reifen 45, die mit einem Rastkegelzahnrad vom Motor an der Wannenseite angetrieben werden. Die Rastachsen führen zu zwei Rast-Z10, die den Zahnkranz Z30 an der Außenwand der Wanne antreiben. Die Kette läuft auf dem Zahnkranz Z32.

Die Flachnabe ganz außen drauf ist mehr als nur Verzierung: sie muss auch verhindern, dass das Planetengetriebe auseinander fällt.

In leicht abgewandelter Form wird das Getriebe im "Panzer 3" verwendet:
https://ftcommunity.de/categories.php?cat_id=3440