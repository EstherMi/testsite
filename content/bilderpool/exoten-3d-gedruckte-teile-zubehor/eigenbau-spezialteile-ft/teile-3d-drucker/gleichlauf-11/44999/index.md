---
layout: "image"
title: "Einzelteile"
date: "2017-01-01T21:43:29"
picture: "gl4.jpg"
weight: "7"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/44999
imported:
- "2019"
_4images_image_id: "44999"
_4images_cat_id: "3346"
_4images_user_id: "4"
_4images_image_date: "2017-01-01T21:43:29"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44999 -->
Die Teile, frisch gedruckt und zum Teil schon mit Sandpapier geglättet. 

Die drei Planeten Z14 sind auf eine Schraube m4 aufgezogen, damit sie im Drehbänkchen auf Maß rund gedreht und mittels Feile noch schön glatt gemacht werden können. 

Das Z10 ist mit roter Farbe beschmiert worden, um zu erkennen, wo es mit den Z14 hakelt. Das taugt aber alles nichts, und deshalb wird das Z10 anders beschafft (siehe vorangegangenes Bild).

Der Außenring Z38 modul 1 hier ist noch vom Vorgängermodell ohne Zahnkranz Z30. Die untenliegende Nut (von links unten nach rechts oben, vom Lagersitz unterbrochen) hat der Nachfolger behalten, damit man den Außenring auch fest montieren kann.