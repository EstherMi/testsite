---
layout: "image"
title: "Nut passt nicht ganz"
date: "2016-09-10T14:26:54"
picture: "bild5.jpg"
weight: "7"
konstrukteure: 
- "3D-Drucker"
fotografen:
- "Phil"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Phil"
license: "unknown"
legacy_id:
- details/44348
imported:
- "2019"
_4images_image_id: "44348"
_4images_cat_id: "3272"
_4images_user_id: "1624"
_4images_image_date: "2016-09-10T14:26:54"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44348 -->
Bei dem Nut-Teil wurde wegen im vorigem Bild beschriebenem Problem die Nut zu klein gedruckt. Es lässt sich nur sehr schwer auf den Zapfen drücken. Die Form selbst der Nut sieht aber ganz brauchbar aus, bis auf dass sie halt ein, zwei 10tel Millimeter zu klein ist. Werde auch hier etwas rumspielen und berichten.