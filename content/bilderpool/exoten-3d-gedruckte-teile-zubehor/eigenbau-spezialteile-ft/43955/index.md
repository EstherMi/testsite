---
layout: "image"
title: "Mikrocontroller im Power Blockgehäuse"
date: "2016-07-25T14:24:24"
picture: "2016-07-24_20.16.151.jpg"
weight: "34"
konstrukteure: 
- "Bernd Langer"
fotografen:
- "Bernd Langer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- details/43955
imported:
- "2019"
_4images_image_id: "43955"
_4images_cat_id: "3242"
_4images_user_id: "2496"
_4images_image_date: "2016-07-25T14:24:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43955 -->
Verbaut sind ein Arduino Board, C-Control pro Board,Relais Board
und Motor Board.
Wenn das Buch Fischertechnik für echte Kerle im September kommt
 bin ich bereit um die Modelle zu steuern.Es kommen noch verschiedene Sensoren.Zb Ultraschall, Licht, Ton  usw