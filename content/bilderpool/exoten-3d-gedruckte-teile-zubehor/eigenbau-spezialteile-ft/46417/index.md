---
layout: "image"
title: "Laser im Motorgehäuse"
date: "2017-09-27T18:24:07"
picture: "2017-09-26_22.23.49.jpg"
weight: "1"
konstrukteure: 
- "Bernd L"
fotografen:
- "Bernd L"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- details/46417
imported:
- "2019"
_4images_image_id: "46417"
_4images_cat_id: "3242"
_4images_user_id: "2496"
_4images_image_date: "2017-09-27T18:24:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46417 -->
In einem ausgedienten Solar Motor Gehäuse ist ein grüner 5mw Laser eingebaut.Er stammt aus einem Laserpointer der mit 3v gespeist wird.Um ihn bei 9v zubereiten ist ein Spannungsregler und ein Gleichrichter verbaut.Die Spiegel sind auf Nabenmutter geklebt.Jeder Spiegelmotor ist einzeln in der Drehzahl und Richtung mit meinen Mini Power opulenten Ansteuerung, natürlich geht es auch mit einem Interface .