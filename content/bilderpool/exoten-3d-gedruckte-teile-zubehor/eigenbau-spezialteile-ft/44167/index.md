---
layout: "image"
title: "C-control pro Baustein Innenleben"
date: "2016-08-03T15:14:05"
picture: "20160731_170113.jpg"
weight: "27"
konstrukteure: 
- "Bernd Langer"
fotografen:
- "Bernd Langer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- details/44167
imported:
- "2019"
_4images_image_id: "44167"
_4images_cat_id: "3242"
_4images_user_id: "2496"
_4images_image_date: "2016-08-03T15:14:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44167 -->
