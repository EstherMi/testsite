---
layout: "image"
title: "Laser im Motorgehäuse"
date: "2017-09-27T18:24:07"
picture: "2017-09-26_22.45.42.jpg"
weight: "6"
konstrukteure: 
- "Bernd L"
fotografen:
- "Bernd L"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- details/46422
imported:
- "2019"
_4images_image_id: "46422"
_4images_cat_id: "3242"
_4images_user_id: "2496"
_4images_image_date: "2017-09-27T18:24:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46422 -->
