---
layout: "image"
title: "Kompressor mit Druck und Vacuumanschluss"
date: "2017-04-11T20:34:31"
picture: "2017-04-08_07.55.01.jpg"
weight: "18"
konstrukteure: 
- "Bernd Langer"
fotografen:
- "Bernd Langer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- details/45733
imported:
- "2019"
_4images_image_id: "45733"
_4images_cat_id: "3242"
_4images_user_id: "2496"
_4images_image_date: "2017-04-11T20:34:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45733 -->
