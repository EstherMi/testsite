---
layout: "image"
title: "Adapter von 26pol robo Interface zu 20pol Modellanschluss"
date: "2016-07-03T17:52:26"
picture: "2016-07-03_10.01.541.jpg"
weight: "40"
konstrukteure: 
- "Bernd Langer"
fotografen:
- "Bernd Langer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- details/43837
imported:
- "2019"
_4images_image_id: "43837"
_4images_cat_id: "3242"
_4images_user_id: "2496"
_4images_image_date: "2016-07-03T17:52:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43837 -->
Das ist eine Platine damit ich das Robo Interface an den alten Teach in Roboter zb anschliessen kann.Der Ausgang hat extra noch den Masse anschluss damit ich die 8 Ausgänge nutzen kann.Die Ausgänge haben Leds .