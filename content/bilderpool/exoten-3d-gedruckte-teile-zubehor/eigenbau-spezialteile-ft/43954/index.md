---
layout: "image"
title: "Intervallschalter im Power Blockgehäuse"
date: "2016-07-25T14:24:24"
picture: "2016-07-07_15.42.361.jpg"
weight: "33"
konstrukteure: 
- "Bernd Langer"
fotografen:
- "Bernd Langer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- details/43954
imported:
- "2019"
_4images_image_id: "43954"
_4images_cat_id: "3242"
_4images_user_id: "2496"
_4images_image_date: "2016-07-25T14:24:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43954 -->
Die Impuls- und Pausenzeit ist getrennt einstellbar. Damit lassen sich  zb Karussells automatisch steuern.