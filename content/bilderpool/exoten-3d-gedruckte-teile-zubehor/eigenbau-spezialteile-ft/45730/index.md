---
layout: "image"
title: "4 Kanal Lauflicht im Batteriegehäuse"
date: "2017-04-11T20:34:31"
picture: "2017-04-10_16.48.37.jpg"
weight: "15"
konstrukteure: 
- "Bernd Langer"
fotografen:
- "Bernd Langer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- details/45730
imported:
- "2019"
_4images_image_id: "45730"
_4images_cat_id: "3242"
_4images_user_id: "2496"
_4images_image_date: "2017-04-11T20:34:31"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45730 -->
7 verschiedene Sequenzen + Automatik Ablauf aller 7 Sequenzen. 
Geschwindigkeitsregler und interne und externe Ansteuerung der black(alle Lampen aus) Funktion.