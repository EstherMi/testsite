---
layout: "image"
title: "Mini Powercontroller"
date: "2016-06-26T11:11:25"
picture: "20160624_2038071.jpg"
weight: "49"
konstrukteure: 
- "Bernd Langer"
fotografen:
- "Bernd Langer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- details/43780
imported:
- "2019"
_4images_image_id: "43780"
_4images_cat_id: "3242"
_4images_user_id: "2496"
_4images_image_date: "2016-06-26T11:11:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43780 -->
Ich habe aus der neuen Batteriebox einen Powercontroller
gebaut.Er hat ein verpolsicheren Eingang mit blauer BlinkLed und am Ausgang eine Duo Led die je nach Schalterstellung rot oder grün leuchtet.