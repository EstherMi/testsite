---
layout: "image"
title: "C-control pro Baustein"
date: "2016-07-25T14:24:24"
picture: "2016-07-24_19.52.101.jpg"
weight: "36"
konstrukteure: 
- "Bernd Langer"
fotografen:
- "Bernd Langer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- details/43957
imported:
- "2019"
_4images_image_id: "43957"
_4images_cat_id: "3242"
_4images_user_id: "2496"
_4images_image_date: "2016-07-25T14:24:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43957 -->
C-control Board im Powerblock eingebaut.Das programm kann über Bausteine oder als Text geschrieben werden.