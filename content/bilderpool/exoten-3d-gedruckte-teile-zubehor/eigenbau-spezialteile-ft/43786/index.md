---
layout: "image"
title: "IC Digital im Sortierkasten"
date: "2016-06-26T11:11:25"
picture: "20160314_2112231.jpg"
weight: "55"
konstrukteure: 
- "Bernd Langer"
fotografen:
- "Bernd Langer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- details/43786
imported:
- "2019"
_4images_image_id: "43786"
_4images_cat_id: "3242"
_4images_user_id: "2496"
_4images_image_date: "2016-06-26T11:11:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43786 -->
