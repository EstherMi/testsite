---
layout: "image"
title: "Leuchtstein für die Rundumleuchte"
date: "2016-06-26T11:11:24"
picture: "20160507_2137191.jpg"
weight: "48"
konstrukteure: 
- "Bernd Langer"
fotografen:
- "Bernd Langer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- details/43779
imported:
- "2019"
_4images_image_id: "43779"
_4images_cat_id: "3242"
_4images_user_id: "2496"
_4images_image_date: "2016-06-26T11:11:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43779 -->
Man kann den Widerstand erkennen.das kleine schwarze ist der Gleichrichter