---
layout: "image"
title: "Schleifring im Drehkranz"
date: "2016-06-26T11:11:25"
picture: "2016-06-07_22.12.231.jpg"
weight: "52"
konstrukteure: 
- "Bernd Langer"
fotografen:
- "Bernd Langer"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischerfreund"
license: "unknown"
legacy_id:
- details/43783
imported:
- "2019"
_4images_image_id: "43783"
_4images_cat_id: "3242"
_4images_user_id: "2496"
_4images_image_date: "2016-06-26T11:11:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43783 -->
Schleifring im Drehkranz.Links sind 6polige und rechts 12 polige.
Die Kontakteinheiten sind abnehmbar um sie ans Modell zu befestigen