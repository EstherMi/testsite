---
layout: "image"
title: "10"
date: "2010-06-25T18:21:28"
picture: "wasserstoffherstellen10.jpg"
weight: "10"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27581
imported:
- "2019"
_4images_image_id: "27581"
_4images_cat_id: "1983"
_4images_user_id: "1082"
_4images_image_date: "2010-06-25T18:21:28"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27581 -->
Fast gerade von oben.