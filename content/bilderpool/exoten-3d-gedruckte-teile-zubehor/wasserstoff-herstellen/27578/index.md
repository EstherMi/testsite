---
layout: "image"
title: "7"
date: "2010-06-25T18:21:28"
picture: "wasserstoffherstellen07.jpg"
weight: "7"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27578
imported:
- "2019"
_4images_image_id: "27578"
_4images_cat_id: "1983"
_4images_user_id: "1082"
_4images_image_date: "2010-06-25T18:21:28"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27578 -->
Die neue Anlage. Ein ganz durchsichtiges Glas, so sieht man besser die Wasserstoffmenge die bereits erzeugt wurde. Weil es auch größer ist, musste auch der Eimer größer sein.