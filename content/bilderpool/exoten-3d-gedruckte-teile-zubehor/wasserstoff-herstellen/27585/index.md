---
layout: "image"
title: "14"
date: "2010-06-25T18:21:28"
picture: "wasserstoffherstellen14.jpg"
weight: "14"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27585
imported:
- "2019"
_4images_image_id: "27585"
_4images_cat_id: "1983"
_4images_user_id: "1082"
_4images_image_date: "2010-06-25T18:21:28"
_4images_image_order: "14"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27585 -->
Hier wurde das Glas schon ein Stückchen gedreht und ein bisschen Wasserstoff ist schon entwichen. Durch das Schneckengetriebe kann man sehr langsam drehen, was gut ist um ein bisschen Wasserstoff auszulassen.