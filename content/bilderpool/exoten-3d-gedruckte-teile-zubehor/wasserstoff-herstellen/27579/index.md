---
layout: "image"
title: "8"
date: "2010-06-25T18:21:28"
picture: "wasserstoffherstellen08.jpg"
weight: "8"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27579
imported:
- "2019"
_4images_image_id: "27579"
_4images_cat_id: "1983"
_4images_user_id: "1082"
_4images_image_date: "2010-06-25T18:21:28"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27579 -->
Hier wieder die Anzeige ob der Trafo auch richtig gepolt ist, jetzt mit größeren LEDs.