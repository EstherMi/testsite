---
layout: "image"
title: "17"
date: "2010-06-25T18:21:28"
picture: "wasserstoffherstellen17.jpg"
weight: "17"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27588
imported:
- "2019"
_4images_image_id: "27588"
_4images_cat_id: "1983"
_4images_user_id: "1082"
_4images_image_date: "2010-06-25T18:21:28"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27588 -->
Durch das Salzwasser und den Strom rosten die Plättchen innerhalb von ca. 15 Minuten.