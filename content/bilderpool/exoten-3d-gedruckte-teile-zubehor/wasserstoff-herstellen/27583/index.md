---
layout: "image"
title: "12"
date: "2010-06-25T18:21:28"
picture: "wasserstoffherstellen12.jpg"
weight: "12"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27583
imported:
- "2019"
_4images_image_id: "27583"
_4images_cat_id: "1983"
_4images_user_id: "1082"
_4images_image_date: "2010-06-25T18:21:28"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27583 -->
Mit diesen beiden Tastern steuert man den Motor der das Glas mit dem Wasserstoff dreht.