---
layout: "image"
title: "11"
date: "2010-06-25T18:21:28"
picture: "wasserstoffherstellen11.jpg"
weight: "11"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27582
imported:
- "2019"
_4images_image_id: "27582"
_4images_cat_id: "1983"
_4images_user_id: "1082"
_4images_image_date: "2010-06-25T18:21:28"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27582 -->
An dem Schalter schaltet man die Wasserstoffproduktion ein und aus. 
Das Schloss ist ein Hauptschalter, genaueres dazu folgt noch.