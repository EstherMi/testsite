---
layout: "image"
title: "15"
date: "2010-06-25T18:21:28"
picture: "wasserstoffherstellen15.jpg"
weight: "15"
konstrukteure: 
- "Fischli"
fotografen:
- "Fischli"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fischli"
license: "unknown"
legacy_id:
- details/27586
imported:
- "2019"
_4images_image_id: "27586"
_4images_cat_id: "1983"
_4images_user_id: "1082"
_4images_image_date: "2010-06-25T18:21:28"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27586 -->
Noch weiter gedreht.