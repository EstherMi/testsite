---
layout: "image"
title: "Platine von unten"
date: "2016-07-28T16:54:28"
picture: "amafc3.jpg"
weight: "18"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/44040
imported:
- "2019"
_4images_image_id: "44040"
_4images_cat_id: "3260"
_4images_user_id: "2228"
_4images_image_date: "2016-07-28T16:54:28"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44040 -->
Das Projekt "Alternativer IR Empfänger" war gleichzeitig auch die Erprobung für den Arduino als alternativen Mikrocontroller. Die Herstellung der Platine ist hier beschrieben: https://ftcommunity.de/details.php?image_id=43341#col3