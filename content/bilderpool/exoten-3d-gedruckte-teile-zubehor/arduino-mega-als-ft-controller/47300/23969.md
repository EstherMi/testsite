---
layout: "comment"
hidden: true
title: "23969"
date: "2018-02-20T14:36:59"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Die Motoren machen 200 Schritte pro Umdrehung, also 1,8° Schrittwinkel. Mein Schrittmotortreiber unterstützt zwar Microstepping, der Plotter läuft jedoch im Vollschrittbetrieb.

Gruß,
David