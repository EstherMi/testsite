---
layout: "image"
title: "Neues Gehäuse"
date: "2017-03-23T11:41:09"
picture: "amafc1_2.jpg"
weight: "10"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/45630
imported:
- "2019"
_4images_image_id: "45630"
_4images_cat_id: "3260"
_4images_user_id: "2228"
_4images_image_date: "2017-03-23T11:41:09"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45630 -->
Das neue Gehäuse ist stabiler, besser in das ft System integriert und besitzt eine deutlichere Beschriftung