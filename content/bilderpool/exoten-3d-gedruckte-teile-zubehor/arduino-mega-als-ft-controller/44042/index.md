---
layout: "image"
title: "Benutzerfunktion"
date: "2016-07-28T16:54:28"
picture: "amafc5.jpg"
weight: "20"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/44042
imported:
- "2019"
_4images_image_id: "44042"
_4images_cat_id: "3260"
_4images_user_id: "2228"
_4images_image_date: "2016-07-28T16:54:28"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44042 -->
Wie vom TX gewohnt gibt es einen Schiebschalter als Powerschalter. Die darunter liegenden Pins dienen der Notabschaltung. Sie müssen verbunden sein, damit die Aktoren (z.B. die Motor ICs) Strom erhalten. Werden sie getrennt, erlischt die gelbe LED und die Motoren bleiben stehen. Vorteil dieser Steuerung ist, dass die Notabschaltung vom nicht multitaskingfähigen Arduino entkoppelt ist, das Programm läuft auch bei abgeschalteter Stromversorgung weiter. Eine Abschaltung bedeutet somit nur das Pausieren der Aktoren, das Programm wird nicht gestoppt.
Zwei LEDs können als Anzeige LEDs verwendet werden. Die grüne LED ist die LED 13, die auch einen Programmupload und Neustart des Arduinos anzeigt.
Darunter befindet sich der Reset Taster vom Arduino, der aufgrund des Shields nach oben verlegt werden musste.