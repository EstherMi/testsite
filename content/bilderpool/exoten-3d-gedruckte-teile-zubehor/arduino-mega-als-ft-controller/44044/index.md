---
layout: "image"
title: "Einsatz in der Praxis"
date: "2016-07-28T16:54:28"
picture: "amafc7.jpg"
weight: "22"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/44044
imported:
- "2019"
_4images_image_id: "44044"
_4images_cat_id: "3260"
_4images_user_id: "2228"
_4images_image_date: "2016-07-28T16:54:28"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44044 -->
Am ft FanClubTag 2016 habe ich den Controller dazu verwendet, einen 3-Achsroboter zu steuern: https://ftcommunity.de/details.php?image_id=43989