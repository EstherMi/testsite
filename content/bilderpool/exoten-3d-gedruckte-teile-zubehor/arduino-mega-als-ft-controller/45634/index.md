---
layout: "image"
title: "Gehäuse"
date: "2017-03-23T11:41:10"
picture: "amafc5_2.jpg"
weight: "14"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/45634
imported:
- "2019"
_4images_image_id: "45634"
_4images_cat_id: "3260"
_4images_user_id: "2228"
_4images_image_date: "2017-03-23T11:41:10"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45634 -->
Gehäuse aus dem 3D Drucker im Überblick: Durch Bohrungen kann der Controller sowohl auf ft Bauplatten als auch auf nicht ft Bauplatten befestigt werden.