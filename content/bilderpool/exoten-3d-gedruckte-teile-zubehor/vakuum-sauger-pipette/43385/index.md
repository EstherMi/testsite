---
layout: "image"
title: "'fliegende' Untertasse"
date: "2016-05-16T16:58:44"
picture: "vakuumsaugerpipette2.jpg"
weight: "2"
konstrukteure: 
- "lemkajen"
fotografen:
- "lemkajen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/43385
imported:
- "2019"
_4images_image_id: "43385"
_4images_cat_id: "3223"
_4images_user_id: "1359"
_4images_image_date: "2016-05-16T16:58:44"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43385 -->
Saugkraft reicht, um eine Untertasse sicher(!) zu halten - über längere Zeit - damit ist die maximale Last noch nicht erreicht