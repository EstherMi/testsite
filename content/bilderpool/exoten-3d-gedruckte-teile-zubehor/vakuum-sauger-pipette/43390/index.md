---
layout: "image"
title: "geteilter Schlauchadapter für Magnetventile"
date: "2016-05-16T16:58:44"
picture: "vakuumsaugerpipette7.jpg"
weight: "7"
konstrukteure: 
- "lemkajen"
fotografen:
- "lemkajen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/43390
imported:
- "2019"
_4images_image_id: "43390"
_4images_cat_id: "3223"
_4images_user_id: "1359"
_4images_image_date: "2016-05-16T16:58:44"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43390 -->
benötigt wird hier nur das vordere (dünne Stück) - der Hintere Teil kann evtl noch für andere Zwecke verwendet werden