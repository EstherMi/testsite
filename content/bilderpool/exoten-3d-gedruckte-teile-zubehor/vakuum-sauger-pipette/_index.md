---
layout: "overview"
title: "Vakuum-Sauger / Pipette"
date: 2019-12-17T18:06:25+01:00
legacy_id:
- categories/3223
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3223 --> 
Aus ausgemusterten Teilen eines Agfa-Speicherfolienlesers für belichtete Röntgen-Speicherfolien konnnte ich ein paar Sauger bekommen.
diese habe ich durch leicht modifizierte Magnet-Ventil-Schlauchanschlüsse auf unsere Fischertechnik-Pneumatik-Schläuche adaptiert