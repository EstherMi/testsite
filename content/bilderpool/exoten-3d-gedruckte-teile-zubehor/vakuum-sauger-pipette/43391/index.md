---
layout: "image"
title: "Der Sauger von vorne"
date: "2016-05-16T16:58:44"
picture: "vakuumsaugerpipette8.jpg"
weight: "8"
konstrukteure: 
- "lemkajen"
fotografen:
- "lemkajen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/43391
imported:
- "2019"
_4images_image_id: "43391"
_4images_cat_id: "3223"
_4images_user_id: "1359"
_4images_image_date: "2016-05-16T16:58:44"
_4images_image_order: "8"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43391 -->
gut erkennbar die dünne Gummilippe - die Speicherfolien , die 3 Stück von diesen Saugern im Gerät angehoben habenm waren teilw. 50x40cm groß (ca) und wogen vielleicht etwa 20-30g.