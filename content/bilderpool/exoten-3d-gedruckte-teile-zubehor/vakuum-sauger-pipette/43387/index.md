---
layout: "image"
title: "Sauger und Geber - bzw. Vakuum-Zylinder"
date: "2016-05-16T16:58:44"
picture: "vakuumsaugerpipette4.jpg"
weight: "4"
konstrukteure: 
- "lemkajen"
fotografen:
- "lemkajen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/43387
imported:
- "2019"
_4images_image_id: "43387"
_4images_cat_id: "3223"
_4images_user_id: "1359"
_4images_image_date: "2016-05-16T16:58:44"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43387 -->
Sauger im teilw. vakuumierten Zustand