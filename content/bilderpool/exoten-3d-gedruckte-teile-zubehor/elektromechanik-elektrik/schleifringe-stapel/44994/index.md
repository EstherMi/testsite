---
layout: "image"
title: "Einbau im Clasic-Kran"
date: "2016-12-31T17:34:58"
picture: "_MG_0077.jpg"
weight: "4"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/44994
imported:
- "2019"
_4images_image_id: "44994"
_4images_cat_id: "3345"
_4images_user_id: "2638"
_4images_image_date: "2016-12-31T17:34:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44994 -->
HIer der Einbau im Kran. Das "Monstrum" hält auch mechanisch den Kopf des Krans.