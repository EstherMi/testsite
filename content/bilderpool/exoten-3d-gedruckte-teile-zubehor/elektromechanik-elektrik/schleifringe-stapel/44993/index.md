---
layout: "image"
title: "3 Schleifringe, Achse und Plattkontakt"
date: "2016-12-31T17:34:58"
picture: "IMG_20161031_123512.jpg"
weight: "3"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/44993
imported:
- "2019"
_4images_image_id: "44993"
_4images_cat_id: "3345"
_4images_user_id: "2638"
_4images_image_date: "2016-12-31T17:34:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44993 -->
Das Kabel der Platte ist mit einer Buchse versehen, um später besser wiederverwendet zu werden.