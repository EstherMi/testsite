---
layout: "image"
title: "Einbau im Clasic-Kran"
date: "2016-12-31T17:34:58"
picture: "Schleifkontakte01.jpg"
weight: "5"
konstrukteure: 
- "Jan und Tilo Rust"
fotografen:
- "Jan und Tilo Rust"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TiloRust"
license: "unknown"
legacy_id:
- details/44995
imported:
- "2019"
_4images_image_id: "44995"
_4images_cat_id: "3345"
_4images_user_id: "2638"
_4images_image_date: "2016-12-31T17:34:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44995 -->
Perspektive von unten.
Die Stromabnahme mit den Federbeinen funktioniert klasse.