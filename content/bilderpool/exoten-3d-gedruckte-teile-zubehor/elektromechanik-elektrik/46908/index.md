---
layout: "image"
title: "Chain module ball track project."
date: "2017-11-08T17:20:12"
picture: "P1017699a_800.jpg"
weight: "1"
konstrukteure: 
- "fotoopa"
fotografen:
- "fotoopa"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fotoopa"
license: "unknown"
legacy_id:
- details/46908
imported:
- "2019"
_4images_image_id: "46908"
_4images_cat_id: "467"
_4images_user_id: "2787"
_4images_image_date: "2017-11-08T17:20:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46908 -->
Chain module ball track. This is a new module that can be added to my project. The steel balls are first pulled upward. Once over the top there are 3 outputs possible. Level 1 and Level 2 each contain a servo. If the servo is on the left pos, then the ball can continue until the next level. If level1 and level 2 are not used then level 3 is the output. In this way, three different pathways are created. A hall detector is located at the bottom. This detector sends a signal to the controller so no servo is allowed to move if a bullet is on an output. The total height is 615mm.