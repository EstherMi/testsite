---
layout: "image"
title: "Motor mit Getriebe und aufgeschobener Hülse"
date: "2005-12-01T18:42:25"
picture: "MotorGetriebeHlse.jpg"
weight: "7"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/5446
imported:
- "2019"
_4images_image_id: "5446"
_4images_cat_id: "599"
_4images_user_id: "184"
_4images_image_date: "2005-12-01T18:42:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5446 -->
