---
layout: "image"
title: "Motor ohne Getriebe"
date: "2005-12-01T18:39:52"
picture: "Motor.jpg"
weight: "5"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/5443
imported:
- "2019"
_4images_image_id: "5443"
_4images_cat_id: "599"
_4images_user_id: "184"
_4images_image_date: "2005-12-01T18:39:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5443 -->
