---
layout: "image"
title: "Stromabzapfung"
date: "2007-06-24T16:06:47"
picture: "HRL60.jpg"
weight: "3"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/10909
imported:
- "2019"
_4images_image_id: "10909"
_4images_cat_id: "987"
_4images_user_id: "456"
_4images_image_date: "2007-06-24T16:06:47"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10909 -->
Hier ist die Sromabzapfung an der Messingachse. Sie ist einfach drumgewickelt und festgeknüllt, aber es hebt sehr gut, hat immer Kontakt und so gut wie keinen Widerstand.