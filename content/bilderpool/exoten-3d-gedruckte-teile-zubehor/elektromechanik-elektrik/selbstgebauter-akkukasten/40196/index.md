---
layout: "image"
title: "Einsatz für Kasterr"
date: "2015-01-07T19:06:05"
picture: "akkuhalter2.jpg"
weight: "2"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/40196
imported:
- "2019"
_4images_image_id: "40196"
_4images_cat_id: "1444"
_4images_user_id: "2321"
_4images_image_date: "2015-01-07T19:06:05"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40196 -->
Die Federkontakte habe ich aus einem gekauften Batteriehalter ausgebaut und und auf Plastikplatten geklebt. Die Platten habe ich mit Draht verbunden, damit sie durch die Federkräfte in der Mitte nicht so stark auseinandergeboden werden (im fertigen Ausbau sind sie noch mit Klebeband isoliert). Das Einsetzen der Akkus ist etwas fummelig.