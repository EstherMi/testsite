---
layout: "image"
title: "Akkukasten"
date: "2008-10-05T17:50:49"
picture: "selbstgebauterakkukasten1.jpg"
weight: "3"
konstrukteure: 
- "Martin Giger"
fotografen:
- "Martin Giger"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ma-gi-er"
license: "unknown"
legacy_id:
- details/15820
imported:
- "2019"
_4images_image_id: "15820"
_4images_cat_id: "1444"
_4images_user_id: "445"
_4images_image_date: "2008-10-05T17:50:49"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15820 -->
Die Metallteile sind aus ehemaligen MickyMaus Magazin Extras. Die beiden Seitölichen Teile sind herausnehmbar (nächste Bilder). In den "Akkupack" passen acht Akkus.