---
layout: "image"
title: "Klebevorrichtung"
date: "2017-01-15T12:58:29"
picture: "IMG_1904.jpg"
weight: "4"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: ["Schleifring", "mehrpolig"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/45039
imported:
- "2019"
_4images_image_id: "45039"
_4images_cat_id: "3354"
_4images_user_id: "1359"
_4images_image_date: "2017-01-15T12:58:29"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45039 -->
ich habe mal ein paar günstige Schleifringe bei EBay erstanden, zur Montage innerhalb des großen Zahnrad Drehkranzes.