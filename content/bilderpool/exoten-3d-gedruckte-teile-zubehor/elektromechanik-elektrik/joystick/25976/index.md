---
layout: "image"
title: "Joystik1"
date: "2009-12-26T19:06:06"
picture: "joystik1.jpg"
weight: "1"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/25976
imported:
- "2019"
_4images_image_id: "25976"
_4images_cat_id: "1828"
_4images_user_id: "182"
_4images_image_date: "2009-12-26T19:06:06"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25976 -->
Ich habe für das Joystik (JX-1D15F1500 , ca, 25 Euro/st ) von APEM ein Gehäuse gemacht.
Das Joystik ist echt Klasse man kann damit 2 Motoren gleichzeitig steuern.
http://www.apemswitches.be/Low-Profile-Switch-Joysticks-v1-d-117.html