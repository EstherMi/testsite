---
layout: "image"
title: "Joystik2"
date: "2009-12-26T19:06:07"
picture: "joystik2.jpg"
weight: "2"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/25977
imported:
- "2019"
_4images_image_id: "25977"
_4images_cat_id: "1828"
_4images_user_id: "182"
_4images_image_date: "2009-12-26T19:06:07"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25977 -->
Hier die Unterseite wo man auch die ft Nuten in der Grundplatte sieht.