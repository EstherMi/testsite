---
layout: "image"
title: "weiße Lampe"
date: "2009-01-28T14:43:28"
picture: "schaltplanteile07.jpg"
weight: "7"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "sebastian g."
license: "unknown"
legacy_id:
- details/17176
imported:
- "2019"
_4images_image_id: "17176"
_4images_cat_id: "1541"
_4images_user_id: "904"
_4images_image_date: "2009-01-28T14:43:28"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17176 -->
Diese Bilder könt ihr nehmen wenn ihr einen Fischertechnik-Schaltplan erstellen wollt.