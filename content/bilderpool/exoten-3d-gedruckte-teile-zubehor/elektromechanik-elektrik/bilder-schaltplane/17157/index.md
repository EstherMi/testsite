---
layout: "image"
title: "Interface"
date: "2009-01-24T16:31:28"
picture: "interfacebild1.jpg"
weight: "14"
konstrukteure: 
- "sebastian g."
fotografen:
- "sebastian g."
keywords: ["iNTERFACE", "iNTERFACEBILD"]
uploadBy: "sebastian g."
license: "unknown"
legacy_id:
- details/17157
imported:
- "2019"
_4images_image_id: "17157"
_4images_cat_id: "1541"
_4images_user_id: "904"
_4images_image_date: "2009-01-24T16:31:28"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17157 -->
Dieses Interface könnt ihr benutzen wenn ihr einen Schaltplan zeichnen wollt!