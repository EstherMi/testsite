---
layout: "image"
title: "Power-Motor von oben"
date: "2006-03-27T19:52:07"
picture: "Fischertechnik-Bilder_017.jpg"
weight: "3"
konstrukteure: 
- "Jakob Wolf"
fotografen:
- "Jakob Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FFBBII"
license: "unknown"
legacy_id:
- details/5976
imported:
- "2019"
_4images_image_id: "5976"
_4images_cat_id: "518"
_4images_user_id: "420"
_4images_image_date: "2006-03-27T19:52:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5976 -->
