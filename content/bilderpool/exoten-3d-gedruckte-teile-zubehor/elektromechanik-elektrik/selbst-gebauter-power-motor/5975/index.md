---
layout: "image"
title: "Power-Motor"
date: "2006-03-27T19:52:07"
picture: "Fischertechnik-Bilder_018.jpg"
weight: "2"
konstrukteure: 
- "Jakob Wolf"
fotografen:
- "Jakob Wolf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FFBBII"
license: "unknown"
legacy_id:
- details/5975
imported:
- "2019"
_4images_image_id: "5975"
_4images_cat_id: "518"
_4images_user_id: "420"
_4images_image_date: "2006-03-27T19:52:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5975 -->
