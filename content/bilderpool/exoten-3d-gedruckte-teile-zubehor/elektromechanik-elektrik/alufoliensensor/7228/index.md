---
layout: "image"
title: "Drehkranz mit Kästchen"
date: "2006-10-24T16:22:05"
picture: "1_002.jpg"
weight: "2"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/7228
imported:
- "2019"
_4images_image_id: "7228"
_4images_cat_id: "692"
_4images_user_id: "453"
_4images_image_date: "2006-10-24T16:22:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7228 -->
