---
layout: "image"
title: "Drehkranz mit Kontakten"
date: "2006-10-24T16:22:05"
picture: "1_001.jpg"
weight: "1"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/7227
imported:
- "2019"
_4images_image_id: "7227"
_4images_cat_id: "692"
_4images_user_id: "453"
_4images_image_date: "2006-10-24T16:22:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7227 -->
