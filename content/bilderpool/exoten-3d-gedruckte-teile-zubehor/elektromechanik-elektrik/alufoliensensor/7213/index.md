---
layout: "image"
title: "Drehkranz mit Alufolie Streifen"
date: "2006-10-23T17:47:14"
picture: "Alu-folien_Sensor_002.jpg"
weight: "4"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/7213
imported:
- "2019"
_4images_image_id: "7213"
_4images_cat_id: "692"
_4images_user_id: "453"
_4images_image_date: "2006-10-23T17:47:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7213 -->
Die Alufolie Streifen werden verbunden wenn ein Kästchen die Streifen berührt,
dann bekommt das Interface bescheid, und weiß das dort ein Kästchen ist.