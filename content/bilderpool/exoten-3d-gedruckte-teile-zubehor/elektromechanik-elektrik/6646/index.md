---
layout: "image"
title: "Schrittmotorbefestigung"
date: "2006-07-22T13:48:18"
picture: "IMG_0398.jpg"
weight: "29"
konstrukteure: 
- "Michael Orlik//Sannchen90"
fotografen:
- "Michael Orlik//Sannchen90"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- details/6646
imported:
- "2019"
_4images_image_id: "6646"
_4images_cat_id: "467"
_4images_user_id: "6"
_4images_image_date: "2006-07-22T13:48:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6646 -->
So habe ich das Problem gelöst. Es handelt sich um eine handelsübliche Aluwelle (Baumarktware). Der Motor ist von Conrad genau wie die Kupplung.

Im Boden der Welle sind 3mm Gewinde um den Motor zu befestigen, am anderen Ende sind 4mm Gewinde, so daß man den BSt 15 mit Loch benutzen kann. Ein wenig spannt die Konstruktion, da der Abstand nicht so ganz FT Kompatibel ist, aber es geht.