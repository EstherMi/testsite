---
layout: "image"
title: "roter Leuchtstein"
date: "2008-09-27T16:22:35"
picture: "dsc01875_resize.jpg"
weight: "16"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich (schnaggels)"
keywords: ["leuchtstein", "rot"]
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- details/15642
imported:
- "2019"
_4images_image_id: "15642"
_4images_cat_id: "467"
_4images_user_id: "120"
_4images_image_date: "2008-09-27T16:22:35"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15642 -->
erster Versuch den so zu machen, wie wir Fans uns den Leuchtstein gewünscht hätten :)

Die Buchsen sind aus der Bestellung von Ludger, passen immer wieder!