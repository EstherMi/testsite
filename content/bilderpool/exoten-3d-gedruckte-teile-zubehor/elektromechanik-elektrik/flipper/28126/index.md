---
layout: "image"
title: "Kugelmagazin ohne Abdeckung"
date: "2010-09-14T20:01:16"
picture: "flipper15.jpg"
weight: "15"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- details/28126
imported:
- "2019"
_4images_image_id: "28126"
_4images_cat_id: "2043"
_4images_user_id: "791"
_4images_image_date: "2010-09-14T20:01:16"
_4images_image_order: "15"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28126 -->
Die Kugeln werden durch einen Pneumatikzylinder auf die Abschussrampe befördert.