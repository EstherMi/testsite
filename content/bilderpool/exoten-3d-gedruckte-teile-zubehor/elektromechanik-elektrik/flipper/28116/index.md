---
layout: "image"
title: "Anleitung"
date: "2010-09-14T20:01:13"
picture: "flipper05.jpg"
weight: "5"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- details/28116
imported:
- "2019"
_4images_image_id: "28116"
_4images_cat_id: "2043"
_4images_user_id: "791"
_4images_image_date: "2010-09-14T20:01:13"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28116 -->
Das ist die Anleitung wie man sie bei einem echten Flipper auch vorfindet. Ich habe lang überlegt ob ich sie in Deutsch oder in Englisch schreiben soll, habe mich letztendlich aber für die englische Version entschieden da die echten Flipper ja auch in Amerika ,,geboren´´ werden.