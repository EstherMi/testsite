---
layout: "image"
title: "Abschussrampe"
date: "2010-09-14T20:01:16"
picture: "flipper13.jpg"
weight: "13"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- details/28124
imported:
- "2019"
_4images_image_id: "28124"
_4images_cat_id: "2043"
_4images_user_id: "791"
_4images_image_date: "2010-09-14T20:01:16"
_4images_image_order: "13"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28124 -->
Links unten werden die Kugeln auf die Abschussrampe gelegt.