---
layout: "image"
title: "Slingshot"
date: "2010-09-14T20:01:16"
picture: "flipper10.jpg"
weight: "10"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- details/28121
imported:
- "2019"
_4images_image_id: "28121"
_4images_cat_id: "2043"
_4images_user_id: "791"
_4images_image_date: "2010-09-14T20:01:16"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28121 -->
Man erkennt hier den Slingshot. Kommt der Ball an das Gummi wird der Kontakt geschlossen und der Arm schlägt aus.