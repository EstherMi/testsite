---
layout: "image"
title: "Ansicht von unten"
date: "2010-09-14T20:01:16"
picture: "flipper17.jpg"
weight: "17"
konstrukteure: 
- "Sebastian"
fotografen:
- "Sebastian"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sebastian"
license: "unknown"
legacy_id:
- details/28128
imported:
- "2019"
_4images_image_id: "28128"
_4images_cat_id: "2043"
_4images_user_id: "791"
_4images_image_date: "2010-09-14T20:01:16"
_4images_image_order: "17"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28128 -->
Man erkennt links oben das Radio, rechts oben die Magnetventile, rechts unten die Steuerung, den Kompressor, die Motoren für die Slingshots und die Mechanik der Flipperfinger.