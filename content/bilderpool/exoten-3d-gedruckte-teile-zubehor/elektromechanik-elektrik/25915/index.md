---
layout: "image"
title: "Akkupack"
date: "2009-12-08T18:41:00"
picture: "akkupack1.jpg"
weight: "13"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guest"
license: "unknown"
legacy_id:
- details/25915
imported:
- "2019"
_4images_image_id: "25915"
_4images_cat_id: "467"
_4images_user_id: "-1"
_4images_image_date: "2009-12-08T18:41:00"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25915 -->
9,6V und 2700 mAh

hat 60*30mm Grundfläche