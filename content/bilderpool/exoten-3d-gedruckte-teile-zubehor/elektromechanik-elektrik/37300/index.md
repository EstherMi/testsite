---
layout: "image"
title: "Aderendhülsen als Steckerersatz 2"
date: "2013-09-06T01:05:26"
picture: "Aderendhlsen_als_Steckerersatz_2.jpg"
weight: "10"
konstrukteure: 
- "tim4441"
fotografen:
- "tim4441"
keywords: ["Aderendhülsen", "Aderendhülse", "Steckerersatz"]
uploadBy: "tim4441"
license: "unknown"
legacy_id:
- details/37300
imported:
- "2019"
_4images_image_id: "37300"
_4images_cat_id: "467"
_4images_user_id: "1121"
_4images_image_date: "2013-09-06T01:05:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=37300 -->
Die Endhülsen wird dann wie eine 1,0er Hülse gequetscht.
Links ist die ungequetschte Aderendhülse, rechts die mit 1,0 gequetschte Aderendhülse.