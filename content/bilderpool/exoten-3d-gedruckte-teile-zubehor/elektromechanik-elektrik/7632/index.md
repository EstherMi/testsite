---
layout: "image"
title: "Power-Adapter 2"
date: "2006-11-26T23:08:57"
picture: "dsc00958_resize.jpg"
weight: "25"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich (schnaggels)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- details/7632
imported:
- "2019"
_4images_image_id: "7632"
_4images_cat_id: "467"
_4images_user_id: "120"
_4images_image_date: "2006-11-26T23:08:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7632 -->
