---
layout: "comment"
hidden: true
title: "10441"
date: "2009-12-30T14:23:12"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
Adapter für Netzteil auf Stecker (falls Buchse am Interface z.B. verbaut) und Akku auf Netzteil-Buchse (z.B. für Power Controller)

Kosten: wenige Cent
Nutzen: unbezahlbar :)