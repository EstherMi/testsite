---
layout: "image"
title: "Innenansicht"
date: "2005-02-19T16:11:16"
picture: "Innen.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chevyfahrer"
license: "unknown"
legacy_id:
- details/3646
imported:
- "2019"
_4images_image_id: "3646"
_4images_cat_id: "114"
_4images_user_id: "103"
_4images_image_date: "2005-02-19T16:11:16"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=3646 -->
Die Taster sind aus dem aktuellen Conrad-Katalog