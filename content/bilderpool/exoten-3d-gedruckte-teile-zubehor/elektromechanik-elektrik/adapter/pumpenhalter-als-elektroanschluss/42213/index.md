---
layout: "image"
title: "Die Vorderseite"
date: "2015-11-06T14:46:04"
picture: "pic1.jpg"
weight: "1"
konstrukteure: 
- "H.A.R.R.Y."
fotografen:
- "H.A.R.R.Y."
keywords: ["Hohlstecker", "Hohlsteckbuchse", "Stromanschluß", "Pumpenhalter", "Elektroanschluß", "130770"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/42213
imported:
- "2019"
_4images_image_id: "42213"
_4images_cat_id: "3149"
_4images_user_id: "1557"
_4images_image_date: "2015-11-06T14:46:04"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42213 -->
Zwei zusätzliche Bohrlöcher sind erforderlich. Die ausgefransten Kanten werden von zwei Unterlegscheiben überdeckt.

Hinter dem zentralen Loch für die Wasserpumpe sitzt dann der Steckanschluß.