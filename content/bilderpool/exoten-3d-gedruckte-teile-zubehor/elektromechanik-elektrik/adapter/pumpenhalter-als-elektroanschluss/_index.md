---
layout: "overview"
title: "Pumpenhalter als Elektroanschluß"
date: 2019-12-17T18:03:32+01:00
legacy_id:
- categories/3149
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3149 --> 
Wie man aus einem Pumpenhalter (130770) einen preisgünstigen und stabilen Niedervolt-Anschluß für seine Modelle machen kann.