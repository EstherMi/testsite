---
layout: "image"
title: "Anschlußadapter"
date: "2011-02-04T23:13:41"
picture: "anschlussadapter1.jpg"
weight: "1"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/29863
imported:
- "2019"
_4images_image_id: "29863"
_4images_cat_id: "2200"
_4images_user_id: "182"
_4images_image_date: "2011-02-04T23:13:41"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29863 -->
Ich habe mir hier einen Anschlußadapter vom runden Hohlsteker auf die 2,6mm Stecker gemacht.
Beim Interface sowie beim TX steht man immer wieder vor dem Problem für zusätzliche Sensoren oder sonstiges Spannung abzugreifen.
Hier läßt sich nun das Interface über die 2,6mm Stecker anschließen und an der anderen Seite zusätzlich ein Kabel einstecken.
Natürlich kann Ihn auch bei anderen Modellen einsetzen um diese mit dem ft Netzteil zu betreiben.

Er besteht aus folgenden Teilen:

36223 Feder-Gelenkstein
38216 Leuchtstein schwarz
HEBL 21 Hohlstecker-Buchse