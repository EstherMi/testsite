---
layout: "image"
title: "Um2x4_07.JPG"
date: "2006-08-15T15:30:45"
picture: "Um2x4_07.JPG"
weight: "2"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/6679
imported:
- "2019"
_4images_image_id: "6679"
_4images_cat_id: "600"
_4images_user_id: "4"
_4images_image_date: "2006-08-15T15:30:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6679 -->
Das Ganze als Explosions-Ansicht.

Der BS5 in Bildmitte ist längs verschiebbar, damit man den Kontaktdruck einstellen kann. Quer durch den BS15 mit den Federkontakten ist ein Loch (1 mm) gebohrt, um die Rastachse mit diesem Stein per Drahtstück zu verstiften.

Der BS15 rechts außen könnte auch entfallen, aber dann biegt sich das Achsende mit der Kurbel ziemlich deutlich.