---
layout: "image"
title: "Um2x4_03.JPG"
date: "2006-08-15T15:26:10"
picture: "Um2x4_03.JPG"
weight: "1"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/6678
imported:
- "2019"
_4images_image_id: "6678"
_4images_cat_id: "600"
_4images_user_id: "4"
_4images_image_date: "2006-08-15T15:26:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6678 -->
Dieser Umschalter soll einen Joystick (Steuerung für 2 Motoren) zwischen 4 Gruppen von je 2 Motoren umschalten. Das ginge auch mit dem ft-Drehschalter, aber der ist mir etwas zu sperrig.

Die äußeren BS15-Loch erhalten 4 Löcher (3 mm) für die Steckhülsen (Erfindung von Ludger). Die Hülsen werden auf 15 mm abgelängt und im Gerät dann von innen nach außen eingesteckt. Der mittlere BS15 erhält 2 Bohrungen (4 mm, möglichst weit außen, d.h. 0,5 mm Wandstärke des BS15 sollte auch weggenommen werden) diagonal gegenüber für die ft-Federkontakte. Die Federkontakte werden durch ft-Stecker fixiert. 

Durch Drehen der Kurbel werden die Federkontakte der Reihe nach über die Kontaktsätze geführt und rasten sogar ein.