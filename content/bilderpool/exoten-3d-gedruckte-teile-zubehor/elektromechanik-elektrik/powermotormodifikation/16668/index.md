---
layout: "image"
title: "Powermotor mit Geschwindigkeitsmesser"
date: "2008-12-19T14:14:50"
picture: "motor1.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "kehrblech"
license: "unknown"
legacy_id:
- details/16668
imported:
- "2019"
_4images_image_id: "16668"
_4images_cat_id: "1509"
_4images_user_id: "521"
_4images_image_date: "2008-12-19T14:14:50"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16668 -->
Bei meinem Kugelroboter muss ich ziemlich genau wissen, wie schnell sich die Motoren drehen. Das normale Impulsrad war mir zu ungenau, außerdem funktioniert das bei mir beim Powermotor 8:1 nicht wirklich gut(der Taster scheint nicht ganz schnell genug zu sein). Für meine alte Lösung mit der Lochscheibe und eine Lichtschranke ist nicht genug Platz. Die jetzige Lösung verbraucht gar keinen Platz - sie ist im Getriebe des Motors eingebaut.