---
layout: "image"
title: "Tamiya-Adapter"
date: "2008-01-27T17:30:27"
picture: "tamiyaadapter1.jpg"
weight: "19"
konstrukteure: 
- "Frank Jakob"
fotografen:
- "Frank Jakob"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Knarf Bokaj"
license: "unknown"
legacy_id:
- details/13448
imported:
- "2019"
_4images_image_id: "13448"
_4images_cat_id: "467"
_4images_user_id: "729"
_4images_image_date: "2008-01-27T17:30:27"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13448 -->
Tamiya-Adapter. 1.75 ? bei Conrad