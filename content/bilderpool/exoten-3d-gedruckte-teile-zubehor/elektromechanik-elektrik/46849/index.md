---
layout: "image"
title: "4 way selector module"
date: "2017-10-28T17:04:33"
picture: "PA027325_800.jpg"
weight: "5"
konstrukteure: 
- "fotoopa"
fotografen:
- "fotoopa"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fotoopa"
license: "unknown"
legacy_id:
- details/46849
imported:
- "2019"
_4images_image_id: "46849"
_4images_cat_id: "467"
_4images_user_id: "2787"
_4images_image_date: "2017-10-28T17:04:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46849 -->
4 way selector test module. 
Via the XS motor one of the 4 possible outputs can be selected. An encoder is used to set the optimal position. The min and max position is determined by 2 Neodymium magnets and detectors. Above each way there is a color neo led. Green indicates the selected output, the others are red. This picture does not show colors due to overexposure. At the bottom of the 4 neo leds there are also 4 reflection detectors ( not visible on this photo ). These detectors count the number of steel bullets that pass. The program can control everything. All signals are connected to the external SPI I / O boxes and then go to the (FPGA) Controller. This is a test setup. Later everything is finally taken into use. This module allows you to change the trajectory of the ball path. I am currently testing a lot of other modules such as an 2-way selector with servo, a stop with servo, optical detector etc. More will follow later.

I'm currently working on the project for about 1 year. It is for my grandchildren. They are still too young to build this, but they like to playing with this! Especially interactive play is important, children must be able to give their own input, change the traject, change the number off balls etc. During the test setup, I look how it can be made interactive. Grandchildren are 2.5 and 4.5 years old.