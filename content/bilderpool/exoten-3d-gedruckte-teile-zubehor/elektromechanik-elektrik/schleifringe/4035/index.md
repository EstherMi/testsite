---
layout: "image"
title: "SR_E13.JPG"
date: "2005-04-20T13:36:07"
picture: "SR_E13.jpg"
weight: "15"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4035
imported:
- "2019"
_4images_image_id: "4035"
_4images_cat_id: "347"
_4images_user_id: "4"
_4images_image_date: "2005-04-20T13:36:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4035 -->
Die Unterseite. Ziel war es, die Eckpositionen auf dem Drehkranz frei zu halten. Das hat geklappt, aber außer den Eckpositionen ist auch nicht mehr viel übrig.