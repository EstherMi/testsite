---
layout: "image"
title: "SR_F01.JPG"
date: "2005-04-20T13:36:07"
picture: "SR_F01.jpg"
weight: "16"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Schleifring"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4036
imported:
- "2019"
_4images_image_id: "4036"
_4images_cat_id: "347"
_4images_user_id: "4"
_4images_image_date: "2005-04-20T13:36:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4036 -->
Modell F stellt mit 12 Ringen den "Griff nach oben" dar. Die Stecker sind auf zwei Blöcke verteilt, damit sie nicht so sperrig werden. Die Trommel wird durch den Konus ++im Inneren++ des Drehkranzes geklemmt, damit trägt der Schleifring auf der schwarzen Seite des Drehkranzes nur 2mm auf (das war nötig für den "Inferno", siehe http://www.ftcommunity.de/categories.php?cat_id=346 ). Die 4mm Innendurchgang sind mittlerweile als Standard etabliert.