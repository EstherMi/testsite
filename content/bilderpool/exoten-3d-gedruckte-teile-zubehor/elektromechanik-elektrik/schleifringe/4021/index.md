---
layout: "image"
title: "SR_005.JPG"
date: "2005-04-19T20:23:04"
picture: "SR_005.jpg"
weight: "26"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Schleifring"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4021
imported:
- "2019"
_4images_image_id: "4021"
_4images_cat_id: "347"
_4images_user_id: "4"
_4images_image_date: "2005-04-19T20:23:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4021 -->
Wenn gehobene Ansprüche gestellt werden, macht man die Anordnung hinreichend groß und sucht ein Rohr, das ganz innen hineinpasst und noch Platz für eine ft-Achse lässt, mit der man Sachen durch den Schleifring hindurch antreiben kann.

Das ist hier das Messingrohr in der Mitte. Es wird vorerst nur durch die Drähte in Position gehalten, deshalb werden diese schön reihum drapiert. Später kommt noch Heißkleber drauf.




(Der BS7,5 sollte dafür sorgen, dass die Kamera etwas zum fokussieren hat, aber sie wollte einfach nicht)