---
layout: "image"
title: "SR_A02.JPG"
date: "2005-04-19T20:23:04"
picture: "SR_A02.jpg"
weight: "30"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4025
imported:
- "2019"
_4images_image_id: "4025"
_4images_cat_id: "347"
_4images_user_id: "4"
_4images_image_date: "2005-04-19T20:23:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4025 -->
Es zeigt sich, dass ab 6 Ringen die Steckerleiste auf eine Bauplatte 15x45 (anstelle 15x30)  muss, was schon wieder unhandlich wird -- zumindest bei meinem ft-Baustil.