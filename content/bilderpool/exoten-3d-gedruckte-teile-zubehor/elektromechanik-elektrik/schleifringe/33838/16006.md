---
layout: "comment"
hidden: true
title: "16006"
date: "2012-01-06T18:12:02"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Schmalle Messingringe mit einer Stärke von 1mm (statt 0,5mm) wäre auch möglich.

Grüss,

Peter
Poederoyen NL