---
layout: "image"
title: "SR_C02.JPG"
date: "2005-04-19T20:23:04"
picture: "SR_C02.jpg"
weight: "35"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4030
imported:
- "2019"
_4images_image_id: "4030"
_4images_cat_id: "347"
_4images_user_id: "4"
_4images_image_date: "2005-04-19T20:23:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4030 -->
Zur Montage wird der Gelenkstein durch den Drehkranz gefädelt.