---
layout: "image"
title: "SR_G03.JPG"
date: "2005-04-20T13:36:08"
picture: "SR_G03.jpg"
weight: "22"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4042
imported:
- "2019"
_4images_image_id: "4042"
_4images_cat_id: "347"
_4images_user_id: "4"
_4images_image_date: "2005-04-20T13:36:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4042 -->
Die Bestandteile von Modell G.

Durch die Führungsnut in den Ringen können die Distanzscheiben kleiner ausfallen und dünner werden.

Die Trommel hat hinter den Schleifringen (Innendurchmesser 16 mm) noch einen dünneren Abschnitt von 15 mm. Da kommt nämlich eine Gray-codierte Lochscheibe drauf, um per Lichtschranke die Drehposition zu ermitteln.