---
layout: "image"
title: "SR_C03.JPG"
date: "2005-04-19T20:23:04"
picture: "SR_C03.jpg"
weight: "36"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4031
imported:
- "2019"
_4images_image_id: "4031"
_4images_cat_id: "347"
_4images_user_id: "4"
_4images_image_date: "2005-04-19T20:23:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4031 -->
Die beiden Verbinder 15 halten die Kontaktdrähte in Position.