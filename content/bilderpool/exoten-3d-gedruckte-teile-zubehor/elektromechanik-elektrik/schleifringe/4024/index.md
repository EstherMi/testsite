---
layout: "image"
title: "SR_A01.JPG"
date: "2005-04-19T20:23:04"
picture: "SR_A01.jpg"
weight: "29"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: ["Schleifring"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4024
imported:
- "2019"
_4images_image_id: "4024"
_4images_cat_id: "347"
_4images_user_id: "4"
_4images_image_date: "2005-04-19T20:23:04"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4024 -->
Hier ist das Ganze zusammengesetzt. Die ft-Achse dient nur zur Führung. Das zweite P-Rohr mit seinen Distanzscheiben hält die Drähte im richtigen Abstand. Man hätte natürlich auch einfach Rillen in dieses Rohr drehen können, aber auf sowas kommt man immer erst später.