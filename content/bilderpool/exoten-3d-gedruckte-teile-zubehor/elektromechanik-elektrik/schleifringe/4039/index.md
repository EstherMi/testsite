---
layout: "image"
title: "SR_F04.JPG"
date: "2005-04-20T13:36:08"
picture: "SR_F04.jpg"
weight: "19"
konstrukteure: 
- "Harald"
fotografen:
- "Harald"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4039
imported:
- "2019"
_4images_image_id: "4039"
_4images_cat_id: "347"
_4images_user_id: "4"
_4images_image_date: "2005-04-20T13:36:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4039 -->
Modell F hat schon fast alles, was ich von einem Schleifring erwarte. Das Restliche kriegt dann Modell G auf den Weg.