---
layout: "image"
title: "SR_F03.JPG"
date: "2005-04-20T13:36:07"
picture: "SR_F03.jpg"
weight: "18"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/4038
imported:
- "2019"
_4images_image_id: "4038"
_4images_cat_id: "347"
_4images_user_id: "4"
_4images_image_date: "2005-04-20T13:36:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=4038 -->
Die Kontaktdrähte haben früher mal einen Spiralblock zusammengehalten. Diese Sorte lässt sich löten und ist elastischer als Kupfer.