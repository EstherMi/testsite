---
layout: "image"
title: "Alternativ Schleiffring: Twist-Stop Pollin"
date: "2008-01-30T14:09:24"
picture: "Twist-Stop-Pollin_005.jpg"
weight: "13"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/13465
imported:
- "2019"
_4images_image_id: "13465"
_4images_cat_id: "347"
_4images_user_id: "22"
_4images_image_date: "2008-01-30T14:09:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13465 -->
