---
layout: "image"
title: "IR LED Baustein"
date: "2008-01-06T20:09:45"
picture: "ledbaustein1.jpg"
weight: "20"
konstrukteure: 
- "bumpf"
fotografen:
- "Walter Mario Graf"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bumpf"
license: "unknown"
legacy_id:
- details/13288
imported:
- "2019"
_4images_image_id: "13288"
_4images_cat_id: "467"
_4images_user_id: "424"
_4images_image_date: "2008-01-06T20:09:45"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13288 -->
Die Hülsen stammen von Zwergstecker-Kupplungen