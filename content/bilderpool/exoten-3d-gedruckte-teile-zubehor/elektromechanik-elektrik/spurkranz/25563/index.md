---
layout: "image"
title: "Spurkranz mit Edelstahlring"
date: "2009-10-20T20:09:37"
picture: "Spurkranz.jpg"
weight: "5"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Andreas Tacke"
keywords: ["modding"]
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/25563
imported:
- "2019"
_4images_image_id: "25563"
_4images_cat_id: "1796"
_4images_user_id: "182"
_4images_image_date: "2009-10-20T20:09:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25563 -->
Ich habe mal einen Spurkranz mit Metallring angefertigt. Damit sollte die Verwirklichung einer FT Lock für LGB Schienen realisieren lassen.