---
layout: "image"
title: "Komplette Lok"
date: "2009-10-28T11:45:12"
picture: "lok1.jpg"
weight: "2"
konstrukteure: 
- "Martin"
fotografen:
- "Martin"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/25577
imported:
- "2019"
_4images_image_id: "25577"
_4images_cat_id: "1796"
_4images_user_id: "373"
_4images_image_date: "2009-10-28T11:45:12"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25577 -->
Und mit ein paar Bauteilen zusätzlich sieht das schon ganz gut aus. Maßstab und solche Dinge bleiben jetzt bitte mal außen vor ;-)