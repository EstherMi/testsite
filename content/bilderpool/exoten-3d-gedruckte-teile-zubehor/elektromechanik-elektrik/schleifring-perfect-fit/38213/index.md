---
layout: "image"
title: "Schleifring Einzelteile"
date: "2014-02-08T22:06:50"
picture: "Schleifring_Einzelteile.jpg"
weight: "5"
konstrukteure: 
- "Richard"
fotografen:
- "Richard"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "NBGer"
license: "unknown"
legacy_id:
- details/38213
imported:
- "2019"
_4images_image_id: "38213"
_4images_cat_id: "2843"
_4images_user_id: "1729"
_4images_image_date: "2014-02-08T22:06:50"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38213 -->
Außer dem Drehkranz und dem Schleifring benötigt man keinerlei weiteren Teile zum Befestigen.
Der Befestigungsring des Schleifringes selbst ist so mit der CNC-Fräse bearbeitet, dass er genau in die inneren Nuten des Drehkranzes passt.
Der Einbau ist dann denkbar einfach: Drehkranz öffnen, Schleifring einlegen, Drehkranz wieder ineinander rasten; FERTIG!

Wer Interesse hat, bitte melden. Ich kann weitere Schleifringe anfertigen.