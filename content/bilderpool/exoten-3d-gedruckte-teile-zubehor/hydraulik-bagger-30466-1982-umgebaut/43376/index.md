---
layout: "image"
title: "Nehmer-Zylinder"
date: "2016-05-16T16:58:44"
picture: "hydraulikbaggerumgebaut12.jpg"
weight: "12"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/43376
imported:
- "2019"
_4images_image_id: "43376"
_4images_cat_id: "3221"
_4images_user_id: "1359"
_4images_image_date: "2016-05-16T16:58:44"
_4images_image_order: "12"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43376 -->
ich habe die Lagerung des Hauptzylinders drehbar gestaltet und schiebe mit dem neuen (schwarzen) Zylinder noch etwas nach..