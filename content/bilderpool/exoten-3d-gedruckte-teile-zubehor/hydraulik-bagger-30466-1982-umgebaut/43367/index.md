---
layout: "image"
title: "Anbau des Geber-zylinders"
date: "2016-05-16T16:58:44"
picture: "hydraulikbaggerumgebaut03.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/43367
imported:
- "2019"
_4images_image_id: "43367"
_4images_cat_id: "3221"
_4images_user_id: "1359"
_4images_image_date: "2016-05-16T16:58:44"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43367 -->
