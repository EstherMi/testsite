---
layout: "image"
title: "Komisches Zeichen"
date: "2008-09-26T08:06:06"
picture: "sonderteile4.jpg"
weight: "37"
konstrukteure: 
- "Fischerwerke"
fotografen:
- "speedy68"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "speedy68"
license: "unknown"
legacy_id:
- details/15631
imported:
- "2019"
_4images_image_id: "15631"
_4images_cat_id: "782"
_4images_user_id: "409"
_4images_image_date: "2008-09-26T08:06:06"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=15631 -->
Bei den unteren 4 Streben ist das X ein bischen komisch geworden.