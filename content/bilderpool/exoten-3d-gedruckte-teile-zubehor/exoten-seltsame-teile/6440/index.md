---
layout: "image"
title: "durchsichtig02.JPG"
date: "2006-06-20T18:40:00"
picture: "durchsichtig02.jpg"
weight: "76"
konstrukteure: 
- "Harald Steinhaus"
fotografen:
- "Harald Steinhaus"
keywords: ["transparent", "durchsichtig", "31503", "31512"]
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/6440
imported:
- "2019"
_4images_image_id: "6440"
_4images_cat_id: "782"
_4images_user_id: "4"
_4images_image_date: "2006-06-20T18:40:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6440 -->
Ein paar durchsichtige Teile (zugehöriger Baukasten unbekannt) - Zur Diskussion hier:
http://www.fischertechnik.de/de/fanclub/forum/default.aspx?g=posts&t=706