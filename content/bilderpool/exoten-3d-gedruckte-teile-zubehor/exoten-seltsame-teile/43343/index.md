---
layout: "image"
title: "Solar 2"
date: "2016-05-07T11:30:24"
picture: "Solar_2_kl.jpg"
weight: "5"
konstrukteure: 
- "Lothar Vogt      Pilami"
fotografen:
- "Lothar Vogt      Pilami"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Pilami"
license: "unknown"
legacy_id:
- details/43343
imported:
- "2019"
_4images_image_id: "43343"
_4images_cat_id: "782"
_4images_user_id: "10"
_4images_image_date: "2016-05-07T11:30:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=43343 -->
Ein mir unbekanntes Teil.
Solarzelle mit Motor und Adapterbaustein