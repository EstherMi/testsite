---
layout: "image"
title: "Strebe X127,2 in gelb, grau und blau"
date: "2010-11-08T22:02:07"
picture: "Strebe127.jpg"
weight: "23"
konstrukteure: 
- "thkais"
fotografen:
- "thkais"
keywords: ["X127", "2", "Strebe", "gelb"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- details/29213
imported:
- "2019"
_4images_image_id: "29213"
_4images_cat_id: "782"
_4images_user_id: "41"
_4images_image_date: "2010-11-08T22:02:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29213 -->
Und es gibt sie doch! Die Strebe X127,2 ist mir beim Aufräumen in die Hände gefallen. Nein, sie ist nicht angepinselt.... Und ich habe keine Ahnung, woher ich sie habe, 12 Stück habe ich in meinem Besitz.