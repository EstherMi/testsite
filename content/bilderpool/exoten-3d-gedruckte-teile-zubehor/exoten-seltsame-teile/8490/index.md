---
layout: "image"
title: "Alte Ritzel und Spannzangen"
date: "2007-01-16T21:46:07"
picture: "RitzelSpannzangen.jpg"
weight: "52"
konstrukteure: 
- "ft"
fotografen:
- "Holger Bernhardt"
keywords: ["Ritzel", "Urteile"]
uploadBy: "Svefisch"
license: "unknown"
legacy_id:
- details/8490
imported:
- "2019"
_4images_image_id: "8490"
_4images_cat_id: "782"
_4images_user_id: "534"
_4images_image_date: "2007-01-16T21:46:07"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8490 -->
Bei diesen Teilen handelt es sich um Ritzel, Kardangelenk und Achsverbinder aus von 1967/68. Besonders sind eigentlich nur die Spannzangen, die noch die grobe Verzahnung aufweisen. Später sollte das Zahnrad mal schwarz, also modern sein :-(