---
layout: "image"
title: "Box 1000 'fischerform' Aufschrift"
date: "2014-08-17T21:31:26"
picture: "fischerform2.jpg"
weight: "2"
konstrukteure: 
- "Martin W. (Masked)"
fotografen:
- "Martin W. (Masked)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Masked"
license: "unknown"
legacy_id:
- details/39244
imported:
- "2019"
_4images_image_id: "39244"
_4images_cat_id: "2936"
_4images_user_id: "373"
_4images_image_date: "2014-08-17T21:31:26"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39244 -->
