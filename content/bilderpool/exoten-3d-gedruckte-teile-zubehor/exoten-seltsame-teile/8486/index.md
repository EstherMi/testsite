---
layout: "image"
title: "Bifi-Präsentation"
date: "2007-01-15T22:50:26"
picture: "Bifi006.jpg"
weight: "53"
konstrukteure: 
- "ft"
fotografen:
- "ft-Club-Heft 1/89"
keywords: ["Bifi"]
uploadBy: "Svefisch"
license: "unknown"
legacy_id:
- details/8486
imported:
- "2019"
_4images_image_id: "8486"
_4images_cat_id: "782"
_4images_user_id: "534"
_4images_image_date: "2007-01-15T22:50:26"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8486 -->
Auf dieser letzten Seite des Clubheftes 1/89 hat fischertechnik die eine Serie von 5 Bifi-Modellen vorgestellt. Es gab zumindest noch eine "Großpackung" mit 5 Modellen