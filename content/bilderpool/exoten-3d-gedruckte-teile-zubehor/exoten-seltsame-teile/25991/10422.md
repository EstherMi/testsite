---
layout: "comment"
hidden: true
title: "10422"
date: "2009-12-26T20:09:57"
uploadBy:
- "Lurchi"
license: "unknown"
imported:
- "2019"
---
Hallo,

auch dieser abgebildete Baustein stammt aus einem Schulprogramm-Baukasten der Fa. LPE Naturwissenschaft und Technik GmbH.

In einer weiteren mir vorliegenden (farbigen) Stückliste eines anderen LPE-Schulbaukastens wird auch dieser Baustein so aufgeführt, allerdings auch wieder ohne die Artikelnummer.

Auch dieser Baustein wird derzeit weder bei LPE noch Knobloch (auch in der 2010er-Liste nicht) geführt noch angeboten.


Gruß

Lurchi