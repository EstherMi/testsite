---
layout: "image"
title: "Baustein schwarz mit runden Zapfen"
date: "2009-12-26T19:06:07"
picture: "neuebauteile2.jpg"
weight: "31"
konstrukteure: 
- "ft Werke"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/25991
imported:
- "2019"
_4images_image_id: "25991"
_4images_cat_id: "782"
_4images_user_id: "182"
_4images_image_date: "2009-12-26T19:06:07"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25991 -->
Gibt es den schon im aktuellen Programm??