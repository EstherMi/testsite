---
layout: "comment"
hidden: true
title: "4401"
date: "2007-10-24T16:58:10"
uploadBy:
- "Eins"
license: "unknown"
imported:
- "2019"
---
Bei den abgebildeten Bausteinen handelt es sich um die erste Generation des Baustein 7,5 rot, diese hatte damals auch schon die Art.-Nr 37468. Enthalten war jeweils ein solcher Stein im Baukasten 50, Art.-Nr. 30120 und im Baukasten 50/2, Art.-Nr. 30141 (Version mit kleiner Wanne). Auf dem Markt waren beide Kästen nur 1975+1976.