---
layout: "image"
title: "Baustein 15"
date: "2010-08-20T17:47:40"
picture: "exoten3.jpg"
weight: "27"
konstrukteure: 
- "fischertechnik"
fotografen:
- "tauchweg (Kurt)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tauchweg"
license: "unknown"
legacy_id:
- details/27833
imported:
- "2019"
_4images_image_id: "27833"
_4images_cat_id: "782"
_4images_user_id: "71"
_4images_image_date: "2010-08-20T17:47:40"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27833 -->
Zapfen fehlt !