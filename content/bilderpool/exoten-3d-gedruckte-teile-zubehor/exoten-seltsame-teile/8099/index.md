---
layout: "image"
title: "rotbraune Teile?"
date: "2006-12-22T19:46:05"
picture: "FT-Teile01b.jpg"
weight: "60"
konstrukteure: 
- "-?-"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- details/8099
imported:
- "2019"
_4images_image_id: "8099"
_4images_cat_id: "782"
_4images_user_id: "488"
_4images_image_date: "2006-12-22T19:46:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8099 -->
Habe vor kurzem ein Minimotor Set bei eBay ersteigert, in dem Teile in einem seltsamen dunklen rotbraun enthalten waren.
Hier auf dem Bild zu sehen:
- links das normale helle rot
- in der Mitte hinten das normale Dunkelrot
- in der Mitte vorne ein BS15 Loch mit einem "Zwischenton" zwischen hell- und dunkelrot (der war auch in dem Set)
- rechts die Teile aus dem Set in einem dunklen rotbraun

Hat jemand von euch auch solch dunkle Teile? Ist das nur ein "Ausrutscher" oder eine "offizielle" Farbe?