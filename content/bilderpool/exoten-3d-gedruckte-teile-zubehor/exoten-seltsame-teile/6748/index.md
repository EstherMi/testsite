---
layout: "image"
title: "Impulsrad für Initiatoren"
date: "2006-08-28T23:29:11"
picture: "impulsrad_01.jpg"
weight: "72"
konstrukteure: 
- "ft"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- details/6748
imported:
- "2019"
_4images_image_id: "6748"
_4images_cat_id: "782"
_4images_user_id: "473"
_4images_image_date: "2006-08-28T23:29:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6748 -->
