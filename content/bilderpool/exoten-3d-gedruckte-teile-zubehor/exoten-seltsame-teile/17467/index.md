---
layout: "image"
title: "Sonderproduktion"
date: "2009-02-20T14:19:07"
picture: "sonderproduktion2.jpg"
weight: "33"
konstrukteure: 
- "ft"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/17467
imported:
- "2019"
_4images_image_id: "17467"
_4images_cat_id: "782"
_4images_user_id: "389"
_4images_image_date: "2009-02-20T14:19:07"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17467 -->
Dieses Teil habe ich auf der didacta 2009 in Hannover entdeckt.