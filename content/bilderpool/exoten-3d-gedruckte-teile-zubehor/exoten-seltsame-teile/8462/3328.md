---
layout: "comment"
hidden: true
title: "3328"
date: "2007-06-03T10:43:10"
uploadBy:
- "heiko"
license: "unknown"
imported:
- "2019"
---
Ach, und wo ich das Bild gerade wiederfinde: Der Gummiring hinten rechts, der war eigentlich dafür gedacht, meinen Unterkiefer nach vorne zu ziehen. 

Beim Kieferorthopäden kriegt man sowas. Die Gummiringe sind sehr klein und vertragen sehr große Kräfte ohne zu reißen.