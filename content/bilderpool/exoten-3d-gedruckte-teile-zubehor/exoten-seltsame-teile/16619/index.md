---
layout: "image"
title: "unbekannte Verkleidungs-  bzw. Verbinderplatte"
date: "2008-12-14T18:49:24"
picture: "verbindungsplatten2.jpg"
weight: "36"
konstrukteure: 
- "ft"
fotografen:
- "Stephan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "stephan"
license: "unknown"
legacy_id:
- details/16619
imported:
- "2019"
_4images_image_id: "16619"
_4images_cat_id: "782"
_4images_user_id: "130"
_4images_image_date: "2008-12-14T18:49:24"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16619 -->
hier hab ich die einmal gedreht