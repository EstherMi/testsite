---
layout: "comment"
hidden: true
title: "18825"
date: "2014-03-07T17:35:00"
uploadBy:
- "Svefisch"
license: "unknown"
imported:
- "2019"
---
Die Prüfriegel hatten aber auch einen pädagogischen Sinn: nämlich um zu testen, ob eine Strebe durch Zug- oder Druckkräfte belastet ist. Siehe z. B. Hobby 1 Band 3, Seit 31.

Gruß
Holger