---
layout: "image"
title: "und noch einmal die 'bunten Platten'"
date: "2007-02-01T18:34:40"
picture: "DSCN1238.jpg"
weight: "48"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/8780
imported:
- "2019"
_4images_image_id: "8780"
_4images_cat_id: "782"
_4images_user_id: "184"
_4images_image_date: "2007-02-01T18:34:40"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8780 -->
man achte hierbei auf den kleinen Zapfen.
Dieser macht es unmöglich die Bauplatte komplett auf zwei Bausteine zu clipsen.
Die rote zeigt eine normale Bauplatte.