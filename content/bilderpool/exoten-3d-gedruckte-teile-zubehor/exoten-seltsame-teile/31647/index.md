---
layout: "image"
title: "Leuchtstein mit (einmal dran gewesenem) Stecker"
date: "2011-08-25T18:50:18"
picture: "Leuchtstein22b.jpg"
weight: "12"
konstrukteure: 
- "unbekannt"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/31647
imported:
- "2019"
_4images_image_id: "31647"
_4images_cat_id: "782"
_4images_user_id: "4"
_4images_image_date: "2011-08-25T18:50:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31647 -->
Diese Leuchtsteine hatten einmal einseitig Stecker dran (allerdings nicht federnd, das war nur Messing ohne Schlitze), so dass man sie direkt anreihen konnte. Die Stecker habe ich abgesägt und später arg bereut. Es wären so schöne exotische Sonderteile gewesen.

Jetzt habe ich mal weiter nach geforscht. Nachdem die Buchse silbern aussah und die Sägekante messing-gelb, mussten das ja wohl zwei Teile sein. Also mit dem Hämmerchen und einem stumpf gefeilten Nagel etwas "gelockt" und siehe da: der Messingstift hat eine Rändelung, die ihn in Position hält. Da hatte sich jemand ziemlich viel Mühe gegeben.