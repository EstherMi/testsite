---
layout: "image"
title: "Exoten"
date: "2011-09-30T16:59:21"
picture: "IMG_6278.JPG"
weight: "11"
konstrukteure: 
- "fischertechnik"
fotografen:
- "Harald Steinhaus"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Harald"
license: "unknown"
legacy_id:
- details/33001
imported:
- "2019"
_4images_image_id: "33001"
_4images_cat_id: "782"
_4images_user_id: "4"
_4images_image_date: "2011-09-30T16:59:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=33001 -->
aus dem Fundus von Arjen Neijsen. Das rote unten links hat einen Stich ins Orange, den es sonstwo nicht gibt.