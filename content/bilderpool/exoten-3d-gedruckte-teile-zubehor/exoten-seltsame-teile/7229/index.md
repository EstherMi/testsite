---
layout: "image"
title: "seltsamer BS 7,5"
date: "2006-10-25T21:11:52"
picture: "BS7_5.jpg"
weight: "61"
konstrukteure: 
- "-?-"
fotografen:
- "Franz Osten (Jettaheizer)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Jettaheizer"
license: "unknown"
legacy_id:
- details/7229
imported:
- "2019"
_4images_image_id: "7229"
_4images_cat_id: "782"
_4images_user_id: "488"
_4images_image_date: "2006-10-25T21:11:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7229 -->
Hier ein seltsamer BS 7,5. Ich weiß nicht mehr, wo der dabei war, ich hab auch bisher nur den einen und auch nie wieder einen solchen gesehen.
Vielleicht weiß ja jemand von euch mehr darüber?