---
layout: "comment"
hidden: true
title: "6911"
date: "2008-08-23T16:04:07"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Das kaputte Zahnrad ist kein Beinbruch (und auch nichts besonderes...).
Die gibt's seit letztem Jahr einzeln von Knobloch, Teilenummern 75104 .. 75108.

Zum Austauschen braucht man nur:
- ein Stück Holz mit nem Loch drin (weiche Auflage fürs Hubgetriebe und Platz für die Achse beim Herausklopfen)
- einen dünnen Metallstift (kleiner 2 mm), ein abgefeilter Bildernagel tut es auch
- einen kleinen Hammer.
Mit Hammer und Stift wird die Achse auf dem betroffenen Zahnrad herausgeklopft und nach Tausch wieder hinein. Wer nicht gerade mit zwei linken Händen und lauter Daumen dran gesegnet ist, kriegt das schon hin. Ich habe jetzt über 20 Getriebe auf die Art repariert (davon 4 eigene).

Die verlängerte Achse sieht ein wenig nach Staudinger oder ft-Messespezialanfertigung aus. Da gehört sicher eine geschlitzte Scheibe zwecks Drehzahlmessung drauf. Winkelmessung eher weniger, weil es ja die erste Achse im Getriebe ist, die also ziemlich schnell dreht.

Gruß,
Harald