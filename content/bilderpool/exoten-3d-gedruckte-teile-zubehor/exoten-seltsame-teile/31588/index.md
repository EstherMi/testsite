---
layout: "image"
title: "Firststein + Giebel"
date: "2011-08-17T17:05:52"
picture: "HE170793.JPG"
weight: "13"
konstrukteure: 
- "-?-"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/31588
imported:
- "2019"
_4images_image_id: "31588"
_4images_cat_id: "782"
_4images_user_id: "9"
_4images_image_date: "2011-08-17T17:05:52"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31588 -->
31515   Firststein 30, 31520 Giebel. Ich bin enttäuscht, hätte gedacht, die hätten den gleichen Winkel. Wie würde man das denn bauen, wenn die Winkel passen sollen? Oder geht das gar nicht?