---
layout: "image"
title: "Giebel- und Firstteile"
date: "2011-08-17T20:13:42"
picture: "ft-Haus003.jpg"
weight: "14"
konstrukteure: 
- "ft"
fotografen:
- "ft"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Svefisch"
license: "unknown"
legacy_id:
- details/31589
imported:
- "2019"
_4images_image_id: "31589"
_4images_cat_id: "782"
_4images_user_id: "534"
_4images_image_date: "2011-08-17T20:13:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31589 -->
"Originalanwndung" der Giebel- und Firstteile von ft aus dem Handbuch der ersten Statikkästen. dr Giebel steckt auf rechtwinkligen Winkelsteinen.