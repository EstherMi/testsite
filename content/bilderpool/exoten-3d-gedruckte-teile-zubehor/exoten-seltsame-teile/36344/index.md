---
layout: "image"
title: "exoten"
date: "2012-12-23T15:24:39"
picture: "exotenbausteine1.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/36344
imported:
- "2019"
_4images_image_id: "36344"
_4images_cat_id: "782"
_4images_user_id: "162"
_4images_image_date: "2012-12-23T15:24:39"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36344 -->
Hier noch ein Bild mit verschiedene Exoten.
Die meiste Teile sind schon bekannte aus den Icare Bausatz (Bausteine Weiss). Aber ich habe jetzt noch 2 Weisse Bausteine 30 die ein bischen durchsichtig sind (ein mit Zapfen und eine ohne). Die sind unbedingt nicht vergilbt. Seht vielleicht so aus.
Die Bausteine 5 (weiss) kommt auch aus das Icare modell, aber ist dann aufgeklebt an das Motorteil.
Wo der schwarze Baustein 5 verwendet wird, ist mir nicht bekannt (fischer logo steht auf die Seite).
Die 2 Winkelbausteine 30 in Grau und Schwarz und denn Winkelstein 60 orange gab es mal bei fischerfriendswoman (noch immer ein Radsel woher die kommen)