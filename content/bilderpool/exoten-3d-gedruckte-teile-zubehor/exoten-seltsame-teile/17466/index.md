---
layout: "image"
title: "Sonderproduktion"
date: "2009-02-20T14:19:07"
picture: "sonderproduktion1.jpg"
weight: "32"
konstrukteure: 
- "-?-"
fotografen:
- "Dirk Kutsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Guilligan"
license: "unknown"
legacy_id:
- details/17466
imported:
- "2019"
_4images_image_id: "17466"
_4images_cat_id: "782"
_4images_user_id: "389"
_4images_image_date: "2009-02-20T14:19:07"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17466 -->
Dieses Teil habe ich auf der didacta 2009 in Hannover entdeckt.