---
layout: "image"
title: "Zeitschaltgerät"
date: "2010-06-27T19:34:32"
picture: "DSCN3506.jpg"
weight: "1"
konstrukteure: 
- "Ludger Mäsing"
fotografen:
- "Ludger Mäsing"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ludger-ftc"
license: "unknown"
legacy_id:
- details/27594
imported:
- "2019"
_4images_image_id: "27594"
_4images_cat_id: "1987"
_4images_user_id: "184"
_4images_image_date: "2010-06-27T19:34:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27594 -->
Leider hat einer der Vorbesitzer dieses Teil als Lötkolbenablage genutzt.