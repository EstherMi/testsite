---
layout: "image"
title: "Profil (-) herstellung 3"
date: "2015-02-08T09:32:36"
picture: "ministeckverbinderfuerrastachsen4.jpg"
weight: "4"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/40457
imported:
- "2019"
_4images_image_id: "40457"
_4images_cat_id: "3034"
_4images_user_id: "2321"
_4images_image_date: "2015-02-08T09:32:36"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40457 -->
Das fertige Profi muss sich leicht in ft-löchern drehen können. Für die Steckverbinder kann man sich dann ein Stück in der richtigen Länge absägen und dann eine Schlüsselfeile gefragt.