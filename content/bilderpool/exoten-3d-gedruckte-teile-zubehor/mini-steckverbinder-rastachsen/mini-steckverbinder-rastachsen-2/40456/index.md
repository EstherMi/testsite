---
layout: "image"
title: "Profil (-) herstellung 2"
date: "2015-02-08T09:32:36"
picture: "ministeckverbinderfuerrastachsen3.jpg"
weight: "3"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/40456
imported:
- "2019"
_4images_image_id: "40456"
_4images_cat_id: "3034"
_4images_user_id: "2321"
_4images_image_date: "2015-02-08T09:32:36"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40456 -->
Das Eisenstück ("Zieh-Eisen") mit Beschriftung, das Loch 4,0-3,9mm ist unbeschriftet.