---
layout: "image"
title: "Profil (-) herstellung 1"
date: "2015-02-08T09:32:36"
picture: "ministeckverbinderfuerrastachsen2.jpg"
weight: "2"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/40455
imported:
- "2019"
_4images_image_id: "40455"
_4images_cat_id: "3034"
_4images_user_id: "2321"
_4images_image_date: "2015-02-08T09:32:36"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40455 -->
Wie schon gesagt, hergestellt habe ich sas Profi aus einem 4x3mm Messing-I-Profil (so ähnlich wie ein H-Profil). Das gibt's im Modellbau und ist zunächst im Querschnitt rechteckig. In dem Eisenstück sind Löcher, die ich von der einen Seite mit einem kleinen Bohrer ganz durchgebohrt habe und von der anderen Seite mit einem etwas größeren Bohrer nur halb durch, So verjüngen sich die Löcher zu einer Seite hin. Mit der größeren Öffnung habe das Eisenstück über das Profil geschoben und dann weiter gepresst und gehämmert (mit dem Hammer und der silbernen Zündkerzen-Nuss im Hintergrund), so dass das Profil hinten etwas runder rauskam. Und das in mehreren Schritten, zuletzt von 4.0 auf 3,9 mm.