---
layout: "image"
title: "Abmessungen"
date: "2015-02-08T09:32:36"
picture: "ministeckverbinderfuerrastachsen1.jpg"
weight: "1"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/40454
imported:
- "2019"
_4images_image_id: "40454"
_4images_cat_id: "3034"
_4images_user_id: "2321"
_4images_image_date: "2015-02-08T09:32:36"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40454 -->
Hier mal die Maße