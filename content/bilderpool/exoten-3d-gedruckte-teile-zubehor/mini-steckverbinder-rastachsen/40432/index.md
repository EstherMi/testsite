---
layout: "image"
title: "Verlängerung um 5mm auch für Differential"
date: "2015-02-01T18:21:42"
picture: "ministeckverbinderfuerrastachsen5.jpg"
weight: "5"
konstrukteure: 
- "Martin Wanke"
fotografen:
- "Martin Wanke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Martin Wanke"
license: "unknown"
legacy_id:
- details/40432
imported:
- "2019"
_4images_image_id: "40432"
_4images_cat_id: "3032"
_4images_user_id: "2321"
_4images_image_date: "2015-02-01T18:21:42"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=40432 -->
Sehr nützlich ist auch eine Verlängerung der Rastachsen in kleinen Schritten von 5mm.