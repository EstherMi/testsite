---
layout: "comment"
hidden: true
title: "9560"
date: "2009-07-14T15:36:52"
uploadBy:
- "Harald"
license: "unknown"
imported:
- "2019"
---
Es ist ja auch weniger der "Verlust" eines Teils, das den SCHAUDER! auslöst. Es ist die Erniedrigung, die das arme Teil unter dem Messer erfährt. Entrechtet und wehrlos in eine Schraubzwinge gespannt, und dann *schnipp* gnadenlos verstümmelt. Wo bleibt amnesty fischertechnikal?

So jedenfalls kann ich mir die Gedankengänge bei den zarter besaiteten Gemütern vorstellen. Der Inschenjöhr denkt halt, dass mit einem gemoddeten, bezahlten und dem geneigten Bastler übereigneten Teil neue Funktionen zugänglich gemacht werden, und für die Standardfunktionen entweder noch genügend O-Teile in der Schublade liegen oder bei Bedarf nachgekauft werden können.

Gruß,
Harald