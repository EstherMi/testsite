---
layout: "image"
title: "Draufsicht"
date: "2017-04-21T22:10:32"
picture: "doppelrelaisbaustein4.jpg"
weight: "4"
konstrukteure: 
- "Titanschorsch"
fotografen:
- "Titanschorsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Titanschorsch"
license: "unknown"
legacy_id:
- details/45785
imported:
- "2019"
_4images_image_id: "45785"
_4images_cat_id: "3402"
_4images_user_id: "1615"
_4images_image_date: "2017-04-21T22:10:32"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45785 -->
Draufsicht mit Messing Bundhülsen