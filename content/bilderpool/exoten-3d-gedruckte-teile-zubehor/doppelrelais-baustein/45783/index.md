---
layout: "image"
title: "geschlossener Baustein"
date: "2017-04-21T22:10:32"
picture: "doppelrelaisbaustein2.jpg"
weight: "2"
konstrukteure: 
- "Titanschorsch"
fotografen:
- "Titanschorsch"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Titanschorsch"
license: "unknown"
legacy_id:
- details/45783
imported:
- "2019"
_4images_image_id: "45783"
_4images_cat_id: "3402"
_4images_user_id: "1615"
_4images_image_date: "2017-04-21T22:10:32"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45783 -->
Geschlossener