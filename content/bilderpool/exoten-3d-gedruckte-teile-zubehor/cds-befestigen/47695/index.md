---
layout: "image"
title: "CDs befestigen, Fixierung 3"
date: "2018-06-19T08:26:44"
picture: "4CD_Fixierung_3.jpg"
weight: "4"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Rüdiger Riedel"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Rüdiger Riedel"
license: "unknown"
legacy_id:
- details/47695
imported:
- "2019"
_4images_image_id: "47695"
_4images_cat_id: "3520"
_4images_user_id: "2635"
_4images_image_date: "2018-06-19T08:26:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47695 -->
Zahnrad Z30 (36264) mit 3 V-Achsen 20 (31690) mit Klemmbuchsen.
Zahnrad Z40 (31022).