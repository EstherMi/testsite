---
layout: "image"
title: "2 Relais im Batterieblockhalter"
date: "2011-03-20T10:28:36"
picture: "SANY0023.jpg"
weight: "13"
konstrukteure: 
- "Markus95 (Forum: ft4ever)"
fotografen:
- "Markus95 (Forum: ft4ever)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Markus95"
license: "unknown"
legacy_id:
- details/30294
imported:
- "2019"
_4images_image_id: "30294"
_4images_cat_id: "463"
_4images_user_id: "1234"
_4images_image_date: "2011-03-20T10:28:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30294 -->
Ich habe hier 2 Relais in einen nicht mehr gebrauchten 9V-Blockbatteriehalter eingebaut. Die Relais und die Kabel wurden mit Heißkleber fixiert.
(Anschlüsse: Seite links/rechts: Stromanschluss
                         Vorderseite: Ausgänge)