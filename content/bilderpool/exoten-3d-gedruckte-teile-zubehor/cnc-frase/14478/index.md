---
layout: "image"
title: "Bild 02 - Fraeser hochkant"
date: "2008-05-05T16:03:46"
picture: "Bild_02_-_Fraeser_hochkant.jpg"
weight: "9"
konstrukteure: 
- "Laserman"
fotografen:
- "Laserman"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/14478
imported:
- "2019"
_4images_image_id: "14478"
_4images_cat_id: "1335"
_4images_user_id: "724"
_4images_image_date: "2008-05-05T16:03:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14478 -->
Fräser hochkant