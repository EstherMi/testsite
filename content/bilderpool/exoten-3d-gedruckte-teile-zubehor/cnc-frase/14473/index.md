---
layout: "image"
title: "Bild 07 - Detail Rollenfuehrung X-Achse"
date: "2008-05-05T16:03:46"
picture: "Bild_07_-_Detail_Rollenfuehrung_X-Achse.jpg"
weight: "4"
konstrukteure: 
- "Laserman"
fotografen:
- "Laserman"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "laserman"
license: "unknown"
legacy_id:
- details/14473
imported:
- "2019"
_4images_image_id: "14473"
_4images_cat_id: "1335"
_4images_user_id: "724"
_4images_image_date: "2008-05-05T16:03:46"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14473 -->
Detail Rollenfuehrung X-Achse

Die Rollen tragen das Gewicht der Holzkonstruktion.

Beim Holz sollte man dicke Sperrholzplaten nehmen (1 cm dick). Sonst bekommt man Probleme mit den Schrauben. Sperrholz bricht dann nämlich.