---
layout: "overview"
title: "Exoten, 3D-Gedruckte Teile, Zubehör, Schnitzereien"
date: 2019-12-17T17:59:42+01:00
legacy_id:
- categories/463
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=463 --> 
Immer wieder bauen Fans bestehende ft-Teile um oder sie basteln selber neue Teile.
Es wird also gebohrt, gesägt, gelötet, 3D-gedruckt, geklebt und gefeilt.
Hier gibts die Fotos dazu!

Außerdem finden sich hier seltene ft-Teile  und ähnliche Exoten.

Achtung! 
Mit Hilfe von ft-Teilen eigen-entwickelte und selbstgebaute Modelle, bei denen NICHTS gefeilt und gesägt wurde,  finden sich in der Kategorie "Modelle".



Nochmal dasselbe:
gebaut, ohne WERKZEUG zu benötigen ==> Kategorie "Modelle".
GEBOHRT/GESÄGT/GELÖTET/anderes Werkzeug verwendet ==> hierher.
Fremdteile (nicht-ft) verwendet ==> hierher.
NICHT gebohrt/gesägt/gelötet/kein Werkzeug verwendet/alles von ft ==> Kategorie "Modelle".