---
layout: "image"
title: "Nabenschlüssel"
date: "2008-06-29T18:30:36"
picture: "Schlssel.jpg"
weight: "35"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Andreas Tacke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/14783
imported:
- "2019"
_4images_image_id: "14783"
_4images_cat_id: "463"
_4images_user_id: "182"
_4images_image_date: "2008-06-29T18:30:36"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14783 -->
Dies ist die Zeichnung von dem Nabenschlüssel. Ich würde Ihn aus 3mm Edelstahl machen. Was haltet Ihr davon?