---
layout: "image"
title: "Bauplatte 15x45 fur Teleskopmobilkran 30474"
date: "2015-10-20T18:21:43"
picture: "bauplatte_15x45_eingeschlagen_Custom.jpg"
weight: "7"
konstrukteure: 
- "Arjen Neijsen"
fotografen:
- "Arjen Neijsen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "jmn"
license: "unknown"
legacy_id:
- details/42104
imported:
- "2019"
_4images_image_id: "42104"
_4images_cat_id: "463"
_4images_user_id: "162"
_4images_image_date: "2015-10-20T18:21:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42104 -->
Hier ein Bild von die Spezielteile fur den Ausleger vom Teleskopmobilkran 30474. Diese Teile sollten eingebaut werden in Stufe 35,36 und 37 damit das innere Aluminium Profil mehr Platz bekommt und einfacher ausschiebt.