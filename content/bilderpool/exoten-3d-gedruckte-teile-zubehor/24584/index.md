---
layout: "image"
title: "hobby 3 Band 2 Seite 47"
date: "2009-07-18T13:03:24"
picture: "zbearbeitung1.jpg"
weight: "29"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/24584
imported:
- "2019"
_4images_image_id: "24584"
_4images_cat_id: "463"
_4images_user_id: "104"
_4images_image_date: "2009-07-18T13:03:24"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=24584 -->
Böse. Ganz böse. :-) Das ist ein elektromechanisches Flipflop, bei dem ein Z20 jedes zweiten Zahnes beraubt wurde.