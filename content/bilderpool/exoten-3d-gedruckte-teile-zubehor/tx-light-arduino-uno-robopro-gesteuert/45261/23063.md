---
layout: "comment"
hidden: true
title: "23063"
date: "2017-02-19T09:57:47"
uploadBy:
- "david-ftc"
license: "unknown"
imported:
- "2019"
---
Hallo Holger,

mich würde auch interessieren, wie die Anbindung an RoboPro aussieht. Was muss man in RoboPro einstellen, damit der Computer den Arduino anstatt des TX(T) erkennt? Wofür sind die Widerstände?

Gruß
David