---
layout: "image"
title: "Stangenverbinder"
date: "2010-09-10T21:56:43"
picture: "stangenverbinder.jpg"
weight: "23"
konstrukteure: 
- "lars b."
fotografen:
- "lars b."
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lars"
license: "unknown"
legacy_id:
- details/28081
imported:
- "2019"
_4images_image_id: "28081"
_4images_cat_id: "463"
_4images_user_id: "1177"
_4images_image_date: "2010-09-10T21:56:43"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=28081 -->
hier hab ich 2  einflügelige radnaben aneinander geklebt damit man 2 stangen 
miteinander (fest) verbinden kann