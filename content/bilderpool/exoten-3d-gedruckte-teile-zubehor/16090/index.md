---
layout: "image"
title: "my new ft interface idea"
date: "2008-10-27T20:01:11"
picture: "dsc01874_resize.jpg"
weight: "31"
konstrukteure: 
- "Thomas Brestrich (schnaggels)"
fotografen:
- "Thomas Brestrich (schnaggels)"
keywords: ["new", "interface", "prototyp"]
uploadBy: "schnaggels"
license: "unknown"
legacy_id:
- details/16090
imported:
- "2019"
_4images_image_id: "16090"
_4images_cat_id: "463"
_4images_user_id: "120"
_4images_image_date: "2008-10-27T20:01:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16090 -->
Das Bild ist meine Vorstellung vom neuen ft Interface Gehäuse aufgrund der Daten aus ersten bekannt gewordenen technischen Details und Abmessungen (90 x 90 x 15 Millimeter) 

Das weiße soll das Display sein...