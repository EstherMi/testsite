---
layout: "image"
title: "Zähler2"
date: "2006-09-03T10:03:24"
picture: "Zhl-rck.jpg"
weight: "22"
konstrukteure: 
- "ft"
fotografen:
- "Kurt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tauchweg"
license: "unknown"
legacy_id:
- details/6779
imported:
- "2019"
_4images_image_id: "6779"
_4images_cat_id: "651"
_4images_user_id: "71"
_4images_image_date: "2006-09-03T10:03:24"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6779 -->
Leiterbahn