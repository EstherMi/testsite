---
layout: "image"
title: "Taktgeber1"
date: "2006-09-03T09:58:17"
picture: "Taktgeber2.jpg"
weight: "19"
konstrukteure: 
- "ft"
fotografen:
- "Kurt"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "tauchweg"
license: "unknown"
legacy_id:
- details/6776
imported:
- "2019"
_4images_image_id: "6776"
_4images_cat_id: "651"
_4images_user_id: "71"
_4images_image_date: "2006-09-03T09:58:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6776 -->
