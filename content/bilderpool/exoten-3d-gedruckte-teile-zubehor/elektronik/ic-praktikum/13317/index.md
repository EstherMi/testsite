---
layout: "image"
title: "auf einen Blick"
date: "2008-01-12T23:52:41"
picture: "Digitalsteine_010.jpg"
weight: "12"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- details/13317
imported:
- "2019"
_4images_image_id: "13317"
_4images_cat_id: "651"
_4images_user_id: "473"
_4images_image_date: "2008-01-12T23:52:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13317 -->
