---
layout: "image"
title: "fischertechnik Interface X2"
date: "2017-04-05T09:16:19"
picture: "ch2.jpg"
weight: "2"
konstrukteure: 
- "chehr"
fotografen:
- "chehr"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chehr"
license: "unknown"
legacy_id:
- details/45723
imported:
- "2019"
_4images_image_id: "45723"
_4images_cat_id: "466"
_4images_user_id: "2374"
_4images_image_date: "2017-04-05T09:16:19"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45723 -->
Bild von der Platine Bestückungsseite