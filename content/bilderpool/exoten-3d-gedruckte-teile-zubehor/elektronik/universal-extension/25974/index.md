---
layout: "image"
title: "Relaisplatine für Universal Extension 1/2"
date: "2009-12-24T14:22:08"
picture: "univex1.jpg"
weight: "1"
konstrukteure: 
- "Peter Pötzi"
fotografen:
- "Peter Pötzi"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "-?-"
license: "unknown"
legacy_id:
- details/25974
imported:
- "2019"
_4images_image_id: "25974"
_4images_cat_id: "1827"
_4images_user_id: "998"
_4images_image_date: "2009-12-24T14:22:08"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25974 -->
Nur einmal ein Prototyp einer Optokoplergesteuerten Relaisplatine (später werden die auf 22x22 Löcher begrenzt)

Die Funktionsweise der Universal Extension:
4 Ausgänge(2 Motoren)
Strombegrenzung auf 10A
Das ansteuern dauert 60mS, Prozeduren in Robo Pro werden veröffentlicht:          
          1.Bit: ist immer high
          10mS warten
          2.Bit: wenn 1: Ausgänge einschalten, wenn 0: ausschalten
         10mS warten
          3. Bit für O1 
          10mS warten
          4.Bit für O2
          10mS warten
          5.Bit für O3
          10mS warten
          6.Bit für O4