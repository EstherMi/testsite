---
layout: "comment"
hidden: true
title: "9343"
date: "2009-05-22T21:41:23"
uploadBy:
- "niekerk"
license: "unknown"
imported:
- "2019"
---
@Ad: when the low sampling frequency is the bottleneck, one solution would be to use the 1 msec timer tick in download mode to sample the input. Just an idea, I never got to actually do this myself.

Regards,
Paul