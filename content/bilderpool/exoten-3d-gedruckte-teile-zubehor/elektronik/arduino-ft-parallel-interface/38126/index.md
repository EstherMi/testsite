---
layout: "image"
title: "Arduino FT Adapterkabel"
date: "2014-01-26T19:53:12"
picture: "IMG_0127.jpg"
weight: "4"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/38126
imported:
- "2019"
_4images_image_id: "38126"
_4images_cat_id: "2738"
_4images_user_id: "1359"
_4images_image_date: "2014-01-26T19:53:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38126 -->
Kabel zum Anschluss von Fischertechnik-Teilen (80er Elektronik, Sensoren) an den Arduino
d.h. Pfostenstecker auf Steckschuh und Pfostenstecker auf FT-Stecker