---
layout: "overview"
title: "Arduino FT Parallel Interface"
date: 2019-12-17T18:02:58+01:00
legacy_id:
- categories/2738
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2738 --> 
Ansteuerung eines alten FT Computing Parallel-Interfaces mit einem Arduino-Board