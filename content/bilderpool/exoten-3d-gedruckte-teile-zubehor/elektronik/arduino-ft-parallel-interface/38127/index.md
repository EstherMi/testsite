---
layout: "image"
title: "Drahtverhau 1 Gesamtübersicht"
date: "2014-01-26T19:53:12"
picture: "IMG_0128.jpg"
weight: "5"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/38127
imported:
- "2019"
_4images_image_id: "38127"
_4images_cat_id: "2738"
_4images_user_id: "1359"
_4images_image_date: "2014-01-26T19:53:12"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38127 -->
Gesamtübersicht. Der Arduiono kann jetzt:

1. alle 4 Motorausgänge des Parallelinterfaces ansteuern
2. alle 8 Tastereingänge des Interfaces abfragen
3. Analog-Eingänge des Arduinos lassen sich z.B. mit Potentiometer verbinden und auswerten
4. Digitale Ausgänge des Arduinos können 80er Elektronik - LST ansteuern

Der Arduino kann jetzt mittels Hohlstecker aus der normalen FT-Spannungsversorgung gespeist werden!