---
layout: "image"
title: "Gesamtansicht Arduino FT Parallelinterface"
date: "2013-04-21T08:03:03"
picture: "IMG_9849.jpg"
weight: "8"
konstrukteure: 
- "Jens"
fotografen:
- "Jens"
keywords: ["Arduino", "FT", "Parallelinterface", "IBM", "PC"]
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/36859
imported:
- "2019"
_4images_image_id: "36859"
_4images_cat_id: "2738"
_4images_user_id: "1359"
_4images_image_date: "2013-04-21T08:03:03"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36859 -->
Versuchsaufbau zur Ansteuerung eines alten (80er/90er) Jahre Parallelinterfaces mit dem Arduino Board.
Ich habe eine Experimentierplatine mit Pfostenleiste ausgestattet.
Das Arduinoboard "wohnt" auf 3 Schrauben, welche ich mit Superkleber auf kleine Verkleidungsplatten geklebt habe . Siehe Detailfotos.
Die Ganze Geschichte funktioniert für 4 Motoren + 8 Tastereingänge.
Analogeingänge nehme ich vom Arduino-Board wegen der besseren Auflösung.

Portmapping:

Anschlüsse zw. Arduino und Interface:
int clock = 5;
int dataout = 4;
int loadout = 2;
int datain = 6;
int loadin = 3;

Analoge Anschlüsse Arduino:
int analog0 = 0;
int analog1 = 1;
int analog2 = 2;
int analog3 = 3;

Anschluss Leistungsstufe:
int digout7 = 7;