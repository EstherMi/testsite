---
layout: "image"
title: "Adapterplatine für Robo-Interface 75151 von Knobloch"
date: "2006-07-22T16:29:00"
picture: "Bild1_man_sieht_Grundplatte.jpg"
weight: "1"
konstrukteure: 
- "Knobloch-GmbH"
fotografen:
- "Reiner Stähler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- details/6647
imported:
- "2019"
_4images_image_id: "6647"
_4images_cat_id: "572"
_4images_user_id: "426"
_4images_image_date: "2006-07-22T16:29:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6647 -->
Hier die Draufsicht ein wenig kann man die Grundplatte erkennen ist 12*6 cm groß !!