---
layout: "image"
title: "Adapterplatine für Robo-Interface 75151"
date: "2006-07-22T16:29:00"
picture: "Bild2_man_sieht_Leuchtdioden.jpg"
weight: "2"
konstrukteure: 
- "Knobloch-GmbH"
fotografen:
- "Reiner Stähler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- details/6648
imported:
- "2019"
_4images_image_id: "6648"
_4images_cat_id: "572"
_4images_user_id: "426"
_4images_image_date: "2006-07-22T16:29:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6648 -->
Hier die Draufsicht man kann unter den Tastern die Led´s sehen die leuchten wenn man den Taster drückt der Eingang wird dann Simuliert oder durch einen angeschlossenen Taster entwerder an den Steckkontakten oder an den Quetschklemmen!!!