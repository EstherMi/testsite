---
layout: "image"
title: "Adapterplatine für Robo-Interface 75151"
date: "2006-07-22T16:29:00"
picture: "Bild5_Taster_gedrckt.jpg"
weight: "5"
konstrukteure: 
- "Knobloch-GmbH"
fotografen:
- "Reiner Stähler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- details/6651
imported:
- "2019"
_4images_image_id: "6651"
_4images_cat_id: "572"
_4images_user_id: "426"
_4images_image_date: "2006-07-22T16:29:00"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6651 -->
Hier ein Bild wo ich die beiden weggeführten drähte zusammen halte dann leuchtet die Led am eingang eins kann aber auch durch den Taster betätigt werden und es kommt eingang i1 beim Interface