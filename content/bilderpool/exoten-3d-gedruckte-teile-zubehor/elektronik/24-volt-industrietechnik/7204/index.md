---
layout: "image"
title: "Infrarot 2"
date: "2006-10-16T20:44:34"
picture: "101MSDCF_002_3.jpg"
weight: "18"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- details/7204
imported:
- "2019"
_4images_image_id: "7204"
_4images_cat_id: "653"
_4images_user_id: "473"
_4images_image_date: "2006-10-16T20:44:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7204 -->
