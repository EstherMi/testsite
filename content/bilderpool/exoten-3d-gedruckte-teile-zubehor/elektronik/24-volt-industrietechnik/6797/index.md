---
layout: "image"
title: "IF5"
date: "2006-09-13T22:41:22"
picture: "101MSDCF_002.jpg"
weight: "70"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- details/6797
imported:
- "2019"
_4images_image_id: "6797"
_4images_cat_id: "653"
_4images_user_id: "473"
_4images_image_date: "2006-09-13T22:41:22"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6797 -->
