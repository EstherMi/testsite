---
layout: "image"
title: "6"
date: "2006-09-16T16:53:05"
picture: "101MSDCF_006_2.jpg"
weight: "24"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- details/6807
imported:
- "2019"
_4images_image_id: "6807"
_4images_cat_id: "653"
_4images_user_id: "473"
_4images_image_date: "2006-09-16T16:53:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6807 -->
