---
layout: "image"
title: "9"
date: "2006-09-16T16:53:05"
picture: "101MSDCF_009.jpg"
weight: "27"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- details/6810
imported:
- "2019"
_4images_image_id: "6810"
_4images_cat_id: "653"
_4images_user_id: "473"
_4images_image_date: "2006-09-16T16:53:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6810 -->
