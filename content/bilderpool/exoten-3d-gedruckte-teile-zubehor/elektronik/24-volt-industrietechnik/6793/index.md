---
layout: "image"
title: "IF2"
date: "2006-09-13T22:02:56"
picture: "101MSDCF_005.jpg"
weight: "68"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- details/6793
imported:
- "2019"
_4images_image_id: "6793"
_4images_cat_id: "653"
_4images_user_id: "473"
_4images_image_date: "2006-09-13T22:02:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6793 -->
Industrieinterface mit 2 Steckplätzen (vergleichbar 2 RoboPros)