---
layout: "image"
title: "IF1"
date: "2006-09-13T22:02:56"
picture: "101MSDCF_006.jpg"
weight: "67"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- details/6792
imported:
- "2019"
_4images_image_id: "6792"
_4images_cat_id: "653"
_4images_user_id: "473"
_4images_image_date: "2006-09-13T22:02:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6792 -->
