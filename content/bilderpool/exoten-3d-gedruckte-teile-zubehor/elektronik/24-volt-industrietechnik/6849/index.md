---
layout: "image"
title: "24V6"
date: "2006-09-16T23:12:56"
picture: "24V_Industrieanlage_006.jpg"
weight: "66"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- details/6849
imported:
- "2019"
_4images_image_id: "6849"
_4images_cat_id: "653"
_4images_user_id: "473"
_4images_image_date: "2006-09-16T23:12:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6849 -->
