---
layout: "image"
title: "23"
date: "2006-09-16T16:53:06"
picture: "101MSDCF_023.jpg"
weight: "41"
konstrukteure: 
- "Micha Etz"
fotografen:
- "Micha Etz"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "FtfanClub"
license: "unknown"
legacy_id:
- details/6824
imported:
- "2019"
_4images_image_id: "6824"
_4images_cat_id: "653"
_4images_user_id: "473"
_4images_image_date: "2006-09-16T16:53:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6824 -->
