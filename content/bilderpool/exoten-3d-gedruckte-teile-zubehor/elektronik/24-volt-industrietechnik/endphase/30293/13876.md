---
layout: "comment"
hidden: true
title: "13876"
date: "2011-03-19T17:55:45"
uploadBy:
- "FtfanClub"
license: "unknown"
imported:
- "2019"
---
ja,Stefan. Die sind jetzt nur mal provisorisch ausgeriichtet. Mir geht es nur darum, was ich mit dem vorhandenen Material etwa erreichen kann. Ich habe ungefähr noch mal das doppelte an Montagebändern. Letztendlich müssen sie alle auf Platten fixiert werden. Das mache ich aber erst, wenn mein Dachgeschoss fertig ist. Dann teile ich das thematisch auf. Problematisch wird das mitder Verkabelung. Ich habe überlegt, das ganze digital über einen Bus zu steuern. Da muß ich mich noch mal richtig schlau machen. Ihr seid aber alle dazu eingeladen mitzumachen ;-)