---
layout: "image"
title: "board in cassette"
date: "2008-06-22T14:52:08"
picture: "cassette800.jpg"
weight: "6"
konstrukteure: 
- "Ad van der Weiden"
fotografen:
- "Ad van der Weiden"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- details/14755
imported:
- "2019"
_4images_image_id: "14755"
_4images_cat_id: "1350"
_4images_user_id: "716"
_4images_image_date: "2008-06-22T14:52:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14755 -->
