---
layout: "overview"
title: "Trägheitssensoren, inertial sensors"
date: 2019-12-17T18:02:30+01:00
legacy_id:
- categories/1350
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1350 --> 
sensor for acceleration, compass etc.