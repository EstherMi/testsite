---
layout: "image"
title: "Humax IR Fernbedienung genutzt zum Fernsteuern von Fischertechnik"
date: "2008-06-14T08:20:58"
picture: "Humax_f.jpg"
weight: "15"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/14674
imported:
- "2019"
_4images_image_id: "14674"
_4images_cat_id: "466"
_4images_user_id: "579"
_4images_image_date: "2008-06-14T08:20:58"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14674 -->
Zusammen mit dem Microcontroller Board ATMEGA16 mit Infrarotempfänger SFH5110 kann man so eine Eigenbau-Fernsteuerung für Fischertechnik-Modelle realisieren.

siehe auch:
http://home.arcor.de/uffmann/RemoteControl.html