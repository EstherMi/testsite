---
layout: "comment"
hidden: true
title: "11312"
date: "2010-04-03T20:42:44"
uploadBy:
- "Triceratops"
license: "unknown"
imported:
- "2019"
---
Hallo

In Planung ist so etwas tatsächlich. Derzeit feile ich noch an einen h4-GB im Format DIL; der paßt dann sogar in eine normale 16polige IC-Fassung.

Alles in allem sind's dann 3 realisierte Varianten des h4-Grundbausteins und etwa 8 weitere, die aus verschiedenen Denkanstößen bei der Entwicklung der jeweiligen Platinen herrühren.

Zusammengestellt wird das ganze dann in einer Art "Silberling"-Fibel (Titel noch offen) und zusammen mit weiteren Tips zu gegebener Zeit veröffentlicht.

Nebenbei gibt's dazu eine Abhandlung über LEDs (mit Tabellen zu Vorwiderständen) und eine weitere Rätselsammlung (hier fehlt vor allem noch das Kinderrätsel).

Gruß, Thomas