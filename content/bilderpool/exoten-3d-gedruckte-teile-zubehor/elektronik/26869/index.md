---
layout: "image"
title: "h4-GB mit umschaltbarer Logik"
date: "2010-04-03T02:15:59"
picture: "h4-GB-Lu.jpg"
weight: "11"
konstrukteure: 
- "Thomas Habig"
fotografen:
- "Thomas Habig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Triceratops"
license: "unknown"
legacy_id:
- details/26869
imported:
- "2019"
_4images_image_id: "26869"
_4images_cat_id: "466"
_4images_user_id: "59"
_4images_image_date: "2010-04-03T02:15:59"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26869 -->
Dieser Nachbau des h4-GB zeichnet sich dadurch
aus, daß er wahlweise in positiver oder negativer
Logik arbeitet; die Auswahl erfolgt via Umschalter.
Die Arbeitsweise sowie die Ausgangszustände
werden jeweils mit 2farb-LED angezeigt. Die Pla-
tine ist passend für die Kassettenbox konfiguriert.