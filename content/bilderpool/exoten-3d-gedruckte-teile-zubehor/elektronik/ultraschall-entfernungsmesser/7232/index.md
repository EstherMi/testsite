---
layout: "image"
title: "SRF05 in action..."
date: "2006-10-28T08:17:34"
picture: "theugly.jpg"
weight: "9"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/7232
imported:
- "2019"
_4images_image_id: "7232"
_4images_cat_id: "602"
_4images_user_id: "371"
_4images_image_date: "2006-10-28T08:17:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7232 -->
