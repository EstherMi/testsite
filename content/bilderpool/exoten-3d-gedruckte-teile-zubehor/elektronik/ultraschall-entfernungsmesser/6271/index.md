---
layout: "image"
title: "Ultrasonic Sensor"
date: "2006-05-17T16:38:32"
picture: "28015.jpg"
weight: "32"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/6271
imported:
- "2019"
_4images_image_id: "6271"
_4images_cat_id: "602"
_4images_user_id: "371"
_4images_image_date: "2006-05-17T16:38:32"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6271 -->
Selected for the price and 'casue of the Ultrasonic in stead of Infra Red.