---
layout: "comment"
hidden: true
title: "1287"
date: "2006-09-07T22:22:58"
uploadBy:
- "rbudding"
license: "unknown"
imported:
- "2019"
---
With this sensor you are measuring the distance between 20cm and 3 meter.

the output is linear from 0..6Volt, so 2 volt per meter.