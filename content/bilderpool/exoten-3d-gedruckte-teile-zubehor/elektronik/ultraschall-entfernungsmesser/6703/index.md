---
layout: "image"
title: "Distance Sensor PCB bottom. Final version 20 aug 2006"
date: "2006-08-21T17:41:14"
picture: "printje_001.jpg"
weight: "18"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/6703
imported:
- "2019"
_4images_image_id: "6703"
_4images_cat_id: "602"
_4images_user_id: "371"
_4images_image_date: "2006-08-21T17:41:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6703 -->
This is the bottom of the distance sensor echo2Voltage converter.