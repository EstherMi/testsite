---
layout: "image"
title: "Ultraschall Sensor"
date: "2007-06-02T11:07:09"
picture: "DSCF0002.jpg"
weight: "6"
konstrukteure: 
- "hans"
fotografen:
- "hans"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "hans"
license: "unknown"
legacy_id:
- details/10654
imported:
- "2019"
_4images_image_id: "10654"
_4images_cat_id: "602"
_4images_user_id: "608"
_4images_image_date: "2007-06-02T11:07:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10654 -->
Ich habe den Bausatz von Conrad und den Optokobler CNY17 - 1 auch