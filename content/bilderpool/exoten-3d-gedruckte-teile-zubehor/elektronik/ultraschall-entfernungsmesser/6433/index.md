---
layout: "image"
title: "Design of the echo pulse to voltage converter for the distance sensor."
date: "2006-06-18T19:44:25"
picture: "distanceSensorEcho2Volt.jpg"
weight: "28"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: ["Distance", "sensor"]
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/6433
imported:
- "2019"
_4images_image_id: "6433"
_4images_cat_id: "602"
_4images_user_id: "371"
_4images_image_date: "2006-06-18T19:44:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6433 -->
The timing is roughly every 10 ms. 

the output is now between 0 and 6 volt.
Every Meter means 2V.
range is between 2cm and 3 meter.

The output is now lineair!