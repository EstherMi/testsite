---
layout: "image"
title: "Ubersicht of the WIP"
date: "2006-06-24T19:38:11"
picture: "Desktop_014.jpg"
weight: "26"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/6572
imported:
- "2019"
_4images_image_id: "6572"
_4images_cat_id: "602"
_4images_user_id: "371"
_4images_image_date: "2006-06-24T19:38:11"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6572 -->
Work in progress, final testing, including Robo interface as power supply and ADC.