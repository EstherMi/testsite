---
layout: "image"
title: "Ultraschall-Entfernungsmesser R. Budding (NL)"
date: "2006-10-02T02:44:55"
picture: "Trein-pendel_over_FT-Tuibrug-Poederoyen_001.jpg"
weight: "11"
konstrukteure: 
- "Richard Budding"
fotografen:
- "Peter Damen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/7052
imported:
- "2019"
_4images_image_id: "7052"
_4images_cat_id: "602"
_4images_user_id: "22"
_4images_image_date: "2006-10-02T02:44:55"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7052 -->
"FT-Schrägseilbrücke-Poederoyen" mit 2 Ultraschall-Entfernungsmessers zum Auslauf/Bremsen der Zugmotor.