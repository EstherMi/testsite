---
layout: "image"
title: "Production picture, back from coating"
date: "2006-10-23T17:47:14"
picture: "project_distance_Ultrasonic_Sensor_007.jpg"
weight: "10"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/7215
imported:
- "2019"
_4images_image_id: "7215"
_4images_cat_id: "602"
_4images_user_id: "371"
_4images_image_date: "2006-10-23T17:47:14"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7215 -->
new batch of prints....