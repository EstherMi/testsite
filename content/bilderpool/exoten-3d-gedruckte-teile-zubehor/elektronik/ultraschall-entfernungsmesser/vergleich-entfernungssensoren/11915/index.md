---
layout: "image"
title: "End Part close to the Wall"
date: "2007-09-23T17:39:05"
picture: "comparedistancesensorfischertechnik06.jpg"
weight: "6"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/11915
imported:
- "2019"
_4images_image_id: "11915"
_4images_cat_id: "1068"
_4images_user_id: "371"
_4images_image_date: "2007-09-23T17:39:05"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11915 -->
The FT sensor is actually reading 3 cm! impressive.
My sensor stops close to 5 cm.
