---
layout: "image"
title: "again..."
date: "2007-09-23T17:39:05"
picture: "comparedistancesensorfischertechnik10.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/11919
imported:
- "2019"
_4images_image_id: "11919"
_4images_cat_id: "1068"
_4images_user_id: "371"
_4images_image_date: "2007-09-23T17:39:05"
_4images_image_order: "10"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11919 -->
