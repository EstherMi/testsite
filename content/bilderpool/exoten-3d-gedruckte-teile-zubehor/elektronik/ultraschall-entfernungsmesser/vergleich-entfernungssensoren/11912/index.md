---
layout: "image"
title: "Clsoe Up of the test environment"
date: "2007-09-23T17:39:04"
picture: "comparedistancesensorfischertechnik03.jpg"
weight: "3"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/11912
imported:
- "2019"
_4images_image_id: "11912"
_4images_cat_id: "1068"
_4images_user_id: "371"
_4images_image_date: "2007-09-23T17:39:04"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11912 -->
