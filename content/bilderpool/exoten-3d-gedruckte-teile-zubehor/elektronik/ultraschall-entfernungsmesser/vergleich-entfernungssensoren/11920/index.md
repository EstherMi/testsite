---
layout: "image"
title: "Train is moving."
date: "2007-09-23T18:24:07"
picture: "comparedistancesensorfischertechnik11.jpg"
weight: "11"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/11920
imported:
- "2019"
_4images_image_id: "11920"
_4images_cat_id: "1068"
_4images_user_id: "371"
_4images_image_date: "2007-09-23T18:24:07"
_4images_image_order: "11"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11920 -->
