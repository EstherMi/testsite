---
layout: "image"
title: "Full length"
date: "2007-09-23T17:39:04"
picture: "comparedistancesensorfischertechnik02.jpg"
weight: "2"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/11911
imported:
- "2019"
_4images_image_id: "11911"
_4images_cat_id: "1068"
_4images_user_id: "371"
_4images_image_date: "2007-09-23T17:39:04"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=11911 -->
