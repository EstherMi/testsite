---
layout: "image"
title: "Ft Interface mod: vergrößerte Darstellung der AVR Platine"
date: "2014-04-08T13:44:57"
picture: "FTUI_mod_zoom.jpg"
weight: "3"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/38536
imported:
- "2019"
_4images_image_id: "38536"
_4images_cat_id: "2877"
_4images_user_id: "579"
_4images_image_date: "2014-04-08T13:44:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38536 -->
In das FT-Interface eingebautes Board der Größe 30x40 mm. Die 5 V-Versorgung wird mit dem schwarzen Kabel zugeführt, das an einem Draht des blauen Kondensators (47 nF)angelötet ist.