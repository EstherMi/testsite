---
layout: "image"
title: "fischertechnik Interface Artikel ?"
date: "2017-04-01T12:40:41"
picture: "ch7.jpg"
weight: "7"
konstrukteure: 
- "chehr"
fotografen:
- "chehr"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chehr"
license: "unknown"
legacy_id:
- details/45711
imported:
- "2019"
_4images_image_id: "45711"
_4images_cat_id: "3392"
_4images_user_id: "2374"
_4images_image_date: "2017-04-01T12:40:41"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45711 -->
