---
layout: "image"
title: "fischertechnik Interface IBM"
date: "2017-04-01T12:40:41"
picture: "ch4.jpg"
weight: "4"
konstrukteure: 
- "chehr"
fotografen:
- "chehr"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "chehr"
license: "unknown"
legacy_id:
- details/45708
imported:
- "2019"
_4images_image_id: "45708"
_4images_cat_id: "3392"
_4images_user_id: "2374"
_4images_image_date: "2017-04-01T12:40:41"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45708 -->
