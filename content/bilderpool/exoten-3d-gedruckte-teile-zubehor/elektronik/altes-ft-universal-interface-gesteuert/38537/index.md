---
layout: "image"
title: "Gehäusedeckel geschlossen: IR-Empfänger im Sichtfenster"
date: "2014-04-08T13:44:57"
picture: "FTUI_mod_total.jpg"
weight: "4"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/38537
imported:
- "2019"
_4images_image_id: "38537"
_4images_cat_id: "2877"
_4images_user_id: "579"
_4images_image_date: "2014-04-08T13:44:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38537 -->
Der Infrarot-Empfänger liegt im Sichtfenster neben der LED, während der Rest des Boards weitgehend hinter dem schwarzen Aufkleber verdeckt ist. Dadurch ist die Modifikation kaum zu erkennen.