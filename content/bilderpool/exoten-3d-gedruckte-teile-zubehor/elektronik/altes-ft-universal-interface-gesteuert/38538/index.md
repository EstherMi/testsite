---
layout: "image"
title: "Signale für ein komplettes Update der Eingänge E1-E8 und Ausgänge M1-M4"
date: "2014-04-08T13:44:57"
picture: "E1_E4_M1_M2_8bit.png"
weight: "5"
konstrukteure: 
- "Dirk Uffmann"
fotografen:
- "Dirk Uffmann"
keywords: ["Datenrate", "Parallel", "Interface", "Universal"]
uploadBy: "uffi"
license: "unknown"
legacy_id:
- details/38538
imported:
- "2019"
_4images_image_id: "38538"
_4images_cat_id: "2877"
_4images_user_id: "579"
_4images_image_date: "2014-04-08T13:44:57"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38538 -->
Zeitliches paralleles Empfangen des Bytes 0x09 (E4 & E1 high) und Senden des Bytes 0x09 (Motor 1 & 2 mit unterschiedlicher Drehrichtung einschalten). Innerhalb von 10 &#956;s ist ein Update des Eingangs- und Ausgangsstatus erfolgt. Die Datenrate liegt bei 800 kbit/s (in beiden Datenflussrichtungen).