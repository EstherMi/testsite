---
layout: "comment"
hidden: true
title: "18900"
date: "2014-04-09T09:09:57"
uploadBy:
- "uffi"
license: "unknown"
imported:
- "2019"
---
Danke für die Blumen, Jens! 

Die hohe Datenraten geht mit einem Trick: Eigentlich liegt DataIn auf Pin 18 am FT-Interface-Pin. Der Ausgang des Schieberegisters HCF4014 für die Eingänge E1-E8 wird auf dem Weg zu Pin 18 über ein Oder-Gatter HCF4071 mit dem Ausgangssignal der Timer NE556 der beiden Analogeingänge verknüpft und dann über einen Treibertransistor BC548 invertiert. Da man die Timersignale aber gar nicht braucht und diese sogar stören, wenn die Triggersignale dafür nicht auf high gelegt werden und die analogen Eingänge nicht über den Flachbandstecker auf die Timer durchgeschleift werden, verwenden wir lieber den direkten Ausgang des Schieberegisters an Pin 17. Hier hat das Ausgangssignal auch noch eine geringere Verzögerungszeit zur positiven Clockflanke und daher kann man dann mit einer deutlich höheren Übertragungsrate arbeiten.