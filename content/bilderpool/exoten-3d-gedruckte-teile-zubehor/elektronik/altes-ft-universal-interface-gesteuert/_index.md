---
layout: "overview"
title: "Altes FT Universal-Interface (Parallelschnittstelle) gesteuert durch AVR Mikrocontroller"
date: 2019-12-17T18:03:02+01:00
legacy_id:
- categories/2877
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2877 --> 
Einbau eines kleinen Mikrocontroller-Boards mit Infrarot-Empfänger in das Gehäuse des FT-Interfaces, um eine offline-fähige Steuereinheit daraus zu machen oder um ein IR-Control-Set für 4 Motoren daraus zu machen. So entfällt das externe Flach-bandkabel zum Steuerrechner. Der Einbau ist von außen kaum zu erkennen, so dass euch ein erstes Erstaunen von FT-Fans sicher sein dürfte, wie ihr es geschafft habt, das FT-Interface off-line-fähig zu machen.