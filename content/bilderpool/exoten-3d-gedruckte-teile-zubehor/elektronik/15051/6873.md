---
layout: "comment"
hidden: true
title: "6873"
date: "2008-08-18T16:09:38"
uploadBy:
- "uffi"
license: "unknown"
imported:
- "2019"
---
Ja, ich hab meinen "ARTIST"-Roboter (Zweirad-Balancierer, siehe: [URL=http://home.arcor.de/uffmann/ARTIST.htm]ARTIST[/URL]) jetzt mit dem Ultraschall-Entfernungsmesser ausgerüstet.

Gruß, uffi.