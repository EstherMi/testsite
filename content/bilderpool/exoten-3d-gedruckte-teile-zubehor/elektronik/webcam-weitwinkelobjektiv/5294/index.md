---
layout: "image"
title: "Objektiv, CCD, Montierung"
date: "2005-11-10T22:35:21"
picture: "Kamera_-_13.jpg"
weight: "7"
konstrukteure: 
- "Heiko Engelke"
fotografen:
- "Heiko Engelke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "heiko"
license: "unknown"
legacy_id:
- details/5294
imported:
- "2019"
_4images_image_id: "5294"
_4images_cat_id: "454"
_4images_user_id: "9"
_4images_image_date: "2005-11-10T22:35:21"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5294 -->
