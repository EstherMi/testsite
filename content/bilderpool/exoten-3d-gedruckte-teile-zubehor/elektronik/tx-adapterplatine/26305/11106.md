---
layout: "comment"
hidden: true
title: "11106"
date: "2010-03-01T22:31:30"
uploadBy:
- "peterholland"
license: "unknown"
imported:
- "2019"
---
Da beim TX die Eingängen I1 bis I8 Taster gegen Masse geschaltet sind, muss man am 75151 Adapterplatine die -Masse für analog Anschlusse ( jetzt für C1 bis C4 ) verbinden mit die Buchsen unter I1 (=I1 bis I8). Auf dem Bild links mit einer grune FT-Steckerbrücke. 

Zum algemeine Verwendung von mehrere (grune) -Masse Anschlusse als Gegenpol für Leitungsausgänge O1..8 nutze ich ein 2-polige "Andreas-Tacker-Verteilerplatte". Diese benutze ich auch für algemeine (rote) +9V Anschlusse.
Die +9V gibt es sich z.B von D1 , wie im Bild.
Die -Masse gibt es sich z.B. von ZE , wie im Bild.

( Die LED´s zeigen den korrekten Zustand nicht an, da diese nur bei einem "+" am Eingang leuchten )

Die andere Link im Forum mit einer Reaktion Ralb Knobloch (10 februari 2010 14:23:02) gibt es unter:
http://www.fischertechnik.de/de/fanclub/forum/default.aspx?g=posts&t=4876


Grüss,

Peter 
Poederoyen NL