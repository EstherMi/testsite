---
layout: "image"
title: "The bridge in action"
date: "2014-03-03T11:03:35"
picture: "txbridgebyjemo3.jpg"
weight: "3"
konstrukteure: 
- "JEMO"
fotografen:
- "JEMO"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- details/38432
imported:
- "2019"
_4images_image_id: "38432"
_4images_cat_id: "2862"
_4images_user_id: "716"
_4images_image_date: "2014-03-03T11:03:35"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38432 -->
The 6 wire flat cable to the TX and the 10 wire flat cable to the Robo Extension.