---
layout: "overview"
title: "tx bridge"
date: 2019-12-17T18:02:41+01:00
legacy_id:
- categories/1913
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=1913 --> 
The circuit diagram of my bridge between the TX-controller and the old I/O-Extensions. The bridge also features an IR interface for the new ControlSet and connections for 4 servos or Sharp distance sensors.