---
layout: "image"
title: "Top view"
date: "2014-03-03T11:03:35"
picture: "txbridge4.jpg"
weight: "5"
konstrukteure: 
- "Ad"
fotografen:
- "Ad"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- details/38421
imported:
- "2019"
_4images_image_id: "38421"
_4images_cat_id: "2860"
_4images_user_id: "716"
_4images_image_date: "2014-03-03T11:03:35"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38421 -->
The distance holders are too long, I need to order some of 8mm. The mounting holes are too close to the edges so it is difficult to fit in the battery case.