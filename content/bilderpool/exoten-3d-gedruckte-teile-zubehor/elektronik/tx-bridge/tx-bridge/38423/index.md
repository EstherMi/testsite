---
layout: "image"
title: "In the lid"
date: "2014-03-03T11:03:35"
picture: "txbridge6.jpg"
weight: "7"
konstrukteure: 
- "Ad"
fotografen:
- "Ad"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- details/38423
imported:
- "2019"
_4images_image_id: "38423"
_4images_cat_id: "2860"
_4images_user_id: "716"
_4images_image_date: "2014-03-03T11:03:35"
_4images_image_order: "6"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38423 -->
Without the mounting posts it fits nicely