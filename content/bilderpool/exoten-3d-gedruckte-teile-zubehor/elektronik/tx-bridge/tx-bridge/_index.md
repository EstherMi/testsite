---
layout: "overview"
title: "TX-Bridge"
date: 2019-12-17T18:02:41+01:00
legacy_id:
- categories/2860
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2860 --> 
An ATMega based hardware interface between the ft TX controller and the old Robo Extension modules and other hardware