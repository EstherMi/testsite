---
layout: "image"
title: "TX-Bridge4"
date: "2010-03-23T21:06:11"
picture: "tx07.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- details/26803
imported:
- "2019"
_4images_image_id: "26803"
_4images_cat_id: "1913"
_4images_user_id: "716"
_4images_image_date: "2010-03-23T21:06:11"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26803 -->
From left to right: LM317, ATMega88, TSOP38, MAX485