---
layout: "image"
title: "5V Version bottom view"
date: "2014-03-03T11:03:35"
picture: "txbridgeotherversions3.jpg"
weight: "3"
konstrukteure: 
- "Ad"
fotografen:
- "Ad"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- details/38427
imported:
- "2019"
_4images_image_id: "38427"
_4images_cat_id: "2861"
_4images_user_id: "716"
_4images_image_date: "2014-03-03T11:03:35"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38427 -->
Here you see that most discrete components are SMD and the MAX485 as well. Also the 7805 had to go to the back side.Note that I accidently also used an SMD LED which is also on the bottom side :(
The diodes on the right are to protect the 3.3V extension module against the 5V of the controller. In a future double sided PCB it would probably be possible to replace the 7805 with an SMD. It will be difficult however to also have mounting holes.