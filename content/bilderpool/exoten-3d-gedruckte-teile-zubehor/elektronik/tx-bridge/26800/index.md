---
layout: "image"
title: "TX-Bridge1"
date: "2010-03-23T19:36:37"
picture: "tx04.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- details/26800
imported:
- "2019"
_4images_image_id: "26800"
_4images_cat_id: "1913"
_4images_user_id: "716"
_4images_image_date: "2010-03-23T19:36:37"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26800 -->
The bridge, except the servo, is powered from the TX