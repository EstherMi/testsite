---
layout: "image"
title: "PCB layout"
date: "2014-04-16T20:17:45"
picture: "tx-bridge_brd_osh.png"
weight: "1"
konstrukteure: 
- "Ad van der Weiden"
fotografen:
- "Ad van der Weiden"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- details/38568
imported:
- "2019"
_4images_image_id: "38568"
_4images_cat_id: "1913"
_4images_user_id: "716"
_4images_image_date: "2014-04-16T20:17:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38568 -->
top layer red
bottom layer blue