---
layout: "image"
title: "TX-bridge schematic"
date: "2010-03-22T07:36:40"
picture: "txbridgeschematic1.jpg"
weight: "12"
konstrukteure: 
- "Ad"
fotografen:
- "Ad"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- details/26792
imported:
- "2019"
_4images_image_id: "26792"
_4images_cat_id: "1913"
_4images_user_id: "716"
_4images_image_date: "2010-03-22T07:36:40"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26792 -->
The circuit diagram of my bridge between the TX-controller and the old I/O-Extensions. The bridge also features an IR interface for the new ControlSet and connections for 4 servos or Sharp distance sensors.