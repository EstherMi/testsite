---
layout: "image"
title: "4 Pin"
date: "2008-12-15T20:21:17"
picture: "4_Pin.jpg"
weight: "2"
konstrukteure: 
- "-Michael-"
fotografen:
- "-Michael-"
keywords: ["4", "Pin", "4Pin"]
uploadBy: "-Michael-"
license: "unknown"
legacy_id:
- details/16621
imported:
- "2019"
_4images_image_id: "16621"
_4images_cat_id: "1505"
_4images_user_id: "820"
_4images_image_date: "2008-12-15T20:21:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=16621 -->
Der schwarze un der rote Pfeil zeigen die Pins die man ans Fischertechnik Interface anschließen muss.