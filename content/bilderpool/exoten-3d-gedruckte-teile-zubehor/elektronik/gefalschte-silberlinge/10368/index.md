---
layout: "image"
title: "Frühe Silberling-Versionen"
date: "2007-05-10T19:16:32"
picture: "fotoausfischertechnikhobbyprospekt1.jpg"
weight: "1"
konstrukteure: 
- "Stefan Falk"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/10368
imported:
- "2019"
_4images_image_id: "10368"
_4images_cat_id: "943"
_4images_user_id: "104"
_4images_image_date: "2007-05-10T19:16:32"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10368 -->
Dies ist eine Seite eines alten hobby-Prospekts (silbern, quadratisch, bestehend aus einem einzigen mehrfach gefalteten sehr breiten Papier). Man sieht hier andere Versionen des h4 Grund- und Relaisbausteins als sie gemeinhin bekannt sind. Vermutung: Dieses Bild ist mit Vorserienexemplaren gemacht worden, oder mit ganz frühen Produktionsexemplaren.