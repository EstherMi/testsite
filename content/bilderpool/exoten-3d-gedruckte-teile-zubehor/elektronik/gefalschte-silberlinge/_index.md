---
layout: "overview"
title: "gefälschte (?) Silberlinge"
date: 2019-12-17T18:02:24+01:00
legacy_id:
- categories/943
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=943 --> 
Die folgenden Bilder habe ich von m0c- im ftc-Chat erhalten. Er hat diese offensichtlich gefälschten Silberlinge zusammen mit einem größeren ft-Konvolut bekommen. Das Original von ft ist jeweils auf der linken Seite die Fälschung auf der rechten zu sehen.

Nachtrag: Womöglich doch keine Fälschungen, siehe Kommentare.