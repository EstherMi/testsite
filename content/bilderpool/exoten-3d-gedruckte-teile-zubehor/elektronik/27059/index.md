---
layout: "image"
title: "h4-GB-Lu - Fertigmodul"
date: "2010-05-04T14:11:09"
picture: "h4-GB-KU.jpg"
weight: "9"
konstrukteure: 
- "Thomas Habig"
fotografen:
- "Thomas Habig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Triceratops"
license: "unknown"
legacy_id:
- details/27059
imported:
- "2019"
_4images_image_id: "27059"
_4images_cat_id: "466"
_4images_user_id: "59"
_4images_image_date: "2010-05-04T14:11:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27059 -->
Dies ist das fertige Modul des h4-Grundbausteins mit umschaltbarer Logik sowie zwei zusätzlichen Buchsen für Plus und Minus. Die spätere Erweiterung auf ein einheitliches Stromversorgungssystem aller Nachbauten sowie des E-Tec Moduls (z. B. verpolungssichere Stecker) ist möglich.