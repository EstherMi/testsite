---
layout: "comment"
hidden: true
title: "5049"
date: "2008-01-17T11:14:27"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
Great work, I was sure that this is possible but nobody tried it before :)

Is this picture just an example? The serial line connector on the left is not connected with the RoboIF?

Could you please share the ft adapted software for the Cam with us? Also think about buying such a cam for a long time...

Thanks,
Thomas