---
layout: "image"
title: "cmucam_explorer_1"
date: "2008-01-16T16:52:10"
picture: "cmucam-11.jpg"
weight: "6"
konstrukteure: 
- "Ad van der Weiden"
fotografen:
- "Ad van der Weiden"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- details/13339
imported:
- "2019"
_4images_image_id: "13339"
_4images_cat_id: "1212"
_4images_user_id: "716"
_4images_image_date: "2008-01-16T16:52:10"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13339 -->
The explorer with a CmuCam3