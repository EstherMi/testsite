---
layout: "image"
title: "cmucam_5"
date: "2008-01-16T16:52:09"
picture: "cmucam-10.jpg"
weight: "5"
konstrukteure: 
- "Ad van der Weiden"
fotografen:
- "Ad van der Weiden"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- details/13338
imported:
- "2019"
_4images_image_id: "13338"
_4images_cat_id: "1212"
_4images_user_id: "716"
_4images_image_date: "2008-01-16T16:52:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13338 -->
Side view with SD/MMC card slot