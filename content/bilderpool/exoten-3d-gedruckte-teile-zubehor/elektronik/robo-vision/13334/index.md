---
layout: "image"
title: "cmucam_1"
date: "2008-01-16T16:52:09"
picture: "cmucam-01.jpg"
weight: "1"
konstrukteure: 
- "Ad van der Weiden"
fotografen:
- "Ad van der Weiden"
keywords: ["camera", "webcam", "cmucan", "vision", "robo", "robot", "image", "recognition", "bilderkennung"]
uploadBy: "Ad"
license: "unknown"
legacy_id:
- details/13334
imported:
- "2019"
_4images_image_id: "13334"
_4images_cat_id: "1212"
_4images_user_id: "716"
_4images_image_date: "2008-01-16T16:52:09"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13334 -->
The CmuCam3 built into a FischerTechnik cassette