---
layout: "image"
title: "Geöffnete Ansicht"
date: "2012-10-09T21:02:19"
picture: "soundmodul04.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kalti"
license: "unknown"
legacy_id:
- details/35855
imported:
- "2019"
_4images_image_id: "35855"
_4images_cat_id: "2678"
_4images_user_id: "1342"
_4images_image_date: "2012-10-09T21:02:19"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35855 -->
Das Sound-Modul geöffnet

Achtung bei dieser Änderung am Modul erlischt die Herstellergarantie!

Diese Anleitung zeigt wie man es machen kann.
Wenn jemand sein Soundmodul oder einen Verstärker bei diesem Umbau zerstört bin ich nicht dafür Verantwortlich!

Dank an C Knobloch für die Erklärung des Prinzips welches er angewendet hat.