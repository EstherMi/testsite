---
layout: "image"
title: "Anschluss"
date: "2012-10-09T21:02:19"
picture: "soundmodul03.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kalti"
license: "unknown"
legacy_id:
- details/35854
imported:
- "2019"
_4images_image_id: "35854"
_4images_cat_id: "2678"
_4images_user_id: "1342"
_4images_image_date: "2012-10-09T21:02:19"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35854 -->
Auf diese Buchse werden die Anschlüsse geleitet.


Achtung bei dieser Änderung am Modul erlischt die Herstellergarantie!

Diese Anleitung zeigt wie man es machen kann.
Wenn jemand sein Soundmodul oder einen Verstärker bei diesem Umbau zerstört bin ich nicht dafür Verantwortlich!

Dank an C Knobloch für die Erklärung des Prinzips welches er angewendet hat.