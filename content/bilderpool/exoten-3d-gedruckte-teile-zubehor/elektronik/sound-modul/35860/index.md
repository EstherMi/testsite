---
layout: "image"
title: "Kabelbelegung"
date: "2012-10-09T21:02:19"
picture: "soundmodul09.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Kalti"
license: "unknown"
legacy_id:
- details/35860
imported:
- "2019"
_4images_image_id: "35860"
_4images_cat_id: "2678"
_4images_user_id: "1342"
_4images_image_date: "2012-10-09T21:02:19"
_4images_image_order: "9"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35860 -->
So ist das Kabel mit Signalen im Normalfall belegt.
Farbabweichungen sind möglich L und R kann auch vertauscht sein das ist hier nicht wichtig.
Grundsätzlich gilt:
Die Drähte die ganz außen sind und nicht isoliert sind, sind GND, Schirm oder auch Minus genannt.
Die 2 Isolierten sind L und R.


Achtung bei dieser Änderung am Modul erlischt die Herstellergarantie!

Diese Anleitung zeigt wie man es machen kann.
Wenn jemand sein Soundmodul oder einen Verstärker bei diesem Umbau zerstört bin ich nicht dafür Verantwortlich!

Dank an C Knobloch für die Erklärung des Prinzips welches er angewendet hat.