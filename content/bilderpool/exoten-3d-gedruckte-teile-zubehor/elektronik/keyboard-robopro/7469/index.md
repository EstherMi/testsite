---
layout: "image"
title: "Keybord mit Robopro"
date: "2006-11-17T00:53:25"
picture: "Keybord_001.jpg"
weight: "1"
konstrukteure: 
- "Peter Damen (Poederoyen Holland)"
fotografen:
- "Peter Damen (Poederoyen Holland)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/7469
imported:
- "2019"
_4images_image_id: "7469"
_4images_cat_id: "1579"
_4images_user_id: "22"
_4images_image_date: "2006-11-17T00:53:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7469 -->
Hier ein Foto vom Inneren mit den Bezeichnungen von Chips.