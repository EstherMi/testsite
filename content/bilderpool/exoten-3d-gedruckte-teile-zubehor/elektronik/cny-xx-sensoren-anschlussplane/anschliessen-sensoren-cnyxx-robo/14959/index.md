---
layout: "image"
title: "Gabellichtschranke"
date: "2008-07-26T16:23:18"
picture: "cny70schb.jpg"
weight: "1"
konstrukteure: 
- "fitec"
fotografen:
- "fitec"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "fitec"
license: "unknown"
legacy_id:
- details/14959
imported:
- "2019"
_4images_image_id: "14959"
_4images_cat_id: "603"
_4images_user_id: "456"
_4images_image_date: "2008-07-26T16:23:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=14959 -->
Ich habe das etwas anders gemacht. So wie das im Schalplan zusehen ist braucht man nur 3 Leitungen, nicht 4.Der Widerstand 150Ohm ist der Vorwiderstand. Der Widerstand 910Ohm begrenzt den Strom auf 10mA. Das ist dann genau wie am IF, da kommt an den I-Eingängen auch 9V und 10mA. So brauche ich keine extra Leitug für die 9V10mA.