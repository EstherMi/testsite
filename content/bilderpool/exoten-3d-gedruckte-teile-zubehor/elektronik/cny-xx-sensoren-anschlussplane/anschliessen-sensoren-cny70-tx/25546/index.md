---
layout: "image"
title: "Leuchtstein mit CNY70"
date: "2009-10-12T00:50:41"
picture: "IMG_2135.jpg"
weight: "4"
konstrukteure: 
- "Paul van Niekerk"
fotografen:
- "Paul van Niekerk"
keywords: ["CNY70", "sensor"]
uploadBy: "niekerk"
license: "unknown"
legacy_id:
- details/25546
imported:
- "2019"
_4images_image_id: "25546"
_4images_cat_id: "1793"
_4images_user_id: "385"
_4images_image_date: "2009-10-12T00:50:41"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=25546 -->
Leuchtstein (38216) mit CNY70.

+9V --> collector des CNY70
emitter des CNY70 --> TX controller eingang UND mit R=10k nach GND.

Die Eingang soll Analog 10V sein.