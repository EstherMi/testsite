---
layout: "image"
title: "LCD für Robo-Interface beleuchtet"
date: "2006-11-25T08:51:01"
picture: "robo-lcd6.jpg"
weight: "17"
konstrukteure: 
- "thkais"
fotografen:
- "thkais"
keywords: ["LCD", "Roboint", "Robointerface"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- details/7597
imported:
- "2019"
_4images_image_id: "7597"
_4images_cat_id: "778"
_4images_user_id: "41"
_4images_image_date: "2006-11-25T08:51:01"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7597 -->
Die Beleuchtung braucht ca. 15mA, das ist noch zu verkraften. Sie ist immer eingeschaltet.