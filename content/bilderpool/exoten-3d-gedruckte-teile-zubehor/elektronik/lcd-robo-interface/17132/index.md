---
layout: "image"
title: "LCD showing the character set in a rectangle"
date: "2009-01-23T08:29:17"
picture: "IMG_3024.jpg"
weight: "3"
konstrukteure: 
- "Ad van der Weiden"
fotografen:
- "Ad van der Weiden"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- details/17132
imported:
- "2019"
_4images_image_id: "17132"
_4images_cat_id: "778"
_4images_user_id: "716"
_4images_image_date: "2009-01-23T08:29:17"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17132 -->
