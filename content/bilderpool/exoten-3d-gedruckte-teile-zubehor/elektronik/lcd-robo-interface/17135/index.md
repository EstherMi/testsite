---
layout: "image"
title: "Solder side of the PCB"
date: "2009-01-23T08:29:18"
picture: "IMG_2978.jpg"
weight: "6"
konstrukteure: 
- "Ad van der Weiden"
fotografen:
- "Ad van der Weiden"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- details/17135
imported:
- "2019"
_4images_image_id: "17135"
_4images_cat_id: "778"
_4images_user_id: "716"
_4images_image_date: "2009-01-23T08:29:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17135 -->
The white component in the middle is the foil connector to the LCD. It has 2 rows and a funny pitch (the 11 pins of one row span 9 rows of the PCB), hence we need a lot of 'Freiluftverdrahtung'.