---
layout: "comment"
hidden: true
title: "7665"
date: "2008-10-27T19:41:04"
uploadBy:
- "ConJacsDad"
license: "unknown"
imported:
- "2019"
---
Hallo thkais, 
ich hab immer gedacht, RS232 ist RS232. Deshalb war mein Gedanke, ein textbasierts RS232-LCD direkt an den RS232-Anschluss des Robo-Interface anzuschließen und die  für's LCD benötigten Befehlssequenzen einfach als "Datenstrom" aus einer entsprechenden Liste, wie sie auch vom RoboPro angeboten wird, über den RS232 des RoboPro an das LCD zu senden. Läßt sich der RS232 vom Robointerface so nicht nutzen? Ähnlich scheint mir dass in dem von Dir und Fredi eingestellten Programm auch abzulaufen. Der Befehlscode (Listen) müsste natürlich auf denjenigen der Electronic-Assembly-Teile angepasst werden, was aber dank der guten Dokumentation kein Problem sein dürfte.
Gruß ConJacsDad