---
layout: "image"
title: "LCD 128x64 (Grafikfähig) extern"
date: "2008-01-07T21:59:42"
picture: "LCD_extern_128x64.jpg"
weight: "12"
konstrukteure: 
- "thkais"
fotografen:
- "thkais"
keywords: ["LCD"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- details/13292
imported:
- "2019"
_4images_image_id: "13292"
_4images_cat_id: "778"
_4images_user_id: "41"
_4images_image_date: "2008-01-07T21:59:42"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=13292 -->
Dies ist der Prototyp meines neuesten Projekts: Ein 128x64 Pixel LCD. Es passt in eine 60x60 Teilebox.