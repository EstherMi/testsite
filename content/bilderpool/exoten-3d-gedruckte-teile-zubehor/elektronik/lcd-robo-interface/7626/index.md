---
layout: "image"
title: "Aufbau des Grafik-LCD"
date: "2006-11-26T12:47:56"
picture: "robo-lcd14.jpg"
weight: "15"
konstrukteure: 
- "thkais"
fotografen:
- "thkais"
keywords: ["LCD", "roboint", "Robointerface"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- details/7626
imported:
- "2019"
_4images_image_id: "7626"
_4images_cat_id: "778"
_4images_user_id: "41"
_4images_image_date: "2006-11-26T12:47:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7626 -->
Einen Wehrmutstropfen muss es ja geben: Der Controller des LCD ist relativ "dumm", deshalb muss ein AVR von Atmel z.B. das Generieren des Zeichensatzes übernehmen. Bedingt durch die Anzahl der benötigten Bauteile (auf der Unterseite der Platine, hier nicht sichtbar) sind viele Teile, u.a. auch der Prozessor, in SMD ausgeführt. Das LCD selber wird mit einem Folienleiter direkt auf die Platine gelötet (unterer Bildrand). Für ungeübte leider völlig unmöglich.