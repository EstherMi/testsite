---
layout: "image"
title: "Scope Demo"
date: "2009-01-23T08:29:19"
picture: "IMG_3035.jpg"
weight: "10"
konstrukteure: 
- "Ad van der Weiden"
fotografen:
- "Ad van der Weiden"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- details/17139
imported:
- "2019"
_4images_image_id: "17139"
_4images_cat_id: "778"
_4images_user_id: "716"
_4images_image_date: "2009-01-23T08:29:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17139 -->
A demo of use as a chart recorder. Here showing the (past) status of the 8 inputs.