---
layout: "image"
title: "Externes LCD"
date: "2006-11-26T12:47:56"
picture: "robo-lcd12.jpg"
weight: "13"
konstrukteure: 
- "thkais"
fotografen:
- "thkais"
keywords: ["LCD", "Roboint", "Robointerface"]
uploadBy: "thkais"
license: "unknown"
legacy_id:
- details/7624
imported:
- "2019"
_4images_image_id: "7624"
_4images_cat_id: "778"
_4images_user_id: "41"
_4images_image_date: "2006-11-26T12:47:56"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=7624 -->
Dies ist ein grafikfähiges LCD, das per serieller Schnittstelle an das Robo-Interface angeschlossen werden kann.