---
layout: "image"
title: "LCD as RI status display"
date: "2009-01-23T08:29:18"
picture: "IMG_3032.jpg"
weight: "9"
konstrukteure: 
- "Ad van der Weiden"
fotografen:
- "Ad van der Weiden"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Ad"
license: "unknown"
legacy_id:
- details/17138
imported:
- "2019"
_4images_image_id: "17138"
_4images_cat_id: "778"
_4images_user_id: "716"
_4images_image_date: "2009-01-23T08:29:18"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=17138 -->
A demonstration, showing the live status of all inputs and outputs of the RI and (in this case) one extension unit.