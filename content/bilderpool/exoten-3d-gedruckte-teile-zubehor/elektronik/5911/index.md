---
layout: "image"
title: "meinzehneurointerface  schema"
date: "2006-03-20T18:01:45"
picture: "INT819_sch.jpg"
weight: "21"
konstrukteure: 
- "xbach"
fotografen:
- "xbach"
keywords: ["interface", "schaltplan"]
uploadBy: "xbach"
license: "unknown"
legacy_id:
- details/5911
imported:
- "2019"
_4images_image_id: "5911"
_4images_cat_id: "466"
_4images_user_id: "427"
_4images_image_date: "2006-03-20T18:01:45"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5911 -->
alles bekannte, leicht beschaffbare 
Bauteile ( Conrad, farnell, reichelt, segor, bastelkiste .... ) 
Bauteilekosten ca. 10 Euro