---
layout: "image"
title: "Strichlaser"
date: "2011-07-14T11:10:48"
picture: "bild03.jpg"
weight: "5"
konstrukteure: 
- "Martin Peché"
fotografen:
- "Martin Peché"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- details/31228
imported:
- "2019"
_4images_image_id: "31228"
_4images_cat_id: "2322"
_4images_user_id: "1218"
_4images_image_date: "2011-07-14T11:10:48"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31228 -->
Hier der Strichlaser