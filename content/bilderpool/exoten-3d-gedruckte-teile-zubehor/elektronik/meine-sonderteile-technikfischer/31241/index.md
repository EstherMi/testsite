---
layout: "image"
title: "Kompressor"
date: "2011-07-14T11:35:20"
picture: "bild5.jpg"
weight: "9"
konstrukteure: 
- "Martin Peché"
fotografen:
- "Martin Peché"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- details/31241
imported:
- "2019"
_4images_image_id: "31241"
_4images_cat_id: "2322"
_4images_user_id: "1218"
_4images_image_date: "2011-07-14T11:35:20"
_4images_image_order: "5"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31241 -->
Kompressor im Gehäuse, 9V Batteriefach, in das ich ein Loch für den Sclauch gefräst habe.