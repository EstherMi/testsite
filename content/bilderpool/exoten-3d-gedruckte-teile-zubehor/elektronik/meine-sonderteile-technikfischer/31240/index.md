---
layout: "image"
title: "Kompressor"
date: "2011-07-14T11:35:20"
picture: "bild4.jpg"
weight: "8"
konstrukteure: 
- "Martin Peché"
fotografen:
- "Martin Peché"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- details/31240
imported:
- "2019"
_4images_image_id: "31240"
_4images_cat_id: "2322"
_4images_user_id: "1218"
_4images_image_date: "2011-07-14T11:35:20"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31240 -->
Ist wahrscheinlich der gleiche Kompressor wie in http://www.ftcommunity.de/details.php?image_id=30986
Auch aus einem Blutdruckmessgerät