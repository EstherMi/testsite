---
layout: "image"
title: "Laser"
date: "2011-07-14T11:10:48"
picture: "bild01.jpg"
weight: "1"
konstrukteure: 
- "Martin Peché"
fotografen:
- "Martin Peché"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "technikfischer"
license: "unknown"
legacy_id:
- details/31226
imported:
- "2019"
_4images_image_id: "31226"
_4images_cat_id: "2322"
_4images_user_id: "1218"
_4images_image_date: "2011-07-14T11:10:48"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=31226 -->
Meine 3 Laserpointer, Punkt,Strich,Kreuz
Alle drei scharfstellbar