---
layout: "image"
title: "Nachbau h4-GB, h4-MF & Doppelrelais"
date: "2010-05-04T14:11:08"
picture: "h4-Module.jpg"
weight: "8"
konstrukteure: 
- "Thomas Habig"
fotografen:
- "Thomas Habig"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Triceratops"
license: "unknown"
legacy_id:
- details/27058
imported:
- "2019"
_4images_image_id: "27058"
_4images_cat_id: "466"
_4images_user_id: "59"
_4images_image_date: "2010-05-04T14:11:08"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=27058 -->
Dies sind die Fertigmodule in positiver Logik des h4-Grundbausteins, h4-Monoflops sowie die Eigenkreation eines Doppelrelais - allesamt im Design E-Tec. Die Stromzuführung erfolgt analog über separate Kabel und kann im Rahmen einer Vereinheitlichung auf ein eigenständiges (verpolungssicheres) Stecksystem erweitert werden.