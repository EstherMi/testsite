---
layout: "image"
title: "Interface without FT"
date: "2006-04-15T23:14:27"
picture: "oude_interface_001.jpg"
weight: "1"
konstrukteure: 
- "Richard R. Budding"
fotografen:
- "Richard R. Budding"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rbudding"
license: "unknown"
legacy_id:
- details/6123
imported:
- "2019"
_4images_image_id: "6123"
_4images_cat_id: "528"
_4images_user_id: "371"
_4images_image_date: "2006-04-15T23:14:27"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=6123 -->
Little print is the speech chip, combined by the interface with 6 digital in and 6 digital out.