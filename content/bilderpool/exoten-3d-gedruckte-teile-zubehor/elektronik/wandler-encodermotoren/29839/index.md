---
layout: "image"
title: "Versuch Encoder Robo"
date: "2011-01-31T19:08:37"
picture: "versuchsaufbau1_2.jpg"
weight: "2"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/29839
imported:
- "2019"
_4images_image_id: "29839"
_4images_cat_id: "2194"
_4images_user_id: "182"
_4images_image_date: "2011-01-31T19:08:37"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29839 -->
Hier wie gewünscht das Testprogramm zum Anschluß des Encodermotors an das Robo Interface mit meinem Impulswandler.