---
layout: "image"
title: "Versuch Encoder TX"
date: "2011-01-31T19:08:38"
picture: "versuchsaufbau2.jpg"
weight: "3"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/29840
imported:
- "2019"
_4images_image_id: "29840"
_4images_cat_id: "2194"
_4images_user_id: "182"
_4images_image_date: "2011-01-31T19:08:38"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=29840 -->
Hier wie gewünscht das Testprogramm zum Anschluß des Encodermotors am TX Controler.