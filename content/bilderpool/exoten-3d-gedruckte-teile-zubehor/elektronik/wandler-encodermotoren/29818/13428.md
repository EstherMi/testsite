---
layout: "comment"
hidden: true
title: "13428"
date: "2011-01-30T13:19:28"
uploadBy:
- "Stefan Falk"
license: "unknown"
imported:
- "2019"
---
Saubere Arbeit, Respekt!

Gibt es denn beim Hin- und Herfahren auch mit dem TX aber nicht immer mal kumulative Fehler, weil durch Nachlaufen oder so Impulse auch mal in die falsche Richtung gezählt werden? Oder ist das wirklich für Dauerbetrieb "sauber"?

Gruß,
Stefan