---
layout: "comment"
hidden: true
title: "13441"
date: "2011-01-31T11:44:18"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
Hallo Andreas (TST),
du hast dazu sicher ein Steuerprogramm. Sei so nett und lege das mal offen. Dann hätte das Kommentieren dazu eine sachliche Basis.
Hallo Martin (Remadus),
dein Kommentar bestätigt kurz und prägnant, was ich im Forum dazu schon mal versucht habe zu kommentieren. Deshalb sind für mich auch Meßwerte bei einer Stromaufnahme nahe an der Grenze 250 mA sowie bei schwankender Last von Interesse. Letzteres dürfte die Werte verschlechtern. Wer schon mal einen solchen Motor an Zapfen und Stator fixiert vorsichtig an der Magnetscheibe und damit den Rotor "bewegt" hat wird das Kommentierte verstehen. Im Gespräch sollen ja nach wie vor auch sein 2-polige ft-Encodermotoren, die man dann wahlweise 2-polig (2 an einen TX) oder 1-polig (4 an einen TX) betreiben könnte. Es fehlt halt noch der praktische Beweis der Notwendigkeit seitens der ft-Fans, der ft dann endgültig überzeugt ...
Gruß, Ingo