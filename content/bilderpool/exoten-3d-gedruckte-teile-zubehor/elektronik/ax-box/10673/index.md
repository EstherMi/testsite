---
layout: "image"
title: "Zusammenbau"
date: "2007-06-03T19:07:01"
picture: "axbox4.jpg"
weight: "4"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/10673
imported:
- "2019"
_4images_image_id: "10673"
_4images_cat_id: "969"
_4images_user_id: "453"
_4images_image_date: "2007-06-03T19:07:01"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10673 -->
