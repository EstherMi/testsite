---
layout: "image"
title: "Zusammenbau"
date: "2007-06-03T19:07:01"
picture: "axbox2.jpg"
weight: "2"
konstrukteure: 
- "Frederik"
fotografen:
- "Frederik"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fredy"
license: "unknown"
legacy_id:
- details/10671
imported:
- "2019"
_4images_image_id: "10671"
_4images_cat_id: "969"
_4images_user_id: "453"
_4images_image_date: "2007-06-03T19:07:01"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10671 -->
