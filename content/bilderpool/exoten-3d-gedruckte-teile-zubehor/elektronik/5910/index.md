---
layout: "image"
title: "meinzehneurointerface"
date: "2006-03-20T18:01:44"
picture: "int819_small.jpg"
weight: "20"
konstrukteure: 
- "xbach"
fotografen:
- "xbach"
keywords: ["interface", "rs232"]
uploadBy: "xbach"
license: "unknown"
legacy_id:
- details/5910
imported:
- "2019"
_4images_image_id: "5910"
_4images_cat_id: "466"
_4images_user_id: "427"
_4images_image_date: "2006-03-20T18:01:44"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=5910 -->
6+ dig. Eingänge, 3 Analogeingänge (intell.Sensor), bis 5 Motoren, bis 2A, PWM, C - programmierbar, kaskadierbar ....
RS232, Bauteilekosten ca. 10 Euro