---
layout: "image"
title: "Wandlerplatine"
date: "2009-04-13T20:49:34"
picture: "Platine.jpg"
weight: "2"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Andreas Tacke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/23711
imported:
- "2019"
_4images_image_id: "23711"
_4images_cat_id: "1619"
_4images_user_id: "182"
_4images_image_date: "2009-04-13T20:49:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23711 -->
Hier ist die fertig aufgebaute Platine zu sehen. Die Platine habe ich mir auf einer Gravograph Graviermaschine IM3 gemacht.