---
layout: "comment"
hidden: true
title: "9087"
date: "2009-04-28T13:36:20"
uploadBy:
- "schnaggels"
license: "unknown"
imported:
- "2019"
---
@TST
Genau, Kondensatoren. So ein Spannungsregler hat eigentlich meist einen am Ein und Ausgang um ein Schwingen zu verhindern. Schau mal in's Datenblatt, findest du beim Artikel im Reichelt Shop. Das geht hier in dem Fall vermutlich auch so die meiste Zeit gut, sicherer wäre es aber mit.

Thomas