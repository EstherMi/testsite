---
layout: "image"
title: "Wandler mit Motor"
date: "2009-04-13T21:12:13"
picture: "Wandler_Motor.jpg"
weight: "4"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Andreas Tacke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/23713
imported:
- "2019"
_4images_image_id: "23713"
_4images_cat_id: "1619"
_4images_user_id: "182"
_4images_image_date: "2009-04-13T21:12:13"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23713 -->
Hier nun komplett mit Motor. An den beiden Anschlüssen "M" wird der Motorausgang vom Interface angeschlossen. "+" und "-" ist die Spannungsversorgung 9V. Der Anschluß "S" ist der Signalausgang der mit den digitalen Eingängen am Interface verbunden wird.