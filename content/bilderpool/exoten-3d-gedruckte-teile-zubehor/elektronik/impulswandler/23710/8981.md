---
layout: "comment"
hidden: true
title: "8981"
date: "2009-04-13T21:52:38"
uploadBy:
- "niekerk"
license: "unknown"
imported:
- "2019"
---
Well done, looks great. This should work fine for the RoboInterface. But for the new TX controller, division by 2 would be more appropriate, since it handles signals up to 1 kHz. So in order to be future proof, you would need to have two outputs: div64 for the RoboInterface as well as div2 for the new TX controller.

Paul