---
layout: "comment"
hidden: true
title: "9005"
date: "2009-04-16T19:35:40"
uploadBy:
- "Ad"
license: "unknown"
imported:
- "2019"
---
Hi Andreas,

Nice construction but electronically I suspect a small snag. This may become noticable when you change the motor direction and is caused by the divider. Suppose you turn the motor clockwise, the divider contains 0 and your software counter is also 0. Now suppose you stop the motor when the count reaches 100 (count only falling edges), the divider will contain an unknown number between 0 and 63, let's assume it is 60. Now when you reverse the motor (counterclockwise) you would expect a rising edge after 60 encoder pulses but this is not the case because the divider still counts up, so instead you will have a falling edge after 4 encoder pulses. This will result in a small error that however will accumulate causing your software zero to drift away from the starting point. The remedy would be to use an up/down counter for the divider. The count direction has to be determined by the other encoder channel. On the Internet I found an IC that might be suitable (uPD4702) but I don't know if it is easy to get. Otherwise we will have to make something in a GAL.
Regards, Ad