---
layout: "image"
title: "Die wichtigsten Einzelteile"
date: "2009-04-13T20:49:34"
picture: "Einzelteile.jpg"
weight: "1"
konstrukteure: 
- "Andreas Tacke"
fotografen:
- "Andreas Tacke"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/23710
imported:
- "2019"
_4images_image_id: "23710"
_4images_cat_id: "1619"
_4images_user_id: "182"
_4images_image_date: "2009-04-13T20:49:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=23710 -->
Nun bin ich auch in den Elektronik Bereich eingestiegen.
Ich habe mit Hilfe von DMDBT (hier noch mal DANKE) einen Impulswandler entworfen der die zu schnellen Impulse des Encoders der an den Maxon Motoren 138062 sitzt teilt. Der Encoder liefert ein Signal mit einer Frequenz von ca 1950 Hz. Das ist für das Interface natürlich viel zu schnell. Der Binärzähler 74HC4040N teilt dieses Signal durch 64. Somit erhält man ein Signal was das Interface verarbeiten kann. Die Motoren lassen sich somit super für Positionerrungen nutzen.
Hier die wichtigsten Bestandteile. Es fehlen im Bild noch ein Spannungswandler, eine Diode sowie die Steckerhülsen.