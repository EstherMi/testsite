---
layout: "image"
title: "28pol-Buchsenplatte mit Flachbandkabel- Nr75082 + Nr75083"
date: "2010-02-06T15:38:25"
picture: "28pol-Buchsenplatte_003.jpg"
weight: "1"
konstrukteure: 
- "Peter Damen (Poederoyen NL)"
fotografen:
- "Peter Damen (Poederoyen NL)"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "peterholland"
license: "unknown"
legacy_id:
- details/26210
imported:
- "2019"
_4images_image_id: "26210"
_4images_cat_id: "1863"
_4images_user_id: "22"
_4images_image_date: "2010-02-06T15:38:25"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26210 -->
28pol-Buchsenplatte mit Flachbandkabel- Nr75082 + Nr75083 

Es wäre gut sein als Knobloch etwas als Alternative für den Robo TX Controller ableiten könnte.