---
layout: "comment"
hidden: true
title: "11137"
date: "2010-03-06T08:33:25"
uploadBy:
- "Udo2"
license: "unknown"
imported:
- "2019"
---
@fischdidel,
ich wollte noch zur Lösungssuche der Realität halber nicht vergessen zu erwähnen, dass der Zeitaufwand für das Drehen der roten und grünen Flachstecker und für das An- und Aufstecken der zur Verdeutlichung andersfarbenen hier bei ca. 15 min lag ...
Gruss, Udo2