---
layout: "image"
title: "07 Aufkleber und Kurzanleitung"
date: "2010-02-26T21:03:47"
picture: "robotxcontrollerstiftleistenanschluss7.jpg"
weight: "10"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/26558
imported:
- "2019"
_4images_image_id: "26558"
_4images_cat_id: "1893"
_4images_user_id: "723"
_4images_image_date: "2010-02-26T21:03:47"
_4images_image_order: "7"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26558 -->
Wer das Anschlussset evtl. 1:1 allerdings dann ohne Garantie nachbauen will, kann diese Abbildung mit der Kurzanleitung hier runterladen und damit seine Aufkleber auf selbstklebende Etikettformate drucken.
Der Aufkleber für die 28pol. Buchsenplatte muss bei angepasster Auflösung von Bild oder Druckertreiber die Grösse 20mm x 59mm haben.

Edit 27.02.2010:
32657 Gehäuse der 28pol. Buchsenplatte (ohne Inhalt) ist "sVr"
Edit 27.02.2010:
Bitte Hinweis zur Polanzahl für +9V und Masse unter Bild 03 beachten!

01.03.2010:
Belegung der Buchsenplatte kompatibel an der Stiftleiste des ROBO TX Controllers auch mit dem Eingang der von Peter Damen modifizierten Adapterplatine 75151 unter
http://www.ftcommunity.de/details.php?image_id=26570