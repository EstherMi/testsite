---
layout: "image"
title: "08 Aufkleber2 und Kurzanleitung"
date: "2010-03-01T16:45:05"
picture: "robotxcontrollerstiftleistenanschluss1.jpg"
weight: "1"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/26570
imported:
- "2019"
_4images_image_id: "26570"
_4images_cat_id: "1895"
_4images_user_id: "723"
_4images_image_date: "2010-03-01T16:45:05"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26570 -->
Wer auf die Diagnosemöglichkeiten mit 75151 Adapterplatine nicht verzichten will, kann sich nun auch dieser Belegung der 28pol. Buchsenplatte bedienen.
Hier wird an der Stiftleiste des ROBO TX Controllers Kompatibilität zum Eingang der von Peter Damen für den ROBO TX Controller modifizierten Adapterplatine des ROBO IF hergestellt.
Die hier in der Kurzanleitung gegebenen Hinweise zur Verkabelung wären dann zwingend zu beachten. Eine Garantie aber kann nicht übernommen werden.

01.03.2010:
Zurück zum Beitrag unter http://www.ftcommunity.de/details.php?image_id=26558