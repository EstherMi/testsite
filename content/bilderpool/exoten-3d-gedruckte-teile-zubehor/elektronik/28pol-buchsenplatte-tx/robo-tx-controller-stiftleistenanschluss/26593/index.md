---
layout: "image"
title: "11 Stiftleiste, permanente externe Verkabelung"
date: "2010-03-04T21:23:29"
picture: "robotxstiftleistenanschluss3.jpg"
weight: "3"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/26593
imported:
- "2019"
_4images_image_id: "26593"
_4images_cat_id: "1893"
_4images_user_id: "723"
_4images_image_date: "2010-03-04T21:23:29"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26593 -->
Diese externe Verkabelung der Stiftleiste kann somit permanent am ROBO TX Controller verbleiben. Dass die max. 34 Flachstecker für einen Direktanschluss hier noch aufsetz- und ansteckbar sind ist auf den Fotos 09+10 erwiesen.
Interessant wäre noch zu wissen wie der ROBO TX Controller mit einer solchen externen Stiftleiste z.B. auf die Modelle der Computing-Baukästen "ROBO TX Training Lab" und ROBO TX Explorer" unter Modifizierung der Bauanleitungen zu setzen geht. Das aber sollte jeder selbst bewältigen können, der an der Anwendung meiner Vorschläge interessiert bleibt.