---
layout: "image"
title: "10 Flachsteckerbelegung kpl. 2"
date: "2010-03-04T21:23:28"
picture: "robotxstiftleistenanschluss2.jpg"
weight: "2"
konstrukteure: 
- "Udo2"
fotografen:
- "Udo2"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Udo2"
license: "unknown"
legacy_id:
- details/26592
imported:
- "2019"
_4images_image_id: "26592"
_4images_cat_id: "1893"
_4images_user_id: "723"
_4images_image_date: "2010-03-04T21:23:28"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=26592 -->
Anordnung wie unter Foto 09 mit zusätzlicher externer Komplettverkabelung der Stifleiste.
An der linken Seite des TX sind mal die Teile der schon beschriebenen Variante zur Aufnahme der Stiftleiste angesetzt.