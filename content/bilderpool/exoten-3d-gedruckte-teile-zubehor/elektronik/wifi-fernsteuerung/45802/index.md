---
layout: "image"
title: "Gehäuse ohne Photon"
date: "2017-05-08T18:14:36"
picture: "wfpp4.jpg"
weight: "5"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/45802
imported:
- "2019"
_4images_image_id: "45802"
_4images_cat_id: "3404"
_4images_user_id: "2228"
_4images_image_date: "2017-05-08T18:14:36"
_4images_image_order: "4"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45802 -->
Man sieht hier den Aufbau. Die Motortreiber nehmen den Mikrocontroller "Huckepack". Dass einzelne Teile höher als 15 mm sind, stört nicht weiter, da die Stecker ohnehin den Raum nach oben beanspruchen.