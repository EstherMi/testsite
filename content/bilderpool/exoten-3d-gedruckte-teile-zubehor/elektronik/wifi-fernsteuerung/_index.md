---
layout: "overview"
title: "Wifi Fernsteuerung"
date: 2019-12-17T18:03:08+01:00
legacy_id:
- categories/3404
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3404 --> 
Ziel dieses Projekts ist es, eine Smartphone Fernsteuerung für Fischertechnik zu bauen. Während der Support für iOS des neuen BT Control Sets noch etwas auf sich warten lässt, habe ich ein Wifi Board entworfen, dass Plattform übergreifend gesteuert werden kann. Herzstück ist der Particle Photon Wifi Mikrocontroller, der Fischertechnik Modelle fortan mit dem Internet der Dinge verbindet. Mittels einer Smartphone App und einem Server (da kann ein "öffentlicher" Blynkserver oder ein privater Server verwendet werde) verbinden sich Smartphone und Mikrocontroller. Die Reichweite ist folglich nur durch die Verfügbarkeit einer Internetverbindung begrenzt.