---
layout: "image"
title: "Wifi-Controller"
date: "2017-05-08T18:14:36"
picture: "wfpp1.jpg"
weight: "2"
konstrukteure: 
- "david"
fotografen:
- "david"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "david-ftc"
license: "unknown"
legacy_id:
- details/45799
imported:
- "2019"
_4images_image_id: "45799"
_4images_cat_id: "3404"
_4images_user_id: "2228"
_4images_image_date: "2017-05-08T18:14:36"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=45799 -->
Ziel dieses Projekts ist es, eine Smartphone Fernsteuerung für Fischertechnik zu bauen. Während der Support für iOS des neuen BT Control Sets noch etwas auf sich warten lässt, habe ich ein Wifi Board entworfen, dass Platform übergreifend gesteuert werden kann. Herzstück ist der Particle Photon Wifi Mikrocontroller, der Fischertechnik Modelle fortan mit dem Internet der Dinge verbindet. Mittels einer Smartphone App und einem Server (dazu kann ein "öffentlicher" Blynkserver oder ein privater Server verwendet werden) verbinden sich Smartphone und Mikrocontroller. Die Reichweite ist folglich nur durch die Verfügbarkeit einer Internetverbindung begrenzt.

Im Video werden die Funktionen des Controllers dargestellt: https://youtu.be/jBZQri6zQZs

Features:
- 3 Motorausgänge (1.2 Ampere)
- 4 GPIOs: z.B. für bis zu 4 Servomotoren (erlaubt verschiedene Lenkprogramme), Sensoreingänge, oder Kommunikation
- Verwendung wie ein herkömmlicher Empfänger im kompakten Gehäuse (60x30x15mm)