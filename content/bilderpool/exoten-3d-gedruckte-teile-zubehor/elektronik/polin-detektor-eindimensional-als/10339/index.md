---
layout: "image"
title: "Polin Detektor eindimensional als Positions-Abstandssensor"
date: "2007-05-07T08:09:49"
picture: "Detektor_2.jpg"
weight: "6"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- details/10339
imported:
- "2019"
_4images_image_id: "10339"
_4images_cat_id: "941"
_4images_user_id: "426"
_4images_image_date: "2007-05-07T08:09:49"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10339 -->
Kurzbeschreibung zum Detektor:

Positionsempfindlicher Detektor (PSD) mit 5x1 mm Sensorfläche. Er erlaubt die kontinuierliche Positionsbestimmung eines Lichtflecks entlang einer Achse.