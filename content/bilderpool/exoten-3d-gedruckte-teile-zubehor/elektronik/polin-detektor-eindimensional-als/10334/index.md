---
layout: "image"
title: "Polin Detektor eindimensional als Positions-Abstandssensor"
date: "2007-05-07T08:09:48"
picture: "Detektor.jpg"
weight: "1"
konstrukteure: 
- "Reiner Stähler"
fotografen:
- "Reiner Stähler"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Fischermän"
license: "unknown"
legacy_id:
- details/10334
imported:
- "2019"
_4images_image_id: "10334"
_4images_cat_id: "941"
_4images_user_id: "426"
_4images_image_date: "2007-05-07T08:09:48"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=10334 -->
Hier mal wieder eine Idee von mir um eine genauere Positions- b.z.w. Abstandsbestimmung zu realisieren! Der Detektor stammt von Pollin Elektronik (www.pollin.de Artikel-Nr.: 120404) dort sind auch genauere Daten über den Sensor vorhanden!
Funktioniert mit Fischertechnik Licht mit Störlichtkappe sowie mit Laser auf sehr große entfernung!!

P.S. Bei fragen einfach mailen!!
     Werde die Tage noch ein bischen 
     weiter Experimentieren!