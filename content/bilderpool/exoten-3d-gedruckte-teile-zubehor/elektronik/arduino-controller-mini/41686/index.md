---
layout: "image"
title: "arduino_minibot"
date: "2015-08-03T10:49:33"
picture: "minibots_arduino1.jpg"
weight: "4"
konstrukteure: 
- "ft/xbach"
fotografen:
- "xbach"
keywords: ["arduino", "minibots", "line", "follower"]
uploadBy: "xbach"
license: "unknown"
legacy_id:
- details/41686
imported:
- "2019"
_4images_image_id: "41686"
_4images_cat_id: "2677"
_4images_user_id: "427"
_4images_image_date: "2015-08-03T10:49:33"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=41686 -->
Der minibots-Baukasten ist  gut gelungen, aber ein Roboter muss frei programmierbar sein.
... hier eine Variante mit ARDUINO (NANO clone) und div. Sensoren.
- line sensor - Kompass - IR Empfänger - Piepser etc..