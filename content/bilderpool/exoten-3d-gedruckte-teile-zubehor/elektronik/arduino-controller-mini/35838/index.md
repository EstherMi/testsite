---
layout: "image"
title: "Controller Frontansicht"
date: "2012-10-07T23:20:05"
picture: "ftArduinoMiniFront.jpg"
weight: "7"
konstrukteure: 
- "Dirk Grebe"
fotografen:
- "Dirk Grebe"
keywords: ["Controller", "Elektronik", "Arduino", "Motor"]
uploadBy: "gravy"
license: "unknown"
legacy_id:
- details/35838
imported:
- "2019"
_4images_image_id: "35838"
_4images_cat_id: "2677"
_4images_user_id: "854"
_4images_image_date: "2012-10-07T23:20:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35838 -->
Frontansicht des Controllers
- DIP Switch kann 8 pol. sein, mit dem 4 pol. sind die TX und RX Anschlüsse zum Programmieren frei.