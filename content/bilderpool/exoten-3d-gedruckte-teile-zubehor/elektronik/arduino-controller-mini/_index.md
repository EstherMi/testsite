---
layout: "overview"
title: "Arduino Controller mini"
date: 2019-12-17T18:02:54+01:00
legacy_id:
- categories/2677
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2677 --> 
Motorcontroller basierend auf ATMega328 mit Arduino Bootloader im kleinen ft Batteriegehäuse.
- 2 Motorausgänge
- 2 PWM Ausgänge
- 6 Analog/ Digitaleingänge
- IR Erweiterung möglich