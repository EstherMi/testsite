---
layout: "image"
title: "Schaltplan"
date: "2012-10-07T23:20:05"
picture: "ftArduinoMiniSchematic.jpg"
weight: "6"
konstrukteure: 
- "Dirk Grebe"
fotografen:
- "Dirk Grebe"
keywords: ["Elektronik", "Arduino", "Controller", "Motor"]
uploadBy: "gravy"
license: "unknown"
legacy_id:
- details/35837
imported:
- "2019"
_4images_image_id: "35837"
_4images_cat_id: "2677"
_4images_user_id: "854"
_4images_image_date: "2012-10-07T23:20:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35837 -->
Schaltplan mit ATMega328 und L293 Motortreiber