---
layout: "image"
title: "Innenansicht des Aufbaus"
date: "2012-10-07T23:20:05"
picture: "ftArduinoMiniTop.jpg"
weight: "8"
konstrukteure: 
- "Dirk Grebe"
fotografen:
- "Dirk Grebe"
keywords: ["Controller", "Elektronik", "Arduino", "Motor"]
uploadBy: "gravy"
license: "unknown"
legacy_id:
- details/35839
imported:
- "2019"
_4images_image_id: "35839"
_4images_cat_id: "2677"
_4images_user_id: "854"
_4images_image_date: "2012-10-07T23:20:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=35839 -->
Innenansicht von oben

- Unter dem ATMega328 sitzt noch ein 16 MHz Quarz
- Der L293 wurde mit "begradigten" Anschlüssen hochkant eingesetzt um Platz für die Anschlüsse zu sparen