---
layout: "comment"
hidden: true
title: "19451"
date: "2014-08-28T14:35:37"
uploadBy:
- "bummtschick"
license: "unknown"
imported:
- "2019"
---
Wenn man das Abbiegen mit ner Flachzange macht, ist das Risiko überschaubar. Dann wird die Platine nicht verbogen. Und wenn ein Pin abbricht, lötet man halt nen neuen dran. Ein paar Pins musste ich auf die Art eh verlängern, damit sie bis zur Buchse reichten. Wie gesagt, alles nicht sehr professionell. :-)