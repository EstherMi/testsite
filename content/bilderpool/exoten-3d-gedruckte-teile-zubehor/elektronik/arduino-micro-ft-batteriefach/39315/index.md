---
layout: "image"
title: "05 Minimale Schaltung (5475)"
date: "2014-08-27T20:53:34"
picture: "05_Minimale_Schaltung_5475.jpg"
weight: "5"
konstrukteure: 
- "bummtschick"
fotografen:
- "bummtschick"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "bummtschick"
license: "unknown"
legacy_id:
- details/39315
imported:
- "2019"
_4images_image_id: "39315"
_4images_cat_id: "2943"
_4images_user_id: "2106"
_4images_image_date: "2014-08-27T20:53:34"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39315 -->
Das ist das typische Arduino-Minimal-Beispiel, mit dem man eine LED an Port 13 zum Blinken bringt -- siehe zB http://arduino.cc/en/tutorial/blink . Die Lampe ist eine LED im ft-Leuchtbaustein (gibt's mit eingelötetem Vorwiderstand beim FFM), eine normale ft-Lampe zöge zu viel Strom.

Dann: das Minimalprogramm in der Arduino-IDE geladen, kompiliert und über USB auf den Micro geschickt, Deckel zu, 9 Volt Spannungsversorgung an GND und VIN, die LED an GND und Pin 13. Blink!