---
layout: "image"
title: "Controller board"
date: "2018-01-07T14:27:05"
picture: "20160507_104321.jpg"
weight: "7"
konstrukteure: 
- "Rubem Pechansky"
fotografen:
- "Rubem Pechansky"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rubem"
license: "unknown"
legacy_id:
- details/47054
imported:
- "2019"
_4images_image_id: "47054"
_4images_cat_id: "2846"
_4images_user_id: "2128"
_4images_image_date: "2018-01-07T14:27:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47054 -->
Detail of the controller board showing the motor drivers and the small buzzer board. The board is designed as an Arduino shield. The fischertechnik connectors are on the solder side (not visible in this picture).