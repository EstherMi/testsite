---
layout: "overview"
title: "Custom Arduino interface"
date: 2019-12-17T18:03:01+01:00
legacy_id:
- categories/2846
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=2846 --> 
Homemade interface between Arduino and fischertechnik with custom hardware, low-level software and GUI (graphical user interface)