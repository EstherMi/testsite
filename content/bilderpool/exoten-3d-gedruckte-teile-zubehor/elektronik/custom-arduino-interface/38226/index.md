---
layout: "image"
title: "Detail of the turntable drive"
date: "2014-02-11T14:29:37"
picture: "20140206_135312_2.jpg"
weight: "9"
konstrukteure: 
- "Rubem Pechansky"
fotografen:
- "Rubem Pechansky"
keywords: ["Wormgear", "turntable", "pinion", "direct", "drive", "gearbox", "gear", "axle"]
uploadBy: "rubem"
license: "unknown"
legacy_id:
- details/38226
imported:
- "2019"
_4images_image_id: "38226"
_4images_cat_id: "2846"
_4images_user_id: "2128"
_4images_image_date: "2014-02-11T14:29:37"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38226 -->
This image shows how I'm driving the turntable. I've simply attached a Z10 pinion to the motor gearbox using a 40mm geared axle. Since I'm using a 6VDC power supply, the original drive (which uses a 35072 wormgear) is a bit slow.