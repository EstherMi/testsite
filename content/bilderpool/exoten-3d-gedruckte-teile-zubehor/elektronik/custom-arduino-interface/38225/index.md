---
layout: "image"
title: "The user interface"
date: "2014-02-10T22:32:19"
picture: "20140206_135734.jpg"
weight: "17"
konstrukteure: 
- "Rubem Pechansky"
fotografen:
- "Rubem Pechansky"
keywords: ["Interface", "Arduino", ".NET", "C#", "script", "language"]
uploadBy: "rubem"
license: "unknown"
legacy_id:
- details/38225
imported:
- "2019"
_4images_image_id: "38225"
_4images_cat_id: "2846"
_4images_user_id: "2128"
_4images_image_date: "2014-02-10T22:32:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38225 -->
Finally, here is the user interface running in my computer. I don't have much time to fiddle with microcontroller software anymore, so the Arduino sketch is pretty basic: it's essentially a dumb USB serial interface to the much more powerful .NET / C# software (depicted here) which does the actual work. I've used a simple script language that allows me to monitor the inputs (shown in blue) and control the motors (yellow). As you can see, this hardware has ten inputs and can control up to four motors, so I believe it's roughly equivalent to the old serial interface or the Robo interface. Of course neither fischertechnik nor Arduino allowed me to use their logos, but this is for personal use only so I believe they won't mind ;)