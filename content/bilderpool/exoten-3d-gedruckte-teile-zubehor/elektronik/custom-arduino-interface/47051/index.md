---
layout: "image"
title: "Controller board"
date: "2018-01-07T14:27:05"
picture: "20160507_104246.jpg"
weight: "4"
konstrukteure: 
- "Rubem Pechansky"
fotografen:
- "Rubem Pechansky"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rubem"
license: "unknown"
legacy_id:
- details/47051
imported:
- "2019"
_4images_image_id: "47051"
_4images_cat_id: "2846"
_4images_user_id: "2128"
_4images_image_date: "2018-01-07T14:27:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47051 -->
Close-up of the controller / connector board. The board was made with a standard perforated PCB. The connectors are made from inexpensive 2.5 mm jacks.