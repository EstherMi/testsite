---
layout: "image"
title: "Box lid with sockets"
date: "2014-02-10T22:32:19"
picture: "20140201_192148.jpg"
weight: "13"
konstrukteure: 
- "Rubem Pechansky"
fotografen:
- "Rubem Pechansky"
keywords: ["Box", "lid", "LEDs"]
uploadBy: "rubem"
license: "unknown"
legacy_id:
- details/38221
imported:
- "2019"
_4images_image_id: "38221"
_4images_cat_id: "2846"
_4images_user_id: "2128"
_4images_image_date: "2014-02-10T22:32:19"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=38221 -->
This pic shows the box lid with the sockets and the little reset button (upper left). The cutout originally had an LCD display but I gave up on it, so now I use it to check the I/O LEDs. In the end I replaced the dark red plastic window with a clear one.