---
layout: "image"
title: "Test model"
date: "2018-01-07T14:27:05"
picture: "20160415_233600.jpg"
weight: "8"
konstrukteure: 
- "Rubem Pechansky"
fotografen:
- "Rubem Pechansky"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rubem"
license: "unknown"
legacy_id:
- details/47055
imported:
- "2019"
_4images_image_id: "47055"
_4images_cat_id: "2846"
_4images_user_id: "2128"
_4images_image_date: "2018-01-07T14:27:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47055 -->
Test model showing the Arduino interface attached to a red base plate.