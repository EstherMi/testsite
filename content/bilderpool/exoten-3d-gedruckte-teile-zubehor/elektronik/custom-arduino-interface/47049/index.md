---
layout: "image"
title: "View from below"
date: "2018-01-07T14:27:05"
picture: "20160507_104724.jpg"
weight: "2"
konstrukteure: 
- "Rubem Pechansky"
fotografen:
- "Rubem Pechansky"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "rubem"
license: "unknown"
legacy_id:
- details/47049
imported:
- "2019"
_4images_image_id: "47049"
_4images_cat_id: "2846"
_4images_user_id: "2128"
_4images_image_date: "2018-01-07T14:27:05"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47049 -->
View from below. The two fischertechnik parts were fixed using 3M VHB tape.