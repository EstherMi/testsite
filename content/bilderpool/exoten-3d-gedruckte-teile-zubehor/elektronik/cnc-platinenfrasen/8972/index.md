---
layout: "image"
title: "Steuerung"
date: "2007-02-11T18:18:19"
picture: "cnc3.jpg"
weight: "3"
konstrukteure: 
- "sannchen90"
fotografen:
- "sannchen90"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Sannchen90"
license: "unknown"
legacy_id:
- details/8972
imported:
- "2019"
_4images_image_id: "8972"
_4images_cat_id: "816"
_4images_user_id: "6"
_4images_image_date: "2007-02-11T18:18:19"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=8972 -->
SMC 800 Steuerung. Leider ein wenig schwach, aber zjm Probieren und für kleinere Arbeiten reicht es.