---
layout: "image"
title: "xbach"
date: "2014-09-14T20:24:06"
picture: "ft_parInt2PICb.jpg"
weight: "3"
konstrukteure: 
- "xbach"
fotografen:
- "xbach"
keywords: ["Interface", "LiPo", "Akku"]
uploadBy: "xbach"
license: "unknown"
legacy_id:
- details/39360
imported:
- "2019"
_4images_image_id: "39360"
_4images_cat_id: "2949"
_4images_user_id: "427"
_4images_image_date: "2014-09-14T20:24:06"
_4images_image_order: "0"
---

<!-- https://www.ftcommunity.de/details.php?image_id=39360 -->
Da das Gehäuse (für heutige Verhältnisse) sehr großzügig dimensioniert ist, wurde die Stromversorgung mit eingebaut. Die LiPos sind kaum 3mm hoch und wurden als Restposten 
bei einem Versender besorgt. (ca. 5¤). Auch Handy Akkus nutzen oft diese Zellen. Für 8-jährige aber nicht zu empfehlen ;-).