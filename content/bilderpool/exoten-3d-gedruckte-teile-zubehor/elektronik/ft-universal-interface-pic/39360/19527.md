---
layout: "comment"
hidden: true
title: "19527"
date: "2014-09-25T11:29:33"
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
imported:
- "2019"
---
Nachtrag:
Welche Motortreiber (die ICs mit nur einer Reihe Pins) sind da drin?
Ich habe nämlich nur Schaltpläne der diskreten Varianten im Schrank.