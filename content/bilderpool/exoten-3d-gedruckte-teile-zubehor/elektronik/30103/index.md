---
layout: "image"
title: "Doppelrelaisbaustein"
date: "2011-02-23T21:00:31"
picture: "doppelrelais1.jpg"
weight: "5"
konstrukteure: 
- "TST"
fotografen:
- "TST"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "TST"
license: "unknown"
legacy_id:
- details/30103
imported:
- "2019"
_4images_image_id: "30103"
_4images_cat_id: "466"
_4images_user_id: "182"
_4images_image_date: "2011-02-23T21:00:31"
_4images_image_order: "1"
---

<!-- https://www.ftcommunity.de/details.php?image_id=30103 -->
Hier habe ich mir einen Doppelrelaisbaustein gemacht.
Auch im gelb/roten Design wie bei meinen Wandlern.
Klein, kompakt, ( back to the Basic :)) )