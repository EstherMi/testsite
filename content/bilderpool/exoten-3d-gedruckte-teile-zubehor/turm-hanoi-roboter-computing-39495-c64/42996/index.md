---
layout: "image"
title: "C64 + Fernseher + Bauanleitung"
date: "2016-03-06T19:14:38"
picture: "csteuerterinterfacenostalgieroboter3.jpg"
weight: "3"
konstrukteure: 
- "Lemkajen"
fotografen:
- "Lemkajen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/42996
imported:
- "2019"
_4images_image_id: "42996"
_4images_cat_id: "3198"
_4images_user_id: "1359"
_4images_image_date: "2016-03-06T19:14:38"
_4images_image_order: "3"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42996 -->
schön waren damals die Bauanleitungen & Beschreibungen zu den Kästen wirklich :-)