---
layout: "image"
title: "Basic / C64 auf dem Fernseher"
date: "2016-03-06T19:14:38"
picture: "csteuerterinterfacenostalgieroboter2.jpg"
weight: "2"
konstrukteure: 
- "Lemkajen"
fotografen:
- "Lemkajen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "lemkajen"
license: "unknown"
legacy_id:
- details/42995
imported:
- "2019"
_4images_image_id: "42995"
_4images_cat_id: "3198"
_4images_user_id: "1359"
_4images_image_date: "2016-03-06T19:14:38"
_4images_image_order: "2"
---

<!-- https://www.ftcommunity.de/details.php?image_id=42995 -->
Krass ist - man vergisst es wirklich nicht, wie das ging..