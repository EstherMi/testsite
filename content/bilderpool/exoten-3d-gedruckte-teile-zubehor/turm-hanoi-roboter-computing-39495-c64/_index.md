---
layout: "overview"
title: "Turm von Hanoi - Roboter Computing 39495 + c64 + Interface C64"
date: 2019-12-17T18:06:21+01:00
legacy_id:
- categories/3198
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3198 --> 
Es hat uns mal wieder gepackt und wir haben den guten alten c64 angeschmissen und mal den Türme von Hanoi - Roboter aus dem Computing-Kasten gebaut ..