+++
layout = "overview"
title = "Fotosammlung"
weight = 1
+++

In der Fotosammlung gibt es unzählige Bilder von fischertechnik-Modellen, und
ständig kommen neue Bilder hinzu.

Für ein bißchen Ordnung im Chaos haben unsere Nutzer und wir versucht die
Bilderflut in Alben und eine halbwegs brauchbare Kategorisierung aufzuteilen.
Schau einfach mal selbst was es alles gibt, die jüngsten 4 Einträge sind
jeweils mit Bildern hervorgehoben.

<small>
<u>Kleine Anleitung</u>  
Ein Klick aufs thumbnail (das Minibild) führt _immer direkt zu diesem Bild_,
ein Klick auf einen Textlink führt entweder auf das Album oder in die
Galerieübersicht. Das Icon hilft bei der Unterscheidung:  
<i class="fas fa-book"></i> = ein Album mit Bilderserien und / oder weiteren
Alben  
<i class="far fa-images"></i> = eine Bilderserie ("Galerie")  
<i class="fas fa-camera"></i> = Anzahl der vorhandenen Bilder
in einer Galerie  
</small>

<!-- <i class="fas fa-camera-retro"></i> sieht in klein leider eher nach
Waschmaschine aus :( -->
