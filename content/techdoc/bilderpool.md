---
title: "Bilderpool"
---
Im Bilderpool gibt es drei Arten von Seiten: 
_Bilder_, _Bilderserien_ und _Übersichtsseiten_.

## Bilder 

Zu einem Bild gehören im Dateisystem zwei Dateien: 
Einmal das eigentliche Bild (bei Fotos typischerweise als JPEG), 
und zum anderen einer Markdown-Datei mit den zugehörigen Daten. 

Die Markdown-Datei enthält die meisten Daten im "Frontmatter"-Abschnitt.
Neben den üblichen Feldern wie `date`, `title`, `weight` gibt es die 
folgenden speziellen Felder und Bedeutungen:

* `layout`: Muss den Wert "image" haben
* `picture`: Der Dateiname des eigentlichen Bilds
* `konstrukteure`: Namen der Konstrukteure des gezeigten Modells (es können mehrere Konstrukteure angegeben werden)
* `fotografen`: Name der Fotografen (es können mehrere Fotografen angegeben werden)
* `uploadBy`: Benutzername desjenigen, der das Bild hochgeladen hat 
(dieses Feld gibt es im wesentlichen für die Kompatibilität mit dem 
alten Bilderpool, eventuell fällt es später auch weg)
* `license`: Lizenz, unter der das Bild verwendet werden darf

Der eigentliche Markdown-Text enthält die Bildbeschreibung.

##### Beispiel
```none
{{% include file="/bilderpool/unsere-modelle/keksdrucker/46531/index.md" %}}
```

Die Anzeige eines Bilds auf der Website besteht aus dem Titel als Überschrift, 
gefolgt vom Bild (mit den Namen von Konstrukteur und Fotograf als Bildunterschrift),
und darunter dann die Bildbeschreibung.

## Bilderserien

Eine Bilderserie ist eine Gruppe zusammengehörige Bilder, 
optional mit einem einleitenden Text. 

Im Dateisystem ist eine Bilderserie ein Verzeichnis, 
das ein ein oder mehrere Bilder enthält,
plus die spezielle Datei `_index.html`, 
die den Titel der Bilderserie 
und optional den Beschreibungstext enthält.

Das Layout für `_index.html` in einer Bilderserie muss "image-collection" sein.

##### Beispiel
```none
{{% include file="/bilderpool/unsere-modelle/keksdrucker/46531/index.md" %}}
```

Auf der Website wird eine Bilderserie unterschiedlich dargestellt 
je nachdem wieviele Bilder sie enthält:

* Eine Bilderserie mit einem einzigen Bild 
wird genau so dargestellt wie das Einzelbild selbst.
In diesem Fall ist der Titel der Bilderserie nur in der Navigation sichtbar,
und der Beschreibungstext wird überhaupt nicht angezeigt.
* Eine Bilderserie mit mehreren Bilder zeigt den Titel als Überschrift,
dann den Beschreibungstext der Serie,
und danach eine Vorschau-Galerie aller enthaltenen Bilder 
(die Sortierung kann wie in Hugo üblich per `weight` 
bei den Bildern selbst vorgegeben werden). 
Als Bildunterschrift in der Vorschau wird der Titel des Bilds verwendet.

Die Bilder in der Vorschau werden automatisch auf ein quadratisches Format zugeschnitten
und auf die Bildmitte zentriert.

## Übersichtsseiten

Übersichtsseiten sind im Dateisystem alle Verzeichnisse, 
die andere Übersichtsseiten oder Bilderserien enthalten.
Übersichtsseiten können eine `_index.html`-Datei enthalten
die Titel, einleitenden Text und Sortierreihenfolge (für die Seitennavigation) enthält.

##### Beispiel
```none
{{% include file="/bilderpool/unsere-modelle/keksdrucker/46531/index.md" %}}
```

Angezeigt wird für eine Übersichtsseite der Titel als Überschrift,
dann der Text aus `_index.html`, und danach für jedes Unterverzeichnis
eine Auswahl von maximal 8 Bildern aus der entsprechenden Unterkategorie
oder Bilderserie. Die Bilderauswahl ist nach Datum sortiert, 
mit den neuesten Einträgen zuerst.

Die Darstellung unterscheidet sich je nachdem ob das jeweilige Unterverzeichnis
eine Bilderserie oder wieder eine Übersichtsseite enthält:

* Für eine Bilderserie werden die bis zu 8 neuesten Bilder gezeigt.
* Für eine Übersichtseite wird jeweils das erste Bild 
der bis zu 8 neuesten Bilderserien gezeigt.
Das Datum der Bilderserie wird dabei 
durch das Datum des neuesten Bilds der jeweiligen Serie bestimmt.

Die Bilder sind jeweils mit Links hinterlegt,
die direkt zum jeweiligen Bild (für direkt angezeite Bilderserien)
bzw. zur Bilderserie (für Bilderserien in Unterkategorien) führt.

Die gewählte Art der Darstellung für Unter-Übersichtsseiten sorgt dafür, 
dass eine neu hochgeladene Bilderseite mit vielen Bilder 
zwar prominent an der ersten Stelle dargestellt wird,
aber trotzdem nicht die gesamte Übersicht "überflutet".
