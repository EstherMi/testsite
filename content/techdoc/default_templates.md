---
title: "Default-Templates"
---



## Globale Templates

Templates (= Schablonen) sind oft für die ganze Seite gültig.
Derart globale Templates erwartet hugo im Verzeichnis `/layouts/_default`.

Aktuell haben wir hier

*  baseof.html
*  [file.html](/techdoc/knowhow/#download-datei-seitenansicht-file-html)
*  list.html
*  single.html

Außerdem gibt es direkt in `layouts` das Template [index.html](/techdoc/home_index),
das für das Layout der Startseite verantwortlich ist.

## Partials

Partials sind so eine Art Unterprogramm, oder hier wohl eher Code-Fragmente,
die nutzbringend an mehreren Stellen eingesetzt werden können.
Partials stehen grundsätzlich im Verzeichnis `/layouts/partials`.

Einige davon kommen schon mit hugo, andere haben wir selbst geschrieben

*  bilderpool-thumbnail.html
*  bilderpool-thumbnail-generic.html
*  custom-footer.html
*  custom-header.html
*  [download-icon.html](/techdoc/partials/doku_download-icon/)
*  [download-size.html](/techdoc/partials/doku_download-size/)
*  favicon.html
*  get_csv.html
*  logo.html
*  menu.html
*  menu-footer.html
*  meta.html
*  navigation.html
*  search.html
*  toc.html
*  topbar.html
