---
title: "Script - ftptoc.html"
---

Das Script findet sich im Repo der Seite als `layouts/ftpedia/ftptoc.html`.
Es beginnt zunächst mit etwas Javascript.
Diese Zeilen ermöglichen dem späteren Besucher die Spalten der Tabelle nach
seinen Wünschen zu sortieren.

Nach dem Javascript Teil kommt zunächst etwas 'Kopf' für den einheitlichen
Seitenaufbau.
````html
<div class="padding highlightable"  style="overflow: scroll;">
   {{ partial "topbar.html" . }}
<div id="body-inner">
````
Es folgen noch der Seitentitel und ein paar begleitende Worte aus dem
Frontmatter sowie dem Inhalt der entsprechenden markdown-Datei.
````html
<h2>{{.Title}}</h2>
{{ .Content }}
````
Danach geht es auch schon los - die Tabelle mit dem Gesamtinhaltsverzeichnis
wird gebaut.
````html
<table>
   {{ $data := (getCSV ";" "content/ftpedia/ftPedia_Artikeluebersicht.csv") }}
````
Dazu wird zunächst der Inhalt der Datei nach `$data` eingelesen.
Der Name und der Ablageort dieser Datei ist im Script fest vorgegeben, s. o.

Die erste Zeile der .csv enthält die Überschriften für die Spalten.
Prinzipiell ist die Bedeutung im Script festgelegt, aber die Texte werden aus
der Datei geholt; das eröffnet ein wenig Flexibilität für Änderungen an den
Texten selbst.
Der Tabellenkopf mit 6 Spalten wird aus dieser ersten Zeile des .csv wie folgt
gebaut:
````html
<thead>
   <tr>
      {{ $header := first 1 $data }}
      {{ range $row := $header }}
         <th style="cursor:n-resize">{{ index $row 0 }}<!--<i class="fas fa-arrows-alt-v"></i>--></th>
         <th style="cursor:n-resize">{{ index $row 1 }}</th>
         <th style="cursor:n-resize">{{ index $row 2 }}</th>
         <th style="cursor:n-resize">{{ index $row 3 }}</th>
         <th style="cursor:n-resize">{{ index $row 4 }}</th>
         <th style="cursor:n-resize">{{ index $row 5 }}</th>
      {{end}}
   </tr>
</thead>
````
Dabei ist ein Symbol für die Sortierei vorbereitet, aber als html-Kommentar
derzeit nicht wirksam.
Die `style`-Definition bewirkt, dass der Mauszeiger seine Gestalt ändert wenn
er sich über einem Feld der Kopfzeile befindet.

Zeilenweise wird nun der Inhalt der Tabelle, also alles nach der ersten Zeile
aufgebaut.
````html
<tbody>
   {{ $body := after 1 $data }}
   {{ range $row := $body }}
   <tr>
      <td>
         {{/* index $row 0    enthält die Ausgabe im Stile '2018-3'. */}}
         {{/* index $row 3    enthält den Titel des Artikels. */}}
         {{/* index $row 4    enthält die Seitenzahlen im Stile '5-33'. */}}
         {{ $Ausgabe := index $row 0 }}
         {{ $Jahr := delimit (findRE "^[0-9]*" $Ausgabe) " " }}
         {{ $Seite := delimit (findRE "^[0-9]*" (index $row 4)) " " }}
         <a
            href = "{{- $.Site.BaseURL -}}ftpedia/{{- $Jahr -}}/{{- $Ausgabe -}}/ftpedia-{{- $Ausgabe -}}.pdf#page={{- $Seite -}}">
            {{ $Ausgabe }}
         </a>
      </td>
      <td>{{ index $row 1 }}</td>
      <td>{{ index $row 2 }}</td>
      <td>{{ index $row 3 }}</td>
      <td>{{ index $row 4 }}</td>
      <td>{{ index $row 5 }}</td>
   </tr>
   {{ end }}
</tbody>
````
Aus den vorgefundenen Daten wird von Spalte 0 ("Null") die Ausgabe (also die
Heftausgabe) an die Variable `$Ausgabe` zugewiesen; das macht die folgenden
Zeilen etwas übersichtlicher.
Per _Regular Expression_ wird aus `$Ausgabe` das Erscheinungsjahr nach `$Jahr`
extrahiert.
````html
{{ $Ausgabe := index $row 0 }}
{{ $Jahr := delimit (findRE "^[0-9]*" $Ausgabe) " " }}
````
`delimit` bereinigt die Rückgabe von `findRE` um unerwünschte Artefakte.
Das ist hugo geschuldet und es geht nicht ohne _(oder kennt da wer den Kniff?)_.

Spalte 4 der Originaldaten enthält die Seitenzahlen in der Anordnung von 'von'
bis 'bis'.
Dazwischen steht bislang ein 'En-Dash'.
Dank der Extraktion per _Regular Expression_ ist das Trennzeichen unerheblich
und wir erhalten die Startseite des Artikels in `$Seite`.
````html
{{ $Seite := delimit (findRE "^[0-9]*" (index $row 4)) " " }}
````

Aus all diesen Informationen kann nun der Link mitsamt Seitenzahl auf die
Ausgabe gelegt werden.
````html
<a
   href = "{{- $.Site.BaseURL -}}ftpedia/{{- $Jahr -}}/{{- $Ausgabe -}}/ftpedia-{{- $Ausgabe -}}.pdf#page={{- $Seite -}}">
   {{ $Ausgabe }}
</a>
````

Die restlichen Spalten werden 1:1 aus dem csv übernommen und ins html
eingesetzt.
Hier sind keinerlei Steuersequenzen enthalten, was die Sache deutlich
vereinfacht.
````html
<td>{{ index $row 1 }}</td>
<td>{{ index $row 2 }}</td>
<td>{{ index $row 3 }}</td>
<td>{{ index $row 4 }}</td>
<td>{{ index $row 5 }}</td>
````
Nach der letzten Zeile wird die Tabelle abgeschlossen.
````html
         {{ end }}
      </tbody>
   </table>
</div>
</div>
````

Einheitliche Fußzeilen (_footer_) sind bislang nicht enthalten!
Diese müssen im Script bei Bedarf noch ergänzt werden,
und natürlich auch dokumentiert.

{{% notice note %}}
Der oben besagte 'En-Dash' hält die beiden Zifferngruppen immer auf einer
Zeile zusammen.
Genau deswegen wird der als Trennzeichen verwendet.
Nach Absprache mit der ft:pedia-Redaktion (die uns zukünftig die .csv liefert)
ist es eventuell nötig den 'En-Dash' per Script einzubauen; anstelle des
ursprünglichen Trennzeichens.
Um entsprechende Dokumentation wird gebeten!

Im Unterschied dazu ist die Angabe zur "Ausgabe" mit dem ordinären "Minus"
versehen.
Das hilft hier enorm weil die Pfad- und Dateinamen das exakt gleiche Minus
verwenden.
Sollte das allerdings eines Tages mal geändert werden, ist das Script
unbedingt anzupassen.
{{% /notice %}}

---

Stand: 8. Juni 2019
