---
title: "Script - ftp-extras.html"
---

Das Script findet sich im Repo der Seite als `layouts/ftpedia/ftp-extras.html`.
Sein Zweck ist es vollautomatisch aus den Download-Dateien zu den diversen
ft:pedia-Ausgaben eine Übersichtsseite aufzubauen. Es wird tritt in Aktion
wenn im Frontmatter das `layout: ftp-extras` eingetragen ist. Das sollte nur
einmal auf der ganzen Site vorkommen.

Die Seite beginnt mit dem üblichen Intro und einer Fehlerbehandlung falls mal
der Titel abhanden kommen sollte.
````html
{{ define "main" }}

{{ if not .Title }}
   {{ errorf "Oh oh, der Seitentitel 'title' fehlt oder ist leer in Seite %q" .Path }}
{{ end }}
````

Danach kommt etwas Javascript.
````html
<script language="javascript" type="text/javascript">
   <!--
   ...
   -->
</script>

````
Diese Zeilen ermöglichen dem späteren Besucher die Spalten der Tabelle nach
seinen Wünschen zu sortieren. Dabei ist die Spalte mit den Download-Links
von der Sortierung ausgeschlossen.

`$date_format_string := "02.01.2006"` sorgt für eine angenehme Darstellung der
Datümer.

Die Seite bekommt ihren Rahmen, die Brotkrumen, den inneren Bereich und ihre
Überschrift aus dem Titel. Dazu kommt noch der Text (`.Content`).
````html
   <div class="padding highlightable"  style="overflow: scroll;">
      {{ partial "topbar.html" . }}
      <div id="body-inner">
         <h2>{{.Title}}</h2>
         {{ .Content }}
````

Sofort im Anschluß ist die Tabelle mit den Dateien angeordnet. Sie bekommt
fünf Spalten ('Jahr', 'Heft', 'Thema', 'Download-Link' und 'Autor(en)').
Ausser der Spalte 'Download-Link', dargestellt durch ein Icon, können alle
Spalten aufsteigend oder absteigend sortiert werden. Der Cursor zeigt das
optisch durch ein anderes Symbol an (`n-resize`).
````html
         <table>
            <thead>
               <tr>
                  <th style = "cursor: n-resize; text-align: center;">Jahr</th>
                  <th style = "cursor: n-resize; text-align: center;">Heft</th>
                  <th style = "cursor: n-resize">Thema</th>
                  <th style = "text-align: center;"><i class="fas fa-download"></i></th>
                  <th style = "cursor: n-resize">Autor(en)</th>
               </tr>
            </thead>
````
Ausgehend vom _section_ Wurzelknoten (`with .Parent`) sucht das Script durch
alle Jahrgänge und dort durch alle Einzelausgaben hinweg. Das sind die beiden
`range .Sections.ByTitle.Reverse` Zeilen.

````html
            <tbody>
               {{ with .Parent }}
                  {{ range .Sections.ByTitle.Reverse }}
                     {{ range .Sections.ByTitle.Reverse }}
````
Nur Download-Dateien zu einer veröffentlichten Ausgabe sind zugelassen. Was
bei einem Teaser steht, muss ignoriert werden. `if eq .Layout "issue"` kümmert
sich genau darum.
````html
                        {{ if eq .Layout "issue" }}
                           {{ $AusgabeJahr := delimit (findRE "[0-9]*$" .Title) " " }}
                           {{ $AusgabeNummer := delimit (findRE "^[0-9]*" .Title) " " }}
                           {{ range .Pages.ByDate.Reverse }}
````
Aus dem Hefttitel holen zwei `findRE` das Jahr und die Heftnummer heraus.
Danach schaut `range .Pages.ByDate.Reverse` sich die 'Extras' an. Für jedes
Extra gibt es eine Zeile in der Tabelle (`<tr>`). In den ersten beiden Spalten
werden Ausgabe (Jahr) und Heftnummer im Jahr eingetragen. Die dritte Spalte
bekommt den Link zur Downloadseite basierend auf dem Titel des 'Extras'.
````html
                              <tr>
                                 <td>{{ $AusgabeJahr}}</td>
                                 <td>{{ $AusgabeNummer }}</td>
                                 <td>
                                    <a href = "{{ .RelPermalink }}">
                                       {{ .Title }}
                                    </a>
                                 </td>
````
Spalte vier bekommt ein Icon für die Download-Datei, den Hyperlink zum
direkten Download und eine Angabe zur Größe. Dabei helfen bereits vorhandene
_partials_ ungemein.
````html
                                 <td style = "text-align: center;">
                                    <a style = "display: block;"
                                       href="{{.Parent.RelPermalink}}{{.Params.File}}">
                                       {{ partial "download-icon.html" . }}
                                    </a>
                                    {{ partial "download-size.html" . }}
                                 </td>
````
Die fünfte Spalte erhält noch eine Auskunft über die Autoren des 'Extras,
ebenfalls durch ein _partial_ aus dem Fundus. Danach endet die Tabellenzeile
per `</tr>`.
````html
                                 <td>{{ partial "authors-list.html" . }}</td>
                              </tr>
                           {{ end }}
                        {{ end }}
                     {{ end }}
                  {{ end }}
               {{ end }}
            </tbody>
         </table>
      </div>
   </div>
{{ end }}
````
Weitere Seiteninhalte gibt es nicht, so dass der Reihe nach die
Schleifenkonstrukte, die Tabelle sowie die beiden Darstellungsbereiche
geschlossen werden.

Eine Navigation ist bislang weder vorgesehen noch nötig.

---

Stand: 28. Oktober 2019
