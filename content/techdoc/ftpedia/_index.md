---
title: "ft:pedia"
---

In der _section_ "ft:pedia" werden die Inhalte unserer geliebten
Fachzeitschrift bereitgestellt. Dabei wird im Repo in zwei Bereiche
unterschieden:

*  Inhalte
*  Technik

Die Seiten der _section_ "ft:pedia" werden durch einige Scripte (Technik) aus
den vorgefundenen Inhalten gebaut.

Ausser der _section_ und ihrer Hauptseite gibt es noch die Untersektionen
'Jahrgang' und 'Ausgabe'.

Bevor jetzt die jeweilige Bechreibung kommt, sollten noch ein paar
Feinheiten erwähnt werden:

*  In jedem Ordner der Inhalte gibt es eine Datei `_index.md`.
*  hugo wendet das Script
   [`themes/website-layout/layouts/ftpedia/list.html`](ftpedia-list-html)
   auf jedes vorhandene `_index.md` in und unterhalb `content/ftpedia/` an.
*  Sowohl die Übersichtsseite als auch jeder 'Jahrgang' und jede 'Ausgabe'
   werden durch jeweils ein eigenes `_index.md` beschrieben.
*  Alle Seitentypen in der _section_ `ftpedia/` müssen vom
   [`themes/website-layout/layouts/ftpedia/list.html`](ftpedia-list-html)
   Script unterschieden und entsprechend behandelt werden.
*  hugo wendet das Script
   [`themes/website-layout/layouts/ftpedia/ftptoc.html`](ftpedia-ftptoc-html)
   auf jede andere `.md`-Datei der _section_ `ftpedia/` an wenn deren
   `layout: "ftptoc"` ausgewählt ist.
   Für unsere Zwecke gibt es genau eine Datei mit diesem Attribut.
*  hugo wendet das Script
   [`themes/website-layout/layouts/ftpedia/ftp-extras.html`](ftpedia-ftp-extras-html)
   auf jede andere `.md`-Datei der _section_ `ftpedia/` an wenn deren
   `layout: "ftp-extras"` ausgewählt ist.
   Für unsere Zwecke gibt es genau eine Datei mit diesem Attribut.
*  hugo wendet das Script
   [`themes/website-layout/layouts/_default/file.html`](../knowhow/#download-datei-seitenansicht-file-html)
   auf jede andere `.md`-Datei (in jeder _Section_ übrigens) an wenn deren
   `layout: "file"` lautet; jedoch nicht auf Dateien mit Namen `_index.md`.
*  Andere Layout-Typen werden nicht dargestellt. Dafür muss bei Bedarf das
   jeweilige Script ergänzt oder ein Zusätzliches geschrieben werden.
*  Beim Erstellen (Neuanlegen) von Inhalten sind _Archetype_ behilflich.

Hier geht es zu den Dokus:

*  Inhalte
   -  [Doku zu `content/ftpedia/`](ftpedia/)
*  Technik
   -  [Doku zu `themes/website-layout/layouts/ftpedia/list.html`](ftpedia-list-html/)
   -  [Doku zu `themes/website-layout/layouts/ftpedia/ftptoc.html`](ftpedia-ftptoc-html/)
   -  [Doku zu `themes/website-layout/layouts/ftpedia/ftp-extras.html`](ftpedia-ftp-extras-html/)
   -  [Doku zu `themes/website-layout/layouts/_default/file.html`](../knowhow/#download-datei)
   -  [Doku zu `themes/website-layout/archetypes/ftpedia.md`](../archetypes/#archetype-ftpedia-md)
   -  [Doku zu `themes/website-layout/archetypes/teaser.md`](../archetypes/#archetype-teaser-md)

---

Stand: 28. Oktober 2019
