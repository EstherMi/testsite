---
title: "Website (Technik)"
weight: "99"
---
Hier findet ihr Informationen darüber, wie diese Website selbst aufgebaut ist:
Welche Technik wird verwendet um die Inhalte zu organisieren und darzustellen,
was haben wir uns für die Darstellung der einzelnen Bereiche gedacht usw.

{{% notice info %}}
Dieser Bereich ist in erster Linie für diejenigen gedacht, 
die beim Aufbau und der Pflege der ftc-Website helfen wollen.
{{% /notice %}}

Die eigentlichen Inhalte für die Website liegen als [Markdown]-Dateien vor.
Diese Dateien werden mit [Hugo] nach HTML übersetzt. 

Die weiteren Abschnitte in diesem Bereich setzen voraus, dass ihr bereits 
Grundkenntnisse über Markdown und Hugo habt.

## Generelles zum Layout
Die Templates und sonstigen Definitionen, 
die das genau Aussehen der Website bestimmen, 
haben wir von einem vorgefertigten "Theme" für Hugo 
-- dem ["Learn"-Theme](https://github.com/matcornic/hugo-theme-learn/) --
übernommen, hier aber der Einfachheit halber nicht als Theme sondern direkt verwendet.
Trotzdem bietet die [Dokumentation zum "Learn"-Theme](https://learn.netlify.com/en/) 
einen guten Einsteig in die Feinheiten des Layouts.

### Unterschiede zum "Learn"-Theme

* Das Learn-Theme nutzt für den "Rahmen" der Seite die beiden Partials
`partials/header.html` und `partials/footer.html` 
wie [hier](https://gohugo.io/templates/partials/#example-header-html) beschrieben.
Wir nutzen stattdessen die modernere Variante 
mit [Base-Templates und Blocks](https://gohugo.io/templates/base/).

* Im Learn-Theme ist das HTML für die Kopfleiste der Seite 
und die Navigation zur nächsten/vorigen Seite
hart in `partials/header.html` kodiert.
Bei uns sind diese Bereiche nach 
`partials/topbar.html` und `partials/navigation.html`
ausgelagert.
Für die ft:pedia haben wir eine eigene, angepasste, Navigation. Diese ist in
den Seitenbau-scripten für die _section_ ft:pedia enthalten.

---

Stand: 26. Oktober 2019

[Markdown]: https://de.wikipedia.org/wiki/Markdown
[Hugo]: https://gohugo.io/

