---
title: "ftc Stylesheet"
---



Außer den Templates zur Darstellung (z. B.: list.html, file.html, usw. ...)
bestimmen auch noch stylesheets (.css) das Aussehen der Seite.
Genauer gesagt, beschreiben wir in den `.html` die Anordnung der
Seitenelemente und bestimmte Formate, die vom Standard abweichen.
Das geschieht zum Teil per lokaler Style-Definition.

Standardformate kommen allerdings global für die ganze Seite aus 
Cascaded Style Sheets (.css).
Diese Style Sheets definieren, welcher Font benutzt wird, wie Hyperlinks
angezeigt werden, welche Farben das Menü (Seitenleiste) hat und ganz viel mehr.

Mit dem hugo Learn Theme kommen bereits viele Style Sheets für dessen
Beschreibung.
Sie finden sich alle in `/static/css`.
Nun wäre es sicher sehr mühsam diese vielen Style Sheets zu sichten, um eine
bestimmte Einstellung zu finden und zu ändern.
Das ist aber gar nicht nötig.
Es gibt eine bestimmte Hierarchie und das zuletzt geladene Style Sheet wird
zuerst verwendet.
Alles was dort nicht definiert ist, wird aus dem vorgelagerten Style Sheet
übernommen und so fort bis zurück zur Wurzel.
Auf diese Art ist sichergestellt, dass es wenigstens für alle wichtigen
Dinge eine Voreinstellung gibt.

Die speziellen Anpassungen für unsere Seite, ftcommunity.de, sind sinnigerweise
im Style Sheet `theme-ftcommunity.css` zusammengefasst.
Der Name richtet sich nach einer hugo Konvention und startet immer mit
`theme-`.
Dieses Style Sheet wird aber erst für den Seitenaufbau wirksam, wenn es auch
in der Datei `config.toml` eingetragen wird.
Das macht die Zeile `themeVariant = "ftcommunity"` innerhalb der Sektion
`[params]`.
````toml
[params]
  disableInlineCopyToClipBoard = true
  disableLanguageSwitchingButton = true
  themeVariant = "ftcommunity"
````
Ohne diesen Eintrag ist unser Style Sheet unwirksam.

Wie schon erwähnt werden alle Spezialitäten unserer Seite, die nicht dem
entsprechen, was das hugo Learn Theme mitbringt, im `theme-ftcommunity.css`
definiert.
Andere Style Sheets lassen wir tunlichst in Ruhe.
Für irgendwelche Syntaxdinge kann man dort aber abgucken.
Begleitend schaut man sich die Doku zu CSS bei
[w3schools.com](https://www.w3schools.com/css/default.asp)
an.
Generell empfiehlt sich ein Besuch dieser Seite, es gibt eine Suchfunktion,
mit der sich die jweiligen Stichworte dort finden lassen.
Leider ist die w3school-Suche werbeverseucht und man muss meist etwas
abwärtsscrollen um die gefundenen Inhalte zu finden.

Aufgrund des Wunsches einiger ftc-Freunde soll die Seite mit einem gut lesbaren
Font und maximalem Farbkontrast ausgestattet sein.
Diese Anforderung bildet den Leitgedanken.
Das Theme von hugo kann hier leider nicht punkten, es verwendet einen sehr
schlanken Font mit einem dunklen Grauton.

Darauf abgestimmt sollen auch andere benutzte Elemente farblich angepasst
werden.
Daher startet die 'Litanei' in `theme-ftcommunity.css` mit einer ganzen
Reihe von Farbdefinitionen:
````css
:root{
    --MAIN-TEXT-color:                       Black;      /* Color of text */
    --MAIN-TEXT-BG-color:                    White;      /* Color of text background */
    --MAIN-TITLES-TEXT-color:                Black;      /* Color of titles h2-h3-h4-h5 */
    --MAIN-LINK-color:                       Blue;       /* Color of links */
    --MAIN-LINK-HOVER-color:                 DarkBlue;   /* Color of hovered links */
    --MAIN-LINK-FTPEDIASITE-color:           Black;      /* Color of links in ft:pedia section */
    --MAIN-LINK-FTPEDIASITE-HOVER-color:     Gray;       /* Color of hovered links in ft:pedia section */
    --MAIN-ANCHOR-color:                     Blue;       /* Color of anchors on titles */
    --MAIN-HR-color:                         LightGray;  /* Color of horizontal ruler */

    --MENU-HEADER-BG-color:                  #252c31;    /* Background color of menu header */
    --MENU-HEADER-BORDER-color:              #33a1ff;    /* Color of menu header border */ 

    --MENU-SEARCH-BOX-color:                 #252c31;    /* Override search field border color */
    --MENU-SEARCH-BOX-BG-color:              #102024;    /* Search field background color (by default borders + icons) */
    --MENU-SEARCH-BOX-ICONS-color:           Gray;       /* Override search field icons color */

    --MENU-SECTIONS-ACTIVE-BG-color:         #20272b;    /* Background color of the active section and its childs */
    --MENU-SECTIONS-BG-color:                #252c31;    /* Background color of other sections */
    --MENU-SECTIONS-LINK-color:              White;      /* Color of link text in menu */
    --MENU-SECTIONS-LINK-HOVER-color:        White;      /* Color of link text in menu, when hovered */
    --MENU-SECTIONS-LINK-HOVER-BG-color:     DarkBlue;   /* Background color of link in text menu, when hovered */
    --MENU-SECTION-ACTIVE-CATEGORY-color:    White;      /* Color of active category text */
    --MENU-SECTION-ACTIVE-CATEGORY-BG-color: Blue;       /* Color of background for the active category (only) */

    --MENU-VISITED-color:                    #33a1ff;    /* Color of 'page visited' icons in menu */
    --MENU-SECTION-HR-color:                 #20272b;    /* Color of <hr> separator in menu */
}
````
(Hier wird vom admin eine gewisse Kenntnis englischsprachiger Fachausdrücke
erwartet, die Angaben sind im Code bereits deutlich dokumentiert.)

Soll eine der angegebenen Farben geändert werden, ist genau hier die jeweilige
Stellschraube zu finden.
Viele Farben haben Namen und eine Beschreibung, die keine Wünsche offen lassen
sollte, findet sich ebenfalls bei
[w3school.com](https://www.w3schools.com/colors/default.asp).

Für den Fall, dass eine neue Stellschraube benötigt wird (z. B.
Tabellenkopfzeilenhintergrund), empfiehlt es sich die Liste der Definitionen
zu ergänzen.

Nachdem nun die Farben ausgewählt sind, müssen diese und weitere Eigenschaften
verschiedenster Seitenelemente eingstellt werden.
Wir starten mit dem Hauptteil der Seite, dem _body_:
````css
body {
    color: var(--MAIN-TEXT-color) !important;
    background-color: var(--MAIN-TEXT-BG-color) !important;
    font-weight: 400 !important; /* Die originalen 300 sind einfach zu mager */
}


textarea:focus, input[type="email"]:focus, input[type="number"]:focus, input[type="password"]:focus, input[type="search"]:focus, input[type="tel"]:focus, input[type="text"]:focus, input[type="url"]:focus, input[type="color"]:focus, input[type="date"]:focus, input[type="datetime"]:focus, input[type="datetime-local"]:focus, input[type="month"]:focus, input[type="time"]:focus, input[type="week"]:focus, select[multiple=multiple]:focus {
    border-color: none;
    box-shadow: none;
}
````
Die Angabe `body` stellt global für alle Texte sowohl die gewünschte
Textfarbe (momentan schwarz) und die Hintergrundfarbe (derzeit weiß) ein.
Außerdem wird der Font mittels `font-weight` nicht so augenreizend schlank
dargestellt.
An dieser Stelle könnte auch ein anderer Font ausgewählt werden.

Was es mit der langen Zeile `textarea:focus, ...` auf sich hat, wird hier
eines Tages nachgetragen werden.
Zumindest werden diese aufgezählten Elemente alle ohne Rahmen und ohne
Box-Schatten dargestellt.

Mit `!important` werden wichtige Deklarationen dargestellt, die nicht durch spätere Angaben überschrieben werden sollen ([weiterführende Erklärung](https://wiki.selfhtml.org/wiki/CSS/Tutorials/Einstieg/Kaskade#.21important)).

Überschriftenelemente (Headings) bekommen auch eine Änderung:
````css
h1 {
    color: var(--MAIN-TITLES-TEXT-color);
    font-weight: 600 !important;
}

h2, h3, h4, h5 {
    color: var(--MAIN-TITLES-TEXT-color) !important;
    font-weight: 600 !important;
}
````
Sie werden einheitlich auf Textfarbe und Schriftgewicht eingestellt.
Warum das zwei Abschnitte sind, ist nicht bekannt.
Mit den gegenwärtig gleichen Definitionen könnte ein Abschnitt
````css
h1, h2, h3, h4, h5 {
    color: var(--MAIN-TITLES-TEXT-color);
    font-weight: 600 !important;
}
````
auch ausreichen.

Nach den Überschriften kommen die Hyperlinks dran:
````css
a {
    color: var(--MAIN-LINK-color);
}

a:hover {
    color: var(--MAIN-LINK-HOVER-color);
}

#body a.highlight:after {
    display: block;
    content: "";
    height: 1px;
    width: 0%;
    -webkit-transition: width 0.5s ease;
    -moz-transition: width 0.5s ease;
    -ms-transition: width 0.5s ease;
    transition: width 0.5s ease;
    background-color: var(--MAIN-LINK-HOVER-color);
}

/* https://www.w3schools.com/cssref/sel_class.asp */
a.ftplink {
   color: var(--MAIN-LINK-FTPEDIASITE-color);
}

a.ftplink:hover {
   color: var(--MAIN-LINK-FTPEDIASITE-HOVER-color);
}
````

Generell wird die übliche Linkfarbe mittels
````css
a {
    color: var(--MAIN-LINK-color);
}
````
eingestellt.

Sobald der Mauscursor über dem Link steht, ändert sich die Farbe.
Das wird per
````css
a:hover {
    color: var(--MAIN-LINK-HOVER-color);
}
````
erledigt.

Dann gibt es noch diese Animation mit der dünnen Linie, die sich von links nach
rechts aufbaut (und in umgekehrter Richtung auch wieder verschwindet):
````css
#body a.highlight:after {
    display: block;
    content: "";
    height: 1px;
    width: 0%;
    -webkit-transition: width 0.5s ease;
    -moz-transition: width 0.5s ease;
    -ms-transition: width 0.5s ease;
    transition: width 0.5s ease;
    background-color: var(--MAIN-LINK-HOVER-color);
}
````
Speziell im Bereich ftpedia (derzeit nur dort, aber nicht zwingend darauf
beschränkt) sollen einige Links nicht in Standardfarbe sondern mit eigener
Farbdefinition dargestellt werden.
Das geht leider nicht direkt, aber per Klassendefiniton `ftplink`:
````css
a.ftplink {
   color: var(--MAIN-LINK-FTPEDIASITE-color);
}
````
Baugleich wird auch die Farbe bei 'hover' umgestellt:
````css
a.ftplink:hover {
   color: var(--MAIN-LINK-FTPEDIASITE-HOVER-color);
}
````
An entsprechender Stelle steht dann nicht einfach nur `<a href = " ...`
sondern `<a class = "ftplink" href = " ...`, und schwupps sieht dieser Link
entsprechend anders aus - in diesem Fall die Textfarbe.
Alle weiteren Eigenschaften eines Hyperlinks bleiben aber erhalten und so gibt
es sogar weiterhin die Animation mit der Linie.

Ankerpunkte sind diese kleinen Symbole hinter diveren Überschriften, die
eigene URLs innerhalb einer längeren Seite darstellen.
Mit den Zeilen
````css
.anchor {
    color: var(--MAIN-ANCHOR-color);
}
````
wird deren Farbe angepasst.

Weitere Feinheiten betreffen dann die Dekoration.
Aktuell gibt es da nur die horizontale Trennlinie, 'horizontal ruler':
````css
hr {
   border-width: 1px;
   border-style: solid;
   border-color: var(--MAIN-HR-color);
   color: var(--MAIN-TEXT-BG-color);
}
````
Ein Pixel breit, solid und mit einheitlicher Farbe in allen Ecken.
`color` scheint hier allerdings den Hintergrund der Linie zu meinen.

Abschliessend folgt noch ein Schwung Einstellungen, die sich auf das Seitenmenü
links (= `Sidebar`) beziehen.

Hier wurde im vorhandenen Style-Fundus zusätzlich der Font 'entschlankt':
````css
    font-weight: 400 !important; /* Die originalen 300 sind einfach zu mager */
````
Alle anderen Einstellungen beziehen sich auf Farben.

### Darstellung von zwei Abschnitten nebeneinander

Auf der Startseite sollen nebeneinander zwei Ankündigungen erscheinen. 
Dies erreichen wir so:

````
div.announcement {
    display: inline-block;
    width: 48%;
    padding-right: 25px;
    padding-left: 25px;
    vertical-align: top;
}
````
Damit wird die Anordnung jeder Box in einer Zeile (inline) festgelegt.
Durch die Festlegung der Breite auf 48%, also etwas weniger als die Hälfte der Fensterbreite,
passen genau zwei dieser Boxen nebeneinander.


### Einstellungen für den Footer

Auf jeder Seite soll direkt eine Fußzeile mit den Links 
zu Datenschutzerklärung und Impressum zu sehen sein.

Die Einstellungen dafür finden sich in der Definition `#general-footer`.
Wichtig ist hier zunächst die Einstellung `position: fixed`,
die sicherstellt, dass sich das Impressum immer am unteren Bildschirmrand befindet.
Der weiße Hintergrund wird durch `background: white` definiert.
Durch diese Angabe hebt sich die Fußzeile optisch deutlich von eventuellen Inhalten ab.
Die Definition von Schriftgewicht usw. erfolgt in `#general-footer h6`.

Der Inhalt der Fußzeile wird in der Datei `layouts\baseof.html` definiert.
Damit erscheint er automatisch auf allen Seiten.

---

Noch eine Anmerkung zum Schluss:
Wenn eine Definition aus 'unserem' Style Sheet nicht wirksam ist (Tipfehler,
fehlender Font oder andere Dinge) greift die Rückfallebene, die notfalls den
Default des Browsers nimmt.
Die Seite selbst wird aufgebaut, sieht eventuell aber etwas seltsam aus.
Bei Änderungen am Style Sheet empfiehlt es sich die Seite testweise komplett
bauen zu lassen und sich die Änderungen in allen Bereichen genau anzusehen.
