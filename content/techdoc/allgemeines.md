---
title: "Allgemeines"
---



## Über die Inhalte

Die Aufteilung in die Bereiche `/content` und `/layouts` erlaubt es sehr einfach
an den Inhalten zu arbeiten ohne sich um deren Darstellung kümmern zu müssen.
Dateien im Inhaltsbereich `/content` können hinzugefügt, verändert, verschoben,
gelöscht oder nach sonstigem Belieben bearbeitet werden.
Änderungen an den Inhalten werden von hugo umgesetzt und schlagen sich dann
(später) in der produktiven Seite nieder.

_Anleitung: Wie man eine neue ft:pedia (mitsamt Jahrgang) einbaut.  
Anleitung: Wie man einen Download zur ft:pedia einbaut.  
Anleitung: Wie man einen Inhalt in eine andere Kategorie verschiebt.  
Anleitung: Wie man eine Download-Datei austauscht.  
..._



## Über Hugo

hugo ist für die Umwandlung der Inhalte unter `/content` in browsertauglichen
HTML-Code verantwortlich, der dann für den Seitenbesucher von einem
Serverprogramm "ausgeliefert" wird.

*  Hugo erstellt fertige HTML-Dateien und legt diese im Verzeichnis `/public` ab.
*  Der dargestellte Inhalt liegt in Markdown-Dateien (`.md`), die im Verzeichnis
   `/content` und weiteren Unterverzeichnissen liegen.
   Jedes Verzeichnis ergibt einen Menüpunkt, Unterverzeichnisse Untermenüs.  
   In den Markdown-Dateien steht im Header (= Frontmatter) unter anderem
   *  der Titel der Seite
   *  optional `weight`, für die Position im Menü
   *  optional `hidden: true`, dann erscheint die Seite nicht im Menü.
*  Die Templates für die HTML-Dateien liegen in und unterhalb `/layouts`.
   Die Template-Dateien haben die Dateiendung `.html`.
   Im Bedarfsfall entspricht die Verzeichnisstruktur der unter `/content`.
   In diesen Dateien befindet sich neben einem HTML-Grundgerüst auch Go-Code.
   *  [Default-Templates](/techdoc/default_templates/)
      finden sich in `/layouts/_default`.
   *  In `/layouts` kann es für jede `section` (jedes Verzeichnis direkt unter
      `/content`) ein gleichnamiges Unterverzeichnis geben.
      Liegt in diesem Verzeichnis ein passendes Template, so wird das
      <u>vor</u> dem Default-Template verwendet.
   *  Übersichtsseiten (`_index.md`) ziehen sich _immer_ das Layout-Template
      `list.html`.
   *  Normale Seiten ziehen sich das Layout-Template `single.html`.
      Hier gibt es allerdings Unterschiede:
      -  `index.md` zieht _immer_ `single.html`
      -  andere Seiten (z. B. `seite.md`) ziehen das nur, wenn sie selbst
         _kein_ eigenes Layout besitzen, also keinen Eintrag `layout:` im
         Frontmatter haben (z. B. `layout: "file"` zieht `file.html`).
   *  Der Inhalt aus den `.md` wird mit `{{ .Content }}` eingefügt.
   *  Kommentare mit `{{/*   */}}` erscheinen nicht in den fertigen Seiten.
*  Sortierung der Seiten und Menüpunkte: standardmäßig nach Name, sonst nach
   `weight`.
   Bei `weight` ist der niedrigste Wert oben. Er kann auch negativ sein.
   Merke: Leichtes schwimmt oben.
   Es gibt allerdings die Möglichkeit das explizit zu ändern.
   Dafür gibt es dann besondere `list.html` in entsprechenden
   Unterverzeichnissen von `/layout`.
*  Menüpunkte mit Links nach extern, also bei uns im Moment zu Forum, ft-Datenbank und cfw,
    werden in `config.toml` eingetragen.
*  In `config.toml` werden auch Einstellungen für den Markdown-Parser "Blackfriday" gemacht. 
   
   ```
   [blackfriday]
        fractions = false
   ```
   Diese Einstellung bewirkt, dass Zahlen vor und nach dem Schrägstrich `/` nicht als Bruch dargestellt werden.
   
*  Nützliche Funktionen im Code:
   *  Sortieren kann man u. a. nach Titel (`.ByTitle`) oder auch Datum (`.ByDate`).
   *  Die Sortierung kann man mit angehängtem `.Reverse` umdrehen.
   *  Die Funktion `getCSV` liest .csv-Dateien ein, so dass man leicht Tabellen
      bauen und verwalten kann.
      Für die .csv ist einiges zu beachten:
      *  Als Quotes für die Felder sind `"` geeignet.
      *  Wünscht man im Text ein '"', so muss dafür im Feld `""` stehen.
         Beispiel: `Gequotet wird mittels '"'.` => `"Gequotet wird mittels '""'."`
      *  Das erste Feld der Datei darf nicht gequotet sein (Bug im hugo).
      *  Folgender Befehl entfernt die `"` in der gesamten ersten Zeile der csv-Datei:
        `sed '1s/"//g'  ftpedia_Artikeluebersicht.csv > ftpauOhneHk.csv`.


