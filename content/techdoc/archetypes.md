---
title: "archetypes"
---

Archetypes dienen bei Hugo als Schablonen für neue Seiten. 
Dadurch wird beim Aufruf von mit `hugo new <verzeichnis><name>`
eine neue Seite angelegt,
in der die Felder im Frontmatter schon vorhanden
oder sogar schon ausgefüllt sind.


## Archetype "ftpedia.md"
Ein solcher Archetype liefert die Schablone für die einzelnen Ausgaben der ft:pedia.
Da die ft:pedia-Ausgaben alle im Verzeichnis `content/ftpedia` liegen,
heißt der Archetype dafür per Default `ftpedia.md` und liegt im Verzeichnis `archetypes`.
Dieser Archetype erzeugt das [hier](/techdoc/ftpedia/ftpedia/#einzelheft)
beschriebene Frontmatter.

Die Datei für neue ft:pedia-Ausgabe wird mit diesem Archetype durch
`hugo new ftpedia/YYYY/YYYY-n/_index.md` angelegt.
Normalerweise existiert die Datei `_index.md` für den Teaser in diesem
Verzeichnis bereits. In diesem Regelfall  muss vor der Verwendung des
Archetypes das `_index.md` vom Teaser manuell gelöscht werden.
Alternativ wird der Inhalt per Texteditor geändert.

Der Archetyp für ein Einzelheft ist wie folgt definiert:
````md
---
hidden: true
layout: "issue"
title: "{{- $s := split .Name "-" }} {{- print (index $s 1) " / " (index $s 0)}}"
file: "ftpedia-{{ .Name }}.pdf"
publishDate: 
date: {{ .Date }}
uploadBy:
- "ft:pedia-Redaktion"
---
````

Die Variable `.Name` liefert den Teil `YYYY-n` aus dem Pfadnamen, z. B.
"2019-3", obwohl der Name der anzulegenden Datei "_index.md" lautet.
Aus diesem `.Name` lässt sich mit etwas String-Zauberei der String "3 / 2019" erzeugen.
Dabei wird zunächst der Name entlang des Bindestriches zerlegt (`split`),
wodurch ein Array bestehend aus den beiden Elementen "2019" und "3" entsteht.
Diese beiden Elemente werden jetzt von `print` in umgekehrter Reihenfolge
und mit " / " verbunden als Titel ausgegeben.

Der Name der PDF-Datei (`file`) muss unbedingt dem Schema "YYYY-n.pdf" folgen;
sonst wird entweder die Datei nicht gefunden, 
oder der Name muss in der md-Datei händisch geändert werden.

Das Erscheinungsdatum `publishDate` ist nach dem Anlegen der Datei
von Hand auszufüllen.
Diese Angabe berücksichtigt hugo beim Bau der Seite; alles was in der Zukunft
liegt, wird **nicht** gebaut. Zusätzlich wertet unser Script diese Angabe als
Erscheinungsdatum aus sobald die Ausgabe öffentlich wird.

Der Parameter `date` wird durch die Variable `.Date` automatisch mit dem aktuellen Datum belegt.
Dieser Parameter kennzeichnet das Hochladedatum der Datei verwendet. Er wird
derzeit nicht für den Bau der Seite verwendet, hilft aber für eventuelle
Rückfragen wann die Datei bereitgestellt wurde.

Die Vorgabe `uploadBy` zum hochladenden Nutzer rundet das Frontmatter ab.
Auch diese Angabe wird nicht öffentlich dargestellt.

Dieser Archetype ist für alle neuen ft:pedia-Ausgaben geeignet.
Die Parameter im Frontmatter, die sich auf den Import der älteren Ausgaben beziehen,
werden nicht angelegt. Das muss auch nicht sein, da alle älteren Ausgaben
bereits von Hand eingepflegt wurden.


## Weitere Archetypes für ft:pedia
Beim Aufruf von `hugo new` ist es möglich,
mit `-k typ` einen Archetype anzugeben.
Diesen Befehl nutzen wir, um auch für die anderen Files, die für die einzelnen ft:pedia-Ausgaben anfallen,
eigene Archetypes zu verwenden.
Dies sind:  

- `year.md` für die `_index.md` im Jahrgang
- `teaser.md` für einen Teaser
- `download.md` für eine Download-Datei.  

Der Aufruf erfolgt jeweils mit  

- `hugo new -k year ftpedia/YYYY/_index.md`
- `hugo new -k teaser ftpedia/YYYY/YYYY-n/_index.md`
- `hugo new -k download ftpedia/YYYY/YYYY-n/filename.md`  

Die so erzeugten Files für Teaser und Downloads sind allerdings noch nicht
fertig! Sie müssen noch zusätzlich bearbeitet werden.
Beispielsweise muss bei einer Download-Datei der Konstrukteur genannt werden,
die Datei-Endung ist zu setzen und das Datum anzupassen.
In den Content-Bereich kann man noch erklärenden Text schreiben, der allerdings
bei manchen Seiten nicht berücksichtigt wird.
Da der Aufruf von `hugo new` auch ggf. noch fehlende Verzeichnisse erzeugt,
wird durch die Verwendung dieser Archetypes das Hochladen einer ft:pedia-Datei
deutlich erleichtert. Man kann das allerdings auch alles manuell erledigen,
wichtig ist der korrekte Dateipfad und die richtige Belegung der Frontmatter.


### Archetype "teaser.md"

Eine neue ft:pedia-Ausgabe wird ein paar Tage vor ihrem Erscheinen durch einen
Teaser (Appetitmacher) angekündigt. Um das Anlegen der zugehörigen Datei zu
vereinfachen gibt es das Archetyp für den Teaser. Wie bereits erklärt,
erledigt die Kommandozeile
`hugo new -k teaser ftpedia/YYYY/YYYY-n/_index.md`
die Vorarbeit. Bei Bedarf ist vorher noch der neue Jahrgang anzulegen.

Die erzeugte Datei `_index.md` bekommt ihr Frontmatter aus dieser Definition:

````md
---
hidden: true
layout: "teaser"
title: "{{- $s := split .Name "-" }} {{- print (index $s 1) " / " (index $s 0)}}"
launchDate: 
date: {{ .Date }}
uploadBy:
- "ft:pedia-Redaktion"
---
````
Generell wird diese neue Seite nie im Seitenmenü angezeigt (`hidden: true`).
Das Layout ist, wie sollte es auch anders sein, für den Teaser gewählt.
Die Stringmagie für den Titel entspricht genau der aus der Einzelausgabe
(s. o.).
Speziell angelegt wird das Erscheinungsdatum `launchDate`. Da hugo nicht 
wissen kann wann die neue ft:pedia erscheinen wird, muss diese Angabe von Hand
nachgetragen werden sobald die Datei angelegt ist.
Abschließend wird noch das Hochladedatum (`date`) eingestellt und der
verantwortliche Nutzer in `uploadBy` eingetragen.

Dieser Archetyp erzeugt zunächst ein ungültiges Frontmatter!
Es ist zwingend nach dem Anlegen der Datei noch per Texteditor das
Erscheinungsdatum einzutragen. Geschieht dies nicht, verweigert hugo den Bau
der Seite mit einer entsprechenden Fehlermeldung.

Möglicherweise hilft ein geänderter Aufruf weiter, laut
[hugo-Dokumentation](https://gohugo.io/commands/hugo_new/#readout)
lässt sich neuer Inhalt unmittelbar per Texteditor ergänzen:
`hugo new -k teaser ftpedia/YYYY/YYYY-n/_index.md --editor vim`
Im Beispiel musste `vim` herhalten, es geht wohl auch jeder andere Editor, so
vorhanden.

---

Stand: 22. Oktober 2019
