﻿---
title: "Partials"
---
Einige Seitenfunktionen sind so gebaut, dass sie an verschiedenen Stellen
wiederverwendet werden können. Das heißt bei hugo "Partial".

Diese Partials (zumindest die die wir selbst geschrieben haben)
werden in dieser Ecke dokumentiert. Sie liegen alle im Verzeichnis
`themes/website-layout/layouts/partials/`.

*  [Doku zu `download-icon.html`](doku_download-icon)
*  [Doku zu `download-size.html`](doku_download-size)
*  [Doku zu `menu.html`](doku_menu)

---

Stand: 25. Oktober 2019
