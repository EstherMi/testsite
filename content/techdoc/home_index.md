---
title: "Startseiten-Layout"
---

## Aufbau

Die Startseite wird von der Layout-Datei `index.html` zusammengebaut.
Mit `{{.Site.Home.Content}}` wird zunächst der Inhalt von `content\_index.md` eingebaut.

## Nächste Veranstaltung

Um die nächste Veranstaltung anzuzeigen, ist etwas Logik nötig,
die jetzt hier erklärt werden soll.


```` 
{{ $fansSections := where .Site.Pages "Section" "fans" }}
{{ $events := where $fansSections ".Params.termin" "!=" nil }}
{{ $futur := slice }}
{{ range $events }}
    {{ if (time (.Params.termin)).After now }}
        {{ $futur = $futur | append  . }}
    {{ end }}
{{ end }}
{{ if gt (len $futur) 0 }}
    {{ range first 1 ($futur.ByParam "termin") }}
        <b>{{ dateFormat $date_format_string .Params.termin }}</b> 
        <a href="{{- .Permalink -}} ">{{ .Title }}</a> {{ .Params.location }}
        <img src ="fans/veranstaltungen/{{ .Params.flyer }}" width="432" alt="Flyer" >
    {{ end }}
{{ end }}
````

In den ersten zwei Zeilen werden die Seiten gesucht, die Veranstaltungen ankündigen.
Dies sind die Seiten in der Section "fans",
die den Paramter `termin` gesetzt haben.

Interessant sind hier nur die Seiten,
bei denen der Termin in der Zukunft liegt.
Dazu legen wir zunächst einen zunächst leeren Array `$futur` für die Liste der kommenden Veranstaltungen an.
Jetzt hangeln wir uns mit `range` durch den Array `events`.
Wir holen den Parameter `.Params.termin`,
wandeln ihn mit `time` in eine Zeit um,
und gucken dann, ob er nach jetzt (`now`) liegt.
Wenn ja, fügen wir die aktuelle Seite (`.`) dem Array `$futur` hinzu.
Anschließend überprüfen wir, 
ob der Array `$futur` länger als 0 ist.
Es könnte ja sein, dass die letzte Veranstaltung rum ist, 
aber noch keine nächste existiert.
Dann soll der Platz vorerst leer bleiben.

Jetzt hangeln wir uns durch die Liste der kommenden Veranstaltungen, also den Array `$futur`.
Dabei wird der Array mit `byParam` nach dem Parameter `termin` sortiert.
Damit wird sichergestellt, dass wirklich die im Kalender nächste Veranstaltung angekündigt wird.
Aus diesem Array wählen wir mit `first 1` den ersten Eintrag aus.
Der Wert von `termin` wird jetzt in ein hübsches Format gebracht 
und zusammen mit einem Link auf den Titel 
und dem Wert des Parameters `location` sowie einem nicht zu großen Bild dargestellt.

## Neuestes Bild im Bilderpool

Neben der Ankündigung der nächsten Veranstaltung wird noch das neueste Bild im Bilderpool gezeigt.
Die dafür nötige Logik ist nicht so komplex:

````
    {{ $imageSections := where .Site.Pages "Section" "bilderpool" }}
    {{ $images := where $imageSections ".Params.layout" "==" "image" }}
    {{ range first 1 ($images.ByParam "date").Reverse }}
        <b>Neu im Bilderpool</b><p><a href="{{- .Permalink -}} ">{{ .Title }}</a></p>
        <img src ="{{- .File.Dir -}}{{ .Params.picture }}" width="432" alt="Bild" >
    {{ end }}
````

In den ersten beiden Zeilen werden die Seiten aus der Sektion "bilderpool" herausgesucht,
die Bilder sind, also den Parameter `layout` gleich "image" gesetzt haben.
Diese werden jetzt nach dem Parameter `date` sortiert, 
und zwar mit `.Reverse` in der umgekehrten Reihenfolge, also von neu nach alt.
Davon nehmen wir mit `first 1` die neueste Seite.  

Um den Link zu der Seite richtig darzustellen, 
unterdrücken wir mit `{{-` und `-}}` die Leerzeichen vor und nach dem Code.
Der Link zu der Seite ist im Wert `.Permalink` gespeichert,
der Titel in `.Title`.
Der Name des Bild selbst steht in der Variablen `.Params.picture`. 
Außerdem brauchen wir noch den relativen Pfad zu der Bilddatei aus `.File.Dir`.

Die Darstellung von Veranstaltungsankündigung und Bild nebeneinander 
wird mit etwas CSS erreicht, das auf der Seite [ftc Stylesheet](../ftc_stylesheet/#darstellung-von-zwei-abschnitten-nebeneinander) beschrieben wird.
