---
layout: "file"
hidden: true
title: "Bauanleitung ft-Lufthansa-Jet"
date: "2010-02-10T00:00:00"
file: "ft_lufthansajet.pdf"
konstrukteure: 
- "Holger Bernhardt"
uploadBy:
- "Holger Bernhardt"
license: "unknown"
legacy_id:
- /data/downloads/bauanleitungen/ft_lufthansajet.pdf
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/bauanleitungen/ft_lufthansajet.pdf -->
Diese Bauanleitung lag dem Lufthansa-Jet bei