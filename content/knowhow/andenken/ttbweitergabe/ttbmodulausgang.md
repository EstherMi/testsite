---
layout: "file"
hidden: true
title: "TTB-Modul Ausgangsportal / PPB-Module Outlet Portal"
date: "2017-01-28T00:00:00"
file: "ttbmodulausgang.ftm"
konstrukteure: 
- "H.A.R.R.Y."
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/ttbmodulausgang.ftm
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/ttbmodulausgang.ftm -->
Das Beispielportal aus der "Anleitung" (Abb. 7)
The example from the "Instructions" (Fig. 7)