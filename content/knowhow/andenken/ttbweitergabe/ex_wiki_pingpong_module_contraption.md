---
layout: "wiki"
title: "Ping Pong Module Contraption"
date: 2018-02-05T06:37:53
hidden: true
konstrukteure: 
- "H.A.R.R.Y."
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /wiki6b94.html
imported:
- "2019"
---
<!-- https://ftcommunity.de/wiki6b94.html?action=show&topic_id=42-->
<!--
Wiki

Thema: Ping Pong Module Contraption

Aktuelle Version

von: H.A.R.R.Y.

am: 05.02.2018, 06:37:53 Uhr

   Historie:
Version 5 von H.A.R.R.Y. am 29.01.2017, 09:54:17 Uhr
Version 4 von H.A.R.R.Y. am 28.01.2017, 20:38:01 Uhr
Version 3 von H.A.R.R.Y. am 28.01.2017, 20:37:23 Uhr
Version 2 von H.A.R.R.Y. am 28.01.2017, 20:36:27 Uhr
Version 1 von H.A.R.R.Y. am 28.01.2017, 20:35:45 Uhr
-->

The Ping Pong Module Contraption is meant as an open project where everybody is invited to join. Please look also to the [discussion](https://forum.ftcommunity.de/viewtopic.php?f=6&t=3984) in the forum.

Each module is designed and built by a single person or a complete team. The modules are interchangeable against each other. Depending on their arrangement they give a different appeal to the whole contraption. To make this idea work it is necessary to agree and follow some rules.

0. The table's surface serves as point of reference for all height definitions in this document.

0. [Ping pong balls](https://en.wikipedia.org/wiki/Table_tennis#Ball) with a diameter of 40 mm are used as payload to convey. One ball is fed through each 2 s. Thus the throughput rate gets 0,5 /s. The ball's color does not matter.

0. The entrance width WE is at least 60 mm, its height HE is at least 70 mm. Its sill crest SE is somewhere in between 77,5 mm up to 85,0 mm.


![Fig. 1](http://i.imgur.com/IImq1bTm.jpg)
Fig. 1 - Entry side view

0. The outlet width WO does not exceed 60 mm, its height HO does not exceed 70 mm. Its sill crest SO is somewhere in between 87,5 mm up to 92,5 mm.


![Fig. 2](http://i.imgur.com/VToLdvym.png)
Fig. 2 - Outlet side view

0. A rectangular base area is reserved to any standard module. Entrance and outlet are located on opposing sides. The maximum usable spatial depth D is defined to be 540 mm, but there is no need to fully use it. The module width WM is exactly an integer multiple of 390 mm. Entrance and outlet are located symmetrically with respect to the centerline of the base area. This centerline is located at half the spatial depth, D/2 = 270 mm.

![Fig. 3](http://i.imgur.com/ry2vNyym.jpg)
Fig. 3 - Bird's eye view

0. A square base area of edge length D = 540 mm is reserved for a corner module. Neither edge length needs to be used fully. Entrance and outlet are located at adjacent sides. Entrance as well as outlet are located symmetrically to the respective centerline of the base area. Those centerlines are located at half the spatial depth, D/2 = 270 mm, each.

![Fig. 4](http://i.imgur.com/edwfTEUm.png)
Fig. 4 - Left hand corner, bird's eye view

![Fig. 5](http://i.imgur.com/EMAWpbxm.png)
Fig. 5 - Right hand corner, bird's eye view

The rules describing standard modules and corner modules leave room for designing a combined module that is able to convey straight forward or around a corner or even both corners just by a simple configuration. Even forks and joins could be designed on this base but currently this option stays unused.

Fig. 6 and Fig. 7 show just possible examples for entrance and outlet portals. In the examples the portals are build on a base plate [Grundplatte 1000](http://ft-datenbank.de/details.php?ArticleVariantId=3ba4e119-4c38-4a71-a614-fe01ae8e3428). The nearly horizontal, red building plates are tilted by 7,5° using angular pieces. The outer edges are approximately above the outer edge of the base plate.

![Fig. 6](http://i.imgur.com/A56k6IDm.jpg)
Fig. 6 - Entrance portal ([ft-Designer file](../ttbmoduleingang.ftm))

![Fig. 7](http://i.imgur.com/YNO3QUGm.jpg)
Fig. 7 - Outlet portal ([ft-Designer file](../ttbmodulausgang.ftm))

It is not required to use any base plates.
