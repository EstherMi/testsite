---
layout: "file"
hidden: true
title: "TTB-Modul Eingangsportal / PPB-Module Entrance Portal"
date: "2017-01-28T00:00:00"
file: "ttbmoduleingang.ftm"
konstrukteure: 
- "H.A.R.R.Y."
uploadBy:
- "H.A.R.R.Y."
license: "unknown"
legacy_id:
- /data/downloads/ftdesignerdateien/ttbmoduleingang.ftm
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/data/downloads/ftdesignerdateien/ttbmoduleingang.ftm -->
Das Beispielportal aus der "Anleitung" (Abb. 6)
The example from the "Instructions" (Fig. 6)