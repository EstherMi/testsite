---
title: "Links"
weight: 80
---

### Weiterführende Links

* [Xbase++ (Win32) and Clipper (DOS) FischerTechnik Interface Drivers](http://members.chello.nl/r.nooteboom2/)
* [PolarFish](https://sourceforge.net/projects/polarfish/)
    (software to keep computing experimental, computing, plotter / scanner and the famous robot kit from the 80s alive)
* [Elektronik und Mikrocontroller von Burkhard Kainka](http://www.b-kainka.de/index.htm)
* [Elektronik-Kompendium](http://www.elektronik-kompendium.de/)
* [Kinder und Technik, 3D-Druck Arduino fischertechnik](http://www.kinder-technik.de/)


### Bezugsquellen für nützliche Technik

* [Conrad Electronic Shop](https://www.conrad.de/)
* [LEMO-SOLAR](https://lemo-solar.de)
* [OPITEC Bastelshop](https://de.opitec.com/opitec-web/st/Home)
* [Pollin Electronic](https://www.pollin.de/)
* [reichelt elektronik](https://www.reichelt.de/)
* [robototer-teile.de-Shop](http://roboter-teile.de)

Diese Listen sind nicht vollständig und können es nie werden. Sie stellen eine mehr oder weniger zufällige Auswahl dar und bedeuten keine Empfehlung für den Kauf bestimmter Produkte oder bei den genannten Anbietern.

