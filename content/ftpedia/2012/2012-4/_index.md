---
hidden: true
layout: "issue"
title: "4 / 2012"
date: 2012-12-24T00:00:00
file: "ftpedia-2012-4.pdf"
uploadBy:
- "ft:pedia-Redaktion"
legacy_id:
- /ftpedia_ausgaben/ftpedia-2012-4.pdf
imported:
- "2019"
---
<!-- https://www.ftcommunity.de/ftpedia_ausgaben/ftpedia-2012-4.pdf -->

Das hier ist absichtlich noch das alte Frontmatter! Dient zum Prüfen der
Fehlerbehandlung wenn die Scripte umgestellt werden.
