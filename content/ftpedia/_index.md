---
title: "ft:pedia"
weight: 70
layout: "ftpediaAll"
legacy_id:
- ftcomm2409.html
---
<!-- https://www.ftcommunity.de/ftcomm2409.html?file=ftpedia -->
#### ft:pedia - das fischertechnik-Kompendium

Viermal jährlich (immer am letzten Samstag im Quartal bzw. am 24.12.)
erscheint mit ft:pedia eine fischertechnik-Fachzeitschrift von und für
fischertechnik-Fans (und solche, die es werden wollen) - als PDF-Datei zum
kostenlosen Herunterladen (einfach auf die jeweilige Titelseite klicken).
Mit Beiträgen zu technischen Grundlagen, Computing, Elektronik sowie der
Vorstellung von Groß- und Kleinmodellen richtet sich ft:pedia ebenso an
Einsteiger wie an "alte Hasen".
Die Herausgeber freuen sich über [Kommentare, Anregungen und
Verbesserungsvorschläge](http://forum.ftcommunity.de/viewforum.php?f=31) - und
natürlich besonders über Beitragseinreichungen per e-mail an
[ftpedia@ftcommunity.de](mailto:ftpedia@ftcommunity.de?subject=<Betreff einsetzen>&body=Hallo ihr Lieben, beim Stöbern in der ftc ist mir etwas eingefallen. ...).

ft:pedia in der
[Deutschen Nationalbibliothek](https://portal.dnb.de/opac.htm?method=simpleSearch&query=ftpedia): ISSN 2192-5879

Für die mittlerweile recht stattliche Ausgabensammlung gibt es natürlich eine
vollständige [Artikelübersicht](overview).

Zu einigen ft:pedia Artikeln gibt es noch ergänzende Dateien. Diese "Extras"
sind im Jahrgang und beim jeweiligen Heft aufgeführt. Wer zusätzlich eine
Gesamtübersicht über diese Dateien benötigt, wird auf unserer Seite
[Extras in der Übersicht](ftp-extras) fündig.

Hinweis: Jede unentgeltliche Verbreitung der unveränderten und vollständigen
Ausgabe sowie einzelner Beiträge (mit vollständiger Quellenangabe: Autor,
Ausgabe, Seitenangabe ft:pedia) ist nicht nur zulässig, sondern ausdrücklich
erwünscht.
Die Verwertungsrechte aller in ft:pedia veröffentlichten Beiträge liegen bei
den jeweiligen Autoren.
