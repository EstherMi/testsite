---
title: "Übersicht Extras"
hidden: "true"
weight: -8999
layout: "ftp-extras"
legacy_id:
- downloadsa5ef.html
---
<!-- https://www.ftcommunity.de/downloads.html?kategorie=ft%3Apedia+Dateien -->
<!-- https://www.ftcommunity.de/downloadsa5ef.html?kategorie=ft%3Apedia+Dateien -->

Zu einigen ft:pedia-Ausgaben gibt es noch Extra-Dateien.

Ein Klick auf den Spaltenkopf sortiert die gewählte Spalte in aufsteigender
Reihenfolge.
Die Sortierrichtung wird mit dem nächsten Klick auf die Spalte umgedreht.
